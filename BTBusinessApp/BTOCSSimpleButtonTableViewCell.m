//
//  BTOCSSimpleButtonTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSSimpleButtonTableViewCell.h"

@implementation BTOCSSimpleButtonTableViewCell

+ (NSString *)reuseId
{
    return @"BTOCSSimpleButtonTableViewCell";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.button.layer.cornerRadius = 5.0f;
    self.button.layer.borderWidth = 1.0f;
    self.button.layer.borderColor = [BrandColours colourBackgroundBTPurplePrimaryColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
