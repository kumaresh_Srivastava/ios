//
//  BTPublicWifiNetworkNameTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 18/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseTableViewCell.h"

@interface BTPublicWifiNetworkNameTableViewCell : BTBaseTableViewCell
@property (strong, nonatomic) IBOutlet UILabel *networkTypeLabel;
@property (strong, nonatomic) IBOutlet UILabel *networkNameLabel;

- (void)updateCellWithNetworkType:(NSString*)type;
- (void)updateCellWithNetworkName:(NSString*)name;

@end
