#import "SpeedTestUpgradeHubViewController.h"
#import "SpeedTestIntroductionViewController.h"
#import "BTInAppBrowserViewController.h"
#import "BTNavigationViewController.h"
#import "AppManager.h"
#import "OmnitureManager.h"
#import "AppConstants.h"

@interface SpeedTestUpgradeHubViewController ()

#define kBTBusinessSmartHub @"BT Business Smart Hub"
//#define kBTBusinessSmartHub @"Smart Hub"
#define kGuestWifi @"Guest Wi-Fi"
#define k4GAssure @"4G Assure"
#define kSpeedTestGuarantee @"BT’s speed guarantee" //Bug 15530
#define kUpgradeYourHUB @"Upgrade your hub"

#define BTBusinessSmartHubURL @""
#define kGuestWifiURL @""
#define k4GAssureURL @""
#define kSpeedTestGuaranteeURL @""

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *upgradeButtonHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hubImageHeightConstraint;

@property (weak, nonatomic) IBOutlet UIImageView *hubHeaderImage;
@property (weak, nonatomic) IBOutlet UILabel *upgradeLabel;
@property (weak, nonatomic) IBOutlet UILabel *btBusinessSmartHubLabel;
@property (weak, nonatomic) IBOutlet UILabel *mostPowerfulWifiLabel;
@property (weak, nonatomic) IBOutlet UILabel *guestWifiLabel;
@property (weak, nonatomic) IBOutlet UILabel *assureLabel;
@property (weak, nonatomic) IBOutlet UILabel *speedTestGuaranteeLabel;
@property (strong, nonatomic) IBOutlet UIButton *upgradeChatNowButton;

@end

@implementation SpeedTestUpgradeHubViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addURLsToLabels];
    
    // Compress things a little for the iPhone 5
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    if(width <= 320) {
        UIFont *upFont = _upgradeLabel.font;
        _upgradeLabel.font = [upFont fontWithSize:20.0];
        UIFont *smartFont = _btBusinessSmartHubLabel.font;
        _btBusinessSmartHubLabel.font = [smartFont fontWithSize:14.0];
        _upgradeButtonHeightConstraint.constant = 30.0;
    }
    
    self.upgradeChatNowButton.layer.cornerRadius = 5.0f;
    
    if(_unsupportedHub) {
        [self updateUIForUnsupportedHub];
    }
    
    [self trackPageForOmniture];
    [self performSelector:@selector(seNavTitle) withObject:nil afterDelay:0.0];
    
   
}

- (void)seNavTitle{
    
    UILabel* lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 208, 60)];
    lbl.font = [UIFont fontWithName:kBtFontBold size:20.0];
    lbl.textColor = [UIColor whiteColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.text = @"Broadband speed test";
    
    [self.navigationItem setTitleView:lbl];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // check if the back button was pressed
    if (self.isMovingFromParentViewController) {
        // Skip back to the start of the process
        for (UIViewController *vc in self.navigationController.viewControllers) {
            if ([vc isKindOfClass:[SpeedTestIntroductionViewController class]]) {
                [self.navigationController popToViewController:vc animated:NO];
            }
        }
    }
}

- (void) updateUIForUnsupportedHub {
    [_hubHeaderImage setImage:nil];
    _hubImageHeightConstraint.constant = 0.0;
    UIFont *upFont = _upgradeLabel.font;
    _upgradeLabel.font = [upFont fontWithSize:30.0];
    UIFont *smartFont = _btBusinessSmartHubLabel.font;
    _btBusinessSmartHubLabel.font = [smartFont fontWithSize:16.0];
    if(IS_IPHONE_5){
         _btBusinessSmartHubLabel.text = [NSString stringWithFormat:@"%@\n%@", kUpgradeYourHUB, @"With the latest BT Business Smart Hub you can get even more benefits for your business:"];
    } else{
    _btBusinessSmartHubLabel.text = [NSString stringWithFormat:@"%@\n\n%@", kUpgradeYourHUB, @"With the latest BT Business Smart Hub you can get even more benefits for your business:"];
    }
    [self makeURLAndFontForLabel:_btBusinessSmartHubLabel withString:kBTBusinessSmartHub];
    _upgradeLabel.text = @"Sorry, we couldn't test your speed";
    
    _mostPowerfulWifiLabel.text = @"The BT Business Smart Hub has our most powerful wi-fi signal yet";
     [self makeURLForLabel:_mostPowerfulWifiLabel withString:kBTBusinessSmartHub];
    _mostPowerfulWifiLabel.userInteractionEnabled = YES;
    [_mostPowerfulWifiLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(businessHubTapped)]];
    _btBusinessSmartHubLabel.userInteractionEnabled = NO;
}

- (void) addURLsToLabels {
    _mostPowerfulWifiLabel.text = @"Our most powerful wi-fi signal yet";
    
    [self makeURLForLabel:_btBusinessSmartHubLabel withString:kBTBusinessSmartHub];
    [self makeURLForLabel:_guestWifiLabel withString:kGuestWifi];
    [self makeURLForLabel:_assureLabel withString:k4GAssure];
    [self makeURLForLabel:_speedTestGuaranteeLabel withString:kSpeedTestGuarantee];
    
    _btBusinessSmartHubLabel.userInteractionEnabled = YES;
    [_btBusinessSmartHubLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(businessHubTapped)]];
    
    _guestWifiLabel.userInteractionEnabled = YES;
    [_guestWifiLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(guestWifiTapped)]];
    
    _assureLabel.userInteractionEnabled = YES;
    [_assureLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(assureTapped)]];
    
    _speedTestGuaranteeLabel.userInteractionEnabled = YES;
    [_speedTestGuaranteeLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(guaranteeTapped)]];
}

- (void) makeURLAndFontForLabel:(UILabel*)label withString:(NSString*)string {
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:label.text attributes:nil];
   
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:kBtFontRegular size: 22.0f] range:NSMakeRange(0, kUpgradeYourHUB.length)];
        label.attributedText = attributedString;
}


- (void) makeURLForLabel:(UILabel*)label withString:(NSString*)string {
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:label.text attributes:nil];
    int startOfHyperlink = [self findIndexOfWord:string inString:label.text];
    if(startOfHyperlink >= 0) {
        NSRange linkRange = NSMakeRange(startOfHyperlink, [string length]);
        NSDictionary *linkAttributes = @{ NSForegroundColorAttributeName : [BrandColours colourTextBTPurplePrimaryColor]};
        [attributedString setAttributes:linkAttributes range:linkRange];
        label.attributedText = attributedString;
    }
}

- (int)findIndexOfWord:(NSString *)word inString:(NSString *)string {
    NSRange range = [string rangeOfString:word];
    if (range.location == NSNotFound) {
        return -1;
    } else {
        return (int)range.location;
    }
}

+ (SpeedTestUpgradeHubViewController *)getSpeedTestUpgradeHubViewController {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    SpeedTestUpgradeHubViewController *controller = (SpeedTestUpgradeHubViewController *)[storyboard instantiateViewControllerWithIdentifier:@"SpeedTestUpgradeHubViewController"];
    
    return controller;
}

- (IBAction)upgradeHubPressed:(id)sender {
    [AppManager openURL:kUpgradeYourHubUrl];
}

- (void)businessHubTapped {
    [self openURL:BTTargetURLTypeSmartHub];
}

- (void)guestWifiTapped {
    [self openURL:BTTargetURLTypeGuestWifi];
}

- (void)assureTapped {
    [self openURL:BTTargetURLType4GAssureOrder];
}

- (void)guaranteeTapped {
    [self openURL:BTTargetURLTypeSpeedTestGuarantee];
}

- (void) openURL:(BTTargetURLType)url {
    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
    viewController.targetURLType = url;
    [viewController updateInAppBrowserWithIsPresentedModally:NO isComingFromHomeScreen:NO isNeedBackButton:NO andIsNeedCloseButton:YES];
    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{
        
    }];
}

#pragma mark - omniture methods
- (void)trackPageForOmniture
{
    [OmnitureManager trackPage:OMNIPAGE_SPEED_TEST_HUB_3_5_UPGRADE];
}

@end
