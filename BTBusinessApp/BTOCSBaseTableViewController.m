//
//  BTOCSBaseTableViewController.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 13/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSBaseTableViewController.h"

#import <ReactiveCocoa/ReactiveCocoa.h>

@interface BTOCSBaseTableViewController ()

@property (nonatomic, strong) UIView *maskView;

@end

@implementation BTOCSBaseTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.view.backgroundColor = [UIColor colorForHexString:@"#EEEEEE"];
    self.tableView.backgroundColor = [UIColor colorForHexString:@"#EEEEEE"];
    self.tableView.separatorColor = [UIColor colorForHexString:@"#EEEEEE"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;

    
    self.hidesTableWhileLoading = YES;
        
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100.0f;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(backButtonAction)];
    
    [self trackPageForOmniture];
    [self registerNibs];
    [self refreshTableView];

    __weak typeof(self) selfWeak = self;
    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([selfWeak networkRequestInProgress]) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (selfWeak.tableView ) {
                    if (selfWeak.tableCells && (selfWeak.tableCells.count > 0)) {
                        [selfWeak.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
                    }
                    [selfWeak.tableView setScrollEnabled:NO];
                }
                [selfWeak createLoadingView];
                [selfWeak hideLoadingItems:NO];
                [selfWeak.navigationController.navigationBar setUserInteractionEnabled:NO];
                [selfWeak.loadingView startAnimatingLoadingIndicatorView];
            });
        }
        
        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (selfWeak.tableView) {
                    [selfWeak.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
                    [selfWeak.tableView setScrollEnabled:YES];
                }
                [selfWeak hideLoadingItems:YES];
                [selfWeak.navigationController.navigationBar setUserInteractionEnabled:YES];
                [selfWeak.loadingView stopAnimatingLoadingIndicatorView];
                
            });
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - initialiser/superclass methods

- (void)registerNibs
{
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSDetailsHeaderTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSDetailsHeaderTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSSimpleDisclosureTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSSimpleDisclosureTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSRequiredQuestionsPromptTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSRequiredQuestionsPromptTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSRadioSelectTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSRadioSelectTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSSectionHeaderTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSSectionHeaderTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSDropDownTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSDropDownTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSDateSelectTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSDateSelectTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSLargeTextEntryTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSLargeTextEntryTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSNoQueriesTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSNoQueriesTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSSimpleButtonTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSSimpleButtonTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSDateTimeSelectTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSDateTimeSelectTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSFileSelectTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSFileSelectTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSRerquiredQuestionsTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSRerquiredQuestionsTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSProjectEnquiryTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSProjectEnquiryTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSCompletionDateTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSCompletionDateTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSSummaryMilestoneTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSSummaryMilestoneTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSHorizontalSelectorTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSHorizontalSelectorTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSProjectInfoHeaderTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSProjectInfoHeaderTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSSimpleInputTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSSimpleInputTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSProjectDetailsTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSProjectDetailsTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSProjectNotesTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSProjectNotesTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BTBackgroundMessageTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTBackgroundMessageTableViewCell"];
     [self.tableView registerNib:[UINib nibWithNibName:@"BTOCSAdditionalProjectDetailsTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BTOCSAdditionalProjectDetailsTableViewCell"];
}

- (void)setupTableCells
{
    if (!self.tableCells || (self.tableCells.count == 0)) {
        self.tableCells = [[NSMutableArray alloc] initWithCapacity:5];
    }
    // override this method to do inital setup on table cells
}

- (void)updateTableCells
{
    if (!self.tableCells || (self.tableCells.count == 0)) {
        [self setupTableCells];
    }
    // override this method to update table cells without recreating them entirely
    // 
}

- (void)refreshTableView
{
    [self updateTableCells];
    [self.tableView reloadData];
}

#pragma mark - action methods

- (void)backButtonAction
{
    [self trackOmniClick:@"Back"];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger rows = 0;
    if (self.tableCells && self.tableCells.count) {
        rows = self.tableCells.count;
    }
    return rows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = UITableViewAutomaticDimension;
    BTBaseTableViewCell *cell = [self.tableCells objectAtIndex:indexPath.row];
    if (cell && [cell respondsToSelector:@selector(heightForCell)]) {
        height = [cell heightForCell];
    }
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BTBaseTableViewCell *cell = nil;
    if (self.tableCells) {
        cell = [self.tableCells objectAtIndex:indexPath.row];
        cell.preservesSuperviewLayoutMargins = false;
        cell.separatorInset = UIEdgeInsetsZero;
        cell.layoutMargins = UIEdgeInsetsZero;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}


#pragma mark - Private Helper Methods

- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow  {
    
    if(self.retryView)
    {
        self.retryView.hidden = NO;
        self.retryView.retryViewDelegate = self;
        [self.retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
    } else {
        self.retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
        self.retryView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
        self.retryView.retryViewDelegate = self;
        
        [self.view addSubview:self.retryView];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:64.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
        
        [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.tableView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
        [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.tableView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    }
}

- (void)hideRetryItems:(BOOL)isHide {
    // (SD) Hide or Show UI elements related to retry.
    [self.retryView setHidden:isHide];
}


- (void)createLoadingView {
    
    if (!self.maskView) {
        self.maskView = [[UIView alloc] initWithFrame:self.tableView.frame];
        self.maskView.backgroundColor = [UIColor colorForHexString:@"#EEEEEE"];//self.tableView.backgroundColor;
        self.maskView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addSubview:self.maskView];
        
        [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:_maskView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_maskView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_maskView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_maskView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_maskView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_maskView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    }
    
    if(!self.loadingView){
        
        self.loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        self.loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        //self.loadingView.backgroundColor = [[UIColor colorForHexString:@"#333333"] colorWithAlphaComponent:1.0f];
        self.loadingView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.loadingView setFrame:self.tableView.frame];
        
        [self.view addSubview:self.loadingView];
        
        [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    }
}

- (void)hideLoadingItems:(BOOL)isHide {
    // (SD) Hide or Show UI elements related to loading.
    
    if (self.hidesTableWhileLoading) {
        [self.maskView setHidden:isHide];
    } else {
        [self.maskView setHidden:YES];
    }
    [self.loadingView setHidden:isHide];
}

#pragma mark - RetryView Delgates

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    // should be overridden
}

#pragma mark - empty dashboard view

- (void)showEmptyViewWithTitle:(NSString *)titleText andNeedToShowImage:(BOOL)needToShowImage
{
    BTEmptyDashboardView *emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTEmptyDashboardView" owner:nil options:nil] objectAtIndex:0];
    emptyDashboardView.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (needToShowImage)
    {
        [emptyDashboardView updateEmptyDashboardViewWithImageName:@"package" title:titleText detailText:nil andButtonTitle:nil];
    }
    else
    {
        [emptyDashboardView updateEmptyDashboardViewWithImageName:nil title:titleText detailText:nil andButtonTitle:nil];
    }
    
    [self.view addSubview:emptyDashboardView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:emptyDashboardView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:emptyDashboardView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:emptyDashboardView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}

#pragma mark - Omniture Methods

- (NSString*)pageNameForOmnitureTracking
{
    // ahould be overridden
    NSString *pageName = @"";
    return pageName;
}

- (void)trackOmniPage:(NSString *)page
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:[AppManager getOmniDictionary]];
    
    [OmnitureManager trackPage:page withContextInfo:params];
}

- (void)trackPageForOmniture
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = [self pageNameForOmnitureTracking];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackPageForOmnitureWithKeyTask:(NSString *)keyTask
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = [self pageNameForOmnitureTracking];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    [data setValue:keyTask forKey:kOmniKeyTask];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackPageForOmnitureWithProductService:(NSString *)product andOrderStatus:(NSString*)status
{
    [self trackPageForOmnitureWithName:[self pageNameForOmnitureTracking] productService:product andOrderStatus:status];
}

- (void)trackPageForOmnitureWithName:(NSString*)pageName productService:(NSString *)product andOrderStatus:(NSString*)status {
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    [data setValue:product forKey:kOmniProductService];
    [data setValue:status forKey:kOmniOrderStatus];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackOmniClick:(NSString *)linkTitle
{
    [self trackOmniClick:linkTitle forPage:[self pageNameForOmnitureTracking]];
}

- (void)trackOmniClick:(NSString *)linkTitle forPage:(NSString *)pageName
{
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle]];
}


@end
