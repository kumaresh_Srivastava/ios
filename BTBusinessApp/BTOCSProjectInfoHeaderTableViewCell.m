//
//  BTOCSProjectInfoHeaderTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 03/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSProjectInfoHeaderTableViewCell.h"

@implementation BTOCSProjectInfoHeaderTableViewCell

+ (NSString *)reuseId
{
    return @"BTOCSProjectInfoHeaderTableViewCell";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateWithPorjectRef:(NSString *)projectRef andProduct:(NSString *)product andSite:(NSString *)site
{
    self.projectRefLabel.text = projectRef;
    
    NSString *productBaseString = [NSString stringWithFormat:@"Product: %@",product];
    NSMutableAttributedString *attributedProductString = [[NSMutableAttributedString alloc] initWithString:productBaseString attributes:@{NSFontAttributeName: [UIFont fontWithName:kBtFontRegular size: 14.0f],NSForegroundColorAttributeName: [UIColor blackColor]}];
    [attributedProductString addAttribute:NSFontAttributeName value:[UIFont fontWithName:kBtFontBold size: 14.0f] range:NSMakeRange(0, 8)];
    self.productLabel.attributedText = attributedProductString;
    
   /* NSString *siteBaseString = [NSString stringWithFormat:@"Site: %@",site];
    NSMutableAttributedString *attributedSiteString = [[NSMutableAttributedString alloc] initWithString:siteBaseString attributes:@{NSFontAttributeName: [UIFont fontWithName:kBtFontRegular size: 14.0f],NSForegroundColorAttributeName: [UIColor blackColor]}];
    [attributedSiteString addAttribute:NSFontAttributeName value:[UIFont fontWithName:kBtFontBold size: 14.0f] range:NSMakeRange(0, 5)];
    self.siteLabel.attributedText = attributedSiteString;*/
   // self.siteLabel.hidden = YES;
}

@end
