//
//  BTOrderSummaryTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 23/11/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface BTOrderSummaryTableViewCell : BTBaseTableViewCell
@property (strong, nonatomic) IBOutlet BTOrderStatusLabel *statusLabel;
@property (strong, nonatomic) IBOutlet UILabel *leftTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *rightTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *leftValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *rightValueLabel;

@end

NS_ASSUME_NONNULL_END
