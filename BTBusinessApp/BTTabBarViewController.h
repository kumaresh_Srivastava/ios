//
//  BTTabBarViewController.h
//  BTBusinessApp
//
//  Copyright © 2019 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTSlideMenuParentViewController.h"
#import "BTHomeNavigationController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BTTabBarViewController : UITabBarController <BTSlideMenuParentViewControllerDelegate, UITabBarControllerDelegate>

+ (BTTabBarViewController *)getViewController;
- (id)initWithHomeNavigationController:(BTHomeNavigationController *)homeNavigationController;

@property (nonatomic, strong) NSDictionary *userDetails;

@end

NS_ASSUME_NONNULL_END
