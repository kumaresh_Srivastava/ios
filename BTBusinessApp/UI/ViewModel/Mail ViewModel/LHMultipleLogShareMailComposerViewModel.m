//
//  LHMultipleLogShareMailModelView.m
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 09/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "LHMultipleLogShareMailComposerViewModel.h"
#import "LHLogFileItem.h"

@implementation LHMultipleLogShareMailComposerViewModel

- (instancetype)initWithArrayOfLogFileItems:(NSArray *)logFileItemArray
{
    self = [super init];
    if(self)
    {
        _logFileItemsArray = logFileItemArray;
    }
    return self;
}

#pragma mark -
#pragma mark Public Methods

- (void)presentMailComposer
{
    // (hd) Settings the mail subject.
    NSString *subjectString = @"BTB Mobile App Logs";

    // (hd) Setting the mail body
    NSString *mailBody = [self shareMailBody];


    // (hd) Setting the file as attachment
    NSMutableArray *mutableArrayOfMailAttachmentItems = [NSMutableArray array];
    for(LHLogFileItem *item in _logFileItemsArray)
    {
        NSString *filePathForAttachment = item.ddLogFileInfo.filePath;
        NSData *fileData = [NSData dataWithContentsOfFile:filePathForAttachment];
        MailAttachmentItem *attachementItem = [[MailAttachmentItem alloc] initWithFileData:fileData mimeType:@"text/plain" andFileName:item.ddLogFileInfo.fileName];
        [mutableArrayOfMailAttachmentItems addObject:attachementItem];
    }

    // (hd) Presenting Mail Composer
    [super presentMailComposerWithSubject:subjectString mailBody:mailBody isMailBodyHTML:YES andArrayOfDataAttachments:[NSArray arrayWithArray:mutableArrayOfMailAttachmentItems]];
}

#pragma mark -
#pragma mark Private Helper Methods

- (NSString *)shareMailBody
{
    // (hd) Loading HTML Template
    NSError *errorWhileLoadingHTMLTemplate = nil;
    NSMutableString *mutableHTMLString = [NSMutableString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"LHMultipleLogShareHTMLTemplate" ofType:@"html"] encoding:4 error:&errorWhileLoadingHTMLTemplate];
    if(errorWhileLoadingHTMLTemplate)
    {
        DDLogError(@"ViewLog: Unable to load the LHMultipleLogShareHTMLTemplate while composing the body of the mail for multiple log files with error as %@.", errorWhileLoadingHTMLTemplate);
        return nil;
    }


    // (hd) Setting up BT Installation ID
    //TODO: (hd) 09-09-2016 Write code to get actual BT Installation ID
    NSString *btInstallationID = @"XXXXXXXXXXXXX";
    [mutableHTMLString replaceOccurrencesOfString:@"<BT_INSTALLATION_ID>" withString:btInstallationID options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableHTMLString length])];


    // (hd) Setting up current logged in username
    //TODO: (hd) 09-09-2016 Write code to get actual current logged in username.
    NSString *currentLoggedInUsername = @"None";
    [mutableHTMLString replaceOccurrencesOfString:@"<CURRENT_USER_ID>" withString:currentLoggedInUsername options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableHTMLString length])];


    return [NSString stringWithString:mutableHTMLString];
}


#pragma mark -
#pragma mark Override For Private Helper Methods

- (NSString *)alertTitleForEmailResultSent
{
    return @"The logs have been shared.";
}

@end
