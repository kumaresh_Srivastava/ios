//
//  LHSingleLogShareMailModelView.h
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 09/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "MailComposerViewModel.h"

@class LHLogFileItem;

@interface LHSingleLogShareMailComposerViewModel : MailComposerViewModel {

}

@property (nonatomic, readonly) LHLogFileItem *logFileItem;

- (instancetype)initWithLogFileItem:(LHLogFileItem *)logFileItem;

- (void)presentMailComposer;

@end
