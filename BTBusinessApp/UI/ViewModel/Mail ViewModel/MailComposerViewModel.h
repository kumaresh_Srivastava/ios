//
//  MailModelView.h
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 09/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@class MailComposerViewModel;




@interface MailAttachmentItem : NSObject {

}

@property (nonatomic, readonly) NSString *mimeType;
@property (nonatomic, readonly) NSString *fileName;
@property (nonatomic, readonly) NSData *fileData;

- (instancetype)initWithFileData:(NSData *)fileData mimeType:(NSString *)mimeType andFileName:(NSString *)fileName;

@end


@protocol MailViewModelDelegate <NSObject>

- (UIViewController *)presentationViewControllerForPresentingMailComposerRequestedByMailComposerViewModel:(MailComposerViewModel *)viewModel;
- (void)mailComposerViewModel:(MailComposerViewModel *)viewModel finishedMailComposerJourneyWithResult:(MFMailComposeResult)result;


@optional
- (NSString *)alertTitleForEmailResultSentAskedByMailComposerViewModel:(MailComposerViewModel *)viewModel;
- (NSString *)alertTitleForEmailResultFailedAskedByMailComposerViewModel:(MailComposerViewModel *)viewModel;
- (NSString *)alertTitleForEmailResultSavedAskedByMailComposerViewModel:(MailComposerViewModel *)viewModel;

@end

@interface MailComposerViewModel : NSObject {

}

@property (nonatomic, weak) id <MailViewModelDelegate> delegate;

+ (BOOL)canEmailBeSentFromThisDevice;

- (void)presentMailComposerWithSubject:(NSString *)subject mailBody:(NSString *)mailBody isMailBodyHTML:(BOOL)isBodyHTML andArrayOfDataAttachments:(NSArray *)arrayOfMailAttachmentObjects;

@end
