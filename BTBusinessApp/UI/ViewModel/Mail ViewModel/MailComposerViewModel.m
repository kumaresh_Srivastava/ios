//
//  MailModelView.m
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 09/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "MailComposerViewModel.h"



@implementation MailAttachmentItem

- (instancetype)initWithFileData:(NSData *)fileData mimeType:(NSString *)mimeType andFileName:(NSString *)fileName
{
    self = [super init];
    if(self)
    {
        _fileData = fileData;
        _mimeType = [mimeType copy];
        _fileName = [fileName copy];
    }
    return self;
}

@end


@interface MailComposerViewModel () <MFMailComposeViewControllerDelegate>

@end

@implementation MailComposerViewModel


#pragma mark -
#pragma mark Public Methods

+ (BOOL)canEmailBeSentFromThisDevice
{
    return [MFMailComposeViewController canSendMail];
}

- (void)presentMailComposerWithSubject:(NSString *)subject mailBody:(NSString *)mailBody isMailBodyHTML:(BOOL)isBodyHTML andArrayOfDataAttachments:(NSArray *)arrayOfMailAttachmentObjects
{
    if([_delegate presentationViewControllerForPresentingMailComposerRequestedByMailComposerViewModel:self] == nil || ![[_delegate presentationViewControllerForPresentingMailComposerRequestedByMailComposerViewModel:self] isKindOfClass:[UIViewController class]])
    {
        DDLogError(@"Unable to present mail composer as there is not valid object of UIViewController to present it.");
        return;
    }


    //TODO: (hd) 09-09-2016 Debg and resolve the console logs that get printed when we present the mail composer.

    MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
    mailComposer.mailComposeDelegate = self;


    // (hd) Settings the mail subject.
    [mailComposer setSubject:subject];



    // (hd) Setting the mail body.
    [mailComposer setMessageBody:mailBody isHTML:isBodyHTML];




    // (hd) Setting the file as attachment
    for(MailAttachmentItem *item in arrayOfMailAttachmentObjects)
    {
        [mailComposer addAttachmentData:item.fileData mimeType:item.mimeType fileName:item.fileName];
    }


    [[_delegate presentationViewControllerForPresentingMailComposerRequestedByMailComposerViewModel:self] presentViewController:mailComposer animated:YES completion:nil];
}

#pragma mark -
#pragma mark MFMailComposer Delegate Methods

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultSent:
        {
            NSString *alertTitle = [self alertTitleForEmailResultSent];
            if([_delegate respondsToSelector:@selector(alertTitleForEmailResultSentAskedByMailComposerViewModel:)])
            {
                alertTitle = [_delegate alertTitleForEmailResultSentAskedByMailComposerViewModel:self];
            }

            UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:alertTitle preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

                [[_delegate presentationViewControllerForPresentingMailComposerRequestedByMailComposerViewModel:self] dismissViewControllerAnimated:YES completion:^{

                    [_delegate mailComposerViewModel:self finishedMailComposerJourneyWithResult:result];
                }];
            }];

            [alert addAction:okAction];

            [controller presentViewController:alert animated:YES completion:^{

            }];

            break;
        }

        case MFMailComposeResultCancelled:
        {
            [[_delegate presentationViewControllerForPresentingMailComposerRequestedByMailComposerViewModel:self] dismissViewControllerAnimated:YES completion:^{

                [_delegate mailComposerViewModel:self finishedMailComposerJourneyWithResult:result];

            }];
            break;
        }

        case MFMailComposeResultFailed:
        {
            NSString *alertTitle = [self alertTitleForEmailResultFailed];
            if([_delegate respondsToSelector:@selector(alertTitleForEmailResultFailedAskedByMailComposerViewModel:)])
            {
                alertTitle = [_delegate alertTitleForEmailResultFailedAskedByMailComposerViewModel:self];
            }

            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:alertTitle preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

                [[_delegate presentationViewControllerForPresentingMailComposerRequestedByMailComposerViewModel:self] dismissViewControllerAnimated:YES completion:^{

                    [_delegate mailComposerViewModel:self finishedMailComposerJourneyWithResult:result];

                }];
            }];

            [alert addAction:okAction];

            [controller presentViewController:alert animated:YES completion:^{

            }];
            break;
        }

        case MFMailComposeResultSaved:
        {
            NSString *alertTitle = [self alertTitleForEmailResultSaved];
            if([_delegate respondsToSelector:@selector(alertTitleForEmailResultSavedAskedByMailComposerViewModel:)])
            {
                alertTitle = [_delegate alertTitleForEmailResultSavedAskedByMailComposerViewModel:self];
            }

            UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:alertTitle preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

                [[_delegate presentationViewControllerForPresentingMailComposerRequestedByMailComposerViewModel:self] dismissViewControllerAnimated:YES completion:^{

                    [_delegate mailComposerViewModel:self finishedMailComposerJourneyWithResult:result];

                }];
            }];
            
            [alert addAction:okAction];
            
            [controller presentViewController:alert animated:YES completion:^{
                
            }];
            
            break;
        }
            
        default:
            break;
    }
    
}

#pragma mark -
#pragma mark Private Helper Methods

- (NSString *)alertTitleForEmailResultSent
{
    return @"The email has been sent.";
}

- (NSString *)alertTitleForEmailResultFailed
{
    return @"Failed to send email. Try Later.";
}

- (NSString *)alertTitleForEmailResultSaved
{
    return @"Email has been saved in drafts.";
}

@end
