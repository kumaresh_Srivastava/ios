//
//  LHMultipleLogShareMailModelView.h
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 09/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "MailComposerViewModel.h"

@interface LHMultipleLogShareMailComposerViewModel : MailComposerViewModel {

}

@property (nonatomic, readonly) NSArray *logFileItemsArray;

- (instancetype)initWithArrayOfLogFileItems:(NSArray *)logFileItemArray;

- (void)presentMailComposer;

@end
