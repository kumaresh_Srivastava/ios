//
//  LHSingleLogShareMailModelView.m
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 09/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "LHSingleLogShareMailComposerViewModel.h"
#import "LHLogFileItem.h"

@implementation LHSingleLogShareMailComposerViewModel

- (instancetype)initWithLogFileItem:(LHLogFileItem *)logFileItem
{
    self = [super init];
    if(self)
    {
        _logFileItem = logFileItem;
    }
    return self;
}

#pragma mark -
#pragma mark Public Methods

- (void)presentMailComposer
{
    // (hd) Settings the mail subject.
    NSString *subjectString = @"BTB Mobile App Log";
    NSArray *fileNameComponents = [self.logFileItem.ddLogFileInfo.fileName componentsSeparatedByString:@"."];
    if(fileNameComponents.count >= 1)
    {
        subjectString = [NSString stringWithFormat:@"BTB Mobile App Log: %@", [fileNameComponents objectAtIndex:0]];
    }


    // (hd) Setting the mail body
    NSString *mailBody = [self shareMailBody];


    // (hd) Setting the file as attachment
    NSString *filePathForAttachment = self.logFileItem.ddLogFileInfo.filePath;
    NSData *fileData = [NSData dataWithContentsOfFile:filePathForAttachment];
    MailAttachmentItem *attachementItem = [[MailAttachmentItem alloc] initWithFileData:fileData mimeType:@"text/plain" andFileName:self.logFileItem.ddLogFileInfo.fileName];


    // (hd) Presenting Mail Composer
    [super presentMailComposerWithSubject:subjectString mailBody:mailBody isMailBodyHTML:YES andArrayOfDataAttachments:[NSArray arrayWithObject:attachementItem]];
}

#pragma mark -
#pragma mark Private Helper Methods

- (NSString *)shareMailBody
{
    // (hd) Loading HTML Template
    NSError *errorWhileLoadingHTMLTemplate = nil;
    NSMutableString *mutableHTMLString = [NSMutableString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"LHSingleLogShareHTMLTemplate" ofType:@"html"] encoding:4 error:&errorWhileLoadingHTMLTemplate];
    if(errorWhileLoadingHTMLTemplate)
    {
        DDLogError(@"ViewLog: Unable to load the LHSingleLogShareHTMLTemplate while composing the body of the mail for log file with name %@ with error %@", self.logFileItem.ddLogFileInfo.fileName, errorWhileLoadingHTMLTemplate);
        return nil;
    }


    // (hd) Setting up log type.
    NSString *logTypeString = @"None";
    if(self.logFileItem.logFileType == LHLogFileTypeComplete)
    {
        logTypeString = @"Complete";
    }
    else if(self.logFileItem.logFileType == LHLogFileTypeImportant)
    {
        logTypeString = @"Important";
    }
    [mutableHTMLString replaceOccurrencesOfString:@"<LOG_TYPE>" withString:logTypeString options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableHTMLString length])];


    // (hd) Setting up BT Installation ID
    //TODO: (hd) 09-09-2016 Write code to get actual BT Installation ID
    NSString *btInstallationID = @"XXXXXXXXXXXXX";
    [mutableHTMLString replaceOccurrencesOfString:@"<BT_INSTALLATION_ID>" withString:btInstallationID options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableHTMLString length])];


    // (hd) Setting up current logged in username
    //TODO: (hd) 09-09-2016 Write code to get actual current logged in username.
    NSString *currentLoggedInUsername = @"None";
    [mutableHTMLString replaceOccurrencesOfString:@"<CURRENT_USER_ID>" withString:currentLoggedInUsername options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableHTMLString length])];


    // (hd) Setting up creation and modification date of the file
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd' 'HH'-'mm'"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];

    NSString *dateCreationString = [dateFormatter stringFromDate:self.logFileItem.ddLogFileInfo.creationDate];
    [mutableHTMLString replaceOccurrencesOfString:@"<CREATION_DATE>" withString:dateCreationString options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableHTMLString length])];

    NSString *dateModificationString = [dateFormatter stringFromDate:self.logFileItem.ddLogFileInfo.creationDate];
    [mutableHTMLString replaceOccurrencesOfString:@"<MODIFICATION_DATE>" withString:dateModificationString options:NSCaseInsensitiveSearch range:NSMakeRange(0, [mutableHTMLString length])];

    return [NSString stringWithString:mutableHTMLString];
}


#pragma mark -
#pragma mark Override For Private Helper Methods

- (NSString *)alertTitleForEmailResultSent
{
    return @"The log has been shared.";
}

@end
