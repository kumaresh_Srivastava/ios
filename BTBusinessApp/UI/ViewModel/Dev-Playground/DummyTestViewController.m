//
//  DummyTestViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/31/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "DummyTestViewController.h"
#import "NLQueryUpdateContactWebService.h"


@interface DummyTestViewController ()<NLQueryUpdateContactWebServiceDelegate> {
    
}

@property (nonatomic) NLQueryUpdateContactWebService *queryUpdateContactWebService;

@end

@implementation DummyTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)cancelButtonAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)faultDashboardButtonAction:(id)sender
{
    NSDictionary *updatedDic = [NSDictionary dictionaryWithObjectsAndKeys:@"5cd6557138284840", @"Key", @"Abc@1234", @"Password", @"test@byg1.com", @"AlternateEmailAddress", @"Gurpreet", @"FirstName", @"Singh", @"LastName", @"Mr", @"Title", nil];
    
    _queryUpdateContactWebService = [[NLQueryUpdateContactWebService alloc] initWithUpdatedContactDetailsDictionary:updatedDic];
    _queryUpdateContactWebService.queryUpdateContactWebServiceDelegate = self;
    [_queryUpdateContactWebService resume];
}


- (void)queryUpdateContactDetailsSuccesfullyFinishedWithQueryUpdateContactWebService:(NLQueryUpdateContactWebService *)webService
{
    _queryUpdateContactWebService.queryUpdateContactWebServiceDelegate = self;
    _queryUpdateContactWebService = nil;
    
    DDLogInfo(@"Info: _queryUpdateProfileWebService should not be in the memory");


}

- (void)queryUpdateContactWebService:(NLQueryUpdateContactWebService *)webService failedToUpdateContactDetailsWithWebServiceError:(NLWebServiceError *)error
{
    
}



@end
