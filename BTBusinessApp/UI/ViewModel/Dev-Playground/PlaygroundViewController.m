//
//  PlaygroundViewController.m
//  BTBusinessApp
//
//  Created by VectoScalar on 10/27/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "PlaygroundViewController.h"
#import "BTOrderAmendViewController.h"
#import "BTChangeActivationDateViewController.h"
#import "BTViewAssetBACOverviewViewController.h"
#import "BTFaultCallDiversionViewController.h"
#import "BTAssetServicesViewController.h"
#import "BTAssetsServiceDetailViewController.h"
#import "BTFaultAmendViewController.h"
#import "BTAssetsDashboardViewController.h"
#import "BTOrderTrackerGroupOrderSummaryViewController.h"
#import "BTSettingsContactDetailViewController.h"
#import "BTUpdatePINViewController.h"
#import "BTSettingsUpdatePasswordViewController.h"
#import "BTCustomInputAlertView.h"
#import "BTSecurityCheckInterceptorViewController.h"
#import "BTCreateNewPasswordViewController.h"
#import "BTFaultAccessTimeSlotViewController.h"
#import "BTHelpAndSupportViewController.h"
#import "BTServiceStatusDetailsViewController.h"


@interface PlaygroundViewController () {
    
    BTCustomInputAlertView *alerTView;
}

@end

@implementation PlaygroundViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //self.view.backgroundColor = [UIColor purpleColor];
    
   // NSString *expression = @"^((00|\\+)44|0)7(\\d{8}|\\d{9})$";
    
    
    
   // NSString *expression = @"^((([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+(\\.([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+)*)|((\\x22)((((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(([\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f]|\\x21|[\\x23-\\x5b]|[\\x5d-\\x7e]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(\\\\([\\x01-\\x09\\x0b\\x0c\\x0d-\\x7f]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF]))))*(((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(\\x22)))@((([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.)+(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.?$";
    
    NSString *expression = @"^([a-zA-Z\\-]+(. )?[ \\']?)+$";
    NSString *newString = @"mishrarahul";
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
                                                        options:0
                                                          range:NSMakeRange(0, [newString length])];
    
    if (numberOfMatches == 0)
    {

    }
    else
    {
        
    }
    
    
    
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    /*

    
    
    */

}

- (void)showErrorMessage
{
   [alerTView showErrorOnCustomInputWithWithErrro:@"Error asldjkas jdlaks djlkas jdalkd alsdj laksda dj askd"];
}

- (void)removeAlert
{
    [alerTView removeErrorMeesageFromCustomInputAlertView];
  
     [self performSelector:@selector(showLoading) withObject:nil afterDelay:4.0];
    [self performSelector:@selector(hideLoading) withObject:nil afterDelay:6.0];
}

- (IBAction)showInterceptSecurityCheck:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTSecurityCheckInterceptorViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTSecurityCheckInterceptorViewControllerID"];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)hideLoading
{
    [alerTView startLoading];
    [alerTView removeErrorMeesageFromCustomInputAlertView];
    [self performSelector:@selector(showLoading) withObject:nil afterDelay:4.0];

}

- (void)showLoading
{
    [alerTView stopLoading];
    [alerTView showErrorOnCustomInputWithWithErrro:@"sasdsadasdasdasd asida sjlk jlk kjh afhd kjha ljkfha kjdfajk"];
    [self performSelector:@selector(hideLoading) withObject:nil afterDelay:2.0];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)userPressedBACAssetOverView:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTViewAssetBACOverviewViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTViewAssetBACOverviewViewController"];
    [self.navigationController pushViewController:vc animated:YES];

    
}

- (IBAction)amendOrderButtonPressed:(id)sender {
    
    BTOrderAmendViewController *vc = [BTOrderAmendViewController getBTOrderAmendViewController];
    vc.needToPopulateDummyData = YES;
    
    [self.navigationController pushViewController:vc animated:YES];

}

- (IBAction)cancelButtonPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)holidayListAction:(id)sender {
    //2016-11-10 18:30:00 +0000
//    NSDate *currentDate = [NSDate date]
    
    NSDateFormatter *dateFormatter  =   [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *timestampdate = [dateFormatter dateFromString:@"2016-11-10 18:30:00 +0000"];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     BTChangeActivationDateViewController *changeActivationDateViewController = [storyboard instantiateViewControllerWithIdentifier:@"ChangeActivationDateScene"];
    [changeActivationDateViewController setOrderReference:@"BTDNT747"];
    [changeActivationDateViewController setItemReference:@"BTDNT7471-1"];
    [changeActivationDateViewController setCurrentSelectedDate:timestampdate];
//    UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:changeActivationDateViewController];
    
    [self presentViewController:changeActivationDateViewController animated:YES completion:nil];
    
//    activationModel = [[DLMAmendActivationDateScreen alloc] init];
//    [activationModel fetchHolidayListForLoggedInUserWithOrderRef:@"BTTZ9137" itemRef:@"BTTZ91371-1"];
}


- (IBAction)faultCallDiversionAction:(id)sender {

    
    BTServiceStatusDetailsViewController *vc = [BTServiceStatusDetailsViewController getServiceStatusDetailsViewController];
    vc.currentServiceType = BTServiceTypePSTN;
    vc.serviceID = @"012910292";
    [self.navigationController pushViewController:vc animated:YES];
    
    
//    @autoreleasepool {
//        
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        BTFaultCallDiversionViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTFaultCallDiversionViewController"];
//        [self.navigationController pushViewController:vc animated:YES];
//    }
//
//

    
}




- (IBAction)assetServicesAction:(id)sender {
    
    NSMutableArray *_dummyData = [[NSMutableArray alloc] init];
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:@"BT Services",@"ServiceText",@"01234567890",@"ServiceNumber",@"AB101AU",@"installedAt", nil];//@{@"BT Services":@"ServiceText",@"01234567890":@"ServiceNumber",@"AB101AU":@"installedAt"};
    //    NSDictionary *dict1 = @{@"BT Services":@"ServiceText",@"":@"ServiceNumber",@"AB101AU":@"installedAt"};
    NSDictionary *dict1 = [[NSDictionary alloc] initWithObjectsAndKeys:@"BT Services",@"ServiceText",@"",@"ServiceNumber",@"AB101AU",@"installedAt", nil];
    [_dummyData addObject:dict];
    [_dummyData addObject:dict1];
    [_dummyData addObject:dict];
    [_dummyData addObject:dict1];
    [_dummyData addObject:dict];
    [_dummyData addObject:dict];
    [_dummyData addObject:dict];

//    AssetServicesScene
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTAssetServicesViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"AssetServicesScene"];
 
    [vc setTitleString:@"Phone Services"];
    [vc setServicesArray:_dummyData];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nc animated:YES completion:nil];
}


- (IBAction)assetsServiceDetailButtonPressed:(id)sender {
    
    
    
    BTAssetsServiceDetailViewController *vc = [BTAssetsServiceDetailViewController getBTAssetsServiceDetailViewController];
    
    [self.navigationController pushViewController:vc animated:YES];



}

- (IBAction)amendFaultButtonPressed:(id)sender {
    
    BTFaultAmendViewController *controller = [BTFaultAmendViewController getBTFaultAmendViewController];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)contactDetailButtonPressed:(id)sender {
    
    BTSettingsContactDetailViewController *controller = [BTSettingsContactDetailViewController getBTSettingsContactDetailViewController];
    [self.navigationController pushViewController:controller animated:YES];
    
}



- (IBAction)updatePinAction:(id)sender {

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTHelpAndSupportViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTHelpAndSupportViewController"];
    vc.isLaunchingFromHomeScreen = NO;
    [self.navigationController pushViewController:vc animated:YES];



}
- (IBAction)updatePasswordButtonPressed:(id)sender {
    
    /*
    BTSettingsUpdatePasswordViewController *updatePasswordVC = [BTSettingsUpdatePasswordViewController getBTSettingsUpdatePasswordViewController];
    
    [self.navigationController pushViewController:updatePasswordVC animated:YES];
    
     */
    
    BTFaultAccessTimeSlotViewController *controller = [BTFaultAccessTimeSlotViewController getBTFaultAccessTimeSlotViewController];
    
    [self.navigationController pushViewController:controller animated:YES];
    
}
- (IBAction)createNewPasswordButtonPressed:(id)sender {
    
    BTCreateNewPasswordViewController *updatePasswordVC = [BTCreateNewPasswordViewController getBTCreateNewPasswordViewController];
    
    [self.navigationController pushViewController:updatePasswordVC animated:YES];
    
}

@end
