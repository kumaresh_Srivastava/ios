#import "AboutTheAppTableViewController.h"
#import "BTOnBoardingViewController.h"
#import "AppConstants.h"
#import "AppManager.h"
#import "BTOCSSimpleDisclosureTableViewCell.h"
#import "AboutTheAppTableViewCell.h"
#import "OmnitureManager.h"

#define kBTAboutTheAppCell @"BTOCSSimpleDisclosureTableViewCell"
#define kBTHeaderAboutTheAppCell @"AboutTheAppTableViewCell"

@interface AboutTheAppTableViewController () <BTOnBoardingViewControllerDelegate>
- (void)trackPageForOmniture;
- (void)trackHowToUseAppCellTapActionForOmniture;
- (void)trackTermsAndConfitionsCellTapActionForOmniture;
- (NSMutableDictionary *)getDirWithLoginStatus;
@end

@implementation AboutTheAppTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self trackPageForOmniture];
    
    self.title = @"About the app";

    UINib *nib = [UINib nibWithNibName:kBTAboutTheAppCell bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kBTAboutTheAppCell];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UINib *headerNib = [UINib nibWithNibName:kBTHeaderAboutTheAppCell bundle:nil];
    [self.tableView registerNib:headerNib forCellReuseIdentifier:kBTHeaderAboutTheAppCell];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        AboutTheAppTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kBTHeaderAboutTheAppCell];
        NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.label.text = version;
        
        return cell;
    }
    else {
        BTOCSSimpleDisclosureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kBTAboutTheAppCell];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.separatorLineView.hidden = NO;
        
        if (indexPath.row == 1) {
            cell.label.text = @"How to use the app";
        }
        else {
            cell.label.text = @"Terms and conditions";
        }
        return cell;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 1) {
        [self trackHowToUseAppCellTapActionForOmniture];
        
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];
        BTOnBoardingViewController *onBoardingViewController = (BTOnBoardingViewController *)[storyboard instantiateViewControllerWithIdentifier:@"OnBoardingScene"];
        onBoardingViewController.onBoardingDelegate = self;
        [onBoardingViewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:onBoardingViewController animated:YES completion:^{
            
        }];
    }
    else if(indexPath.row == 2) {
        [self trackTermsAndConfitionsCellTapActionForOmniture];
        
        [AppManager openURL:kTermsAndConditionsUrl];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 200;
    }
    return 53;
}

#pragma mark - BTOnBoardingViewControllerDelegate

- (void)userTappedOnLastOnboardingScreenOfOnBoardingViewController:(BTOnBoardingViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:^{

    }];
}

#pragma mark - omniture methods
- (void)trackPageForOmniture
{
    [OmnitureManager trackPage:OMNIPAGE_ABOUT_APP withContextInfo:[self getDirWithLoginStatus]];
}

- (void)trackHowToUseAppCellTapActionForOmniture {
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",OMNIPAGE_ABOUT_APP, OMNICLICK_HOW_TO_USE_MY_BT] withContextInfo:[self getDirWithLoginStatus]];
}

- (void)trackTermsAndConfitionsCellTapActionForOmniture {
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",OMNIPAGE_ABOUT_APP, OMNICLICK_TERMS_AND_CONDITIONS] withContextInfo:[self getDirWithLoginStatus]];
}

- (NSMutableDictionary *)getDirWithLoginStatus {
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    return data;
}


@end
