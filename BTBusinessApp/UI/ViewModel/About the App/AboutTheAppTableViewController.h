//
//  AboutTheAppTableViewController.h
//  BTBusinessApp
//
//  Created by Manik on 22/07/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AboutTheAppTableViewController : UITableViewController

@end

NS_ASSUME_NONNULL_END
