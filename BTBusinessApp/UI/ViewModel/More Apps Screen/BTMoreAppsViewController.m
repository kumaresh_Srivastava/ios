//
//  BTMoreAppsViewController.m
//  BTBusinessApp
//
//  Created by Manik on 05/03/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import "BTMoreAppsViewController.h"
#import "MoreAppsTableViewCell.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import <StoreKit/SKStoreProductViewController.h>

#define kBTMoreAppsCell @"MoreAppsTableViewCell"


@interface BTMoreAppsViewController () <UITableViewDataSource, UITableViewDelegate,SKStoreProductViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *moreAppsTableView;
@property (strong, nonatomic) NSMutableArray *moreAppsTitleArray;
@property (strong, nonatomic) NSMutableArray *moreAppsDescriptionArray;
@property (strong, nonatomic) NSMutableArray *moreAppsImageArray;
@property (strong, nonatomic) NSMutableArray *urlPathArray;


@end

@implementation BTMoreAppsViewController

#pragma mark - View methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // (LP) Navigation bar appearence
    //[BTUICommon updateNavigationBar:self.navigationController];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon_Home_White"] style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonAction)];
    
    self.title = @"Our Apps";
    
    UINib *nib = [UINib nibWithNibName:kBTMoreAppsCell bundle:nil];
    [self.moreAppsTableView registerNib:nib forCellReuseIdentifier:kBTMoreAppsCell];
    self.moreAppsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    self.moreAppsTableView.rowHeight = UITableViewAutomaticDimension;
    self.moreAppsTableView.estimatedRowHeight = 100;

    [self loadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];

    [OmnitureManager trackPage:OMNIPAGE_MORE_APPS withContextInfo:data];
}

- (void)loadData {
    
    _moreAppsTitleArray = [[NSMutableArray alloc]initWithObjects:@"BT Sport",@"BT Wi-fi",@"BT Cloud Voice Express",nil];
    _moreAppsDescriptionArray = [[NSMutableArray alloc]initWithObjects:@"Watch all the latest action live and on the go.",@"Get free access to over 5 million UK wi-fi hotspots.",@"The easy way to use your BT Cloud Voice Express service on the go.",nil];
    _moreAppsImageArray = [[NSMutableArray alloc]initWithObjects:@"BTSportAppLogo",@"BTWifiAppLogo",@"BTCloudVoiceAppLogo",nil];
    //_urlPathArray = [[NSMutableArray alloc]initWithObjects:@"https://itunes.apple.com/gb/app/bt-sport/id669428471",@"https://itunes.apple.com/gb/app/bt-wi-fi/id384404559",@"https://itunes.apple.com/us/app/bt-cloud-voice-express/id1457358475?mt=8",nil];
    
    _urlPathArray = [[NSMutableArray alloc]initWithObjects:@"669428471",@"384404559",@"1457358475",nil];
}

- (void)openStoreProductViewControllerWithITunesItemIdentifier:(NSInteger)iTunesItemIdentifier {
    
    SKStoreProductViewController *storeViewController = [[SKStoreProductViewController alloc] init];
    storeViewController.delegate = self;
    
    NSNumber *identifier = [NSNumber numberWithInteger:iTunesItemIdentifier];
    
    NSDictionary *parameters = @{ SKStoreProductParameterITunesItemIdentifier:identifier };
    [storeViewController loadProductWithParameters:parameters completionBlock:nil];
    
    [self presentViewController:storeViewController animated:YES completion:nil];
    
}

- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - table view datasource methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_moreAppsTitleArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MoreAppsTableViewCell *moreAppCell = [tableView dequeueReusableCellWithIdentifier:kBTMoreAppsCell];
    moreAppCell.selectionStyle = UITableViewCellSelectionStyleNone;
    moreAppCell.titleLabel.text = [_moreAppsTitleArray objectAtIndex:indexPath.row];
    moreAppCell.descriptionLabel.text = [_moreAppsDescriptionArray objectAtIndex:indexPath.row];
    [moreAppCell.btnMoreAppLogo setImage:[UIImage imageNamed:[_moreAppsImageArray objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
    [moreAppCell.actionButton addTarget:self action:@selector(moreAppButtonDownloadAction:) forControlEvents:UIControlEventTouchUpInside];
    moreAppCell.actionButton.tag = indexPath.row;

//    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[_urlPathArray objectAtIndex:indexPath.row]]]) {
//        // app is installed
//        [moreAppCell.actionButton setTitle:[NSString stringWithFormat:@"Open"] forState:UIControlStateNormal];
//        [moreAppCell.actionButton setTitleColor:[UIColor colorWithRed:0.392 green:0.0 blue:0.67 alpha:1] forState:UIControlStateNormal];
//        moreAppCell.containerButtonView.backgroundColor = [UIColor colorWithRed:0.392 green:0.0 blue:0.67 alpha:1];
//        moreAppCell.innerButtonView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1];
//    }
//    else {
        // app is not-installed yet
        [moreAppCell.actionButton setTitle:[NSString stringWithFormat:@"Download"] forState:UIControlStateNormal];
    [moreAppCell.actionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //[moreAppCell.actionButton setTitleColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1] forState:UIControlStateNormal];

//        moreAppCell.containerButtonView.backgroundColor = [UIColor clearColor];
//        moreAppCell.innerButtonView.backgroundColor = [UIColor blueColor];
//        moreAppCell.innerButtonView.backgroundColor = [UIColor colorWithRed:0.392 green:0.0 blue:0.67 alpha:1];

//    }
    
    return moreAppCell;
}

#pragma mark - table view delegate methods

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}


#pragma mark - Action methods

-(void)moreAppButtonDownloadAction:(id)sender {
    
    UIButton *moreAppButton = (UIButton *)sender;
    
    if ([moreAppButton tag] == 0){ //"BT Sport
        [self trackOmnitureEvent:@"BT Sport download" forPage:OMNIPAGE_MORE_APPS];
    }
    else if ([moreAppButton tag] == 1) {//@"BT Wi-fi"
        [self trackOmnitureEvent:@"BT Wi-fi download" forPage:OMNIPAGE_MORE_APPS];
    }
    else { //Cloud Voice Express
        [self trackOmnitureEvent:@"Cloud Voice app download" forPage:OMNIPAGE_MORE_APPS];
    }
   // [AppManager openURL:[_urlPathArray objectAtIndex:[moreAppButton tag]]];
    NSInteger iTuneID = [[_urlPathArray objectAtIndex:[moreAppButton tag]] integerValue];
    [self openStoreProductViewControllerWithITunesItemIdentifier:iTuneID];
    

//    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[_urlPathArray objectAtIndex:[moreAppButton tag]]]]) {
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_urlPathArray objectAtIndex:[moreAppButton tag]]]];
//    }
    // else {
    //        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_urlPathArray objectAtIndex:indexPath.row]]];
    
    //  }
}

- (void)doneButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Omniture Tracking
- (void)trackOmnitureEvent:(NSString *)event forPage:(NSString *)page
{
    NSString *pageName = page;
    NSString *linkTitle = event;
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:data];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
