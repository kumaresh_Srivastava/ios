//
//  DLMServiceStatusScreen.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 22/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "DLMServiceStatusScreen.h"
#import "NLServiceStatusDashboardWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "CDCug+CoreDataClass.h"

@interface DLMServiceStatusScreen()<NLServiceStatusDashboardWebServiceDelegate>

@property (nonatomic) NLServiceStatusDashboardWebService *getServiceStatusDashboardWebService;

@end

@implementation DLMServiceStatusScreen


#pragma mark - Public methods

- (void)fetchServiceStatusDashboardDetails {

    self.getServiceStatusDashboardWebService = [[NLServiceStatusDashboardWebService alloc] init];
    self.getServiceStatusDashboardWebService.serviceStatusDashBoardWebServiceDelegate = self;
    [self.getServiceStatusDashboardWebService resume];

}


- (void)cancelServiceStatusDashboardAPIRequest {

    self.getServiceStatusDashboardWebService.serviceStatusDashBoardWebServiceDelegate = nil;
    [self.getServiceStatusDashboardWebService cancel];
    self.getServiceStatusDashboardWebService = nil;
}


- (void)changeSelectedCUGTo:(BTCug *)selectedCug {

    NSManagedObjectContext *context = [AppDelegate sharedInstance].managedObjectContext;
    CDCug *newPersistenceCug = [CDCug newCugInManagedObjectContext:context];
    newPersistenceCug.groupKey = selectedCug.groupKey;
    newPersistenceCug.cugName = selectedCug.cugName;
    newPersistenceCug.refKey = selectedCug.refKey;
    newPersistenceCug.cugRole = @(selectedCug.cugRole);
    newPersistenceCug.indexInAPIResponse = @(selectedCug.indexInAPIResponse);

    // Get the currently logged in User
    CDUser *loggedInUser = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    CDCug *previouslySelectedCug = loggedInUser.currentSelectedCug;
    loggedInUser.currentSelectedCug = newPersistenceCug;

    if(previouslySelectedCug) {

        [context deleteObject:previouslySelectedCug];
    }

    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
}



#pragma mark - NLAssetsDashBoardWebServiceDelegate Methods
- (void)getServiceStatusDashboardWebService:(NLServiceStatusDashboardWebService *)webService successfullyFetchedServiceStatusDashBoardData:(NSArray *)assets {

    if(webService == self.getServiceStatusDashboardWebService) {

        _assetsArray = assets;
        [self.serviceStatusDashboardScreenDelegate successfullyFetchedServiceStatusDashboardDataOnDLMServiceStatusDashboardScreen:self];

        self.getServiceStatusDashboardWebService.serviceStatusDashBoardWebServiceDelegate = nil;
        self.getServiceStatusDashboardWebService = nil;
    }
    else {

        DDLogError(@"This delegate method gets called because of success of an object of NLServiceStatusDashboardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLServiceStatusDashboardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }

}


- (void)getServiceStatusDashBoardWebService:(NLServiceStatusDashboardWebService *)webService failedToFetchServiceStatusDashBoardDataWithWebServiceError:(NLWebServiceError *)webServiceError {

    if(webService == self.getServiceStatusDashboardWebService) {

        [self.serviceStatusDashboardScreenDelegate serviceStatusDashboardScreen:self failedToFetchServiceStatusDashboardDataWithWebServiceError:webServiceError];

        self.getServiceStatusDashboardWebService.serviceStatusDashBoardWebServiceDelegate = nil;
        self.getServiceStatusDashboardWebService = nil;
    }
    else {
        
        DDLogError(@"This delegate method gets called because of failure of an object of NLServiceStatusDashboardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLServiceStatusDashboardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}




@end
