//
//  BTServiceStatusDetailsViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 2/22/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTServiceStatusDetailsViewController.h"
#import "BTServiceStatusTableViewCell.h"
#import "BTServiceStatusTakeActionTableViewCell.h"

@interface BTServiceStatusDetailsViewController () <UITableViewDelegate, UITableViewDataSource> {
    
}

@property (weak, nonatomic) IBOutlet UITableView *serviceStatusDetailsTableView;
@property (weak, nonatomic) IBOutlet UIImageView *headerServiceIconImageView;

@end

@implementation BTServiceStatusDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //[self setEdgesForExtendedLayout:UIRectEdgeNone];
    self.navigationController.navigationBar.barTintColor = [UIColor greenColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor greenColor];
    self.navigationController.navigationBar.translucent = NO;
    
    self.title = @"Business PSTN service";
    
    
    // Service status table view setup
    self.serviceStatusDetailsTableView.delegate = self;
    self.serviceStatusDetailsTableView.dataSource = self;
    self.serviceStatusDetailsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.serviceStatusDetailsTableView.estimatedRowHeight = 50;
    self.serviceStatusDetailsTableView.rowHeight = UITableViewAutomaticDimension;
    
    UINib *dataCell = [UINib nibWithNibName:@"BTServiceStatusTableViewCell" bundle:nil];
    UINib *actionCell = [UINib nibWithNibName:@"BTServiceStatusTakeActionTableViewCell" bundle:nil];
    [self.serviceStatusDetailsTableView registerNib:dataCell forCellReuseIdentifier:@"BTServiceStatusTableViewCell"];
    [self.serviceStatusDetailsTableView registerNib:actionCell forCellReuseIdentifier:@"BTServiceStatusTakeActionTableViewCell"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
    self.navigationController.navigationBar.barTintColor = [UIColor greenColor];
    self.navigationController.navigationBar.translucent = NO;
    
    [self resizeTableViewHeaderToFit];
    
}

/*
 Resize header view of table to update height of header view
 */
- (void)resizeTableViewHeaderToFit {
    UITableViewHeaderFooterView *headerView = (UITableViewHeaderFooterView *)self.serviceStatusDetailsTableView.tableHeaderView;
    
    [headerView setNeedsLayout];
    [headerView layoutIfNeeded];
    
    CGFloat height = [headerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CGRect frame = headerView.frame;
    frame.size.height = height;
    headerView.frame = frame;
    
    self.serviceStatusDetailsTableView.tableHeaderView = headerView;
    
}

#pragma mark - Class Methods

+ (BTServiceStatusDetailsViewController *)getServiceStatusDetailsViewController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"OptimiseServiceStatus" bundle:nil];
    BTServiceStatusDetailsViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"BTServiceStatusDetailsViewController"];
    
    return controller;
}

#pragma mark - Table View Datasource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 1;
    }
    else
    {
        return 2;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        BTServiceStatusTableViewCell *serviceStatusCell = [tableView dequeueReusableCellWithIdentifier:@"BTServiceStatusTableViewCell"];
        return serviceStatusCell;
    }
    else
    {
        BTServiceStatusTakeActionTableViewCell *serviceStatusTakeActionCell = [tableView dequeueReusableCellWithIdentifier:@"BTServiceStatusTakeActionTableViewCell"];
        return serviceStatusTakeActionCell;
    }
}

@end
