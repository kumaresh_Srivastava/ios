//
//  BTSplashViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 13/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTSplashViewController.h"
#import "AppConstants.h"
#import "AppManager.h"
#import "OmnitureManager.h"
#import "JailbreakDetection.h"
@implementation BTSplashViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleVersionCheckCompletedWithNotification:) name:kNotificationVersionCheckCompleted object:nil];
    
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.constraintSnakeBarHeight.constant = 35 + self.bottomLayoutGuide.length;
    
    self.snakeBarBottomConstraint.constant = - self.constraintSnakeBarHeight.constant;
        
    [self checkForVersionUpdate];
}

- (void)makeCheckVersionUpdateApiCall
{
    if ([AppManager isInternetConnectionAvailable])
    {
        // [self hideAnimatedSnakeBar];
        [self checkForVersionUpdate];
    }
    else
    {
        [self showAnimatedSnakeBarWithMessage:@"No connection"];
        [AppManager trackNoInternetErrorBeforeLoginOnPage:OMNIPAGE_LAUNCH];
    }
}

-(void)showJailbreakMessage {
    UIAlertController *alert=[ UIAlertController alertControllerWithTitle:@"Jailbreak detected" message:@"This app is not supported on jailbroken devices. Please restore or update your device then launch the app again." preferredStyle:UIAlertControllerStyleActionSheet];
    alert.popoverPresentationController.sourceRect = self.errorMessageLabel.frame;
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.canOverlapSourceViewRect = NO;
    [self presentViewController:alert animated:YES
                     completion:nil];
}


- (void)handleVersionCheckCompletedWithNotification: (NSNotification*)notification
{
    if (!notification.object && !isJailbroken())
    {
        [self.delegate initialIntroAnimationFinishedBySplashViewController:self];
    } else if(isJailbroken())
    {
        [self showJailbreakMessage];
    }
    else
    {
        NSString *errorMessage = [notification.object valueForKey:@"error"];
        [self showAnimatedSnakeBarWithMessage:errorMessage];
        [AppManager trackGenericAPIErrorBeforeLoginOnPage:OMNIPAGE_LAUNCH];
//#warning bypass forceupdate failure in test environment
//        if (kBTServerType == BTServerTypeModelA) {
//            [self.delegate initialIntroAnimationFinishedBySplashViewController:self];
//        }
    }
}

- (IBAction)tryAgainButtonAction:(id)sender {
    [self hideAnimatedSnakeBar];
    [self makeCheckVersionUpdateApiCall];
}

-(void)showAnimatedSnakeBarWithMessage: (NSString*)errorMessage
{
    self.errorMessageLabel.text = errorMessage;
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.5 animations:^{
        self.snakeBarBottomConstraint.constant = 0.0;
        [self.view layoutIfNeeded];
    }];
    [UIView commitAnimations];
}

- (void)hideAnimatedSnakeBar
{
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.5 animations:^{
        self.snakeBarBottomConstraint.constant = - self.constraintSnakeBarHeight.constant;
        [self.view layoutIfNeeded];
    }];
    [UIView commitAnimations];
}
@end
