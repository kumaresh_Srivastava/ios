//
//  BTSplashViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 13/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTVersionCheckViewController.h"

@class BTSplashViewController;

@protocol BTSplashViewControllerDelegate <NSObject>

- (void)initialIntroAnimationFinishedBySplashViewController:(BTSplashViewController *)viewController;

@end

@interface BTSplashViewController : BTVersionCheckViewController

@property (nonatomic, weak) id<BTSplashViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *snakeBarBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintSnakeBarHeight;

@property (weak, nonatomic) IBOutlet UILabel *errorMessageLabel;

- (IBAction)tryAgainButtonAction:(id)sender;

@end
