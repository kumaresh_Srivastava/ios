//
//  DLMAssetBacOverviewScreen.m
//  BTBusinessApp
//
//  Created by vectoscalar on 09/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMAssetBacOverviewScreen.h"
#import "NLAssetBacOverviewWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "NLConstants.h"
#import "CDCug+CoreDataClass.h"
#import "NSObject+APIResponseCheck.h"

#import "BTAsset.h"
#import "BTAssetModel.h"
#import "BTAssetDetailCollection.h"
#import "BTAssetCombinedRental.h"

@interface DLMAssetBacOverviewScreen () <NLAssetsBacOverviewWebServiceDelegate>

@property (nonatomic) NLAssetBacOverviewWebService *getAssetsBacOverviewWebService;

@end

@implementation DLMAssetBacOverviewScreen
@synthesize assetBacOverviewScreenDelegate;


#pragma mark - Public methods
- (void)fetchAssetsBacOverviewDetailsWithBAC:(NSString*)BAC
{
    self.getAssetsBacOverviewWebService = [[NLAssetBacOverviewWebService alloc] initWithBillingAccountNumber:BAC];
    self.getAssetsBacOverviewWebService.assetsBacViewWebServiceDelegate = self;
    
    [self.getAssetsBacOverviewWebService resume];
    
    //[self fetchOfflineData];
}



- (void)cancelAssetsBacOverviewAPIRequest
{
    self.getAssetsBacOverviewWebService.assetsBacViewWebServiceDelegate = nil;
    [self.getAssetsBacOverviewWebService cancel];
    self.getAssetsBacOverviewWebService = nil;
}



#pragma mark - Web Service Delegate
- (void)getAssetsBacOverviewWebService:(NLAssetBacOverviewWebService *)webService successfullyFetchedAssetsDashBoardData:(BTAsset *)asset
{
    if(webService == self.getAssetsBacOverviewWebService)
    {
        self.btAsset = asset;
        [self.assetBacOverviewScreenDelegate successfullyFetchedAssetsBACDataOnDLMAssetBACOverviewScreen:self];
        
        self.getAssetsBacOverviewWebService.assetsBacViewWebServiceDelegate = nil;
        self.getAssetsBacOverviewWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLAssetBacOverviewWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLAssetBacOverviewWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }

}


- (void)getAssetsBACOverViewWebService:(NLAssetBacOverviewWebService *)webService failedToFetchAssetsBACOverviewDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    if(webService == self.getAssetsBacOverviewWebService)
    {
        [self.assetBacOverviewScreenDelegate assetDashboardScreen:self failedToFetchAssetsBACDataWithWebServiceError:webServiceError];
        
        self.getAssetsBacOverviewWebService.assetsBacViewWebServiceDelegate = nil;
        self.getAssetsBacOverviewWebService = nil;
        
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLAssetBacOverviewWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLAssetBacOverviewWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}


#pragma mark - Private Helper methods
- (void)fetchOfflineData
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"assetJSON" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    
    NSError *error;
    
    NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if([responseObject isValidSMSessionAPIResponseObject])
    {
        BOOL isSuccess = [[responseObject valueForKey:kNLResponseKeyForIsSuccess] boolValue];
        
        if(isSuccess)
        {
            NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
            if([resultDic isKindOfClass:[NSDictionary class]])
            {
                
                
                NSMutableArray *assetModelArray = [NSMutableArray array];
                NSMutableArray *assetDetailCollectionArray = [NSMutableArray array];
                NSMutableArray *assetCombinedRentalArray = [NSMutableArray array];
                
                NSDictionary *assetModelDict = [resultDic valueForKey:@"AssetModels"];
                
                if([assetModelDict isKindOfClass:[NSArray class]])
                {
                    
                    for(NSDictionary *dict in assetModelDict)
                    {
                        //Extract AssetDetailCollection First
                        NSArray *detailCollectionArray = [dict valueForKey:@"AssetDetailCollection"];
                        
                        if([detailCollectionArray isKindOfClass:[NSArray class]])
                        {
                            for(NSDictionary *dict in detailCollectionArray)
                            {
                                
                                //CombinedRentalDesc Extraction
                                NSArray *tempCombinedRentalArray = [dict valueForKey:@"CombinedRentalDesc"];
                                
                                if([tempCombinedRentalArray isKindOfClass:[NSArray class]])
                                {
                                    for(NSDictionary *dict in tempCombinedRentalArray)
                                    {
                                        BTAssetCombinedRental *combinedRental = [[BTAssetCombinedRental alloc] initWithResponseDictionaryFromAssetsBACAPIResponse:dict];
                                        [assetCombinedRentalArray addObject:combinedRental];
                                    }
                                    
                                }
                                
                                
                                
                                BTAssetDetailCollection *assetDetailCollection = [[BTAssetDetailCollection alloc] initWithResponseDictionaryFromAssetsBACAPIResponse:dict];
                                [assetDetailCollectionArray addObject:assetDetailCollection];
                            }
                            
                        }
                        
                        
                        BTAssetModel *assetModel = [[BTAssetModel alloc] initWithResponseDictionaryFromAssetsBACAPIResponse:dict];
                        [assetModelArray addObject:assetModel];
                    }
                }
                
                self.btAsset = [[BTAsset alloc] initWithResponseDictionaryFromAssetsBACAPIResponse:resultDic];
                
                
                
                [self.assetBacOverviewScreenDelegate successfullyFetchedAssetsBACDataOnDLMAssetBACOverviewScreen:self];
                
                
                
                //    [self.delegate getBillSummaryAndChargesWebService:self successfullyFetchedBillSummaryAndChargesDataWithBillSummary:bill billCharges:billCharges andProductCharges:arrayOfProductCharges];
                
            }
            else
            {
                [self.assetBacOverviewScreenDelegate successfullyFetchedAssetsBACDataOnDLMAssetBACOverviewScreen:self];
            }
        }
        
    }
    
}



@end
