//
//  BTViewAssetBACOverviewViewController.m
//  BTBusinessApp
//
//  Created by vectoscalar on 08/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTViewAssetBACOverviewViewController.h"
#import "ViewAssetBACOverviewTableHeaderView.h"
#import "ViewAssetBACOverviewTableViewCell.h"
#import "AppConstants.h"
#import "NLConstants.h"
#import "BTEmptyDashboardView.h"
#import "BTBillDetailViewController.h"

#import "BTAssetModel.h"
#import "BTUICommon.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "NLWebServiceError.h"
#import "AppConstants.h"
#import "AppDelegate.h"
#import "DLMAssetDashboardScreen.h"

#define kTableViewCell @"ViewAssetBACOverviewTableViewCell"
#define kTableViewHeader @"ViewAssetBACOverviewTableHeaderView"
#define kBillDetailScreen @"BTBillDetailViewController"

#define kEstimatedRowHeight 82
#define kEuroSymbol @"£"

#define kAssetBACOverviewScreen @"BTViewAssetBACOverviewViewController"

#define kSectionHeight 40
#define kXPadding 16

#define kSectionTitle @"Services"

#define kNavigationBarHeightWithStatusBar 0



@interface BTViewAssetBACOverviewViewController ()<UITableViewDelegate,UITableViewDataSource,ViewAsserBACObverviewTableHeaderDelegate,DLMAssetBACOverviewScreenDelegate,DLMAssetDashboardScreenDelegate>
{
    BOOL _isUILoaded;
    BTAsset *_btAsset;
    BTAssetModel *_btAssetModel;
    ViewAssetBACOverviewTableHeaderView *_headerView;
    BTEmptyDashboardView *_emptyDashboardView;
    BOOL _isAllBACApiRequestDone;
}
@property (strong,nonatomic) DLMAssetBacOverviewScreen *bacOverviewScreenModel;
@property (nonatomic, readwrite) DLMAssetDashboardScreen *assetDashboardScreenModel;
@property (weak, nonatomic) IBOutlet UIView *zeroStateView;

@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end

@implementation BTViewAssetBACOverviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;

    
    //self.view.backgroundColor = [UIColor whiteColor];
    
    if(self.userSelectedBTAsset.assetName && self.userSelectedBTAsset.assetName.length >0){
        NSString* titleStr = @"";
        
        if(self.userSelectedBTAsset.assetName.length >25 && IS_IPHONE_5){
            titleStr = [self.userSelectedBTAsset.assetName substringToIndex:25];
            titleStr = [titleStr stringByAppendingString:@"..."];
        } else if (self.userSelectedBTAsset.assetName.length >32 && IS_IPHONE){
            titleStr = [self.userSelectedBTAsset.assetName substringToIndex:32];
            titleStr = [titleStr stringByAppendingString:@"..."];
        }
        else {
            titleStr = self.userSelectedBTAsset.assetName;
        }
        self.navigationItem.title = titleStr;//self.userSelectedBTAsset.assetName;
    }
    else
        self.navigationItem.title = @"Asset BAC Detail";
    
    //Inititalize DLM
    self.bacOverviewScreenModel = [[DLMAssetBacOverviewScreen alloc] init];
    self.bacOverviewScreenModel.assetBacOverviewScreenDelegate = self;
    
    
    [self configureTableView];

    [self makeAPIRequest];
    
    _isAllBACApiRequestDone = YES;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self trackPageForOmniture:OMNIPAGE_ASSETS_ACCOUNT_ASSETS needToSendLoginStatus:YES] ;
 
    // Do any additional setup after loading the view.
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleRotation) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if(self.isSingleBacCUG == YES){
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationItem.hidesBackButton = YES;
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if(appDelegate.isSingleBacCugBackNavButtonPressed == YES){
            appDelegate.isSingleBacCugBackNavButtonPressed = NO;
            return;
        }
        if(_isAllBACApiRequestDone == NO){
            // Fetch ALLBAC to see if we receive more accounts
            self.assetDashboardScreenModel = [[DLMAssetDashboardScreen alloc] init];
            self.assetDashboardScreenModel.assetDashboardScreenDelegate = self;
            [self fetchAssetsDashboardAPI];
            self.tableView.hidden = YES;
        }
         _isAllBACApiRequestDone = NO;
    }
    
}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    self.networkRequestInProgress = NO;
    [self.bacOverviewScreenModel cancelAssetsBacOverviewAPIRequest];
    [self hideProgressView];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(self.isSingleBacCUG == YES) {
        if (self.isMovingFromParentViewController || self.isBeingDismissed) {
            appDelegate.isSingleBacCugBackNavButtonPressed = YES;
        }
        else{
            appDelegate.isSingleBacCugBackNavButtonPressed = NO;
        }
    } else{
        appDelegate.isSingleBacCugBackNavButtonPressed = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if(!_isUILoaded)
    {
        _isUILoaded = YES;
        [self setTableHeaderView];
    }
    
}


#pragma mark - Private helper methods
- (void)configureTableView
{
    self.tableView.hidden = YES;
    self.zeroStateView.hidden = YES;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = kEstimatedRowHeight;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self registerNibs];
    
}


- (void)registerNibs
{
    UINib *nib = [UINib nibWithNibName:kTableViewCell bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kTableViewCell];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    UIViewController *cloneViewController = (BTViewAssetBACOverviewViewController*)[storyboard instantiateViewControllerWithIdentifier:kAssetBACOverviewScreen];
    [(BTViewAssetBACOverviewViewController*)cloneViewController setUserSelectedBTAsset:_btAsset];
    [self.navigationController pushViewController:cloneViewController animated:NO];
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
    [navigationArray removeObjectAtIndex: [navigationArray count] - 2];
    self.navigationController.viewControllers = navigationArray;
}

- (void)setTableHeaderView
{
    _headerView = [[[NSBundle mainBundle] loadNibNamed:kTableViewHeader owner:self options:nil] firstObject];
    _headerView.delegate = self;
    
    [_headerView updateDataWithAsset:_btAsset];
    
    self.tableView.tableHeaderView = _headerView;
    
    [_headerView setNeedsLayout];
    [_headerView layoutIfNeeded];
    
    //CGFloat height = [_headerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    
    //update the header's frame and set it again
    CGRect headerFrame = _headerView.frame;
    if(kIsMobileServiceAssetsEnabled == YES){
        headerFrame.size.height = 80.0; 
    } else {
        headerFrame.size.height = 145;
    }
    _headerView.frame = headerFrame;
    self.tableView.tableHeaderView = _headerView;

    [_headerView layoutIfNeeded];
}

- (void)handleRotation {
    if(UIDeviceOrientationIsPortrait([[UIDevice currentDevice] orientation])) {
        CGRect headerFrame = _headerView.frame;
        headerFrame.size.height = 150.0f;
        _headerView.frame = headerFrame;
    } else {
        CGRect headerFrame = _headerView.frame;
        headerFrame.size.height = 150.0f;
        _headerView.frame = headerFrame;
    }
}
- (void)fetchAssetsDashboardAPI {
    
    if([AppManager isInternetConnectionAvailable])
    {
        [self hideRetryView];
        self.networkRequestInProgress = YES;
        [self.assetDashboardScreenModel fetchAssetsDashboardDetails];
    }
    else
    {
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_ASSETS_ACCOUNT];
    }
}

- (void)makeAPIRequest
{
    if ([self.userSelectedBTAsset.assetAccountNumber containsString:@"***"])
    {
        [self showZeroStateView];
    }
    else
    {
        if(![AppManager isInternetConnectionAvailable])
        {
            [self showRetryViewshowRetryViewWithInternetStrip:YES];
            [AppManager trackNoInternetErrorOnPage:OMNIPAGE_ASSETS_ACCOUNT_ASSETS];
        }
        else
        {
            [self hideRetryView];
            self.networkRequestInProgress = YES;
            [self.bacOverviewScreenModel fetchAssetsBacOverviewDetailsWithBAC:self.userSelectedBTAsset.assetAccountNumber];
        }
    }
}


- (UIView*)getSectionHeaderView
{
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,[[UIScreen mainScreen] bounds].size.width, kSectionHeight)];
    bgView.backgroundColor = [UIColor colorForHexString:@"EEEEEE"];//[BrandColours colourBtNeutral30];
    
    UIView *topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0,[[UIScreen mainScreen] bounds].size.width, 1)];
    topLine.backgroundColor = [UIColor colorForHexString:@"EEEEEE"];//[BrandColours colourBtNeutral60];
    [bgView addSubview:topLine];
    
    UIView *bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0,kSectionHeight-1,[[UIScreen mainScreen] bounds].size.width, 1)];
    bottomLine.backgroundColor = [UIColor colorForHexString:@"EEEEEE"];//[BrandColours colourBtNeutral60];
    [bgView addSubview:bottomLine];
    
    
    
    UILabel *lblTakeAction = [[UILabel alloc] initWithFrame:CGRectMake(16, 10,2*kSectionHeight, kSectionHeight/2)];
    lblTakeAction.text = kSectionTitle;
    lblTakeAction.font = [UIFont fontWithName:@"BTFont-Bold" size:14.0f];
    lblTakeAction.textColor = [UIColor colorForHexString:@"333333"];//[UIColor darkGrayColor];
    
    [bgView addSubview:lblTakeAction];
    
    return bgView;
}


- (void)removeProgressView
{
    [self setNetworkRequestInProgress:false];
    [self hideProgressView];
}


- (void)manageErrorInAPIRequest
{
    [self showRetryViewshowRetryViewWithInternetStrip:NO];
    [self hideProgressView];
    self.tableView.hidden = YES;
    
}


- (void)showRetryViewshowRetryViewWithInternetStrip:(BOOL)needToShowInternetStrip {
    _isRetryViewShown = YES;
    
    if(!_retryView)
    {
        _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
        _retryView.translatesAutoresizingMaskIntoConstraints = NO;
        _retryView.retryViewDelegate = self;
        
        [_retryView updateRetryViewWithInternetStrip:needToShowInternetStrip];
        
        [self.view addSubview:_retryView];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    }
    
    _retryView.hidden = NO;
    _retryView.retryViewDelegate = self;
    [_retryView updateRetryViewWithInternetStrip:needToShowInternetStrip];
    
}


- (void)showZeroStateView
{
    self.tableView.hidden = YES;
    [self.zeroStateView removeFromSuperview];
    //self.zeroStateView.hidden = YES;
    
    _emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTEmptyDashboardView" owner:nil options:nil] objectAtIndex:0];
    _emptyDashboardView.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSURL *url;
    
    if ([kBTServerType isEqualToString:@"BTServerTypeProd"]) {
        url = [[NSURL alloc] initWithString:@"https://secure.business.bt.com/Account/Addbillingaccounts.aspx"];
    } else{
        url = [[NSURL alloc] initWithString:@"https://eric1-dmze2e-ukb.bt.com/Account/Addbillingaccounts.aspx"];
    }
    
    NSMutableAttributedString *hypLink = [[NSMutableAttributedString  alloc] initWithString:@"Your products and services will show here once your bill is ready to view online. If you have recently moved to a BT OneBill and you can no longer see your products and services, click here to add missing accounts."];
    
    NSDictionary *linkAttributes = @{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle),
                                     NSLinkAttributeName : url};
    
    [hypLink addAttributes:linkAttributes range:NSMakeRange(185,4)];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
    [hypLink addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,hypLink.length)];
    
    [hypLink addAttribute:NSForegroundColorAttributeName value:[BrandColours colourBtNeutral70] range:NSMakeRange(0, hypLink.length)];
    
    [hypLink addAttribute:NSFontAttributeName
                    value:[UIFont fontWithName:kBtFontRegular size:16.0] range:NSMakeRange(0, hypLink.length)];
    
    [_emptyDashboardView updateEmptyDashboardViewWithImageNameAndLink:@"package" title:@"None to display" detailText:hypLink andButtonTitle:nil];
    
    //_emptyDashboardView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_emptyDashboardView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
    [self trackPageForOmniture:OMNIPAGE_ASSETS_EMPTY needToSendLoginStatus:YES] ;
    
    [OmnitureManager trackError:OMNIERROR_NO_ASSET_TO_DISPLAY onPageWithName:OMNIPAGE_ASSETS_ACCOUNT_ASSETS contextInfo:[AppManager getOmniDictionary]];
}

#pragma mark - TableView Delegate/Datasource Methods



- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return  [self getSectionHeaderView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return kSectionHeight;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_btAsset.assetModels count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ViewAssetBACOverviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCell];
    
    BTAssetModel *assetModel = [_btAsset.assetModels objectAtIndex:indexPath.row];
    
    [cell updateDataWithAssetModel:assetModel];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTAssetServicesViewController *assetServicesViewController = (BTAssetServicesViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"BTAssetServicesViewController"];
    BTAssetModel *assetModel = [_btAsset.assetModels objectAtIndex:indexPath.row];
    
    NSString *eventName = [NSString stringWithFormat:@"%@ %@",OMNICLICK_ASSETS_SERVICE_DETAILS,assetModel.name];
     [self trackClickForOmniture:eventName];
    
    assetServicesViewController.titleString = assetModel.name;
    assetServicesViewController.servicesArray = assetModel.assetDetailCollection;
    assetServicesViewController.selectedAsset = _btAsset;
    [self.navigationController  pushViewController:assetServicesViewController animated:YES];
    
}


#pragma mark - ViewAsset BAC Overview Delegate

- (void)userPressedViewLatestBill
{
    [self trackClickForOmniture:OMNICLICK_ASSETS_VIEWBILL];
    
    BTBillDetailViewController *billDetailVC = (BTBillDetailViewController*)[self.storyboard instantiateViewControllerWithIdentifier:kBillDetailScreen];
    billDetailVC.btAsset = _btAsset;

    [self.navigationController pushViewController:billDetailVC animated:YES];
    
}


#pragma mark - Retry View Delegate
- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
   [self makeAPIRequest];
}

#pragma mark - DLMAssetDashboardScreenDelegate ALLBAC API response
- (void)successfullyFetchedAssetsDashboardDataOnDLMAssetDashboardScreen:(DLMAssetDashboardScreen *)assetDashboardScreen
{
    self.networkRequestInProgress = NO;
   
    if(assetDashboardScreen.assetsArray.count > 0)
    {
        NSArray* assetsArray =  [assetDashboardScreen.assetsArray mutableCopy];
        NSArray* cugs = [[AppDelegate sharedInstance] savedCugs];
        if(cugs.count > 1 || assetsArray.count > 1){
            self.isSingleBacCUG = NO;
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            appDelegate.isSingleBacCugBackNavButtonPressed = NO;
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [self makeAPIRequest];
        }
    }
    else
    {
        DDLogError(@"Assets Dashboard: Assets not found.");
    }
    
    
    
}

- (void)assetDashboardScreen:(DLMAssetDashboardScreen *)assetDashboardScreen failedToFetchAssetsDashboardDataWithWebServiceError:(NLWebServiceError *)webServiceError {
    self.networkRequestInProgress = NO;
    [self makeAPIRequest];
}


#pragma mark - DLMAsset BAC Overview Model Delegate
- (void)successfullyFetchedAssetsBACDataOnDLMAssetBACOverviewScreen:(DLMAssetBacOverviewScreen *)assetBACOverViewScreen
{
    _btAsset = assetBACOverViewScreen.btAsset;
    _btAsset.assetName = self.userSelectedBTAsset.assetName;

    if(_btAsset && [_btAsset.assetModels count]>0)
    {
        [self.tableView reloadData];
        [_headerView updateDataWithAsset:_btAsset];
        self.tableView.hidden = NO;
        self.zeroStateView.hidden = YES;
    }
    else
    {
        [self showZeroStateView];
        
        // KUMARESH HACK
        /*BTAssetServicesViewController *assetServicesViewController = (BTAssetServicesViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"BTAssetServicesViewController"];
       
        
         assetServicesViewController.titleString = @"Mobile services";
        //assetServicesViewController.servicesArray = assetModel.assetDetailCollection;
        assetServicesViewController.selectedAsset = _btAsset;
        [self.navigationController  pushViewController:assetServicesViewController animated:YES];
         */
        // KUMARESH HACK END
      
    }
    
    [self performSelector:@selector(removeProgressView) withObject:nil afterDelay:0.001];

}

- (void)assetDashboardScreen:(DLMAssetBacOverviewScreen *)assetBACOverviewScreen failedToFetchAssetsBACDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    [self setNetworkRequestInProgress:false];
    self.networkRequestInProgress = NO;
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
    
    if(errorHandled)
    {
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_ASSETS_ACCOUNT_ASSETS];
    }
    
    
    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {
        
        switch (webServiceError.error.code)
        {
                
            case BTNetworkErrorCodeAPINoDataFound:
            {
                [AppManager trackNoDataFoundErrorOnPage:OMNIPAGE_ASSETS_ACCOUNT_ASSETS];
                [self showZeroStateView];
                errorHandled = YES;
                break;
            }
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }
    if(errorHandled == NO)
    {
        [self manageErrorInAPIRequest];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_ASSETS_ACCOUNT_ASSETS];
    }

}


#pragma mark - Event tracking methods
- (void)trackPageForOmniture:(NSString *)page needToSendLoginStatus:(BOOL)needToSendLogin
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];

    if(needToSendLogin)
        [data setValue:@"Logged In" forKey:kOmniLoginStatus];
   
    [OmnitureManager trackPage:page withContextInfo:data];
}

- (void)trackClickForOmniture:(NSString *)clickEvent
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",OMNIPAGE_ASSETS_ACCOUNT_ASSETS,clickEvent] withContextInfo:data];
}

@end
