//
//  DLMAssetBacOverviewScreen.h
//  BTBusinessApp
//
//  Created by vectoscalar on 09/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
#import "BTAsset.h"

@class DLMAssetBacOverviewScreen;
@class NLWebServiceError;

@protocol  DLMAssetBACOverviewScreenDelegate <NSObject>

- (void)successfullyFetchedAssetsBACDataOnDLMAssetBACOverviewScreen:(DLMAssetBacOverviewScreen *)assetBACOverViewScreen;

- (void)assetDashboardScreen:(DLMAssetBacOverviewScreen *)assetBACOverviewScreen failedToFetchAssetsBACDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface DLMAssetBacOverviewScreen : DLMObject


@property (weak,nonatomic) id <DLMAssetBACOverviewScreenDelegate> assetBacOverviewScreenDelegate;

@property (strong,nonatomic) BTAsset *btAsset;

- (void)fetchAssetsBacOverviewDetailsWithBAC:(NSString*)BAC;
- (void)cancelAssetsBacOverviewAPIRequest;

@end
