//
//  BTViewAssetBACOverviewViewController.h
//  BTBusinessApp
//
//  Created by vectoscalar on 08/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseViewController.h"
#import "DLMAssetBacOverviewScreen.h"
#import "BTAssetServicesViewController.h"

@interface BTViewAssetBACOverviewViewController : BTBaseViewController

@property (strong,nonatomic) BTAsset *userSelectedBTAsset;

@property (assign,nonatomic) BOOL isSingleBacCUG;

@end
