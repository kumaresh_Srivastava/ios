//
//  DLMMobileServiceAssetScreen.m
//  BTBusinessApp
//
//  Created by kumaresh shrivastava on 10/06/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import "DLMMobileServiceAssetDetailScreen.h"
#import "NLMobileAssetsDetailWebService.h"
#import "NLMobileSIMDetailWebService.h"
#import "NSObject+APIResponseCheck.h"



@interface DLMMobileServiceAssetDetailScreen () <NLMobileAssetsDetailWebServiceDelegate,NLMobileSIMDetailWebServiceDelegate> {
     NSArray *_summarySectionArray;
}

@property (nonatomic) NLMobileAssetsDetailWebService *getMobileAssetsDetailWebService;
@property (nonatomic) NLMobileSIMDetailWebService* getMobileSIMDetailWebService;

@end

@implementation DLMMobileServiceAssetDetailScreen

#pragma mark - Public methods

- (void)fetchMobileServiceAssetsDetails:(NSString*)mobileServiceId
{
    self.getMobileAssetsDetailWebService = [[NLMobileAssetsDetailWebService alloc] initWithMobileSubscriptionDetailWithServiceID:mobileServiceId];
    self.getMobileAssetsDetailWebService.mobileAssetsDetailWebServiceDelegate = self;
    [self.getMobileAssetsDetailWebService resume];
    
}

- (void)fetchMobileSIMDetails:(NSString*)mobileServiceId
{
    self.getMobileSIMDetailWebService = [[NLMobileSIMDetailWebService alloc] initWithMobileSIMDetailWithServiceID:mobileServiceId];
    self.getMobileSIMDetailWebService.mobileSIMDetailWebServiceDelegate = self;
    [self.getMobileSIMDetailWebService resume];
    
}

- (void)cancelMobileServiceAssetsDetailsAPIRequest
{
    self.getMobileAssetsDetailWebService.mobileAssetsDetailWebServiceDelegate = nil;
    [self.getMobileAssetsDetailWebService cancel];
    self.getMobileAssetsDetailWebService = nil;
    
    self.getMobileSIMDetailWebService.mobileSIMDetailWebServiceDelegate = nil;
    [self.getMobileSIMDetailWebService cancel];
    self.getMobileSIMDetailWebService = nil;
}


#pragma mark - NLMobileAssetsDetailWebServiceDelegate Methods
- (void)getMobileAssetsDetailWebService:(NLMobileAssetsDetailWebService *)webService successfullyFetchedMobileAssetsDetailData:(NSArray *)asset
{
    if(webService == self.getMobileAssetsDetailWebService)
    {
        self.mobileServiceDataArray = asset;
        [self.mobileAssetDetailScreenDelegate successfullyFetchedMobileAssetsDetailDataOnDLMMobileServiceAssetScreen:self];
        
       // self.getMobileAssetsDetailWebService.mobileAssetsDetailWebServiceDelegate = nil;
        //self.getMobileAssetsDetailWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLAssetsDashboardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLAssetsDashboardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
    
}


- (void)getMobileAssetsDetailWebService:(NLMobileAssetsDetailWebService *)webService failedToFetchMobileAssetsDetailDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    if(webService == self.getMobileAssetsDetailWebService)
    {
        [self.mobileAssetDetailScreenDelegate mobileAssetDetailScreen:self failedToFetchMobileAssetsDetailDataWithWebServiceError:webServiceError];
        
        //self.getMobileAssetsDetailWebService.mobileAssetsDetailWebServiceDelegate = nil;
        //self.getMobileAssetsDetailWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLAssetsDashboardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLAssetsDashboardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

#pragma mark - NLMobileSIMDetailWebServiceDelegate Methods
- (void)getMobileSIMDetailWebService:(NLMobileSIMDetailWebService *)webService successfullyFetchedMobileSIMDetailData:(NSArray *)asset
{
    if(webService == self.getMobileSIMDetailWebService)
    {
        self.mobileSIMDataArray = asset;
        [self.mobileAssetDetailScreenDelegate successfullyFetchedMobileSIMDetailDataOnDLMMobileServiceAssetScreen:self];
        
        //self.getMobileAssetsDetailWebService.mobileAssetsDetailWebServiceDelegate = nil;
        //self.getMobileAssetsDetailWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLAssetsDashboardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLAssetsDashboardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
    
}


- (void)getMobileSIMDetailWebService:(NLMobileSIMDetailWebService *)webService failedToFetchMobileSIMDetailDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    if(webService == self.getMobileSIMDetailWebService)
    {
        [self.mobileAssetDetailScreenDelegate mobileSIMDetailScreen:self failedToFetchMobileSIMDataWithWebServiceError:webServiceError];
        
       // self.getMobileAssetsDetailWebService.mobileAssetsDetailWebServiceDelegate = nil;
        //self.getMobileAssetsDetailWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLAssetsDashboardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLAssetsDashboardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}


- (NSArray*)getDataForMobileAssetsService:(BTAssetDetailCollection*)assetDetailCollectionModel mobileServiceProductSummary:(NSArray*)mobileServiceProductArray mobileServiceSIM:(NSArray*)mobileServiceSIMData {
    
    //Summary tab
    NSMutableArray *summarySectionArray = [NSMutableArray array];
        
    NSMutableArray *summaryWrapperArray = [NSMutableArray array];
    NSMutableArray *summaryDeviceWrapperArray = [NSMutableArray array];
    
    for(BTMobileServiceAssetModel* data in mobileServiceProductArray){
        NSString* dataType = data.type;
        if ( [dataType caseInsensitiveCompare:@"Mobile-Package"] == NSOrderedSame ){
            MobileAssetsDetailRowWrapper *mobilePlanRowWrappper = [[MobileAssetsDetailRowWrapper alloc] init];
            mobilePlanRowWrappper.cellType = MobileServiceDetailRowTypeMobilePlan;//Mobile-Package
            if(data.productDisplayName && [data.productDisplayName validAndNotEmptyStringObject]){
                mobilePlanRowWrappper.mobilePlan = data.productDisplayName;
                
                [summaryWrapperArray addObject:mobilePlanRowWrappper];
            }
            break;
        }
    }
    
    MobileAssetsDetailRowWrapper *contractSummaryRowWrappper = [[MobileAssetsDetailRowWrapper alloc] init];
    contractSummaryRowWrappper.cellType = MobileServiceDetailRowTypeContractDetailSummary;//Mobile-Package
    
    for(BTMobileServiceAssetModel* data in mobileServiceProductArray){
        NSString* dataType = data.type;
        if ( [dataType caseInsensitiveCompare:@"Mobile-Package"] == NSOrderedSame ){
            
            if(data.contractExpiry && [data.contractExpiry validAndNotEmptyStringObject]){
                
                contractSummaryRowWrappper.contractEndDate = data.contractExpiry;
            }
            else{
                contractSummaryRowWrappper.contractEndDate = @"-";
            }
            
            
            
            break;
        }
    }
    
    
    if(assetDetailCollectionModel.contractTerm && [assetDetailCollectionModel.contractTerm validAndNotEmptyStringObject]){
        
        if ([assetDetailCollectionModel.contractTerm integerValue] > 1) {
            contractSummaryRowWrappper.contractTerms = [assetDetailCollectionModel.contractTerm  stringByAppendingString:@" Months"];
        } else {
            contractSummaryRowWrappper.contractTerms = [assetDetailCollectionModel.contractTerm  stringByAppendingString:@" Month"];
        }
        
    }
    else{
        
        contractSummaryRowWrappper.contractTerms = @"-";
    }
   
    for(BTMobileSIMServiceAssetsModel* simData in mobileServiceSIMData){
        NSString* simStatus = simData.status;
        if ( [simStatus caseInsensitiveCompare:@"Active"] == NSOrderedSame ){
            if(simData.simSerialNumber && [simData.simSerialNumber validAndNotEmptyStringObject]){
                contractSummaryRowWrappper.simSerialNumber = simData.simSerialNumber;
            } else {
                contractSummaryRowWrappper.simSerialNumber = @"-";
            }
            break;
        }
    }
    
    if(assetDetailCollectionModel.combinedRent && [assetDetailCollectionModel.combinedRent validAndNotEmptyStringObject]){
        
        contractSummaryRowWrappper.totalRental = assetDetailCollectionModel.combinedRent;
    }
    else{
        contractSummaryRowWrappper.totalRental = @"-";
    }
    
    [summaryWrapperArray addObject:contractSummaryRowWrappper];
    
    NSMutableDictionary *sectionDict1 = [NSMutableDictionary dictionary];
    [sectionDict1 setValue:@"" forKey:@"title"];
    [sectionDict1 setValue:summaryWrapperArray forKey:@"rows"];
    
    [summarySectionArray addObject:sectionDict1];
    
     for (BTMobileServiceAssetModel* mobileservice in  mobileServiceProductArray){
             NSString* serviveType = mobileservice.type;
             if ( [serviveType caseInsensitiveCompare:@"Device"] == NSOrderedSame ) {
                 MobileAssetsDetailRowWrapper *deviceSummaryRowWrappper = [[MobileAssetsDetailRowWrapper alloc] init];
                 deviceSummaryRowWrappper.cellType = MobileServiceDetailRowTypeMobileDevice;
                 deviceSummaryRowWrappper.mobileDevice = @"-";
                 deviceSummaryRowWrappper.imieNumber = @"-";
                 if(mobileservice.productName && [mobileservice.productName validAndNotEmptyStringObject]){
                       deviceSummaryRowWrappper.mobileDevice = mobileservice.productName;
                 }
                 [summaryDeviceWrapperArray addObject:deviceSummaryRowWrappper];
                 
                 NSMutableDictionary *sectionDict2 = [NSMutableDictionary dictionary];
                 [sectionDict2 setValue:@"Device detail" forKey:@"title"];
                 [sectionDict2 setValue:summaryDeviceWrapperArray forKey:@"rows"];
                 
                 [summarySectionArray addObject:sectionDict2];
                 break;
             }
         }
   
    
    ///////////////////////////
    
    
    //Take Action row wrappers
    MobileAssetsDetailRowWrapper *viewBillsRowWrappper = [[MobileAssetsDetailRowWrapper alloc] init];
    viewBillsRowWrappper.cellType = MobileServiceDetailRowTypeTakeAction;
    viewBillsRowWrappper.actionTitle = @"View latest bill";
    
    MobileAssetsDetailRowWrapper *mobileServiceFAQRowWrappper = [[MobileAssetsDetailRowWrapper alloc] init];
    mobileServiceFAQRowWrappper.cellType = MobileServiceDetailRowTypeTakeAction;
    mobileServiceFAQRowWrappper.actionTitle = @"Mobile Services FAQs";
    
    NSMutableArray *takeAcrtionWrapperArray = [NSMutableArray array];

    [takeAcrtionWrapperArray addObject:viewBillsRowWrappper];
    [takeAcrtionWrapperArray addObject:mobileServiceFAQRowWrappper];
 
    
    NSMutableDictionary *sectionDict5 = [NSMutableDictionary dictionary];
    [sectionDict5 setValue:@"Useful links" forKey:@"title"];
    [sectionDict5 setValue:takeAcrtionWrapperArray forKey:@"rows"];
    [summarySectionArray addObject:sectionDict5];
    
    
    _summarySectionArray = summarySectionArray;
    
    return _summarySectionArray;
}


@end

@implementation MobileAssetsDetailRowWrapper



@end
