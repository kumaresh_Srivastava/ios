//
//  DLMMobileServiceAssetScreen.h
//  BTBusinessApp
//
//  Created by kumaresh shrivastava on 10/06/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DLMObject.h"
#import "BTAsset.h"
#import "BTMobileServiceAssetModel.h"
#import "BTMobileSIMServiceAssetsModel.h"
#import "BTAssetDetailCollection.h"

NS_ASSUME_NONNULL_BEGIN


typedef enum{
    
    MobileServiceDetailRowTypeUnknown,
    MobileServiceDetailRowTypeTakeAction,
    MobileServiceDetailRowTypeMobilePlan,
    MobileServiceDetailRowTypeContractDetailSummary,
    MobileServiceDetailRowTypeSIMDetailSummary,
    MobileServiceDetailRowTypeYourRentedSummary,
    MobileServiceDetailRowTypeMobileDevice,
    MobileServiceDetailRowTypeMobileIMIE,
    
}MobileServiceDetailRowType;


@class DLMMobileServiceAssetDetailScreen;
@class  NLWebServiceError;

@protocol DLMMobileServiceAssetsScreenDelegate <NSObject>

- (void)successfullyFetchedMobileAssetsDetailDataOnDLMMobileServiceAssetScreen:(DLMMobileServiceAssetDetailScreen *)mobileAssetDetailScreen;

- (void)mobileAssetDetailScreen:(DLMMobileServiceAssetDetailScreen *)assetDashboardScreen failedToFetchMobileAssetsDetailDataWithWebServiceError:(NLWebServiceError *)webServiceError;

- (void)successfullyFetchedMobileSIMDetailDataOnDLMMobileServiceAssetScreen:(DLMMobileServiceAssetDetailScreen *)mobileAssetDetailScreen;

- (void)mobileSIMDetailScreen:(DLMMobileServiceAssetDetailScreen *)assetDashboardScreen failedToFetchMobileSIMDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end


@interface DLMMobileServiceAssetDetailScreen : DLMObject

@property (nonatomic, copy) NSArray<BTMobileServiceAssetModel*> *mobileServiceDataArray;
@property (nonatomic, copy) NSArray<BTMobileSIMServiceAssetsModel*> *mobileSIMDataArray;
@property (nonatomic, weak) id <DLMMobileServiceAssetsScreenDelegate> mobileAssetDetailScreenDelegate;

- (void)fetchMobileServiceAssetsDetails:(NSString*)mobileServiceId;
- (void)fetchMobileSIMDetails:(NSString*)mobileServiceId;
- (void)cancelMobileServiceAssetsDetailsAPIRequest;
- (NSArray*)getDataForMobileAssetsService:(BTAssetDetailCollection*)assetDetailCollectionModel mobileServiceProductSummary:(NSArray*)mobileServiceProductArray mobileServiceSIM:(NSArray*)mobileServiceSIMData;

@end


//Wrapper is created to wrap different data for population on UI. Properties of wrapper could hold any thing(price, location, contact etc.)
@interface MobileAssetsDetailRowWrapper : NSObject
@property(nonatomic, assign) MobileServiceDetailRowType cellType;
@property(nonatomic, strong) NSString *actionTitle;
@property(nonatomic, strong) NSString *mobilePlan;
@property(nonatomic, strong) NSString *contractTerms;
@property(nonatomic, strong) NSString *contractEndDate;
@property(nonatomic, strong) NSString *simSerialNumber;
@property(nonatomic, strong) NSString *totalRental;
@property(nonatomic, strong) NSString *mobileDevice;
@property(nonatomic, strong) NSString *imieNumber;
//- (NSAttributedString *)getRentalAttributtedStringForSrting:(NSString *)inputString;
//- (NSAttributedString *)attributedStringFotBoldString:(NSString *)boldString andBoldString:(NSString *)regularSting;
@end

NS_ASSUME_NONNULL_END
