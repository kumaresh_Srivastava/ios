//
//  BTAssetsDetailViewController.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/9/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMAssetServiceDetailScreen.h"
#import "BTAsset.h"

@interface BTAssetsServiceDetailViewController : UIViewController
@property(nonatomic, strong) BTAssetDetailCollection *assetsDetailCollectionModel;

@property (strong,nonatomic) NSString *assetType;
@property (strong,nonatomic) BTAsset *selectedAsset;

+ (BTAssetsServiceDetailViewController *)getBTAssetsServiceDetailViewController;

@end
