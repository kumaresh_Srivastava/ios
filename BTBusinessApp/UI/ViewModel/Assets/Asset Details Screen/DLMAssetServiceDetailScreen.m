//
//  DLMAssetServiceDetailScreen.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/9/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMAssetServiceDetailScreen.h"
#import "NSObject+APIResponseCheck.h"
#import "BTAssetCombinedRental.h"
#import "AppConstants.h"
#import "AppManager.h"

@interface DLMAssetServiceDetailScreen()
{
    NSArray *_summarySectionArray;
    NSArray *_rentalDetailWrapperArray;
}

@end

@implementation DLMAssetServiceDetailScreen


#pragma mark- Public methods
- (id)initWithBTAssetDetailCollection:(BTAssetDetailCollection *)model andAssetType:(NSString *)assetType{
    
    self = [super init];
    
    if(self){
        
        //BTAssetCombinedRental
        [self createRowWrapperFromAssetsDetailModel:model withAssetType:assetType];
    }
    
    return self;
}


- (NSArray *)getSummarySectionArray{
    
    return _summarySectionArray;
}


- (NSArray *)getRentalSectionArray{
    
    return _rentalDetailWrapperArray;
}


#pragma mark - Private helper methods
- (void)createRowWrapperFromAssetsDetailModel:(BTAssetDetailCollection *)assetDetailCollectionModel withAssetType:(NSString *)assetType{
    
    
    //Summary tab
    NSMutableArray *summarySectionArray = [NSMutableArray array];
    
    
    NSMutableArray *summaryWrapperArray = [NSMutableArray array];
    
    //Summary Row Wrappers
    AssetsDetailRowWrapper *summaryRowWrappper = [[AssetsDetailRowWrapper alloc] init];
    summaryRowWrappper.cellType = ServiceDetailRowTypeSummary;
    
    if(assetDetailCollectionModel.installedAt && [assetDetailCollectionModel.installedAt validAndNotEmptyStringObject]){
        
        summaryRowWrappper.heading = assetDetailCollectionModel.installedAt;
    }
    else{
        
        summaryRowWrappper.heading = @"-";
    }
    
    if(assetDetailCollectionModel.contractTerm && [assetDetailCollectionModel.contractTerm validAndNotEmptyStringObject]){
        
        if ([assetDetailCollectionModel.contractTerm integerValue] > 1) {
            summaryRowWrappper.subHeading1 = [assetDetailCollectionModel.contractTerm  stringByAppendingString:@" Months"];
        } else {
            summaryRowWrappper.subHeading1 = [assetDetailCollectionModel.contractTerm  stringByAppendingString:@" Month"];
        }
        
    }
    else{
        
        summaryRowWrappper.subHeading1 = @"-";
    }
    
    if(assetDetailCollectionModel.combinedRent && [assetDetailCollectionModel.combinedRent validAndNotEmptyStringObject]){
        
        summaryRowWrappper.subHeading2 = assetDetailCollectionModel.combinedRent;
    }
    else{
        
        summaryRowWrappper.subHeading2 = @"-";
    }
    
    // Kumaresh Added for Contract End Date. US - 13277
    if(assetDetailCollectionModel.contractEnd && [assetDetailCollectionModel.contractEnd validAndNotEmptyStringObject]){
        
        summaryRowWrappper.subHeading3 = assetDetailCollectionModel.contractEnd;
    }
    else{
        
        summaryRowWrappper.subHeading3 = @"-";
    }
    
    [summaryWrapperArray addObject:summaryRowWrappper];
    
    NSMutableDictionary *sectionDict1 = [NSMutableDictionary dictionary];
    [sectionDict1 setValue:@"" forKey:kSectionTitleKey];
    [sectionDict1 setValue:summaryWrapperArray forKey:kSectionRowsKey];
    
    [summarySectionArray addObject:sectionDict1];
    _summarySectionArray = summarySectionArray;
    
    
    
    //Take Action row wrappers
    AssetsDetailRowWrapper *checkStatusRowWrappper = [[AssetsDetailRowWrapper alloc] init];
    checkStatusRowWrappper.cellType = ServiceDetailRowTypeTakeAction;
    checkStatusRowWrappper.heading = kCheckServiceText;
    
    AssetsDetailRowWrapper *viewBillRowWrappper = [[AssetsDetailRowWrapper alloc] init];
    viewBillRowWrappper.cellType = ServiceDetailRowTypeTakeAction;
    viewBillRowWrappper.heading = kViewBillText;
    
    
    AssetsDetailRowWrapper *reviewUsageRowWrappper = [[AssetsDetailRowWrapper alloc] init];
    reviewUsageRowWrappper.cellType = ServiceDetailRowTypeTakeAction;
    reviewUsageRowWrappper.heading = kReviewUsageText;
    
     if ( [assetType caseInsensitiveCompare:@"Mobile services"] == NSOrderedSame && kIsMobileServiceAssetsEnabled == YES)
     {
         AssetsDetailRowWrapper *reviewUsageRowWrappper = [[AssetsDetailRowWrapper alloc] init];
         reviewUsageRowWrappper.cellType = ServiceDetailRowTypeTakeAction;
         reviewUsageRowWrappper.heading = kMobileServiceFAQText;
     }
    
    NSMutableArray *takeAcrtionWrapperArray = [NSMutableArray array];

    
    Boolean isPSTNOrBroadband = ([assetType isEqualToString:@"Phone Services"] || [assetType isEqualToString:@"Broadband & Internet"]);
    
    if (isPSTNOrBroadband) {
        [takeAcrtionWrapperArray addObject:checkStatusRowWrappper];
    }
    [takeAcrtionWrapperArray addObject:viewBillRowWrappper];
    if (isPSTNOrBroadband) {
        [takeAcrtionWrapperArray addObject:reviewUsageRowWrappper];
    }

    NSMutableDictionary *sectionDict2 = [NSMutableDictionary dictionary];
    [sectionDict2 setValue:@"Useful links" forKey:kSectionTitleKey];
    [sectionDict2 setValue:takeAcrtionWrapperArray forKey:kSectionRowsKey];
    [summarySectionArray addObject:sectionDict2];
    

    //Rental Detail tab
    NSMutableArray *rentalDetailSectionArray = [NSMutableArray array];
    NSMutableArray *rentalDetailArray = [NSMutableArray array];
    
    if(assetDetailCollectionModel.combinedRentalDesc && [assetDetailCollectionModel.combinedRentalDesc count] > 0){
        
        for(BTAssetCombinedRental *combinedRental in assetDetailCollectionModel.combinedRentalDesc){
            
            if((combinedRental.assetItemHeaderDesc && [combinedRental.assetItemHeaderDesc validAndNotEmptyStringObject]) || (combinedRental.assetItemCharge && [combinedRental.assetItemCharge validAndNotEmptyStringObject]) || (combinedRental.assetItemPeriod && [combinedRental.assetItemPeriod validAndNotEmptyStringObject])){
                
                
                //Desc
                AssetsDetailRowWrapper *combinedRentalRowWrappper = [[AssetsDetailRowWrapper alloc] init];
                combinedRentalRowWrappper.cellType = ServiceDetailRowTypeRentalDetail;
                
                if(combinedRental.assetItemHeaderDesc && ([combinedRental.assetItemHeaderDesc validAndNotEmptyStringObject])){
                    
                    
                    NSArray *descComponents = [combinedRental.assetItemHeaderDesc componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"-"]];
                    
                    if(descComponents && [descComponents count] > 0){
                        
                        combinedRentalRowWrappper.heading = [descComponents firstObject];
                        
                        if([descComponents count] > 1){
                        
                            combinedRentalRowWrappper.subHeading1 = [combinedRental.assetItemHeaderDesc stringByReplacingOccurrencesOfString:combinedRentalRowWrappper.heading  withString:@""];
                            
                            NSRange rOriginal = [combinedRentalRowWrappper.subHeading1 rangeOfString:@"-"];
                            
                            if (NSNotFound != rOriginal.location) {
                                combinedRentalRowWrappper.subHeading1 = [combinedRentalRowWrappper.subHeading1
                                            stringByReplacingCharactersInRange: rOriginal
                                            withString:                         @""];
                            }
                        }
                    }
                    else{
                        
                        combinedRentalRowWrappper.heading = combinedRental.assetItemHeaderDesc;
                    }
                    
                }
                else{
                    
                    combinedRentalRowWrappper.heading = nil;
                    combinedRentalRowWrappper.subHeading1 = nil;
                }
                
                
               //Charge
                if(combinedRental.assetItemCharge && ([combinedRental.assetItemCharge validAndNotEmptyStringObject])){
                    
                    combinedRentalRowWrappper.subHeading2 = combinedRental.assetItemCharge;
                }
                else{
                    
                    combinedRentalRowWrappper.subHeading2 = nil;
                }
                
                //Period
                if(combinedRental.assetItemPeriod && ([combinedRental.assetItemPeriod validAndNotEmptyStringObject])){
                    
                    combinedRentalRowWrappper.subHeading3 = combinedRental.assetItemPeriod;
                }
                else{
                    
                    combinedRentalRowWrappper.subHeading3 = nil;
                }
                
                [rentalDetailArray addObject:combinedRentalRowWrappper];
                
            }
        }
    }
    
    
    
    AssetsDetailRowWrapper *rentalTotalRowWrappper = [[AssetsDetailRowWrapper alloc] init];
    rentalTotalRowWrappper.cellType = ServiceDetailRowTypeRentalTotal;
    
    if([assetDetailCollectionModel.combinedRent validAndNotEmptyStringObject]){
        
        rentalTotalRowWrappper.heading = [NSString stringWithFormat:@"%@", assetDetailCollectionModel.combinedRent];
    }
    else{
        
        rentalTotalRowWrappper.heading = @"-";
    }
    [rentalDetailArray addObject:rentalTotalRowWrappper];
    
    
    
    if([rentalDetailArray count] > 0){
        
        NSMutableDictionary *sectionDict = [NSMutableDictionary dictionary];
        [sectionDict setValue:@"" forKey:kSectionTitleKey];
        [sectionDict setValue:rentalDetailArray forKey:kSectionRowsKey];
        [rentalDetailSectionArray addObject:sectionDict];
    }
    
    
    NSMutableDictionary *sectionDict = [NSMutableDictionary dictionary];
    [sectionDict setValue:@"Useful links" forKey:kSectionTitleKey];
    [sectionDict setValue:takeAcrtionWrapperArray forKey:kSectionRowsKey];
    [rentalDetailSectionArray addObject:sectionDict2];
    
    _rentalDetailWrapperArray = rentalDetailSectionArray;
    
}


@end


@implementation AssetsDetailRowWrapper


#pragma mark - Public methods
- (NSAttributedString *)getRentalAttributtedStringForSrting:(NSString *)inputString{
    
    NSString *regularSting = @"Rental for";
    NSString *boldString = @"";
    
    NSRange range = [inputString rangeOfString:regularSting options:NSCaseInsensitiveSearch];
    
    if(range.location == NSNotFound){
        
        boldString = inputString;
    }
    else{
        
        boldString = [inputString  stringByReplacingOccurrencesOfString:regularSting withString:@" " options:NSCaseInsensitiveSearch range:NSMakeRange(0, [inputString length])];
    }
    
    
 
    return [self attributedStringFotRegularString:regularSting andBoldString:boldString];
    
 }


- (NSAttributedString *)attributedStringFotBoldString:(NSString *)boldString andBoldString:(NSString *)regularSting{
    
    UIFont *textFont = [UIFont fontWithName:kBtFontRegular size:13.0];
    NSDictionary *textDict = [NSDictionary dictionaryWithObject:textFont forKey:NSFontAttributeName];
    
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:regularSting attributes:textDict];
    
    UIFont *valueFont = [UIFont fontWithName:kBtFontBold size:13.0];
    NSDictionary *valueDict = [NSDictionary dictionaryWithObject:valueFont forKey:NSFontAttributeName];
    NSMutableAttributedString *vAttrString = [[NSMutableAttributedString alloc]initWithString:boldString attributes:valueDict];
    
    [vAttrString appendAttributedString:aAttrString];
    
    
    return vAttrString;
    
    
}


#pragma mark - Private helper methods
- (NSAttributedString *)attributedStringFotRegularString:(NSString *)regularSting andBoldString:(NSString *)boldString{
    
    UIFont *textFont = [UIFont fontWithName:kBtFontRegular size:13.0];
    NSDictionary *textDict = [NSDictionary dictionaryWithObject:textFont forKey:NSFontAttributeName];
    
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:regularSting attributes:textDict];
    
    UIFont *valueFont = [UIFont fontWithName:kBtFontBold size:13.0];
    NSDictionary *valueDict = [NSDictionary dictionaryWithObject:valueFont forKey:NSFontAttributeName];
    NSMutableAttributedString *vAttrString = [[NSMutableAttributedString alloc]initWithString:boldString attributes:valueDict];
    
    [aAttrString appendAttributedString:vAttrString];
    
    
    return aAttrString;
    

}


@end
