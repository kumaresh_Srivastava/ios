//
//  BTAssetsDetailViewController.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/9/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTAssetsServiceDetailViewController.h"
#import "BTAssetServiceDetailHeaderView.h"
#import "BTTakeActionTableViewCell.h"
#import "BTAssetServiceDetailHeaderView.h"
#import "BTRentalDetailTableViewCell.h"
#import "BTAssetsDetailSummaryTableViewCell.h"
#import "BTBillDetailViewController.h"
#import "BTRentalTotalTableViewCell.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "BTServiceStatusDetailsViewController.h"
#import "BTPhoneUsageSummaryViewController.h"
#import "BTBroadbandUsageViewController.h"
#import "AppConstants.h"
#import "BTMobileServiceDetailHeaderViewTableViewCell.h"
#import "DLMMobileServiceAssetDetailScreen.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "NLWebServiceError.h"
#import "BTAsset.h"
#import "BTMobileServiceContractDetailTableViewCell.h"
#import "BTEmptyDashboardView.h"
#import "BTMobileServiceDetailTableViewCell.h"
#import "BTMobileServiceContractDetailTableViewCell.h"
#import "BTDeviceDetailTableViewCell.h"
#import "BTTakeActionHeaderView.h"

#define kTakeActionCell @"BTTakeActionTableViewCell"
#define kTakeActionCellID @"BTTakeActionTableViewCellID"

#define kRentalDetailCell @"BTRentalDetailTableViewCell"
#define kRentalDetailCellID @"BTRentalDetailTableViewCellID"

#define kRentalTotalCell @"BTRentalTotalTableViewCell"
#define kRentalTotalCellID @"BTRentalTotalTableViewCellID"


#define kAssetsDetailSummaryTableViewCell @"BTAssetsDetailSummaryTableViewCell"
#define kAssetsDetailSummaryTableViewCellID @"BTAssetsDetailSummaryTableViewCellID"

#define kMobileServiceDetailTableViewCell @"BTMobileServiceDetailTableViewCell"
#define kMobileServiceDetailTableViewCellID @"BTMobileServiceDetailTableViewCellID"

#define kMobileServiceContractDetailTableViewCell @"BTMobileServiceContractDetailTableViewCell"

#define kMobileServiceContractDetailTableViewCellID @"BTMobileServiceContractDetailTableViewCellID"

#define kBTDeviceDetailTableViewCell @"BTDeviceDetailTableViewCell"

#define kBTDeviceDetailTableViewCellID @"BTDeviceDetailTableViewCellID"


@interface BTAssetsServiceDetailViewController ()<UITableViewDelegate, UITableViewDataSource, BTAssetServiceDetailHeaderViewDelegate,BTMobileServiceDetailHeaderViewDelegate,BTRetryViewDelegate,DLMMobileServiceAssetsScreenDelegate>{
    
    NSArray *_firstTabDataArray;
    NSArray *_secondTabDataArray;
    NSInteger _seclectedTabIndex;
    BTAssetServiceDetailHeaderView *_headerView;
    BTMobileServiceDetailHeaderViewTableViewCell *_headerViewMobileServices;
    DLMAssetServiceDetailScreen *_screenModel;
    NSString *_pageNameForOmniture;
    BTRetryView *_retryView;
    NSMutableArray *_mobileAssetsDetailArray;
    NSMutableArray *_mobileSIMDetailArray;
    BTEmptyDashboardView *_emptyDashboardView;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic, assign) BOOL networkRequestInProgress;
@property (nonatomic,strong)  CustomSpinnerView *loadingView;
@property (nonatomic, readwrite) DLMMobileServiceAssetDetailScreen *mobileAssetDetailScreenModel;

@end

@implementation BTAssetsServiceDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if ([_assetsDetailCollectionModel.serviceNumber rangeOfString:@"cv" options:NSCaseInsensitiveSearch].location != NSNotFound) { // Cloud Voice details
        self.title = @"Cloud Voice Express service";
    }
    else if ( [self.assetType caseInsensitiveCompare:@"Mobile services"] == NSOrderedSame  && kIsMobileServiceAssetsEnabled == YES)  { // Mobile service
       self.title = @"Service details";
    }
    else
    {
            NSString* titleStr = nil;
            if(_assetsDetailCollectionModel.serviceText.length >25 && IS_IPHONE_5){
                titleStr = [_assetsDetailCollectionModel.serviceText substringToIndex:25];
                titleStr = [titleStr stringByAppendingString:@"..."];
            } else if (_assetsDetailCollectionModel.serviceText .length >32 && IS_IPHONE){
                titleStr = [_assetsDetailCollectionModel.serviceText  substringToIndex:32];
                titleStr = [titleStr stringByAppendingString:@"..."];
            }
            else {
                titleStr = _assetsDetailCollectionModel.serviceText;
            }
            self.navigationItem.title = titleStr;
    }
    
    if (!_assetsDetailCollectionModel.serviceText) {
        self.title = @"Service Details";
    }
    
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 150.0;
    
    _seclectedTabIndex = 0;
    
    //self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    //Nib Registration
    UINib *nib = [UINib nibWithNibName:kTakeActionCell bundle:nil];
    [_tableView registerNib:nib forCellReuseIdentifier:kTakeActionCellID];
    nib = [UINib nibWithNibName:kRentalDetailCell bundle:nil];
    [_tableView registerNib:nib forCellReuseIdentifier:kRentalDetailCellID];
    
    _screenModel = [[DLMAssetServiceDetailScreen alloc] initWithBTAssetDetailCollection:self.assetsDetailCollectionModel andAssetType:self.assetType];
    _secondTabDataArray = [_screenModel getRentalSectionArray];
   
    
    if ( [self.assetType caseInsensitiveCompare:@"Mobile services"] == NSOrderedSame && kIsMobileServiceAssetsEnabled == YES)  { // Mobile service
       
        nib = [UINib nibWithNibName:kMobileServiceDetailTableViewCell bundle:nil];
        [_tableView registerNib:nib forCellReuseIdentifier:kMobileServiceDetailTableViewCellID];
        
        nib = [UINib nibWithNibName:kMobileServiceContractDetailTableViewCell bundle:nil];
        [_tableView registerNib:nib forCellReuseIdentifier:kMobileServiceContractDetailTableViewCellID];
        
        ////
        
        nib = [UINib nibWithNibName:kBTDeviceDetailTableViewCell bundle:nil];
        [_tableView registerNib:nib forCellReuseIdentifier:kBTDeviceDetailTableViewCellID];
        
        
        //[SD] View Model Initialization
        self.mobileAssetDetailScreenModel = [[DLMMobileServiceAssetDetailScreen alloc] init];
        self.mobileAssetDetailScreenModel.mobileAssetDetailScreenDelegate = self;
       
        _headerViewMobileServices= [[[NSBundle mainBundle] loadNibNamed:@"BTMobileServiceDetailHeaderViewTableViewCell" owner:self options:nil] firstObject];
        [_headerViewMobileServices updateWithAssetsDetailCollection:self.assetsDetailCollectionModel withAssetType:self.assetType];
        _headerViewMobileServices.delegate = self;
        
        [self fetchMobileServiceAssetsAPI];

    } else{
        nib = [UINib nibWithNibName:kAssetsDetailSummaryTableViewCell bundle:nil];
        [_tableView registerNib:nib forCellReuseIdentifier:kAssetsDetailSummaryTableViewCellID];
        
        _headerView= [[[NSBundle mainBundle] loadNibNamed:@"BTAssetServiceDetailHeaderView" owner:self options:nil] firstObject];
        [_headerView updateWithAssetsDetailCollection:self.assetsDetailCollectionModel withAssetType:self.assetType];
        _headerView.delegate = self;
         _firstTabDataArray = [_screenModel getSummarySectionArray];
    }
    
    nib = [UINib nibWithNibName:kRentalDetailCell bundle:nil];
    [_tableView registerNib:nib forCellReuseIdentifier:kRentalDetailCellID];
    
    
    nib = [UINib nibWithNibName:kRentalTotalCell bundle:nil];
    [_tableView registerNib:nib forCellReuseIdentifier:kRentalTotalCellID];
    
    
    _pageNameForOmniture = OMNIPAGE_ASSETS_DETAIL_SUMMARY;
    [self trackPageForOmniture:OMNIPAGE_ASSETS_DETAIL_SUMMARY];
   
}


- (void)viewDidLayoutSubviews{
    
    if ( [self.assetType caseInsensitiveCompare:@"Mobile services"] == NSOrderedSame && kIsMobileServiceAssetsEnabled == YES)  { // Mobile service
        [self resizeMobileServiceTableViewHeaderToFit];
    } else {
        [self resizeTableViewHeaderToFit];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)btMobileServiceDetailHeaderView:(BTMobileServiceDetailHeaderViewTableViewCell *)view didSelectedSegmentAtIndex:(NSInteger)index{
    if(index ==0)
    {
        _pageNameForOmniture = OMNIPAGE_ASSETS_DETAIL_SUMMARY;
        [self trackPageForOmniture:OMNIPAGE_ASSETS_DETAIL_SUMMARY];
    }
    else
    {
        _pageNameForOmniture = OMNIPAGE_ASSETS_DETAIL_RENTAL;
        [self trackPageForOmniture:OMNIPAGE_ASSETS_DETAIL_RENTAL];
    }
    
    
    
    _seclectedTabIndex = index;
    [_tableView reloadData];
}
- (void)btChooseNumberAction{
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)createLoadingView {
    
    if(!_loadingView){
        
        _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.view addSubview:_loadingView];
    }
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}

- (void)hideLoadingItems:(BOOL)isHide {
    
    // (SD) Hide or Show UI elements related to loading.
    [_loadingView setHidden:isHide];
    
}

#pragma mark - api call methods

- (void)fetchMobileServiceAssetsAPI {
    
    if([AppManager isInternetConnectionAvailable])
    {
        [self createLoadingView];
        [self hideRetryItems:YES];
        // self.networkRequestInProgress = YES;
        //[self.mobileAssetDetailScreenModel fetchMobileServiceAssetsDetails:/*self.assetsDetailCollectionModel.serviceNumber*/@"07997497058"];////07997305396
        //[self.mobileAssetDetailScreenModel fetchMobileSIMDetails:/*self.assetsDetailCollectionModel.serviceNumber*/@"07997497058"];
        
        [self.mobileAssetDetailScreenModel fetchMobileServiceAssetsDetails:self.assetsDetailCollectionModel.serviceNumber];////07997305396
        
    }
    else
    {
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_ASSETS_ACCOUNT];
    }
}
#pragma mark - DLMAssetDashboardScreenDelegate


- (void)successfullyFetchedMobileAssetsDetailDataOnDLMMobileServiceAssetScreen:(DLMMobileServiceAssetDetailScreen *)mobileAssetDetailScreen {
    /* self.networkRequestInProgress = NO;
     _assetsTableView.hidden = NO;*/
    
    if(mobileAssetDetailScreen.mobileServiceDataArray.count > 0)
    {
        _mobileAssetsDetailArray =  [mobileAssetDetailScreen.mobileServiceDataArray mutableCopy];
        /*if(_mobileSIMDetailArray && _mobileSIMDetailArray.count>0 && _mobileAssetsDetailArray && _mobileAssetsDetailArray.count>0){
            [_tableView reloadData];
            [self hideLoadingItems:YES];
        }*/
        [self.mobileAssetDetailScreenModel fetchMobileSIMDetails:self.assetsDetailCollectionModel.serviceNumber];
    }
    else if(mobileAssetDetailScreen.mobileServiceDataArray.count == 0)
    {
         [self showEmptyDashBoardView];
        [self hideLoadingItems:YES];
    }
    else
    {
        DDLogError(@"Assets Dashboard: Assets not found.");
        [self hideLoadingItems:YES];
    }
    
    
    
}

- (void)mobileAssetDetailScreen:(DLMMobileServiceAssetDetailScreen *)assetDashboardScreen failedToFetchMobileAssetsDetailDataWithWebServiceError:(NLWebServiceError *)webServiceError {
    // self.networkRequestInProgress = NO;
    [self hideLoadingItems:YES];
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
    
    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {
        
        switch (webServiceError.error.code)
        {
                
            case BTNetworkErrorCodeAPINoDataFound:
            {
                errorHandled = YES;
                //[self showEmptyDashBoardView];
                [self showRetryViewWithInternetStrip:NO];
                [AppManager trackNoDataFoundErrorOnPage:OMNIPAGE_ASSETS_ACCOUNT];
                break;
            }
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }
    
    if(errorHandled == NO)
    {
        [self showRetryViewWithInternetStrip:NO];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_ASSETS_ACCOUNT];
    }
    
}

- (void)successfullyFetchedMobileSIMDetailDataOnDLMMobileServiceAssetScreen:(DLMMobileServiceAssetDetailScreen *)mobileAssetDetailScreen{
    
    /* self.networkRequestInProgress = NO;
     _assetsTableView.hidden = NO;*/
    
    if(mobileAssetDetailScreen.mobileSIMDataArray.count > 0)
    {
        [self hideLoadingItems:YES];
        _mobileSIMDetailArray =  [mobileAssetDetailScreen.mobileSIMDataArray mutableCopy];
        if(_mobileSIMDetailArray && _mobileSIMDetailArray.count>0 && _mobileAssetsDetailArray && _mobileAssetsDetailArray.count>0){
           
           _firstTabDataArray = [self.mobileAssetDetailScreenModel getDataForMobileAssetsService:self.assetsDetailCollectionModel mobileServiceProductSummary:_mobileAssetsDetailArray mobileServiceSIM:_mobileSIMDetailArray];
            
             [_tableView reloadData];
        }
    }
    else if(mobileAssetDetailScreen.mobileSIMDataArray.count == 0)
    {
         [self showEmptyDashBoardView];
    }
    else
    {
        DDLogError(@"Assets Dashboard: Assets not found.");
    }
    
}

- (void)mobileSIMDetailScreen:(DLMMobileServiceAssetDetailScreen *)assetDashboardScreen failedToFetchMobileSIMDataWithWebServiceError:(NLWebServiceError *)webServiceError{
    
    [self hideLoadingItems:YES];
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
    
    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {
        
        switch (webServiceError.error.code)
        {
                
            case BTNetworkErrorCodeAPINoDataFound:
            {
                errorHandled = YES;
                [self showRetryViewWithInternetStrip:NO];
                [AppManager trackNoDataFoundErrorOnPage:OMNIPAGE_ASSETS_ACCOUNT];
                break;
            }
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }
    
    if(errorHandled == NO)
    {
        [self showRetryViewWithInternetStrip:NO];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_ASSETS_ACCOUNT];
    }
    
}


- (void)showEmptyDashBoardView {
    
    //Error Tracker
    [OmnitureManager trackError:OMNIERROR_EMPTY_MOBILE_SERVICE_PROCESSSING onPageWithName:OMNIPAGE_ASSETS_MOBILE_SERVICE contextInfo:[AppManager getOmniDictionary]];
    
    _emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTEmptyDashboardView" owner:nil options:nil] objectAtIndex:0];
    _emptyDashboardView.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSURL *url;
    
    if ([kBTServerType isEqualToString:@"BTServerTypeProd"]) {
        url = [[NSURL alloc] initWithString:@"https://secure.business.bt.com/Account/Addbillingaccounts.aspx"];
    } else{
        url = [[NSURL alloc] initWithString:@"https://eric1-dmze2e-ukb.bt.com/Account/Addbillingaccounts.aspx"];
    }
    
    NSMutableAttributedString *hypLink = [[NSMutableAttributedString  alloc] initWithString:@"There’s no information to show just yet. Your account is either pending approval by your admin, or you can add an account here."];
    
    NSDictionary *linkAttributes = @{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle),
                                     NSLinkAttributeName : url};
    
    [hypLink addAttributes:linkAttributes range:NSMakeRange(hypLink.length-5,4)];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
    [hypLink addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,hypLink.length)];
    
    [hypLink addAttribute:NSForegroundColorAttributeName value:[BrandColours colourBtNeutral70] range:NSMakeRange(0, hypLink.length)];
    
    [hypLink addAttribute:NSFontAttributeName
                    value:[UIFont fontWithName:kBtFontRegular size:16.0] range:NSMakeRange(0, hypLink.length)];
    
    [_emptyDashboardView updateEmptyDashboardViewWithImageNameAndLink:@"package" title:@"No accounts to display" detailText:hypLink andButtonTitle:nil];
    
    [self.view addSubview:_emptyDashboardView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
   
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
   
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
}

#pragma mark - RetryView Delgates

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView {
    //[self.mobileAssetDetailScreenModel fetchMobileServiceAssetsDetails:@"07997497058"];
    [self fetchMobileServiceAssetsAPI];
}



#pragma mark - Private Helper Methods
- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow  {
    
    if(_retryView)
    {
        _retryView.hidden = NO;
        _retryView.retryViewDelegate = self;
        [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
        return;
    }
    _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
    _retryView.translatesAutoresizingMaskIntoConstraints = NO;
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
    _retryView.retryViewDelegate = self;
    
    [self.view addSubview:_retryView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-50.0]];
    
}
/*
 - (void)hideEmmptyDashBoardItems:(BOOL)isHide {
 
 // (SD) Hide or Show UI elements related to retry.
 [_emptyDashboardView setHidden:isHide];
 [_emptyDashboardView removeFromSuperview];
 _emptyDashboardView = nil;
 
 }
 */

- (void)hideRetryItems:(BOOL)isHide {
    
    // (SD) Hide or Show UI elements related to retry.
    [_retryView setHidden:isHide];
    
}

#pragma mark - Class Methods


+ (BTAssetsServiceDetailViewController *)getBTAssetsServiceDetailViewController{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTAssetsServiceDetailViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTAssetsServiceDetailViewControllerID"];

    return vc;
}



#pragma mark- Private helper Method

- (void)resizeTableViewHeaderToFit{
    
    [_headerView setNeedsLayout];
    [_headerView layoutIfNeeded];
    
    CGFloat height = [_headerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CGRect frame = _headerView.frame;
    frame.size.height = height;
    _headerView.frame = frame;
    
    _tableView.tableHeaderView = _headerView;
    
}

- (void)resizeMobileServiceTableViewHeaderToFit{
    
    [_headerViewMobileServices setNeedsLayout];
    [_headerViewMobileServices layoutIfNeeded];
    
   // CGFloat height = [_headerViewMobileServices systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CGRect frame = _headerViewMobileServices.frame;
    frame.size.height = 130.0;
    _headerViewMobileServices.frame = frame;
    
    _tableView.tableHeaderView = _headerViewMobileServices;
    
}

- (UITableViewCell *)cellForRowWrapper:(AssetsDetailRowWrapper *)rowWrapper{
    
    
    UITableViewCell *cell = nil;
    
    switch (rowWrapper.cellType) {
            
        case ServiceDetailRowTypeSummary:
        {
            cell = [_tableView dequeueReusableCellWithIdentifier:kAssetsDetailSummaryTableViewCellID];
            [(BTAssetsDetailSummaryTableViewCell *)cell upadateWithRowWrapper:rowWrapper];
        }
            break;
            
        case ServiceDetailRowTypeTakeAction:
        {
            cell = [_tableView dequeueReusableCellWithIdentifier:kTakeActionCellID];
            [(BTTakeActionTableViewCell *)cell upadateWithRowWrapper:rowWrapper];
        }
            break;
            
        case ServiceDetailRowTypeRentalDetail:
        {
            cell = [_tableView dequeueReusableCellWithIdentifier:kRentalDetailCellID];
            [(BTRentalDetailTableViewCell *)cell upadateWithRowWrapper:rowWrapper];
        }
            break;
            
        case ServiceDetailRowTypeRentalTotal:
        {
            cell = [_tableView dequeueReusableCellWithIdentifier:kRentalTotalCellID];
            [(BTRentalTotalTableViewCell *)cell upadateWithRowWrapper:rowWrapper];
        }
            break;
            
        default:
            break;
    }
    
    
    return cell;
}


- (UITableViewCell *)cellForMobileAssetsRowWrapper:(MobileAssetsDetailRowWrapper *)rowWrapper{
    
    UITableViewCell *cell = nil;
    
    switch (rowWrapper.cellType) {
            
        case MobileServiceDetailRowTypeMobilePlan:
        {
            cell = [_tableView dequeueReusableCellWithIdentifier:kMobileServiceDetailTableViewCellID];
            [(BTMobileServiceDetailTableViewCell *)cell upadateWithRowWrapper:rowWrapper];
        }
            break;
            
        case MobileServiceDetailRowTypeContractDetailSummary:
        {
            cell = [_tableView dequeueReusableCellWithIdentifier:kMobileServiceContractDetailTableViewCellID];
            [(BTMobileServiceContractDetailTableViewCell *)cell upadateWithRowWrapper:rowWrapper];
        }
            break;
            
        case MobileServiceDetailRowTypeMobileDevice:
        {
            cell = [_tableView dequeueReusableCellWithIdentifier:kBTDeviceDetailTableViewCellID];
            [(BTDeviceDetailTableViewCell *)cell upadateWithRowWrapper:rowWrapper];
        }
            break;
            
        case MobileServiceDetailRowTypeTakeAction:
        {
            cell = [_tableView dequeueReusableCellWithIdentifier:kTakeActionCellID];
            [(BTTakeActionTableViewCell *)cell upadateWithMobileAssetsRowWrapper:rowWrapper];
        }
            break;
            
        default:
            break;
    }
    
    
    return cell;
}

#pragma mark- Table view delgate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if(_seclectedTabIndex == 0){
        return [_firstTabDataArray count];
    }
    
    return [_secondTabDataArray count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSDictionary *sectionDict = nil;
    
    if(_seclectedTabIndex == 0){
     /*if(_mobileSIMDetailArray && _mobileSIMDetailArray.count>0 && _mobileAssetsDetailArray && _mobileAssetsDetailArray.count>0){
         
     }*/
         sectionDict = [_firstTabDataArray objectAtIndex:section];
    }
    else{
       
        sectionDict = [_secondTabDataArray objectAtIndex:section];
    }
    
    
    if(sectionDict && [sectionDict valueForKey:kSectionRowsKey]){
        
        NSArray *rowsArray = [sectionDict valueForKey:kSectionRowsKey];
        return [rowsArray count];
    }

    return 0;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *sectionDict = nil;
    
    if(_seclectedTabIndex == 0){
        sectionDict = [_firstTabDataArray objectAtIndex:indexPath.section];
        if ( [self.assetType caseInsensitiveCompare:@"Mobile services"] == NSOrderedSame && kIsMobileServiceAssetsEnabled == YES)  { // Mobile service
            NSArray *rowsArray = [sectionDict valueForKey:kSectionRowsKey];
            MobileAssetsDetailRowWrapper* rowWrapper = [rowsArray objectAtIndex:indexPath.row];
            return [self cellForMobileAssetsRowWrapper:rowWrapper];
        }
    }
    else{
        
         sectionDict = [_secondTabDataArray objectAtIndex:indexPath.section];
    }
   
    NSArray *rowsArray = [sectionDict valueForKey:kSectionRowsKey];
    AssetsDetailRowWrapper *rowWrapper = [rowsArray objectAtIndex:indexPath.row];
        
    return [self cellForRowWrapper:rowWrapper];

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if(section == 0){
        
        return nil;
    }
    
    if (_mobileAssetsDetailArray && _mobileAssetsDetailArray.count>1){
    
        for (BTMobileServiceAssetModel* mobileservice in  _mobileAssetsDetailArray){
            NSString* serviveType = mobileservice.type;
            if ( [serviveType caseInsensitiveCompare:@"Device"] == NSOrderedSame  && section == 1) {
                UIView *sectionHeaderView = [[[NSBundle mainBundle] loadNibNamed:@"BTTakeActionHeaderView" owner:self options:nil] firstObject];
                 [(BTTakeActionHeaderView *)sectionHeaderView updateLabelTitle:@"Device detail"];
                return sectionHeaderView;
            }
        }
    }
    
    UIView *sectionHeaderView = [[[NSBundle mainBundle] loadNibNamed:@"BTTakeActionHeaderView" owner:self options:nil] firstObject];
    
    return sectionHeaderView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if(section == 0){
        
        return 0;
    }
    
    return 40.0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
   if ( [self.assetType caseInsensitiveCompare:@"Mobile services"] == NSOrderedSame && kIsMobileServiceAssetsEnabled == YES)
   {
        return UITableViewAutomaticDimension;
   }
    
    if(indexPath.section == 0){
        if(_seclectedTabIndex == 0){
            return 175.0;
        } else{
            // if(indexPath.section == 0 && indexPath.row == 0){
            //   return 140.0;
            //} else {
            return UITableViewAutomaticDimension;
            //}
        }
    } else{
        return UITableViewAutomaticDimension;
    }

}



#pragma mark- UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *sectionDict = nil;
    
    if(_seclectedTabIndex == 0){
        
        sectionDict = [_firstTabDataArray objectAtIndex:indexPath.section];
    }
    else{
        
        sectionDict = [_secondTabDataArray objectAtIndex:indexPath.section];
    }
    NSArray *rowsArray = [sectionDict valueForKey:kSectionRowsKey];

    if ( [self.assetType caseInsensitiveCompare:@"Mobile services"] == NSOrderedSame && _seclectedTabIndex == 0 && kIsMobileServiceAssetsEnabled == YES)  { // Mobile service
        MobileAssetsDetailRowWrapper* mobileServiceRowWrapper = [rowsArray objectAtIndex:indexPath.row];
        if(mobileServiceRowWrapper.cellType == ServiceDetailRowTypeTakeAction){
            if([mobileServiceRowWrapper.actionTitle isEqualToString:kViewBillText]){
                
                //View Bill Button Pressed;
                [self trackClickForOmniture:OMNICLICK_ASSETS_VIEW_PAY_BILL];
                
                BTBillDetailViewController *billDetailVC = (BTBillDetailViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"BTBillDetailViewController"];
                billDetailVC.btAsset = self.selectedAsset;
                
                [self.navigationController pushViewController:billDetailVC animated:YES];
                
            } else if([mobileServiceRowWrapper.actionTitle isEqualToString:kMobileServiceFAQText]){
                
                //View Bill Button Pressed;
                [self trackClickForOmniture:OMNICLICK_ASSETS_VIEW_MOBILESERVICE_FAQ];;
                
                [self openSafariBrowserWithURL:kMobileFAQUrl];
                
            }
        }
       
    } else {
        AssetsDetailRowWrapper *rowWrapper = [rowsArray objectAtIndex:indexPath.row];
        if(rowWrapper.cellType == ServiceDetailRowTypeTakeAction){
            
            NSString *asseType = self.assetType;
            
            if([rowWrapper.heading isEqualToString:kCheckServiceText]){
                
                [self trackClickForOmniture:OMNICLICK_ASSETS_CHECK_SERVICE_STATUS];
                
                BTServiceStatusDetailsViewController *serviceStatusDetailsViewController = [BTServiceStatusDetailsViewController getServiceStatusDetailsViewController];
                if ([asseType isEqualToString:@"Phone Services"]) {
                    serviceStatusDetailsViewController.currentServiceType = BTServiceTypePSTN;
                }
                else if ([_assetType isEqualToString:@"Broadband & Internet"]) {
                    serviceStatusDetailsViewController.currentServiceType = BTServiceTypeBroadband;
                }
                serviceStatusDetailsViewController.postCode = self.assetsDetailCollectionModel.installedAt;
                serviceStatusDetailsViewController.serviceID = self.assetsDetailCollectionModel.serviceNumber;
                serviceStatusDetailsViewController.productName = self.assetsDetailCollectionModel.serviceText;
                serviceStatusDetailsViewController.isFromAccountFlow = YES;
                [self.navigationController pushViewController:serviceStatusDetailsViewController animated:YES];
            }
            else if([rowWrapper.heading isEqualToString:kViewBillText]){
                
                //View Bill Button Pressed;
                
                [self trackClickForOmniture:OMNICLICK_ASSETS_VIEW_PAY_BILL];;
                
                BTBillDetailViewController *billDetailVC = (BTBillDetailViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"BTBillDetailViewController"];
                billDetailVC.btAsset = self.selectedAsset;
                
                [self.navigationController pushViewController:billDetailVC animated:YES];
                
            }
            else if([rowWrapper.heading isEqualToString:kReviewUsageText]){
                
                [self trackClickForOmniture:OMNICLICK_ASSETS_REVIEW_USAGE];
                
                if ([asseType isEqualToString:@"Phone Services"]) {
                    BTPhoneUsageSummaryViewController *phoneUsageVC = [[BTPhoneUsageSummaryViewController alloc] init];
                    phoneUsageVC.selectedAsset = self.selectedAsset;
                    phoneUsageVC.serviceNumber = self.assetsDetailCollectionModel.serviceNumber;
                    
                    if ([self.assetsDetailCollectionModel.serviceNumber rangeOfString:@"cv" options:NSCaseInsensitiveSearch].location != NSNotFound) { // Cloud Voice details
                        phoneUsageVC.isCloudVoiceExpress = YES;
                        phoneUsageVC.navigationTitle = @"Cloud Voice Express usage";
                    }
                    else { // Other phone calls details
                        phoneUsageVC.isCloudVoiceExpress = NO;
                        phoneUsageVC.navigationTitle  = @"Phone usage summary";
                    }
                    
                    [self.navigationController pushViewController:phoneUsageVC animated:YES];
                }
                else if ([_assetType isEqualToString:@"Broadband & Internet"]) {
                    BTBroadbandUsageViewController *broadbandUsageVC = [[BTBroadbandUsageViewController alloc] init];
                    broadbandUsageVC.assetIntegrationID = self.assetsDetailCollectionModel.assetIntegrationID;
                    broadbandUsageVC.selectedAsset = self.selectedAsset;
                    [self.navigationController pushViewController:broadbandUsageVC animated:YES];
                }
            }
        }
    }
    
    

    
}

- (void)openSafariBrowserWithURL:(NSString *)urlString
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}

#pragma mark- BTAssetServiceDetailHeaderView Delegate

- (void)btAssetServiceDetailHeaderView:(BTAssetServiceDetailHeaderView *)view didSelectedSegmentAtIndex:(NSInteger)index{
    
    if(index ==0)
    {
        _pageNameForOmniture = OMNIPAGE_ASSETS_DETAIL_SUMMARY;
        [self trackPageForOmniture:OMNIPAGE_ASSETS_DETAIL_SUMMARY];
    }
    else
    {
        _pageNameForOmniture = OMNIPAGE_ASSETS_DETAIL_RENTAL;
        [self trackPageForOmniture:OMNIPAGE_ASSETS_DETAIL_RENTAL];
    }

    
    
    _seclectedTabIndex = index;
    [_tableView reloadData];
    
}

#pragma mark - Event tracking methods
- (void)trackClickForOmniture:(NSString *)clickEvent
{
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",_pageNameForOmniture,clickEvent]];
}

- (void)trackPageForOmniture:(NSString *)page
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];

    [OmnitureManager trackPage:page withContextInfo:data];
}

@end
