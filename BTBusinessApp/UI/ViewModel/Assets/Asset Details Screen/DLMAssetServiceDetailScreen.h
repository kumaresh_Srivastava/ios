//
//  DLMAssetServiceDetailScreen.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/9/16.
//  Copyright © 2016 Accolite. All rights reserved.
//


typedef enum{
    
    ServiceDetailRowTypeUnknown,
    ServiceDetailRowTypeTakeAction,
    ServiceDetailRowTypeSummary,
    ServiceDetailRowTypeRentalDetail,
    ServiceDetailRowTypeRentalTotal
    
}ServiceDetailRowType;


#import <Foundation/Foundation.h>
#import "BTAssetDetailCollection.h"

#define kCheckServiceText @"Check service status"
//#define kViewBillText @"View or pay latest bill"
//#define kReviewUsageText @"Review my usage"
#define kViewBillText @"View latest bill"
#define kReviewUsageText @"View usage"
#define kMobileServiceFAQText @"Mobile Services FAQs"

#define kSectionTitleKey @"title"
#define kSectionRowsKey @"rows"

@interface DLMAssetServiceDetailScreen : NSObject

- (id)initWithBTAssetDetailCollection:(BTAssetDetailCollection *)model andAssetType:(NSString *)assetType;

- (NSArray *)getSummarySectionArray;
- (NSArray *)getRentalSectionArray;

@end



//Wrapper is created to wrap different data for population on UI. Properties of wrapper could hold any thing(price, location, contact etc.)
@interface AssetsDetailRowWrapper : NSObject
@property(nonatomic, assign) ServiceDetailRowType cellType;
@property(nonatomic, strong) NSString *heading;
@property(nonatomic, strong) NSString *subHeading1;
@property(nonatomic, strong) NSString *subHeading2;
@property(nonatomic, strong) NSString *subHeading3;
- (NSAttributedString *)getRentalAttributtedStringForSrting:(NSString *)inputString;
- (NSAttributedString *)attributedStringFotBoldString:(NSString *)boldString andBoldString:(NSString *)regularSting;
@end




