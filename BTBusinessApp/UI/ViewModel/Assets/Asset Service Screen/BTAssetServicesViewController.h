//
//  BTAssetServicesViewController.h
//  BTBusinessApp
//
//  Created by Accolite on 09/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTAsset.h"

@interface BTAssetServicesViewController : UIViewController
@property (nonatomic,strong) NSArray *servicesArray;
@property (nonatomic,strong) NSString *titleString;
@property (nonatomic,strong) BTAsset *selectedAsset;
@end
