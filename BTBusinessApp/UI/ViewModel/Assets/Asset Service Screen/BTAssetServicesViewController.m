//
//  BTAssetServicesViewController.m
//  BTBusinessApp
//
//  Created by Accolite on 09/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTAssetServicesViewController.h"
#import "BTAssetServicesTableViewCell.h"
#import "BTAssetDetailCollection.h"
#import "BTAssetsServiceDetailViewController.h"
#import "OmnitureManager.h"
#import "BTMobileServiceTableViewCell.h"
#import "AppManager.h"
#import "AppDelegate.h"

@interface BTAssetServicesViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *assetsServicesTableView;
@end
//BTAssetServicesTableViewCell
@implementation BTAssetServicesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

//    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonAction)];
//    [self.navigationItem setRightBarButtonItem:rightBarButton];
    
    //table view config
    self.assetsServicesTableView.delegate = self;
    self.assetsServicesTableView.dataSource = self;
    self.assetsServicesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.assetsServicesTableView.backgroundColor = [UIColor clearColor];
    self.assetsServicesTableView.estimatedRowHeight = 120;
    self.assetsServicesTableView.rowHeight = UITableViewAutomaticDimension;

    self.title = self.titleString;
    //Registering the NIbs
    if( [self.titleString caseInsensitiveCompare:@"Mobile services"] == NSOrderedSame && kIsMobileServiceAssetsEnabled == YES){
    
        UINib *dataCell = [UINib nibWithNibName:@"BTMobileServiceTableViewCell" bundle:nil];
        [self.assetsServicesTableView registerNib:dataCell forCellReuseIdentifier:@"mobileServiceTableViewCell"];
        
        //// KUMARESH HACK
        // BTAssetDetailCollection *assetsDetailCollectionObj = [self.servicesArray objectAtIndex:indexPath.row];
      /*
        BTAssetsServiceDetailViewController *conroller = [BTAssetsServiceDetailViewController getBTAssetsServiceDetailViewController];
        //conroller.assetsDetailCollectionModel = assetsDetailCollectionObj;
        conroller.assetType = @"Mobile services";
        conroller.selectedAsset = self.selectedAsset;
        [self.navigationController pushViewController:conroller animated:YES];
       */
        // KUMARESH HACK END
        
    } else{
        
        UINib *dataCell = [UINib nibWithNibName:@"BTAssetServicesTableViewCell" bundle:nil];
        [self.assetsServicesTableView registerNib:dataCell forCellReuseIdentifier:@"assetServicesTableViewCell"];
    }
     self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self trackPageForOmniture];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.assetsServicesTableView deselectRowAtIndexPath:[self.assetsServicesTableView indexPathForSelectedRow] animated:YES];
   // AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //appDelegate.isSingleBacCugBackNavButtonPressed = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark Action Methods

- (void)doneButtonAction {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidDisappear:(BOOL)animated{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if( appDelegate.isSingleBacCug == YES){
        if (self.isMovingFromParentViewController || self.isBeingDismissed) { // BACK Button is PRESSED
            appDelegate.isSingleBacCugBackNavButtonPressed = YES;
        }
        else{
            appDelegate.isSingleBacCugBackNavButtonPressed = NO;
        }
    }
}

#pragma mark - table view methods


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.servicesArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( [self.titleString caseInsensitiveCompare:@"Mobile services"] == NSOrderedSame && kIsMobileServiceAssetsEnabled == YES) {

        BTMobileServiceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mobileServiceTableViewCell" forIndexPath:indexPath];
        BTAssetDetailCollection* data = [self.servicesArray objectAtIndex:indexPath.row];
        [cell updateMobileServicesCell:data.assetNumber];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    } else {
        
        BTAssetServicesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"assetServicesTableViewCell" forIndexPath:indexPath];
        [cell updateAssetServicesCell:[self.servicesArray objectAtIndex:indexPath.row]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
   
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Add the pushViewController functionality here
    
    [self trackClickForOmniture];
    BTAssetDetailCollection *assetsDetailCollectionObj = [self.servicesArray objectAtIndex:indexPath.row];
    
    BTAssetsServiceDetailViewController *conroller = [BTAssetsServiceDetailViewController getBTAssetsServiceDetailViewController];
    conroller.assetsDetailCollectionModel = assetsDetailCollectionObj;
    conroller.assetType = self.titleString;
    conroller.selectedAsset = self.selectedAsset;
    [self.navigationController pushViewController:conroller animated:YES];
    
}



#pragma mark - Omniture Tracking

- (void)trackPageForOmniture
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
   
    [OmnitureManager trackPage:OMNIPAGE_ASSETS_SERVICES withContextInfo:data];
}


- (void)trackClickForOmniture
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];

    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",OMNIPAGE_ASSETS_SERVICES,OMNICLICK_ASSETS_SERVICE_DETAILS] withContextInfo:data];
}



@end
