//
//  BTViewAssetsContainerViewController.h
//  BTBusinessApp
//
//  Created by Accolite on 27/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BTAssetsDashboardViewController : UIViewController
@property (nonatomic, copy) NSString *groupKey;
@property (nonatomic) NSArray *cugs;

+ (BTAssetsDashboardViewController *)getAssetsDashboardViewController;

@end
