//
//  DLMAssetDashboard.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 08/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMAssetDashboardScreen.h"
#import "NLAssetsDashboardWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "CDCug+CoreDataClass.h"

@interface DLMAssetDashboardScreen () <NLAssetsDashBoardWebServiceDelegate> {

}

@property (nonatomic) NLAssetsDashboardWebService *getAssetsDashboardWebService;

@end

@implementation DLMAssetDashboardScreen


#pragma mark - Public methods
- (void)fetchAssetsDashboardDetails
{
    self.getAssetsDashboardWebService = [[NLAssetsDashboardWebService alloc] init];
    self.getAssetsDashboardWebService.assetsDashBoardWebServiceDelegate = self;
    [self.getAssetsDashboardWebService resume];

}


- (void)cancelAssetsDashboardAPIRequest
{
    self.getAssetsDashboardWebService.assetsDashBoardWebServiceDelegate = nil;
    [self.getAssetsDashboardWebService cancel];
    self.getAssetsDashboardWebService = nil;
}


- (void)changeSelectedCUGTo:(BTCug *)selectedCug
{
    NSManagedObjectContext *context = [AppDelegate sharedInstance].managedObjectContext;
    CDCug *newPersistenceCug = [CDCug newCugInManagedObjectContext:context];
    newPersistenceCug.groupKey = selectedCug.groupKey;
    newPersistenceCug.cugName = selectedCug.cugName;
    newPersistenceCug.cugId = selectedCug.cugID;
    newPersistenceCug.refKey = selectedCug.refKey;
    newPersistenceCug.contactId = selectedCug.contactId;
    newPersistenceCug.cugRole = @(selectedCug.cugRole);
    newPersistenceCug.indexInAPIResponse = @(selectedCug.indexInAPIResponse);

    // Get the currently logged in User
    CDUser *loggedInUser = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    CDCug *previouslySelectedCug = loggedInUser.currentSelectedCug;
    loggedInUser.currentSelectedCug = newPersistenceCug;

    if(previouslySelectedCug)
    {
        [context deleteObject:previouslySelectedCug];
    }

    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
}


#pragma mark - NLAssetsDashBoardWebServiceDelegate Methods
- (void)getAssetsDashBoardWebService:(NLAssetsDashboardWebService *)webService successfullyFetchedAssetsDashBoardData:(NSArray *)assets
{
    if(webService == self.getAssetsDashboardWebService)
    {
        _assetsArray = assets;
        [self.assetDashboardScreenDelegate successfullyFetchedAssetsDashboardDataOnDLMAssetDashboardScreen:self];

        self.getAssetsDashboardWebService.assetsDashBoardWebServiceDelegate = nil;
        self.getAssetsDashboardWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLAssetsDashboardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLAssetsDashboardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }

}


- (void)getAssetsDashBoardWebService:(NLAssetsDashboardWebService *)webService failedToFetchAssetsDashBoardDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    if(webService == self.getAssetsDashboardWebService)
    {
        [self.assetDashboardScreenDelegate assetDashboardScreen:self failedToFetchAssetsDashboardDataWithWebServiceError:webServiceError];
        
        self.getAssetsDashboardWebService.assetsDashBoardWebServiceDelegate = nil;
        self.getAssetsDashboardWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLAssetsDashboardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLAssetsDashboardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

@end
