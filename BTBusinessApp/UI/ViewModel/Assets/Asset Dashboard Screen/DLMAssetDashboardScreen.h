//
//  DLMAssetDashboard.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 08/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
#import "BTAsset.h"
#import "BTCug.h"

@class DLMAssetDashboardScreen;

@class  NLWebServiceError;
@protocol DLMAssetDashboardScreenDelegate <NSObject>

- (void)successfullyFetchedAssetsDashboardDataOnDLMAssetDashboardScreen:(DLMAssetDashboardScreen *)assetDashboardScreen;

- (void)assetDashboardScreen:(DLMAssetDashboardScreen *)assetDashboardScreen failedToFetchAssetsDashboardDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface DLMAssetDashboardScreen : DLMObject

@property (nonatomic, copy) NSArray<BTAsset*> *assetsArray;
@property (nonatomic, weak) id <DLMAssetDashboardScreenDelegate> assetDashboardScreenDelegate;

- (void)fetchAssetsDashboardDetails;
- (void)cancelAssetsDashboardAPIRequest;
- (void)changeSelectedCUGTo:(BTCug *)selectedCug;

@end
