//
//  BTViewAssetsContainerViewController.m
//  BTBusinessApp
//
//  Created by Accolite on 27/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTAssetsDashboardViewController.h"
#import "BTViewAssetBACOverviewViewController.h"
#import "AppConstants.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "DLMAssetDashboardScreen.h"
#import "BTAssetTableViewCell.h"
#import "BTCug.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "BTEmptyDashboardView.h"
#import "BTAsset.h"
#import "BTRetryView.h"
#import "CustomSpinnerView.h"
#import "OmnitureManager.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "NLWebServiceError.h"
#import "AppManager.h"
#import "BTAccountLocationDropdownView.h"

#define kAssetBACOverviewScreen @"BTViewAssetBACOverviewViewController"
#define kNavigationBarHeightWithStatusBar 64

@interface BTAssetsDashboardViewController ()<UITableViewDelegate,UITableViewDataSource,DLMAssetDashboardScreenDelegate,BTRetryViewDelegate>
{
    UITableView *_assetsTableView;
    NSMutableArray *_assetsArray;
    BTEmptyDashboardView *_emptyDashboardView;
    BTRetryView *_retryView;
}

@property (nonatomic, readwrite) DLMAssetDashboardScreen *assetDashboardScreenModel;
@property(nonatomic, assign) BOOL networkRequestInProgress;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic,strong)  CustomSpinnerView *loadingView;
@property (weak, nonatomic) BTAccountLocationDropdownView *accountDropdownView;


@end

@implementation BTAssetsDashboardViewController



- (void)viewDidLoad
{
    [super viewDidLoad];

    //KUMARESH HACK
    
    /*
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    
    BTViewAssetBACOverviewViewController *bacOverviewScreen = (BTViewAssetBACOverviewViewController*)[storyboard instantiateViewControllerWithIdentifier:kAssetBACOverviewScreen];
    bacOverviewScreen.userSelectedBTAsset = nil;//[_assetsArray objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:bacOverviewScreen animated:YES];
    
    return;
     */

    //KUMARESH HACK END
    // Do any additional setup after loading the view
    [self setEdgesForExtendedLayout:UIRectEdgeNone];

    //[SD] View Model Initialization
    self.assetDashboardScreenModel = [[DLMAssetDashboardScreen alloc] init];
    self.assetDashboardScreenModel.assetDashboardScreenDelegate = self;

    if(!self.cugs) {
        self.cugs = [[AppDelegate sharedInstance] savedCugs];
    }
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

    
    //[SD] Initial UI SetUP
    [self createInitialUI];
    [self updateGroupSelection];

    UINib *dataCell = [UINib nibWithNibName:@"BTAssetTableViewCell" bundle:nil];
    [_assetsTableView registerNib:dataCell forCellReuseIdentifier:@"BTAssetTableViewCell"];

    self.navigationItem.hidesBackButton = YES;
    
    [self createLoadingView];

    __weak typeof(self) selfWeak = self;

    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([selfWeak networkRequestInProgress]) {

            dispatch_async(dispatch_get_main_queue(), ^{

                [selfWeak hideLoadingItems:NO];
                selfWeak.navigationItem.rightBarButtonItem.enabled = NO;
                [selfWeak.loadingView startAnimatingLoadingIndicatorView];


            });
        }

        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
            dispatch_async(dispatch_get_main_queue(), ^{

                [selfWeak hideLoadingItems:YES];
                selfWeak.navigationItem.rightBarButtonItem.enabled = YES;
                [selfWeak.loadingView stopAnimatingLoadingIndicatorView];

            });
        }
    }];

    [self trackPageForOmniture];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChangeAssetsDashBoard:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
        }
        _accountDropdownView.locationLabel.text = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugName;
        if(self.cugs.count == 1 && _assetsArray.count == 1) {
            _assetsTableView.hidden = YES;
        }
    
     [self tabelViewOrientation];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if( appDelegate.isSingleBacCugBackNavButtonPressed == YES ){
        appDelegate.isSingleBacCugBackNavButtonPressed = NO;
        return;
    }
     [self fetchAssetsDashboardAPI];

}

- (void)viewDidDisappear:(BOOL)animated{

    [super viewDidDisappear:animated];
    [self.assetDashboardScreenModel cancelAssetsDashboardAPIRequest];
    self.networkRequestInProgress = NO;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (self.isMovingFromParentViewController || self.isBeingDismissed) {
        appDelegate.isSingleBacCugBackNavButtonPressed = YES;
    }
    else{
        appDelegate.isSingleBacCugBackNavButtonPressed = NO;
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

#pragma mark - Public Methods

+ (BTAssetsDashboardViewController *)getAssetsDashboardViewController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    BTAssetsDashboardViewController *controller = (BTAssetsDashboardViewController *)[storyboard instantiateViewControllerWithIdentifier:@"BTAssetsDashboardViewController"];

    return controller;


}


#pragma mark - InitialUI Methods

- (void)createInitialUI
{

    CGSize screenSize = [UIScreen mainScreen].bounds.size;

    self.titleLabel.text = @"Accounts";
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    int cugSelectorOffset = 110;

    self.view.backgroundColor = [UIColor colorForHexString:@"eeeeee"];//[BrandColours colourBtNeutral30];

    //[SD] Strip for GugName if there is more than one cug
    if(self.cugs.count > 1)
    {
        self.navigationItem.rightBarButtonItem = nil;
        
        _accountDropdownView = [[[NSBundle mainBundle] loadNibNamed:@"BTAccountLocationDropdownView" owner:self options:nil] firstObject];
        BTCug *firstCug = self.cugs[0];
        _accountDropdownView.locationLabel.text = firstCug.cugName;
        
        UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(groupSelectionAction)];
        [_accountDropdownView addGestureRecognizer:tapped];
        
        [self.view insertSubview:_accountDropdownView atIndex:0];
        
        [self.view addConstraints:@[
                                    [NSLayoutConstraint constraintWithItem:_accountDropdownView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0],
                                    [NSLayoutConstraint constraintWithItem:_accountDropdownView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                    [NSLayoutConstraint constraintWithItem:_accountDropdownView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]
                                    ]];
    }
    else
    {

        UIFont *titleFont = self.titleLabel.font;
        self.titleLabel.font = [titleFont fontWithSize:20];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        cugSelectorOffset = 0;
    }
    //[SD]  UITableView Initialiation
    _assetsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,
                                                                     cugSelectorOffset,screenSize.width, screenSize.height - (71 + cugSelectorOffset)) style:UITableViewStylePlain
                        ];
    _assetsTableView.backgroundColor = [UIColor colorForHexString:@"eeeeee"];//[BrandColours colourBtNeutral30];
    _assetsTableView.delegate = self;
    _assetsTableView.dataSource = self;

    _assetsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _assetsTableView.translatesAutoresizingMaskIntoConstraints = NO;

    [self.view addSubview:_assetsTableView];

    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:_assetsTableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:_assetsTableView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:_assetsTableView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]
                                ]];
    if (cugSelectorOffset > 0) {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_assetsTableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_accountDropdownView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    } else {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_assetsTableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    }
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];

}

- (void)statusBarOrientationChangeAssetsDashBoard:(NSNotification *)notification {
    // handle the interface orientation as needed
    [self tabelViewOrientation];
}


- (void) tabelViewOrientation {
    
    int cugSelectorOffset = 110;
    if(self.cugs.count < 1){
        cugSelectorOffset = 0;
    }
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    _assetsTableView.frame = CGRectMake(0, _assetsTableView.frame.origin.y, screenSize.width, screenSize.height - (71 + cugSelectorOffset));
    _assetsTableView.contentInset = UIEdgeInsetsMake(0, 0, 120, 0);
}

#pragma mark - api call methods

- (void)fetchAssetsDashboardAPI {

    if([AppManager isInternetConnectionAvailable])
    {
         [self hideRetryItems:YES];
        self.networkRequestInProgress = YES;
        [self hideEmmptyDashBoardItems:YES];
        [self.assetDashboardScreenModel fetchAssetsDashboardDetails];
    }
    else
    {
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_ASSETS_ACCOUNT];
    }
}





#pragma mark - UITableViewDataSource Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _assetsArray.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTAssetTableViewCell *assetCell = [tableView dequeueReusableCellWithIdentifier:@"BTAssetTableViewCell" forIndexPath:indexPath];
    assetCell.selectionStyle = UITableViewCellSelectionStyleNone;
    [assetCell updateAssetsCellWithAsset:[_assetsArray objectAtIndex:indexPath.row]];
    return assetCell;

}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return 86;

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];

    BTViewAssetBACOverviewViewController *bacOverviewScreen = (BTViewAssetBACOverviewViewController*)[storyboard instantiateViewControllerWithIdentifier:kAssetBACOverviewScreen];
    bacOverviewScreen.userSelectedBTAsset = [_assetsArray objectAtIndex:indexPath.row];
    bacOverviewScreen.isSingleBacCUG = NO;
    [self.navigationController pushViewController:bacOverviewScreen animated:YES];
}


#pragma mark - Private Helper Methods
- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow  {

    if(_retryView)
    {
        _retryView.hidden = NO;
        _retryView.retryViewDelegate = self;
        [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
        return;
    }
    _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
    _retryView.translatesAutoresizingMaskIntoConstraints = NO;
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
    _retryView.retryViewDelegate = self;

    [self.view addSubview:_retryView];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-50.0]];

}

- (void)hideRetryItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to retry.
    [_retryView setHidden:isHide];

}

- (void)createLoadingView {

    if(!_loadingView){

        _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        _loadingView.translatesAutoresizingMaskIntoConstraints = NO;

        [self.view addSubview:_loadingView];
    }

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}

- (void)hideLoadingItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to loading.
    [_loadingView setHidden:isHide];

}


/*
 Updates the currently selected group and UI on the basis of group selection
 */
- (void) checkForSuperUserAndUpdateUIWithIndex:(NSUInteger)index {

    [self trackCUGChange];
    
    for(BTCug *cug in self.cugs) {
        if(index == cug.indexInAPIResponse) {
            [self setGroupKey:cug.groupKey];
            _accountDropdownView.locationLabel.text = cug.cugName;
            [self.assetDashboardScreenModel changeSelectedCUGTo:cug];
            _accountDropdownView.locationLabel.text = cug.cugName;
            [self resetDataAndUIAfterGroupChange];
        }
    }
}


- (void)resetDataAndUIAfterGroupChange
{

    BOOL needToRefresh = YES;

    if(_retryView)
    {
        if((![AppManager isInternetConnectionAvailable] && !_retryView.isHidden))
            needToRefresh = NO;
    }

    if(needToRefresh){

        [self hideRetryItems:YES];
        _assetsTableView.hidden = YES;
        [self fetchAssetsDashboardAPI];
    }
    else
    {
        [_retryView updateRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_ASSETS_ACCOUNT];
    }

}



- (void)updateGroupSelection {
    if (self.cugs.count == 1) {
        return;
    }

    BTCug *cug = (BTCug *)[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug;

    [self setGroupKey:cug.groupKey];
    _accountDropdownView.locationLabel.text = cug.cugName;
}

- (void)showEmptyDashBoardView {

    //Error Tracker
     [OmnitureManager trackError:OMNIERROR_EMPTY_ACCOUNT_PROCESSSING onPageWithName:OMNIPAGE_ASSETS_ACCOUNT contextInfo:[AppManager getOmniDictionary]];
    
    _emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTEmptyDashboardView" owner:nil options:nil] objectAtIndex:0];
    _emptyDashboardView.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSURL *url;
    
    if ([kBTServerType isEqualToString:@"BTServerTypeProd"]) {
        url = [[NSURL alloc] initWithString:@"https://secure.business.bt.com/Account/Addbillingaccounts.aspx"];
    } else{
        url = [[NSURL alloc] initWithString:@"https://eric1-dmze2e-ukb.bt.com/Account/Addbillingaccounts.aspx"];
    }
    
    NSMutableAttributedString *hypLink = [[NSMutableAttributedString  alloc] initWithString:@"There’s no information to show just yet. Your account is either pending approval by your admin, or you can add an account here."];
    
    NSDictionary *linkAttributes = @{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle),
                                     NSLinkAttributeName : url};
    
    [hypLink addAttributes:linkAttributes range:NSMakeRange(hypLink.length-5,4)];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
    [hypLink addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,hypLink.length)];
    
    [hypLink addAttribute:NSForegroundColorAttributeName value:[BrandColours colourBtNeutral70] range:NSMakeRange(0, hypLink.length)];
    
    [hypLink addAttribute:NSFontAttributeName
                    value:[UIFont fontWithName:kBtFontRegular size:16.0] range:NSMakeRange(0, hypLink.length)];
    
    [_emptyDashboardView updateEmptyDashboardViewWithImageNameAndLink:@"package" title:@"No accounts to display" detailText:hypLink andButtonTitle:nil];
    
    [self.view addSubview:_emptyDashboardView];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    if(self.accountDropdownView){
       [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.accountDropdownView  attribute:NSLayoutAttributeTop multiplier:1.0 constant:self.accountDropdownView.frame.size.height]];
    } else{
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    }
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];

}

- (void)hideEmmptyDashBoardItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to retry.
    [_emptyDashboardView setHidden:isHide];
    [_emptyDashboardView removeFromSuperview];
    _emptyDashboardView = nil;

}


- (void)groupSelectionAction
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Select Group" preferredStyle:UIAlertControllerStyleActionSheet];

    DDLogVerbose(@"Bills: Total cugs belongs to user %lu.",(unsigned long)self.cugs.count);

    // (SD) Add group names in actionsheet
    if (self.cugs != nil && self.cugs.count > 0) {

        // (SD) Add action for each cug selection.
        int index = 0;
        for (BTCug *groupData in self.cugs) {

            __weak typeof(self) selfWeak = self;

            UIAlertAction *action = [UIAlertAction actionWithTitle:groupData.cugName
                                                             style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                 [selfWeak checkForSuperUserAndUpdateUIWithIndex:groupData.indexInAPIResponse];
                                                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                                             }];
            [alert addAction:action];

            index++;
        }
    }

    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    [alert addAction:cancel];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = _accountDropdownView.frame;
    alert.popoverPresentationController.canOverlapSourceViewRect = NO;

    [self presentViewController:alert animated:YES completion:^{

    }];

}



#pragma mark - Action Methods

- (void)doneButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - DLMAssetDashboardScreenDelegate

- (void)successfullyFetchedAssetsDashboardDataOnDLMAssetDashboardScreen:(DLMAssetDashboardScreen *)assetDashboardScreen
{
    self.networkRequestInProgress = NO;
    _assetsTableView.hidden = NO;
    if(assetDashboardScreen.assetsArray.count > 0)
    {
        _assetsArray =  [assetDashboardScreen.assetsArray mutableCopy];
        //// Account - CUG/BAC if one then navigate to next screen
        if(self.cugs.count == 1 && _assetsArray.count == 1){
            
             AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            appDelegate.isSingleBacCug = YES;
          
            // redirect to detail screen
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                        @"Main" bundle:[NSBundle mainBundle]];
            
            BTViewAssetBACOverviewViewController *bacOverviewScreen = (BTViewAssetBACOverviewViewController*)[storyboard instantiateViewControllerWithIdentifier:kAssetBACOverviewScreen];
            bacOverviewScreen.userSelectedBTAsset = [_assetsArray objectAtIndex:0];
            bacOverviewScreen.isSingleBacCUG = YES;
            [self.navigationController pushViewController:bacOverviewScreen animated:NO];
            
        } else {
             [_assetsTableView reloadData];
        }
    }
    else if(assetDashboardScreen.assetsArray.count == 0)
    {
        [self showEmptyDashBoardView];
    }
    else
    {
        DDLogError(@"Assets Dashboard: Assets not found.");
    }



}

- (void)assetDashboardScreen:(DLMAssetDashboardScreen *)assetDashboardScreen failedToFetchAssetsDashboardDataWithWebServiceError:(NLWebServiceError *)webServiceError {
    self.networkRequestInProgress = NO;
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];

    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {

        switch (webServiceError.error.code)
        {

            case BTNetworkErrorCodeAPINoDataFound:
            {
                errorHandled = YES;
                [self showEmptyDashBoardView];
                [AppManager trackNoDataFoundErrorOnPage:OMNIPAGE_ASSETS_ACCOUNT];
                break;
            }
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }

    if(errorHandled == NO)
    {
        [self showRetryViewWithInternetStrip:NO];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_ASSETS_ACCOUNT];
    }

}


#pragma mark - RetryView Delgates

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    [self fetchAssetsDashboardAPI];

}




#pragma mark - Omniture Methods


- (void)trackPageForOmniture
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:OMNIPAGE_ASSETS_ACCOUNT withContextInfo:data];

}


- (void)trackCUGChange
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",OMNIPAGE_ASSETS_ACCOUNT,OMNICLICK_CUG_CHANGE] withContextInfo:data];
}

@end
