//
//  UIViewController+WebServiceErrorHandling.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 02/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "UIViewController+WebServiceErrorHandling.h"
#import "NLWebServiceError.h"
#import "AppConstants.h"

@implementation UIViewController (WebServiceErrorHandling)

#pragma mark- Public methods

- (BOOL)attemptVordelProtectedAPIErrorHandlingOfWebServiceError:(NLWebServiceError *)error
{
    BOOL errorHandled = NO;

    if([error.sourceError.error.domain isEqualToString:BTNetworkErrorDomain] && (error.sourceError.error.code == BTNetworkErrorCodeVordelTokenInvalidOrExpired || error.sourceError.error.code == BTNetworkErrorCodeVordelRefreshInvalidOrExpired))
    {
        errorHandled = YES;

        switch (error.error.code)
        {
            case BTNetworkErrorCodeVordelParameterMissing:
            {
                [self raiseAppLockDownToLoginScreenNotificationWithAlertMessage:kAppLockDownToLoginScreenAlertMessage1];
                break;
            }

            case BTNetworkErrorCodeVordelParameterInvalid:
            {
                [self raiseAppLockDownToLoginScreenNotificationWithAlertMessage:kAppLockDownToLoginScreenAlertMessage1];
                break;
            }

            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }


    if([error.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {
        errorHandled = YES;

        switch (error.error.code)
        {
//            case BTNetworkErrorCodeNoInternet:
//            {
//                // TODO: (hds) Handle this case
//                break;
//            }

            case BTNetworkErrorCodeVordelUserUnauthenticated:
            {
                [self raiseAppLockDownToLoginScreenNotificationWithAlertMessage:kAppLockDownToLoginScreenAlertMessagePasswordChanged];
                break;
            }

            case BTNetworkErrorCodeVordelTokenInvalidOrExpired:
            {
                [self raiseAppLockDownToLoginScreenNotificationWithAlertMessage:kAppLockDownToLoginScreenAlertMessagePasswordChanged];
                break;
            }

            case BTNetworkErrorCodeVordelDeviceMismatch:
            {
                [self raiseAppLockDownToLoginScreenNotificationWithAlertMessage:kAppLockDownToLoginScreenAlertMessage1];
                break;
            }

            case BTNetworkErrorCodeVordelRefreshInvalidOrExpired:
            {
                [self raiseAppLockDownToLoginScreenNotificationWithAlertMessage:kAppLockDownToLoginScreenAlertMessagePasswordChanged];
                break;
            }

            case BTNetworkErrorCodeVordelAccountLocked:
            {
                [self raiseAppLockDownToPinUnlockScreenNotificationWithAlertMessage:kProfileLockedMessage andAlertTitle:@"Account Locked"];
                break;
            }

            case BTNetworkErrorCodeVordelPlannedServiceOutage:
            {
                [self raiseAppLockDownToPinUnlockScreenNotificationWithAlertMessage:kAppLockDownToPinUnlockScreenAlertMessageUnderMaintenance andAlertTitle:@"Under Maintenance"];
                break;
            }

            case BTNetworkErrorCodeVordelUnplannedServiceOutage:
            {
                [self raiseAppLockDownToPinUnlockScreenNotificationWithAlertMessage:kAppLockDownToPinUnlockScreenAlertMessageUnderMaintenance andAlertTitle:@"Under Maintenance"];
                break;
            }

            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }

    return errorHandled;
}

- (BOOL)attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:(NLWebServiceError *)error
{
    BOOL errorHandled = NO;

    if([error.sourceError.error.domain isEqualToString:BTNetworkErrorDomain] && error.sourceError.error.code == BTNetworkErrorCodeSMSessionUnauthenticaiton)
    {
        errorHandled = YES;

        switch (error.error.code)
        {
            case BTNetworkErrorCodeVordelParameterMissing:
            {
                [self raiseAppLockDownToLoginScreenNotificationWithAlertMessage:kAppLockDownToLoginScreenAlertMessage1];
                break;
            }

            case BTNetworkErrorCodeVordelParameterInvalid:
            {
                [self raiseAppLockDownToLoginScreenNotificationWithAlertMessage:kAppLockDownToLoginScreenAlertMessage1];
                break;
            }

            case BTNetworkErrorCodeVordelUserUnauthenticated:
            {
                [self raiseAppLockDownToLoginScreenNotificationWithAlertMessage:kAppLockDownToLoginScreenAlertMessagePasswordChanged];
                break;
            }

            case BTNetworkErrorCodeVordelDeviceMismatch:
            {
                [self raiseAppLockDownToLoginScreenNotificationWithAlertMessage:kAppLockDownToLoginScreenAlertMessage1];
                break;
            }

            case BTNetworkErrorCodeVordelAccountLocked:
            {
                [self raiseAppLockDownToPinUnlockScreenNotificationWithAlertMessage:kProfileLockedMessage andAlertTitle:@"Account Locked"];
                break;
            }

            case BTNetworkErrorCodeVordelPlannedServiceOutage:
            {
                [self raiseAppLockDownToPinUnlockScreenNotificationWithAlertMessage:kAppLockDownToPinUnlockScreenAlertMessageUnderMaintenance andAlertTitle:@"Under Maintenance"];
                break;
            }

            case BTNetworkErrorCodeVordelUnplannedServiceOutage:
            {
                [self raiseAppLockDownToPinUnlockScreenNotificationWithAlertMessage:kAppLockDownToPinUnlockScreenAlertMessageUnderMaintenance andAlertTitle:@"Under Maintenance"];
                break;
            }

            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }


    if([error.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {
        errorHandled = YES;

        switch (error.error.code)
        {

            case BTNetworkErrorCodeSMSessionUnauthenticaiton:
            {
                [self raiseAppLockDownToLoginScreenNotificationWithAlertMessage:kAppLockDownToLoginScreenAlertMessagePasswordChanged];
                break;
            }


            case BTNetworkErrorCodeAPIUserProfileLocked:
            {
                [self raiseAppLockDownToPinUnlockScreenNotificationWithAlertMessage:kProfileLockedMessage andAlertTitle:@"Account Locked"];
                break;
            }

            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }

    return errorHandled;
}




#pragma mark- Private Helper methods

- (void)raiseAppLockDownToLoginScreenNotificationWithAlertMessage:(NSString *)alertMessage
{
    NSDictionary *infoDic = [NSDictionary dictionaryWithObjectsAndKeys:@"Password changed", @"title", alertMessage, @"message", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAppNeedsLockDownWithLoginScreen object:nil userInfo:infoDic];
}

- (void)raiseAppLockDownToPinUnlockScreenNotificationWithAlertMessage:(NSString *)alertMessage andAlertTitle:(NSString *)alertTitle
{
    NSDictionary *infoDic = [NSDictionary dictionaryWithObjectsAndKeys:alertTitle, @"title", alertMessage, @"message", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAppNeedsLockDownWithPINUnlockScreen object:nil userInfo:infoDic];
}

@end
