//
//  UIViewController+WebServiceErrorHandling.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 02/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NLWebServiceError;

@interface UIViewController (WebServiceErrorHandling)

- (BOOL)attemptVordelProtectedAPIErrorHandlingOfWebServiceError:(NLWebServiceError *)error;

- (BOOL)attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:(NLWebServiceError *)error;

@end
