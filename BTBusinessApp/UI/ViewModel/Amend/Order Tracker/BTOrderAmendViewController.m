//
//  OrderAmendViewController.m
//  SampleActionViewProject
//
//  Created by VectoScalar on 10/26/16.
//  Copyright © 2016 VectoScalar. All rights reserved.
//

#import "BTOrderAmendViewController.h"
#import "AmendViewModel.h"
#import "BTSiteContactsViewController.h"
#import "BTOrderSiteContactsViewController.h"
#import "DLMAmmendOrderScreen.h"
#import "AppManager.h"
#import "AppConstants.h"
#import "BTAmendEngineerViewController.h"
#import "BTOrderSiteContactModel.h"
#import "BTContact.h"
#import "BTProduct.h"
#import "BTAppointment.h"
#import "NSObject+APIResponseCheck.h"
#import "BTChangeActivationDateViewController.h"
#import "BTOrderMilestoneNodeModel.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "NLWebServiceError.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "BTAppointmentPickerViewController.h"
@interface BTOrderAmendViewController ()<DLMAmmendOrderScreenDelegate, BTOrderSiteContactsViewControllerDelegate, BTAmendEngineerViewControllerDelegate, BTChangeActivationDateDelegate, BTAppointmentPickerViewControllerDelegate>{
    
    DLMAmmendOrderScreen *_amendScreenModel;
    
    BTAppointment *_previousEngineerApointment;
    BTAppointment *_currentSelectedEngineerApointment;
    
    BTOrderSiteContactModel *_previousSiteContact;
    BTOrderSiteContactModel *_currentSelectedSiteContact;
    
    BTOrderSiteContactModel *_previousAltSiteContact;
    BTOrderSiteContactModel *_currentAltSelectedSiteContact;
    
    NSDate *_currectSelectedActivationDate;
    
    NSDictionary *_newAddedSiteContactDict;
}


//@property(nonatomic, strong) DLMAmmendOrderScreen *amendCheckModel;

@end

@implementation BTOrderAmendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Creating screen model
    _amendScreenModel = [[DLMAmmendOrderScreen alloc] init];
    _amendScreenModel.ammendOrderDelegate = self;
    //self.needToPopulateDummyData = YES;
    //Create data
    [self createData];
    
    [self makeAmendCheckAPICall];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self setDataChangeStatus];
}


#pragma mark - Class Method

//Class Method to get the instance of this class
+ (BTOrderAmendViewController *)getBTOrderAmendViewController{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTOrderAmendViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTOrderAmendViewControllerID"];
    return vc;
}



#pragma mark- ***********Super Class Methods**********

- (void)createData{
    
    
    //Dummy Data loading for dev purpose only
    if(self.needToPopulateDummyData){

        NSDate *date = [AppManager NSDateWithUTCFormatFromNSString:@"2016-12-30T00:00:00Z"];
        
        DLMOrderDetailsScreen *dummyModel = [[DLMOrderDetailsScreen alloc] init];
        dummyModel.engineerAppointmentDate = date;
        dummyModel.activationDate = date;
        dummyModel.orderRef = @"BTTZ9137";
        dummyModel.itemRef = @"BTTZ91371-1";
        self.orderReference = @"BTTZ9137";
        self.itemReference = @"BTTZ91371-1";
        
        //Site Contact
        NSMutableDictionary *siteContactDict = [NSMutableDictionary dictionary];
        [siteContactDict setValue:@"salman@gmail.com" forKey:@"Email"];
        [siteContactDict setValue:@"Mohd" forKey:@"FirstName"];
        [siteContactDict setValue:@"salman" forKey:@"LastName"];
        [siteContactDict setValue:@"0798765434" forKey:@"Phone"];
        [siteContactDict setValue:@"Mr" forKey:@"Title"];
        [siteContactDict setValue:@"820ZDHT716" forKey:@"ContactKey"];
        

        BTOrderSiteContactModel *siteContact = [[BTOrderSiteContactModel alloc] initSiteContactWithResponseDict:siteContactDict];
        dummyModel.siteContact = siteContact;
        
        
        //Alt Site Contact
        siteContactDict = [NSMutableDictionary dictionary];
        [siteContactDict setValue:@"salman@gmail.com" forKey:@"Email"];
        [siteContactDict setValue:@"B" forKey:@"FirstName"];
        [siteContactDict setValue:@"T" forKey:@"LastName"];
        [siteContactDict setValue:@"0798765434" forKey:@"Phone"];
        [siteContactDict setValue:@"Mr" forKey:@"Title"];
        [siteContactDict setValue:@"820ZDHT717" forKey:@"ContactKey"];
        
        
        BTOrderSiteContactModel *altSiteContact = [[BTOrderSiteContactModel alloc] initSiteContactWithResponseDict:siteContactDict];
        dummyModel.altSiteContact = altSiteContact;
        
        self.oderDetailModel = dummyModel;
    }
    
    _currectSelectedActivationDate = self.oderDetailModel.activationDate;
    
    
    //Creating Apointment model from date
    BTAppointment *appointment = nil;
    if(self.oderDetailModel.engineerAppointmentDate){
        
        NSString *timeslot = self.oderDetailModel.productSummary.engineeringAppointmentSlot;
        timeslot = [timeslot stringByReplacingOccurrencesOfString:@"(" withString:@""];
        timeslot = [timeslot stringByReplacingOccurrencesOfString:@")" withString:@""];
        
        appointment = [[BTAppointment alloc] initWithDate:self.oderDetailModel.productSummary.engineeringAppointmentDate andTimeslot:timeslot];
        
//        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//
//        NSInteger hour = [AppManager getHourFromDate:self.oderDetailModel.engineerAppointmentDate];
//
//        if (hour < 13) {
//
//            [dict setValue:[NSNumber numberWithBool:YES] forKeyPath:@"HasAM"];
//            [dict setValue:[NSNumber numberWithBool:YES] forKeyPath:@"IsAMSelected"];
//
//        }
//        else{
//
//            [dict setValue:[NSNumber numberWithBool:YES] forKeyPath:@"HasPM"];
//            [dict setValue:[NSNumber numberWithBool:YES] forKeyPath:@"IsPMSelected"];
//
//        }
//
//        NSString *dateFormat = @"dd/MM/yyyy";
//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:dateFormat];
//
//
//        NSString *dateString = [dateFormatter stringFromDate:self.oderDetailModel.engineerAppointmentDate];
//        [dict setValue:dateString forKeyPath:@"DateInfo"];
//
//        if(self.needToPopulateDummyData){
//            //For dev only
//            [dict setValue:@"2016-12-30T00:00:00Z" forKeyPath:@"DateInfo"];
//        }
//
//
//        appointment = [[BTAppointment alloc] initWithResponseDictionaryFromAppointmentsAPIResponse:dict];
    }

    
    //Setting instaces to keep track changes and original data
    _previousEngineerApointment = appointment;
    _currentSelectedEngineerApointment = appointment;
    
    _previousSiteContact = self.oderDetailModel.siteContact;
    _currentSelectedSiteContact = _previousSiteContact;
    
    _previousAltSiteContact = self.oderDetailModel.altSiteContact;
    _currentAltSelectedSiteContact = _previousAltSiteContact;
    

    [self populateData];
}



- (void)populateData{
    
    //Creating model objects for data population in tableview
    NSMutableArray *dataArray = [NSMutableArray array];
    
    
    //Engineer Appointment ---
    
    //Logic to calulate the shift(am or pm) from date
    
    if(_currentSelectedEngineerApointment){
        
//        NSString *amPM = nil;
//        if (_currentSelectedEngineerApointment.isAMSelected) {
//
//            amPM = @"8am - 1pm";
//
//        } else if (_currentSelectedEngineerApointment.isPMSelected) {
//
//            amPM = @"1pm - 6pm";
//        }

        NSString *amPM = _currentSelectedEngineerApointment.selectedTimeslot;
        NSString *dateString = [self getAppointmentFormattedDate:_currentSelectedEngineerApointment.dateInfo];
        
        
        //Model object for Engineer Appointment Cell
        AmendViewModel *engineerAppointment = [[AmendViewModel alloc] init];
        engineerAppointment.amendContext = kEngineerAppointmentText;
        
        NSString *amendAttribute = [self getAmendAttributeForAmendContext:engineerAppointment.amendContext];
        if(self.isShowAppointment == NO){
            engineerAppointment.isEditable =  NO;
            engineerAppointment.subHeading1 = [[NSAttributedString alloc] initWithString:@"Will be updated shortly"];
            engineerAppointment.subHeading2 = nil;
            
        } else {
            engineerAppointment.isEditable =  [_amendScreenModel isOrderAmendableForAmendAttribute:amendAttribute];
            engineerAppointment.subHeading1 = [engineerAppointment getAttributtedStringForTextString:@"Date " andValuString:dateString];
            if(amPM)
                engineerAppointment.subHeading2 = [engineerAppointment getAttributtedStringForTextString:@"Time " andValuString:amPM];
            else
                engineerAppointment.subHeading2 = nil;
        }

        [dataArray addObject:engineerAppointment];

    }
    
    
    NSString *siteContactName = [self getName:_currentSelectedSiteContact];
    if(_currentSelectedSiteContact && siteContactName.length > 0){
        
        //Model object for Site Contact Cell
        AmendViewModel *siteContactAmend = [self getAmendModelFromSiteContact:_currentSelectedSiteContact andHeadtingText:kSiteContactText];
        [dataArray addObject:siteContactAmend];

    }
    
    
    NSString *altSiteContactName = [self getName:_currentAltSelectedSiteContact];
    if(_currentAltSelectedSiteContact && altSiteContactName.length >0){
        
        //Model object for Site Contact Cell
        AmendViewModel *altSiteContactAmend = [self getAmendModelFromSiteContact:_currentAltSelectedSiteContact andHeadtingText:kAlternateContactText];
        [dataArray addObject:altSiteContactAmend];

    }
    
    
    if(_currectSelectedActivationDate){
        
        
        //(Sal):Commenting time slot code, as we are not showing time slot with activation date
        /*
        NSInteger hour = [AppManager getHourFromDate:self.oderDetailModel.engineerAppointmentDate];
        NSString *amPm = @"";
        
        if (hour < 12) {
            
            amPm = @"am";
        
        }
        else{
            
            amPm = @"pm";
        }
        */

        NSString *dateString = [self getAppointmentFormattedDate:_currectSelectedActivationDate];
        
        
        //Model object for activation date Cell
        AmendViewModel *activationDateModel = [[AmendViewModel alloc] init];
        activationDateModel.amendContext = kActivationDateText;
        
        if(self.isShowAppointment == NO){
            activationDateModel.isEditable =  NO;
            activationDateModel.subHeading1 = [[NSAttributedString alloc] initWithString:@"Will be updated shortly"];
            activationDateModel.subHeading2 = nil;
            
        } else {
        
            NSString *amendAttribute = [self getAmendAttributeForAmendContext:activationDateModel.amendContext];
            activationDateModel.isEditable = [_amendScreenModel isOrderAmendableForAmendAttribute:amendAttribute];
            activationDateModel.subHeading1 = [activationDateModel getAttributtedStringForTextString:@"Date " andValuString:dateString];
            activationDateModel.subHeading2 = nil;
        }
        [dataArray addObject:activationDateModel];

    }
    
     //Setting DataSource Array
    _dataArray = dataArray;
    [self.tableView reloadData];
}


//Method to Create the actual cell for particular index path
- (UITableViewCell *)getCellForIndexPath:(NSIndexPath *)indexPath{
    
    AmendTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kAmendTableViewCellID];
    [cell updateWithAmend:[_dataArray objectAtIndex:indexPath.row]];
    return cell;
}



//Returns the screen name, appeared on navigation bar
- (NSString *)getScreenTitle{
    
    return @"Change order details";
}


//This Method is called on tableview cell selection
- (void)cellSelectedAtIndexPath:(NSIndexPath *)indexPath{
    
    
    AmendViewModel *amendModel = [_dataArray objectAtIndex:indexPath.row];
    if(!amendModel.isEditable && !self.needToPopulateDummyData){
        //No action on cell selection if its not editable
        return;
    }
    
    
    if([amendModel.amendContext isEqualToString:kEngineerAppointmentText]){
        [self trackClickEvent:OMNICLICK_ORDER_ENGG_APPOINTMENT];
        
        //Opening Amend Engineer Screen
        BTAppointmentPickerViewController *vc = [BTAppointmentPickerViewController getBTAppointmentPickerViewController];
        vc.orderReference = self.orderReference;
        vc.itemReference = self.itemReference;
        vc.postalCode = self.postalCode;
        vc.appointmentDate = self.oderDetailModel.engineerAppointmentDate;
        vc.isSIM2Order = self.isSIM2Order;
        vc.SIM2OrderRef = self.itemBBReferenceSIM2;
        
        if([[[_oderDetailModel productSummary] installType] isEqualToString:@"C"]) {
            vc.pcpOnly = YES;
        } else {
            vc.pcpOnly = NO;
        }
        
        if (_currentSelectedEngineerApointment) {
            vc.initialAppointment = _currentSelectedEngineerApointment;
        } else {
            vc.initialAppointment = _previousEngineerApointment;
        }
        vc.delegate = self;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if([amendModel.amendContext isEqualToString:kSiteContactText] || [amendModel.amendContext isEqualToString:kAlternateContactText]){
        
        if([amendModel.amendContext isEqualToString:kSiteContactText])
           [self trackClickEvent:OMNICLICK_ORDER_SITE_CONTACT];
        else
            [self trackClickEvent:OMNICLICK_ORDER_ENGG_ALTERNATIVE_CONTACT];
            
        
        //Opening Site Contacts Screen
        BTOrderSiteContactsViewController *vc = [BTOrderSiteContactsViewController getBTOrderSiteContactsViewController];
        vc.orderReference = self.orderReference;
        vc.itemReference = self.itemReference;
        vc.postalCode = self.postalCode;
        vc.siteContact = _currentSelectedSiteContact;
        vc.siteContactDict = _newAddedSiteContactDict;
        vc.orderSiteContactDelegate = self;
        
        if([amendModel.amendContext isEqualToString:kAlternateContactText]){
            
            vc.isAlternativeContact = YES;
            vc.siteContact = _currentAltSelectedSiteContact;
        }
        
       [self.navigationController pushViewController:vc animated:YES];
    }
    else if([amendModel.amendContext isEqualToString:kActivationDateText]){
        [self trackClickEvent:OMNICLICK_ORDER_ENGG_ACTIVATION_DATE];
        
        if([[[_oderDetailModel productSummary] installType] isEqualToString:@"C"]) {
            BTAppointmentPickerViewController *vc = [BTAppointmentPickerViewController getBTAppointmentPickerViewController];
            vc.orderReference = self.orderReference;
            vc.itemReference = self.itemReference;
            vc.postalCode = self.postalCode;
            vc.appointmentDate = _currectSelectedActivationDate;
            vc.pcpOnly = YES;
            vc.isSIM2Order = self.isSIM2Order;
            vc.SIM2OrderRef = self.itemBBReferenceSIM2;
            if (_currentSelectedEngineerApointment) {
                vc.initialAppointment = _currentSelectedEngineerApointment;
            } else {
                BTAppointment *newAppointment = [[BTAppointment alloc] initWithDate:_currectSelectedActivationDate andTimeslot:nil];
                vc.initialAppointment = newAppointment;
            }
            
            vc.delegate = self;
            [self.navigationController pushViewController:vc animated:YES];
        } else {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            BTChangeActivationDateViewController *changeActivationDateViewController = [storyboard instantiateViewControllerWithIdentifier:@"ChangeActivationDateScene"];
            [changeActivationDateViewController setAmmendChangeActivationDateDelegate:self];
            [changeActivationDateViewController setOrderReference:self.orderReference];
            [changeActivationDateViewController setItemReference:self.itemReference];
            [changeActivationDateViewController setSIM2OrderRef:self.itemBBReferenceSIM2];
            [changeActivationDateViewController setPostalCode:self.postalCode];
            [changeActivationDateViewController setCurrentSelectedDate:_currectSelectedActivationDate];
            [changeActivationDateViewController setIsSIM2Order:self.isSIM2Order];
            
            [self.navigationController pushViewController:changeActivationDateViewController animated:YES];
        }
    }
}


//Method to cancell in-progress APIs
- (void)cancelInProgressAPI{
    
    [self setNetworkRequestInProgress:NO];
    [_amendScreenModel cancelAmmendCheckAPIRequest];
}


- (void)closeButtonPressed:(id)sender{
    
    
    if(self.isDataChanged){
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"Are you sure you want to cancel? You haven't saved any changes." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self.navigationController popViewControllerAnimated:YES];
        }];
        
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:^{}];
        
        
    }
    else{
        
        //[self.orderAmendDelegate btOrderAmendViewController:self didAmendOrderForOrderDetailsModel:self.oderDetailModel isEngineerAppointmentChanged:[self isEngineerAppointmentChanged]];
        [self.navigationController popViewControllerAnimated:YES];
    }
}



- (void)confirmButtonPressed:(id)sender{
    
    if(self.isDataChanged){
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"Are you sure you want to make the changes?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self sumbitChangesToServer];
        }];
        
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:^{}];

        
    }
    else{
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}

#pragma mark UpdateFromAmendSpecificScreens

- (void)updateCurrentSelectedActivationDateWithNewActivationDate:(NSDate *)newActivationDate {
    _currectSelectedActivationDate = [newActivationDate copy];
    [self populateData];
}


#pragma mark- *********************


#pragma mark- Helper methods

- (NSString *)getName:(BTOrderSiteContactModel*)contactModel
{
    //(RLM) 17 Mar
   NSString *name = @"";
    
    if(contactModel.title && contactModel.title.length >0)
        name = contactModel.title;
    
    if(contactModel.firstName && contactModel.firstName.length >0)
        name = [NSString stringWithFormat:@"%@ %@",name,contactModel.firstName];
    
    if(contactModel.lastName && contactModel.lastName.length >0)
        name = [NSString stringWithFormat:@"%@ %@",name,contactModel.lastName];
    

    return name;
}


- (void)makeAmendCheckAPICall{
    
    if(self.needToPopulateDummyData)
        return;
    
   if (![AppManager isInternetConnectionAvailable]) {
        
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_ORDER_CHANGE_ORDER_DETAIL];
        [self showAlertWithTitle:kNoConnectionTitle andMessage:kNoConnectionMessage];
        return;
    }
     NSString *amendAttributesString = @"";
    if(self.isSIM2Order == YES && self.isSecondaryOrder == YES){
         amendAttributesString = @"APPOINTMENT,SITECONTACT,CUSTOMERAGREEDDATE";
        //Making API Call
        [_amendScreenModel checkAmmendOrderForLoggedInUserWithOrderRef:self.orderReference itemRef:self.itemBBReferenceSIM2 ammendAttribute:amendAttributesString];
    } else {
       
        
        for(int i = 0; i < [_dataArray count]; i++){
            
            AmendViewModel *amendModel = [_dataArray objectAtIndex:i];

            if(amendModel.amendContext){
                
                NSString *amendAttribute = [self getAmendAttributeForAmendContext:amendModel.amendContext];
                
                amendAttributesString = [amendAttributesString stringByAppendingString:amendAttribute];
                
                if(i < [_dataArray count] - 1){
                    
                    amendAttributesString = [amendAttributesString stringByAppendingString:@","];
                }
            }
        }
        
        //Making API Call
        [_amendScreenModel checkAmmendOrderForLoggedInUserWithOrderRef:self.orderReference itemRef:self.itemReference ammendAttribute:amendAttributesString];
    }
    self.networkRequestInProgress = YES;
    /*
    AmendViewModel *amendModel = [_dataArray firstObject];
    
    if(amendModel){
        
        if([amendModel.anemdContext isEqualToString:kSiteContactText]){
            
            amendAttribute = kAmendAttributeSiteContact;
        }
        else if([amendModel.anemdContext isEqualToString:kAlternateContactText]){
            
            amendAttribute = kAmendAttributeAlterSiteContact;
        }
        else if([amendModel.anemdContext isEqualToString:kActivationDateText]){
            
            amendAttribute = kAmendAttributeCustomerAgreedDate;
        }

        
        //Making API Call
        self.networkRequestInProgress = YES;
        [_amendScreenModel checkAmmendOrderForLoggedInUserWithOrderRef:self.orderReference itemRef:self.itemReference ammendAttribute:amendAttribute];
    }

     */
}

//Go get appointment formatted date
- (NSString *)getAppointmentFormattedDate:(NSDate *)date
{
    
    NSString *dateFormat = @"dd/MM/yyyy";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
}



- (NSString *)getAmendAttributeForAmendContext:(NSString *)amendContext{
    
    NSString *amendAttribute = @"";
    
    if([amendContext isEqualToString:kSiteContactText]){
        
        amendAttribute = kAmendAttributeSiteContact;
    }
    else if([amendContext isEqualToString:kEngineerAppointmentText]){
        
        amendAttribute = kAmendAttributeAppointment;
    }
    else if([amendContext isEqualToString:kAlternateContactText]){
        
        amendAttribute = kAmendAttributeAlterSiteContact;
    }
    else if([amendContext isEqualToString:kActivationDateText]){
        
        amendAttribute = kAmendAttributeCustomerAgreedDate;
    }
    
    return amendAttribute;

}

- (void)setDataChangeStatus{
    
    
    BOOL isDataUpdated = [self isEngineerAppointmentChanged];
    
    if(!isDataUpdated)
    {
        isDataUpdated = [self isSiteContactChanged];
    }
    
    if(!isDataUpdated)
    {
        isDataUpdated = [self isAltSiteContactChanged];
    }
    
    if(!isDataUpdated)
    {
        isDataUpdated = [self isActivationDateChanged];
    }
    
    self.isDataChanged = isDataUpdated;
}


- (BOOL)isEngineerAppointmentChanged{
    
    BOOL isEngineerAppointmentChanged = NO;
    
    if(_previousEngineerApointment && _currentSelectedEngineerApointment){
        
        if([_previousEngineerApointment.dateInfo compare:_currentSelectedEngineerApointment.dateInfo] != NSOrderedSame){
            
            isEngineerAppointmentChanged = YES;
        }
        else if ((_previousEngineerApointment.isAMSelected != _currentSelectedEngineerApointment.isAMSelected)){
            
            isEngineerAppointmentChanged = YES;
        }
        else if ((_previousEngineerApointment.isPMSelected != _currentSelectedEngineerApointment.isPMSelected)){
            
            isEngineerAppointmentChanged = YES;
        }
        else if (![_previousEngineerApointment.selectedTimeslot isEqualToString:_currentSelectedEngineerApointment.selectedTimeslot]) {
            isEngineerAppointmentChanged = YES;
        }
    }
    
    return isEngineerAppointmentChanged;
}

- (BOOL)isSiteContactChanged{
    
    BOOL isSiteChanged = NO;
    
    if(_previousSiteContact && _currentSelectedSiteContact){
        
        if(![_previousSiteContact.contactKey isEqualToString:_currentSelectedSiteContact.contactKey]){
            
            isSiteChanged = YES;
        }
    }
    
    return isSiteChanged;
}

- (BOOL)isAltSiteContactChanged{
    
    BOOL isAltSiteChanged = NO;
    
    if(_previousAltSiteContact && _currentAltSelectedSiteContact){
        
        if(_previousSiteContact.contactKey && !_currentSelectedSiteContact.contactKey){
            
            isAltSiteChanged = YES;
        }
        else if(![_previousAltSiteContact.contactKey isEqualToString:_currentAltSelectedSiteContact.contactKey]){
            
            isAltSiteChanged = YES;
        }
    }
    
    return isAltSiteChanged;
}


- (BOOL)isActivationDateChanged{
    
    BOOL isActivationDateChanged = NO;
    
    if(_currectSelectedActivationDate && self.oderDetailModel.activationDate){
        
        if([_currectSelectedActivationDate compare:self.oderDetailModel.activationDate] !=NSOrderedSame){
            
            isActivationDateChanged = YES;
        }
    }
    
    return isActivationDateChanged;
}


- (AmendViewModel *)getAmendModelFromSiteContact:(BTOrderSiteContactModel *)siteContact andHeadtingText:(NSString *)headingText{
    
    if(!siteContact)
        return nil;
    
    //Alt Site Contact
    NSString *title = siteContact.title;
    NSString *firstName = siteContact.firstName;
    NSString *secondName = siteContact.lastName;
    
    //Nill Checks to avoid "null" string appearance on UI
    if(!title){
        
        title = @"";
    }
    
    if(!firstName){
        
        firstName = @"";
    }
    
    if(!secondName){
        
        secondName = @"";
    }
    
    
    NSString *name = [NSString stringWithFormat:@"%@ %@ %@",title,firstName,secondName];
    name = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *contatNumber = [AppManager getContactNumberFromSiteContact:siteContact];
    
    //Model object for Alt Site Contact Cell
    AmendViewModel *amendModel = [[AmendViewModel alloc] init];
    amendModel.amendContext = headingText;
    
    NSString *amendAttribute = [self getAmendAttributeForAmendContext:amendModel.amendContext];
    amendModel.isEditable = [_amendScreenModel isOrderAmendableForAmendAttribute:amendAttribute];
    amendModel.subHeading1 = [amendModel getAttributtedStringForTextString:name andValuString:@""];
    amendModel.subHeading2 = nil;
    
    if([contatNumber validAndNotEmptyStringObject]){
        
        amendModel.subHeading2 = [amendModel getAttributtedStringForTextString:@"Phone " andValuString:contatNumber];
    }
    
    return amendModel;

}

- (void)sumbitChangesToServer{

    //[self.orderAmendDelegate btOrderAmendViewController:self didAmendOrderForOrderDetailsModel:[self getOrderDetailForCurrentDataChanges]];
    
    
    if (![AppManager isInternetConnectionAvailable]) {
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_ORDER_CHANGE_ORDER_DETAIL];
        [self showAlertWithTitle:kNoConnectionTitle andMessage:kNoConnectionMessage];
        return;
    }
   

    
    BTAppointment *engineerAppointment = nil;
    BTOrderSiteContactModel *siteContact = nil;
    BTOrderSiteContactModel *altSiteContact = nil;
    NSDate *activationDate = nil;
    NSString *postalCode = nil;
    
    if([self isEngineerAppointmentChanged]){
       engineerAppointment = _currentSelectedEngineerApointment;
    }
    
    if([self isSiteContactChanged]){
    
       siteContact = _currentSelectedSiteContact;
    }
    
   if([self isAltSiteContactChanged]){
    
        altSiteContact = _currentAltSelectedSiteContact;
   }

    if([self isActivationDateChanged]){
    
       activationDate = _currectSelectedActivationDate;
   }
   
   if([self.postalCode validAndNotEmptyStringObject]){
    
        postalCode = self.postalCode;
    }

   self.networkRequestInProgress = YES;
    NSString* itemReference = nil;
    if(self.isSIM2Order == YES){
        itemReference = self.itemBBReferenceSIM2;
    } else{
        itemReference = self.itemReference;
    }
    if([[[_oderDetailModel productSummary] installType] isEqualToString:@"C"]) {
        [_amendScreenModel submitAndMakeAmendOrderToServerWithNewEngineeringAppointment:engineerAppointment newSiteContact:siteContact alternateSiteContact:altSiteContact newActivationDate:activationDate postalCode:postalCode pcp:true forOrderRef:self.orderReference andItemReference:itemReference];
    } else {
        [_amendScreenModel submitAndMakeAmendOrderToServerWithNewEngineeringAppointment:engineerAppointment newSiteContact:siteContact alternateSiteContact:altSiteContact newActivationDate:activationDate postalCode:postalCode pcp:false forOrderRef:self.orderReference andItemReference:itemReference];
    }
}


- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)messageText{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:messageText preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:^{}];
}


- (void)handleError:(NSError *)error{
    
    dispatch_async(dispatch_get_main_queue(), ^{

    [self setNetworkRequestInProgress:false];
    });

    if([error.domain isEqualToString:BTNetworkErrorDomain])
    {
        switch (error.code)
        {
            case BTNetworkErrorCodeLoginInvalid:
            {

                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAppNeedsLockDownWithLoginScreen object:nil userInfo:nil];
                break;
            }
                
            case BTNetworkErrorCodeUserProfileLocked:
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAppNeedsLockDownWithPINUnlockScreen object:nil userInfo:nil];
                break;
            }
                
            case BTNetworkErrorCodeAmendAlreadyInProgress:
            {
                [self showAlertWithTitle:@"Message" andMessage:@"Amend is in already in progress. Immediate amend is not allowed. Please try after sometime."];

                break;
            }
                
            default:
            {
                [self handleUnknownErrors];
                
                break;
            }
        }
    }
    else{
        
        [self handleUnknownErrors];
    }
    
}


- (DLMOrderDetailsScreen *)getOrderDetailForCurrentDataChanges{
    
    
    
    if([self isEngineerAppointmentChanged]){
        
        self.oderDetailModel.engineerAppointmentDate = _currentSelectedEngineerApointment.dateInfo;
        
    }
    
    if([self isSiteContactChanged]){
        
        self.oderDetailModel.siteContact = _currentSelectedSiteContact;
    }
    
    
    if([self isAltSiteContactChanged]){
        
        self.oderDetailModel.altSiteContact = _currentAltSelectedSiteContact;
    }

    
    if([self isActivationDateChanged]){
        
        self.oderDetailModel.activationDate = _currectSelectedActivationDate;
    }
    
    return self.oderDetailModel;
}

- (void)handleUnknownErrors{
    
    [self showAlertWithTitle:@"Message" andMessage:@"Sorry, something has gone wrong. Please try again later."];
}



- (void)updateEngineerAppointmentNodeFromAppointmentDate {
    
    BTOrderMilestoneNodeModel *engineerAppointmentNode = nil;
    
    // sincere apologies for this piece of code
    // for some reason the array or milestones can be triple nested inside other arrays
    
    for (NSObject *obj in self.oderDetailModel.arrayOfMilestoneNodes) {
        if ([obj isKindOfClass:[BTOrderMilestoneNodeModel class]]) {
            BTOrderMilestoneNodeModel *node = (BTOrderMilestoneNodeModel *)obj;
            if([node.nodeDisplayName isEqualToString:@"Engineer appointment"]){
                engineerAppointmentNode = node;
            }
        } else if ([obj isKindOfClass:[NSArray class]]) {
            for (NSObject *nestedObj in (NSArray*)obj) {
                if ([nestedObj isKindOfClass:[BTOrderMilestoneNodeModel class]]) {
                    BTOrderMilestoneNodeModel *node = (BTOrderMilestoneNodeModel *)nestedObj;
                    if([node.nodeDisplayName isEqualToString:@"Engineer appointment"]){
                        engineerAppointmentNode = node;
                    }
                } else if ([nestedObj isKindOfClass:[NSArray class]]){
                    for (NSObject *nestedNestedObj in (NSArray*)nestedObj) {
                        if ([nestedNestedObj isKindOfClass:[BTOrderMilestoneNodeModel class]]) {
                            BTOrderMilestoneNodeModel *node = (BTOrderMilestoneNodeModel *)nestedNestedObj;
                            if([node.nodeDisplayName isEqualToString:@"Engineer appointment"]){
                                engineerAppointmentNode = node;
                            }
                        } else if ([nestedNestedObj isKindOfClass:[NSArray class]]){
                            for (NSObject *nestedNestedNestedObj in (NSArray*)nestedNestedObj) {
                                if ([nestedNestedNestedObj isKindOfClass:[BTOrderMilestoneNodeModel class]]) {
                                    BTOrderMilestoneNodeModel *node = (BTOrderMilestoneNodeModel *)nestedNestedNestedObj;
                                    if([node.nodeDisplayName isEqualToString:@"Engineer appointment"]){
                                        engineerAppointmentNode = node;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
//    for(BTOrderMilestoneNodeModel *node in self.oderDetailModel.arrayOfMilestoneNodes){
//
//        if([node.nodeDisplayName isEqualToString:@"Engineer appointment"]){
//
//            engineerAppointmentNode = node;
//        }
//    }
    
    
    if(engineerAppointmentNode)
    {
        
        //[engineerAppointmentNode updateNodeDataWithEngineerAppointmentDate:_currentSelectedEngineerApointment.dateInfo];
        [engineerAppointmentNode updateNodeDataWithEngineerAppointment:_currentSelectedEngineerApointment];
    }
   
}

- (void)updateCompletionNodeFromActivationDate:(NSDate *)date {

    BTOrderMilestoneNodeModel *activationDateNode = nil;
    
    // sincere apologies for this piece of code
    // for some reason the array or milestones can be triple nested inside other arrays
    
    for (NSObject *obj in self.oderDetailModel.arrayOfMilestoneNodes) {
        if ([obj isKindOfClass:[BTOrderMilestoneNodeModel class]]) {
            BTOrderMilestoneNodeModel *node = (BTOrderMilestoneNodeModel *)obj;
            if([node.nodeDisplayName isEqualToString:@"Completion"]){
                activationDateNode = node;
            }
        } else if ([obj isKindOfClass:[NSArray class]]) {
            for (NSObject *nestedObj in (NSArray*)obj) {
                if ([nestedObj isKindOfClass:[BTOrderMilestoneNodeModel class]]) {
                    BTOrderMilestoneNodeModel *node = (BTOrderMilestoneNodeModel *)nestedObj;
                    if([node.nodeDisplayName isEqualToString:@"Completion"]){
                        activationDateNode = node;
                    }
                } else if ([nestedObj isKindOfClass:[NSArray class]]){
                    for (NSObject *nestedNestedObj in (NSArray*)nestedObj) {
                        if ([nestedNestedObj isKindOfClass:[BTOrderMilestoneNodeModel class]]) {
                            BTOrderMilestoneNodeModel *node = (BTOrderMilestoneNodeModel *)nestedNestedObj;
                            if([node.nodeDisplayName isEqualToString:@"Completion"]){
                                activationDateNode = node;
                            }
                        } else if ([nestedNestedObj isKindOfClass:[NSArray class]]){
                            for (NSObject *nestedNestedNestedObj in (NSArray*)nestedNestedObj) {
                                if ([nestedNestedNestedObj isKindOfClass:[BTOrderMilestoneNodeModel class]]) {
                                    BTOrderMilestoneNodeModel *node = (BTOrderMilestoneNodeModel *)nestedNestedNestedObj;
                                    if([node.nodeDisplayName isEqualToString:@"Completion"]){
                                        activationDateNode = node;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
//    for(BTOrderMilestoneNodeModel *node in self.oderDetailModel.arrayOfMilestoneNodes){
//        
//        if([node.nodeDisplayName isEqualToString:@"Completion"]){
//            
//            activationDateNode = node;
//        }
//    }
    
    
    if(activationDateNode)
    {
        [activationDateNode updateNodeDataWithActivationDate:date];
    }
    
}





#pragma mark- BTOrderSiteContactsViewControllerDelegate Method

- (void)btOrderSiteContactsViewController:(BTOrderSiteContactsViewController *)controller didSubmitNewUserData:(NSDictionary *)dataDict{
    
    _newAddedSiteContactDict = dataDict;
    
    if(controller.isAlternativeContact){
        
        _currentAltSelectedSiteContact = [[BTOrderSiteContactModel alloc] initSiteContactWithAddedSiteContactDict:dataDict];
    }
    else{
        
        _currentSelectedSiteContact = [[BTOrderSiteContactModel alloc] initSiteContactWithAddedSiteContactDict:dataDict];
    }

    
    [self setDataChangeStatus];
    [self populateData];;
}


- (void)btOrderSiteContactsViewController:(BTOrderSiteContactsViewController *)controller didSelectSiteContact:(BTOrderSiteContactModel *)siteContactModel{
    
    _newAddedSiteContactDict = nil;
    
    if(controller.isAlternativeContact){
        
        _currentAltSelectedSiteContact = siteContactModel;
    }
    else{
        
        _currentSelectedSiteContact = siteContactModel;
    }
    [self setDataChangeStatus];
    [self populateData];
    
}


#pragma mark- BTAmendEngineerViewControllerDelegate Method
- (void)btAmendEngineerViewController:(BTAmendEngineerViewController *)controller didSelectAppointment:(BTAppointment *)appointment{
    if([[[_oderDetailModel productSummary] installType] isEqualToString:@"C"]) {
        _currectSelectedActivationDate = [appointment date];
    } else {
        _currentSelectedEngineerApointment = appointment;
    }
    [self setDataChangeStatus];
    [self populateData];
}


#pragma mark- AmmendOrderOnAmmendOrder Delegate

- (void)successfullyCheckedAmmendOrderOnAmmendOrderScreen:(DLMAmmendOrderScreen *)ammendScreen{
    
    //By Default all model are not editable, Now updating editable property based on server response
    if(ammendScreen){
        
         BOOL isAnyAmendable = NO;
            for(AmendViewModel *amend in _dataArray){
                
                NSString *amendAttribute = [self getAmendAttributeForAmendContext:amend.amendContext];
                amend.isEditable = [_amendScreenModel isOrderAmendableForAmendAttribute:amendAttribute];
                
                if(!isAnyAmendable){
                    
                    isAnyAmendable = amend.isEditable;
                }
            }
            
            
            if(!isAnyAmendable){
                
                //Nothing is amendable
                [self showAlertWithTitle:@"Message" andMessage:@"Sorry, You can't make any changes to this order."];
                [self trackAmendNotAllowedError];
            }
    
    }
    else{
        
        for(AmendViewModel *amend in _dataArray){
            
            amend.isEditable = NO;
        }
        
        [self showAlertWithTitle:@"Message" andMessage:@"Sorry, You can't make any changes to this order."];
        [self trackAmendNotAllowedError];
    }
    
    
    
    [self.tableView reloadData];
    self.networkRequestInProgress = NO;
    
}


- (void)ammendOrderScreen:(DLMAmmendOrderScreen *)ammendScreen failedToFetchUserDataWithWebServiceError:(NLWebServiceError *)error{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self setNetworkRequestInProgress:false];
    });
    
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if(errorHandled == NO)
    {
        [self handleUnknownErrors];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_ORDER_CHANGE_ORDER_DETAIL];
    }
    
}


- (void)amendSuccessfullyFinishedByAmmendOrderScreen:(DLMAmmendOrderScreen *)ammendScreen
{

    dispatch_async(dispatch_get_main_queue(), ^{

    self.networkRequestInProgress = NO;
    });

    [self trackKeyTaskForAmendOrder];
    if ([self isActivationDateChanged]) {
        [self.oderDetailModel.productSummary updateProductDuedateWithNSDate:_currectSelectedActivationDate];
        [self updateCompletionNodeFromActivationDate:_currectSelectedActivationDate];
    }
    
    if ([self isEngineerAppointmentChanged]) {
        [self.oderDetailModel.productSummary updateProductDuedateWithNSDate:_currentSelectedEngineerApointment.dateInfo];
        [self.oderDetailModel.productSummary updateEngineeringAppointmentDateWithNSDate:_currentSelectedEngineerApointment.dateInfo];
        [self.oderDetailModel.productSummary updateEngineeringAppointmentSlotWithSlot:_currentSelectedEngineerApointment.selectedTimeslot];
        [self updateEngineerAppointmentNodeFromAppointmentDate];
        [self updateCompletionNodeFromActivationDate:_currentSelectedEngineerApointment.dateInfo];
    }
    
    if([self isSiteContactChanged]){
        
        self.oderDetailModel.siteContact = _currentSelectedSiteContact;
    }
    
    if([self isAltSiteContactChanged]){
        
        self.oderDetailModel.siteContact = _currentAltSelectedSiteContact;
    }
    
    [self.orderAmendDelegate btOrderAmendViewController:self didAmendOrderForOrderDetailsModel:self.oderDetailModel isEngineerAppointmentChanged:[self isEngineerAppointmentChanged]];
    [self.navigationController popViewControllerAnimated:YES];
    
    
    //UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Amend Successful!" message:@"Thank you for your order. The details have been  updated in our records." preferredStyle:UIAlertControllerStyleAlert];
    
    /*UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        [self.orderAmendDelegate btOrderAmendViewController:self didAmendOrderForOrderDetailsModel:[self getOrderDetailForCurrentDataChanges]];
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:^{
    }];*/
}

- (void)ammendOrderScreen:(DLMAmmendOrderScreen *)ammendScreen failedToSubmitAmendOrderWithWebServiceError:(NLWebServiceError *)error
{
    
    [self trackAmendConfirmPressedButFails];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self setNetworkRequestInProgress:false];
    });
    
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if([error.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {
        errorHandled = YES;
    
        switch (error.error.code){
            
                //TODO : (Sal) need to confirm this error code with Lakhpat/Harman
            case BTNetworkErrorCodeAmendAlreadyInProgress:
            {
                [self showAlertWithTitle:@"Message" andMessage:@"Sorry, You can't make any changes to this order."];
                
                break;
            }
            case BTNetworkErrorCodeHTTPTimeOut:
            {
                [self showAlertWithTitle:@"Message" andMessage:@"Sorry, the request timed out. Please try again later."];
                break;
            }
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }
    
    if(errorHandled == NO)
    {
        [self showAlertWithTitle:@"Failure" andMessage:@"Sorry, that change hasn't worked, Please try again later."];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_ORDER_CHANGE_ORDER_DETAIL];
    
    }
    
}

#pragma mark - Omnitutre Event Tracking
- (void)trackClickEvent:(NSString *)clickEvent
{
    NSMutableDictionary *contextDict = [[NSMutableDictionary alloc] init];
    NSString *orderRef = [OmnitureManager getOrderRefFormattedString:self.oderDetailModel.orderRef];
    [contextDict setValue:orderRef forKey:kOmniOrderRef];

    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",OMNIPAGE_ORDER_CHANGE_ORDER_DETAIL,clickEvent] withContextInfo:contextDict];
}

- (void)trackKeyTaskForAmendOrder
{
    if([self isEngineerAppointmentChanged]){
        [self trackPageWithKeyTask:OMNI_KEYTASK_ORDER_TRACKER_ENGG_APPOINTMENT_CHANGED];
    }
    
    if([self isSiteContactChanged]){
        [self trackPageWithKeyTask:OMNI_KEYTASK_ORDER_TRACKER_DETAILS_SITE_CONTACT_CHANGED];
        
    }
    
    if([self isAltSiteContactChanged]){
        
       [self trackPageWithKeyTask:OMNI_KEYTASK_ORDER_TRACKER_DETAILS_ALT_SITE_CONTACT_CHANGED];
    }
    
    if([self isActivationDateChanged]){
        
        [self trackPageWithKeyTask:OMNI_KEYTASK_ORDER_TRACKER_DETAILS_ACTIVATION_DATE_CHANGED];
    }

}


- (void)trackPageWithKeyTask:(NSString *)keytask
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *orderRef = [OmnitureManager getOrderRefFormattedString:self.oderDetailModel.orderRef];
    [data setObject:keytask forKey:kOmniKeyTask];
    [data setObject:orderRef forKey:kOmniOrderRef];
    
    [OmnitureManager trackPage:OMNIPAGE_ORDER_CHANGE_ORDER_DETAIL withContextInfo:data];
}


- (void)trackAmendNotAllowedError
{
  [OmnitureManager trackError:OMNIERROR_ORDER_CHANGE_AMEND_NOT_ALLOWED
               onPageWithName:OMNIPAGE_ORDER_CHANGE_ORDER_DETAIL contextInfo:[AppManager getOmniDictionary]];
}

- (void)trackAmendConfirmPressedButFails
{
    [OmnitureManager trackError:OMNIERROR_CONFRIM_AMENED_BUT_FAILS
                 onPageWithName:OMNIPAGE_ORDER_CHANGE_ORDER_DETAIL contextInfo:[AppManager getOmniDictionary]];
}

@end
