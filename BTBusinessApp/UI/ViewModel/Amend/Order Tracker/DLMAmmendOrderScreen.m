//
//  DLMAmmendOrderScreen.m
//  BTBusinessApp
//
//  Created by Accolite on 27/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMAmmendOrderScreen.h"
#import "AppDelegate.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppDelegateViewModel.h"
#import "AppConstants.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"
#import "NLCheckAmmendOrderWebService.h"
#import "NLAmendOrderSubmitWebService.h"
#import "BTAppointment.h"
#import "BTOrderSiteContactModel.h"


@interface DLMAmmendOrderScreen () <NLCheckAmmendOrderWebServiceDelegate, NLAmendOrderSubmitWebServiceDelegate>
@property (nonatomic) NLCheckAmmendOrderWebService *checkAmmendOrderWebService;
@property (nonatomic) NLAmendOrderSubmitWebService *amendOrderSubmitWebService;

- (NSString *)stringRepresentationOfDate:(NSDate *)date;

@end

@implementation DLMAmmendOrderScreen

#pragma mark Public Methods

- (void)checkAmmendOrderForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef ammendAttribute:(NSString *)ammendAttribute {

    self.checkAmmendOrderWebService = [[NLCheckAmmendOrderWebService alloc] initWithOrderRef:orderRef itemRef:itemRef andAmmendAttribute:ammendAttribute];
    self.checkAmmendOrderWebService.getCheckAmmendOrderWebServiceDelegate = self;
    [self.checkAmmendOrderWebService resume];
}

- (void)submitAndMakeAmendOrderToServerWithNewEngineeringAppointment:(BTAppointment *)newEngineeringAppointment newSiteContact:(BTOrderSiteContactModel *)newSiteContact alternateSiteContact:(BTOrderSiteContactModel *)newAlternateSiteContact newActivationDate:(NSDate *)newActivationDate postalCode:(NSString *)postalCode pcp:(BOOL)pcpOnly forOrderRef:(NSString *)orderRef andItemReference:(NSString *)itemRef
{
    NSMutableDictionary *amendDictionary = [NSMutableDictionary dictionary];

    [amendDictionary setValue:orderRef forKey:@"TrackedOrderKey"];
    [amendDictionary setValue:[NSNumber numberWithBool:false] forKey:@"IsLegacy"];
    [amendDictionary setValue:itemRef forKey:@"ItemReference"];
    [amendDictionary setObject:[NSNull null] forKey:@"PONRServicePointStatus"];
    [amendDictionary setObject:[NSNull null] forKey:@"PONRServicePointSubStatus"];
    
    if(pcpOnly) {
        [amendDictionary setObject:@YES forKey:@"PcpOnly"];
    } else {
        [amendDictionary setObject:@NO forKey:@"PcpOnly"];
    }
    
    if(newEngineeringAppointment)
    {
        [amendDictionary setValue:[NSNumber numberWithBool:true] forKey:@"IsAppointmentChanged"];
        [amendDictionary setValue:[self stringRepresentationOfDate:newEngineeringAppointment.dateInfo] forKey:@"AppointmentDate"];
        NSString *amPM = @"";
        if (newEngineeringAppointment.isAMSelected) {
            amPM = @"AM";
        } else if (newEngineeringAppointment.isPMSelected) {
            amPM = @"PM";
        } else if (newEngineeringAppointment.isAS0900Selected) {
            amPM = @"0900";
        } else if (newEngineeringAppointment.isAS1000Selected) {
            amPM = @"1000";
        } else if (newEngineeringAppointment.isAS1100Selected) {
            amPM = @"1100";
        } else if (newEngineeringAppointment.isAS1200Selected) {
            amPM = @"1200";
        } else if (newEngineeringAppointment.isAS1300Selected) {
            amPM = @"1300";
        } else if (newEngineeringAppointment.isAS1400Selected) {
            amPM = @"1400";
        } else if (newEngineeringAppointment.isAS1500Selected) {
            amPM = @"1500";
        } else if (newEngineeringAppointment.isAS1600Selected) {
            amPM = @"1600";
        } else if (newEngineeringAppointment.isAS1700Selected) {
            amPM = @"1700";
        } else if (newEngineeringAppointment.isAS1800Selected) {
            amPM = @"1800";
        } else if (newEngineeringAppointment.isAS1900Selected) {
            amPM = @"1900";
        } else if (newEngineeringAppointment.isAS2000Selected) {
            amPM = @"2000";
        }
        
        if ([amPM isEqualToString:@""]) {
            // falback to old style
            amPM = @"PM";
            if (newEngineeringAppointment.hasAM) {
                amPM = @"AM";
            }
        }
        [amendDictionary setValue:amPM forKey:@"AppointmentSlot"];

        // Create and set Appointment Dictionary
        NSMutableDictionary *appointmentDictionary = [NSMutableDictionary dictionary];
        [appointmentDictionary setValue:amPM forKey:@"AppointmentSlot"];
        [appointmentDictionary setValue:[self stringRepresentationOfDate:newEngineeringAppointment.dateInfo] forKey:@"AppointmentDate"];
        [appointmentDictionary setValue:[NSNull null] forKey:@"AddressKey"];
        [appointmentDictionary setValue:[NSNull null] forKey:@"DBIDValue"];
        [appointmentDictionary setValue:[NSNull null] forKey:@"AppointmentBook"];
        [amendDictionary setValue:appointmentDictionary forKey:@"Appointment"];
    }
    else
    {
        [amendDictionary setValue:[NSNumber numberWithBool:false] forKey:@"IsAppointmentChanged"];
        [amendDictionary setValue:[NSNull null] forKey:@"AppointmentDate"];
        [amendDictionary setValue:[NSNull null] forKey:@"AppointmentSlot"];
        [amendDictionary setValue:[NSNull null] forKey:@"Appointment"];
    }


    if (newSiteContact) {
        [amendDictionary setValue:[NSNumber numberWithBool:true] forKey:@"IsSiteContactChanged"];

        NSMutableDictionary *siteContactDictionary = [NSMutableDictionary dictionary];

        [siteContactDictionary setValue:newSiteContact.title forKey:@"Title"];
        [siteContactDictionary setValue:newSiteContact.firstName forKey:@"FirstName"];
        [siteContactDictionary setValue:newSiteContact.lastName forKey:@"LastName"];
        [siteContactDictionary setValue:newSiteContact.workPhone forKey:@"WorkPhone"];
        [siteContactDictionary setValue:newSiteContact.extension forKey:@"Extension"];
        [siteContactDictionary setValue:newSiteContact.mobile forKey:@"Mobile"];
        [siteContactDictionary setValue:newSiteContact.homePhone forKey:@"HomePhone"];
        [siteContactDictionary setValue:newSiteContact.email forKey:@"Email"];
        [siteContactDictionary setValue:newSiteContact.contactKey forKey:@"ContactKey"];
        [siteContactDictionary setValue:newSiteContact.contactContext forKey:@"ContactContext"];
        [siteContactDictionary setValue:newSiteContact.preferredContact forKey:@"PreferredContact"];
        [amendDictionary setValue:siteContactDictionary forKey:@"SiteContact"];
    }
    else
    {
        [amendDictionary setValue:[NSNumber numberWithBool:false] forKey:@"IsSiteContactChanged"];
        [amendDictionary setValue:[NSNull null] forKey:@"SiteContact"];
    }




    if(newAlternateSiteContact)
    {
        [amendDictionary setValue:[NSNumber numberWithBool:true] forKey:@"IsASCChanged"];

        NSMutableDictionary *altSiteContactDictionary = [NSMutableDictionary dictionary];

        [altSiteContactDictionary setValue:newAlternateSiteContact.title forKey:@"Title"];
        [altSiteContactDictionary setValue:newAlternateSiteContact.firstName forKey:@"FirstName"];
        [altSiteContactDictionary setValue:newAlternateSiteContact.lastName forKey:@"LastName"];
        [altSiteContactDictionary setValue:newAlternateSiteContact.workPhone forKey:@"WorkPhone"];
        [altSiteContactDictionary setValue:newAlternateSiteContact.extension forKey:@"Extension"];
        [altSiteContactDictionary setValue:newAlternateSiteContact.mobile forKey:@"Mobile"];
        [altSiteContactDictionary setValue:newAlternateSiteContact.homePhone forKey:@"HomePhone"];
        [altSiteContactDictionary setValue:newAlternateSiteContact.email forKey:@"Email"];
        [altSiteContactDictionary setValue:newAlternateSiteContact.contactKey forKey:@"ContactKey"];
        [altSiteContactDictionary setValue:newAlternateSiteContact.contactContext forKey:@"ContactContext"];
        [altSiteContactDictionary setValue:newAlternateSiteContact.preferredContact forKey:@"PreferredContact"];

        [amendDictionary setValue:altSiteContactDictionary forKey:@"AlternateSiteContact"];
    }
    else
    {
        [amendDictionary setValue:[NSNumber numberWithBool:false] forKey:@"IsASCChanged"];
        [amendDictionary setValue:[NSNull null] forKey:@"AlternateSiteContact"];
    }

    [amendDictionary setValue:[NSNull null] forKey:@"EngineeringNotes"];
    [amendDictionary setValue:[NSNull null] forKey:@"EngineeringNotes2"];

    if(newActivationDate)
    {
        [amendDictionary setValue:[NSNumber numberWithBool:true] forKey:@"IsActivationDateChanged"];
        [amendDictionary setValue:[self stringRepresentationOfDate:newActivationDate] forKey:@"ActivationDate"];
        if(newEngineeringAppointment.hasAM) {
            [amendDictionary setValue:@"AM" forKey:@"ActivationSlot"];
        } else {
            [amendDictionary setValue:@"PM" forKey:@"ActivationSlot"];
        }
    }
    else
    {
        [amendDictionary setValue:[NSNumber numberWithBool:false] forKey:@"IsActivationDateChanged"];
        [amendDictionary setValue:[NSNull null] forKey:@"ActivationDate"];
    }

    [amendDictionary setValue:[NSNull null] forKey:@"MoveInDate"];
    [amendDictionary setValue:[NSNull null] forKey:@"MoveInSlot"];
    [amendDictionary setValue:[NSNull null] forKey:@"MoveOutDate"];

    CDUser *loggedInUser = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    NSString *usernameString = [NSString stringWithFormat:@"%@. %@ %@", loggedInUser.titleInName, loggedInUser.firstName, loggedInUser.lastName];
    [amendDictionary setValue:usernameString forKey:@"UserName"];

    if(postalCode)
    {
        [amendDictionary setValue:postalCode forKey:@"PostCode"];
    }
    else
    {
        [amendDictionary setValue:[NSNull null] forKey:@"PostCode"];
    }

    self.amendOrderSubmitWebService = [[NLAmendOrderSubmitWebService alloc] initWithAmendOrderRequestDictionary:amendDictionary];
    self.amendOrderSubmitWebService.amendOrderSubmitWebServiceDelegate = self;
    [self.amendOrderSubmitWebService resume];

    //[self.amendOrderSubmitWebService fetchOfflineData];
}



- (BOOL)isOrderAmendableForAmendAttribute:(NSString *)amendAttribute{
    
    BOOL isContextAmendable = NO;
    
    if(_amendStatusDict && amendAttribute){
        
        if([_amendStatusDict valueForKey:[amendAttribute lowercaseString]] || [_amendStatusDict valueForKey:[amendAttribute uppercaseString]]){
            
            BOOL isAmendable = [[_amendStatusDict valueForKey:[amendAttribute uppercaseString]] boolValue];
            
            
            if(!isAmendable){
                
                isAmendable = [[_amendStatusDict valueForKey:[amendAttribute lowercaseString]] boolValue];
            }
            
            isContextAmendable = isAmendable;
        }
    }
    
    return isContextAmendable;
}


#pragma mark Cancelling API

- (void)cancelAmmendCheckAPIRequest {

    self.checkAmmendOrderWebService.getCheckAmmendOrderWebServiceDelegate = nil;
    [self.checkAmmendOrderWebService cancel];
    self.checkAmmendOrderWebService = nil;
}

- (void)cancelAmendOrderSubmitAPIRequest {

    self.amendOrderSubmitWebService.amendOrderSubmitWebServiceDelegate = nil;
    [self.amendOrderSubmitWebService cancel];
    self.amendOrderSubmitWebService = nil;
}


#pragma mark - NLCheckAmmendOrderWebService Methods

- (void)ammendOrderCheckWebService:(NLCheckAmmendOrderWebService *)webService successfullyCheckedAmmendOrder:(NSDictionary *)ammendCheckDict {
    if(webService == _checkAmmendOrderWebService)
    {
        [self setAmendStatusDict:ammendCheckDict];
        [self.ammendOrderDelegate successfullyCheckedAmmendOrderOnAmmendOrderScreen:self];
        self.checkAmmendOrderWebService.getCheckAmmendOrderWebServiceDelegate = nil;
        self.checkAmmendOrderWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLCheckAmmendOrderWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLCheckAmmendOrderWebService but this object is not the one stored in member variable of this class %@.", [self class]);

    }
}

- (void)ammendOrderCheckWebService:(NLCheckAmmendOrderWebService *)webService failedToCheckAmmendOrderWithWebServiceError:(NLWebServiceError *)webServiceError {

    if(webService == _checkAmmendOrderWebService)
    {
        [self.ammendOrderDelegate ammendOrderScreen:self failedToFetchUserDataWithWebServiceError:webServiceError];
        self.checkAmmendOrderWebService.getCheckAmmendOrderWebServiceDelegate = nil;
        self.checkAmmendOrderWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLCheckAmmendOrderWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLCheckAmmendOrderWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}



#pragma mark - NLAmendOrderSubmitWebService Methods

- (void)orderAmendSubmissionSuccesfullyFinishedByAmendOrderSubmitWebService:(NLAmendOrderSubmitWebService *)webService
{
    if(webService == _amendOrderSubmitWebService)
    {
        [self.ammendOrderDelegate amendSuccessfullyFinishedByAmmendOrderScreen:self];
        self.amendOrderSubmitWebService.amendOrderSubmitWebServiceDelegate = nil;
        self.amendOrderSubmitWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLAmendOrderSubmitWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLAmendOrderSubmitWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)amendOrderSubmitWebService:(NLAmendOrderSubmitWebService *)webService orderAmendSubmissionSuccesfullyFinishedAndFetchedUpdatedProductData:(BTProduct *)productSummary billingDetails:(BTBillingDetails *)billingDetails pricingDetails:(BTPricingDetails *)pricingDetails productDetails:(BTProductDetails *)productDetails isAuthLevel:(NSInteger)authLevel
{

    if(webService == _amendOrderSubmitWebService)
    {
        [self.ammendOrderDelegate amendSuccessfullyFinishedByAmmendOrderScreen:self];
        self.amendOrderSubmitWebService.amendOrderSubmitWebServiceDelegate = nil;
        self.amendOrderSubmitWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLAmendOrderSubmitWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLAmendOrderSubmitWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)amendOrderSubmitWebService:(NLAmendOrderSubmitWebService *)webService failedToSubmitAmendOrderWithWebServiceError:(NLWebServiceError *)webServiceError
{
    if(webService == _amendOrderSubmitWebService)
    {
        [self.ammendOrderDelegate ammendOrderScreen:self failedToSubmitAmendOrderWithWebServiceError:webServiceError];
        self.amendOrderSubmitWebService.amendOrderSubmitWebServiceDelegate = nil;
        self.amendOrderSubmitWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLAmendOrderSubmitWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLAmendOrderSubmitWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}




#pragma mark - Private Methods

- (NSString *)stringRepresentationOfDate:(NSDate *)date
{
    NSString *dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}


@end
