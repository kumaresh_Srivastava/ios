//
//  DLMAmendActivationDateScreen.m
//  BTBusinessApp
//
//  Created by Accolite on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMAmendActivationDateScreen.h"
#import "AppDelegate.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppDelegateViewModel.h"
#import "AppConstants.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"

#import "NLHolidayListWebService.h"

@interface DLMAmendActivationDateScreen () <NLHolidayListWebServiceDelegate>
@property (nonatomic) NLHolidayListWebService *getHolidayListWebService;
@end

@implementation DLMAmendActivationDateScreen


#pragma mark- Public methods

- (void)fetchHolidayListForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef
{
    self.getHolidayListWebService = [[NLHolidayListWebService alloc] initWithOrderRef:orderRef andItemRef:itemRef];
    self.getHolidayListWebService.getHolidayListWebServiceDelegate = self;
    [self.getHolidayListWebService resume];
}

- (void)fetchHolidayListForLoggedInUserWithPostCode:(NSString *)postCode
{
    self.getHolidayListWebService = [[NLHolidayListWebService alloc] initWithPostCode:postCode];
    self.getHolidayListWebService.getHolidayListWebServiceDelegate = self;
    [self.getHolidayListWebService resume];
}



- (void) cancelHolidayListAPIRequest {
    self.getHolidayListWebService.getHolidayListWebServiceDelegate = nil;
    [self.getHolidayListWebService cancel];
    self.getHolidayListWebService = nil;
}



#pragma mark- NLHolidayListWebServiceDelegate methods

- (void)getHolidayListWebService:(NLHolidayListWebService *)webService successfullyFetchedHolidayList:(NSArray *)holidayList {
    if(webService == _getHolidayListWebService)
    {
        [self setHolidayList:holidayList];
        [self.activationDateDelegate successfullyFetchedUserDataOnAmmendActivationDateScreen:self];
        self.getHolidayListWebService.getHolidayListWebServiceDelegate = nil;
        self.getHolidayListWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLHolidayListWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLHolidayListWebService but this object is not the one stored in member variable of this class %@.", [self class]);

    }
}

- (void)getHolidayListWebService:(NLHolidayListWebService *)webService failedToFetchHolidayListWithWebServiceError:(NLWebServiceError *)error {

    if(webService == _getHolidayListWebService)
    {
        [self.activationDateDelegate ammendActivationDateScreen:self failedToFetchUserDataWithWebServiceError:error];
        self.getHolidayListWebService.getHolidayListWebServiceDelegate = nil;
        self.getHolidayListWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of faliure of an object of NLHolidayListWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of faliure of an object of NLHolidayListWebService but this object is not the one stored in member variable of this class %@.", [self class]);


    }

}

@end
