//
//  BTChangeActivationDateViewController.m
//  BTBusinessApp
//
//  Created by Accolite on 08/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTChangeActivationDateViewController.h"
#import "MBProgressHUD.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "BTChangeActivationDateModel.h"
#import "AppConstants.h"
#import "DLMAmendActivationDateScreen.h"
#import "CustomSpinnerView.h"
#import "AppManager.h"
#import "OmnitureManager.h"

@interface BTChangeActivationDateViewController () <DLMAmmendActivationDateScreenDelegate> {
    BTChangeActivationDateModel *changeActivationDateModel;
    
    NSMutableDictionary *_eventsByDate;
    
    NSDate *_todayDate;
    NSDate *_minDate;
    NSDate *_maxDate;
    
    DLMAmendActivationDateScreen *amendActivationModel;
    BOOL _isHoliday;
    UIBarButtonItem *_rightBarButton;
    CustomSpinnerView *_loadingView;
}
@property (nonatomic,strong)NSDate *dateSelected;
@property (nonatomic,strong)NSArray *holidaysArray;
@property (nonatomic,strong)NSMutableArray *datesArray;
@property (nonatomic,strong)NSString *selectedDateString;
@end

@implementation BTChangeActivationDateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.datesArray = [NSMutableArray array];
    self.selectedDateString = [NSString string];
    
    [self setTitle:@"Activation date"];
    [self.navigationItem setHidesBackButton:TRUE];
    
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelButtonAction)];
    [self.navigationItem setLeftBarButtonItem:leftBarButton];

    _rightBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveButtonAction)];
    [self.navigationItem setRightBarButtonItem:nil];

    changeActivationDateModel = [[BTChangeActivationDateModel alloc] init];
    
    _isHoliday = NO;
    [self setNetworkRequestInProgress:YES];
    amendActivationModel = [[DLMAmendActivationDateScreen alloc] init];
    amendActivationModel.activationDateDelegate = self;
    [self getHolidayListFromAPI];
    
    [self trackOmniPage:OMNIPAGE_ORDER_AMEND_ACTIVATION_DATE];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self setNetworkRequestInProgress:false];
    [amendActivationModel cancelHolidayListAPIRequest];
}

#pragma mark private methods

- (void)getHolidayListFromAPI {
    
    if(![AppManager isInternetConnectionAvailable]){
        
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_ORDER_AMEND_ACTIVATION_DATE];
        [self showRetryViewWithInternetStrip:YES];
        return;
    }
    
    [self hideRetryView];
    [self setNetworkRequestInProgress:YES];
    
    if ((self.postalCode == nil) || ([self.postalCode isEqualToString:@""])) {
        if(self.isSIM2Order == YES){
             [amendActivationModel fetchHolidayListForLoggedInUserWithOrderRef:self.orderReference itemRef:self.SIM2OrderRef];
        } else {
            [amendActivationModel fetchHolidayListForLoggedInUserWithOrderRef:self.orderReference itemRef:self.itemReference];//self.orderRef self.itemRef @"BTTZ9137" @"BTTZ91371-1"
        }
    } else {
        [amendActivationModel fetchHolidayListForLoggedInUserWithPostCode:self.postalCode];
    }
    
}

- (void) createCalanderView {
    
    self.calendarManager = [JTCalendarManager new];
    self.calendarManager.delegate = self;
    
    // Generate random events sort by date using a dateformatter for the demonstration
    [self createRandomEvents];
    
    // Create a min and max date for limit the calendar, optional
    [self createMinAndMaxDate];
    
    [self.calendarManager setMenuView:self.calanderMenuView];
    [self.calendarManager setContentView:self.calanderContentView];
    [self.calendarManager setDate:_todayDate];
}

- (void)validateForCurrentDateSelection:(NSDate *)daySelected {
    if (([self.currentSelectedDate compare:daySelected] == NSOrderedSame)) {
    
        [self.navigationItem setRightBarButtonItem:nil];
    } else {
        
        [self.navigationItem setRightBarButtonItem:_rightBarButton];
    }
}

- (void)trackOmniPage:(NSString *)page
{
    NSMutableDictionary *contextDict = [NSMutableDictionary dictionary];
    
    NSString *orderRef = [OmnitureManager getOrderRefFormattedString:self.orderReference];
   
    [contextDict setValue:@"Logged In" forKey:kOmniLoginStatus];
    [contextDict setValue:orderRef forKey:kOmniOrderRef];
    
    [OmnitureManager trackPage:page withContextInfo:contextDict];
}

- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:^{}];
}

#pragma mark -

- (void) setFormattedDatesToDatesArray {
    for (NSString *dateString in self.holidaysArray) {
        
        NSString *myString = dateString;
        NSDateFormatter *dateFormatter  =   [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssz"];
        NSDate *timestampDate = [dateFormatter dateFromString:myString];
        if (timestampDate == nil) {
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
            timestampDate = [dateFormatter dateFromString:myString];
        }
        [dateFormatter setDateFormat:@"YYYY-MM-DD"];
        NSString *newDate = [dateFormatter stringFromDate:timestampDate];
        NSDate *timestamp = [dateFormatter dateFromString:newDate];

        [self.datesArray addObject:timestamp];
    }
}

#pragma mark Action methods

- (void)cancelButtonAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)saveButtonAction {
    
    [self.ammendChangeActivationDateDelegate updateCurrentSelectedActivationDateWithNewActivationDate:self.dateSelected];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Buttons callback

- (IBAction)didGoTodayTouch
{
    [self.calendarManager setDate:_todayDate];
}

- (IBAction)didChangeModeTouch
{
    self.calendarManager.settings.weekModeEnabled = !self.calendarManager.settings.weekModeEnabled;
    [self.calendarManager reload];
    
    //CGFloat newHeight = 300;
    if(self.calendarManager.settings.weekModeEnabled){
      //  newHeight = 85.;
    }
    
//    self.calendarContentViewHeight.constant = newHeight;
    [self.view layoutIfNeeded];
}

#pragma mark - CalendarManager delegate

// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    
    [dayView.dotView setHidden:YES];
    [dayView.circleView setHidden:YES];
    [dayView.textLabel setFont:[UIFont fontWithName:kBtFontBold size:15.0]];
    // Today
    // Selected date
    //BOOL isDateHighlighted = NO;
    if(self.dateSelected && [self.calendarManager.dateHelper date:self.dateSelected isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        //isDateHighlighted = YES;
        dayView.circleView.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Other month
    else if(![self.calendarManager.dateHelper date:self.calanderContentView.date isTheSameMonthThan:dayView.date]){
        dayView.circleView.hidden = YES;
        dayView.textLabel.textColor = [BrandColours colourBtNeutral50];
    }
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
        dayView.textLabel.textColor = [BrandColours colourTextBTPurplePrimaryColor];
    }
    
    
    if([dayView isFromAnotherMonth]){
        dayView.circleView.hidden = YES;
        dayView.textLabel.textColor = [BrandColours colourTextBTPurplePrimaryColor];;
    }
    
    for (int i = 0; i < self.datesArray.count; i++) {
        NSDate *arrDate = [self.datesArray objectAtIndex:i];
        if (([arrDate compare:dayView.date] == NSOrderedSame)) {
            dayView.textLabel.textColor = [BrandColours colourBtNeutral50];
            break;
        }
    }
    NSDate *date = [NSDate date];
   if (([date compare:dayView.date] == NSOrderedDescending)) {
        dayView.textLabel.textColor = [BrandColours colourBtNeutral50];
    }
    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.day = 90;
    NSDate *newDate = [cal dateByAddingComponents:components toDate:date options:0];
    
    if (([newDate compare:dayView.date] == NSOrderedAscending)) {
        dayView.textLabel.textColor = [BrandColours colourBtNeutral50];
    }
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    //(VRK) temp date is required to redo the dateselected when holiday is selected
    NSDate *tempDate = [self.dateSelected copy];
    self.dateSelected = dayView.date;
    // Animation for the circleView
    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
    
    
    // Don't change page in week mode because block the selection of days in first and last weeks of the month
    if(self.calendarManager.settings.weekModeEnabled){
        return;
    }
    
    NSDate *date = [NSDate date];
    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.day = 90;
    NSDate *newDate = [cal dateByAddingComponents:components toDate:date options:0];
    BOOL isProperFirstDate = NO;
    BOOL isProperSecondDate = NO;
    if (([date compare:self.dateSelected] == NSOrderedAscending)) {
        isProperFirstDate = YES;
    } else {
        isProperFirstDate = NO;
    }
    if (([newDate compare:self.dateSelected] == NSOrderedDescending)) {
        isProperSecondDate = YES;
    } else {
        isProperSecondDate = NO;
    }

    if (isProperFirstDate && isProperSecondDate) {
        _isHoliday = NO;
        for (int i = 0; i < self.datesArray.count; i++) {
            NSDate *arrDate = [self.datesArray objectAtIndex:i];
            if (([arrDate compare:dayView.date] == NSOrderedSame)) {
                _isHoliday = YES;
                break;
            }
        }
        if (_isHoliday) {
            [self setDateSelected:tempDate];
            dayView.circleView.hidden = YES;
            [self showAlertWithTitle:@"Message" andMessage:@"Wrong selection. Please choose different date."];
            return;
        }
        // Load the previous or next page if touch a day from another month
        //(VRK) following condition should happen only if the current selected day is not a holiday
        if(![self.calendarManager.dateHelper date:self.calanderContentView.date isTheSameMonthThan:dayView.date]){
            if([self.calanderContentView.date compare:dayView.date] == NSOrderedAscending){
                [self.calanderContentView loadNextPageWithAnimation];
            }
            else{
                [self.calanderContentView loadPreviousPageWithAnimation];
            }
        }

        //(VRK) following calendar reload should happen only if the current selected day is not a holiday
        [UIView transitionWithView:dayView
                          duration:.3
                           options:0
                        animations:^{
                            dayView.circleView.transform = CGAffineTransformIdentity;
                            [self.calendarManager reload];
                        } completion:nil];

    } else {
        [self setDateSelected:tempDate];
        dayView.circleView.hidden = YES;
        [self showAlertWithTitle:@"Message" andMessage:@"Wrong selection. Please choose different date."];
        return;
    }

    [self validateForCurrentDateSelection:self.dateSelected];
}

#pragma mark - CalendarManager delegate - Page mangement

// Used to limit the date for the calendar, optional
- (BOOL)calendar:(JTCalendarManager *)calendar canDisplayPageWithDate:(NSDate *)date
{
    return [self.calendarManager.dateHelper date:date isEqualOrAfter:_minDate andEqualOrBefore:_maxDate];
}

//if the following methods are not getting used delete them
- (void)calendarDidLoadNextPage:(JTCalendarManager *)calendar
{

}

- (void)calendarDidLoadPreviousPage:(JTCalendarManager *)calendar
{
    
}

#pragma mark - Fake data

- (void)createMinAndMaxDate
{
    _todayDate = [NSDate date];
    
//    minDate = [self.calendarManager.dateHelper addToDate:todayDate months:0];
    _minDate = [self.calendarManager.dateHelper addToDate:_todayDate days:0];
    
//    maxDate = [self.calendarManager.dateHelper addToDate:todayDate months:2];
    _maxDate = [self.calendarManager.dateHelper addToDate:_todayDate days:90];
}

// Used only to have a key for _eventsByDate
- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy";
    }
    
    return dateFormatter;
}

- (BOOL)haveEventForDay:(NSDate *)date
{
    NSString *key = [[self dateFormatter] stringFromDate:date];
    
    if(_eventsByDate[key] && [_eventsByDate[key] count] > 0){
        return YES;
    }
    
    return NO;
    
}

- (void)createRandomEvents
{
    _eventsByDate = [NSMutableDictionary new];
    
    for(int i = 0; i < 30; ++i){
        // Generate 30 random dates between now and 60 days later
        NSDate *randomDate = [NSDate dateWithTimeInterval:(rand() % (3600 * 24 * 60)) sinceDate:[NSDate date]];
        
        // Use the date as key for eventsByDate
        NSString *key = [[self dateFormatter] stringFromDate:randomDate];
        
        if(!_eventsByDate[key]){
            _eventsByDate[key] = [NSMutableArray new];
        }
        
        [_eventsByDate[key] addObject:randomDate];
    }
}

#pragma mark - RetryView Delegate

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    [self getHolidayListFromAPI];
    
}

#pragma mark DLMAmmendActivationDateScreenDelegate methods

- (void)successfullyFetchedUserDataOnAmmendActivationDateScreen:(DLMAmendActivationDateScreen *)activationDateScreen {
    [self setNetworkRequestInProgress:false];
    [self setHolidaysArray:activationDateScreen.holidayList];
    [self setFormattedDatesToDatesArray];
    [self setDateSelected:self.currentSelectedDate];
    [self createCalanderView];
}

- (void)ammendActivationDateScreen:(DLMAmendActivationDateScreen *)activationDateScreen failedToFetchUserDataWithWebServiceError:(NLWebServiceError *)error {
    
    [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_ORDER_AMEND_ACTIVATION_DATE];
    [self setNetworkRequestInProgress:false];
    
    [self handleWebServiceError:error];
}


@end
