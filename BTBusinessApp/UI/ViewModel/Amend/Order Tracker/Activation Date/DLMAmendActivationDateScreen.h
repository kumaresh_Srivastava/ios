//
//  DLMAmendActivationDateScreen.h
//  BTBusinessApp
//
//  Created by Accolite on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
@class DLMAmendActivationDateScreen;
@class NLWebServiceError;

@protocol DLMAmmendActivationDateScreenDelegate <NSObject>

- (void)successfullyFetchedUserDataOnAmmendActivationDateScreen:(DLMAmendActivationDateScreen *)activationDateScreen;

- (void)ammendActivationDateScreen:(DLMAmendActivationDateScreen *)activationDateScreen failedToFetchUserDataWithWebServiceError:(NLWebServiceError *)error;

@end

@interface DLMAmendActivationDateScreen : DLMObject
@property (nonatomic, weak) id<DLMAmmendActivationDateScreenDelegate> activationDateDelegate;
@property (nonatomic) NSArray *holidayList;

- (void) cancelHolidayListAPIRequest;
- (void)fetchHolidayListForLoggedInUserWithPostCode:(NSString *)postCode;
- (void)fetchHolidayListForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef;

@end
