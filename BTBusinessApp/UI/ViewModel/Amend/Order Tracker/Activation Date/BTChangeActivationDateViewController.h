//
//  BTChangeActivationDateViewController.h
//  BTBusinessApp
//
//  Created by Accolite on 08/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JTCalendar/JTCalendar.h>
#import "BTBaseViewController.h"
@class BTAmmendViewController;

@protocol BTChangeActivationDateDelegate <NSObject>

@required
- (void)updateCurrentSelectedActivationDateWithNewActivationDate:(NSDate *)newActivationDate;

@end

@interface BTChangeActivationDateViewController : BTBaseViewController <JTCalendarDelegate>

@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calanderMenuView;
//@property (weak, nonatomic) IBOutlet JTHorizontalCalendarView *calanderContentView;
@property (weak, nonatomic) IBOutlet JTVerticalCalendarView *calanderContentView;

@property (strong, nonatomic) JTCalendarManager *calendarManager;
//@property (nonatomic,strong) NSString *orderRef;
//@property (nonatomic,strong) NSString *itemRef;
//@property (nonatomic,strong) NSString *postCode;
@property (nonatomic,strong) NSDate *currentSelectedDate;
@property (nonatomic) BOOL pcpOnly;
@property (nonatomic,strong) NSString* SIM2OrderRef;
@property (nonatomic) BOOL isSIM2Order;

@property (nonatomic,weak) id<BTChangeActivationDateDelegate> ammendChangeActivationDateDelegate;
@end
