//
//  BTOrderSiteContactsViewController.m
//  SampleActionViewProject
//
//  Created by VectoScalar on 10/26/16.
//  Copyright © 2016 VectoScalar. All rights reserved.
//

#import "BTOrderSiteContactsViewController.h"
#import "BTTextFiledModel.h"
#import "DLMAmmendSiteContactScreen.h"
#import "NSObject+APIResponseCheck.h"
#import "AppConstants.h"
#import "AppManager.h"
#import "OmnitureManager.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "NLWebServiceError.h"

@interface BTOrderSiteContactsViewController () <DLMAmmendSiteContactScreenDelegate, BTAddSiteContactTableViewControllerDelegare, SiteContactTableViewCellDelegate> {
    DLMAmmendSiteContactScreen *siteContactModel;
    BTOrderSiteContactModel *_previousContact;
    BTOrderSiteContactModel *_currentSelectedContact;
    NSString *_pageNameForOmniture;
}

@end

@implementation BTOrderSiteContactsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self trackPage];
    
    _previousContact = self.siteContact;
    _currentSelectedContact = self.siteContact;
    
    
    siteContactModel = [[DLMAmmendSiteContactScreen alloc] init];
    siteContactModel.siteContactDelegate = self;
    
    
    [self makeAPICall];
    [self updateRightNavigationBar];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark- Super Class Methods

- (UITableViewCell *)getCellForIndexPath:(NSIndexPath *)indexPath{
    
    
    SiteContactTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kSiteContactTableViewCellID];
    cell.delegate = self;
    
    BTOrderSiteContactModel *contact = [_dataArray objectAtIndex:indexPath.row];
    [cell updateWithBTContact:contact];
    
    
    if([contact.contactKey isEqualToString:_currentSelectedContact.contactKey]){
        
        [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 110.0;
}


- (void)cellSelectedAtIndexPath:(NSIndexPath *)indexPath{
    
    [self updateCellSelectionForIndexPath:indexPath];
    
}



- (void)selectViewForIndex:(NSInteger)selectedIndex{
    
    if(selectedIndex == 0){
        
        [self.view addSubview:tableView];
        [_addContactTableViewController.view removeFromSuperview];
        
    }
    else if (selectedIndex == 1){
        
        if(!_addContactTableViewController){
            
            _addContactTableViewController = [BTAddSiteContactTableViewController getBTAddSiteContactTableViewController];
            _addContactTableViewController.siteContactDict = self.siteContactDict;
            _addContactTableViewController.delegate = self;
        }
        
        _addContactTableViewController.view.frame = tableView.frame;
        [self addChildViewController:_addContactTableViewController];
        [self.view addSubview:_addContactTableViewController.view];
        [_addContactTableViewController didMoveToParentViewController:self];
        
    }
   
    [self updateRightNavigationBar];
}


- (void)cancelInProgressAPI{
    
    if(!self.isOpeningNeedHelp){
        
        [siteContactModel cancelSiteContactAPIRequest];
    }
    
}


- (void)saveButtonPressed:(id)sender{
    
    if(siteContactSegmentControl.selectedSegmentIndex == 0){
        
        [self.orderSiteContactDelegate btOrderSiteContactsViewController:self didSelectSiteContact:_currentSelectedContact];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        
        [_addContactTableViewController saveContactInfo];
    }
    
}



#pragma mark- class method

+ (BTOrderSiteContactsViewController *)getBTOrderSiteContactsViewController{
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTOrderSiteContactsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTOrderSiteContactsViewControllerID"];
    
    return vc;
}


#pragma mark- Helper Methods


- (void)dataChanged{
    
    [self updateRightNavigationBar];
}


- (void)makeAPICall{
    
    
    if(![AppManager isInternetConnectionAvailable]){
        
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:_pageNameForOmniture];
        return;
    }
    
    
    [self hideRetryView];
    self.networkRequestInProgress = YES;
    
    if([self.postalCode validAndNotEmptyStringObject]){
        
        [siteContactModel fetchSiteContactsForLoggedInUserWithOrderRef:self.orderReference itemRef:self.itemReference postCode:self.postalCode];
    }
    else{
        
        [siteContactModel fetchSiteContactsForLoggedInUserWithOrderRef:self.orderReference itemRef:self.itemReference];
    }
    
}


- (void)updateRightNavigationBar{
    
    
    if(siteContactSegmentControl.selectedSegmentIndex == 1 || self.isDataChanged){
        
        self.navigationItem.rightBarButtonItem = [self getSaveButtonItem];
    }
    else{
        
        self.navigationItem.rightBarButtonItem = nil;
    }
    
}


- (void)setDataChangeStatus{
    
    if(_previousContact && _currentSelectedContact){
        
        if([_previousContact.contactKey isEqualToString:_currentSelectedContact.contactKey]){
            
            self.isDataChanged = NO;
        }
        else{
            
            self.isDataChanged = YES;
        }
    }
    
}


- (void)updateCellSelectionForIndexPath:(NSIndexPath *)indexPath{
    
    BTOrderSiteContactModel *contact = [_dataArray objectAtIndex:indexPath.row];
    _currentSelectedContact = contact;
    [tableView reloadData];
    [self setDataChangeStatus];
    
    [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    
}




#pragma mark- SiteContactTableViewCellDelegate Method



- (void)userPressedRatioButtonOnSiteContactTableViewCell:(SiteContactTableViewCell *)cell{
    NSIndexPath *indexPath = [tableView indexPathForCell:cell];
    [self updateCellSelectionForIndexPath:indexPath];
    
}


#pragma mark - RetryView Delegate

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView{
    
    [self makeAPICall];
}


#pragma mark- BTAddSiteContactTableViewControllerDelegate Method

- (void)btAddSiteContactTableViewController:(BTAddSiteContactTableViewController *)controller didSubmitUserData:(NSDictionary *)dataDict{
    
    [self.orderSiteContactDelegate btOrderSiteContactsViewController:self didSubmitNewUserData:dataDict];
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark DLMAmmendSiteContactScreenDelegate methods

- (void)successfullyFetchedUserDataOnAmmendSiteContactScreen:(DLMAmmendSiteContactScreen *)siteContactScreen {
    
    self.networkRequestInProgress = NO;
    
    if(siteContactScreen && siteContactScreen.siteContacts && [siteContactScreen.siteContacts count] > 0){
        
        _dataArray = [NSArray arrayWithArray:siteContactScreen.siteContacts];
        
        [tableView reloadData];
    }
}

- (void)ammendSiteContactScreen:(DLMAmmendSiteContactScreen *)siteContactScreen failedToFetchUserDataWithWebServiceError:(NLWebServiceError *)webServiceError {
    
    self.networkRequestInProgress = NO;
    [self handleWebServiceError:webServiceError];
    [AppManager trackGenericAPIErrorOnPage:_pageNameForOmniture];
    
}


#pragma mark - Omniture Methods

- (void)trackPage
{
    if(self.isAlternativeContact)
    {
        [self trackOmniPage:OMNIPAGE_ORDER_ALTERNATE_SITECONTACT];
        _pageNameForOmniture = OMNIPAGE_ORDER_ALTERNATE_SITECONTACT;
    }
    else
    {
        [self trackOmniPage:OMNIPAGE_ORDER_SITECONTACT_SAVED_CONTACTS];
        _pageNameForOmniture = OMNIPAGE_ORDER_SITECONTACT_SAVED_CONTACTS;
    }
}

- (void)trackOmniPage:(NSString *)page
{
    NSMutableDictionary *contextDict = [NSMutableDictionary dictionary];
    NSString *orderRef = [OmnitureManager getOrderRefFormattedString:self.orderReference];
    [contextDict setValue:@"Logged In" forKey:kOmniLoginStatus];
    [contextDict setValue:orderRef forKey:kOmniOrderRef];

    [OmnitureManager trackPage:page withContextInfo:contextDict];
}



@end
