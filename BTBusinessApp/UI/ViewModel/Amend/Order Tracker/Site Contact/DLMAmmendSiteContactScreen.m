//
//  DLMAmmendSiteContactScreen.m
//  BTBusinessApp
//
//  Created by Accolite on 02/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMAmmendSiteContactScreen.h"
#import "AppDelegate.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppDelegateViewModel.h"
#import "AppConstants.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"

#import "NLSiteContactWebService.h"

@interface DLMAmmendSiteContactScreen () <NLSiteContactWebServiceDelegate>
@property (nonatomic) NLSiteContactWebService *getSiteContactWebService;
@end

@implementation DLMAmmendSiteContactScreen

#pragma mark Public Methods
- (void)fetchSiteContactsForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef
{
    self.getSiteContactWebService = [[NLSiteContactWebService alloc] initWithOrderRef:orderRef andItemRef:itemRef];
    self.getSiteContactWebService.getSiteContactWebServiceDelegate = self;
    [self.getSiteContactWebService resume];
}

- (void)fetchSiteContactsForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef postCode:(NSString *)postCode
{
    self.getSiteContactWebService = [[NLSiteContactWebService alloc] initWithOrderRef:orderRef itemRef:itemRef andPostCode:postCode];
    self.getSiteContactWebService.getSiteContactWebServiceDelegate = self;
    [self.getSiteContactWebService resume];
}

#pragma mark CancellingDashBoard request methods

- (void) cancelSiteContactAPIRequest {
    self.getSiteContactWebService.getSiteContactWebServiceDelegate = nil;
    [self.getSiteContactWebService cancel];
    self.getSiteContactWebService  = nil;
}


#pragma mark - NLSiteContactWebServiceDelegate Methods
- (void)getSiteContactsWebService:(NLSiteContactWebService *)webService successfullyFetchedSiteContacts:(NSArray *)siteContacts currentSiteContactIndex:(int)siteContactIndex currentAltSiteContactIndex:(int)altSiteContactIndex
{
    if(webService == self.getSiteContactWebService)
    {
        [self setSiteContacts:siteContacts];
        [self setCurrentSiteContactIndex:siteContactIndex];
        [self setCurrentAltSiteContactIndex:altSiteContactIndex];
        [self.siteContactDelegate successfullyFetchedUserDataOnAmmendSiteContactScreen:self];
        
        self.getSiteContactWebService.getSiteContactWebServiceDelegate = nil;
        self.getSiteContactWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLSiteContactWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLSiteContactWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)getSiteContactsWebService:(NLSiteContactWebService *)webService failedToFetchSiteContactsWithWebServiceError:(NLWebServiceError *)webServiceError
{
    if(webService == self.getSiteContactWebService)
    {
        [self.siteContactDelegate ammendSiteContactScreen:self failedToFetchUserDataWithWebServiceError:webServiceError];
        
        self.getSiteContactWebService.getSiteContactWebServiceDelegate = nil;
        self.getSiteContactWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLSiteContactWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLSiteContactWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}
@end
