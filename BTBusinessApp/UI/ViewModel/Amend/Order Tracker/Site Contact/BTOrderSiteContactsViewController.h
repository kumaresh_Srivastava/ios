//
//  BTOrderSiteContactsViewController.h
//  SampleActionViewProject
//
//  Created by VectoScalar on 10/26/16.
//  Copyright © 2016 VectoScalar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTSiteContactsViewController.h"
#import "BTOrderSiteContactModel.h"

@class BTOrderSiteContactsViewController;

@protocol BTOrderSiteContactsViewControllerDelegate <NSObject>

- (void)btOrderSiteContactsViewController:(BTOrderSiteContactsViewController *)controller didSubmitNewUserData:(NSDictionary *)dataDict;

- (void)btOrderSiteContactsViewController:(BTOrderSiteContactsViewController *)controller didSelectSiteContact:(BTOrderSiteContactModel *)siteContactModel;

@end

@interface BTOrderSiteContactsViewController : BTSiteContactsViewController
@property(nonatomic, weak) id<BTOrderSiteContactsViewControllerDelegate>orderSiteContactDelegate;
@property(nonatomic, strong) BTOrderSiteContactModel *siteContact;
@property(nonatomic, strong) NSDictionary *siteContactDict;

+ (BTOrderSiteContactsViewController *)getBTOrderSiteContactsViewController;

@end
