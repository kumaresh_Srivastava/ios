//
//  DLMAmmendSiteContactScreen.h
//  BTBusinessApp
//
//  Created by Accolite on 02/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
@class DLMAmmendSiteContactScreen;
@class NLWebServiceError;
@protocol DLMAmmendSiteContactScreenDelegate <NSObject>

- (void)successfullyFetchedUserDataOnAmmendSiteContactScreen:(DLMAmmendSiteContactScreen *)siteContactScreen;

- (void)ammendSiteContactScreen:(DLMAmmendSiteContactScreen *)siteContactScreen failedToFetchUserDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface DLMAmmendSiteContactScreen : DLMObject
@property (nonatomic, weak) id<DLMAmmendSiteContactScreenDelegate> siteContactDelegate;
@property (nonatomic) NSArray *siteContacts;
@property (nonatomic,assign) int currentSiteContactIndex;
@property (nonatomic,assign) int currentAltSiteContactIndex;

- (void)fetchSiteContactsForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef;
- (void)fetchSiteContactsForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef postCode:(NSString *)postCode;
- (void) cancelSiteContactAPIRequest;
@end
