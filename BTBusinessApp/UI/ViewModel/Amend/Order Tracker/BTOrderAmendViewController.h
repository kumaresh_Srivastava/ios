//
//  OrderAmendViewController.h
//  SampleActionViewProject
//
//  Created by VectoScalar on 10/26/16.
//  Copyright © 2016 VectoScalar. All rights reserved.
//

#import "BTAmendViewController.h"
#import "DLMOrderDetailsScreen.h"

@class BTOrderAmendViewController;

@protocol BTOrderAmendViewControllerDelegate <NSObject>

- (void)btOrderAmendViewController:(BTOrderAmendViewController *)controller didAmendOrderForOrderDetailsModel:(DLMOrderDetailsScreen *)oderDetailModel isEngineerAppointmentChanged:(BOOL)isChanged;

@end

@interface BTOrderAmendViewController : BTAmendViewController

@property(nonatomic, weak) id<BTOrderAmendViewControllerDelegate> orderAmendDelegate;
@property(nonatomic, strong) DLMOrderDetailsScreen *oderDetailModel;
@property(nonatomic, assign) BOOL needToPopulateDummyData;
@property(nonatomic, assign) BOOL isShowAppointment;
@property(nonatomic, assign) BOOL isSIM2Order;
@property(nonatomic, assign) BOOL isSecondaryOrder;

//Class Method to get the instance of this class
+ (BTOrderAmendViewController *)getBTOrderAmendViewController;
@end
