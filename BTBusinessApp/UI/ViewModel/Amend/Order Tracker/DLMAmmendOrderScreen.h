
//
//  DLMAmmendOrderScreen.h
//  BTBusinessApp
//
//  Created by Accolite on 27/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"

@class DLMAmmendOrderScreen;
@class BTAppointment;
@class BTOrderSiteContactModel;
@class BTProduct;
@class BTBillingDetails;
@class BTPricingDetails;
@class BTProductDetails;
@class NLWebServiceError;

@protocol DLMAmmendOrderScreenDelegate <NSObject>

- (void)successfullyCheckedAmmendOrderOnAmmendOrderScreen:(DLMAmmendOrderScreen *)ammendScreen;
- (void)ammendOrderScreen:(DLMAmmendOrderScreen *)ammendScreen failedToFetchUserDataWithWebServiceError:(NLWebServiceError *)error;
- (void)amendSuccessfullyFinishedByAmmendOrderScreen:(DLMAmmendOrderScreen *)ammendScreen;
- (void)ammendOrderScreen:(DLMAmmendOrderScreen *)ammendScreen failedToSubmitAmendOrderWithWebServiceError:(NLWebServiceError *)error;

@end

@interface DLMAmmendOrderScreen : DLMObject
@property (nonatomic, weak) id<DLMAmmendOrderScreenDelegate> ammendOrderDelegate;
@property (nonatomic) NSDictionary *amendStatusDict;
@property (nonatomic, assign) NSInteger authLevel;
@property (nonatomic) BTProduct *productSummary;
@property (nonatomic) BTBillingDetails *billingDetails;
@property (nonatomic) BTPricingDetails *pricingDetails;
@property (nonatomic) BTProductDetails *productDetails;



- (void)checkAmmendOrderForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef ammendAttribute:(NSString *)ammendAttribute;
- (void)cancelAmmendCheckAPIRequest;

- (void)submitAndMakeAmendOrderToServerWithNewEngineeringAppointment:(BTAppointment *)newEngineeringAppointment newSiteContact:(BTOrderSiteContactModel *)newSiteContact alternateSiteContact:(BTOrderSiteContactModel *)newAlternateSiteContact newActivationDate:(NSDate *)newActivationDate postalCode:(NSString *)postalCode pcp:(BOOL)pcpOnly forOrderRef:(NSString *)orderRef andItemReference:(NSString *)itemRef;
- (void)cancelAmendOrderSubmitAPIRequest;
- (BOOL)isOrderAmendableForAmendAttribute:(NSString *)amendAttribute;

@end
