//
//  DLMAmendFaultScreen.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMAmendFaultScreen.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "NLGetFaultContactDetailsWebService.h"
#import "NLFaultCallDiversionWebService.h"
#import "NLFaultAppointmentWebService.h"
#import "AppManager.h"
#import "NSObject+APIResponseCheck.h"
#import "NLAmendFaultSubmitWebService.h"
#import "BTCallDiversionDetail.h"
#import "BTFaultContactDetails.h"
#import "BTOrderSiteContactModel.h"

@interface DLMAmendFaultScreen () <NLGetFaultSiteContactWebServiceDelegate,NLFaultCallDiversionWebServiceDelegate,NLFaultAppointmentWebServiceDelegate, NLAmendFaultSubmitWebServiceDelegate> {
    
    BOOL _isGetAppointmentDateAPICallSuccessfullyDone;
    BOOL _isGetCallDiversionAPICallSuccessfullyDone;
}

@property (nonatomic) NLGetFaultContactDetailsWebService *getFaultContactDetailsWebService;
@property (nonatomic) NLFaultCallDiversionWebService *getFaultCallDiversionWebService;
@property (nonatomic) NLFaultAppointmentWebService *getFaultAppointmentDetailWebService;

@property (nonatomic) NLAmendFaultSubmitWebService *amendFaultSubmitWebService;

@end


@implementation DLMAmendFaultScreen

#pragma mark - Public Methods

- (void)fetchAmendFaultDataWithFaultRef:(NSString *)faultRef
{
    _faultRef = faultRef;
    
    // (LP) Here we first fetch contact details. In the response, we get boolean values related to appointment and call diversion. On the basis of those values, we fetch remaining data.
    [self fetchFaultContactDetails];
    
    DDLogInfo(@"Fetching amend fault data from server for fault ref %@", faultRef);
    
}

- (void)submitAndMakeAmendFaultToServerWithNewContactDetails:(BTFaultContactDetails *)newFaultContactDetails appointmentDict:(NSDictionary *)appointmentDict andCallDiversionDetails:(BTCallDiversionDetail *)callDiversionDetail{
    
    
    NSMutableDictionary *amendDictionary = [NSMutableDictionary dictionary];
    
    [amendDictionary addEntriesFromDictionary:appointmentDict];//Appointment Dict
    
    [amendDictionary setValue:callDiversionDetail.callDivertFlag forKey:@"callDivertFlag"];
    [amendDictionary setValue:callDiversionDetail.callDivertNumber forKey:@"callDivertNumber"];
    [amendDictionary setValue:callDiversionDetail.callDivertDate forKey:@"callDivertDate"];
    [amendDictionary setValue:[NSNull null] forKey:@"hazardNotes"];
    [amendDictionary setValue:[NSNull null] forKey:@"splIntrNotes"];
    [amendDictionary setValue:@"edit" forKey:@"operatingMode"];
    
    // Data for contact model dictionary
    NSMutableDictionary *reporterContactsDictionary = [NSMutableDictionary dictionary];
    
    [reporterContactsDictionary setValue:newFaultContactDetails.reporterContacts.title forKey:@"Title"];
    [reporterContactsDictionary setValue:newFaultContactDetails.reporterContacts.firstName forKey:@"FirstName"];
    [reporterContactsDictionary setValue:newFaultContactDetails.reporterContacts.lastName forKey:@"LastName"];
    [reporterContactsDictionary setValue:newFaultContactDetails.reporterContacts.mobile forKey:@"TextNo"];
    [reporterContactsDictionary setValue:newFaultContactDetails.reporterContacts.phone forKey:@"VoiceNo"];
    [reporterContactsDictionary setValue:newFaultContactDetails.reporterContacts.email forKey:@"EmailId"];
    [reporterContactsDictionary setValue:newFaultContactDetails.reporterContacts.preferredContact forKey:@"PreferredContact"];
    
    NSMutableDictionary *siteContactsDictionary = [NSMutableDictionary dictionary];
    
    [siteContactsDictionary setValue:newFaultContactDetails.siteContacts.title forKey:@"Title"];
    [siteContactsDictionary setValue:newFaultContactDetails.siteContacts.firstName forKey:@"FirstName"];
    [siteContactsDictionary setValue:newFaultContactDetails.siteContacts.lastName forKey:@"LastName"];
    [siteContactsDictionary setValue:newFaultContactDetails.siteContacts.mobile forKey:@"TextNo"];
    [siteContactsDictionary setValue:newFaultContactDetails.siteContacts.phone forKey:@"VoiceNo"];
    [siteContactsDictionary setValue:newFaultContactDetails.siteContacts.email forKey:@"EmailId"];
    [siteContactsDictionary setValue:newFaultContactDetails.siteContacts.preferredContact forKey:@"PreferredContact"];
    
    NSMutableDictionary *contactModelDictionary = [NSMutableDictionary dictionary];
    
    [contactModelDictionary setValue:reporterContactsDictionary forKey:@"reporterContacts"];
    [contactModelDictionary setValue:siteContactsDictionary forKey:@"siteContacts"];
    [contactModelDictionary setValue:newFaultContactDetails.reporterSameasSite forKey:@"reporterSameasSite"];
    [contactModelDictionary setValue:[NSNumber numberWithBool:newFaultContactDetails.isNetworkFault] forKey:@"isNetworkFault"];
    [contactModelDictionary setValue:newFaultContactDetails.pageToRedirect forKey:@"pageToRedirect"];
    [contactModelDictionary setValue:newFaultContactDetails.moduleId forKey:@"moduleId"];
    
    [amendDictionary setValue:contactModelDictionary forKey:@"contactModel"];
    
    
    self.amendFaultSubmitWebService.amendFaultSubmitWebServiceDelegate = nil;
    self.amendFaultSubmitWebService = [[NLAmendFaultSubmitWebService alloc] initWithAmendFaultRequestDictionary:amendDictionary andFaultReference:_faultRef];
    
    self.amendFaultSubmitWebService.amendFaultSubmitWebServiceDelegate = self;
    [self.amendFaultSubmitWebService resume];
    
    
}


- (NSDictionary *)amendAppointmentDictionaryForAppointment:(BTFaultAppointment *)appointment isAppointmentChanged:(BOOL)appointmentChanged{
    
    NSMutableDictionary *amendDictionary = [NSMutableDictionary dictionary];
    
    [amendDictionary setValue:[NSNull null] forKey:@"latestAccessTime"];
    [amendDictionary setValue:[NSNull null] forKey:@"earlierAccessTime"];
    
    if (appointmentChanged) {
        [amendDictionary setValue:[NSNumber numberWithBool:false] forKey:@"isAmndApptmntDateSameAsSubmit"];
        
        if (appointment.isAmSelected) {
            [amendDictionary setValue:[NSString stringWithFormat:@"%@-AM",[AppManager NSStringFromNSDateWithReverseDateFormat:appointment.dateInfo]] forKey:@"appReservedDate"];
        }
        else
        {
            [amendDictionary setValue:[NSString stringWithFormat:@"%@-PM",[AppManager NSStringFromNSDateWithReverseDateFormat:appointment.dateInfo]] forKey:@"appReservedDate"];
        }
        
    } else {
        [amendDictionary setValue:[NSNumber numberWithBool:true] forKey:@"isAmndApptmntDateSameAsSubmit"];
        
        NSInteger hour = [AppManager getHourFromDate:_faultAppointmentDetail.bookedEndDate];
        
        if (hour<13) {
            [amendDictionary setValue:[NSString stringWithFormat:@"%@-AM",[AppManager NSStringFromNSDateWithReverseDateFormat:_faultAppointmentDetail.bookedEndDate]] forKey:@"appReservedDate"];
        }
        else
        {
            [amendDictionary setValue:[NSString stringWithFormat:@"%@-PM",[AppManager NSStringFromNSDateWithReverseDateFormat:_faultAppointmentDetail.bookedEndDate]] forKey:@"appReservedDate"];
        }
    }
    
    
    return [NSDictionary dictionaryWithDictionary:amendDictionary];
}


- (NSDictionary *)amendAppointmentDictionaryForAppointmentDate:(NSDate *)appointmentDate slotEarliestTime:(NSString *)earliestTime slotLatestTime:(NSString *)latestTime andIsAppointmentChanged:(BOOL)isAppointmentChanged{
    
    
    NSMutableDictionary *amendDictionary = [NSMutableDictionary dictionary];
    
    if (isAppointmentChanged) {
        
        [amendDictionary setValue:latestTime forKey:@"latestAccessTime"];
        [amendDictionary setValue:earliestTime forKey:@"earlierAccessTime"];
        [amendDictionary setValue:[NSNumber numberWithBool:false] forKey:@"isAmndApptmntDateSameAsSubmit"];
        
        NSString *formattedDateString = [self getFormattedDateSrtringForAccessTimeSlotFromDate:appointmentDate];
        [amendDictionary setValue:formattedDateString forKey:@"appReservedDate"];
        
    } else {
        
        [amendDictionary setValue:_faultAppointmentDetail.latestAccessTime forKey:@"latestAccessTime"];
        [amendDictionary setValue:_faultAppointmentDetail.earlierAccessTime forKey:@"earlierAccessTime"];
        
        [amendDictionary setValue:[NSNumber numberWithBool:true] forKey:@"isAmndApptmntDateSameAsSubmit"];
        NSString *formattedDateString = [self getFormattedDateSrtringForAccessTimeSlotFromDate:_faultAppointmentDetail.bookedStartDate];
        [amendDictionary setValue:formattedDateString forKey:@"appReservedDate"];
    }
    
    
    return [NSDictionary dictionaryWithDictionary:amendDictionary];
}


- (AmendViewModel *)getAmendModelForAppointmentDate:(NSDate *)date{
    
    AmendViewModel *engineerAppointment = nil;
    
    if(date){
        //Engineer Appointment
        
        
        NSInteger hour = [AppManager getHourFromDate:date];
        NSString *amPM = nil;
        
        
        if (hour < 13) {
            
            amPM = @"8am - 1pm";
            
        } else{
            
            amPM = @"1pm - 6pm";
        }
        
        NSString *dateString = [self getAppointmentFormattedDate:date];
        
        //Model object for Engineer Appointment Cell
        engineerAppointment = [[AmendViewModel alloc] init];
        engineerAppointment.isEditable = YES;
        engineerAppointment.amendContext = kEngineerAppointmentText;
        engineerAppointment.subHeading1 = [engineerAppointment getAttributtedStringForTextString:@"Date " andValuString:dateString];
        
        if(amPM)
            engineerAppointment.subHeading2 = [engineerAppointment getAttributtedStringForTextString:@"Time " andValuString:amPM];
        else
            engineerAppointment.subHeading2 = nil;
        
        if(_faultAppointmentDetail.isHourAccessPresent){
            
            engineerAppointment.subHeading2 = nil; //To hide time when access time available
        }
    }
    
    return engineerAppointment;
    
}


- (AmendViewModel *)getAmendModelForAppointment:(BTFaultAppointment *)appointment{
    
    AmendViewModel *engineerAppointment = nil;
    
    
    NSString *amPM = @"";
    
    if(appointment.isAmSelected){
        
        amPM = @"8am - 1pm";
    }
    else if(appointment.isPmSelected){
        
        amPM = @"1pm - 6pm";
    }
    
    NSString *dateString = [self getAppointmentFormattedDate:appointment.dateInfo];
    
    //Model object for Engineer Appointment Cell
    engineerAppointment = [[AmendViewModel alloc] init];
    engineerAppointment.isEditable = YES;
    engineerAppointment.amendContext = kEngineerAppointmentText;
    engineerAppointment.subHeading1 = [engineerAppointment getAttributtedStringForTextString:@"Date " andValuString:dateString];
    
    if(amPM)
        engineerAppointment.subHeading2 = [engineerAppointment getAttributtedStringForTextString:@"Time " andValuString:amPM];
    else
        engineerAppointment.subHeading2 = nil;
    
    if(_faultAppointmentDetail.isHourAccessPresent){
        
        engineerAppointment.subHeading2 = nil; //To hide time when access time available
    }
    
    
    return engineerAppointment;
}


- (AmendViewModel *)getamendModelForContactDetail:(BTFaultContactDetails *)contactDetail{
    
    AmendViewModel *contactDetailModel = nil;
    
    if(contactDetail){
        
        contactDetailModel = [self getAmendModelFromSiteContact:contactDetail.reporterContacts];
        
    }
    
    return contactDetailModel;
    
}


- (AmendViewModel *)getamendModelForCallDiversionDetail:(BTCallDiversionDetail *)callDiversionDetail{
    
    AmendViewModel *callDiversionAmend = nil;
    
    if(callDiversionDetail){
        
        callDiversionAmend = [[AmendViewModel alloc] init];
        callDiversionAmend.isEditable = YES;
        callDiversionAmend.amendContext = kCallDiversionText;
        
        if([[callDiversionDetail.callDivertFlag lowercaseString] isEqualToString:@"add"]){
            
            callDiversionAmend.subHeading1 = [callDiversionAmend getAttributtedStringForTextString:@"Don't divert my calls" andValuString:@""];
        }
        else if(callDiversionDetail.callDivertNumber && [callDiversionDetail.callDivertNumber validAndNotEmptyStringObject]){
            
            callDiversionAmend.subHeading1 = [callDiversionAmend getAttributtedStringForTextString:@"Calls diverting to " andValuString:callDiversionDetail.callDivertNumber];
        }
        
        
        callDiversionAmend.subHeading2 = nil;
        
        
    }
    
    return callDiversionAmend;
}


- (void)addTimeForEarliestTime:(NSString *)earliestTime latestTime:(NSString *)latestTimeSlot toAmendViewModel:(AmendViewModel *)amendViewModel{
    
    if(!earliestTime && !latestTimeSlot){
        
        earliestTime = _faultAppointmentDetail.earlierAccessTime;
        latestTimeSlot = _faultAppointmentDetail.latestAccessTime;
    }
    
    if(earliestTime && latestTimeSlot){
        
        ///*****************Earliest Time****************
        //Removing colon and zeros after colon
        NSString *latestTimenewString = [latestTimeSlot stringByReplacingOccurrencesOfString:@":00" withString:@""];
        latestTimenewString = [latestTimenewString stringByReplacingOccurrencesOfString:@":00" withString:@""];
        
        //Removing leading zero
        NSRange range = [latestTimenewString rangeOfString:@"^0*" options:NSRegularExpressionSearch];
        latestTimenewString = [latestTimenewString stringByReplacingCharactersInRange:range withString:@""];
        
        
        ///*****************Latest Time****************
        //Removing colon and zeros after colon
        NSString *earliestTimeNewString = [earliestTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
        earliestTimeNewString = [earliestTimeNewString stringByReplacingOccurrencesOfString:@":00" withString:@""];
        
        //Removing leading zero
        range = [earliestTimeNewString rangeOfString:@"^0*" options:NSRegularExpressionSearch];
        earliestTimeNewString = [earliestTimeNewString stringByReplacingCharactersInRange:range withString:@""];
        
        NSString *timeString = [NSString stringWithFormat:@"%@ - %@", earliestTimeNewString, latestTimenewString];
        amendViewModel.subHeading2 = [amendViewModel getAttributtedStringForTextString:@"Time " andValuString:timeString];
        
    }
    
}

#pragma mark - Private helper methods

- (void)fetchFaultContactDetails {

    self.getFaultContactDetailsWebService.getContactDetailsWebServiceDelegate = nil;
    self.getFaultContactDetailsWebService = [[NLGetFaultContactDetailsWebService alloc] initWithFaultRef:_faultRef];
    self.getFaultContactDetailsWebService.getContactDetailsWebServiceDelegate = self;
    [self.getFaultContactDetailsWebService resume];
    
}



- (void)fetchFaultCallDiversionDetails
{
    self.getFaultCallDiversionWebService.getFaultCallDiversionWebServiceDelegate = nil;
    self.getFaultCallDiversionWebService = [[NLFaultCallDiversionWebService alloc] initWithFaultID:_faultRef];
    self.getFaultCallDiversionWebService.getFaultCallDiversionWebServiceDelegate = self;
    [self.getFaultCallDiversionWebService resume];

}


- (void)fetchFaultAppointmentDetails
{

    self.getFaultAppointmentDetailWebService.getfaultAppointmentWebServiceDelegateDelegate = nil;
    self.getFaultAppointmentDetailWebService = [[NLFaultAppointmentWebService alloc] initWithFaultID:_faultRef andInputDate:[self getFormatedDateFromDate:[NSDate date]]];
    self.getFaultAppointmentDetailWebService.getfaultAppointmentWebServiceDelegateDelegate = self;
    [self.getFaultAppointmentDetailWebService resume];

}


- (NSString *)getFormattedDateSrtringForAccessTimeSlotFromDate:(NSDate *)date{
    
    NSString *outputFormate = @"dd/MM/yyyy";
    NSString *outputDateString = @"";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if(date){
        
        [dateFormatter setDateFormat:outputFormate];
        outputDateString= [dateFormatter stringFromDate:date];
    }
    
    return outputDateString;
    
}


- (AmendViewModel *)getAmendModelFromSiteContact:(BTOrderSiteContactModel *)siteContact{
    
    if(!siteContact)
        return nil;
    
    //Alt Site Contact
    NSString *title = siteContact.title;
    NSString *firstName = siteContact.firstName;
    NSString *secondName = siteContact.lastName;
    
    //Nill Checks to avoid "null" string appearance on UI
    if(!title){
        
        title = @"";
    }
    
    if(!firstName){
        
        firstName = @"";
    }
    
    if(!secondName){
        
        secondName = @"";
    }
    
    
    NSString *name = [NSString stringWithFormat:@"%@ %@ %@",title,firstName,secondName];
    name = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *contatNumber = [self getContactNumberFromSiteContact:siteContact];
    
    
    AmendViewModel *amendModel = [[AmendViewModel alloc] init];
    amendModel.isEditable = YES;
    amendModel.amendContext = kSiteContactText;
    amendModel.subHeading1 = [amendModel getAttributtedStringForTextString:name andValuString:@""];
    amendModel.subHeading2 = nil;
    
    if([contatNumber validAndNotEmptyStringObject]){
        
        amendModel.subHeading2 = [amendModel getAttributtedStringForTextString:@"Phone " andValuString:contatNumber];
    }
    
    
    return amendModel;
    
}


- (NSString *)getAppointmentFormattedDate:(NSDate *)date
{
    
    NSString *dateFormat = @"dd/MM/yyyy";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
}


- (NSString *)getContactNumberFromSiteContact:(BTOrderSiteContactModel *)siteContact{
    
    if(!siteContact)
        return nil;
    
    NSString *contatNumber = @"";
    NSString *mobile = siteContact.mobile;
    NSString *phone = siteContact.phone;//phone
    
    
    if(mobile && [mobile validAndNotEmptyStringObject]){
        
        contatNumber = mobile;
    }
    else{
        
        contatNumber = phone;
    }
    
    return contatNumber;
    
    
}


- (void)fetchRemainingDataForAmendFaultAfterContactDetails
{
    if (_showAppointment && _showCallDivert) {
        
        [self fetchFaultAppointmentDetails];
        [self fetchFaultCallDiversionDetails];
        
    } else if (_showAppointment ) {
        
        [self fetchFaultAppointmentDetails];
        
    } else if (!_showAppointment ) {
        
        [self fetchFaultCallDiversionDetails];
        
    } else
    {
        [self.amendFaultScreenDelegate successfullyFetchedAmendFaultDataOnAmendFaultScreen:self];
    }
    
}

- (void)checkForCompleteAmendDataAvailablity
{
    if (_showAppointment && _showCallDivert) {
        
        if (_isGetAppointmentDateAPICallSuccessfullyDone && _isGetCallDiversionAPICallSuccessfullyDone) {
            
            [self.amendFaultScreenDelegate successfullyFetchedAmendFaultDataOnAmendFaultScreen:self];
        }
        
    } else if (_showAppointment ) {
        
        if (_isGetAppointmentDateAPICallSuccessfullyDone) {
            
            [self.amendFaultScreenDelegate successfullyFetchedAmendFaultDataOnAmendFaultScreen:self];
        }
        
    } else if ( _showCallDivert) {
        
        if (_isGetCallDiversionAPICallSuccessfullyDone) {
            
            [self.amendFaultScreenDelegate successfullyFetchedAmendFaultDataOnAmendFaultScreen:self];
        }
        
    } else
    {
        [self.amendFaultScreenDelegate successfullyFetchedAmendFaultDataOnAmendFaultScreen:self];
    }
    
}


- (NSString *)getFormatedDateFromDate:(NSDate *)date {
    
    //NSString *dateFormat = @"yyyy-MM-dd";
    NSString *dateFormat = @"dd-MM-yyyy";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
    
}


#pragma mark - Cancel API request

- (void)cancelFetchAmendFaultDataAPIRequest
{
    
    self.getFaultContactDetailsWebService.getContactDetailsWebServiceDelegate = nil;
    [self.getFaultContactDetailsWebService cancel];
    self.getFaultContactDetailsWebService = nil;
    
    self.getFaultCallDiversionWebService.getFaultCallDiversionWebServiceDelegate = nil;
    [self.getFaultCallDiversionWebService cancel];
    self.getFaultCallDiversionWebService = nil;
    
    self.getFaultAppointmentDetailWebService.getfaultAppointmentWebServiceDelegateDelegate = nil;
    [self.getFaultAppointmentDetailWebService cancel];
    self.getFaultAppointmentDetailWebService = nil;
    
}

- (void)cancelAmendOrderSubmitAPIRequest
{
    
    self.amendFaultSubmitWebService.amendFaultSubmitWebServiceDelegate = nil;
    [self.amendFaultSubmitWebService cancel];
    self.amendFaultSubmitWebService = nil;
    
}


#pragma mark - NLGetContactDetailWebServiceDelegate Methods

- (void)getContactDetailsWebService:(NLGetFaultContactDetailsWebService *)webService successfullyFetchedFaultContactDetails:(BTFaultContactDetails *)faultContactDetails withShowCallDivert:(BOOL)showCallDivert andShowAppointment:(BOOL)showAppointment
{
    
    if (webService == _getFaultContactDetailsWebService)
    {
        _contactDetails = faultContactDetails;
        _showCallDivert = showCallDivert;
        _showAppointment = showAppointment;
        
        [self fetchRemainingDataForAmendFaultAfterContactDetails];
        
        self.getFaultContactDetailsWebService.getContactDetailsWebServiceDelegate = nil;
        self.getFaultContactDetailsWebService = nil;
        
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLGetFaultContactDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLGetFaultContactDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
    
}

- (void)getContactDetailsWebService:(NLGetFaultContactDetailsWebService *)webService failedToFetchSiteContactsWithWebServiceError:(NLWebServiceError *)webServiceError
{
    if (webService == _getFaultContactDetailsWebService)
    {
        [self.amendFaultScreenDelegate amendFaultScreen:self failedToFetchAmendFaultDataWithWebServiceError:webServiceError];
        
        self.getFaultContactDetailsWebService.getContactDetailsWebServiceDelegate = nil;
        self.getFaultContactDetailsWebService = nil;
        
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLGetFaultContactDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLGetFaultContactDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}


#pragma mark - NLFaultCallDiversionWebService Delegate

- (void)faultCallDiversionWebService:(NLFaultCallDiversionWebService *)webService successfullyFetchedFaultCallDiversionData:(BTCallDiversionDetail *)callDiversionDetail
{
    if (webService == _getFaultCallDiversionWebService)
    {
        _callDiversionDetail = callDiversionDetail;
        _isGetCallDiversionAPICallSuccessfullyDone = YES;
        
        [self checkForCompleteAmendDataAvailablity];
        
        self.getFaultCallDiversionWebService.getFaultCallDiversionWebServiceDelegate = nil;
        self.getFaultCallDiversionWebService = nil;
        
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLFaultCallDiversionWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLFaultCallDiversionWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)faultCallDiversionWebService:(NLFaultCallDiversionWebService *)webService failedToFetchFaultCallDiversionDataWithWebServiceError:(NLWebServiceError *)error
{
    if (webService == _getFaultCallDiversionWebService)
    {
        [self.amendFaultScreenDelegate amendFaultScreen:self failedToFetchAmendFaultDataWithWebServiceError:error];
        
        self.getFaultCallDiversionWebService.getFaultCallDiversionWebServiceDelegate = nil;
        self.getFaultCallDiversionWebService = nil;
        
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLFaultCallDiversionWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLFaultCallDiversionWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
    
}



#pragma mark -  NLFaultAppointmentWebService Delegate


- (void)faultAppointmentWebService:(NLFaultAppointmentWebService *)webService successfullyFetchedfaultAppointmentWebServiceData:(BTFaultAppointmentDetail *)faultAppointmentDetail
{
    if (webService == _getFaultAppointmentDetailWebService)
    {
        _faultAppointmentDetail = faultAppointmentDetail;
        _isGetAppointmentDateAPICallSuccessfullyDone = YES;
        
        [self checkForCompleteAmendDataAvailablity];
        
        self.getFaultAppointmentDetailWebService.getfaultAppointmentWebServiceDelegateDelegate = nil;
        self.getFaultAppointmentDetailWebService = nil;
        
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLFaultAppointmentWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLFaultAppointmentWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
    
}

- (void)faultAppointmentWebService:(NLFaultAppointmentWebService *)webService failedToFetchFaultAppointmentWebServiceDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    if (webService == _getFaultAppointmentDetailWebService)
    {
        [self.amendFaultScreenDelegate amendFaultScreen:self failedToFetchAmendFaultDataWithWebServiceError:webServiceError];
        
        self.getFaultAppointmentDetailWebService.getfaultAppointmentWebServiceDelegateDelegate = nil;
        self.getFaultAppointmentDetailWebService = nil;
        
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLFaultAppointmentWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLFaultAppointmentWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}


#pragma mark - NLAmendFaultSubmitWebService Methods

- (void)faultAmendSubmissionSuccesfullyFinishedByAmendFaultSubmitWebService:(NLAmendFaultSubmitWebService *)webService
{
    if (webService == _amendFaultSubmitWebService)
    {
        [self.amendFaultScreenDelegate amendSuccessfullyFinishedByAmmendFaultScreen:self];
        
        self.amendFaultSubmitWebService.amendFaultSubmitWebServiceDelegate = nil;
        self.amendFaultSubmitWebService = nil;
        
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLAmendFaultSubmitWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLAmendFaultSubmitWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)amendFaultSubmitWebService:(NLAmendFaultSubmitWebService *)webService failedToSubmitAmendOrderWithWebServiceError:(NLWebServiceError *)webServiceError
{
    if (webService == _amendFaultSubmitWebService)
    {
        [self.amendFaultScreenDelegate ammendFaultScreen:self failedToSubmitAmendFaultWithWebServiceError:webServiceError];
        
        self.amendFaultSubmitWebService.amendFaultSubmitWebServiceDelegate = nil;
        self.amendFaultSubmitWebService = nil;
        
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLAmendFaultSubmitWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLAmendFaultSubmitWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}



@end
