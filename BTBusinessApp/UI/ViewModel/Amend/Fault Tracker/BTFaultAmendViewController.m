//
//  BTFaultAmendViewController.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultAmendViewController.h"
#import "BTAmendEngineerViewController.h"
#import "BTOrderSiteContactsViewController.h"
#import "DLMAmendFaultScreen.h"
#import "BTFaultSiteContactViewController.h"
#import "BTFaultAppointmentDetail.h"
#import "BTFaultCallDiversionViewController.h"
#import "BTFaultAmendEngineerViewController.h"
#import "BTFaultSiteContactContainerVC.h"
#import "AppConstants.h"
#import "BTFaultAccessTimeSlotViewController.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "UIViewController+WebServiceErrorHandling.h"


@interface BTFaultAmendViewController ()<BTAmendEngineerViewControllerDelegate, DLMAmendFaultScreenDelegate, BTAddSiteContactTableViewControllerDelegare,BTFaultCallDiversionViewControllerDelegate, BTFaultSiteContactContainerVCDelegate, BTFaultAccessTimeSlotViewControllerDelegate>{
    
    
    BTFaultAppointmentDetail *_appointmentDetail;
    
    // (LP) Using to send back updated appointment details to fault details screen
    BTFaultAppointmentDetail *_updatedAppointmentDetail;
    BTFaultAppointment *_currentSelectedEngineerApointment;
    
    BTFaultContactDetails *_previousContactDetail;
    BTFaultContactDetails *_currentSelectedContactDetail;
    
    BTCallDiversionDetail *_previousCallDiversionDetail;
    BTCallDiversionDetail *_currentSelectedCallDiversionDetail;
    
    DLMAmendFaultScreen *_amendFaultModel;
    
    
    //For Access Time Slot
    NSDate *_selectedAppointmentDate;
    NSString *_selectedEarliestTimeSlot;
    NSString *_selectedLatestTimeSlot;
    
    //Used in Omniture (RLM)
    BOOL _siteContactDataChanged;
    BOOL _enggAppointmentDataChanged;
    BOOL _callDiversionDataChanged;
}


@end

@implementation BTFaultAmendViewController

+ (BTFaultAmendViewController *)getBTFaultAmendViewController{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTFaultAmendViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTFaultAmendViewControllerID"];
    return vc;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
    
    //Create data
    _amendFaultModel = [[DLMAmendFaultScreen alloc] init];
    _amendFaultModel.amendFaultScreenDelegate = self;
    
    [self makeAmendCheckAPICall];
    
    [self trackOmniPage:OMNIPAGE_FAULT_CHANGE_FAULT_DETAILS];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Super Class Method

//Method to cancell in-progress APIs
- (void)cancelInProgressAPI{
    
    [_amendFaultModel callDiversionDetail];
    
}

- (void)closeButtonPressed:(id)sender{
    
    if(self.isDataChanged){
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"Are you sure you want to cancel? You haven't saved any changes." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self.navigationController popViewControllerAnimated:YES];
        }];
        
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:^{}];
        
        
    }
    else{
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}


- (void)confirmButtonPressed:(id)sender{
    
    
    if(self.isDataChanged){
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"Are you sure you want to make the changes?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self submitAmendToServer];
        }];
        
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:^{}];
        
    }
    else{
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}


- (void)setDataChangeStatus{
    
    BOOL isDataChanged = NO;
    
    
    if(_currentSelectedEngineerApointment){
        
        isDataChanged = YES;
        
        if([_currentSelectedEngineerApointment.dateInfo compare:_appointmentDetail.bookedEndDate] == NSOrderedSame){
            
            isDataChanged = NO;
        }
        
        [_updatedAppointmentDetail updateBookedEndDateWithNSDate:_currentSelectedEngineerApointment.dateInfo];
        
    }
    
    if(_selectedEarliestTimeSlot){
        
        if(![_selectedEarliestTimeSlot isEqualToString:_appointmentDetail.earlierAccessTime]){
            
            isDataChanged = YES;
            [_updatedAppointmentDetail updateearlierAccessTimeWithString:_selectedEarliestTimeSlot];
        }
    }
    
    
    
    if(_selectedLatestTimeSlot){
        
        if(![_selectedLatestTimeSlot isEqualToString:_appointmentDetail.latestAccessTime]){
            
            isDataChanged = YES;
            [_updatedAppointmentDetail updateLatestAccessTimeWithString:_selectedLatestTimeSlot];
        }
    }
    
    
    if(_selectedAppointmentDate){
        
        if(([_selectedAppointmentDate compare:_appointmentDetail.bookedStartDate] != NSOrderedSame)){
            
            isDataChanged = YES;
            [_updatedAppointmentDetail updateBookedStartDateWithNSDate:_selectedAppointmentDate];
        }
    }
    
    
    if(_currentSelectedContactDetail && _previousContactDetail){
        
        if(![_currentSelectedContactDetail.reporterSameasSite isEqualToString:_previousContactDetail.reporterSameasSite]){
            
            isDataChanged = YES;
        }
        else if(![_currentSelectedContactDetail.reporterContacts.contactKey isEqualToString:_previousContactDetail.reporterContacts.contactKey]){
            
            isDataChanged = YES;
        }
        else if(![_currentSelectedContactDetail.siteContacts.contactKey isEqualToString:_previousContactDetail.siteContacts.contactKey]){
            
            isDataChanged = YES;
        }
        else if(![self compareSiteContact:_currentSelectedContactDetail.reporterContacts andSiteContact:_previousContactDetail.reporterContacts]){
            
            isDataChanged = YES;
        }
        else if([self compareSiteContact:_currentSelectedContactDetail.siteContacts andSiteContact:_previousContactDetail.siteContacts]){
            
            isDataChanged = YES;
        }
    }
    
    if(_currentSelectedCallDiversionDetail && _previousCallDiversionDetail){
        
        if(![_currentSelectedCallDiversionDetail.callDivertFlag isEqualToString:_previousCallDiversionDetail.callDivertFlag]){
            
            isDataChanged = YES;
        }
        else if(![_currentSelectedCallDiversionDetail.callDivertNumber isEqualToString:_previousCallDiversionDetail.callDivertNumber]){
            
            isDataChanged = YES;
        }
        else if(!([_currentSelectedCallDiversionDetail.callDivertDate compare:_previousCallDiversionDetail.callDivertDate] == NSOrderedSame)){
            
            isDataChanged = YES;
        }
    }
    
    self.isDataChanged = isDataChanged;
    
}


- (BOOL)compareSiteContact:(BTOrderSiteContactModel *)firstSiteContact andSiteContact:(BTOrderSiteContactModel *)secondSiteContact{
    
    
    BOOL areSame = YES;
    
    if(firstSiteContact && secondSiteContact){
        
        if(firstSiteContact.phone && ![firstSiteContact.phone isEqualToString:secondSiteContact.phone]){
            
            areSame = NO;
        }
        else if(![firstSiteContact.mobile isEqualToString:secondSiteContact.mobile]){
            
            areSame = NO;
        }
        else if(![firstSiteContact.email isEqualToString:secondSiteContact.email]){
            
            areSame = NO;
        }
        else if(![[firstSiteContact.preferredContact lowercaseString] isEqualToString:[secondSiteContact.preferredContact lowercaseString]]){
            
            areSame = NO;
        }
    }
    
    return areSame;
}



//Method to Create the actual cell for particular index path
- (UITableViewCell *)getCellForIndexPath:(NSIndexPath *)indexPath{
    
    AmendTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kAmendTableViewCellID];
    [cell updateWithAmend:[_dataArray objectAtIndex:indexPath.row]];
    return cell;
}



//Returns the screen name, appeared on navigation bar
- (NSString *)getScreenTitle{
    
    return @"Change fault details";
}


//This Method is called on tableview cell selection
- (void)cellSelectedAtIndexPath:(NSIndexPath *)indexPath{
    
    
    AmendViewModel *amendModel = [_dataArray objectAtIndex:indexPath.row];
    
    if([amendModel.amendContext isEqualToString:kEngineerAppointmentText]){
        
        
        [self trackOmniClick:OMNICLICK_FAULT_ENGG_APPT forPage:OMNIPAGE_FAULT_CHANGE_FAULT_DETAILS];
        
        if(_appointmentDetail.isHourAccessPresent){
            
            //AccessTime Slot
            BTFaultAccessTimeSlotViewController *vc = [BTFaultAccessTimeSlotViewController getBTFaultAccessTimeSlotViewController];
            vc.delegate = self;
            vc.faultAppointmentDetail = _appointmentDetail;
            vc.faultReference = self.faultRefrence;
            [self.navigationController pushViewController:vc animated:YES];
            
        }
        else{
            
            BTFaultAmendEngineerViewController *vc = [BTFaultAmendEngineerViewController getBTFaultAmendEngineerViewController];
            vc.ammendFaultArray = _appointmentDetail.faultAppointmentsArray;
            vc.appointmendDate = _appointmentDetail.bookedEndDate;
            vc.delegate = self;
            vc.itemRefNumber = self.faultRefrence;
            [self.navigationController pushViewController:vc animated:YES];
            
        }
    }
    
    else if([amendModel.amendContext isEqualToString:kSiteContactText]){
        
        //Opening Site Contacts Screen
        [self trackOmniClick:OMNICLICK_FAULT_SITE_CONTACTS forPage:OMNIPAGE_FAULT_CHANGE_FAULT_DETAILS];
        BTFaultSiteContactContainerVC *viewController =  [[BTFaultSiteContactContainerVC alloc] init];
        viewController.view.backgroundColor = [UIColor whiteColor];
        viewController.view.frame = CGRectMake(0,
                                               0,
                                               kScreenSize.width,
                                               kScreenSize.height);
        viewController.delegate = self;
        viewController.contactDetail = _currentSelectedContactDetail;
        [self.navigationController pushViewController:viewController animated:YES];
        
        /*
         BTFaultSiteContactViewController *vc = [BTFaultSiteContactViewController getBTFaultSiteContactViewController];
         vc.contactDetail = _currentSelectedContactDetail;
         vc.delegate = self;
         [self.navigationController pushViewController:vc animated:YES];
         */
    }
    else if([amendModel.amendContext isEqualToString:kCallDiversionText]){
        
        [self trackOmniClick:OMNICLICK_FAULT_CALL_DIVERSION forPage:OMNIPAGE_FAULT_CHANGE_FAULT_DETAILS];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        BTFaultCallDiversionViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTFaultCallDiversionViewController"];
        vc.delegate = self;
        vc.callDiversionDetail = _currentSelectedCallDiversionDetail;
        [self.navigationController pushViewController:vc animated:YES];
        
        
       
        
    }
    
    
}





#pragma mark- Helper Method

- (void)makeAmendCheckAPICall{
    
    if(![AppManager isInternetConnectionAvailable]){
        
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_FAULT_CHANGE_FAULT_DETAILS];
        return;
    }

    [self hideRetryView];
    
    self.networkRequestInProgress = YES;
    [_amendFaultModel fetchAmendFaultDataWithFaultRef:_faultRefrence];
    
}


- (void)populateData{
    
    NSMutableArray *amendmentArray = [NSMutableArray array];
    
    //Engineer appointment
    if(_currentSelectedEngineerApointment){
        
        AmendViewModel *enginnerAppointmentModel = [_amendFaultModel getAmendModelForAppointment:_currentSelectedEngineerApointment];
        
        if(enginnerAppointmentModel){
            
            [amendmentArray addObject:enginnerAppointmentModel];
        }
    }
    else if(_appointmentDetail){
        
        NSDate *appointmentDate = nil;
        
        if(_appointmentDetail.isHourAccessPresent){
            
            if(_selectedAppointmentDate){
                
                appointmentDate = _selectedAppointmentDate;
            }
            else{
                
                appointmentDate = _appointmentDetail.bookedStartDate;
            }
        }
        else{
            
            appointmentDate = _appointmentDetail.bookedEndDate;
        }
        
        AmendViewModel *enginnerAppointmentModel = [_amendFaultModel getAmendModelForAppointmentDate:appointmentDate];
        
        if(enginnerAppointmentModel){
            
            if(_appointmentDetail.isHourAccessPresent){
                
                //Showing access slot time
                [_amendFaultModel addTimeForEarliestTime:_selectedEarliestTimeSlot latestTime:_selectedLatestTimeSlot toAmendViewModel:enginnerAppointmentModel];
                
            }
            
            [amendmentArray addObject:enginnerAppointmentModel];
        }
    }
    
    
    //Site Contact
    if(_currentSelectedContactDetail){
        
        AmendViewModel *siteContactModel = [_amendFaultModel getamendModelForContactDetail:_currentSelectedContactDetail];
        
        if(siteContactModel){
            
            [amendmentArray addObject:siteContactModel];
        }
    }
    
    
    //Call Diversion
    if(_currentSelectedCallDiversionDetail){
        
        AmendViewModel *callDiversionModel = [_amendFaultModel getamendModelForCallDiversionDetail:_currentSelectedCallDiversionDetail];
        
        if(callDiversionModel){
            
            [amendmentArray addObject:callDiversionModel];
        }
    }
    
    _dataArray = amendmentArray;
    
    [self.tableView reloadData];
    
}


- (void)trackOmniClick:(NSString *)clickEvent forPage:(NSString *)page
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:[AppManager getOmniDictionary]];
    NSString *pageName = page;
    NSString *linkTitle = clickEvent;
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:params];
}

- (void)trackOmniPage:(NSString *)page
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:[AppManager getOmniDictionary]];
    
    [OmnitureManager trackPage:page withContextInfo:params];
}


- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)messageText{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:messageText preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:^{}];
}

- (void)handleUnknownErrors{
    
    [self showAlertWithTitle:@"Message" andMessage:@"Sorry, something has gone wrong. Please try again later."];
}

- (void)submitAmendToServer{
    
    
    if (![AppManager isInternetConnectionAvailable]) {
        
        [self showAlertWithTitle:kNoConnectionTitle andMessage:kNoConnectionMessage];
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_FAULT_CHANGE_FAULT_DETAILS];
        return;
    }
    
    
    self.networkRequestInProgress = YES;
    
    BOOL isDateChanged = NO;
    
    if(_currentSelectedEngineerApointment && [_currentSelectedEngineerApointment.dateInfo compare:_appointmentDetail.bookedEndDate] != NSOrderedSame){
        
        isDateChanged = YES;
    }
    
    
    if(_appointmentDetail.isHourAccessPresent){
        
        if(_selectedEarliestTimeSlot){
            
            if(![_selectedEarliestTimeSlot isEqualToString:_appointmentDetail.earlierAccessTime]){
                
                isDateChanged = YES;
            }
        }
        
        
        if(_selectedLatestTimeSlot){
            
            if(![_selectedLatestTimeSlot isEqualToString:_appointmentDetail.latestAccessTime]){
                
                isDateChanged = YES;
            }
        }
        
        
        if(_selectedAppointmentDate){
            
            if(([_selectedAppointmentDate compare:_appointmentDetail.bookedStartDate] != NSOrderedSame)){
                
                isDateChanged = YES;
            }
        }
    }
    
    
    NSDictionary *amendAppointmentDict = nil;
    
    if(_appointmentDetail.isHourAccessPresent){
        
        amendAppointmentDict = [_amendFaultModel amendAppointmentDictionaryForAppointmentDate:_selectedAppointmentDate slotEarliestTime:_selectedEarliestTimeSlot slotLatestTime:_selectedLatestTimeSlot andIsAppointmentChanged:isDateChanged];
    }
    else{
        
        amendAppointmentDict = [_amendFaultModel amendAppointmentDictionaryForAppointment:_currentSelectedEngineerApointment isAppointmentChanged:isDateChanged];
    }
    
    
    
    if([[_previousCallDiversionDetail.callDivertFlag lowercaseString] isEqualToString:@"modify"] && [[_currentSelectedCallDiversionDetail.callDivertFlag lowercaseString] isEqualToString:@"add"]){
        
        [_currentSelectedCallDiversionDetail setDivertFlag:@"Delete"];
        
    }
    
    [_amendFaultModel submitAndMakeAmendFaultToServerWithNewContactDetails:_currentSelectedContactDetail appointmentDict:amendAppointmentDict andCallDiversionDetails:_currentSelectedCallDiversionDetail];
    
    /// [_amendFaultModel submitAndMakeAmendFaultToServerWithNewContactDetails:_currentSelectedContactDetail Appointment:_currentSelectedEngineerApointment isAppointmentDateChanged:isDateChanged andCallDiversionDetails:_currentSelectedCallDiversionDetail];

}

#pragma mark - BTFaultAmendEngineer Delegate

- (void)btAmendEngineerViewController:(BTAmendEngineerViewController *)controller didSelectFaultAppointment:(BTFaultAppointment *)faultAppointment
{
    _currentSelectedEngineerApointment = faultAppointment;
    [self setDataChangeStatus];
    _enggAppointmentDataChanged = YES;
    [self populateData];
}


#pragma mark - RetryView Delegate

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    
    if(!_dataArray || [_dataArray count] < 1){
        
        [self makeAmendCheckAPICall];
    }
    else{
        
        [self submitAmendToServer];
    }
    
}



#pragma mark- BTOrderSiteContactsViewControllerDelegate Method


- (void)btFaultSiteContactContainerVC:(BTFaultSiteContactContainerVC *)controller didSubmitContactDetail:(BTFaultContactDetails *)contactDetail{
    
    _currentSelectedContactDetail = contactDetail;
    _siteContactDataChanged = YES;
    [self setDataChangeStatus];
    [self populateData];
    
}


#pragma mark- BTAmendEngineerViewControllerDelegate Method
- (void)btAmendEngineerViewController:(BTAmendEngineerViewController *)controller didSelectAppointment:(BTAppointment *)appointment{
    
    //_currentSelectedEngineerApointment = appointment;
    [self setDataChangeStatus];
    
}



#pragma mark- BTFaultAccessTimeSlotViewControllerDelegate Method

- (void)btFaultAccessTimeSlotViewController:(BTFaultAccessTimeSlotViewController *)controller didSelectDate:(NSDate *)date earliarTimeSlot:(NSString *)earlierTimeSlot andLatestTimeSlot:(NSString *)latestTimeSlot{
    
    _selectedAppointmentDate = date;
    _selectedEarliestTimeSlot = earlierTimeSlot;
    _selectedLatestTimeSlot = latestTimeSlot;
    _enggAppointmentDataChanged = YES;
    [self populateData];
    [self setDataChangeStatus];
}



#pragma mark- DLMAmendFaultScreenDelegate

- (void)successfullyFetchedAmendFaultDataOnAmendFaultScreen:(DLMAmendFaultScreen *)amendFaultScreen{
    
    
    [self performSelector:@selector(removeProgress) withObject:nil afterDelay:0.001];
    
    _appointmentDetail = amendFaultScreen.faultAppointmentDetail;
    _updatedAppointmentDetail = [_appointmentDetail copy];
    
    _previousContactDetail = amendFaultScreen.contactDetails;
    _currentSelectedContactDetail = _previousContactDetail;
    
    _previousCallDiversionDetail = _amendFaultModel.callDiversionDetail;
    _currentSelectedCallDiversionDetail = _previousCallDiversionDetail;
    
    [self populateData];
    
    
    
}

- (void)removeProgress
{
    self.networkRequestInProgress = NO;
}

- (void)amendFaultScreen:(DLMAmendFaultScreen *)amendFaultScreen failedToFetchAmendFaultDataWithWebServiceError:(NLWebServiceError *)error{
    
    [self handleWebServiceError:error];
    [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_FAULT_CHANGE_FAULT_DETAILS];
    
}

- (void)amendSuccessfullyFinishedByAmmendFaultScreen:(DLMAmendFaultScreen *)ammendScreen
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.networkRequestInProgress = NO;
    });
    
    [self trackKeyTaskForFault];
    
    if (_enggAppointmentDataChanged)
    {
        [self.faultAmendDelegate bTFaultAmendSubmitSuccessfullyOnFaultAmendViewController:self isEngineerAppointmentChanged:YES andAppointmentDetail:_updatedAppointmentDetail];
    }
    else
    {
        [self.faultAmendDelegate bTFaultAmendSubmitSuccessfullyOnFaultAmendViewController:self isEngineerAppointmentChanged:NO andAppointmentDetail:nil];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)ammendFaultScreen:(DLMAmendFaultScreen *)ammendScreen failedToSubmitAmendFaultWithWebServiceError:(NLWebServiceError *)error {
    
    self.networkRequestInProgress = NO;
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if(errorHandled == NO)
    {
        [self showAlertWithTitle:@"Failure" andMessage:@"Sorry, that change hasn't worked, Please try again later."];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_FAULT_CHANGE_FAULT_DETAILS];
    }
    
}

#pragma mark - BTFaultCallDiversionViewControllerDelegate

- (void)userUpdatedFaultCallDiversionDetailOnCallDiversionScreen:(BTFaultCallDiversionViewController *)faultCallDiversion withDiversionDetailObj:(BTCallDiversionDetail *)callDiversionDetail
{
    
    _currentSelectedCallDiversionDetail = callDiversionDetail;
    [self setDataChangeStatus];
    [self populateData];
    _callDiversionDataChanged = YES;
}


#pragma mark - Omnitutre event tracking
- (void)trackKeyTaskForFault
{
    if(_enggAppointmentDataChanged){
        [self trackPageWithKeyTask:OMNI_KEYTASK_FAULT_ENGG_APPOINTMENT_CHANGED];
    }
    
    if(_callDiversionDataChanged){
        [self trackPageWithKeyTask:OMNI_KEYTASK_FAULT_DETAILS_CALL_DIVERSION_CHANGED];
        
    }
    
    if(_siteContactDataChanged){
        
        [self trackPageWithKeyTask:OMNI_KEYTASK_FAULT_DETAILS_SITE_CONTACT_CHANGED];
    }

}


- (void)trackPageWithKeyTask:(NSString *)keytask
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = OMNIPAGE_FAULT_CHANGE_FAULT_DETAILS;
    NSString *faultRef =  [NSString stringWithFormat:@"%@%@",kOmniFaultRefFormat,_faultRefrence];;
    [data setValue:keytask forKey:kOmniKeyTask];
    [data setValue:faultRef forKey:kOmniFaultRef];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}


@end
