//
//  DLMFaultAppointmentScreen.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 14/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
#import "BTFaultAppointmentSlotDetail.h"
#import "NLFaultAppointmentSlotWebService.h"

@class DLMFaultAppointmentSlotScreen;

@protocol DLMFaultAppointmentSlotScreenDelegate <NSObject>

- (void)successfullyFetchedFaultAppointmentSlotScreen:(DLMFaultAppointmentSlotScreen *)faultAppointmentDetailScreen withFaultAppointmentDetail:(BTFaultAppointmentSlotDetail *)faultAppointmentDetail;

- (void)faultAppointmentSlotScreen:(DLMFaultAppointmentSlotScreen *)faultAppointmentSlotScreen failedToFetchFaultAppointmentSlotWithWebServiceError:(NLWebServiceError *)webServiceError;

@end



@interface DLMFaultAppointmentSlotScreen : DLMObject

@property (nonatomic, strong) BTFaultAppointmentSlotDetail *appointmentsSlotDetail;

@property (nonatomic, weak) id<DLMFaultAppointmentSlotScreenDelegate> faultAppointmentSlotScreenDelegate;

- (void)fetchFaultAppointmentSlotForFaultReference:(NSString *)faultRef andInputDate:(NSString *)date;
- (void)cancelFetchFaultAppointmentAPIRequest;

@end
