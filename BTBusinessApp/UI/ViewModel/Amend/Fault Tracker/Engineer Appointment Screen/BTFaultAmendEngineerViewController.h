//
//  BTFaultAmendEngineerViewController.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/14/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTAmendEngineerViewController.h"


@interface BTFaultAmendEngineerViewController : BTAmendEngineerViewController


@property (nonatomic, strong) NSArray *ammendFaultArray;
@property (nonatomic, copy) NSString *itemRefNumber;
@property (nonatomic, strong) BTFaultAppointment *faultAppointment;

+ (BTFaultAmendEngineerViewController *)getBTFaultAmendEngineerViewController;

@end
