//
//  DLMFaultAppointmentScreen.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 14/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMFaultAppointmentSlotScreen.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "NLFaultAppointmentSlotWebService.h"
#import "NLWebServiceError.h"

@interface DLMFaultAppointmentSlotScreen()<NLFaultAppointmentSlotWebServiceDelegate>

@property (nonatomic) NLFaultAppointmentSlotWebService *faultAppointmentSlotWebService;

@end

@implementation DLMFaultAppointmentSlotScreen


#pragma mark - Public Methods

- (void)fetchFaultAppointmentSlotForFaultReference:(NSString *)faultRef andInputDate:(NSString *)date
{
    self.faultAppointmentSlotWebService.getfaultAppointmentSlotWebServiceDelegateDelegate = nil;
    self.faultAppointmentSlotWebService = [[NLFaultAppointmentSlotWebService alloc] initWithFaultID:faultRef andInputDate:date];
    self.faultAppointmentSlotWebService.getfaultAppointmentSlotWebServiceDelegateDelegate = self;
    [self.faultAppointmentSlotWebService resume];

}

- (void)cancelFetchFaultAppointmentAPIRequest {

    self.faultAppointmentSlotWebService.getfaultAppointmentSlotWebServiceDelegateDelegate = nil;
    [self.faultAppointmentSlotWebService cancel];
    self.faultAppointmentSlotWebService = nil;
}


#pragma mark - NLFaultAppointmentSlotWebServiceDelegate

- (void)faultAppointmentSlotWebService:(NLFaultAppointmentSlotWebService *)webService successfullyFetchedFaultAppointmentSlotWebServiceData:(BTFaultAppointmentSlotDetail *)faultAppointmentSlotDetail
{
    if(webService == _faultAppointmentSlotWebService)
    {
        self.appointmentsSlotDetail = faultAppointmentSlotDetail;

        [self.faultAppointmentSlotScreenDelegate successfullyFetchedFaultAppointmentSlotScreen:self withFaultAppointmentDetail:faultAppointmentSlotDetail];

        _faultAppointmentSlotWebService.getfaultAppointmentSlotWebServiceDelegateDelegate = nil;
        _faultAppointmentSlotWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLFaultAppointmentSlotWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLFaultAppointmentSlotWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)faultAppointmentWebService:(NLFaultAppointmentSlotWebService *)webService failedToFetchFaultAppointmentSlotWebServiceDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    if(webService == _faultAppointmentSlotWebService)
    {
        [self.faultAppointmentSlotScreenDelegate faultAppointmentSlotScreen:self failedToFetchFaultAppointmentSlotWithWebServiceError:webServiceError];

        _faultAppointmentSlotWebService.getfaultAppointmentSlotWebServiceDelegateDelegate = nil;
        _faultAppointmentSlotWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLFaultAppointmentSlotWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLFaultAppointmentSlotWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}





@end
