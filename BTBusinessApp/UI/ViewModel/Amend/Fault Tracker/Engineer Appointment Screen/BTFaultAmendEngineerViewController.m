//
//  BTFaultAmendEngineerViewController.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/14/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultAmendEngineerViewController.h"

#import "BTAmendEngineerViewController.h"
#import "AmendEngineerTableViewCell.h"
#import "CustomSpinnerAnimationView.h"
#import "DLMFaultAppointmentSlotScreen.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "AppConstants.h"
#import "BTAmendFooterActionView.h"
#import "BTNeedHelpViewController.h"
#import "AppManager.h"
#import "OmnitureManager.h"
#import "NLWebServiceError.h"
#import "UIViewController+WebServiceErrorHandling.h"

#define kAmendEngineerTableViewCell @"AmendEngineerTableViewCell"
#define kAmendEngineerTableViewCellID @"AmendEngineerTableViewCellID"

#define kAppointmentMaxSize 90


@interface BTFaultAmendEngineerViewController ()<UITableViewDelegate,UITableViewDataSource,DLMFaultAppointmentSlotScreenDelegate,AmendEngineerTableViewCellDelegate>

{
    BTFaultAppointment *_selectedModel;
    DLMFaultAppointmentSlotScreen *appointmentModel;
    
    BTFaultAppointment *_previousAppointment;
    BTFaultAppointment *_currentSelectedAppointment;
    
    BTAmendFooterActionView *f;

    UIView *_retryFooterView;
}
@property (assign, nonatomic) BOOL isLoading;
@property (assign, nonatomic) BOOL hasMoreData;


@end

@implementation BTFaultAmendEngineerViewController

- (void)viewDidLoad {
    self.isFaultAccessed = YES;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self trackOmniPage:OMNIPAGE_FAULT_AMMEND_ENGG_APPOINTMENT];
    [self trackPageWithKeyTask:OMNI_KEYTASK_FAULT_ENGINEER_APPOINTMENT_CHECKED];
    
    appointmentModel = [[DLMFaultAppointmentSlotScreen alloc] init];
    appointmentModel.faultAppointmentSlotScreenDelegate = self;
    
    self.title = @"Engineer appointment";
    _dataArray = [[NSMutableArray alloc] init];
    [_dataArray addObjectsFromArray:self.ammendFaultArray];
    
    if([_dataArray count]==0)
    {
        [self showZeroStateView];
    }
    else{
        
        [self updateSlotAsPerCurrentAppointment];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark- Class Method

+ (BTFaultAmendEngineerViewController *)getBTFaultAmendEngineerViewController{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTFaultAmendEngineerViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTFaultAmendEngineerViewControllerID"];
    
    return vc;
}


#pragma mark- Super Class Methods

- (void)hideProgressView{
    
    [super hideProgressView];
    //_amendFooterActionView.hidden = NO;
}


- (void)saveButtonPressed:(id)sender{
    
   
    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
    
   
    NSString *dateFormat = @"dd/MM/yyyy";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    NSString *dateString = [dateFormatter stringFromDate:_currentSelectedAppointment.dateInfo];
    [tempDict setValue:dateString forKeyPath:@"slotDate"];
    [tempDict setValue:@(_currentSelectedAppointment.AMSlot) forKey:@"AMSlot"];
    [tempDict setValue:@(_currentSelectedAppointment.PMSlot) forKey:@"PMSlot"];
    [tempDict setValue:_currentSelectedAppointment.morningDateShift forKey:@"slotMorningDateShift"];
    [tempDict setValue:_currentSelectedAppointment.eveningDateShift forKey:@"slotEveningDateShift"];
    [tempDict setValue:[NSNumber numberWithBool:_currentSelectedAppointment.isAmSelected] forKey:@"isAMSelected"];
    [tempDict setValue:[NSNumber numberWithBool:_currentSelectedAppointment.isPmSelected] forKey:@"isPMSelected"];
    
    
    BTFaultAppointment *selectedFaultAppointment = [[BTFaultAppointment alloc] initWithResponseDictionaryFromFaultAppointmentsAPIResponse:tempDict];

    [self.delegate btAmendEngineerViewController:self didSelectFaultAppointment:selectedFaultAppointment];
    
    _currentSelectedAppointment.isAmSelected = NO;
    _currentSelectedAppointment.isPmSelected = NO;
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)closeButtonPressed:(id)sender{
    
    _currentSelectedAppointment.isAmSelected = NO;
    _currentSelectedAppointment.isPmSelected = NO;
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark- UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if(!_dataArray || [_dataArray count] == 0){
        
        return 0;
    }
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_dataArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AmendEngineerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAmendEngineerTableViewCellID];
    cell.delegate = self;
    
    BTFaultAppointment *faultAppointmentObj = [_dataArray objectAtIndex:indexPath.row];
    [cell updateWithBTFaultEngineerAppointmentSlotModel:faultAppointmentObj];
    
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view = [[[NSBundle mainBundle] loadNibNamed:@"AmendEngineerTableSectionHeaderView" owner:self options:nil] firstObject];
    
    return view;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 36.0;
}




#pragma mark - Helper Methods



- (void)updateSlotAsPerCurrentAppointment{


    //Updating slot as per current appointment

    NSDate *currentAppointmentDate = self.appointmendDate;

    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger comps = (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay);
    
    NSDateComponents *currAppointmentComponent = [calendar components:comps
                                                             fromDate:currentAppointmentDate];

    BTFaultAppointment *appointmentNeedToBeExcluded = nil;
    

    for(BTFaultAppointment *appointment in _dataArray){

        NSDateComponents *appointmentComponent = [calendar components:comps
                                                             fromDate:appointment.dateInfo];
        
        NSDateComponents *currDateComponent = [calendar components:comps
                                                                 fromDate:[NSDate date]];

        NSDate *appointmentDate = [calendar dateFromComponents:appointmentComponent];
        NSDate *currentAppointmentDate = [calendar dateFromComponents:currAppointmentComponent];
        NSDate *currentDate = [calendar dateFromComponents:currDateComponent];

        
        NSComparisonResult result1 = [appointmentDate compare:currentDate];
        
        
        
//        if(result1 == NSOrderedSame){
//            
//            NSInteger hour = [AppManager getHourFromDate:self.appointmendDate];
//            NSInteger currentHour = [AppManager getHourFromDate:[NSDate date]];
//            
//            //Dates are same
//            if (hour < 13) {
//                //Current appointment with AM
//                
//                [appointment setAMSlot:NO];
//                
//                
//            } else{
//                
//                appointmentNeedToBeExcluded = appointment;
//            }
//
//        }
        
         if(result1 == NSOrderedSame ){
            
             NSInteger currentHour = [AppManager getHourFromDate:[NSDate date]];
            
             if( currentHour < 13) {
                 NSComparisonResult result2 = [currentDate compare:currentAppointmentDate];
                 if(result2 == NSOrderedSame)
                 {
                     NSInteger hour = [AppManager getHourFromDate:self.appointmendDate];
                     if(hour >= 13 ){
                         appointmentNeedToBeExcluded = appointment;
                     }
                 }
                 [appointment setAMSlot:NO];
                 
             } else {
                  appointmentNeedToBeExcluded = appointment;
             }
             
         }

    }


    if(appointmentNeedToBeExcluded){

        [_dataArray removeObject:appointmentNeedToBeExcluded];
    }
    
    
    
}



- (void)showZeroStateView
{
    UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _tableView.bounds.size.width-20, _tableView.bounds.size.height)];
    noDataLabel.text             = @"NO ENGINEER APPOINTMENT SLOTS FOUND";
    noDataLabel.numberOfLines = 3;
    noDataLabel.font = [UIFont fontWithName:@"BTFont-Bold" size:16];
    noDataLabel.textColor        = [UIColor lightGrayColor];
    noDataLabel.textAlignment    = NSTextAlignmentCenter;
    _tableView.backgroundView = noDataLabel;
    _tableView.bounces = NO;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}



- (void)makeAPICall{
    
    
    if(![AppManager isInternetConnectionAvailable]){
        
        
        if([_dataArray count] <= 0){
            
            [self showRetryViewWithInternetStrip:YES];
        }
        
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_FAULT_AMMEND_ENGG_APPOINTMENT];
        return;
    }
    
    [self hideRetryView];
    
    if([_dataArray count] <= 0){
        
        self.networkRequestInProgress = YES;
    }
    
    
    NSString *date = @"";
    
    BTFaultAppointment *appointment = [_dataArray lastObject];
    
    NSDate *lastDate = appointment.dateInfo;

    //Compare
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setDay:1];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *nextToLastDate = [calendar dateByAddingComponents:dateComponents toDate:lastDate options:0];
    date = [self getFormatedDateFromDate:nextToLastDate];
    
    [appointmentModel fetchFaultAppointmentSlotForFaultReference:self.itemRefNumber andInputDate:date];
}


- (void)setUpDataChangeStatus{
    
    if(_currentSelectedAppointment){
       
        self.isDataChanged = YES;
    }
    else{
        
         self.isDataChanged = NO;
    }
}


- (void)createFooterView{
    
    footerView = [[CustomSpinnerAnimationView alloc] initWithText:@"" withLoadingImage:[UIImage imageNamed:@"Spinner"]];
    
    footerView.translatesAutoresizingMaskIntoConstraints = NO;
    [footerView addConstraint:[NSLayoutConstraint constraintWithItem:footerView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:155.0]];
    [footerView addConstraint:[NSLayoutConstraint constraintWithItem:footerView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:100.0]];
    footerView.backgroundColor = [UIColor clearColor];
    [_tableView setNeedsLayout];
}


- (void)loadMore{
    
    [self makeAPICall];
}


- (void)showProgressView{
    
    //  [super showProgressView];
    //_amendFooterActionView.hidden = YES;
}


- (NSString *)getFormatedDateFromDate:(NSDate *)date{
    
    NSString *dateFormat = @"yyyy-MM-dd";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
}


//State when no data to show
- (void)showZeroState{
    
    
    UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0,
                                                                             0,
                                                                             _tableView.bounds.size.width,
                                                                             _tableView.bounds.size.height)];
    noDataLabel.text             = @"NO ENGINEER APPOINTMENT SLOTS FOUND";
    noDataLabel.font = [UIFont fontWithName:kBtFontRegular size:15.0];
    noDataLabel.textColor        = [UIColor blackColor];
    noDataLabel.textAlignment    = NSTextAlignmentCenter;
    _tableView.backgroundView = noDataLabel;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.scrollEnabled = NO;
}

- (void)hideZeroState{
    
    _tableView.backgroundView = nil;
    _tableView.scrollEnabled = YES;
}

- (NSDate *)getDateOnlyFromDate:(NSDate *)inputDate{
    
    unsigned int flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:flags fromDate:inputDate];
    return [calendar dateFromComponents:components];
}


- (NSArray *)getFilteredArray:(NSArray *)inputArray{
    
    NSMutableArray *inputSlotsArray = [NSMutableArray arrayWithArray:inputArray];
    NSMutableArray *slotsToBeExcluded = [NSMutableArray array];
    
    for(BTFaultAppointment *appointment in inputSlotsArray){
        
        
        NSDate *date1 = [self getDateOnlyFromDate:appointment.dateInfo];
        NSDate *date2 = [self getDateOnlyFromDate:self.maxDate];
        
        if(([date1 compare:date2] == NSOrderedDescending))
        {
            [slotsToBeExcluded addObject:appointment];
        }
    }
    
    [inputSlotsArray removeObjectsInArray:slotsToBeExcluded];
    return inputSlotsArray;
    
}


#pragma mark - AmendEngineerTableViewCellDelegate Methods

- (void)amendFaultEngineerTableViewCell:(AmendEngineerTableViewCell *)cell didToggleShiftForModel:(BTFaultAppointment *)model{
    
    
    
    if(_currentSelectedAppointment && [[_currentSelectedAppointment dateInfo] compare:model.dateInfo] != NSOrderedSame)
    {
        _currentSelectedAppointment.isAmSelected = NO;
        _currentSelectedAppointment.isPmSelected = NO;
        
    }
    
    _currentSelectedAppointment = model;
    
    [_tableView reloadData];
    
    [self setUpDataChangeStatus];
    
}

- (void)amendEngineerTableViewCell:(AmendEngineerTableViewCell *)cell didToggleShiftForModel:(BTAppointment *)model
{

}

- (void)userPressedOnBTAmendFooterActionView:(BTAmendFooterActionView *)view{

    
    BTNeedHelpViewController *helpViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"EngineerAppointmentNeedHelpViewController"];
    [self presentViewController:helpViewController animated:YES completion:nil];
}


#pragma mark - RetryView Delegate

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView{

    [self makeAPICall];
    
}

#pragma mark DLMAmmendEngineerAppointmentScreenDelegate methods

- (void)successfullyFetchedFaultAppointmentSlotScreen:(DLMFaultAppointmentSlotScreen *)faultAppointmentDetailScreen withFaultAppointmentDetail:(BTFaultAppointmentSlotDetail *)faultAppointmentDetail
{
    
    [self hideZeroState];
    
    if(faultAppointmentDetail && [faultAppointmentDetail.faultAppointmentSlotArray count] > 0){
        
        
        [_dataArray addObjectsFromArray:[self getFilteredArray:faultAppointmentDetail.faultAppointmentSlotArray]];
        
        
        /*
        for(BTFaultAppointment *appointment in faultAppointmentDetail.faultAppointmentSlotArray)
        {
            //Compare
            NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
            [dateComponents setMonth:3];
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:self.appointmendDate options:0];
            
            
            
            NSTimeInterval appointmentInterval = [self.appointmendDate timeIntervalSince1970];
            NSTimeInterval newDateTimeInterval = [newDate timeIntervalSince1970];
            NSTimeInterval serverDateTimeInterval = [appointment.dateInfo timeIntervalSince1970];
            
            if(!(serverDateTimeInterval <= newDateTimeInterval && serverDateTimeInterval > appointmentInterval))
            {
                [_dataArray removeObject:appointment];
            }

        }
        
        */
        
        [self updateSlotAsPerCurrentAppointment];
        [_tableView reloadData];
    }
    else if([_dataArray count] > 0){
        
        //We have some items but not getting more items from server
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"No further engineer appointment slots available." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:^{}];
        
    }
    else{
        
        //Show zero state
        [self showZeroState];
        
    }
    
    
    if([_dataArray count] >= kAppointmentMaxSize){
        
        self.hasMoreData = NO;
        
    }
    else{
        
        self.hasMoreData = YES;
    }
    
    
    
    if(self.hasMoreData){

        //Compare
        
        BTFaultAppointment *appointment = [_dataArray lastObject];
        
        NSDate *date1 = [self getDateOnlyFromDate:appointment.dateInfo];
        NSDate *date2 = [self getDateOnlyFromDate:self.maxDate];
        
        if(([date1 compare:date2] == NSOrderedAscending)) //
        {
            
        }
        else{
            
            self.hasMoreData = NO;
        }
        
    }
    
    
    
    self.networkRequestInProgress = NO;
    _tableView.tableFooterView = nil;
    self.isLoading = NO;
    
}


- (void)faultAppointmentSlotScreen:(DLMFaultAppointmentSlotScreen *)faultAppointmentSlotScreen failedToFetchFaultAppointmentSlotWithWebServiceError:(NLWebServiceError *)webServiceError {
    
    _tableView.tableFooterView = nil;
    self.networkRequestInProgress = NO;
    self.isLoading = NO;
    
    if(_dataArray && [_dataArray count] > 0){
        
        BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];

        
        if(errorHandled == NO)
        {
            [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_FAULT_AMMEND_ENGG_APPOINTMENT];
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Message" message:kDefaultErrorMessage preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:^{}];
        }
                
    }
    else
    {
        //If no data show retry view
        [self handleWebServiceError:webServiceError];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_FAULT_AMMEND_ENGG_APPOINTMENT];
    }
}



#pragma mark - Omniture Methods

- (void)trackOmniPage:(NSString *)page
{
    [OmnitureManager trackPage:page];
}

- (void)trackPageWithKeyTask:(NSString *)keytask
{
    NSMutableDictionary *contextDict = [NSMutableDictionary dictionary];
    
    if(self.itemRefNumber && self.itemRefNumber.length>0)
    {
        NSString *ref = [NSString stringWithFormat:@"%@%@",kOmniFaultRefFormat,self.itemRefNumber];
        [contextDict setValue:ref forKey:kOmniFaultRef];
    }
    
    NSString *pageName = OMNIPAGE_FAULT_AMMEND_ENGG_APPOINTMENT;
    [contextDict setValue:keytask forKey:kOmniKeyTask];
    [OmnitureManager trackPage:pageName withContextInfo:contextDict];
}

@end
