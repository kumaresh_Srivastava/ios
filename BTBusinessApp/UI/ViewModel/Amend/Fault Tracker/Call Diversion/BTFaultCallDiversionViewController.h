//
//  BTFaultCallDiversionViewController.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 09/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseViewController.h"
#import "BTCallDiversionDetail.h"

@class BTFaultCallDiversionViewController;

@protocol BTFaultCallDiversionViewControllerDelegate <NSObject>

- (void)userUpdatedFaultCallDiversionDetailOnCallDiversionScreen:(BTFaultCallDiversionViewController *)faultCallDiversion withDiversionDetailObj:(BTCallDiversionDetail *)callDiversionDetail;
@end

@interface BTFaultCallDiversionViewController : BTBaseViewController

@property (nonatomic, weak) id<BTFaultCallDiversionViewControllerDelegate> delegate;

@property (nonatomic, strong) BTCallDiversionDetail *callDiversionDetail;

@end
