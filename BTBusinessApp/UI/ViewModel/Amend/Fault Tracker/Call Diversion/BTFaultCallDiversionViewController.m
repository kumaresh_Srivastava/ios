//
//  BTFaultCallDiversionViewController.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 09/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultCallDiversionViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "AppConstants.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "UITextField+BTTextFieldProperties.h"

@interface BTFaultCallDiversionViewController ()<UITextFieldDelegate>
{
    UIImage *_selectedImage;
    UIImage *_unselectedImage;
    BOOL _isDivertButtonSelected;
    UIBarButtonItem *_rightBarButtonItem;
    BOOL _isDeviceLessThaniPhone5;
    NSString *_initialPhoneNumberString;
    BOOL _isDivertedSelectedInitially;
    BOOL _isNoDivertedSelectedInitially;
    BOOL _isKeyboardAppered;

}

@property (weak, nonatomic) IBOutlet UILabel *phoneNumberTextLabel;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *disclaimerTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *dontDivertCallButton;
@property (weak, nonatomic) IBOutlet UIButton *divertCallButton;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextfield;
@property (assign, nonatomic) BOOL saveButtonNeedToShow;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewTopConstraint;

@property (weak, nonatomic) IBOutlet UILabel *errorLabel;


@end

@implementation BTFaultCallDiversionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self trackOmniPage:OMNIPAGE_FAULT_CALL_DIVERSION];

    self.edgesForExtendedLayout = UIRectEdgeNone;

    //[SD] Initial UI Setup
    [self configureInitialUI];
    _isDeviceLessThaniPhone5 = YES;
    if([UIScreen mainScreen].bounds.size.height > 568)
        _isDeviceLessThaniPhone5 = NO;

    __weak typeof(self) weakSelf = self;
    [RACObserve(self, saveButtonNeedToShow) subscribeNext:^(NSNumber* state) {

        if (weakSelf.saveButtonNeedToShow) {
            [weakSelf updateSaveButtonWithStatus:YES];
        }
        else {
            [weakSelf updateSaveButtonWithStatus:NO];
        }

    }];

    //[SD] Keyboard Handling
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];

}


#pragma mark - Configure Initial UI

- (void)configureInitialUI
{
    //Hide the error Label
    self.errorLabel.hidden = YES;
    self.errorLabel.textColor = [UIColor redColor];

    //[SD] SetUp Navigation
    // _rightBarButtonItem = ;
    self.navigationItem.leftBarButtonItem = [self getCloseButtonItem];
    self.navigationItem.rightBarButtonItem = [self getSaveButtonItem];;
    self.title = @"Call diversion";

    //[SD] Initializing selected/unselectd radio images
    _selectedImage = [UIImage imageNamed:@"radio_selected"];
    _unselectedImage = [UIImage imageNamed:@"radio_unselected"];

    _initialPhoneNumberString = [NSString string];
    _isDivertButtonSelected = YES;
    if(self.callDiversionDetail.callDivertFlag && [[self.callDiversionDetail.callDivertFlag lowercaseString] isEqualToString:@"add"])
    {
        _isDivertButtonSelected = NO;
        _isNoDivertedSelectedInitially = YES;
    }
    else
    {
        _initialPhoneNumberString = self.callDiversionDetail.callDivertNumber;
        _isDivertedSelectedInitially = YES;
    }
    _initialPhoneNumberString = self.callDiversionDetail.callDivertNumber;
    [self updateButtonsStateWithDivertedButtonState:_isDivertButtonSelected];
    self.dontDivertCallButton.layer.borderWidth = 0.5;
    self.divertCallButton.layer.borderWidth = 0.5;
    self.dontDivertCallButton.layer.cornerRadius = 3.0;
    self.divertCallButton.layer.cornerRadius = 3.0;

    [self.phoneNumberTextfield getBTTextFieldStyle];
    self.phoneNumberTextfield.keyboardType = UIKeyboardTypePhonePad;

    //[SD] Initially Hiding Right Save Button on Navigation Bar
    self.navigationItem.rightBarButtonItem = nil;

}

#pragma mark - Overidden Methods

- (UIBarButtonItem *)getSaveButtonItem{

    UIBarButtonItem *saveButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonPressed:)];

    return saveButtonItem;
}


#pragma mark - Private helper methods

- (void)updateSaveButtonWithStatus:(BOOL)status
{
    if(status) {
        self.navigationItem.rightBarButtonItem =  [self getSaveButtonItem];
    }
    else {
        self.navigationItem.rightBarButtonItem = nil;
    }
}

- (BOOL)validatePhoneNumberWithText:(NSString *)text
{
    if ([text rangeOfString:kPhoneRegularExpression options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location == NSNotFound ) {
        return NO;
    }
    return YES;
}



- (void)updateButtonsStateWithDivertedButtonState:(BOOL)divertedButtonState
{
    if(divertedButtonState)
    {
        [self.dontDivertCallButton setImage:_unselectedImage forState:UIControlStateNormal];
        [self.divertCallButton setImage:_selectedImage forState:UIControlStateNormal];
        [self.dontDivertCallButton setBackgroundColor:[BrandColours colourBtNeutral30]];
        [self.divertCallButton setBackgroundColor:[UIColor whiteColor]];

        self.dontDivertCallButton.layer.borderColor =[UIColor whiteColor].CGColor;

        self.divertCallButton.layer.borderColor = [BrandColours colourBtNeutral60].CGColor;
        self.phoneNumberTextfield.hidden = NO;
        self.phoneNumberTextLabel.hidden = NO;
        self.phoneNumberTextfield.text = _initialPhoneNumberString;//self.callDiversionDetail.callDivertNumber;

    }
    else
    {
        [self.dontDivertCallButton setImage:_selectedImage forState:UIControlStateNormal];
        [self.divertCallButton setImage:_unselectedImage forState:UIControlStateNormal];
        [self.dontDivertCallButton setBackgroundColor:[UIColor whiteColor]];
        [self.divertCallButton setBackgroundColor:[BrandColours colourBtNeutral30]];

        self.dontDivertCallButton.layer.borderColor =[BrandColours colourBtNeutral30].CGColor;

        self.divertCallButton.layer.borderColor = [UIColor whiteColor].CGColor;

        self.phoneNumberTextfield.hidden = YES;
        self.phoneNumberTextLabel.hidden = YES;
    }


}


- (BOOL)validateInputWithString:(NSString *)aString
{
    NSString * const regularExpression = @"^((\\(?0\\d{9}\\)?)|(\\(?0\\d{5}\\)?\\s?\\d{5})|(\\(?0\\d{4}\\)?\\s?\\d{3}\\s?\\d{3})|(\\(?0\\d{3}\\)?\\s?\\d{3}\\s?\\d{4})|(\\(?0\\d{2}\\)?\\s?\\d{4}\\s?\\d{4}))+$";

    NSError *error = nil;

    NSRegularExpression * const regExpr =
    [NSRegularExpression regularExpressionWithPattern:regularExpression
                                              options:NSRegularExpressionCaseInsensitive
                                                error:&error];

    NSTextCheckingResult * const matchResult = [regExpr firstMatchInString:aString
                                                                   options:0 range:NSMakeRange(0, [aString length])];

    return matchResult ? YES : NO;
}



#pragma mark - Action Method


- (IBAction)dontDivertButtionAction:(id)sender {

    self.errorLabel.hidden = YES;
    self.phoneNumberTextfield.layer.borderColor = [BrandColours colourBtNeutral60].CGColor;
    self.phoneNumberTextfield.layer.borderWidth = 1.0f;

    if(_isDivertButtonSelected)
    {
        if(!_isNoDivertedSelectedInitially)
            self.saveButtonNeedToShow = YES;
        else
            self.saveButtonNeedToShow = NO;
        _isDivertButtonSelected = NO;
    }
    else
    {
        _isDivertButtonSelected = NO;
    }

    [self updateButtonsStateWithDivertedButtonState:_isDivertButtonSelected];
}


- (IBAction)divertButtonAction:(id)sender {

    self.errorLabel.hidden = YES;
    self.phoneNumberTextfield.layer.borderColor = [BrandColours colourBtNeutral60].CGColor;
    self.phoneNumberTextfield.layer.borderWidth = 1.0f;

    if(_isDivertButtonSelected)
    {
        if(!_isDivertedSelectedInitially)
            self.saveButtonNeedToShow = YES;
    }
    else
    {
        _isDivertButtonSelected = YES;
        if(!_isDivertedSelectedInitially)
            self.saveButtonNeedToShow = YES;
        else
            self.saveButtonNeedToShow = NO;

    }

    [self updateButtonsStateWithDivertedButtonState:_isDivertButtonSelected];

}



- (void)saveButtonPressed:(id)sender{


    if(![self validateInputWithString:self.phoneNumberTextfield.text])
    {

        //Error Tracker
        [OmnitureManager trackError:OMNIERROR_CALL_DIVERT_PHONE_FAIL onPageWithName:OMNIPAGE_FAULT_CALL_DIVERSION contextInfo:[AppManager getOmniDictionary]];
        
        self.errorLabel.hidden = NO;
        self.phoneNumberTextfield.layer.borderColor = [UIColor redColor].CGColor;
        self.phoneNumberTextfield.layer.borderWidth = 1.0;

        if(self.phoneNumberTextfield.text.length < 1) {
            self.errorLabel.text = @"Phone number is required";
        }
        else {
            self.errorLabel.text = @"Sorry, we can't divert calls to that number. Please check it's correct and try again.";
        }

        return;
    }

    NSMutableDictionary *callDiversionDetailDict = [NSMutableDictionary dictionary];

    if(self.callDiversionDetail.callDivertNumber)
        [callDiversionDetailDict setValue:self.callDiversionDetail.callDivertNumber forKey:@"CallDivertNumber"];
    if(self.callDiversionDetail.callDivertDate)
        [callDiversionDetailDict setValue:self.callDiversionDetail.callDivertDate forKey:@"callDivertDate"];
    if(self.callDiversionDetail.callDivertFlag)
        [callDiversionDetailDict setValue:self.callDiversionDetail.callDivertFlag forKey:@"callDivertFlag"];
    if(self.callDiversionDetail.showDivertFlag)
        [callDiversionDetailDict setValue:[NSNumber numberWithBool:self.callDiversionDetail.showDivertFlag] forKey:@"showDivert"];
    if(self.callDiversionDetail.customerCug)
        [callDiversionDetailDict setValue:self.callDiversionDetail.customerCug forKey:@"CustCug"];



    if(_isDivertButtonSelected)
    {
        [callDiversionDetailDict setValue:_phoneNumberTextfield.text forKey:@"calldivertNumber"];
        [callDiversionDetailDict setValue:@"Modify" forKey:@"callDivertFlag"];
    }
    else
    {
        [callDiversionDetailDict setValue:@"Add" forKey:@"callDivertFlag"];
    }

    BTCallDiversionDetail *updatedCallDiversion = [[BTCallDiversionDetail alloc] initCallDiversionDetailWithResponse:[NSDictionary dictionaryWithDictionary:callDiversionDetailDict]];
    [self.delegate userUpdatedFaultCallDiversionDetailOnCallDiversionScreen:self withDiversionDetailObj:updatedCallDiversion];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Textfield delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    self.errorLabel.hidden = YES;
    [self.phoneNumberTextfield updateWithEnabledState];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 1.0 / [UIScreen mainScreen].scale)];
    separatorView.backgroundColor = [UIColor lightGrayColor];
    textField.inputAccessoryView = separatorView;

    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    self.errorLabel.hidden = YES;

    self.phoneNumberTextfield.layer.borderColor = [BrandColours colourBtNeutral60].CGColor;
    self.phoneNumberTextfield.layer.borderWidth = 1.0;
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];

    if(_isDivertedSelectedInitially)
    {
        if([[newString lowercaseString] isEqualToString:_initialPhoneNumberString])
        {
            self.saveButtonNeedToShow = NO;
        }
        else
        {
            self.saveButtonNeedToShow = YES;
        }
    }


    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.phoneNumberTextfield updateWithEnabledState];
    
    self.errorLabel.hidden = YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField updateWithDisabledState];
}


#pragma mark - UIKeyboard Notification Handling


- (void)keyboardWillShow:(NSNotification*)aNotification
{

    if(_isDeviceLessThaniPhone5 && !_isKeyboardAppered)
    {
        _isKeyboardAppered = YES;
        self.containerViewTopConstraint.constant -= 90;
        [self.view setNeedsUpdateConstraints];

        [UIView animateWithDuration:0.5 animations:^{

            [self.view layoutIfNeeded];

        }];

    }
}


- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{

    if(_isDeviceLessThaniPhone5 && _isKeyboardAppered)
    {
        _isKeyboardAppered = NO;
        self.containerViewTopConstraint.constant += 90;
        [self.view setNeedsUpdateConstraints];

        [UIView animateWithDuration:0.5 animations:^{

            [self.view layoutIfNeeded];
        }];

    }
}


#pragma mark - Touch Method

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.phoneNumberTextfield resignFirstResponder];
}


#pragma mark - Omniture Methods

- (void)trackOmniPage:(NSString *)page
{
    [OmnitureManager trackPage:page];
}


@end
