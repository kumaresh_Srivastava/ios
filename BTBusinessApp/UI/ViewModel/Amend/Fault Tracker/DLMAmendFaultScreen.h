//
//  DLMAmendFaultScreen.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
#import "AmendViewModel.h"
#import "BTFaultContactDetails.h"
#import "BTCallDiversionDetail.h"
#import "BTFaultAppointment.h"

@class DLMAmendFaultScreen;
@class BTFaultAppointmentDetail;
@class NLWebServiceError;

#define kEngineerAppointmentText @"Engineer appointment"
#define kSiteContactText @"Site contact"
#define kCallDiversionText @"Call diversion"

@protocol DLMAmendFaultScreenDelegate <NSObject>

- (void)successfullyFetchedAmendFaultDataOnAmendFaultScreen:(DLMAmendFaultScreen *)amendFaultScreen;

- (void)amendFaultScreen:(DLMAmendFaultScreen *)amendFaultScreen failedToFetchAmendFaultDataWithWebServiceError:(NLWebServiceError *)error;

- (void)amendSuccessfullyFinishedByAmmendFaultScreen:(DLMAmendFaultScreen *)ammendScreen;

- (void)ammendFaultScreen:(DLMAmendFaultScreen *)ammendScreen failedToSubmitAmendFaultWithWebServiceError:(NLWebServiceError *)error;

@end

@interface DLMAmendFaultScreen : DLMObject

@property (nonatomic, strong) NSString *faultRef;
@property (nonatomic, strong) BTFaultContactDetails *contactDetails;
@property (nonatomic, strong) BTFaultAppointmentDetail *faultAppointmentDetail;
@property (nonatomic, strong) BTCallDiversionDetail *callDiversionDetail;
@property (nonatomic, assign) BOOL showCallDivert;
@property (nonatomic, assign) BOOL showAppointment;
@property (nonatomic, weak) id<DLMAmendFaultScreenDelegate> amendFaultScreenDelegate;

- (void)fetchAmendFaultDataWithFaultRef:(NSString *)faultRef;
- (void)cancelFetchAmendFaultDataAPIRequest;
- (void)submitAndMakeAmendFaultToServerWithNewContactDetails:(BTFaultContactDetails *)newFaultContactDetails appointmentDict:(NSDictionary *)appointmentDict andCallDiversionDetails:(BTCallDiversionDetail *)callDiversionDetail;
- (NSDictionary *)amendAppointmentDictionaryForAppointment:(BTFaultAppointment *)appointment isAppointmentChanged:(BOOL)appointmentChanged;
- (NSDictionary *)amendAppointmentDictionaryForAppointmentDate:(NSDate *)appointmentDate slotEarliestTime:(NSString *)earliestTime slotLatestTime:(NSString *)latestTime andIsAppointmentChanged:(BOOL)isAppointmentChanged;
- (void)cancelAmendOrderSubmitAPIRequest;
- (AmendViewModel *)getAmendModelForAppointmentDate:(NSDate *)date;
- (AmendViewModel *)getAmendModelForAppointment:(BTFaultAppointment *)appointment;
- (AmendViewModel *)getamendModelForContactDetail:(BTFaultContactDetails *)contactDetail;
- (AmendViewModel *)getamendModelForCallDiversionDetail:(BTCallDiversionDetail *)callDiversionDetail;
- (void)addTimeForEarliestTime:(NSString *)earliestTime latestTime:(NSString *)latestTimeSlot toAmendViewModel:(AmendViewModel *)amendViewModel;
@end
