//
//  BTFaultAmendViewController.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTAmendViewController.h"

@class BTFaultAmendViewController;
@class DLMFaultDetailsScreen;
@class BTFaultAppointmentDetail;

@protocol BTFaultAmendViewControllerDelegate <NSObject>

- (void)bTFaultAmendSubmitSuccessfullyOnFaultAmendViewController:(BTFaultAmendViewController *)controller isEngineerAppointmentChanged:(BOOL)isChanged andAppointmentDetail:(BTFaultAppointmentDetail *)faultAppointmentDetail;

@end

@interface BTFaultAmendViewController : BTAmendViewController

@property(nonatomic, copy) NSString *faultRefrence;
@property(nonatomic, weak) id<BTFaultAmendViewControllerDelegate> faultAmendDelegate;

@property (nonatomic, strong) NSArray *ammendFaultArray;

+ (BTFaultAmendViewController *)getBTFaultAmendViewController;

@end
