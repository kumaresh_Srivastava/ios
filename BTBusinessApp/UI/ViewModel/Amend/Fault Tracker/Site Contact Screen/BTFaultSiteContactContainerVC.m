//
//  BTFaultSiteContactContainerVCViewController.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/30/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultSiteContactContainerVC.h"
#import "BTFaultSiteContactViewController.h"
#import "BTAmendFooterActionView.h"
#import "BTNeedHelpViewController.h"
#import "OmnitureManager.h"
#import "AppManager.h"

@interface BTFaultSiteContactContainerVC ()<BTAddSiteContactTableViewControllerDelegare, BTAmendFooterActionViewDelegate>
{
    
    BTFaultSiteContactViewController *faultSiteContactController;
}
@end

@implementation BTFaultSiteContactContainerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //[self createSiteContactView];
    self.navigationItem.leftBarButtonItem = [self getCloseButtonItem];
    
     self.title = @"Contact details";
    
    [self trackOmniPage:OMNIPAGE_FAULT_AMEND_SITE_CONTACT];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self performSelector:@selector(configureUI) withObject:nil afterDelay:0];
}



#pragma mark- Helper Method

- (UIBarButtonItem *)getCloseButtonItem{
    
    UIBarButtonItem *closeButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonPressed:)];
    
    return closeButtonItem;
}


- (UIBarButtonItem *)getSaveButtonItem{
    
    UIBarButtonItem *saveButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonPressed:)];
    
    return saveButtonItem;
}


- (void)configureUI{
    
    
    
    
    BTAmendFooterActionView *amendFooterActionView = [[[NSBundle mainBundle] loadNibNamed:@"BTAmendFooterActionView" owner:self options:nil] firstObject];
    amendFooterActionView.delegate = self;
    
    CGFloat actionViewHeight = 45.0;
    amendFooterActionView.frame = CGRectMake(0,
                                             self.view.frame.size.height - (actionViewHeight + self.bottomLayoutGuide.length),
                                             self.view.frame.size.width,
                                             actionViewHeight);
    
    [self.view addSubview:amendFooterActionView];
    
    //Opening Site Contacts Screen
    faultSiteContactController = [BTFaultSiteContactViewController getBTFaultSiteContactViewController];
    faultSiteContactController.contactDetail = self.contactDetail;
    faultSiteContactController.delegate = self;
    [self.view addSubview:faultSiteContactController.view];
    
    faultSiteContactController.view.frame = CGRectMake(0,
                                                       0,
                                                       self.view.frame.size.width,
                                                       self.view.frame.size.height - (actionViewHeight + self.bottomLayoutGuide.length));
    [self addChildViewController:faultSiteContactController];
    [faultSiteContactController didMoveToParentViewController:self];
    
}



#pragma mark - action methods

- (void)saveButtonPressed:(id)sender{
    
    [faultSiteContactController saveContactInfo];
}

- (void)closeButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark- Delegate Methods

- (void)btAddSiteContactTableViewController:(BTAddSiteContactTableViewController *)controller didSubmitContactDetail:(BTFaultContactDetails *)contactDetail{
    
    [self.delegate btFaultSiteContactContainerVC:self didSubmitContactDetail:contactDetail];
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)btAddSiteContactTableViewController:(BTAddSiteContactTableViewController *)controller didChangeData:(BOOL)isDataChanged{
    
    if(isDataChanged){
        
        self.navigationItem.rightBarButtonItem = [self getSaveButtonItem];
    }
    else{
        
        self.navigationItem.rightBarButtonItem = nil;
    }
}



- (void)userPressedOnBTAmendFooterActionView:(BTAmendFooterActionView *)view{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    BTNeedHelpViewController *helpViewController = [storyboard instantiateViewControllerWithIdentifier:@"SiteContactNeedHelpViewController"];
    [self presentViewController:helpViewController animated:YES completion:nil];
}



#pragma mark - Omniture Methods


- (void)trackOmniPage:(NSString *)page
{

    [OmnitureManager trackPage:page];
}



@end
