//
//  BTFaultSiteContactViewController.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultSiteContactViewController.h"
#import "BTFaultTrackerSiteContactTableSectionHeader.h"
#import "DLMFaultContactDetailScreen.h"
#import "BTFaultTrackerSiteContactRadioButtonView.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "BTOrderSiteContactModel.h"

#define kFooterTitleText  @"Select you services from the following:"

@interface BTFaultSiteContactViewController ()<BTFaultTrackerSiteContactRadioButtonViewDelegate>{
    
    DLMFaultContactDetailScreen *_faultContactDetailScreenModel;
    BOOL _isSavedContactSelected;
    
    BTFaultContactDetails *_currectContactDetail;
    BTFaultContactDetails *_previousContactDetail;
    
    NSMutableDictionary *_cacheDictionary;
}

@end

@implementation BTFaultSiteContactViewController

+ (BTFaultSiteContactViewController *)getBTFaultSiteContactViewController{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    
    BTFaultSiteContactViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTFaultSiteContactViewControllerID"];
    return vc;

}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view
    
    self.title = @"Contact details";
    
    _cacheDictionary = [[NSMutableDictionary alloc] init];
    
    //Adding observer for data change
    
    __weak typeof(self) weakSelf = self;
    
    [RACObserve(self, self.isDataChanged) subscribeNext:^(NSNumber* state) {
        
        
        [weakSelf.delegate btAddSiteContactTableViewController:weakSelf didChangeData:weakSelf.isDataChanged];
        
    }];
    
    self.isDataChanged = NO;

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)createData{
    
    _currectContactDetail = self.contactDetail;
    _previousContactDetail = self.contactDetail;
    _faultContactDetailScreenModel = [[DLMFaultContactDetailScreen alloc] initWithContactDetail:self.contactDetail];
    _dataArray = [_faultContactDetailScreenModel getSectionsArray];
    
    if([_dataArray count] > 1){
        
        FaultContactDetailSection *section = [_dataArray objectAtIndex:1];
        if(section.selectedOptionIndex == 0){
            
            _isSavedContactSelected = YES;
        }
        else{
            
            _isSavedContactSelected = NO;
        }
        
        
    }
    
    [self.tableView reloadData];
}

- (void)reloadData{
    
    [self.tableView reloadData];
}




- (void)save{
    
    [self.view endEditing:YES];
    
    BOOL isValidationSuccessful = YES;
    
    NSInteger sectionNum = 0;
    
    for(FaultContactDetailSection *section in _dataArray){
        
        if(section.rowsArray && [section.rowsArray count] > 0){
            
            NSInteger rowNum = 0;
            
            for(BTTextFiledModel *model in section.rowsArray){
                
                if(isValidationSuccessful){
                    
                    isValidationSuccessful = [model isValid];
                    
                }
                else{
                    
                    break;
                }
                
                
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rowNum inSection:sectionNum];
                NSString *identifier = [NSString stringWithFormat:@"cell%d%d", (int)indexPath.section, (int)indexPath.row];
                AddContactTableViewCell *cell = [_cacheDictionary valueForKey:identifier];
                [cell updateUIAsPerValidity];

                rowNum++;
            }

        }
        
        
        sectionNum++;
        
    }
    
    
    if(isValidationSuccessful){
        
        BTFaultContactDetails *contactDetailObj = [self getContactDetail];//nil;// [self getUserDataDictionary];
        
        if([self.delegate respondsToSelector:@selector(btAddSiteContactTableViewController:didSubmitContactDetail:)]){
            
            [self.delegate btAddSiteContactTableViewController:self didSubmitContactDetail:contactDetailObj];
        }
        
       // [self.navigationController popViewControllerAnimated:YES];
    }
}


- (BTFaultContactDetails *)getContactDetail{
    
    //Pass entered data to delegate
    
    NSMutableDictionary *savedContactDict = [NSMutableDictionary dictionary];
    
    //Saved Contact
    if([_dataArray count] > 0){
        
        FaultContactDetailSection *section1 = [_dataArray objectAtIndex:0];
        
         //NSMutableDictionary *userDataDict = [NSMutableDictionary dictionary];

        for(BTTextFiledModel *textFieldModel in section1.rowsArray){
            
            if([textFieldModel.textFieldName isEqualToString:kTitleText]){
                
                [savedContactDict setValue:textFieldModel.inputText forKey:kUserTitleKey];
            }
            else if([textFieldModel.textFieldName isEqualToString:kFirstNameText]){
                
                [savedContactDict setValue:textFieldModel.inputText forKey:kUserFirstNameKey];
            }
            else if([textFieldModel.textFieldName isEqualToString:kLastNameText]){
                
                [savedContactDict setValue:textFieldModel.inputText forKey:kUserLastNameKey];
            }
            else if([textFieldModel.textFieldName isEqualToString:kMobileText]){
                
                [savedContactDict setValue:textFieldModel.inputText forKey:@"TextNo"];
                
            }
            else if([textFieldModel.textFieldName isEqualToString:kOptionalPhoneText]){
                
                [savedContactDict setValue:textFieldModel.inputText forKey:@"VoiceNo"];
            }
            else if([textFieldModel.textFieldName isEqualToString:kEmailText]){
                
                [savedContactDict setValue:textFieldModel.inputText forKey:@"EmailId"];
            }
            
            if(section1.selectedOptionIndex == 0){
                
                [savedContactDict setValue:@"sms" forKey:@"PreferredContact"];
            }
            else{
                
                [savedContactDict setValue:@"email" forKey:@"PreferredContact"];
            }

        }

    }
    
    
    
    //Site Contact
    NSMutableDictionary *siteContactDict = [NSMutableDictionary dictionary];
    
    if([_dataArray count] > 2){
        
        FaultContactDetailSection *section1 = [_dataArray objectAtIndex:2];
        
        for(BTTextFiledModel *textFieldModel in section1.rowsArray){
            
            if([textFieldModel.textFieldName isEqualToString:kTitleText]){
                
                [siteContactDict setValue:textFieldModel.inputText forKey:kUserTitleKey];
            }
            else if([textFieldModel.textFieldName isEqualToString:kFirstNameText]){
                
                [siteContactDict setValue:textFieldModel.inputText forKey:kUserFirstNameKey];
            }
            else if([textFieldModel.textFieldName isEqualToString:kLastNameText]){
                
                [siteContactDict setValue:textFieldModel.inputText forKey:kUserLastNameKey];
            }
            else if([textFieldModel.textFieldName isEqualToString:kMobileText]){
                
                [siteContactDict setValue:textFieldModel.inputText forKey:@"TextNo"];
                
            }
            else if([textFieldModel.textFieldName isEqualToString:kOptionalPhoneText]){
                
                [siteContactDict setValue:textFieldModel.inputText forKey:@"VoiceNo"];
            }
            else if([textFieldModel.textFieldName isEqualToString:kEmailText]){
                
                [siteContactDict setValue:textFieldModel.inputText forKey:@"EmailId"];
            }
            
            if(section1.selectedOptionIndex == 0){
                
                [siteContactDict setValue:@"sms" forKey:@"PreferredContact"];
            }
            else{
                
                [siteContactDict setValue:@"email" forKey:@"PreferredContact"];
            }

    }
    }
    
    if(siteContactDict && savedContactDict){
        
        NSMutableDictionary *contactDetailDict = [NSMutableDictionary dictionary];
        [contactDetailDict setValue:savedContactDict forKey:@"reporterContacts"];
        [contactDetailDict setValue:siteContactDict forKey:@"siteContacts"];
        
        
        FaultContactDetailSection *sectionObj = [_dataArray objectAtIndex:1];
        
        if(sectionObj.selectedOptionIndex == 0){
            
            [contactDetailDict setValue:@"true" forKey:@"reporterSameasSite"];
        }
        else{
            
            [contactDetailDict setValue:@"false" forKey:@"reporterSameasSite"];
        }
        
        //What else need to send
        
        BTFaultContactDetails  *contactDetail = [[BTFaultContactDetails alloc] initFaultContactDetailWithResponse:contactDetailDict];
        
        return contactDetail;
    }
    
    return nil;

}


- (void)setDataChangeState{
    
    BOOL dataChanged = NO;
    
    if(_currectContactDetail && _previousContactDetail){
        
        if(![self compareSiteContact:_currectContactDetail.reporterContacts andSiteContact:_previousContactDetail.reporterContacts]){
            
            dataChanged = YES;
        }
        
        if(![self compareSiteContact:_currectContactDetail.siteContacts andSiteContact:_previousContactDetail.siteContacts]){
            
            dataChanged = YES;
        }
        else if(![_currectContactDetail.reporterSameasSite isEqualToString:_previousContactDetail.reporterSameasSite]){
            
            dataChanged = YES;
        }

        
        
    }
    
    self.isDataChanged = dataChanged;
    
}


- (BOOL)compareSiteContact:(BTOrderSiteContactModel *)firstSiteContact andSiteContact:(BTOrderSiteContactModel *)secondSiteContact{
    
    
    BOOL areSame = YES;
    
    if(firstSiteContact && secondSiteContact){
        
        if(![firstSiteContact.phone isEqualToString:secondSiteContact.phone]){
            
            areSame = NO;
        }
        else if(![firstSiteContact.mobile isEqualToString:secondSiteContact.mobile]){
            
            areSame = NO;
        }
        else if(![firstSiteContact.email isEqualToString:secondSiteContact.email]){
            
            areSame = NO;
        }
        else if(![[firstSiteContact.preferredContact lowercaseString] isEqualToString:[secondSiteContact.preferredContact lowercaseString]]){
            
            areSame = NO;
        }
    }
    
    return areSame;
}

#pragma mark - action method

- (void)saveContactInfo{
    
    [self save];
    
}

- (void)closeButtonPressed:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if(_isSavedContactSelected){
        
        return [_dataArray count] - 1;
    }
    
    return [_dataArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    FaultContactDetailSection *sectionObje = (FaultContactDetailSection *)[_dataArray objectAtIndex:section];
    
    if(sectionObje.rowsArray){
        
        return [sectionObje.rowsArray count];
    }
    
    return 0;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    FaultContactDetailSection *sectionObj = [_dataArray objectAtIndex:indexPath.section];
    
    //Temp Fix, creating different cell for each row
    NSString *identifier = [NSString stringWithFormat:@"cell%d%d", (int)indexPath.section, (int)indexPath.row];
    AddContactTableViewCell *cell = [_cacheDictionary valueForKey:identifier];
    
    if(!cell){
        
        cell = [[[NSBundle mainBundle] loadNibNamed:@"AddContactTableViewCell" owner:self options:nil] firstObject];
        [_cacheDictionary setValue:cell forKey:identifier];
    }

    cell.delegate = self;
    [cell updateWithTextFieldModel:[sectionObj.rowsArray objectAtIndex:indexPath.row]];
    //[cell updateUIAsPerValidity];
    
    return cell;
}



- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    FaultContactDetailSection *sectionObj = [_dataArray objectAtIndex:section];
    
    if(!sectionObj.title)
        return [[UIView alloc] initWithFrame:CGRectZero];
    
    if(!sectionObj.headerView){
        
        BTFaultTrackerSiteContactTableSectionHeader *headerView = [[[NSBundle mainBundle] loadNibNamed:@"BTFaultTrackerSiteContactTableSectionHeader" owner:self options:nil] firstObject];
        
        [headerView updateSectionTitle:sectionObj.title];
        
        sectionObj.headerView = headerView;
    }
    
    
    return sectionObj.headerView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    
    FaultContactDetailSection *sectionObj = [_dataArray objectAtIndex:section];
    
    if(!sectionObj.title)
        return CGFLOAT_MIN;
    
    return 35.0;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    FaultContactDetailSection *sectionObj = [_dataArray objectAtIndex:section];
    
    if(!sectionObj.footerView){
        
        BTFaultTrackerSiteContactRadioButtonView *footerView = [[BTFaultTrackerSiteContactRadioButtonView alloc] initWithFrame:CGRectZero andOptionArray:sectionObj.footerOptions andTitleHeading:sectionObj.footerDescription andSelectedIndex:sectionObj.selectedOptionIndex];
        
        footerView.delegate = self;
        
        sectionObj.footerView = footerView;
        footerView.section = section;
    }
    
    
    return sectionObj.footerView;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    FaultContactDetailSection *sectionObj = [_dataArray objectAtIndex:section];

    CGFloat height = [BTFaultTrackerSiteContactRadioButtonView getFooterHeightForOptionArray:sectionObj.footerOptions withTitleText:sectionObj.footerDescription];
    
    return height;
}




#pragma AddContactTableViewCellDelegate

- (void)retunKeyPressedForAddContactTableViewCell:(AddContactTableViewCell *)cell{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    

    if(indexPath.section == 0 || indexPath.section == 2){

        FaultContactDetailSection *faultSectionModel = [_dataArray objectAtIndex:indexPath.section];
        
        BTTextFiledModel *textFieldModel = [faultSectionModel.rowsArray objectAtIndex:indexPath.row];
        
        if(textFieldModel.returnKeyType == UIReturnKeyNext && indexPath.row < [faultSectionModel.rowsArray count] - 1){
            
            BTTextFiledModel *textFielModel = [faultSectionModel.rowsArray objectAtIndex:indexPath.row + 1];
            [textFielModel makeFirstResponder];
            
        }
        else if(textFieldModel.returnKeyType == UIReturnKeyDone){
            
            [textFieldModel resignFirstResponder];
            
        }

    }
    

    
}

- (void)addContactTableViewCell:(AddContactTableViewCell *)cell
didSelectFiledForBTTextFiledModel:(BTTextFiledModel *)textFieldModel{

    //Nothing to do on selection

}

- (void)addContactTableViewCell:(AddContactTableViewCell *)cell
         didChangeTextForMoldel:(BTTextFiledModel *)textFieldModel{
    
    _currectContactDetail = [self getContactDetail];
    
    [self setDataChangeState];
}


#pragma mark - BTFaultTrackerSiteContactRadioButtonView delegate

- (void)userDidSelectedOption:(NSString *)optionText atIndex:(int)index onView:(BTFaultTrackerSiteContactRadioButtonView *)view
{
    
    FaultContactDetailSection *sectionObj = [_dataArray objectAtIndex:view.section];
    sectionObj.selectedOptionIndex = index;
    
    NSString *selectedOption = nil;
    
    if(sectionObj.footerOptions && [sectionObj.footerOptions count] > index){
        
        selectedOption = [sectionObj.footerOptions objectAtIndex:index];
    }

 
    if(view.section == 0 || view.section == 2){
        
        //As of now nothing to do
        /*
        //Email/Phone selection for saved contact
        if([selectedOption isEqualToString:kMobileText]){
            
            if(view.section == 0){
                
                //Saved contact mobile selection
            }
            else{
                
                //Site contact mobile selection
            }
            
        }
        else if([selectedOption isEqualToString:kEmailText]){
            
            if(view.section == 0){
                
                //Saved contact mobile selection
            }
            else{
                
                //Site contact mobile selection
            
            }

        }
         */
        
    }
    else if(view.section == 1 && selectedOption){
        
        //Same as above or enter another contact options
        
        if([selectedOption isEqualToString:kSameAsAboveText]){
            
            FaultContactDetailSection *firstSectionObj = [_dataArray objectAtIndex:0];
            FaultContactDetailSection *secondSectionObj = [_dataArray objectAtIndex:2];
            secondSectionObj.rowsArray = firstSectionObj.rowsArray;
            
            [self.tableView reloadData];
            
            _isSavedContactSelected = YES;
        
        }
        else if([selectedOption isEqualToString:kEnterSomeoneElseDetailText]){
            
            _isSavedContactSelected = NO;
            
        }
        [self.tableView reloadData];
        
    }
    
    _currectContactDetail = [self getContactDetail];
    [self setDataChangeState];
    
}

@end
