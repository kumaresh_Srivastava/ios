//
//  DLMFaultContactSetailScreen.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMFaultContactDetailScreen.h"
#import "BTOrderSiteContactModel.h"
#import "NSObject+APIResponseCheck.h"

@interface DLMFaultContactDetailScreen(){
    
    NSArray *_sectionsArray;
}

@end

@implementation DLMFaultContactDetailScreen

- (id)initWithContactDetail:(BTFaultContactDetails *)contactDetail{
    
    self = [super init];
    
    if(self){
        
        [self createModelsForContactDetail:contactDetail];
    }
    
    return self;
}



- (void)createModelsForContactDetail:(BTFaultContactDetails *)contactDetail{
    
    BTOrderSiteContactModel *savedContact = contactDetail.reporterContacts;
    BTOrderSiteContactModel *siteContact = contactDetail.siteContacts;
    NSMutableArray *sectionsTempArray = [NSMutableArray array];
    
    
    if(savedContact){
        
        NSMutableArray *tempDataArray = [NSMutableArray array];
        
        //Select Title
        BTTextFiledModel *selectTitleFieldModel = [[BTTextFiledModel alloc] init];
        selectTitleFieldModel.textFieldType = TextFiledTypeDropDown;
        selectTitleFieldModel.keyBoardType = UIKeyboardTypeDefault;
        selectTitleFieldModel.returnKeyType = UIReturnKeyNext;
        selectTitleFieldModel.textFieldName = kTitleText;
        selectTitleFieldModel.inputText = savedContact.title;
        selectTitleFieldModel.isDisabled = YES;
        [tempDataArray addObject:selectTitleFieldModel];
        
        //First Name Field Data
        BTTextFiledModel *firstNameFieldModel = [[BTTextFiledModel alloc] init];
        firstNameFieldModel.textFieldType = TextFiledTypeName;
        firstNameFieldModel.keyBoardType = UIKeyboardTypeDefault;
        firstNameFieldModel.returnKeyType = UIReturnKeyNext;
        firstNameFieldModel.textFieldName = kFirstNameText;
        firstNameFieldModel.inputText = savedContact.firstName;
        firstNameFieldModel.isDisabled = YES;
        [tempDataArray addObject:firstNameFieldModel];
        
        //Last Name Field Data
        BTTextFiledModel *lastNameFieldModel = [[BTTextFiledModel alloc] init];
        lastNameFieldModel.textFieldType = TextFiledLastName;
        lastNameFieldModel.keyBoardType = UIKeyboardTypeDefault;
        lastNameFieldModel.returnKeyType = UIReturnKeyNext;
        lastNameFieldModel.textFieldName = kLastNameText;
        lastNameFieldModel.inputText = savedContact.lastName;
        lastNameFieldModel.isDisabled = YES;
        [tempDataArray addObject:lastNameFieldModel];
        
        
        
        //Phone Field Data
        BTTextFiledModel *phoneFieldModel = [[BTTextFiledModel alloc] init];
        phoneFieldModel.textFieldType = TextFiledTypePhone;
        phoneFieldModel.keyBoardType = UIKeyboardTypePhonePad;
        phoneFieldModel.returnKeyType = UIReturnKeyNext;
        phoneFieldModel.textFieldName = kMobileText;
        phoneFieldModel.inputText = savedContact.mobile;//  [self getContactNumberFromSiteContact:savedContact];
        [tempDataArray addObject:phoneFieldModel];
        
        //Alt Phone Field Data
        BTTextFiledModel *altPhoneFieldModel = [[BTTextFiledModel alloc] init];
        altPhoneFieldModel.textFieldType = TextFiledTypePhoneOptional;
        altPhoneFieldModel.keyBoardType = UIKeyboardTypePhonePad;
        altPhoneFieldModel.returnKeyType = UIReturnKeyNext;
        altPhoneFieldModel.textFieldName = kOptionalPhoneText;
        altPhoneFieldModel.inputText = savedContact.phone;//[self getAltContactNumberFromSiteContact:savedContact];
        [tempDataArray addObject:altPhoneFieldModel];
        
        
        
        
        //Email Field Data
        BTTextFiledModel *emailFieldModel = [[BTTextFiledModel alloc] init];
        emailFieldModel.textFieldType = TextFiledTypeEmail;
        emailFieldModel.keyBoardType = UIKeyboardTypeEmailAddress;
        emailFieldModel.returnKeyType = UIReturnKeyDone;
        emailFieldModel.textFieldName = kEmailText;
        emailFieldModel.inputText = savedContact.email;
        [tempDataArray addObject:emailFieldModel];
        
        FaultContactDetailSection *contactDetailSection = [[FaultContactDetailSection alloc] init];
        contactDetailSection.title = @"Saved contact";
        contactDetailSection.footerDescription = @"What's the best way to contact you?";
        contactDetailSection.footerOptions = [NSArray arrayWithObjects:@"Mobile", kEmailText, nil];
        
        if([[savedContact.preferredContact lowercaseString] isEqualToString:@"sms"]){
            
            contactDetailSection.selectedOptionIndex = 0;
        }
        else{
            
            contactDetailSection.selectedOptionIndex = 1;
        }
        
        contactDetailSection.rowsArray = tempDataArray;
        
        
        FaultContactDetailSection *controllSection = [[FaultContactDetailSection alloc] init];
        controllSection.title = @"On-site contact";
        controllSection.footerDescription = @"If we need to visit your property, who should we contact?";
        controllSection.selectedOptionIndex = 0;
       
        if([[contactDetail.reporterSameasSite lowercaseString] isEqualToString:@"true"]){
            
            controllSection.selectedOptionIndex = 0;
        }
        else{
            
            controllSection.selectedOptionIndex = 1;
        }
        
        controllSection.footerOptions = [NSArray arrayWithObjects:kSameAsAboveText, kEnterSomeoneElseDetailText, nil];
        
        [sectionsTempArray addObject:contactDetailSection];
        [sectionsTempArray addObject:controllSection];

    }
    
    
    if(siteContact){
        
        
        NSMutableArray *tempDataArray = [NSMutableArray array];
        
        //Select Title
        BTTextFiledModel *selectTitleFieldModel = [[BTTextFiledModel alloc] init];
        selectTitleFieldModel.textFieldType = TextFiledTypeDropDown;
        selectTitleFieldModel.keyBoardType = UIKeyboardTypeDefault;
        selectTitleFieldModel.returnKeyType = UIReturnKeyNext;
        selectTitleFieldModel.textFieldName = kTitleText;
        selectTitleFieldModel.inputText = siteContact.title;
        selectTitleFieldModel.isDisabled = YES;
        [tempDataArray addObject:selectTitleFieldModel];
        
        //First Name Field Data
        BTTextFiledModel *firstNameFieldModel = [[BTTextFiledModel alloc] init];
        firstNameFieldModel.textFieldType = TextFiledTypeName;
        firstNameFieldModel.keyBoardType = UIKeyboardTypeDefault;
        firstNameFieldModel.returnKeyType = UIReturnKeyNext;
        firstNameFieldModel.textFieldName = kFirstNameText;
        firstNameFieldModel.inputText = siteContact.firstName;
        firstNameFieldModel.isDisabled = YES;
        [tempDataArray addObject:firstNameFieldModel];
        
        //Last Name Field Data
        BTTextFiledModel *lastNameFieldModel = [[BTTextFiledModel alloc] init];
        lastNameFieldModel.textFieldType = TextFiledLastName;
        lastNameFieldModel.keyBoardType = UIKeyboardTypeDefault;
        lastNameFieldModel.returnKeyType = UIReturnKeyNext;
        lastNameFieldModel.textFieldName = kLastNameText;
        lastNameFieldModel.inputText = siteContact.lastName;
        lastNameFieldModel.isDisabled = YES;
        [tempDataArray addObject:lastNameFieldModel];
        
        
        
        //Phone Field Data
        BTTextFiledModel *phoneFieldModel = [[BTTextFiledModel alloc] init];
        phoneFieldModel.textFieldType = TextFiledTypePhone;
        phoneFieldModel.keyBoardType = UIKeyboardTypePhonePad;
        phoneFieldModel.returnKeyType = UIReturnKeyNext;
        phoneFieldModel.textFieldName = kMobileText;
        phoneFieldModel.inputText = siteContact.mobile;//[self getContactNumberFromSiteContact:siteContact];
        [tempDataArray addObject:phoneFieldModel];
        
        //Alt Phone Field Data
        BTTextFiledModel *altPhoneFieldModel = [[BTTextFiledModel alloc] init];
        altPhoneFieldModel.textFieldType = TextFiledTypePhoneOptional;
        altPhoneFieldModel.keyBoardType = UIKeyboardTypePhonePad;
        altPhoneFieldModel.returnKeyType = UIReturnKeyNext;
        altPhoneFieldModel.textFieldName = kOptionalPhoneText;
        altPhoneFieldModel.inputText = siteContact.phone;//[self getAltContactNumberFromSiteContact:siteContact];
        [tempDataArray addObject:altPhoneFieldModel];
        
        
        
        
        //Email Field Data
        BTTextFiledModel *emailFieldModel = [[BTTextFiledModel alloc] init];
        emailFieldModel.textFieldType = TextFiledTypeEmail;
        emailFieldModel.keyBoardType = UIKeyboardTypeEmailAddress;
        emailFieldModel.returnKeyType = UIReturnKeyDone;
        emailFieldModel.textFieldName = kEmailText;
        emailFieldModel.inputText = siteContact.email;
        [tempDataArray addObject:emailFieldModel];
        
        FaultContactDetailSection *contactDetailSection2 = [[FaultContactDetailSection alloc] init];
        contactDetailSection2.title = nil;
        contactDetailSection2.footerDescription = @"What's the best way to contact you?";
        contactDetailSection2.footerOptions = [NSArray arrayWithObjects:@"Mobile", @"Email", nil];
        if([[savedContact.preferredContact lowercaseString] isEqualToString:@"sms"]){
            
            contactDetailSection2.selectedOptionIndex = 0;
        }
        else{
            
            contactDetailSection2.selectedOptionIndex = 1;
        }
        contactDetailSection2.rowsArray = tempDataArray;
        
        [sectionsTempArray addObject:contactDetailSection2];
        
    }
    
    
    for(FaultContactDetailSection *sectionObj in sectionsTempArray){
        
        for(BTTextFiledModel *textFieldModel in sectionObj.rowsArray){
            
            textFieldModel.screenName = @"FaultAddContact";
            
        }
        
    }
    
    _sectionsArray = sectionsTempArray;
    
    
    
}


- (NSArray *)getSectionsArray{
    
    
    return _sectionsArray;
    
}

- (NSString *)getContactNumberFromSiteContact:(BTOrderSiteContactModel *)siteContact{
    
    if(!siteContact)
        return nil;
    
    NSString *contatNumber = @"";
    NSString *mobile = siteContact.mobile;
    NSString *phone = siteContact.phone;//phone

    
    if(mobile && [mobile validAndNotEmptyStringObject]){
        
        contatNumber = mobile;
    }
    else{
        
        contatNumber = phone;
    }
    
    return contatNumber;

    
}

- (NSString *)getAltContactNumberFromSiteContact:(BTOrderSiteContactModel *)siteContact{
    
    if(!siteContact)
        return nil;
    
    NSString *contatNumber = @"";
    NSString *mobile = siteContact.mobile;
    NSString *phone = siteContact.phone;//phone
    
    
    if(siteContact.preferredContact && [siteContact.preferredContact validAndNotEmptyStringObject]){
        
        if([[siteContact.preferredContact lowercaseString]  isEqualToString:@"sms"]){
            
            contatNumber = phone;
        }
        else{
            contatNumber = mobile;
        }
        
    }
    else{
        
        contatNumber = mobile;
    }
    
    return contatNumber;
    
    
}


@end



@implementation FaultContactDetailSection


@end
