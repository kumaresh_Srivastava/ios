//
//  BTFaultSiteContactViewController.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTAddSiteContactTableViewController.h"
#import "BTFaultContactDetails.h"

@interface BTFaultSiteContactViewController : BTAddSiteContactTableViewController {
    
}

@property(nonatomic, strong) BTFaultContactDetails *contactDetail;
@property (assign, nonatomic) BOOL isDataChanged;
+ (BTFaultSiteContactViewController *)getBTFaultSiteContactViewController;
- (void)saveContactInfo;

@end
