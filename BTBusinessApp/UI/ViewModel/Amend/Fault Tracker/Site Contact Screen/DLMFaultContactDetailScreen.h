//
//  DLMFaultContactSetailScreen.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMAddNewContactScreen.h"
#import "BTTextFiledModel.h"
#import "BTFaultContactDetails.h"

#define kSameAsAboveText @"Same as above"
#define kEnterSomeoneElseDetailText @"Enter someone else's details"
//#define kMobileText @"Mobile"
#define kEmailText @"Email"

@interface DLMFaultContactDetailScreen : DLMAddNewContactScreen
- (id)initWithContactDetail:(BTFaultContactDetails *)contactDetail;
- (NSArray *)getSectionsArray;
@end


@interface FaultContactDetailSection : NSObject
@property(nonatomic, copy) NSString *title;
@property(nonatomic, strong) NSArray *rowsArray;
@property(nonatomic, strong) UIView *headerView;
@property(nonatomic, strong) UIView *footerView;
@property(nonatomic, strong) NSString *footerDescription;
@property(nonatomic, strong) NSArray *footerOptions;
@property(nonatomic, assign) NSInteger selectedOptionIndex;

@end
