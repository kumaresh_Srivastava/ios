//
//  BTFaultSiteContactContainerVCViewController.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/30/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTFaultContactDetails.h"

@class BTFaultContactDetails;
@class BTFaultSiteContactContainerVC;

@protocol BTFaultSiteContactContainerVCDelegate <NSObject>

- (void)btFaultSiteContactContainerVC:(BTFaultSiteContactContainerVC *)controller didSubmitContactDetail:(BTFaultContactDetails *)contactDetail;

@end

@interface BTFaultSiteContactContainerVC : UIViewController
@property(nonatomic, strong) BTFaultContactDetails *contactDetail;
@property(nonatomic, assign) id<BTFaultSiteContactContainerVCDelegate> delegate;

@end
