//
//  CustomPopoverController.m
//  Popover
//
//  Created by VectoScalar on 1/11/17.
//  Copyright © 2017 VectoScalar. All rights reserved.
//

#import "CustomPopoverController.h"
#import "DropDownTableViewCell.h"

#define kDropDownTableViewCellID @"DropDownTableViewCellID"

@interface CustomPopoverController ()<UIPopoverPresentationControllerDelegate, UITableViewDelegate, UITableViewDataSource>{
    
    NSArray *_dataArray;
}

@end

@implementation CustomPopoverController


- (instancetype)init {
    if (self = [super init]) {
        self.modalPresentationStyle = UIModalPresentationPopover;
        self.popoverPresentationController.delegate = self;
        self.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
        self.view.superview.layer.cornerRadius = 0;
    }
    return self;

}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    UINib *nib = [UINib nibWithNibName:@"DropDownTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kDropDownTableViewCellID];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.tableView.estimatedRowHeight = 35.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
}



- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    self.view.superview.layer.cornerRadius = 0;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Public Methods

- (void)updateWithOptionArray:(NSArray *)optionArray{
    
    if(optionArray){
        
        _dataArray = [NSArray arrayWithArray:optionArray];
    }

    [self.tableView reloadData];
}



#pragma mark- TableView Data Source and Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_dataArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    DropDownTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDropDownTableViewCellID];
    cell.timeLabel.text = [_dataArray objectAtIndex:indexPath.row];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.delegate customPopoverController:self didSelectTimeSlot:[_dataArray objectAtIndex:indexPath.row]];
    
}



#pragma mark - Popover Methods

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController{
    
    [self.delegate popoverDismissAtCustomPopoverController:self];

    return YES;
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone; //You have to specify this particular value in order to make it work on iPhone.
}



@end
