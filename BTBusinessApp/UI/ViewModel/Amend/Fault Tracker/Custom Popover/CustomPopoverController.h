//
//  CustomPopoverController.h
//  Popover
//
//  Created by VectoScalar on 1/11/17.
//  Copyright © 2017 VectoScalar. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CustomPopoverController;

@protocol CustomPopoverControllerDelegate <NSObject>
- (void)popoverDismissAtCustomPopoverController:(CustomPopoverController *)controller;
- (void)customPopoverController:(CustomPopoverController *)controller didSelectTimeSlot:(NSString *)timeSlot;
@end


@interface CustomPopoverController : UITableViewController
@property(nonatomic, weak) id<CustomPopoverControllerDelegate>delegate;

- (void)updateWithOptionArray:(NSArray *)optionArray;
@end
