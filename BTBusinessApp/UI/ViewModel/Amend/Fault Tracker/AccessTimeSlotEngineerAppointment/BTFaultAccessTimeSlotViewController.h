//
//  BTFaultAccessTimeSlotViewController.h
//  BTBusinessApp
//
//  Created by VectoScalar on 1/4/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseViewController.h"

@class BTFaultAppointmentDetail;
@class BTFaultAccessTimeSlotViewController;

@protocol BTFaultAccessTimeSlotViewControllerDelegate <NSObject>

- (void)btFaultAccessTimeSlotViewController:(BTFaultAccessTimeSlotViewController *)controller didSelectDate:(NSDate *)date earliarTimeSlot:(NSString *)earlierTimeSlot andLatestTimeSlot:(NSString *)latestTimeSlot;

@end

@interface BTFaultAccessTimeSlotViewController : BTBaseViewController
@property(nonatomic, assign) id<BTFaultAccessTimeSlotViewControllerDelegate>delegate;
@property(nonatomic, strong) BTFaultAppointmentDetail *faultAppointmentDetail;
@property(nonatomic, copy) NSString *faultReference;
+ (BTFaultAccessTimeSlotViewController *)getBTFaultAccessTimeSlotViewController;

@end
