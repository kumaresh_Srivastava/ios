//
//  DLMFaultAccessTimeSlotScreen.m
//  BTBusinessApp
//
//  Created by VectoScalar on 1/9/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "DLMFaultAccessTimeSlotScreen.h"
#import "BTFaultAppointmentDetail.h"
#import "BTFaultAppointment.h"
//#import "NLFaultAppointmentSlotWebService.h"
#import "NLFaultAppointmentAccessSlotWebService.h"

@interface DLMFaultAccessTimeSlotScreen()<NLFaultAppointmentSlotWebServiceDelegate>{
    
    BTFaultAppointmentDetail *_faultAppointmentDetail;
}
@property (nonatomic) NLFaultAppointmentSlotWebService *faultAppointmentSlotWebService;

@end

@implementation DLMFaultAccessTimeSlotScreen

#pragma mark- public methods

- (id)initWithFaultDetail:(BTFaultAppointmentDetail *)faultDetail{
    
    self = [super init];
    
    if(self){
        
        _faultAppointmentDetail = faultDetail;
    }
    
    return self;
}


- (void)fetchFaultAppointmentSlotForFaultReference:(NSString *)faultRef andInputDate:(NSString *)date
{
    // Get the currently logged in User

    DDLogInfo(@"Fetching fault summary details data from server for fault ref %@", faultRef);
    
    self.faultAppointmentSlotWebService = [[NLFaultAppointmentAccessSlotWebService alloc] initWithFaultID:faultRef andInputDate:date];
    
    self.faultAppointmentSlotWebService.getfaultAppointmentSlotWebServiceDelegateDelegate = self;
    [self.faultAppointmentSlotWebService resume];
    
}


- (void)cancelFetchFaultAppointmentAPIRequest {
    
    self.faultAppointmentSlotWebService.getfaultAppointmentSlotWebServiceDelegateDelegate = nil;
    [self.faultAppointmentSlotWebService cancel];
    self.faultAppointmentSlotWebService = nil;
}


- (NSArray *)getEarliestTimeOptionArrayForDate:(NSDate *)date{
    
    
    NSMutableArray *optionsArray = [NSMutableArray arrayWithArray:_faultAppointmentDetail.acessTimeOptionArray];
    [optionsArray removeLastObject];
    
    NSDate *currentDate = [NSDate date];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    

    if ([[dateFormat stringFromDate:currentDate] isEqualToString:[dateFormat stringFromDate:date]])
    {
        //It's the same day, Remove earlier slots
        NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:currentDate];
        NSInteger minut = [components minute];
        
        NSInteger minutesNeedToBeAdded = 60 - minut;
        NSDate *roudedOffDate = [currentDate dateByAddingTimeInterval:minutesNeedToBeAdded * 60];
        
        [dateFormat setDateFormat:@"hh:mma"];
        NSString *timeSlot = [[dateFormat stringFromDate:roudedOffDate] lowercaseString];
        
        if([optionsArray containsObject:timeSlot]){
            
            NSInteger indexOfTimeSlote = [optionsArray indexOfObject:timeSlot];
            [optionsArray removeObjectsInRange:NSMakeRange(0, indexOfTimeSlote)];
        }
        else{
            
            if([_faultAppointmentDetail.acessTimeOptionArray containsObject:timeSlot]){
                
                //When current date is being selected after 9:59 pm
                [optionsArray removeAllObjects];
            }
        }
    }

    return optionsArray;
}


- (NSArray *)getLatestTimeOptionArrayForSelectedEarliestTime:(NSString *)earliestTime andDate:(NSDate *)date{
    
    NSInteger selectedIndex = 0;
    
    NSMutableArray *optionsArray = [NSMutableArray arrayWithArray:_faultAppointmentDetail.acessTimeOptionArray];
    
    if([optionsArray containsObject:earliestTime]){
        
        selectedIndex = [optionsArray indexOfObject:earliestTime];
        [optionsArray removeObjectsInRange:NSMakeRange(0, selectedIndex + 1)];
    }
    else{
        
        NSDate *currentDate = [NSDate date];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        
        if ([[dateFormat stringFromDate:currentDate] isEqualToString:[dateFormat stringFromDate:date]])
        {
            //It's the same day, Remove earlier slots
            NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:currentDate];
            NSInteger minut = [components minute];
            
            NSInteger minutesNeedToBeAdded = 60 - minut;
            NSDate *roudedOffDate = [currentDate dateByAddingTimeInterval:minutesNeedToBeAdded * 60];
            
            [dateFormat setDateFormat:@"hh:mma"];
            NSString *timeSlot = [[dateFormat stringFromDate:roudedOffDate] lowercaseString];
            NSInteger indexOfTimeSlote = [optionsArray indexOfObject:timeSlot];
            
            if(indexOfTimeSlote + 1 < [optionsArray count]){
                
                [optionsArray removeObjectsInRange:NSMakeRange(0, indexOfTimeSlote + 1)];
            }
            else{
                
                //When current date is being selected after 9:59 pm
                [optionsArray removeAllObjects];
            }
        }
        else{
            
            //Removing first option
            [optionsArray removeObjectAtIndex:0];
        }

    }
    
    return optionsArray;
}

- (BOOL)appointmentDate:(NSDate *)appointmentDate isSameAsDate:(NSDate *)currentDate{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    if ([[dateFormat stringFromDate:currentDate] isEqualToString:[dateFormat stringFromDate:appointmentDate]])
    {
        //It's the same day, Remove earlier slots
       
        return YES;
    }
    
    return NO;

}



#pragma mark - NLFaultAppointmentSlotWebService Delegate

- (void)faultAppointmentSlotWebService:(NLFaultAppointmentSlotWebService *)webService successfullyFetchedFaultAppointmentSlotWebServiceData:(BTFaultAppointmentSlotDetail *)faultAppointmentSlotDetail
{
    
    if(self.faultAppointmentSlotWebService == webService){
        
        [self.faultAccessTimeSlotScreenDelegate successfullyFetchedFaultAccessTimeSlotScreen:self withFaultAppointmentDetail:faultAppointmentSlotDetail];
        
        self.faultAppointmentSlotWebService.getfaultAppointmentSlotWebServiceDelegateDelegate = nil;
        self.faultAppointmentSlotWebService = nil;
        
    }
    else{
        
        DDLogError(@"This delegate method gets called because of success of an object of BTFaultAppointmentSlotDetail but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of BTFaultAppointmentSlotDetail but this object is not the one stored in member variable of this class %@.", [self class]);
    }

    
}

- (void)faultAppointmentWebService:(NLFaultAppointmentSlotWebService *)webService failedToFetchFaultAppointmentSlotWebServiceDataWithWebServiceError:(NLWebServiceError *)webServiceError{

    
    if(self.faultAppointmentSlotWebService == webService){
        
        [self.faultAccessTimeSlotScreenDelegate faultAccessTimeSlotScreen:self failedToFetchFaultAppointmentSlotWithWebServiceError:webServiceError];
        
        self.faultAppointmentSlotWebService.getfaultAppointmentSlotWebServiceDelegateDelegate = nil;
        self.faultAppointmentSlotWebService = nil;
        
    }
    else{
        
        DDLogError(@"This delegate method gets called because of success of an object of BTFaultAppointmentSlotDetail but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of BTFaultAppointmentSlotDetail but this object is not the one stored in member variable of this class %@.", [self class]);
    }

}




@end


