//
//  DLMFaultAccessTimeSlotScreen.h
//  BTBusinessApp
//
//  Created by VectoScalar on 1/9/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "DLMObject.h"

@class BTFaultAppointmentDetail;
@class BTFaultAppointment;
@class DLMFaultAccessTimeSlotScreen;
@class BTFaultAppointmentSlotDetail;
@class NLWebServiceError;

@protocol DLMFaultAccessTimeSlotScreenDelegate <NSObject>

- (void)successfullyFetchedFaultAccessTimeSlotScreen:(DLMFaultAccessTimeSlotScreen *)faultAppointmentDetailScreen withFaultAppointmentDetail:(BTFaultAppointmentSlotDetail *)faultAppointmentDetail;

- (void)faultAccessTimeSlotScreen:(DLMFaultAccessTimeSlotScreen *)faultAppointmentSlotScreen failedToFetchFaultAppointmentSlotWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface DLMFaultAccessTimeSlotScreen : DLMObject
@property (nonatomic, weak) id<DLMFaultAccessTimeSlotScreenDelegate>faultAccessTimeSlotScreenDelegate;
- (id)initWithFaultDetail:(BTFaultAppointmentDetail *)faultDetail;
- (void)fetchFaultAppointmentSlotForFaultReference:(NSString *)faultRef andInputDate:(NSString *)date;
- (void)cancelFetchFaultAppointmentAPIRequest;
- (NSArray *)getEarliestTimeOptionArrayForDate:(NSDate *)date;
- (NSArray *)getLatestTimeOptionArrayForSelectedEarliestTime:(NSString *)earliestTime andDate:(NSDate *)date;
- (BOOL)appointmentDate:(NSDate *)appointmentDate isSameAsDate:(NSDate *)currentDate;
@end
