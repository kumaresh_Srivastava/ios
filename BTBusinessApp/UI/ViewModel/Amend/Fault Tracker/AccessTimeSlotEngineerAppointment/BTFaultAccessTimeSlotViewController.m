//
//  BTFaultAccessTimeSlotViewController.m
//  BTBusinessApp
//
//  Created by VectoScalar on 1/4/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTFaultAccessTimeSlotViewController.h"
#import "BTFaultAccessTimeSlotCell.h"
#import "BTFaultAppointmentDetail.h"
#import "BTFaultAppointment.h"
#import "CustomPopoverController.h"
#import "CustomPopoverBackgroundView.h"
#import "DLMFaultAccessTimeSlotScreen.h"
#import "BTFaultAppointmentSlotDetail.h"
#import "BTAmendFooterActionView.h"
#import "CustomSpinnerAnimationView.h"
#import "AppConstants.h"
#import "AppManager.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "NLWebServiceError.h"
#import "BTAmendFooterActionView.h"
#import "BTNeedHelpViewController.h"
#import "OmnitureManager.h"
#import "AppManager.h"

#define kEarliestSelectioTag 111
#define kLatestSelectioTag 112

#define kSelectTimeText @"Select"
#define kNavTitle @"Engineer appointment"

#define kBTFaultAccessTimeSlotCell @"BTFaultAccessTimeSlotCell"
#define kBTFaultAccessTimeSlotCellID @"BTFaultAccessTimeSlotCellID"

#define kAppointmentMaxSize 90

@interface BTFaultAccessTimeSlotViewController ()<UITableViewDataSource, UITableViewDelegate, BTFaultAccessTimeSlotCellDelegate, CustomPopoverControllerDelegate, DLMFaultAccessTimeSlotScreenDelegate, BTAmendFooterActionViewDelegate>{
    
    NSMutableArray *_slotArray;
    NSInteger _selectedRow;
    NSArray *_accessTimeOptionArray;
    
    NSString *_earliestTimeSlot;
    NSString *_latestTimeSlot;
    NSDate *_appointmentSlotDate;
    
    NSString *_selectedEarliestTimeSlot;
    NSString *_selectedLatestTimeSlot;
    NSDate *_selectedAppointmentSlotDate;
    
    CustomPopoverController *_customPopover;
    
    DLMFaultAccessTimeSlotScreen *_viewModel;
    
    
    BTAmendFooterActionView *_amendFooterActionView;
    UIView *_retryFooterView;
    CustomSpinnerAnimationView *_footerView;
    
    BOOL _isOpeningNeedHelp;
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSDate *maxDate;
@property (assign, nonatomic) BOOL isDataChanged;
@property (assign, nonatomic) BOOL isLoading;
@property (assign, nonatomic) BOOL hasMoreData;

@end

@implementation BTFaultAccessTimeSlotViewController




- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self trackOmniPage:OMNIPAGE_FAULT_AMMEND_ENGG_APPOINTMENT];
    
    [self configure];
    
}


- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    _isOpeningNeedHelp = NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Class Methods

+ (BTFaultAccessTimeSlotViewController *)getBTFaultAccessTimeSlotViewController{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    BTFaultAccessTimeSlotViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"BTFaultAccessTimeSlotViewControllerID"];

    return controller;

}

#pragma mark - Overriden Methods

- (void)setIsDataChanged:(BOOL)isDataChanged{
    
    _isDataChanged = isDataChanged;
    
    if(isDataChanged){
        
        self.navigationItem.rightBarButtonItem = [self getNextButtonItem];
    }
    else{
        
        self.navigationItem.rightBarButtonItem = nil;
    }
}


- (void)cancelInProgressAPI{
    
    if(!_isOpeningNeedHelp){
        
        [_viewModel cancelFetchFaultAppointmentAPIRequest];
    }
    
}


#pragma mark- Helper Method

//Method to configure components
- (void)configure{
    
    
    _selectedRow = -1;
    //Setup table view
    self.tableView.estimatedRowHeight = 58;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    //Register nib
    UINib *nib = [UINib nibWithNibName:kBTFaultAccessTimeSlotCell bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kBTFaultAccessTimeSlotCellID];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    //Initializing and fetching data
    _viewModel = [[DLMFaultAccessTimeSlotScreen alloc] initWithFaultDetail:_faultAppointmentDetail];
    _viewModel.faultAccessTimeSlotScreenDelegate = self;
    
    if(_faultAppointmentDetail){
        
        _earliestTimeSlot = _faultAppointmentDetail.earlierAccessTime;
        _latestTimeSlot = _faultAppointmentDetail.latestAccessTime;
        _appointmentSlotDate = _faultAppointmentDetail.bookedStartDate;
        
        _selectedEarliestTimeSlot = kSelectTimeText;
        _selectedLatestTimeSlot = kSelectTimeText;
        _selectedAppointmentSlotDate = _appointmentSlotDate;
        
        if(_faultAppointmentDetail.faultAppointmentsArray && [_faultAppointmentDetail.faultAppointmentsArray count] > 0){
            
            _slotArray = [NSMutableArray arrayWithArray:_faultAppointmentDetail.faultAppointmentsArray];
        }
        
    }
    
    if(_appointmentSlotDate){
        
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        [dateComponents setDay:90];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        self.maxDate = [calendar dateByAddingComponents:dateComponents toDate:_appointmentSlotDate options:0];
    }

    
    [self createFooterActionView];
    
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.leftBarButtonItem = [self getCloseButtonItem];
    self.navigationItem.title = kNavTitle;
    
    self.hasMoreData = YES;
    
}

- (UIBarButtonItem *)getCloseButtonItem{
    
    UIBarButtonItem *closeButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonPressed)];
    
    return closeButtonItem;
}



//Method to check and update data change status
- (void)updateDataChangeStatus{
    
    BOOL hasDataChanged = NO;
    
    if([_selectedEarliestTimeSlot isEqualToString:kSelectTimeText] || [_selectedLatestTimeSlot isEqualToString:kSelectTimeText]){
        
        self.isDataChanged = hasDataChanged;
        return;
    }
    
    
    if(!([_selectedAppointmentSlotDate compare:_appointmentSlotDate] == NSOrderedSame)){
        
        hasDataChanged = YES;
    }
    
    if(![_selectedEarliestTimeSlot isEqualToString:kSelectTimeText] && ![_selectedEarliestTimeSlot isEqualToString:_earliestTimeSlot]){
        
        hasDataChanged = YES;
    }
    
    if(![_selectedEarliestTimeSlot isEqualToString:kSelectTimeText] && ![_selectedLatestTimeSlot isEqualToString:_latestTimeSlot]){
        
        hasDataChanged = YES;
    }
    
    self.isDataChanged = hasDataChanged;
}


//Method to get save button
- (UIBarButtonItem *)getNextButtonItem{
    
    UIBarButtonItem *nextButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextButtonPressed:)];
    return nextButtonItem;
}


//Method to reload current selected cell
- (void)reloadSelectedCell{
    
    if(_selectedRow >= 0){
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_selectedRow inSection:0];
        [_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
}


//Method to open popover from a soucrce view

- (void)openPopoverForView:(UIView *)sourceView andEariliestTime:(BOOL)isEarliestTime{
    
    
    //Creating popover
    _customPopover = [[CustomPopoverController alloc] init];
    _customPopover.delegate = self;
    
    
    //Taking options as per selections type
    NSArray *optionsArray = nil;
    BTFaultAppointment *appointment = [_slotArray objectAtIndex:_selectedRow];
    
    if(isEarliestTime){
        
        _customPopover.view.tag = kEarliestSelectioTag;
        optionsArray = [_viewModel getEarliestTimeOptionArrayForDate:appointment.dateInfo];
    }
    else{
        
        _customPopover.view.tag = kLatestSelectioTag;
        optionsArray = [_viewModel getLatestTimeOptionArrayForSelectedEarliestTime:_selectedEarliestTimeSlot andDate:appointment.dateInfo];
    }
    
    
    if(!optionsArray || [optionsArray count] <= 0){
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"No slots available" message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self reloadSelectedCell];
            
        }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:^{}];
    
        
        return;
    }
    
    //Setting popover frame
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    CGSize popoverSize = CGSizeMake(sourceView.frame.size.width,
                                    5*35);
    
    if([optionsArray count] < 5){
        
        popoverSize.height = [optionsArray count] * 35;
    }
    
    CGRect sourceRespectiveFrame = [sourceView convertRect:sourceView.bounds toView:self.view];
    sourceRespectiveFrame.origin.y += 5;
    
    //Changing direction based on space availability
    if(screenSize.height - 70 - (sourceRespectiveFrame.origin.y + sourceRespectiveFrame.size.height + popoverSize.height) < 0){
        
        _customPopover.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionDown;
        sourceRespectiveFrame.origin.y -= 34;
    }
    
    [_customPopover updateWithOptionArray:optionsArray];
    
    [_customPopover.popoverPresentationController setPopoverBackgroundViewClass:[CustomPopoverBackgroundView class]];
    _customPopover.preferredContentSize = popoverSize;
    _customPopover.popoverPresentationController.sourceView = self.view; //The view containing the anchor rectangle for the popover.

    _customPopover.popoverPresentationController.sourceRect = sourceRespectiveFrame;//The rectangle in the specified view in which to anchor the popover.
    [self presentViewController:_customPopover animated:YES completion:nil];
    
}



//Mathod to make API calll
- (void)makeAPICall{
    
    NSString *date = @"";
    
    BTFaultAppointment *appointment = [_slotArray lastObject];

    
    NSDate *lastDate = nil;
    
    if(appointment){
        
        lastDate = appointment.dateInfo;
    }
    else{
        
        lastDate = _faultAppointmentDetail.bookedStartDate;
    }
    
    
    //Compare
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setDay:1];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *nextToLastDate = [calendar dateByAddingComponents:dateComponents toDate:lastDate options:0];
    date = [self getFormatedDateFromDate:nextToLastDate];
    
    [_viewModel fetchFaultAppointmentSlotForFaultReference:self.faultReference andInputDate:date];
}



//Load More footer view
- (void)createFooterView{
    
    _footerView = [[CustomSpinnerAnimationView alloc] initWithText:@"" withLoadingImage:[UIImage imageNamed:@"Spinner"]];
    
    _footerView.translatesAutoresizingMaskIntoConstraints = NO;
    [_footerView addConstraint:[NSLayoutConstraint constraintWithItem:_footerView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:155.0]];
    [_footerView addConstraint:[NSLayoutConstraint constraintWithItem:_footerView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:100.0]];
    _footerView.backgroundColor = [UIColor clearColor];
    [_tableView setNeedsLayout];
}


- (void)loadMore{
    
    [self makeAPICall];
}


//Method to get formatted date to use in api call making
- (NSString *)getFormatedDateFromDate:(NSDate *)date{
    
    NSString *dateFormat = @"yyyy-MM-dd";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
}


- (void)showLoadMoreFooterView{

    if(!_footerView){

        [self createFooterView];
    }

    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tableView.frame.size.width, 50)];
    [view addSubview:_footerView];
    [view addConstraint:[NSLayoutConstraint constraintWithItem:_footerView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [view addConstraint:[NSLayoutConstraint constraintWithItem:_footerView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];

    _tableView.tableFooterView = view;
}

- (void)showRetryFooterView{


    if(!_retryFooterView){

        _retryFooterView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                    0,
                                                                    _tableView.frame.size.width,
                                                                    50)];

        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:@"RETRY" forState:UIControlStateNormal];
        [button setTitleColor:[BrandColours colourTextBTPurplePrimaryColor] forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont fontWithName:kBtFontBold size:13.0]];
        [button addTarget:self action:@selector(retryBttonPressed) forControlEvents:UIControlEventTouchUpInside];
        button.layer.borderColor = [UIColor blackColor].CGColor;
        button.layer.borderWidth = 1.0;
        button.layer.cornerRadius = 5.0;

        button.frame = CGRectMake([UIScreen mainScreen].bounds.size.width/2 - 50,
                                  9,
                                  100,
                                  35);

        [_retryFooterView addSubview:button];
        [_retryFooterView addConstraint:[NSLayoutConstraint constraintWithItem:button attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:_retryFooterView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
        [_retryFooterView addConstraint:[NSLayoutConstraint constraintWithItem:button attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:_retryFooterView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    }


    _tableView.tableFooterView = _retryFooterView;

}

- (void)hideFooterView{

    _tableView.tableFooterView = nil;
}


- (void)scrollTableViewToBottom{

    if([_slotArray count] == 0)
        return;

    int lastRow = (int)[_tableView numberOfRowsInSection:0] - 1;
    [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:lastRow inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}


- (NSDate *)getDateOnlyFromDate:(NSDate *)inputDate{
    
    unsigned int flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:flags fromDate:inputDate];
    return [calendar dateFromComponents:components];
}


- (NSArray *)getFilteredArray:(NSArray *)inputArray{
    
    NSMutableArray *inputSlotsArray = [NSMutableArray arrayWithArray:inputArray];
    NSMutableArray *slotsToBeExcluded = [NSMutableArray array];
    
    for(BTFaultAppointment *appointment in inputSlotsArray){
        
        
        NSDate *date1 = [self getDateOnlyFromDate:appointment.dateInfo];
        NSDate *date2 = [self getDateOnlyFromDate:self.maxDate];
        
        if(([date1 compare:date2] == NSOrderedDescending))
        {
            [slotsToBeExcluded addObject:appointment];
        }
    }
    
    [inputSlotsArray removeObjectsInArray:slotsToBeExcluded];
    return inputSlotsArray;
    
}


- (void)createFooterActionView{
    
    //Footer actionview:Need Help Button
    
    if(!_amendFooterActionView)
        _amendFooterActionView = [[[NSBundle mainBundle] loadNibNamed:@"BTAmendFooterActionView" owner:self options:nil] firstObject];
    _amendFooterActionView.translatesAutoresizingMaskIntoConstraints = NO;
    _amendFooterActionView.delegate = self;
    [self.view addSubview:_amendFooterActionView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_amendFooterActionView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_amendFooterActionView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_amendFooterActionView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:45.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_amendFooterActionView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.bottomLayoutGuide  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    
    // [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_amendFooterActionView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    
}




#pragma mark- Action Method

//Method to be called on save button press
- (void)nextButtonPressed:(id)sender{
    
    [self.delegate btFaultAccessTimeSlotViewController:self didSelectDate:_selectedAppointmentSlotDate earliarTimeSlot:_selectedEarliestTimeSlot andLatestTimeSlot:_selectedLatestTimeSlot];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)closeButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)retryBttonPressed{
    
    if(![AppManager isInternetConnectionAvailable])
    {
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_FAULT_AMMEND_ENGG_APPOINTMENT];
        
        return;
    }

    
    [self hideFooterView];
    [self showLoadMoreFooterView];
    self.isLoading = YES;
    [_footerView stopAnimation];
    [_footerView startAnimation];
    [self loadMore];
    [self scrollTableViewToBottom];
}


#pragma mark- TableView Datasource and Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_slotArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BTFaultAccessTimeSlotCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kBTFaultAccessTimeSlotCellID];
    cell.delegate = self;
    
    BTFaultAppointment *faultAppointment = [_slotArray objectAtIndex:indexPath.row];
    
    //[cell updateWithSlotDate:faultAppointment.dateInfo earlierTime:_selectedEarliestTimeSlot latestTime:_selectedLatestTimeSlot andShowDetail:([_selectedAppointmentSlotDate compare:faultAppointment.dateInfo] == NSOrderedSame)];
    
    [cell updateWithSlotDate:faultAppointment.dateInfo earlierTime:_selectedEarliestTimeSlot latestTime:_selectedLatestTimeSlot andShowDetail:(_selectedRow == indexPath.row)];
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    _selectedRow = indexPath.row;
    
    BTFaultAppointment *faultAppointment = [_slotArray objectAtIndex:indexPath.row];
    _selectedAppointmentSlotDate  = faultAppointment.dateInfo;
    
    //Handle the case : row being selected is for curret date
    if([_viewModel appointmentDate:_selectedAppointmentSlotDate isSameAsDate:[NSDate date]]){
        
        //Same day
        NSArray *latestOptionArray = [_viewModel getEarliestTimeOptionArrayForDate:_selectedAppointmentSlotDate];
        
        if(![latestOptionArray containsObject:_selectedEarliestTimeSlot]){
            
            _selectedEarliestTimeSlot = kSelectTimeText;
            _selectedLatestTimeSlot = kSelectTimeText;
        }
        
    }
    
    
    [self.tableView reloadData];
    [self updateDataChangeStatus];
    
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:YES];
}


#pragma mark - UIScrollview Delegates Method

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    //NSInteger result = maximumOffset - currentOffset;
    
    // Change 10.0 to adjust the distance from bottom
    if (maximumOffset - currentOffset <= 10.0) {
        
        if (self.hasMoreData && !self.isLoading && !scrollView.dragging && !scrollView.decelerating)
        {
            
            if(![AppManager isInternetConnectionAvailable])
            {
                //if data available and internet is not reachable
                [self showRetryFooterView];
                 [AppManager trackNoInternetErrorOnPage:OMNIPAGE_FAULT_AMMEND_ENGG_APPOINTMENT];
                
            }
            else{
                
                _footerView = nil;
                [self createFooterView];
                [self showLoadMoreFooterView];
                self.isLoading = YES;
                [_footerView stopAnimation];
                [_footerView startAnimation];
                [self loadMore];
                self.networkRequestInProgress = NO;
                
            }
            
            [self scrollTableViewToBottom];
            
        }
    }
}




#pragma mark- BTAmendFooterActionViewDelegate method

- (void)userPressedOnBTAmendFooterActionView:(BTAmendFooterActionView *)view{
    
    
    BTNeedHelpViewController *helpViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"EngineerAppointmentNeedHelpViewController"];
    _isOpeningNeedHelp = YES;
    [self presentViewController:helpViewController animated:YES completion:nil];
}


#pragma mark- BTFaultAccessTimeSlotCellDelegate Method

- (void)btFaultAccessTimeSlotCell:(BTFaultAccessTimeSlotCell *)cell didSelectEarliestTimeView:(UIView *)earliestTimeView;{
    
    [self openPopoverForView:earliestTimeView andEariliestTime:YES];
}


- (void)btFaultAccessTimeSlotCell:(BTFaultAccessTimeSlotCell *)cell didLatestTimeView:(UIView *)latestTimeView{
    
    [self openPopoverForView:latestTimeView andEariliestTime:NO];
}


#pragma mark- CustomPopoverControllerDelegate mothod

- (void)popoverDismissAtCustomPopoverController:(CustomPopoverController *)controller{
    
    [self reloadSelectedCell];//Reloading cell to remove focus from time view
    _customPopover = nil;
}


- (void)customPopoverController:(CustomPopoverController *)controller didSelectTimeSlot:(NSString *)timeSlot{
    
    
    if(_customPopover.view.tag == kLatestSelectioTag){
        
        //faultAppointment.latestTime = timeSlot;
        _selectedLatestTimeSlot = timeSlot;

    }
    else{
        
        //faultAppointment.earliestTime = timeSlot;
        _selectedEarliestTimeSlot = timeSlot;
        
        if([_faultAppointmentDetail.acessTimeOptionArray containsObject:_selectedLatestTimeSlot]){
            
            if([_faultAppointmentDetail.acessTimeOptionArray indexOfObject:_selectedLatestTimeSlot] <= [_faultAppointmentDetail.acessTimeOptionArray indexOfObject:_selectedEarliestTimeSlot]){
                
                NSInteger indexForLatest = [_faultAppointmentDetail.acessTimeOptionArray indexOfObject:_selectedEarliestTimeSlot] + 1;
                
                if(indexForLatest < [_faultAppointmentDetail.acessTimeOptionArray count]){
                    
                    _selectedLatestTimeSlot = [_faultAppointmentDetail.acessTimeOptionArray objectAtIndex:indexForLatest];
                }
                
            }
            
        }
    }
    
    [_customPopover dismissViewControllerAnimated:YES completion:nil];
    [self reloadSelectedCell];//Reloading cell to update selected time
    [self updateDataChangeStatus];
    _customPopover = nil;
    
}




#pragma mark- DLMFaultAccessTimeSlotScreenDelegate

- (void)successfullyFetchedFaultAccessTimeSlotScreen:(DLMFaultAccessTimeSlotScreen *)faultAppointmentDetailScreen withFaultAppointmentDetail:(BTFaultAppointmentSlotDetail *)faultAppointmentDetail{
    
    
    if(faultAppointmentDetail){
        
        [_slotArray addObjectsFromArray:[self getFilteredArray:faultAppointmentDetail.faultAppointmentSlotArray]];

        [_tableView reloadData];
    }
    else if([_slotArray count] > 0){
        
        //We have some items but not getting more items from server
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"No further engineer appointment slots available." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:^{}];
        
    }

    
    
    if([_slotArray count] >= kAppointmentMaxSize){
        
        self.hasMoreData = NO;
        
    }
    else{
        
        self.hasMoreData = YES;
    }
    

    if(self.hasMoreData){
        
        BTFaultAppointment *appointment = [_slotArray lastObject];
        
        NSDate *date1 = [self getDateOnlyFromDate:appointment.dateInfo];
        NSDate *date2 = [self getDateOnlyFromDate:self.maxDate];
        
        if(([date1 compare:date2] == NSOrderedAscending))
        {
            
        }
        else{
            
            self.hasMoreData = NO;
        }
    }
    
     
    
    self.networkRequestInProgress = NO;
    _tableView.tableFooterView = nil;
    self.isLoading = NO;
    
}

- (void)faultAccessTimeSlotScreen:(DLMFaultAccessTimeSlotScreen *)faultAppointmentSlotScreen failedToFetchFaultAppointmentSlotWithWebServiceError:(NLWebServiceError *)webServiceError{
    
    
    _tableView.tableFooterView = nil;
    self.networkRequestInProgress = NO;
    self.isLoading = NO;
    
    
    if(_slotArray && [_slotArray count] > 0){
        
        
        BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
        
        if(errorHandled == NO)
        {
           [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_FAULT_AMMEND_ENGG_APPOINTMENT];
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"Some thing went wrong. please re-try after sometime." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:^{}];
        }
        
    }
    else
    {
        //If no data show retry view
        [self handleWebServiceError:webServiceError];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_FAULT_AMMEND_ENGG_APPOINTMENT];
    }


}

#pragma mark - Omniture Methods

- (void)trackOmniPage:(NSString *)page
{
    [OmnitureManager trackPage:page];
}



@end
