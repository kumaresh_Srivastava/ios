//
//  BTBaseViewController.m
//  BTBusinessApp
//
//  Created by VectoScalar on 10/30/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBaseViewController.h"
#import "MBProgressHUD.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "BTRetryView.h"
#import "AppConstants.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "NLWebServiceError.h"

@interface BTBaseViewController ()<BTRetryViewDelegate>
{

}
@end

@implementation BTBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self createLoadingView];
    
    
    __weak typeof(self) weakSelf = self;
    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
    

        if (weakSelf.networkRequestInProgress) {
            
            [weakSelf showProgressView];
            
                  }
        
        else {
           
            [weakSelf hideProgressView];
        }
        
    }];
    
    
    self.networkRequestInProgress = NO;
    
}


- (void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    [self cancelInProgressAPI];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark- Private Helper Method

- (void)cancelInProgressAPI{
    
    
}

- (void)hideLoadingItems:(BOOL)isHide {
    
    // (SD) Hide or Show UI elements related to loading.
    [_loadingView setHidden:isHide];
    
}

- (void)hideRetryItems:(BOOL)isHide {
    
    // (SD) Hide or Show UI elements related to retry.
    [_retryView setRetryViewDelegate:nil];
    [_retryView setHidden:isHide];
    _isRetryViewShown = NO;
}



#pragma mark - Public Helper Methods

- (void)handleError:(NSError *)error{
    
    self.networkRequestInProgress = false;
    
    if([error.domain isEqualToString:BTNetworkErrorDomain])
    {
        switch (error.code)
        {
            case BTNetworkErrorCodeLoginInvalid:
            {
                [self setNetworkRequestInProgress:false];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAppNeedsLockDownWithLoginScreen object:nil userInfo:nil];
                break;
            }
                
            case BTNetworkErrorCodeUserProfileLocked:
            {
                [self setNetworkRequestInProgress:false];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAppNeedsLockDownWithPINUnlockScreen object:nil userInfo:nil];
                break;
            }
                
            default:
            {
                //(SD) implement error handling here
                [self hideLoadingItems:YES];
                [_loadingView stopAnimatingLoadingIndicatorView];
                [self showRetryViewWithInternetStrip:NO];
                break;
            }
        }
    }
    else
    {
        //(SD) implement error handling here
        [self hideLoadingItems:YES];
        [_loadingView stopAnimatingLoadingIndicatorView];
        [self showRetryViewWithInternetStrip:NO];
    }
}


- (void)handleWebServiceError:(NLWebServiceError *)error{
    
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if(errorHandled == NO)
    {
        [self hideLoadingItems:YES];
        [_loadingView stopAnimatingLoadingIndicatorView];
        [self showRetryViewWithInternetStrip:NO];
    }
}


- (void)hideRetryView{
    
    [self hideRetryItems:YES];
}


- (UIBarButtonItem *)getCloseButtonItem{
    
    UIBarButtonItem *closeButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonPressed:)];
    
    return closeButtonItem;
}



- (UIBarButtonItem *)getConfirmButtonItem{
    
    UIBarButtonItem *confirmButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Confirm" style:UIBarButtonItemStylePlain target:self action:@selector(confirmButtonPressed:)];
    
    return confirmButtonItem;
}


- (UIBarButtonItem *)getSaveButtonItem{
    
    UIBarButtonItem *saveButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonPressed:)];
    
    return saveButtonItem;
}



/*

- (void)showRetryView {
    _isRetryViewShown = YES;
    _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
    _retryView.translatesAutoresizingMaskIntoConstraints = NO;
    _retryView.retryViewDelegate = self;
    
    [self.view addSubview:_retryView];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:-64.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
}

 */


- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow {
    
    if(!_retryView){
        
        _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
        _retryView.translatesAutoresizingMaskIntoConstraints = NO;
        _retryView.retryViewDelegate = self;
        [self.view addSubview:_retryView];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:-64.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    }
    
    _retryView.hidden = NO;
    _isRetryViewShown = YES;
    _retryView.retryViewDelegate = self;
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
}


- (void)showProgressView{
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideLoadingItems:NO];
        [_loadingView startAnimatingLoadingIndicatorView];
        
    });
    
}


- (void)hideProgressView{

    [self hideLoadingItems:YES];
     [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
    [_loadingView stopAnimatingLoadingIndicatorView];
}


#pragma mark- action methods

- (void)closeButtonPressed:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)confirmButtonPressed:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)saveButtonPressed:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark- ****************************Delegate Methods****************************

#pragma mark - RetryView Delegate

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    [self hideRetryItems:YES];
    
}


#pragma mark ErrorHandling

- (void)createLoadingView {
    
    if(_loadingView.isHidden == YES){
        return;
    }
    if(!_loadingView){
        
        _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.view addSubview:_loadingView];
    }
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}



@end
