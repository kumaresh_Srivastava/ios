//
//  BTNeedHelpViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 06/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTNeedHelpViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *alternateSiteContactDetailTextLabel;

@end
