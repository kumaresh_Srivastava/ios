//
//  BTNeedHelpViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 06/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTNeedHelpViewController.h"
#import "AppManager.h"
#import "OmnitureManager.h"
@interface BTNeedHelpViewController ()

@end

@implementation BTNeedHelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if(_alternateSiteContactDetailTextLabel)
    {
        NSRange rangeToBeChanged = NSMakeRange(0, 37);
        NSDictionary *attributeDic = @{
                                NSFontAttributeName:[UIFont boldSystemFontOfSize:15]
                                };
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:_alternateSiteContactDetailTextLabel.text];
        [attributedString setAttributes:attributeDic range:rangeToBeChanged];
        [_alternateSiteContactDetailTextLabel setAttributedText:attributedString];
    }
    
    [self trackOmniPage:OMNIPAGE_FAULT_AMEND_SITE_CONTACT_HELP];
    [self createAndAddCustomStatusBarView];
  }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
   
}

- (IBAction)cancelButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark - Private methods

- (void)createAndAddCustomStatusBarView
{
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIApplication sharedApplication].statusBarFrame.size.height)];

    statusBarView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:statusBarView];
}



- (void)trackOmniPage:(NSString *)page
{
    [OmnitureManager trackPage:page];
}

@end
