//
//  BTBaseViewController.h
//  BTBusinessApp
//
//  Created by VectoScalar on 10/30/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTRetryView.h"
#import "CustomSpinnerView.h"
@class NLWebServiceError;

@interface BTBaseViewController : UIViewController<BTRetryViewDelegate>{
    
    BTRetryView *_retryView;
    CustomSpinnerView *_loadingView;
    BOOL _isRetryViewShown;
}
@property(nonatomic, assign) BOOL networkRequestInProgress;
@property (strong, nonatomic) NSString *orderReference;
@property (strong, nonatomic) NSString *itemReference;
@property (strong, nonatomic) NSString *itemBBReferenceSIM2;
@property (strong, nonatomic) NSString *postalCode;

- (UIBarButtonItem *)getCloseButtonItem;
- (UIBarButtonItem *)getConfirmButtonItem;
- (UIBarButtonItem *)getSaveButtonItem;
- (void)handleError:(NSError *)error;
//- (void)showRetryView; 
- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow;
- (void)hideRetryView;
- (void)showProgressView;
- (void)hideProgressView;
- (void)handleWebServiceError:(NLWebServiceError *)error;
- (void)createLoadingView;

@end
