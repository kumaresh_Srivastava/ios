//
//  BTAmendViewController.m
//  SampleActionViewProject
//
//  Created by VectoScalar on 10/25/16.
//  Copyright © 2016 VectoScalar. All rights reserved.
//



#import "BTAmendViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface BTAmendViewController ()

@end

@implementation BTAmendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    //Register nib
    UINib *nib = [UINib nibWithNibName:kAmendTableViewCell bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kAmendTableViewCellID];
    
    //Auto height cell setup
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 108;
    
    //Hiding Default cell Separator
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    

    //Left navigation close button
    self.navigationItem.leftBarButtonItem = [self getCloseButtonItem];
    
    //Setting screen titke
    self.title = [self getScreenTitle];
    
    
    self.tableView.delegate = self;
    
    //Hiding back button
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    
    //Adding observer for data change
    __weak typeof(self) weakSelf = self;
    [RACObserve(self, self.isDataChanged) subscribeNext:^(NSNumber* state) {
        
        if (weakSelf.isDataChanged) {
            
            weakSelf.navigationItem.rightBarButtonItem = [weakSelf getConfirmButtonItem];
        }
        
        else {
            
            weakSelf.navigationItem.rightBarButtonItem = nil;
        }
    }];
    self.isDataChanged = NO;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc{


}



#pragma mark - Private Methods

- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow {

    if(!_retryView){

        _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
        _retryView.translatesAutoresizingMaskIntoConstraints = NO;
        _retryView.retryViewDelegate = self;
        [self.view addSubview:_retryView];

        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    }

    _retryView.hidden = NO;
    _isRetryViewShown = YES;
    _retryView.retryViewDelegate = self;
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
}



#pragma mark - UITableView DataSource & Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_dataArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    UITableViewCell *cell = [self getCellForIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(!cell){
        
        [NSException raise:@"Cell can't be nill" format:@""];
    }
    
    
    return cell;
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self cellSelectedAtIndexPath:indexPath];
}




#pragma mark- Helper Methods : Need to be overidden in child classes


- (void)setDataChangeStatus{
    
}

- (void)createData{
    
    
}


//Method to create cell
- (UITableViewCell *)getCellForIndexPath:(NSIndexPath *)indexPath{
    
    [NSException raise:@"getCellForIndexPath must be overridden in child class" format:@"%@", @"Cell cant be nil"];

    return nil;
}



- (NSString *)getScreenTitle{
    
    return nil;
}



- (void)cellSelectedAtIndexPath:(NSIndexPath *)indexPath{
    
    
}



@end
