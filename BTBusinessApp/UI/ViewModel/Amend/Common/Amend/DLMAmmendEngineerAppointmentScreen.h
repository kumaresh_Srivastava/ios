//
//  DLMAmmendEngineerAppointmentScreen.h
//  BTBusinessApp
//
//  Created by Accolite on 03/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
@class DLMAmmendEngineerAppointmentScreen;
@class  NLWebServiceError;
@protocol DLMAmmendEngineerAppointmentScreenDelegate <NSObject>

- (void)successfullyFetchedUserDataOnAmmendEngineerAppointmentScreen:(DLMAmmendEngineerAppointmentScreen *)appointmentScreen;

- (void)ammendEngineerAppointmentScreen:(DLMAmmendEngineerAppointmentScreen *)appointmentScreen failedToFetchUserDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface DLMAmmendEngineerAppointmentScreen : DLMObject
@property (nonatomic, weak) id<DLMAmmendEngineerAppointmentScreenDelegate> appointmentDelegate;
@property (nonatomic) NSArray *appointments;

- (void)fetchEngineerAppointmentsForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef appointmentDate:(NSString *)appointmentDate;
- (void)fetchEngineerAppointmentsForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef postCode:(NSString *)postCode appointmentDate:(NSString *)appointmentDate;
- (void)fetchEngineerAppointmentsForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef appointmentDate:(NSString *)appointmentDate andPCP:(BOOL)pcpOnly;
- (void)fetchEngineerAppointmentsForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef postCode:(NSString *)postCode appointmentDate:(NSString *)appointmentDate andPCP:(BOOL)pcpOnly;
- (void) cancelEngineerAppointmentsAPIRequest;

- (void)fetchEngineerPreAppointmentsForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef appointmentDate:(NSString *)appointmentDate;
- (void)fetchEngineerPreAppointmentsForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef postCode:(NSString *)postCode appointmentDate:(NSString *)appointmentDate;
- (void)fetchEngineerPreAppointmentsForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef appointmentDate:(NSString *)appointmentDate andPCP:(BOOL)pcpOnly;
- (void)fetchEngineerPreAppointmentsForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef postCode:(NSString *)postCode appointmentDate:(NSString *)appointmentDate andPCP:(BOOL)pcpOnly;

- (void) cancelEngineerPreAppointmentsAPIRequest;

@end
