//
//  BTAmendViewController.h
//  SampleActionViewProject
//
//  Created by VectoScalar on 10/25/16.
//  Copyright © 2016 VectoScalar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AmendTableViewCell.h"
#import "BTBaseViewController.h"

#define kAmendTableViewCell @"AmendTableViewCell"
#define kAmendTableViewCellID @"AmendTableViewCellID"
#define kAmendAttributeAppointment @"APPOINTMENT"
#define kAmendAttributeCustomerAgreedDate @"CUSTOMERAGREEDDATE"
#define kAmendAttributeSiteContact @"SITECONTACT"
#define kAmendAttributeAlterSiteContact @"ALTERNATESITECONTACT"

#define kEngineerAppointmentText @"Engineer appointment"
#define kSiteContactText @"Site contact"
#define kAlternateContactText @"Alternative contact"
#define kActivationDateText @"Activation date"

@interface BTAmendViewController : BTBaseViewController<UITableViewDelegate, UITableViewDataSource>{
    
    NSArray *_dataArray;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (copy, nonatomic) NSString *productName;
@property (assign, nonatomic) BOOL isDataChanged;

- (void)setDataChangeStatus;

@end
