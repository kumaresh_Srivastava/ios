//
//  AmendViewModel.m
//  SampleActionViewProject
//
//  Created by VectoScalar on 10/26/16.
//  Copyright © 2016 VectoScalar. All rights reserved.
//

#import "AmendViewModel.h"
#import "AppConstants.h"

@implementation AmendViewModel



- (NSMutableAttributedString *)getAttributtedStringForTextString:(NSString *)text andValuString:(NSString *)valueString{
    
    if(!valueString){
        valueString = @"";
    }
    
    UIFont *textFont = [UIFont fontWithName:kBtFontBold size:14.0];
    NSMutableDictionary *textDict = [NSMutableDictionary dictionaryWithObject:textFont forKey:NSFontAttributeName];
    [textDict setObject:[UIColor colorForHexString:@"333333"] forKey:NSForegroundColorAttributeName];
    
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:text attributes: textDict];
    
    UIFont *valueFont = [UIFont fontWithName:kBtFontRegular size:14.0];
    NSMutableDictionary *valueDict = [NSMutableDictionary dictionaryWithObject:valueFont forKey:NSFontAttributeName];
    [valueDict setObject:[UIColor colorForHexString:@"666666"] forKey:NSForegroundColorAttributeName];
    NSMutableAttributedString *vAttrString = [[NSMutableAttributedString alloc]initWithString:valueString attributes:valueDict];
    
    [aAttrString appendAttributedString:vAttrString];
    

    return aAttrString;
}


@end
