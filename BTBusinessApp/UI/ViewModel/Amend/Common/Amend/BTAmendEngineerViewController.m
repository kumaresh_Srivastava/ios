//
//  BTAmendEngineerViewController.m
//  BTBusinessApp
//
//  Created by VectoScalar on 10/28/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTAmendEngineerViewController.h"
#import "AmendEngineerTableViewCell.h"
#import "CustomSpinnerAnimationView.h"
#import "DLMAmmendEngineerAppointmentScreen.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "AppConstants.h"
#import "BTAmendFooterActionView.h"
#import "BTNeedHelpViewController.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "NLWebServiceError.h"

#define kAmendEngineerTableViewCell @"AmendEngineerTableViewCell"
#define kAmendEngineerTableViewCellID @"AmendEngineerTableViewCellID"

#define kAppointmentMaxSize 90

@interface BTAmendEngineerViewController ()<UITableViewDelegate, UITableViewDataSource, AmendEngineerTableViewCellDelegate,DLMAmmendEngineerAppointmentScreenDelegate, BTAmendFooterActionViewDelegate>{
    
    BTAppointment *_selectedModel;
    DLMAmmendEngineerAppointmentScreen *_appointmentModel;
    
    BTAppointment *_previousAppointment;
    BTAppointment *_currentSelectedAppointment;
    
    BTAmendFooterActionView *_amendFooterActionView;
    
    UIView *_retryFooterView;
    
    BOOL _isOpeningNeedHelp;
}
@property (assign, nonatomic) BOOL isLoading;
@property (assign, nonatomic) BOOL hasMoreData;

@end

@implementation BTAmendEngineerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Engineer appointment";
    
    //Register Nib
    UINib *nib = [UINib nibWithNibName:kAmendEngineerTableViewCell bundle:nil];
    [_tableView registerNib:nib forCellReuseIdentifier:kAmendEngineerTableViewCellID];
    
    
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 64.0;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.dataSource = self;
    _tableView.delegate = self;

    
    self.navigationItem.leftBarButtonItem = [self getCloseButtonItem];
    
    _dataArray = [[NSMutableArray alloc] init];
    
    
    //[self createFooterView];
    self.hasMoreData = YES;
    
    [self createFooterActionView];
    
    _previousAppointment = self.engineerAppointment;
    _currentSelectedAppointment = nil;// self.engineerAppointment;
    
    
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setDay:90];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    self.maxDate = [calendar dateByAddingComponents:dateComponents toDate:self.appointmendDate options:0];
    
    _appointmentModel = [[DLMAmmendEngineerAppointmentScreen alloc] init];
    _appointmentModel.appointmentDelegate = self;
    
    [self makeAPICall];
    
    __weak typeof(self) weakSelf = self;
    
    [RACObserve(self, self.isDataChanged) subscribeNext:^(NSNumber* state) {
        
        if (weakSelf.isDataChanged) {
            
            weakSelf.navigationItem.rightBarButtonItem = [weakSelf getSaveButtonItem];
        }
        
        else {
            
            weakSelf.navigationItem.rightBarButtonItem = nil;
        }
    }];
    
    self.isDataChanged = NO;
    if (!self.isFaultAccessed) {
        [self trackOmniPage:OMNIPAGE_ORDER_AMEND_APPOINTMENT];
        [self trackPageWithKeyTask:OMNI_KEYTASK_ORDER_TRACKER_ENGINEER_APPOINTMENT_CHECKED];
    }
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    _isOpeningNeedHelp = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark- Class Method

+ (BTAmendEngineerViewController *)getBTAmendEngineerViewController{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTAmendEngineerViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTAmendEngineerViewControllerID"];
    
    return vc;
}


#pragma mark- Super Class Methods

- (void)hideProgressView{
    
    [super hideProgressView];
    //_amendFooterActionView.hidden = NO;
}


- (void)saveButtonPressed:(id)sender{
    [self.delegate btAmendEngineerViewController:self didSelectAppointment:_currentSelectedAppointment];
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)cancelInProgressAPI{
    
    if(!_isOpeningNeedHelp){
        
        [_appointmentModel cancelEngineerAppointmentsAPIRequest];
    }

}


- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow{
    
    if(!_retryView){
        
        _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
        _retryView.translatesAutoresizingMaskIntoConstraints = NO;
        _retryView.retryViewDelegate = self;
        [self.view addSubview:_retryView];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_amendFooterActionView  attribute:NSLayoutAttributeTop multiplier:1.0 constant:5.0]];
    }
    
    _retryView.hidden = NO;
    _isRetryViewShown = YES;
    _retryView.retryViewDelegate = self;
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
}



#pragma mark- UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if(!_dataArray || [_dataArray count] == 0){
        
        return 0;
    }
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_dataArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AmendEngineerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kAmendEngineerTableViewCellID];
    cell.delegate = self;
    
    BTAppointment *appointment = [_dataArray objectAtIndex:indexPath.row];
    
    if(_currentSelectedAppointment && [_currentSelectedAppointment.dateInfo compare:appointment.dateInfo] == NSOrderedSame){
        
        appointment.isAMSelected = _currentSelectedAppointment.isAMSelected;
        appointment.isPMSelected = _currentSelectedAppointment.isPMSelected;
        _currentSelectedAppointment = appointment;
    }
    
    [cell updateWithBTEngineerAppointmentSlotModel:appointment];
    
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view = [[[NSBundle mainBundle] loadNibNamed:@"AmendEngineerTableSectionHeaderView" owner:self options:nil] firstObject];
    
    return view;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 36.0;
}



-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{

    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
   //NSInteger result = maximumOffset - currentOffset;

   // Change 10.0 to adjust the distance from bottom
    if (maximumOffset - currentOffset <= 10.0) {

        if (self.hasMoreData && !self.isLoading && !scrollView.dragging && !scrollView.decelerating)
       {           
           
           if(![AppManager isInternetConnectionAvailable])
           {
               //if data available and internet is not reachable
               [AppManager trackNoInternetErrorOnPage:OMNIPAGE_ORDER_AMEND_APPOINTMENT];
               [self showRetryFooterView];
    
           }
           else{
               
               footerView = nil;
               [self createFooterView];
               [self showLoadMoreFooterView];
               self.isLoading = YES;
               [footerView stopAnimation];
               [footerView startAnimation];
               [self loadMore];
               self.networkRequestInProgress = NO;
               
           }
    
           [self scrollTableViewToBottom];
           
       }
    }
}



#pragma mark - Helper Methods

- (void)createFooterActionView{
    
    //Footer actionview:Need Help Button
    
    if(!_amendFooterActionView)
         _amendFooterActionView = [[[NSBundle mainBundle] loadNibNamed:@"BTAmendFooterActionView" owner:self options:nil] firstObject];
    _amendFooterActionView.translatesAutoresizingMaskIntoConstraints = NO;
    _amendFooterActionView.delegate = self;
    [self.view addSubview:_amendFooterActionView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_amendFooterActionView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_amendFooterActionView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_amendFooterActionView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:45.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_amendFooterActionView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.bottomLayoutGuide  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    
   // [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_amendFooterActionView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    
}



- (void)makeAPICall{
    
    
    if(![AppManager isInternetConnectionAvailable]){
        
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_ORDER_AMEND_APPOINTMENT];
        if([_dataArray count] <= 0){
            
            [self showRetryViewWithInternetStrip:YES];
        }
        return;
    }
    
    [self hideRetryView];

    if([_dataArray count] <= 0){
        
        self.networkRequestInProgress = YES;
    }

    
    
    
    /*
    NSDate *date = self.appointmendDate;
    
    if([_dataArray count] > 0){
        
        BTAppointment *appointment = [_dataArray lastObject];
        date = appointment.dateInfo;
    }
    
    
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setDay:1];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *nextToLastDate = [calendar dateByAddingComponents:dateComponents toDate:date options:0];
    NSString *dateString = [self getFormatedDateFromDate:nextToLastDate];
    
     [_appointmentModel fetchSiteContactsForLoggedInUserWithOrderRef:self.orderReference itemRef:self.itemReference appointmentDate:dateString];
    
    */
    
    
    
     NSString *date = @"";
    
    if([_dataArray count] > 0){
        
        BTAppointment *appointment = [_dataArray lastObject];
    
        //Compare
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        [dateComponents setDay:1];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDate *nextToLastDate = [calendar dateByAddingComponents:dateComponents toDate:appointment.dateInfo options:0];
         
        date = [self getFormatedDateFromDate:nextToLastDate];
    }
    else{
        
        date = [self getFormatedDateFromDate:self.appointmendDate];
    }
    
    [_appointmentModel fetchEngineerAppointmentsForLoggedInUserWithOrderRef:self.orderReference itemRef:self.itemReference appointmentDate:date];
    
}


- (void)setUpDataChangeStatus{
    
    if(_currentSelectedAppointment && _previousAppointment){
        
        if(!([_currentSelectedAppointment.dateInfo compare:_previousAppointment.dateInfo] == NSOrderedSame)){
            
            self.isDataChanged = YES;
        }
        else if(_currentSelectedAppointment.isAMSelected != _previousAppointment.isAMSelected){
            
            self.isDataChanged = YES;
        }
        else if(_currentSelectedAppointment.isPMSelected != _previousAppointment.isPMSelected){
            
            self.isDataChanged = YES;
        }
        else{
            
            self.isDataChanged = NO;
        }
    }
}


- (void)createFooterView{
    
    
    footerView = [[CustomSpinnerAnimationView alloc] initWithText:@"" withLoadingImage:[UIImage imageNamed:@"Spinner"]];
    
    footerView.translatesAutoresizingMaskIntoConstraints = NO;
    [footerView addConstraint:[NSLayoutConstraint constraintWithItem:footerView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:155.0]];
    [footerView addConstraint:[NSLayoutConstraint constraintWithItem:footerView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:100.0]];
    footerView.backgroundColor = [UIColor clearColor];
    [_tableView setNeedsLayout];
    
}

- (void)loadMore{
    
    [self makeAPICall];
}


- (void)showProgressView{
    
    [super showProgressView];
    //_amendFooterActionView.hidden = YES;
}


- (NSString *)getFormatedDateFromDate:(NSDate *)date{
    
    NSString *dateFormat = @"dd-MM-yyyy";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
}


//State when no data to show
- (void)showZeroState{
    
    
    UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0,
                                                                             0,
                                                                             _tableView.bounds.size.width,
                                                                             _tableView.bounds.size.height)];
    noDataLabel.text             = @"NO ENGINEER APPOINTMENT SLOTS FOUND";
    noDataLabel.font = [UIFont fontWithName:kBtFontRegular size:15.0];
    noDataLabel.textColor        = [UIColor blackColor];
    noDataLabel.textAlignment    = NSTextAlignmentCenter;
    _tableView.backgroundView = noDataLabel;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.scrollEnabled = NO;
}

- (void)hideZeroState{
    
    _tableView.backgroundView = nil;
    _tableView.scrollEnabled = YES;
}


- (void)showLoadMoreFooterView{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tableView.frame.size.width, 50)];
    [view addSubview:footerView];
    [view addConstraint:[NSLayoutConstraint constraintWithItem:footerView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [view addConstraint:[NSLayoutConstraint constraintWithItem:footerView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    
    _tableView.tableFooterView = view;
}

- (void)showRetryFooterView{
    
    
    if(!_retryFooterView){
        
        _retryFooterView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                         0,
                                                         _tableView.frame.size.width,
                                                         50)];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:@"RETRY" forState:UIControlStateNormal];
        [button setTitleColor:[BrandColours colourTextBTPurplePrimaryColor] forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont fontWithName:kBtFontBold size:13.0]];
        [button addTarget:self action:@selector(retryBttonPressed) forControlEvents:UIControlEventTouchUpInside];
        button.layer.borderColor = [UIColor blackColor].CGColor;
        button.layer.borderWidth = 1.0;
        button.layer.cornerRadius = 5.0;

        button.frame = CGRectMake([UIScreen mainScreen].bounds.size.width/2 - 50,
                                  9,
                                  100,
                                  35);
        
        [_retryFooterView addSubview:button];
        [_retryFooterView addConstraint:[NSLayoutConstraint constraintWithItem:button attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:_retryFooterView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
        [_retryFooterView addConstraint:[NSLayoutConstraint constraintWithItem:button attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:_retryFooterView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    }

    
    _tableView.tableFooterView = _retryFooterView;
    
}

- (void)hideFooterView{
    
    _tableView.tableFooterView = nil;
}


- (void)retryBttonPressed{
    
    [self hideFooterView];
    [self showLoadMoreFooterView];
    [self loadMore];
    [self scrollTableViewToBottom];
}


- (void)scrollTableViewToBottom{
    
    if([_dataArray count] == 0)
        return;
        
    int lastRow = (int)[_tableView numberOfRowsInSection:0] - 1;
    [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:lastRow inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (NSArray *)getFilteredArray:(NSArray *)inputArray{
    
    NSMutableArray *inputSlotsArray = [NSMutableArray arrayWithArray:inputArray];
    NSMutableArray *slotsToBeExcluded = [NSMutableArray array];
    
    for(BTAppointment *appointment in inputSlotsArray){
        
        
        NSDate *date1 = [self getDateOnlyFromDate:appointment.dateInfo];
        NSDate *date2 = [self getDateOnlyFromDate:self.maxDate];
        
        if(([date1 compare:date2] == NSOrderedDescending))
        {
            [slotsToBeExcluded addObject:appointment];
        }
    }
    
    [inputSlotsArray removeObjectsInArray:slotsToBeExcluded];
    return inputSlotsArray;

}


- (NSDate *)getDateOnlyFromDate:(NSDate *)inputDate{

    unsigned int flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:flags fromDate:inputDate];
    return [calendar dateFromComponents:components];
}

- (void)updateSlotAsPerCurrentAppointment{
    
    
    //Updating slot as per current appointment
    
    NSDate *currentAppointmentDate = self.appointmendDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger comps = (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay);
    
    NSDateComponents *currAppointmentComponent = [calendar components:comps
                                                             fromDate:currentAppointmentDate];
    
    BTAppointment *appointmentNeedToBeExcluded = nil;
    
    //NSMutableArray *excludedAppointment = [NSMutableArray array];
    
    for(BTAppointment *appointment in _dataArray){
        
        NSDateComponents *appointmentComponent = [calendar components:comps
                                                             fromDate:appointment.dateInfo];
        
        NSDateComponents *currDateComponent = [calendar components:comps
                                                          fromDate:[NSDate date]];
        
        NSDate *appointmentDate = [calendar dateFromComponents:appointmentComponent];
        NSDate *currentAppointmentDate = [calendar dateFromComponents:currAppointmentComponent];
        NSDate *currentDate = [calendar dateFromComponents:currDateComponent];
        
        NSComparisonResult result1 = [appointmentDate compare:currentDate];
        
        
        
        /*if(result1 == NSOrderedSame){
            
            NSInteger hour = [AppManager getHourFromDate:self.appointmendDate];
            
            //Dates are same
            if (hour < 13) {
                //Current appointment with AM
                
                [appointment setHasAM:NO];
                
                
            } else{
                
                //Current Appointment with PM
                [excludedAppointment addObject:appointment];
            }
            
        }*/
        if(result1 == NSOrderedSame ){
            
            NSInteger currentHour = [AppManager getHourFromDate:[NSDate date]];
            
            if( currentHour < 13) {
                
                NSComparisonResult result2 = [currentDate compare:currentAppointmentDate];
                if(result2 == NSOrderedSame)
                {
                    NSInteger hour = [AppManager getHourFromDate:currentAppointmentDate];
                    if(hour >= 13 ){
                        appointmentNeedToBeExcluded = appointment;
                    }
                }
                [appointment setHasAM:NO];
                
            } else {
                appointmentNeedToBeExcluded = appointment;
            }
            
        }
        
    }
    
    
    if(appointmentNeedToBeExcluded){
        
        [_dataArray removeObject:appointmentNeedToBeExcluded];
    }
    
}

#pragma mark - Omniture reporting related methods

- (void)trackOmniPage:(NSString *)page
{
    NSMutableDictionary *contextDict = [NSMutableDictionary dictionary];
    
    NSString *orderRef = [OmnitureManager getOrderRefFormattedString:self.orderReference];
    
    [contextDict setValue:@"Logged In" forKey:kOmniLoginStatus];
    [contextDict setValue:orderRef forKey:kOmniOrderRef];
    
    [OmnitureManager trackPage:page withContextInfo:contextDict];
}

- (void)trackPageWithKeyTask:(NSString *)keytask
{
    NSMutableDictionary *contextDict = [NSMutableDictionary dictionary];
    
    NSString *orderRef = [OmnitureManager getOrderRefFormattedString:self.orderReference];
    [contextDict setValue:orderRef forKey:kOmniOrderRef];
    NSString *pageName = OMNIPAGE_ORDER_AMEND_APPOINTMENT;
    [contextDict setValue:keytask forKey:kOmniKeyTask];
    
    [OmnitureManager trackPage:pageName withContextInfo:contextDict];
}


#pragma mark - AmendEngineerTableViewCellDelegate Methods

- (void)amendEngineerTableViewCell:(AmendEngineerTableViewCell *)cell didToggleShiftForModel:(BTAppointment *)model{
    
    
    if(_currentSelectedAppointment && [[_currentSelectedAppointment dateInfo] compare:model.dateInfo] != NSOrderedSame)
    {
        _currentSelectedAppointment.isAMSelected = NO;
        _currentSelectedAppointment.isPMSelected = NO;
        
    }
    
    _currentSelectedAppointment = model;
    [_tableView reloadData];
    
    [self setUpDataChangeStatus];
    
}

- (void)amendFaultEngineerTableViewCell:(AmendEngineerTableViewCell *)cell didToggleShiftForModel:(BTFaultAppointment *)model
{
    
}

- (void)userPressedOnBTAmendFooterActionView:(BTAmendFooterActionView *)view{
    
    
    [self trackOmniPage:OMNIPAGE_ORDER_AMEND_APPOINTMENT_HELP];
    
    BTNeedHelpViewController *helpViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"EngineerAppointmentNeedHelpViewController"];
    _isOpeningNeedHelp = YES;
    [self presentViewController:helpViewController animated:YES completion:nil];
}


#pragma mark - RetryView Delegate

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView{
    
    [self makeAPICall];
    
}


#pragma mark DLMAmmendEngineerAppointmentScreenDelegate methods

- (void)successfullyFetchedUserDataOnAmmendEngineerAppointmentScreen:(DLMAmmendEngineerAppointmentScreen *)appointmentScreen {
    
    
    [self hideZeroState];
    
    if(appointmentScreen && appointmentScreen.appointments && [appointmentScreen.appointments count] > 0){
        
        
        NSMutableArray *appointmentsArray = [NSMutableArray arrayWithArray:appointmentScreen.appointments];
        
        /*
        
        BTAppointment *firstAppointment = [appointmentsArray firstObject];
        
        NSDate *date1 = [self getDateOnlyFromDate:firstAppointment.dateInfo];
        NSDate *date2 = [self getDateOnlyFromDate:self.appointmendDate];
        
        if([_dataArray count] == 0 && [date1 compare:date2] == NSOrderedSame){
            
            
            if(self.engineerAppointment.isAMSelected){
                
                firstAppointment.isServerSelectedAM = YES;
            }
            else{
                
                [appointmentsArray removeObject:firstAppointment];
            }

        }
        
        */

        
        NSArray *filteredArray = [self getFilteredArray:appointmentsArray];
        
        [_dataArray addObjectsFromArray:filteredArray];
        
        [self updateSlotAsPerCurrentAppointment];

        [_tableView reloadData];
    }
    else if([_dataArray count] > 0){
        
        //We have some items but not getting more items from server
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"No further engineer appointment slots available." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:^{}];
        
    }
    else{
        
        //Show zero state
        [self showZeroState];

    }
    
    
    if([_dataArray count] >= kAppointmentMaxSize){
        
        self.hasMoreData = NO;
        
    }
    else{
        
        self.hasMoreData = YES;
    }
    
    
    
    if(self.hasMoreData){
        
        //Compare
        
        BTAppointment *appointment = [_dataArray lastObject];
        
        NSDate *date1 = [self getDateOnlyFromDate:appointment.dateInfo];
        NSDate *date2 = [self getDateOnlyFromDate:self.maxDate];
        
        if(([date1 compare:date2] == NSOrderedAscending)) //
        {
            
        }
        else{
            
            self.hasMoreData = NO;
        }
        
    }

    
    self.networkRequestInProgress = NO;
    _tableView.tableFooterView = nil;
    self.isLoading = NO;
    
}

- (void)ammendEngineerAppointmentScreen:(DLMAmmendEngineerAppointmentScreen *)appointmentScreen failedToFetchUserDataWithWebServiceError:(NLWebServiceError *)webServiceError {
    DDLogError(@"Appointments: Fetch User details failed");
    
    _tableView.tableFooterView = nil;
    self.networkRequestInProgress = NO;
    self.isLoading = NO;
    
    if(_dataArray && [_dataArray count] > 0){

        
        BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
        
        if(errorHandled == NO)
        {
            [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_ORDER_AMEND_APPOINTMENT];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Message" message:kDefaultErrorMessage preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:^{}];
        }
        
    }
    else
    {
        //If no data show retry view
        [self handleWebServiceError:webServiceError];
    }
}

@end
