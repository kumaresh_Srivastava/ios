//
//  AmendViewModel.h
//  SampleActionViewProject
//
//  Created by VectoScalar on 10/26/16.
//  Copyright © 2016 VectoScalar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AmendViewModel : NSObject

@property (nonatomic, strong) NSString *amendContext;
@property (nonatomic, strong) NSAttributedString *subHeading1;
@property (nonatomic, strong) NSAttributedString *subHeading2;
@property (nonatomic, assign) BOOL isEditable;
@property (nonatomic, readonly) BOOL isChaged;

- (NSMutableAttributedString *)getAttributtedStringForTextString:(NSString *)text andValuString:(NSString *)valueString;

@end
