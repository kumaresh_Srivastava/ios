//
//  DLMAmmendEngineerAppointmentScreen.m
//  BTBusinessApp
//
//  Created by Accolite on 03/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMAmmendEngineerAppointmentScreen.h"
#import "AppDelegate.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppDelegateViewModel.h"
#import "AppConstants.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"

#import "NLEngineerAppointmentWebService.h"
#import "NLEngineerPreAppointmentWebService.h"

@interface DLMAmmendEngineerAppointmentScreen () <NLEngineerAppointmentWebServiceDelegate>
@property (nonatomic) NLEngineerAppointmentWebService *getEngineerAppointmentWebService;
@property (nonatomic) NLEngineerPreAppointmentWebService *getEngineerPreAppointmentWebService;
@end

@implementation DLMAmmendEngineerAppointmentScreen

#pragma mark - Public methods

// get appointment slots from current appointment onwards
- (void)fetchEngineerAppointmentsForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef appointmentDate:(NSString *)appointmentDate
{
    self.getEngineerAppointmentWebService = [[NLEngineerAppointmentWebService alloc] initWithOrderRef:orderRef itemRef:itemRef andAppointmentDate:appointmentDate];
    self.getEngineerAppointmentWebService.getEngineerAppointmentWebServiceDelegate = self;
    [self.getEngineerAppointmentWebService resume];
}

- (void)fetchEngineerAppointmentsForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef appointmentDate:(NSString *)appointmentDate andPCP:(BOOL)pcpOnly
{
    self.getEngineerAppointmentWebService = [[NLEngineerAppointmentWebService alloc] initWithOrderRef:orderRef itemRef:itemRef appointmentDate:appointmentDate andPCP:pcpOnly];
    self.getEngineerAppointmentWebService.getEngineerAppointmentWebServiceDelegate = self;
    [self.getEngineerAppointmentWebService resume];
}


- (void)fetchEngineerAppointmentsForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef postCode:(NSString *)postCode appointmentDate:(NSString *)appointmentDate
{
    self.getEngineerAppointmentWebService = [[NLEngineerAppointmentWebService alloc] initWithOrderRef:orderRef itemRef:itemRef postCode:postCode andAppointmentDate:appointmentDate];
    self.getEngineerAppointmentWebService.getEngineerAppointmentWebServiceDelegate = self;
    [self.getEngineerAppointmentWebService resume];
}

- (void)fetchEngineerAppointmentsForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef postCode:(NSString *)postCode appointmentDate:(NSString *)appointmentDate andPCP:(BOOL)pcpOnly
{
    self.getEngineerAppointmentWebService = [[NLEngineerAppointmentWebService alloc] initWithOrderRef:orderRef itemRef:itemRef postCode:postCode appointmentDate:appointmentDate andPCP:pcpOnly];
    self.getEngineerAppointmentWebService.getEngineerAppointmentWebServiceDelegate = self;
    [self.getEngineerAppointmentWebService resume];
}


- (void) cancelEngineerAppointmentsAPIRequest {
    self.getEngineerAppointmentWebService.getEngineerAppointmentWebServiceDelegate = nil;
    [self.getEngineerAppointmentWebService cancel];
    self.getEngineerAppointmentWebService = nil;
}


// get appointment slots for bringing forward an appointment (preponing)
- (void)fetchEngineerPreAppointmentsForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef appointmentDate:(NSString *)appointmentDate
{
    self.getEngineerPreAppointmentWebService = [[NLEngineerPreAppointmentWebService alloc] initWithOrderRef:orderRef itemRef:itemRef andAppointmentDate:appointmentDate];
    self.getEngineerPreAppointmentWebService.getEngineerAppointmentWebServiceDelegate = self;
    [self.getEngineerPreAppointmentWebService resume];
}

- (void)fetchEngineerPreAppointmentsForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef appointmentDate:(NSString *)appointmentDate andPCP:(BOOL)pcpOnly
{
    self.getEngineerPreAppointmentWebService = [[NLEngineerPreAppointmentWebService alloc] initWithOrderRef:orderRef itemRef:itemRef appointmentDate:appointmentDate andPCP:pcpOnly];
    self.getEngineerPreAppointmentWebService.getEngineerAppointmentWebServiceDelegate = self;
    [self.getEngineerPreAppointmentWebService resume];
}


- (void)fetchEngineerPreAppointmentsForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef postCode:(NSString *)postCode appointmentDate:(NSString *)appointmentDate
{
    self.getEngineerPreAppointmentWebService = [[NLEngineerPreAppointmentWebService alloc] initWithOrderRef:orderRef itemRef:itemRef postCode:postCode andAppointmentDate:appointmentDate];
    self.getEngineerPreAppointmentWebService.getEngineerAppointmentWebServiceDelegate = self;
    [self.getEngineerPreAppointmentWebService resume];
}

- (void)fetchEngineerPreAppointmentsForLoggedInUserWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef postCode:(NSString *)postCode appointmentDate:(NSString *)appointmentDate andPCP:(BOOL)pcpOnly
{
    self.getEngineerPreAppointmentWebService = [[NLEngineerPreAppointmentWebService alloc] initWithOrderRef:orderRef itemRef:itemRef postCode:postCode appointmentDate:appointmentDate andPCP:pcpOnly];
    self.getEngineerPreAppointmentWebService.getEngineerAppointmentWebServiceDelegate = self;
    [self.getEngineerPreAppointmentWebService resume];
}

- (void) cancelEngineerPreAppointmentsAPIRequest {
    self.getEngineerPreAppointmentWebService.getEngineerAppointmentWebServiceDelegate = nil;
    [self.getEngineerPreAppointmentWebService cancel];
    self.getEngineerPreAppointmentWebService = nil;
}

#pragma mark - NLEngineerAppointmentWebServiceDelegate Methods

- (void)getEngineerAppointmentsWebService:(NLEngineerAppointmentWebService *)webService successfullyFetchedEngineerAppointments:(NSArray *)appointments
{
    if(webService == self.getEngineerAppointmentWebService)
    {
        [self setAppointments:appointments];
        [self.appointmentDelegate successfullyFetchedUserDataOnAmmendEngineerAppointmentScreen:self];
        
        self.getEngineerAppointmentWebService.getEngineerAppointmentWebServiceDelegate = nil;
        self.getEngineerAppointmentWebService = nil;
    }
    else if (webService == self.getEngineerPreAppointmentWebService)
    {
        [self setAppointments:appointments];
        [self.appointmentDelegate successfullyFetchedUserDataOnAmmendEngineerAppointmentScreen:self];
        
        self.getEngineerPreAppointmentWebService.getEngineerAppointmentWebServiceDelegate = nil;
        self.getEngineerPreAppointmentWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLEngineerAppointmentWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLEngineerAppointmentWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        
    }

}


- (void)getEngineerAppointmentsWebService:(NLEngineerAppointmentWebService *)webService failedToFetchEngineerAppointmentsWithWebServiceError:(NLWebServiceError *)webServiceError
{
    if(webService == self.getEngineerAppointmentWebService)
    {
         [self.appointmentDelegate ammendEngineerAppointmentScreen:self failedToFetchUserDataWithWebServiceError:webServiceError];
        
        self.getEngineerAppointmentWebService.getEngineerAppointmentWebServiceDelegate = nil;
        self.getEngineerAppointmentWebService = nil;
    }
    else if (webService == self.getEngineerPreAppointmentWebService)
    {
        [self.appointmentDelegate ammendEngineerAppointmentScreen:self failedToFetchUserDataWithWebServiceError:webServiceError];
        
        self.getEngineerPreAppointmentWebService.getEngineerAppointmentWebServiceDelegate = nil;
        self.getEngineerPreAppointmentWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLEngineerAppointmentWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLEngineerAppointmentWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        
    }
}

@end
