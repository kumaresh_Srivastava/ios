//
//  BTAmendEngineerViewController.h
//  BTBusinessApp
//
//  Created by VectoScalar on 10/28/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseViewController.h"
#import "BTAppointment.h"
#import "BTFaultAppointment.h"
#import "CustomSpinnerAnimationView.h"
#import "BTFaultAppointmentDetail.h"

@class BTAmendEngineerViewController;

@protocol BTAmendEngineerViewControllerDelegate <NSObject>

@optional

@optional
- (void)btAmendEngineerViewController:(BTAmendEngineerViewController *)controller didSelectAppointment:(BTAppointment *)appointment;

- (void)btAmendEngineerViewController:(BTAmendEngineerViewController *)controller didSelectFaultAppointment:(BTFaultAppointment *)faultAppointment;


@end


@interface BTAmendEngineerViewController : BTBaseViewController{
    
    __weak IBOutlet UITableView *_tableView;
    NSMutableArray *_dataArray;
    CustomSpinnerAnimationView *footerView;
}

@property(nonatomic, strong) BTAppointment *engineerAppointment;
@property(nonatomic, strong) NSDate *appointmendDate;
@property(nonatomic, strong) NSDate *maxDate;
@property (assign, nonatomic) BOOL isDataChanged;
@property(nonatomic, assign) id<BTAmendEngineerViewControllerDelegate>delegate;
@property (nonatomic,assign) BOOL isFaultAccessed;

+ (BTAmendEngineerViewController *)getBTAmendEngineerViewController;
- (void)showRetryFooterView;
- (NSArray *)getFilteredArray:(NSArray *)inputArray;
@end
