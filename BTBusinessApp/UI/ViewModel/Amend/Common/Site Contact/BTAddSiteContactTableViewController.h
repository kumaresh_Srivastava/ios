//
//  BTAddSiteContactTableViewController.h
//  SampleActionViewProject
//
//  Created by VectoScalar on 10/27/16.
//  Copyright © 2016 VectoScalar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMAddNewContactScreen.h"
#import "BTTextFiledModel.h"
#import "AddContactTableViewCell.h"
#import "BTFaultContactDetails.h"
#import "DLMAddNewContactScreen.h"

#define kAddContactTableViewCell @"AddContactTableViewCell"
#define kAddContactTableViewCellID @"AddContactTableViewCellID"

@class BTAddSiteContactTableViewController;

@protocol BTAddSiteContactTableViewControllerDelegare <NSObject>

@optional

- (void)btAddSiteContactTableViewController:(BTAddSiteContactTableViewController *)controller didSubmitUserData:(NSDictionary *)dataDict;

- (void)btAddSiteContactTableViewController:(BTAddSiteContactTableViewController *)controller didSubmitContactDetail:(BTFaultContactDetails *)contactDetail;

- (void)btAddSiteContactTableViewController:(BTAddSiteContactTableViewController *)controller didChangeData:(BOOL)isDataChanged;

@end

@interface BTAddSiteContactTableViewController : UITableViewController<AddContactTableViewCellDelegate>{
    
    NSArray *_dataArray;
    DLMAddNewContactScreen *_addNewContactScreenModel;
    BTTextFiledModel *_currectSelectedTexFieldtModel;
}

@property(nonatomic, weak) id<BTAddSiteContactTableViewControllerDelegare>delegate;
@property(nonatomic, strong) NSDictionary *siteContactDict;

+ (BTAddSiteContactTableViewController *)getBTAddSiteContactTableViewController;
- (void)saveContactInfo;

@end
