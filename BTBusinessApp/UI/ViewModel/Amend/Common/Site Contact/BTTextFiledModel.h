//
//  BTTextFiledModel.h
//  BTBusinessApp
//
//  Created by VectoScalar on 10/27/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define kDropDownDefaultText @"Select"
#define kAddContactContact @"addContact"

typedef enum {
    TextFiledTypeUnknown,
    TextFiledTypePassword,
    TextFiledTypeEmail,
    TextFiledTypeEmailOptional,
    TextFiledTypePhone,
    TextFiledTypePhoneOptional,
    TextFiledTypeName,
    TextFiledLastName,
    TextFiledTypeDropDown,
    TextFiledTypePhoneMandatory
    
}TextFiledType;

@interface BTTextFiledModel : NSObject
@property(nonatomic, assign) TextFiledType textFieldType;
@property(nonatomic, strong) NSString *textFieldName;
@property(nonatomic, strong) NSString *errorMessage;
//Quick Fix for Settings Error Validation
@property (nonatomic,strong) NSString *errorMessageForSettings;
@property(nonatomic, strong) NSString *inputText;
@property(nonatomic, assign) UIKeyboardType keyBoardType;
@property(nonatomic, assign) UIReturnKeyType returnKeyType;
@property(nonatomic, assign) UITextField *correspondingTextField;
@property(nonatomic, assign) NSString *regex;
@property(nonatomic, assign) BOOL isDisabled;


//Talikng for quick fig of a bug
@property(nonatomic, strong) NSString *dependentCondition;
@property(nonatomic, strong) NSString *screenName;

- (BOOL)isValid;
- (void)makeFirstResponder;
- (void)resignFirstResponder;


@end
