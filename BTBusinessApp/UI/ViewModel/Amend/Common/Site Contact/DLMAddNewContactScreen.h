//
//  DLMAddNewContactScreen.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"

#define kTitleText @"Title"
#define kFirstNameText @"First name"
#define kLastNameText @"Last name"
#define kPhoneText @"Number you want us to call on"
#define kAltPhoneText @"Alternative phone number(optional)"
#define kEmailText @"Email"

#define kMobileText @"Mobile contact number"
#define kOptionalPhoneText @"Phone contact number (Optional)"

#define kUserTitleKey @"Title"
#define kUserFirstNameKey @"FirstName"
#define kUserLastNameKey @"LastName"
#define kUserPhoneKey @"Phone"
#define kUserAltPhoneKey @"altPhone"
#define kUserEmailKey @"Email"


@interface DLMAddNewContactScreen : DLMObject{
    
    NSArray *_textFieldsModelArray;
}

- (id)initWithContactDict:(NSDictionary *)siteContactDict;
- (NSArray *)getTextFieldsModelsArray;
- (NSArray *)getTitlesArray;

@end
