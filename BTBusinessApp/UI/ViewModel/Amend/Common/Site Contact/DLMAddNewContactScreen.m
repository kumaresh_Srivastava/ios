//
//  DLMAddNewContactScreen.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMAddNewContactScreen.h"
#import "BTTextFiledModel.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"


@interface DLMAddNewContactScreen(){
    
    NSArray *_titlesArray;
}

@end


@implementation DLMAddNewContactScreen


#pragma mark- public methods

- (id)initWithContactDict:(NSDictionary *)siteContactDict{
    
    self = [super init];
    
    if(self){
        
        [self createDataForSiteContactDict:siteContactDict];
    }
    
    
    return self;
}

- (NSArray *)getTextFieldsModelsArray{
    
    return _textFieldsModelArray;
}

- (NSArray *)getTitlesArray{
    
    
    if(!_titlesArray)
    {
        
        _titlesArray = [AppManager getBTUserTitles];
        
    }
    
    return _titlesArray;
}


#pragma mark- helper method

- (void)createDataForSiteContactDict:(NSDictionary *)siteContactDict{
    
    
    NSMutableArray *tempDataArray = [NSMutableArray array];
    
    if(siteContactDict){
        
        //Select Title
        BTTextFiledModel *selectTitleFieldModel = [[BTTextFiledModel alloc] init];
        selectTitleFieldModel.textFieldType = TextFiledTypeDropDown;
        selectTitleFieldModel.keyBoardType = UIKeyboardTypeDefault;
        selectTitleFieldModel.returnKeyType = UIReturnKeyNext;
        selectTitleFieldModel.textFieldName = kTitleText;
        selectTitleFieldModel.inputText = [siteContactDict valueForKey:kUserTitleKey];
        [tempDataArray addObject:selectTitleFieldModel];
        
        //First Name Field Data
        BTTextFiledModel *firstNameFieldModel = [[BTTextFiledModel alloc] init];
        firstNameFieldModel.textFieldType = TextFiledTypeName;
        firstNameFieldModel.keyBoardType = UIKeyboardTypeDefault;
        firstNameFieldModel.returnKeyType = UIReturnKeyNext;
        firstNameFieldModel.textFieldName = kFirstNameText;
        firstNameFieldModel.inputText = [siteContactDict valueForKey:kUserFirstNameKey];
        [tempDataArray addObject:firstNameFieldModel];
        
        //Last Name Field Data
        BTTextFiledModel *lastNameFieldModel = [[BTTextFiledModel alloc] init];
        lastNameFieldModel.textFieldType = TextFiledLastName;
        lastNameFieldModel.keyBoardType = UIKeyboardTypeDefault;
        lastNameFieldModel.returnKeyType = UIReturnKeyNext;
        lastNameFieldModel.textFieldName = kLastNameText;
        lastNameFieldModel.inputText = [siteContactDict valueForKey:kUserLastNameKey];
        [tempDataArray addObject:lastNameFieldModel];
        
        
        
        //Phone Field Data
        BTTextFiledModel *phoneFieldModel = [[BTTextFiledModel alloc] init];
        phoneFieldModel.textFieldType = TextFiledTypePhone;
        phoneFieldModel.keyBoardType = UIKeyboardTypePhonePad;
        phoneFieldModel.returnKeyType = UIReturnKeyNext;
        phoneFieldModel.textFieldName = kPhoneText;
        phoneFieldModel.inputText = [siteContactDict valueForKey:kUserPhoneKey];
        [tempDataArray addObject:phoneFieldModel];
        
        //Alt Phone Field Data
        BTTextFiledModel *altPhoneFieldModel = [[BTTextFiledModel alloc] init];
        altPhoneFieldModel.textFieldType = TextFiledTypePhoneOptional;
        altPhoneFieldModel.keyBoardType = UIKeyboardTypePhonePad;
        altPhoneFieldModel.returnKeyType = UIReturnKeyNext;
        altPhoneFieldModel.textFieldName = kAltPhoneText;
        altPhoneFieldModel.inputText = [siteContactDict valueForKey:kUserAltPhoneKey];
        [tempDataArray addObject:altPhoneFieldModel];
        
        
        
        
        //Email Field Data
        BTTextFiledModel *emailFieldModel = [[BTTextFiledModel alloc] init];
        emailFieldModel.textFieldType = TextFiledTypeEmail;
        emailFieldModel.keyBoardType = UIKeyboardTypeEmailAddress;
        emailFieldModel.returnKeyType = UIReturnKeyDone;
        emailFieldModel.textFieldName = kEmailText;
        emailFieldModel.inputText = [siteContactDict valueForKey:kUserEmailKey];;
        [tempDataArray addObject:emailFieldModel];
        
        
        _textFieldsModelArray = tempDataArray;
        
    }
    else{
        
        
        //Select Title
        BTTextFiledModel *selectTitleFieldModel = [[BTTextFiledModel alloc] init];
        selectTitleFieldModel.textFieldType = TextFiledTypeDropDown;
        selectTitleFieldModel.keyBoardType = UIKeyboardTypeDefault;
        selectTitleFieldModel.returnKeyType = UIReturnKeyNext;
        selectTitleFieldModel.textFieldName = kTitleText;
        selectTitleFieldModel.inputText = kDropDownDefaultText;
        [tempDataArray addObject:selectTitleFieldModel];
        
        //First Name Field Data
        BTTextFiledModel *firstNameFieldModel = [[BTTextFiledModel alloc] init];
        firstNameFieldModel.textFieldType = TextFiledTypeName;
        firstNameFieldModel.keyBoardType = UIKeyboardTypeDefault;
        firstNameFieldModel.returnKeyType = UIReturnKeyNext;
        firstNameFieldModel.textFieldName = kFirstNameText;
        [tempDataArray addObject:firstNameFieldModel];
        
        //Last Name Field Data
        BTTextFiledModel *lastNameFieldModel = [[BTTextFiledModel alloc] init];
        lastNameFieldModel.textFieldType = TextFiledLastName;
        lastNameFieldModel.keyBoardType = UIKeyboardTypeDefault;
        lastNameFieldModel.returnKeyType = UIReturnKeyNext;
        lastNameFieldModel.textFieldName = kLastNameText;
        [tempDataArray addObject:lastNameFieldModel];
        
        
        
        //Phone Field Data
        BTTextFiledModel *phoneFieldModel = [[BTTextFiledModel alloc] init];
        phoneFieldModel.textFieldType = TextFiledTypePhone;
        phoneFieldModel.keyBoardType = UIKeyboardTypePhonePad;
        phoneFieldModel.returnKeyType = UIReturnKeyNext;
        phoneFieldModel.textFieldName = kPhoneText;
        [tempDataArray addObject:phoneFieldModel];
        
        //Alt Phone Field Data
        BTTextFiledModel *altPhoneFieldModel = [[BTTextFiledModel alloc] init];
        altPhoneFieldModel.textFieldType = TextFiledTypePhoneOptional;
        altPhoneFieldModel.keyBoardType = UIKeyboardTypePhonePad;
        altPhoneFieldModel.returnKeyType = UIReturnKeyNext;
        altPhoneFieldModel.textFieldName = kAltPhoneText;
        [tempDataArray addObject:altPhoneFieldModel];
        
        
        
        //Email Field Data
        BTTextFiledModel *emailFieldModel = [[BTTextFiledModel alloc] init];
        emailFieldModel.textFieldType = TextFiledTypeEmail;
        emailFieldModel.keyBoardType = UIKeyboardTypeEmailAddress;
        emailFieldModel.returnKeyType = UIReturnKeyDone;
        emailFieldModel.textFieldName = kEmailText;
        [tempDataArray addObject:emailFieldModel];
        _textFieldsModelArray = tempDataArray;
    }
    
    
    //Setting Screen Name, to handle different validation for different screen
    
    for(BTTextFiledModel *textFieldModel in _textFieldsModelArray){
        
        textFieldModel.screenName = kAddContactContact;
    }
    
    
}

@end
