//
//  SiteContactsViewController.m
//  SampleActionViewProject
//
//  Created by VectoScalar on 10/26/16.
//  Copyright © 2016 VectoScalar. All rights reserved.
//

#import "BTSiteContactsViewController.h"
#import "SiteContactTableViewCell.h"
#import "AppConstants.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "BTNeedHelpViewController.h"
#import "OmnitureManager.h"
#import "AppManager.h"


@interface BTSiteContactsViewController ()<UITableViewDelegate, UITableViewDataSource, BTAmendFooterActionViewDelegate, BTAmendFooterActionViewDelegate>

@end

@implementation BTSiteContactsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.backgroundColor = [UIColor clearColor];
    
    //Register nib
    UINib *nib = [UINib nibWithNibName:kSiteContactTableViewCell bundle:nil];
    [tableView registerNib:nib forCellReuseIdentifier:kSiteContactTableViewCellID];
    
    //Auto height cell setup
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 84;
    
    //Hiding Default cell Separator
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.contentInset = UIEdgeInsetsMake(0, 0, 120, 0);

    
    //Left navigation close button
    self.navigationItem.leftBarButtonItem = [self getCloseButtonItem];
    
    //Setting screen titke
    self.title = [self getScreenTitle];
    
    
    tableView.delegate = self;
    
    //Hiding back button
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    //[siteContactSegmentControl setTitleTextAttributes:[[AppManager sharedAppManager] fetchSegmentedControlFontDict] forState:UIControlStateNormal];
    
    [self selectViewForIndex:0];
    
    [self createFooterActionView];
    
    __weak typeof(self) weakSelf = self;
    [RACObserve(self, self.isDataChanged) subscribeNext:^(NSNumber* state) {

        [weakSelf dataChanged];
        
    }];
    self.isDataChanged = NO;

    
}


- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    _isOpeningNeedHelp = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark- Super Class Methods


- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow{

    if(siteContactSegmentControl.selectedSegmentIndex == 1){
        
        //No need to show retry view at second page
        return;
    }
    
    _isRetryViewShown = YES;
    
    if(!_retryView){
        
        _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
        _retryView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view insertSubview:_retryView aboveSubview:tableView];
        
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:siteContactSegmentControl attribute:NSLayoutAttributeBottom multiplier:1.0 constant:7.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_amendFooterActionView  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    }
    else{
        
         [self.view insertSubview:_retryView aboveSubview:tableView];
    }
    
    _retryView.hidden = NO;
    
    _retryView.retryViewDelegate = self;
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
}


#pragma mark - UITableView DataSource & Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_dataArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    UITableViewCell *cell = [self getCellForIndexPath:indexPath];
    
    if(!cell){
        
        [NSException raise:@"Cell can't be nill" format:@""];
    }
    
    
    return cell;
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self cellSelectedAtIndexPath:indexPath];
}




#pragma mark- Helper Methods

- (UITableViewCell *)getCellForIndexPath:(NSIndexPath *)indexPath{
    
    
    return nil;
}

- (NSString *)getScreenTitle{
    
    if(self.isAlternativeContact){
        
        return @"Alternative site contact";
    }
    
    return @"Site contact";
}




- (void)cellSelectedAtIndexPath:(NSIndexPath *)indexPath{
    
    
}



- (void)selectViewForIndex:(NSInteger)selectedIndex{
    
}



- (void)dataChanged{
    
    
}


- (void)createFooterActionView{
    
    //Footer actionview:Need Help Button
   
        _amendFooterActionView = [[[NSBundle mainBundle] loadNibNamed:@"BTAmendFooterActionView" owner:self options:nil] firstObject];
        _amendFooterActionView.translatesAutoresizingMaskIntoConstraints = NO;
        _amendFooterActionView.delegate = self;
        [self.view addSubview:_amendFooterActionView];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_amendFooterActionView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_amendFooterActionView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_amendFooterActionView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:45.0]];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_amendFooterActionView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.bottomLayoutGuide  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    
    //[self.view addConstraint:[NSLayoutConstraint constraintWithItem:tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_amendFooterActionView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    
}


- (void)trackOmniPage:(NSString *)page
{
    NSMutableDictionary *contextDict = [NSMutableDictionary dictionary];
    
    NSString *orderRef = [OmnitureManager getOrderRefFormattedString:self.orderReference];
    [contextDict setValue:@"Logged In" forKey:kOmniLoginStatus];
    [contextDict setValue:orderRef forKey:kOmniOrderRef];
    
    [OmnitureManager trackPage:page withContextInfo:contextDict];
}

#pragma mark- action methods

- (IBAction)siteContactSegmentControlChanged:(id)sender {
    
    UISegmentedControl *segmentControll = (UISegmentedControl *)sender;
    
    [self selectViewForIndex:segmentControll.selectedSegmentIndex];
    [self createFooterActionView];
    
    if(segmentControll.selectedSegmentIndex == 0 && _isRetryViewShown){
        
        if(![AppManager isInternetConnectionAvailable]){
            
            [self showRetryViewWithInternetStrip:YES];
        }
        else{
            
            [self showRetryViewWithInternetStrip:NO];
        }
        
    }
    else if(_isRetryViewShown){
        
        [self hideRetryView];
        _isRetryViewShown = YES; //To keep the status that we need to show retry view.
    }
    
    
    //Hidding activity indicator on second page
    if(self.networkRequestInProgress){
        
        if(segmentControll.selectedSegmentIndex == 0){
            
            [self showProgressView];
        }
        else{
            
            [self hideProgressView];
        }
    }
    
    if(self.isAlternativeContact)
    {
        [self trackOmniPage:OMNIPAGE_ORDER_ALTERNATE_SITECONTACT_ADD_NEW_CONTACT];
    }
    else
    {
        [self trackOmniPage:OMNIPAGE_ORDER_SITECONTACT_ADD_NEW_CONTACT];
    }
}


#pragma mark - RetryView Delegate

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView{
    
    [self hideRetryView];
    self.networkRequestInProgress = YES;
    
}


#pragma mark- BTAmendFooterActionViewDelegate Method

- (void)userPressedOnBTAmendFooterActionView:(BTAmendFooterActionView *)view{
    

    if(!self.isAlternativeContact){
        
        self.isOpeningNeedHelp = YES;
        
        [self trackOmniPage:OMNIPAGE_ORDER_SITECONTACT_HELP];
        
        BTNeedHelpViewController *helpViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SiteContactNeedHelpViewController"];
        [self presentViewController:helpViewController animated:YES completion:nil];
    }
    else{
        
        self.isOpeningNeedHelp = YES;
        [self trackOmniPage:OMNIPAGE_ORDER_ALTERNATE_SITECONTACT_HELP];
        BTNeedHelpViewController *helpViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AltSiteContactNeedHelpViewController"];
        [self presentViewController:helpViewController animated:YES completion:nil];
    }
    
}


- (void)createLoadingView {
    
    if(!_loadingView){
        
        _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        _loadingView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5f];
        _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.view addSubview:_loadingView];
    }
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:siteContactSegmentControl  attribute:NSLayoutAttributeTop multiplier:1.0 constant:30.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}



@end
