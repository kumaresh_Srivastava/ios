//
//  BTAddSiteContactTableViewController.m
//  SampleActionViewProject
//
//  Created by VectoScalar on 10/27/16.
//  Copyright © 2016 VectoScalar. All rights reserved.
//

#import "BTAddSiteContactTableViewController.h"

@interface BTAddSiteContactTableViewController (){
    
    NSMutableDictionary *cacheDictionary;
}
   
@end

@implementation BTAddSiteContactTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    cacheDictionary = [[NSMutableDictionary alloc] init];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 112;
    
    UINib *nib = [UINib nibWithNibName:kAddContactTableViewCell bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kAddContactTableViewCellID];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 90, 0);

    self.tableView.bounces = NO;
    [self createData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Class Method
+ (BTAddSiteContactTableViewController *)getBTAddSiteContactTableViewController{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTAddSiteContactTableViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTAddSiteContactTableViewControllerID"];
    
    return vc;
}


#pragma mark - Public Method

- (void)saveContactInfo{
    
    [self save];
}

#pragma mark - Private Methods

- (void)createData{

    _addNewContactScreenModel = [[DLMAddNewContactScreen alloc] initWithContactDict:self.siteContactDict];
    _dataArray = [_addNewContactScreenModel getTextFieldsModelsArray];
    [self.tableView reloadData];
}


- (NSArray *)getContactTitlesArry{

    return [_addNewContactScreenModel getTitlesArray];
}


- (void)openTitleSelction:(UITextField*)textField{

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:/*@"Select Title"*/nil preferredStyle:UIAlertControllerStyleActionSheet];

    NSArray* titleArray = [self getContactTitlesArry];
    NSMutableArray *titlesArray = [[NSMutableArray alloc] initWithArray:titleArray];
    [titlesArray insertObject:@"Select Title" atIndex:0]; // BUG 15001

    for(NSString *title in titlesArray){

        UIAlertAction *titleAction = [UIAlertAction actionWithTitle:title
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  if ([title isEqualToString:@"Select Title"]) {
                                                                      self->_currectSelectedTexFieldtModel.inputText = @"Select";
                                                                      [self.tableView reloadData];
                                                                  } else {
                                                                      self->_currectSelectedTexFieldtModel.inputText = title;
                                                                      [self.tableView reloadData];
                                                                  }
                                                                 
                                                                  [alert dismissViewControllerAnimated:YES completion:nil];
                                                              }];

        [alert addAction:titleAction];

    }

    /*if([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPad){
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                         style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                         }];
        [alert addAction:cancel];
    }*/
    alert.popoverPresentationController.sourceView = self.view;//
    alert.popoverPresentationController.sourceRect = textField.frame;
    alert.popoverPresentationController.canOverlapSourceViewRect = NO;

    [self presentViewController:alert animated:YES completion:^{

    }];
}


- (void)save{

    [self.view endEditing:YES];

    BOOL isValidationSuccessful = YES;

    for(BTTextFiledModel *model in _dataArray){

        if(isValidationSuccessful){

            isValidationSuccessful = [model isValid];
        }
        else{

            break;
        }

        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[_dataArray indexOfObject:model] inSection:0];

        NSString *identifier = [NSString stringWithFormat:@"cell%d%d", (int)indexPath.section, (int)indexPath.row];
        AddContactTableViewCell *cell = [cacheDictionary valueForKey:identifier];
        [cell updateUIAsPerValidity];
    }

    if(isValidationSuccessful){

        NSDictionary *userEnteredDatDict = [self getUserDataDictionary];
        [self.delegate btAddSiteContactTableViewController:self didSubmitUserData:userEnteredDatDict];

    }

}

- (NSDictionary *)getUserDataDictionary{

    //Pass entered data to delegate
    NSMutableDictionary *userDataDict = [NSMutableDictionary dictionary];

    for(BTTextFiledModel *textFieldModel in _dataArray){

        if([textFieldModel.textFieldName isEqualToString:kTitleText]){

            [userDataDict setValue:textFieldModel.inputText forKey:kUserTitleKey];
        }
        else if([textFieldModel.textFieldName isEqualToString:kFirstNameText]){

            [userDataDict setValue:textFieldModel.inputText forKey:kUserFirstNameKey];
        }
        else if([textFieldModel.textFieldName isEqualToString:kLastNameText]){

            [userDataDict setValue:textFieldModel.inputText forKey:kUserLastNameKey];
        }
        else if([textFieldModel.textFieldName isEqualToString:kPhoneText]){

            [userDataDict setValue:textFieldModel.inputText forKey:kUserPhoneKey];

        }
        else if([textFieldModel.textFieldName isEqualToString:kAltPhoneText]){

            [userDataDict setValue:textFieldModel.inputText forKey:kUserAltPhoneKey];
        }
        else if([textFieldModel.textFieldName isEqualToString:kEmailText]){

            [userDataDict setValue:textFieldModel.inputText forKey:kUserEmailKey];
        }

    }

    return userDataDict;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_dataArray count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //Temp Fix, creating different cell for each row
    NSString *identifier = [NSString stringWithFormat:@"cell%d%d", (int)indexPath.section, (int)indexPath.row];
    AddContactTableViewCell *cell = [cacheDictionary valueForKey:identifier];
    
    if(!cell){
        
        cell = [[[NSBundle mainBundle] loadNibNamed:@"AddContactTableViewCell" owner:self options:nil] firstObject];
        [cacheDictionary setValue:cell forKey:identifier];
    }
    
    cell.delegate = self;
    [cell updateWithTextFieldModel:[_dataArray objectAtIndex:indexPath.row]];
    //[cell updateUIAsPerValidity];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.view endEditing:YES];
}






#pragma mark - AddContactTableViewCellDelegate

- (void)retunKeyPressedForAddContactTableViewCell:(AddContactTableViewCell *)cell{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    BTTextFiledModel *textFieldModel = [_dataArray objectAtIndex:indexPath.row];
    
    if(textFieldModel.returnKeyType == UIReturnKeyNext && indexPath.row < [_dataArray count] - 1){
        
        BTTextFiledModel *textFielModel = [_dataArray objectAtIndex:indexPath.row + 1];
        [textFielModel makeFirstResponder];
        
    }
    else if(textFieldModel.returnKeyType == UIReturnKeyDone){
        
        [textFieldModel resignFirstResponder];
        [self save];
    }
    
}


- (void)addContactTableViewCell:(AddContactTableViewCell *)cell
didSelectFiledForBTTextFiledModel:(BTTextFiledModel *)textFieldModel andTextField:(UITextField*)textField{
    
    _currectSelectedTexFieldtModel = textFieldModel;
    if(textFieldModel.textFieldType == TextFiledTypeDropDown){
        
        [self.view endEditing:YES];
        [self openTitleSelction:textField];
    }
}


@end
