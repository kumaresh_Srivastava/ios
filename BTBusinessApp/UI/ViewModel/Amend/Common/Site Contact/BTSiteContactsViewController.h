//
//  SiteContactsViewController.h
//  SampleActionViewProject
//
//  Created by VectoScalar on 10/26/16.
//  Copyright © 2016 VectoScalar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SiteContactTableViewCell.h"
#import "BTAddSiteContactTableViewController.h"
#import "BTBaseViewController.h"
#import "BTBaseViewController.h"
#import "BTAmendFooterActionView.h"

#define kSiteContactTableViewCell @"SiteContactTableViewCell"
#define kSiteContactTableViewCellID @"SiteContactTableViewCellID"

@interface BTSiteContactsViewController : BTBaseViewController{
    
    __weak IBOutlet UITableView *tableView;
    __weak IBOutlet UISegmentedControl *siteContactSegmentControl;
    __weak IBOutlet NSLayoutConstraint *tableviewBottomConstraint;
   // UIActivityIndicatorView *_actvityIndicatorView;
    
    
    NSArray *_dataArray;
    
    BTAddSiteContactTableViewController *_addContactTableViewController;
    BTAmendFooterActionView *_amendFooterActionView;
}

@property (assign, nonatomic) BOOL isDataChanged;
@property (assign, nonatomic) BOOL isAlternativeContact;
@property (assign, nonatomic) BOOL isOpeningNeedHelp;

@end
