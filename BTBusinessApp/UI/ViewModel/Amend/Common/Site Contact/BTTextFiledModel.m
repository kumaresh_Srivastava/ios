//
//  BTTextFiledModel.m
//  BTBusinessApp
//
//  Created by VectoScalar on 10/27/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTTextFiledModel.h"
#import "AppConstants.h"
#import "NSObject+APIResponseCheck.h"

@interface BTTextFiledModel()

@end

@implementation BTTextFiledModel

- (BOOL)isValid{
    
    if(self.isDisabled)
        return YES;
    
    switch (self.textFieldType) {
            
        case TextFiledLastName:
        case TextFiledTypeName:
        {
            if(self.regex)
            {
                if(!self.inputText || [self.inputText isEqualToString:@""]){
                    
                    return NO;
                }

                
                
                if(![self validateRegex:self.regex forText:self.inputText])
                {
                    return NO;
                }
            }
            else
            {
                if (!self.inputText || [self.inputText rangeOfString:kNameRegularExpression options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location == NSNotFound ) {
                    
                    return NO;
                    
                }

            }
            
            
        }
            break;
            
        case TextFiledTypePhone:
        {
            
            if(self.regex)
            {
                if(!self.inputText || ![self.inputText validAndNotEmptyStringObject] || ![self validateRegex:self.regex forText:self.inputText]){
                    
                    return NO;
                }
                
                return YES;
            }
            
            
            if (!self.inputText || [self.inputText rangeOfString:kPhoneRegularExpression options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location == NSNotFound ) {
                
                return NO;
                
            }
            
        }
            break;
            
            
        case TextFiledTypePhoneOptional:
        {
            if(self.regex)
            {
                if(!self.inputText || [self.inputText isEqualToString:@""]){
                    
                    return YES;
                }
                
                if(![self validateRegex:self.regex forText:self.inputText])
                 {
                     return NO;
                 }
            }
            else
            {
                if (!self.inputText || [self.inputText rangeOfString:kPhoneOptionalRegularExpresstion options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location == NSNotFound ) {
                    
                    if(!self.inputText || [self.inputText isEqualToString:@""]){
                        
                        return YES;
                    }
                    
                    return NO;
                    
                }
            }
            
        }
            break;
            
        case TextFiledTypeEmail:
        {
            if(self.regex)
            {
                if(!self.inputText || [self.inputText isEqualToString:@""]){
                    
                    return NO;
                }
                
                
                if(![self validateRegex:self.regex forText:self.inputText])
                {
                    return NO;
                }
            }
            else
            {
                if (!self.inputText || [self.inputText rangeOfString:kEmailRegularExpression options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location == NSNotFound ) {
                    
                    return NO;
                    
                }
                

            }

            
            
        }
            break;
            
        case TextFiledTypeEmailOptional:
        {
            
            
            if(self.regex)
            {
                if(!self.inputText || [self.inputText isEqualToString:@""]){
                    
                    return YES;
                }
                
                
                if(![self validateRegex:self.regex forText:self.inputText])
                {
                    return NO;
                }
                else
                {
                    if([self.dependentCondition validAndNotEmptyStringObject] && [self.dependentCondition isEqualToString:self.inputText])
                    {
                        
                        return NO;
                    }
   
                }
            }
          else
            {
                if (!self.inputText || [self.inputText rangeOfString:kEmailRegularExpression options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location == NSNotFound ) {
                    
                    
                    if([self.dependentCondition validAndNotEmptyStringObject] && [self.dependentCondition isEqualToString:self.inputText])
                    {
                        
                        return NO;
                    }

                    
                    
                    if(!self.inputText || [self.inputText isEqualToString:@""]){
                        
                        return YES;
                    }
                    
                    
                    
                    return NO;
                    
                }

            }
           
            

        }
            
            break;
            
        case TextFiledTypeDropDown:
        {
            if (!self.inputText || [self.inputText isEqualToString:kDropDownDefaultText]) {
                
                return NO;
            }
            
        }
            
            break;
            
        case TextFiledTypePhoneMandatory:
        {
            
            if(self.regex)
            {
                if(!self.inputText || ![self.inputText validAndNotEmptyStringObject] || ![self validateRegex:self.regex forText:self.inputText]){
                    
                    return NO;
                }
                
                return YES;
            }
            
            
            if (!self.inputText || [self.inputText rangeOfString:kPhoneRegularExpression options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location == NSNotFound ) {
                
                return NO;
                
            }
            
        }
            break;

        default:
            break;
    }
    
    
    return YES;
}


- (NSString *)errorMessage{
    
    
    NSString *errorMessage = nil;
    
    switch (self.textFieldType) {
            
        case TextFiledTypeName:
        {
            //Check for empty Text field
            
            if(self.inputText.length == 0)
               errorMessage = @"First name is required";
            
            else
                errorMessage = @"Please enter valid first name";
                
        }
            break;
            
        case TextFiledLastName:
        {
            //Check for empty Text field
            
            if(self.inputText.length == 0)
                errorMessage = @"Last name is required";
            
            else
                errorMessage = @"Please enter valid last name";
            
        }
            break;
            
        case TextFiledTypePhone:
        {
           if(self.inputText.length == 0)
               if(![self.screenName isEqualToString:kAddContactContact])
               {
                  errorMessage = @"Mobile number is required";
               }
               else
               {
                   if(self.regex)
                   {
                        errorMessage = @"";
                   }
                   else
                   {
                        //(RLM) 14 feb commented to fix consistencey
                       //  errorMessage = @"Reference number is required";
                       errorMessage = @"Mobile number is required";
                   }
                   
                  
               }
            
            
           else if(self.regex)
           {
               errorMessage = @"Please enter a valid mobile number";
           }
            
            else if ([self.inputText rangeOfString:kPhoneRegularExpression options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location == NSNotFound ) {
                
                if(![self.screenName isEqualToString:kAddContactContact])
                {
                     errorMessage = @"Please enter a valid mobile number";
                }
                else
                {
                     //(RLM) 14 feb commented to fix consistencey
                    // errorMessage = @"Please enter a valid number to call";
                     errorMessage = @"Please enter a valid mobile number";
                }
               
                
            }
            
        }
            break;
            
    
        case TextFiledTypePhoneOptional:
        {
            if(self.inputText.length == 0)
                errorMessage = @"";
            
            else if(self.regex)
            {
                errorMessage = @"Please enter a valid phone number";
            }
            
            else if ([self.inputText rangeOfString:kPhoneRegularExpression options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location == NSNotFound ) {
                
                if(self.regex)
                {
                    errorMessage = @"Please enter a valid phone number";
                }
                else
                {
                    if(![self.screenName isEqualToString:kAddContactContact])
                    {
                        errorMessage = @"Please enter a valid phone number";
                    }
                    else
                    {
                        //(RLM) 14 feb commented to fix consistencey
                        //errorMessage = @"Please enter a valid mobile number";
                        errorMessage = @"Please enter a valid phone number";
                    }


                }
                
                
            }

            
        }
            break;
            

            
            
        case TextFiledTypeEmail:
        {
            if(self.inputText.length == 0)
                errorMessage = @"Email is required";
            
            else if ([self.inputText rangeOfString:kEmailRegularExpression options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location == NSNotFound ) {
                
                errorMessage = @"Please enter a valid email address";
                
            }
            
        }
            break;
            
        case TextFiledTypeEmailOptional:
        {
            if(self.inputText.length == 0)
                errorMessage = @"";

            else if ([self.inputText rangeOfString:kEmailRegularExpression options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location == NSNotFound ) {
                
                errorMessage = @"Please enter a valid email address";
                
            }
            else if ([self.dependentCondition validAndNotEmptyStringObject]){
                
                if([self.inputText isEqualToString:self.dependentCondition]){
                    
                    errorMessage = @"Your primary and alternative email need to be different";
                }
            }
            
        }
            
        case TextFiledTypeDropDown:
        {
            if ([self.inputText isEqualToString:kDropDownDefaultText]) {
                
                errorMessage = @"Title is required";
            }
            
        }
            
            break;
            
        case TextFiledTypePhoneMandatory:
        {
            if(self.inputText.length == 0)
                if(![self.screenName isEqualToString:kAddContactContact])
                {
                    errorMessage = @"Phone number is required";
                }
                else
                {
                    if(self.regex)
                    {
                        errorMessage = @"";
                    }
                    else
                    {
                        //(RLM) 14 feb commented to fix consistencey
                        //  errorMessage = @"Reference number is required";
                        errorMessage = @"Phone number is required";
                    }
                    
                    
                }
            
            
                else if(self.regex)
                {
                    errorMessage = @"Please enter a valid phone number";
                }
            
                else if ([self.inputText rangeOfString:kPhoneRegularExpression options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location == NSNotFound ) {
                    
                    if(![self.screenName isEqualToString:kAddContactContact])
                    {
                        errorMessage = @"Please enter a valid phone number";
                    }
                    else
                    {
                        //(RLM) 14 feb commented to fix consistencey
                        // errorMessage = @"Please enter a valid number to call";
                        errorMessage = @"Please enter a valid phone number";
                    }
                    
                    
                }
            
        }
            break;
            

            
        default:
            break;
    }
    
    return errorMessage;
}



- (void)makeFirstResponder{
    
    [self.correspondingTextField becomeFirstResponder];
}

- (void)resignFirstResponder{
    
    [self.correspondingTextField resignFirstResponder];
}


- (BOOL)validateRegex:(NSString *)regexpression forText:(NSString *)text
{
    
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexpression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:text
                                                        options:0
                                                          range:NSMakeRange(0, [text length])];
    
    if (numberOfMatches == 0)
    {
        return  NO;
    }
    else
    {
        return YES;
    }
}



@end
