//
//  BTSignInViewControllerTableViewController.m
//  BTBusinessApp
//
//  Created by Rohini Kumar on 13/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//


#import "BTSignInViewController.h"
#import "BTPINViewController.h"
#import <HTAutocompleteTextField/HTAutocompleteTextField.h>
#import "AppDelegateViewModel.h"
#import "BTInAppBrowserViewController.h"
#import "CustomSpinnerAnimationView.h"
#import "DLMSignInScreen.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "AppManager.h"
#import "ViewWithRoundedHairline.h"
#import "OmnitureManager.h"
#import "BTAuthenticationToken.h"
#import "NLWebServiceError.h"
#import "BTSecurityCheckInterceptorViewController.h"
#import "BTSecurityQuestionsAndAnswerViewController.h"
#import "BTSecurityNumberViewController.h"
#import "BTOneFAInterceptViewController.h"
#import "BTGSProfileUpliftInterceptViewController.h"
#import "BTUser.h"
#import "CDUser+CoreDataProperties.h"
#import "CDCug+CoreDataClass.h"
#import "AppManager.h"
#import "UIButton+BTButtonProperties.h"
#import "BTNavigationViewController.h"

#define kFirstTimeLogin @"FirstTimeLogin"
#define kCreatePassword @"ResetPassword"
#define kGSProfileUplift @"GSProfileUplift"

@import SafariServices;

#define kLowerGapBetweenContainerViewAndItems 30;
#define kTableViewYPos 20
#define kAlertBannerHeight 40


@interface BTSignInViewController () <DLMSignInScreenDelegate, BTPINViewControllerDelegate,BTSecurityNumberViewControllerDelegate> {
    BOOL _isAnimationDone;
    BOOL _isInitialSetupForTableView;
    UITapGestureRecognizer *_tapGestureWhileKeyboardActive;
    
    NSString *_currentSelectedKeyForIntercepter;
}


@property (nonatomic, readwrite) DLMSignInScreen *viewModel;

@property (weak, nonatomic) IBOutlet UIView *loginSubView;
@property (weak, nonatomic) IBOutlet UIButton *logInButton;
@property (weak, nonatomic) IBOutlet ViewWithRoundedHairline *userNameTextFieldContainerView;
@property (weak, nonatomic) IBOutlet ViewWithRoundedHairline *passwordTextFieldContainerView;
@property (weak, nonatomic) IBOutlet HTAutocompleteTextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UILabel *notificationAlertLabel;

@property (weak, nonatomic) IBOutlet UIView *notificationLoginErrorView;

@property (weak, nonatomic) IBOutlet UILabel *logInLabel;
@property (weak, nonatomic) IBOutlet UIButton *forgottenBTIdButton;
@property (weak, nonatomic) IBOutlet UILabel *btIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;
@property (weak, nonatomic) IBOutlet UIButton *forgottenBTPasswordButton;
@property (weak, nonatomic) IBOutlet UIButton *dontHaveBTIdButton;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIButton *showOrHidePasswordButton;
@property (strong, nonatomic) CustomSpinnerAnimationView *indicatorView;
@property (weak, nonatomic) IBOutlet UIView *loadingView;
@property (strong,nonatomic) UINavigationController *interceptorNavigationController;

@property (weak, nonatomic) UIImageView *backgroundImageView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong,nonatomic) BTUser *currentUserForIntercepter;

-(void)animateSignInScreenItems;

@end

@implementation BTSignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.viewModel = [[DLMSignInScreen alloc] init];
    self.viewModel.signInScreenDelegate = self;
    
    //[self.logInButton getSuperButtonStyle];
    [self.headerView setBackgroundColor:[BrandColours colourBackgroundBTPurplePrimaryColor]];
    
   
   
    [self performSelector:@selector(loadingSpinView) withObject:nil afterDelay:0.1];
    
    //track page
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    NSString *pageName = OMNIPAGE_LOGIN;
    NSString *timestamp = [AppManager NSStringWithOmnitureFormatFromNSDate:[NSDate date]];
    
    [data setValue:@"Not Logged In" forKey:@"LoginStatus"];
    [data setValue:timestamp forKey:@"TimeStamp"];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
    
    
    //Notification label
    [[self notificationAlertLabel] setText:@""];
    [[self notificationAlertLabel] setHidden:YES];
    
    //Delegates for textfields
    [[self userNameTextField] setDelegate:self];
    [[self passwordTextField] setDelegate:self];
    
    //Textfield colours
    if([AppManager isAppOpnedUsingDeepLinking] && ![[AppManager sharedAppManager] getDeeplinkAlertShownStatusOnSignin]) {
        //TODO: (VM) 10-03-2017 Apply design changes if app is opened using deep linking
        
        NSString *activationCode = [[AppManager sharedAppManager] getDeepLinkedActivationCode];
        if(activationCode && ![activationCode isEqualToString:@""])
        {
            [self showAlertForEmailVerifiactionWitTitle:@"One more thing to do ..." andMessage:kAccountVerificationPopupMessage];
        }
        else
        {
            [self showAlertForEmailVerifiactionWitTitle:@"Account verification" andMessage:kHandcraftedURLMessage];
        }
    } else {
        
    }
    [[self userNameTextField] setTintColor:[BrandColours colourBtPrimaryColor]];
    [[self userNameTextField] setTextColor:[BrandColours colourBtNeutral90]];
    [[self passwordTextField] setTintColor:[BrandColours colourBtPrimaryColor]];
    [[self passwordTextField] setTextColor:[BrandColours colourBtNeutral90]];
    
    [[self.userNameTextFieldContainerView layer] setBorderColor:[[BrandColours colourBtNeutral60] CGColor]];
    [[self.passwordTextFieldContainerView layer] setBorderColor:[[BrandColours colourBtNeutral60] CGColor]];
    
    // Start monitoring network activity state changes.
    __weak typeof(self) weakSelf = self;
    [RACObserve(self, logInRequestInProgress) subscribeNext:^(NSNumber* state) {
        
        if ([weakSelf logInRequestInProgress]) {
            [[weakSelf userNameTextField] setEnabled:false];
            [[weakSelf passwordTextField] setEnabled:false];
            [[weakSelf logInButton] setEnabled:false];
            //[[weakSelf logInButton] setHidden:YES];
            [weakSelf.indicatorView setIsStopped:NO];
            [weakSelf.indicatorView startAnimation];
            [weakSelf disableInlineButtons];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
        } else {
            [[weakSelf userNameTextField] setEnabled:true];
            [[weakSelf passwordTextField] setEnabled:true];
            [[weakSelf logInButton] setEnabled:true];
            [[weakSelf logInButton] setHidden:NO];
            [weakSelf.indicatorView setIsStopped:YES];
            [weakSelf.indicatorView stopAnimation];
            [weakSelf enableInlineButtons];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
        }
        
    }];
    
    // BT ID / email address signal.
    RACSignal* btIdSignal = [[[self userNameTextField] rac_textSignal] map:^id (NSString* btId) {
        btId = [AppManager stringByTrimmingWhitespaceInString:btId];
        
        // btId field Validation
        BOOL isValid = [btId length] > 0;
        
        return [NSNumber numberWithBool:isValid];
    }];
    
    // Password signal.
    RACSignal* passwordSignal = [[[self passwordTextField] rac_textSignal] map:^id (NSString* password) {
        password = [AppManager stringByTrimmingWhitespaceInString:password];
        
        // Passowrd field Validation.
        BOOL isValid = [password length] > 0;
        
        return [NSNumber numberWithBool:isValid];
    }];
    
    
    if(self.shouldPopulateUsernameForLoggedInUser) {
        self.userNameTextField.text = [self.viewModel usernameForCurrentlyLoggedInUser];
    }
    else
    {
        if([AppManager sharedAppManager].usernameForSignInIntercepter)
        {
            self.userNameTextField.text = [AppManager sharedAppManager].usernameForSignInIntercepter;
        }
        
    }
    
    
    // Both BT ID and password are required fields.
    RACSignal* shouldEnableLogInButtonSignal = [RACSignal combineLatest:@[btIdSignal, passwordSignal]
                                                                     reduce:^id (NSNumber* btIdIsValid,
                                                                                 NSNumber* passwordIsValid) {
                                                                         //checking for the scenario when you enter signin screen from pinScreen by clicking return to login
                                                                         if (weakSelf.shouldPopulateUsernameForLoggedInUser) {
                                                                             BOOL isValidusername = false;
                                                                             if (weakSelf.userNameTextField.text.length>0) {
    
                                                                                 isValidusername = true;
                                                                             }
                                                                             return @(isValidusername && [passwordIsValid boolValue] && ![weakSelf logInRequestInProgress]);
                                                                         }
    
                                                                         return @([btIdIsValid boolValue] && [passwordIsValid boolValue] && ![weakSelf logInRequestInProgress]);
                                                                     }];
        // Enable / disable button as appropriate.
        [shouldEnableLogInButtonSignal subscribeNext:^(NSNumber* shouldEnable) {
            [[weakSelf logInButton] setEnabled:[shouldEnable boolValue]];
        }];
    
   
}

/*- (void) hack{
    [self redirectToUpdate1FAInterceptor:@"1FA"];
   
}*/

- (void) loadingSpinView{
    
    if(_indicatorView){
        [_indicatorView removeFromSuperview];
    }
     _indicatorView = [[CustomSpinnerAnimationView alloc] initWithText:@"" withLoadingImage:[UIImage imageNamed:@"Spinner"]];
    _indicatorView.frame = CGRectMake(0, 0, _loadingView.frame.size.width, _loadingView.frame.size.height);
    _indicatorView.translatesAutoresizingMaskIntoConstraints = NO;
    [_loadingView addSubview:_indicatorView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.interceptorNavigationController = nil;
    //notification for keyboard
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appplicationDidBecomActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChangeLoaderNotify:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
}

- (void)statusBarOrientationChangeLoaderNotify:(NSNotification *)notification {
    // handle the interface orientation as needed
    [self loadingSpinView];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    //(VRK) removing keyboard notifications
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self animateSignInScreenItems];
}


#pragma mark - Application active status notification
- (void)appplicationDidBecomActive: (NSNotification*)notification {
    
}


#pragma mark - Private helper method

- (void)disableInlineButtons
{
    _forgottenBTIdButton.enabled = false;
    _forgottenBTPasswordButton.enabled = false;
    _dontHaveBTIdButton.enabled = false;
    
    [_forgottenBTIdButton setTitleColor:[BrandColours colourBtDisableInlineButton] forState:UIControlStateDisabled];
    [_forgottenBTPasswordButton setTitleColor:[BrandColours colourBtDisableInlineButton] forState:UIControlStateDisabled];
    [_dontHaveBTIdButton setTitleColor:[BrandColours colourBtDisableInlineButton] forState:UIControlStateDisabled];
    
}

- (void)enableInlineButtons
{
    _forgottenBTIdButton.enabled = true;
    _forgottenBTPasswordButton.enabled = true;
    _dontHaveBTIdButton.enabled = true;
    
    [_forgottenBTIdButton setTitleColor:[BrandColours colourTextBTPurplePrimaryColor] forState:UIControlStateNormal];
    [_forgottenBTPasswordButton setTitleColor:[BrandColours colourTextBTPurplePrimaryColor] forState:UIControlStateNormal];
    [_dontHaveBTIdButton setTitleColor:[BrandColours colourTextBTPurplePrimaryColor] forState:UIControlStateNormal];
}

- (void)startSigningIn
{
    //Track sign in action
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    NSString *pageName = OMNIPAGE_LOGIN;
    NSString *linkTitle = OMNICLICK_LOGIN_SIGNIN;
    
    //[data setValue:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] forKey:@"LinkName"];
    [data setValue:@"Not Logged In" forKey:@"LoginStatus"];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:data];
    
    
    if (![AppManager isInternetConnectionAvailable]) {
        DDLogInfo(@"No Internet connection present when Sign-In button pressed");
        [self displayInlineErrorMessageWithMessage:kNoConnectionMessage withTextFieldColorChanged:NO];
        
        [OmnitureManager trackError:OMNIERROR_NO_INTERNET_LOGIN onPageWithName:OMNIPAGE_LOGIN contextInfo:data];
    }
    else
    {
        //_indicatorView.hidden = YES;
        [self loginButtonAnimation];
        
        // (LP) Used to show indicator view while login API call being made.
        self.logInRequestInProgress = YES;
        
        // (LP) Remove error label
        [self clearInlineErrorMessage];
        
        NSString *username = [[[[self userNameTextField] text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] lowercaseString];
        NSString *password = [[[self passwordTextField] text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        DDLogInfo(@"Sign-In API reqeuest Initiated");
        
        [self.viewModel performLoginWithUsername:username andPassword:password];
        
    }
}

- (void)showAlertWithIncorrectActivationCode
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Account verification" message:kIncorrectActivationCodeErrorMessage preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //Do query user detail call again with in-correct activation code
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *differentUserAction = [UIAlertAction actionWithTitle:@"Login as a different user" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        // GO to login and on submit make fresh login call without activoation code
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:kIsAppOpenedFromEmailVerification];
        [[AppManager sharedAppManager] setDeepLinkedActivationCode:nil];
        self.userNameTextField.text = nil;
        self.passwordTextField.text = nil;
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:differentUserAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showAlertWithUserAlreadyVerifiedError
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Account verification" message:kEmailAlreadyVerifiedErrorMessage preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //Do auto sign in  and then query user detail call again without activation code
        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:kIsAppOpenedFromEmailVerification];
        [[AppManager sharedAppManager] setDeepLinkedActivationCode:nil];
        [self startSigningIn];
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        // GO to login but save activation code
        [self clearPasswordTextFieldOnError];
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showAlertForEmailVerifiactionWitTitle:(NSString *)title andMessage:(NSString *)message
{
    [[AppManager sharedAppManager] setDeeplinkAlertShownStatusOnSignin:YES];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alertController addAction:okAction];
    [[self navigationController] presentViewController:alertController animated:YES completion:nil];
}

- (void)trackOmnitureError:(NSString *)error
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    NSString *pageName = OMNIPAGE_LOGIN;
    NSString *timestamp = [AppManager NSStringWithOmnitureFormatFromNSDate:[NSDate date]];
    
    [data setValue:@"Not Logged In" forKey:@"LoginStatus"];
    [data setValue:timestamp forKey:@"TimeStamp"];
    
    [OmnitureManager trackError:error onPageWithName:pageName contextInfo:data];
}

- (void)redirectToFirstTimeLoginIntercepterWithQuestion:(NSString *)securityQuestion
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTSecurityCheckInterceptorViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTSecurityCheckInterceptorViewControllerID"];
    vc.questionFromSignInScreen = securityQuestion;
    vc.cugKeyFromSignInScreen = _currentSelectedKeyForIntercepter;
    [self openInterceptorController:vc];
}

- (void)redirectToCreatePasswordIntercepter
{
    //Redirect to security question screen
//    BTSecurityQuestionsAndAnswerViewController *changeSecurityQuestion = [self.storyboard instantiateViewControllerWithIdentifier:@"SecurityQuestionsAndAnswerScene"];
//    changeSecurityQuestion.isScreenRelatedToInterceptorModule = YES;
//    changeSecurityQuestion.cugKeyFromSignInScreen = _currentSelectedKeyForIntercepter;
//    [self openInterceptorController:changeSecurityQuestion];
    
    BTSecurityNumberViewController *updateSecurityNumber = [self.storyboard instantiateViewControllerWithIdentifier:@"BTSecurityNumberViewController"];
    updateSecurityNumber.isScreenrelatedTo1FAMigration = NO;
    updateSecurityNumber.isScreenRelatedToInterceptorModule = YES;
    [self openInterceptorController:updateSecurityNumber];
    
//    BTOneFAInterceptViewController *oneFAInterceptVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BTOneFAInterceptViewController"];
//    //oneFAInterceptVC.delegate = self;
//    oneFAInterceptVC.isScreenrelatedTo1FAMigration = NO;
//    oneFAInterceptVC.isScreenRelatedToInterceptorModule = YES;
//    [self openInterceptorController:oneFAInterceptVC];
    
}

- (void)redirectToUpdate1FAInterceptor:(NSString *)securityQuestion
{
    // First time login with question "1FA"
    BTOneFAInterceptViewController *oneFAInterceptVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BTOneFAInterceptViewController"];
    oneFAInterceptVC.delegate = self;
    oneFAInterceptVC.isScreenRelatedToInterceptorModule = NO;
    oneFAInterceptVC.isScreenrelatedTo1FAMigration = YES;
    [self openInterceptorController:oneFAInterceptVC];
}

- (void)redirectToUpdate1FAMigrationInterceptor
{
    BTSecurityNumberViewController *updateSecurityNumber = [self.storyboard instantiateViewControllerWithIdentifier:@"BTSecurityNumberViewController"];
    updateSecurityNumber.delegate = self;
    updateSecurityNumber.isScreenrelatedTo1FAMigration = YES;
    updateSecurityNumber.isScreenRelatedToInterceptorModule = NO;
    [self openInterceptorController:updateSecurityNumber];
}

- (void)openInterceptorController:(UIViewController *)controller{
    
    if(!self.interceptorNavigationController){
        
        self.interceptorNavigationController = [[UINavigationController alloc] initWithRootViewController:controller];
        [self presentViewController:self.interceptorNavigationController animated:YES completion:nil];
        
    }
    else{
        
        [self.interceptorNavigationController pushViewController:controller animated:YES];
    }
}

- (void)redirectToGSProfileUpliftInterceptor:(NSString *)upliftUrl
{
    BTGSProfileUpliftInterceptViewController *gsProfileUpliftVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BTGSProfileUpliftInterceptViewController"];
    
    gsProfileUpliftVC.gsProfileUpliftUrl = upliftUrl;
    [self openGSProfileUpliftController:gsProfileUpliftVC];
}

- (void)openGSProfileUpliftController:(UIViewController *)controller{
    
    if(!self.interceptorNavigationController){
        
        self.interceptorNavigationController = [[UINavigationController alloc] initWithRootViewController:controller];
        [self presentViewController:self.interceptorNavigationController animated:YES completion:nil];
        
    }
    else{
        
        [self.interceptorNavigationController pushViewController:controller animated:YES];
    }
}


#pragma mark - BTSecurityNumberViewControllerDelegate (1FA intercept)

- (void)successfullyUpdatedSecurityNumberOnSecurityNumberViewController:(BTSecurityNumberViewController *)securityNumberViewController
{
    //[self.viewModel checkForInterceptor];
}

#pragma mark - 

- (void)showInlineErrorMessage
{
    if(![AppManager isInternetConnectionAvailable])
    {
        DDLogError(@"Sign-In API Reqeust failed due to no internet connection");
        [self displayInlineErrorMessageWithMessage:kNoConnectionMessage withTextFieldColorChanged:NO];
    }
    else
    {
        DDLogError(@"ERROR: General API error");
        [self displayInlineErrorMessageWithMessage:kSignErrorMessage withTextFieldColorChanged:NO];
        [AppManager trackGenericAPIErrorBeforeLoginOnPage:OMNIPAGE_LOGIN];
        
    }
}

- (void)clearPasswordTextFieldOnError
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[self passwordTextField] setText:@""];
        [self.passwordTextField sendActionsForControlEvents:UIControlEventEditingChanged];
    });
}

#pragma mark - animation handler
//Animating the signin page elements
- (void)animateSignInScreenItems {
    _isInitialSetupForTableView = NO;
    [self.view layoutIfNeeded];
    if (!_isAnimationDone) {
        [UIView animateWithDuration:0.8 delay:0.0 options:0 animations:^{
            
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            _isAnimationDone = YES;
        }];
    }
}

- (void)loginButtonAnimation {
    
    [self.view layoutIfNeeded];

    [_loadingView setHidden:NO];
    
    [self loadingSpinView];
    
}

#pragma mark - Log in action methods

- (IBAction)logInButtonAction:(id)sender {
    DDLogInfo(@"Sign In button pressed.");
    [self startSigningIn];
}

//In-App browser link for forgottenBTID
- (IBAction)forgottenBTIdButtonAction:(id)sender {
    
    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
    
    viewController.targetURLType = BTTargetURLTypeForgotUsername;
    
    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:NO andIsNeedCloseButton:YES];
    
    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{
        
    }];
    
}

//In-App browser link for the forgotten BTPassword
- (IBAction)forgottenBTPasswordButtonAction:(id)sender {
    
    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
    
    viewController.targetURLType = BTTargetURLTypeForgotPassword;
    
    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:NO andIsNeedCloseButton:YES];
    
    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{
        
    }];
    
}

//In-App browser link for dont have BTID
- (IBAction)dontHaveBTIdButtonAction:(id)sender {
    
    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
    
    viewController.targetURLType = BTTargetURLTypeRegistration;
    
    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:NO andIsNeedCloseButton:YES];
    
    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{
        
    }];
    
}


/*
  (LP) Provide handler to show/hide password in readable text format
 */
- (IBAction)displayPasswordTextInReadableFormat:(id)sender {
    
    // (LP) Check the current state and update the ui.
    BOOL isSecurePasswordEntry = [[self passwordTextField] isSecureTextEntry];
    
    if (isSecurePasswordEntry) {
        
        [[self passwordTextField] setSecureTextEntry:NO];
        [[self showOrHidePasswordButton] setImage:[UIImage imageNamed:@"icon_password_eye_purple.png"] forState:UIControlStateNormal];
    } else {
        
        [[self passwordTextField] setSecureTextEntry:YES];
        [[self showOrHidePasswordButton] setImage:[UIImage imageNamed:@"icon_password_eye_gray.png"] forState:UIControlStateNormal];
    }
    
}

#pragma mark - textfield delegate methods

- (BOOL) textFieldShouldReturn : (UITextField*) textField {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        // Attempt to sign in if our validation passes.
        if ([[self logInButton] isEnabled]) {
            [textField resignFirstResponder];
            [textField layoutIfNeeded];
            [[self userNameTextField] endEditing:true];
            [[self passwordTextField] endEditing:true];
            [self logInButtonAction:nil];
        }
        
        // Otherwise skip to the next available input field.
        else {
            NSInteger nextInputTag = [textField tag] + 1;
            UIView* nextInputView = [[self view] viewWithTag:nextInputTag];
            
            if (nextInputView) {
                [textField resignFirstResponder];
                [textField layoutIfNeeded];
                [nextInputView becomeFirstResponder];
            }
            
            else {
                [textField resignFirstResponder];
                [textField layoutIfNeeded];
            }
        }
    });
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [self updateContainerViewWithDisabledStateForTextField:textField];
    [textField resignFirstResponder];
    [textField layoutIfNeeded];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self updateContainerViewWithEnabledStateForTextField:textField];
    [textField becomeFirstResponder];
    [self clearInlineErrorMessage];
}

#pragma mark - display notification alert label

- (void) displayInlineErrorMessageWithMessage:(NSString*)message withTextFieldColorChanged:(BOOL)changeColor {
    
    [[self notificationAlertLabel] setHidden:false];
    [[self notificationAlertLabel] setText:message];
    [[self notificationLoginErrorView] setHidden:false];
    
    if (changeColor) {
        //Change border colour of username and password textfields
        [self.btIdLabel setTextColor:[BrandColours colourBtErrorColor]];
        [self.passwordLabel setTextColor:[BrandColours colourBtErrorColor]];
        [[self.userNameTextFieldContainerView layer] setBorderColor:[[BrandColours colourBtErrorColor] CGColor]];
        [[self.passwordTextFieldContainerView layer] setBorderColor:[[BrandColours colourBtErrorColor] CGColor]];
    }
    
}

- (void)displayAlertMessageWithTitle:(NSString *)title andMessage:(NSString *)message
{
    UIAlertController *logInErrorAlertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
    {
        [logInErrorAlertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [logInErrorAlertController addAction:okAction];
    
    [self presentViewController:logInErrorAlertController animated:YES completion:nil];
}

- (void)clearInlineErrorMessage {
    
    [[self notificationAlertLabel] setText:@""];
    [[self notificationAlertLabel] setHidden:true];
    [[self notificationLoginErrorView] setHidden:true];
    
    //chnage border colour of username and password textfields
    [self.btIdLabel setTextColor:[BrandColours colourBtNeutral90]];
    [self.passwordLabel setTextColor:[BrandColours colourBtNeutral90]];
    [[self.userNameTextFieldContainerView layer] setBorderColor:[[BrandColours colourBtNeutral60] CGColor]];
    [[self.passwordTextFieldContainerView layer] setBorderColor:[[BrandColours colourBtNeutral60] CGColor]];
    
}

- (void)updateContainerViewWithEnabledStateForTextField:(UITextField *)textField
{
    if (textField == _userNameTextField)
    {
        [[self.userNameTextFieldContainerView layer] setBorderColor:[[BrandColours colourBtNeutral60] CGColor]];
    }
    else
    {
        [[self.passwordTextFieldContainerView layer] setBorderColor:[[BrandColours colourBtNeutral60] CGColor]];
    }
}

- (void)updateContainerViewWithDisabledStateForTextField:(UITextField *)textField
{
    if (textField == _userNameTextField)
    {
        [[self.userNameTextFieldContainerView layer] setBorderColor:[[BrandColours colourBtNeutral60] CGColor]];
    }
    else
    {
        [[self.passwordTextFieldContainerView layer] setBorderColor:[[BrandColours colourBtNeutral60] CGColor]];
    }
}

#pragma mark - alternative login authentication
- (IBAction)alternativeLoginSwitchAction:(id)sender {
    UISwitch *vordelSwitch = (UISwitch *)sender;
    DDLogInfo(@"switch state changed\t\t\t:::%d",vordelSwitch.isOn);
//    [[BTUserAuthenticationWrapper instance] setIsVordelAuthentication:vordelSwitch.isOn];
    [[NSUserDefaults standardUserDefaults] setBool:vordelSwitch.isOn forKey:@"isVordelAuthentication"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Notification Methods

//Keyboard notification methods on showing the keyboard
- (void)keyboardWillShow:(NSNotification *)notification
{
    
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:[[notification.userInfo valueForKey:@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue]
                          delay:0.0
                        options:[[notification.userInfo valueForKey:@"UIKeyboardAnimationCurveUserInfoKey"] doubleValue]
                     animations:^{
                         
                         CGRect keyboardEndFrame = [[notification.userInfo valueForKey:@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
                         
                         
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         
                         _tapGestureWhileKeyboardActive = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOutsideUserInputContainerViewWhileKeyboardActive:)];
                         [self.view addGestureRecognizer:_tapGestureWhileKeyboardActive];
                     }];
    
    NSDictionary* userInfo = [notification userInfo];
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [self manageSignInViewWithKeyboardHeight:keyboardSize.height];

}

- (void)manageSignInViewWithKeyboardHeight:(CGFloat)keyboardHeight
{
    CGFloat keyboardPosition = self.view.frame.size.height - keyboardHeight;
    
    CGFloat visibleArea = self.scrollView.frame.size.height - keyboardHeight;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0,visibleArea/1.3, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
}

//Keyboard notification methods on keyboard hide
- (void)keyboardWillHide:(NSNotification *)notification
{

    [self.view layoutIfNeeded];
    [UIView animateWithDuration:[[notification.userInfo valueForKey:@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue]
                          delay:0.0
                        options:[[notification.userInfo valueForKey:@"UIKeyboardAnimationCurveUserInfoKey"] doubleValue]
                     animations:^{
                         
                         
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         
                     }];
    
    
}

#pragma mark - UIGesture Recognizer Methods

- (void)userTappedOutsideUserInputContainerViewWhileKeyboardActive:(UITapGestureRecognizer *)gestureRecognizer
{
    CGPoint locationInView = [gestureRecognizer locationInView:self.view];
    BOOL isGestureInsideUserNameInputTextField = CGRectContainsPoint(self.userNameTextField.frame, locationInView);
    BOOL isGestureInsidePasswordInputTextField = CGRectContainsPoint(self.passwordTextField.frame, locationInView);
    if(!(isGestureInsideUserNameInputTextField || isGestureInsidePasswordInputTextField))
    {
        [self.userNameTextField resignFirstResponder];
        [self.passwordTextField resignFirstResponder];
    }
}


#pragma mark - DLMSignInScreenDelegate Methods

- (void)signInScreen:(DLMSignInScreen *)signInScreen loginSuccesfulWithUsername:(NSString *)username password:(NSString *)password token:(BTAuthenticationToken *)token smSession:(BTSMSession *)smSession andUserDetails:(NSDictionary *)userDetails
{
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:kIsAppOpenedFromEmailVerification];
    // (LP) Used to show/hide indicator view
    self.logInRequestInProgress = NO;
    
    //RLM- If intercepter is present in NLUserQuery API then we have to SKIP pin creation screen.
    NSString *intercepterString = [userDetails valueForKey:@"interfaces"];
    [_indicatorView stopAnimation];
    [_loadingView setHidden:YES];
    if ( intercepterString == (NSString *)[NSNull null] ){
        intercepterString = nil;
    }
        
    if(intercepterString && intercepterString.length >0 )
    {
       //if([intercepterString containsString:kFirstTimeLogin] || [intercepterString containsString:kCreatePassword] || [intercepterString containsString:kUpdate1FA])
        if([intercepterString containsString:kFirstTimeLogin] || [intercepterString containsString:kCreatePassword])
        //if([intercepterString containsString:kFirstTimeLogin] )
       {
          DDLogInfo(@"INFO: Login successfull with Intercepter move to Home Screen");
           
           self.currentUserForIntercepter = [userDetails valueForKey:@"user"];
           
           //Find the cug key that is used in reset password api
           
           if([self.currentUserForIntercepter.arrayOfCugs count] >0)
           {
               CDCug *cug = [self.currentUserForIntercepter.arrayOfCugs firstObject];
               _currentSelectedKeyForIntercepter = cug.groupKey;
           }
           
           [AppManager sharedAppManager].usernameForSignInIntercepter = username;
            [[NSUserDefaults standardUserDefaults] setObject:password forKey:kPasswordLoginIntercepter];
            [[NSUserDefaults standardUserDefaults] setObject:token.accessToken forKey:kTokenLoginIntercepter];
            [[NSUserDefaults standardUserDefaults] setObject:smSession.smsessionString forKey:kSMSessionLoginIntercepter];
           [[NSUserDefaults standardUserDefaults] setObject:_currentSelectedKeyForIntercepter forKey:kGroupKeyLoginIntercepter];
           [[NSUserDefaults standardUserDefaults] synchronize];
    
         //  [AppManager sharedAppManager].isIntercepterPresent = @"Yes";
           
           [self.viewModel saveInterceptorWithOrder:[userDetails valueForKey:@"order"] andInterfaces:intercepterString];
           
           [self.viewModel checkForInterceptor];
           
    
       } else if ([intercepterString containsString:kGSProfileUplift]) {
           // Redirect the user to Webview with nextStepUrl
           BTUser* user = [userDetails valueForKey:@"user"];
           [self redirectToGSProfileUpliftInterceptor:user.nextStepURL];
           
       } else {
         [self redirectToPinScreenWithUsername:username andPassword:password andToken:token andSMSession:smSession andUserDetails:userDetails];
       }
    }
    else
    {
        [self redirectToPinScreenWithUsername:username andPassword:password andToken:token andSMSession:smSession andUserDetails:userDetails];
       /* [AppManager sharedAppManager].usernameForSignInIntercepter = username;
        [[NSUserDefaults standardUserDefaults] setObject:password forKey:kPasswordLoginIntercepter];
        [[NSUserDefaults standardUserDefaults] setObject:token.accessToken forKey:kTokenLoginIntercepter];
        [[NSUserDefaults standardUserDefaults] setObject:smSession.smsessionString forKey:kSMSessionLoginIntercepter];
        [[NSUserDefaults standardUserDefaults] setObject:_currentSelectedKeyForIntercepter forKey:kGroupKeyLoginIntercepter];
        [[NSUserDefaults standardUserDefaults] synchronize];
         [self performSelector:@selector(hack) withObject:nil afterDelay:0.2];*/
    }
}


- (void)signInScreen:(DLMSignInScreen *)signInScreen hasPendingIntercenptorWithType:(InterceptorType)intercepterType
{
        
        switch (intercepterType) {
                
            case InterceptorTypeFirstTimeLogin:
            {
                if([self.currentUserForIntercepter.securityQuestion isEqualToString:@"1FA"]){
                    [self redirectToUpdate1FAInterceptor:self.currentUserForIntercepter.securityQuestion];
                } else{
                    [self redirectToFirstTimeLoginIntercepterWithQuestion:self.currentUserForIntercepter.securityQuestion];
                }
            }
                break;
                
            case InterceptorTypeCreatePassword:
            {
                [self redirectToCreatePasswordIntercepter];
            }
                break;
             
            case IntercepterType1FA:
            {
                //[self redirectToUpdate1FAInterceptor:self.currentUserForIntercepter.securityQuestion];
                [self redirectToUpdate1FAMigrationInterceptor];
            }
                break;
            
                
                default:
                break;
        }

}


- (void)redirectToPinScreenWithUsername:(NSString *)username andPassword:(NSString *)password andToken:(BTAuthenticationToken *)token andSMSession:(BTSMSession *)smSession andUserDetails:(NSDictionary *)userDetails
{
    
    DDLogInfo(@"INFO: Login successfull and move to PIN Screen.");
    // (hd) Now its time to show the PIN Creation Screen.
    BTPINViewController *pinRoadblock = (BTPINViewController *)[self.storyboard instantiateViewControllerWithIdentifier:kStoryboardViewPinRoadblock];
    // Set flag to indicate that our roadblock will be responsible for
    // authenticating.
    [pinRoadblock setIsPinSetupJourney:YES];
    pinRoadblock.pinCreationInfoDic = [NSDictionary dictionaryWithObjectsAndKeys:username, @"username", password, @"password", token, @"token", smSession, @"smsession", userDetails, @"userDetails", nil];
    pinRoadblock.pinControllerDelegate = self;
    [self.navigationController pushViewController:pinRoadblock animated:YES];

}

- (void)signInScreen:(DLMSignInScreen *)signInScreen loginAttemptFailedWithWebServiceError:(NLWebServiceError *)error
{
    // (LP) Used to show/hide indicator view
    self.logInRequestInProgress = NO;
    NSError *logInError = error.error;
    
    [_indicatorView removeFromSuperview];
     [_loadingView setHidden:YES];

    if([logInError.domain isEqualToString:BTNetworkErrorDomain])
    {
        switch (logInError.code)
        {
            case BTNetworkErrorCodeVordelUserUnauthenticated:
            {
                DDLogError(@"ERROR: Entered username or password are incorrect.");
                
                //Track error
                
                NSMutableDictionary *data = [NSMutableDictionary dictionary];
                
                NSString *pageName = OMNIPAGE_LOGIN;
                NSString *error = OMNIERROR_LOGIN_INVALIDCREDENTIALS;
                NSString *timestamp = [AppManager NSStringWithOmnitureFormatFromNSDate:[NSDate date]];
                
                [data setValue:@"Not Logged In" forKey:@"LoginStatus"];
                [data setValue:timestamp forKey:@"TimeStamp"];
                
                [OmnitureManager trackError:error onPageWithName:pageName contextInfo:data];
                [self clearPasswordTextFieldOnError];
                [self displayInlineErrorMessageWithMessage:kInvalidCredentialsMessage withTextFieldColorChanged:YES];
                break;
            }

            case BTNetworkErrorCodeVordelAccountLocked:
            {
                DDLogError(@"ERROR: when User profile locked for 20 minutes.");
                [self trackOmnitureError:OMNIERROR_PROFILE_LOCKED];
                [self clearPasswordTextFieldOnError];
                [self displayInlineErrorMessageWithMessage:kProfileLockedMessage withTextFieldColorChanged:NO];
                break;
            }

                
            case BTNetworkErrorCodeVordelPlannedServiceOutage:
            {
                DDLogError(@"ERROR: The app is under maintanance.");
                [self displayAlertMessageWithTitle:@"Under Maintenance" andMessage:kAppLockDownToPinUnlockScreenAlertMessageUnderMaintenance];
                break;
            }
            
            case BTNetworkErrorCodeVordelUnplannedServiceOutage:
            {
                DDLogError(@"ERROR: The app is under maintanance.");
                [self displayAlertMessageWithTitle:@"Under Maintenance" andMessage:kAppLockDownToPinUnlockScreenAlertMessageUnderMaintenance];
                break;
            }
            case BTNetworkErrorCodeVordelInvalidDataError:
            {
                DDLogError(@"ERROR: Invalid data error when Email is not verified.");
                [self clearPasswordTextFieldOnError];
                [self displayInlineErrorMessageWithMessage:kEmailNotVerifiedErrorMessage withTextFieldColorChanged:NO];
                break;
            }
            case BTNetworkErrorCodeAPINoDataFound:
            case BTNetworkErrorCodeAPIServerError:
            {
                DDLogError(@"ERROR: No data found either in Sign in OR updateDNP call.");
                [self clearPasswordTextFieldOnError];
                [self displayInlineErrorMessageWithMessage:kDefaultErrorMessage withTextFieldColorChanged:NO];
                break;
            }
            case BTNetworkErrorCodeIncorrectActivationCode:
            {
                DDLogError(@"ERROR: In-correct activation code in QueryUserDetail call.");
                [self clearPasswordTextFieldOnError];
                [self showAlertWithIncorrectActivationCode];
                break;
            }
            case BTNetworkErrorCodeEmailAlreadyVerified:
            {
                DDLogError(@"ERROR: Email already verified in QueryUserDetail call.");
                [self showAlertWithUserAlreadyVerifiedError];
                break;
            }
            case BTNetworkErrorCodeEmailNotVerified:
            {
                DDLogError(@"ERROR: Email not verified in QueryUserDetail call.");
                [self clearPasswordTextFieldOnError];
                [self displayInlineErrorMessageWithMessage:kEmailNotVerifiedErrorMessage withTextFieldColorChanged:NO];
                break;
            }
            case BTNetworkErrorCodeVordelLegacyAccount:
            {
                DDLogError(@"ERROR: Legacy account, not able to login.");
                NSMutableDictionary *data = [NSMutableDictionary dictionary];
                [data setValue:@"Not Logged In" forKey:kOmniLoginStatus];
                
                [OmnitureManager trackError:OMNIERROR_LEGACY_PROFILE_LOGIN onPageWithName:OMNIPAGE_LOGIN contextInfo:data];
                [self clearPasswordTextFieldOnError];
                [self displayInlineErrorMessageWithMessage:kLegacyUserLoginErrorMessage withTextFieldColorChanged:NO];
                break;
            }
                
            default:
            {
                DDLogError(@"ERROR: Default error other than LoginInvalid & UserProfileLocked. Invalid Error Code.");
                [self clearPasswordTextFieldOnError];
                [self showInlineErrorMessage];
                break;
            }
        }
    }
    else
    {
        [self clearPasswordTextFieldOnError];
        [self showInlineErrorMessage];
        [_loadingView setHidden:YES];
    }
    
}



#pragma mark - BTPINViewControllerDelegate Methods

- (void)pinViewController:(BTPINViewController *)pinViewController userHasSuccesfullySetupThePINWithInfo:(NSDictionary *)infoDic
{
    [self.delegate signInViewController:self userSuccesfullyLoggedInAndCreatedPINWithInfo:infoDic];
}


@end
