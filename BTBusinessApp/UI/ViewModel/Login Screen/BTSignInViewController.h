//
//  BTSignInViewControllerTableViewController.h
//  BTBusinessApp
//
//  Created by Rohini Kumar on 13/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTSignInViewController;

@protocol BTSignInViewControllerDelegate <NSObject>

- (void)signInViewController:(BTSignInViewController *)controller userSuccesfullyLoggedInAndCreatedPINWithInfo:(NSDictionary *)userInfo;

@end

@interface BTSignInViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, weak) id <BTSignInViewControllerDelegate> delegate;

@property (strong, nonatomic) NSString* tmpBtId;
@property (strong, nonatomic) NSString* tmpPassword;
@property (nonatomic) BOOL logInRequestInProgress;
@property (nonatomic) NSInteger numRetries;
@property (nonatomic) BOOL shouldPopulateUsernameForLoggedInUser;

@end
