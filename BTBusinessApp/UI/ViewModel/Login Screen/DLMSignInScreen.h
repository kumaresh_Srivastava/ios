//
//  DLMSignInScreen.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 29/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"

@class DLMSignInScreen;
@class BTAuthenticationToken;
@class BTSMSession;
@class NLWebServiceError;
@class BTUser;


#define kFirstTimeLogin @"FirstTimeLogin"
#define kCreatePassword @"ResetPassword"
#define kUpdateContact @"UpdateContact"
#define kUpdateProfile @"UpdateProfile"
#define kUpdate1FA @"OneFA"


typedef enum{
    InterceptorTypeUnknown,
    InterceptorTypeFirstTimeLogin,
    InterceptorTypeCreatePassword,
    InterceptorTypeUpdateProfile,
    InterceptorTypeUpdateContact,
    IntercepterType1FA
}InterceptorType;




@protocol DLMSignInScreenDelegate <NSObject>

- (void)signInScreen:(DLMSignInScreen *)signInScreen loginSuccesfulWithUsername:(NSString *)username password:(NSString *)password token:(BTAuthenticationToken *)token smSession:(BTSMSession *)smSession andUserDetails:(NSDictionary *)userDetails;

- (void)signInScreen:(DLMSignInScreen *)signInScreen loginAttemptFailedWithWebServiceError:(NLWebServiceError *)error;

- (void)signInScreen:(DLMSignInScreen *)signInScreen hasPendingIntercenptorWithType:(InterceptorType)intercepterType;

@end

@interface DLMSignInScreen : DLMObject {

    NSMutableArray *_interceptorArray;
    
}

@property (nonatomic, weak) id <DLMSignInScreenDelegate> signInScreenDelegate;

/* The username and password parameters here cannot be nil. */
- (void)performLoginWithUsername:(NSString *)username andPassword:(NSString *)password;

/* Returns username for currently logged in user. */
- (NSString *)usernameForCurrentlyLoggedInUser;

- (void)saveInterceptorWithOrder:(NSString *)order andInterfaces:(NSString *)interfaces;

- (void)checkForInterceptor;

@end
