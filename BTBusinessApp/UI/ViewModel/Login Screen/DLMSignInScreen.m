//
//  DLMSignInScreen.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 29/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMSignInScreen.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "BTAuthenticationManager.h"
#import "NLWebServiceError.h"
#import "AppManager.h"
#import "NLQueryUserDetailsWebService.h"
#import "NLUpdateDNPWebService.h"
#import "BTSMSession.h"
#import "BTAuthenticationToken.h"
#import "NSObject+APIResponseCheck.h"

@interface DLMSignInScreen ()<NLQueryUserDetailsWebServiceDelegate> {
    
    NSString *_username;
    NSString *_password;
    NSNotification *signinSuccessNotificationObject;

}
@property (nonatomic) NLQueryUserDetailsWebService *queryUserDetailsWebService;

@end

@implementation DLMSignInScreen

#pragma mark - Public Methods

- (void)performLoginWithUsername:(NSString *)username andPassword:(NSString *)password
{
    if(username && password)
    {
        _username = username;
        _password = password;
        
        [self addObserverToNotificationForSignInAuthenticationCall];
        [[BTAuthenticationManager sharedManager] signInAuthenticateWithUsername:username andPassword:password];
    }
    else
    {
        DDLogError(@"Either username or password or both of them are nil.");
        NSAssert(NO, @"Either username or password or both of them are nil.");
    }
}

- (NSString *)usernameForCurrentlyLoggedInUser
{
    return [AppDelegate sharedInstance].viewModel.app.loggedInUser.username;
}

#pragma mark - Private Helper Methods

- (void)fetchUserDetailsToVerifyEmailValidationWithAccessToken:(NSString *)accessToken {
    NSString *username = [signinSuccessNotificationObject.userInfo valueForKey:@"username"];
    _queryUserDetailsWebService.queryUserDetailsWebServiceDelegate = nil;
    _queryUserDetailsWebService = [[NLQueryUserDetailsWebService alloc] initWithUsername:username andAuthenticationToken:accessToken];
    _queryUserDetailsWebService.queryUserDetailsWebServiceDelegate = self;
    [_queryUserDetailsWebService resume];
}

- (void)addObserverToNotificationForSignInAuthenticationCall
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signInAuthenticationAPICallSuccessfullyFinishedNotification:) name:kSignInAuthenticationApiCallSuccessfullyFinishedNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signInAuthenticationAPICallFailedNotification:) name:kSignInAuthenticationApiCallFailedNotification object:nil];
}

- (void)removeObserverToNotificationForSignInAuthenticationCall
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSignInAuthenticationApiCallSuccessfullyFinishedNotification object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSignInAuthenticationApiCallFailedNotification object:nil];
}


#pragma mark - Intercepter Handlers

- (void)saveInterceptorWithOrder:(NSString *)order andInterfaces:(NSString *)interfaces
{
    //Interceptor
    if(!_interceptorArray){
        
        _interceptorArray = [[NSMutableArray alloc] init];
    }
    
    [_interceptorArray removeAllObjects];
    
    if([order validAndNotEmptyStringObject] && [interfaces validAndNotEmptyStringObject]){
        
        NSArray *ordersArray = [order componentsSeparatedByString:@","];
        NSArray *interfaceArray = [interfaces componentsSeparatedByString:@","];
        
        if([ordersArray count] == [interfaceArray count]){
            
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"intValue" ascending:YES];
            NSArray *sortedArray = [ordersArray sortedArrayUsingDescriptors:@[sortDescriptor]];
            
            for(NSString *order in sortedArray){
                
                NSInteger index = [ordersArray indexOfObject:order];
                NSString *interFace = [interfaceArray objectAtIndex:index];
                [_interceptorArray addObject:interFace];
            }
        }
    }
}


- (void)checkForInterceptor{
    
    InterceptorType interceptorType = InterceptorTypeUnknown;
    
    if(_interceptorArray && [_interceptorArray count] > 0){
        
        
        for(NSString *intercepterName in _interceptorArray)
        {
            if([intercepterName isEqualToString:kCreatePassword])
            {
             
                interceptorType = [self interceptorTypeForIntercentorName:intercepterName];
                break;
             
            }
            else if([intercepterName isEqualToString:kFirstTimeLogin])
            {
                interceptorType = [self interceptorTypeForIntercentorName:intercepterName];
                 break;
    
            }
            else if ([intercepterName isEqualToString:kUpdate1FA])
            {
                interceptorType = [self interceptorTypeForIntercentorName:intercepterName];
                break;
            }
            
        }
    }
    
    
    [self.signInScreenDelegate signInScreen:self hasPendingIntercenptorWithType:interceptorType];
}


- (InterceptorType)interceptorTypeForIntercentorName:(NSString *)interceptorName{
    
    InterceptorType interceptorType = InterceptorTypeUnknown;
    
    if([[interceptorName lowercaseString] isEqualToString:[kFirstTimeLogin lowercaseString]]){
        
        interceptorType = InterceptorTypeFirstTimeLogin;
    }
    else if([[interceptorName lowercaseString] isEqualToString:[kCreatePassword lowercaseString]]){
        
        interceptorType = InterceptorTypeCreatePassword;
    }
    else if([[interceptorName lowercaseString] isEqualToString:[kUpdateContact lowercaseString]]){
        
        interceptorType = InterceptorTypeUpdateContact;
    }
    else if([[interceptorName lowercaseString] isEqualToString:[kUpdateProfile lowercaseString]]){
        
        interceptorType = InterceptorTypeUpdateProfile;
    }
    else if ([[interceptorName lowercaseString] isEqualToString:[kUpdate1FA lowercaseString]] ){
        interceptorType = IntercepterType1FA;
    }
    
    return interceptorType;
    
}



#pragma mark - Notification Methods

- (void)signInAuthenticationAPICallSuccessfullyFinishedNotification:(NSNotification *)notification
{
    if ([[notification.userInfo valueForKey:@"username"] isEqualToString:_username])
    {
        DDLogInfo(@"Sign In authentication API call completed. Initiating user query details API");
       
        [self removeObserverToNotificationForSignInAuthenticationCall];
        signinSuccessNotificationObject = notification;
        BTAuthenticationToken *token = [signinSuccessNotificationObject.userInfo valueForKey:@"token"];
        [self fetchUserDetailsToVerifyEmailValidationWithAccessToken:token.accessToken];
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of signInAuthenticationAPICall but this notification is referring to some other username than for what the Sign In was request in this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of signInAuthenticationAPICall but this notification is referring to some other username than for what the Sign In was request in this class %@.", [self class]);
    }
}

- (void)signInAuthenticationAPICallFailedNotification:(NSNotification *)notification
{
    if ([[notification.userInfo valueForKey:@"username"] isEqualToString:_username])
    {
        [self removeObserverToNotificationForSignInAuthenticationCall];

        NLWebServiceError *error = [notification.userInfo valueForKey:@"error"];
        [self.signInScreenDelegate signInScreen:self loginAttemptFailedWithWebServiceError:error];
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of signInAuthenticationAPICall but this notification is referring to some other username than for what the Sign In was request in this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of signInAuthenticationAPICall but this notification is referring to some other username than for what the Sign In was request in this class %@.", [self class]);
    }
}

#pragma mark - NLQueryUserDetailsWebService Methods


- (void)getUserWebService:(NLQueryUserDetailsWebService *)webService successfullyFetchedUserData:(BTUser *)user
{
    // (hd) Save the latest data from web service into the persistence.
    if(webService == _queryUserDetailsWebService)
    {
        BTAuthenticationToken *token = [signinSuccessNotificationObject.userInfo valueForKey:@"token"];
        BTSMSession *smSession = [signinSuccessNotificationObject.userInfo valueForKey:@"smsession"];
        NSString *username = [signinSuccessNotificationObject.userInfo valueForKey:@"username"];
        
        _queryUserDetailsWebService.queryUserDetailsWebServiceDelegate = nil;
        _queryUserDetailsWebService = nil;
        
        NSDictionary *userDetailsDic = nil;
      
        userDetailsDic = [NSDictionary dictionaryWithObjectsAndKeys:user, @"user", webService.interfaces, @"interfaces", webService.order, @"order" , nil];
        
        [self.signInScreenDelegate signInScreen:self loginSuccesfulWithUsername:username password:_password token:token smSession:smSession andUserDetails:userDetailsDic];
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLQueryUserDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLQueryUserDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}


- (void)getUserWebService:(NLQueryUserDetailsWebService *)webService failedToFetchUserDataWithWebServiceError:(NLWebServiceError *)error
{
    if(webService == _queryUserDetailsWebService)
    {
        DDLogError(@"Failed to fetch User details in Sign In Screen");
        
        [self.signInScreenDelegate signInScreen:self loginAttemptFailedWithWebServiceError:error];
        _queryUserDetailsWebService.queryUserDetailsWebServiceDelegate = nil;
        _queryUserDetailsWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLQueryUserDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLQueryUserDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}


@end
