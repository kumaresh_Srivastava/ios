//
//  DLMPinCreationScreen.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 19/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"

@class DLMPinCreationScreen;

typedef NS_ENUM(NSUInteger, DLMPinCreationTouchIDState) {
    DLMPinCreationTouchIDStateTouchIDNotAvailable,
    DLMPinCreationTouchIDStateTouchIDNotEnrolled,
    DLMPinCreationTouchIDStateTouchIDEnabled,
};

typedef NS_ENUM(NSUInteger, DLMPinCreationFaceIDState) {
    DLMPinCreationFaceIDStateFaceIDNotAvailable,
    DLMPinCreationFaceIDStateFaceIDNotEnrolled,
    DLMPinCreationFaceIDStateFaceIDEnabled,
};

@protocol DLMPinCreationScreenDelegate <NSObject>


@end

@interface DLMPinCreationScreen : DLMObject {
    
}

@property (nonatomic, weak) id <DLMPinCreationScreenDelegate> pinCreationScreenDelegate;
@property (nonatomic, readonly) DLMPinCreationTouchIDState currentStatusOfFingerprintAuthenticationPolicy;
@property (nonatomic, readonly) DLMPinCreationFaceIDState currentStatusOfFaceIDAuthenticationPolicy;

/* Checks if the device supports fingerprint scanning */
- (void)checkDeviceSupportsFingerprintScanningStatus;
- (void)enableTouchIdForUnlock;

/* Check if the device supports Face ID */
- (void)checkDeviceSupportsFaceIDStatus;
- (void)enableFaceIDForUnlock;

@end

