//
//  DLMPinUnlockScreen.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 13/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"

@class DLMPinUnlockScreen;
@class NLWebServiceError;

typedef NS_ENUM(NSUInteger, DLMPinUnlockTouchIDState) {
    DLMPinUnlockTouchIDStateTouchIDNotAvailable,
    DLMPinUnlockTouchIDStateTouchIDNotEnrolled,
    DLMPinUnlockTouchIDStateTouchIDEnabled,
};

typedef NS_ENUM(NSUInteger, DLMPinUnlockFaceIDState) {
    DLMPinUnlockFaceIDStateFaceIDNotAvailable,
    DLMPinUnlockFaceIDStateFaceIDNotEnrolled,
    DLMPinUnlockFaceIDStateFaceIDEnabled,
};

@protocol DLMPinUnlockScreenDelegate <NSObject>

- (void)loggedInUserSuccessfullyAuthenticatedOnPinUnlockScreen:(DLMPinUnlockScreen *)screen;

- (void)pinUnlockScreen:(DLMPinUnlockScreen *)screen loginAttemptFailedForLoggedInUserWithWebServiceError:(NLWebServiceError *)error;

@end

@interface DLMPinUnlockScreen : DLMObject {

}

@property (nonatomic, weak) id <DLMPinUnlockScreenDelegate> pinUnlockScreenDelegate;
@property (nonatomic, readonly) DLMPinUnlockTouchIDState currentStatusOfFingerprintAuthenticationPolicy;
@property (nonatomic, readonly) DLMPinUnlockFaceIDState currentStatusOfFaceAuthenticationPolicy;

/* This Method checks if device supports fingerprint scanning */
- (void)checkDeviceSupportsFingerprintScanningStatus;

/* This Method checks if device supports face scanning */
- (void)checkDeviceSupportsFaceScanningStatus;

/* The username and password parameters here cannot be nil. */
- (void)performLoginForCurrentlyLoggedInUser;

/* This method returns the pin stored in keychain for the logged in User. */
- (NSString *)pinForCurrentlyLoggedInUser;

@end
