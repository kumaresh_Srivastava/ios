//
//  DLMPinCreationScreen.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 19/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMPinCreationScreen.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"

@implementation DLMPinCreationScreen

- (void)checkDeviceSupportsFingerprintScanningStatus
{
    // (LP) Get the local authentication context.
    LAContext* localAuthContext = [[LAContext alloc] init];
    NSError* localAuthError = nil;
    
    // (LP) Check if the user has enabled the feature.
    if ([localAuthContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&localAuthError]) {
        
        _currentStatusOfFingerprintAuthenticationPolicy = DLMPinCreationTouchIDStateTouchIDEnabled;
        DDLogInfo(@"Pin: Touch ID is supported on this device.");
        
    } else {
        
        // (LP) Check error code
        if ([localAuthError code]) {
            
            // (LP) The user hasn't registered any fingerprints.
            if ([localAuthError code] == LAErrorTouchIDNotEnrolled) {
                
                _currentStatusOfFingerprintAuthenticationPolicy = DLMPinCreationTouchIDStateTouchIDNotEnrolled;
                DDLogInfo(@"Pin: Touch ID is supported but no fingerprints are enrolled.");
                
            }
            
            // (LP) Not supported? This shouldn't happen.
            else {
                
                _currentStatusOfFingerprintAuthenticationPolicy = DLMPinCreationTouchIDStateTouchIDNotAvailable;
                DDLogInfo(@"Pin: Touch ID is not supported on this device. (%@)", localAuthError);
                
            }
        } else {
            
            _currentStatusOfFingerprintAuthenticationPolicy = DLMPinCreationTouchIDStateTouchIDNotAvailable;
            DDLogInfo(@"Pin: Touch ID is not supported on this device. (%@)", localAuthError);
            
        }
    }
}

- (void)enableTouchIdForUnlock
{
    // (lp) Enabling touch id unlock in app. In future user can use touch id/ fingerprint for unlock the app instead entering pin.
    [AppDelegate sharedInstance].viewModel.app.isTouchIDEnabledForUnlock = [NSNumber numberWithBool:YES];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
}

- (void)checkDeviceSupportsFaceIDStatus
{
    DDLogInfo(@"Pin: Checking for Face ID status.");
    LAContext* localAuthContext = [[LAContext alloc] init];
    NSError* localAuthError = nil;
    
//    if ([localAuthContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:nil]){
//        return ( context.biometryType == LABiometryTypeFaceID);
//    }
    
    if (@available(iOS 11.0, *)) {
        if ([localAuthContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&localAuthError]) {
            if(localAuthContext.biometryType == LABiometryTypeFaceID) {
                _currentStatusOfFaceIDAuthenticationPolicy = DLMPinCreationFaceIDStateFaceIDEnabled;
                DDLogInfo(@"Pin: Face ID is supported on this device.");
            } else {
                DDLogInfo(@"No biometry faceID");
                if ([localAuthError code]) {
                    if ([localAuthError code] == LAErrorBiometryNotEnrolled) {
                        _currentStatusOfFaceIDAuthenticationPolicy = DLMPinCreationFaceIDStateFaceIDNotEnrolled;
                        DDLogInfo(@"Pin: Face ID is supported but not enrolled.");
                    } else {
                        _currentStatusOfFaceIDAuthenticationPolicy = DLMPinCreationFaceIDStateFaceIDNotAvailable;
                        DDLogInfo(@"Pin: Face ID is not supported on this device. (%@)", localAuthError);
                    }
                } else {
                    _currentStatusOfFaceIDAuthenticationPolicy = DLMPinCreationFaceIDStateFaceIDNotAvailable;
                    DDLogInfo(@"Pin: Face ID is not supported on this device. (No Auth Error code)");
                }
            }
        } else {
            _currentStatusOfFaceIDAuthenticationPolicy = DLMPinCreationFaceIDStateFaceIDNotAvailable;
            DDLogInfo(@"Pin: Face ID is not supported on this device. (No Auth Error code)");
        }
    } else {
        // Fallback on earlier versions
    }
}

- (void)enableFaceIDForUnlock
{
    [AppDelegate sharedInstance].viewModel.app.isFaceIDEnabledForUnlock = [NSNumber numberWithBool:YES];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
}

@end

