//
//  BTPINViewController.m
//  BTBusinessApp
//
//  Created by Rohini Kumar on 14/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

/*
 Console Error:Can't find keyplane that supports type 4 for keyboard iPhone-Portrait-NumberPad; using 563160167_Portrait_iPhone-Simple-Pad_Default
 the above warning in console is coming because of xcode bug reference:https://forums.developer.apple.com/thread/19639
 */

#import "BTPINViewController.h"
#import "BTUICommon.h"
#import <UIView+Shake/UIView+Shake.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import "DLMPinUnlockScreen.h"
#import "DLMPinCreationScreen.h"
#import "BTEnableTouchIdView.h"
#import "BTTouchIdDisabledView.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "AppDelegate.h"
#import "CDUser.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "PlaygroundViewController.h"
#import "CustomSpinnerAnimationView.h"
#import "NLWebServiceError.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "AppConstants.h"

@interface BTPINViewController () <DLMPinCreationScreenDelegate, DLMPinUnlockScreenDelegate, BTEnableTouchIdViewDelegate, BTTouchIdDisabledViewDelegate> {
    BOOL _isAnimationDone;
    UIView *_overlayView;
    BTEnableTouchIdView *_enableTouchIDView;
    BTTouchIdDisabledView *_touchIdDisabledView;
    BOOL _isTextFieldEnabled;
    BOOL _isTouchIdCancelled;
    CustomSpinnerAnimationView *_loaderView;
    // (VM) to check whether use authenticated with touch id, based on that we will hide keyboard when app becomes or resigns active
    BOOL isAuthenticatedUsingTouchID;
    // (VM) Boolean to check whether app is locked or minimized (touch id will be cannceled by system in that case)
    BOOL touchIDCancelledBySystem;
}

@property (nonatomic, readwrite) DLMPinUnlockScreen *unlockViewModel;
@property (nonatomic, readwrite) DLMPinCreationScreen *creationViewModel;

@property (weak, nonatomic) IBOutlet UIButton *enterPasswordButton;
@property (weak, nonatomic) IBOutlet UILabel *pinMessageLabel;

@property (weak, nonatomic) IBOutlet UIView *pinErrorMessageView;

@property (nonatomic,strong) NSString *PIN;
@property (strong, nonatomic) IBOutlet UILabel *promptLabel;
@property (strong, nonatomic) IBOutlet UIView *pinDotsContainerView;//
@property (weak, nonatomic) IBOutlet UIView *InputPinContainerView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;


@property (weak, nonatomic) IBOutlet UIView *pinDotBoxView01;
@property (weak, nonatomic) IBOutlet UIView *pinDotBoxView02;
@property (weak, nonatomic) IBOutlet UIView *pinDotBoxView04;
@property (weak, nonatomic) IBOutlet UIView *pinDotBoxView03;

@property (strong, nonatomic) IBOutlet UIView *pinDotView01;
@property (strong, nonatomic) IBOutlet UIView *pinDotView02;
@property (strong, nonatomic) IBOutlet UIView *pinDotView03;
@property (strong, nonatomic) IBOutlet UIView *pinDotView04;
@property (strong, nonatomic) IBOutlet UIImageView *animationImageView;
@property (weak, nonatomic) IBOutlet UILabel *authnenticationLabel;
@property (strong, nonatomic) IBOutlet UITextField *pinEntryTextField;
@property (strong, nonatomic) IBOutlet UIView *keyboardPlaceholderView;
@property (weak, nonatomic) IBOutlet UIView *spinnerView;

@property (strong,nonatomic) UIView *separatorViewForKeyboard;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *enterPasswordButtonTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pinMessageLabelTopConstraint;

@end

@implementation BTPINViewController

static NSInteger const kMaxNumPinEntryAttempts = 3;

static NSTimeInterval const kStateSwitchDelay = 0.25;//0.15 remove this comment after testing
static NSTimeInterval const kKickToAuthDelayInterval = 0.85;    // Minor delay after someone locks themselves out.
static NSTimeInterval const kAnimationDurationInterval = 0.68;  // 25 FPS.

static NSInteger const kNumPinDotShakes = 10;
static CGFloat const kPinDotShakeDelta = 8.0;
static NSTimeInterval const kPinDotShakeSpeed = 0.05;

//static CGFloat const kPinDotsContainerHeightCompressed = 60.0;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
    
    // Configure / bootstrap.
    if (![self isPinSetupJourney])
    {
        DDLogInfo(@"Pin: It's not a setup journey");
        self.unlockViewModel = [[DLMPinUnlockScreen alloc] init];
        self.unlockViewModel.pinUnlockScreenDelegate = self;
        _isTextFieldEnabled = NO;
    } else {
        DDLogInfo(@"Pin: It's a setup journey");
        _isTextFieldEnabled = YES;
        self.creationViewModel = [[DLMPinCreationScreen alloc] init];
        self.creationViewModel.pinCreationScreenDelegate = self;
    }
    
    // Alloc required objects.
    [self setPinDotContainers:[NSMutableArray new]];
    [self setPinDots:[NSMutableArray new]];
    
    // Load frames and configure animation.
    NSArray* animationFrames = [BTUICommon framesForAnimationWithName:@"animate_tick" andLength:17];
    
    [[self animationImageView] setImage:[animationFrames lastObject]];
    [[self animationImageView] setAnimationImages:animationFrames];
    [[self animationImageView] setAnimationRepeatCount:1];
    [[self animationImageView] setAnimationDuration:kAnimationDurationInterval];
    
    [[self pinDotContainers] addObject:[self pinDotBoxView01]];
    [[self pinDotContainers] addObject:[self pinDotBoxView02]];
    [[self pinDotContainers] addObject:[self pinDotBoxView03]];
    [[self pinDotContainers] addObject:[self pinDotBoxView04]];
    
    [[self pinDots] addObject:[self pinDotView01]];
    [[self pinDots] addObject:[self pinDotView02]];
    [[self pinDots] addObject:[self pinDotView03]];
    [[self pinDots] addObject:[self pinDotView04]];
    
    // Set delegates.
    [[self pinEntryTextField] setDelegate:self];
    
    // Configure / bootstrap.
    if ([self isPinSetupJourney]) {
        [self.InputPinContainerView setHidden:NO];
        [self.pinDotsContainerView setHidden:NO];
        [self setCurrentState:StateSetupInit];
        [self setTmpPinForConfirmation:@""];
        
        DDLogInfo(@"Pin: PIN view controller has been displayed as part of setup journey.");
        //[OmnitureManager trackPage:OMNIPAGE_SETUP_PIN];
    } else {
        
        [self.pinMessageLabel setHidden:YES];
        [self.pinErrorMessageView setHidden:YES];
        
        [self.enterPasswordButton setHidden:NO];
        [self.enterPasswordButton setTitle:@"Forgotten PIN or different user?" forState:UIControlStateNormal];
        
        [self setCurrentState:StateRoadblockDefault];
        [self setTmpPinForConfirmation:[self.unlockViewModel pinForCurrentlyLoggedInUser]];

        
        DDLogInfo(@"Pin: PIN view controller has been displayed as roadblock. (PIN: %@)", self.PIN);
        
        // Face ID or Touch ID?
        LAContext *context = [[LAContext alloc] init];
        NSError *localAuthError = nil;
        if (@available(iOS 11.0, *)) {
            if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&localAuthError]) {
                if(context.biometryType == LABiometryTypeFaceID) {
                    DDLogInfo(@"Pin: Face ID check.");
                    [self checkIfUserCanUnlockWithFaceID];
                } else if (context.biometryType == LABiometryTypeTouchID) {
                    DDLogInfo(@"Pin: Touch ID check");
                    [self checkIfUserCanUnlockWithTouchID];
                }
            } else {
                DDLogInfo(@"Pin: Touch ID check");
                [self checkIfUserCanUnlockWithTouchID];
            }
        } else {
            DDLogInfo(@"Pin: Touch ID check (pre-iOS11");
            [self checkIfUserCanUnlockWithTouchID];
        }
        //[OmnitureManager trackPage:OMNIPAGE_PIN_LOGIN];
    }
    
    // Start monitoring network activity state changes.
    __weak typeof(self) weakSelf = self;
    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([weakSelf networkRequestInProgress]) {
            [weakSelf.authnenticationLabel setHidden:NO];
            [weakSelf.animationImageView setHidden:NO];
            [weakSelf showLoaderView];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
        }
        
        else {
            [weakSelf.animationImageView setHidden:YES];
            [weakSelf.authnenticationLabel setHidden:YES];
            [weakSelf hideLoaderView];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
        }
    }];
    
    // Start monitoring state changes.
    [RACObserve(self, currentState) subscribeNext:^(NSNumber* state) {
        [weakSelf refreshViewForCurrentState];
    }];
    
    // Monitor PIN entry text field.
    [[[self pinEntryTextField] rac_textSignal] subscribeNext:^(id next) {
        // Normalise everything first.
        [weakSelf colourPinPlaceholderDotsWithColour:[UIColor clearColor]];
        
        // Colour the correct number of dots.
        for (NSInteger i = 0; i < [[[weakSelf pinEntryTextField] text] length]; i ++) {
            [[[weakSelf pinDots] objectAtIndex:i] setBackgroundColor:[UIColor blackColor]];
        }
        if ([[[weakSelf pinEntryTextField] text] length] == 4) {
            // Debug.
            DDLogInfo(@"Pin: PIN entered. Checking...");
            DDLogInfo(@"Pin: %@", [[weakSelf pinEntryTextField] text]);
            
            if (![weakSelf pinEntryTextFieldIsFrozen]) {
                [weakSelf verifyPinAndUpdateStateForPin:[[weakSelf pinEntryTextField] text]];
            }
            else {
                DDLogInfo(@"Pin: Ignoring check as PIN text field has been locked.");
            }
        }
    }];
    
    // Special handling for small devices.
    if (IS_IPHONE_4_OR_LESS) {
        [[self promptLabel] setFont:[UIFont fontWithName:kBtFontRegular size:kBodyFontSizeCompressed]];
    }
    
    //(Salman)Code for dev mode only
    if(kIsDevMode){
        
        UIView *dummyView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                     0,
                                                                     50,
                                                                     50)];
        
        dummyView.backgroundColor = [UIColor clearColor];
        
        
        UITapGestureRecognizer *guesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dummyViewTapped)];
        guesture.numberOfTapsRequired = 3;
        [dummyView addGestureRecognizer:guesture];
        
        [self.view addSubview:dummyView];
    }
    
}

- (void) viewWillAppear : (BOOL) animated {
    [super viewWillAppear:animated];
    //notification for keyboard
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appplicationDidBecomActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appplicationWillResignActive:)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    
   // [self performSelector:@selector(loadingSpinView) withObject:nil afterDelay:0.2];
    
}



- (void) viewWillDisappear : (BOOL) animated {
    [super viewWillDisappear:animated];
    //removing all the registered observers
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (![self isPinSetupJourney]) {
        [self animatePinRoadblockScreenItems];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark- version check super class method
- (void)finishingAppVersionCheckProcess{
    if([self currentState] != StateRoadblockTouchId || [self currentState] != StateRoadblockFaceId)
    {
        [[self pinEntryTextField] becomeFirstResponder];
    }
    [self refreshViewForCurrentState];
    
}


#pragma mark - Application active status notification
- (void)appplicationDidBecomActive: (NSNotification *)notification {
    self.separatorViewForKeyboard.hidden = NO;
    if(!isAuthenticatedUsingTouchID && self.currentState != StateRoadblockOk && self.currentState != StateSetupConfirmPinOk && !touchIDCancelledBySystem) {
        [[self pinEntryTextField] becomeFirstResponder];
        DDLogInfo(@"Pin: Pin is now first responder");
    }
}

- (void)appplicationWillResignActive: (NSNotification *)notification {
    self.separatorViewForKeyboard.hidden = YES;
    [[self pinEntryTextField] resignFirstResponder];
    
}

- (void)applicationWillEnterForeground: (NSNotification *)notification {
    if(touchIDCancelledBySystem)
        self.currentState = StateRoadblockTouchId;
}

#pragma mark Dummy TestViewController method
//(Salman)//Dummy
- (void)dummyViewTapped{
    
    UIViewController *playgoundViewController = [[PlaygroundViewController alloc] initWithNibName:@"PlaygroundViewController" bundle:nil];
    
    UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:playgoundViewController];
    
    [self presentViewController:navC animated:YES completion:nil];
}



#pragma mark - animation handler

- (void)animatePinRoadblockScreenItems {
    DDLogInfo(@"Pin: Animating the PINScreen items after the view got loaded");
    [self.view layoutIfNeeded];
    if (!_isAnimationDone) {
        [UIView animateWithDuration:0.8 delay:0.0 options:0 animations:^{
            
            self.InputPinContainerView.hidden = NO;
            [self.pinDotsContainerView setHidden:NO];
            
            _isTextFieldEnabled = YES;
            
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
            _isAnimationDone = YES;
            
            //Added by (RM)
            //Show keyboard when touch id is disabled.
            if([self currentState] != StateRoadblockTouchId || [self currentState] != StateRoadblockFaceId)
            {
                [[self pinEntryTextField] becomeFirstResponder];
            }
        }];
    }
}

#pragma mark - button action methods

- (IBAction)enterPasswordAction:(id)sender {
    
    if (![self isPinSetupJourney]) {
        [self trackOmnitureClickEvent];
        [self.pinControllerDelegate userDecidedToEnterPasswordInsteadOfPinOnPinViewController:self];
    } else {
        [self setCurrentState:StateSetupInit];
    }
}

#pragma mark Omniture methods

- (void)trackOmnitureClickEvent
{
    NSString *pageName = OMNIPAGE_LOGIN;
    NSString *linkTitle = OMNICLICK_LOGIN_RETURN_TO_LOGIN;
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setValue:@"Not Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:data];
}

- (void)trackOmnitureEventForPage:(NSString *)page
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = page;
    [data setValue:@"Not Logged In" forKey:kOmniLoginStatus];
    [OmnitureManager trackPage:pageName withContextInfo:data];
}


- (void)trackOmnitureKeyTask
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = OMNIPAGE_RECREATEPIN;
    [data setValue:@"Not Logged In" forKey:kOmniLoginStatus];
    [data setValue:OMNICLICK_LOGIN_PIN_CREATED forKey:kOmniKeyTask];
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackOmnitureError:(NSString *)error
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    NSString *timestamp = [AppManager NSStringWithOmnitureFormatFromNSDate:[NSDate date]];
    
    [data setValue:@"Not Logged In" forKey:@"LoginStatus"];
    [data setValue:timestamp forKey:@"TimeStamp"];
    
    [OmnitureManager trackError:error onPageWithName:OMNIPAGE_LOGINPIN contextInfo:data];
}

- (void)trackContentViewedForPage:(NSString *)page andNotificationPopupDetails:(NSString *)notificationPopup
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = page;
    [data setValue:@"Not Logged In" forKey:kOmniLoginStatus];
    
    NSString *notificaiton = [NSString stringWithFormat:@"%@%@",STATE_PREFIX,notificationPopup];
    [data setValue:notificaiton forKey:kOmniContentViewed];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
    
}

#pragma mark PINState Methods

//Checking all the possible states for the PIN creation and Unlock with all the positive and negative states
- (void) refreshViewForCurrentState {
    CDUser *user = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    
    // Setup - initial stage.
    if ([self currentState] == StateSetupInit) {
        DDLogInfo(@"Pin: We're in the init state");
        [self setPinEntryTextFieldIsFrozen:false];
        
        
        //Omniture
        [self trackOmnitureEventForPage:OMNIPAGE_CREATEPIN];
        
        [self backgroundColourPinDotContainersWithColour:[UIColor whiteColor]];
        [self colourPinDotContainersWithColour:[UIColor grayColor]];
        [[self promptLabel] setHidden:false];
        [[self promptLabel] setText:@"Welcome to BT Business"];
        [[self titleLabel] setText:[NSString stringWithFormat:@"Hello %@", [_pinCreationInfoDic[@"userDetails"][@"user"] firstName]]];
        [self.enterPasswordButton setHidden:YES];
        [self.pinMessageLabel setHidden:NO];
        [self.pinErrorMessageView setHidden:YES];
        [self.pinMessageLabel setText:@"You've logged in successfully, now choose a PIN. You'll be asked for your PIN whenever you open the app. It saves you having to remember your username."];
        
        _pinMessageLabelTopConstraint.constant = 22;
        [self updateViewConstraints];
        
        [[self animationImageView] setHidden:true];
        [self hideForgottenIdActionButtons];
        
        [[self pinEntryTextField] setText:@""];
        [[self pinEntryTextField] sendActionsForControlEvents:UIControlEventEditingChanged];
        [[self pinEntryTextField] becomeFirstResponder];
    }
    
    // Setup - confirm PIN entered in first stage.
    else if ([self currentState] == StateSetupConfirmPin) {
        [self setPinEntryTextFieldIsFrozen:false];
        [self backgroundColourPinDotContainersWithColour:[UIColor whiteColor]];
        [self colourPinDotContainersWithColour:[UIColor grayColor]];
        
        //Omniture
        [self trackOmnitureEventForPage:OMNIPAGE_RECREATEPIN];
        
        [[self promptLabel] setText:@"Welcome to BT Business"];
        [[self titleLabel] setText:[NSString stringWithFormat:@"Hello %@", [_pinCreationInfoDic[@"userDetails"][@"user"] firstName]]];
        [self.pinMessageLabel setText:@"Please confirm your PIN"];
        
        [self.pinMessageLabel setHidden:NO];
        [self.pinErrorMessageView setHidden:YES];
        
        _enterPasswordButtonTopConstraint.constant = 45;
        _pinMessageLabelTopConstraint.constant = 4;
        [self updateViewConstraints];
        
        [self.enterPasswordButton setHidden:NO];
        [self.enterPasswordButton setTitle:@"Back" forState:UIControlStateNormal];
        
        [[self animationImageView] setHidden:true];
        // Accessibility.
        UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, [self promptLabel]);
    }
    
    // Setup - temporary state to show the user that they entered their PIN incorrectly.
    else if ([self currentState] == StateSetupConfirmPinFail) {
        [self setPinEntryTextFieldIsFrozen:true];
        
        [[self animationImageView] setHidden:true];
        [self hideForgottenIdActionButtons];
        
        [self trackOmnitureError:OMNIERROR_REGISTRATION_INCORRECT_PIN];
        
        [self.pinMessageLabel setHidden:YES];
       // [self.pinMessageLabel setText:kPinIncorrectErrorMessage];
       // [self.pinMessageLabel setTextColor:[UIColor redColor]];
        [self.pinErrorMessageView setHidden:NO];
        
        [self colourPinDotContainersWithColour:[BrandColours colourMyBtRed]];
        [self colourPinPlaceholderDotsWithColour:[UIColor blackColor]];
        
        [[self pinDotView01] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{}];
        [[self pinDotView02] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{}];
        [[self pinDotView03] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{}];
        [[self pinDotView04] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, kStateSwitchDelay * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                
                [self setCurrentState:StateSetupInit];
                [self setTmpPinForConfirmation:@""];
            });
        }];
    }
    
    // Setup - user failed to enter the same PIN again.
    // They'll need to stay here until they do this.
    else if ([self currentState] == StateSetupConfirmPinFailTryAgain) {
        [self setPinEntryTextFieldIsFrozen:false];
        
        [[self animationImageView] setHidden:true];
        [self hideForgottenIdActionButtons];
        
        [self colourPinDotContainersWithColour:[BrandColours colourMyBtMediumGrey]];
        
        [[self pinEntryTextField] setText:@""];
        [[self pinEntryTextField] sendActionsForControlEvents:UIControlEventEditingChanged];
    }
    
    // Setup - all done!
    else if ([self currentState] == StateSetupConfirmPinOk) {
        
        //Remove UserName from Appmanager
        [AppManager sharedAppManager].usernameForSignInIntercepter = nil;
        
        //Remove All Intercepter related changes if exist
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kPasswordLoginIntercepter];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kTokenLoginIntercepter];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kSMSessionLoginIntercepter];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kGroupKeyLoginIntercepter];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        [self setPinEntryTextFieldIsFrozen:true];
        [[self pinEntryTextField] endEditing:true];
        
        [[self promptLabel] setHidden:false];
        [[self promptLabel] setText:@"PIN confirmed"];
        
        [self hideForgottenIdActionButtons];
        
        [self colourPinPlaceholderDotsWithColour:[UIColor blackColor]];
        [self colourPinDotContainersWithColour:[BrandColours colourMyBtGreen]];
        
        [[self animationImageView] setHidden:false];
        [[self animationImageView] startAnimating];
        
        // Accessibility.
        UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, [self promptLabel]);
        
        // Debug.
        DDLogInfo(@"Pin: Setup complete!");
        
        [self trackOmnitureKeyTask];
        
        // When Pin setup journey is completed, ask user to enable face or touch ID validation for future user to unlock the app instead of entering pin.        
        // Face ID or Touch ID?
        LAContext *context = [[LAContext alloc] init];
        NSError *localAuthError = nil;
        if (@available(iOS 11.0, *)) {
            DDLogInfo(@"Pin: iOS 11 available.");
            if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&localAuthError]) {
                if(context.biometryType == LABiometryTypeFaceID) {
                    DDLogInfo(@"Pin: Face ID auth process");
                    [self startFaceIDEnableForAuthenticationProcess];
                } else {
                    DDLogInfo(@"Pin: Touch ID auth process");
                    [self startTouchIDEnableForAuthenticationProcess];
                }
            } else {
                DDLogInfo(@"Pin: Touch ID auth process - edge case");
                [self startTouchIDEnableForAuthenticationProcess];
            }
        } else {
            DDLogInfo(@"Pin: Touch ID auth process (pre-iOS11");
            [self startTouchIDEnableForAuthenticationProcess];
        }
    }
    
    // Roadblock - default screen. PIN Unlock Screen
    else if ([self currentState] == StateRoadblockDefault) {
        [self setPinEntryTextFieldIsFrozen:false];
        
        [self.animationImageView setHidden:YES];
        [[self promptLabel] setHidden:false];
        [[self promptLabel] setText:@"Sign in"];
        [[self titleLabel] setText:[NSString stringWithFormat:@"Hello %@", user.firstName]];
        
        //Omniture
        [self trackOmnitureEventForPage:OMNIPAGE_LOGINPIN];
        
        [self.pinMessageLabel setHidden:true];
         [self.pinErrorMessageView setHidden:YES];
        [self.enterPasswordButton setHidden:false];
        
        [[self animationImageView] setHidden:true];
        //        [[self forgottenPinButton] setHidden:true];
        //        [[self forgottenPinDifferentUserButton] setHidden:false];
        [[self enterPasswordButton] setHidden:false];
        
        [self colourPinDotContainersWithColour:[UIColor grayColor]];//[BrandColours colourMyBtMediumGrey]
        [self backgroundColourPinDotContainersWithColour:[UIColor whiteColor]];
        
        [[self pinEntryTextField] setText:@""];
        [[self pinEntryTextField] sendActionsForControlEvents:UIControlEventEditingChanged];
        //(VRK) commented because keyboard should come after the animation completes not before.
        //        [[self pinEntryTextField] becomeFirstResponder];
        
        // Accessibility.
        UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, @"Enter your PIN to unlock the app.");
    }
    
    // Roadblock - Touch ID screen. This is pretty much the same as the default screen,
    // but we don't give focus to the text field.
    else if ([self currentState] == StateRoadblockTouchId) {
        DDLogInfo(@"Pin: Putting up the Touch ID roadblock");
        
        [self setPinEntryTextFieldIsFrozen:false];
        
        [[self promptLabel] setHidden:false];
        [[self promptLabel] setText:@"Sign in"];
         [[self titleLabel] setText:[NSString stringWithFormat:@"Hello %@", user.firstName]];
        
        [[self animationImageView] setHidden:true];
        [self.pinMessageLabel setHidden:true];
        [self.pinErrorMessageView setHidden:YES];
        [[self enterPasswordButton] setHidden:false];
        
        [self colourPinDotContainersWithColour:[UIColor grayColor]];
        [self backgroundColourPinDotContainersWithColour:[UIColor whiteColor]];
        
        [[self pinEntryTextField] setText:@""];
        [[self pinEntryTextField] sendActionsForControlEvents:UIControlEventEditingChanged];
        //[[self pinEntryTextField] becomeFirstResponder];
        
        LAContext* localAuthContext = [[LAContext alloc] init];
        [localAuthContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                         localizedReason:@"Use your fingerprint to enter"
                                   reply:^(BOOL success, NSError* error) {
                                       // Successful response - add PIN.
                                       NSLog(@"Error %@", error.description);
                                       if (success) {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               isAuthenticatedUsingTouchID = YES;
                                               touchIDCancelledBySystem = NO;
                                               [[self pinEntryTextField] setText:[self.unlockViewModel pinForCurrentlyLoggedInUser]];
                                               [[self pinEntryTextField] sendActionsForControlEvents:UIControlEventEditingChanged];
                                           });
                                       }
                                       
                                       // Verification failed or Authentication cancelled.
                                       else if (error.code == LAErrorUserCancel || error.code == LAErrorAuthenticationFailed || error.code == LAErrorUserFallback || error.code == LAErrorTouchIDLockout) {
                                           DDLogError(@"Pin: Authentication cancelled by user.");
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               isAuthenticatedUsingTouchID = NO;
                                               touchIDCancelledBySystem = NO;
                                               _isTextFieldEnabled = YES;
                                               self.separatorViewForKeyboard.hidden = NO;
                                               _isTouchIdCancelled = YES;
                                               
                                               [self trackContentViewedForPage:OMNIPAGE_RECREATEPIN andNotificationPopupDetails:OMNIPAGE_TOUCH_ID_CANCEL];
                                               
                                               // [self performSelector:@selector(openKeyBoard) withObject:nil afterDelay:0.01]; // delay was creating random issue like grid line and keyboard disapper.
                                           });
                                       }
                                       else if(error.code == LAErrorSystemCancel)
                                       {
                                           isAuthenticatedUsingTouchID = NO;
                                           _isTextFieldEnabled = YES;
                                           self.separatorViewForKeyboard.hidden = NO;
                                           _isTouchIdCancelled = YES;
                                           dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, 2.0 * NSEC_PER_SEC);
                                           dispatch_after(delayTime, dispatch_get_main_queue(), ^(void) {
                                               touchIDCancelledBySystem = YES;
                                           });
                                       }
                                       // Verification failed.
                                       else {
                                           DDLogError(@"Pin: TouchIDverification failed generic, and calling StateRoadblockFailTryAgainTouchId");
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               [self setCurrentState:StateRoadblockFailTryAgainTouchId];
                                           });
                                       }
                                   }];
        
        
    }
    
    // Roadblock - user needs to use their face to log in
    else if ([self currentState] == StateRoadblockFaceId) {
        DDLogInfo(@"Pin: Putting up the Face ID roadblock");
        [self setPinEntryTextFieldIsFrozen:false];
        
        [[self promptLabel] setHidden:false];
        [[self promptLabel] setText:@"Sign in"];
        [[self titleLabel] setText:[NSString stringWithFormat:@"Hello %@", user.firstName]];
        
        [[self animationImageView] setHidden:true];
        [self.pinMessageLabel setHidden:true];
        [self.pinErrorMessageView setHidden:YES];
        [[self enterPasswordButton] setHidden:false];
        
        [self colourPinDotContainersWithColour:[UIColor grayColor]];
        [self backgroundColourPinDotContainersWithColour:[UIColor whiteColor]];
        
        [[self pinEntryTextField] setText:@""];
        [[self pinEntryTextField] sendActionsForControlEvents:UIControlEventEditingChanged];
        
        //TODO - LA code
        LAContext* localAuthContext = [[LAContext alloc] init];
        [localAuthContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                         localizedReason:@"Use Face ID to unlock the app."
                                   reply:^(BOOL success, NSError* error) {
                                       DDLogInfo(@"Pin: Face check successful");
                                       // Successful response - add PIN.
                                       NSLog(@"Error %@", error.description);
                                       if (success) {
                                           DDLogInfo(@"Pin: Face success, adding pin");
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               isAuthenticatedUsingTouchID = YES;
                                               touchIDCancelledBySystem = NO;
                                               [[self pinEntryTextField] setText:[self.unlockViewModel pinForCurrentlyLoggedInUser]];
                                               [[self pinEntryTextField] sendActionsForControlEvents:UIControlEventEditingChanged];
                                           });
                                       }
                                       
                                       // Verification failed or Authentication cancelled.
                                       else if (error.code == LAErrorUserCancel || error.code == LAErrorAuthenticationFailed || error.code == LAErrorUserFallback || error.code == LAErrorTouchIDLockout) {
                                           DDLogInfo(@"Pin: Face authentication cancelled by user.");
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               isAuthenticatedUsingTouchID = NO;
                                               touchIDCancelledBySystem = NO;
                                               _isTextFieldEnabled = YES;
                                               self.separatorViewForKeyboard.hidden = NO;
                                               _isTouchIdCancelled = YES;
                                               
                                               [self trackContentViewedForPage:OMNIPAGE_RECREATEPIN andNotificationPopupDetails:OMNIPAGE_TOUCH_ID_CANCEL];
                                               
                                               // [self performSelector:@selector(openKeyBoard) withObject:nil afterDelay:0.01]; // delay was creating random issue like grid line and keyboard disapper.
                                           });
                                       }
                                       else if(error.code == LAErrorSystemCancel)
                                       {
                                           DDLogInfo(@"Face system cancel");
                                           isAuthenticatedUsingTouchID = NO;
                                           _isTextFieldEnabled = YES;
                                           self.separatorViewForKeyboard.hidden = NO;
                                           _isTouchIdCancelled = YES;
                                           dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, 2.0 * NSEC_PER_SEC);
                                           dispatch_after(delayTime, dispatch_get_main_queue(), ^(void) {
                                               touchIDCancelledBySystem = YES;
                                           });
                                       }
                                       // Verification failed.
                                       else {
                                           DDLogInfo(@"Pin: FaceIDverification failed generic, and calling StateRoadblockFailTryAgainFaceId");
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               [self setCurrentState:StateRoadblockFailTryAgainTouchId];
                                           });
                                       }
                                   }];
    }
    
    // Roadblock - temporary state to show the user they entered their PIN incorrectly.
    else if ([self currentState] == StateRoadblockFail) {
        
        [self setPinEntryTextFieldIsFrozen:true];
        [self trackOmnitureError:OMNIERROR_LOGIN_INVALID_PIN];
        [[self promptLabel] setHidden:false];
        [[self promptLabel] setText:@"Sign in"];
        [[self titleLabel] setText:[NSString stringWithFormat:@"Hello %@", user.firstName]];
        
        [[self animationImageView] setHidden:true];
        [[self enterPasswordButton] setHidden:true];
        [self.pinMessageLabel setHidden:YES];
        [self.pinErrorMessageView setHidden:NO];
        //[self.pinMessageLabel setText:kPinIncorrectErrorMessage];
        //[self.pinMessageLabel setTextColor:[UIColor redColor]];
        
        // Uncomment the below line to announce the error message on entering incorrect pin with accessibility
        //UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, kPinIncorrectErrorMessage);
        
        
        [self colourPinDotContainersWithColour:[BrandColours colourMyBtRed]];
        [self colourPinPlaceholderDotsWithColour:[UIColor blackColor]];
        
        [[self pinDotView01] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{}];
        [[self pinDotView02] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{}];
        [[self pinDotView03] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{}];
        [[self pinDotView04] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, kStateSwitchDelay * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                [self setCurrentState:StateRoadblockFailTryAgain];
            });
        }];
    }
    
    // Roadblock - user got their PIN wrong.
    // They'll need to try again. (Number of attempts is handled outside this function)
    else if ([self currentState] == StateRoadblockFailTryAgain) {
        
        [self setPinEntryTextFieldIsFrozen:false];
        
        [[self promptLabel] setHidden:false];
        [[self promptLabel] setText:@"Sign in"];
        [[self titleLabel] setText:@"Enter PIN"];
        
        [[self animationImageView] setHidden:true];
       /* [self.pinMessageLabel setTextColor:[UIColor lightGrayColor]];
        [self.pinMessageLabel setHidden:true];
        [self.pinErrorMessageView setHidden:YES];
        [[self enterPasswordButton] setHidden:false];*/
        
        [self colourPinDotContainersWithColour:[BrandColours colourMyBtMediumGrey]];
        
        [[self pinEntryTextField] setText:@""];
        [[self pinEntryTextField] sendActionsForControlEvents:UIControlEventEditingChanged];
        
    }
    
    // Roadblock - Touch ID fail.
    // The user will need to enter their PIN. (Number of attempts is handled outside this function)
    else if ([self currentState] == StateRoadblockFailTryAgainTouchId) {
        [self setPinEntryTextFieldIsFrozen:false];
        
        [[self promptLabel] setHidden:false];
        [[self promptLabel] setText:@"Sign in"];
        [[self titleLabel] setText:@"Enter PIN"];
        
        [[self animationImageView] setHidden:true];
        [[self enterPasswordButton] setHidden:false];
        
        [self.pinMessageLabel setTextColor:[UIColor lightGrayColor]];
        [self.pinMessageLabel setHidden:true];
        [self.pinErrorMessageView setHidden:YES];
        _pinMessageLabelTopConstraint.constant = 22;
        
        [self colourPinDotContainersWithColour:[BrandColours colourMyBtMediumGrey]];
        
        [[self pinEntryTextField] setText:@""];
        [[self pinEntryTextField] sendActionsForControlEvents:UIControlEventEditingChanged];
        [[self pinEntryTextField] becomeFirstResponder];
        
    }
    
    // Roadblock - temporary state that kicks users back to the sign in screen.
    else if ([self currentState] == StateRoadblockFailKickToAuth) {
        
        [self trackOmnitureError:OMNIERROR_LOGIN_INVALID_PIN_3_TIMES];
        
        [self trackContentViewedForPage:OMNIPAGE_LOGINPIN andNotificationPopupDetails:OMNIPAGE_PIN_INCORRECT_OK];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Incorrect PIN" message:@" You have entered an incorrect PIN 3 times. You will be redirected to log in with your email and password." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self setPinEntryTextFieldIsFrozen:true];
            [[self view] endEditing:true];
            [[self animationImageView] setHidden:true];
            [[self enterPasswordButton] setHidden:true];
            [self.pinMessageLabel setHidden:YES];
           // [self.pinMessageLabel setTextColor:[UIColor redColor]];
            //[self.pinMessageLabel setText:kPinIncorrectErrorMessage];
            [self.pinErrorMessageView setHidden:NO];
            
            [self colourPinDotContainersWithColour:[BrandColours colourMyBtRed]];
            [self colourPinPlaceholderDotsWithColour:[UIColor blackColor]];
            
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, kKickToAuthDelayInterval * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                
                [self.pinControllerDelegate userEnteredIncorrectPINThreeTimesOnPinViewController:self];
            });
            
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    
    // Roadblock - all good. PIN entered successfully.
    else if ([self currentState] == StateRoadblockOk) {
        [self setPinEntryTextFieldIsFrozen:true];
        [[self pinEntryTextField] endEditing:true];
        [[self promptLabel] setHidden:false];
        [[self promptLabel] setText:@"PIN confirmed"];
        [[self enterPasswordButton] setHidden:true];
        
        [self colourPinPlaceholderDotsWithColour:[UIColor blackColor]];
        [self colourPinDotContainersWithColour:[BrandColours colourMyBtGreen]];
        
        [[self animationImageView] setHidden:false];
        [[self animationImageView] startAnimating];
        
        // Debug.
        DDLogInfo(@"Note. Roadblock cleared. Dismissing and performing initialisation if required.");
        
        // Set global flag.
        //        [[AppDelegate sharedInstance
        // (hd) Commenting this as we are phasing this method out step by sep] setAppIsLocked:false];
        
        // Accessibility.
        UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, [self promptLabel]);
        
        // The PIN roadblock can have one of two possible states.
        //
        // It can be the root view controller - where a previous user has just launched the app, for example.
        // In this case, it will need to perform any initialisation routines such as fetching the customer's profile.
        //
        // Alternatively, it can simply be a roadblock that is presented when the app is locked. In this case, it just
        // needs to dismiss itself on a successful PIN entry.
        
        
        if ([self initRequired]) {
            if([AppManager isInternetConnectionAvailable]) {
                self.initRequired = NO;
                [self setNetworkRequestInProgress:true];
                [self.unlockViewModel performLoginForCurrentlyLoggedInUser];
            } else {
                [self showGenericAlertWithTitle:kNoConnectionTitle andMessage:kNoConnectionMessage];
                [AppManager trackNoInternetErrorBeforeLoginOnPage:OMNIPAGE_LOGINPIN];
            }
        }
    }
}

- (void) verifyPinAndUpdateStateForPin : (NSString*) pin {
    // If we're in the setup process, move to the confirm stage.
    // We introduce a slight delay here so the user has time to see their final PIN
    // digit appearing.
    if ([self currentState] == StateSetupInit) {
        [self setTmpPinForConfirmation:[[self pinEntryTextField] text]];
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, kStateSwitchDelay * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            [[self pinEntryTextField] setText:@""];
            [[self pinEntryTextField] sendActionsForControlEvents:UIControlEventEditingChanged];
            
            [self setCurrentState:StateSetupConfirmPin];
            
            //[OmnitureManager trackPage:OMNIPAGE_CONFIRM_PIN];
        });
    }
    
    // Confirmation.
    // This affects the setup journey and a normal roadblock.
    else if ([self currentState] == StateSetupConfirmPin ||
             [self currentState] == StateSetupConfirmPinFailTryAgain ||
             [self currentState] == StateRoadblockDefault ||
             [self currentState] == StateRoadblockFailTryAgain ||
             [self currentState] == StateRoadblockFailTryAgainTouchId ||
             [self currentState] == StateRoadblockTouchId ||
             [self currentState] == StateRoadblockFaceId) {
        
        BOOL pinConfirmedOk = false;
        
        if ([self tmpPinForConfirmation] && [[self tmpPinForConfirmation] compare:[[self pinEntryTextField] text]] == NSOrderedSame) {
            // Reset counter.
            [self setNumPinEntryAttempts:0];
            
            // Set flag.
            pinConfirmedOk = true;
        }
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, kStateSwitchDelay * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            if (pinConfirmedOk) {
                DDLogInfo(@"Pin: Pin confirmed okay and is being saved.");
                // PIN ok and we're in the setup journey.
                if ([self currentState] == StateSetupConfirmPin || [self currentState] == StateSetupConfirmPinFailTryAgain) {
                    [self setPIN:[[self pinEntryTextField] text]];
                    [self setCurrentState:StateSetupConfirmPinOk];
                    DDLogInfo(@"Pin: Pin saved.");
                }
                
                // PIN ok and we're on a roadblock.
                else {
                    [self setCurrentState:StateRoadblockOk];
                }
            }
            
            else {
                // PIN failed and we're in the setup journey.
                if ([self currentState] == StateSetupConfirmPin || [self currentState] == StateSetupConfirmPinFailTryAgain) {
                    //[OmnitureManager trackError:OMNIERROR_PIN_MISMATCH onPageWithName:OMNIPAGE_SETUP_PIN];
                    
                    [self setCurrentState:StateSetupConfirmPinFail];
                }
                
                // PIN failed and we're on a roadblock.
                else {
                    //[OmnitureManager trackError:OMNIERROR_PIN_INCORRECT onPageWithName:OMNIPAGE_PIN_LOGIN];
                    
                    [self setNumPinEntryAttempts:[self numPinEntryAttempts] + 1];
                    
                    if ([self numPinEntryAttempts] < kMaxNumPinEntryAttempts) {
                        [self setCurrentState:StateRoadblockFail];
                    }
                    
                    else {
                        [self setCurrentState:StateRoadblockFailKickToAuth];
                    }
                }
            }
        });
    }
}

- (void)pinSetupJourneyCompleted
{
    //(VRK) sending pin & user Details to Signin page
    NSMutableDictionary *pinInfoDict = [[NSMutableDictionary alloc] init];
    [pinInfoDict setObject:self.PIN forKey:@"pin"];
    [pinInfoDict setObject:[self.pinCreationInfoDic objectForKey:@"username"] forKey:@"username"];
    [pinInfoDict setObject:[self.pinCreationInfoDic objectForKey:@"password"] forKey:@"password"];
    [pinInfoDict setObject:[self.pinCreationInfoDic objectForKey:@"token"] forKey:@"token"];
    [pinInfoDict setObject:[self.pinCreationInfoDic objectForKey:@"smsession"] forKey:@"smsession"];
    [pinInfoDict setObject:[self.pinCreationInfoDic objectForKey:@"userDetails"] forKey:@"userDetails"];
    
    if ([self.pinControllerDelegate respondsToSelector:@selector(pinViewController:userHasSuccesfullySetupThePINWithInfo:)]) {
        [self.pinControllerDelegate pinViewController:self userHasSuccesfullySetupThePINWithInfo:pinInfoDict];
    }
    
}

#pragma ColorRelated Methods
//change the color of PIN dots
- (void) colourPinPlaceholderDotsWithColour : (UIColor*) colour {
    for (UIView* tmpDot in [self pinDots]) {
        [tmpDot setBackgroundColor:colour];
    }
}

//Change the color of the PIN containers
- (void) colourPinDotContainersWithColour : (UIColor*) colour {
    for (UIView* tmpDot in [self pinDotContainers]) {
        [[tmpDot layer] setBorderWidth:1.0];
        [[tmpDot layer] setBorderColor:[colour CGColor]];
    }
}

//change the background color and assign border width to the PIN dots
- (void) backgroundColourPinDotContainersWithColour : (UIColor*) colour {
    for (UIView* tmpDot in [self pinDotContainers]) {
        [[tmpDot layer] setBorderWidth:1.0];
        [tmpDot setBackgroundColor:colour];
    }
}


#pragma mark PrivateMethods

- (void)openKeyBoard
{
    [[self pinEntryTextField] becomeFirstResponder];
}

- (void) hideForgottenIdActionButtons {
    DDLogInfo(@"INFO: Hiding the returnto login button");
    if ([self enterPasswordButton]) {
        [[self enterPasswordButton] setHidden:YES];
    }
}

#pragma mark - Touch ID enable methods

- (void)checkIfUserCanUnlockWithTouchID
{
    [self.unlockViewModel checkDeviceSupportsFingerprintScanningStatus];
    
    if ( self.unlockViewModel.currentStatusOfFingerprintAuthenticationPolicy == DLMPinUnlockTouchIDStateTouchIDEnabled ) {
        
        if ( ([[AppDelegate sharedInstance].viewModel.app.isTouchIDEnabledForUnlock boolValue]) && ([[self.unlockViewModel pinForCurrentlyLoggedInUser] length] > 0)) {
            
            DDLogInfo(@"Pin: Invoking Touch ID authentication.");
            self.separatorViewForKeyboard.hidden = YES;
            // (lp) Change current state to touch id roadbloack
            [self setCurrentState:StateRoadblockTouchId];
            
        } else {
            _isTextFieldEnabled = YES;
            // [[self pinEntryTextField] becomeFirstResponder];
            
            DDLogInfo(@"Pin: Can't invoke Touch ID authentication for login.");
        }
        
    } else {
        //        _isTextFieldEnabled = YES;
        //        [[self pinEntryTextField] becomeFirstResponder];
        
        DDLogInfo(@"Note. Not invoking Touch ID authentication - device incompatible or not enabled by user.");
    }
}

-(void)checkIfUserCanUnlockWithFaceID
{
    DDLogInfo(@"Pin: Can the user log in with Face ID?");
    
    [self.unlockViewModel checkDeviceSupportsFaceScanningStatus];
    
    if ( self.unlockViewModel.currentStatusOfFaceAuthenticationPolicy == DLMPinCreationFaceIDStateFaceIDEnabled) {
        DDLogInfo(@"Pin: Face ID is enabled.");
        if(([[AppDelegate sharedInstance].viewModel.app.isFaceIDEnabledForUnlock boolValue]) && ([[self.unlockViewModel pinForCurrentlyLoggedInUser] length] > 0)) {
            DDLogInfo(@"Pin: Yes, Face ID is enabled. Setting Face ID roadblock.");
            self.separatorViewForKeyboard.hidden = YES;
            _isTextFieldEnabled = NO;
            [self setCurrentState:StateRoadblockFaceId];
        } else {
            DDLogInfo(@"Pin: Cannot invoke Face ID.");
            _isTextFieldEnabled = YES;
        }
    }
}

/* Ask the user to enable touch ID to unlock app instead entering pin in future. */
- (void)startTouchIDEnableForAuthenticationProcess
{
    // (lp) Checking current status of device related to touch id and device supports touch id or not.
    [self.creationViewModel checkDeviceSupportsFingerprintScanningStatus];
    
    // (lp) If device supports touch id then we will ask to user to enable touch id for unlock the app.
    if ( (self.creationViewModel.currentStatusOfFingerprintAuthenticationPolicy == DLMPinCreationTouchIDStateTouchIDEnabled) /*|| (self.creationViewModel.currentStatusOfFingerprintAuthenticationPolicy == DLMPinCreationTouchIDStateTouchIDNotEnrolled)*/ ) {
        
        DDLogInfo(@"Pin: User is asked to enable touch id for future use");
        [self showEnableTouchIDView];
        
    } else {
        
        // (lp) Device does not support touch id. Will proceed without enable touch id for unlock.
        DDLogInfo(@"Pin: Device does not support touch id.");
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, kKickToAuthDelayInterval * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            [self pinSetupJourneyCompleted];
        });
    }
}

/* Ask if the user if they'd like to use Face ID to unlock the app */
- (void)startFaceIDEnableForAuthenticationProcess
{
    [self.creationViewModel checkDeviceSupportsFaceIDStatus];
    
    if( (self.creationViewModel.currentStatusOfFaceIDAuthenticationPolicy == DLMPinCreationFaceIDStateFaceIDEnabled) ||
       (self.creationViewModel.currentStatusOfFaceIDAuthenticationPolicy ==
        DLMPinCreationFaceIDStateFaceIDNotEnrolled)) {
           [self showEnableFaceIDView];
       } else {
           DDLogInfo(@"Pin: Device does not support Face ID");
           dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, kKickToAuthDelayInterval * NSEC_PER_SEC);
           dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
               [self pinSetupJourneyCompleted];
           });
       }
}


- (void)showEnableTouchIDView {
    
    // (lp) overlay to display touch id related views. No need to make the same overlay again for touch id disable view and use the same.
    _overlayView = [[UIView alloc] init];
    _overlayView.translatesAutoresizingMaskIntoConstraints = NO;
    _overlayView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    
    // (lp) Display enable touch id view on overlay to user.
    _enableTouchIDView = [[[NSBundle mainBundle] loadNibNamed:@"BTEnableTouchIdView" owner:nil options:nil] objectAtIndex:0];
    _enableTouchIDView.enableTouchIdViewDelegate = self;
    _enableTouchIDView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:_overlayView];
    [self.view addSubview:_enableTouchIDView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_overlayView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_overlayView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_overlayView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_overlayView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_enableTouchIDView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:25.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_enableTouchIDView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-25.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_enableTouchIDView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
}

- (void)showEnableFaceIDView {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Face ID"
                                                                   message:@"Would you like to log in even faster, next time? Enable Face ID to log in with facial recognition."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* enableFaceID = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        [self trackContentViewedForPage:OMNIPAGE_RECREATEPIN andNotificationPopupDetails:OMNIPAGE_PIN_CONFIRMED_OK];

        [self.creationViewModel checkDeviceSupportsFaceIDStatus];

        if ( self.creationViewModel.currentStatusOfFaceIDAuthenticationPolicy == DLMPinCreationFaceIDStateFaceIDEnabled) {

            [self->_overlayView removeFromSuperview];

            // (lp) Enabling Face ID unlock in app. In future user can use their face for unlock the app instead entering a pin.
            [self.creationViewModel enableFaceIDForUnlock];
            [self pinSetupJourneyCompleted];

        } else if (self.creationViewModel.currentStatusOfFaceIDAuthenticationPolicy == DLMPinCreationFaceIDStateFaceIDNotEnrolled ) {
            DDLogInfo(@"Pin: User is asked to enrolled Face ID in device setting for future use");
            [self showFaceIDDisabledView];

        } else {
            [self->_overlayView removeFromSuperview];
            [self pinSetupJourneyCompleted];
        }
    }];
    
    UIAlertAction* cancelFaceID = [UIAlertAction actionWithTitle:@"No, thanks" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self trackContentViewedForPage:OMNIPAGE_RECREATEPIN andNotificationPopupDetails:OMNIPAGE_PIN_CONFIRMED_NO_THANKS];
        [self->_overlayView removeFromSuperview];
        [self pinSetupJourneyCompleted];
    }];
    
    [alert addAction:cancelFaceID];
    [alert addAction:enableFaceID];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showTouchIdDisabledView
{
    
    // (lp) Display touch id disable view on overlay to user.
    _touchIdDisabledView = [[[NSBundle mainBundle] loadNibNamed:@"BTTouchIdDisabledView" owner:nil options:nil] objectAtIndex:0];
    _touchIdDisabledView.touchIdDisabledViewDelegate = self;
    _touchIdDisabledView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:_touchIdDisabledView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_touchIdDisabledView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:25.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_touchIdDisabledView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-25.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_touchIdDisabledView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
    // (lp) Add observer to get notification when app comes from background.
    [self addObserverForDidBecomeActiveNotification];
}

- (void)showFaceIDDisabledView
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"FaceID"
                                                                   message:@"Face ID is currently disabled. Go to device settings to enable Face ID."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* goToSettings = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self trackContentViewedForPage:OMNIPAGE_RECREATEPIN andNotificationPopupDetails:OMNIPAGE_PIN_CONFIRMED_NO_THANKS];
        [_overlayView removeFromSuperview];
        [self pinSetupJourneyCompleted];
    }];
    
    [alert addAction:goToSettings];
    [alert addAction:cancel];
}

- (void)checkIfUserHasEnabledTouchIDInDeviceSettings
{
    [self.creationViewModel checkDeviceSupportsFingerprintScanningStatus];
    
    if ( self.creationViewModel.currentStatusOfFingerprintAuthenticationPolicy == DLMPinCreationTouchIDStateTouchIDEnabled) {
        
        [_overlayView removeFromSuperview];
        [_touchIdDisabledView removeFromSuperview];
        
        [self removeObserverForDidBecomeActiveNotification];
        
        // (lp) Enabling touch id unlock in app. In future user can use touch id/ fingerprint for unlock the app instead entering pin.
        [self.creationViewModel enableTouchIdForUnlock];
        
        [self pinSetupJourneyCompleted];
        
    }
    
}

- (void)checkIfUserHasEnabledFaceIDInDeviceSettings
{
    [self.creationViewModel checkDeviceSupportsFaceIDStatus];
    
    if ( self.creationViewModel.currentStatusOfFaceIDAuthenticationPolicy == DLMPinCreationFaceIDStateFaceIDEnabled) {
        
        [_overlayView removeFromSuperview];
        [_touchIdDisabledView removeFromSuperview];
        
        [self removeObserverForDidBecomeActiveNotification];
        
        // Enabling Face ID unlock in app. In future user can use their face for unlocking the app instead entering a pin.
        [self.creationViewModel enableFaceIDForUnlock];
        
        [self pinSetupJourneyCompleted];
    }
}

#pragma mark - BTEnableTouchIdView delegate methods

- (void)enableTouchIdView:(BTEnableTouchIdView *)enableTouchIdView userWantsToProceedEnableTouchIdWith:(BOOL)enable
{
    [_enableTouchIDView removeFromSuperview];
    
    
    if (enable) {
        
        [self trackContentViewedForPage:OMNIPAGE_RECREATEPIN andNotificationPopupDetails:OMNIPAGE_PIN_CONFIRMED_OK];
        
        [self.creationViewModel checkDeviceSupportsFingerprintScanningStatus];
        
        if ( self.creationViewModel.currentStatusOfFingerprintAuthenticationPolicy == DLMPinCreationTouchIDStateTouchIDEnabled) {
            
            [_overlayView removeFromSuperview];
            
            // (lp) Enabling touch id unlock in app. In future user can use touch id/ fingerprint for unlock the app instead entering pin.
            [self.creationViewModel enableTouchIdForUnlock];
            
            [self pinSetupJourneyCompleted];
            
        } else if (self.creationViewModel.currentStatusOfFingerprintAuthenticationPolicy == DLMPinCreationTouchIDStateTouchIDNotEnrolled ) {
            
            DDLogInfo(@"Pin: User is asked to enrolled touch id in device setting for future use");
            [self showTouchIdDisabledView];
            
        } else {
            
            [_overlayView removeFromSuperview];
            [self pinSetupJourneyCompleted];
        }
        
    } else {
        
        [self trackContentViewedForPage:OMNIPAGE_RECREATEPIN andNotificationPopupDetails:OMNIPAGE_PIN_CONFIRMED_NO_THANKS];
        [_overlayView removeFromSuperview];
        [self pinSetupJourneyCompleted];
    }
}

#pragma mark - BTTouchIdDisableView delegate methods

- (void)redirectToDeviceSettingsWithTouchIDDisbaledView:(BTTouchIdDisabledView *)touchIdDisabledView
{
    [self trackContentViewedForPage:OMNIPAGE_RECREATEPIN andNotificationPopupDetails:OMNIPAGE_TOUCH_ID_DISABLED_SETTINGS];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

- (void)redirectToDeviceSettingsWithFaceIDDisabledView
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

- (void)cancelledEnrollTouchIDWithTouchIdDisabledView:(BTTouchIdDisabledView *)touchIdDisabledView
{
    [_touchIdDisabledView removeFromSuperview];
    [_overlayView removeFromSuperview];
    
    [self trackContentViewedForPage:OMNIPAGE_RECREATEPIN andNotificationPopupDetails:OMNIPAGE_TOUCH_ID_DISABLED_CANCEL];
    
    [self pinSetupJourneyCompleted];
    
}

#pragma mark - display alert for with error message

- (void)displayAlertMessageWithTitle:(NSString *)title andMessage:(NSString *)message
{
    UIAlertController *logInErrorAlertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self setCurrentState:StateRoadblockDefault];
        [[self pinEntryTextField] becomeFirstResponder];
        [logInErrorAlertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [logInErrorAlertController addAction:okAction];
    
    [self presentViewController:logInErrorAlertController animated:YES completion:nil];
}

- (void)showGenericAlertWithTitle:(NSString*)title andMessage:(NSString*)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if(!_isTouchIdCancelled) {
            [self setCurrentState:StateRoadblockTouchId];
            [[self pinEntryTextField] resignFirstResponder];
            [[self separatorViewForKeyboard] setHidden:YES];
        } else {
            [self setCurrentState:StateRoadblockDefault];
            [[self pinEntryTextField] becomeFirstResponder];
        }
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Notification methods
/*
 (lp) App will provide option to go to device setting app. There user can enable touch id and set fingerprints for unlock in future. Our App will use this notification to check current status of device regarding touch id enabled or not when user comes back from background.
 */
- (void)addObserverForDidBecomeActiveNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkIfUserHasEnabledTouchIDInDeviceSettings)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)removeObserverForDidBecomeActiveNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
}

#pragma mark KeyboardNotification Methods
//this method called when the keyboard comes into picture
- (void)keyboardWillShow:(NSNotification *)notification
{
    [self.view layoutIfNeeded];
    
    self.separatorViewForKeyboard.hidden = NO;
    
    [UIView animateWithDuration:0.0 //[[notification.userInfo valueForKey:@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue]
                          delay:0.0
                        options:[[notification.userInfo valueForKey:@"UIKeyboardAnimationCurveUserInfoKey"] doubleValue]
                     animations:^{
                         
                         
                         if (IS_IPHONE_4_OR_LESS) {
                             CGRect keyboardEndFrame = [[notification.userInfo valueForKey:@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
//                             [self.pinScrollView setContentOffset:CGPointMake(0, 30)];
                         } else if (IS_IPHONE_5) {

                         }
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                     }];
}

//this method called when the keyboard Hides
- (void)keyboardWillHide:(NSNotification *)notification
{
    [self.view layoutIfNeeded];
    
    self.separatorViewForKeyboard.hidden = YES;
    
    [UIView animateWithDuration:0.0 //[[notification.userInfo valueForKey:@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue]
                          delay:0.0
                        options:[[notification.userInfo valueForKey:@"UIKeyboardAnimationCurveUserInfoKey"] doubleValue]
                     animations:^{
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                     }];
    
}

- (void) keyboardWillChange : (NSNotification*) notification {
    CGRect keyboardRect = [[notification userInfo][UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardHeight = [[self view] convertRect:keyboardRect fromView:nil].size.height;
}

#pragma mark - Text field delegate methods.

- (BOOL) textField : (UITextField*) textField shouldChangeCharactersInRange : (NSRange) range replacementString : (NSString*) string {
    if ([self pinEntryTextFieldIsFrozen]) {
        return false;
    } else {
        
        if(self.pinErrorMessageView.isHidden == NO){
            [self.pinMessageLabel setTextColor:[UIColor lightGrayColor]];
            [self.pinMessageLabel setHidden:true];
            [self.pinErrorMessageView setHidden:YES];
            [[self enterPasswordButton] setHidden:false];
        }
        
        NSUInteger pinLength = 4;
        
        NSUInteger oldLength = [[textField text] length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= pinLength || returnKey;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(!self.separatorViewForKeyboard)
        self.separatorViewForKeyboard = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 1.0 / [UIScreen mainScreen].scale)];
    
    self.separatorViewForKeyboard.frame =CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 1.0 / [UIScreen mainScreen].scale);
    self.separatorViewForKeyboard.backgroundColor = [UIColor lightGrayColor];
    textField.inputAccessoryView =  self.separatorViewForKeyboard;
    
    if([self currentState] == StateRoadblockTouchId && !_isTouchIdCancelled)
    {
        self.separatorViewForKeyboard.hidden  =YES;
    }
    else
    {
        self.separatorViewForKeyboard.hidden  =NO;
    }
    
    return _isTextFieldEnabled;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    self.separatorViewForKeyboard.hidden = YES;
    return YES;
}

#pragma mark - DLMPinUnlockScreenDelegate Methods

- (void)loggedInUserSuccessfullyAuthenticatedOnPinUnlockScreen:(DLMPinUnlockScreen *)screen
{
    self.initRequired = YES;
    DDLogInfo(@"INFO: userSuccesfullyEnteredTheCorrectPINAndUserAuthenticationDoneOnPinViewController");
    [self.pinControllerDelegate userSuccesfullyEnteredTheCorrectPINAndUserAuthenticationDoneOnPinViewController:self];
}

- (void)pinUnlockScreen:(DLMPinUnlockScreen *)screen loginAttemptFailedForLoggedInUserWithWebServiceError:(NLWebServiceError *)error
{
    self.initRequired = YES;
    [self setNetworkRequestInProgress:NO];
    
    [AppManager trackGenericAPIErrorBeforeLoginOnPage:OMNIPAGE_LOGINPIN];
    
    NSError *logInError = error.error;
    
    if([logInError.domain isEqualToString:BTNetworkErrorDomain])
    {
        switch (logInError.code)
        {
            case BTNetworkErrorCodeVordelUserUnauthenticated:
            {
                DDLogError(@"ERROR: When Login invalid from the code not from the API");
                NSDictionary *infoDic = [NSDictionary dictionaryWithObjectsAndKeys:@"Password changed", @"title", kAppLockDownToLoginScreenAlertMessagePasswordChanged, @"message", nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAppNeedsLockDownWithLoginScreen object:nil userInfo:infoDic];
                break;
            }
                
            case BTNetworkErrorCodeVordelAccountLocked:
            {
                DDLogError(@"ERROR: When UserProfileLocked for 20 minutes");
                
                [self displayAlertMessageWithTitle:@"Account Locked" andMessage:kProfileLockedMessage];
                
                break;
            }
                
            case BTNetworkErrorCodeVordelPlannedServiceOutage:
            {
                DDLogError(@"ERROR: The app is under maintanance.");
                
                [self displayAlertMessageWithTitle:@"Under Maintenance" andMessage:kAppLockDownToPinUnlockScreenAlertMessageUnderMaintenance];
                
                break;
            }
                
            case BTNetworkErrorCodeVordelUnplannedServiceOutage:
            {
                DDLogError(@"ERROR: The app is under maintanance.");
                
                [self displayAlertMessageWithTitle:@"Under Maintenance" andMessage:kAppLockDownToPinUnlockScreenAlertMessageUnderMaintenance];
                
                break;
            }
                
            default:
            {
                DDLogError(@"ERROR: Default error other than UserProfileLocked & Login Invalid");
                
                [self displayAlertMessageWithTitle:@"Message" andMessage:kDefaultErrorMessage];
                
                break;
            }
        }
    }
    else
    {
        DDLogError(@"ERROR: General API error");
        [self displayAlertMessageWithTitle:@"Message" andMessage:kDefaultErrorMessage];
    }
}

#pragma mark - Loading spinner view

- (void)createLoaderImageView
{
    _loaderView = [[CustomSpinnerAnimationView alloc] initWithText:@"" loadingImage:[UIImage imageNamed:@"Spinner"] andLoaderImageSize:20.0f];
    
    [self.spinnerView addSubview:_loaderView];
    
    [self.spinnerView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.spinnerView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [self.spinnerView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.spinnerView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    [_loaderView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:100]];
    [_loaderView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:155]];
    _loaderView.translatesAutoresizingMaskIntoConstraints = NO;
    
}


/*- (void) loadingSpinView{
    _loaderView.frame = CGRectMake(0, 0, _loaderView.frame.size.width, _loaderView.frame.size.height);
    _loaderView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.spinnerView addSubview:_loaderView];
}*/

- (void)showLoaderView
{
    [self createLoaderImageView];
    
    _loaderView.hidden = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_loaderView startAnimation];
    });
}

- (void)hideLoaderView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [_loaderView stopAnimation];
    });
    
    _loaderView.hidden = YES;
    
    [_loaderView removeFromSuperview];
    
    _loaderView = nil;
    
}

@end
