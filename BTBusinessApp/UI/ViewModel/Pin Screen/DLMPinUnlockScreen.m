//
//  DLMPinUnlockScreen.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 13/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMPinUnlockScreen.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "CDAuthenticationToken.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import "BTAuthenticationManager.h"
#import "NLWebServiceError.h"


@interface DLMPinUnlockScreen () {
    NSString *_username;
}

@end


@implementation DLMPinUnlockScreen

#pragma mark - Public Methods

- (void)performLoginForCurrentlyLoggedInUser
{
    _username = [AppDelegate sharedInstance].viewModel.app.loggedInUser.username;
    if(_username == nil)
    {
        DDLogError(@"Unexpected Case: Username of logged in user not found in persistence while trying to make login call.");
        return;
    }

    NSError *errorReadingPassword = nil;
    NSString *password = [SAMKeychain passwordForService:kKeyChainServiceBTUserPassword account:_username error:&errorReadingPassword];
    if(errorReadingPassword)
    {
        DDLogError(@"Failed to fetch password from keychain for username  '%@'", _username);
        return;
    }

    [self addObserverForAuthenticationCall];
    
    [[BTAuthenticationManager sharedManager] signInAuthenticateWithUsername:_username andPassword:password];
}

- (NSString *)pinForCurrentlyLoggedInUser
{
    NSString *username = [AppDelegate sharedInstance].viewModel.app.loggedInUser.username;

    NSError *errorReadingPIN = nil;
    NSString *pin = [SAMKeychain passwordForService:kKeyChainServiceBTUserPIN account:username error:&errorReadingPIN];
    if(errorReadingPIN)
    {
        DDLogError(@"Failed to fetch PIN from keychain for username  '%@'", username);
        return nil;
    }

    return pin;
}

- (void)checkDeviceSupportsFingerprintScanningStatus
{
    // (LP) Get the local authentication context.
    LAContext* localAuthContext = [[LAContext alloc] init];
    NSError* localAuthError = nil;

    // (LP) Check if the user has enabled the feature.
    if ([localAuthContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&localAuthError]) {

        _currentStatusOfFingerprintAuthenticationPolicy = DLMPinUnlockTouchIDStateTouchIDEnabled;
        DDLogInfo(@"Pin: Touch ID is supported on this device.");

    } else {

        // (LP) Check error code
        if ([localAuthError code]) {

            // (LP) The user hasn't registered any fingerprints.
            if ([localAuthError code] == LAErrorTouchIDNotEnrolled) {

                _currentStatusOfFingerprintAuthenticationPolicy = DLMPinUnlockTouchIDStateTouchIDNotEnrolled;
                DDLogInfo(@"Pin: Touch ID is supported but no fingerprints are enrolled.");

            }

            // (LP) Not supported? This shouldn't happen.
            else {

                _currentStatusOfFingerprintAuthenticationPolicy = DLMPinUnlockTouchIDStateTouchIDNotAvailable;
                DDLogInfo(@"Pin: Touch ID is not supported on this device. (%@)", localAuthError);

            }
        } else {

            _currentStatusOfFingerprintAuthenticationPolicy = DLMPinUnlockTouchIDStateTouchIDNotAvailable;
            DDLogInfo(@"Pin: Touch ID is not supported on this device. (%@)", localAuthError);

        }
    }
}

-(void)checkDeviceSupportsFaceScanningStatus {
    // (LP) Get the local authentication context.
    LAContext* localAuthContext = [[LAContext alloc] init];
    NSError* localAuthError = nil;
    
    if (@available(iOS 11.0, *)) {
        if ([localAuthContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&localAuthError]) {
            if(localAuthContext.biometryType == LABiometryTypeFaceID) {
                DDLogInfo(@"Pin: Face ID is supported on this device");
                _currentStatusOfFaceAuthenticationPolicy = DLMPinUnlockFaceIDStateFaceIDEnabled;
            } else {
                DDLogInfo(@"Pin: Face ID is not enrolled");
                _currentStatusOfFaceAuthenticationPolicy = DLMPinUnlockFaceIDStateFaceIDNotEnrolled;
            }
        }
    } else {
        DDLogInfo(@"Pin: Face ID is not available");
        _currentStatusOfFaceAuthenticationPolicy = DLMPinUnlockFaceIDStateFaceIDNotAvailable;
    }
}


#pragma mark - Private Helper Methods

- (void)addObserverForAuthenticationCall
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signInAuthenticationAPICallSuccessfullyFinishedNotification:) name:kSignInAuthenticationApiCallSuccessfullyFinishedNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signInAuthenticationAPICallFailedNotification:) name:kSignInAuthenticationApiCallFailedNotification object:nil];
}

- (void)removeObserverToNotificationForSignInAuthenticationCall
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSignInAuthenticationApiCallSuccessfullyFinishedNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSignInAuthenticationApiCallFailedNotification object:nil];
}

#pragma mark - Notification Methods

- (void)signInAuthenticationAPICallSuccessfullyFinishedNotification:(NSNotification *)notification
{
    if ([[notification.userInfo valueForKey:@"username"] isEqualToString:_username])
    {
        [self removeObserverToNotificationForSignInAuthenticationCall];
        
        BTAuthenticationToken *token = [notification.userInfo valueForKey:@"token"];
        BTSMSession *smSession = [notification.userInfo valueForKey:@"smsession"];

        [[BTAuthenticationManager sharedManager] saveForCurrentlyLoggedInUserTheAuthenticationToken:token smSession:smSession andPassword:nil];
        
        [self.pinUnlockScreenDelegate loggedInUserSuccessfullyAuthenticatedOnPinUnlockScreen:self];
    }
    else
    {
        DDLogError(@"This method gets called because of success of an object of DLMPinUnlockScreen but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This method gets called because of success of an object of DLMPinUnlockScreen but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)signInAuthenticationAPICallFailedNotification:(NSNotification *)notification
{
    if ([[notification.userInfo valueForKey:@"username"] isEqualToString:_username])
    {
        [self removeObserverToNotificationForSignInAuthenticationCall];
        
        NLWebServiceError *error = [notification.userInfo valueForKey:@"error"];
        [self.pinUnlockScreenDelegate pinUnlockScreen:self loginAttemptFailedForLoggedInUserWithWebServiceError:error];
    }
    else
    {
        DDLogError(@"This method gets called because of failure of an object of DLMPinUnlockScreen but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This method gets called because of failure of an object of DLMPinUnlockScreen but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}




@end
