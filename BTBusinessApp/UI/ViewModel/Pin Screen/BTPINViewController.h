//
//  BTPINViewController.h
//  BTBusinessApp
//
//  Created by Rohini Kumar on 14/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTPINViewController;

@protocol BTPINViewControllerDelegate <NSObject>

@optional

- (void)pinViewController:(BTPINViewController *)pinViewController userHasSuccesfullySetupThePINWithInfo:(NSDictionary *)infoDic;

- (void)userDecidedToEnterPasswordInsteadOfPinOnPinViewController:(BTPINViewController *)pinViewController;

- (void)userSuccesfullyEnteredTheCorrectPINAndUserAuthenticationDoneOnPinViewController:(BTPINViewController *)pinViewController;

- (void)userEnteredIncorrectPINThreeTimesOnPinViewController:(BTPINViewController *)pinViewController;


@end

@interface BTPINViewController : UIViewController <UITextFieldDelegate>

typedef NS_ENUM(NSUInteger, MyBtPinEntryState) {
    StateRoadblockDefault,
    StateRoadblockFail,
    StateRoadblockFailTryAgain,
    StateRoadblockFailTryAgainTouchId,
    StateRoadblockFailKickToAuth,
    StateRoadblockOk,
    StateRoadblockTouchId,
    StateRoadblockFaceId,
    StateSetupInit,
    StateSetupConfirmPin,
    StateSetupConfirmPinFail,
    StateSetupConfirmPinFailTryAgain,
    StateSetupConfirmPinOk
};

@property (nonatomic, weak) id<BTPINViewControllerDelegate> pinControllerDelegate;
@property (nonatomic) NSDictionary *pinCreationInfoDic;
@property (strong, nonatomic) NSMutableArray* pinDotContainers;
@property (strong, nonatomic) NSMutableArray* pinDots;
@property (strong, nonatomic) NSString* tmpPinForConfirmation;
@property (nonatomic) MyBtPinEntryState currentState;
@property (nonatomic) NSInteger numPinEntryAttempts;
@property (nonatomic) NSInteger numRetries; // Auth / customer profile requests.
@property (nonatomic) BOOL isPinSetupJourney;   // Indicates if this is part of the setup journey. (Eg: For setting PIN)
@property (nonatomic) BOOL initRequired;    // Indicates if this view will be responsible for authentication / fetching customer profile.
@property (nonatomic) BOOL pinEntryTextFieldIsFrozen;
@property (nonatomic) BOOL networkRequestInProgress;
@end
