//
//  BTSecuritySettingsViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 26/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTSecuritySettingsViewController.h"
#import "DLMSecuritySettingsScreen.h"
#import "BTTouchIdDisabledView.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "BTTakeActionWithSwitchTableViewCell.h"
#import "BTTakeActionTableViewCell.h"
#import "BTSecurityQuestionsAndAnswerViewController.h"
#import "BTSecurityNumberViewController.h"
#import "BTSettingsUpdatePasswordViewController.h"
#import "BTAlertBannerView.h"
#import "BTUpdatePINViewController.h"
#import "AppDelegateViewModel.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import <CocoaLumberjack/DDFileLogger.h>

#define kCellWithSwitch @"BTTakeActionWithSwitchTableViewCell"
#define kTakeActionCell @"BTTakeActionTableViewCellID"

#define kTableViewYPos 0
#define kAlertBannerHeight 40

typedef enum securitySettingSelectedOption
{
    UpdatePIN,
    ChangePassword,
    //ChangeSecurityQuestion,
    ChangeSecurityNumber,
    EnableTouchID
    
}SecuritySettingSelectedOption;


@interface BTSecuritySettingsViewController () <BTTouchIdDisabledViewDelegate,UITableViewDelegate,UITableViewDataSource,BTTakeActionSwitchTableViewCellDelegate, BTSecurityNumberViewControllerDelegate, BTSecurityQuestionsAndAnswerViewControllerDelegate, BTSettingsUpdatePasswordViewControllerDelegate, BTUpdatePINViewControllerDelegate> {
    
    UIView *_overlayView;
    BTTouchIdDisabledView *_touchIdDisabledView;
    NSMutableArray *securityArray;
    UITableView *_tableView;
    BTTakeActionWithSwitchTableViewCell *_switchCell;
    BTAlertBannerView *_alertBannerView;
    CGFloat originalTableViewTopContraint;
}

@property (nonatomic, readwrite) DLMSecuritySettingsScreen *viewModel;

@property (weak, nonatomic) IBOutlet UILabel *touchIDNotSupportedLabel;
@property (weak, nonatomic) IBOutlet UILabel *useTouchIDLabel;

@property (weak, nonatomic) IBOutlet UISwitch *touchIDSwitchButton;

@property (nonatomic, readwrite) AppDelegateViewModel *appDelegateviewModel;

@end

@implementation BTSecuritySettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.appDelegateviewModel = [[AppDelegateViewModel alloc] init];
    
    self.navigationItem.title = @"Security settings";
    
    // Do any additional setup after loading the view.
    
    self.viewModel = [[DLMSecuritySettingsScreen alloc] init];
    [self.touchIDSwitchButton setOn:NO];
    
    [self updateUserInterface];
    
    [self createData];
    
    [self createTableView];
    
    [_tableView reloadData];
    
    [self trackPage];
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChangeDetailSettings:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
    
}

- (void)statusBarOrientationChangeDetailSettings:(NSNotification *)notification {
    // handle the interface orientation as needed
    [self tabelViewOrientation];
}


- (void)addBanner
{
    [self showAlertBannerWithMessage:@"PIN updated successfully"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
     [self tabelViewOrientation];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    originalTableViewTopContraint = _tableView.frame.origin.y;
   
}

- (void)viewDidDisappear:(BOOL)animated{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
}

- (void)trackPage
{

    NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:[AppManager getOmniDictionary]];
    NSString *pageName = OMNIPAGE_SETTINGS_SECURITY;
    [OmnitureManager trackPage:pageName withContextInfo:data];

}


- (void)createData
{
    if(!securityArray)
        securityArray = [[NSMutableArray alloc] init];
    
    DLMSecuritySettingsScreen *updatePin = [[DLMSecuritySettingsScreen alloc]init];
    updatePin.heading = @"Update PIN";
    updatePin.cellTpye  = TakeActionCell;
    
    DLMSecuritySettingsScreen *chngePass = [[DLMSecuritySettingsScreen alloc]init];
    chngePass.heading = @"Change password";
    chngePass.cellTpye  = TakeActionCell;
    
//    DLMSecuritySettingsScreen *chngeSecurityQuestion = [[DLMSecuritySettingsScreen alloc]init];
//    chngeSecurityQuestion.heading = @"Change security question";
//    chngeSecurityQuestion.cellTpye  = TakeActionCell;
    
    DLMSecuritySettingsScreen *chngeSecurityNumber = [[DLMSecuritySettingsScreen alloc] init];
    chngeSecurityNumber.heading = @"Update security number";
    chngeSecurityNumber.cellTpye = TakeActionCell;
    
    [securityArray addObject:updatePin];
    [securityArray addObject:chngePass];
    //[securityArray addObject:chngeSecurityQuestion];
    [securityArray addObject:chngeSecurityNumber];
    
    if ([self.viewModel isTouchIDAvailableinDevice]) {
        DDLogInfo(@"Touch ID available in device.");
        DLMSecuritySettingsScreen *touchId = [[DLMSecuritySettingsScreen alloc]init];
        touchId.cellTpye  = SwitchCell;
        [securityArray addObject:touchId];
    }
    
}

- (void)createTableView
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,[[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _tableView.estimatedRowHeight = 80;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    
    UINib *nib = [UINib nibWithNibName:kCellWithSwitch bundle:nil];
    [_tableView registerNib:nib forCellReuseIdentifier:kCellWithSwitch];
    
    nib = [UINib nibWithNibName:@"BTTakeActionTableViewCell" bundle:nil];
    [_tableView registerNib:nib forCellReuseIdentifier:kTakeActionCell];
    
    
    [self.view addSubview:_tableView];
}

- (void) tabelViewOrientation {
    
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    _tableView.frame = CGRectMake(0, 0,screenSize.width, screenSize.height);
    //_tableView.contentInset = UIEdgeInsetsMake(0, 0, 120, 0);
}


- (void)updateUserInterface {
    DDLogInfo(@"Updating the security settings UI.");
    LAContext *context = [[LAContext alloc] init];
    NSError *localAuthError = nil;
    if (@available(iOS 11.0, *)) {
        if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&localAuthError]) {
            if(context.biometryType == LABiometryTypeFaceID) {
                DDLogInfo(@"Does the device support Face ID?");
                [self.viewModel checkDeviceSupportsFaceIDScanningStatus];
                [self setUpUIForFaceID];
            } else {
                [self.viewModel checkDeviceSupportsFingerprintScanningStatus];
                [self setUpUIForTouchID];
            }
        }
    } else {
        [self.viewModel checkDeviceSupportsFingerprintScanningStatus];
        [self setUpUIForTouchID];
    }
}

- (void)setUpUIForTouchID {
    DDLogInfo(@"Setting up for Touch ID.");
    if (self.viewModel.currentStatusOfFingerprintAuthenticationPolicy == DLMSecuritySettingsTouchIDStateTouchIDNotAvailable) {
        self.touchIDSwitchButton.hidden = YES;
        self.useTouchIDLabel.hidden = YES;
    } else if ([[AppDelegate sharedInstance].viewModel.app.isTouchIDEnabledForUnlock boolValue]) {
        self.touchIDNotSupportedLabel.hidden = YES;
        [self.touchIDSwitchButton setOn:YES];
    } else
    {
        self.touchIDNotSupportedLabel.hidden = YES;
        [self.touchIDSwitchButton setOn:NO];
    }
}

-(void)setUpUIForFaceID {
    DDLogInfo(@"Setting up for Face ID.");
    if (self.viewModel.currentStatusOfFaceIDAuthenticationPolicy == DLMSecuritySettingsFaceIDStateFaceIDNotAvailable) {
        self.touchIDSwitchButton.hidden = YES;
        self.useTouchIDLabel.hidden = YES;
    } else if ([[AppDelegate sharedInstance].viewModel.app.isFaceIDEnabledForUnlock boolValue]) {
        DDLogInfo(@"The user has enabled Face ID already.");
        self.touchIDNotSupportedLabel.hidden = YES;
        [self.touchIDSwitchButton setOn:YES];
    } else
    {
        self.touchIDNotSupportedLabel.hidden = YES;
        [self.touchIDSwitchButton setOn:NO];
    }
}

- (IBAction)touchIDEnableSwitchAction:(id)sender {
    
    DDLogInfo(@" Enable touch id Switch value changed to: %d",self.touchIDSwitchButton.isOn);
    
    if (self.touchIDSwitchButton.isOn) {
        [self enableTouchIDForUnlockApp];
    }
    else
    {
        [self disableTouchIDForUnlockApp];
    }
    
}

- (void)showAlertBannerWithMessage:(NSString *)alertMessage{
    
    if(_alertBannerView.alpha > 0.5)
        return;
    
    if(!_alertBannerView){
        
        _alertBannerView = [[[NSBundle mainBundle] loadNibNamed:@"BTAlertBannerView" owner:self options:nil] firstObject];
        
        _alertBannerView.frame = CGRectMake(0,
                                            kTableViewYPos,
                                            self.view.frame.size.width,
                                            kAlertBannerHeight);
        
        [self.view addSubview:_alertBannerView];
    }
    
    [_alertBannerView showAnimated:YES];
    
    [_alertBannerView setMessage:alertMessage];
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:kBannerAnimationTime animations:^{
        
        CGRect frame = _tableView.frame;
        frame.origin.y  = kTableViewYPos+kAlertBannerHeight;
        _tableView.frame = frame;
        
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        [self performSelector:@selector(hideAlertBanner) withObject:nil afterDelay:3.0];
    }];
    
    
}


- (void)hideAlertBanner{
    
    [_alertBannerView hideAnimated:YES];
    
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:kBannerAnimationTime animations:^{
        
        CGRect frame = _tableView.frame;
        frame.origin.y  = kTableViewYPos;
        _tableView.frame = frame;
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
    }];
    
}



- (void)enableTouchIDForUnlockApp {
    DDLogInfo(@"TouchID not enrolled method");
    if (self.viewModel.currentStatusOfFingerprintAuthenticationPolicy == DLMSecuritySettingsTouchIDStateTouchIDNotEnrolled) {
        [self.touchIDSwitchButton setOn:NO];
        [_switchCell.takeActionSwich setOn:NO];
        [self showTouchIdDisabledView];
    }
    else
    {
        [self.viewModel enableTouchIdForUnlock];
    }
    
}

- (void)disableTouchIDForUnlockApp {
    [self.viewModel disableTouchIdForUnlock];
}

- (void)showTouchIdDisabledView
{
    
    // (lp) overlay to display touch id related views. No need to make the same overlay again for touch id disable view and use the same.
    _overlayView = [[UIView alloc] init];
    _overlayView.translatesAutoresizingMaskIntoConstraints = NO;
    _overlayView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    
    // (lp) Display touch id disable view on overlay to user.
    _touchIdDisabledView = [[[NSBundle mainBundle] loadNibNamed:@"BTTouchIdDisabledView" owner:nil options:nil] objectAtIndex:0];
    _touchIdDisabledView.touchIdDisabledViewDelegate = self;
    _touchIdDisabledView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:_overlayView];
    [self.view addSubview:_touchIdDisabledView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_overlayView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_overlayView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_overlayView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_overlayView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_touchIdDisabledView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:25.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_touchIdDisabledView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-25.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_touchIdDisabledView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
    // (lp) Add observer for get notification when app comes from background.
    //[self addObserverForDidBecomeActiveNotification];
}

- (BOOL)getTouchIdStatus
{
    [self.viewModel checkDeviceSupportsFingerprintScanningStatus];
    
    if (self.viewModel.currentStatusOfFingerprintAuthenticationPolicy == DLMSecuritySettingsTouchIDStateTouchIDNotAvailable) {
        return  YES;
    } else if ([[AppDelegate sharedInstance].viewModel.app.isTouchIDEnabledForUnlock boolValue]) {
        return YES;
    } else
    {
        return NO;
    }
}

-(BOOL)getFaceIdStatus
{
    [self.viewModel checkDeviceSupportsFaceIDScanningStatus];
    if (self.viewModel.currentStatusOfFaceIDAuthenticationPolicy == DLMSecuritySettingsFaceIDStateFaceIDNotAvailable) {
        return  YES;
    } else if ([[AppDelegate sharedInstance].viewModel.app.isFaceIDEnabledForUnlock boolValue]) {
        return YES;
    } else
    {
        return NO;
    }
    
}


#pragma mark - BTTouchIdDisableViewDelegate methods
- (void)redirectToDeviceSettingsWithTouchIDDisbaledView:(BTTouchIdDisabledView *)touchIdDisabledView {
    [_overlayView removeFromSuperview];
    [_touchIdDisabledView removeFromSuperview];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

- (void)cancelledEnrollTouchIDWithTouchIdDisabledView:(BTTouchIdDisabledView *)touchIdDisabledView {
    [_overlayView removeFromSuperview];
    [_touchIdDisabledView removeFromSuperview];
}

#pragma mark - TableView Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [securityArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DLMSecuritySettingsScreen  * wrapper = [securityArray objectAtIndex:indexPath.row];
    
    if(wrapper.cellTpye == TakeActionCell)
    {
        BTTakeActionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BTTakeActionTableViewCellID"];
        
        [cell updateWithTitle:wrapper.heading];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }
    else
    {
        BTTakeActionWithSwitchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellWithSwitch];
        
        LAContext *context = [[LAContext alloc] init];
        NSError *localAuthError = nil;
        if (@available(iOS 11.0, *)) {
            if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&localAuthError]) {
                if(context.biometryType == LABiometryTypeFaceID) {
                    [cell updateSwitchStatus:[self getFaceIdStatus]];
                } else {
                    [cell updateSwitchStatus:[self getTouchIdStatus]];
                }
            }
        } else {
            [cell updateSwitchStatus:[self getTouchIdStatus]];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
        return cell;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == UpdatePIN)
    {
        [self trackOmnitureClick:OMNICLICK_SETTINGS_UPDATEPIN];
        BTUpdatePINViewController *controller = [BTUpdatePINViewController getUpdatePINViewController];
        controller.delegate = self;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if(indexPath.row == ChangePassword)
    {
        //Redirect to change password screen
        [self trackOmnitureClick:OMNICLICK_SETTINGS_CHANGE_PASSWORD];
        BTSettingsUpdatePasswordViewController *updatePasswordVC = [BTSettingsUpdatePasswordViewController getBTSettingsUpdatePasswordViewController];
        updatePasswordVC.delegate = self;
        [self.navigationController pushViewController:updatePasswordVC animated:YES];
    }
//    else if(indexPath.row == ChangeSecurityQuestion)
//    {
//        //Redirect to change security question screen
//        [self trackOmnitureClick:OMNICLICK_SETTINGS_CHANGE_QUESTION];
//        BTSecurityQuestionsAndAnswerViewController *changeSecurityQuestion = [self.storyboard instantiateViewControllerWithIdentifier:@"SecurityQuestionsAndAnswerScene"];
//        changeSecurityQuestion.delegate = self;
//        //changeSecurityQuestion.isScreenRelatedToInterceptorModule = YES;
//        [self.navigationController pushViewController:changeSecurityQuestion animated:YES];
//    }
    else if(indexPath.row == ChangeSecurityNumber)
    {
        //Redirect to change security question screen
        // TODO: Omniture
        [self trackOmnitureClick:OMNICLICK_SETTINGS_UPDATE_SECURITY_NUMBER];
        BTSecurityNumberViewController *changeSecurityNumber = [self.storyboard instantiateViewControllerWithIdentifier:@"BTSecurityNumberViewController"];
        changeSecurityNumber.isScreenRelatedToInterceptorModule = NO;
        changeSecurityNumber.isScreenrelatedTo1FAMigration = NO;
        changeSecurityNumber.delegate = self;
        [self.navigationController pushViewController:changeSecurityNumber animated:YES];
    }
    else
    {
        
    }
    
}

- (void)trackOmnitureClick:(NSString *)event
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",OMNIPAGE_SETTINGS_SECURITY,event] withContextInfo:data];
}

#pragma mark - BTSecurityNumberrViewControllerDelegate methods

- (void)successfullyUpdatedSecurityNumberOnSecurityNumberViewController:(BTSecurityNumberViewController *)securityNumberViewController
{
    [self showAlertBannerWithMessage:@"Security number updated successfully"];
    // TODO: Omniture
    //[AppManager trackOmnitureKeyTask:OMNI_KEYTASK_SETTINGS_SECURITY_QUESTION_UPDATED forPageName:OMNIPAGE_SETTINGS_SECURITY];
}

#pragma mark - BTSecurityQuestionsAndAnswerViewControllerDelegate methods
- (void)successfullyUpdatedSecurityQuestionOnSecurityQuestionsAndAnswerViewController:(BTSecurityQuestionsAndAnswerViewController *)securityQuestionsAndAnswerViewController
{
    [self showAlertBannerWithMessage:@"Security question updated successfully"];
     [AppManager trackOmnitureKeyTask:OMNI_KEYTASK_SETTINGS_SECURITY_QUESTION_UPDATED forPageName:OMNIPAGE_SETTINGS_SECURITY];
}

#pragma mark - BTSettingsUpdatePasswordViewControllerDelegate methods
- (void)successfullyUpdatedPasswordOnUpdatePasswordViewController:(BTSettingsUpdatePasswordViewController *)updatePasswordViewController
{
    [AppManager trackOmnitureKeyTask:OMNI_KEYTASK_SETTINGS_PASSWORD_CHANGE forPageName:OMNIPAGE_SETTINGS_SECURITY];
    [self showAlertBannerWithMessage:@"Password updated successfully"];
}

#pragma mark - Take Action Swtich Delegate

- (void)userDidChangedSwitchValueOnTakeActionCell:(BTTakeActionWithSwitchTableViewCell *)cell withSwitchStatus:(BOOL)switchStatus
{
    DDLogInfo(@"Is this being invoked?");
    _switchCell = cell;
    [self.touchIDSwitchButton setOn:switchStatus];
    [self touchIDEnableSwitchAction:nil];
    
    //Track event
    [self trackOmnitureClick:OMNICLICK_SETTINGS_TOUCH_ID];
}


#pragma mark - BTUpdatePINViewController Delegate

- (void)updatePINViewController:(BTUpdatePINViewController *)updatePINViewController userHasSuccesfullyUpdateThePINWithPIN:(NSString *)pin
{
    [self showAlertBannerWithMessage:@"PIN updated successfully"];
    
    //Track Event
    [AppManager trackOmnitureKeyTask:OMNI_KEYTASK_SETTINGS_PIN_UPDATED forPageName:OMNIPAGE_SETTINGS_SECURITY];
    
    [self.appDelegateviewModel updatePINWithUpdatedPIN:pin];
    
}




@end
