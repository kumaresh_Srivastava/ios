//
//  DLMSecuritySettingsScreen.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 26/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"

typedef NS_ENUM(NSUInteger, DLMSecuritySettingsTouchIDState) {
    DLMSecuritySettingsTouchIDStateTouchIDNotAvailable,
    DLMSecuritySettingsTouchIDStateTouchIDNotEnrolled,
    DLMSecuritySettingsTouchIDStateTouchIDEnabled,
};

typedef NS_ENUM(NSUInteger, DLMSecuritySettingsFaceIDState) {
    DLMSecuritySettingsFaceIDStateFaceIDNotAvailable,
    DLMSecuritySettingsFaceIDStateFaceIDNotEnrolled,
    DLMSecuritySettingsFaceIDStateFaceIDEnabled,
};

typedef enum typeOfCell
{
   TakeActionCell,
    SwitchCell
}TypeOfCell;

@interface DLMSecuritySettingsScreen : DLMObject 

@property (nonatomic, readonly) DLMSecuritySettingsTouchIDState currentStatusOfFingerprintAuthenticationPolicy;
@property (nonatomic, readonly) DLMSecuritySettingsFaceIDState currentStatusOfFaceIDAuthenticationPolicy;
@property (strong,nonatomic) NSString *heading;
@property (nonatomic , assign) TypeOfCell cellTpye;

/* This Method checks if device supports fingerprint scanning */
- (void)checkDeviceSupportsFingerprintScanningStatus;
- (void)checkDeviceSupportsFaceIDScanningStatus;
- (void)enableTouchIdForUnlock;
- (void)disableTouchIdForUnlock;
- (BOOL)isTouchIDAvailableinDevice;
@end
