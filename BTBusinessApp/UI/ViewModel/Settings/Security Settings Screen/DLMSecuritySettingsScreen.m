//
//  DLMSecuritySettingsScreen.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 26/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMSecuritySettingsScreen.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"

@implementation DLMSecuritySettingsScreen

#pragma mark - Public methods
- (void)checkDeviceSupportsFingerprintScanningStatus
{
    // (LP) Get the local authentication context.
    LAContext* localAuthContext = [[LAContext alloc] init];
    NSError* localAuthError = nil;
    
    // (LP) Check if the user has enabled the feature.
    if ([localAuthContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&localAuthError]) {
        _currentStatusOfFingerprintAuthenticationPolicy = DLMSecuritySettingsTouchIDStateTouchIDEnabled;
        DDLogInfo(@"Pin: Touch ID is supported on this device.");
        
    } else {
        // (LP) Check error code
        if ([localAuthError code]) {
            // (LP) The user hasn't registered any fingerprints.
            if ([localAuthError code] == LAErrorTouchIDNotEnrolled) {
                _currentStatusOfFingerprintAuthenticationPolicy = DLMSecuritySettingsTouchIDStateTouchIDNotEnrolled;
                DDLogInfo(@"Pin: Touch ID is supported but no fingerprints are enrolled.");
            }
            
            // (LP) Not supported? This shouldn't happen.
            else {
                _currentStatusOfFingerprintAuthenticationPolicy = DLMSecuritySettingsTouchIDStateTouchIDNotAvailable;
                DDLogInfo(@"Pin: Touch ID is not supported on this device. (%@)", localAuthError);
            }
        } else {
            _currentStatusOfFingerprintAuthenticationPolicy = DLMSecuritySettingsTouchIDStateTouchIDNotAvailable;
            DDLogInfo(@"Pin: Touch ID is not supported on this device. (%@)", localAuthError);
        }
    }
}

- (void)checkDeviceSupportsFaceIDScanningStatus;
{
    DDLogInfo(@"Pin: Checking for Face ID status.");
    LAContext* localAuthContext = [[LAContext alloc] init];
    NSError* localAuthError = nil;
    
    if (@available(iOS 11.0, *)) {
        if ([localAuthContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&localAuthError]) {
            if(localAuthContext.biometryType == LABiometryTypeFaceID) {
                _currentStatusOfFaceIDAuthenticationPolicy = DLMSecuritySettingsFaceIDStateFaceIDEnabled;
                DDLogInfo(@"Pin: Face ID is supported on this device.");
            } else {
                DDLogInfo(@"No biometry faceID");
                if ([localAuthError code]) {
                    if ([localAuthError code] == LAErrorBiometryNotEnrolled) {
                        _currentStatusOfFaceIDAuthenticationPolicy = DLMSecuritySettingsFaceIDStateFaceIDNotEnrolled;
                        DDLogInfo(@"Pin: Face ID is supported but not enrolled.");
                    } else {
                        _currentStatusOfFaceIDAuthenticationPolicy = DLMSecuritySettingsFaceIDStateFaceIDNotAvailable;
                        DDLogInfo(@"Pin: Face ID is not supported on this device. (%@)", localAuthError);
                    }
                } else {
                    _currentStatusOfFaceIDAuthenticationPolicy = DLMSecuritySettingsFaceIDStateFaceIDNotAvailable;
                    DDLogInfo(@"Pin: Face ID is not supported on this device. (No Auth Error code)");
                }
            }
        } else {
            _currentStatusOfFaceIDAuthenticationPolicy = DLMSecuritySettingsFaceIDStateFaceIDNotAvailable;
            DDLogInfo(@"Pin: Face ID is not supported on this device. (No Auth Error code)");
        }
    } else {
        // Fallback on earlier versions
    }
}

- (void)enableTouchIdForUnlock
{
    // Face ID or Touch ID?
    LAContext *context = [[LAContext alloc] init];
    NSError *localAuthError = nil;
    if (@available(iOS 11.0, *)) { //BUG 16332
        if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&localAuthError]) {
            [AppDelegate sharedInstance].viewModel.app.isTouchIDEnabledForUnlock = [NSNumber numberWithBool:YES];
              [AppDelegate sharedInstance].viewModel.app.isFaceIDEnabledForUnlock = [NSNumber numberWithBool:YES];
        } else {
            [AppDelegate sharedInstance].viewModel.app.isTouchIDEnabledForUnlock = [NSNumber numberWithBool:YES];
            [AppDelegate sharedInstance].viewModel.app.isFaceIDEnabledForUnlock = [NSNumber numberWithBool:YES];
        }
    } else {
        [AppDelegate sharedInstance].viewModel.app.isTouchIDEnabledForUnlock = [NSNumber numberWithBool:YES];
          [AppDelegate sharedInstance].viewModel.app.isFaceIDEnabledForUnlock = [NSNumber numberWithBool:YES];
    }
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
}

- (void)disableTouchIdForUnlock
{
    // Face ID or Touch ID?
    LAContext *context = [[LAContext alloc] init];
    NSError *localAuthError = nil;
    if (@available(iOS 11.0, *)) { //BUG 16332
        if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&localAuthError]) {
            [AppDelegate sharedInstance].viewModel.app.isTouchIDEnabledForUnlock = [NSNumber numberWithBool:NO];
            [AppDelegate sharedInstance].viewModel.app.isFaceIDEnabledForUnlock = [NSNumber numberWithBool:NO];
        } else {
            [AppDelegate sharedInstance].viewModel.app.isTouchIDEnabledForUnlock = [NSNumber numberWithBool:NO];
            [AppDelegate sharedInstance].viewModel.app.isFaceIDEnabledForUnlock = [NSNumber numberWithBool:NO];
        }
    } else {
        [AppDelegate sharedInstance].viewModel.app.isTouchIDEnabledForUnlock = [NSNumber numberWithBool:NO];
        [AppDelegate sharedInstance].viewModel.app.isFaceIDEnabledForUnlock = [NSNumber numberWithBool:NO];
    }
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
}

- (BOOL)isTouchIDAvailableinDevice {
    LAContext* localAuthContext = [[LAContext alloc] init];
    NSError* localAuthError = nil;
    return [localAuthContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&localAuthError];
}

@end
