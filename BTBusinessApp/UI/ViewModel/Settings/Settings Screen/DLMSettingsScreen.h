//
//  DLMSettingsScreen.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 18/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"

@class DLMSettingsScreen;

@protocol DLMSettingsScreenDelegate <NSObject>

@end

@interface DLMSettingsScreen : DLMObject


@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *subTitle;
@property (strong, nonatomic) NSString *imgName;

@property (nonatomic, weak) id <DLMSettingsScreenDelegate> settingsScreenDelegate;

@end
