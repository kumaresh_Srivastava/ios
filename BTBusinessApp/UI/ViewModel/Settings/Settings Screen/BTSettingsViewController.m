//
//  BTSettingsViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 17/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTSettingsViewController.h"
#import "AppDelegate.h"
#import "BTSecuritySettingsViewController.h"
#import "DLMSettingsScreen.h"
#import "BTUICommon.h"
#import "AppConstants.h"
#import "BTSettingTableViewCell.h"
#import "BTSecuritySettingsViewController.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"
#import "BTSettingsContactDetailViewController.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "BTSettingsDeveloperModeViewController.h"

#define kBTSettingCell @"BTSettingTableViewCell"
#define kDeveloperModeButtonHeight 100 //Added in table footer view
#define kBaseTag 8878 //RLM (6 Feb) used to find the UIView taped

typedef enum selectedSettings
{
    PersonalSettings,
    SecuritySettings,
    NotificationSettings
}SelectedSettings;

@interface BTSettingsViewController () <UITableViewDataSource, UITableViewDelegate, DLMSettingsScreenDelegate> {
    
}

@property (nonatomic, readwrite) DLMSettingsScreen *viewModel;
@property (nonatomic, strong) NSMutableArray *settingsArray;
@property (nonatomic, strong) UIBarButtonItem *leftBarButtonItem;
@property (weak, nonatomic) IBOutlet UIButton *signOutButton;

@end

@implementation BTSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Setup View Model
    self.viewModel = [[DLMSettingsScreen alloc] init];
    self.viewModel.settingsScreenDelegate = self;
    
    
    
    
    // (LP) Navigation bar appearence
    //[BTUICommon updateNavigationBar:self.navigationController];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon_Home_White"] style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonAction)];
    
    self.title = @"Settings";
    // (LP) Table view setup
    self.settingsTableView.delegate = self;
    self.settingsTableView.dataSource = self;
    self.settingsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.settingsTableView.backgroundColor = [UIColor clearColor];
    
    self.settingsTableView.estimatedRowHeight = 100;
    self.settingsTableView.rowHeight = UITableViewAutomaticDimension;
    
    [self.view addSubview:self.settingsTableView];
    [self.view sendSubviewToBack:self.settingsTableView];
    
    UINib *nib = [UINib nibWithNibName:kBTSettingCell bundle:nil];
    [self.settingsTableView registerNib:nib forCellReuseIdentifier:kBTSettingCell];
    
    
    
    self.settingsArray = [[NSMutableArray alloc] init];
    
    // (LP) Create settings data
    [self createSettingsData];
    [self.settingsTableView reloadData];
    
    self.signOutButton.layer.cornerRadius = 4.0f;
    self.signOutButton.layer.borderWidth = 1;
    self.signOutButton.layer.borderColor = [UIColor blackColor].CGColor;
    
    
    [self trackPageForOmniture];
    
}


#pragma mark - Private helper methods

- (void)trackPageForOmniture
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:OMNIPAGE_SETTINGS_HOME withContextInfo:data];
}

- (void)trackOmnitureClickEvent:(NSString *)event
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",OMNIPAGE_SETTINGS_HOME,event] withContextInfo:data];
}

- (void)createSettingsData {
    
    DLMSettingsScreen *personalOption = [[DLMSettingsScreen alloc] init];
    personalOption.title = @"Personal";
    personalOption.subTitle = @"Update your contact information";
    personalOption.imgName = @"personalSetting";
    
    DLMSettingsScreen *securityOption = [[DLMSettingsScreen alloc] init];
    securityOption.title = @"Security";
    //securityOption.subTitle = @"Update your security details";
    securityOption.subTitle = @"Update your login details";
    securityOption.imgName = @"securitySetting";
    
    /*DLMSettingsScreen *notificationOption = [[DLMSettingsScreen alloc] init];
    notificationOption.title = @"Notifications";
    notificationOption.subTitle = @"Update your alert preferences";
    notificationOption.imgName = @"notificationSetting";*/
    
    
    [self.settingsArray addObject:personalOption];
    [self.settingsArray addObject:securityOption];
    //[self.settingsArray addObject:notificationOption];

}


- (void)logOutUser
{
    [[AppDelegate sharedInstance].viewModel logoutCurrentUser];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUserLoggedOutFromApp object:nil userInfo:nil];
}

- (void)showAlertView
{
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Are you sure?"  message:@"You'll need to re-enter PIN."  preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Sign out" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self logOutUser];
    }]];
    
    
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - table view datasource methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.settingsArray count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BTSettingTableViewCell *settingCell = [tableView dequeueReusableCellWithIdentifier:kBTSettingCell];
    settingCell.selectionStyle = UITableViewCellSelectionStyleNone;
    DLMSettingsScreen *screenModel = [_settingsArray objectAtIndex:indexPath.row];
    [settingCell updateInfoWithSettingsDLM:screenModel];
    
    return settingCell;
}

#pragma mark - table view delegate methods

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == PersonalSettings)
    {
        //Open Personal Settngs
        [self trackOmnitureClickEvent:OMNICLICK_SETTINGS_PERSONAL];
        BTSettingsContactDetailViewController *controller = (BTSettingsContactDetailViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"BTSettingsContactDetailViewControllerID"];

        [self.navigationController pushViewController:controller animated:YES];
        
    }
    else if(indexPath.row == SecuritySettings)
    {
        //Open Security Settings
        [self trackOmnitureClickEvent:OMNICLICK_SETTINGS_SECURITY];
        BTSecuritySettingsViewController *controller = (BTSecuritySettingsViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"SecuritySettingsScene"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else
    {
        //Open Notification
       // [self trackOmnitureClickEvent:OMNICLICK_SETTINGS_NOTIFICATION];
    }
    
}


#pragma mark - Action methods
- (IBAction)homeButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)userPressedSignOutButton:(id)sender {
    
    [self trackOmnitureClickEvent:OMNICLICK_SETTINGS_SIGNOUT];
    [self showAlertView];
}

- (void)doneButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
