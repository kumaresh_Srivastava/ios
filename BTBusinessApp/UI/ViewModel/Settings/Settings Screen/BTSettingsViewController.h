//
//  BTSettingsViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 17/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseViewController.h"

@interface BTSettingsViewController : BTBaseViewController

@property (strong, nonatomic) IBOutlet UITableView *settingsTableView;

@property (strong, nonatomic) NSArray *groupsArray;

- (IBAction)userPressedSignOutButton:(id)sender;
@end
