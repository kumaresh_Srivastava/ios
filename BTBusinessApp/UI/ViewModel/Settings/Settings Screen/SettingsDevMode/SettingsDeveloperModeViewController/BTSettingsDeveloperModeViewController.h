//
//  SettingsDeveloperModeViewController.h
//  BTBusinessApp
//
//  Created by vectoscalar on 06/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseViewController.h"

typedef enum developerModeType
{
   BTDevModePersonalDetail ,
   BTDevModeChangeSecurityQuestion,
    BTDevModeChangePassword
}DeveloperModeType;

@interface BTSettingsDeveloperModeViewController : BTBaseViewController

@property (nonatomic,assign) DeveloperModeType developerModeType;

@end
