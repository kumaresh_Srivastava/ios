//
//  SettingsDeveloperModeViewController.m
//  BTBusinessApp
//
//  Created by vectoscalar on 06/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//


#import "BTSettingsDeveloperModeViewController.h"
#import "AppManager.h"
#import "BrandColours.h"
#import "BTSMSession.h"
#import "BTAuthenticationManager.h"

#define kPersonalDetailsMode @"Personal details"
#define kChangeSecurityQuestionMode @"Change security question"
#define kChangePasswordMode @"Change password"

#define kDevModeBaseURL  @"account/loginredirect.aspx?target="
#define kPersonalDetailEndPointUrl @"Account/AccountSettings/PersonalDetails"
#define kChangeSecurityQuestionEndPointURL @"Account/AccountSettings/ChangeSecurityQuestion"
#define kChangePasswordEndPointURL @"Account/AccountSettings/ChangePassword"



@interface BTSettingsDeveloperModeViewController ()<UIWebViewDelegate>

{
    UIWebView *_mWebView;
    UIActivityIndicatorView *_activityIndicatorView;
}

@end

@implementation BTSettingsDeveloperModeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = [self naviagtionTitle];
    
    //Create web view
    [self createAndShowWebView];
    
    
    //Add back button on navigation bar
    [self setNavigationButton];
    
    //Create Activity Indicator View
    [self createActivityIndicatorView];
    
    //Start loading Request
    [self loadURLRquestInWebView];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Helper methods
- (void)createAndShowWebView
{
    
    
    
    _mWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, [[UIScreen mainScreen] bounds].size.height-64)];
    _mWebView.delegate = self;
    
    
    [self.view addSubview:_mWebView];
}


- (void)setNavigationButton
{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(-15,0,60,40)];
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0,12, 14, 25)];
    imgView.backgroundColor = [UIColor clearColor];
    imgView.image = [UIImage imageNamed:@"Back_Arrow"];
    
    UIButton *leftArrowButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftArrowButton.frame = CGRectMake(-15, 0,60, 40);
    // [leftArrowButton setImage:[UIImage imageNamed:@"Back_Arrow"] forState:UIControlStateNormal];
    [leftArrowButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    leftArrowButton.backgroundColor = [UIColor clearColor];
    
    [view addSubview:imgView];
    [view addSubview:leftArrowButton];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:view];
    
    self.navigationItem.leftBarButtonItem = barButtonItem;
}


- (void)backButtonPressed
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSString *)naviagtionTitle
{
    if(self.developerModeType == BTDevModePersonalDetail)
    {
        return kPersonalDetailsMode;
    }
    else  if(self.developerModeType == BTDevModeChangeSecurityQuestion)
    {
        return kChangeSecurityQuestionMode;;
    }
    else
    {
        return  kChangePasswordMode;
    }
}

- (void)createActivityIndicatorView
{
    _activityIndicatorView  = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _activityIndicatorView.color = [BrandColours colourBtDarkViolet];
    _activityIndicatorView.center = _mWebView.center;
    _activityIndicatorView.hidesWhenStopped = YES;
    
    [self.view addSubview:_activityIndicatorView];
}


- (void)loadURLRquestInWebView
{
    
    //Create the complete URl
    NSString *baseUrl = [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel];
    
    NSString *devBaseUrl = kDevModeBaseURL;
    
    NSString *endPointUrl =  [self getEndPointURLForSelectedDeveloperMode];
    
    NSString *completeURL = [NSString stringWithFormat:@"https://%@/%@https://%@/%@",baseUrl,devBaseUrl,baseUrl,endPointUrl];
    
    BTSMSession *smsession = [[BTAuthenticationManager sharedManager] smsessionForCurrentLoggedInUser];
    
    //Set Cookie
    NSMutableDictionary *cookieProperties = [NSMutableDictionary dictionary];
    [cookieProperties setObject:@"SMSESSION" forKey:NSHTTPCookieName];
    [cookieProperties setObject:smsession.smsessionString forKey:NSHTTPCookieValue];
    [cookieProperties setObject:@".bt.com" forKey:NSHTTPCookieDomain];
    [cookieProperties setObject:@".bt.com" forKey:NSHTTPCookieOriginURL];
    [cookieProperties setObject:@"/" forKey:NSHTTPCookiePath];
    [cookieProperties setObject:@"0" forKey:NSHTTPCookieVersion];
    [cookieProperties setValue:@"1" forKey:NSHTTPCookieSecure];
    
    [cookieProperties setObject:[[NSDate date] dateByAddingTimeInterval:2629743] forKey:NSHTTPCookieExpires];
    
    NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
   [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
    
    //Create Reqeust
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:completeURL]];
    
    [_mWebView loadRequest:request];
    
}


- (void)showAlert
{
    __weak typeof(self) weakSelf = self;
    
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Message"  message:@"Failed to load URL"  preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [weakSelf backButtonPressed];
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [weakSelf loadURLRquestInWebView];
    }]];
    
    
    
    [self presentViewController:alertController animated:YES completion:nil];
}


- (NSString *)getEndPointURLForSelectedDeveloperMode
{
    
    NSString *endPointURL;
    if(self.developerModeType == BTDevModePersonalDetail)
    {
        endPointURL = kPersonalDetailEndPointUrl;
    }
    else if(self.developerModeType == BTDevModeChangeSecurityQuestion)
    {
        endPointURL = kChangeSecurityQuestionEndPointURL;
    }
    else
    {
        endPointURL = kChangePasswordEndPointURL;
    }
    
    return endPointURL;
}


#pragma mark - WebView Delegate methods
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    [_activityIndicatorView startAnimating];
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_activityIndicatorView stopAnimating];
    NSLog(@"---web view finish loading");
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [_activityIndicatorView stopAnimating];
    [self showAlert];
}

@end
