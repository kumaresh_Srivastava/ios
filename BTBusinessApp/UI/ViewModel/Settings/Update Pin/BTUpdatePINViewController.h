//
//  BTUpdatePINViewController.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 17/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>


@class BTUpdatePINViewController;

@protocol BTUpdatePINViewControllerDelegate <NSObject>

- (void)updatePINViewController:(BTUpdatePINViewController *)updatePINViewController userHasSuccesfullyUpdateThePINWithPIN:(NSString *)pin;

@end


@interface BTUpdatePINViewController : UIViewController

+ (BTUpdatePINViewController *)getUpdatePINViewController;

typedef NS_ENUM(NSUInteger, MyBtPinUpdateEntryState) {
    StateRoadblockDefault,
    StateRoadblockFail,
    StateRoadblockFailTryAgain,
    StateRoadblockFailTryAgainTouchId,
    StateRoadblockFailKickToAuth,
    StateRoadblockOk,
    StateRoadblockTouchId,
    StateRoadblockFaceId,
    StateSetupInit,
    StateSetupConfirmPin,
    StateSetupConfirmPinFail,
    StateSetupConfirmPinFailTryAgain,
    StateSetupConfirmPinOk
};

@property (nonatomic, assign) id<BTUpdatePINViewControllerDelegate> delegate;

@end
