//
//  BTUpdatePINViewController.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 17/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTUpdatePINViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "DLMPinUnlockScreen.h"
#import "DLMPinCreationScreen.h"
#import <UIView+Shake/UIView+Shake.h>
#import "AppDelegateViewModel.h"
#import "CDUser.h"
#import "AppDelegate.h"
#import "CDApp.h"
#import "BTUICommon.h"
#import "AppManager.h"
#import "OmnitureManager.h"
#import "AppConstants.h"

static NSInteger const kNumPinDotShakes = 10;
static CGFloat const kPinDotShakeDelta = 8.0;
static NSTimeInterval const kPinDotShakeSpeed = 0.05;
static NSTimeInterval const kStateSwitchDelay = 0.25;//0.15 remove this comment after testing

@interface BTUpdatePINViewController ()<DLMPinUnlockScreenDelegate>

@property (strong, nonatomic) NSMutableArray* pinDotContainers;
@property (strong, nonatomic) NSMutableArray* pinDots;
@property (weak, nonatomic) IBOutlet UITextField *pinEntryTextField;

@property (weak, nonatomic) IBOutlet UIView *pinStripView;

@property (weak, nonatomic) IBOutlet UIView *pinView;
@property (weak, nonatomic) IBOutlet UILabel *pinStatusLabel;

@property (weak, nonatomic) IBOutlet UIView *pinErrorView;

@property (weak, nonatomic) IBOutlet UIView *pinDotBoxView01;
@property (weak, nonatomic) IBOutlet UIView *pinDotBoxView02;
@property (weak, nonatomic) IBOutlet UIView *pinDotBoxView03;
@property (weak, nonatomic) IBOutlet UIView *pinDotBoxView04;

@property (weak, nonatomic) IBOutlet UIView *pinDotView01;
@property (weak, nonatomic) IBOutlet UIView *pinDotView02;
@property (weak, nonatomic) IBOutlet UIView *pinDotView03;
@property (weak, nonatomic) IBOutlet UIView *pinDotView04;

@property (nonatomic) MyBtPinUpdateEntryState currentState;
@property (nonatomic) BOOL pinEntryTextFieldIsFrozen;

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *dontMatchWarningLabel;

@property (nonatomic, readwrite) DLMPinUnlockScreen *unlockViewModel;
@property (nonatomic, readwrite) DLMPinCreationScreen *creationViewModel;
@property (strong, nonatomic) NSString* tmpPinForConfirmation;

@property (strong,nonatomic) UIView *separatorViewForKeyboard;

@end

@implementation BTUpdatePINViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.title = @"Update PIN";
    self.navigationItem.hidesBackButton = YES;

    
    UIBarButtonItem *closeButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonPressed:)];

    self.navigationItem.leftBarButtonItem = closeButtonItem;
    

    //Setup texfiled
    [_pinEntryTextField becomeFirstResponder];
    [_pinEntryTextField setHidden:YES];
    [_pinEntryTextField setKeyboardType:UIKeyboardTypeNumberPad];

    _backButton.hidden = YES;
    _dontMatchWarningLabel.hidden = YES;
    _pinErrorView.hidden = YES;
    _dontMatchWarningLabel.text = kPinIncorrectErrorMessage;

    // Alloc required objects.
    [self setPinDotContainers:[NSMutableArray new]];
    [self setPinDots:[NSMutableArray new]];

    [[self pinDotContainers] addObject:[self pinDotBoxView01]];
    [[self pinDotContainers] addObject:[self pinDotBoxView02]];
    [[self pinDotContainers] addObject:[self pinDotBoxView03]];
    [[self pinDotContainers] addObject:[self pinDotBoxView04]];

    [[self pinDots] addObject:[self pinDotView01]];
    [[self pinDots] addObject:[self pinDotView02]];
    [[self pinDots] addObject:[self pinDotView03]];
    [[self pinDots] addObject:[self pinDotView04]];

    _currentState = StateSetupInit;

    self.unlockViewModel = [[DLMPinUnlockScreen alloc] init];
    self.unlockViewModel.pinUnlockScreenDelegate = self;


    // Start monitoring state changes.
    __weak typeof(self) weakSelf = self;
    [RACObserve(self, currentState) subscribeNext:^(NSNumber* state) {
        [weakSelf refreshViewForCurrentState];
    }];



    // Monitor PIN entry text field.
    [[[self pinEntryTextField] rac_textSignal] subscribeNext:^(id next) {
        // Normalise everything first.
        [weakSelf colourPinPlaceholderDotsWithColour:[UIColor clearColor]];

        // Colour the correct number of dots.
        for (NSInteger i = 0; i < [[[weakSelf pinEntryTextField] text] length]; i ++) {
            [[[weakSelf pinDots] objectAtIndex:i] setBackgroundColor:[UIColor blackColor]];
        }
        if ([[[weakSelf pinEntryTextField] text] length] == 4) {
            // Debug.
            DDLogInfo(@"Note. PIN entered. Checking...");

            if (![weakSelf pinEntryTextFieldIsFrozen]) {
                if(weakSelf.currentState == StateSetupInit)
                    [weakSelf verifyPinAndUpdateStateForPin:[[weakSelf pinEntryTextField] text]];
                else if(weakSelf.currentState == StateSetupConfirmPin)
                    [weakSelf tempStorePinAndUpdateStateForPin:weakSelf.pinEntryTextField.text];
                else if(weakSelf.currentState == StateSetupConfirmPinOk)
                    [weakSelf verifyNewPinAndUpdateStateForPin:[[weakSelf pinEntryTextField] text]];

            }

            else {
                DDLogInfo(@"Note. Ignoring check as PIN text field has been locked.");
            }
        }
    }];

    
    [self updatePinBox:self.pinDotBoxView01  withBackgroundColor:[UIColor whiteColor]];
    [self updatePinBox:self.pinDotBoxView02  withBackgroundColor:[UIColor whiteColor]];
    [self updatePinBox:self.pinDotBoxView03  withBackgroundColor:[UIColor whiteColor]];
    [self updatePinBox:self.pinDotBoxView04  withBackgroundColor:[UIColor whiteColor]];
    
    [self trackOmnitureForPage:OMNIPAGE_SETTINGS_CURRENT_PIN];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appplicationDidBecomActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
}


#pragma mark - Public Class Methods

+ (BTUpdatePINViewController *)getUpdatePINViewController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTUpdatePINViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTUpdatePINViewController"];
    return vc;

}



#pragma mark - Private Methods

- (void)updatePinBox:(UIView *)view withBackgroundColor:(UIColor*)color
{
    view.backgroundColor = color;
}

- (void) refreshViewForCurrentState {
    // Setup - initial stage.
    if (_currentState == StateSetupInit) {
        [self setPinEntryTextFieldIsFrozen:false];

        [self backgroundColourPinDotContainersWithColour:[BrandColours colourBtNeutral30]];
        [self colourPinDotContainersWithColour:[UIColor grayColor]];
        [self colourPinPlaceholderDotsWithColour:[UIColor clearColor]];



    }
    else if (_currentState == StateSetupConfirmPin)
    {
        [self backgroundColourPinDotContainersWithColour:[BrandColours colourBtNeutral30]];
        [self colourPinDotContainersWithColour:[UIColor grayColor]];
        [self colourPinPlaceholderDotsWithColour:[UIColor clearColor]];
        _pinEntryTextField.text = @"";

    }

}



//change the background color and assign border width to the PIN dots
- (void) backgroundColourPinDotContainersWithColour : (UIColor*) colour {
    for (UIView* tmpDot in [self pinDotContainers]) {
        [[tmpDot layer] setBorderWidth:1.0];
        [tmpDot setBackgroundColor:colour];
    }
}



//Change the color of the PIN containers
- (void) colourPinDotContainersWithColour : (UIColor*) colour {
    for (UIView* tmpDot in [self pinDotContainers]) {
        [[tmpDot layer] setBorderWidth:1.0];
        [[tmpDot layer] setBorderColor:[colour CGColor]];
    }
}


//change the color of PIN dots
- (void) colourPinPlaceholderDotsWithColour : (UIColor*) colour {
    for (UIView* tmpDot in [self pinDots]) {
        [tmpDot setBackgroundColor:colour];
    }
}



- (void) verifyPinAndUpdateStateForPin:(NSString*) pin {

    if([pin isEqualToString:[self.unlockViewModel pinForCurrentlyLoggedInUser]])
    {
        [self colourPinDotContainersWithColour:[BrandColours colourMyBtGreen]];
        _currentState = StateSetupConfirmPin;
        _pinStatusLabel.text = @"PIN Confirmed";
        [self performSelector:@selector(updatePinViewWithCurrentState) withObject:self afterDelay:1.5];

    }
    else
    {
         self.dontMatchWarningLabel.hidden = NO;
         self.pinErrorView.hidden = NO;

        [self colourPinDotContainersWithColour:[BrandColours colourMyBtRed]];

        [[self pinDotView01] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{}];
        [[self pinDotView02] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{}];
        [[self pinDotView03] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{}];

        [[self pinDotView04] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, kStateSwitchDelay * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {

self.dontMatchWarningLabel.hidden = YES;
self.pinErrorView.hidden = YES;
                [self setCurrentState:StateSetupInit];
                _pinEntryTextField.text = @"";
            });
        }];

    }

}

- (void)verifyNewPinAndUpdateStateForPin:(NSString *)pin
{

    if([[pin lowercaseString] isEqualToString:[_tmpPinForConfirmation lowercaseString]])
    {
          _pinStatusLabel.text = @"PIN Confirmed";
        [self colourPinDotContainersWithColour:[BrandColours colourMyBtGreen]];
        [self performSelector:@selector(updateNewPIN) withObject:self afterDelay:1.5];
    }
    else
    {
        [self colourPinDotContainersWithColour:[BrandColours colourMyBtRed]];
        [[self pinDotView01] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{}];
        [[self pinDotView02] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{}];
        [[self pinDotView03] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{}];
        [[self pinDotView04] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, kStateSwitchDelay * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                _pinEntryTextField.text = @"";
                self.backButton.hidden = true;
                self.dontMatchWarningLabel.hidden = NO;
                self.pinErrorView.hidden = NO;
                _currentState = StateSetupConfirmPin;
                [self performSelector:@selector(updatePinViewWithCurrentState) withObject:self afterDelay:1.5];
            });
        }];

    }


}

- (void)tempStorePinAndUpdateStateForPin:(NSString*) pin {

    [self setTmpPinForConfirmation:_pinEntryTextField.text];
    _currentState = StateSetupConfirmPinOk;

    [self performSelector:@selector(updatePinViewWithCurrentState) withObject:self afterDelay:0.5];
}


- (void)updatePinViewWithCurrentState
{
    [self colourPinDotContainersWithColour:[UIColor grayColor]];

    if(_currentState == StateSetupInit)
    {
        [self colourPinPlaceholderDotsWithColour:[UIColor clearColor]];
        _pinStatusLabel.text = @"Enter your current PIN";
        _pinEntryTextField.text = @"";
        self.dontMatchWarningLabel.hidden = YES;
        self.pinErrorView.hidden = YES;

    }
    else if (_currentState == StateSetupConfirmPin)
    {
        _pinStatusLabel.text = @"Choose a new PIN";
        _pinEntryTextField.text = @"";
        self.dontMatchWarningLabel.hidden = YES;
        self.pinErrorView.hidden = YES;
        [self colourPinPlaceholderDotsWithColour:[UIColor clearColor]];
        [self trackOmnitureForPage:OMNIPAGE_SETTINGS_NEW_PIN];


    }
    else if(_currentState == StateSetupConfirmPinOk)
    {
        _pinStatusLabel.text = @"Confirm new PIN";
        _pinEntryTextField.text = @"";
        self.backButton.hidden = NO;
        [self colourPinPlaceholderDotsWithColour:[UIColor clearColor]];
        self.dontMatchWarningLabel.hidden = YES;
        self.pinErrorView.hidden = YES;
        [self trackOmnitureForPage:OMNIPAGE_SETTINGS_CONFIRM_NEW_PIN];
    }
}


- (void)updateNewPIN
{
    if([self.delegate respondsToSelector:@selector(updatePINViewController:userHasSuccesfullyUpdateThePINWithPIN:)])
    {
        [self.delegate updatePINViewController:self userHasSuccesfullyUpdateThePINWithPIN:_tmpPinForConfirmation];

        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)trackOmnitureForPage:(NSString *)page
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:page withContextInfo:data];
}

#pragma mark - Text field delegate methods.

- (BOOL) textField : (UITextField*) textField shouldChangeCharactersInRange : (NSRange) range replacementString : (NSString*) string {
    if ([self pinEntryTextFieldIsFrozen]) {
        return false;
    } else {
        NSUInteger pinLength = 4;

        NSUInteger oldLength = [[textField text] length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= pinLength || returnKey;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if(!self.separatorViewForKeyboard)
        self.separatorViewForKeyboard = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 1.0 / [UIScreen mainScreen].scale)];
    
    self.separatorViewForKeyboard.frame =CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 1.0 / [UIScreen mainScreen].scale);
    self.separatorViewForKeyboard.backgroundColor = [UIColor lightGrayColor];
    textField.inputAccessoryView =  self.separatorViewForKeyboard;
    self.separatorViewForKeyboard.hidden = NO;
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    self.separatorViewForKeyboard.hidden = YES;
    return YES;
}

#pragma mark - Action Method
- (IBAction)backButtonAction:(id)sender {

    _currentState = StateSetupConfirmPin;
    [self updatePinViewWithCurrentState];
    self.backButton.hidden = YES;
}


- (void)closeButtonPressed:(id)sender{

    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Application active status notification
- (void)appplicationDidBecomActive: (NSNotification *)notification {
    self.separatorViewForKeyboard.hidden = NO;
    [[self pinEntryTextField] becomeFirstResponder];
}


#pragma mark - DLMPinUnlockScreenDelegate Methods

- (void)loggedInUserSuccessfullyAuthenticatedOnPinUnlockScreen:(DLMPinUnlockScreen *)screen;
{

}

- (void)pinUnlockScreen:(DLMPinUnlockScreen *)screen loginAttemptFailedForLoggedInUserWithWebServiceError:(NLWebServiceError *)error
{
    
}
@end
