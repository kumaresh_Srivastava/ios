//
//  BTSecurityQuestionsAndAnswerViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 17/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseViewController.h"

@class BTSecurityQuestionsAndAnswerViewController;

@protocol BTSecurityQuestionsAndAnswerViewControllerDelegate <NSObject>

- (void)successfullyUpdatedSecurityQuestionOnSecurityQuestionsAndAnswerViewController:(BTSecurityQuestionsAndAnswerViewController *)securityQuestionsAndAnswerViewController;

@end

@interface BTSecurityQuestionsAndAnswerViewController : BTBaseViewController {
    
}

@property (nonatomic, assign) BOOL isScreenRelatedToInterceptorModule;
@property (nonatomic, weak) id<BTSecurityQuestionsAndAnswerViewControllerDelegate> delegate;
@property (strong,nonatomic) NSString *cugKeyFromSignInScreen;

@end
