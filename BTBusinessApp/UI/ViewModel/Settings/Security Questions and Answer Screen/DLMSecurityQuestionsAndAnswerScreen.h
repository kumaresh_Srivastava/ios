//
//  DLMSecurityQuestionsAndAnswerScreen.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 19/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"

@class DLMSecurityQuestionsAndAnswerScreen;
@class NLWebServiceError;

@protocol DLMSecurityQuestionsAndAnswerScreenDelegate <NSObject>

- (void)successfullyFetchedSecurityQuestionsOnSecurityQuestionsAndAnswerScreen:(DLMSecurityQuestionsAndAnswerScreen *)securityQuestionsAndAnswerScreen;

- (void)updateSuccessfullyFinishedBySecurityQuestionsAndAnswerScreen:(DLMSecurityQuestionsAndAnswerScreen *)securityQuestionsAndAnswerScreen;

- (void)securityQuestionsAndAnswerScreen:(DLMSecurityQuestionsAndAnswerScreen *)securityQuestionsAndAnswerScreen failedToSubmitUpdatedProfileDetailsWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface DLMSecurityQuestionsAndAnswerScreen : DLMObject {
    
}

@property (nonatomic, copy) NSArray *arrayOfSecurityQuestions;
@property (nonatomic, weak) id <DLMSecurityQuestionsAndAnswerScreenDelegate> securityQuestionAnswerDelegate;

- (void)fetchSecurityQuestions;
- (void)submitAndCreateProfileDetailsModelWithSecurityQuestion:(NSString *)securityQuestion securityAnswer:(NSString *)securityAnswer andPassword:(NSString *)password;

@end
