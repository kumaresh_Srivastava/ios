//
//  BTSecurityQuestionsAndAnswerViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 17/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTSecurityQuestionsAndAnswerViewController.h"
#import "BTTextWithBackgroundViewTableViewCell.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "DLMSecurityQuestionsAndAnswerScreen.h"
#import "AppConstants.h"
#import "BTNeedHelpViewController.h"
#import "BTCustomInputAlertView.h"
#import "AppConstants.h"
#import "BTCreateNewPasswordViewController.h"
#import "OmnitureManager.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "AppManager.h"
#import "NLWebServiceError.h"
#import "UITextField+BTTextFieldProperties.h"
#import "UIButton+BTButtonProperties.h"

@interface BTSecurityQuestionsAndAnswerViewController ()<UITableViewDelegate, UITableViewDataSource, BTTextWithBackgroundViewTableViewCellDelegate, DLMSecurityQuestionsAndAnswerScreenDelegate, UITextFieldDelegate,BTCustomInputAlertViewDelegate, BTCreateNewPasswordViewControllerDelegate> {

    NSInteger _selectedIndexOfSecurityQuestion;
    BTCustomInputAlertView *_customlAlertView;
    BOOL _isAnswerMatched;
    BOOL _isDeviceLessThaniPhone5;
    BOOL _isKeyboardAppered;
}

@property (nonatomic, readwrite) DLMSecurityQuestionsAndAnswerScreen *viewModel;
@property(nonatomic, assign) BOOL isUserSelectedSecurityQuestion;

@property (weak, nonatomic) IBOutlet UIButton *toolTipButton;
@property (weak, nonatomic) IBOutlet UILabel *securityQuestionLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerLabel;
@property (weak, nonatomic) IBOutlet UILabel *retypeAnswerLabel;

@property (weak, nonatomic) IBOutlet UITextField *answerInputTextField;
@property (weak, nonatomic) IBOutlet UITextField *retypeAnswerInputTextField;

@property (weak, nonatomic) IBOutlet UITableView *securityQuestionsTableView;

@property (weak, nonatomic) IBOutlet UIImageView *answerInputBackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *retypeAnswerInputBackgroundView;

@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@property (weak, nonatomic) IBOutlet UIButton *disabledNextButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *disabledNextButtonContraint;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end

@implementation BTSecurityQuestionsAndAnswerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    _viewModel = [[DLMSecurityQuestionsAndAnswerScreen alloc] init];
    _viewModel.securityQuestionAnswerDelegate = self;


    //Security questions table view
    self.securityQuestionsTableView.delegate = self;
    self.securityQuestionsTableView.dataSource = self;
    self.securityQuestionsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.securityQuestionsTableView.backgroundColor = [UIColor clearColor];
    self.securityQuestionsTableView.estimatedRowHeight = 70;
    self.securityQuestionsTableView.rowHeight = UITableViewAutomaticDimension;

    self.answerInputTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.retypeAnswerInputTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.retypeAnswerInputTextField.delegate = self;
    self.answerInputTextField.delegate = self;
    
    [self.answerInputTextField getBTTextFieldStyle];
    [self.retypeAnswerInputTextField getBTTextFieldStyle];
    
       __weak typeof(self) weakSelf = self;
    
    // Order reference textfield signal.
    RACSignal *answerSignal = [[[self answerInputTextField] rac_textSignal] map:^id (NSString* answerString) {
        
        NSString *aString = [weakSelf simplifiedStringFromString:answerString];

        NSString *qString = [weakSelf simplifiedStringFromString:[weakSelf.viewModel.arrayOfSecurityQuestions objectAtIndex:[weakSelf getSelectedIndexOfSecurityQuestion]]];
                
        // Order reference textfield Validation.
        BOOL isValid = NO;
        if ([aString length] > 3 && ![aString isEqualToString:qString]) {
            isValid = YES;
            NSArray *strParts = [answerString componentsSeparatedByString:@" "];
            for (NSString *part in strParts) {
                NSString *simplePart = [self simplifiedStringFromString:part];
                if ([qString containsString:simplePart]) {
                    isValid = NO;
                }
            }
        }
        
        aString = nil;
        qString = nil;
        
        return [NSNumber numberWithBool:isValid];
    }];

    
    // Order Id is required field.
    RACSignal *shouldEnableTrackOrderButtonSignal = [RACSignal combineLatest:@[answerSignal] reduce:^id (NSNumber *answerIsValid) {
        return @([answerIsValid boolValue]);
    }].distinctUntilChanged;

    // Enable / disable button as appropriate.
 
    [shouldEnableTrackOrderButtonSignal subscribeNext:^(NSNumber* shouldEnable) {
        if(!weakSelf.isScreenRelatedToInterceptorModule)
            [weakSelf showSaveButton:[shouldEnable boolValue]];
    }];

    //Fetch security questions from view model;
    [_viewModel fetchSecurityQuestions];

    [self initialUIRelatedSetup];
    [self updateViewWithInterceptorBehaviour];

    [_securityQuestionsTableView setScrollEnabled:NO];

    _disabledNextButton.backgroundColor = [BrandColours colourBtNeutral60];

    [_nextButton getBTButtonStyle];
    _nextButton.enabled = NO;
    //_nextButton.backgroundColor = [BrandColours colourBtNeutral60];

    if(_isScreenRelatedToInterceptorModule)
    {
        _securityQuestionLabel.text = @"Question";

        //(Sal)Navigation bar back button
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.toolTipButton.hidden = YES;
        _disabledNextButton.hidden = YES;
    }
    else
    {
        _securityQuestionLabel.text = @"Security question";
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed:)];
        _disabledNextButton.hidden = YES;

    }


    if([UIScreen mainScreen].bounds.size.height > 570)
    {
        _disabledNextButtonContraint.constant = - 68;
    }
    else
    {
        _disabledNextButtonContraint.constant = - 48;
    }



    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHandler:)];
    [self.containerView addGestureRecognizer:tapRecognizer];



}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setObject:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:OMNIPAGE_SETTINGS_CHANGE_SECURE_QUESTION withContextInfo:data];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [_customlAlertView removeFromSuperview];
}

- (NSInteger)getSelectedIndexOfSecurityQuestion
{
    return _selectedIndexOfSecurityQuestion;
}

- (NSString*)simplifiedStringFromString:(NSString*)string
{
    // convenince method for complaring similar strings
    NSString *newString = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    newString = [newString stringByReplacingOccurrencesOfString:@" " withString:@""];
    newString = [newString stringByReplacingOccurrencesOfString:@"'" withString:@""];
    newString = [newString stringByReplacingOccurrencesOfString:@"?" withString:@""];
    return [newString lowercaseString];
}

- (void)setIsUserSelectedSecurityQuestion:(BOOL)isUserSelectedSecurityQuestion{

    _isUserSelectedSecurityQuestion = isUserSelectedSecurityQuestion;

    if(_isUserSelectedSecurityQuestion && self.isScreenRelatedToInterceptorModule){

        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Back_Arrow"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed:)];

        self.navigationItem.leftBarButtonItem.imageInsets = UIEdgeInsetsMake(2,
                                                                             -5,
                                                                             0,
                                                                             0);
    }
    else if(self.isScreenRelatedToInterceptorModule){

        self.navigationItem.leftBarButtonItem = nil;
    }


}


#pragma mark - UI related methods
- (void)initialUIRelatedSetup {

    if(_isScreenRelatedToInterceptorModule)
    {
        self.title = @"Set a new security question";
        self.navigationItem.hidesBackButton = YES;

    }
    else
    {
        self.title = @"Security question";




    }

    [_answerLabel setHidden:YES];
    [_answerInputBackgroundView setHidden:YES];
    [_answerInputTextField setHidden:YES];

    self.navigationItem.leftBarButtonItem = nil;

}





- (void)updateViewWithInterceptorBehaviour {
    if (!_isScreenRelatedToInterceptorModule) {

        [_nextButton removeFromSuperview];
        [_retypeAnswerLabel removeFromSuperview];
        [_retypeAnswerInputTextField removeFromSuperview];
        [_retypeAnswerInputBackgroundView removeFromSuperview];
    }
}

#pragma mark - Table view delegate methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_isUserSelectedSecurityQuestion) {
        return 1;
    }
    else
    {
        return _viewModel.arrayOfSecurityQuestions.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!_isUserSelectedSecurityQuestion) {


        BTTextWithBackgroundViewTableViewCell *cell = [self.securityQuestionsTableView dequeueReusableCellWithIdentifier:@"BTTextWithBackgroundViewTableViewCell" forIndexPath:indexPath];
        cell.delegate = self;
        [cell removeSelection];

        cell.cellTextLabel.text = [_viewModel.arrayOfSecurityQuestions objectAtIndex:indexPath.row];
        return cell;
    }
    else
    {
        BTTextWithBackgroundViewTableViewCell *cell = [self.securityQuestionsTableView dequeueReusableCellWithIdentifier:@"BTTextWithBackgroundViewTableViewCell" forIndexPath:indexPath];
        cell.cellTextLabel.text = [_viewModel.arrayOfSecurityQuestions objectAtIndex:_selectedIndexOfSecurityQuestion];
        [cell updateCellViewOnSelection];
        return cell;
    }
}

#pragma mark - Table view datasource methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //BTTextWithBackgroundViewTableViewCell *cell = [self.securityQuestionsTableView cellForRowAtIndexPath:indexPath];
    //[cell updateCellViewOnSelection];

    if(_isUserSelectedSecurityQuestion)
    {
        _selectedIndexOfSecurityQuestion = 99;
        self.isUserSelectedSecurityQuestion = NO;
        _disabledNextButton.hidden = YES;
    }
    else
    {
        _selectedIndexOfSecurityQuestion = indexPath.row;
        self.isUserSelectedSecurityQuestion = YES;
        _disabledNextButton.hidden = YES;
        //_nextButton.backgroundColor = [BrandColours colourBtNeutral60];
    }



    [self updateViewOnSecurityQuestionSelection];
    [self.view endEditing:YES];
}

#pragma mark - Table view cell delegate methods
- (void)updatedSuccessfullyOnSelectionOnTextWithBackgroundViewTableViewCell:(BTTextWithBackgroundViewTableViewCell *)textWithBackgroundViewTableViewCell
{
    NSIndexPath *indexPath = [self.securityQuestionsTableView indexPathForCell:textWithBackgroundViewTableViewCell];
    _selectedIndexOfSecurityQuestion = indexPath.row;
    self.isUserSelectedSecurityQuestion = YES;

    [_securityQuestionsTableView reloadData];

    [_answerLabel setHidden:NO];
    [_answerInputTextField setHidden:NO];
    [_answerInputBackgroundView setHidden:NO];
}

#pragma mark - Private helper methods

- (void)doValidationOnPassword:(NSString *)password
{
    if ([password length] > 0) {
        
        if (![AppManager isInternetConnectionAvailable]) {
            [_customlAlertView showErrorOnCustomInputWithWithErrro:kNoConnectionMessage];
            [AppManager trackNoInternetErrorOnPage:OMNIPAGE_SETTINGS_CHANGE_SECURE_QUESTION];
        }
        else
        {
            [_customlAlertView removeErrorMeesageFromCustomInputAlertView];
            
            NSString *securityQuestion = [_viewModel.arrayOfSecurityQuestions objectAtIndex:_selectedIndexOfSecurityQuestion];
            NSString *securityAnswer = [AppManager stringByTrimmingWhitespaceInString:[self.answerInputTextField text]];
            
            //  self.networkRequestInProgress = YES;
            [_customlAlertView startLoading];
            
            [self.viewModel submitAndCreateProfileDetailsModelWithSecurityQuestion:securityQuestion securityAnswer:securityAnswer andPassword:password];
        }
    }
    else
    {
        //  [self displayEnterPasswordViewWithErrorMessageHidden:NO andMessage:@"Please enter your password"];
        [_customlAlertView showErrorOnCustomInputWithWithErrro:@"Please enter your password"];
    }
}


- (void)updateViewOnSecurityQuestionSelection {

    [_securityQuestionsTableView reloadData];
    [_securityQuestionsTableView setScrollEnabled:NO];

    _answerInputTextField.text = @"";
    _retypeAnswerInputTextField.text = @"";

    if(_isUserSelectedSecurityQuestion)
    {
        [self trackSecurityAnswerScreen];
        [_answerLabel setHidden:NO];
        [_answerInputTextField setHidden:NO];
        [_answerInputBackgroundView setHidden:NO];

        if(!_isScreenRelatedToInterceptorModule)
            _disabledNextButton.hidden = YES;

    }
    else
    {
        [_answerLabel setHidden:YES];
        [_answerInputBackgroundView setHidden:YES];
        [_answerInputTextField setHidden:YES];

        if(!_isScreenRelatedToInterceptorModule)
            _disabledNextButton.hidden = YES;
    }

}


- (void)trackSecurityAnswerScreen
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:OMNIPAGE_SETTINGS_CHANGE_SECURE_ANSWER withContextInfo:data];
}

- (void)updateToDefaultState{

    _selectedIndexOfSecurityQuestion = 99;
    self.isUserSelectedSecurityQuestion = NO;
    _disabledNextButton.hidden = NO;
    [self updateViewOnSecurityQuestionSelection];
    [self.view endEditing:YES];
}

- (void)showSaveButton:(BOOL)isShow {
    if (isShow) {

        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveButtonPressed:)];
    } else {
        self.navigationItem.rightBarButtonItem = nil;
    }
}

- (void)showErrorAlertWithMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Message" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Action methods
- (void)cancelButtonPressed:(id)sender {

    [self.answerInputTextField resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)saveButtonPressed:(id)sender {

    [self.answerInputTextField resignFirstResponder];
    [self displayEnterPasswordViewWithErrorMessageHidden:YES andMessage:nil];
}

- (void)backButtonPressed:(id)sender{

    [self updateToDefaultState];
}

- (IBAction)nextButtonAction:(id)sender {
    
    if(_isAnswerMatched)
    {
        
        NSString *question  = [_viewModel.arrayOfSecurityQuestions objectAtIndex:_selectedIndexOfSecurityQuestion];
        NSString *answer = _retypeAnswerInputTextField.text;
        
        BTCreateNewPasswordViewController *newPasswordViewController = [BTCreateNewPasswordViewController getBTCreateNewPasswordViewController];
        newPasswordViewController.delegate = self;
        newPasswordViewController.securityQuestion = question;
        newPasswordViewController.securityAnswer = answer;
        newPasswordViewController.cugKeyFromSignInScreen = self.cugKeyFromSignInScreen;
        [self.navigationController  pushViewController:newPasswordViewController animated:YES];
        
    }
    
}

- (IBAction)diabledNextButtonAction:(id)sender {
    
}

- (IBAction)userClickedToolTipButton:(id)sender
{
    
    BTNeedHelpViewController *helpViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SecurityQuestionsHelpViewController"];
    [self presentViewController:helpViewController animated:YES completion:nil];
}


#pragma mark - DLMSecurityQuestionsAndAnswerScreenDelegate methods
- (void)successfullyFetchedSecurityQuestionsOnSecurityQuestionsAndAnswerScreen:(DLMSecurityQuestionsAndAnswerScreen *)securityQuestionsAndAnswerScreen
{
    [self.securityQuestionsTableView reloadData];
}

#pragma mark - DLMSecurityQuestionsAndAnswerScreenDelegate methods
- (void)updateSuccessfullyFinishedBySecurityQuestionsAndAnswerScreen:(DLMSecurityQuestionsAndAnswerScreen *)securityQuestionsAndAnswerScreen
{
    //self.networkRequestInProgress = NO;
    [_customlAlertView stopLoading];
    [_customlAlertView deRegisterKeyboardNotification];
    [_customlAlertView removeFromSuperview];
    [self.delegate successfullyUpdatedSecurityQuestionOnSecurityQuestionsAndAnswerViewController:self];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)securityQuestionsAndAnswerScreen:(DLMSecurityQuestionsAndAnswerScreen *)securityQuestionsAndAnswerScreen failedToSubmitUpdatedProfileDetailsWithWebServiceError:(NLWebServiceError *)webServiceError
{
    //self.networkRequestInProgress = NO;
    BOOL errorHandled = [self attemptVordelProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
    
    if (errorHandled == NO && [webServiceError.error.domain isEqualToString:BTNetworkErrorDomain])
    {
        errorHandled = YES;
        
        switch (webServiceError.error.code)
        {
            case BTNetworkErrorCodeVordelParameterInvalid:
            {
                [_customlAlertView stopLoading];
                [_customlAlertView showErrorOnCustomInputWithWithErrro:@"Please enter a valid password."];
                [OmnitureManager trackError:OMNIERROR_SECURITY_QUESTION_CHANGE_PASSWORD_INCORRECT onPageWithName:OMNIPAGE_SETTINGS_CHANGE_SECURE_QUESTION contextInfo:[AppManager getOmniDictionary]];
                break;
            }
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }
    else
    {
        [_customlAlertView stopLoading];
        [_customlAlertView removeFromSuperview];
       
    }
    
    
    if(errorHandled == NO)
    {
        [_customlAlertView stopLoading];
        [_customlAlertView showErrorOnCustomInputWithWithErrro:kDefaultErrorMessage];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_SETTINGS_CHANGE_SECURE_QUESTION];
    }
    
}

- (void)displayEnterPasswordViewWithErrorMessageHidden:(BOOL)hideErrorMessage andMessage:(NSString *)errorMessage
{

    _customlAlertView = [[[NSBundle mainBundle] loadNibNamed:@"BTCustomInputAlertView" owner:nil options:nil] firstObject];
    _customlAlertView.frame = CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    _customlAlertView.delegate = self;

    [_customlAlertView updateCustomAlertViewWithTitle:@"Enter your password to save changes"  andMessage:nil];
    [_customlAlertView updateTextFieldPlaceholderText:@"Password" andIsSecureEntry:YES andNeedToShowRightView:YES];
    [_customlAlertView updateLoadingMessage:@"Validating password"];

    if(_customlAlertView)
        [_customlAlertView showKeyboard];
    
    [[[UIApplication sharedApplication] keyWindow] addSubview:_customlAlertView];

}


#pragma mark - UITextFieldDelegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];

    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [textField updateWithDisabledState];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [textField updateWithEnabledState];
}

- (void)showTextButtonPressed:(id)sender{

    UIButton *button = (UIButton *)sender;
    button.selected = !button.selected;

    ((UITextField *)[sender superview]).secureTextEntry = !((UITextField *)[sender superview]).isSecureTextEntry;

}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *str  = [textField.text stringByReplacingCharactersInRange:range withString:string];

    if(textField == _retypeAnswerInputTextField)
    {
        if([str isEqualToString:_answerInputTextField.text] && _answerInputTextField.text.length > 0)
        {
            _nextButton.enabled = YES;
            //_nextButton.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
            _isAnswerMatched = YES;
        }
        else
        {
            _nextButton.enabled = NO;
            //_nextButton.backgroundColor = [BrandColours colourBtNeutral60];
            _isAnswerMatched = NO;
        }
    }
    else
    {
        if([str isEqualToString:_retypeAnswerInputTextField.text] && _retypeAnswerInputTextField.text.length > 0)
        {
            _nextButton.enabled = YES;
            //_nextButton.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
            _isAnswerMatched = YES;
        }
        else
        {
            _nextButton.enabled = NO;
            //_nextButton.backgroundColor = [BrandColours colourBtNeutral60];
            _isAnswerMatched = NO;
        }

    }
    
    NSString *question = [_viewModel.arrayOfSecurityQuestions objectAtIndex:_selectedIndexOfSecurityQuestion];
    if (str.length < 4 || [str isEqualToString:question]) {
        _nextButton.enabled = NO;
        _isAnswerMatched = NO;
    }

    return YES;
}


#pragma mark- BTCreateNewPasswordViewControllerDelegate Methods
- (void)userPressedBackButtonOnBTCreateNewPasswordViewController:(BTCreateNewPasswordViewController *)controller{

    [self updateToDefaultState];

}


#pragma mark - BTCustom Alert View Delegate

- (void)userDidPressCancelOnCustomInputAlertView:(BTCustomInputAlertView *)alertView
{
    [_customlAlertView deRegisterKeyboardNotification];
    [_customlAlertView removeFromSuperview];
    _customlAlertView = nil;
}

- (void)userDidPressConfirmOnCustomInputAlertView:(BTCustomInputAlertView *)alertView withPassword:(NSString *)passwordText
{
    NSString *password = [AppManager stringByTrimmingWhitespaceInString:passwordText];
    [self doValidationOnPassword:password];
    
}


#pragma mark - UITouch Methods

- (void)tapHandler:(UITapGestureRecognizer*)sender {
    
    [self.retypeAnswerInputTextField resignFirstResponder];
    [self.answerInputTextField resignFirstResponder];
    
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.retypeAnswerInputTextField resignFirstResponder];
    [self.answerInputTextField resignFirstResponder];
    
}


@end
