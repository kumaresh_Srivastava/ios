//
//  DLMSecurityQuestionsAndAnswerScreen.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 19/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMSecurityQuestionsAndAnswerScreen.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "CDAuthenticationToken.h"
#import "NLQueryUpdateProfileWebService.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "AppManager.h"

@interface DLMSecurityQuestionsAndAnswerScreen () <NLQueryUpdateProfileWebServiceDelegate> {
    
}

@property (nonatomic) NLQueryUpdateProfileWebService *queryUpdateProfileDetailsWebService;

@end

@implementation DLMSecurityQuestionsAndAnswerScreen

#pragma mark - Public methods
- (void)fetchSecurityQuestions
{
    _arrayOfSecurityQuestions = [AppManager getBTUserSecurityQuestions];
    
    [self.securityQuestionAnswerDelegate successfullyFetchedSecurityQuestionsOnSecurityQuestionsAndAnswerScreen:self];
}

- (void)submitAndCreateProfileDetailsModelWithSecurityQuestion:(NSString *)securityQuestion securityAnswer:(NSString *)securityAnswer andPassword:(NSString *)password
{
    NSMutableDictionary *updatedDictionary = [NSMutableDictionary dictionary];
    
    
//    [updatedDictionary setValue:securityQuestion forKey:@"SecretQuestion"];
//    [updatedDictionary setValue:securityAnswer forKey:@"SecretAnswer"];
//    [updatedDictionary setValue:password forKey:@"Password"];
    
    [updatedDictionary setValue:[AppManager getRSAEncryptedString:securityQuestion] forKey:@"SecretQuestion"];
    [updatedDictionary setValue:[AppManager getRSAEncryptedString:securityAnswer] forKey:@"SecretAnswer"];
    [updatedDictionary setValue:[AppManager getRSAEncryptedString:password] forKey:@"Password"];
    [updatedDictionary setValue:[NSNumber numberWithBool:true] forKey:@"IsSecurityQuestionChanged"];
    
    // Get the currently logged in User
    CDUser *user = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    
    //(lp) Send the groupkey with text "Key"
    [updatedDictionary setValue:user.currentSelectedCug.groupKey forKey:@"Key"];
    
    self.queryUpdateProfileDetailsWebService = [[NLQueryUpdateProfileWebService alloc] initWithUpdatedProfileDetailsDictionary:updatedDictionary];
    self.queryUpdateProfileDetailsWebService.queryUpdateProfileWebServiceDelegate = self;
    [self.queryUpdateProfileDetailsWebService resume];
    
}


#pragma mark - NLQueryUpdateProfileWebService methods
- (void)queryUpdateProfileDetailsSuccesfullyFinishedWithQueryUpdateProfileWebService:(NLQueryUpdateProfileWebService *)webService
{
    if(webService == self.queryUpdateProfileDetailsWebService)
    {
        [self.securityQuestionAnswerDelegate updateSuccessfullyFinishedBySecurityQuestionsAndAnswerScreen:self];
        
        self.queryUpdateProfileDetailsWebService.queryUpdateProfileWebServiceDelegate = nil;
        self.queryUpdateProfileDetailsWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLQueryUpdateProfileWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLQueryUpdateProfileWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }

}


-(void)queryUpdateProfileWebService:(NLQueryUpdateProfileWebService *)webService failedToUpdateProfileDetailsWithWebServiceError:(NLWebServiceError *)webServiceError
{
    if(webService == self.queryUpdateProfileDetailsWebService)
    {
        [self.securityQuestionAnswerDelegate securityQuestionsAndAnswerScreen:self failedToSubmitUpdatedProfileDetailsWithWebServiceError:webServiceError];
        
        self.queryUpdateProfileDetailsWebService.queryUpdateProfileWebServiceDelegate = nil;
        self.queryUpdateProfileDetailsWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLQueryUpdateProfileWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLQueryUpdateProfileWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }

}


@end
