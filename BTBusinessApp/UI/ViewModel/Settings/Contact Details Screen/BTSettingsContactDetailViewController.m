//
//  BTSettingsContactDetailViewController.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/17/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTSettingsContactDetailViewController.h"
#import "BTSettingsContactDetailTableViewCell.h"
#import "DLMSettingsContactDetailScreen.h"
#import "BTSettingsEditContactViewController.h"
#import "BTAlertBannerView.h"
#import "OmnitureManager.h"
#import "AppManager.h"


#define kContactDetailCell @"BTSettingsContactDetailTableViewCell"
#define kContactDetailCellID @"BTSettingsContactDetailTableViewCellID"

#define kNavigationBarHeightWithStatusBar 64

@interface BTSettingsContactDetailViewController ()<UITableViewDelegate, UITableViewDataSource, BTSettingsEditContactViewControllerDelegate>{
    
    NSArray *_dataArray;
    DLMSettingsContactDetailScreen *_contactDetailViewModel;
    BTAlertBannerView *_alertBannerView;
    CGFloat originalTableViewTopContraint;
    
}

@property (strong, nonatomic) UITableView *tableView;
@property (assign, nonatomic)  CGFloat tableViewTopConstraint;

@end

@implementation BTSettingsContactDetailViewController




- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Setting for cell auto height
    _tableView = [[UITableView alloc] init];
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
     _tableView.frame = CGRectMake(0, 0,screenSize.width, screenSize.height);
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 80.0;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    //Hidding default separator
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
   
    
    //Discble bounce
    //_tableView.bounces = NO;
    
    //cell nib registration
    UINib *nib = [UINib nibWithNibName:kContactDetailCell bundle:nil];
    [_tableView registerNib:nib forCellReuseIdentifier:kContactDetailCellID];
    
    
    //Edit button on navigation bar
    self.navigationItem.rightBarButtonItem= [self getEditButtonItem];
    
    
    //Screen Title
    self.title = @"Contact details";
    
    
    //View Model
    _contactDetailViewModel = [[DLMSettingsContactDetailScreen alloc] init];
    _dataArray = [NSArray arrayWithArray:[_contactDetailViewModel getContactWrappers]];
    
    
    originalTableViewTopContraint = _tableView.frame.origin.y;
    
    [self trackPageForOmniture];
    
}

- (void)viewDidDisappear:(BOOL)animated{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
}

- (void) setTableFrame{
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    _tableView.frame = CGRectMake(0, 0,screenSize.width, screenSize.height);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChangeContact:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
    
    [self setTableFrame];
    
}

- (void)statusBarOrientationChangeContact:(NSNotification *)notification {
    // handle the interface orientation as needed
    [self setTableFrame];
}

#pragma mark - Class Methods

+ (BTSettingsContactDetailViewController *)getBTSettingsContactDetailViewController{

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    BTSettingsContactDetailViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"BTSettingsContactDetailViewControllerID"];

    return controller;
}


#pragma mark- Helper

- (UIBarButtonItem *)getEditButtonItem{
    
    UIBarButtonItem *editButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(editButtonPressed:)];
    
    return editButtonItem;
}


- (void)trackPageForOmniture
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setObject:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:OMNIPAGE_SETTINGS_CONTACT withContextInfo:data];
}

- (void)showAlertBannerWithMessage:(NSString *)alertMessage{
    
    
    if(!_alertBannerView){
        
        _alertBannerView = [[[NSBundle mainBundle] loadNibNamed:@"BTAlertBannerView" owner:self options:nil] firstObject];
        
        _alertBannerView.frame = CGRectMake(0,
                                            0,
                                            self.view.frame.size.width,
                                            40);
        
        [self.view addSubview:_alertBannerView];
    }

    [_alertBannerView showAnimated:YES];
    
    [_alertBannerView setMessage:alertMessage];
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:kBannerAnimationTime animations:^{
        
        CGRect frame = self.tableView.frame;
        frame.origin.y = _alertBannerView.frame.size.height;
        self.tableView.frame = frame;

        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        [self performSelector:@selector(hideAlertBanner) withObject:nil afterDelay:3.0];
    }];
    
    
}


- (void)hideAlertBanner{
    
    [_alertBannerView hideAnimated:YES];
    
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:kBannerAnimationTime animations:^{
        
        CGRect frame = self.tableView.frame;
        frame.origin.y = 0;
        self.tableView.frame = frame;
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
    }];
    
}



#pragma mark- action method

- (void)editButtonPressed:(id)sender{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",OMNIPAGE_SETTINGS_CONTACT,OMNICLICK_SETTINGS_EDIT] withContextInfo:data];
    
    BTSettingsEditContactViewController *editViewController = [[BTSettingsEditContactViewController alloc] init];
    editViewController.siteContactDict = [_contactDetailViewModel getUserDataDictionary];
    editViewController.editContactViewDelegate = self;
    
    [self.navigationController pushViewController:editViewController animated:YES];
}

#pragma mark- UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [_dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BTSettingsContactDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kContactDetailCellID];
    
    [cell updateWitContactWrapper:[_dataArray objectAtIndex:indexPath.row]];
    return cell;
}



#pragma mark- BTSettingsEditContactViewControllerDelegate Method

- (void)userDetailSucessfullyUpdatedOnBTSettingsEditContactViewController:(BTSettingsEditContactViewController *)viewController{
    

    _dataArray = [NSArray arrayWithArray:[_contactDetailViewModel getContactWrappers]];
    [self.tableView reloadData];
    
    [self showAlertBannerWithMessage:@"Updated successfully"];
    
    [AppManager trackOmnitureKeyTask:OMNI_KEYTASK_SETTINGS_CONTACT forPageName:OMNIPAGE_SETTINGS_CONTACT];
}


@end
