//
//  DLMSettingsContactDetailScreen.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/17/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMSettingsContactDetailScreen.h"
#import "CDUser.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "NSObject+APIResponseCheck.h"

@implementation DLMSettingsContactDetailScreen

#pragma mark - Public methods
- (NSArray *)getContactWrappers{
    
    
    NSMutableArray *contailDetailRowWrappers = [NSMutableArray array];
    CDUser *user = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    
    
    NSString *completeName = @"";
    
    if(user.titleInName && [user.titleInName validAndNotEmptyStringObject]){
        
        completeName = user.titleInName;
    }
    
    if(user.firstName && [user.firstName validAndNotEmptyStringObject]){
        
        completeName = [completeName stringByAppendingString:[NSString stringWithFormat:@" %@", user.firstName]];
    }
    
    if(user.lastName && [user.lastName validAndNotEmptyStringObject]){
        
        completeName = [completeName stringByAppendingString:[NSString stringWithFormat:@" %@", user.lastName]];
    }
    
    
    
    //User Name
    BTContactRowWrapper *nameRowWrapper = [[BTContactRowWrapper alloc] initWithTitle:@"Name" andValue:completeName];
    [contailDetailRowWrappers addObject:nameRowWrapper];
    
    //User Mobile
    BTContactRowWrapper *mobileRowWrapper = [[BTContactRowWrapper alloc] initWithTitle:@"Mobile" andValue:user.mobileNumber];
    [contailDetailRowWrappers addObject:mobileRowWrapper];
    
    
    //User Landline
    BTContactRowWrapper *landlineRowWrapper = [[BTContactRowWrapper alloc] initWithTitle:@"Landline" andValue:user.landlineNumber];
    [contailDetailRowWrappers addObject:landlineRowWrapper];
    
    //User Email
    BTContactRowWrapper *primaryEmailRowWrapper = [[BTContactRowWrapper alloc] initWithTitle:@"Main email" andValue:user.primaryEmailAddress];
    [contailDetailRowWrappers addObject:primaryEmailRowWrapper];
    
    //User Alt Email
    BTContactRowWrapper *altEmailRowWrapper = [[BTContactRowWrapper alloc] initWithTitle:@"Alternative email" andValue:user.alternativeEmailAddress];
    [contailDetailRowWrappers addObject:altEmailRowWrapper];
    
    return contailDetailRowWrappers;
    
}

- (NSDictionary *)getUserDataDictionary{
    
    
    CDUser *user = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    
    NSMutableDictionary *userDataDict = [NSMutableDictionary dictionary];
    [userDataDict setValue:user.titleInName forKey:kSettingsUserTitle];
    
    [userDataDict setValue:user.firstName forKey:kSettingsUserFirstName];
    
    [userDataDict setValue:user.lastName forKey:kSettingsUserLastName];
    
    [userDataDict setValue:user.mobileNumber forKey:kSettingsUserMobile];
    
    [userDataDict setValue:user.landlineNumber forKey:kSettingsUserLandline];
    
    [userDataDict setValue:user.primaryEmailAddress forKey:kSettingsUserPriEmail];
    
    [userDataDict setValue:user.alternativeEmailAddress forKey:kSettingsUserUserAltEmail];
    
    return [NSDictionary dictionaryWithDictionary:userDataDict];
}

@end


@implementation BTContactRowWrapper

#pragma mark - Public methods
- (id)initWithTitle:(NSString *)title andValue:(NSString *)valueString{
    
    self = [super init];
    
    if(self){
        
        _titleString = title;
        _ValueString = valueString;
        
        if(!valueString || ![valueString validAndNotEmptyStringObject]){
            
            _ValueString = @"-";
        }
    }
    
    return self;
}

@end
