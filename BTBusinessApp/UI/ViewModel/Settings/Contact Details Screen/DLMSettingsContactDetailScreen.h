//
//  DLMSettingsContactDetailScreen.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/17/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"

#define kSettingsUserTitle @"Title"
#define kSettingsUserFirstName @"First name"
#define kSettingsUserLastName @"Last name"
#define kSettingsUserMobile @"Mobile"
#define kSettingsUserLandline @"Landline"
#define kSettingsUserPriEmail @"Main email"
#define kSettingsUserUserAltEmail @"Alternative email"

@interface DLMSettingsContactDetailScreen : DLMObject

- (NSArray *)getContactWrappers;
- (NSDictionary *)getUserDataDictionary;

@end

@interface BTContactRowWrapper : NSObject {
    
}

@property(nonatomic, readonly) NSString *titleString;
@property(nonatomic, readonly) NSString *ValueString;

- (id)initWithTitle:(NSString *)title andValue:(NSString *)valueString;


@end
