//
//  BTSettingsEditContactScreen.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/17/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMAddNewContactScreen.h"
#import "DLMSettingsContactDetailScreen.h"

@class DLMSettingsEditContactScreen;
@class NLWebServiceError;

@protocol DLMSettingsEditContactScreenDelegate <NSObject>

- (void)updateSuccessfullyFinishedByEditContactScreen:(DLMSettingsEditContactScreen *)settingsEditContactScreen;

- (void)settingsEditContactScreen:(DLMSettingsEditContactScreen *)settingsEditContactScreen failedToUpdateContactDetailsWithWebServiceError:(NLWebServiceError *)webServiceError;

- (void)successfullyFetchedTitlesOnEditContactScreen:(DLMSettingsEditContactScreen *)editContactScreen;

@end

@interface DLMSettingsEditContactScreen : DLMAddNewContactScreen {
    
}

@property (nonatomic, readonly, copy) NSArray *arrayOfTitles;
@property (nonatomic, weak) id <DLMSettingsEditContactScreenDelegate> settingsEditContactScreenDelegate;

- (void)submitAndCreateContactDetailsModelWithContactDetailsDictionary:(NSDictionary *)updatedContactDetailsDict andPassword:(NSString *)password;

- (void)fetchTitles;

@end
