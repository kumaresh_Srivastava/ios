//
//  BTSettingsEditContactViewController.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/17/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTSettingsEditContactViewController.h"
#import "DLMSettingsEditContactScreen.h"
#import "CustomSpinnerView.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "AppConstants.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "BrandColours.h"
#import "AppManager.h"
#import "DLMSettingsEditContactScreen.h"
#import "NSObject+APIResponseCheck.h"
#import "BTCustomInputAlertView.h"
#import "OmnitureManager.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "NLWebServiceError.h"

@interface BTSettingsEditContactViewController ()<BTAddSiteContactTableViewControllerDelegare, DLMSettingsEditContactScreenDelegate,BTCustomInputAlertViewDelegate> {
    
    NSDictionary *_previousContactDetailDict;
    NSDictionary *_updatedContactDetailsDict;
    NSArray *_titlesArray;
    CustomSpinnerView *_loadingView;
    BTCustomInputAlertView *custolAlertView;
}


@property(nonatomic, assign) BOOL networkRequestInProgress;
@property(atomic, assign) BOOL isDataChanged;

@end

@implementation BTSettingsEditContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    //Navigation Bar Buttons
    self.navigationItem.leftBarButtonItem = [self getCancelButtonItem];
    
    //Screen Title
    self.title = @"Contact details";
    
    self.delegate = self;
    
    [self createLoadingView];
    
    __weak typeof(self) weakSelf = self;
    [RACObserve(self, self.networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        
        
        if (weakSelf.networkRequestInProgress) {
            
            [weakSelf showProgressView];
            
        }
        
        else {
            
            [weakSelf hideProgressView];
        }
        
    }];
    
    self.networkRequestInProgress = NO;
    
    
    
    [RACObserve(self, self.isDataChanged) subscribeNext:^(NSNumber* state) {
        
        
        if (weakSelf.isDataChanged) {
            
            weakSelf.navigationItem.rightBarButtonItem = [weakSelf getSaveButtonItem];
            
        }
        
        else {
            
            weakSelf.navigationItem.rightBarButtonItem = nil;
        }
        
    }];
    
    //self.isDataChanged = NO;
    
    _previousContactDetailDict = self.siteContactDict;
    
    _addNewContactScreenModel = [[DLMSettingsEditContactScreen alloc] initWithContactDict:_previousContactDetailDict];
    [(DLMSettingsEditContactScreen *)_addNewContactScreenModel setSettingsEditContactScreenDelegate:self];
    
    [(DLMSettingsEditContactScreen *)_addNewContactScreenModel fetchTitles];
    
    [self createData];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
   
    [OmnitureManager trackPage:OMNIPAGE_SETTINGS_EDIT_CONTACT withContextInfo:data];
}


#pragma mark- Helper

- (void)setDataChangeStatus{
    
    BOOL isDataChanged = NO;
    
    if(![[_previousContactDetailDict valueForKey:kSettingsUserTitle] isEqualToString:[_updatedContactDetailsDict valueForKey:kSettingsUserTitle]]){
        
        isDataChanged = YES;
    }
    
    
    
    if(!isDataChanged && ![[_previousContactDetailDict valueForKey:kSettingsUserFirstName] isEqualToString:[_updatedContactDetailsDict valueForKey:@"FirstName"]]){
        
        isDataChanged = YES;
    }
    
    
    if(!isDataChanged && ![[_previousContactDetailDict valueForKey:kSettingsUserLastName] isEqualToString:[_updatedContactDetailsDict valueForKey:@"LastName"]]){
        
        isDataChanged = YES;
    }
    
    
    
    if(!isDataChanged && ![[_previousContactDetailDict valueForKey:kSettingsUserMobile] isEqualToString:[_updatedContactDetailsDict valueForKey:@"MobileNumber"]]){
        
        isDataChanged = YES;
    }
    
    
    
    NSString *previousLndNum = [_previousContactDetailDict valueForKey:kSettingsUserLandline];
    NSString *updatedLndNum = [_updatedContactDetailsDict valueForKey:@"LandLineNumber"];
    
    
    if(!isDataChanged){
        
        if(previousLndNum || updatedLndNum){
            
            if(![previousLndNum isEqualToString:updatedLndNum]){
                
                isDataChanged = YES;
            }
        }
        
    }
    
    if(!isDataChanged && ![[_previousContactDetailDict valueForKey:kSettingsUserPriEmail] isEqualToString:[_updatedContactDetailsDict valueForKey:@"PrimaryEmailAddress"]]){
        
        isDataChanged = YES;
    }
    
    
    NSString *previousAltEmail = [_previousContactDetailDict valueForKey:kSettingsUserUserAltEmail];
    NSString *updatedAltEmail = [_updatedContactDetailsDict valueForKey:@"AlternateEmailAddress"];
    
    
    if(!isDataChanged){
        
        if(previousAltEmail || updatedAltEmail){
            
            if(![previousAltEmail isEqualToString:updatedAltEmail]){
                
                isDataChanged = YES;
            }
        }
        
    }
    
    self.isDataChanged = isDataChanged;
    
}


- (void)createData{
    
    
    _dataArray = [_addNewContactScreenModel getTextFieldsModelsArray];
    [self.tableView reloadData];
    
}


- (void)createLoadingView {
    
    if(!_loadingView){
        
        _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        
        _loadingView.frame = CGRectMake(0,
                                        64,
                                        kScreenSize.width,
                                        kScreenSize.height - 64);
        //_loadingView.backgroundColor = [UIColor yellowColor];
        [[[[UIApplication sharedApplication] delegate] window] addSubview:_loadingView];
        
        // [self.navigationController.view addSubview:_loadingView];
    }
    
}


- (void)showProgressView{
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideLoadingItems:NO];
        [_loadingView startAnimatingLoadingIndicatorView];
        [[[[UIApplication sharedApplication] delegate] window] addSubview:_loadingView];
    });
    
}


- (void)hideProgressView{
    
    [self hideLoadingItems:YES];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
    [_loadingView stopAnimatingLoadingIndicatorView];
    [_loadingView removeFromSuperview];
}


- (void)hideLoadingItems:(BOOL)isHide {
    
    [_loadingView setHidden:isHide];
    
}



- (NSDictionary *)getUserDataDictionary{
    
    //Pass entered data to delegate
    NSMutableDictionary *userDataDict = [NSMutableDictionary dictionary];
    
    for(BTTextFiledModel *textFieldModel in _dataArray){
        
        if([textFieldModel.textFieldName isEqualToString:kSettingsUserTitle]){
            
            [userDataDict setValue:textFieldModel.inputText forKey:kSettingsUserTitle];
        }
        else if([textFieldModel.textFieldName isEqualToString:kSettingsUserFirstName]){
            
            [userDataDict setValue:textFieldModel.inputText forKey:@"FirstName"];
        }
        else if([textFieldModel.textFieldName isEqualToString:kSettingsUserLastName]){
            
            [userDataDict setValue:textFieldModel.inputText forKey:@"LastName"];
        }
        else if([textFieldModel.textFieldName isEqualToString:kSettingsUserMobile]){
            
            [userDataDict setValue:textFieldModel.inputText forKey:@"MobileNumber"];
            
        }
        else if([textFieldModel.textFieldName isEqualToString:kSettingsUserLandline]){
            
            [userDataDict setValue:textFieldModel.inputText forKey:@"LandLineNumber"];
        }
        else if([textFieldModel.textFieldName isEqualToString:kSettingsUserPriEmail]){
            
            [userDataDict setValue:textFieldModel.inputText forKey:@"PrimaryEmailAddress"];
        }
        else if([textFieldModel.textFieldName isEqualToString:kSettingsUserUserAltEmail]){
            
            [userDataDict setValue:textFieldModel.inputText forKey:@"AlternateEmailAddress"];
        }
        
    }
    
    return userDataDict;
}

- (UIBarButtonItem *)getCancelButtonItem{
    
    UIBarButtonItem *cancelButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed:)];
    
    return cancelButtonItem;
}


- (UIBarButtonItem *)getSaveButtonItem{
    
    UIBarButtonItem *saveButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonPressed:)];
    
    return saveButtonItem;
}

- (void)updateAndSaveContactDetailsLocally {
    
    // (lp) Save the security question and answer locally.
    [AppDelegate sharedInstance].viewModel.app.loggedInUser.titleInName = [_updatedContactDetailsDict valueForKey:kSettingsUserTitle];
    [AppDelegate sharedInstance].viewModel.app.loggedInUser.firstName = [_updatedContactDetailsDict valueForKey:@"FirstName"];
    [AppDelegate sharedInstance].viewModel.app.loggedInUser.lastName = [_updatedContactDetailsDict valueForKey:@"LastName"];
    [AppDelegate sharedInstance].viewModel.app.loggedInUser.mobileNumber = [_updatedContactDetailsDict valueForKey:@"MobileNumber"];
    [AppDelegate sharedInstance].viewModel.app.loggedInUser.landlineNumber = [_updatedContactDetailsDict valueForKey:@"LandLineNumber"];
    [AppDelegate sharedInstance].viewModel.app.loggedInUser.primaryEmailAddress = [_updatedContactDetailsDict valueForKey:@"PrimaryEmailAddress"];
    [AppDelegate sharedInstance].viewModel.app.loggedInUser.alternativeEmailAddress = [_updatedContactDetailsDict valueForKey:@"AlternateEmailAddress"];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
    
}



- (void)openTitleSelction{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Select Title" preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    for(NSString *title in _titlesArray){
        
        UIAlertAction *titleAction = [UIAlertAction actionWithTitle:title
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  
                                                                  _currectSelectedTexFieldtModel.inputText = title;
                                                                  [self.tableView reloadData];
                                                                  
                                                                  _updatedContactDetailsDict = [self getUserDataDictionary];
                                                                  [self setDataChangeStatus];
                                                                  [alert dismissViewControllerAnimated:YES completion:nil];
                                                              }];
        
        [alert addAction:titleAction];
        
    }
    
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    [alert addAction:cancel];
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = self.view.frame;
    alert.popoverPresentationController.canOverlapSourceViewRect = NO;
    
    [self presentViewController:alert animated:YES completion:^{
        
    }];
}



- (void)showErrorAlertWithMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Message" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}


- (void)displayEnterPasswordViewWithErrorMessageHidden:(BOOL)hideErrorMessage andMessage:(NSString *)errorMessage
{
    custolAlertView = [[[NSBundle mainBundle] loadNibNamed:@"BTCustomInputAlertView" owner:nil options:nil] firstObject];
    custolAlertView.frame = CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    custolAlertView.delegate = self;
    
    [custolAlertView updateCustomAlertViewWithTitle:@"Enter your password to save changes"  andMessage:nil];
    [custolAlertView updateTextFieldPlaceholderText:@"Password" andIsSecureEntry:YES andNeedToShowRightView:YES];
    [custolAlertView updateLoadingMessage:@"Validating password"];
    
    if(custolAlertView)
    {
        [custolAlertView showKeyboard];
    }
    
    [[[UIApplication sharedApplication] keyWindow] addSubview:custolAlertView];
    
}

- (void)doValidationOnPassword:(NSString *)password
{
    if ([password length] > 0) {
        
        if (![AppManager isInternetConnectionAvailable]) {
            
            [custolAlertView showErrorOnCustomInputWithWithErrro:kNoConnectionMessage];
            [AppManager trackNoInternetErrorOnPage:OMNIPAGE_SETTINGS_EDIT_CONTACT];
        }
        else
        {
            
            [custolAlertView removeErrorMeesageFromCustomInputAlertView];
            [custolAlertView startLoading];
            
            
            [(DLMSettingsEditContactScreen *)_addNewContactScreenModel submitAndCreateContactDetailsModelWithContactDetailsDictionary:_updatedContactDetailsDict andPassword:password];
        }
    } else {
        //[self displayEnterPasswordViewWithErrorMessageHidden:NO andMessage:@"Invalid password"];
        [custolAlertView showErrorOnCustomInputWithWithErrro:@"Please enter your password"];
    }
    
}



#pragma mark- action method

- (void)cancelButtonPressed:(id)sender{
    
    [self hideProgressView];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)saveButtonPressed:(id)sender{
    
    if(self.networkRequestInProgress) {
        return;
    }
    
    NSString *pageName = OMNIPAGE_SETTINGS_EDIT_CONTACT;
    NSString *linkTitle = OMNICLICK_SETTINGS_SAVE;
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:data];
    
    [self saveContactInfo];
}

- (void)showTextButtonPressed:(id)sender{
    
    UIButton *button = (UIButton *)sender;
    button.selected = !button.selected;
    
    ((UITextField *)[sender superview]).secureTextEntry = !((UITextField *)[sender superview]).isSecureTextEntry;
    
}


#pragma mark- DLMSettingsEditContactScreenDelegate Methods

- (void)successfullyFetchedTitlesOnEditContactScreen:(DLMSettingsEditContactScreen *)editContactScreen
{
    _titlesArray = [NSArray arrayWithArray:editContactScreen.arrayOfTitles];
}


- (void)addContactTableViewCell:(AddContactTableViewCell *)cell
         didChangeTextForMoldel:(BTTextFiledModel *)textFieldModel{
    
    
    //to fix the data change issue, if optional value is not available its nil and emty textfield give emty string instead of nil
    
    if([textFieldModel.inputText isEqualToString:@""]){
        
        textFieldModel.inputText = nil;
    }
    
    _updatedContactDetailsDict = [self getUserDataDictionary];
    [self setDataChangeStatus];
    
    
    if(textFieldModel.textFieldType == TextFiledTypeEmail){
        
        BTTextFiledModel *altEmailModel = [_dataArray lastObject];
        altEmailModel.dependentCondition = textFieldModel.inputText;
    }
    
}



#pragma mark- BTAddSiteContactTableViewControllerDelegate

- (void)btAddSiteContactTableViewController:(BTAddSiteContactTableViewController *)controller didSubmitUserData:(NSDictionary *)dataDict{
    
    _updatedContactDetailsDict = dataDict;
    
    [self displayEnterPasswordViewWithErrorMessageHidden:YES andMessage:nil];
}

#pragma mark - DLMUpdatePasswordScreenDelegate methods
- (void)updateSuccessfullyFinishedByEditContactScreen:(DLMSettingsEditContactScreen *)settingsEditContactScreen
{
    //[self hideProgressView];
    //[self.delegate :self];
    [custolAlertView stopLoading];
    
    [self updateAndSaveContactDetailsLocally];
    [custolAlertView deRegisterKeyboardNotification];
    [custolAlertView removeFromSuperview];
    
    [self.editContactViewDelegate userDetailSucessfullyUpdatedOnBTSettingsEditContactViewController:self];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)settingsEditContactScreen:(DLMSettingsEditContactScreen *)settingsEditContactScreen failedToUpdateContactDetailsWithWebServiceError:(NLWebServiceError *)webServiceError
{
    self.networkRequestInProgress = NO;
    // [self hideProgressView];
    [custolAlertView stopLoading];
    

    BOOL errorHandled = [self attemptVordelProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
    
    if(errorHandled)
    {
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_SETTINGS_EDIT_CONTACT];
    }
    
    if(errorHandled == NO && [webServiceError.error.domain isEqualToString:BTNetworkErrorDomain])
    {
        errorHandled = YES;
        
        switch (webServiceError.error.code)
        {
            case BTNetworkErrorCodeVordelParameterInvalid:
                [custolAlertView showErrorOnCustomInputWithWithErrro:@"Please enter a valid password."];
                break;
                
            default:
                [custolAlertView showErrorOnCustomInputWithWithErrro:kDefaultErrorMessage];
                break;
        }
    }
    else
    {
        [custolAlertView removeFromSuperview];
    }
    
    if (errorHandled == NO)
    {
        [custolAlertView showErrorOnCustomInputWithWithErrro:kDefaultErrorMessage];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_SETTINGS_EDIT_CONTACT];
    }
    
}


#pragma mark  - BTCustom Alert View Delegate

- (void)userDidPressCancelOnCustomInputAlertView:(BTCustomInputAlertView *)alertView
{
    [custolAlertView deRegisterKeyboardNotification];
    [custolAlertView removeFromSuperview];
    custolAlertView = nil;

}

- (void)userDidPressConfirmOnCustomInputAlertView:(BTCustomInputAlertView *)alertView withPassword:(NSString *)passwordText
{
    [self doValidationOnPassword:passwordText];
}

@end
