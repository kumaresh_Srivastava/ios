//
//  BTSettingsEditContactScreen.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/17/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMSettingsEditContactScreen.h"
#import "BTTextFiledModel.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "CDAuthenticationToken.h"
#import "NLQueryUpdateContactWebService.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "AppManager.h"
#import "NSObject+APIResponseCheck.h"

@interface DLMSettingsEditContactScreen () < NLQueryUpdateContactWebServiceDelegate> {
    
}

@property (nonatomic) NLQueryUpdateContactWebService *queryUpdateContactWebService;

@end

@implementation DLMSettingsEditContactScreen

#pragma mark - Public Methods

- (void)fetchTitles
{
    _arrayOfTitles = [AppManager getBTUserTitles];
    
    [self.settingsEditContactScreenDelegate successfullyFetchedTitlesOnEditContactScreen:self];
}


- (void)submitAndCreateContactDetailsModelWithContactDetailsDictionary:(NSDictionary *)updatedContactDetailsDict andPassword:(NSString *)password
{
    NSMutableDictionary *updatedDictionary = [NSMutableDictionary dictionaryWithDictionary:updatedContactDetailsDict];
    
    //[updatedDictionary setValue:password forKey:@"Password"];
    
    [updatedDictionary setValue:[AppManager getRSAEncryptedString:password] forKey:@"Password"];
    
    // Get the currently logged in User
    CDUser *user = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    
    //(lp) Send the groupkey with text "Key"
    [updatedDictionary setValue:user.currentSelectedCug.groupKey forKey:@"Key"];
    
    self.queryUpdateContactWebService.queryUpdateContactWebServiceDelegate = nil;
    self.queryUpdateContactWebService = [[NLQueryUpdateContactWebService alloc] initWithUpdatedContactDetailsDictionary:updatedDictionary];
    
    self.queryUpdateContactWebService.queryUpdateContactWebServiceDelegate = self;
    [self.queryUpdateContactWebService resume];
    
}

#pragma mark - Private helper methods

- (void)createDataForSiteContactDict:(NSDictionary *)siteContactDict{
    
    
    NSMutableArray *tempDataArray = [NSMutableArray array];
    
    if(siteContactDict){
        
        //Select Title
        BTTextFiledModel *selectTitleFieldModel = [[BTTextFiledModel alloc] init];
        selectTitleFieldModel.textFieldType = TextFiledTypeDropDown;
        selectTitleFieldModel.keyBoardType = UIKeyboardTypeDefault;
        selectTitleFieldModel.returnKeyType = UIReturnKeyNext;
        selectTitleFieldModel.textFieldName = kSettingsUserTitle;
        selectTitleFieldModel.inputText = [siteContactDict valueForKey:kSettingsUserTitle];
        [tempDataArray addObject:selectTitleFieldModel];
        
        //First Name Field Data
        BTTextFiledModel *firstNameFieldModel = [[BTTextFiledModel alloc] init];
        firstNameFieldModel.textFieldType = TextFiledTypeName;
        firstNameFieldModel.keyBoardType = UIKeyboardTypeDefault;
        firstNameFieldModel.returnKeyType = UIReturnKeyNext;
        firstNameFieldModel.textFieldName = kSettingsUserFirstName;
        firstNameFieldModel.regex = @"^([a-zA-Z\\-]+(. )?[ \\']?)+$";
        firstNameFieldModel.inputText = [siteContactDict valueForKey:kSettingsUserFirstName];
        [tempDataArray addObject:firstNameFieldModel];
        
        //Last Name Field Data
        BTTextFiledModel *lastNameFieldModel = [[BTTextFiledModel alloc] init];
        lastNameFieldModel.textFieldType = TextFiledLastName;
        lastNameFieldModel.keyBoardType = UIKeyboardTypeDefault;
        lastNameFieldModel.returnKeyType = UIReturnKeyNext;
        lastNameFieldModel.textFieldName = kSettingsUserLastName;
        lastNameFieldModel.inputText = [siteContactDict valueForKey:kSettingsUserLastName];
        lastNameFieldModel.regex = @"^([a-zA-Z\\-]+(. )?[ \\']?)+$";
        [tempDataArray addObject:lastNameFieldModel];
        
        
        
        //Mobile Field Data
        BTTextFiledModel *phoneFieldModel = [[BTTextFiledModel alloc] init];
        phoneFieldModel.textFieldType = TextFiledTypePhone;
        phoneFieldModel.keyBoardType = UIKeyboardTypePhonePad;
        phoneFieldModel.returnKeyType = UIReturnKeyNext;
        phoneFieldModel.textFieldName = kSettingsUserMobile;
        phoneFieldModel.regex = @"^((00|\\+)44|0)7([0-9]{8,9})$";
        phoneFieldModel.inputText = [siteContactDict valueForKey:kSettingsUserMobile];
        
        //(Sal) If coming from server then make it mendatory
        if([phoneFieldModel.inputText validAndNotEmptyStringObject]){
            
            phoneFieldModel.textFieldType = TextFiledTypePhone;
        }
        [tempDataArray addObject:phoneFieldModel];
        
        //Landline
        BTTextFiledModel *altPhoneFieldModel = [[BTTextFiledModel alloc] init];
        altPhoneFieldModel.textFieldType = TextFiledTypePhoneOptional;
        altPhoneFieldModel.keyBoardType = UIKeyboardTypePhonePad;
        altPhoneFieldModel.returnKeyType = UIReturnKeyNext;
        altPhoneFieldModel.textFieldName = kSettingsUserLandline;
        altPhoneFieldModel.inputText = [siteContactDict valueForKey:kSettingsUserLandline];

        //(Sal) If coming from server then make it mendatory
        if([altPhoneFieldModel.inputText validAndNotEmptyStringObject]){
            
            altPhoneFieldModel.textFieldType = TextFiledTypePhoneMandatory;
        }
        
        altPhoneFieldModel.regex =  @"^0[0-9]{9,10}$";
        [tempDataArray addObject:altPhoneFieldModel];
        
        
        
        
        //Primary Email Field Data
        BTTextFiledModel *emailFieldModel = [[BTTextFiledModel alloc] init];
        emailFieldModel.textFieldType = TextFiledTypeEmail;
        emailFieldModel.keyBoardType = UIKeyboardTypeEmailAddress;
        emailFieldModel.returnKeyType = UIReturnKeyDone;
        emailFieldModel.textFieldName = kSettingsUserPriEmail;
        emailFieldModel.inputText = [siteContactDict valueForKey:kSettingsUserPriEmail];;
        emailFieldModel.regex = @"^((([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+(\\.([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+)*)|((\\x22)((((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(([\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f]|\\x21|[\\x23-\\x5b]|[\\x5d-\\x7e]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(\\\\([\\x01-\\x09\\x0b\\x0c\\x0d-\\x7f]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF]))))*(((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(\\x22)))@((([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.)+(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.?$";
        [tempDataArray addObject:emailFieldModel];
        
        //Alt Email Field Data
        BTTextFiledModel *altEmailFieldModel = [[BTTextFiledModel alloc] init];
        altEmailFieldModel.textFieldType = TextFiledTypeEmailOptional;
        altEmailFieldModel.keyBoardType = UIKeyboardTypeEmailAddress;
        altEmailFieldModel.returnKeyType = UIReturnKeyDone;
        altEmailFieldModel.textFieldName = kSettingsUserUserAltEmail;
        altEmailFieldModel.inputText = [siteContactDict valueForKey:kSettingsUserUserAltEmail];
        
        //(Sal) If coming from server then make it mendatory
        if([altEmailFieldModel.inputText validAndNotEmptyStringObject]){
            
            altEmailFieldModel.textFieldType = TextFiledTypeEmail;
        }
        
        altEmailFieldModel.regex = @"^((([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+(\\.([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+)*)|((\\x22)((((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(([\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f]|\\x21|[\\x23-\\x5b]|[\\x5d-\\x7e]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(\\\\([\\x01-\\x09\\x0b\\x0c\\x0d-\\x7f]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF]))))*(((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(\\x22)))@((([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.)+(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.?$";
        altEmailFieldModel.dependentCondition = [siteContactDict valueForKey:kSettingsUserPriEmail];
        [tempDataArray addObject:altEmailFieldModel];
        
        _textFieldsModelArray = tempDataArray;
    }
    
}


#pragma mark - NLQueryUpdateContactWebServiceDelegate methods
- (void)queryUpdateContactDetailsSuccesfullyFinishedWithQueryUpdateContactWebService:(NLQueryUpdateContactWebService *)webService
{
    if (webService == _queryUpdateContactWebService)
    {
        [self.settingsEditContactScreenDelegate updateSuccessfullyFinishedByEditContactScreen:self];
        
        _queryUpdateContactWebService.queryUpdateContactWebServiceDelegate = nil;
        _queryUpdateContactWebService = nil;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UserContactDetailsUpdatedSuccessfullyNotification" object:nil];
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLQueryUpdateContactWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLQueryUpdateContactWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)queryUpdateContactWebService:(NLQueryUpdateContactWebService *)webService failedToUpdateContactDetailsWithWebServiceError:(NLWebServiceError *)error
{
    if(webService == _queryUpdateContactWebService)
    {
        [self.settingsEditContactScreenDelegate settingsEditContactScreen:self failedToUpdateContactDetailsWithWebServiceError:error];
        
        _queryUpdateContactWebService.queryUpdateContactWebServiceDelegate = nil;
        _queryUpdateContactWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLQueryUpdateContactWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLQueryUpdateContactWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

@end
