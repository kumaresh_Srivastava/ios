//
//  BTSettingsEditContactViewController.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/17/16.
//  Copyright © 2016 Accolite. All rights reserved.
//


@class BTSettingsEditContactViewController;

@protocol BTSettingsEditContactViewControllerDelegate <NSObject>

- (void)userDetailSucessfullyUpdatedOnBTSettingsEditContactViewController:(BTSettingsEditContactViewController *)viewController;

@end

#import "BTAddSiteContactTableViewController.h"

@interface BTSettingsEditContactViewController : BTAddSiteContactTableViewController
@property(nonatomic, weak) id<BTSettingsEditContactViewControllerDelegate>editContactViewDelegate;

@end
