//
//  BTSettingsContactDetailViewController.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/17/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTSettingsContactDetailViewController : UIViewController

+ (BTSettingsContactDetailViewController *)getBTSettingsContactDetailViewController;

@end
