//
//  DLMUpdatePasswordScreen.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/18/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
#import "NLQueryUpdateProfileWebService.h"

@class DLMUpdatePasswordScreen;
@class NLWebServiceError;

@protocol DLMUpdatePasswordScreenDelegate <NSObject>

- (void)updateSuccessfullyFinishedByUpdatePasswordScreen:(DLMUpdatePasswordScreen *)updatePasswordScreen;

- (void)updatePasswordScreen:(DLMUpdatePasswordScreen *)updatePasswordScreen failedToSubmitUpdatedProfileDetailsWithWebServiceError:(NLWebServiceError *)error;

@end

@interface DLMUpdatePasswordScreen : DLMObject<NLQueryUpdateProfileWebServiceDelegate> {
    
}

@property (nonatomic) NLQueryUpdateProfileWebService *queryUpdateProfileWebService;
@property (nonatomic, weak) id <DLMUpdatePasswordScreenDelegate> updatePasswordScreenDelegate;


- (BOOL)isUppercaseConditionTrueForText:(NSString *)textString;
- (BOOL)isLowerConditionTrueForText:(NSString *)textString;
- (BOOL)isLengthConditionTrueForText:(NSString *)textString;
- (BOOL)isNumberConditionTrueForText:(NSString *)textString;

- (BOOL)checkNewPasswordValidtyForText:(NSString *)inputText;

- (void)submitAndCreateProfileDetailsModelWithNewPassword:(NSString *)newPassword andOldPassword:(NSString *)oldPassword;

@end
