//
//  BTSettingsUpdatePasswordViewController.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/17/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseViewController.h"

@class BTSettingsUpdatePasswordViewController;

@protocol BTSettingsUpdatePasswordViewControllerDelegate <NSObject>

- (void)successfullyUpdatedPasswordOnUpdatePasswordViewController:(BTSettingsUpdatePasswordViewController *)updatePasswordViewController;

@end

@interface BTSettingsUpdatePasswordViewController : BTBaseViewController {
    
}

@property (nonatomic, weak) id<BTSettingsUpdatePasswordViewControllerDelegate> delegate;
+ (BTSettingsUpdatePasswordViewController *)getBTSettingsUpdatePasswordViewController;

@end
