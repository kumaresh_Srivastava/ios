//
//  DLMUpdatePasswordScreen.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/18/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMUpdatePasswordScreen.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "CDAuthenticationToken.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"

@interface DLMUpdatePasswordScreen () {
    
}

@end

@implementation DLMUpdatePasswordScreen


#pragma mark - Public methods
- (BOOL)isUppercaseConditionTrueForText:(NSString *)textString{
    
    NSString *inputString = textString;
    
    return ![[inputString lowercaseString] isEqualToString:textString];
}


- (BOOL)isLowerConditionTrueForText:(NSString *)textString{
    
    NSString *inputString = textString;
    
    return ![[inputString uppercaseString] isEqualToString:textString];
}


- (BOOL)isLengthConditionTrueForText:(NSString *)textString{
    
    if(textString.length >=8 && textString.length <=16){
        
        return YES;
    }
    
    return NO;
}

- (BOOL)isNumberConditionTrueForText:(NSString *)textString{
    
    return ([textString rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location != NSNotFound);
}


- (BOOL)checkNewPasswordValidtyForText:(NSString *)inputText{
    
    if([self isUppercaseConditionTrueForText:inputText] && [self isLowerConditionTrueForText:inputText] &&
       [self isLengthConditionTrueForText:inputText] &&
       [self isNumberConditionTrueForText:inputText]){
        
        return YES;
    }
    
    return NO;
}


- (void)submitAndCreateProfileDetailsModelWithNewPassword:(NSString *)newPassword andOldPassword:(NSString *)oldPassword
{
    NSMutableDictionary *updatedDictionary = [NSMutableDictionary dictionary];
    
    [updatedDictionary setValue:[AppManager getRSAEncryptedString:newPassword] forKey:@"NewPassword"];
    [updatedDictionary setValue:[AppManager getRSAEncryptedString:oldPassword] forKey:@"Password"];
    [updatedDictionary setValue:[NSNumber numberWithBool:false] forKey:@"IsSecurityQuestionChanged"];
    
    // Get the currently logged in User
    CDUser *user = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    
    //(lp) Send the groupkey with text "Key"
    [updatedDictionary setValue:user.currentSelectedCug.groupKey forKey:@"Key"];
    
    _queryUpdateProfileWebService.queryUpdateProfileWebServiceDelegate = nil;
    _queryUpdateProfileWebService = [[NLQueryUpdateProfileWebService alloc] initWithUpdatedProfileDetailsDictionary:updatedDictionary];
    
    _queryUpdateProfileWebService.queryUpdateProfileWebServiceDelegate = self;
    [_queryUpdateProfileWebService resume];
    
}


#pragma mark - NLQueryUpdateProfileWebServiceDelegate methods
- (void)queryUpdateProfileDetailsSuccesfullyFinishedWithQueryUpdateProfileWebService:(NLQueryUpdateProfileWebService *)webService
{
    if(webService == _queryUpdateProfileWebService)
    {
        [self.updatePasswordScreenDelegate updateSuccessfullyFinishedByUpdatePasswordScreen:self];
        
        self.queryUpdateProfileWebService.queryUpdateProfileWebServiceDelegate = nil;
        self.queryUpdateProfileWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLQueryUpdateProfileWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLQueryUpdateProfileWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        
    }
}

- (void)queryUpdateProfileWebService:(NLQueryUpdateProfileWebService *)webService failedToUpdateProfileDetailsWithWebServiceError:(NLWebServiceError *)error
{
    if(webService == _queryUpdateProfileWebService)
    {
        [self.updatePasswordScreenDelegate updatePasswordScreen:self failedToSubmitUpdatedProfileDetailsWithWebServiceError:error];
        
        self.queryUpdateProfileWebService.queryUpdateProfileWebServiceDelegate = nil;
        self.queryUpdateProfileWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLQueryUpdateProfileWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLQueryUpdateProfileWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        
    }
}


@end
