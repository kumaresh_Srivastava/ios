//
//  BTSettingsUpdatePasswordViewController.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/17/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTSettingsUpdatePasswordViewController.h"
#import "PasswordInputView.h"
#import "PasswordValidationView.h"
#import "DLMUpdatePasswordScreen.h"
#import "NSObject+APIResponseCheck.h"
#import "AppConstants.h"
#import "AppManager.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "BTSignInViewController.h"
#import "OmnitureManager.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "NLWebServiceError.h"

#define kInputViewHeight 110
#define kValidationViewHeight 66


#define kCurrentPasswordTag 111
#define kNewPasswordTag 112
#define kConfirmPasswordTag 113
#define kNavigationHeightWithStatusBar 64

#define kMessageTitle @"Message"

@interface BTSettingsUpdatePasswordViewController ()<PasswordInputViewDelegate, DLMUpdatePasswordScreenDelegate>{
    
    PasswordInputView *_currentPasswordView;
    PasswordInputView *_newPasswordView;
    PasswordInputView *_confirmPasswordView;
    PasswordValidationView *_passwordValidationView;
    DLMUpdatePasswordScreen *_screenViewModel;
    BOOL isUILoaded;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


@end

@implementation BTSettingsUpdatePasswordViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    


    self.title = @"Update password";
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = [self getCancelButtonItem];


    self.automaticallyAdjustsScrollViewInsets = NO;
    
    _screenViewModel = [[DLMUpdatePasswordScreen alloc] init];
    _screenViewModel.updatePasswordScreenDelegate = self;
    
    //Tap Guesture to hide keyboard
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.cancelsTouchesInView = NO;
    [_scrollView addGestureRecognizer:singleTap];
    
   // self.navigationItem.leftBarButtonItem = [self getCancelButtonItem];
    
    [self trackPageForOmniture];
    
}


- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if(!isUILoaded)
    {
        isUILoaded = YES;
        [self configureUI];
    }
}


- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}



- (void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark- Class Method

+ (BTSettingsUpdatePasswordViewController *)getBTSettingsUpdatePasswordViewController{

UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

BTSettingsUpdatePasswordViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"BTSettingsUpdatePasswordViewControllerID"];

return controller;
}


#pragma mark- Helper Method

- (void)configureUI{


    _currentPasswordView = [self getPasswordInputView];
    _currentPasswordView.tag = kCurrentPasswordTag;
    [_currentPasswordView setTitle:@"Current password"];
    _currentPasswordView.delegate = self;
    [_currentPasswordView becomeFirstResponderForTextfield];
    [self.scrollView addSubview:_currentPasswordView];
    
    _newPasswordView = [self getPasswordInputView];
    _newPasswordView.tag = kNewPasswordTag;
    [_newPasswordView setTitle:@"New password"];
    _newPasswordView.delegate = self;
    [self.scrollView addSubview:_newPasswordView];
    
    _confirmPasswordView = [self getPasswordInputView];
    _confirmPasswordView.tag = kConfirmPasswordTag;
    [_confirmPasswordView setTitle:@"Confirm password"];
    _confirmPasswordView.delegate = self;
    [self.scrollView addSubview:_confirmPasswordView];
    
    _passwordValidationView = [[[NSBundle mainBundle] loadNibNamed:@"PasswordValidationView" owner:self options:nil] firstObject];
    [self.scrollView addSubview:_passwordValidationView];
    


    _currentPasswordView.frame = CGRectMake(0,
                                            10,
                                            _scrollView.frame.size.width,
                                            kInputViewHeight);
    
    _newPasswordView.frame = CGRectMake(0,
                                        _currentPasswordView.frame.origin.y + kInputViewHeight,
                                        _scrollView.frame.size.width,
                                        kInputViewHeight - 10);
    
    
    _passwordValidationView.frame = CGRectMake(0,
                                               _newPasswordView.frame.origin.y + _newPasswordView.frame.size.height,
                                               _scrollView.frame.size.width,
                                                kValidationViewHeight);
    
    
    _confirmPasswordView.frame = CGRectMake(0,
                                            _passwordValidationView.frame.origin.y + kValidationViewHeight,
                                            _scrollView.frame.size.width,
                                            kInputViewHeight);
    

    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width,
                                         _confirmPasswordView.frame.origin.y + _confirmPasswordView.frame.size.height);
    
}


- (PasswordInputView *)getPasswordInputView{
    
    return [[[NSBundle mainBundle] loadNibNamed:@"PasswordInputView" owner:self options:nil] firstObject];

}


- (void)trackPageForOmniture
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setObject:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:OMNIPAGE_SETTINGS_UPDATE_PASSWORD withContextInfo:data];
}

- (void)updateUIBasedOnValidation{
    
    if([_screenViewModel checkNewPasswordValidtyForText:[_newPasswordView getInputText]]){
        
        if(!self.navigationItem.rightBarButtonItem){
            
            self.navigationItem.rightBarButtonItem = [self getSaveButtonItem];
        }
       
    }
    else{
        
        self.navigationItem.rightBarButtonItem = nil;
    }
}

- (UIBarButtonItem *)getSaveButtonItem{
    
    UIBarButtonItem *saveButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonPressed:)];
    
    return saveButtonItem;
}


- (UIBarButtonItem *)getCancelButtonItem{
    
    UIBarButtonItem *cancelButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed:)];
    
    return cancelButtonItem;
}

-(Boolean)liesWithinTextfieldRange:(CGRect)frameToBeCalculated withGestureRange: (CGPoint)tapPoint{
    CGFloat extremeLeft = frameToBeCalculated.origin.x + 20;
    CGFloat extremeRight = frameToBeCalculated.size.width - 20;
    CGFloat extremeTop = frameToBeCalculated.origin.y + 36;
    CGFloat extremeBottom = extremeTop + 50;
    if ((tapPoint.x >= extremeLeft && tapPoint.x <= extremeRight) && (tapPoint.y >= extremeTop && tapPoint.y <= extremeBottom)) {
        return YES;
    }
    return NO;
}

- (void)showErrorAlertWithMessage:(NSString *)message andTitle:(NSString *)title
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}


- (void)showSuccessfullPasswordChangeAlert
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Password changed" message:@"Please enter your username and password again" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction  *action) {
        [self navigateToLoginScreen];
    }];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
}


- (void)navigateToLoginScreen
{
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:@"needToPrefillEmail"];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAppNeedsLockDownWithLoginScreenWithoutAnyPrompt object:nil userInfo:dictionary];
}


#pragma mark- Guesture Method

- (void)handleTap:(id)guesture{
    CGPoint tapLocation = [guesture locationInView:self.scrollView];
    
    CGRect currentPasswordFrame = [_scrollView convertRect:_currentPasswordView.bounds fromView:_currentPasswordView];
    CGRect newPasswordFrame = [_scrollView convertRect:_newPasswordView.bounds fromView:_newPasswordView];
    CGRect confirmPasswordView = [_scrollView convertRect:_confirmPasswordView.bounds fromView:_confirmPasswordView];
    
    
    if (!([self liesWithinTextfieldRange:currentPasswordFrame withGestureRange:tapLocation] || [self liesWithinTextfieldRange:newPasswordFrame withGestureRange:tapLocation] || [self liesWithinTextfieldRange:confirmPasswordView withGestureRange:tapLocation])) {
        [_currentPasswordView resignFirstResponder];
        [_newPasswordView resignFirstResponder];
        [_confirmPasswordView resignFirstResponder];
    }
    //    else if ([self liesWithinTextfieldRange:_newPasswordView withGestureRange:tapLocation]) {
    //        [_newPasswordView becomeFirstResponder];
    //    }
    //    else if ([self liesWithinTextfieldRange:_confirmPasswordView withGestureRange:tapLocation]) {
    //        [_confirmPasswordView becomeFirstResponder];
    //    } else {
    //        [_currentPasswordView resignFirstResponder];
    //        [_newPasswordView resignFirstResponder];
    //        [_confirmPasswordView resignFirstResponder];
    //    }
}



#pragma mark- Action Methods

- (void)saveButtonPressed:(id)sender{
    
    [_currentPasswordView resignFirstResponderForTextfield];
    [_newPasswordView resignFirstResponderForTextfield];
    [_confirmPasswordView resignFirstResponderForTextfield];
    
    BOOL isInputValid = YES;
    
    if(![[_currentPasswordView getInputText] validAndNotEmptyStringObject]){
        
        isInputValid = NO;
        [_currentPasswordView showErrorWithMessage:@"Please enter current password"];
    }
    
    
    if(![[_confirmPasswordView getInputText] validAndNotEmptyStringObject]){
        
        isInputValid = NO;
        [_confirmPasswordView showErrorWithMessage:@"Please enter confirm password"];
    }
    
    
    if(isInputValid && ![[_newPasswordView getInputText] isEqualToString:[_confirmPasswordView getInputText]]){
        
        isInputValid = NO;
        
        [_confirmPasswordView showErrorWithMessage:@"Passwords don't match"];
        
    }
    
    
    if(isInputValid){
        
        if (![AppManager isInternetConnectionAvailable]) {
            [AppManager trackNoInternetErrorOnPage:OMNIPAGE_SETTINGS_UPDATE_PASSWORD];
            [self showErrorAlertWithMessage:kNoConnectionMessage andTitle:kNoConnectionTitle];
        }
        else
        {
            NSString *currentPassword = [_currentPasswordView getInputText];
            NSString *newPassword = [_newPasswordView getInputText];
            
            [_screenViewModel submitAndCreateProfileDetailsModelWithNewPassword:newPassword andOldPassword:currentPassword];
            self.networkRequestInProgress = YES;
        }
    }
    
}

- (void)cancelButtonPressed:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}



#pragma mark- Keyboard Notifications Method

- (void)keyboardDidShow:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0,
                                                  0.0,
                                                  kbRect.size.height + 15,
                                                  0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
   
}

- (void)keyboardWillBeHidden:(NSNotification *)notification
{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    [self.scrollView setContentOffset:CGPointMake(0, 0)];
}




#pragma mark - PasswordInputViewDelegate method

- (void)passwordInputView:(PasswordInputView *)view didChangeWithText:(NSString *)inputTextString{
    
    if(view.tag == kCurrentPasswordTag){
        
    }
    else if(view.tag == kNewPasswordTag){
        
        [_passwordValidationView updateWithUdateModel:_screenViewModel andInputText:inputTextString];
        [self updateUIBasedOnValidation];
        
    }
    else if(view.tag == kConfirmPasswordTag){
        
    }
    
}

#pragma mark - DLMUpdatePasswordScreenDelegate methods

- (void)updateSuccessfullyFinishedByUpdatePasswordScreen:(DLMUpdatePasswordScreen *)updatePasswordScreen
{
    self.networkRequestInProgress = NO;
    // [self.delegate successfullyUpdatedPasswordOnUpdatePasswordViewController:self];
    // [self.navigationController popViewControllerAnimated:YES];
    
    [AppManager trackOmnitureKeyTask:OMNI_KEYTASK_SETTINGS_PASSWORD_CHANGE forPageName:OMNIPAGE_SETTINGS_SECURITY];
    
    //Now we have to show alert Message
    [self showSuccessfullPasswordChangeAlert];
}

- (void)updatePasswordScreen:(DLMUpdatePasswordScreen *)updatePasswordScreen failedToSubmitUpdatedProfileDetailsWithWebServiceError:(NLWebServiceError *)error
{
    
    self.networkRequestInProgress = NO;
    BOOL errorHandled = [self attemptVordelProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if(errorHandled)
    {
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_SETTINGS_UPDATE_PASSWORD];
    }
    
    if([error.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {
        errorHandled = YES;
        
        switch (error.error.code)
        {
            case BTNetworkErrorCodeVordelParameterInvalid:
            {
                [_currentPasswordView textFieldClear];
                [_currentPasswordView showErrorWithMessage:@"Please enter a valid password."];
                break;
            }
                
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }
    
    if(errorHandled == NO)
    {
        [self showErrorAlertWithMessage:kDefaultErrorMessage andTitle:kMessageTitle];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_SETTINGS_UPDATE_PASSWORD];
    }
}



@end
