#import "BTOnBoardingViewController.h"
#import "BTSignInViewController.h"
#import "BTOnBoardingChildViewController.h"
#import "CustomPageControl.h"
#import "OmnitureManager.h"
#import "AppManager.h"

@interface BTOnBoardingViewController () <UIPageViewControllerDataSource,UIPageViewControllerDelegate>

@property (weak, nonatomic) IBOutlet CustomPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic) UIPageViewController *pageController;
@property (nonatomic, assign) NSUInteger lastContentOffset;

@property(nonatomic) NSInteger previousIndex; //Used to decide forward and backward animation
@property (nonatomic) NSInteger currentSwipedIndex; //This is used to check if user has made a small transition or not

@end

@implementation BTOnBoardingViewController {
    NSArray *_titleArray, *_descArray, *_imageArray;
    BTOnBoardingChildViewController *_childViewController1, *_childViewController2, *_childViewController3, *_childViewController4, *_childViewController5,*_childViewController6,*_childViewController7;
    NSUInteger _globalIndex;
    int _currentPage;
    
    BOOL isTapEnabled;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.backgroundImageView.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
    
    // (VRK) setting pageviewcontroller
    _currentPage = 0;
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    self.pageController.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self.pageController.view setBackgroundColor:[UIColor clearColor]];
    [[self view] addSubview:[self.pageController view]];
    [self addConstraintsToPageView];
    self.pageController.dataSource = self;
    self.pageController.delegate = self;
    
    
    self.pageControl.numberOfPages = 5;
    [self.pageControl setCurrentPage:0];
    [self.view bringSubviewToFront:self.pageControl];
    _previousIndex = 0;
    _currentSwipedIndex = 0;
    [self addGestureRecogniser];

    
    [self setValuesTotheArrays];
    
    // (VRK) making the initialViewcontroller and assigning to pageviewcontroller
    BTOnBoardingChildViewController *initialViewController = (BTOnBoardingChildViewController *)[self viewControllerAtIndex:0];//[[BTOnBoardingChildViewController alloc]init];
//    initialViewController = (BTOnBoardingChildViewController *)[self viewControllerAtIndex:0];
    initialViewController.isFirstTime = YES;
    _globalIndex = 0;
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // (VRK) making the PageViewController as the child to the CurrentViewController
    [self addChildViewController:self.pageController];
    [self.pageController didMoveToParentViewController:self];
    
    [self trackPageWithOmnitureForPageName:OMNIPAGE_ONBOARDING_1];
    
}


#pragma mark - Constraints

// (VRK) Adding constraints to PageViewController
- (void)addConstraintsToPageView {
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.pageController.view
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view
                                                          attribute:NSLayoutAttributeLeft
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.pageController.view
                                                          attribute:NSLayoutAttributeLeft
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.pageController.view
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view
                                                          attribute:NSLayoutAttributeRight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.pageController.view
                                                          attribute:NSLayoutAttributeRight
                                                         multiplier:1.0
                                                           constant:0.0]];
    
}

#pragma mark - GestureRecognizer Methods

// (VRK) make SigninPage as Root
- (void)userTapped:(UITapGestureRecognizer *)tapGestureRecognizer {

    if (isTapEnabled) {

        if ([self.onBoardingDelegate respondsToSelector:@selector(userTappedOnLastOnboardingScreenOfOnBoardingViewController:)]) {
            [self.onBoardingDelegate userTappedOnLastOnboardingScreenOfOnBoardingViewController:self];
        }
    }
}

#pragma mark - PrivateHelper Methods

- (void)trackPageWithOmnitureForPageName:(NSString *)pageName
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Not Logged In" forKey:@"LoginStatus"];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

// (VRK) adding gesture for the controller
-(void)addGestureRecogniser {
    
    UITapGestureRecognizer *singleTapRecogniszer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(userTapped:)];
    [self.view addGestureRecognizer:singleTapRecogniszer];
}


// (VRK) getting ChildViewController based on index
- (UIViewController *)viewControllerAtIndex:(NSUInteger)index {
   
    BTOnBoardingChildViewController *childViewController = nil;
    switch (index) {
        case 0:
            if (_childViewController1 == nil) {
                
                _childViewController1 = [self.storyboard instantiateViewControllerWithIdentifier:@"OnBoardingChildScene"];
                
            }
            childViewController = _childViewController1;
            [childViewController hideControls];
            [childViewController setIsShowTapLabel:NO];
            break;
        case 1:
            if (_childViewController2 == nil) {
                
                _childViewController2 = [self.storyboard instantiateViewControllerWithIdentifier:@"OnBoardingChildScene"];
                
            }
            childViewController = _childViewController2;
            [childViewController hideControls];
            [childViewController setIsShowTapLabel:NO];
            break;
        case 2:
            if (_childViewController3 == nil) {
                
                _childViewController3 = [self.storyboard instantiateViewControllerWithIdentifier:@"OnBoardingChildScene"];
            }
            childViewController = _childViewController3;
            [childViewController hideControls];
            [childViewController setIsShowTapLabel:NO];
            break;
        case 3:
            if (_childViewController4 == nil) {
                
                _childViewController4 = [self.storyboard instantiateViewControllerWithIdentifier:@"OnBoardingChildScene"];
            }
            childViewController = _childViewController4;
            [childViewController hideControls];
            [childViewController setIsShowTapLabel:NO];
            break;
        case 4:
            if (_childViewController5 == nil) {
                
                _childViewController5 = [self.storyboard instantiateViewControllerWithIdentifier:@"OnBoardingChildScene"];
            }
            childViewController = _childViewController5;
            [childViewController hideControls];
            [childViewController setIsShowTapLabel:YES];
            break;
//        case 5:
//            if (_childViewController6 == nil) {
//
//                _childViewController6 = [self.storyboard instantiateViewControllerWithIdentifier:@"OnBoardingChildScene"];
//            }
//            childViewController = _childViewController6;
//            [childViewController hideControls];
//            [childViewController setIsShowTapLabel:YES];
//            break;
    }
    
    childViewController.index = index;

    [childViewController setImageName:[_imageArray objectAtIndex:index]];
    [childViewController setTitleString:[_titleArray objectAtIndex:index]];
    [childViewController setDescriptionString:[_descArray objectAtIndex:index]];
    return childViewController;
}

#pragma mark DummyData Methods

// (VRK) Setting the default data to the different arrays
- (void)setValuesTotheArrays {
    if (_titleArray == nil) {
        
        //_titleArray = [NSArray arrayWithObjects:@"Track current & past orders",@"Monitor usage & pay bills",@"Report & track faults",@"Get update notifications", nil];
        
         _titleArray = [NSArray arrayWithObjects:@"View and pay your BT bills",@"Keep an eye on your usage",@"Track orders and update appointments easily",@"Check the status of your BT services",@"Give guests free wi-fi at the flick of a switch",@"Check your service status", nil];
    }
    if (_descArray == nil) {
        _descArray = [NSArray arrayWithObjects:@"Track open orders, check delivery or appointment details and see closed orders.",
                      @"Keep an eye on phone usage. Pay all your BT Business bills quickly and easily.",
                      @"Report and track broadband, email and phone line faults.",
                      @"Updates about your orders, faults, engineer visits, phone and broadband usage, bills, payments and more.",
                      @"Free wi-fi for your guests.\nSecurity and speed protected for you.",
                      @"Plus report faults and see how quickly we put them right.",
                      nil];
//        @"Check your usage and service status in an instant."
    }
    if (_imageArray == nil) {
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
            _imageArray = [NSArray arrayWithObjects:@"billing_ipad_portrait",@"usage_ipad_portrait",@"order_tracker_ipad_portrait",@"service_status_ipad_portrait",@"whats_new_public_wifi_ipad_portrait",@"report", nil];
        }
        else {
            _imageArray = [NSArray arrayWithObjects:@"billing",@"account_usage",@"order_tracker",@"service_status",@"whats_new_public_wifi",@"report", nil];
        }
        
    }
}

#pragma mark PageViewControllerDataSourceMethods
//(VRK) PageViewController datasource methods

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    

    
    NSUInteger index = [(BTOnBoardingChildViewController *)viewController index];
    _globalIndex = index;
 
    if (index == 0) {
        return nil;
    }
    // Decrease the index by 1 to return
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    

    NSUInteger index = [(BTOnBoardingChildViewController *)viewController index];
    
    
    _globalIndex = index;
    
    index++;
    
    
    
    if (index == 5) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}


- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {
    
    UIViewController * viewController = [pageViewController.viewControllers lastObject];
    
   NSUInteger currentPage = [(BTOnBoardingChildViewController *)viewController index];
   BTOnBoardingChildViewController *childCtrlr = (BTOnBoardingChildViewController*)viewController;
    
    switch (currentPage) {
        case 0:
            [self trackPageWithOmnitureForPageName:OMNIPAGE_ONBOARDING_1];
            break;
            
        case 1:
            [self trackPageWithOmnitureForPageName:OMNIPAGE_ONBOARDING_2];
            break;
            
        case 2:
            [self trackPageWithOmnitureForPageName:OMNIPAGE_ONBOARDING_3];
            break;
            
        case 3:
            [self trackPageWithOmnitureForPageName:OMNIPAGE_ONBOARDING_4];
            break;
            
        case 4:
            [self trackPageWithOmnitureForPageName:OMNIPAGE_ONBOARDING_GUESTWIFI];
            break;
        case 5:
            [self trackPageWithOmnitureForPageName:OMNIPAGE_ONBOARDING_5];
            break;
            
//        case 6:
//            [self trackPageWithOmnitureForPageName:OMNIPAGE_ONBOARDING_6];
//            break;
    }
    
    
    [self.pageControl setCurrentPage:currentPage];
    
    if(_currentSwipedIndex != currentPage)
    {
        if(_previousIndex < currentPage)
        {
            [childCtrlr performForwardAnimation];
            if(_previousIndex <=4)
                _previousIndex++;
        }
        else
        {
            [childCtrlr performBackwardAnimation];
            if(_previousIndex >=1)
                _previousIndex--;
        }   
    }
    else
    {
        [childCtrlr ShowControls];
    }
    
    _currentSwipedIndex = currentPage;
    
    
    if(currentPage == 4)
    {
        isTapEnabled = YES;
    }
    else
    {
        isTapEnabled = NO;
    }
        
}

//- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
  //  self.pageControl.numberOfPages = 4;
 //   return 4;
//}

//- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
 //   return 0;
//}



@end
