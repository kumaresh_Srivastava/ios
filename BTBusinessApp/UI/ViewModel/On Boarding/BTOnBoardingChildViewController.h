//
//  BTOnBoardingChildViewController.h
//  BTBusinessApp
//
//  Created by Accolite on 08/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//
//  Code Review Process:
//  13-09-2016: Reviewed By Harman

#import <UIKit/UIKit.h>

@interface BTOnBoardingChildViewController : UIViewController

@property (assign, nonatomic) NSInteger index;
@property (nonatomic) NSString *imageName;
@property (nonatomic) NSString *titleString;
@property (nonatomic) NSString *descriptionString;
@property (nonatomic, assign) BOOL isShowTapLabel;
@property (nonatomic, assign) BOOL isFirstTime;
@property (weak, nonatomic) IBOutlet UIView *contentView;



- (void)showControlsWithAnimation;
- (void)moveControlsToLeft;
- (void)moveControlToRight;

- (void)performForwardAnimation;
- (void)performBackwardAnimation;
- (void)hideControls;
- (void)ShowControls;

@end
