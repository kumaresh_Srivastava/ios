//
//  BTOnBoardingViewController.h
//  BTBusinessApp
//
//  Created by Accolite on 08/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//
//  Code Review Process:
//  13-09-2016: Reviewed By Harman

#import <UIKit/UIKit.h>
@class BTOnBoardingViewController;

@protocol BTOnBoardingViewControllerDelegate <NSObject>

- (void)userTappedOnLastOnboardingScreenOfOnBoardingViewController:(BTOnBoardingViewController *)controller;
@end

@interface BTOnBoardingViewController : UIViewController
@property (nonatomic, weak) id<BTOnBoardingViewControllerDelegate> onBoardingDelegate;
@end
