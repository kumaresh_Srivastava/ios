//
//  BTOnBoardingChildViewController.m
//  BTBusinessApp
//
//  Created by Accolite on 08/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//
//  Code Review Process:
//  13-09-2016: Reviewed By Harman

#import "BTOnBoardingChildViewController.h"

@interface BTOnBoardingChildViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
//@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UILabel *tapLabel;
@property (weak, nonatomic) IBOutlet UIImageView *btLogoImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descLabelLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageCenterX;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tapLabelLeading;

@end

@implementation BTOnBoardingChildViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // (VRK) setting values to the controls
    [self.logoImageView setImage:nil];
    [self.logoImageView setImage:[UIImage imageNamed:self.imageName]];
    
    [self.titleLabel setText:self.titleString];
//    [self.descLabel setText:self.descriptionString];
//    self.descLabel.textColor = [UIColor whiteColor];
    
    [self.view setClipsToBounds:true];
    
    // (VRK) show or hide the taplabel
    if (self.isShowTapLabel) {
        self.tapLabel.hidden = NO;
    } else {
        self.tapLabel.hidden = YES;
    }
    
//    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
//    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && UIInterfaceOrientationIsPortrait(interfaceOrientation)){
//        _logoImageView.contentMode = UIViewContentModeScaleAspectFill;
//    } else {
//        _logoImageView.contentMode = UIViewContentModeScaleAspectFit;
//    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.logoImageView.hidden = NO;
    [self hideControls];
    
    if(!self.index && self.isFirstTime)
    {
        self.isFirstTime = NO;
        // [self performForwardAnimation];
        [self hideControls];
        [self moveControlToRight];
        [self ShowControls];
//        self.titleLabelLeading.constant = 0;
//        self.descLabelLeading.constant = 0;
//        self.tapLabelLeading.constant = 4;
        
    }
    
    
    //  [self performSelector:@selector(showControlsWithAnimation) withObject:nil afterDelay:0.2];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // [self hideControls];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    
}

#pragma mark - Public helper method

- (void)hideControls
{
    ///self.logoImageView.hidden = YES;
    self.titleLabel.hidden = YES;
//    self.descLabel.hidden = YES;
    self.tapLabel.hidden = YES;
}

- (void)ShowControls
{
    self.logoImageView.hidden = NO;
    self.titleLabel.hidden = NO;
    //self.descLabel.hidden = NO;
    //self.tapLabel.hidden = NO;
    if (self.isShowTapLabel) {
        self.tapLabel.hidden = NO;
    } else {
        self.tapLabel.hidden = YES;
    }
    
}


- (void)performBackwardAnimation
{
    [self hideControls];
    [self moveControlsToLeft];
    [self ShowControls];
    [self performSelector:@selector(showControlsWithAnimation) withObject:nil afterDelay:0.06];
}


- (void)performForwardAnimation
{
    [self hideControls];
    [self moveControlToRight];
    [self ShowControls];
    [self performSelector:@selector(showControlsWithAnimation) withObject:nil afterDelay:0.06];
}


- (void)moveControlToRight
{
    self.titleLabelLeading.constant = [[UIScreen mainScreen] bounds].size.width;
//    self.descLabelLeading.constant = [[UIScreen mainScreen] bounds].size.width;
    //  self.imageCenterX.constant = [[UIScreen mainScreen] bounds].size.width;
    
    self.tapLabelLeading.constant = [[UIScreen mainScreen] bounds].size.width;
    ;
    
}

- (void)moveControlsToLeft
{
    self.titleLabelLeading.constant = -2.2*[[UIScreen mainScreen] bounds].size.width;
//    self.descLabelLeading.constant = -[[UIScreen mainScreen] bounds].size.width;
    // self.imageCenterX.constant = -[[UIScreen mainScreen] bounds].size.width;
    
    
    self.tapLabelLeading.constant = -[[UIScreen mainScreen] bounds].size.width;
    ;
    
}




- (void)showControlsWithAnimation
{
    
    
    //   self.imageCenterX.constant = 0;
    
    //[UIView animateWithDuration:0.1 animations:^{
    // [self.view layoutIfNeeded];
    // }];
    //  [UIView commitAnimations];
    
    
    self.titleLabelLeading.constant = 0;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    }];
    [UIView commitAnimations];
    
//    self.descLabelLeading.constant = 0;
    self.tapLabelLeading.constant = 4;
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    [UIView commitAnimations];
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
