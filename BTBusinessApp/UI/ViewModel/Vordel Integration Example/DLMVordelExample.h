//
//  DLMVordelExample.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 29/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "DLMObject.h"

@class DLMVordelExample;
@class NLWebServiceError;

@protocol DLMVordelExampleDelegate <NSObject>

- (void)successfullyFetchedUserDataOnVordelExampleScreen:(DLMVordelExample *)vordelExample;

- (void)vordelExampleScreen:(DLMVordelExample *)vordelExample failedToFetchUserDataWithError:(NLWebServiceError *)error;

- (void)successfullyFetchedUserDataOnOnVordelExampleScreen:(DLMVordelExample *)vordelExample;

- (void)vordelExampleScreen:(DLMVordelExample *)vordelExample failedToFetchTrackOrderDashboardDataWithError:(NLWebServiceError *)error;

@end

@interface DLMVordelExample : DLMObject {

}

- (void)fetchUserDetails;

- (void)fetchOrderDashboardData;

@end
