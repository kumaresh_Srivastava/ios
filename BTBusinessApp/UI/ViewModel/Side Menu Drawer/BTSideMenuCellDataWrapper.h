//
//  BTSideMenuCellDataWrapper.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 14/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BTSideMenuItem;

@interface BTSideMenuCellDataWrapper : NSObject

@property (nonatomic, strong) BTSideMenuItem *sideMenuItem;
@property (nonatomic, assign) BOOL needToShowIndicator;
@property (nonatomic, assign) BOOL needsToDisplayOpenItems;

- (instancetype)initWitSideMenuItem:(BTSideMenuItem *)sideMenuItem needsToShowIndicator:(BOOL)needToShowIndicator andNeedsToDisplayOpenItems:(BOOL)needsToDisplayOpenItems;

@end
