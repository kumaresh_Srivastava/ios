//
//  BTSlideMenuParentViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 13/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTSideMenuViewController;
@class BTSlideMenuParentViewController;
@class BTPullNotificationsViewController;

@protocol BTSlideMenuParentViewControllerDelegate <NSObject>

- (UIViewController *)viewControllerForMenuButton;
- (void)redirectToDashboardScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController;
- (void)redirectToTrackOrdersScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController;
- (void)redirectToBillsScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController;
- (void)redirectToServiceStatusScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController;
- (void)redirectToTrackFaultsScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController;
- (void)redirectToAccountScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController;
- (void)redirectToUsageScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController;
- (void)redirectToPublicWifiScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController;
- (void)redirectToHelpScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController;
- (void)redirectToSettingsScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController;
- (void)performLogOutActionForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController;
- (void)redirectToLoggerScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController;
- (void)redirectToTermsAndConditonScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController;
- (void)redirectToOnBoardingScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController;
- (void)redirectToMoreBTBusinessAppsScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController;
- (void)redirectToSpeedTestScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController;

- (void)userPressedOnRetryButtonForNotifications;
- (void)noUnReadItemsLeftInNotifications;

@end

@interface BTSlideMenuParentViewController : UIViewController {
    
}

@property (nonatomic, strong) UIViewController <BTSlideMenuParentViewControllerDelegate> *screenViewController;
@property (nonatomic, strong) BTSideMenuViewController *slideMenuViewController;
@property (nonatomic, strong) BTPullNotificationsViewController *pullNotificationsViewController;

@property (nonatomic, weak) id <BTSlideMenuParentViewControllerDelegate> delegate;

@end
