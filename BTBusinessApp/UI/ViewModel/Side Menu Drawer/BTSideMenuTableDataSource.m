//
//  BTSideMenuTableDataSource.m
//  CustomSlideMenu
//
//  Created by Lakhpat Meena on 12/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTSideMenuTableDataSource.h"
#import "BTSideMenuItem.h"
#import "BTSideMenuCellDataWrapper.h"
#import "AppManager.h"

@interface BTSideMenuTableDataSource () {
    NSInteger _numberOfSections;
    NSArray *_arrayOfSideMenuData;
}


@end

@implementation BTSideMenuTableDataSource

- (void)createSideMenuData
{
    [self getSideMenuDataForHomeScreen];
}

- (NSInteger)numberOfRowsInBTSideMenuTableInSection:(NSInteger)section
{
    return [[_arrayOfSideMenuData objectAtIndex:section] count];
}

- (NSInteger)numberOfSectionsInBTSideMenuTableView:(UITableView *)tableView
{
    return [_arrayOfSideMenuData count];
}

- (BTSideMenuCellDataWrapper *)sideMenuTableView:(UITableView *)tableView cellDataForIndexPath:(NSIndexPath *)indexPath
{
    return [[_arrayOfSideMenuData objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
}

- (void)updateSideMenuTrackOrdersCellDataWithNeedsToShowIndicator:(BOOL)needsToShowIndicator needsToShowOpenItems:(BOOL)needsToShowOpenItems andCellDataDictionary:(NSDictionary *)cellDataDic
{
    for (BTSideMenuCellDataWrapper *cellData in [_arrayOfSideMenuData objectAtIndex:0]) {
        
        if ([cellData.sideMenuItem.title isEqualToString:@"Track orders"]) {
            
            cellData.needToShowIndicator = needsToShowIndicator;
            cellData.needsToDisplayOpenItems = needsToShowOpenItems;
            
            if (cellDataDic != nil && [cellDataDic objectForKey:@"trackOrder"]) {
                
                [cellData.sideMenuItem updateNumberOfOpenItems:[[cellDataDic valueForKey: @"trackOrder"] integerValue]];
            }
        }
    }
}

- (void)updateSideMenuTrackFaultsCellDataWithNeedsToShowIndicator:(BOOL)needsToShowIndicator needsToShowOpenItems:(BOOL)needsToShowOpenItems andCellDataDictionary:(NSDictionary *)cellDataDic
{
    for (BTSideMenuCellDataWrapper *cellData in [_arrayOfSideMenuData objectAtIndex:0]) {
        
        if ([cellData.sideMenuItem.title isEqualToString:@"Track faults"]) {
            
            cellData.needToShowIndicator = needsToShowIndicator;
            cellData.needsToDisplayOpenItems = needsToShowOpenItems;
            
            if (cellDataDic != nil && [cellDataDic objectForKey:@"faultData"]) {
                
                [cellData.sideMenuItem updateNumberOfOpenItems:[[cellDataDic valueForKey:@"faultData"] integerValue]];
            }
        }
    }
}

- (void)updateSideMenuBillsCellDataWithNeedsToShowIndicator:(BOOL)needsToShowIndicator needsToShowOpenItems:(BOOL)needsToShowOpenItems andCellDataDictionary:(NSDictionary *)cellDataDic
{
    for (BTSideMenuCellDataWrapper *cellData in [_arrayOfSideMenuData objectAtIndex:0]) {
        
        if ([cellData.sideMenuItem.title isEqualToString:@"Bills"]) {
            
            cellData.needToShowIndicator = needsToShowIndicator;
            cellData.needsToDisplayOpenItems = needsToShowOpenItems;
            
            if (cellDataDic != nil) {
                
                [cellData.sideMenuItem updateNumberOfOpenItems:[[cellDataDic valueForKey:@"OpenItems"] integerValue]];
            }
        }
    }
}

- (void)updateSideMenuServiceStatusCellDataWithNeedsToShowIndicator:(BOOL)needsToShowIndicator needsToShowOpenItems:(BOOL)needsToShowOpenItems andCellDataDictionary:(NSDictionary *)cellDataDic
{
    
}

- (void)getSideMenuDataForHomeScreen
{
    NSMutableArray *arrayOfSideMenuData = [NSMutableArray array];
    
    NSMutableArray *arrayOfSideMenuDataForSection1 = [NSMutableArray array];
    BTSideMenuItem *item11 = [[BTSideMenuItem alloc] initWithTitle:@"Track orders" imageName:@"track_order_disable" andStatusType:SMSideMenuStatusTypeNumeric];
    BTSideMenuItem *item12 = [[BTSideMenuItem alloc] initWithTitle:@"Service status" imageName:@"service_status_disable" andStatusType:SMSideMenuStatusTypeExtendedNumeric];
    BTSideMenuItem *item13 = [[BTSideMenuItem alloc] initWithTitle:@"Broadband speed test" imageName:@"" andStatusType:SMSideMenuStatusTypeNone];
    BTSideMenuItem *item14 = [[BTSideMenuItem alloc] initWithTitle:@"Track faults" imageName:@"track_fault_disable" andStatusType:SMSideMenuStatusTypeNumeric];
    BTSideMenuItem *item15 = [[BTSideMenuItem alloc] initWithTitle:@"Public wi-fi" imageName:@"public_wifi_disable" andStatusType:SMSideMenuStatusTypeNone];
    BTSideMenuItem *item16 = [[BTSideMenuItem alloc] initWithTitle:@"Help" imageName:@"help_disable" andStatusType:SMSideMenuStatusTypeNone];
    
    BTSideMenuCellDataWrapper *sideMenuCellData11 = [[BTSideMenuCellDataWrapper alloc] initWitSideMenuItem:item11 needsToShowIndicator:NO andNeedsToDisplayOpenItems:YES];
    BTSideMenuCellDataWrapper *sideMenuCellData12 = [[BTSideMenuCellDataWrapper alloc] initWitSideMenuItem:item12 needsToShowIndicator:NO andNeedsToDisplayOpenItems:NO];
    BTSideMenuCellDataWrapper *sideMenuCellData13 = [[BTSideMenuCellDataWrapper alloc] initWitSideMenuItem:item13 needsToShowIndicator:NO andNeedsToDisplayOpenItems:NO];
    BTSideMenuCellDataWrapper *sideMenuCellData14 = [[BTSideMenuCellDataWrapper alloc] initWitSideMenuItem:item14 needsToShowIndicator:NO andNeedsToDisplayOpenItems:YES];
    BTSideMenuCellDataWrapper *sideMenuCellData15 = [[BTSideMenuCellDataWrapper alloc] initWitSideMenuItem:item15 needsToShowIndicator:NO andNeedsToDisplayOpenItems:NO];
    BTSideMenuCellDataWrapper *sideMenuCellData16 = [[BTSideMenuCellDataWrapper alloc] initWitSideMenuItem:item16 needsToShowIndicator:NO andNeedsToDisplayOpenItems:NO];
    
    [arrayOfSideMenuDataForSection1 addObject:sideMenuCellData11];
    [arrayOfSideMenuDataForSection1 addObject:sideMenuCellData12];
    [arrayOfSideMenuDataForSection1 addObject:sideMenuCellData13];
    [arrayOfSideMenuDataForSection1 addObject:sideMenuCellData14];
    [arrayOfSideMenuDataForSection1 addObject:sideMenuCellData15];
    [arrayOfSideMenuDataForSection1 addObject:sideMenuCellData16];
    
    NSMutableArray *arrayOfSideMenuDataForSection2 = [NSMutableArray array];
    BTSideMenuItem *item21 = [[BTSideMenuItem alloc] initWithTitle:@"Settings" imageName:@"settings_disable" andStatusType:SMSideMenuStatusTypeNone];
    BTSideMenuItem *item22 = [[BTSideMenuItem alloc] initWithTitle:@"About the app" imageName:@"onboarding_disable" andStatusType:SMSideMenuStatusTypeNone];
    BTSideMenuItem *item23 = [[BTSideMenuItem alloc] initWithTitle:@"More BT Business apps" imageName:@"termsAndCondition_disable" andStatusType:SMSideMenuStatusTypeNone];
//    BTSideMenuItem *item24 = [[BTSideMenuItem alloc] initWithTitle:@"Terms and conditions" imageName:@"termsAndCondition_disable" andStatusType:SMSideMenuStatusTypeNone];
    
    BTSideMenuCellDataWrapper *sideMenuCellData21 = [[BTSideMenuCellDataWrapper alloc] initWitSideMenuItem:item21 needsToShowIndicator:NO andNeedsToDisplayOpenItems:NO];
    BTSideMenuCellDataWrapper *sideMenuCellData22 = [[BTSideMenuCellDataWrapper alloc] initWitSideMenuItem:item22 needsToShowIndicator:NO andNeedsToDisplayOpenItems:NO];
    BTSideMenuCellDataWrapper *sideMenuCellData23 = [[BTSideMenuCellDataWrapper alloc] initWitSideMenuItem:item23 needsToShowIndicator:NO andNeedsToDisplayOpenItems:NO];
//    BTSideMenuCellDataWrapper *sideMenuCellData24 = [[BTSideMenuCellDataWrapper alloc] initWitSideMenuItem:item24 needsToShowIndicator:NO andNeedsToDisplayOpenItems:NO];

    
    [arrayOfSideMenuDataForSection2 addObject:sideMenuCellData21];
    [arrayOfSideMenuDataForSection2 addObject:sideMenuCellData22];
    [arrayOfSideMenuDataForSection2 addObject:sideMenuCellData23];
//    [arrayOfSideMenuDataForSection2 addObject:sideMenuCellData24];

    [arrayOfSideMenuData addObject:arrayOfSideMenuDataForSection1];
    [arrayOfSideMenuData addObject:arrayOfSideMenuDataForSection2];
    
    _arrayOfSideMenuData = arrayOfSideMenuData;
    
}

@end
