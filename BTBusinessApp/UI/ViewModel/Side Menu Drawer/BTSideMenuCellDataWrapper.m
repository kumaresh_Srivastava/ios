//
//  BTSideMenuCellDataWrapper.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 14/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTSideMenuCellDataWrapper.h"

@implementation BTSideMenuCellDataWrapper

- (instancetype)initWitSideMenuItem:(BTSideMenuItem *)sideMenuItem needsToShowIndicator:(BOOL)needToShowIndicator andNeedsToDisplayOpenItems:(BOOL)needsToDisplayOpenItems
{
    self = [super init];
    
    if (self) {
        _sideMenuItem = sideMenuItem;
        _needToShowIndicator = needToShowIndicator;
        _needsToDisplayOpenItems = needsToDisplayOpenItems;
    }
    
    return self;
}

@end
