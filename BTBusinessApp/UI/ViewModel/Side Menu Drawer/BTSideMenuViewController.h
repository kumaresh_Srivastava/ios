//
//  BTSideMenuViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 13/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTSideMenuViewController;

@protocol BTSideMenuViewControllerDelegate <NSObject>

- (void)hideSideMenuViewWithSideMenuViewController:(UIViewController *)sideMenuViewController;

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedDashboardCellInSideMenuTableView:(UITableView *)tableView;
- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedTrackOrdersCellInSideMenuTableView:(UITableView *)tableView;
- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedBillsCellInSideMenuTableView:(UITableView *)tableView;
- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedServiceStatusCellInSideMenuTableView:(UITableView *)tableView;
- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedTrackFaultsCellInSideMenuTableView:(UITableView *)tableView;
- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedAccountCellInSideMenuTableView:(UITableView *)tableView;
- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedUsageCellInSideMenuTableView:(UITableView *)tableView;
- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedPublicWifiCellInSideMenuTableView:(UITableView *)tableView;
- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedHelpCellInSideMenuTableView:(UITableView *)tableView;
- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedSettingsCellInSideMenuTableView:(UITableView *)tableView;
- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedLogOutCellInSideMenuTableView:(UITableView *)tableView;
- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedDevDebuggerCellInSideMenuTableView:(UITableView *)tableView;
- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedTermsAndConditionsCellInSideMenuTableView:(UITableView *)tableView;
- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedMoreBTBusinessAppsCellInSideMenuTableView:(UITableView *)tableView;
- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedOnBoardingCellInSideMenuTableView:(UITableView *)tableView;
- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedSpeedTestCellInSideMenuTableView:(UITableView *)tableView;

@end

@interface BTSideMenuViewController : UIViewController {
    
}

@property (nonatomic, weak) id <BTSideMenuViewControllerDelegate> sideMenuViewControllerDelegate;

+ (BTSideMenuViewController *)getViewController;

- (void)checkCurrentSelectedScreenViewController;

@end
