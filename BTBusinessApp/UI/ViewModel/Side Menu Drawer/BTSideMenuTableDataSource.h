//
//  BTSideMenuTableDataSource.h
//  CustomSlideMenu
//
//  Created by Lakhpat Meena on 12/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class BTSideMenuItem;
@class BTSideMenuCellDataWrapper;

@interface BTSideMenuTableDataSource : NSObject {
    
}

@property (nonatomic, assign) NSInteger currentlySelectedSideMenuItem;
@property (nonatomic, readonly) NSArray *arrayOfSideMenuTableCellData;

- (NSInteger)numberOfRowsInBTSideMenuTableInSection:(NSInteger)section;
- (NSInteger)numberOfSectionsInBTSideMenuTableView:(UITableView *)tableView;
- (BTSideMenuCellDataWrapper *)sideMenuTableView:(UITableView *)tableView cellDataForIndexPath:(NSIndexPath *)indexPath;
- (void)createSideMenuData;

- (void)updateSideMenuTrackOrdersCellDataWithNeedsToShowIndicator:(BOOL)needsToShowIndicator needsToShowOpenItems:(BOOL)needsToShowOpenItems andCellDataDictionary:(NSDictionary *)cellDataDic;
- (void)updateSideMenuBillsCellDataWithNeedsToShowIndicator:(BOOL)needsToShowIndicator needsToShowOpenItems:(BOOL)needsToShowOpenItems andCellDataDictionary:(NSDictionary *)cellDataDic;
- (void)updateSideMenuServiceStatusCellDataWithNeedsToShowIndicator:(BOOL)needsToShowIndicator needsToShowOpenItems:(BOOL)needsToShowOpenItems andCellDataDictionary:(NSDictionary *)cellDataDic;
- (void)updateSideMenuTrackFaultsCellDataWithNeedsToShowIndicator:(BOOL)needsToShowIndicator needsToShowOpenItems:(BOOL)needsToShowOpenItems andCellDataDictionary:(NSDictionary *)cellDataDic;

@end
