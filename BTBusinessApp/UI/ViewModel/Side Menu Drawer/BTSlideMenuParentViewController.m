//
//  BTSlideMenuParentViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 13/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTSlideMenuParentViewController.h"
#import "BTSideMenuViewController.h"
#import "BTPullNotificationsViewController.h"
#import "DLMPullNotificationsScreen.h"
#import "AppManager.h"
#import "OmnitureManager.h"
#import <QuartzCore/QuartzCore.h>

#import "BTHomeViewController.h"

#define CENTER_TAG 1
#define LEFT_TAG 2
#define RIGHT_TAG 3

#define SLIDING_IN_TIMING .275
#define SLIDING_OUT_TIMING .25

@interface BTSlideMenuParentViewController ()< BTSideMenuViewControllerDelegate, BTPullNotificationsViewControllerDelegate> {
    
    BOOL _slideMainScreen;
    UIView *_transparentView;
    
}

@end

@implementation BTSlideMenuParentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //_slideMainScreen = YES;
    
    [self initialChildViewControllerSetup];
    
    
    // Setup Menu Button
    UIViewController *viewController = [self.delegate viewControllerForMenuButton];
    if(viewController.navigationController == nil)
    {
        //TODO: (LP) 13/12/2016 Handle if menu button will be on UIView.
    }
    else
    {
        
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:self action:@selector(showSlideMenu)];
        viewController.navigationItem.leftBarButtonItem.accessibilityLabel = @"Menu";
        
        viewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"notification_read_bell"] style:UIBarButtonItemStylePlain target:self action:@selector(showNotifications)];
        viewController.navigationItem.rightBarButtonItem.accessibilityLabel = @"Notifications";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    _pullNotificationsViewController.view.frame = CGRectMake(size.width, 0, size.width, size.height);
}

- (void)initialChildViewControllerSetup {
    
    if (_slideMainScreen) {
        //[self setupLeftMenuView];
        [self setupCenterView];
    }
    else
    {
        [self setupCenterView];
        [self addTransparentLayerBelowChildView];
        //[self setupLeftMenuView];
        [self setupRightNotificationsView];
    }
    
    
    // Code to enable swipping in side menu
//    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
//    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
//    
//    // Setting the swipe direction.
//    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
//    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
//    
//    // Adding the swipe gesture on image view
//    [self.view addGestureRecognizer:swipeLeft];
//    [self.view addGestureRecognizer:swipeRight];
    
}

//- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
//    
//    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
//        [self hideSideMenuView];
//    }
//    
//    if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
//        [self showSlideMenu];
//    }
//    
//}

- (void)setupCenterView {
    
    _screenViewController.view.tag = CENTER_TAG;
    
    [self.view addSubview:_screenViewController.view];
    
    [self addChildViewController:_screenViewController];
    
    [_screenViewController didMoveToParentViewController:self];
    
}

- (void)setupLeftMenuView {
    
    _slideMenuViewController.view.tag = LEFT_TAG;
    _slideMenuViewController.sideMenuViewControllerDelegate = self;

    [self.view addSubview:_slideMenuViewController.view];
    [self addChildViewController:_slideMenuViewController];

    [_slideMenuViewController didMoveToParentViewController:self];

    if (_slideMainScreen) {
        _slideMenuViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
    else
    {
        _slideMenuViewController.view.frame = CGRectMake(- self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
    
}

- (void)setupRightNotificationsView {
    
    _pullNotificationsViewController.view.tag = RIGHT_TAG;
    _pullNotificationsViewController.pullNotificationsViewControllerDelegate = self;
    
    [self.view addSubview:_pullNotificationsViewController.view];
    [self addChildViewController:_pullNotificationsViewController];
    
    [_pullNotificationsViewController didMoveToParentViewController:self];
    
    _pullNotificationsViewController.view.frame = CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
    
}

- (UIView *)getLeftMenuView {
    if (_slideMenuViewController == nil) {
        DDLogError(@"Slide menu view controller is not assigned properly. Assign before using this class.");
        return nil;
    }
    
    UIView *view = _slideMenuViewController.view;
    return view;
}

- (void)addTransparentLayerBelowChildView
{
    _transparentView = [[UIView alloc] init];
    _transparentView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    _transparentView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_transparentView];
    _transparentView.hidden = YES;
}

- (void)showTransparentLayerBelowChildView
{
    _transparentView.hidden = NO;
    _transparentView.layer.backgroundColor = [UIColor clearColor].CGColor;
    [UIView animateWithDuration:SLIDING_IN_TIMING animations:^{
        _transparentView.layer.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2f].CGColor;
    }];
}

- (void)hideTransparentLayerBelowChildView
{
    [UIView animateWithDuration:SLIDING_OUT_TIMING animations:^{
        _transparentView.layer.backgroundColor = [UIColor clearColor].CGColor;
    } completion:^(BOOL finished) {
        _transparentView.hidden = YES;
    }];
}

#pragma mark - ScreenView Controller delagate methods
- (void)showSlideMenu {
    
    if (_slideMainScreen) {
        
        UIView *leftView = [self getLeftMenuView];
        [self.view sendSubviewToBack:leftView];
        
        [UIView animateWithDuration:SLIDING_IN_TIMING delay:0 options:UIViewAnimationOptionCurveEaseInOut  animations:^{
            
            _screenViewController.view.frame = CGRectMake(self.view.frame.size.width * 0.85, 0, self.view.frame.size.width, self.view.frame.size.height);
            
        } completion:^(BOOL finished)
        {
            
        }];
    }
    else
    {
        [UIView animateWithDuration:SLIDING_IN_TIMING delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            
            [_slideMenuViewController checkCurrentSelectedScreenViewController];
            
            _slideMenuViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [_slideMenuViewController.view setBackgroundColor:[UIColor clearColor]];
            
        } completion:^(BOOL finished)
        {
            
        }];
    }
    
    [self showTransparentLayerBelowChildView];
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = OMNIPAGE_NAVIGATE;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];

    
}

#pragma mark - Notifications related methods
- (void)showNotifications {
    
    [OmnitureManager trackPage:OMNIPAGE_NOTIFICATION withContextInfo:[AppManager getOmniDictionary]];
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",OMNIPAGE_HOME,OMNIPAGE_NOTIFICATION] withContextInfo:[AppManager getOmniDictionary]];
    
    _pullNotificationsViewController.notificationViewModel.isNotificationScreenCurrentlyVisible = YES;
    
    if (_pullNotificationsViewController.notificationViewModel.isGetNotificationFailedOnOtherScreen) {
        [_pullNotificationsViewController startFetchingNotificationsData];
        
    }
    
    [UIView animateWithDuration:SLIDING_IN_TIMING delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        self->_pullNotificationsViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self->_pullNotificationsViewController.view setBackgroundColor:[UIColor clearColor]];
        
    } completion:^(BOOL finished) {
        
    }];
    
    [self showTransparentLayerBelowChildView];
    
}

- (void)hideNotificationForPullNotificationsViewController:(BTPullNotificationsViewController *)pullNotificationsViewController {
    
    //(RLM) Uncomment this if we are shwoing no Internet strip on notificaiton screen.
    //[_pullNotificationsViewController removeNoInternetStrip];
    
    [UIView animateWithDuration:SLIDING_OUT_TIMING delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        self->_pullNotificationsViewController.view.frame = CGRectMake( self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self->_pullNotificationsViewController.view setBackgroundColor:[UIColor clearColor]];
        
    } completion:^(BOOL finished) {
        
        self->_pullNotificationsViewController.notificationViewModel.isNotificationScreenCurrentlyVisible = NO;
    }];
    
    [self hideTransparentLayerBelowChildView];
    
}

//BUG 16705
- (void)handleNotificationOrientation:(BTPullNotificationsViewController *)pullNotificationsViewController {
    
    if(pullNotificationsViewController.notificationViewModel.isNotificationScreenCurrentlyVisible == YES){
        self->_pullNotificationsViewController.view.frame = CGRectMake( self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        self->_pullNotificationsViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self->_pullNotificationsViewController.view setBackgroundColor:[UIColor clearColor]];
    }
}

- (void)userPressedRetryButtonOnNotificationsViewController:(BTPullNotificationsViewController *)pullNotificationsViewController
{
    [self.delegate userPressedOnRetryButtonForNotifications];
}

- (void)noUnReadItemsLeftOnNotificationsViewController:(BTPullNotificationsViewController *)pullNotificationsViewController
{
    [self.delegate noUnReadItemsLeftInNotifications];
}

#pragma mark - SideMenu Controller delegate methods
- (void)hideSideMenuViewWithSideMenuViewController:(UIViewController *)sideMenuViewController {
    
    if (_slideMainScreen) {
        
        [UIView animateWithDuration:SLIDING_OUT_TIMING delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            
            _screenViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            
        } completion:^(BOOL finished) {
            
        }];
    }
    else
    {
        [UIView animateWithDuration:SLIDING_OUT_TIMING delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            
            _slideMenuViewController.view.frame = CGRectMake(- self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
            [_slideMenuViewController.view setBackgroundColor:[UIColor clearColor]];
            
        } completion:^(BOOL finished) {
            
        }];
        
    }
    
    [self hideTransparentLayerBelowChildView];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedDashboardCellInSideMenuTableView:(UITableView *)tableView
{
    [self trackOmnitureClickEvent:OMNICLICK_HOME_DASHBOARD onPage:OMNIPAGE_NAVIGATE];
    
    [self.delegate redirectToDashboardScreenForSlideMenuParentViewController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedTrackOrdersCellInSideMenuTableView:(UITableView *)tableView
{
    [self trackOmnitureClickEvent:OMNICLICK_HOME_TRACK_ORDERS onPage:OMNIPAGE_NAVIGATE];
    [self.delegate redirectToTrackOrdersScreenForSlideMenuParentViewController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedBillsCellInSideMenuTableView:(UITableView *)tableView
{
    [self trackOmnitureClickEvent:OMNICLICK_HOME_BILLS onPage:OMNIPAGE_NAVIGATE];
    [self.delegate redirectToBillsScreenForSlideMenuParentViewController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedServiceStatusCellInSideMenuTableView:(UITableView *)tableView
{
    [self trackOmnitureClickEvent:OMNICLICK_HOME_SERVICE onPage:OMNIPAGE_NAVIGATE];
    [self.delegate redirectToServiceStatusScreenForSlideMenuParentViewController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedTrackFaultsCellInSideMenuTableView:(UITableView *)tableView
{
    [self trackOmnitureClickEvent:OMNICLICK_HOME_TRACK_FAULTS onPage:OMNIPAGE_NAVIGATE];
    [self.delegate redirectToTrackFaultsScreenForSlideMenuParentViewController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedAccountCellInSideMenuTableView:(UITableView *)tableView
{
   [self trackOmnitureClickEvent:OMNICLICK_HOME_ACCOUNT onPage:OMNIPAGE_NAVIGATE];
    [self.delegate redirectToAccountScreenForSlideMenuParentViewController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedUsageCellInSideMenuTableView:(UITableView *)tableView
{
    [self trackOmnitureClickEvent:OMNICLICK_HOME_USAGE onPage:OMNIPAGE_NAVIGATE];
    [self.delegate redirectToUsageScreenForSlideMenuParentViewController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedPublicWifiCellInSideMenuTableView:(UITableView *)tableView
{
    [self trackOmnitureClickEvent:OMNICLICK_HOME_PUBLICWIFI onPage:OMNIPAGE_NAVIGATE];
    [self.delegate redirectToPublicWifiScreenForSlideMenuParentViewController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedHelpCellInSideMenuTableView:(UITableView *)tableView
{
    [self trackOmnitureClickEvent:OMNICLICK_HOME_HELP onPage:OMNIPAGE_NAVIGATE];
    [self.delegate redirectToHelpScreenForSlideMenuParentViewController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedSettingsCellInSideMenuTableView:(UITableView *)tableView
{
    [self trackOmnitureClickEvent:OMNICLICK_HOME_SETTINGS onPage:OMNIPAGE_NAVIGATE];
    [self.delegate redirectToSettingsScreenForSlideMenuParentViewController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedLogOutCellInSideMenuTableView:(UITableView *)tableView
{
    [self trackOmnitureClickEvent:OMNICLICK_HOME_LOGOUT onPage:OMNIPAGE_NAVIGATE];
    [self.delegate performLogOutActionForSlideMenuParentViewController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedDevDebuggerCellInSideMenuTableView:(UITableView *)tableView
{
    [self.delegate redirectToLoggerScreenForSlideMenuParentViewController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedTermsAndConditionsCellInSideMenuTableView:(UITableView *)tableView
{
    [self.delegate redirectToTermsAndConditonScreenForSlideMenuParentViewController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedMoreBTBusinessAppsCellInSideMenuTableView:(UITableView *)tableView
{
    [self.delegate redirectToMoreBTBusinessAppsScreenForSlideMenuParentViewController:self];
}


- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedOnBoardingCellInSideMenuTableView:(UITableView *)tableView
{
    [self.delegate redirectToOnBoardingScreenForSlideMenuParentViewController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedSpeedTestCellInSideMenuTableView:(UITableView *)tableView
{
    [self.delegate redirectToSpeedTestScreenForSlideMenuParentViewController:self];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


//Added by (RM) To track click event
- (void)trackOmnitureClickEvent:(NSString *)clickEvent onPage:(NSString *)page
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",page,clickEvent] withContextInfo:data];
}

@end

