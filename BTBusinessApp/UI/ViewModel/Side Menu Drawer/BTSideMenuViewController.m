//
//  BTSideMenuViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 13/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTSideMenuViewController.h"
#import "BTSideMenuTableDataSource.h"
#import "BTSideMenuItem.h"
#import "BTSideMenuCellDataWrapper.h"
#import "BTSideMenuWithStatusTableViewCell.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "BTServiceStatusActionsHeaderView.h"
#import "BTSideMenuLogoutView.h"

@interface BTSideMenuViewController ()<UITableViewDelegate, UITableViewDataSource> {
    
    BTSideMenuTableDataSource *_sideMenuTableDataSource;
}

@property (weak, nonatomic) IBOutlet UITableView *sideMenuTableView;
@property (weak, nonatomic) IBOutlet UILabel *sideMenuHeaderTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLeadingSideMenuTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTrailingSideMenuTableView;

@end

@implementation BTSideMenuViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
   
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Create datasource for table view data
    _sideMenuTableDataSource = [[BTSideMenuTableDataSource alloc] init];
    _sideMenuTableDataSource.currentlySelectedSideMenuItem = 0;
    [_sideMenuTableDataSource createSideMenuData];
    
    _headerView.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
    
    _sideMenuTableView.delegate = self;
    _sideMenuTableView.dataSource = self;
    _sideMenuTableView.backgroundColor = [UIColor colorForHexString:@"eeeeee"];//[BrandColours colourBtNeutral30];
    _sideMenuTableView.separatorInset = UIEdgeInsetsZero;
    
    _sideMenuTableView.clipsToBounds = NO;
    _sideMenuTableView.layer.masksToBounds = NO;
    
    self.navigationItem.title = @"More";
    self.navigationItem.hidesBackButton = YES;
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    
   // [self registerForNotificationsRelatedToAPICallToCheckOpenItems];
   
}

- (void)viewWillAppear:(BOOL)animated {
     [self checkDataFromHomeScreen];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.sideMenuViewControllerDelegate hideSideMenuViewWithSideMenuViewController:self];
    
    [super touchesBegan:touches withEvent:event];
}


#pragma mark - Private helper methods

- (void)updateUserName {
    
//    NSString *currentUserName = [NSString stringWithFormat:@"%@ %@", [AppDelegate sharedInstance].viewModel.app.loggedInUser.firstName, [AppDelegate sharedInstance].viewModel.app.loggedInUser.lastName];
//
//    if (![_sideMenuHeaderTitleLabel.text isEqualToString:currentUserName])
//    {
//        _sideMenuHeaderTitleLabel.text = currentUserName;
//    }
}

- (void)registerForNotificationsRelatedToAPICallToCheckOpenItems
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkUserDetailsAPICallFinishedSuccessfully) name:@"APICallForUserDetailsFinishedSuccessfullyAtHomeScreenNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkActiveOrdersAPICallStarted) name:@"APICallForActiveOrdersStartedAtHomeScreenNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkOpenFaultsAPICallStarted) name:@"APICallForOpenFaultsStartedAtHomeScreenNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkReadyBillsAPICallStarted) name:@"APICallForReadyBillsStartedAtHomeScreenNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkActiveOrdersAPICallFinishedSuccessfully:) name:@"APICallForActiveOrdersFinishedSuccessfullyAtHomeScreenNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkOpenFaultsAPICallFinishedSuccessfully:) name:@"APICallForOpenFaultsFinishedSuccessfullyAtHomeScreenNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkReadyBillsAPICallFinishedSuccessfully:) name:@"APICallForReadyBillsFinishedSuccessfullyAtHomeScreenNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkActiveOrdersAPICallFailed) name:@"APICallForActiveOrdersFailedAtHomeScreenNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkOpenFaultsAPICallFailed) name:@"APICallForOpenFaultsFailedAtHomeScreenNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkReadyBillsAPICallFailed) name:@"APICallForReadyBillsFailedAtHomeScreenNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkActiveOrdersAPICallCancelled) name:@"APICallForActiveOrdersCancelledAtHomeScreenNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkOpenFaultsAPICallCancelled) name:@"APICallForOpenFaultsCancelledAtHomeScreenNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkReadyBillsAPICallCancelled) name:@"APICallForReadyBillsCancelledAtHomeScreenNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUserName) name:@"UserContactDetailsUpdatedSuccessfullyNotification" object:nil];
    
}

- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
        [self.sideMenuViewControllerDelegate hideSideMenuViewWithSideMenuViewController:self];
    }
}


#pragma mark - Public methods

+ (BTSideMenuViewController *)getViewController
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTSideMenuViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SideMenuViewController"];
   // [self registerForNotificationsRelatedToAPICallToCheckOpenItems];
    return vc;
}

- (void)checkCurrentSelectedScreenViewController
{
    [_sideMenuTableView reloadData];
}


#pragma mark - UITableViewDatasource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_sideMenuTableDataSource numberOfSectionsInBTSideMenuTableView:tableView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_sideMenuTableDataSource numberOfRowsInBTSideMenuTableInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTSideMenuCellDataWrapper *cellData = [_sideMenuTableDataSource sideMenuTableView:tableView cellDataForIndexPath:indexPath];
    
    BTSideMenuWithStatusTableViewCell *sideMenuCell = [tableView dequeueReusableCellWithIdentifier:@"SideMenuCell" forIndexPath:indexPath];
    
    [sideMenuCell updateCellWithSideMenuItemData:cellData];
    
    return sideMenuCell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"Support";
        case 1:
            return @"App settings";
    }
    return @"";
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 53.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section > 2) {
        return 0;
    } else {
        return 40.0f;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(section == [_sideMenuTableDataSource numberOfSectionsInBTSideMenuTableView:tableView] - 1) {
        return 200.0;
    } else {
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    BTServiceStatusActionsHeaderView *serviceStatusActionHeaderView = [[[NSBundle mainBundle] loadNibNamed:@"BTServiceStatusActionsHeaderView" owner:nil options:nil] objectAtIndex:0];
    
    if(section == 0) {
        //[serviceStatusActionHeaderView updateTitleLabelWithTitleText:@"Services"];
        [serviceStatusActionHeaderView updateTitleLabelWithTitleText:@"Support"];
    } else if(section == 1) {
        //[serviceStatusActionHeaderView updateTitleLabelWithTitleText:@"Support"];
        [serviceStatusActionHeaderView updateTitleLabelWithTitleText:@"App settings"];
    } else if(section == 2) {
        //[serviceStatusActionHeaderView updateTitleLabelWithTitleText:@"App settings"];
        [serviceStatusActionHeaderView updateTitleLabelWithTitleText:@""];
    } else {
        return nil;
    }
    
    return serviceStatusActionHeaderView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(section == [_sideMenuTableDataSource numberOfSectionsInBTSideMenuTableView:tableView] - 1) {
        BTSideMenuLogoutView *logOutView = [[[NSBundle mainBundle] loadNibNamed:@"BTSideMenuLogoutView" owner:self options:nil] objectAtIndex:0];
        CGRect frame = logOutView.frame;
        frame.size.width  = [UIScreen mainScreen].bounds.size.width;
        [logOutView setFrame:frame];
        logOutView.logOutButton.layer.masksToBounds = YES;
        logOutView.logOutButton.layer.cornerRadius = 1.0f;
        
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(logOutTapped)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [logOutView.logOutButton addGestureRecognizer:tapGestureRecognizer];
        logOutView.logOutButton.userInteractionEnabled = YES;
                
        return logOutView;
    } else {
        return nil;
    }
}

- (void) logOutTapped{
    [self.sideMenuViewControllerDelegate sideMenuViewController:self userPressedLogOutCellInSideMenuTableView:nil];
}

#pragma mark - UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTSideMenuCellDataWrapper *cellData = [_sideMenuTableDataSource sideMenuTableView:tableView cellDataForIndexPath:indexPath];
    
    BTSideMenuWithStatusTableViewCell *sideMenuCell = [tableView cellForRowAtIndexPath:indexPath];
    [sideMenuCell updateSideMenuCellOnSelectionWithCellData:cellData];
    
    [self.sideMenuViewControllerDelegate hideSideMenuViewWithSideMenuViewController:self];
    
    if ([cellData.sideMenuItem.title isEqualToString:@"Dashboard"])
    {
        [self.sideMenuViewControllerDelegate sideMenuViewController:self userPressedDashboardCellInSideMenuTableView:tableView];
    }
    else if ([cellData.sideMenuItem.title isEqualToString:@"Track orders"])
    {
        [self.sideMenuViewControllerDelegate sideMenuViewController:self userPressedTrackOrdersCellInSideMenuTableView:tableView];
    }
    else if ([cellData.sideMenuItem.title isEqualToString:@"Bills"])
    {
        [self.sideMenuViewControllerDelegate sideMenuViewController:self userPressedBillsCellInSideMenuTableView:tableView];
    }
    else if ([cellData.sideMenuItem.title isEqualToString:@"Service status"])
    {
        [self.sideMenuViewControllerDelegate sideMenuViewController:self userPressedServiceStatusCellInSideMenuTableView:tableView];
    }
    else if ([cellData.sideMenuItem.title isEqualToString:@"Track faults"])
    {
        [self.sideMenuViewControllerDelegate sideMenuViewController:self userPressedTrackFaultsCellInSideMenuTableView:tableView];
    }
    else if ([cellData.sideMenuItem.title isEqualToString:@"Account"])
    {
        [self.sideMenuViewControllerDelegate sideMenuViewController:self userPressedAccountCellInSideMenuTableView:tableView];
    }
    else if ([cellData.sideMenuItem.title isEqualToString:@"Usage"])
    {
        [self.sideMenuViewControllerDelegate sideMenuViewController:self userPressedUsageCellInSideMenuTableView:tableView];
    }
    else if ([cellData.sideMenuItem.title isEqualToString:@"Public wi-fi"])
    {
        [self.sideMenuViewControllerDelegate sideMenuViewController:self userPressedPublicWifiCellInSideMenuTableView:tableView];
    }
    else if ([cellData.sideMenuItem.title isEqualToString:@"Help"])
    {
        [self.sideMenuViewControllerDelegate sideMenuViewController:self userPressedHelpCellInSideMenuTableView:tableView];
    }
    else if ([cellData.sideMenuItem.title isEqualToString:@"Settings"])
    {
        [self.sideMenuViewControllerDelegate sideMenuViewController:self userPressedSettingsCellInSideMenuTableView:tableView];
    }
    else if ([cellData.sideMenuItem.title isEqualToString:@"Log out"])
    {
        [self.sideMenuViewControllerDelegate sideMenuViewController:self userPressedLogOutCellInSideMenuTableView:tableView];
    }
    else if ([cellData.sideMenuItem.title isEqualToString:@"Dev debugger"])
    {
        [self.sideMenuViewControllerDelegate sideMenuViewController:self userPressedDevDebuggerCellInSideMenuTableView:tableView];
    }
    else if ([cellData.sideMenuItem.title isEqualToString:@"More BT Business apps"])
    {
        [self.sideMenuViewControllerDelegate sideMenuViewController:self userPressedMoreBTBusinessAppsCellInSideMenuTableView:tableView];
    }
    else if ([cellData.sideMenuItem.title isEqualToString:@"Terms and conditions"])
    {
        [self.sideMenuViewControllerDelegate sideMenuViewController:self userPressedTermsAndConditionsCellInSideMenuTableView:tableView];
    }
    else if ([cellData.sideMenuItem.title isEqualToString:@"About the app"])
    {
        [self.sideMenuViewControllerDelegate sideMenuViewController:self userPressedOnBoardingCellInSideMenuTableView:tableView];
    }
    else if ([cellData.sideMenuItem.title isEqualToString:@"Broadband speed test"]) {
        [self.sideMenuViewControllerDelegate sideMenuViewController:self userPressedSpeedTestCellInSideMenuTableView:tableView];
    }
    
}


#pragma mark - API calls to check open items related handler methods

- (void)checkUserDetailsAPICallFinishedSuccessfully
{
//    self.sideMenuHeaderTitleLabel.text = [NSString stringWithFormat:@"%@ %@", [AppDelegate sharedInstance].viewModel.app.loggedInUser.firstName, [AppDelegate sharedInstance].viewModel.app.loggedInUser.lastName];
}

- (void)checkActiveOrdersAPICallStarted
{
    [_sideMenuTableDataSource updateSideMenuTrackOrdersCellDataWithNeedsToShowIndicator:YES needsToShowOpenItems:NO andCellDataDictionary:nil];
    
    [_sideMenuTableView reloadData];
}

- (void)checkOpenFaultsAPICallStarted
{
    [_sideMenuTableDataSource updateSideMenuTrackFaultsCellDataWithNeedsToShowIndicator:YES needsToShowOpenItems:NO andCellDataDictionary:nil];
    
    [_sideMenuTableView reloadData];
}

- (void)checkReadyBillsAPICallStarted
{
    [_sideMenuTableDataSource updateSideMenuBillsCellDataWithNeedsToShowIndicator:YES needsToShowOpenItems:NO andCellDataDictionary:nil];
    
    [_sideMenuTableView reloadData];
}

- (void) checkDataFromHomeScreen{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSDictionary * dict = appDelegate.homeScreenData;
    
    [_sideMenuTableDataSource updateSideMenuTrackOrdersCellDataWithNeedsToShowIndicator:NO needsToShowOpenItems:YES andCellDataDictionary:dict];
     [_sideMenuTableDataSource updateSideMenuTrackFaultsCellDataWithNeedsToShowIndicator:NO needsToShowOpenItems:YES andCellDataDictionary:dict];
    
    [_sideMenuTableView reloadData];
}

- (void)checkActiveOrdersAPICallFinishedSuccessfully:(NSNotification *)notification
{
    
    NSDictionary *dict = notification.userInfo;
    [_sideMenuTableDataSource updateSideMenuTrackOrdersCellDataWithNeedsToShowIndicator:NO needsToShowOpenItems:YES andCellDataDictionary:dict];
   
    
    [_sideMenuTableView reloadData];
}

- (void)checkOpenFaultsAPICallFinishedSuccessfully:(NSNotification *)notification
{
    NSDictionary *dict = notification.userInfo;
    [_sideMenuTableDataSource updateSideMenuTrackFaultsCellDataWithNeedsToShowIndicator:NO needsToShowOpenItems:YES andCellDataDictionary:dict];
    
    [_sideMenuTableView reloadData];
}

- (void)checkReadyBillsAPICallFinishedSuccessfully:(NSNotification *)notification
{
    NSDictionary *dict = notification.userInfo;
    [_sideMenuTableDataSource updateSideMenuBillsCellDataWithNeedsToShowIndicator:NO needsToShowOpenItems:YES andCellDataDictionary:dict];
    
    [_sideMenuTableView reloadData];
}

- (void)checkActiveOrdersAPICallFailed
{
    [_sideMenuTableDataSource updateSideMenuTrackOrdersCellDataWithNeedsToShowIndicator:NO needsToShowOpenItems:NO andCellDataDictionary:nil];
    
    [_sideMenuTableView reloadData];
}

- (void)checkOpenFaultsAPICallFailed
{
    [_sideMenuTableDataSource updateSideMenuTrackFaultsCellDataWithNeedsToShowIndicator:NO needsToShowOpenItems:NO andCellDataDictionary:nil];
    
    [_sideMenuTableView reloadData];
}

- (void)checkReadyBillsAPICallFailed
{
    [_sideMenuTableDataSource updateSideMenuBillsCellDataWithNeedsToShowIndicator:NO needsToShowOpenItems:NO andCellDataDictionary:nil];
    
    [_sideMenuTableView reloadData];
}

- (void)checkActiveOrdersAPICallCancelled
{
    [_sideMenuTableDataSource updateSideMenuTrackOrdersCellDataWithNeedsToShowIndicator:NO needsToShowOpenItems:NO andCellDataDictionary:nil];
    
    [_sideMenuTableView reloadData];
}

- (void)checkOpenFaultsAPICallCancelled
{
    [_sideMenuTableDataSource updateSideMenuTrackFaultsCellDataWithNeedsToShowIndicator:NO needsToShowOpenItems:NO andCellDataDictionary:nil];
    
    [_sideMenuTableView reloadData];
}

- (void)checkReadyBillsAPICallCancelled
{
    [_sideMenuTableDataSource updateSideMenuBillsCellDataWithNeedsToShowIndicator:NO needsToShowOpenItems:NO andCellDataDictionary:nil];
    
    [_sideMenuTableView reloadData];
}

@end

