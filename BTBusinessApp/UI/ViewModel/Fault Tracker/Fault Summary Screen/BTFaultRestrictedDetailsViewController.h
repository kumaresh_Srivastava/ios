//
//  BTFaultRestrictedDetailsViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 16/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTFaultRestrictedDetailsViewController : UIViewController

@property (nonatomic, strong) NSString *faultRef;
@property (nonatomic) BOOL networkRequestInProgress;

@end
