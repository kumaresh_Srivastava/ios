//
//  BTFaultRestrictedDetailsViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 16/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultRestrictedDetailsViewController.h"
#import "DLMFaultSummaryScreen.h"
#import "BTRetryView.h"
#import "CustomSpinnerView.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "AppConstants.h"
#import "BTFault.h"
#import "BTFaultDetailsMilestoneViewController.h"
#import "DLMFaultDetailsScreen.h"
#import "BTFault.h"
#import "BTCustomInputAlertView.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "NLWebServiceError.h"
#import "BTNoFaultSummaryView.h"
#import "BTHelpAndSupportViewController.h"
#import "BTInAppBrowserViewController.h"
#import "BTNavigationViewController.h"
#import "BTOrderStatusLabel.h"

@interface BTFaultRestrictedDetailsViewController () <DLMFaultSummaryScreenDelegate, BTRetryViewDelegate,BTCustomInputAlertViewDelegate,BTNoFaultSummaryViewDelegate> {
    CustomSpinnerView *_loadingView;
    BTRetryView *_retryView;
   
    BOOL _isAPICallForFaultDetails;
}


@property (nonatomic, readwrite) DLMFaultSummaryScreen *viewModel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintAccountNumberViewHeight;

@property (weak, nonatomic) IBOutlet UIView *faultTopView;
@property (weak, nonatomic) IBOutlet UIView *viewFullDetailsView;
@property (weak, nonatomic) IBOutlet UIView *accountNumberView;
@property (weak, nonatomic) IBOutlet UIButton *accountNumberButton;
@property (weak, nonatomic) IBOutlet UIImageView *faultTypeImageView;
@property (weak, nonatomic) IBOutlet UILabel *faultReferenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *faultDescriptionLabel;
@property (weak, nonatomic) IBOutlet BTOrderStatusLabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *reportedOnLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *viewFullDetailsButton;

@property (strong,nonatomic)  BTCustomInputAlertView *customlAlertView;


@end

@implementation BTFaultRestrictedDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // (LP) Navigation bar appearence
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

     self.edgesForExtendedLayout = UIRectEdgeNone;

    // (lp) setup viewModel for fault summary screen
    self.viewModel = [[DLMFaultSummaryScreen alloc] init];
    self.viewModel.faultSummaryScreenDelegate = self;
    
    __weak typeof(self) weakSelf = self;
    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([weakSelf networkRequestInProgress]) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
            
        }
        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
            
        }
    }];
    
    [self initialSetupForFaultSummaryScreen];
    [self fetchFaultSummaryDetails];
    
    
    
    [self trackOmniPage:OMNIPAGE_FAULT_LOCKED withFaultRef:self.faultRef];
    // (lp) Update UI with fault summary data
    //[self updateUserInterface];
    
}

- (void)viewWillDisappear:(BOOL)animated {

    [super viewWillDisappear:animated];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    self.networkRequestInProgress = NO;
    
    _customlAlertView.delegate = nil;
    [_customlAlertView removeFromSuperview];
    [self.viewModel cancelSummaryAPI];
    [self.viewModel cancelDetailsAPI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI setup related methods

- (void)initialSetupForFaultSummaryScreen
{
    
    // (LP) initial setup for UI items
    self.title = @"Fault summary";
    
    [self.viewFullDetailsButton.layer setBorderColor:[BrandColours colourBackgroundBTPurplePrimaryColor].CGColor];
    self.viewFullDetailsButton.layer.borderWidth = 1.0f;
    self.viewFullDetailsButton.layer.cornerRadius = 5.0f;
    
    [self createLoadingView];
    
    [self hideorShowControls:YES];
}

/*- (void)redirectToFaultDetailsMileStoneControllerWithViewModel:(DLMFaultDetailsScreen *)faultViewModel {
 
    BTFaultDetailsMilestoneViewController *faultDetailsMilestoneViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"faultDetailsMileStoneScene"];
    
    [faultDetailsMilestoneViewController setBacNeeded:YES];
    faultDetailsMilestoneViewController.viewModel = faultViewModel;
    
    [self.navigationController pushViewController:faultDetailsMilestoneViewController animated:YES];
}*/




#pragma mark - action methods
- (IBAction)viewFullDetailsButtonAction:(id)sender {
    if (!self.viewModel.isAuthenticated) {
        [self displayEnterAccountNumberViewWithErrorMessageHidden:YES];
    }
    else
    {
        BTFaultDetailsMilestoneViewController *faultDetailsMilestoneViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"faultDetailsMileStoneScene"];
        
        faultDetailsMilestoneViewController.faultReference = self.viewModel.fault.faultReference;
        faultDetailsMilestoneViewController.assetId = self.viewModel.fault.assetId;
        DLMFaultDetailsScreen *detailsViewModel = [[DLMFaultDetailsScreen alloc] init];
        detailsViewModel.productGroup = self.viewModel.productGroup;
        detailsViewModel.productIcon = self.viewModel.productImage;
        detailsViewModel.reportedOnDate = self.viewModel.fault.reportedOnDate;
        
        faultDetailsMilestoneViewController.faultDetailsViewModel = detailsViewModel;
        
        faultDetailsMilestoneViewController.bacNeeded = NO;
        
        [self.navigationController pushViewController:faultDetailsMilestoneViewController animated:YES];
    }
}


- (IBAction)enterAccountNumberButtonAction:(id)sender {
    [self displayEnterAccountNumberViewWithErrorMessageHidden:YES];
}

#pragma mark - Fetch fault summary details methods
- (void)fetchFaultSummaryDetails {
    
    if(![AppManager isInternetConnectionAvailable])
    {
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_FAULT_LOCKED];
    }
    else
    {
        // (LP) Ask to viewModel to provide fault summary details. The viewModel takes care of fetching data.
        self.networkRequestInProgress = YES;
        [self hideLoadingItems:NO];
        [self hideRetryItems:YES];
        [_loadingView startAnimatingLoadingIndicatorView];
        
        [self.viewModel fetchFaultSummaryForFaultReference:self.faultRef];
    }
}


- (void)callTrackFaultDetailsAPIWithFaultId:(NSString *)faultId withAssetId:(NSString *)assetId withBAC:(NSString *)bac {
    
    _customlAlertView.frame = CGRectMake(0,64, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-64);
    
    self.networkRequestInProgress = YES;
    // [self hideLoadingItems:NO];
    // [_loadingView startAnimatingLoadingIndicatorView];
    self.viewModel.faultDetailModel = nil;
    self.viewModel.faultSummaryScreenDelegate = self;
    [self.viewModel fetchFaultDetailsForFaultReference:faultId assetId:assetId andBAC:bac];
}



#pragma mark - Private helper methods

/*
 Updates the UI after getting user details
 */
- (void)updateUserInterface {
    
    if (self.viewModel.isAuthenticated) {
        self.accountNumberView.hidden = YES;
        self.constraintAccountNumberViewHeight.constant = 0;
        [self.view layoutIfNeeded];
    }
    
//    // (LP) Update status color according to fault status
//    if ([self.viewModel.fault.colourwithStatus isEqualToString:@"status-green"]) {
//        self.statusLabel.textColor = [BrandColours colourMyBtGreen];
//    } else if ([self.viewModel.fault.colourwithStatus isEqualToString:@"status-orange"]) {
//        self.statusLabel.textColor = [BrandColours colourMyBtOrange];
//    } else {
//        self.statusLabel.textColor = [BrandColours colourMyBtRed];
//    }
    
    //self.faultReferenceLabel.text = [NSString stringWithFormat:@"Ref: %@",self.viewModel.fault.faultReference];
    self.faultReferenceLabel.text = self.viewModel.fault.faultReference;
    self.faultDescriptionLabel.text = self.viewModel.productGroup;
    self.reportedOnLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:self.viewModel.fault.reportedOnDate];
    self.phoneNumberLabel.text = self.viewModel.fault.serviceID;
    
    if ([self.viewModel.fault.status rangeOfString:@"Fault" options:NSCaseInsensitiveSearch].location == 0) {
        //self.statusLabel.text = [NSString stringWithFormat:@"%@",[[self.viewModel.fault.status substringFromIndex:6] capitalizedString]];
        [self.statusLabel setOrderStatus:[NSString stringWithFormat:@"%@",[[self.viewModel.fault.status substringFromIndex:6] capitalizedString]]];
    } else
    {
        //self.statusLabel.text = [self.viewModel.fault.status capitalizedString];
        [self.statusLabel setOrderStatus:[self.viewModel.fault.status capitalizedString]];
    }
    
    self.faultTypeImageView.image = [UIImage imageNamed:[self.viewModel getImageWithProductName]];
    
    [self hideorShowControls:NO];
    [self hideRetryItems:YES];
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
}


- (void) hideorShowControls:(BOOL)isHide {
    
    // (lp) Hide UI elements during loading time
    self.faultTopView.hidden = isHide;
    self.viewFullDetailsView.hidden = isHide;
    
}

- (void)showRetryViewWithInternetStrip:(BOOL)needToShowInternetStrip {
    
    if(!_retryView)
    {
        _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
        _retryView.translatesAutoresizingMaskIntoConstraints = NO;
        _retryView.retryViewDelegate = self;
        [_retryView updateRetryViewWithInternetStrip:needToShowInternetStrip];
        [self.view addSubview:_retryView];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];

    }
    
    _retryView.hidden = NO;
    _retryView.retryViewDelegate = self;
    [_retryView updateRetryViewWithInternetStrip:needToShowInternetStrip];
}


- (void)zeroStateView
{
 
   BTNoFaultSummaryView * _emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTNoFaultSummaryView" owner:nil options:nil] objectAtIndex:0];
    _emptyDashboardView.delegate = self;
    _emptyDashboardView.frame = CGRectMake(0, 0,self.view.frame.size.width , self.view.frame.size.height);
    
    [_emptyDashboardView updateWarningLabelWithMessage:@"No fault found"];
    
    [self.view addSubview:_emptyDashboardView];
}


- (void)createLoadingView {
    
    _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
    _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:_loadingView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}


- (void)hideRetryItems:(BOOL)isHide {
    
    // (lp) Hide or Show UI elements related to retry.
    [_retryView setRetryViewDelegate:nil];
    [_retryView setHidden:isHide];
    
}

- (void)hideLoadingItems:(BOOL)isHide {
    
    // (lp) Hide or Show UI elements related to loading.
    [_loadingView setHidden:isHide];
    
}

- (void)displayEnterAccountNumberViewWithErrorMessageHidden:(BOOL)hideErrorMessage
{
    
    [self trackOmniClick:OMNICLICK_FAULT_VIEW_FULL_DETAILS forPage:OMNIPAGE_FAULT_LOCKED withFaultRef:self.faultRef];
    
    _customlAlertView = [[[NSBundle mainBundle] loadNibNamed:@"BTCustomInputAlertView" owner:nil options:nil] firstObject];
    _customlAlertView.frame = CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    _customlAlertView.delegate = self;
    
    [_customlAlertView updateCustomAlertViewWithTitle:@"Enter account number"  andMessage:@"Please confirm this is your fault with your account number. It's on the top of your bill"];
    [_customlAlertView updateTextFieldPlaceholderText:@"For example AB12345678" andIsSecureEntry:NO andNeedToShowRightView:NO];
    
    
    [_customlAlertView updateAlertButtonTitleForActionButton:@"View details" andCancelButtonTitle:@"Cancel"];
    
    [_customlAlertView makeTitleLabelForTextFieldWithTitle:@"Account number"];
    
    [_customlAlertView updateLoadingMessage:@"Validating account number"];
    
    if(_customlAlertView)
        [_customlAlertView showKeyboard];
    
    
    [[[UIApplication sharedApplication] keyWindow] addSubview:_customlAlertView];
    
}


- (void)removeCustomAlertViewFromWindow
{
    [_customlAlertView stopLoading];
    [_customlAlertView deRegisterKeyboardNotification];
    [_customlAlertView removeFromSuperview];
    _customlAlertView = nil;
}


- (void)doValidationOnBAC:(NSString *)accountNumber
{
    if ([accountNumber length] == 10) {
        
        if(![AppManager isInternetConnectionAvailable])
        {
            [_customlAlertView showErrorOnCustomInputWithWithErrro:kNoConnectionMessage];
            [AppManager trackNoInternetErrorOnPage:OMNIPAGE_FAULT_LOCKED];
            return;
        }
        
        _isAPICallForFaultDetails = YES;
        
        [_customlAlertView removeErrorMeesageFromCustomInputAlertView];
        [_customlAlertView startLoading];
        [self callTrackFaultDetailsAPIWithFaultId:self.viewModel.fault.faultReference withAssetId:self.viewModel.fault.assetId withBAC:accountNumber];
    } else {
        [_customlAlertView showErrorOnCustomInputWithWithErrro:@"Invalid account number"];
    }
}

- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }]];
    
    [self presentViewController:alertController animated:YES completion:^{
        
    }];
}

#pragma mark - BTNO Fault view delegate
- (void)noFaultSummaryView:(BTNoFaultSummaryView *)noFaultSummaryView reportNewFaultAction:(id)sender{
    
    if ([AppManager isInternetConnectionAvailable])
    {
        _networkRequestInProgress = YES;
        [self hideLoadingItems:NO];
        [_loadingView startAnimatingLoadingIndicatorView];
        [self.view bringSubviewToFront:_loadingView];
        [self.viewModel checkForLiveChatAvailibilityForFault];
    }
    else
    {
        [self showAlertWithTitle:kNoConnectionTitle andMessage:kNoConnectionMessage];
    }
    
}


#pragma mark - BTRetryView delegate methods
- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView {
    
    [self fetchFaultSummaryDetails];
}


#pragma mark - DLMFaultSummaryScreenDelegate Methods

- (void)successfullyFetchedFaultSummaryDataOnFaultSummaryScreen:(DLMFaultSummaryScreen *)faultSummaryScreen
{
    self.networkRequestInProgress = NO;
    
    if (self.viewModel.fault == nil) {
        [self hideRetryItems:NO];
        [self hideLoadingItems:YES];
        [_loadingView stopAnimatingLoadingIndicatorView];
        
    }
    else
    {
        if (self.viewModel.fault.faultReference) {
            
            // (lp) Update UI with fault summary data
            [self updateUserInterface];
            
        } else {
            
            // (lp) Retry to fetch fault details
            [self hideRetryItems:NO];
            [self hideLoadingItems:YES];
            [_loadingView stopAnimatingLoadingIndicatorView];
            
        }
    }
    
}


- (void)faultSummaryScreen:(DLMFaultSummaryScreen *)faultSummaryScreen failedToFetchFaultSummaryDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    self.networkRequestInProgress = NO;
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
    
    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {
        
        switch (webServiceError.error.code)
        {
                
            case BTNetworkErrorCodeAPINoDataFound:
            {
                [AppManager trackNoDataFoundErrorOnPage:OMNIPAGE_FAULT_LOCKED];
                [self zeroStateView];
                errorHandled = YES;
                break;
            }
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }
    
    if(errorHandled == NO)
    {
        [self showRetryViewWithInternetStrip:NO];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_FAULT_LOCKED];
    }
    
}

#pragma mark - Fault full detail API delegate
- (void)successfullyFetchedFaultDetailsDataOnFaultSummaryScreen:(DLMFaultSummaryScreen *)faultSummaryScreen
{
    self.networkRequestInProgress = NO;
    [_customlAlertView stopLoading];
    [_loadingView stopAnimatingLoadingIndicatorView];
    if (self.viewModel.faultDetailModel.fault != nil) {
        
        [_customlAlertView deRegisterKeyboardNotification];
        [_customlAlertView removeFromSuperview];
        _customlAlertView = nil;
        
        [self trackContentViewedForPage:OMNIPAGE_FAULT_LOCKED andNotificationPopupDetails:@"Account number validated: OK"];
        
        // (lp) Hide loading view
        [self hideLoadingItems:YES];
        
        BTFaultDetailsMilestoneViewController *faultDetailsMilestoneViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"faultDetailsMileStoneScene"];
        
        faultDetailsMilestoneViewController.faultReference = self.viewModel.fault.faultReference;
        faultDetailsMilestoneViewController.assetId = self.viewModel.fault.assetId;
        faultDetailsMilestoneViewController.faultDetailsViewModel = self.viewModel.faultDetailModel;
        faultDetailsMilestoneViewController.bacNeeded = YES;
        
        [self.navigationController pushViewController:faultDetailsMilestoneViewController animated:YES];
        
    }
    else
    {
        _customlAlertView.frame = CGRectMake(0,0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
        
        [OmnitureManager trackError:OMNIERROR_INVALID_BAC_NUMBER onPageWithName:OMNIPAGE_FAULT_LOCKED contextInfo:[AppManager getOmniDictionary]];
        
        [_customlAlertView showErrorOnCustomInputWithWithErrro:@"The account number doesn't belong to this fault"];
    }
}


- (void)faultSummaryScreen:(DLMFaultSummaryScreen *)faultSummaryScreen failedToFetchFaultDetailsDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    self.networkRequestInProgress = NO;
    [_customlAlertView stopLoading];
    [_loadingView stopAnimatingLoadingIndicatorView];
    BOOL needToOpenAnotherScreen = NO;
    
    if([webServiceError.sourceError.error.domain isEqualToString:BTNetworkErrorDomain] && webServiceError.sourceError.error.code == BTNetworkErrorCodeSMSessionUnauthenticaiton)
    {
        switch (webServiceError.error.code)
        {
            case BTNetworkErrorCodeVordelParameterMissing:
            case BTNetworkErrorCodeVordelParameterInvalid:
            case BTNetworkErrorCodeVordelUserUnauthenticated:
            case BTNetworkErrorCodeVordelUnplannedServiceOutage:
            case BTNetworkErrorCodeVordelPlannedServiceOutage:
            case BTNetworkErrorCodeVordelAccountLocked:
            case BTNetworkErrorCodeVordelDeviceMismatch:
            {
                needToOpenAnotherScreen = YES;
                break;
            }
                
            default:
            {
               needToOpenAnotherScreen = NO;
                break;
            }
        }
    }
    
    
    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && needToOpenAnotherScreen == NO)
    {
    switch (webServiceError.error.code)
        {
            case BTNetworkErrorCodeAPIUserProfileLocked:
            case BTNetworkErrorCodeSMSessionUnauthenticaiton:
            {
                needToOpenAnotherScreen = YES;;
                break;
            }
            default:
            {
                needToOpenAnotherScreen = NO;
                break;
            }
        }
    }
    else
    {
        [self removeCustomAlertViewFromWindow];
    }
    if(needToOpenAnotherScreen)
    {
      [self removeCustomAlertViewFromWindow];
    }
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
    
    if (errorHandled == NO && [webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && webServiceError.error.code == BTNetworkErrorCodeAPINoDataFound) {
        _customlAlertView.frame = CGRectMake(0,0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
        
        [OmnitureManager trackError:OMNIERROR_INVALID_BAC_NUMBER onPageWithName:OMNIPAGE_FAULT_LOCKED contextInfo:[AppManager getOmniDictionary]];
        
        [_customlAlertView showErrorOnCustomInputWithWithErrro:@"The account number doesn't belong to this fault"];
    }
    
    else if(errorHandled == NO)
    {
        _customlAlertView.frame = CGRectMake(0,0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
        
        [_customlAlertView showErrorOnCustomInputWithWithErrro:kDefaultErrorMessage];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_FAULT_LOCKED];
    }
    
}



- (void)successfullyCancelledFaultDetailsDataOnFaultSummaryScreen:(DLMFaultSummaryScreen *)faultSummaryScreen
{
    //ANother class delegate need to be removed or make it optional(RM)
}


- (void)faultSummaryScreen:(DLMFaultSummaryScreen *)faultSummaryScreen failedToCancelFaultSummaryWithWebServiceError:(NLWebServiceError *)webServiceError
{
   //ANother class delegate need to be removed make it optional(RM)
}

- (void)liveChatAvailableForFaultWithScreen:(DLMFaultSummaryScreen *)faultSummaryScreen
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
    
    viewController.targetURLType = BTTargetURLTypeFaultsChatScreen;
    
    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:NO andIsNeedCloseButton:YES];
    
    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{
        
    }];
    
}

- (void)faultDetailsScreen:(DLMFaultSummaryScreen *)faultSummaryScreen liveChatCurrentlyNotAvailableWithSpecificCategory:(NSArray *)specificCategory generalCategory:(NSArray *)generalCategory andCurrentTime:(NSString *)currentTime
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BTHelpAndSupportViewController *helpAndSupportController = [BTHelpAndSupportViewController getHelpAndSupportViewController];
    helpAndSupportController.isLaunchingFromHomeScreen = NO;
    helpAndSupportController.isDataAlreadyFetched = YES;
    helpAndSupportController.specificCategory = specificCategory;
    helpAndSupportController.generalCategory = generalCategory;
    helpAndSupportController.currentTime = currentTime;
    
    [self.navigationController pushViewController:helpAndSupportController animated:YES];
}

- (void)faultDetailsScreen:(DLMFaultSummaryScreen *)faultSummaryScreen failedToCheckLiveChatAvailabilityWithWebServiceError:(NLWebServiceError *)error
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if(errorHandled == NO)
    {
        [self showAlertWithTitle:@"Message" andMessage:kDefaultErrorMessage];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_FAULT_LOCKED];
    }
    
}

#pragma mark - Custom Input alert view delegate

- (void)userDidPressConfirmOnCustomInputAlertView:(BTCustomInputAlertView *)alertView withPassword:(NSString *)passwordText
{
     [self trackOmniClick:OMNICLICK_FAULT_VIEW_FULL_DETAILS forPage:OMNIPAGE_FAULT_LOCKED withFaultRef:self.faultRef];
    [self trackContentViewedForPage:OMNIPAGE_FAULT_LOCKED andNotificationPopupDetails:@"Enter account number: View details"];
    
    
     [self doValidationOnBAC:passwordText];
}

- (void)userDidPressCancelOnCustomInputAlertView:(BTCustomInputAlertView *)alertView
{
    [self trackContentViewedForPage:OMNIPAGE_FAULT_LOCKED andNotificationPopupDetails:@"Enter account number: Cancel"];
    [_customlAlertView deRegisterKeyboardNotification];
    [_customlAlertView removeFromSuperview];
    _customlAlertView = nil;
   

}

#pragma mark - Event tracking methods
- (void)trackOmniPage:(NSString *)page  withFaultRef:(NSString *)faultRef
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:[AppManager getOmniDictionary]];
    
    [params removeObjectForKey:kOmniLoginStatus];
    if(faultRef && faultRef.length>0)
    {
        NSString *ref = [NSString stringWithFormat:@"%@%@",kOmniFaultRefFormat,faultRef];
        [params setValue:ref forKey:kOmniFaultRef];
    }
    
    [OmnitureManager trackPage:page withContextInfo:params];
}



- (void)trackOmniClick:(NSString *)clickEvent forPage:(NSString *)page withFaultRef:(NSString *)faultRef
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:[AppManager getOmniDictionary]];
    NSString *pageName = page;
    NSString *linkTitle = clickEvent;
      [params removeObjectForKey:kOmniLoginStatus];
    if(faultRef && faultRef.length>0)
    {
        NSString *ref = [NSString stringWithFormat:@"%@%@",kOmniFaultRefFormat,faultRef];
        [params setValue:ref forKey:kOmniFaultRef];
    }
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:params];
}

- (void)trackContentViewedForPage:(NSString *)page andNotificationPopupDetails:(NSString *)notificationPopup
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = page;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    NSString *notificaiton = [NSString stringWithFormat:@"%@%@",STATE_PREFIX,notificationPopup];
    [data setValue:notificaiton forKey:kOmniContentViewed];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
    
}



@end
