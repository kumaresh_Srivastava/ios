//
//  DLMFaultSummaryScreen.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 16/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"

@class DLMFaultSummaryScreen;
@class DLMFaultDetailsScreen;
@class BTFault;
@class NLWebServiceError;

@protocol DLMFaultSummaryScreenDelegate <NSObject>

- (void)successfullyFetchedFaultSummaryDataOnFaultSummaryScreen:(DLMFaultSummaryScreen *)faultSummaryScreen;

- (void)faultSummaryScreen:(DLMFaultSummaryScreen *)faultSummaryScreen failedToFetchFaultSummaryDataWithWebServiceError:(NLWebServiceError *)webServiceError;

- (void)successfullyFetchedFaultDetailsDataOnFaultSummaryScreen:(DLMFaultSummaryScreen *)faultSummaryScreen;

- (void)faultSummaryScreen:(DLMFaultSummaryScreen *)faultSummaryScreen failedToFetchFaultDetailsDataWithWebServiceError:(NLWebServiceError *)webServiceError;

- (void)successfullyCancelledFaultDetailsDataOnFaultSummaryScreen:(DLMFaultSummaryScreen *)faultSummaryScreen;

- (void)faultSummaryScreen:(DLMFaultSummaryScreen *)faultSummaryScreen failedToCancelFaultSummaryWithWebServiceError:(NLWebServiceError *)webServiceError;

- (void)liveChatAvailableForFaultWithScreen:(DLMFaultSummaryScreen *)faultSummaryScreen;
- (void)faultDetailsScreen:(DLMFaultSummaryScreen *)faultSummaryScreen liveChatCurrentlyNotAvailableWithSpecificCategory:(NSArray *)specificCategory generalCategory:(NSArray *)generalCategory andCurrentTime:(NSString *)currentTime;
- (void)faultDetailsScreen:(DLMFaultSummaryScreen *)faultSummaryScreen failedToCheckLiveChatAvailabilityWithWebServiceError:(NLWebServiceError *)error;

@end

@interface DLMFaultSummaryScreen : DLMObject {
    
}

@property (nonatomic, strong) BTFault *fault;
@property (nonatomic, assign) BOOL isAuthenticated;
@property (nonatomic, copy) NSString *productGroup;
@property (nonatomic, copy) NSString *productImage;
@property (nonatomic, weak) id <DLMFaultSummaryScreenDelegate> faultSummaryScreenDelegate;

@property (nonatomic, strong) DLMFaultDetailsScreen *faultDetailModel;

- (void)fetchFaultSummaryForFaultReference:(NSString *)faultRef;
- (void)fetchFaultDetailsForFaultReference:(NSString *)faultRef assetId:(NSString *)assetId andBAC:(NSString *)BAC;
- (NSString *)getImageWithProductName;
- (void)cancelSummaryAPI;
- (void)cancelDetailsAPI;
- (void)cancelChatAvailabilityCheckerAPI;

- (void)checkForLiveChatAvailibilityForFault;

@end
