//
//  DLMFaultSummaryScreen.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 16/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMFaultSummaryScreen.h"
#import "NLGetFaultSummaryWebService.h"
#import "NLFaultDetailsWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "AppConstants.h"
#import "BTFault.h"
#import "CDRecentSearchedFault.h"
#import "DLMFaultDetailsScreen.h"
#import "BTLiveChatAvailabilityChecker.h"

@interface DLMFaultSummaryScreen () <NLGetFaultSummaryWebServiceDelegate, NLFaultDetailsWebServiceDelegate, BTLiveChatAvailabilityCheckerDelegate> {

    BTLiveChatAvailabilityChecker *_liveChatAvailabityChecker;
}

@property (nonatomic) NLGetFaultSummaryWebService *getFaultSummaryWebService;
@property (nonatomic) NLFaultDetailsWebService *getFaultDetailsWebService;

@end

@implementation DLMFaultSummaryScreen

- (void)fetchFaultSummaryForFaultReference:(NSString *)faultRef
{
    [self setupToFetchFaultSummaryForFaultReference:faultRef];
    [self fetchFaultSummaryForCurrentFault];
}

- (void) setupToFetchFaultSummaryForFaultReference:(NSString *) faultRef {
    
    self.getFaultSummaryWebService = [[NLGetFaultSummaryWebService alloc] initWithFaultRef:faultRef];
    self.getFaultSummaryWebService.getFaultSummaryWebServiceDelegate = self;
}

- (void) fetchFaultSummaryForCurrentFault {
    
    [self.getFaultSummaryWebService resume];
}

- (void)fetchFaultDetailsForFaultReference:(NSString *)faultRef assetId:(NSString *)assetId andBAC:(NSString *)BAC
{
    [self setupToFetchFaultDetailsForFaultReference:faultRef assetId:assetId andBAC:BAC];
    [self fetchFaultDetails];
}

- (void) setupToFetchFaultDetailsForFaultReference:(NSString *)faultRef assetId:(NSString *)assetId andBAC:(NSString *)BAC {
    
    self.getFaultDetailsWebService = [[NLFaultDetailsWebService alloc] initWithFaultID:faultRef assetID:assetId andBac:BAC];
    self.getFaultDetailsWebService.getFaultDetailsWebServiceDelegate = self;
}

- (void) fetchFaultDetails {
    
    [self.getFaultDetailsWebService resume];
}

#pragma mark Public Methods

- (NSString *)getImageWithProductName
{
    NSString *image = nil;

    if ([self.productImage isEqualToString:@"#icon-telephone"]) {
        image = @"pstn_fault";
    } else if ([self.productImage isEqualToString:@"#icon-broadband-hub"]) {
        image = @"broadband_fault";
    } else if ([self.productImage isEqualToString:@"#icon-isdn"]) {
        image = @"pstn_fault";
    } else if ([self.productImage isEqualToString:@"#icon-email"]) {
        image = @"broadband_fault";
    }

    return image;
}

- (void)checkForLiveChatAvailibilityForFault
{
    [self setupToCheckForLiveChatAvailibilityForFault];
    [self checkFaultLiveChatAvailability];
}

- (void) setupToCheckForLiveChatAvailibilityForFault {
    
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = [[BTLiveChatAvailabilityChecker alloc] init];
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = self;
}

- (void) checkFaultLiveChatAvailability {
    
    [_liveChatAvailabityChecker getLiveChatAvailableSlots];
}

#pragma mark CancellingMethods

- (void)cancelSummaryAPI {
    self.getFaultSummaryWebService.getFaultSummaryWebServiceDelegate = nil;
    [self.getFaultSummaryWebService cancel];
    self.getFaultSummaryWebService = nil;
}

- (void)cancelDetailsAPI {
    self.getFaultDetailsWebService.getFaultDetailsWebServiceDelegate = nil;
    [self.getFaultDetailsWebService cancel];
    self.getFaultDetailsWebService = nil;
}

- (void)cancelChatAvailabilityCheckerAPI
{
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    [_liveChatAvailabityChecker cancel];
    _liveChatAvailabityChecker = nil;
}

#pragma mark - NLGetFaultSummaryWebServiceDelegate Methods

- (void)getFaultSummaryWebService:(NLGetFaultSummaryWebService *)webService successfullyFetchedFaultSummaryData:(BTFault *)fault isAuthenticated:(BOOL)isAuthenticated productGroup:(NSString *)productGroup productImage:(NSString *)productImage
{
    if(webService == _getFaultSummaryWebService)
    {
        if (fault != nil) {
            _fault = fault;
            _isAuthenticated = isAuthenticated;
            _productGroup = productGroup;
            _productImage = productImage;

            // (LP) Save the latest data from web service into the persistence for recently searched.

            DDLogInfo(@"Saving data from response of NLGetFaultSummaryWebServiceDelegate API Call into persistence in recent searched items for fault %@.", fault.faultReference);

            NSManagedObjectContext *context = [AppDelegate sharedInstance].managedObjectContext;
            CDUser *persistenceUser = [CDUser userWithUsername:[AppDelegate sharedInstance].viewModel.app.loggedInUser.username inManagedObjectContext:context];

            NSArray *arrayOfRecentSearchedFaults = [persistenceUser.recentlySearchedFaults allObjects];

            CDRecentSearchedFault *searchedFault = nil;
            for (CDRecentSearchedFault *recentSearchedFault in arrayOfRecentSearchedFaults) {
                if ([recentSearchedFault.faultRef isEqualToString:fault.faultReference]) {
                    searchedFault = recentSearchedFault;
                }
            }

            if (searchedFault == nil) {
                searchedFault = [CDRecentSearchedFault newRecentSearchedFaultInManagedObjectContext:context];
                searchedFault.faultRef = fault.faultReference;
                searchedFault.faultStatus = fault.status;
                searchedFault.faultDescription = productGroup;
                searchedFault.reportedOnDate = fault.reportedOnDate;
                searchedFault.colourWithStatus = fault.colourwithStatus;
                searchedFault.lastSearchedDate = [NSDate date];

                [persistenceUser addRecentlySearchedFaultsObject:searchedFault];
            }
            else
            {
                searchedFault.faultStatus = fault.status;
                searchedFault.lastSearchedDate = [NSDate date];

                [persistenceUser addRecentlySearchedFaultsObject:searchedFault];
            }

            [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];

        }

        [self.faultSummaryScreenDelegate successfullyFetchedFaultSummaryDataOnFaultSummaryScreen:self];
        self.getFaultSummaryWebService.getFaultSummaryWebServiceDelegate = nil;
        self.getFaultSummaryWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLGetFaultSummaryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLGetFaultSummaryWebService but this object is not the one stored in member variable of this class %@.", [self class]);

    }

}

- (void)getFaultSummaryWebService:(NLGetFaultSummaryWebService *)webService failedToFetchFaultSummaryDataWithWebServiceError:(NLWebServiceError *)error
{
    if(webService == _getFaultSummaryWebService)
    {
        [self.faultSummaryScreenDelegate faultSummaryScreen:self failedToFetchFaultSummaryDataWithWebServiceError:error];
        self.getFaultSummaryWebService.getFaultSummaryWebServiceDelegate = nil;
        self.getFaultSummaryWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of faliure of an object of NLGetFaultSummaryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of faliure of an object of NLGetFaultSummaryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

#pragma mark - NLFaultDetailsWebServiceDelegate Methods

- (void)getFaultDetailsWebService:(NLFaultDetailsWebService *)webService successfullyFetchedFaultDetailsData:(BTFault *)fault milestoneNodes:(NSArray *)mileStoneNodes
{
    if(webService == _getFaultDetailsWebService)
    {
        if (fault != nil) {

            _faultDetailModel = [[DLMFaultDetailsScreen alloc] init];
            [fault updateReportedOnDate:self.fault.reportedOnDate];
            _faultDetailModel.fault = fault;
            _faultDetailModel.arrayOfMilestoneNodes = mileStoneNodes;
            _faultDetailModel.productGroup = _productGroup;
            _faultDetailModel.productIcon = _productImage;

        }


        if([webService isKindOfClass:[NLFaultDetailsCancelWebService class]]){

            [self.faultSummaryScreenDelegate successfullyCancelledFaultDetailsDataOnFaultSummaryScreen:self];
        }
        else{

            [self.faultSummaryScreenDelegate successfullyFetchedFaultDetailsDataOnFaultSummaryScreen:self];
        }
        
        self.getFaultDetailsWebService.getFaultDetailsWebServiceDelegate = nil;
        self.getFaultDetailsWebService = nil;
    }
    else
    {
        
        if([webService isKindOfClass:[NLFaultDetailsCancelWebService class]]){
            
            DDLogError(@"This delegate method gets called because of success of an object of NLFaultDetailsCancelWebService but this object is not the one stored in member variable of this class %@.", [self class]);
            NSAssert(NO, @"This delegate method gets called because of success of an object of NLFaultDetailsCancelWebService but this object is not the one stored in member variable of this class %@.", [self class]);

        }
        else{
            
            DDLogError(@"This delegate method gets called because of success of an object of NLFaultDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
            NSAssert(NO, @"This delegate method gets called because of success of an object of NLFaultDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        }
        

    }


}

- (void)getFaultDetailsWebService:(NLFaultDetailsWebService *)webService failedToFetchFaultDetailsDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    if(webService == _getFaultDetailsWebService)
    {
        
        if([webService isKindOfClass:[NLFaultDetailsCancelWebService class]]){
            
            [self.faultSummaryScreenDelegate faultSummaryScreen:self failedToCancelFaultSummaryWithWebServiceError:webServiceError];
        }
        else{
            
            [self.faultSummaryScreenDelegate faultSummaryScreen:self failedToFetchFaultDetailsDataWithWebServiceError:webServiceError];

        }

        self.getFaultDetailsWebService.getFaultDetailsWebServiceDelegate = nil;
        self.getFaultDetailsWebService = nil;
    }
    else
    {
        
        if([webService isKindOfClass:[NLFaultDetailsCancelWebService class]]){
            
            
            DDLogError(@"This delegate method gets called because of faliure of an object of NLFaultDetailsCancelWebService but this object is not the one stored in member variable of this class %@.", [self class]);
            NSAssert(NO, @"This delegate method gets called because of faliure of an object of NLFaultDetailsCancelWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        }
        else{
            
            DDLogError(@"This delegate method gets called because of faliure of an object of NLFaultDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
            NSAssert(NO, @"This delegate method gets called because of faliure of an object of NLFaultDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        }
    }
}

#pragma mark - BTLiveChatAvailabilityCheckerDelegate Methods
- (void)liveChatAvailabilityChecker:(BTLiveChatAvailabilityChecker *)liveChatAvailabilityChecker successfullyFetchedSlotsWithGeneralCategory:(NSArray *)generalCategoryArray specificeCategory:(NSArray *)specificCategory andCurrentTime:(NSString *)currentTime
{
    BOOL chatAvailable = [BTLiveChatAvailabilityChecker checkAvailabilityWithSpecificCategory:specificCategory currentTime:currentTime andChatType:BTChatTypeFault];
    
    if (chatAvailable)
    {
        [self.faultSummaryScreenDelegate liveChatAvailableForFaultWithScreen:self];
    }
    else
    {
        [self.faultSummaryScreenDelegate faultDetailsScreen:self liveChatCurrentlyNotAvailableWithSpecificCategory:specificCategory generalCategory:generalCategoryArray andCurrentTime:currentTime];
    }
    
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = nil;
}

- (void)liveChatAvailabilityChecker:(BTLiveChatAvailabilityChecker *)liveChatAvailabilityChecker failedToFetchSlotsWithWebServiceError:(NLWebServiceError *)webServiceError
{
    [self.faultSummaryScreenDelegate faultDetailsScreen:self failedToCheckLiveChatAvailabilityWithWebServiceError:webServiceError];
    
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = nil;
}

@end
