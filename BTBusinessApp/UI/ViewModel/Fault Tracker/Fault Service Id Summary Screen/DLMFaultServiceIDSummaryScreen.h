//
//  DLMFaultServiceIDSummaryScreen.h
//  BTBusinessApp
//
//  Created by Accolite on 27/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
@class DLMFaultServiceIDSummaryScreen;
@class BTFaultSummary;
@class NLWebServiceError;

@protocol DLMFaultServiceIDSummaryScreenDelegate <NSObject>

- (void)successfullyFetchedUserDataOnFaultServiceIDSummaryScreen:(DLMFaultServiceIDSummaryScreen *)faultServiceIDSummaryScreen;

- (void)faultServiceIDSummaryScreen:(DLMFaultServiceIDSummaryScreen *)faultServiceIDSummaryScreen failedToFetchDataWithWebServiceError:(NLWebServiceError *)webServiceError;

- (void)liveChatAvailableForFaultWithScreen:(DLMFaultServiceIDSummaryScreen *)faultServiceIDSummaryScreen;
- (void)faultServiceIDSummaryScreen:(DLMFaultServiceIDSummaryScreen *)faultServiceIDSummaryScreen liveChatCurrentlyNotAvailableWithSpecificCategory:(NSArray *)specificCategory generalCategory:(NSArray *)generalCategory andCurrentTime:(NSString *)currentTime;
- (void)faultServiceIDSummaryScreen:(DLMFaultServiceIDSummaryScreen *)faultServiceIDSummaryScreen failedToCheckLiveChatAvailabilityWithWebServiceError:(NLWebServiceError *)error;

@end


@interface DLMFaultServiceIDSummaryScreen : DLMObject

@property (nonatomic, strong) NSString *faultReference;
@property (nonatomic, weak) id<DLMFaultServiceIDSummaryScreenDelegate> faultServiceIDSummaryScreenDelegate;
@property (nonatomic) BTFaultSummary *faultSummary;
@property (nonatomic) NSArray *faultList;
@property (nonatomic, assign) BOOL isAuthenticated;

- (void)fetchFaultServiceIDSummaryDetailsForLoggedInUserWithServiceID:(NSString *)serviceID;
- (void) cancelServiceIDSummaryAPIRequest;
- (void)cancelChatAvailabilityCheckerAPI;

- (void)checkForLiveChatAvailibilityForFault;

@end
//productGroup openFaults closedFaults
