//
//  BTFaultTrackerSummaryViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 28/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTFaultTrackerSummaryViewController : UIViewController

@property (nonatomic) BOOL networkRequestInProgress;
@property (nonatomic, strong) NSString *faultReference;
@property (nonatomic) BOOL isServiceId;
@property (nonatomic) BOOL isAuthenticated;
@property (nonatomic, assign) BOOL isFromAPNS;

@end
