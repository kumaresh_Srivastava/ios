//
//  BTFaultTrackerSummaryViewController.m
//  BTBusinessApp
//
//  Created by Saddam Husian on 26/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultTrackerSummaryViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "BTUICommon.h"
#import "BTFaultTrackerSummaryTableViewCell.h"
#import "CustomSpinnerView.h"
#import "BTRetryView.h"
#import "DLMFaultServiceIDSummaryScreen.h"
#import "BTFaultSummary.h"
#import "BTFault.h"
#import "AppConstants.h"
#import "BTFaultRestrictedDetailsViewController.h"
#import "BTFaultDetailsMilestoneViewController.h"
#import "BTNoFaultSummaryView.h"
#import "AppManager.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "NLWebServiceError.h"
#import "BTHelpAndSupportViewController.h"
#import "BTInAppBrowserViewController.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "BTNavigationViewController.h"

#define kOriginalHeaderViewHeight 50;

@interface BTFaultTrackerSummaryViewController ()<UITextFieldDelegate, BTRetryViewDelegate, DLMFaultServiceIDSummaryScreenDelegate,BTNoFaultSummaryViewDelegate>
{
     BTRetryView *_retryView;
     BTNoFaultSummaryView *_emptyDashboardView;

     BOOL _isRetryShown;
    BOOL _isEmptyDashboardShown;
    NSInteger _currentSelectedSegmentedIndex;
    NSMutableArray *_faultListArray;
    NSArray *_faultArray;
    NSString *_pageNameForOmniture;
    
    
}

@property (nonatomic, readwrite) DLMFaultServiceIDSummaryScreen *serviceIDSummaryModel;
@property (strong, nonatomic) IBOutlet UITableView *faultSummaryTableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *faultSegmentedControl;
@property (strong, nonatomic) CustomSpinnerView *loadingView;

@property (nonatomic) BOOL isAPICallForFaultDetails;

@end

@implementation BTFaultTrackerSummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.edgesForExtendedLayout = UIRectEdgeNone;

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.title = [NSString stringWithFormat:@"Faults on %@", self.faultReference];

   //[SD] View Model Initialization
    self.serviceIDSummaryModel = [[DLMFaultServiceIDSummaryScreen alloc] init];
    self.serviceIDSummaryModel.faultServiceIDSummaryScreenDelegate = self;

    //order details table view config

    self.faultSummaryTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.faultSummaryTableView.backgroundColor = [UIColor clearColor];
    self.faultSummaryTableView.estimatedRowHeight = 50;
    self.faultSummaryTableView.rowHeight = UITableViewAutomaticDimension;

     [self createLoadingView];
    
    __weak typeof(self) weakSelf = self;
    
    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([weakSelf networkRequestInProgress]) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
            
            [weakSelf hideLoadingItems:NO];
            [weakSelf.loadingView startAnimatingLoadingIndicatorView];
        }
        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
            [weakSelf.loadingView stopAnimatingLoadingIndicatorView];
            [weakSelf hideLoadingItems:YES];
        }
    }];
    
    //[_faultSegmentedControl setTintColor:[BrandColours colourBackgroundBTPurplePrimaryColor]];
    //[_faultSegmentedControl setTitleTextAttributes:[[AppManager sharedAppManager] fetchSegmentedControlFontDict] forState:UIControlStateNormal];

    UINib *dataCell = [UINib nibWithNibName:@"BTFaultTrackerSummaryTableViewCell" bundle:nil];
    [self.faultSummaryTableView registerNib:dataCell forCellReuseIdentifier:@"BTFaultTrackerSummaryTableViewCell"];

    _faultListArray = [NSMutableArray array];

    [self fetchServiceIDSummaryAPI];
    
    [self trackPage:OMNIPAGE_FAULT_SEARCH_FAULT_OPEN];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    [self.serviceIDSummaryModel cancelServiceIDSummaryAPIRequest];
}

#pragma mark - api call methods

- (void)fetchServiceIDSummaryAPI {

   if(![AppManager isInternetConnectionAvailable])
   {
       [self showRetryViewWithInternetStrip:YES];
       [AppManager trackNoInternetErrorOnPage:[self getPageNameForOmniture]];
   }
   else
   {
       self.networkRequestInProgress = YES;
       [self hideLoadingItems:NO];
       [_loadingView startAnimatingLoadingIndicatorView];
       
       [self.serviceIDSummaryModel fetchFaultServiceIDSummaryDetailsForLoggedInUserWithServiceID:self.faultReference];
   }
}



#pragma mark - table view methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_faultListArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary *dataDict = [_faultListArray objectAtIndex:section];

    if(_currentSelectedSegmentedIndex == 0)
    {
        NSArray *openFaultsArray = [self getOpenFaultArrayfromDataDict:dataDict];
            return [openFaultsArray count];
    }
    else if(_currentSelectedSegmentedIndex == 1)
    {
        NSArray *closeFaultsArray = [self getCloseFaultArrayfromDataDict:dataDict];
        return [closeFaultsArray count];
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSDictionary *cellDataDict = [self getDataDictForIndex:indexPath.section];
    NSArray *openFaultsArray = [self getOpenFaultArrayfromDataDict:cellDataDict];
    NSArray *closeFaultsArray = [self getCloseFaultArrayfromDataDict:cellDataDict];

        BTFaultTrackerSummaryTableViewCell *faultDetailCell = [tableView dequeueReusableCellWithIdentifier:@"BTFaultTrackerSummaryTableViewCell" forIndexPath:indexPath];
    BTFault *fault = nil;
    if(_currentSelectedSegmentedIndex == 0)
    {
        fault = [openFaultsArray objectAtIndex:indexPath.row];
    }
    else if(_currentSelectedSegmentedIndex == 1)
    {
        fault = [closeFaultsArray objectAtIndex:indexPath.row];
    }

        [faultDetailCell updateCellWithFaultData:fault];
        return faultDetailCell;
}




-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25;
}
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 25)];
     [headerView setBackgroundColor:[BrandColours colourBtNeutral30]];
    NSString *sectionTitle = nil;
    NSDictionary *dataDict = [_faultListArray objectAtIndex:section];
    if(dataDict && [dataDict valueForKey:@"productGroup"])
    {
        sectionTitle = [dataDict valueForKey:@"productGroup"];
    }
    else
    {
        sectionTitle = @"";
    }
    UILabel *headerTitleLable = [[UILabel alloc] initWithFrame:CGRectMake(20, 0,tableView.bounds.size.width - 40, 25)];
    headerTitleLable.text = sectionTitle;
    headerTitleLable.textColor = [BrandColours colourBtNeutral70];
    headerTitleLable.font = [UIFont fontWithName:@"BTFont-Bold" size:15];
    headerTitleLable.backgroundColor = [UIColor clearColor];
    [headerView addSubview:headerTitleLable];
    return headerView;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *cellDataDict = [self getDataDictForIndex:indexPath.section];
    NSArray *openFaultsArray = [self getOpenFaultArrayfromDataDict:cellDataDict];
    NSArray *closeFaultsArray = [self getCloseFaultArrayfromDataDict:cellDataDict];
    BTFault *fault = nil;
    if(_currentSelectedSegmentedIndex == 0)
    {
        fault = [openFaultsArray objectAtIndex:indexPath.row];
    }
    else if(_currentSelectedSegmentedIndex == 1)
    {
        fault = [closeFaultsArray objectAtIndex:indexPath.row];
    }

    if (!_isAuthenticated) {


                // (SD) Riderect to fault summary or fault details screen.
                BTFaultRestrictedDetailsViewController *faultSummaryViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FaultRestrictedDetailsScene"];
                faultSummaryViewController.faultRef = fault.faultReference;
                [self.navigationController pushViewController:faultSummaryViewController animated:YES];

            }    else
            {


        BTFaultDetailsMilestoneViewController *faultDetailsMilestoneViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"faultDetailsMileStoneScene"];

        faultDetailsMilestoneViewController.faultReference = fault.faultReference;

        [self.navigationController pushViewController:faultDetailsMilestoneViewController animated:YES];
    }


}


#pragma mark - Action methods
- (IBAction)segmentSwitch:(id)sender {

    _currentSelectedSegmentedIndex = self.faultSegmentedControl.selectedSegmentIndex;
    _faultSummaryTableView.contentOffset = CGPointMake(0, 0);
    [self preparingDataFromDataDictArray:_faultArray];
    [self updateUIIfNoFaultOpenOrClosed];
    [self.faultSummaryTableView reloadData];
    

    [self trackPage];
}

#pragma mark - textfield delegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


#pragma mark- Private helper methods

- (void)cancelGetFaultDetails {
    
}

- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }]];
    
    [self presentViewController:alertController animated:YES completion:^{
        
    }];
}

- (void)trackPage
{
    if(_currentSelectedSegmentedIndex == 0)
    {
        
        if(_faultArray && [_faultArray count] > 0)
            [self trackPage:OMNIPAGE_FAULT_SEARCH_FAULT_OPEN];
        else
            [self trackPage:OMNIPAGE_FAULT_NO_OPEN_FAULTS_PHONE];
    }
    else
    {
        if(_faultArray && [_faultArray count] > 0)
            [self trackPage:OMNIPAGAE_FAULT_SEARCH_FAULT_CLOSED];
        else
            [self trackPage:OMNIPAGE_FAULT_NO_CLOSED_FAULTS_PHONE];
    }

}

- (void)createLoadingView {

    _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
    _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    _loadingView.translatesAutoresizingMaskIntoConstraints = NO;

    [self.view addSubview:_loadingView];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}

- (void)showRetryViewWithInternetStrip:(BOOL)needToShowInternetStrip {

    _isRetryShown = YES;

     if(!_retryView)
     {
         _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
         _retryView.translatesAutoresizingMaskIntoConstraints = NO;
         _retryView.retryViewDelegate = self;
         
         [_retryView updateRetryViewWithInternetStrip:needToShowInternetStrip];
         
         [self.view addSubview:_retryView];
         
         [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
         [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
         [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.faultSegmentedControl  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:10.0]];
         [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
     }
    
    _retryView.hidden  = NO;
    _retryView.retryViewDelegate = self;
    [_retryView updateRetryViewWithInternetStrip:needToShowInternetStrip];
    

}


- (void)showEmptyDashBoardView {

    if(!_isRetryShown)
    {
        _isEmptyDashboardShown = YES;
    _emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTNoFaultSummaryView" owner:nil options:nil] objectAtIndex:0];
        _emptyDashboardView.delegate = self;
    _emptyDashboardView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_emptyDashboardView];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.faultSegmentedControl  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:10.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    }

}


- (void)updateUIIfNoFaultOpenOrClosed
{
    BOOL isNeedToShowEmptyDashboardOnOpenFault = YES;
    BOOL isNeedToShowEmptyDashboardOnCloseFault = YES;

    for (NSDictionary *faultDetailDict in self.serviceIDSummaryModel.faultList) {
        NSArray *openFaultArray = [self getOpenFaultArrayfromDataDict:faultDetailDict];
        NSArray *closeFaultArray = [self getCloseFaultArrayfromDataDict:faultDetailDict];
        if(openFaultArray.count> 0)
            isNeedToShowEmptyDashboardOnOpenFault = NO;
        if(closeFaultArray.count> 0)
            isNeedToShowEmptyDashboardOnCloseFault = NO;
    }

    if(_currentSelectedSegmentedIndex == 0)
    {
        if(isNeedToShowEmptyDashboardOnOpenFault)
        {
            [self showEmptyDashBoardView];
        }
        else
        {
            [self hideEmptyDashboardItems:YES];
        }
    }
    else
    {
        if(isNeedToShowEmptyDashboardOnCloseFault)
        {
            [self showEmptyDashBoardView];
        }
        else
        {
            [self hideEmptyDashboardItems:YES];
        }

    }
}

- (void)hideLoadingItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to loading.
    [_loadingView setHidden:isHide];

}

- (void)hideRetryItems:(BOOL)isHide {

    _isRetryShown = NO;
    // (SD) Hide or Show UI elements related to retry.
    [_retryView setRetryViewDelegate:nil];
    [_retryView setHidden:isHide];

}

- (void)hideEmptyDashboardItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to retry.
    _isEmptyDashboardShown = NO;
    [_emptyDashboardView setDelegate:nil];
    [_emptyDashboardView setHidden:isHide];
    
}

- (NSDictionary *)getDataDictForIndex:(NSInteger)index
{
    NSDictionary *dataDict = nil;
    dataDict = [_faultListArray objectAtIndex:index];

    return dataDict;
}


- (NSArray *)getOpenFaultArrayfromDataDict:(NSDictionary *)dict
{
    NSArray *openFaultsArray = nil;
    if(dict && [dict valueForKey:@"openFaults"])
        openFaultsArray = [dict valueForKey:@"openFaults"];
    return openFaultsArray;

}

- (NSArray *)getCloseFaultArrayfromDataDict:(NSDictionary *)dict
{
    NSArray *closeFaultsArray = nil;
    if(dict && [dict valueForKey:@"closedFaults"])
        closeFaultsArray = [dict valueForKey:@"closedFaults"];
    return closeFaultsArray;

}

- (void)preparingDataFromDataDictArray:(NSArray *)dataDictArray {

    [_faultListArray removeAllObjects];
    
    if(_currentSelectedSegmentedIndex == 0) {
        if(dataDictArray && dataDictArray.count > 0) {
        NSArray *openFaultsArray = [self getOpenFaultArrayfromDataDict:[dataDictArray objectAtIndex:0]];
            if(openFaultsArray && openFaultsArray.count > 0)
               [_faultListArray addObject:[dataDictArray objectAtIndex:0]];
        }

        if(dataDictArray && dataDictArray.count > 1) {
            NSArray *openFaultsArray = [self getOpenFaultArrayfromDataDict:[dataDictArray objectAtIndex:1]];
            if(openFaultsArray && openFaultsArray.count > 0)
                [_faultListArray addObject:[dataDictArray objectAtIndex:1]];
        }

    }
    else if(_currentSelectedSegmentedIndex == 1) {

        if(dataDictArray && dataDictArray.count > 0) {
            NSArray *closeFaultsArray = [self getCloseFaultArrayfromDataDict:[dataDictArray objectAtIndex:0]];
            if(closeFaultsArray && closeFaultsArray.count > 0)
                [_faultListArray addObject:[dataDictArray objectAtIndex:0]];
        }

        if(dataDictArray && dataDictArray.count > 1) {
            NSArray *closeFaultsArray = [self getCloseFaultArrayfromDataDict:[dataDictArray objectAtIndex:1]];
            if(closeFaultsArray && closeFaultsArray.count > 0)
                [_faultListArray addObject:[dataDictArray objectAtIndex:1]];
        }

    }


}


- (void)trackPage:(NSString *)pageName
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params addEntriesFromDictionary:[AppManager getOmniDictionary]];
    NSString *ref = [NSString stringWithFormat:@"%@%@",kOmniFaultRefFormat,self.faultReference];
    [params setValue:ref forKey:kOmniFaultRef];
    
    [OmnitureManager trackPage:pageName withContextInfo:params];

}


- (NSString *)getPageNameForOmniture
{
    if(_currentSelectedSegmentedIndex == 0)
    {
        
        if(_faultArray && [_faultArray count] > 0)
            return OMNIPAGE_FAULT_SEARCH_FAULT_OPEN;
        else
            return OMNIPAGE_FAULT_NO_OPEN_FAULTS_PHONE;
    }
    else
    {
        if(_faultArray && [_faultArray count] > 0)
            return OMNIPAGAE_FAULT_SEARCH_FAULT_CLOSED;
        else
            return OMNIPAGE_FAULT_NO_CLOSED_FAULTS_PHONE;
    }

}

#pragma mark - BTRetryView Delegates

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    if(![AppManager isInternetConnectionAvailable])
    {
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:[self getPageNameForOmniture]];
    }
    else
    {
        [self hideRetryItems:YES];
         self.networkRequestInProgress = YES;
        [self fetchServiceIDSummaryAPI];
    }
}

#pragma mark ViewmodelDelegateMethods

- (void)successfullyFetchedUserDataOnFaultServiceIDSummaryScreen:(DLMFaultServiceIDSummaryScreen *)faultServiceIDSummaryScreen {

    self.networkRequestInProgress = NO;
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];

    if(faultServiceIDSummaryScreen.faultList)
    {
        if(faultServiceIDSummaryScreen.faultList.count == 0)
        {
            [self trackPage:OMNIPAGE_FAULT_NO_OPEN_FAULTS_PHONE];
            DDLogError(@"Fault Summary: Fault not found.");
        }
        else
        {
            DDLogInfo(@"fecthed Service ID details ");
            _faultArray = faultServiceIDSummaryScreen.faultList;
            [self preparingDataFromDataDictArray:faultServiceIDSummaryScreen.faultList];
            
            [self trackPage];
        }
        [_faultSummaryTableView reloadData];
        [self updateUIIfNoFaultOpenOrClosed];
    }
    else
    {
        [self trackPage:OMNIPAGE_FAULT_NO_OPEN_FAULTS_PHONE];
        [self showEmptyDashBoardView];
    }
    
}

- (void)faultServiceIDSummaryScreen:(DLMFaultServiceIDSummaryScreen *)faultServiceIDSummaryScreen failedToFetchDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    [self setNetworkRequestInProgress:false];
   
    self.networkRequestInProgress = NO;
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
    
    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {
        
        switch (webServiceError.error.code)
        {
                
            case BTNetworkErrorCodeAPINoDataFound:
            {
                [AppManager trackNoDataFoundErrorOnPage:OMNIPAGE_FAULT_NO_OPEN_FAULTS_PHONE];
                [self showEmptyDashBoardView];
                [self trackPage:OMNIPAGE_FAULT_NO_OPEN_FAULTS_PHONE];
                errorHandled = YES;
                break;
            }
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }
    
    if(errorHandled == NO)
    {
       [self showRetryViewWithInternetStrip:NO];
        [AppManager trackGenericAPIErrorOnPage:[self getPageNameForOmniture]];
    }
    
}

- (void)liveChatAvailableForFaultWithScreen:(DLMFaultServiceIDSummaryScreen *)faultServiceIDSummaryScreen
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
    
    viewController.targetURLType = BTTargetURLTypeFaultsChatScreen;
    
    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:NO andIsNeedCloseButton:YES];
    
    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{
        
    }];
    
}

- (void)faultServiceIDSummaryScreen:(DLMFaultServiceIDSummaryScreen *)faultServiceIDSummaryScreen liveChatCurrentlyNotAvailableWithSpecificCategory:(NSArray *)specificCategory generalCategory:(NSArray *)generalCategory andCurrentTime:(NSString *)currentTime
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BTHelpAndSupportViewController *helpAndSupportController = [BTHelpAndSupportViewController getHelpAndSupportViewController];
    helpAndSupportController.isLaunchingFromHomeScreen = NO;
    helpAndSupportController.isDataAlreadyFetched = YES;
    helpAndSupportController.specificCategory = specificCategory;
    helpAndSupportController.generalCategory = generalCategory;
    helpAndSupportController.currentTime = currentTime;
    
    [self.navigationController pushViewController:helpAndSupportController animated:YES];
}

- (void)faultServiceIDSummaryScreen:(DLMFaultServiceIDSummaryScreen *)faultServiceIDSummaryScreen failedToCheckLiveChatAvailabilityWithWebServiceError:(NLWebServiceError *)error
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if(errorHandled == NO)
    {
        [self showAlertWithTitle:@"Message" andMessage:kDefaultErrorMessage];
        [AppManager trackGenericAPIErrorOnPage:[self getPageNameForOmniture]];
    }
    
}


#pragma mark - No Fault Dashboard View delegate


- (void)noFaultSummaryView:(BTNoFaultSummaryView *)noFaultSummaryView reportNewFaultAction:(id)sender{
    
    if ([AppManager isInternetConnectionAvailable])
    {
        _networkRequestInProgress = YES;
        [self hideLoadingItems:NO];
        [_loadingView startAnimatingLoadingIndicatorView];
        [self.view bringSubviewToFront:_loadingView];
        [self.serviceIDSummaryModel checkForLiveChatAvailibilityForFault];
    }
    else
    {
        [self showAlertWithTitle:kNoConnectionTitle andMessage:kNoConnectionMessage];
    }
}



@end
