//
//  DLMFaultServiceIDSummaryScreen.m
//  BTBusinessApp
//
//  Created by Accolite on 27/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMFaultServiceIDSummaryScreen.h"
#import "NLFaultServiceIDSummaryWebService.h"
#import "AppDelegate.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppDelegateViewModel.h"
#import "AppConstants.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"
#import "CDRecentSearchedFault.h"
#import "BTFaultSummary.h"
#import "NLWebServiceError.h"
#import "BTLiveChatAvailabilityChecker.h"

@interface DLMFaultServiceIDSummaryScreen () <NLFaultServiceIDSummaryWebServiceDelegate, BTLiveChatAvailabilityCheckerDelegate> {
    
    BTLiveChatAvailabilityChecker *_liveChatAvailabityChecker;
}

@property (nonatomic) NLFaultServiceIDSummaryWebService *getFaultServiceIDSummaryWebService;

@end

@implementation DLMFaultServiceIDSummaryScreen
#pragma mark Public Methods

- (void)fetchFaultServiceIDSummaryDetailsForLoggedInUserWithServiceID:(NSString *)serviceID
{
    DDLogInfo(@"Fetching fault service id summary data from server for fault ref %@", serviceID);
    
    self.faultReference = serviceID;
    [self setupToFetchFaultServiceIDSummaryDetailsForLoggedInUserWithServiceID:serviceID];
    [self callFaultServiceIDSummaryDetails];
    
}

- (void)setupToFetchFaultServiceIDSummaryDetailsForLoggedInUserWithServiceID:(NSString *)serviceID {
    
    self.getFaultServiceIDSummaryWebService = [[NLFaultServiceIDSummaryWebService alloc] initWithServiceID:serviceID];
    self.getFaultServiceIDSummaryWebService.getFaultServiceIDSummaryWebServiceDelegate = self;
}

- (void) callFaultServiceIDSummaryDetails {
  [self.getFaultServiceIDSummaryWebService resume];
}

- (void)checkForLiveChatAvailibilityForFault
{
    [self setupToCheckForLiveChatAvailibilityForFault];
    [self callLiveChatAvailableSlots];
}

- (void)setupToCheckForLiveChatAvailibilityForFault {
    
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = [[BTLiveChatAvailabilityChecker alloc] init];
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = self;
}

- (void) callLiveChatAvailableSlots {
    
    [_liveChatAvailabityChecker getLiveChatAvailableSlots];
}

#pragma mark CancellingDashBoard request methods

- (void) cancelServiceIDSummaryAPIRequest {
    self.getFaultServiceIDSummaryWebService.getFaultServiceIDSummaryWebServiceDelegate = nil;
    [self.getFaultServiceIDSummaryWebService cancel];
    self.getFaultServiceIDSummaryWebService = nil;
}

- (void)cancelChatAvailabilityCheckerAPI
{
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    [_liveChatAvailabityChecker cancel];
    _liveChatAvailabityChecker = nil;
}

#pragma mark - NLFaultServiceIDSummaryWebServiceDelegate Methods

- (void)getFaultServiceIDSummaryWebService:(NLFaultServiceIDSummaryWebService *)webService successfullyFetchedFaultServiceIDSummaryData:(NSArray *)faultList isAuthenticated:(BOOL)isAuthenticated faultSummary:(BTFaultSummary *)faultSummary
{
    if(webService == _getFaultServiceIDSummaryWebService) {
        [self setFaultList:faultList];
        [self setIsAuthenticated:isAuthenticated];
        [self setFaultSummary:faultSummary];
        
        if (faultList != nil && faultList.count > 0) {
            
            // (LP) Save the latest data from web service into the persistence for recently searched.
            
            DDLogInfo(@"Saving data from response of NLFaultServiceIDSummaryWebServiceDelegate API Call into persistence in recent searched items for fault reference %@.", self.faultReference);
            
            NSManagedObjectContext *context = [AppDelegate sharedInstance].managedObjectContext;
            CDUser *persistenceUser = [CDUser userWithUsername:[AppDelegate sharedInstance].viewModel.app.loggedInUser.username inManagedObjectContext:context];
            
            NSArray *arrayOfRecentSearchedFaults = [persistenceUser.recentlySearchedFaults allObjects];
            
            CDRecentSearchedFault *searchedFault = nil;
            for (CDRecentSearchedFault *recentSearchedFault in arrayOfRecentSearchedFaults) {
                if ([recentSearchedFault.faultRef isEqualToString:self.faultReference]) {
                    searchedFault = recentSearchedFault;
                }
            }
            
            if (searchedFault == nil) {
                searchedFault = [CDRecentSearchedFault newRecentSearchedFaultInManagedObjectContext:context];
                searchedFault.faultRef = self.faultReference;
                searchedFault.faultStatus = @"";
                
                NSMutableArray *arrayOfProductGroupName = [NSMutableArray array];
                for (NSDictionary *faultData in faultList) {
                    if ([faultData valueForKey:@"productGroup"]) {
                        [arrayOfProductGroupName addObject:[faultData valueForKey:@"productGroup"]];
                    }
                }
                
                NSString *description = [arrayOfProductGroupName componentsJoinedByString:@", "];
                
                searchedFault.faultDescription = description;
                searchedFault.reportedOnDate = nil;
                searchedFault.colourWithStatus = nil;
                searchedFault.lastSearchedDate = [NSDate date];
                
                [persistenceUser addRecentlySearchedFaultsObject:searchedFault];
            }
            else
            {
                searchedFault.faultStatus = @"";
                searchedFault.lastSearchedDate = [NSDate date];
                
                [persistenceUser addRecentlySearchedFaultsObject:searchedFault];
            }
            
            [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
        }
        
        [self.faultServiceIDSummaryScreenDelegate successfullyFetchedUserDataOnFaultServiceIDSummaryScreen:self];
        _getFaultServiceIDSummaryWebService.getFaultServiceIDSummaryWebServiceDelegate = nil;
        _getFaultServiceIDSummaryWebService = nil;
    } else {
        DDLogError(@"This delegate method gets called because of success of an object of NLFaultServiceIDSummaryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLFaultServiceIDSummaryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)getFaultServiceIDSummaryWebService:(NLFaultServiceIDSummaryWebService *)webService failedToFetchFaultServiceIDSummaryDataWithWebServiceError:(NLWebServiceError *)error {
    if(webService == _getFaultServiceIDSummaryWebService) {
        
        [self.faultServiceIDSummaryScreenDelegate faultServiceIDSummaryScreen:self failedToFetchDataWithWebServiceError:error];
        _getFaultServiceIDSummaryWebService.getFaultServiceIDSummaryWebServiceDelegate = nil;
        _getFaultServiceIDSummaryWebService = nil;
        
    } else {
        DDLogError(@"This delegate method gets called because of failure of an object of NLFaultServiceIDSummaryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLFaultServiceIDSummaryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

#pragma mark - BTLiveChatAvailabilityCheckerDelegate Methods
- (void)liveChatAvailabilityChecker:(BTLiveChatAvailabilityChecker *)liveChatAvailabilityChecker successfullyFetchedSlotsWithGeneralCategory:(NSArray *)generalCategoryArray specificeCategory:(NSArray *)specificCategory andCurrentTime:(NSString *)currentTime
{
    BOOL chatAvailable = [BTLiveChatAvailabilityChecker checkAvailabilityWithSpecificCategory:specificCategory currentTime:currentTime andChatType:BTChatTypeFault];
    
    if (chatAvailable)
    {
        [self.faultServiceIDSummaryScreenDelegate liveChatAvailableForFaultWithScreen:self];
    }
    else
    {
        [self.faultServiceIDSummaryScreenDelegate faultServiceIDSummaryScreen:self liveChatCurrentlyNotAvailableWithSpecificCategory:specificCategory generalCategory:generalCategoryArray andCurrentTime:currentTime];
    }
    
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = nil;
}

- (void)liveChatAvailabilityChecker:(BTLiveChatAvailabilityChecker *)liveChatAvailabilityChecker failedToFetchSlotsWithWebServiceError:(NLWebServiceError *)webServiceError
{
    [self.faultServiceIDSummaryScreenDelegate faultServiceIDSummaryScreen:self failedToCheckLiveChatAvailabilityWithWebServiceError:webServiceError];
    
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = nil;
}


@end
