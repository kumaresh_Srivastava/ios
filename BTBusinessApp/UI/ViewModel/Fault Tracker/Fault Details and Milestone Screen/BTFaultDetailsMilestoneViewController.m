//
//  BTFaultDetailsMilestoneViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 21/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultDetailsMilestoneViewController.h"
#import "BTFault.h"
#import "BTFaultTrackerNodeView.h"
#import "BTFaultMilestoneNode.h"
#import "BTFaultDetailsTakeActionView.h"
#import "BTFaultSummaryView.h"
#import "BTFaultDetailsClosedActionView.h"
#import "BTAlertBannerView.h"
#import "BTRetryView.h"
#import "CustomSpinnerView.h"
#import "AppConstants.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "DLMFaultDetailsScreen.h"
#import "BTMessageFromAgentViewController.h"
#import "BTFaultAmendViewController.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "NLWebServiceError.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "BTHelpAndSupportViewController.h"
#import "AppDelegate.h"
#import "CDEngineerAppointmentForFault.h"
#import <EventKit/EventKit.h>
#import "BTFaultAppointmentDetail.h"
#import "BTEmptyOrderSummaryAndMilestoneView.h"
#import "BTInAppBrowserViewController.h"
#import "BTNavigationViewController.h"
#import "BTOrderStatusLabel.h"

@interface BTFaultDetailsMilestoneViewController () <DLMFaultDetailsScreenDelegate, BTRetryViewDelegate, BTFaultDetailsTakeActionViewDelegate, BTFaultTrackerNodeViewDelegate, BTFaultAmendViewControllerDelegate, BTFaultDetailsClosedActionViewDelegate, BTEmptyOrderSummaryAndMilestoneViewDelegate> {
    UIView *_milestoneView;
    UIView *_summaryView;
    UIView *_bottomView;
    CustomSpinnerView *_loadingView;
    BTRetryView *_retryView;
    BOOL _isClosedFault;
    NSInteger _selectedTab;
    BTFaultSummaryView *_faultSummaryView;
    BTEmptyOrderSummaryAndMilestoneView *_emptyMilestoneView;
    BTFaultDetailsTakeActionView *_takeActionView;
    BTFaultDetailsClosedActionView *_closedActionView;
    BTAlertBannerView *_alertBannerView;
    CGFloat _originalMainScrollTopConstraintValue;
    NSString *_pageNameForOmniture; //RM
    BOOL _addAppointmentToCalenderIsInProgress;

}

@property (weak, nonatomic) IBOutlet UIImageView *faultImageView;
@property (weak, nonatomic) IBOutlet UILabel *faultReferenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *faultDescriptionLabel;
@property (weak, nonatomic) IBOutlet BTOrderStatusLabel *statusLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *faultSegmentedControl;
@property (weak, nonatomic) IBOutlet UIScrollView *mainContainerScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainScrollViewTopConstraint;

@end

@implementation BTFaultDetailsMilestoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _pageNameForOmniture = OMNIPAGE_FAULT_DETAIL;
    
    [self setEdgesForExtendedLayout:UIRectEdgeNone];

    _milestoneView = [[UIView alloc] init];
    _summaryView = [[UIView alloc] init];
    _bottomView = nil;
    _isClosedFault = NO;

    _originalMainScrollTopConstraintValue = self.mainScrollViewTopConstraint.constant;


    __weak typeof(self) weakSelf = self;

    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([weakSelf networkRequestInProgress]) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];

        }
        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];

        }
    }];

    _selectedTab = TrackFaultDetailsTypeFaultSummary;
    //[_faultSegmentedControl setTintColor:[BrandColours colourBackgroundBTPurplePrimaryColor]];
    //[_faultSegmentedControl setTitleTextAttributes:[[AppManager sharedAppManager] fetchSegmentedControlFontDict] forState:UIControlStateNormal];
    [self initialUISetupForFaultDetails];
    [self checkAccountValidated];
    
    
    [self fetchFaultDetailsData];
    
    [self trackOmniPage:OMNIPAGE_FAULT_DETAIL withFaultRef:self.faultReference];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    self.networkRequestInProgress = NO;

    [self.faultDetailsViewModel cancelDetailsAPI];
    [self.faultDetailsViewModel cancelFaultAPI];
    [self.faultDetailsViewModel cancelDetailsAPIWithAssetID];
    [self.faultDetailsViewModel cancelChatAvailabilityCheckerAPI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark privatemethods

- (void)checkAccountValidated {
    if (self.bacNeeded) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Account number validated" message:@"You can now view full details of this fault." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


#pragma mark - UI Related methods

- (void)initialUISetupForFaultDetails
{
    self.title = @"Fault details";

    [self createFaultSummaryView];
    [self createTakeActionView];
    [self createClosedActionView];

    [self createLoadingView];
    [self hideLoadingItems:YES];

    [self hideorShowControls:YES];

}

- (void)createFaultSummaryView
{
    _faultSummaryView = [[[NSBundle mainBundle] loadNibNamed:@"BTFaultSummaryView" owner:nil options:nil] objectAtIndex:0];

}

- (void)createTakeActionView
{
    _takeActionView = [[[NSBundle mainBundle] loadNibNamed:@"BTFaultDetailsTakeActionView" owner:nil options:nil] objectAtIndex:0];
    _takeActionView.delegate = self;
}

- (void)createClosedActionView
{
    _closedActionView = [[[NSBundle mainBundle] loadNibNamed:@"BTFaultDetailsClosedActionView" owner:nil options:nil] objectAtIndex:0];
    _closedActionView.delegate = self;
}

- (void)createEmptyFaultMilestoneView
{
    _emptyMilestoneView = [[[NSBundle mainBundle] loadNibNamed:@"BTEmptyOrderSummaryAndMilestoneView" owner:nil options:nil] objectAtIndex:0];
    [_emptyMilestoneView setEmptyOrderSummaryAndMilestoneViewDelegate:self];
}

//(Salman)
- (void)createCancelFaultBannerView{


    if(!_alertBannerView){

        _alertBannerView = [[[NSBundle mainBundle] loadNibNamed:@"BTAlertBannerView" owner:self options:nil] firstObject];

        _alertBannerView.frame = CGRectMake(0,
                                            self.mainContainerScrollView.frame.origin.y,
                                            self.mainContainerScrollView.frame.size.width,
                                            40);

        [self.view addSubview:_alertBannerView];
    }


}

//Salman
- (void)showAlertBannerWithMessage:(NSString *)alertMessage{

    [self createCancelFaultBannerView];

    [_alertBannerView showAnimated:YES];

    [_alertBannerView setMessage:alertMessage];
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:kBannerAnimationTime animations:^{

        self.mainScrollViewTopConstraint.constant = _alertBannerView.frame.size.height;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {

        [self performSelector:@selector(hideCancelAlertBanner) withObject:nil afterDelay:3.0];
    }];


}

//Salman
- (void)hideCancelAlertBanner{

    [_alertBannerView hideAnimated:YES];

    [self.view layoutIfNeeded];
    [UIView animateWithDuration:kBannerAnimationTime animations:^{

        self.mainScrollViewTopConstraint.constant = _originalMainScrollTopConstraintValue;
        [self.view layoutIfNeeded];

    } completion:^(BOOL finished) {

    }];

}


#pragma mark Action Methods

- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow
{
    if (_retryView != nil) {
        _retryView.hidden = NO;
        _retryView.retryViewDelegate = self;
        [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
        return;
    }

    _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
    _retryView.translatesAutoresizingMaskIntoConstraints = NO;
    _retryView.retryViewDelegate = self;
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];

    [self.view addSubview:_retryView];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.mainContainerScrollView  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.mainContainerScrollView  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.mainContainerScrollView  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.mainContainerScrollView  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];

}

- (void)createLoadingView {

    _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
    _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    _loadingView.translatesAutoresizingMaskIntoConstraints = NO;

    [self.view addSubview:_loadingView];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}

- (void) hideorShowControls:(BOOL)isHide {

    // (lp) Hide UI elements during loading time
    self.mainContainerScrollView.hidden = isHide;

}

- (void)hideRetryItems:(BOOL)isHide {

    // (lp) Hide or Show UI elements related to retry.
    [_retryView setRetryViewDelegate:nil];
    [_retryView setHidden:isHide];

}

- (void)hideLoadingItems:(BOOL)isHide {

    // (lp) Hide or Show UI elements related to loading.
    [_loadingView setHidden:isHide];

}

#pragma mark - Calendar Methods

// (lpm) On Main thread
- (CDEngineerAppointmentForFault *)getEngineerAppointmentDataForCurrentFaultFromPersistentStore {
    // Fetch Persistence Object For Fault to Local Calender Event ID
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"CDEngineerAppointmentForFault" inManagedObjectContext:[[AppDelegate sharedInstance] managedObjectContext]]];
    
    NSPredicate *faultRefPredicate = [NSPredicate predicateWithFormat:@"faultRef = %@", self.faultReference];
    [fetchRequest setPredicate:faultRefPredicate];
    
    NSError *__autoreleasing fetchError = nil;
    NSArray *resultArray = [[[AppDelegate sharedInstance] managedObjectContext] executeFetchRequest:fetchRequest error:&fetchError];
    if (resultArray.count > 0) {
        
        CDEngineerAppointmentForFault *appointmentFault = [resultArray objectAtIndex:0];
        return appointmentFault;
    }
    return nil;
}

// (lpm) On Main thread
- (void)deleteEngineerAppointmentForCurrentFault:(CDEngineerAppointmentForFault *)engineerAppointmentForFault
{
    // (lpm) Delelte the current appointment for order.
    CDEngineerAppointmentForFault *previousAppointment = engineerAppointmentForFault;
    engineerAppointmentForFault = nil;
    if(previousAppointment)
    {
        [[AppDelegate sharedInstance].managedObjectContext deleteObject:previousAppointment];
    }
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
}


- (BOOL)checkForAlreadyAddedAppointmentAndDeleteForCurrentFault
{
    BOOL isAlreadyAdded = NO;
    
    CDEngineerAppointmentForFault *engineerAppointmentData = [self getEngineerAppointmentDataForCurrentFaultFromPersistentStore];
    
    NSString *calendarEventIdentifier = engineerAppointmentData.localCalenderEventID;
    
    if (calendarEventIdentifier != nil)
    {
        // isAlreadyAdded = YES;
        
        EKEventStore *store = [[EKEventStore alloc] init];
        EKEvent *eventToRemove = [store eventWithIdentifier:calendarEventIdentifier];
        
        NSError *errorInRemove = nil;
        
        if (eventToRemove != nil)
        {
            [store removeEvent:eventToRemove span:EKSpanThisEvent error:&errorInRemove];
        }
        
        if (errorInRemove)
        {
            isAlreadyAdded = YES;
            DDLogError(@"Error in removing current event from calender");
        }
        else
        {
            isAlreadyAdded = NO;
            
            if ([NSThread isMainThread])
            {
                [self deleteEngineerAppointmentForCurrentFault:engineerAppointmentData];
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self deleteEngineerAppointmentForCurrentFault:engineerAppointmentData];
                });
            }
        }
    }
    
    return isAlreadyAdded;
}

- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }]];
    
    [self presentViewController:alertController animated:YES completion:^{
        
    }];
}

#pragma mark - BTRetryView delegate methods
- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView {

    DDLogInfo(@"Retry to fecth fault summary details for fault reference : %@",self.faultReference);
    [self fetchFaultDetailsData];
}

#pragma mark - BTFaultDetailsClosedActionViewDelegate Methods

- (void)userClickedOnContactUsButtonOfFaultDetailsClosedActionView:(BTFaultDetailsClosedActionView *)faultDetailsClosedActionView
{
    if ([AppManager isInternetConnectionAvailable])
    {
        _networkRequestInProgress = YES;
        [self hideLoadingItems:NO];
        [_loadingView startAnimatingLoadingIndicatorView];
        [self.faultDetailsViewModel checkForLiveChatAvailibilityForFault];
    }
    else
    {
        [self showAlertWithTitle:kNoConnectionTitle andMessage:kNoConnectionMessage];
    }
    
}

#pragma mark - BTFaultDetailsTakeActionViewDelegate Method

- (void)userPressCancelFaultOfTakeActionView:(BTFaultDetailsTakeActionView *)takeActionView {

    
    [self trackOmniClick:OMNICLICK_FAULT_CANCEL_FAULT forPage:_pageNameForOmniture withFaultRef:self.faultReference];
    
    [OmnitureManager trackError:OMNIERROR_FAULT_CANCEL_WARNING onPageWithName:_pageNameForOmniture contextInfo:[AppManager getOmniDictionary]];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Do you want to cancel your fault?" message:@"If you cancel it, we'll stop working on your fault right away." preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];

    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self cancelFaultDetailsData];
    }];

    [alertController addAction:cancelAction];
    [alertController addAction:confirmAction];

    [self presentViewController:alertController animated:YES completion:nil];


}

- (void)userPressedAmendFaultDetailsOfTakeActionView:(BTFaultDetailsTakeActionView *)takeActionView {

    [self trackOmniClick:OMNICLICK_FAULT_CHANGE_FAULT_DETAILS forPage:_pageNameForOmniture withFaultRef:self.faultReference];
    
    BTFaultAmendViewController *controller = [BTFaultAmendViewController getBTFaultAmendViewController];
    controller.faultRefrence = self.faultDetailsViewModel.fault.faultReference;
    controller.faultAmendDelegate = self;
    [self.navigationController pushViewController:controller animated:YES];

}

- (void)userPressedAddAppointmentToCalenderOfTakeActionView:(BTFaultDetailsTakeActionView *)takeActionView {
    
    if (_addAppointmentToCalenderIsInProgress) {
        return;
    }
    
    _addAppointmentToCalenderIsInProgress = YES;
    
    [self trackOmniClick:OMNICLICK_ORDER_ADD_APPT_CALENDAR forPage:_pageNameForOmniture withFaultRef:self.faultReference];
    
    BOOL isAlreadyAddedAndNotAbleToDelete = [self checkForAlreadyAddedAppointmentAndDeleteForCurrentFault];
    
    if (isAlreadyAddedAndNotAbleToDelete)
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Message" message:kDefaultErrorMessage preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:^{
            
            _addAppointmentToCalenderIsInProgress = NO;
        }];
        
    }
    else
    {
        EKEventStore *store = [[EKEventStore alloc] init];
        
        __weak typeof(self) weakSelf = self;
        [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            if (!granted) {
                DDLogInfo(@"INFO:User declined calendar access.");
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Add appointment" message:@"Access to Calendar is disabled, please enable in the settings." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [alertController dismissViewControllerAnimated:YES completion:nil];
                }];
                [alertController addAction:okAction];
                [weakSelf presentViewController:alertController animated:YES completion:^{
                    
                    _addAppointmentToCalenderIsInProgress = NO;
                }];
                
            }
            
            EKEvent *event = [EKEvent eventWithEventStore:store];
            event.title = @"BT engineer appointment";
            
            // (LP) Logic to check simple fault or access time slot fault
            if (self.faultDetailsViewModel.fault.faultAppointmentDetail.isHourAccessPresent)
            {
                //Start time
                NSDate *appointmentDate = self.faultDetailsViewModel.fault.faultAppointmentDetail.bookedStartDate;
                
                NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:appointmentDate];
                [components setHour:[self.faultDetailsViewModel getHourFromText:self.faultDetailsViewModel.fault.faultAppointmentDetail.earlierAccessTime]];
                
                event.startDate = [[NSCalendar currentCalendar] dateFromComponents:components];
                
                [components setHour:[self.faultDetailsViewModel getHourFromText:self.faultDetailsViewModel.fault.faultAppointmentDetail.latestAccessTime]];
                
                event.endDate = [[NSCalendar currentCalendar] dateFromComponents:components];
                
            }
            else
            {
                event.startDate = self.faultDetailsViewModel.fault.faultAppointmentDetail.bookedEndDate;
                
                //(VRK) calculating the end time from the start time based on the appointment actual slots
                NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:self.faultDetailsViewModel.fault.faultAppointmentDetail.bookedEndDate];
                
                long startHour = components.hour;
                long endTimeInterval;
                if (startHour < 13) {
                    endTimeInterval = 60*60*(13 - startHour);
                } else {
                    
                    if (startHour < 18)
                    {
                        endTimeInterval = 60*60*(18 - startHour);
                    }
                    else
                    {
                        endTimeInterval = 60*60*(24 - startHour);
                    }
                }
                
                event.endDate = [event.startDate dateByAddingTimeInterval:endTimeInterval];  // Duration 1 hr
            }
            
            event.alarms = [NSArray arrayWithObject:[EKAlarm alarmWithRelativeOffset:(-(60*60)*48)]];
            NSString *eventMessage = [NSString stringWithFormat:@"BT Engineer Appointment for %@", self.faultDetailsViewModel.fault.serviceID];
            event.notes = eventMessage;
            [event setCalendar:[store defaultCalendarForNewEvents]];
            NSError *errorInSaving = nil;
            [store saveEvent:event span:EKSpanThisEvent commit:YES error:&errorInSaving];
            
            if(errorInSaving)
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Message" message:kDefaultErrorMessage preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [alertController dismissViewControllerAnimated:YES completion:nil];
                }];
                [alertController addAction:okAction];
                [weakSelf presentViewController:alertController animated:YES completion:^{
                    
                    _addAppointmentToCalenderIsInProgress = NO;
                    
                }];
                
                return;
            }
            else
            {
                NSString* eventIdentifier = [[NSString alloc] initWithFormat:@"%@", event.eventIdentifier];
                
                CDEngineerAppointmentForFault *newEngineerAppointmentForFault = [CDEngineerAppointmentForFault newEngineerAppointmentForFaultInManagedObjectContext:[[AppDelegate sharedInstance] managedObjectContext] withFaultRef:self.faultReference andCalenderEventID:eventIdentifier];
                if(newEngineerAppointmentForFault == nil)
                {
                    DDLogError(@"Error: Unable to create appointment data in persistent store.");
                    _addAppointmentToCalenderIsInProgress = NO;
                    return;
                }
                else
                {
                    [[AppDelegate sharedInstance] saveContext];
                    [self trackPageWithKeyTask:OMNI_KEYTASK_FAULT_APPOINTMENT_ADDED_TO_CALENDAR];
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"Appointment reminder added successfully." preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [alertController dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okAction];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf presentViewController:alertController animated:YES completion:^{
                            
                            _addAppointmentToCalenderIsInProgress = NO;
                            
                        }];
                    });
                }
            }
        }];
    }
}

#pragma mark - Fetch fault details methods
- (void)fetchFaultDetailsData {

    // (LP) Ask to viewModel to provide fault summary details. The viewModel takes care of fetching data.

    if (_redirectedFromDashboard) {

        if ([AppManager isInternetConnectionAvailable])
        {
            self.networkRequestInProgress = YES;
            [self hideLoadingItems:NO];
            [self hideRetryItems:YES];
            [_loadingView startAnimatingLoadingIndicatorView];
            
            [self setupFaultDetailsViewModel];
            [self callFaultDetailsForFaultRef:self.faultReference];
        }
        else
        {
            [self hideLoadingItems:YES];
            [self showRetryViewWithInternetStrip:YES];
            [AppManager trackNoInternetErrorOnPage:_pageNameForOmniture];
        }
        
    } else if (!_bacNeeded)
    {
        if ([AppManager isInternetConnectionAvailable])
        {
            self.networkRequestInProgress = YES;
            [self hideLoadingItems:NO];
            [self hideRetryItems:YES];
            [_loadingView startAnimatingLoadingIndicatorView];
            
            _faultDetailsViewModel.faultDetailsScreenDelegate = self;
            [self.faultDetailsViewModel fetchFaultSummaryDetailsForFaultReference:self.faultReference andAssetId:self.assetId];
        }
        else
        {
            [self hideLoadingItems:YES];
            [self showRetryViewWithInternetStrip:YES];
            [AppManager trackNoInternetErrorOnPage:_pageNameForOmniture];
        }
        
        
        
    }
    else
    {
        _faultDetailsViewModel.faultDetailsScreenDelegate = self;
        
        [self updateUserInterface];

        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Account number validated" message:@"You can now view full details of this fault." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }

}

- (void) setupFaultDetailsViewModel {
    
    _faultDetailsViewModel = [[DLMFaultDetailsScreen alloc] init];
    _faultDetailsViewModel.faultDetailsScreenDelegate = self;
}

- (void) callFaultDetailsForFaultRef:(NSString *)faultRef {
   
    [self.faultDetailsViewModel fetchFaultSummaryDetailsForFaultReference:self.faultReference];
}

#pragma mark - Cancel fault details methods
- (void)cancelFaultDetailsData {

    // (Salman) making cancel API Call

    self.networkRequestInProgress = YES;
    [self hideLoadingItems:NO];
    [self hideRetryItems:YES];
    [_loadingView startAnimatingLoadingIndicatorView];
    [self.faultDetailsViewModel cancelSummaryDetailsForFaultReference:self.faultDetailsViewModel.fault.faultReference andAssetId:self.faultDetailsViewModel.fault.assetId];
}



#pragma mark - Action Methods

- (IBAction)orderDetailsSegmentAction:(id)sender {

    long selectedSegment = (long)[self.faultSegmentedControl selectedSegmentIndex];
    switch (selectedSegment) {
        case TrackFaultDetailsTypeFaultSummary:// For summary tab
            _selectedTab = TrackFaultDetailsTypeFaultSummary;

            [self trackOmniPage:OMNIPAGE_FAULT_DETAIL withFaultRef:self.faultReference];
            _pageNameForOmniture = OMNIPAGE_FAULT_DETAIL;
            
            [self updateAndShowFaultSummaryView];

            break;
        case TrackFaultDetailsTypeFaultTracker:// for fault tracker tab
            _selectedTab = TrackFaultDetailsTypeFaultTracker;

            [self trackOmniPage:OMNIPAGE_FAULT_DETAIL_TRACKER withFaultRef:self.faultReference];
            _pageNameForOmniture = OMNIPAGE_FAULT_DETAIL_TRACKER;
            
            [self updateAndShowFaultMilestoneView];

            break;
    }
}

- (void)updateAndShowFaultMilestoneView
{

    [_summaryView removeFromSuperview];
    [[_milestoneView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [_bottomView removeFromSuperview];
    [_milestoneView removeFromSuperview];

    BTFaultTrackerNodeView *previousView = [[BTFaultTrackerNodeView alloc] init];
    previousView.delegate = self;

    for (int i=0; i<self.faultDetailsViewModel.arrayOfMilestoneNodes.count; i++) {
        BTFaultMilestoneNode *milestoneNode = [self.faultDetailsViewModel.arrayOfMilestoneNodes objectAtIndex:i];
        BTFaultTrackerNodeView *milestoneNodeView = [[[NSBundle mainBundle] loadNibNamed:@"BTFaultTrackerNodeView" owner:nil options:nil] objectAtIndex:0];

        milestoneNodeView.delegate = self;

        if ( i==0 ) {
            if (i==(self.faultDetailsViewModel.arrayOfMilestoneNodes.count-1)) {
                [milestoneNodeView updateMilestoneNodeViewWithData:milestoneNode isFirstNode:YES isLastNode:YES];
            } else {
                [milestoneNodeView updateMilestoneNodeViewWithData:milestoneNode isFirstNode:YES isLastNode:NO];
            }
        } else {
            if (i==(self.faultDetailsViewModel.arrayOfMilestoneNodes.count-1)) {
                [milestoneNodeView updateMilestoneNodeViewWithData:milestoneNode isFirstNode:NO isLastNode:YES];
            } else {
                [milestoneNodeView updateMilestoneNodeViewWithData:milestoneNode isFirstNode:NO isLastNode:NO];
            }
        }

        milestoneNodeView.translatesAutoresizingMaskIntoConstraints = NO;
        [_milestoneView addSubview:milestoneNodeView];

        NSDictionary *views = @{@"milestoneNodeView":milestoneNodeView,@"previousView":previousView,@"milestoneView":_milestoneView};

        [_milestoneView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[milestoneNodeView(==milestoneView)]|" options:0 metrics:nil views:views]];

        if (i==0) {
            [_milestoneView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[milestoneNodeView]" options:0 metrics:nil views:views]];
        } else {
            [_milestoneView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[previousView]-0-[milestoneNodeView]" options:0 metrics:nil views:views]];
        }

        previousView = milestoneNodeView;
    }

    if ([self.faultDetailsViewModel.fault.status isEqualToString:@"Completed"] || [self.faultDetailsViewModel.fault.status isEqualToString:@"Fault Cancelled"]) {

        _closedActionView.translatesAutoresizingMaskIntoConstraints = NO;
        _bottomView = _closedActionView;
    } else {

        if (self.faultDetailsViewModel.fault.btnSecondaryClickID == 3 && self.faultDetailsViewModel.fault.btnPrimaryClickID == 8) {
            [_takeActionView updateTakeActionWithShowAmendView:YES andShowCancelFault:YES];
            
        }
        else if (self.faultDetailsViewModel.fault.btnSecondaryClickID == 3)
        {
            [_takeActionView updateTakeActionWithShowAmendView:NO andShowCancelFault:YES];
        }
        else if (self.faultDetailsViewModel.fault.btnPrimaryClickID == 8)
        {
            [_takeActionView updateTakeActionWithShowAmendView:YES andShowCancelFault:NO];
        }
        else
        {
            [_takeActionView updateTakeActionWithShowAmendView:NO andShowCancelFault:NO];
        }

        _takeActionView.translatesAutoresizingMaskIntoConstraints = NO;
        _bottomView = _takeActionView;
    }

    if (self.faultDetailsViewModel.arrayOfMilestoneNodes.count > 0) {

        previousView.translatesAutoresizingMaskIntoConstraints = NO;
        _bottomView.translatesAutoresizingMaskIntoConstraints = NO;
        _milestoneView.translatesAutoresizingMaskIntoConstraints = NO;
        [_milestoneView addSubview:_bottomView];

        [_milestoneView addConstraint:[NSLayoutConstraint constraintWithItem:_bottomView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:_milestoneView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [_milestoneView addConstraint:[NSLayoutConstraint constraintWithItem:_bottomView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:_milestoneView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [_milestoneView addConstraint:[NSLayoutConstraint constraintWithItem:_bottomView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:previousView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
        [_milestoneView addConstraint:[NSLayoutConstraint constraintWithItem:_milestoneView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_bottomView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];

        [self.mainContainerScrollView addSubview:_milestoneView];
        [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_milestoneView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.mainContainerScrollView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0]];
        [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_milestoneView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.mainContainerScrollView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0]];
        [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_milestoneView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.faultSegmentedControl attribute:NSLayoutAttributeBottom multiplier:1.0 constant:8.0]];
        [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:self.mainContainerScrollView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_milestoneView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:12.0]];
    }
    else
    {
        [self createEmptyFaultMilestoneView];
        _emptyMilestoneView.translatesAutoresizingMaskIntoConstraints = NO;
        [_milestoneView addSubview:_emptyMilestoneView];
        
        [_milestoneView addConstraint:[NSLayoutConstraint constraintWithItem:_emptyMilestoneView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:_milestoneView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [_milestoneView addConstraint:[NSLayoutConstraint constraintWithItem:_emptyMilestoneView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:_milestoneView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [_milestoneView addConstraint:[NSLayoutConstraint constraintWithItem:_emptyMilestoneView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_milestoneView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
        [_milestoneView addConstraint:[NSLayoutConstraint constraintWithItem:_milestoneView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_emptyMilestoneView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
        
        _milestoneView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.mainContainerScrollView addSubview:_milestoneView];
        
        [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_milestoneView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.mainContainerScrollView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0]];
        [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_milestoneView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.mainContainerScrollView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0]];
        [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_milestoneView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.faultSegmentedControl attribute:NSLayoutAttributeBottom multiplier:1.0 constant:8.0]];
        [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:self.mainContainerScrollView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_milestoneView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    }

    [self.view layoutSubviews];
    [self.view layoutIfNeeded];
}

- (void)updateAndShowFaultSummaryView
{
    [_milestoneView removeFromSuperview];
    [_faultSummaryView removeFromSuperview];
    [_summaryView removeFromSuperview];
    [_bottomView removeFromSuperview];

    [_faultSummaryView updateFaultSummaryViewWithFault:self.faultDetailsViewModel.fault];

    if ([self.faultDetailsViewModel.fault.status isEqualToString:@"Completed"] || [self.faultDetailsViewModel.fault.status isEqualToString:@"Fault Cancelled"]) {

        _closedActionView.translatesAutoresizingMaskIntoConstraints = NO;
        _bottomView = _closedActionView;
    } else {

        if (self.faultDetailsViewModel.fault.btnSecondaryClickID == 3 && self.faultDetailsViewModel.fault.btnPrimaryClickID == 8) {
            [_takeActionView updateTakeActionWithShowAmendView:YES andShowCancelFault:YES];
            [self updateVisibilityOfAddAppointmentToCalenderView];
        }
        else if (self.faultDetailsViewModel.fault.btnSecondaryClickID == 3)
        {
            [_takeActionView updateTakeActionWithShowAmendView:NO andShowCancelFault:YES];
        }
        else if (self.faultDetailsViewModel.fault.btnPrimaryClickID == 8)
        {
            [_takeActionView updateTakeActionWithShowAmendView:YES andShowCancelFault:NO];
            [self updateVisibilityOfAddAppointmentToCalenderView];
        }
        else
        {
            [_takeActionView updateTakeActionWithShowAmendView:NO andShowCancelFault:NO];
        }

        _takeActionView.translatesAutoresizingMaskIntoConstraints = NO;
        _bottomView = _takeActionView;

    }

    _faultSummaryView.translatesAutoresizingMaskIntoConstraints = NO;
    _summaryView.translatesAutoresizingMaskIntoConstraints = NO;
    _bottomView.translatesAutoresizingMaskIntoConstraints = NO;
    [_summaryView addSubview:_faultSummaryView];
    [_summaryView addSubview:_bottomView];

    [_summaryView addConstraint:[NSLayoutConstraint constraintWithItem:_faultSummaryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:_summaryView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [_summaryView addConstraint:[NSLayoutConstraint constraintWithItem:_faultSummaryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:_summaryView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [_summaryView addConstraint:[NSLayoutConstraint constraintWithItem:_faultSummaryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_summaryView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [_summaryView addConstraint:[NSLayoutConstraint constraintWithItem:_bottomView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:_summaryView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [_summaryView addConstraint:[NSLayoutConstraint constraintWithItem:_bottomView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:_summaryView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [_summaryView addConstraint:[NSLayoutConstraint constraintWithItem:_bottomView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_faultSummaryView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    [_summaryView addConstraint:[NSLayoutConstraint constraintWithItem:_summaryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_bottomView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];

    [self.mainContainerScrollView addSubview:_summaryView];

    [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_summaryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.mainContainerScrollView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_summaryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.mainContainerScrollView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_summaryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.faultSegmentedControl attribute:NSLayoutAttributeBottom multiplier:1.0 constant:10.0]];
    [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:self.mainContainerScrollView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_summaryView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:8.0]];

    [self.view layoutSubviews];
    [self.view layoutIfNeeded];

}

/*
 Updates the visibility of add appointment to calender view
 */
- (void)updateVisibilityOfAddAppointmentToCalenderView
{
    if (self.faultDetailsViewModel.fault.faultAppointmentDetail.isHourAccessPresent)
    {
        if (![AppManager isValidDate:self.faultDetailsViewModel.fault.faultAppointmentDetail.bookedStartDate] || ([self.faultDetailsViewModel.fault.faultAppointmentDetail.bookedStartDate timeIntervalSinceNow] < 0.0) || ([self.faultDetailsViewModel getHourFromText:self.faultDetailsViewModel.fault.faultAppointmentDetail.earlierAccessTime] >= [self.faultDetailsViewModel getHourFromText:self.faultDetailsViewModel.fault.faultAppointmentDetail.latestAccessTime]))
        {
            [_takeActionView hideAddAppointmentToCalenderView];
        }
        else
        {
            [_takeActionView showAddAppointmentToCalenderView];
        }
    }
    else
    {
        if (![AppManager isValidDate:self.faultDetailsViewModel.fault.faultAppointmentDetail.bookedEndDate] || ([self.faultDetailsViewModel.fault.faultAppointmentDetail.bookedEndDate timeIntervalSinceNow] < 0.0))
        {
            [_takeActionView hideAddAppointmentToCalenderView];
        }
        else
        {
            [_takeActionView showAddAppointmentToCalenderView];
        }
    }
}


/*
 Updates the UI after getting user details
 */
- (void)updateUserInterface {

//    self.faultReferenceLabel.text = [NSString stringWithFormat:@"Ref: %@",self.faultDetailsViewModel.fault.faultReference];
    self.faultReferenceLabel.text = self.faultDetailsViewModel.fault.faultReference;
    self.faultDescriptionLabel.text = self.faultDetailsViewModel.productGroup;

    NSString *statusString = [self.faultDetailsViewModel.fault.status lowercaseString];


    if ([statusString rangeOfString:@"Fault" options:NSCaseInsensitiveSearch].location == 0) {
        NSString *finalString = [self.faultDetailsViewModel.fault.status substringFromIndex:6];

        //self.statusLabel.text = [finalString stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[finalString substringToIndex:1] uppercaseString]];
        [self.statusLabel setOrderStatus:[finalString stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[finalString substringToIndex:1] uppercaseString]]];


    }
    else
    {

        //self.statusLabel.text = [statusString stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[statusString substringToIndex:1] uppercaseString]];
        [self.statusLabel setOrderStatus:[statusString stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[statusString substringToIndex:1] uppercaseString]]];


    }

    self.faultImageView.image = [UIImage imageNamed:[self.faultDetailsViewModel getImageWithProductName]];

//    // (LP) Update status color according to fault status
//    if ([self.faultDetailsViewModel.fault.colourwithStatus isEqualToString:@"status-green"]) {
//        self.statusLabel.textColor = [BrandColours colourMyBtGreen];
//    } else if ([self.faultDetailsViewModel.fault.colourwithStatus isEqualToString:@"status-orange"]) {
//        self.statusLabel.textColor = [BrandColours colourMyBtOrange];
//    } else {
//        self.statusLabel.textColor = [BrandColours colourMyBtRed];
//    }

    [self hideorShowControls:NO];
    [self updateAndShowFaultSummaryView];

}



#pragma mark- BTFaultTrackerNodeViewDelegate
- (void)userPressedViewMessageButtonOnFaultMilestoneNodeView:(BTFaultTrackerNodeView *)faultMilestoneNodeView withBTFaultMilestoneNode:(BTFaultMilestoneNode *)faultMileStoneNode{

    BTMessageFromAgentViewController *messageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BTMessageFromAgentViewController"];
    messageViewController.messageDate = faultMileStoneNode.milestoneDateTime;
    messageViewController.message = faultMileStoneNode.milestoneText;

    UINavigationController *messageNavigationController = [[UINavigationController alloc] initWithRootViewController:messageViewController];

    [self presentViewController:messageNavigationController animated:YES completion:nil];

}

#pragma mark - BTEmptyOrderSummaryAndMilestoneViewDelegate Methods
- (void)userPressedNeedHelpButtonOnEmptyOrderSummaryAndMilestoneView:(BTEmptyOrderSummaryAndMilestoneView *)emptyOrderSummaryAndMilestoneView
{
    if ([AppManager isInternetConnectionAvailable])
    {
        _networkRequestInProgress = YES;
        [self hideLoadingItems:NO];
        [_loadingView startAnimatingLoadingIndicatorView];
        [self.faultDetailsViewModel checkForLiveChatAvailibilityForFault];
    }
    else
    {
        [self showAlertWithTitle:kNoConnectionTitle andMessage:kNoConnectionMessage];
    }
}

#pragma mark- BTFaultAmendViewControllerDelegate method

- (void)bTFaultAmendSubmitSuccessfullyOnFaultAmendViewController:(BTFaultAmendViewController *)controller isEngineerAppointmentChanged:(BOOL)isChanged andAppointmentDetail:(BTFaultAppointmentDetail *)faultAppointmentDetail {

    [self showAlertBannerWithMessage:@"Fault details updated"];
    
    if (isChanged)
    {
        if (self.faultDetailsViewModel.fault.faultAppointmentDetail != nil)
        {
            BOOL isAlreadyAddedAndNotAbleToDelete = [self checkForAlreadyAddedAppointmentAndDeleteForCurrentFault];
            
            if (isAlreadyAddedAndNotAbleToDelete)
            {
                DDLogError(@"Error: Not able to remove appointment from calender");
            }
            
            [self.faultDetailsViewModel.fault.faultAppointmentDetail updateBookedStartDateWithNSDate: faultAppointmentDetail.bookedStartDate];
            [self.faultDetailsViewModel.fault.faultAppointmentDetail updateBookedEndDateWithNSDate: faultAppointmentDetail.bookedEndDate];
            [self.faultDetailsViewModel.fault.faultAppointmentDetail updateLatestAccessTimeWithString: faultAppointmentDetail.latestAccessTime];
            [self.faultDetailsViewModel.fault.faultAppointmentDetail updateearlierAccessTimeWithString: faultAppointmentDetail.earlierAccessTime];
            
        }
        
        [self updateVisibilityOfAddAppointmentToCalenderView];
        
        if (_selectedTab == TrackFaultDetailsTypeFaultSummary)
        {
            [self updateAndShowFaultSummaryView];
        }
    }
}


#pragma mark - DLMFaultDetailsScreenDelegate Methods
- (void)successfullyFetchedFaultSummaryDetailsDataOnFaultDetailsScreen:(DLMFaultDetailsScreen *)faultDetailsScreen
{
    self.networkRequestInProgress = NO;

    if (self.faultDetailsViewModel.fault != nil) {

        DDLogInfo(@"Fault Summary: Fault summary details fetched successfully");


        // (lp) Retry to fetch fault details
        [self hideRetryItems:YES];
        [self hideLoadingItems:YES];
        [_loadingView stopAnimatingLoadingIndicatorView];

        // (lp) Update UI with fault summary data
        [self updateUserInterface];
    }
    else
    {
        DDLogError(@"Fault Details: Fetch fault details failed");

        // (lp) Retry to fetch fault details
        [self hideRetryItems:NO];
        [self hideLoadingItems:YES];
        [_loadingView stopAnimatingLoadingIndicatorView];
    }
}

- (void)faultDetailsScreen:(DLMFaultDetailsScreen *)faultDetailsScreen failedToFetchFaultSummaryDetailsDataWithWebServiceError:(NLWebServiceError *)error
{
    DDLogError(@"Fault Summary: Fetch fault details failed");
    self.networkRequestInProgress = NO;

    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if([error.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {
        
        switch (error.error.code)
        {
                
            case BTNetworkErrorCodeAPINoDataFound:
            {
                [AppManager trackNoDataFoundErrorOnPage:_pageNameForOmniture];
                
                [self hideLoadingItems:YES];
                [_loadingView stopAnimatingLoadingIndicatorView];
                
                // No fault found
                UILabel *errorLabel = [[UILabel alloc] init];
                errorLabel.text = @"NO RESULTS FOUND";
                errorLabel.textColor = [UIColor blackColor];
                errorLabel.textAlignment = NSTextAlignmentCenter;
                errorLabel.translatesAutoresizingMaskIntoConstraints = NO;
                [self.view addSubview:errorLabel];
                [self.view addConstraint:[NSLayoutConstraint constraintWithItem:errorLabel attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
                [self.view addConstraint:[NSLayoutConstraint constraintWithItem:errorLabel attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
                [self.view addConstraint:[NSLayoutConstraint constraintWithItem:errorLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
                
                errorHandled = YES;
                
                break;
            }
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }
    
    if(errorHandled == NO)
    {
        // (lp) Retry to fetch fault summary details
        [self showRetryViewWithInternetStrip:NO];
        [self hideLoadingItems:YES];
        [_loadingView stopAnimatingLoadingIndicatorView];
        [AppManager trackGenericAPIErrorOnPage:_pageNameForOmniture];
    }
    
}

- (void)successfullyCancelledFaultSummaryDetailsDataOnFaultDetailsScreen:(DLMFaultDetailsScreen *)faultDetailsScreen{

    self.networkRequestInProgress = NO;


    if (self.faultDetailsViewModel.fault != nil) {

        DDLogInfo(@"Fault Summary: Fault summary details cancel successfully");

        // (lp) Retry to fetch fault details
        [self hideRetryItems:NO];
        [self hideLoadingItems:YES];
        [_loadingView stopAnimatingLoadingIndicatorView];

        // (lp) Update UI with fault summary data
        [self updateUserInterface];

        [self showAlertBannerWithMessage:@"Fault cancelled successfully"];
    }
    else
    {
        DDLogError(@"Fault Details: Cancel fault details failed");

        [self hideLoadingItems:YES];
        [_loadingView stopAnimatingLoadingIndicatorView];
        
        // (lp) Didn't get proper response
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Message" message:kDefaultErrorMessage preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                   {
                                       [alertController dismissViewControllerAnimated:YES completion:nil];
                                   }];
        
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    
}


- (void)faultDetailsScreen:(DLMFaultDetailsScreen *)faultDetailsScreen failedToCancelFaultSummaryDetailsDataWithWebServiceError:(NLWebServiceError *)error
{
    DDLogError(@"Fault Summary: Cancel fault failed");
    self.networkRequestInProgress = NO;
    
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];

    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if(errorHandled == NO)
    {
        // (lp) Cancel this fault failed
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Message" message:kDefaultErrorMessage preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
        {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
        [AppManager trackGenericAPIErrorOnPage:_pageNameForOmniture];
    }
    
}

- (void)liveChatAvailableForFaultWithScreen:(DLMFaultDetailsScreen *)faultDetailsScreen
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
    
    viewController.targetURLType = BTTargetURLTypeFaultsChatScreen;
    
    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:NO andIsNeedCloseButton:YES];
    
    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{
        
    }];
    
}

- (void)faultDetailsScreen:(DLMFaultDetailsScreen *)faultDetailsScreen liveChatCurrentlyNotAvailableWithSpecificCategory:(NSArray *)specificCategory generalCategory:(NSArray *)generalCategory andCurrentTime:(NSString *)currentTime
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BTHelpAndSupportViewController *helpAndSupportController = [BTHelpAndSupportViewController getHelpAndSupportViewController];
    helpAndSupportController.isLaunchingFromHomeScreen = NO;
    helpAndSupportController.isDataAlreadyFetched = YES;
    helpAndSupportController.specificCategory = specificCategory;
    helpAndSupportController.generalCategory = generalCategory;
    helpAndSupportController.currentTime = currentTime;
    
    [self.navigationController pushViewController:helpAndSupportController animated:YES];
}

- (void)faultDetailsScreen:(DLMFaultDetailsScreen *)faultDetailsScreen failedToCheckLiveChatAvailabilityWithWebServiceError:(NLWebServiceError *)error
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if(errorHandled == NO)
    {
        [self showAlertWithTitle:@"Message" andMessage:kDefaultErrorMessage];
        [AppManager trackGenericAPIErrorOnPage:_pageNameForOmniture];
    }
    
}

#pragma mark - Omniture reporting methods

- (void)trackOmniPage:(NSString *)page  withFaultRef:(NSString *)faultRef
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:[AppManager getOmniDictionary]];
    
    [params removeObjectForKey:kOmniLoginStatus];
    if(faultRef && faultRef.length>0)
    {
        NSString *ref = [NSString stringWithFormat:@"%@%@",kOmniFaultRefFormat,faultRef];
        [params setValue:ref forKey:kOmniFaultRef];
    }
    [params setValue:@"Fault details" forKey:kOmniKeyTask];
    
    [OmnitureManager trackPage:page withContextInfo:params];

}

- (void)trackOmniClick:(NSString *)clickEvent forPage:(NSString *)page withFaultRef:(NSString *)faultRef
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:[AppManager getOmniDictionary]];
    NSString *pageName = page;
    NSString *linkTitle = clickEvent;
    [params removeObjectForKey:kOmniLoginStatus];
    if(faultRef && faultRef.length>0)
    {
        NSString *ref = [NSString stringWithFormat:@"%@%@",kOmniFaultRefFormat,faultRef];
        [params setValue:ref forKey:kOmniFaultRef];
    }
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:params];
}

- (void)trackPageWithKeyTask:(NSString *)keytask
{
    NSMutableDictionary *contextDict = [NSMutableDictionary dictionary];
    
    if(self.faultReference && self.faultReference.length>0)
    {
        NSString *ref = [NSString stringWithFormat:@"%@%@",kOmniFaultRefFormat,self.faultReference];
        [contextDict setValue:ref forKey:kOmniFaultRef];
    }
    
    NSString *pageName = OMNIPAGE_FAULT_DETAIL;
    [contextDict setValue:keytask forKey:kOmniKeyTask];
    [OmnitureManager trackPage:pageName withContextInfo:contextDict];
}


@end
