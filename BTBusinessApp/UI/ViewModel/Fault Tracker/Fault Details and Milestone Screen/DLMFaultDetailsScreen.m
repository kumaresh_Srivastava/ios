//
//  DLMFaultDetailsScreen.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 21/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMFaultDetailsScreen.h"
#import "NLGetFaultSummaryDetailsWebService.h"
#import "NLFaultDetailsWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDUser.h"
#import "CDApp.h"
#import "CDAuthenticationToken.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "BTFault.h"
#import "NLWebServiceError.h"
#import "CDRecentSearchedFault.h"
#import "BTLiveChatAvailabilityChecker.h"

@interface DLMFaultDetailsScreen () <NLGetFaultSummaryDetailsWebServiceDelegate, NLFaultDetailsWebServiceDelegate, BTLiveChatAvailabilityCheckerDelegate> {
    
    BTLiveChatAvailabilityChecker *_liveChatAvailabityChecker;
}

@property (nonatomic) NLGetFaultSummaryDetailsWebService *getFaultSummaryDetailsWebService;
@property (nonatomic) NLFaultDetailsWebService *faultDetailsWebService;

@end

@implementation DLMFaultDetailsScreen

- (void)fetchFaultSummaryDetailsForFaultReference:(NSString *)faultRef
{    
    [self setupToFetchFaultSummaryDetailsForFaultReference:faultRef];
    [self callFaultSummaryDetails];
}

- (void) setupToFetchFaultSummaryDetailsForFaultReference:(NSString *)faultRef {
    
    self.getFaultSummaryDetailsWebService = [[NLGetFaultSummaryDetailsWebService alloc] initWithFaultRef:faultRef];
    self.getFaultSummaryDetailsWebService.getFaultSummaryDetailsWebServiceDelegate = self;
}

- (void) callFaultSummaryDetails {
    
   [self.getFaultSummaryDetailsWebService resume];
}

- (void)fetchFaultSummaryDetailsForFaultReference:(NSString *)faultRef andAssetId:(NSString *)assetId
{
    
    [self setupToFetchFaultSummaryDetailsForFaultReference:faultRef andAssetId:assetId];
    [self callFaultSummaryDetailsWithAssetID];
}

- (void) setupToFetchFaultSummaryDetailsForFaultReference:(NSString *)faultRef andAssetId:(NSString *)assetId {
    
    self.faultDetailsWebService = [[NLFaultDetailsWebService alloc] initWithFaultID:faultRef andAssetID:assetId];
    self.faultDetailsWebService.getFaultDetailsWebServiceDelegate = self;
}

- (void) callFaultSummaryDetailsWithAssetID {
    [self.faultDetailsWebService resume];
}

- (void)cancelSummaryDetailsForFaultReference:(NSString *)faultRef andAssetId:(NSString *)assetId{
    
    [self setupToCancelSummaryDetailsForFaultReference:faultRef andAssetId:assetId];
    [self callCancelSummaryDetails];
    
}

- (void) setupToCancelSummaryDetailsForFaultReference:(NSString *)faultRef andAssetId:(NSString *)assetId {
    
    self.faultDetailsWebService = [[NLFaultDetailsCancelWebService alloc] initWithFaultID:faultRef assetID:assetId];
    self.faultDetailsWebService.getFaultDetailsWebServiceDelegate = self;
}

- (void) callCancelSummaryDetails {
    
    [self.faultDetailsWebService resume];
}

#pragma mark Public Methods

- (NSString *)getImageWithProductName
{
    NSString *image = nil;
    
    if ([self.productIcon isEqualToString:@"#icon-telephone"]) {
        image = @"pstn_fault";
    } else if ([self.productIcon isEqualToString:@"#icon-broadband-hub"]) {
        image = @"broadband_fault";
    } else if ([self.productIcon isEqualToString:@"#icon-isdn"]) {
        image = @"pstn_fault";
    } else if ([self.productIcon isEqualToString:@"#icon-email"]) {
        image = @"broadband_fault";
    }
    
    return image;
}

- (NSInteger)getHourFromText:(NSString *)hourText
{
    //Removing colon and zeros after colon
    NSString *newString = [hourText stringByReplacingOccurrencesOfString:@":00" withString:@""];
    newString = [newString stringByReplacingOccurrencesOfString:@":00" withString:@""];
    
    //Removing leading zero
    NSRange range = [newString rangeOfString:@"^0*" options:NSRegularExpressionSearch];
    newString = [newString stringByReplacingCharactersInRange:range withString:@""];
    
    NSString *amPmText = [newString substringFromIndex:newString.length-2];
    
    NSString *hourString = [newString substringWithRange:NSMakeRange(0, newString.length-2)];
    
    if ([amPmText isEqualToString:@"am"])
    {
        NSInteger hourValue = [hourString integerValue];
        if (hourValue == 12)
        {
            return 0;
        }
        else
        {
            return hourValue;
        }
    }
    else
    {
        NSInteger hourValue = [hourString integerValue];
        if (hourValue == 12)
        {
            return 12;
        }
        else
        {
            return hourValue + 12;
        }
    }
}

- (void)checkForLiveChatAvailibilityForFault
{
    [self setupToCheckForLiveChatAvailibilityForFault];
    [self callLiveChatAvailableSlots];
}

- (void) setupToCheckForLiveChatAvailibilityForFault {
    
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = [[BTLiveChatAvailabilityChecker alloc] init];
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = self;
}

- (void) callLiveChatAvailableSlots {
    
  [_liveChatAvailabityChecker getLiveChatAvailableSlots];
}

#pragma mark Canceling methods

- (void)cancelDetailsAPIWithAssetID {
    self.faultDetailsWebService.getFaultDetailsWebServiceDelegate = nil;
    [self.faultDetailsWebService cancel];
    self.faultDetailsWebService = nil;
}

- (void)cancelDetailsAPI {
    self.getFaultSummaryDetailsWebService.getFaultSummaryDetailsWebServiceDelegate = nil;
    [self.getFaultSummaryDetailsWebService cancel];
    self.getFaultSummaryDetailsWebService = nil;
}

- (void)cancelFaultAPI {
    self.faultDetailsWebService.getFaultDetailsWebServiceDelegate = nil;
    [self.faultDetailsWebService cancel];
    self.faultDetailsWebService = nil;
}

- (void)cancelChatAvailabilityCheckerAPI
{
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    [_liveChatAvailabityChecker cancel];
    _liveChatAvailabityChecker = nil;
}

#pragma mark - NLGetFaultSummaryDetailsWebService methods
- (void)getFaultSummaryDetailsWebService:(NLGetFaultSummaryDetailsWebService *)webService successfullyFetchedFaultSummaryDetailsData:(BTFault *)fault milestoneNodes:(NSArray *)arrayOfMilestoneNodes isAuthenticated:(BOOL)isAuthenticated productGroup:(NSString *)productGroup productImage:(NSString *)productImage
{

    if(self.getFaultSummaryDetailsWebService == webService)
    {
        
        if (fault != nil) {
            
            _fault = fault;
            _arrayOfMilestoneNodes = arrayOfMilestoneNodes;
            _productGroup = productGroup;
            _productIcon = productImage;
            
            // (LP) Save the latest data from web service into the persistence for recently searched.
            
            DDLogInfo(@"Saving data from response of NLGetFaultSummaryWebServiceDelegate API Call into persistence in recent searched items for fault %@.", fault.faultReference);
            
            NSManagedObjectContext *context = [AppDelegate sharedInstance].managedObjectContext;
            CDUser *persistenceUser = [CDUser userWithUsername:[AppDelegate sharedInstance].viewModel.app.loggedInUser.username inManagedObjectContext:context];
            
            NSArray *arrayOfRecentSearchedFaults = [persistenceUser.recentlySearchedFaults allObjects];
            
            CDRecentSearchedFault *searchedFault = nil;
            for (CDRecentSearchedFault *recentSearchedFault in arrayOfRecentSearchedFaults) {
                if ([recentSearchedFault.faultRef isEqualToString:fault.faultReference]) {
                    searchedFault = recentSearchedFault;
                }
            }
            
            if (searchedFault == nil) {
                searchedFault = [CDRecentSearchedFault newRecentSearchedFaultInManagedObjectContext:context];
                searchedFault.faultRef = fault.faultReference;
                searchedFault.faultStatus = fault.status;
                searchedFault.faultDescription = productGroup;
                searchedFault.reportedOnDate = fault.reportedOnDate;
                searchedFault.colourWithStatus = fault.colourwithStatus;
                searchedFault.lastSearchedDate = [NSDate date];
                
                [persistenceUser addRecentlySearchedFaultsObject:searchedFault];
            }
            else
            {
                searchedFault.faultStatus = fault.status;
                searchedFault.lastSearchedDate = [NSDate date];
                
                [persistenceUser addRecentlySearchedFaultsObject:searchedFault];
            }
            
            [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
            
        }
        
        [self.faultDetailsScreenDelegate successfullyFetchedFaultSummaryDetailsDataOnFaultDetailsScreen:self];
        self.getFaultSummaryDetailsWebService.getFaultSummaryDetailsWebServiceDelegate = nil;
        self.getFaultSummaryDetailsWebService = nil;
        
    }
    else{
        
        DDLogError(@"This delegate method gets called because of success of an object of NLGetFaultSummaryDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLGetFaultSummaryDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }

    
}

- (void)getFaultSummaryDetailsWebService:(NLGetFaultSummaryDetailsWebService *)webService failedToFetchFaultSummaryDetailsDataWithWebServiceError:(NLWebServiceError *)error
{
    
    if(self.getFaultSummaryDetailsWebService == webService){
        
       
        [self.faultDetailsScreenDelegate faultDetailsScreen:self failedToFetchFaultSummaryDetailsDataWithWebServiceError:error];
        self.getFaultSummaryDetailsWebService.getFaultSummaryDetailsWebServiceDelegate = nil;
        self.getFaultSummaryDetailsWebService = nil;
        
    }
    else{
        
        DDLogError(@"This delegate method gets called because of failure of an object of NLGetFaultSummaryDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLGetFaultSummaryDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }

}

#pragma mark - NLFaultDetailsWebServiceDelegate Methods

- (void)getFaultDetailsWebService:(NLFaultDetailsWebService *)webService successfullyFetchedFaultDetailsData:(BTFault *)fault milestoneNodes:(NSArray *)mileStoneNodes
{
    
    if(self.faultDetailsWebService == webService){
        
        if (fault != nil) {
            
            _fault = fault;
            [_fault updateReportedOnDate:self.reportedOnDate];
            
            _arrayOfMilestoneNodes = mileStoneNodes;
            
        }
        
        
        if([webService isKindOfClass:[NLFaultDetailsCancelWebService class]]){
            
            [self.faultDetailsScreenDelegate successfullyCancelledFaultSummaryDetailsDataOnFaultDetailsScreen:self];
        }
        else{
            
            [self.faultDetailsScreenDelegate successfullyFetchedFaultSummaryDetailsDataOnFaultDetailsScreen:self];
        }
        
        self.faultDetailsWebService.getFaultDetailsWebServiceDelegate = nil;
        self.faultDetailsWebService = nil;
        
    }
    else{
        
        if([webService isKindOfClass:[NLFaultDetailsCancelWebService class]]){
            
            DDLogError(@"This delegate method gets called because of success of an object of NLFaultDetailsCancelWebService but this object is not the one stored in member variable of this class %@.", [self class]);
            NSAssert(NO, @"This delegate method gets called because of success of an object of NLFaultDetailsCancelWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        }
        else{
            
            DDLogError(@"This delegate method gets called because of success of an object of NLFaultDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
            NSAssert(NO, @"This delegate method gets called because of success of an object of NLFaultDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
            
        }
    }
    
    
}

- (void)getFaultDetailsWebService:(NLFaultDetailsWebService *)webService failedToFetchFaultDetailsDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    
    
    if(self.faultDetailsWebService == webService){
        
        if([webService isKindOfClass:[NLFaultDetailsCancelWebService class]]){
            
           [self.faultDetailsScreenDelegate faultDetailsScreen:self failedToCancelFaultSummaryDetailsDataWithWebServiceError:webServiceError];
            
        }
        else{
            
            [self.faultDetailsScreenDelegate faultDetailsScreen:self failedToFetchFaultSummaryDetailsDataWithWebServiceError:webServiceError];
        }
        
        self.faultDetailsWebService.getFaultDetailsWebServiceDelegate = nil;
        self.faultDetailsWebService = nil;
        
    }
    else{
        
        if([webService isKindOfClass:[NLFaultDetailsCancelWebService class]]){
            
            DDLogError(@"This delegate method gets called because of failure of an object of NLFaultDetailsCancelWebService but this object is not the one stored in member variable of this class %@.", [self class]);
            NSAssert(NO, @"This delegate method gets called because of failure of an object of NLFaultDetailsCancelWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        }
        else{
            
            DDLogError(@"This delegate method gets called because of failure of an object of NLFaultDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
            NSAssert(NO, @"This delegate method gets called because of failure of an object of NLFaultDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
            
        }
    }
    
}

#pragma mark - BTLiveChatAvailabilityCheckerDelegate Methods
- (void)liveChatAvailabilityChecker:(BTLiveChatAvailabilityChecker *)liveChatAvailabilityChecker successfullyFetchedSlotsWithGeneralCategory:(NSArray *)generalCategoryArray specificeCategory:(NSArray *)specificCategory andCurrentTime:(NSString *)currentTime
{
    BOOL chatAvailable = [BTLiveChatAvailabilityChecker checkAvailabilityWithSpecificCategory:specificCategory currentTime:currentTime andChatType:BTChatTypeFault];
    
    if (chatAvailable)
    {
        [self.faultDetailsScreenDelegate liveChatAvailableForFaultWithScreen:self];
    }
    else
    {
        [self.faultDetailsScreenDelegate faultDetailsScreen:self liveChatCurrentlyNotAvailableWithSpecificCategory:specificCategory generalCategory:generalCategoryArray andCurrentTime:currentTime];
    }
    
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = nil;
}

- (void)liveChatAvailabilityChecker:(BTLiveChatAvailabilityChecker *)liveChatAvailabilityChecker failedToFetchSlotsWithWebServiceError:(NLWebServiceError *)webServiceError
{
    [self.faultDetailsScreenDelegate faultDetailsScreen:self failedToCheckLiveChatAvailabilityWithWebServiceError:webServiceError];
    
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = nil;
}

@end
