//
//  BTFaultDetailsMilestoneViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 21/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BTFault;
@class DLMFaultDetailsScreen;

typedef NS_ENUM(NSUInteger, TrackFaultDetailsType) {
    TrackFaultDetailsTypeFaultSummary = 0,
    TrackFaultDetailsTypeFaultTracker = 1
};

@interface BTFaultDetailsMilestoneViewController : UIViewController

@property (nonatomic, readwrite) DLMFaultDetailsScreen *faultDetailsViewModel;

@property (nonatomic, copy) NSString *faultReference;
@property (nonatomic, copy) NSString *assetId;
@property (nonatomic) BOOL redirectedFromDashboard;
@property (nonatomic) BOOL bacNeeded;
@property (nonatomic) BOOL networkRequestInProgress;

@end
