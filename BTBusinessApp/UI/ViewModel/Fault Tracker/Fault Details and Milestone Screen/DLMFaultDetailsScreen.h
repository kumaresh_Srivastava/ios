//
//  DLMFaultDetailsScreen.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 21/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
@class DLMFaultDetailsScreen;
@class BTFault;
@class NLWebServiceError;

@protocol DLMFaultDetailsScreenDelegate <NSObject>

- (void)successfullyFetchedFaultSummaryDetailsDataOnFaultDetailsScreen:(DLMFaultDetailsScreen *)faultDetailsScreen;

- (void)faultDetailsScreen:(DLMFaultDetailsScreen *)faultDetailsScreen failedToFetchFaultSummaryDetailsDataWithWebServiceError:(NLWebServiceError *)error;

- (void)successfullyCancelledFaultSummaryDetailsDataOnFaultDetailsScreen:(DLMFaultDetailsScreen *)faultDetailsScreen;//Salman
- (void)faultDetailsScreen:(DLMFaultDetailsScreen *)faultDetailsScreen failedToCancelFaultSummaryDetailsDataWithWebServiceError:(NLWebServiceError *)error;

- (void)liveChatAvailableForFaultWithScreen:(DLMFaultDetailsScreen *)faultDetailsScreen;
- (void)faultDetailsScreen:(DLMFaultDetailsScreen *)faultDetailsScreen liveChatCurrentlyNotAvailableWithSpecificCategory:(NSArray *)specificCategory generalCategory:(NSArray *)generalCategory andCurrentTime:(NSString *)currentTime;
- (void)faultDetailsScreen:(DLMFaultDetailsScreen *)faultDetailsScreen failedToCheckLiveChatAvailabilityWithWebServiceError:(NLWebServiceError *)error;

@end

@interface DLMFaultDetailsScreen : DLMObject

@property (nonatomic, weak) id <DLMFaultDetailsScreenDelegate> faultDetailsScreenDelegate;

@property (nonatomic, strong) BTFault *fault;
@property (nonatomic, copy) NSArray *arrayOfMilestoneNodes;
@property (nonatomic, copy) NSString *productGroup;
@property (nonatomic, copy) NSString *productIcon;
@property (nonatomic, strong) NSDate *reportedOnDate;
@property (nonatomic, strong) NSDate *engineerAppointmentDate;

- (NSString *)getImageWithProductName;
- (void)fetchFaultSummaryDetailsForFaultReference:(NSString *)faultRef;
- (void)fetchFaultSummaryDetailsForFaultReference:(NSString *)faultRef andAssetId:(NSString *)assetId;
- (void)cancelSummaryDetailsForFaultReference:(NSString *)faultRef andAssetId:(NSString *)assetId; //Salman

- (void)cancelDetailsAPIWithAssetID;
- (void)cancelDetailsAPI;
- (void)cancelFaultAPI;
- (void)cancelChatAvailabilityCheckerAPI;

- (NSInteger)getHourFromText:(NSString *)hourText;
- (void)checkForLiveChatAvailibilityForFault;

@end
