//
//  BTFaultTrackerDashboardViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 27/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppConstants.h"


@interface BTFaultTrackerDashboardViewController : UIViewController

@property (nonatomic) BOOL networkRequestInProgress;
@property (nonatomic) NSArray *cugs;
@property (nonatomic) NSDictionary *firstPageOpenFaultDataDict;

@end
