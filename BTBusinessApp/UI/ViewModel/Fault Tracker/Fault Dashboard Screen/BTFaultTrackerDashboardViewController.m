//
//  BTFaultTrackerDashboardViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 27/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultTrackerDashboardViewController.h"
#import "MBProgressHUD.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "UIViewController+WebServiceErrorHandling.h"
#import "BTUICommon.h"
#import "BTFaultTrackerDashboardTableViewCell.h"
#import "BTFaultTrackerSummaryViewController.h"
#import "DLMFaultDashBoardScreen.h"
#import "BTFaultTrackerSearchViewController.h"
#import "BTFault.h"
#import "BTFaultDashBoardTableFooterView.h"
#import "BTFaultRestrictedDetailsViewController.h"
#import "BTFaultEmptyDashboardView.h"
#import "BTRetryView.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "BTCug.h"
#import "DLMFaultSummaryScreen.h"
#import "DLMFaultDetailsScreen.h"
#import "CustomSpinnerView.h"
#import "BTFaultDetailsMilestoneViewController.h"
#import "OmnitureManager.h"
#import "NLWebServiceError.h"
#import "AppManager.h"
#import "BTHelpAndSupportViewController.h"
#import "BTInAppBrowserViewController.h"
#import "BTNavigationViewController.h"
#import "BTAccountLocationDropdownView.h"

#define kBoldFont @"BTFont-Bold"

@interface BTFaultTrackerDashboardViewController ()<UITableViewDataSource, UITableViewDelegate, DLMFaultDashBoardScreenDelegate, BTFaultDashBoardFooterDelegate, BTRetryViewDelegate, BTFaultEmptyDashboardViewDelegate> {
    BTFaultDashBoardTableFooterView *faultDashboardFooterView;
    BTFaultEmptyDashboardView *_emptyDashboardView;
    BTRetryView *_retryView;
    BTAccountLocationDropdownView *accountDropdownView;
    BOOL _isRetryShown, _isEmptyViewShown, _isFooterViewCreated;
    BOOL _isLazyLoadingInOpenFault, _isLazyLoadingInCompleted, _islazyLoadingOpenFaultClosed, _isLazyLoadingCompletedClosed;
    BOOL _isTabSwitch, _isViewDidLoadCalled;
    BOOL _isFetchingWhileScrolling;
    BOOL _isResetGroup;
    BOOL _isRetryViewForOpenFault,_isRetryViewShownForClosedFault;
    
    // (lp) Both flags used for status Initial API calls for open and closed faults
    BOOL _apiExecutedForOpen,_apiExecutedForRecentlyClosed;
}

@property (strong, nonatomic) IBOutlet UITableView *faultDashboardTableView;

@property (nonatomic, strong) NSMutableArray *faultDataArray;

@property (nonatomic, strong) UIBarButtonItem *rightBarButtonItem;
@property (nonatomic) DLMFaultDashBoardScreen *openFaultViewModel;
@property (nonatomic) DLMFaultDashBoardScreen *recentlyUsedFaultViewModel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *searchBarButtonItem;
@property (weak, nonatomic) IBOutlet UISegmentedControl *faultSegmentedControl;
@property (strong, nonatomic) IBOutlet UIView *segmentContainerView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *segmentContainerTopConstraint;

@property (nonatomic,strong) NSMutableArray *openFaultData;
@property (nonatomic,strong) NSMutableArray *completedFaultData;
@property (nonatomic,assign) int trackFaultType;
@property (nonatomic,strong) NSString *currentGroupKey;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *groupNameLabel;
@property (strong, nonatomic) CustomSpinnerView *loadingView;


@end

@implementation BTFaultTrackerDashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    _isViewDidLoadCalled = YES;
    // Do any additional setup after loading the view.
    self.openFaultViewModel = [[DLMFaultDashBoardScreen alloc] initWithFirstPageOpenFaultDataDict:self.firstPageOpenFaultDataDict];
    self.openFaultViewModel.faultDashBoardDelegate = self;
    
    self.recentlyUsedFaultViewModel = [[DLMFaultDashBoardScreen alloc] init];
    self.recentlyUsedFaultViewModel.faultDashBoardDelegate = self;
    
    if(!self.cugs) {
        self.cugs = [[AppDelegate sharedInstance] savedCugs];
    }
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationBar_background"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName:[UIColor whiteColor],
                                                                      NSFontAttributeName:[UIFont fontWithName:kBtFontBold size:20                      ]
                                                                      }];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.titleLabel.text = @"Track faults";
    [self.titleLabel setFont:[UIFont fontWithName:kBtFontBold size:20.0f]];
    [self updateGroupSelection];
    //order details table view config
    self.faultDashboardTableView.delegate = self;
    self.faultDashboardTableView.dataSource = self;
    self.faultDashboardTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.faultDashboardTableView.backgroundColor = [UIColor clearColor];
    self.view.backgroundColor = [UIColor colorForHexString:@"eeeeee"];//[UIColor whiteColor];
    
    _faultSegmentedControl.backgroundColor = [UIColor clearColor];
    
    self.openFaultData = [NSMutableArray array];
    self.completedFaultData = [NSMutableArray array];
    
    _isRetryShown = NO;
    _isEmptyViewShown = NO;
    _isFooterViewCreated = NO;
    _isTabSwitch = NO;
    _isFetchingWhileScrolling = NO;
    _isResetGroup = NO;
    [self createLoadingView];
    
    __weak typeof(self) weakSelf = self;
    
    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([weakSelf networkRequestInProgress]) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf createLoadingView];
                [weakSelf hideLoadingItems:NO];
                [weakSelf.loadingView startAnimatingLoadingIndicatorView];
            });
            
        }
        
        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf hideLoadingItems:YES];
                [weakSelf.loadingView stopAnimatingLoadingIndicatorView];
            });
        }
    }];
    
    self.faultDataArray = [NSMutableArray array];
    
    UINib *dataCell = [UINib nibWithNibName:@"BTFaultTrackerDashboardTableViewCell" bundle:nil];
    [self.faultDashboardTableView registerNib:dataCell forCellReuseIdentifier:@"BTFaultTrackerDashboardTableViewCell"];
    
    if (self.cugs.count > 1) {
        accountDropdownView = [[[NSBundle mainBundle] loadNibNamed:@"BTAccountLocationDropdownView" owner:self options:nil] firstObject];
        BTCug *firstCug = self.cugs[0];
        accountDropdownView.locationLabel.text = firstCug.cugName;
        UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(groupBarButtonAction:)];
        [accountDropdownView addGestureRecognizer:tapped];
        
       /* [self.view insertSubview:accountDropdownView atIndex:0];
        [self.view removeConstraint:self.segmentContainerTopConstraint];
        [self.view addConstraints:@[
                                    [NSLayoutConstraint constraintWithItem:accountDropdownView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0],
                                    [NSLayoutConstraint constraintWithItem:accountDropdownView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                    [NSLayoutConstraint constraintWithItem:accountDropdownView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0],
                                    [NSLayoutConstraint constraintWithItem:accountDropdownView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.segmentContainerView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]
                                    
                                    ]];
        */
        
        [self.view addSubview:accountDropdownView];
        
        accountDropdownView.frame = CGRectMake(0,self.segmentContainerView.frame.size.height/2 + 28, accountDropdownView.frame.size.width, accountDropdownView.frame.size.height);
        //[self.view removeConstraint:self.segmentContainerTopConstraint];
        
        
        [self.view addConstraints:@[
                                    
                                    [NSLayoutConstraint constraintWithItem:accountDropdownView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.segmentContainerView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-14.0],
                                    
                                    [NSLayoutConstraint constraintWithItem:accountDropdownView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                    [NSLayoutConstraint constraintWithItem:accountDropdownView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0],
                                    
                                    [NSLayoutConstraint constraintWithItem:accountDropdownView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.faultDashboardTableView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]
                                    
                                    
                                    ]];
        
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        
    }
    
    
   
    
    self.trackFaultType = TrackFaultDashBoardTypeOpenOrder;
    self.openFaultViewModel.pageIndex = 1;
    self.recentlyUsedFaultViewModel.pageIndex = 1;
    
    
    [self trackOmniPage:[self getPageNameForCurrentGroup]];
    
    if (self.firstPageOpenFaultDataDict || [AppManager isInternetConnectionAvailable]) {
        
        self.networkRequestInProgress = true;
        [self fetchFaultDashBoardAPIWithPageIndex:1];
        
    } else {
        
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
    }
    
    //[_faultSegmentedControl setTintColor:[BrandColours colourBackgroundBTPurplePrimaryColor]];
    //[_faultSegmentedControl setTitleTextAttributes:[[AppManager sharedAppManager] fetchSegmentedControlFontDict] forState:UIControlStateNormal];


}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    _isViewDidLoadCalled = NO;
    [self setNetworkRequestInProgress:false];
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    if (self.trackFaultType == TrackFaultDashBoardTypeOpenOrder) {
        [self.openFaultViewModel cancelFaultDashBoardAPIRequest];
    } else {
        [self.recentlyUsedFaultViewModel cancelFaultDashBoardAPIRequest];
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    if (!_isViewDidLoadCalled) {
            [self manageAPIRequestBasedOnSegmentSelection];
    }
    
    accountDropdownView.locationLabel.text = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugName;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChangeFaultScreen:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
}

- (void)statusBarOrientationChangeFaultScreen:(NSNotification *)notification {
    // handle the interface orientation as needed
    //self.faultDashboardTableView.tableFooterView = nil;
    //[self resizeTableViewFooterToFit];
    [self performSelector:@selector(resizeTableViewFooterToFit) withObject:nil afterDelay:0.1];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private helper methods


- (void)updateGroupSelection {
    
    [self hideGroupSelectionForOneAvailableGroup];
    if (self.cugs.count == 1) {
        
        return;
    }
    
    BTCug *cug = (BTCug *)[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug;
    [self setCurrentGroupKey:cug.groupKey];
    //self.groupNameLabel.text = cug.cugName;
    self.groupNameLabel.text = @"";
    accountDropdownView.locationLabel.text = cug.cugName;
}

/*
 Updates the currently selected group and UI on the basis of group selection
 */
- (void) checkForSuperUserAndUpdateUIWithIndex:(NSUInteger)index {
    _apiExecutedForOpen = NO;
    _apiExecutedForRecentlyClosed = NO;
    
    for(BTCug *cug in self.cugs) {
        if(index == cug.indexInAPIResponse) {
            [self setCurrentGroupKey:cug.groupKey];
            //[self.groupNameLabel setText:cug.cugName];
            [self.openFaultViewModel changeSelectedCUGTo:cug];
            accountDropdownView.locationLabel.text = cug.cugName;
            [self resetDataAndUIAfterGroupChange];
        }
    }
}

//Checking for the number of groups, if 1 group is available hide topview and groupselection button
- (void)hideGroupSelectionForOneAvailableGroup {
    self.faultSegmentedControl.translatesAutoresizingMaskIntoConstraints = NO;
    NSLayoutConstraint *segmentConstraint = [NSLayoutConstraint constraintWithItem:self.faultSegmentedControl
                                                                         attribute:NSLayoutAttributeTop
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.view
                                                                         attribute:NSLayoutAttributeTop
                                                                        multiplier:1.0
                                                                          constant:10.0];
    [self.view addConstraint:segmentConstraint];
    
    
    [self.groupNameLabel removeFromSuperview];
    UIFont *titleFont = self.titleLabel.font;
    self.titleLabel.font = [titleFont fontWithSize:20];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
}

- (void)updateUserInterfaceWithFaults:(NSArray *)faults tabID:(int)tabID {
    
    if (self.trackFaultType == TrackFaultDashBoardTypeOpenOrder) {
        
        [self.openFaultData addObjectsFromArray:faults];
    } else {
        
        [self.completedFaultData addObjectsFromArray:faults];
    }
    [self.faultDashboardTableView reloadData];
}

- (void)createFooterView {
    
    _isFooterViewCreated = YES;
    faultDashboardFooterView = [[[NSBundle mainBundle] loadNibNamed:@"BTFaultDashBoardTableFooterView" owner:nil options:nil] objectAtIndex:0];
    faultDashboardFooterView.faultDashBoardFooterDelegate = self;
    
    [self.faultDashboardTableView setTableFooterView:faultDashboardFooterView];
    self.faultDashboardTableView.tableFooterView.hidden = NO;
}

/*
 Resize header view of table to update height of header view
 */
- (void)resizeTableViewFooterToFit {
    UITableViewHeaderFooterView *footerView = (UITableViewHeaderFooterView *)self.faultDashboardTableView.tableFooterView;
    
    [faultDashboardFooterView setNeedsLayout];
    [faultDashboardFooterView layoutIfNeeded];
    
    CGFloat height = [faultDashboardFooterView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CGRect frame = faultDashboardFooterView.frame;
    frame.size.height = height;
    faultDashboardFooterView.frame = frame;
    
    self.faultDashboardTableView.tableFooterView = footerView;
    
}

- (void)hideShowFooterIndicatorView:(BOOL)isHide {
    if (!isHide) {
        
        [faultDashboardFooterView hideRetryView];
        _isRetryViewForOpenFault = NO;
        _isRetryViewShownForClosedFault = NO;
        [faultDashboardFooterView startLoadingIndicatorAnimation];
        
        [faultDashboardFooterView.retryIndicatorView startAnimating];
    } else {
        
        [faultDashboardFooterView stopCustomLoadingIndicatorAnimation];
        [faultDashboardFooterView.retryIndicatorView stopAnimating];
    }
}

- (void)resetDataAndUIAfterGroupChange {
    [self clearDataAfterGroupSelection];
    if (_isRetryShown) {
        if([AppManager isInternetConnectionAvailable])
        {
            self.faultDashboardTableView.tableFooterView.hidden = NO;
            [self hideRetryItems:YES];
        }
        else
        {
            [_retryView updateRetryViewWithInternetStrip:YES];
            _isRetryShown = NO;
            [self showRetryViewWithInternetStrip:YES];
            [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
            return;
        }
    }
    if (_isEmptyViewShown) {
        self.faultDashboardTableView.tableFooterView.hidden = NO;
        [self hideEmmptyDashBoardItems:YES];
    }
    
    
    //Check for Internet connection
    if([AppManager isInternetConnectionAvailable])
    {
        [self resetUIAndMakeAPIRequest];
    }
    else
    {
        self.faultDashboardTableView.tableFooterView.hidden = YES;
        [self hideEmmptyDashBoardItems:YES];
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
    }
}


- (BOOL)needToRefresh
{
    BOOL needToRefresh = YES;
    
    if(_retryView)
    {
        if((![AppManager isInternetConnectionAvailable] && !_retryView.isHidden))
            needToRefresh = NO;
    }
    return needToRefresh;
}

- (void)resetUIAndMakeAPIRequest
{
    if (self.faultSegmentedControl.selectedSegmentIndex == 0) {
        self.trackFaultType = TrackFaultDashBoardTypeOpenOrder;
        _isResetGroup = YES;
        [self cancelOpenFaultAPI];
        if (self.openFaultData.count == 0) {
            self.networkRequestInProgress = true;
            self.faultDashboardTableView.tableFooterView.hidden = YES;
            self.openFaultViewModel.pageIndex = 1;
            [self fetchFaultDashBoardAPIWithPageIndex:self.openFaultViewModel.pageIndex];
        }
    } else if (self.faultSegmentedControl.selectedSegmentIndex == 1) {
        self.trackFaultType = TrackFaultDashBoardTypeCompletedOrder;
        _isResetGroup = YES;
        [self cancelCompletedFaultAPI];
        if (self.completedFaultData.count == 0) {
            
            self.networkRequestInProgress = true;
            self.faultDashboardTableView.tableFooterView.hidden = YES;
            self.recentlyUsedFaultViewModel.pageIndex = 1;
            [self fetchFaultDashBoardAPIWithPageIndex:self.recentlyUsedFaultViewModel.pageIndex];
        }
    }
    
}


- (void) clearDataAfterGroupSelection {
    if (self.openFaultData.count > 0) {
        
        [self.openFaultData removeAllObjects];
        [self.faultDashboardTableView reloadData];
    }
    if (self.completedFaultData.count > 0) {
        
        [self.completedFaultData removeAllObjects];
        [self.faultDashboardTableView reloadData];
    }
}


- (void)createLoadingView {
    
    _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
    _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:_loadingView];
    [self.view bringSubviewToFront:_loadingView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}


- (BOOL)isFaultId:(NSString *)inputString
{
    if ([inputString containsString:@"-"]) {
        return true;
    } else {
        return false;
    }
}


- (void)hideLoadingItems:(BOOL)isHide {
    
    // (lp) Hide or Show UI elements related to loading.
    [_loadingView setHidden:isHide];
    
}

- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow {
    
    if(_isRetryShown)
        return;
    
    _isRetryShown = YES;
    
    self.faultDashboardTableView.tableFooterView.hidden = YES;
    if(_retryView){
        [_retryView removeFromSuperview];
    }
    
    _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
    _retryView.translatesAutoresizingMaskIntoConstraints = NO;
    _retryView.retryViewDelegate = self;
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
    
    [self.view addSubview:_retryView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    if(internetStripNeedToShow == YES){
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    } else{
         [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.faultSegmentedControl  attribute:NSLayoutAttributeTop multiplier:1.0 constant:self.faultSegmentedControl.frame.size.height+20]];
    }
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
}

- (void)hideRetryItems:(BOOL)isHide {
    
    _isRetryShown = NO;
    // (lp) Hide or Show UI elements related to retry.
    [_retryView setRetryViewDelegate:nil];
    [_retryView setHidden:isHide];
    _retryView = nil;
    
}

- (void)showEmptyDashBoardView {
    
    if(_isEmptyViewShown)
        return;
    
    _isEmptyViewShown = YES;
    self.faultDashboardTableView.tableFooterView.hidden = YES;
    _emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTFaultEmptyDashboardView" owner:nil options:nil] objectAtIndex:0];
    _emptyDashboardView.translatesAutoresizingMaskIntoConstraints = NO;
    _emptyDashboardView.delegate = self;
    
    
//    CGFloat height;
//
//    height = self.view.frame.size.height - self.faultSegmentedControl.frame.origin.y-self.faultSegmentedControl.frame.size.height;
//
//
//    CGRect frame = _emptyDashboardView.frame;
//    frame.size.height = height;
//    frame.size.width = self.view.frame.size.width;
//    frame.origin.y = self.faultSegmentedControl.frame.origin.y+self.faultSegmentedControl.frame.size.height;
//    _emptyDashboardView.frame  = frame;
    
    NSURL *url;
    
    if ([kBTServerType isEqualToString:@"BTServerTypeProd"]) {
        url = [[NSURL alloc] initWithString:@"https://secure.business.bt.com/Account/Addbillingaccounts.aspx"];
    } else{
        url = [[NSURL alloc] initWithString:@"https://eric1-dmze2e-ukb.bt.com/Account/Addbillingaccounts.aspx"];
    }
    
    NSMutableAttributedString *hypLink = [[NSMutableAttributedString  alloc] initWithString:@"Can't see your fault? We are either still working on processing your fault to view online, or you need to add your account by going here"];
    
    NSDictionary *linkAttributes = @{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle),
                                     NSLinkAttributeName : url};
    
    [hypLink addAttributes:linkAttributes range:NSMakeRange(hypLink.length-4,4)];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
    [hypLink addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,hypLink.length)];
    
    [hypLink addAttribute:NSForegroundColorAttributeName value:[BrandColours colourBtNeutral70] range:NSMakeRange(0, hypLink.length)];
    
    [hypLink addAttribute:NSFontAttributeName
                    value:[UIFont fontWithName:kBtFontRegular size:16.0] range:NSMakeRange(0, hypLink.length)];
    
    [hypLink addAttribute:NSFontAttributeName
                    value:[UIFont fontWithName:kBtFontBold size:18.0] range:NSMakeRange(hypLink.length-4,4)];
    
    [_emptyDashboardView updateEmptyDashboardViewWithdetailText:hypLink];
    
    [self.view addSubview:_emptyDashboardView];
    
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.faultDashboardTableView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.faultDashboardTableView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.faultDashboardTableView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.faultDashboardTableView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]
                                ]];
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
}

- (void)hideEmmptyDashBoardItems:(BOOL)isHide {
    
    _isEmptyViewShown = NO;
    // (lp) Hide or Show UI elements related to retry.
    // [_emptyDashboardView setEmptyDashboardViewDelegate:nil];
    [_emptyDashboardView setHidden:isHide];
    
}

- (void) showEmptyOrdersView {
    
    [self showEmptyDashBoardView];
}

- (void)trackOmniPage:(NSString *)page
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:[AppManager getOmniDictionary]];
    
    [OmnitureManager trackPage:page withContextInfo:params];
}


- (NSString *)getPageNameForCurrentGroup
{
    NSString *pageName;
    int index = (int)[self.faultSegmentedControl selectedSegmentIndex];
    if(self.cugs.count == 1)
    {
        if(index ==0)
        {
            pageName = OMNIPAGE_FAULT_OPEN;
        }
        else
        {
            pageName = OMNIPAGE_FAULT_CLOSED;
            
        }
    }
    else
    {
        if(index == 0)
        {
            pageName = OMNIPAGE_FAULT_OPEN_CUG;
            
        }
        else
        {
            pageName = OMNIPAGE_FAULT_CLOSED_CUG;
        }
    }
    
    return  pageName;
    
}

- (void)trackOmniClick:(NSString *)clickEvent forPage:(NSString *)page withFaultRef:(NSString *)faultRef
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:[AppManager getOmniDictionary]];
    NSString *pageName = page;
    NSString *linkTitle = clickEvent;
    
    if(faultRef && faultRef.length>0)
    {
        NSString *ref = [NSString stringWithFormat:@"%@%@",kOmniFaultRefFormat,faultRef];
        [params setValue:ref forKey:kOmniFaultRef];
    }
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:params];
}


#pragma mark - Segmented control methods

- (void)manageAPIRequestBasedOnSegmentSelection
{
    if(!_apiExecutedForOpen || !_apiExecutedForRecentlyClosed)
    {
        if (_isRetryShown) {
            
            if (![AppManager isInternetConnectionAvailable]) {
                [_retryView updateRetryViewWithInternetStrip:YES];
                _isRetryShown = NO;
                [self showRetryViewWithInternetStrip:YES];
                [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
                return;
            }
            
            self.faultDashboardTableView.tableFooterView.hidden = NO;
            [self hideRetryItems:YES];
        }
        if (_isEmptyViewShown)
        {
            self.faultDashboardTableView.tableFooterView.hidden = NO;
            [self hideEmmptyDashBoardItems:YES];
            
            if (![AppManager isInternetConnectionAvailable])
            {
                [self showRetryViewWithInternetStrip:YES];
                [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
                return;
            }
            
        }
    }
    if (self.faultSegmentedControl.selectedSegmentIndex == 0) {
        self.trackFaultType = TrackFaultDashBoardTypeOpenOrder;
        if (self.openFaultData.count == 0) {
            self.networkRequestInProgress = true;
            self.faultDashboardTableView.tableFooterView.hidden = YES;
            self.openFaultViewModel.pageIndex = 1;
            [self fetchFaultDashBoardAPIWithPageIndex:self.openFaultViewModel.pageIndex];
        }
        else
        {
            [self fetchOpenFaultsAutomatically];
        }
    } else if (self.faultSegmentedControl.selectedSegmentIndex == 1) {
        self.trackFaultType = TrackFaultDashBoardTypeCompletedOrder;
        if (self.completedFaultData.count == 0) {
            
            self.networkRequestInProgress = true;
            self.faultDashboardTableView.tableFooterView.hidden = YES;
            self.recentlyUsedFaultViewModel.pageIndex = 1;
            [self fetchFaultDashBoardAPIWithPageIndex:self.recentlyUsedFaultViewModel.pageIndex];
        }
        else
        {
            [self fetchCompletedFaultsAutomatically];
        }
    }
}

#pragma mark - Api call methods

- (void)fetchFaultDashBoardAPIWithPageIndex:(int)pageIndex {
    
    if (self.trackFaultType == TrackFaultDashBoardTypeOpenOrder) {
        self.openFaultViewModel.pageIndex = pageIndex;
        [self.openFaultViewModel fetchFaultDashBoardDetailsForLoggedInUserWithPageSize:3 pageIndex:pageIndex tabID:self.trackFaultType];
    } else if (self.trackFaultType == TrackFaultDashBoardTypeCompletedOrder) {
        self.recentlyUsedFaultViewModel.pageIndex = pageIndex;
        [self.recentlyUsedFaultViewModel fetchFaultDashBoardDetailsForLoggedInUserWithPageSize:3 pageIndex:pageIndex tabID:self.trackFaultType];
    }
    
}

- (void)fetchOpenFaultsAutomatically {
    int pageIndex = _openFaultViewModel.pageIndex;
    int totalSize = _openFaultViewModel.totalSize;
    [faultDashboardFooterView hideRetryView];
    _isRetryViewForOpenFault = NO;
    if ((self.openFaultData.count < 9) && (((pageIndex - 1) * 3) < totalSize) ) {
        if ([AppManager isInternetConnectionAvailable]) {
        
            _isLazyLoadingInOpenFault = YES;
            [self hideShowFooterIndicatorView:NO];
            [faultDashboardFooterView showLoaderContainerView];
            [self resizeTableViewFooterToFit];
            [self fetchFaultDashBoardAPIWithPageIndex:pageIndex];
        }
        else {
            [faultDashboardFooterView showLoaderContainerView];
            [faultDashboardFooterView showRetryView];
            [self resizeTableViewFooterToFit];
            [faultDashboardFooterView showRetryViewWithErrorMessage:kNoConnectionMessage];
            [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
        }
    }
}

- (void)fetchCompletedFaultsAutomatically {
    int pageIndex = _recentlyUsedFaultViewModel.pageIndex;
    int totalSize = _recentlyUsedFaultViewModel.totalSize;
    [faultDashboardFooterView hideRetryView];
    _isRetryViewShownForClosedFault = NO;
    if ((self.completedFaultData.count < 9) && (((pageIndex - 1) * 3) < totalSize)) {
        if ([AppManager isInternetConnectionAvailable]) {
            
            _isLazyLoadingInCompleted = YES;
            [self hideShowFooterIndicatorView:NO];
            [faultDashboardFooterView showLoaderContainerView];
            [self resizeTableViewFooterToFit];
            [self fetchFaultDashBoardAPIWithPageIndex:pageIndex];
        } else {
            [faultDashboardFooterView showLoaderContainerView];
            [faultDashboardFooterView showRetryView];
            [self resizeTableViewFooterToFit];
            [faultDashboardFooterView showRetryViewWithErrorMessage:kNoConnectionMessage];
            [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
        }
    }
}


#pragma mark - Cancelling API Methods

- (void) cancelOpenFaultAPI {
    if (_isLazyLoadingInOpenFault) {
        _islazyLoadingOpenFaultClosed = YES;
        _isLazyLoadingInOpenFault = NO;
    }
    
    if (_isFetchingWhileScrolling)
        _isFetchingWhileScrolling = NO;
    
    [self hideShowFooterIndicatorView:YES];
    [faultDashboardFooterView hideLoaderContainerView];
    [self resizeTableViewFooterToFit];
    
    [self.openFaultViewModel cancelFaultDashBoardAPIRequest];
}

- (void)cancelCompletedFaultAPI {
    if (_isLazyLoadingInCompleted) {
        _isLazyLoadingCompletedClosed = YES;
        _isLazyLoadingInCompleted = NO;
    }
    
    if (_isFetchingWhileScrolling)
        _isFetchingWhileScrolling = NO;
    
    [self hideShowFooterIndicatorView:YES];
    [faultDashboardFooterView hideLoaderContainerView];
    [self resizeTableViewFooterToFit];
    
    [self.recentlyUsedFaultViewModel cancelFaultDashBoardAPIRequest];
}


#pragma mark - Button Action Methods

- (IBAction)searchBarButtonAction:(id)sender {
    
    if (_faultSegmentedControl.selectedSegmentIndex == 0)
    {
        [self cancelOpenFaultAPI];
    }
    else
    {
        [self cancelCompletedFaultAPI];
    }
    
    BTFaultTrackerSearchViewController *faultSearchViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"trackFaultSearchScene"];
    [self.navigationController pushViewController:faultSearchViewController animated:YES];
}

- (IBAction)groupBarButtonAction:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Select Group" preferredStyle:UIAlertControllerStyleActionSheet];
    
    DDLogVerbose(@"Home: Total cugs belongs to user %lu.",(unsigned long)self.cugs.count);
    
    // (LP) Add group names in actionsheet
    if (self.cugs != nil && self.cugs.count > 0) {
        
        // (LP) Add action for each cug selection.
        int index = 0;
        for (BTCug *groupData in self.cugs) {
            
            __weak typeof(self) weakSelf = self;
            UIAlertAction *action = [UIAlertAction actionWithTitle:groupData.cugName
                                                             style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                 [weakSelf checkForSuperUserAndUpdateUIWithIndex:groupData.indexInAPIResponse];
                                                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                                             }];
            [alert addAction:action];
            
            index++;
        }
    }
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    [alert addAction:cancel];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = accountDropdownView.frame;
    alert.popoverPresentationController.canOverlapSourceViewRect = NO;
    
    [self presentViewController:alert animated:YES completion:^{
        
    }];
    
    
}

- (IBAction)faultDashBoardSegmentAction:(id)sender {
    
   
    [self trackOmniPage:[self getPageNameForCurrentGroup]];
    
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    int index = (int)[segmentedControl selectedSegmentIndex];
    
    if(!index)
        self.trackFaultType = TrackFaultDashBoardTypeOpenOrder;
    else
        self.trackFaultType = TrackFaultDashBoardTypeCompletedOrder;

    
    if(!_apiExecutedForOpen && self.trackFaultType == TrackFaultDashBoardTypeOpenOrder)
    {
       //If API is not executed control comes here
        if (_isRetryShown) {
            
                self.faultDashboardTableView.tableFooterView.hidden = NO;
                [self hideRetryItems:YES];

        }
        
        if (_isEmptyViewShown) {
           
                self.faultDashboardTableView.tableFooterView.hidden = NO;
                [self hideEmmptyDashBoardItems:YES];
        }
        
    }
    else
    {
       if(self.trackFaultType == TrackFaultDashBoardTypeOpenOrder)
       {
           
           if([self.openFaultData count] == 0)
           {
               [self showEmptyOrdersView];
           }
           else
           {
               self.faultDashboardTableView.tableFooterView.hidden = NO;
               [self hideShowFooterIndicatorView:YES];
               [self hideEmmptyDashBoardItems:YES];
               [self hideRetryItems:YES];
               if(!_isRetryViewForOpenFault)
               {
                   [faultDashboardFooterView hideLoaderContainerView];
                   [faultDashboardFooterView hideRetryView];
                   [self resizeTableViewFooterToFit];
               }
               else
               {
                   [faultDashboardFooterView showRetryView];
                   [faultDashboardFooterView showLoaderContainerView];
                   [self resizeTableViewFooterToFit];

               }
               
           }
       }

    }
    
    
     if(!_apiExecutedForRecentlyClosed  && self.trackFaultType == TrackFaultDashBoardTypeCompletedOrder)
     {
         //If API is not executed control comes here
         if (_isRetryShown) {
             
             self.faultDashboardTableView.tableFooterView.hidden = NO;
             [self hideRetryItems:YES];
          
         }
         
         if (_isEmptyViewShown) {
          
              self.faultDashboardTableView.tableFooterView.hidden = NO;
             [self hideEmmptyDashBoardItems:YES];
         }

     }
    else
    {
       if(self.trackFaultType == TrackFaultDashBoardTypeCompletedOrder)
       {
           if([self.completedFaultData count] == 0)
           {
               [self showEmptyOrdersView];
           }
           else
           {
               self.faultDashboardTableView.tableFooterView.hidden = NO;
               [self hideShowFooterIndicatorView:YES];
               [self hideEmmptyDashBoardItems:YES];
               [self hideRetryItems:YES];
               if(!_isRetryViewShownForClosedFault)
               {
                   [faultDashboardFooterView hideRetryView];
                   [faultDashboardFooterView hideLoaderContainerView];
                   [self resizeTableViewFooterToFit];
               }
               else
               {
                   [faultDashboardFooterView showRetryView];
                   [faultDashboardFooterView showLoaderContainerView];
                   [self resizeTableViewFooterToFit];
         
               }

           }
       }
    }
    
    [self.faultDashboardTableView reloadData];
   
    long selectedSegment = (long)[segmentedControl selectedSegmentIndex];
    switch (selectedSegment) {
        case 0:// for the OpenOrders
            _isTabSwitch = YES;
            self.trackFaultType = TrackFaultDashBoardTypeOpenOrder;
            [self cancelCompletedFaultAPI];
            if (self.openFaultData.count == 0) {
                 if(!_apiExecutedForOpen)
                    {
                        if([AppManager isInternetConnectionAvailable])
                        {
                            self.networkRequestInProgress = true;
                            self.faultDashboardTableView.tableFooterView.hidden = YES;
                            self.openFaultViewModel.pageIndex = 1;
                            [self fetchFaultDashBoardAPIWithPageIndex:self.openFaultViewModel.pageIndex];
                        }
                        else
                        {
                            [self showRetryViewWithInternetStrip:YES];
                        }
                    }
                
            } else {
               [self fetchOpenFaultsAutomatically];
            }
            break;
        case 1:// for the CompletedOrders
            _isTabSwitch = YES;
            self.trackFaultType = TrackFaultDashBoardTypeCompletedOrder;
            [self cancelOpenFaultAPI];
            if (self.completedFaultData.count == 0) {
                if(!_apiExecutedForRecentlyClosed)
                    {
                        if([AppManager isInternetConnectionAvailable])
                        {
                            self.networkRequestInProgress = true;
                            self.faultDashboardTableView.tableFooterView.hidden = YES;
                            self.recentlyUsedFaultViewModel.pageIndex = 1;
                            [self fetchFaultDashBoardAPIWithPageIndex:self.recentlyUsedFaultViewModel.pageIndex];
                        }
                        else
                        {
                            [self showRetryViewWithInternetStrip:YES];
                        }
                    }
                
            } else {
              [self fetchCompletedFaultsAutomatically];
            }
            break;
    }
}

- (IBAction)homeButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }]];
    
    [self presentViewController:alertController animated:YES completion:^{
        
    }];
}

#pragma mark - BTFaultDashBoardFooterDelegate Action Methods

- (void) faultDashBoardTableFooterView:(BTFaultDashBoardTableFooterView *)faultDashBoardFooterView retryBUttonActionMethod:(id)sender {
    //[self hideShowFooterRetryView:YES];
    
    if(![AppManager isInternetConnectionAvailable])
    {
        [faultDashboardFooterView showRetryViewWithErrorMessage:kNoConnectionMessage];
        [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
        return;
    }
    
    if (self.faultSegmentedControl.selectedSegmentIndex == 0) {
        
        int pageIndex = _openFaultViewModel.pageIndex;

        if (!_isLazyLoadingInOpenFault)
        {
            _isFetchingWhileScrolling = YES;
        }
        
        [self hideShowFooterIndicatorView:NO];
        [faultDashboardFooterView showLoaderContainerView];
        [self resizeTableViewFooterToFit];
        [self fetchFaultDashBoardAPIWithPageIndex:pageIndex];
        
    } else if (self.faultSegmentedControl.selectedSegmentIndex == 1) {
        int pageIndex = _recentlyUsedFaultViewModel.pageIndex;

        if(!_isLazyLoadingInCompleted)
        {
            _isFetchingWhileScrolling = YES;
        }
        
        [self hideShowFooterIndicatorView:NO];
        [faultDashboardFooterView showLoaderContainerView];
        [self resizeTableViewFooterToFit];
        [self fetchFaultDashBoardAPIWithPageIndex:pageIndex];
        
    }
}

- (void) faultDashBoardTableFooterView:(BTFaultDashBoardTableFooterView *)faultDashBoardFooterView reportNewFaultActionMethod:(id)sender
{
    if ([AppManager isInternetConnectionAvailable])
    {
        _networkRequestInProgress = YES;
        [self hideLoadingItems:NO];
        [_loadingView startAnimatingLoadingIndicatorView];
        
        if (self.faultSegmentedControl.selectedSegmentIndex == 0)
        {
            [self.openFaultViewModel checkForLiveChatAvailibilityForFault];
        }
        else if (self.faultSegmentedControl.selectedSegmentIndex == 1)
        {
            [self.recentlyUsedFaultViewModel checkForLiveChatAvailibilityForFault];
        }
    }
    else
    {
        [self showAlertWithTitle:kNoConnectionTitle andMessage:kNoConnectionMessage];
    }
}

#pragma mark - table view datasource methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 105.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.faultSegmentedControl.selectedSegmentIndex == 0) {
        
        return [self.openFaultData count];
    }
    return [self.completedFaultData count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTFaultTrackerDashboardTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BTFaultTrackerDashboardTableViewCell" forIndexPath:indexPath];
    if (self.faultSegmentedControl.selectedSegmentIndex == 0) {
        
        [cell updateCellWithFaultData:[self.openFaultData objectAtIndex:indexPath.row] orderType:TrackFaultDashBoardTypeOpenOrder];
    } else {
        [cell updateCellWithFaultData:[self.completedFaultData objectAtIndex:indexPath.row] orderType:TrackFaultDashBoardTypeCompletedOrder];
        
    }
    
    return cell;
    
}

#pragma mark - table view delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTFault *fault = nil;
    if (self.faultSegmentedControl.selectedSegmentIndex == 0)
    {
        fault = [self.openFaultData objectAtIndex:indexPath.row];
        [self cancelOpenFaultAPI];
    }
    else
    {
        fault = [self.completedFaultData objectAtIndex:indexPath.row];
        [self cancelCompletedFaultAPI];
    }
    
    NSString *linkName = [NSString stringWithFormat:@"%@ %@",fault.status,OMNICLICK_FAULT_DETAILS];
    
    [self trackOmniClick:linkName forPage:[self getPageNameForCurrentGroup] withFaultRef:fault.faultReference];
    
    
    if ([self isFaultId:fault.faultReference])
    {
        BTFaultDetailsMilestoneViewController *faultDetailsMilestoneViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"faultDetailsMileStoneScene"];
        
        faultDetailsMilestoneViewController.faultReference = fault.faultReference;
        faultDetailsMilestoneViewController.redirectedFromDashboard = YES;
        
        [self.navigationController pushViewController:faultDetailsMilestoneViewController animated:YES];
    }
    else
    {
        BTFaultTrackerSummaryViewController *faultSummaryViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FaultSummaryScene"];
        faultSummaryViewController.faultReference = fault.faultReference;
        faultSummaryViewController.isServiceId = YES;
        
        [self.navigationController pushViewController:faultSummaryViewController animated:YES];
    }
}



#pragma mark - Scroll View Methods

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    if (self.faultSegmentedControl.selectedSegmentIndex == 0) {
        
        int pageIndex = _openFaultViewModel.pageIndex;
        //int totalSize = _openFaultViewModel.totalSize;
        int numberOfPages = [_openFaultViewModel getTotalNumberOfPagesToFetch];
        
        if (pageIndex <= numberOfPages && self.openFaultData.count >= 9)
        {
            if (!_isFetchingWhileScrolling)
            {
                if ([AppManager isInternetConnectionAvailable])
                {
                    _isFetchingWhileScrolling = YES;
                    [self hideShowFooterIndicatorView:NO];
                    [faultDashboardFooterView showLoaderContainerView];
                    [self resizeTableViewFooterToFit];
                    [self fetchFaultDashBoardAPIWithPageIndex:pageIndex];
                }
                else
                {
                    [self hideShowFooterIndicatorView:YES];
                    [faultDashboardFooterView showRetryViewWithErrorMessage:kNoConnectionMessage];
                    [faultDashboardFooterView showLoaderContainerView];
                    [self resizeTableViewFooterToFit];
                    _isRetryViewForOpenFault = YES;
                }
            }
        }
    } else if (self.faultSegmentedControl.selectedSegmentIndex == 1) {
        int pageIndex = _recentlyUsedFaultViewModel.pageIndex;
        //int totalSize = _recentlyUsedFaultViewModel.totalSize;
        int numberOfPages = [_recentlyUsedFaultViewModel getTotalNumberOfPagesToFetch];
        
        if (pageIndex <= numberOfPages && self.completedFaultData.count >= 9)
        {
            if (!_isFetchingWhileScrolling)
            {
                if ([AppManager isInternetConnectionAvailable])
                {
                    _isFetchingWhileScrolling = YES;
                    [self hideShowFooterIndicatorView:NO];
                    [faultDashboardFooterView showLoaderContainerView];
                    [self resizeTableViewFooterToFit];
                    [self fetchFaultDashBoardAPIWithPageIndex:pageIndex];
                }
                else
                {
                    [self hideShowFooterIndicatorView:YES];
                    [faultDashboardFooterView showRetryViewWithErrorMessage:kNoConnectionMessage];
                    [faultDashboardFooterView showLoaderContainerView];
                    [self resizeTableViewFooterToFit];
                    _isRetryViewForOpenFault = YES;
                }
            }
        }
    }
}



#pragma mark - BTRetryView delegate methods
- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView {
    
    DDLogInfo(@"Retry to fecth order Dashboard details ");
    if([AppManager isInternetConnectionAvailable])
    {
        [self hideRetryItems:YES];
        [self hideEmmptyDashBoardItems:YES];
        self.faultDashboardTableView.tableFooterView.hidden = YES;
        self.networkRequestInProgress = YES;
        [self fetchFaultDashBoardAPIWithPageIndex:1];
    }
    else
    {
        [_retryView updateRetryViewWithInternetStrip:YES];
        _isRetryShown = NO;
        [self showRetryViewWithInternetStrip:YES];
    }
}

#pragma mark - BTEmptyDashboardView delegate methods

- (void)userPressedSearchByFaultReferenceButtonOfEmptyDashboardView:(BTFaultEmptyDashboardView *)retryView
{
    BTFaultTrackerSearchViewController *faultSearchViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"trackFaultSearchScene"];
    [self.navigationController pushViewController:faultSearchViewController animated:YES];
}


- (void)userPressedCreateNewFaultButtonOfEmptyDashboardView:(BTFaultEmptyDashboardView *)retryView
{
    if ([AppManager isInternetConnectionAvailable])
    {
        _networkRequestInProgress = YES;
        [self hideLoadingItems:NO];
        [self.view bringSubviewToFront:_loadingView];
        [_loadingView startAnimatingLoadingIndicatorView];
        
        if (self.faultSegmentedControl.selectedSegmentIndex == 0)
        {
            [self.openFaultViewModel checkForLiveChatAvailibilityForFault];
        }
        else if (self.faultSegmentedControl.selectedSegmentIndex == 1)
        {
            [self.recentlyUsedFaultViewModel checkForLiveChatAvailibilityForFault];
        }
    }
    else
    {
        [self showAlertWithTitle:kNoConnectionTitle andMessage:kNoConnectionMessage];
    }
}


- (void)loadNextOpenFaults{
    
    if (!_isFooterViewCreated) {
        
        [self createFooterView];
    }
    self.faultDashboardTableView.tableFooterView.hidden = NO;
    int pageIndex = _openFaultViewModel.pageIndex;
    int totalSize = _openFaultViewModel.totalSize;
    if ((self.openFaultData.count < 9) && (((pageIndex - 1) * 3) < totalSize))
    {
        if ([AppManager isInternetConnectionAvailable])
        {
            _isLazyLoadingInOpenFault = YES;
            [self hideShowFooterIndicatorView:NO];
            [faultDashboardFooterView showLoaderContainerView];
            [self resizeTableViewFooterToFit];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self fetchFaultDashBoardAPIWithPageIndex:pageIndex];
            });
        }
        else
        {
            [self hideShowFooterIndicatorView:YES];
            [faultDashboardFooterView showRetryViewWithErrorMessage:kNoConnectionMessage];
            [faultDashboardFooterView showLoaderContainerView];
            [self resizeTableViewFooterToFit];
            _isRetryViewForOpenFault = YES;
        }
    }
    
    if ((self.openFaultData.count == totalSize) || (self.openFaultData.count >= 9)) {
        
        _isLazyLoadingInOpenFault = NO;
        [self hideShowFooterIndicatorView:YES];
        [faultDashboardFooterView hideRetryView];
        _isRetryViewForOpenFault = NO;
        [faultDashboardFooterView hideLoaderContainerView];
        [self resizeTableViewFooterToFit];
    }

}

#pragma mark DLMFaultDashBoardScreenDelegate Methods

- (void)successfullyFetchedFaultDashboardDataOnFaultDashBoardScreen:(DLMFaultDashBoardScreen *)dashBoardScreen {
    
    [self setNetworkRequestInProgress:false];
    
    [self updateUserInterfaceWithFaults:dashBoardScreen.faults tabID:dashBoardScreen.tabID];
    
    if (self.faultSegmentedControl.selectedSegmentIndex == 0) {
        _apiExecutedForOpen = YES;
        if (self.openFaultData.count == 0) {
            
            [self trackOmniPage:OMNIPAGE_FAULT_NO_OPEN_FAULTS];
            
            //[self showEmptyDashBoardView];
            [self performSelector:@selector(showEmptyDashBoardView) withObject:nil afterDelay:0.01];
        } else {
            
            [self performSelector:@selector(loadNextOpenFaults) withObject:nil afterDelay:0.01];
        }
        
    } else if (self.faultSegmentedControl.selectedSegmentIndex == 1) {
        
        _apiExecutedForRecentlyClosed = YES;
        if (self.completedFaultData.count == 0) {
             [self trackOmniPage:OMNIPAGE_FAULT_NO_CLOSED_FAULTS];
            [self showEmptyDashBoardView];
        } else {
            
            if (_isFetchingWhileScrolling) {
                _isFetchingWhileScrolling = NO;
            }
            
            if (!_isFooterViewCreated) {
                
                [self createFooterView];
            }
            self.faultDashboardTableView.tableFooterView.hidden = NO;
            int pageIndex = _recentlyUsedFaultViewModel.pageIndex;
            int totalSize = _recentlyUsedFaultViewModel.totalSize;
            if ((self.completedFaultData.count < 9) && (((pageIndex - 1) * 3) < totalSize))
            {
                if ([AppManager isInternetConnectionAvailable])
                {
                    _isLazyLoadingInCompleted = YES;
                    [self hideShowFooterIndicatorView:NO];
                    [faultDashboardFooterView showLoaderContainerView];
                    [self resizeTableViewFooterToFit];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self fetchFaultDashBoardAPIWithPageIndex:pageIndex];
                    });
                }
                else
                {
                    [self hideShowFooterIndicatorView:YES];
                    [faultDashboardFooterView showRetryViewWithErrorMessage:kNoConnectionMessage];
                    [faultDashboardFooterView showLoaderContainerView];
                    [self resizeTableViewFooterToFit];
                    _isRetryViewForOpenFault = YES;
                }
            }
            
            if ((self.completedFaultData.count == totalSize) || (self.completedFaultData.count >= 9)) {
                
                _isLazyLoadingInCompleted = NO;
                [self hideShowFooterIndicatorView:YES];
                [faultDashboardFooterView hideRetryView];
                _isRetryViewShownForClosedFault = NO;
                [faultDashboardFooterView hideLoaderContainerView];
                [self resizeTableViewFooterToFit];
            }
        }
    }
}

- (void)faultDashBoardScreen:(DLMFaultDashBoardScreen *)dashBoardScreen failedToFetchFaultDashBoardDataWithWebServiceError:(NLWebServiceError *)error {
    
    DDLogError(@"Fault DashBoard: Fetch Fault DashBoard details failed");
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if(errorHandled)
        [AppManager trackGenericAPIErrorOnPage:[self getPageNameForCurrentGroup]];
    
    if ( [error.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {
        if (self.faultSegmentedControl.selectedSegmentIndex == 0) {
            
            if (_isTabSwitch && _isLazyLoadingCompletedClosed && (dashBoardScreen == _recentlyUsedFaultViewModel)) {
                _isLazyLoadingCompletedClosed = NO;
                _isTabSwitch = NO;
                return;
            }
            if (_isResetGroup && _islazyLoadingOpenFaultClosed && (dashBoardScreen == _openFaultViewModel)) {
                _islazyLoadingOpenFaultClosed = NO;
                _isResetGroup = NO;
                return;
            }
        } else if (self.faultSegmentedControl.selectedSegmentIndex == 1) {
            
            if (_isTabSwitch && _islazyLoadingOpenFaultClosed && (dashBoardScreen == _openFaultViewModel)) {
                _islazyLoadingOpenFaultClosed = NO;
                _isTabSwitch = NO;
                return;
            }
            if (_isResetGroup && _isLazyLoadingCompletedClosed && (dashBoardScreen == _recentlyUsedFaultViewModel)) {
                _isLazyLoadingCompletedClosed = NO;
                _isResetGroup = NO;
                return;
            }
        }
        [self setNetworkRequestInProgress:false];
        if (self.faultSegmentedControl.selectedSegmentIndex == 0) {
            
            if (self.openFaultData.count == 0) {
                
                if (error.error.code == BTNetworkErrorCodeAPINoDataFound)
                {
                    [self showEmptyDashBoardView];
                    [AppManager trackNoDataFoundErrorOnPage:[self getPageNameForCurrentGroup]];
                }
                else
                {
                    [self showRetryViewWithInternetStrip:NO];
                    [AppManager trackGenericAPIErrorOnPage:[self getPageNameForCurrentGroup]];
                }
            }
            if (_isLazyLoadingInOpenFault || _isFetchingWhileScrolling)
            {
                [self hideShowFooterIndicatorView:YES];
                [faultDashboardFooterView showRetryViewWithErrorMessage:kDefaultErrorMessageWithLazyLoading];
                [faultDashboardFooterView showLoaderContainerView];
                
                _isFetchingWhileScrolling = NO;
                
                [OmnitureManager trackError:OMNIERROR_FAULT_LAZY_LOADING_ERROR onPageWithName:[self getPageNameForCurrentGroup] contextInfo:[AppManager getOmniDictionary]];
                [AppManager trackGenericAPIErrorOnPage:[self getPageNameForCurrentGroup]];
                
                [self resizeTableViewFooterToFit];
            }
            
        } else if (self.faultSegmentedControl.selectedSegmentIndex == 1) {
            
            if (self.completedFaultData.count == 0) {
                
                if (error.error.code == BTNetworkErrorCodeAPINoDataFound)
                {
                    [self showEmptyDashBoardView];
                    [AppManager trackNoDataFoundErrorOnPage:[self getPageNameForCurrentGroup]];
                }
                else
                {
                    [self showRetryViewWithInternetStrip:NO];
                    [AppManager trackGenericAPIErrorOnPage:[self getPageNameForCurrentGroup]];
                }
            }
            if (_isLazyLoadingInCompleted || _isFetchingWhileScrolling)
            {
                [self hideShowFooterIndicatorView:YES];
                [faultDashboardFooterView showRetryViewWithErrorMessage:kDefaultErrorMessageWithLazyLoading];
                [faultDashboardFooterView showLoaderContainerView];
                
                _isFetchingWhileScrolling = NO;
                
                [OmnitureManager trackError:OMNIERROR_FAULT_LAZY_LOADING_ERROR onPageWithName:[self getPageNameForCurrentGroup] contextInfo:[AppManager getOmniDictionary]];
                [AppManager trackGenericAPIErrorOnPage:[self getPageNameForCurrentGroup]];
                
                [self resizeTableViewFooterToFit];
            }
        }
    }
    else
    {
        if (self.faultSegmentedControl.selectedSegmentIndex == 0) {
            
            if (_isTabSwitch && _isLazyLoadingCompletedClosed && (dashBoardScreen == _recentlyUsedFaultViewModel)) {
                _isLazyLoadingCompletedClosed = NO;
                _isTabSwitch = NO;
                return;
            }
            if (_isResetGroup && _islazyLoadingOpenFaultClosed && (dashBoardScreen == _openFaultViewModel)) {
                _islazyLoadingOpenFaultClosed = NO;
                _isResetGroup = NO;
                return;
            }
        } else if (self.faultSegmentedControl.selectedSegmentIndex == 1) {
            
            if (_isTabSwitch && _islazyLoadingOpenFaultClosed && (dashBoardScreen == _openFaultViewModel)) {
                _islazyLoadingOpenFaultClosed = NO;
                _isTabSwitch = NO;
                return;
            }
            if (_isResetGroup && _isLazyLoadingCompletedClosed && (dashBoardScreen == _recentlyUsedFaultViewModel)) {
                _isLazyLoadingCompletedClosed = NO;
                _isResetGroup = NO;
                return;
            }
        }
        [self setNetworkRequestInProgress:false];
        
        if (self.faultSegmentedControl.selectedSegmentIndex == 0) {
            if (self.openFaultData.count == 0) {
                
                if (error.error.code == BTNetworkErrorCodeAPINoDataFound)
                {
                    [self showEmptyDashBoardView];
                    [AppManager trackNoDataFoundErrorOnPage:[self getPageNameForCurrentGroup]];
                }
                else
                {
                    [self showRetryViewWithInternetStrip:NO];
                    [AppManager trackGenericAPIErrorOnPage:[self getPageNameForCurrentGroup]];
                }
            }
            if (_isLazyLoadingInOpenFault || _isFetchingWhileScrolling)
            {
                [self hideShowFooterIndicatorView:YES];
                [faultDashboardFooterView showRetryViewWithErrorMessage:kDefaultErrorMessageWithLazyLoading];
                [faultDashboardFooterView showLoaderContainerView];
                
                _isFetchingWhileScrolling = NO;
                
                [self resizeTableViewFooterToFit];
                _isRetryViewForOpenFault = YES;
                [AppManager trackGenericAPIErrorOnPage:[self getPageNameForCurrentGroup]];
            }
            
        } else if (self.faultSegmentedControl.selectedSegmentIndex == 1) {
            
            if (self.completedFaultData.count == 0) {
                
                if (error.error.code == BTNetworkErrorCodeAPINoDataFound)
                {
                    [self showEmptyDashBoardView];
                    [AppManager trackNoDataFoundErrorOnPage:[self getPageNameForCurrentGroup]];
                }
                else
                {
                    [self showRetryViewWithInternetStrip:NO];
                    [AppManager trackGenericAPIErrorOnPage:[self getPageNameForCurrentGroup]];
                }
            }
            if (_isLazyLoadingInCompleted || _isFetchingWhileScrolling)
            {
                [self hideShowFooterIndicatorView:YES];
                [faultDashboardFooterView showRetryViewWithErrorMessage:kDefaultErrorMessageWithLazyLoading];
                [faultDashboardFooterView showLoaderContainerView];
                
                _isFetchingWhileScrolling = NO;
                
                [self resizeTableViewFooterToFit];
                _isRetryViewShownForClosedFault = YES;
                [AppManager trackGenericAPIErrorOnPage:[self getPageNameForCurrentGroup]];
            }
        }
    }
}

- (void)liveChatAvailableForFaultWithScreen:(DLMFaultDashBoardScreen *)faultDashBoardScreen
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
    
    viewController.targetURLType = BTTargetURLTypeFaultsChatScreen;
    
    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:NO andIsNeedCloseButton:YES];
    
    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{
        
    }];
    
}

- (void)faultDashBoardScreen:(DLMFaultDashBoardScreen *)faultDashBoardScreen liveChatCurrentlyNotAvailableWithSpecificCategory:(NSArray *)specificCategory generalCategory:(NSArray *)generalCategory andCurrentTime:(NSString *)currentTime
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BTHelpAndSupportViewController *helpAndSupportController = [BTHelpAndSupportViewController getHelpAndSupportViewController];
    helpAndSupportController.isLaunchingFromHomeScreen = NO;
    helpAndSupportController.isDataAlreadyFetched = YES;
    helpAndSupportController.specificCategory = specificCategory;
    helpAndSupportController.generalCategory = generalCategory;
    helpAndSupportController.currentTime = currentTime;
    
    [self.navigationController pushViewController:helpAndSupportController animated:YES];
}

- (void)faultDashBoardScreen:(DLMFaultDashBoardScreen *)faultDashBoardScreen failedToCheckLiveChatAvailabilityWithWebServiceError:(NLWebServiceError *)error
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if(errorHandled == NO)
    {
        [self showAlertWithTitle:@"Message" andMessage:kDefaultErrorMessage];
        [AppManager trackGenericAPIErrorOnPage:[self getPageNameForCurrentGroup]];
    }

}

@end
