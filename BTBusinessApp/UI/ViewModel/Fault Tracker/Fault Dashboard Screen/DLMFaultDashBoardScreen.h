//
//  DLMFaultDashBoardScreen.h
//  BTBusinessApp
//
//  Created by Accolite on 14/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DLMObject.h"
@class DLMFaultDashBoardScreen;
@class DLMFaultSummaryScreen;
@class DLMFaultDetailsScreen;
@class BTCug;
@class NLWebServiceError;

@protocol DLMFaultDashBoardScreenDelegate <NSObject>

- (void)successfullyFetchedFaultDashboardDataOnFaultDashBoardScreen:(DLMFaultDashBoardScreen *)dashBoardScreen;

- (void)faultDashBoardScreen:(DLMFaultDashBoardScreen *)dashBoardScreen failedToFetchFaultDashBoardDataWithWebServiceError:(NLWebServiceError *)error;

- (void)liveChatAvailableForFaultWithScreen:(DLMFaultDashBoardScreen *)faultDashBoardScreen;
- (void)faultDashBoardScreen:(DLMFaultDashBoardScreen *)faultDashBoardScreen liveChatCurrentlyNotAvailableWithSpecificCategory:(NSArray *)specificCategory generalCategory:(NSArray *)generalCategory andCurrentTime:(NSString *)currentTime;
- (void)faultDashBoardScreen:(DLMFaultDashBoardScreen *)faultDashBoardScreen failedToCheckLiveChatAvailabilityWithWebServiceError:(NLWebServiceError *)error;

@end

@interface DLMFaultDashBoardScreen : NSObject

@property (nonatomic,assign)int pageSize;
@property (nonatomic,assign)int pageIndex;
@property (nonatomic,assign)int tabID;
@property (nonatomic,assign)int totalSize;
@property (nonatomic, weak) id<DLMFaultDashBoardScreenDelegate> faultDashBoardDelegate;
@property (nonatomic) NSArray *faults;
@property (nonatomic, strong) DLMFaultSummaryScreen *faultSummaryModel;
@property (nonatomic, strong) DLMFaultDetailsScreen *faultDetailModel;

- (id)initWithFirstPageOpenFaultDataDict:(NSDictionary *)firstPageOpenFaultDict;
- (void)fetchFaultDashBoardDetailsForLoggedInUserWithPageSize:(int)pageSize pageIndex:(int)pageIndex tabID:(int)tabID;
- (void) cancelFaultDashBoardAPIRequest;
- (void)cancelChatAvailabilityCheckerAPI;
- (void)changeSelectedCUGTo:(BTCug *)selectedCug;
- (int)getTotalNumberOfPagesToFetch;
- (void)checkForLiveChatAvailibilityForFault;

@end
