//
//  DLMFaultDashBoardScreen.m
//  BTBusinessApp
//
//  Created by Accolite on 14/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMFaultDashBoardScreen.h"
#import "NLFaultDashBoardWebService.h"
#import "AppDelegate.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppDelegateViewModel.h"
#import "AppConstants.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"
#import "BTCug.h"
#import "CDCug+CoreDataClass.h"
#import "BTUser.h"
#import "BTFault.h"
#import "CDRecentSearchedFault.h"
#import "NLWebServiceError.h"
#import "BTLiveChatAvailabilityChecker.h"

@interface DLMFaultDashBoardScreen () <NLFaultDashBoardWebServiceDelegate, BTLiveChatAvailabilityCheckerDelegate> {
    
    NSDictionary *_firstPageOpenFaultDict;
    BTLiveChatAvailabilityChecker *_liveChatAvailabityChecker;
}

@property (nonatomic) NLFaultDashBoardWebService *getFaultDashBoardWebService;

@end

@implementation DLMFaultDashBoardScreen

#pragma mark Public Methods

- (id)initWithFirstPageOpenFaultDataDict:(NSDictionary *)firstPageOpenFaultDict{
    
    self = [super init];
    
    if(self){
        
        _firstPageOpenFaultDict = firstPageOpenFaultDict;
    }
    
    return self;
    
}

#pragma mark fetchFaultDashBoardDetails

- (void)fetchFaultDashBoardDetailsForLoggedInUserWithPageSize:(int)pageSize pageIndex:(int)pageIndex tabID:(int)tabID
{
    self.tabID = tabID;
    
    //check for first page open fault
    if(_firstPageOpenFaultDict && self.pageIndex == 1 && self.tabID == 1){
        
        [self checkForAlreadyFetchedOpenFaults];
    } else {
        [self setupToFetchFaultDashboardDetailsWithPageSize:pageSize withPageIndex:pageIndex withTabID:tabID];
        [self fetchFaultDashboardDetails];
    }
}

- (void) checkForAlreadyFetchedOpenFaults {
    
    NSArray *faults = [_firstPageOpenFaultDict valueForKey:@"faults"];
    int totalSize = [[_firstPageOpenFaultDict valueForKey:@"totalSize"] intValue];
    int tabID = [[_firstPageOpenFaultDict valueForKey:@"tabID"] intValue];
    int pageIndex = [[_firstPageOpenFaultDict valueForKey:@"pageIndex"] intValue];
    
    [self setFaults:faults];
    [self setTotalSize:totalSize];
    if (tabID == self.tabID) {
        if (pageIndex == self.pageIndex) {
            self.pageIndex ++;
        }
    }
    [self.faultDashBoardDelegate successfullyFetchedFaultDashboardDataOnFaultDashBoardScreen:self];
}

- (void) setupToFetchFaultDashboardDetailsWithPageSize:(int)pageSize withPageIndex:(int)pageIndex withTabID:(int)tabID {
    
    self.getFaultDashBoardWebService = [[NLFaultDashBoardWebService alloc] initWithPageSize:pageSize pageIndex:pageIndex andTabID:tabID];
    self.getFaultDashBoardWebService.getFaultDashBoardWebServiceDelegate = self;
}

- (void) fetchFaultDashboardDetails {
    [self.getFaultDashBoardWebService resume];
}

#pragma mark -

- (int)getTotalNumberOfPagesToFetch
{
    int numberOfPages = self.totalSize / 3;
    int remaining = self.totalSize % 3;
    
    if (remaining > 0)
    {
        numberOfPages++;
    }
    
    return numberOfPages;
}

- (void)checkForLiveChatAvailibilityForFault
{
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = [[BTLiveChatAvailabilityChecker alloc] init];
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = self;
    [self getLiveChatAvailableSlotsForFaults];
}

- (void) getLiveChatAvailableSlotsForFaults {
    [_liveChatAvailabityChecker getLiveChatAvailableSlots];
}

#pragma mark PersistanceMethods

- (void)changeSelectedCUGTo:(BTCug *)selectedCug
{
    DDLogInfo(@"Saving %@ as selected cug into persistence.", selectedCug);
    
    NSManagedObjectContext *context = [AppDelegate sharedInstance].managedObjectContext;
    CDCug *newPersistenceCug = [CDCug newCugInManagedObjectContext:context];
    newPersistenceCug.groupKey = selectedCug.groupKey;
    newPersistenceCug.refKey = selectedCug.refKey;
    newPersistenceCug.contactId = selectedCug.contactId;
    newPersistenceCug.cugName = selectedCug.cugName;
    newPersistenceCug.cugId = selectedCug.cugID;
    newPersistenceCug.cugRole = @(selectedCug.cugRole);
    newPersistenceCug.indexInAPIResponse = @(selectedCug.indexInAPIResponse);
    
    // Get the currently logged in User
    CDUser *loggedInUser = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    CDCug *previouslySelectedCug = loggedInUser.currentSelectedCug;
    loggedInUser.currentSelectedCug = newPersistenceCug;
    
    if(previouslySelectedCug)
    {
        [context deleteObject:previouslySelectedCug];
    }
    
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
    
    _firstPageOpenFaultDict = nil;
}


#pragma mark CancellingDashBoard request methods

- (void) cancelFaultDashBoardAPIRequest {
    self.getFaultDashBoardWebService.getFaultDashBoardWebServiceDelegate = nil;
    [self.getFaultDashBoardWebService cancel];
    self.getFaultDashBoardWebService = nil;
}

- (void)cancelChatAvailabilityCheckerAPI
{
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    [_liveChatAvailabityChecker cancel];
    _liveChatAvailabityChecker = nil;
}

#pragma mark - NLFaultDashBoardWebServiceDelegate Methods
- (void)getFaultDashBoardWebService:(NLFaultDashBoardWebService *)webService successfullyFetchedFaultDashBoardData:(NSArray *)faults pageIndex:(int)pageIndex totalSize:(int)totalSize andTabID:(int)tabID {
    if (webService == self.getFaultDashBoardWebService) {
        
        [self setFaults:faults];
        [self setTotalSize:totalSize];
        if (tabID == self.tabID) {
            if (pageIndex == self.pageIndex) {
                self.pageIndex ++;
            }
        }
        
        self.getFaultDashBoardWebService.getFaultDashBoardWebServiceDelegate = nil;
        self.getFaultDashBoardWebService = nil;
        
        [self.faultDashBoardDelegate successfullyFetchedFaultDashboardDataOnFaultDashBoardScreen:self];
        
    } else {
        DDLogError(@"This delegate method gets called because of success of an object of NLFaultDashBoardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLFaultDashBoardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)getFaultDashBoardWebService:(NLFaultDashBoardWebService *)webService failedToFetchFaultDashBoardDataWithWebServiceError:(NLWebServiceError *)error {
    if (webService == self.getFaultDashBoardWebService) {
        
        self.getFaultDashBoardWebService.getFaultDashBoardWebServiceDelegate = nil;
        self.getFaultDashBoardWebService = nil;
        
        [self.faultDashBoardDelegate  faultDashBoardScreen:self failedToFetchFaultDashBoardDataWithWebServiceError:error];
        
    } else {
        DDLogError(@"This delegate method gets called because of failure of an object of NLFaultDashBoardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLFaultDashBoardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

#pragma mark - BTLiveChatAvailabilityCheckerDelegate Methods
- (void)liveChatAvailabilityChecker:(BTLiveChatAvailabilityChecker *)liveChatAvailabilityChecker successfullyFetchedSlotsWithGeneralCategory:(NSArray *)generalCategoryArray specificeCategory:(NSArray *)specificCategory andCurrentTime:(NSString *)currentTime
{
    BOOL chatAvailable = [BTLiveChatAvailabilityChecker checkAvailabilityWithSpecificCategory:specificCategory currentTime:currentTime andChatType:BTChatTypeFault];
    
    if (chatAvailable)
    {
        [self.faultDashBoardDelegate liveChatAvailableForFaultWithScreen:self];
    }
    else
    {
        [self.faultDashBoardDelegate faultDashBoardScreen:self liveChatCurrentlyNotAvailableWithSpecificCategory:specificCategory generalCategory:generalCategoryArray andCurrentTime:currentTime];
    }
    
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = nil;
}

- (void)liveChatAvailabilityChecker:(BTLiveChatAvailabilityChecker *)liveChatAvailabilityChecker failedToFetchSlotsWithWebServiceError:(NLWebServiceError *)webServiceError
{
    [self.faultDashBoardDelegate faultDashBoardScreen:self failedToCheckLiveChatAvailabilityWithWebServiceError:webServiceError];
    
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = nil;
}


@end
