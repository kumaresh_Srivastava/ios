//
//  BTMessageFromAgentViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 25/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTMessageFromAgentViewController.h"
#import "AppManager.h"
#import "OmnitureManager.h"

@interface BTMessageFromAgentViewController ()

@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end

@implementation BTMessageFromAgentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Message from BT Agent";
    
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonPressed:)];
    self.navigationItem.rightBarButtonItem = rightButtonItem;
    
    self.topLabel.text = [NSString stringWithFormat:@"Sent on %@ at %@",[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:_messageDate], [AppManager getTimeFromDate:_messageDate]];
    self.descriptionLabel.text = _message;
    
    [self trackPage];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action Methods

- (void)closeButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Track Page
- (void)trackPage
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = OMNIPAGE_FAULT_AGENT_COMMENT;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
   
    [OmnitureManager trackPage:pageName withContextInfo:data];
}


@end
