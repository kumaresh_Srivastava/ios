//
//  BTMessageFromAgentViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 25/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTMessageFromAgentViewController : UIViewController

@property (nonatomic, strong) NSDate *messageDate;
@property (nonatomic, strong) NSString *message;

@end
