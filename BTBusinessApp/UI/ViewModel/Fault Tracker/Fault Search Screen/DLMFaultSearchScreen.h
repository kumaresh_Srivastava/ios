//
//  DLMFaultSearchScreen.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 19/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
@class DLMFaultSearchScreen;
@class DLMFaultSummaryScreen;
@class DLMFaultDetailsScreen;
@class BTFault;
@class NLWebServiceError;

@protocol DLMFaultSearchScreenDelegate <NSObject>

- (void)successfullyFetchedRecentSearchedFaultDataOnFaultSearchScreen:(DLMFaultSearchScreen *)faultSearchScreen;
@end

@interface DLMFaultSearchScreen : DLMObject

@property (nonatomic, copy) NSArray *arrayOfRecentSearchedFaults;

@property (nonatomic, weak) id <DLMFaultSearchScreenDelegate> faultSearchScreenDelegate;

@property (nonatomic, strong) DLMFaultSummaryScreen *faultSummaryModel;
@property (nonatomic, strong) DLMFaultDetailsScreen *faultDetailModel;

- (void)fetchRecentSearchedFaults;
@end
