//
//  BTFaultTrackerSearchViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 27/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

// Code Review Process:
// 09-09-2016: Reviewed By Harman

#import "BTFaultTrackerSearchViewController.h"
#import "BTFaultTrackerSummaryViewController.h"
#import "BTFaultTrackerRecentlySearchedTableViewCell.h"
#import "BTFaultRestrictedDetailsViewController.h"
#import "BTFaultDetailsMilestoneViewController.h"
#import "DLMFaultSearchScreen.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "BTRecentSearchedFault.h"
#import "BTFaultSearchHelpViewController.h"
#import "UIButton+BTButtonProperties.h"
#import "CustomSpinnerView.h"
#import "AppConstants.h"
#import "BTFault.h"
#import "DLMFaultSummaryScreen.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "NLWebServiceError.h"
#import "UITextField+BTTextFieldProperties.h"

#define kRecentlyViewedText @"Recently viewed"
#define kNoRecentlyViewedText @"No recent searched item found"

#define kTallDeviceHeight 812.0f
#define kHelpCardMovement 400.0f
#define kSpaceFromBottom 110.0f
#define kTallDeviceAdditionalSpacing 60.0f

@interface BTFaultTrackerSearchViewController ()<UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, DLMFaultSearchScreenDelegate> {
    CustomSpinnerView *_loadingView;
}

@property (nonatomic, readwrite) DLMFaultSearchScreen *viewModel;

@property (weak, nonatomic) IBOutlet UIView *trackFaultInputContainerView;
@property (weak, nonatomic) IBOutlet UITextField *enterFaultReferenceTextField;
@property (weak, nonatomic) IBOutlet UITableView *recentlySearchedFaultIdTableView;
@property (weak, nonatomic) IBOutlet UIButton *trackFaultButton;
@property (weak, nonatomic) IBOutlet UILabel *recentlySearchLabel;
@property (weak, nonatomic) UIView *helpCard;

@property BOOL helpCardVisible;

@end


@implementation BTFaultTrackerSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // (lp) setup viewModel for fault search screen
    self.viewModel = [[DLMFaultSearchScreen alloc] init];
    self.viewModel.faultSearchScreenDelegate = self;

    // (LP) Navigation bar appearence
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.title = @"Search faults";
   self.enterFaultReferenceTextField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    self.enterFaultReferenceTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    // (LP) Recently searched faults
    self.recentlySearchedFaultIdTableView.delegate = self;
    self.recentlySearchedFaultIdTableView.dataSource = self;
    self.recentlySearchedFaultIdTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    self.recentlySearchedFaultIdTableView.backgroundColor = [UIColor clearColor];
    self.recentlySearchedFaultIdTableView.estimatedRowHeight = 50;
    self.recentlySearchedFaultIdTableView.rowHeight = UITableViewAutomaticDimension;

    self.trackFaultInputContainerView.layer.cornerRadius = 5.0f;
    self.trackFaultInputContainerView.layer.borderWidth = 1.0f;
    self.trackFaultInputContainerView.layer.borderWidth = 1.0f;
    self.trackFaultInputContainerView.layer.borderColor = [UIColor colorForHexString:@"666666"].CGColor;
    self.trackFaultInputContainerView.layer.cornerRadius = 5.0f;
    
    UINib *recentlySearchedDataCell = [UINib nibWithNibName:@"BTFaultTrackerRecentlySearchedTableViewCell" bundle:nil];
    [self.recentlySearchedFaultIdTableView registerNib:recentlySearchedDataCell forCellReuseIdentifier:@"BTFaultTrackerRecentlySearchedTableViewCell"];

    self.enterFaultReferenceTextField.delegate = self;
    //[self.enterFaultReferenceTextField getBTTextFieldStyle];
    
    /*UIButton *questionHint = [[UIButton alloc] initWithFrame:CGRectMake(0.0,
                                                                        0.0,
                                                                        30, 30)];
    [questionHint setImage:[UIImage imageNamed:@"question"] forState:UIControlStateNormal];
    [questionHint addTarget:self action:@selector(helpButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    self.enterFaultReferenceTextField.rightView = questionHint;
    self.enterFaultReferenceTextField.rightViewMode = UITextFieldViewModeAlways;
    questionHint.hidden = NO;*/
    
    //[self.trackFaultButton getBTButtonStyle];
    
    // Order reference textfield signal.
    RACSignal *faultIdSignal = [[[self enterFaultReferenceTextField] rac_textSignal] map:^id (NSString* faultId) {
        faultId = [AppManager stringByTrimmingWhitespaceInString:faultId];
        
        // Order reference textfield Validation.
        BOOL isValid = [faultId length] > 0;
        
        return [NSNumber numberWithBool:isValid];
    }];
    __weak typeof(self) selfWeak = self;
    // Order Id is required field.
    RACSignal *shouldEnableTrackFaultButtonSignal = [RACSignal combineLatest:@[faultIdSignal] reduce:^id (NSNumber *faultIdIsValid) {
        return @([faultIdIsValid boolValue]);
    }];
    
    // Enable / disable button as appropriate.
    [shouldEnableTrackFaultButtonSignal subscribeNext:^(NSNumber* shouldEnable) {
        [[selfWeak trackFaultButton] setEnabled:[shouldEnable boolValue]];
        if([shouldEnable boolValue] == NO){
            [[selfWeak trackFaultButton] setBackgroundColor:[UIColor grayColor]];
            [[selfWeak trackFaultButton] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        } else {
             [[selfWeak trackFaultButton] setBackgroundColor:[UIColor colorWithRed:100.0/255.0 green:0.0/255.0 blue:170.0/255.0 alpha:1]];
            [[selfWeak trackFaultButton] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
    }];

    [self createLoadingView];
    [self hideLoadingItems:YES];
    
    //[self addHelpCard];
    
    [self trackOmniPage:OMNIPAGE_FAULT_SEARCH_FAULT];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.viewModel fetchRecentSearchedFaults];
        [self updateRecentlyViewedLabel];
    });
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.enterFaultReferenceTextField becomeFirstResponder];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self.enterFaultReferenceTextField setText:@""];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //[self addHelpCard];
}


#pragma mark - table view datasource protocol methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.viewModel.arrayOfRecentSearchedFaults count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTFaultTrackerRecentlySearchedTableViewCell *recentlySearchedFaultDataCell = [tableView dequeueReusableCellWithIdentifier:@"BTFaultTrackerRecentlySearchedTableViewCell" forIndexPath:indexPath];
    [recentlySearchedFaultDataCell updateCellWithRecentSearchedFaultData:self.viewModel.arrayOfRecentSearchedFaults[indexPath.row]];
    return recentlySearchedFaultDataCell;
}


#pragma mark - table view delegate protocol methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    BTRecentSearchedFault *fault = self.viewModel.arrayOfRecentSearchedFaults[indexPath.row];
    NSString *faultReference = [fault.faultRef stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *linkName = [NSString stringWithFormat:@"%@ %@",fault.faultStatus,OMNICLICK_FAULT_DETAILS];
    [self trackOmniClick:linkName forPage:OMNIPAGE_FAULT_SEARCH_FAULT withFaultRef:faultReference];

    BOOL isValidated = [self validateFaultIdOrServiceId:faultReference];

    if (isValidated) {
        
        [self.enterFaultReferenceTextField resignFirstResponder];
        [self redirectToFaultSummaryWithFaultReference:faultReference];
    }
}


#pragma mark - UITextFieldDelegateMethods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if(![textField.text isEqualToString:@""]) {
        [self performTrackFaultActionMethod];
    }
    
    return true;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

#pragma mark - UI Methods
- (void)createLoadingView {
    
    _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
    _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:_loadingView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}

- (void)hideLoadingItems:(BOOL)isHide {
    
    // (lp) Hide or Show UI elements related to loading.
    [_loadingView setHidden:isHide];
    
}



#pragma mark - Private Helper Method


- (void)updateRecentlyViewedLabel
{
    if([self.viewModel.arrayOfRecentSearchedFaults count]==0)
    {
        self.recentlySearchLabel.text = kNoRecentlyViewedText;
        self.recentlySearchLabel.textAlignment = NSTextAlignmentCenter;
        self.recentlySearchLabel.textColor = [UIColor colorForHexString:@"666666"];
        self.recentlySearchLabel.font = [UIFont fontWithName:kBtFontRegular size:16.0f];
    }
    else
    {
        self.recentlySearchLabel.text = kRecentlyViewedText;
        self.recentlySearchLabel.textAlignment = NSTextAlignmentLeft;
        self.recentlySearchLabel.textColor = [UIColor colorForHexString:@"333333"];
        self.recentlySearchLabel.font = [UIFont fontWithName:kBtFontBold size:14.0f];
    }
}




-(void)performTrackFaultActionMethod
{
    
    __weak typeof(self) weakSelf = self;
    
    NSString *inputFaultString = [self.enterFaultReferenceTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (inputFaultString.length > 0) {
        
        BOOL isValidated = [self validateFaultIdOrServiceId:inputFaultString];
        
        if (isValidated)
        {
            
            self.enterFaultReferenceTextField.text = @"";
            [self.enterFaultReferenceTextField resignFirstResponder];
            
            [self redirectToFaultSummaryWithFaultReference:inputFaultString];
        }
        else
        {
            
            //Event tracking
            [OmnitureManager trackError:OMNIERROR_FAULT_REF_NOT_EXIST onPageWithName:OMNIPAGE_FAULT_SEARCH_FAULT contextInfo:[AppManager getOmniDictionary]];
            
            DDLogError(@"Error : Fault reference/broadband/phone number isn't right.");
            UIAlertController *invalidFaultIdAlert = [UIAlertController alertControllerWithTitle:@"Sorry, that reference isn’t right." message:@"" preferredStyle:UIAlertControllerStyleAlert];
            
            NSMutableAttributedString *messageString = [[NSMutableAttributedString  alloc] initWithString:@"For guidance click on 'Need help?' or try again!"];
            [messageString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:NSMakeRange(0, messageString.length)];
           // [messageString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:NSMakeRange(23, 1)];
            
            [invalidFaultIdAlert setValue:messageString forKey:@"attributedMessage"];
            
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
            
            [invalidFaultIdAlert addAction:okButton];
            [self presentViewController:invalidFaultIdAlert animated:YES completion:nil];
        }
    } else {
        //Event tracking
        [OmnitureManager trackError:OMNIERROR_FAULT_REF_NOT_EXIST onPageWithName:OMNIPAGE_FAULT_SEARCH_FAULT contextInfo:[AppManager getOmniDictionary]];
        
        DDLogError(@"Error : Fault reference/broadband/phone number isn't right.");
        UIAlertController *invalidFaultIdAlert = [UIAlertController alertControllerWithTitle:@"Sorry, that reference isn’t right." message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        NSMutableAttributedString *messageString = [[NSMutableAttributedString  alloc] initWithString:@"For guidance click on 'Need help?' or try again!"];
        [messageString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:NSMakeRange(0, messageString.length)];
       // [messageString addAttribute:NSFontAttributeName  value:[UIFont boldSystemFontOfSize:13] range:NSMakeRange(23, 1)];
        
        [invalidFaultIdAlert setValue:messageString forKey:@"attributedMessage"];
        
        [self trackContentViewedForPage:OMNIPAGE_FAULT_SEARCH_FAULT andNotificationPopupDetails:@"Sorry we didn’t recognise that fault reference"];
        
        UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
            [weakSelf trackContentViewedForPage:OMNIPAGE_FAULT_SEARCH_FAULT andNotificationPopupDetails:@"OK"];
        }];
        
        [invalidFaultIdAlert addAction:okButton];
        [self presentViewController:invalidFaultIdAlert animated:YES completion:nil];
    }
}

- (void)redirectToFaultSummaryWithFaultReference:(NSString *)inputFaultString
{
    
    if ([self isFaultId:inputFaultString]) {
        

        BTFaultRestrictedDetailsViewController *faultSummaryViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FaultRestrictedDetailsScene"];
        faultSummaryViewController.faultRef = inputFaultString;
        
        [self.navigationController pushViewController:faultSummaryViewController animated:YES];
        
    } else {
        
        BTFaultTrackerSummaryViewController *faultSummaryViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FaultSummaryScene"];
        faultSummaryViewController.faultReference = inputFaultString;
        faultSummaryViewController.isServiceId = YES;
        [self.navigationController pushViewController:faultSummaryViewController animated:YES];
    }
    
    
}


- (BOOL)validateFaultIdOrServiceId:(NSString *)inputString
{
    BOOL isValidFault = NO;
    //Sample Faults 1.VOL022-190384011 2.VOL012-12345678901
    if ( ([inputString rangeOfString:@"^[Vv][Oo0][Ll][0][1234Ii]{2}[-]{0,1}[0-9OoSsIi]{9}$" options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location != NSNotFound ) || ( [inputString rangeOfString:@"^[Vv][Oo0][Ll][0][1234Ii]{2}[-]{0,1}[0-9OoSsIi]{11}$" options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location != NSNotFound ) ) {
        isValidFault = YES;
    } else if ( [inputString rangeOfString:@"^[1Ii3][-]{0,1}[0-9OoSsIi]{9}$" options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location != NSNotFound ) {
        //Sample FaultRef:1-190251813 for Regex:^[1Ii3][-]{0,1}[0-9OoSsIi]{9}$
        isValidFault = YES;
    } else if ( [inputString rangeOfString:@"^[0Oo][0-9OoSsIi]{10}$" options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location != NSNotFound ) {
        //Sample FaultRef:^[0Oo][0-9OoSsIi]{10}$   Ref:01212345678
        isValidFault = YES;
    } else if ( ( [inputString rangeOfString:@"^[0Oo][12Ii]{2}[-]{0,1}[0-9OoSsIi]{9}$" options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location != NSNotFound ) || ( [inputString rangeOfString:@"^[0Oo][12Ii]{2}[-]{0,1}[0-9OoSsIi]{11}$" options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location != NSNotFound )) {
        isValidFault = YES;
    } else if ( [inputString rangeOfString:@"^[0Oo][12Ii]{2}[-]{0,1}[0-9OoSsIi]{12,99}$" options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location != NSNotFound ) {
        isValidFault = NO;
    } else {
        isValidFault = NO;
    }
    return isValidFault;
}



- (BOOL)isFaultId:(NSString *)inputString
{
    if ([inputString containsString:@"-"]) {
        return true;
    } else {
        return false;
    }
}



#pragma mark - Button action methods


- (IBAction)trackFaultButtonAction:(id)sender {

    [self performTrackFaultActionMethod];

}


- (IBAction)helpButtonAction:(id)sender {
    
    BTFaultSearchHelpViewController *helpViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"faultSearchHelpViewController"];
    [self.navigationController presentViewController:helpViewController animated:YES completion:nil];
    
}



#pragma mark - DLMFaultSearchScreen delegate  methods
- (void)successfullyFetchedRecentSearchedFaultDataOnFaultSearchScreen:(DLMFaultSearchScreen *)faultSearchScreen
{
    [_recentlySearchedFaultIdTableView reloadData];
}

#pragma mark - Text field delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSRange lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
    
    if (lowercaseCharRange.location != NSNotFound) {
        textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                 withString:[string uppercaseString]];
        return NO;
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [_enterFaultReferenceTextField updateWithDisabledState];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [_enterFaultReferenceTextField updateWithEnabledState];
}

#pragma mark - StatusBarMethods

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

# pragma mark - help card
- (void) addHelpCard {
    _helpCardVisible = NO;
    _helpCard = [[[NSBundle mainBundle] loadNibNamed:@"FaultTrackerSearchHelp" owner:nil options:nil] objectAtIndex:0];
    CGRect helpCardFrame = _helpCard.frame;
    
    helpCardFrame.origin.x = ([UIScreen mainScreen].bounds.size.width / 2) - (helpCardFrame.size.width / 2);
    
    UITabBarController *tabBarController = [UITabBarController new];
    CGFloat tabBarHeight = tabBarController.tabBar.frame.size.height; // what's the height of a tabbarviewcontroller?
    float height = [UIScreen mainScreen].bounds.size.height;
    if(height == kTallDeviceHeight) {
        helpCardFrame.origin.y = [UIScreen mainScreen].bounds.size.height - (tabBarHeight + + kSpaceFromBottom + kTallDeviceAdditionalSpacing);
    } else {
        helpCardFrame.origin.y = [UIScreen mainScreen].bounds.size.height - (tabBarHeight + kSpaceFromBottom);
    }
    
    [_helpCard setFrame:helpCardFrame];

    UITapGestureRecognizer *tappedHelpCard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moveHelpCard)];
    [_helpCard addGestureRecognizer:tappedHelpCard];
    [self.view addSubview:_helpCard];
}

- (void) moveHelpCard {
    CGRect helpCardFrame = _helpCard.frame;

    if(_helpCardVisible){
        _helpCardVisible = NO;
        helpCardFrame.origin.y += kHelpCardMovement;
    }
    else {
        _helpCardVisible = YES;
        helpCardFrame.origin.y -= kHelpCardMovement;
    }
    
    _helpCard.frame = helpCardFrame;
}

#pragma mark - Omniture Methods

- (void)trackOmniPage:(NSString *)page
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:[AppManager getOmniDictionary]];
    
    [OmnitureManager trackPage:page withContextInfo:params];
}


- (void)trackOmniClick:(NSString *)clickEvent forPage:(NSString *)page
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:[AppManager getOmniDictionary]];
    NSString *pageName = page;
    NSString *linkTitle = clickEvent;
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:params];
}


- (void)trackOmniClick:(NSString *)clickEvent forPage:(NSString *)page withFaultRef:(NSString *)faultRef
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:[AppManager getOmniDictionary]];
    NSString *pageName = page;
    NSString *linkTitle = clickEvent;
    
    if(faultRef && faultRef.length>0)
    {
        NSString *ref = [NSString stringWithFormat:@"%@%@",kOmniFaultRefFormat,faultRef];
        [params setValue:ref forKey:kOmniFaultRef];
    }
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:params];
}



- (void)trackContentViewedForPage:(NSString *)page andNotificationPopupDetails:(NSString *)notificationPopup
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = page;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    NSString *notificaiton = [NSString stringWithFormat:@"%@%@",STATE_PREFIX,notificationPopup];
    [data setValue:notificaiton forKey:kOmniContentViewed];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
    
}



@end
