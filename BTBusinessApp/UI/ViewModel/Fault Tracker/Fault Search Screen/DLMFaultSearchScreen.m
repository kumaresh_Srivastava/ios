//
//  DLMFaultSearchScreen.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 19/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMFaultSearchScreen.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDRecentSearchedFault.h"
#import "BTRecentSearchedFault.h"
#import "NLGetFaultSummaryDetailsWebService.h"
#import "CDAuthenticationToken.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "BTFault.h"
#import "DLMFaultSummaryScreen.h"
#import "DLMFaultDetailsScreen.h"
#import "NLWebServiceError.h"

@interface DLMFaultSearchScreen ()

@property (nonatomic) NLGetFaultSummaryDetailsWebService *getFaultSummaryDetailsWebService;

@end

@implementation DLMFaultSearchScreen

#pragma mark - Public helper method

- (void)fetchRecentSearchedFaults {
    
    NSArray *recentSearchedFaults = [[AppDelegate sharedInstance].viewModel.app.loggedInUser.recentlySearchedFaults allObjects];
    
    NSArray *sortedRecentSearchedFaults = [recentSearchedFaults sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"lastSearchedDate" ascending:NO]]];
    
    NSMutableArray *faults = [NSMutableArray array];
    int i = 0;
    for (CDRecentSearchedFault *recentSearchedFault in sortedRecentSearchedFaults) {
        BTRecentSearchedFault *searchedFault = [[BTRecentSearchedFault alloc] initWithCDRecentSearchedFault:recentSearchedFault];
        [faults addObject:searchedFault];
        i++;
        if (i == 10) {
            break;
        }
    }
    
    _arrayOfRecentSearchedFaults = [NSArray arrayWithArray:faults];
    [self.faultSearchScreenDelegate successfullyFetchedRecentSearchedFaultDataOnFaultSearchScreen:self];
    
}

@end
