//
//  BTFaultTrackerSearchViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 27/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

// Code Review Process:
// 09-09-2016: Reviewed By Harman

#import <UIKit/UIKit.h>

@interface BTFaultTrackerSearchViewController : UIViewController

@property (nonatomic) BOOL networkRequestInProgress;

@end
