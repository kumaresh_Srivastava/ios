//
//  BTFaultSearchHelpViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 20/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultSearchHelpViewController.h"
#import "OmnitureManager.h"
#import "AppManager.h"

@interface BTFaultSearchHelpViewController ()
@property (nonatomic, retain) UIView *statusBarView;

@end

@implementation BTFaultSearchHelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self createAndAddCustomStatusBarView];
    
    
    [self trackOmniPage:OMNIPAGE_FAULT_SEARCH_FAULT_HELP];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Private methods

- (void)createAndAddCustomStatusBarView
{
    _statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 20)];

    _statusBarView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:_statusBarView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:_statusBarView
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:_statusBarView
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:_statusBarView
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1.0
                                                           constant:20.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:_statusBarView
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
}



#pragma mark - Action Methods

- (IBAction)cancelButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Rotation Methods

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id)coordinator {
    
    // before rotation
    
    [coordinator animateAlongsideTransition:^(id  _Nonnull context) {
        
        self.statusBarView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 20);
        
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        
    } completion:^(id  _Nonnull context) {
        
        // after rotation
        
    }];
}

#pragma mark - Omniture Methods

- (void)trackOmniPage:(NSString *)page
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:[AppManager getOmniDictionary]];
    
    [OmnitureManager trackPage:page withContextInfo:params];
}




@end
