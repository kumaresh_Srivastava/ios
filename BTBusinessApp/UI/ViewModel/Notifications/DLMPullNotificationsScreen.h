//
//  DLMPullNotificationsScreen.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 27/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"

@interface DLMPullNotificationsScreen : DLMObject {
    
}

@property (nonatomic, copy) NSArray *arrayOfNotifications;
@property (nonatomic) BOOL isNotificationScreenCurrentlyVisible;
@property (nonatomic) BOOL isGetNotificationFailedOnOtherScreen;

- (void)updateNotificationStatusForNotificationId:(NSString *)notificationId;

@end
