//
//  DLMPullNotificationsScreen.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 27/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMPullNotificationsScreen.h"
#import "NLUpdateNotificationWebService.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "CDAuthenticationToken.h"
#import "AppDelegateViewModel.h"
#import "AppDelegate.h"

@interface DLMPullNotificationsScreen()<NLUpdateNotificationWebServiceDelegate> {
    
    BOOL _updateNotificationAPICallInProgress;
    NSMutableArray *_arrayOfPendingNotificationIds;
}

@property(nonatomic, strong) NLUpdateNotificationWebService *updateNotificationWebService;

@end

@implementation DLMPullNotificationsScreen

- (void)updateNotificationStatusForNotificationId:(NSString *)notificationId
{
    if (_updateNotificationAPICallInProgress)
    {
        if (_arrayOfPendingNotificationIds == nil)
        {
            _arrayOfPendingNotificationIds = [NSMutableArray array];
        }
        
        [_arrayOfPendingNotificationIds addObject:notificationId];
    }
    else
    {
        [self updateNotificationStatusAPICallForNotificationId:notificationId];
    }
}

- (void)updateNotificationStatusAPICallForNotificationId:(NSString *)notificationId
{
    _updateNotificationAPICallInProgress = YES;
    self.updateNotificationWebService = [[NLUpdateNotificationWebService alloc] initWithNotificationId:notificationId];
    self.updateNotificationWebService.updateNotificationWebServiceDelegate = self;
    [self.updateNotificationWebService resume];
}

- (void)checkForPendingNotificationsToUpdateStatusWithApiCall
{
    if (_arrayOfPendingNotificationIds.count > 0)
    {
        NSString *notificationId = [[_arrayOfPendingNotificationIds objectAtIndex:0] copy];
        
        [_arrayOfPendingNotificationIds removeObjectAtIndex:0];
        
        [self updateNotificationStatusAPICallForNotificationId:notificationId];
    }
}

#pragma mark- NLUpdateNotificationWebServiceDelegate methods

- (void)successfullyUpdatedNotificationsDataByUpdateNotificationWebService:(NLUpdateNotificationWebService *)webService{
    
    if(self.updateNotificationWebService == webService){
        
        
        self.updateNotificationWebService.updateNotificationWebServiceDelegate = nil;
        self.updateNotificationWebService = nil;
        
        _updateNotificationAPICallInProgress = NO;
        
        [self checkForPendingNotificationsToUpdateStatusWithApiCall];
    }
    else{
        
        DDLogError(@"This delegate method gets called because of success of an object of NLUpdateNotificationWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLUpdateNotificationWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
    
}


- (void)updateNotificationWebService:(NLUpdateNotificationWebService *)webService failedToUpdateNotificationsDataWithWebServiceError:(NLWebServiceError *)error{
    
    
    if(self.updateNotificationWebService == webService){
    
        self.updateNotificationWebService.updateNotificationWebServiceDelegate = nil;
        self.updateNotificationWebService = nil;
        _updateNotificationAPICallInProgress = NO;
        
        [self checkForPendingNotificationsToUpdateStatusWithApiCall];
    }
    else{
        
        DDLogError(@"This delegate method gets called because of failure of an object of NLUpdateNotificationWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLUpdateNotificationWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

@end
