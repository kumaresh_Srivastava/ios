
#import "BTPullNotificationsViewController.h"
#import "BTPullNotificationsTableViewCell.h"
#import "CustomSpinnerAnimationView.h"
#import "DLMPullNotificationsScreen.h"
#import "AppConstants.h"
#import "BTNotification.h"
#import "AppManager.h"
#import "BTRetryView.h"
#import "OmnitureManager.h"
#import "NotificationNoInternetStripView.h"
#import "AppManager.h"
#define kNotificationBarHeight 68
#import "BTFaultTrackerDashboardViewController.h"

@interface BTPullNotificationsViewController ()<UITableViewDelegate, UITableViewDataSource, BTRetryViewDelegate,NotificationNoInternetStripViewDelegate> {
    CustomSpinnerAnimationView *_loaderView;

    UILabel *_loadingLabel;
    BTRetryView *_retryView;
    NotificationNoInternetStripView *_noInternetStripView;
    NSIndexPath *_selectedNotificationIndexPath;
    UIView *_UIBlockerView;
}

@property (weak, nonatomic) IBOutlet UITableView *pullNotificationsTableView;
@property (weak, nonatomic) IBOutlet UIView *noNotificationView;
@property (weak, nonatomic) IBOutlet UIView *headerView;

@end

@implementation BTPullNotificationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
    _headerView.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
    
    _pullNotificationsTableView.delegate = self;
    _pullNotificationsTableView.dataSource = self;
    _pullNotificationsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _pullNotificationsTableView.estimatedRowHeight = 94;
    _pullNotificationsTableView.rowHeight = UITableViewAutomaticDimension;
    _pullNotificationsTableView.backgroundColor = [UIColor whiteColor];
    [_pullNotificationsTableView.layer setShadowOffset:CGSizeMake(0, 0)];
    _pullNotificationsTableView.clipsToBounds = NO;
    _pullNotificationsTableView.layer.masksToBounds = NO;
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNotificationsAPICallFinishedSuccessfully:) name:@"APICallForGetNotificationsFinishedSuccessfullyNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNotificationsAPICallStarted) name:@"APICallForGetNotificationsStartedNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNotificationsAPICallFailed) name:@"APICallForGetNotificationsFailedNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNotificationsAPICallCancelled) name:@"APICallForGetNotificationsCancelledNotification" object:nil];
    
    // Code to enable swipping in side menu
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    
    // Setting the swipe direction.
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    
    [self.view addGestureRecognizer:swipeRight];
    
    
    
}


- (void)viewDidAppear:(BOOL)animated {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationNotificationController1:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)statusBarOrientationNotificationController1:(NSNotification *)notification {
    // handle the interface orientation as needed
    if (self.notificationViewModel.isNotificationScreenCurrentlyVisible) //BUG 16705
     [self performSelector:@selector(handleNotification) withObject:nil afterDelay:0.1];
}

- (void) handleNotification {
    [self.pullNotificationsViewControllerDelegate handleNotificationOrientation:self];
}

#pragma mark - Public methods
+ (BTPullNotificationsViewController *)getViewController {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTPullNotificationsViewController *notificationViewController = [storyboard instantiateViewControllerWithIdentifier:@"PullNotificationsViewController"];
    notificationViewController.notificationViewModel = [[DLMPullNotificationsScreen alloc] init];
    notificationViewController.notificationViewModel.isNotificationScreenCurrentlyVisible = NO;
    notificationViewController.notificationViewModel.isGetNotificationFailedOnOtherScreen = NO;
    
    return notificationViewController;
}



#pragma mark - Private helper methods
- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
        [self.pullNotificationsViewControllerDelegate hideNotificationForPullNotificationsViewController:self];
    }
}


- (void)updateStatusAndCheckForUnreadNotificationsWith:(NSInteger )notificationId
{
    
    [self.notificationViewModel updateNotificationStatusForNotificationId:[NSString stringWithFormat:@"%ld", (long)notificationId]];
    
    [self checkForUnreadNotifications];
    
}


- (void)checkForUnreadNotifications {
    
    BOOL unreadNotificationsAvailable = false;
    
    for (BTNotification *notification in self.notificationViewModel.arrayOfNotifications) {
        if (notification.notificationStatus == BTNotificationTypeUnRead) {
            
            unreadNotificationsAvailable = true;
            break;
        }
    }
    
    if (!unreadNotificationsAvailable) {
        [self.pullNotificationsViewControllerDelegate noUnReadItemsLeftOnNotificationsViewController:self];
    }
}


- (void)resetNotificationsRelatedDataAndTableView
{
    self.notificationViewModel.arrayOfNotifications = nil;
    [self.pullNotificationsTableView reloadData];
    
    [self.noNotificationView setHidden:YES];
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if ([self isTappedOutsideNotificationsViewWithTouchLoaction:touchLocation])
    {
        [self.pullNotificationsViewControllerDelegate hideNotificationForPullNotificationsViewController:self];
    }
}


- (BOOL)isTappedOutsideNotificationsViewWithTouchLoaction:(CGPoint)touchLocation
{
    if (touchLocation.x < (self.view.frame.size.width * 0.15)) {
        return YES;
    }
    return NO;
}



- (void)updateUIWithNotificationData
{
    if ([self.notificationViewModel.arrayOfNotifications count] > 0) {
        [self.pullNotificationsTableView reloadData];
        [self.noNotificationView setHidden:YES];
    }
    else
    {
        [self.noNotificationView setHidden:NO];
    }
}



- (void)createLoaderImageView
{
    _loaderView = [[CustomSpinnerAnimationView alloc] initWithText:@"" loadingImage:[UIImage imageNamed:@"Spinner"] andLoaderImageSize:40.0f];
    
    [self.view addSubview:_loaderView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.pullNotificationsTableView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.pullNotificationsTableView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    [_loaderView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:100]];
    [_loaderView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:155]];
    _loaderView.translatesAutoresizingMaskIntoConstraints = NO;
    
    _loadingLabel = [[UILabel alloc] init];
    _loadingLabel.text = @"Loading...";
    _loadingLabel.textColor = [UIColor blackColor];
    _loadingLabel.font = [UIFont fontWithName:kBtFontRegular size:15];
    _loadingLabel.numberOfLines = 1;
    _loadingLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _loadingLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.view addSubview:_loadingLabel];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.pullNotificationsTableView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_loaderView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:20.0f]];
}


- (void)showLoaderView
{
    if (_loaderView == nil) {
        [self createLoaderImageView];
    }
    
    _loaderView.hidden = NO;
    _loadingLabel.hidden = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_loaderView startAnimation];
    });
}



- (void)hideLoaderView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [_loaderView stopAnimation];
    });
    
    _loaderView.hidden = YES;
    _loadingLabel.hidden = YES;
    
    [_loaderView removeFromSuperview];
    [_loadingLabel removeFromSuperview];
    
    _loadingLabel = nil;
    _loaderView = nil;
    
}



- (void)createRetryView
{
    _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
    _retryView.translatesAutoresizingMaskIntoConstraints = NO;
    _retryView.retryViewDelegate = self;
    
    [self.view addSubview:_retryView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.pullNotificationsTableView  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.pullNotificationsTableView  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.pullNotificationsTableView  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
}



- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow {
    if (_retryView == nil) {
        [self createRetryView];
    }
    
    if(internetStripNeedToShow)
        [self trackNoConntectionError];
    
    _retryView.hidden = NO;
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
}


- (void)hideRetryView
{
    [_retryView setRetryViewDelegate:nil];
    [_retryView setHidden:YES];
    [_retryView removeFromSuperview];
    _retryView = nil;
}

- (void)startFetchingNotificationsData
{
    
    self.notificationViewModel.isGetNotificationFailedOnOtherScreen = NO;
    
    if ([AppManager isInternetConnectionAvailable]) {
        [self.pullNotificationsViewControllerDelegate userPressedRetryButtonOnNotificationsViewController:self];
    }
    else
    {
        [self trackNoConntectionError];
        [self showRetryViewWithInternetStrip:NO];
    }
    
}

- (void)changeTableViewFrame
{
    CGRect tableViewNewFrame = self.pullNotificationsTableView.frame;
    tableViewNewFrame.origin.y = kNotificationBarHeight+ _noInternetStripView.frame.size.height;
    tableViewNewFrame.size.height -= _noInternetStripView.frame.size.height;
    
    self.pullNotificationsTableView.frame = tableViewNewFrame;

}


- (void)updateCellForIndexPath:(NSIndexPath *)selectedIndexPath
{
    BTNotification *notificationData = [self.notificationViewModel.arrayOfNotifications objectAtIndex:selectedIndexPath.row];
    [notificationData updateNotificationStatus];
    
    BTPullNotificationsTableViewCell *notificationCell = [self.pullNotificationsTableView cellForRowAtIndexPath:selectedIndexPath];
    [notificationCell updateCellWithNotificationData:notificationData];
    
    [self updateStatusAndCheckForUnreadNotificationsWith:notificationData.notificationId];
}


- (void)showNoInternetStrip
{
    _noInternetStripView = [[[NSBundle mainBundle] loadNibNamed:@"NotificationNoInternetStripView" owner:nil options:nil] firstObject];
    _noInternetStripView.noInternetStripDelegate = self;
    [self.view addSubview:_noInternetStripView];
   
    //Update table frame
    [self performSelector:@selector(changeTableViewFrame) withObject:nil afterDelay:0.001];
    
    //Update no internet strip frame
    _noInternetStripView.frame = CGRectMake(self.pullNotificationsTableView.frame.origin.x,kNotificationBarHeight,self.pullNotificationsTableView.frame.size.width, _noInternetStripView.frame.size.height);
    
    //Create Blocker view
    _UIBlockerView = [[UIView alloc] initWithFrame:CGRectMake(self.pullNotificationsTableView.frame.origin.x, kNotificationBarHeight+_noInternetStripView.frame.size.height, self.pullNotificationsTableView.frame.size.width, self.pullNotificationsTableView.frame.size.height)];
    
    _UIBlockerView.backgroundColor = [UIColor blackColor];
    _UIBlockerView.alpha = 0.3;
    
    [self.view addSubview:_UIBlockerView];

}


- (void)removeNoInternetStrip
{
   
    CGRect tableViewNewFrame = self.pullNotificationsTableView.frame;
    tableViewNewFrame.origin.y = kNotificationBarHeight;
    tableViewNewFrame.size.height += _noInternetStripView.frame.size.height;
    self.pullNotificationsTableView.frame = tableViewNewFrame;
    
    [_noInternetStripView removeFromSuperview];
    _noInternetStripView = nil;
    
    [_UIBlockerView removeFromSuperview];
    _UIBlockerView = nil;
}


- (void)trackNoConntectionError
{
    [OmnitureManager trackError:OMNIERROR_NOTIFICAION_NO_CONNECTION onPageWithName:OMNIPAGE_NOTIFICATION contextInfo:[AppManager getOmniDictionary]];
}


#pragma mark - Table view datasource methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.notificationViewModel.arrayOfNotifications count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BTNotification *notificationData = [self.notificationViewModel.arrayOfNotifications objectAtIndex:indexPath.row];
    
    BTPullNotificationsTableViewCell *notificationCell = [tableView dequeueReusableCellWithIdentifier:@"PullNotificationsCell" forIndexPath:indexPath];
    [notificationCell updateCellWithNotificationData:notificationData];
    return notificationCell;
}


#pragma mark - TableView delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self trackClickOnNotificationActionForOmniture];
    
    BTNotification *notificationData = [self.notificationViewModel.arrayOfNotifications objectAtIndex:indexPath.row];
    
    if (notificationData.notificationStatus == BTNotificationTypeUnRead) {
        
        [notificationData updateNotificationStatus];
        
        BTPullNotificationsTableViewCell *notificationCell = [self.pullNotificationsTableView cellForRowAtIndexPath:indexPath];
        [notificationCell updateCellWithNotificationData:notificationData];
        
        [self updateStatusAndCheckForUnreadNotificationsWith:notificationData.notificationId];
       
    }
    
    NSString* notificationCat = notificationData.notificationCategory;
    [self.pullNotificationsViewControllerDelegate hideNotificationForPullNotificationsViewController:self];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:notificationCat forKey:@"notificationCategory"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"navigateToSelectedScreenFromNotification" object:nil userInfo:dictionary];
}

-(void) trackClickOnNotificationActionForOmniture{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
       
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",OMNIPAGE_NOTIFICATION, OMNICLICK_NOTIFICATIONS_DETAILS] withContextInfo:data];
}

#pragma mark - Notification related methods
- (void)getNotificationsAPICallStarted
{
    [self resetNotificationsRelatedDataAndTableView];
    
    [self hideRetryView];
    [self showLoaderView];
}


- (void)getNotificationsAPICallFinishedSuccessfully:(NSNotification *)notification
{
    [self hideLoaderView];
    
    NSDictionary *dict = notification.userInfo;
    self.notificationViewModel.arrayOfNotifications = [dict valueForKey:@"notifications"];
    
    [self updateUIWithNotificationData];
}


- (void)getNotificationsAPICallFailed
{
    [self hideLoaderView];
    [self showRetryViewWithInternetStrip:NO];
    [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_NOTIFICATION];
    
    if (!self.notificationViewModel.isNotificationScreenCurrentlyVisible) {
        self.notificationViewModel.isGetNotificationFailedOnOtherScreen = YES;
    }
}


- (void)getNotificationsAPICallCancelled
{
    [self hideLoaderView];
    [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_NOTIFICATION];
    [self showRetryViewWithInternetStrip:NO];
}


#pragma mark - BTRetryView delegate methods
- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView {
    
    DDLogInfo(@"Retry to fetch notifications data");
   if([AppManager isInternetConnectionAvailable])
   {
    [self.pullNotificationsViewControllerDelegate userPressedRetryButtonOnNotificationsViewController:self];
   }
   else
   {
       [self trackNoConntectionError];
   }
}

#pragma mark - Notification NO Internet strip view delegate
- (void)userPressedRetryViewOnNotificationNoInternetStripView:(NotificationNoInternetStripView *)noInternetStripView
{
    if([AppManager isInternetConnectionAvailable])
    {
        [self removeNoInternetStrip];
        [self updateCellForIndexPath:_selectedNotificationIndexPath];
    }
    else
    {
         [self trackNoConntectionError];
    }
}

@end
