//
//  BTPullNotificationsViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 25/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTPullNotificationsViewController;
@class DLMPullNotificationsScreen;

@protocol BTPullNotificationsViewControllerDelegate <NSObject>

- (void)hideNotificationForPullNotificationsViewController:(BTPullNotificationsViewController *)pullNotificationsViewController;
- (void)userPressedRetryButtonOnNotificationsViewController:(BTPullNotificationsViewController *)pullNotificationsViewController;
- (void)noUnReadItemsLeftOnNotificationsViewController:(BTPullNotificationsViewController *)pullNotificationsViewController;
- (void)handleNotificationOrientation:(BTPullNotificationsViewController *)pullNotificationsViewController;

@end

@interface BTPullNotificationsViewController : UIViewController {
    
}

@property (nonatomic, weak) id <BTPullNotificationsViewControllerDelegate> pullNotificationsViewControllerDelegate;
@property (nonatomic, strong) DLMPullNotificationsScreen *notificationViewModel;

+ (BTPullNotificationsViewController *)getViewController;
- (void)startFetchingNotificationsData;
//- (void)removeNoInternetStrip;(RLM) This method needs to be called from Menu slider in order to remove no internet strip if Notificaiton screen closes.

@end
