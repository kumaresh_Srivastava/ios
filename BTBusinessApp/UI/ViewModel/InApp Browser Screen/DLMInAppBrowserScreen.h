//
//  DLMInAppBrowserScreen.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 9/11/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, BTUserJourneyType) {
    
    BTUserJourneyTypeForgotPassword,
    BTUserJourneyTypeForgotUsername,
    BTUserJourneyTypeRegistration,
    BTUserJourneyTypeUnknown
};

@interface DLMInAppBrowserScreen : NSObject

- (void)createTitleURLData;
- (NSString *)getTitleForURLString:(NSString *)urlString withJourneyType:(BTUserJourneyType)userJourneyType;

@end
