//
//  DLMBillBrowserScreen.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 13/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "DLMObject.h"
@class NLWebServiceError;
@class DLMBillBrowserScreen;

@protocol DLMBillBrowserScreenDelegate <NSObject>

- (void)getGetRefKeyWebService:(DLMBillBrowserScreen *)billBrowserScreen successfullyFetchedRefKey:(NSString *)refKey;

- (void)getRefKeyWebService:(DLMBillBrowserScreen *)billBrowserScreen failedToFetchRefKeyWithWebServiceError:(NLWebServiceError *)webServiceError;

@end
@interface DLMBillBrowserScreen : DLMObject

@property (nonatomic, weak) id<DLMBillBrowserScreenDelegate> billBrowserScreenDelegate;

- (void)fetchRefKey;
- (void)cancelRefKeyApi;
@end
