//
//  DLMBillBrowserScreen.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 13/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "DLMBillBrowserScreen.h"
#import "NLGetRefKeyWebService.h"

@interface DLMBillBrowserScreen()<NLGetRefKeyWebServiceDelegate>
@property (nonatomic, strong) NLGetRefKeyWebService *getRefKeyWebService;

@end

@implementation DLMBillBrowserScreen


#pragma mark - Public Methods

- (void)fetchRefKey {

    self.getRefKeyWebService = [[NLGetRefKeyWebService alloc] init];
    self.getRefKeyWebService.getRefKeyWebServiceDelegate = self;
    [self.getRefKeyWebService resume];
}

- (void)cancelRefKeyApi {

    self.getRefKeyWebService.getRefKeyWebServiceDelegate = nil;
    [self.getRefKeyWebService cancel];
    self.getRefKeyWebService = nil;
}


#pragma mark - NLGetRefKeyWebServiceDelegate

- (void)getGetRefKeyWebService:(NLGetRefKeyWebService *)webService successfullyFetchedRefKey:(NSString *)refKey {

    if(webService == self.getRefKeyWebService)
    {

        [self.billBrowserScreenDelegate getGetRefKeyWebService:self successfullyFetchedRefKey:refKey];

        self.getRefKeyWebService.getRefKeyWebServiceDelegate = nil;
        self.getRefKeyWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLGetRefKeyWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLGetRefKeyWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        
    }


}


- (void)getRefKeyWebService:(NLGetRefKeyWebService *)webService failedToFetchRefKeyWithWebServiceError:(NLWebServiceError *)webServiceError
{
    if(webService == self.getRefKeyWebService)
    {
        [self.billBrowserScreenDelegate getRefKeyWebService:self failedToFetchRefKeyWithWebServiceError:webServiceError];
        
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLGetRefKeyWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLGetRefKeyWebService but this object is not the one stored in member variable of this class %@.", [self class]);

    }

}
@end
