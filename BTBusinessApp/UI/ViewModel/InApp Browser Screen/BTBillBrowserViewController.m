//
//  BTBillBrowserViewController.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 13/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTBillBrowserViewController.h"
#import "AppConstants.h"
#import "BTCug.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "DLMBillBrowserScreen.h"
#import "CustomSpinnerView.h"

#define kSucessfullPayBillUrl @"mobPaymentComplete.html"

@interface BTBillBrowserViewController ()<DLMBillBrowserScreenDelegate>
{
    BOOL _hasWebViewLoadFinished;
    BOOL _hasTheFirstPageBeenFinished;
    NSString *_currentUrl;
    NSInteger apiFaliureCounter;
    NSString *_refKey;
    CustomSpinnerView *_loadingView;
}


@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, strong) DLMBillBrowserScreen *billBrowserScreenModel;
//@property(strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;

@end



@implementation BTBillBrowserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //self.title = @"Select bills to pay";
    self.title = @"Pick bills to pay";
    //[BTUICommon updateNavigationBar:self.navigationController];
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController.navigationBar setTranslucent:NO];

    UIBarButtonItem *closeButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonPressed:)];
    self.navigationItem.rightBarButtonItem = closeButtonItem;

    self.billBrowserScreenModel = [[DLMBillBrowserScreen alloc] init];
    self.billBrowserScreenModel.billBrowserScreenDelegate = self;

    [self.webView setBackgroundColor:[UIColor whiteColor]];
    [self.view setBackgroundColor:[UIColor whiteColor]];
//    [self createActivityIndicatorView];
    //[self createLoadingView];
//    [_loadingView stopAnimatingLoadingIndicatorView];
   // [_loadingView startAnimatingLoadingIndicatorView];

    
    [self fetchRefKey];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidDisappear:(BOOL)animated {

    [super viewDidDisappear:animated];
    [self.billBrowserScreenModel cancelRefKeyApi];
//    [self.activityIndicatorView stopAnimating];
    [self toggleLoadingItems:YES];
}

#pragma mark - Action Methods

- (void)closeButtonPressed:(id)sender {
    
//    [_activityIndicatorView stopAnimating];
//    [_activityIndicatorView removeFromSuperview];

    [self toggleLoadingItems:YES];
    if([_currentUrl containsString:kSucessfullPayBillUrl])
    {

        [[NSNotificationCenter defaultCenter] postNotificationName:kBillPaidSuccessfullyNotification object:self];

        CATransition* transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionReveal;
        transition.subtype = kCATransitionFromBottom;
        [self.navigationController.view.layer addAnimation:transition forKey:nil];

        [self.navigationController popToRootViewControllerAnimated:NO];
    }
    else
    {
        CATransition* transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionReveal;
        transition.subtype = kCATransitionFromBottom;
        [self.navigationController.view.layer addAnimation:transition forKey:nil];

        [self.navigationController popViewControllerAnimated:NO];
    }
}


#pragma mark - Private methods

/*- (void)createActivityIndicatorView
{
    _activityIndicatorView  = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _activityIndicatorView.color = [BrandColours colourBtDarkViolet];
    _activityIndicatorView.center = CGPointMake(self.view.center.x, self.view.center.y-64);
    _activityIndicatorView.hidesWhenStopped = YES;
     [self.activityIndicatorView startAnimating];
    [self.view addSubview:_activityIndicatorView];
}*/

- (void)createLoadingView {
    
    if(!_loadingView) {
        
        _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [_loadingView.spinnerBackgroundView setBackgroundColor:[UIColor clearColor]];
        [_loadingView.loadingLabel setHidden:YES];
        [_loadingView setBackgroundColor:[UIColor whiteColor]];
        [self.view addSubview:_loadingView];
    }
    
    _loadingView.hidden = YES;
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}

- (void)toggleLoadingItems:(BOOL)isHide {
    // (SD) Hide or Show UI elements related to loading.
    if(isHide)
    {
        [_loadingView stopAnimatingLoadingIndicatorView];
        [_loadingView setHidden:isHide];
        _loadingView = nil;
    }
    else
    {
        [self createLoadingView];
        [_loadingView setHidden:isHide];
        [_loadingView startAnimatingLoadingIndicatorView];
    }
}

#pragma mark - Webview Delegate
- (BOOL)webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType
{
    if([[request URL] absoluteString] == nil || [[[[request URL] absoluteString] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] || [[[[request URL] absoluteString] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@"about:blank"])
    {
        return NO;
    }

    BOOL shouldLoad = YES;
    _currentUrl = [[request URL] absoluteString];
    [self toggleLoadingItems:NO];
    return shouldLoad;
}


- (void)webViewDidStartLoad:(UIWebView *)webView
{
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    _hasTheFirstPageBeenFinished = YES;
//    [self.activityIndicatorView stopAnimating];
    [self toggleLoadingItems:YES];
}


- (void)webView:(UIWebView *)webView
didFailLoadWithError:(NSError *)error
{
//   [self.activityIndicatorView stopAnimating];
    [self toggleLoadingItems:YES];
}


#pragma mark - Private Helper Methods
- (void)fetchRefKey {

    [self.billBrowserScreenModel fetchRefKey];
}


- (void)loadUrlInWebViewWithURL:(NSString *)url andRefKey:(NSString *)refKey {

    self.webView.scalesPageToFit = YES;
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request setValue:refKey forHTTPHeaderField:@"_ref"];

    [self.webView loadRequest:request];
}

#pragma mark - DLMBillBrowserScreenDelegate

- (void)getGetRefKeyWebService:(DLMBillBrowserScreen *)billBrowserScreen successfullyFetchedRefKey:(NSString *)refKey {

    if(refKey)
        [self loadUrlInWebViewWithURL:_urlToBeLoaded andRefKey:refKey];
}


- (void)getRefKeyWebService:(DLMBillBrowserScreen *)billBrowserScreen failedToFetchRefKeyWithWebServiceError:(NLWebServiceError *)webServiceError {

    apiFaliureCounter = apiFaliureCounter + 1;
    if(apiFaliureCounter <=3)
    {
        [self fetchRefKey];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:kDefaultErrorMessage preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

            CATransition* transition = [CATransition animation];
            transition.duration = 0.3;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionReveal;
            transition.subtype = kCATransitionFromBottom;
            [self.navigationController.view.layer addAnimation:transition forKey:nil];

            [self.navigationController popViewControllerAnimated:NO];

        }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:^{}];
    }
}

@end
