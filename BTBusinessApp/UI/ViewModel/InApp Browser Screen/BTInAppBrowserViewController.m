//
//  BTInAppBrowserViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 12/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTInAppBrowserViewController.h"
#import "AppConstants.h"
#import "BTUICommon.h"
#import "BTCug.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CustomSpinnerView.h"
#import "AppManager.h"
#import "DLMInAppBrowserScreen.h"

@interface BTInAppBrowserViewController () <UIWebViewDelegate> {
    
    BOOL _opensExternally;
    BOOL _hasWebViewLoadFinished;
    BOOL _hasTheFirstPageBeenFinished;
    UIBarButtonItem *shareButton;
    NSString *currFileName;
    CustomSpinnerView *_loadingView;
}

@property (nonatomic, strong) IBOutlet UIWebView *webview;
@property (nonatomic, strong) IBOutlet UINavigationBar *navBar;
@property (nonatomic,strong) UIActivityIndicatorView *activityIndicatorView;

@property (nonatomic, strong) DLMInAppBrowserScreen *inAppBrowserViewModel;

@end

@implementation BTInAppBrowserViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    if (_opensExternally) {
        [AppManager openURL:_urlToBeLoaded];
        
        SEL action = NULL;
        if (self.navigationItem.leftBarButtonItem) {
            action = self.navigationItem.leftBarButtonItem.action;
        } else if (self.navigationItem.rightBarButtonItem) {
            action = self.navigationItem.rightBarButtonItem.action;
        }
        if (action) {
            [self performSelector:action];
        } else {
            [self dismissViewControllerAnimated:NO completion:^{
                //
            }];
        }
            
    }
    _smSession = [[BTAuthenticationManager sharedManager] smsessionForCurrentLoggedInUser];

    [BTUICommon updateNavigationBar:self.navigationController];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.webview.scalesPageToFit = YES;
    self.webview.keyboardDisplayRequiresUserAction = NO;
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:_urlToBeLoaded]];
    [request setValue:_smSession.smsessionString forHTTPHeaderField:@"SMSession"];
    if(!_isBillDownloading)
    {
        [self.webview loadRequest:request];
    }
    
    [self createLoadingView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(_isBillDownloading) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self downloadPDFInBackground];
        });
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    _isBillDownloading = NO;

    [self toggleLoadingItems:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public methods

+ (BTInAppBrowserViewController *)getInAppBrowserViewController {
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];
    BTInAppBrowserViewController *inAppBrowserViewController = (BTInAppBrowserViewController *)[storyboard instantiateViewControllerWithIdentifier:@"InAppBrowser"];
    return inAppBrowserViewController;
}

- (void)updateInAppBrowserWithIsPresentedModally:(BOOL)isPresentedModally isComingFromHomeScreen:(BOOL)isComingFromHomeScreen isNeedBackButton:(BOOL)isNeedBackButton andIsNeedCloseButton:(BOOL)isNeedCloseButton
{
    [self updateHeaderWithIsPresentedModally:isPresentedModally isComingFromHomeScreen:isComingFromHomeScreen isNeedBackButton:isNeedBackButton andIsNeedCloseButton:isNeedCloseButton];
    [self setTargetURLWithTargetURLType];
    
    _inAppBrowserViewModel = [[DLMInAppBrowserScreen alloc] init];
    
    [_inAppBrowserViewModel createTitleURLData];
}

- (void)updateHeaderWithIsPresentedModally:(BOOL)isPresentedModally isComingFromHomeScreen:(BOOL)isComingFromHomeScreen isNeedBackButton:(BOOL)isNeedBackButton andIsNeedCloseButton:(BOOL)isNeedCloseButton
{
    UIImage *backImage;
    if (isComingFromHomeScreen)
    {
        backImage = [UIImage imageNamed:@"Icon_Home_White.png"];
        [self createBackButtonWithImage:backImage isPresentedModally:isPresentedModally];
    }
    else
    {
        backImage = [UIImage imageNamed:@"Icon_Left_ArrowHead_White.png"];
        
        if (isNeedCloseButton)
        {
            [self createCloseButton];
        }
        
        if (isNeedBackButton)
        {
            [self createBackButtonWithImage:backImage isPresentedModally:isPresentedModally];
        }
    }
}

#pragma mark - Private methods

- (void)setTargetURLWithTargetURLType
{
    switch (_targetURLType)
    {
        case BTTargetURLTypeForgotUsername:
            _opensExternally = NO;
            _urlToBeLoaded = [NSString stringWithFormat:@"https://%@/%@", [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel], kForgotBtIdUrl];
            self.title = @"Forgot username";
            break;
            
        case BTTargetURLTypeForgotPassword:
            _opensExternally = NO;
            _urlToBeLoaded = [NSString stringWithFormat:@"https://%@/%@", [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel], kForgotPasswordUrl];
            self.title = @"Forgot password";
            break;
            
        case BTTargetURLTypeRegistration:
            _opensExternally = NO;
            _urlToBeLoaded = [NSString stringWithFormat:@"https://%@/%@", [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel], kRegistrationUrl];
            self.title = @"Register";
            break;
        
        case BTTargetURLTypeFaultsChatScreen:
            _opensExternally = YES;
            _urlToBeLoaded = kFaultsChatScreenUrl;
            self.title = @"Live chat";
            break;
        case BTTargetURLTypeCloudVoiceFaultsChatScreen:
            _opensExternally = YES;
            _urlToBeLoaded = kCloudVoiceFaultsChatScreenUrl;
            self.title = @"Live chat";
            break;
        case BTTargetURLTypeOrdersChatScreen:
            _opensExternally = YES;
            _urlToBeLoaded = kOrdersChatScreenUrl;
            self.title = @"Live chat";
            break;
            
        case BTTargetURLTypeBillingChatScreen:
            _opensExternally = YES;
            _urlToBeLoaded = kBillsChatScreenUrl;
            self.title = @"Live chat";
            break;
            
        case BTTargetURLTypeCloudVoiceExpressChatScreen:
            _opensExternally = YES;
            _urlToBeLoaded = kCloudVoiceExpressChatScreenUrl;
            self.title = @"Live chat";
            break;
            
        case BTTargetURLTypeGuestWifi:
            _opensExternally = YES;
            _urlToBeLoaded = kGuestWiFiOrderNowUrl;
            self.title = @"Guest Wi-Fi";
            break;
        
        case BTTargetURLTypeFAQChatScreen:
            _opensExternally = YES;
            _urlToBeLoaded = kFAQChatScreenUrl;
            self.title = @"Popular FAQs";
            break;
            
        case BTTargetURLTypeDownloadBill:
            _opensExternally = NO;
            self.title = @"Bill";
            break;
            
        case BTTargetURLTypeTermsAndConditions:
            _opensExternally = NO;
            _urlToBeLoaded = kTermsAndConditionsUrl;
            self.title = @"Terms and conditions";
            break;
            
        case BTTargetURLTypeTrackParcel:
            _opensExternally = YES;
            self.title = @"Track your parcel";
            break;
            
        case BTTargetURLTypeUpgradeHub:
            _opensExternally = YES;
            _urlToBeLoaded = kUpgradeYourHubUrl;
            self.title = @"Upgrade hub";
            break;
            
        case BTTargetURLType4GAssureOrder:
            _opensExternally = YES;
            _urlToBeLoaded = k4GAssureOrderNowUrl;
            self.title = @"4G Assure";
            break;
        // newly added
        case BTTargetURLTYPEReportAFaultBroadband:
            _opensExternally = YES;
            _urlToBeLoaded = kReportAFaultBroadBandUrl;
            self.title = @"Report a fault - Broadband";
            break;
        case BTTargetURLTYPEReportAFaultPhoneline:
            _opensExternally = YES;
            _urlToBeLoaded = kReportAFaultPhoneUrl;
            self.title = @"Report a fault - Phone";
            break;
        case BTTargetURLTYPEReportAFaultEmail:
            _opensExternally = YES;
            _urlToBeLoaded = kReportAFaultEmailUrl;
            self.title = @"Report a fault - Email";
            break;
        case BTTargetURLTYPEReportAFaultBTCloudVoice:
            _opensExternally = YES;
            _urlToBeLoaded = kReportAFaultBtCloudVoiceServicelUrl;
            self.title = @"Report a fault - BT cloud voice service";
            break;
            
        case BTTargetURLTypeReportAFault:
            _opensExternally = YES;
            _urlToBeLoaded = kReportFaultUrl;
            break;
        case  BTTargetURLTypeReportAFaultHomeScreen:
            _opensExternally = YES;
            _urlToBeLoaded = kReportaFaultHomeUrl;
            break;
        case BTTargetURLTypeWifiSpeedTips:
            _opensExternally = YES;
            _urlToBeLoaded = kWifiSpeedTipsUrl;
            self.title = @"Wi-fi speed tips";
            break;
            
        case BTTargetURLTypeBroadbandSpeedTips:
            _opensExternally = YES;
            _urlToBeLoaded = kBroadbandSpeedTipsUrl;
            self.title = @"Broadband speed tips";
            break;
            
        case BTTargetURLTypeSpeedTestGuarantee:
            _opensExternally = YES;
            _urlToBeLoaded = kSpeedTestGuaranteeUrl;
            self.title = @"BT Speed Guarantee";
            break;
            
        case BTTargetURLTypeSmartHub:
            _opensExternally = YES;
            _urlToBeLoaded = kSmartHubUrl;
            self.title = @"BT Business Smart Hub";
            
        default:
            break;
    }
}

- (void)updateTitleIfOpenedForUserJourneyWithURLString:(NSString *)urlString
{
    if (_inAppBrowserViewModel != nil)
    {
        BTUserJourneyType userJourneyType = [self currentJourneyType];
        
        NSString *titleString = [_inAppBrowserViewModel getTitleForURLString:urlString withJourneyType:userJourneyType];
        
        if (titleString != nil && ![titleString isEqualToString:@""])
        {
            self.title = titleString;
        }
    }
}

- (BTUserJourneyType)currentJourneyType
{
    if (_inAppBrowserViewModel != nil)
    {
        BTUserJourneyType userJourneyType;
        if (_targetURLType == BTTargetURLTypeRegistration)
        {
            userJourneyType = BTUserJourneyTypeRegistration;
        }
        else if (_targetURLType == BTTargetURLTypeForgotUsername)
        {
            userJourneyType = BTUserJourneyTypeForgotUsername;
        }
        else if (_targetURLType == BTTargetURLTypeForgotPassword)
        {
            userJourneyType = BTUserJourneyTypeForgotPassword;
        }
        else
        {
            userJourneyType = BTUserJourneyTypeUnknown;
        }
        
        return userJourneyType;
    }
    
    return BTUserJourneyTypeUnknown;
}

- (void)createBackButtonWithImage:(UIImage *)backButtonImage isPresentedModally:(BOOL)isPresentedModally
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 25, 26);
    [button setImage:backButtonImage forState:UIControlStateNormal];
    
    if (isPresentedModally)
    {
        [button addTarget:self action:@selector(closeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [button addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}

- (void)createCloseButton
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(50, 0, 50, 30);
    [button setTitle:@"Close" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(closeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}

- (void)createActivityIndicatorView
{
    _activityIndicatorView  = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _activityIndicatorView.color = [BrandColours colourBackgroundBTPurplePrimaryColor];
    _activityIndicatorView.center = CGPointMake(self.view.center.x, self.view.center.y-64);
    _activityIndicatorView.hidesWhenStopped = YES;
    [self.activityIndicatorView startAnimating];
    [self.view addSubview:_activityIndicatorView];
}

- (void)createLoadingView {
    
    if(!_loadingView) {
        
        _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [_loadingView.spinnerBackgroundView setBackgroundColor:[UIColor clearColor]];
        [_loadingView.loadingLabel setHidden:YES];
        [_loadingView setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:_loadingView];
    }
    
    _loadingView.hidden = NO;
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}

- (void)toggleLoadingItems:(BOOL)isHide {
    dispatch_async(dispatch_get_main_queue(), ^{
        if(isHide)
        {
            [self->_loadingView stopAnimatingLoadingIndicatorView];
            [self->_loadingView setHidden:isHide];
            self->_loadingView = nil;
        }
        else
        {
            [self createLoadingView];
            [self->_loadingView setHidden:isHide];
            [self->_loadingView startAnimatingLoadingIndicatorView];
        }
    });
}

- (void)deleteCookies
{
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
    
    for(NSHTTPCookie *cookie in cookies)
    {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Action Methods
- (void)closeButtonAction:(id)sender {
    [self deleteCookies];
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)share:(id)sender {
    NSString* downloadPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];

    NSString* str = [downloadPath stringByAppendingPathComponent:@"ShareBTBill.pdf"];

    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[[NSURL fileURLWithPath:str]] applicationActivities:nil];
    activityViewController.excludedActivityTypes = @[];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        activityViewController.popoverPresentationController.sourceView = self.view;
        activityViewController.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/4, 0, 0);
    }
    [self presentViewController:activityViewController animated:true completion:nil];
}


#pragma mark - WebView Delegate

- (BOOL)webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *urlString = [[request URL] absoluteString];
    
    //NSLog(@"%@", urlString);
    if((urlString && [urlString rangeOfString:@"LoginRedirect" options:NSCaseInsensitiveSearch].location != NSNotFound)){
        [self closeButtonAction:nil];
        return YES;
    }
    if((urlString == nil || [[urlString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""] || [[urlString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@"about:blank"] || ([urlString containsString:@"#"]) || [urlString containsString:@"javascript:void"]) && (_targetURLType != BTTargetURLTypeTrackParcel && _targetURLType != BTTargetURLTypeUnknown) )
    {
        [self updateTitleIfOpenedForUserJourneyWithURLString:urlString];
        return NO;
    }
    
    BOOL shouldLoad = YES;

    if(navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        shouldLoad = NO;
        
        BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
        
        if (_inAppBrowserViewModel != nil)
        {
            NSString *titleString = [_inAppBrowserViewModel getTitleForURLString:[[request URL] absoluteString] withJourneyType:[self currentJourneyType]];
            if (titleString)
            {
                viewController.title = titleString;
            }
            else
            {
                viewController.title = self.navigationItem.title;
            }
        }
        else
        {
            viewController.title = self.navigationItem.title;
        }
        
        viewController.urlToBeLoaded = [[request URL] absoluteString];
        viewController.targetURLType = BTTargetURLTypeUnknown;
        
        [viewController updateInAppBrowserWithIsPresentedModally:NO isComingFromHomeScreen:NO isNeedBackButton:YES andIsNeedCloseButton:YES];
        
        [self.navigationController pushViewController:viewController animated:YES];
        return shouldLoad;
    }
    
    [self updateTitleIfOpenedForUserJourneyWithURLString:urlString];
    [self toggleLoadingItems:NO];
    return shouldLoad;
}




- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if(!_isBillDownloading)
    _hasTheFirstPageBeenFinished = YES;
    
    [self toggleLoadingItems:YES];

}

- (void)webView:(UIWebView *)webView
didFailLoadWithError:(NSError *)error
{

    [self toggleLoadingItems:YES];
    
}

- (void)downloadPDFInBackground {
    NSURL *testUrl = [NSURL URLWithString:self.urlToBeLoaded];
    NSString *query = testUrl.query;
    NSArray *queryArray = [query componentsSeparatedByString:@"&"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", @"url"];
    id urlstring = [[queryArray filteredArrayUsingPredicate:predicate] firstObject];
    NSString *absoluteURLString = [[urlstring componentsSeparatedByString:@"="] lastObject];
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:absoluteURLString]];
    
    [urlRequest setValue:@"application/octet-stream" forHTTPHeaderField:@"content-type"];
    [urlRequest setValue:[NSString stringWithFormat:@"SMSESSION=%@", _smSession.smsessionString] forHTTPHeaderField:@"Cookie"];
    [self toggleLoadingItems:NO];
    NSURLSessionDownloadTask *testDataTask = [[NSURLSession sharedSession] downloadTaskWithRequest:urlRequest completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSData *downloadedData = [NSData dataWithContentsOfURL:location];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentDirectory = [paths objectAtIndex:0];
        NSString *dataPath = [documentDirectory stringByAppendingPathComponent:@"ShareBTBill.pdf"];
       [downloadedData writeToFile:dataPath atomically:YES];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self loadPDFInWebview];
        });
    }];
    [testDataTask resume];
}

- (void)loadPDFInWebview
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentDirectory stringByAppendingPathComponent:@"ShareBTBill.pdf"];
    
    NSData *data = [[NSFileManager defaultManager] contentsAtPath:dataPath];
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    CGPDFDocumentRef document = CGPDFDocumentCreateWithProvider(provider);
    
    [self toggleLoadingItems:YES];
    
    if (document == nil) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:kDefaultErrorMessage preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        shareButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(share:)];
        self.navigationItem.rightBarButtonItem = shareButton;
        [self.webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:dataPath]]];
    }
    CGDataProviderRelease(provider);
    CGPDFDocumentRelease(document);
}


@end
