//
//  BTNavigationViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 9/5/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTNavigationViewController.h"

@interface BTNavigationViewController ()

@end

@implementation BTNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion
{
    if ( self.presentedViewController)
    {
        [super dismissViewControllerAnimated:flag completion:completion];
    }
}

@end
