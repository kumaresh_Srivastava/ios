//
//  DLMInAppBrowserScreen.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 9/11/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "DLMInAppBrowserScreen.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"

@interface DLMInAppBrowserScreen ()

@property (nonatomic, strong) NSDictionary *titleURLDictionary;

@end

@implementation DLMInAppBrowserScreen

- (void)createTitleURLData
{
    NSMutableDictionary *titleDict = [NSMutableDictionary dictionary];
    [titleDict setValue:@"Register" forKey:@"/registration/retrieve"];
    [titleDict setValue:@"Email address" forKey:@"/registration/retrieve#email"];
    [titleDict setValue:@"Billing accounts" forKey:@"/registration/retrieve#billingaccountsvalidation"];
    [titleDict setValue:@"Billing accounts" forKey:@"/registration/retrieve#registrationinterceptforgroups"];
    [titleDict setValue:@"Billing accounts" forKey:@"/registration/retrieve#registrationinterceptwithbacs"];
    [titleDict setValue:@"My details" forKey:@"/registration/retrieve#mydetails"];
    [titleDict setValue:@"My details" forKey:@"/registration/retrieve#showsecurepasswordsection"];
    [titleDict setValue:@"Terms & conditions" forKey:@"/registration/retrieve#termsandconditions"];
    [titleDict setValue:@"Complete" forKey:@"/registration/retrieve#completion"];
    
    [titleDict setValue:@"Register" forKey:@"/registration/retrieve"];
    [titleDict setValue:@"Email address" forKey:@"/registration/retrieve#/email"];
    [titleDict setValue:@"Billing accounts" forKey:@"/registration/retrieve#/billingaccountsvalidation"];
    [titleDict setValue:@"Billing accounts" forKey:@"/registration/retrieve#/registrationinterceptforgroups"];
    [titleDict setValue:@"Billing accounts" forKey:@"/registration/retrieve#/registrationinterceptwithbacs"];
    [titleDict setValue:@"My details" forKey:@"/registration/retrieve#/mydetails"];
    [titleDict setValue:@"My details" forKey:@"/registration/retrieve#/showsecurepasswordsection"];
    [titleDict setValue:@"Terms & conditions" forKey:@"/registration/retrieve#/termsandconditions"];
    [titleDict setValue:@"Complete" forKey:@"/registration/retrieve#/completion"];
    
//    [titleDict setValue:@"Forgot username" forKey:@"/forgotusername/retrieve"];
//    [titleDict setValue:@"Password" forKey:@"/forgotusername/retrieve#funpasswordstep"];
//    [titleDict setValue:@"Password" forKey:@"/forgotusername/retrieve#funsecquesstep"];
//    [titleDict setValue:@"Confirmation" forKey:@"/forgotusername/retrieve#funconfirmationstep"];
//
//    [titleDict setValue:@"Forgot username" forKey:@"/forgotusername/retrieve"];
//    [titleDict setValue:@"Password" forKey:@"/forgotusername/retrieve#/funpasswordstep"];
//    [titleDict setValue:@"Password" forKey:@"/forgotusername/retrieve#/funsecquesstep"];
//    [titleDict setValue:@"Confirmation" forKey:@"/forgotusername/retrieve#/funconfirmationstep"];

    [titleDict setValue:@"Forgot username" forKey:@"/forgotusername/retrieve"];
    [titleDict setValue:@"Forgot username" forKey:@"/forgotusername/retrieve#funpasswordstep"];
    [titleDict setValue:@"Forgot username" forKey:@"/forgotusername/retrieve#funsecquesstep"];
    [titleDict setValue:@"Forgot username" forKey:@"/forgotusername/retrieve#funconfirmationstep"];
    
    [titleDict setValue:@"Forgot username" forKey:@"/forgotusername/retrieve"];
    [titleDict setValue:@"Forgot username" forKey:@"/forgotusername/retrieve#/funpasswordstep"];
    [titleDict setValue:@"Forgot username" forKey:@"/forgotusername/retrieve#/funsecquesstep"];
    [titleDict setValue:@"Forgot username" forKey:@"/forgotusername/retrieve#/funconfirmationstep"];
    
    [titleDict setValue:@"Forgot password" forKey:@"/forgotpassword/retrieve"];
    [titleDict setValue:@"Forgot password" forKey:@"/forgotpassword/retrieve#fpwsecquesstep"];
    [titleDict setValue:@"Forgot password" forKey:@"/forgotpassword/retrieve#fpwenterpinstep"];
    [titleDict setValue:@"Forgot password" forKey:@"/forgotpassword/retrieve#fpwnewpasswordstep"];
    [titleDict setValue:@"Forgot password" forKey:@"/forgotpassword/retrieve#fpwconfirmationstep"];
    
    [titleDict setValue:@"Forgot password" forKey:@"/forgotpassword/retrieve"];
    [titleDict setValue:@"Forgot password" forKey:@"/forgotpassword/retrieve#/fpwsecquesstep"];
    [titleDict setValue:@"Forgot password" forKey:@"/forgotpassword/retrieve#/fpwenterpinstep"];
    [titleDict setValue:@"Forgot password" forKey:@"/forgotpassword/retrieve#/fpwnewpasswordstep"];
    [titleDict setValue:@"Forgot password" forKey:@"/forgotpassword/retrieve#/fpwconfirmationstep"];
    
    _titleURLDictionary = [[NSDictionary alloc] initWithDictionary:titleDict];
}

- (NSString *)getTitleForURLString:(NSString *)urlString withJourneyType:(BTUserJourneyType)userJourneyType
{
    NSString *baseUrlString = [NSString stringWithFormat:@"https://%@", [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel]];
    NSString *trailingUrlString = [[urlString stringByReplacingOccurrencesOfString:baseUrlString withString:@""] lowercaseString];
    
    NSString *titleToShow = [[[_titleURLDictionary valueForKey:trailingUrlString] validAndNotEmptyStringObject] copy];
    
    if (titleToShow == nil || [titleToShow isEqualToString:@""])
    {
        switch (userJourneyType)
        {
            case BTUserJourneyTypeRegistration:
                
                return @"Register";
                
            case BTUserJourneyTypeForgotPassword:
                
                return @"Forgot password";
                
            case BTUserJourneyTypeForgotUsername:
                
                return @"Forgot username";
                
            default:
                break;
        }
    }
    
    return titleToShow;
}

@end
