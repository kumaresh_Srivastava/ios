//
//  BTInAppBrowserViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 12/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTAuthenticationManager.h"
#import "BTSMSession.h"

typedef NS_ENUM(NSInteger, BTTargetURLType) {
    BTTargetURLTypeForgotUsername,
    BTTargetURLTypeForgotPassword,
    BTTargetURLTypeRegistration,
    BTTargetURLTypeBillingChatScreen,
    BTTargetURLTypeFaultsChatScreen,
    BTTargetURLTypeCloudVoiceFaultsChatScreen,
    BTTargetURLTypeOrdersChatScreen,
    BTTargetURLTypeCloudVoiceExpressChatScreen,
    BTTargetURLTypeFAQChatScreen,
    BTTargetURLTypeDownloadBill,
    BTTargetURLTypeTermsAndConditions,
    BTTargetURLTypeTrackParcel,
    BTTargetURLTypeUnknown,
    BTTargetURLType4GAssureOrder,
    BTTargetURLTypeUpgradeHub,
    BTTargetURLTypeSmartHub,
    BTTargetURLTypeReportAFault,
    BTTargetURLTypeReportAFaultHomeScreen,
    BTTargetURLTypeGuestWifi,
    BTTargetURLTypeSpeedTestGuarantee,
    BTTargetURLTypeBroadbandSpeedTips,
    BTTargetURLTypeWifiSpeedTips,
    BTTargetURLTYPEReportAFaultBroadband,
    BTTargetURLTYPEReportAFaultPhoneline,
    BTTargetURLTYPEReportAFaultEmail,
    BTTargetURLTYPEReportAFaultBTCloudVoice
};


@interface BTInAppBrowserViewController : UIViewController<NSURLConnectionDataDelegate> {

    BTSMSession *_smSession;
}

@property (nonatomic, copy) NSString *urlToBeLoaded;
@property (nonatomic, strong) NSString *titleString;
@property (nonatomic, assign) BOOL isBillDownloading;
@property (nonatomic, assign) BTTargetURLType targetURLType;

+ (BTInAppBrowserViewController *)getInAppBrowserViewController;
- (void)updateInAppBrowserWithIsPresentedModally:(BOOL)isPresentedModally isComingFromHomeScreen:(BOOL)isComingFromHomeScreen isNeedBackButton:(BOOL)isNeedBackButton andIsNeedCloseButton:(BOOL)isNeedCloseButton;

@end
