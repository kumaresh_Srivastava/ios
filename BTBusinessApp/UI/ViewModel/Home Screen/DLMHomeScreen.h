//
//  DLMHomeScreen.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 13/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"


typedef enum{
    HomeGridViewStateUnkown,
    HomeGridViewStateProgress,
    HomeGridViewStateFailled,
    HomeGridViewStateCompleted,
    HomeGridViewStateCompletedZero,
    InvalidState
}HomeGridViewState;


typedef enum{
    InterceptorTypeUnknown,
    InterceptorTypeFirstTimeLogin,
    InterceptorTypeCreatePassword,
    InterceptorTypeUpdateProfile,
    InterceptorTypeUpdateContact,
    IntercepterType1FA
}InterceptorType;



#define kTrackOrdersText @"Track orders"
#define kBillsText @"Billing"//@"Bills"  @Manik Fix
#define kServiceStatusText @"Service status"
#define kTrackFaultsText @"Track faults"
#define kSettingsText @"Settings"
#define kAccountText @"Account"
#define kUsageText @"Usage"
#define kHelpText @"Help"
#define kPublicWiFiText @"Public wi-fi"
#define kBroadbandSpeedTest @"Broadband speed test"
#define kReportaFault @"Report a fault"


#define kFirstTimeLogin @"FirstTimeLogin"
#define kCreatePassword @"ResetPassword"
#define kUpdateContact @"UpdateContact"
#define kUpdateProfile @"UpdateProfile"
#define kUpdate1FA @"OneFA"



@class DLMHomeScreen;
@class BTUser;
@class BTCug;
@class HomeGridViewModel;
@class NLWebServiceError;

@protocol DLMHomeScreenDelegate <NSObject>

- (void)successfullyFetchedUserDataOnHomeScreen:(DLMHomeScreen *)homeScreen;

- (void)homeScreen:(DLMHomeScreen *)homeScreen failedToFetchUserDataWithWebServiceError:(NLWebServiceError *)error;

- (void)homeScreen:(DLMHomeScreen *)homeScreen didUpdateDataForGridViewModel:(HomeGridViewModel *)gridViewModel;

- (void)homeScreen:(DLMHomeScreen *)homeScreen failedToFetchGridDataWithWebServiceError:(NLWebServiceError *)webServiceError;

- (void)homeScreen:(DLMHomeScreen *)homeScreen hasPendingIntercenptorWithType:(InterceptorType)interceptorType;

- (void)successfullyFetchedNotificationDataOnHomeScreen:(DLMHomeScreen *)homeScreen;

- (void)homeScreen:(DLMHomeScreen *)homeScreen failedToFetchNotificationsWithWebServiceError:(NLWebServiceError *)webServiceError;

- (void)successfullyParsedSatusMatrixForHomeScreen:(NSString *)statusMatrix;


@end

@interface DLMHomeScreen : DLMObject {

    NSArray *_gridModelArray;
    NSArray *_bottomGridModelArray;
    NSArray *_bottomGridButtonModelArray;
    NSMutableArray *_interceptorArray;
}

@property (nonatomic, readonly) BTUser *user;
@property (nonatomic, copy) NSArray *arrayOfNotifications;
@property (nonatomic, weak) id <DLMHomeScreenDelegate> homeScreenDelegate;

- (void)fetchUserDetails;
- (void)takeActionOnPrefetchedUserDetails:(NSDictionary *)userDetails;
- (void)changeSelectedCUGTo:(BTCug *)selectedCug;
- (void)fetchUserGroupNotifications;
- (void)cancelGetUserAccountNotificationsAPICall;
- (void)fetchBillPaymentHistoryData;
- (void)fetchGridData;
- (NSArray *)gridModelArray;
- (NSArray *)bottomGridModelArray;
- (NSArray *)bottomGridButtonModelArray;
- (void)cancelAPIs;
- (void)checkForInterceptor;
- (void)makeUpdateContactInterceptorAPIRequestWithContactArray:(NSArray *)contactArray;
- (NSDictionary *)getOrdersDataDictionary;
- (NSDictionary *)getFaultsDataDictionary;
- (NSDictionary *)getBillsDataDictionary;

@end


@interface HomeGridViewModel : NSObject
@property(nonatomic, copy) NSString *titleText;
@property(nonatomic, copy) NSString *valueText;
@property(nonatomic, strong) UIImage *iconImage;
@property(nonatomic, strong) UIColor *valueBackgroudColor;
@property(nonatomic, assign) HomeGridViewState gridSate;
@property(nonatomic, assign) NSInteger count;

@end
