//
//  DLMHomeScreen.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 13/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMHomeScreen.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "CDAuthenticationToken.h"
#import "AppDelegateViewModel.h"
#import "AppDelegate.h"
#import <SAMKeychain/SAMKeychain.h>
#import "BTUser.h"
#import "BTCug.h"
#import "AppConstants.h"
#import "NLFaultDashBoardWebService.h"
#import "NLTrackOrderDashBoardWebService.h"
#import "NLGetBillPaymentHistoryWebService.h"
#import "BrandColours.h"
#import "NSObject+APIResponseCheck.h"
#import "NLGetNotificationsWebService.h"
#import "BTAuthenticationToken.h"
#import "BTAuthenticationManager.h"
#import "NLQueryUserDetailsWebService.h"
#import "NLWebServiceError.h"
#import "DLMUpdateContactInerceptScreen.h"

#import "NLGetProjectsByCUG.h"
#import "BTOCSProject.h"

#define kMaxNumOfTries 3

@interface DLMHomeScreen () <NLFaultDashBoardWebServiceDelegate, NLTrackOrderDashBoardWebServiceDelegate, NLGetBillPaymentHistoryWebServiceDelegate, NLGetNotificationsWebServiceDelegate, NLQueryUserDetailsWebServiceDelegate, DLMUpdateContactInerceptScreenDelegate, NLAPIGEEAuthenticationProtectedWebServiceDelegate> {
    
    
    BOOL _isTrackFaultAPICompleted;
    BOOL _isTrackOrderAPICompleted;
    BOOL _isBillsAPICompleted;
    BOOL _isOCSAPICompleted;
    
    NSString *_faultStatusMatrix;
    NSString *_orderStatusMatrix;
    NSString *_billStatusMatrix;
    
    NSMutableArray *updateContactInterceptorArray; //RLM
    NSInteger interceptorNumOfTries;

    //sal
    NSMutableDictionary *_orderDataDictionary;
    NSDictionary *_faultDataDictionary;
    NSDictionary *_billDataDictionary;
}

@property (nonatomic) NLQueryUserDetailsWebService *queryUserDetailsWebService;
@property (nonatomic) NLFaultDashBoardWebService *getFaultDashBoardWebService;
@property (nonatomic) NLTrackOrderDashBoardWebService *getOrderDashBoardWebService;
@property (nonatomic) NLGetBillPaymentHistoryWebService *getBillPaymentHistoryWebService;
@property (nonatomic) NLGetNotificationsWebService *getNotificationsWebService;
@property (nonatomic) DLMUpdateContactInerceptScreen *updateContactInterceptorModel;

@property (nonatomic) NLGetProjectsByCUG *getProjectsByCUGWebService;

@end

@implementation DLMHomeScreen

#pragma mark - Public Methods

- (void)setupToFetchUserDetails
{
    // Get the currently logged in User
    CDUser *user = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    
    _queryUserDetailsWebService.queryUserDetailsWebServiceDelegate = nil;
    _queryUserDetailsWebService = [[NLQueryUserDetailsWebService alloc] initWithUsername:user.username];
    _queryUserDetailsWebService.queryUserDetailsWebServiceDelegate = self;
}

- (void)fetchUserDetails
{
    [self setupToFetchUserDetails];
    [self fetchUserDetailsForLoggedInUser];
}

- (void)fetchUserDetailsForLoggedInUser
{
    [_queryUserDetailsWebService resume];
}

- (void)takeActionOnPrefetchedUserDetails:(NSDictionary *)userDetails
{
    BTUser *user = [userDetails valueForKey:@"user"];
    NSString *order = [userDetails valueForKey:@"order"];
    NSString *interfaces = [userDetails valueForKey:@"interfaces"];
    
    [self saveUserDataInPersistenceWithUser:user];
    NSString *groupKey;
    if ([user.arrayOfCugs count] > 0)
    {
        groupKey = [(BTCug *)[user.arrayOfCugs objectAtIndex:0] groupKey];
    }
    [self saveInterceptorWithOrder:order andInterfaces:interfaces];
    [self takeActionAfterFetchingUserDetailsWithGroup:groupKey];
}

- (void)setupToFetchUserGroupNotifications
{
    self.getNotificationsWebService.getNotificationsWebServiceDelegate = nil;
    self.getNotificationsWebService = [[NLGetNotificationsWebService alloc] init];
    self.getNotificationsWebService.getNotificationsWebServiceDelegate = self;
}

- (void)fetchUserGroupNotifications
{
    [self setupToFetchUserGroupNotifications];
    [self fetchUserNotifications];
}

- (void)fetchUserNotifications
{
    [self.getNotificationsWebService resume];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"APICallForGetNotificationsStartedNotification" object:nil];
}

- (void)cancelGetUserAccountNotificationsAPICall
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"APICallForGetNotificationsCancelledNotification" object:nil];

    self.getNotificationsWebService.getNotificationsWebServiceDelegate = nil;
    [self.getNotificationsWebService cancel];
    self.getNotificationsWebService = nil;
}

- (void)changeSelectedCUGTo:(BTCug *)selectedCug
{
    DDLogInfo(@"Saving %@ as selected cug into persistence.", selectedCug);

    NSManagedObjectContext *context = [AppDelegate sharedInstance].managedObjectContext;
    CDCug *newPersistenceCug = [CDCug newCugInManagedObjectContext:context];
    newPersistenceCug.groupKey = selectedCug.groupKey;
    newPersistenceCug.refKey = selectedCug.refKey;
    newPersistenceCug.contactId = selectedCug.contactId;
    newPersistenceCug.cugName = selectedCug.cugName;
    newPersistenceCug.cugId = selectedCug.cugID;
    newPersistenceCug.cugRole = @(selectedCug.cugRole);
    newPersistenceCug.indexInAPIResponse = @(selectedCug.indexInAPIResponse);

    // Get the currently logged in User
    CDUser *loggedInUser = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    CDCug *previouslySelectedCug = loggedInUser.currentSelectedCug;
    loggedInUser.currentSelectedCug = newPersistenceCug;

    if(previouslySelectedCug)
    {
        [context deleteObject:previouslySelectedCug];
    }

    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];


    // Create immutable BTUser Object from persistence
    BTUser *modifiedUser = [[BTUser alloc] initWithCDUser:loggedInUser securityQuestion:self.user.securityQuestion securityAnswer:self.user.securityAnswer mobileNumber:self.user.mobileNumber landlineNumber:self.user.landlineNumber primaryEmail:self.user.primaryEmailAddress contactID:self.user.contactId  andAlternativeEmail:self.user.alternativeEmailAddress];
    
    _user = modifiedUser;
}

- (void)fetchGridData{

    [self fetchOrderDashBoardDetailsForLoggedInUser];
    [self fetchFaultDashBoardDetailsForLoggedInUser];
    [self fetchBillPaymentHistoryData];
}


- (void)cancelAPIs
{
    [self cancelFaultDashboardAPICall];
    [self cancelOrderDashboardAPICall];
    [self cancelBillDashboardAPICall];
}

- (void)cancelFaultDashboardAPICall
{
    self.getFaultDashBoardWebService.getFaultDashBoardWebServiceDelegate = nil;
    [self.getFaultDashBoardWebService cancel];
    self.getFaultDashBoardWebService = nil;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"APICallForOpenFaultsCancelledAtHomeScreenNotification" object:nil];
}

- (void)cancelOrderDashboardAPICall
{
    self.getOrderDashBoardWebService.getTrackOrderDashBoardWebServiceDelegate = nil;
    [self.getOrderDashBoardWebService cancel];
    self.getOrderDashBoardWebService = nil;
    
    self.getProjectsByCUGWebService.delegate = nil;
    [self.getProjectsByCUGWebService cancel];
    self.getProjectsByCUGWebService = nil;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"APICallForActiveOrdersCancelledAtHomeScreenNotification" object:nil];
}

- (void)cancelBillDashboardAPICall
{
    self.getBillPaymentHistoryWebService.getBillPaymentHistoryWebServiceDelegate = nil;
    [self.getBillPaymentHistoryWebService cancel];
    self.getBillPaymentHistoryWebService = nil;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"APICallForReadyBillsCancelledAtHomeScreenNotification" object:nil];
}

- (void)setupToFetchOrderDashboardData
{
    self.getOrderDashBoardWebService.getTrackOrderDashBoardWebServiceDelegate = nil;
    self.getOrderDashBoardWebService = [[NLTrackOrderDashBoardWebService alloc] initWithPageSize:3 pageIndex:1 andTabID:1];
    self.getOrderDashBoardWebService.getTrackOrderDashBoardWebServiceDelegate = self;
    
    self.getProjectsByCUGWebService.delegate = nil;
    //NSString *cugName = [[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSString *cugId = [[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugId stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    //self.getProjectsByCUGWebService = [[NLGetProjectsByCUG alloc] initWithCUG:cugName];
    self.getProjectsByCUGWebService = [[NLGetProjectsByCUG alloc] initWithCUGID:cugId];
    self.getProjectsByCUGWebService.delegate = self;
    
    if (!_orderDataDictionary) {
        _orderDataDictionary = [NSMutableDictionary new];
    } else {
        [_orderDataDictionary removeAllObjects];
    }
    
    _isTrackOrderAPICompleted = NO;
    _isOCSAPICompleted = NO;
}

- (void)fetchOrderDashBoardDetailsForLoggedInUser
{
    [self setupToFetchOrderDashboardData];
    [self fetchOrderDashBoardData];
}

- (void)fetchOrderDashBoardData
{
    [self.getOrderDashBoardWebService resume];
    [self.getProjectsByCUGWebService resume];
    
    //_orderDataDictionary = nil;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"APICallForActiveOrdersStartedAtHomeScreenNotification" object:nil];
}

- (void)setupToFetchFaultDashboardData
{
    self.getFaultDashBoardWebService.getFaultDashBoardWebServiceDelegate = nil;
    self.getFaultDashBoardWebService = [[NLFaultDashBoardWebService alloc] initWithPageSize:3
                                                                                  pageIndex:1
                                                                                   andTabID:TrackFaultDashBoardTypeOpenOrder];
    self.getFaultDashBoardWebService.getFaultDashBoardWebServiceDelegate = self;
}

- (void)fetchFaultDashBoardDetailsForLoggedInUser
{
    [self setupToFetchFaultDashboardData];
    [self fetchFaultDashBoardData];
}

- (void)fetchFaultDashBoardData
{
    [self.getFaultDashBoardWebService resume];
    
    _faultDataDictionary = nil;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"APICallForOpenFaultsStartedAtHomeScreenNotification" object:nil];
}

- (void)setupToFetchBillDashboardData
{
    self.getBillPaymentHistoryWebService.getBillPaymentHistoryWebServiceDelegate = nil;
    self.getBillPaymentHistoryWebService = [[NLGetBillPaymentHistoryWebService alloc] init];
    self.getBillPaymentHistoryWebService.getBillPaymentHistoryWebServiceDelegate = self;
}

- (void)fetchBillPaymentHistoryData
{
    [self setupToFetchBillDashboardData];
    [self fetchBillDashBoardData];
}

- (void)fetchBillDashBoardData
{
    [self.getBillPaymentHistoryWebService resume];
    
    _billDataDictionary = nil;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"APICallForReadyBillsStartedAtHomeScreenNotification" object:nil];
}

- (NSArray *)gridModelArray{
    
    
    NSMutableArray *gridsArray = [NSMutableArray array];
    
    //Bills
    HomeGridViewModel *bills = [[HomeGridViewModel alloc] init];
    bills.titleText = kBillsText;
    bills.valueText = @"1 new";
    bills.iconImage = [UIImage imageNamed:@"bills"];
    bills.gridSate = HomeGridViewStateProgress;
    [gridsArray addObject:bills];
    
    //Track Order
    HomeGridViewModel *trackOrder = [[HomeGridViewModel alloc] init];
    trackOrder.titleText = kTrackOrdersText;
    trackOrder.valueText = @"1 active";
    trackOrder.iconImage = [UIImage imageNamed:@"track_order"];
    trackOrder.gridSate = HomeGridViewStateProgress;
    [gridsArray addObject:trackOrder];
    
    //Service status
    HomeGridViewModel *serviceStatus = [[HomeGridViewModel alloc] init];
    serviceStatus.titleText = kServiceStatusText;
    serviceStatus.valueText = @"Good";
    serviceStatus.iconImage = [UIImage imageNamed:@"service_status-1"];
    serviceStatus.gridSate = HomeGridViewStateFailled;
    serviceStatus.valueBackgroudColor = [BrandColours colourMyBtGreen];
    [gridsArray addObject:serviceStatus];
    
    //  Broadband speed test
    HomeGridViewModel *broadbandSpeedTest = [[HomeGridViewModel alloc] init];
    broadbandSpeedTest.titleText = kBroadbandSpeedTest;
    broadbandSpeedTest.valueText = @"Good";
    broadbandSpeedTest.iconImage = [UIImage imageNamed:@"service_status-1"];
    broadbandSpeedTest.gridSate = HomeGridViewStateFailled;
    broadbandSpeedTest.valueBackgroudColor = [BrandColours colourMyBtGreen];
    [gridsArray addObject:broadbandSpeedTest];
    
    //Report a fault
    HomeGridViewModel *reportAFault = [[HomeGridViewModel alloc] init];
    reportAFault.titleText = kReportaFault;
    reportAFault.valueText = @"";
    reportAFault.iconImage = [UIImage imageNamed:@"service_status-1"];
    reportAFault.gridSate = HomeGridViewStateFailled;
    reportAFault.valueBackgroudColor = [BrandColours colourMyBtGreen];
    [gridsArray addObject:reportAFault];
    
    //Track faults
    HomeGridViewModel *tarckFault = [[HomeGridViewModel alloc] init];
    tarckFault.titleText = kTrackFaultsText;
    tarckFault.valueText = @"1 open";
    tarckFault.iconImage = [UIImage imageNamed:@"track_fault"];
    tarckFault.gridSate = HomeGridViewStateProgress;
    [gridsArray addObject:tarckFault];
    
    //Public wi-fi
    HomeGridViewModel *publicWifi = [[HomeGridViewModel alloc] init];
    publicWifi.titleText = kPublicWiFiText;
    publicWifi.valueText = @"";
    publicWifi.iconImage = [UIImage imageNamed:@"pubclic_wifi"];
    publicWifi.gridSate = HomeGridViewStateFailled;
    publicWifi.valueBackgroudColor = [BrandColours colourMyBtGreen];
    [gridsArray addObject:publicWifi];
    
    _gridModelArray = gridsArray;
    
    //Check for internet connectivity, if network not availale then failed state as default state
    if(![AppManager isInternetConnectionAvailable])
    {
        trackOrder.gridSate = HomeGridViewStateFailled;
        tarckFault.gridSate = HomeGridViewStateFailled;
        bills.gridSate = HomeGridViewStateFailled;
    }
    
    return _gridModelArray;
}


- (NSArray *)bottomGridModelArray
{
    if(!_bottomGridModelArray || [_bottomGridModelArray count] == 0){
        
        
        NSMutableArray *bottomGridsArray = [NSMutableArray array];
        
        //Account
        HomeGridViewModel *account = [[HomeGridViewModel alloc] init];
        account.titleText = kAccountText;
        account.valueText = @"";
        account.iconImage = [UIImage imageNamed:@"account"];
        account.gridSate = InvalidState;
        [bottomGridsArray addObject:account];
        
        
        
        //Usage
        HomeGridViewModel *usage = [[HomeGridViewModel alloc] init];
        usage.titleText = kUsageText;
        usage.valueText = @"";
        usage.iconImage = [UIImage imageNamed:@"usage"];
        usage.gridSate = InvalidState;
        [bottomGridsArray addObject:usage];
        
        
        //Help
        HomeGridViewModel *help = [[HomeGridViewModel alloc] init];
        help.titleText = kHelpText;
        help.valueText = @"";
        help.iconImage = [UIImage imageNamed:@"help"];
        help.gridSate = InvalidState;
        [bottomGridsArray addObject:help];
        
        _bottomGridModelArray = bottomGridsArray;
        
    }
    
    
    return _bottomGridModelArray;
}

- (NSArray *)bottomGridButtonModelArray
{
    if (!_bottomGridButtonModelArray || _bottomGridButtonModelArray.count == 0) {
        NSMutableArray *bottomGridTextButtonArray = [NSMutableArray arrayWithCapacity:4];
        
        //Help
        HomeGridViewModel *help = [[HomeGridViewModel alloc] init];
        help.titleText = kHelpText;
        help.valueText = @"";
        help.iconImage = [UIImage imageNamed:@"help"];
        help.gridSate = InvalidState;
        [bottomGridTextButtonArray addObject:help];
        
        //Usage
        HomeGridViewModel *usage = [[HomeGridViewModel alloc] init];
        usage.titleText = kUsageText;
        usage.valueText = @"";
        usage.iconImage = [UIImage imageNamed:@"usage"];
        usage.gridSate = InvalidState;
        [bottomGridTextButtonArray addObject:usage];
        
        //Account
        HomeGridViewModel *account = [[HomeGridViewModel alloc] init];
        account.titleText = kAccountText;
        account.valueText = @"";
        account.iconImage = [UIImage imageNamed:@"account"];
        account.gridSate = InvalidState;
        [bottomGridTextButtonArray addObject:account];
        
        //Public wi-fi
        HomeGridViewModel *publicWifi = [[HomeGridViewModel alloc] init];
        help.titleText = kPublicWiFiText;
        help.valueText = @"";
        help.iconImage = [UIImage imageNamed:@"pubclic_wifi"];
        help.gridSate = InvalidState;
        [bottomGridTextButtonArray addObject:publicWifi];
        
        _bottomGridButtonModelArray = bottomGridTextButtonArray;
    }
    return _bottomGridButtonModelArray;
}



- (void)updateGridViewModelForFailureWithTitle:(NSString *)titleString{
    
    
    HomeGridViewModel *correspondingModel  = nil;
    
    for(HomeGridViewModel *gridModel in _gridModelArray){
        
        if([gridModel.titleText isEqualToString:titleString]){
            
            correspondingModel = gridModel;
            break;
        }
    }
    
    correspondingModel.gridSate = HomeGridViewStateFailled;
    [self.homeScreenDelegate homeScreen:self didUpdateDataForGridViewModel:correspondingModel];
    
}


- (void)updateGridViewModelWithSize:(int)size title:(NSString *)titleString andTextState:(NSString *)textState{
    
    
    HomeGridViewModel *correspondingModel  = nil;
    
    for(HomeGridViewModel *gridModel in _gridModelArray){
        
        if([gridModel.titleText isEqualToString:titleString]){
            
            correspondingModel = gridModel;
            break;
        }
    }
    
    correspondingModel.valueText = [NSString stringWithFormat:@"%d", size];
    correspondingModel.gridSate = HomeGridViewStateCompleted;
    correspondingModel.count = size;
    if(size > 0){
        
        correspondingModel.valueBackgroudColor = [BrandColours colourMyBtLightRed];
    }
    else{
        
        correspondingModel.valueBackgroudColor = [UIColor clearColor];
        correspondingModel.gridSate = HomeGridViewStateCompletedZero;
    }
    
    [self.homeScreenDelegate homeScreen:self didUpdateDataForGridViewModel:correspondingModel];
    
}


- (void)checkForInterceptor{

    InterceptorType interceptorType = InterceptorTypeUnknown;
    
    if(_interceptorArray && [_interceptorArray count] > 0){
        
        NSString *interceptorName = [_interceptorArray firstObject];
        interceptorType = [self interceptorTypeForIntercentorName:interceptorName];
        [_interceptorArray removeObject:interceptorName];
    }
    
    
    [self.homeScreenDelegate homeScreen:self hasPendingIntercenptorWithType:interceptorType];
}


- (InterceptorType)interceptorTypeForIntercentorName:(NSString *)interceptorName{
    
    InterceptorType interceptorType = InterceptorTypeUnknown;
    
    if([[interceptorName lowercaseString] isEqualToString:[kFirstTimeLogin lowercaseString]]){
        
        interceptorType = InterceptorTypeFirstTimeLogin;
    }
    else if([[interceptorName lowercaseString] isEqualToString:[kCreatePassword lowercaseString]]){
        
        interceptorType = InterceptorTypeCreatePassword;
    }
    else if([[interceptorName lowercaseString] isEqualToString:[kUpdateContact lowercaseString]]){
        
        interceptorType = InterceptorTypeUpdateContact;
    }
    else if([[interceptorName lowercaseString] isEqualToString:[kUpdateProfile lowercaseString]]){
        
        interceptorType = InterceptorTypeUpdateProfile;
    } else if ([[interceptorName lowercaseString] isEqualToString:[kUpdate1FA lowercaseString]]) {
        
        interceptorType = IntercepterType1FA;
    }
    
    return interceptorType;
    
}



- (NSDictionary *)getOrdersDataDictionary{
    
    return _orderDataDictionary;
}

- (NSDictionary *)getFaultsDataDictionary{
    
    return _faultDataDictionary;
}


- (NSDictionary *)getBillsDataDictionary{
    
    return _billDataDictionary;
}

- (void)saveUserDataInPersistenceWithUser:(BTUser *)user
{
    DDLogInfo(@"Saving data from response of NLQueryUserDetailsWebService API Call into persistence for username %@.", user.username);
    
    NSManagedObjectContext *context = [AppDelegate sharedInstance].managedObjectContext;
    CDUser *persistenceUser = [CDUser userWithUsername:user.username inManagedObjectContext:context];
    persistenceUser.firstName = user.firstName;
    persistenceUser.lastName = user.lastName;
    persistenceUser.titleInName = user.titleInName;
    persistenceUser.securityQuestion = user.securityQuestion;
    persistenceUser.securityAnswer = user.securityAnswer;
    persistenceUser.mobileNumber = user.mobileNumber;
    persistenceUser.landlineNumber = user.landlineNumber;
    persistenceUser.primaryEmailAddress = user.primaryEmailAddress;
    persistenceUser.alternativeEmailAddress = user.alternativeEmailAddress;
    persistenceUser.contactId = user.contactId;
    
    NSArray *arrayOfCugs = [persistenceUser.cugs allObjects];
    for(CDCug *persistenceCug in arrayOfCugs)
    {
        [persistenceUser removeCugsObject:persistenceCug];
        [context deleteObject:persistenceCug];
    }
    
    for(BTCug *cugObject in user.arrayOfCugs)
    {
        CDCug *newPersistenceCug = [CDCug newCugInManagedObjectContext:context];
        
        newPersistenceCug.cugName = cugObject.cugName;
        newPersistenceCug.cugId = cugObject.cugID;
        newPersistenceCug.cugRole = [NSNumber numberWithInteger:cugObject.cugRole];
        newPersistenceCug.groupKey = cugObject.groupKey;
        newPersistenceCug.contactId = cugObject.contactId;
        newPersistenceCug.refKey = cugObject.refKey;
        newPersistenceCug.indexInAPIResponse = [NSNumber numberWithInteger:cugObject.indexInAPIResponse];
        [persistenceUser addCugsObject:newPersistenceCug];
    }
    
    if(persistenceUser.currentSelectedCug == nil)
    {
        NSArray *sortedArrayOfCugs = [user.arrayOfCugs sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"indexInAPIResponse" ascending:YES]]];
        if([sortedArrayOfCugs count] >= 1)
        {
            BTCug *cugInFirstIndex = [sortedArrayOfCugs objectAtIndex:0];
            CDCug *newPersistenceCug = [CDCug newCugInManagedObjectContext:context];
            newPersistenceCug.cugName = cugInFirstIndex.cugName;
            newPersistenceCug.cugId = cugInFirstIndex.cugID;
            newPersistenceCug.refKey = cugInFirstIndex.refKey;
            newPersistenceCug.contactId = cugInFirstIndex.contactId;
            newPersistenceCug.cugRole = [NSNumber numberWithInteger:cugInFirstIndex.cugRole];
            newPersistenceCug.groupKey = cugInFirstIndex.groupKey;
            newPersistenceCug.indexInAPIResponse = [NSNumber numberWithInteger:cugInFirstIndex.indexInAPIResponse];
            persistenceUser.currentSelectedCug = newPersistenceCug;
        }
    }
    
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
    
    // Create immutable BTUser Object from persistence
    _user = [[BTUser alloc] initWithCDUser:persistenceUser securityQuestion:user.securityQuestion securityAnswer:user.securityAnswer mobileNumber:user.mobileNumber landlineNumber:user.landlineNumber primaryEmail:user.primaryEmailAddress contactID:user.contactId andAlternativeEmail:user.alternativeEmailAddress ];
    
}

- (void)saveInterceptorWithOrder:(NSString *)order andInterfaces:(NSString *)interfaces
{
    //Interceptor
    if(!_interceptorArray){
        
        _interceptorArray = [[NSMutableArray alloc] init];
    }
    
    [_interceptorArray removeAllObjects];
    
    if([order validAndNotEmptyStringObject] && [interfaces validAndNotEmptyStringObject]){
        
        NSArray *ordersArray = [order componentsSeparatedByString:@","];
        NSArray *interfaceArray = [interfaces componentsSeparatedByString:@","];
        
        if([ordersArray count] == [interfaceArray count]){
            
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"intValue" ascending:YES];
            NSArray *sortedArray = [ordersArray sortedArrayUsingDescriptors:@[sortDescriptor]];
            
            for(NSString *order in sortedArray){
                
                NSInteger index = [ordersArray indexOfObject:order];
                NSString *interFace = [interfaceArray objectAtIndex:index];
                [_interceptorArray addObject:interFace];
            }
        }
    }
 
// Uncomment the following to force app to show an intercept screen
//#warning - force intercept for debug
//    if (kBTServerType == BTServerTypeModelA) {
////        NSString *forcedInt = @"FirstTimeLogin";
////        NSString *forcedInt = @"ResetPassword";
////        NSString *forcedInt = @"UpdateContact";
////        NSString *forcedInt = @"UpdateProfile";
////        NSString *forcedInt = @"OneFA";
//        BOOL found = NO;
//        for (NSString *intfce in _interceptorArray) {
//            if ([forcedInt isEqualToString:intfce]) {
//                found = YES;
//            }
//        }
//        if (!found) {
//            [_interceptorArray addObject:forcedInt];
//        }
//    }
    
}

- (void)takeActionAfterFetchingUserDetailsWithGroup:(NSString *)groupKey
{
    [[AppDelegate sharedInstance].viewModel getUserDetailsAPICallSuccessfullyFinishedWithGroupKey:groupKey];
    
    [self.homeScreenDelegate successfullyFetchedUserDataOnHomeScreen:self];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"APICallForUserDetailsFinishedSuccessfullyAtHomeScreenNotification" object:nil];
}

#pragma mark - NLQueryUserDetailsWebService Methods
- (void)getUserWebService:(NLQueryUserDetailsWebService *)webService successfullyFetchedUserData:(BTUser *)user
{
    // (hd) Save the latest data from web service into the persistence.
    if(webService == _queryUserDetailsWebService)
    {
        [self saveUserDataInPersistenceWithUser:user];
        
        [self saveInterceptorWithOrder:webService.order andInterfaces:webService.interfaces];

        NSString *groupKey;
        if ([user.arrayOfCugs count] > 0)
        {
            groupKey = [(BTCug *)[user.arrayOfCugs objectAtIndex:0] groupKey];
        }
        
        [self takeActionAfterFetchingUserDetailsWithGroup:groupKey];

        _queryUserDetailsWebService.queryUserDetailsWebServiceDelegate = nil;
        _queryUserDetailsWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLQueryUserDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLQueryUserDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}


- (void)getUserWebService:(NLQueryUserDetailsWebService *)webService failedToFetchUserDataWithWebServiceError:(NLWebServiceError *)error
{
    if(webService == _queryUserDetailsWebService)
    {
        [self.homeScreenDelegate homeScreen:self failedToFetchUserDataWithWebServiceError:error];
        _queryUserDetailsWebService.queryUserDetailsWebServiceDelegate = nil;
        _queryUserDetailsWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLQueryUserDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLQueryUserDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

#pragma mark - NLGetNotificationsWebServiceDelegate methods
- (void)getNotificationsWebService:(NLGetNotificationsWebService *)webService successfullyFetchedNotificationsData:(NSArray *)arrayOfNotifications
{
    if(webService == _getNotificationsWebService)
    {
        _arrayOfNotifications = arrayOfNotifications;

        [self.homeScreenDelegate successfullyFetchedNotificationDataOnHomeScreen:self];

        NSDictionary *notifications = [NSDictionary dictionaryWithObject:_arrayOfNotifications forKey:@"notifications"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"APICallForGetNotificationsFinishedSuccessfullyNotification" object:nil userInfo:notifications];

        _getNotificationsWebService.getNotificationsWebServiceDelegate = nil;
        _getNotificationsWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLGetNotificationsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLGetNotificationsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }

}

- (void)getNotificationsWebService:(NLGetNotificationsWebService *)webService failedToFetchNotificationsDataWithWebServiceError:(NLWebServiceError *)error
{
    if(webService == _getNotificationsWebService)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"APICallForGetNotificationsFailedNotification" object:nil];

        [self.homeScreenDelegate homeScreen:self failedToFetchNotificationsWithWebServiceError:error];
        
        _getNotificationsWebService.getNotificationsWebServiceDelegate = nil;
        _getNotificationsWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLGetNotificationsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLGetNotificationsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

#pragma mark- NLFaultDashBoardWebServiceDelegate Methods

- (void)getFaultDashBoardWebService:(NLFaultDashBoardWebService *)webService successfullyFetchedFaultDashBoardData:(NSArray *)faults pageIndex:(int)pageIndex totalSize:(int)totalSize andTabID:(int)tabID
{

    if(webService == _getFaultDashBoardWebService)
    {
        _isTrackFaultAPICompleted = YES;
        if([faults count]>0)
        {
            _faultStatusMatrix = @"Fault - Red";
        }
        else
        {
            _faultStatusMatrix = @"Fault - ok";
        }
        [self notifyHomeScreenForStatusMatrix];
        
        
        //Keeping fault response
        NSMutableDictionary *faultsDataDict = [NSMutableDictionary dictionary];
        
        [faultsDataDict setValue:faults forKey:@"faults"];
        [faultsDataDict setValue:[NSNumber numberWithInt:pageIndex] forKey:@"pageIndex"];
        [faultsDataDict setValue:[NSNumber numberWithInt:totalSize] forKey:@"totalSize"];
        [faultsDataDict setValue:[NSNumber numberWithInt:tabID] forKey:@"tabID"];
        _faultDataDictionary = [NSDictionary dictionaryWithDictionary:faultsDataDict];


        [self updateGridViewModelWithSize:totalSize title:kTrackFaultsText andTextState:@"open"];

        NSDictionary *openItemsDict = [NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:totalSize] forKey:@"OpenItems"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"APICallForOpenFaultsFinishedSuccessfullyAtHomeScreenNotification" object:nil userInfo:openItemsDict];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate.homeScreenData setObject:[NSNumber numberWithInteger:totalSize] forKey:@"faultData"];
        _getFaultDashBoardWebService.getFaultDashBoardWebServiceDelegate = nil;
        _getFaultDashBoardWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLFaultDashBoardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLFaultDashBoardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)getFaultDashBoardWebService:(NLFaultDashBoardWebService *)webService failedToFetchFaultDashBoardDataWithWebServiceError:(NLWebServiceError *)error
{
    if(webService == _getFaultDashBoardWebService)
    {
        _isTrackFaultAPICompleted = YES;
        _faultStatusMatrix = @"";
        [self notifyHomeScreenForStatusMatrix];

        [self updateGridViewModelForFailureWithTitle:kTrackFaultsText];

        [[NSNotificationCenter defaultCenter] postNotificationName:@"APICallForOpenFaultsFailedAtHomeScreenNotification" object:nil];
        
        [self.homeScreenDelegate homeScreen:self failedToFetchGridDataWithWebServiceError:error];

        _getFaultDashBoardWebService.getFaultDashBoardWebServiceDelegate = nil;
        _getFaultDashBoardWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLFaultDashBoardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLFaultDashBoardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}


#pragma mark- NLTrackOrderDashBoardWebServiceDelegate Methods
- (void)getOrderDashBoardWebService:(NLTrackOrderDashBoardWebService *)webService successfullyFetchedOrderDashBoardData:(NSArray *)orders pageIndex:(int)pageIndex totalSize:(int)totalSize tabID:(int)tabID{

    if(webService == _getOrderDashBoardWebService)
    {
        
        //Keeping Order response
        NSMutableDictionary *orderDataDict = [NSMutableDictionary dictionary];
        
        [orderDataDict setValue:orders forKey:@"orders"];
        [orderDataDict setValue:[NSNumber numberWithInt:pageIndex] forKey:@"pageIndex"];
        [orderDataDict setValue:[NSNumber numberWithInt:totalSize] forKey:@"totalSize"];
        [orderDataDict setValue:[NSNumber numberWithInt:tabID] forKey:@"tabID"];
        //_orderDataDictionary = [NSDictionary dictionaryWithDictionary:orderDataDict];
        [_orderDataDictionary addEntriesFromDictionary:orderDataDict];
        
        //Updating Grid Data
//        [self updateGridViewModelWithSize:totalSize title:kTrackOrdersText andTextState:@"active"];

        _isTrackOrderAPICompleted = YES;
//        if([orders count]>0)
//        {
//            _orderStatusMatrix = @"Orders - Red";
//        }
//        else
//        {
//            _orderStatusMatrix = @"Orders - ok";
//        }
        //[self notifyHomeScreenForStatusMatrix];

        [self ordersAPIsFinished];

        _getOrderDashBoardWebService.getTrackOrderDashBoardWebServiceDelegate = nil;
        _getOrderDashBoardWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLTrackOrderDashBoardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLTrackOrderDashBoardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)getOrderDashBoardWebService:(NLTrackOrderDashBoardWebService *)webService failedToFetchOrderDashBoardDataWithWebServiceError:(NLWebServiceError *)error {

    if(webService == _getOrderDashBoardWebService)
    {
        _isTrackOrderAPICompleted = YES;
        //_orderStatusMatrix = @"";
        //[self notifyHomeScreenForStatusMatrix];

        [self ordersAPIsFinished];

//        [self updateGridViewModelForFailureWithTitle:kTrackOrdersText];
//
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"APICallForActiveOrdersFailedAtHomeScreenNotification" object:nil];

        [self.homeScreenDelegate homeScreen:self failedToFetchGridDataWithWebServiceError:error];
        
        _getOrderDashBoardWebService.getTrackOrderDashBoardWebServiceDelegate = nil;
        _getOrderDashBoardWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLTrackOrderDashBoardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLTrackOrderDashBoardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

#pragma mark - NLAPIGEEAuthenticationProtectedWebServiceDelegate (for getProjectsByCUG)

- (void)webService:(NLAPIGEEAuthenticatedWebService *)webService finshedWithSuccessResponse:(NSObject *)responseObject
{
    if (webService == self.getProjectsByCUGWebService) {
        //
        _isOCSAPICompleted = YES;
        
        NSMutableArray *projects = [NSMutableArray new];
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSObject *projectsObj = [(NSDictionary*)responseObject objectForKey:@"Projects"];
            if ([projectsObj isKindOfClass:[NSArray class]]) {
                for (NSObject *project in (NSArray*)projectsObj) {
                    if ([project isKindOfClass:[NSDictionary class]]) {
                        BTOCSProject *newProject = [[BTOCSProject alloc] initWithAPIResponse:(NSDictionary*)project];
                        [projects addObject:newProject];
                    }
                }
            } else if ([projectsObj isKindOfClass:[NSDictionary class]]) {
                BTOCSProject *newProject = [[BTOCSProject alloc] initWithAPIResponse:(NSDictionary*)projectsObj];
                [projects addObject:[newProject copy]];
            }
        }
        
        [_orderDataDictionary setObject:projects forKey:@"projects"];
        
        [self ordersAPIsFinished];
        
        self.getProjectsByCUGWebService.delegate = nil;
        self.getProjectsByCUGWebService = nil;
    }
}

- (void)webService:(NLAPIGEEAuthenticatedWebService *)webService failedWithError:(NLWebServiceError *)error
{
    if (webService == self.getProjectsByCUGWebService) {
        //
        _isOCSAPICompleted = YES;
        
        //[self.homeScreenDelegate homeScreen:self failedToFetchGridDataWithWebServiceError:error];
        
        [self ordersAPIsFinished];
        
        self.getProjectsByCUGWebService.delegate = nil;
        self.getProjectsByCUGWebService = nil;
    }
}

#pragma mark - finalise orders API responses

- (void)ordersAPIsFinished
{
    if (_isTrackOrderAPICompleted && _isOCSAPICompleted) {
        if ([_orderDataDictionary objectForKey:@"orders"] || [_orderDataDictionary objectForKey:@"projects"]) {
            // success from at least one api
            int totalSize = 0;
            
            if ([_orderDataDictionary objectForKey:@"orders"] && [_orderDataDictionary objectForKey:@"totalSize"]) {
                NSUInteger count = [[_orderDataDictionary objectForKey:@"totalSize"] integerValue];
                totalSize += count;
            }
            
            if ([_orderDataDictionary objectForKey:@"projects"] && [[_orderDataDictionary objectForKey:@"projects"] isKindOfClass:[NSArray class]]) {
                NSArray<BTOCSProject*> *projects = [_orderDataDictionary objectForKey:@"projects"];
                for (BTOCSProject *project in projects) {
                    if ([project.status.lowercaseString isEqualToString:@"open"]) {
                        totalSize++;
                    }
                }
            }
            
            if(totalSize>0)
            {
                _orderStatusMatrix = @"Orders - Red";
            }
            else
            {
                _orderStatusMatrix = @"Orders - ok";
            }
            [self notifyHomeScreenForStatusMatrix];
            
            [self updateGridViewModelWithSize:totalSize title:kTrackOrdersText andTextState:@"active"];
            //NSDictionary *openItemsDict = [NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:totalSize] forKey:@"OpenItems"];
            //[[NSNotificationCenter defaultCenter] postNotificationName:@"APICallForActiveOrdersFinishedSuccessfullyAtHomeScreenNotification" object:nil userInfo:openItemsDict];
            
            NSDictionary *openItemsDict = [NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:totalSize] forKey:@"OpenItems"];
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate.homeScreenData setObject:[NSNumber numberWithInteger:totalSize] forKey:@"trackOrder"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"APICallForActiveOrdersFinishedSuccessfullyAtHomeScreenNotification" object:nil userInfo:openItemsDict];
            
        } else {
            //fail
            _orderStatusMatrix = @"";
            [self notifyHomeScreenForStatusMatrix];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"APICallForActiveOrdersFailedAtHomeScreenNotification" object:nil];
        }
    }
}


#pragma mark- NLGetBillPaymentHistoryWebService Methods

- (void)getBillPaymentHistoryWebService:(NLGetBillPaymentHistoryWebService *)webService successfullyFetchedPaymentHistoryDataWithAwaitingPayments:(NSArray *)arrayOfAwaitingPayments andRecentlyPaid:(NSArray *)arrayOfRecentlyPaid
{
    if(webService == _getBillPaymentHistoryWebService)
    {
        _isBillsAPICompleted = YES;
        if([arrayOfAwaitingPayments count]>0)
        {
            _billStatusMatrix = @"Billing - Green";
        }
        else
        {
            _billStatusMatrix = @"Billing - ok";
        }
        [self notifyHomeScreenForStatusMatrix];
        
        
        NSMutableDictionary *billDataDict = [NSMutableDictionary dictionary];
        
        [billDataDict setValue:arrayOfAwaitingPayments forKey:@"awaitingPayments"];
        [billDataDict setValue:arrayOfRecentlyPaid forKey:@"recentlyPaid"];
        _billDataDictionary = [NSDictionary dictionaryWithDictionary:billDataDict];
        


        [self updateGridViewModelWithSize:(int)[arrayOfAwaitingPayments count] title:kBillsText andTextState:@"new"];

        NSDictionary *openItemsDict = [NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:[arrayOfAwaitingPayments count]] forKey:@"OpenItems"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"APICallForReadyBillsFinishedSuccessfullyAtHomeScreenNotification" object:nil userInfo:openItemsDict];

        _getBillPaymentHistoryWebService.getBillPaymentHistoryWebServiceDelegate = nil;
        _getBillPaymentHistoryWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLGetBillPaymentHistoryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLGetBillPaymentHistoryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)getBillPaymentHistoryWebService:(NLGetBillPaymentHistoryWebService *)webService failedToFetchPaymentHistoryDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    if(webService == _getBillPaymentHistoryWebService)
    {
        _isBillsAPICompleted = YES;
        _billStatusMatrix = @"";
        [self notifyHomeScreenForStatusMatrix];

        [self updateGridViewModelForFailureWithTitle:kBillsText];

        [[NSNotificationCenter defaultCenter] postNotificationName:@"APICallForReadyBillsFailedAtHomeScreenNotification" object:nil];
        
        [self.homeScreenDelegate homeScreen:self failedToFetchGridDataWithWebServiceError:webServiceError];

        _getBillPaymentHistoryWebService.getBillPaymentHistoryWebServiceDelegate = nil;
        _getBillPaymentHistoryWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLGetBillPaymentHistoryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLGetBillPaymentHistoryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)notifyHomeScreenForStatusMatrix
{
    if(_isTrackOrderAPICompleted && _isTrackFaultAPICompleted && _isBillsAPICompleted && _isOCSAPICompleted)
    {
        NSString *statusMatrix = [self getStatusMatrixForHomeScreen];
        [self.homeScreenDelegate successfullyParsedSatusMatrixForHomeScreen:statusMatrix];
    }
}


- (NSString *)getStatusMatrixForHomeScreen
{
    NSString *completeString = @"";
    
    if(_orderStatusMatrix && _orderStatusMatrix.length > 1)
    {
        completeString = _orderStatusMatrix;
    }
    
    if(_faultStatusMatrix && _faultStatusMatrix.length > 1)
    {
        completeString = [NSString stringWithFormat:@"%@, %@",completeString,_faultStatusMatrix];
    }
    
    if(_billStatusMatrix && _billStatusMatrix.length > 1)
    {
        completeString = [NSString stringWithFormat:@"%@, %@",completeString,_billStatusMatrix];
    }
    
    
    return completeString;
}


#pragma mark - Interceptor API Calling

- (void)makeUpdateContactInterceptorAPIRequestWithContactArray:(NSArray *)contactArray
{
    if (contactArray == nil) {
        return;
    }
    
    if(!updateContactInterceptorArray)
    {
        updateContactInterceptorArray = [[NSMutableArray alloc] init];
        
        [updateContactInterceptorArray addObject:contactArray];
        
        self.updateContactInterceptorModel = [[DLMUpdateContactInerceptScreen alloc] init];
        self.updateContactInterceptorModel.updateContactInerceptScreenDelegate = self;
        
        [self makeAPIReqeustWithParams:contactArray];
    }
    else
    {
        [updateContactInterceptorArray addObject:contactArray];
    }
    
}


- (void)makeAPIReqeustWithParams:(NSArray *)contactArray
{
    
    interceptorNumOfTries ++;
    
    NSString *password = [SAMKeychain passwordForService:kKeyChainServiceBTUserPassword account:[AppDelegate sharedInstance].viewModel.app.loggedInUser.username];

    NSDictionary *params = [self.updateContactInterceptorModel getUserDataDictionaryForContactWrapperArray:contactArray];
    
    [self.updateContactInterceptorModel submitAndCreateContactDetailsModelWithContactDetailsDictionary:params andPassword:password];
}
    


#pragma mark - Update Contact/Profile interceptor screenmodel delegate

- (void)updateSuccessfullyFinishedByUpdateContactInerceptScreen:(DLMUpdateContactInerceptScreen *)updateContactInerceptScreen
{
    if([updateContactInterceptorArray count] >0)
    {
        [self.updateContactInterceptorModel updateLocalDatabaseWithContacts:[updateContactInterceptorArray objectAtIndex:0]];
        [updateContactInterceptorArray removeObjectAtIndex:0];
        interceptorNumOfTries = 0;
    }
    
    if([updateContactInterceptorArray count]>0)
    {
        [self makeAPIReqeustWithParams:[updateContactInterceptorArray objectAtIndex:0]];
    }
}

- (void)updateContactInerceptScreen:(DLMUpdateContactInerceptScreen *)updateContactInerceptScreen failedToSubmitUpdateContactInerceptScreenWithWebServiceError:(NLWebServiceError *)error
{
    if(interceptorNumOfTries <= kMaxNumOfTries)
    {
      if([updateContactInterceptorArray count] >0)
      {
          [self makeAPIReqeustWithParams:[updateContactInterceptorArray objectAtIndex:0]];
      }
    }
    else
    {
        interceptorNumOfTries = 0;
        
        if([updateContactInterceptorArray count]>0)
        {
            [updateContactInterceptorArray removeObjectAtIndex:0];
            
            if([updateContactInterceptorArray count]>0)
            {
                [self makeAPIReqeustWithParams:[updateContactInterceptorArray objectAtIndex:0]];
            }
        }
        
    }
}


@end





@implementation HomeGridViewModel


@end

