//
//  BTHomeViewController.h
//  BTBusinessApp
//
//  Created by Rohini Kumar on 13/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTHomeNavigationController.h"
#import "BTBaseViewController.h"

@class DLMHomeScreen;
@class BTUser;

typedef NS_ENUM(NSInteger, RollID) {
    RollID_Public = 1,
    RollID_OrderDashBoard,
    RollID_FaultsDashBoard,
    RollID_All_SuperUser
};

@interface BTHomeViewController : UIViewController <BTHomeNavigationControllerDelegate>

@property (nonatomic) BOOL networkRequestInProgress;
@property (nonatomic, strong) NSDictionary *userDetails;
@property (nonatomic, readwrite) DLMHomeScreen *viewModel;

- (void)fetchUserDetails;
- (void) launchRespectiveScreenWithDictionary:(NSDictionary *)apnsDict;

@end
