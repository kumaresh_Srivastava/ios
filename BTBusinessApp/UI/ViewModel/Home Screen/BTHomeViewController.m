//
//  BTHomeViewController.m
//  BTBusinessApp
//
//  Created by Rohini Kumar on 13/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTHomeViewController.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "BTAssetsDashboardViewController.h"
#import "BTBillsDashBoardViewController.h"
#import "BTSettingsViewController.h"
#import "BTInAppBrowserViewController.h"
#import "BTOrderTrackerDetailsViewController.h"
#import "BTFaultTrackerSummaryViewController.h"
#import "BTFaultTrackerDashboardViewController.h"
#import "BTFaultTrackerSearchViewController.h"
#import "BTOrderTrackerHomeViewController.h"
#import "BTOrderTrackerDashboardViewController.h"
#import "BTOnBoardingViewController.h"
#import "LHLogsViewController.h"
#import "DLMHomeScreen.h"
#import "BTUser.h"
#import "BTCug.h"
#import "PlaygroundViewController.h" //Salman
#import "AppConstants.h"
#import "HomeScreenGridView.h"
#import "HomeBottomView.h"
#import "BTSecurityCheckInterceptorViewController.h"
#import "BTSecurityQuestionsAndAnswerViewController.h"
#import "BTSecurityNumberViewController.h"
#import "BTOneFAInterceptViewController.h"
#import "BTUpdateContactInerceptViewController.h"
#import "BTUsageDashboardViewController.h"
#import "BTNotification.h"
#import "BTRetryView.h"
#import "CustomSpinnerAnimationView.h"
#import "NLWebServiceError.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "DummyTestViewController.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "BTHelpAndSupportViewController.h"
#import "BTServiceStatusDashboardViewController.h"

#import "BTAccountSelectTableViewController.h"
#import "BTProductSelectTableViewController.h"
#import "BTOrderDashboardTableViewController.h"

#import "SpeedTestIntroductionViewController.h"

#import "BTAccountLocationDropdownView.h"
#import "BTNavigationViewController.h"
#import "AboutTheAppTableViewController.h"

#define kXpadding 0
#define kHeightAdjsutmentFactor 16
#define kHeightAdjsutmentFactorForIphone5 14
#define IS_NOT_IPHONE4 ([[UIScreen mainScreen] bounds].size.height != 480.0)
#define IS_IPHONE5 ([[UIScreen mainScreen] bounds].size.height == 568.0)

@interface BTHomeViewController ()< DLMHomeScreenDelegate, HomeScreenGridViewDelegate,UIScrollViewDelegate,HomeBottomViewDelegate, BTUpdateContactInerceptViewControllerDelegate, BTSecurityNumberViewControllerDelegate, BTRetryViewDelegate, BTOnBoardingViewControllerDelegate> {
    
    // UIButton *_groupNameSelectionButton;
    BOOL _isFirstLaunch;
    CustomSpinnerView *_loadingView;
    CustomSpinnerAnimationView *_loaderView;
    UILabel *_loadingLabel;
    BTRetryView *_retryView;
    BTAccountLocationDropdownView *_accountDropdownView;
    BOOL _isBillViewNeedToUpdate;
    BOOL _isFirstAppear;
    NSString *_pageNameForOmniture;//RM
    BOOL _isErrorHandlingInProgressForAnotherAPI;//(sal)taken to avoid multiple redirection alerts, in case of multiple API failure
    HomeScreenGridView *gridView;
}
@property (weak, nonatomic) IBOutlet UIView *spinnerLoadingView;

@property (strong, nonatomic) IBOutlet UIScrollView *containerScrollView;
//@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (strong, nonatomic) UIView *topView;

@property (copy, nonatomic) NSString *groupKey;
@property (strong, nonatomic) BTCug *currentCug;

@property(assign, nonatomic) BOOL isUIPopulated;//UI will be populated only after all interceptors completion
@property (strong, nonatomic) UINavigationController *interceptorNavigationController;

@end

@implementation BTHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // (lp) Setup viewModel for home screen
    self.viewModel = [[DLMHomeScreen alloc] init];
    self.viewModel.homeScreenDelegate = self;
    
    self.navigationItem.hidesBackButton = YES;
    
    // (LP) Do any additional setup after loading the view.
    
    // (LP) UI handling with the network request in progress.
    __weak typeof(self) weakSelf = self;
    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        
        if ([weakSelf networkRequestInProgress]) {
            
            [weakSelf hideAndShowTheControlsIsHide:YES];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
            
        } else {
            
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
        }
    }];
    
    // (LP) Fetch user details from viewModel and update UI
    _isFirstAppear = YES;
    [self initialHomeScreenSetup];
    _isFirstLaunch = YES;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(billPaidSuccessfully:) name:kBillPaidSuccessfullyNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkReadyBillsAPICallStarted) name:@"APICallForReadyBillsStartedAtHomeScreenNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(navigateToSelectedScreenFromNotificationScreen:) name:@"navigateToSelectedScreenFromNotification" object:nil];
    
    [AppManager trackOmnitureKeyTask:@"Successful login" forPageName:OMNIPAGE_HOME];
    [self trackPage];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    
    if(!_isFirstLaunch){
        
        [self checkForUpdatedScreenTitle];
        
        if(!self.isUIPopulated){
            
            //RLM now no need of this
            // [self.viewModel checkForInterceptor];
        }
    }
    
    _isFirstLaunch = NO;
    
    if(_currentCug) {
        _accountDropdownView.locationLabel.text = _currentCug.cugName;
    }
    
    [self performSelector:@selector(createLoaderImageView) withObject:nil afterDelay:0.1];
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChangeLoaderNotifyHome:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
    
}

- (void) viewDidDisappear:(BOOL)animated{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
}

- (void)statusBarOrientationChangeLoaderNotifyHome:(NSNotification *)notification {
    // handle the interface orientation as needed
    [self performSelector:@selector(createLoaderImageView) withObject:nil afterDelay:0.1];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if (_isFirstAppear)
    {
        _isFirstAppear = NO;
        
        if (_userDetails)
        {
            [self.viewModel takeActionOnPrefetchedUserDetails:_userDetails];
        }
        else
        {
            if ([AppManager isInternetConnectionAvailable])
            {
                [self fetchUserDetails];
            }
            else
            {
                [self hideLoaderView];
                [AppManager trackNoInternetErrorOnPage:OMNIPAGE_HOME];
                [self showRetryViewWithInternetStrip:YES];
            }
        }
        _userDetails = nil;
    }
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)navigateToSelectedScreenFromNotificationScreen:(NSNotification*)notify
{
    NSString* notificationCat  = [notify.userInfo objectForKey:@"notificationCategory"];
    
    if ([notificationCat isEqualToString:@"Your account"]) {
        [self.tabBarController setSelectedIndex:accountTag];
    }
    else if ([notificationCat isEqualToString:@"Orders"]) {
         [self redirectToTrackAnOrderScreen];
    }
    else if ([notificationCat isEqualToString:@"Faults"]) {
        [self redirectToTrackFaultScreen];
    }
    else if ([notificationCat isEqualToString:@"Billing"]) {
         [self.tabBarController setSelectedIndex:billsTag];
    }
    else if ([notificationCat isEqualToString:@"Hub Fault"]) {
        
    }
}

- (void)trackPage
{
    //track Home page
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];

    [OmnitureManager trackPage:OMNIPAGE_HOME withContextInfo:data];
}



- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kBillPaidSuccessfullyNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"APICallForReadyBillsStartedAtHomeScreenNotification" object:nil];
}

//:Temporary code

- (void)addRightNavButton
{
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 20)];
    [btn addTarget:self action:@selector(showIntercept) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    NSMutableArray *buttonArray = [NSMutableArray arrayWithArray:self.navigationItem.rightBarButtonItems];
    
    self.navigationItem.rightBarButtonItem = nil;
    
    [buttonArray addObject:item];
    
    self.navigationItem.rightBarButtonItems = buttonArray;
    
}

- (void)showIntercept
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Message" message:@"Select which intercept your want to see." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"First time login" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self redirectToFirstTimeLoginIntercepter];
        
    }];
    
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    
    
    UIAlertAction *resetPass = [UIAlertAction actionWithTitle:@"Reset password" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self redirectToCreatePasswordIntercepter];
        
    }];
    
    UIAlertAction *updateContact = [UIAlertAction actionWithTitle:@"Update contact details" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self redirectToUpdateContactDetailsIntercepter];
        
    }];
    
    
    
    [alertController addAction:okAction];
    [alertController addAction:resetPass];
    [alertController addAction:updateContact];
    [alertController addAction:cancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}


- (void)openInterceptorController:(UIViewController *)controller{
    
    if(!self.interceptorNavigationController){
        
        self.interceptorNavigationController = [[UINavigationController alloc] initWithRootViewController:controller];
        [self presentViewController:self.interceptorNavigationController animated:YES completion:nil];
        
    }
    else{
        
        [self.interceptorNavigationController pushViewController:controller animated:YES];
    }
}


- (void)redirectToFirstTimeLoginIntercepter
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTSecurityCheckInterceptorViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTSecurityCheckInterceptorViewControllerID"];
    [self openInterceptorController:vc];
}

- (void)redirectToCreatePasswordIntercepter
{
    //    //Redirect to security question screen
    //    BTSecurityQuestionsAndAnswerViewController *changeSecurityQuestion = [self.storyboard instantiateViewControllerWithIdentifier:@"SecurityQuestionsAndAnswerScene"];
    //    changeSecurityQuestion.isScreenRelatedToInterceptorModule = YES;
    //    [self openInterceptorController:changeSecurityQuestion];
    
        BTSecurityNumberViewController *updateSecurityNumber = [self.storyboard instantiateViewControllerWithIdentifier:@"BTSecurityNumberViewController"];
        updateSecurityNumber.isScreenrelatedTo1FAMigration = NO;
        updateSecurityNumber.isScreenRelatedToInterceptorModule = YES;
        [self openInterceptorController:updateSecurityNumber];
    
//    BTOneFAInterceptViewController *oneFAInterceptVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BTOneFAInterceptViewController"];
//    oneFAInterceptVC.delegate = self;
//    oneFAInterceptVC.isScreenrelatedTo1FAMigration = NO;
//    oneFAInterceptVC.isScreenRelatedToInterceptorModule = YES;
//    [self openInterceptorController:oneFAInterceptVC];
    
}


- (void)redirectToUpdateContactDetailsIntercepter
{
    BTUpdateContactInerceptViewController *updateContactVC = [[BTUpdateContactInerceptViewController alloc] init];
    updateContactVC.delegate = self;
    updateContactVC.intercepterType = UpdateContactIntercepter;
    [self openInterceptorController:updateContactVC];
    
}


- (void)redirectToUpdateProfileIntercepter
{
    BTUpdateContactInerceptViewController *updateContactVC = [[BTUpdateContactInerceptViewController alloc] init];
    updateContactVC.delegate = self;
    updateContactVC.intercepterType = UpdateProfileIntercepter;
    [self openInterceptorController:updateContactVC];
    
}

- (void)redirectToUpdate1FAInterceptor
{
    BTOneFAInterceptViewController *oneFAInterceptVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BTOneFAInterceptViewController"];
    oneFAInterceptVC.delegate = self;
    oneFAInterceptVC.isScreenrelatedTo1FAMigration = NO;
    oneFAInterceptVC.isScreenRelatedToInterceptorModule = YES;
    [self openInterceptorController:oneFAInterceptVC];
    
//        BTSecurityNumberViewController *updateSecurityNumber = [self.storyboard instantiateViewControllerWithIdentifier:@"BTSecurityNumberViewController"];
//        updateSecurityNumber.isScreenrelatedTo1FAMigration = YES;
//        updateSecurityNumber.isScreenRelatedToInterceptorModule = NO;
//        [self openInterceptorController:updateSecurityNumber];
}

- (void)redirectToUpdate1FAMigrationInterceptor
{
    BTSecurityNumberViewController *updateSecurityNumber = [self.storyboard instantiateViewControllerWithIdentifier:@"BTSecurityNumberViewController"];
    updateSecurityNumber.delegate = self;
    updateSecurityNumber.isScreenrelatedTo1FAMigration = YES;
    updateSecurityNumber.isScreenRelatedToInterceptorModule = NO;
    [self openInterceptorController:updateSecurityNumber];
}


#pragma mark - Initial Setup Methods

/*
 Handles the initial home screen setup for UI Items and animation.
 */

- (void)updateNavigationControllerForHomeScreen
{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar setTranslucent:YES];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
}

- (void)initialHomeScreenSetup
{
    
    DDLogInfo(@"Home: Initial setup for home screen done.");
    
    self.topView.backgroundColor = [UIColor colorForHexString:@"eeeeee"];//[BrandColours colourBtNeutral30];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self.navigationItem.leftBarButtonItem setTintColor:[UIColor clearColor]];
    [self.navigationItem.leftBarButtonItem setEnabled:NO];
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor clearColor]];
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    
    
}


- (void)setUpGridsUI
{
    //Clear UI
    for(UIView *subView in self.topView.subviews){
        
        [subView removeFromSuperview];
    }
    for (UIView *subview in self.bottomView.subviews) {
        [subview removeFromSuperview];
    }
    
    //[self addTopGridButtons];
    [self addTopGridItems];
    //[self addBottomGridImageButtons];
    //[self addBottomGridTextButtons];
}

- (void)addTopGridButtons
{
    //Adding Top Grid
    NSArray *gridModelArray = [self.viewModel gridModelArray];
    CGFloat xPointer = kXpadding;
    NSInteger count = 0;
    CGFloat yPos = 0;
    
    for(HomeGridViewModel *gridModel in gridModelArray)
    {
        HomeScreenGridView *gridView = [[[NSBundle mainBundle] loadNibNamed:@"HomeScreenGridView" owner:self options:nil] firstObject];
        
        //Make Initial GridView in this Frame is not based on Autolayout
        gridView.frame = CGRectMake(xPointer,
                                    0,
                                    kScreenSize.width,
                                    gridView.frame.size.height);
        
        gridView.delegate = self;
        [gridView updateWithGridViewModel:gridModel];
        [self.topView addSubview:gridView];
        
        //Find new height based on autolayout
        CGFloat heightAdjustmentFactor;
        heightAdjustmentFactor = 0;
        
        if(IS_NOT_IPHONE4)
        {
            //Add extra space in order to manage spacing between labels for Iphones other than 4.
            if(IS_IPHONE5)
            {
                //In case of iPhone 5 only we have to reduce the factor by 2
                heightAdjustmentFactor = kHeightAdjsutmentFactorForIphone5;
            }
            else
            {
                heightAdjustmentFactor = kHeightAdjsutmentFactor;
            }
        }
        
        CGFloat newHeight = [gridView height] + heightAdjustmentFactor;
        
        //Bottom Margin is Uniform Spacing between Grids
        CGFloat bottomMargin = 0;
        
        //We have to add bottom margin only when row changes
        if(yPos == 0)
            yPos = yPos + bottomMargin;
        
        //Find new Frame
        gridView.frame = CGRectMake(xPointer,
                                    yPos,
                                    (kScreenSize.width-(2*kXpadding))/2,
                                    newHeight);
        
        if(IS_NOT_IPHONE4)
        {
            [gridView updateGridViewConstraint];
        }
        
        
        count++;
        if(count %2 == 0)
        {
            yPos += newHeight+bottomMargin;
            xPointer = kXpadding;
        }
        else
        {
            xPointer += gridView.frame.size.width;
        }
    }
}

- (void)addBottomGridImageButtons
{
    //Now Add Bottom Grid View
    
    // Find Width Of One Circle
    CGFloat xPadding  = 16;
    CGFloat xPointer = xPadding;
    CGFloat circleWidth = (kScreenSize.width - 4*xPadding)/3;
    
    CGFloat bottomGridOrigin ;
    
    
    NSArray *bottomGridArray = [self.viewModel bottomGridModelArray];
    
    for(HomeGridViewModel *gridModel in bottomGridArray){
        
        HomeBottomView *bottomGridView = [[[NSBundle mainBundle] loadNibNamed:@"HomeBottomView" owner:self options:nil] firstObject];
        
        bottomGridOrigin = 0;
        
        bottomGridView.delegate = self;
        
        bottomGridView.frame = CGRectMake(xPointer,
                                          bottomGridOrigin,
                                          circleWidth,
                                          self.bottomView.frame.size.height);
        
        [bottomGridView updateWithGridViewModel:gridModel];
        
        [bottomGridView layoutIfNeeded];
        
        //Calculate New Height
        CGFloat newHeight = [bottomGridView height];
        
        bottomGridOrigin =  (self.bottomView.frame.size.height - newHeight)/2;
        bottomGridView.frame = CGRectMake(xPointer,
                                          bottomGridOrigin,
                                          circleWidth,
                                          newHeight);
        
        bottomGridView.hidden = NO;
        [self.bottomView addSubview:bottomGridView];
        
        xPointer += bottomGridView.frame.size.width+xPadding;
    }
}

- (void)addTopGridItems
{
    self.topView = [[UIView alloc] init];
    //CGFloat xPos = 0.0;
    //CGFloat yPos = 0.0;
    //CGFloat frameWidth = kScreenSize.width;//self.topView.frame.size.width;
    CGFloat frameHeight = 66.0;
    CGFloat helloFrameHeight = 84.0;
    CGFloat cugSelectionHeight = 110.0;
    
    
    // Add the 'Good morning' bit
    //UIView *helloView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.topView.frame.size.width, helloFrameHeight)];
    UIView *helloView = [[UIView alloc] init];
    helloView.backgroundColor = [UIColor whiteColor];
    helloView.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSString *welcomeString = [NSString stringWithFormat:@"%@ %@",[self welcomeStringForCurrentTime],[AppDelegate sharedInstance].viewModel.app.loggedInUser.firstName];
    //UILabel *helloLabel = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 20.0, frameWidth-30.0, 44.0)];
    UILabel *helloLabel = [[UILabel alloc] init];
    helloLabel.numberOfLines = 0;
    helloLabel.lineBreakMode = NSLineBreakByWordWrapping;
    UIFont *btFont = [UIFont fontWithName:kBtFontLight size:36];
    [helloLabel setFont:btFont];
    [helloLabel setText:welcomeString];
    helloLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    [helloView addSubview:helloLabel];
    
    [helloView addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:helloLabel attribute:NSLayoutAttributeLeading
                                                             relatedBy:NSLayoutRelationEqual toItem:helloView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:15.0],
                                [NSLayoutConstraint constraintWithItem:helloLabel attribute:NSLayoutAttributeTrailing
                                                             relatedBy:NSLayoutRelationEqual toItem:helloView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-15.0],
                                [NSLayoutConstraint constraintWithItem:helloLabel attribute:NSLayoutAttributeTop
                                                             relatedBy:NSLayoutRelationEqual toItem:helloView attribute:NSLayoutAttributeTop multiplier:1.0 constant:20.0],
                                [NSLayoutConstraint constraintWithItem:helloLabel attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual toItem:helloView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:20.0],
                                [NSLayoutConstraint constraintWithItem:helloLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:44.0]
                                ]];
    
    [self.topView addSubview:helloView];
    
    [self.topView addConstraints:@[
                                   [NSLayoutConstraint constraintWithItem:helloView attribute:NSLayoutAttributeLeading
                                                                relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                   [NSLayoutConstraint constraintWithItem:helloView attribute:NSLayoutAttributeTrailing
                                                                relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0],
                                   [NSLayoutConstraint constraintWithItem:helloView attribute:NSLayoutAttributeTop
                                                                relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0],
                                   [NSLayoutConstraint constraintWithItem:helloView attribute:NSLayoutAttributeWidth
                                                                relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeWidth multiplier:1.0 constant:1.0],
                                   [NSLayoutConstraint constraintWithItem:helloView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:helloFrameHeight]
                                   ]];
    
    [helloView setNeedsLayout];
    [helloView layoutIfNeeded];
    
    //yPos += helloFrameHeight;
    UIView *previousView = helloView;
    
    if (self.viewModel.user.arrayOfCugs != nil && self.viewModel.user.arrayOfCugs.count > 1) {
        
        // Add the group picker
        _accountDropdownView = [[[NSBundle mainBundle] loadNibNamed:@"BTAccountLocationDropdownView" owner:self options:nil] firstObject];
        _accountDropdownView.locationLabel.text = _currentCug.cugName;
        _accountDropdownView.translatesAutoresizingMaskIntoConstraints = NO;
        
        //    CGRect accountFrame = _accountDropdownView.frame;
        //    accountFrame.origin.y = yPos;
        //    [_accountDropdownView setFrame:accountFrame];
        
        [self.topView addSubview:_accountDropdownView];
        
        [self.topView addConstraints:@[
                                       [NSLayoutConstraint constraintWithItem:_accountDropdownView attribute:NSLayoutAttributeLeading
                                                                    relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                       [NSLayoutConstraint constraintWithItem:_accountDropdownView attribute:NSLayoutAttributeTrailing
                                                                    relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0],
                                       [NSLayoutConstraint constraintWithItem:_accountDropdownView attribute:NSLayoutAttributeTop
                                                                    relatedBy:NSLayoutRelationEqual toItem:previousView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0],
                                       [NSLayoutConstraint constraintWithItem:_accountDropdownView attribute:NSLayoutAttributeTop
                                                                    relatedBy:NSLayoutRelationEqual toItem:helloLabel attribute:NSLayoutAttributeBottom multiplier:1.0 constant:10.0],
                                       [NSLayoutConstraint constraintWithItem:_accountDropdownView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:cugSelectionHeight]
                                       ]];
        
        UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(groupSelectionButtonPressed:)];
        [_accountDropdownView addGestureRecognizer:tapped];
        
        //yPos += cugSelectionHeight;
        previousView = _accountDropdownView;
    }
    
    //    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 109, self.topView.frame.size.width, 1.0)];
    UIView *separatorView = [[UIView alloc] init];
    separatorView.backgroundColor = [UIColor colorForHexString:@"eeeeee"];//[BrandColours colourBtNeutral30];
    separatorView.translatesAutoresizingMaskIntoConstraints = NO;
    [previousView addSubview:separatorView];
    
    [previousView addConstraints:@[
                                   [NSLayoutConstraint constraintWithItem:separatorView attribute:NSLayoutAttributeLeading
                                                                relatedBy:NSLayoutRelationEqual toItem:previousView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                   [NSLayoutConstraint constraintWithItem:separatorView attribute:NSLayoutAttributeTrailing
                                                                relatedBy:NSLayoutRelationEqual toItem:previousView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0],
                                   [NSLayoutConstraint constraintWithItem:separatorView attribute:NSLayoutAttributeBottom
                                                                relatedBy:NSLayoutRelationEqual toItem:previousView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0],
                                   [NSLayoutConstraint constraintWithItem:separatorView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:1.0]
                                   ]];
    if (previousView == helloView) {
        [previousView addConstraint:[NSLayoutConstraint constraintWithItem:separatorView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:helloLabel attribute:NSLayoutAttributeBottom multiplier:1.0 constant:19.0]];
    }
    
    NSArray *gridModelArray = [self.viewModel gridModelArray];
    for (HomeGridViewModel *gridModel in gridModelArray) {
        /*HomeScreenGridView */gridView = [[[NSBundle mainBundle] loadNibNamed:@"HomeScreenGridView" owner:self options:nil] firstObject];
        
        //CGRect frame = CGRectMake(xPos,yPos,frameWidth,frameHeight);
        //gridView.frame = frame;
        gridView.translatesAutoresizingMaskIntoConstraints = NO;
        gridView.delegate = self;
        [gridView updateWithGridViewModel:gridModel];
        
        [self.topView addSubview:gridView];
        
        [self.topView addConstraints:@[
                                       [NSLayoutConstraint constraintWithItem:gridView attribute:NSLayoutAttributeLeading
                                                                    relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                       [NSLayoutConstraint constraintWithItem:gridView attribute:NSLayoutAttributeTrailing
                                                                    relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0],
                                       [NSLayoutConstraint constraintWithItem:gridView attribute:NSLayoutAttributeTop
                                                                    relatedBy:NSLayoutRelationEqual toItem:previousView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0],
                                       [NSLayoutConstraint constraintWithItem:gridView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:frameHeight]
                                       ]];
        
        if (previousView == helloView) {
            [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:gridView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:helloLabel attribute:NSLayoutAttributeBottom multiplier:1.0 constant:12.0]];
        }
        
        previousView = gridView;
        
        //yPos += frameHeight;
    }
    
    [self.topView addConstraint:[NSLayoutConstraint constraintWithItem:previousView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.topView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    [self.topView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [self.containerScrollView addSubview:self.topView];
    
    [self.containerScrollView addConstraints:@[
                                               [NSLayoutConstraint constraintWithItem:self.topView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.containerScrollView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0],
                                               [NSLayoutConstraint constraintWithItem:self.topView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.containerScrollView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0],
                                               [NSLayoutConstraint constraintWithItem:self.topView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.containerScrollView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                               [NSLayoutConstraint constraintWithItem:self.topView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.containerScrollView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0],
                                               [NSLayoutConstraint constraintWithItem:self.topView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.containerScrollView attribute:NSLayoutAttributeWidth multiplier:1.0 constant:1.0]
                                               ]];
    
    [self.containerScrollView setNeedsLayout];
    [self.containerScrollView layoutIfNeeded];
    [self.containerScrollView setAlwaysBounceHorizontal:NO];
    
    
}

- (void)addBottomGridTextButtons
{
    NSArray *titles = @[@"Help",@"Usage",@"Account",@"Public wi-fi"];
    
    CGFloat xPadding = kXpadding;
    CGFloat yPadding = kXpadding;
    CGFloat xPos = 0.0;
    CGFloat yPos = 173.0;
    CGFloat buttonHeight = 50.0;
    
    CGFloat xMargin = 0.0;
    CGFloat buttonWidth = (self.bottomView.frame.size.width - 2*xMargin - 3*xPadding)/2;
    
    CGFloat yMargin = 0.0;//(self.bottomView.frame.size.height - 2*buttonHeight - 3*yPadding);
    
    xPos = xMargin;
    yPos = yMargin;
    
    for (NSString *buttonTitle in titles) {
        xPos += xPadding;
        if ([titles indexOfObject:buttonTitle] %2 == 0) {
            yPos += yPadding;
        }
        UIButton *gridButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [gridButton setFrame:CGRectMake(xPos, yPos, buttonWidth, buttonHeight)];
        [gridButton setTitle:buttonTitle forState:UIControlStateNormal];
        [gridButton setTitleColor:[UIColor colorForHexString:@"666666"] forState:UIControlStateNormal];
        [gridButton.titleLabel setFont:[UIFont fontWithName:kBtFontBold size:15.0]];
        [gridButton setBackgroundColor:[UIColor colorForHexString:@"DDDDDD"]];
        [gridButton.layer setCornerRadius:4.0];
        [gridButton addTarget:self action:@selector(tappedGridButton:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.bottomView addSubview:gridButton];
        
        if ([titles indexOfObject:buttonTitle] %2 == 0) {
            xPos += buttonWidth;
        } else {
            xPos = xMargin;
            yPos += buttonHeight;
        }
        
    }
}

- (void)updateUserAccountNotificationsData
{
    NSString *groupKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    if(self.currentCug.groupKey && ![self.currentCug.groupKey isEqualToString:groupKey]){
        
        //Update Notifications Data with Current Cug Selection
        [self noUnReadItemsLeftInNotifications];
        
        [self.viewModel cancelGetUserAccountNotificationsAPICall];
        [self.viewModel fetchUserGroupNotifications];
    }
}

- (void)updateGridsIfNeeded{
    
    NSString *groupKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    if(self.currentCug.groupKey && ![self.currentCug.groupKey isEqualToString:groupKey]){
        
        //Update Grids with Current Cug Selection
        
        [self setUpGridsUI];
        [self.viewModel cancelAPIs];
        [self.viewModel fetchGridData];
    }
    
}


/*
 Cug selection view is a part of navigation bar on home screen.
 */
- (void)updateNavigationItemsForHomeScreen
{
    
    NSArray *sortedArrayOfCugs = self.viewModel.user.arrayOfCugs;
    
    if(!sortedArrayOfCugs || [sortedArrayOfCugs count] <= 1){
        
        NSString *userName = [AppDelegate sharedInstance].viewModel.app.loggedInUser.firstName;
        _pageNameForOmniture = OMNIPAGE_HOME;
        if(userName){
            
            //NSString *welcomeMessage = [NSString stringWithFormat:@"Welcome, %@", userName];
            
            //[self setTitle:welcomeMessage];
            self.navigationController.tabBarItem.title = @"Home";
        }
        
        //Remove this only for testing porpose
        //self.navigationItem.rightBarButtonItem = nil;
        //(VRK) commenting interceptor invisible button.
        //[self addRightNavButton];
    }
    else{
        
        _pageNameForOmniture = OMNIPAGE_MULTICUG;
        
        BTCug *cug = (BTCug *)[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug;
        NSString *groupName = cug.cugName;
        
        [self setTitle:groupName];
        self.navigationController.tabBarItem.title = @"Home";
        [self updateGridsIfNeeded];
        self.groupKey = cug.groupKey;
        
        NSMutableArray *buttonArray = [NSMutableArray arrayWithArray:self.navigationItem.rightBarButtonItems];
        
        //        [buttonArray addObject:[self getCugSelectionItemButton]];
        
        self.navigationItem.rightBarButtonItems = buttonArray;
        
    }
    
}


#pragma mark - Fetch user details methods
- (void)fetchUserDetails {
    // (LP) Ask to viewModel to provide user details. The viewModel takes care of fetching data.
    [self.viewModel fetchUserDetails];
    self.networkRequestInProgress = YES;
    [self showLoaderView];
    [self hideRetryView];
}

#pragma mark - Fetch user details methods
- (void)fetchUserAccountNotifications {
    
    // (LP) Ask to viewModel to provide notifications. The viewModel takes care of fetching data.
    [self.viewModel fetchUserGroupNotifications];
    
}

- (void)createLoaderImageView
{
    if(_loaderView){
        [_loaderView removeFromSuperview];
    }
    _loaderView = [[CustomSpinnerAnimationView alloc] initWithText:@"Retrieving your details..." loadingImage:[UIImage imageNamed:@"Spinner"] andLoaderImageSize:40.0f];
    
    _loaderView.frame = CGRectMake(0, 0, self.spinnerLoadingView.frame.size.width, self.spinnerLoadingView.frame.size.height);
    _loaderView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.spinnerLoadingView addSubview:_loaderView];
    
    /*[self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
     [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
     [_loaderView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:100]];
     [_loaderView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:155]];
     _loaderView.translatesAutoresizingMaskIntoConstraints = NO;
     
     _loadingLabel = [[UILabel alloc] init];
     _loadingLabel.text = @"Retrieving your details...";
     _loadingLabel.textColor = [UIColor blackColor];
     _loadingLabel.font = [UIFont fontWithName:kBtFontRegular size:15];
     _loadingLabel.numberOfLines = 1;
     _loadingLabel.translatesAutoresizingMaskIntoConstraints = NO;
     _loadingLabel.textAlignment = NSTextAlignmentCenter;
     
     [self.view addSubview:_loadingLabel];
     
     [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
     [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_loaderView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:10.0f]];*/
    
}

- (void)showLoaderView
{
    [self createLoaderImageView];
    
    //_loaderView.hidden = NO;
    //_loadingLabel.hidden = NO;
    self.spinnerLoadingView.hidden = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_loaderView startAnimation];
    });
}

- (void)hideLoaderView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [_loaderView stopAnimation];
    });
    
    //_loaderView.hidden = YES;
    //_loadingLabel.hidden = YES;
    
    [_loaderView removeFromSuperview];
   // [_loadingLabel removeFromSuperview];
    
    //_loadingLabel = nil;
    _loaderView = nil;
     self.spinnerLoadingView.hidden = YES;
    
}

- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow
{
    _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
    _retryView.translatesAutoresizingMaskIntoConstraints = NO;
    _retryView.retryViewDelegate = self;
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
    
    [self.view addSubview:_retryView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    if(self.topView){
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.topView  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    } else{
         [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    }
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
}

- (void)hideRetryView
{
    [_retryView setRetryViewDelegate:nil];
    [_retryView setHidden:YES];
    [_retryView removeFromSuperview];
    _retryView = nil;
}

#pragma mark - BTRetryView delegate methods
- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView {
    
    if ([AppManager isInternetConnectionAvailable])
    {
        DDLogInfo(@"Home: Trying to reload user details.");
        [self fetchUserDetails];
    }
    else
    {
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_HOME];
        [_retryView updateRetryViewWithInternetStrip:YES];
    }
}

- (void)showGridView:(BOOL)isVisible
{
    self.topView.hidden = !isVisible;
    self.bottomView.hidden  = !isVisible;
}

- (void)checkForUpdatedScreenTitle
{
    [self updateScreenTitle];
}

- (void)updateScreenTitle{
    
    
    NSArray *sortedArrayOfCugs = self.viewModel.user.arrayOfCugs;
    
    if(!sortedArrayOfCugs || [sortedArrayOfCugs count] <= 1){
        
        NSString *userName = [AppDelegate sharedInstance].viewModel.app.loggedInUser.firstName;
        
        if(userName){
            
            //NSString *welcomeMessage = [NSString stringWithFormat:@"Welcome, %@", userName];
            //[self setTitle:welcomeMessage];
            [self setTitle:@"BT Business"];
            self.navigationController.tabBarItem.title = @"Home";
        }
        
    }
    else{
        
        CDCug *cug = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug;
        [self setTitle:cug.cugName];
        self.navigationController.tabBarItem.title = @"Home";
        
        [self updateGridsIfNeeded];
        [self updateUserAccountNotificationsData];
        self.groupKey = cug.groupKey;
        
        _currentCug = [[BTCug alloc] initWithCugName:cug.cugName cugID:cug.cugId groupKey:cug.groupKey cugRole:[cug.cugRole integerValue] indexInAPIResponse:[cug.indexInAPIResponse integerValue] andRefKey:cug.refKey andContactId:cug.contactId];
    }
    
}


- (UIBarButtonItem *)getCugSelectionItemButton{
    
    //Salman
    UIImage *groupSelectionImage = [UIImage imageNamed:@"Group_Selection_White.png"];
    UIButton *groupSelectionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    groupSelectionButton.accessibilityLabel = @"Group selection";
    [groupSelectionButton setImage:groupSelectionImage forState:UIControlStateNormal];
    groupSelectionButton.frame = CGRectMake(0, 0, 30, 20);
    [groupSelectionButton addTarget:self action:@selector(groupSelectionButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:groupSelectionButton];
    return barButtonItem;
}

- (void)showGroupSelection{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Select Group" preferredStyle:UIAlertControllerStyleActionSheet];
    
    DDLogVerbose(@"Home: Total cugs belongs to user %lu.",(unsigned long)self.viewModel.user.arrayOfCugs.count);
    
    // (LP) Add group names in actionsheet
    if (self.viewModel.user.arrayOfCugs != nil && self.viewModel.user.arrayOfCugs.count > 0) {
        
        NSArray *sortedArrayOfCugs = [self.viewModel.user.arrayOfCugs sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"indexInAPIResponse" ascending:YES]]];
        
        // (LP) Add action for each cug selection.
        int index = 0;
        
        for (BTCug *groupData in sortedArrayOfCugs) {
            
            __weak typeof(self) weakSelf = self;
            UIAlertAction *action = [UIAlertAction actionWithTitle:groupData.cugName
                                                             style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                 [weakSelf trackOmnitureClickEvent:OMNICLICK_CUG_CHANGE onPage:OMNIPAGE_MULTICUG];
                                                                 [weakSelf checkForSuperUserAndUpdateUIWithIndex:groupData.indexInAPIResponse];
                                                                 self->_accountDropdownView.locationLabel.text = groupData.cugName;
                                                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                                             }];
            [alert addAction:action];
            
            index++;
        }
    }
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    [alert addAction:cancel];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = _accountDropdownView.frame;
    alert.popoverPresentationController.canOverlapSourceViewRect = NO;
    [self presentViewController:alert animated:YES completion:^{
        
    }];
    
}

#pragma mark - Home screen Button action methods
/*
 Handles navigating to View Assets screen
 */
- (void)redirectToMyServiceScreen {
    BTAssetsDashboardViewController *assestsContainerViewController = [BTAssetsDashboardViewController getAssetsDashboardViewController];
    
    DDLogInfo(@"Home: Presenting view assets screen.");
    NSArray *sortedArrayOfCugs = [self.viewModel.user.arrayOfCugs sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"indexInAPIResponse" ascending:YES]]];
    [assestsContainerViewController setCugs:sortedArrayOfCugs];
    assestsContainerViewController.groupKey = self.viewModel.user.currentSelectedCug.groupKey;
    [self.navigationController pushViewController:assestsContainerViewController animated:NO];
}

/*
 Handles navigating to Help screen
 */
- (void)redirectToSupportArticlesScreen {
    
    BTHelpAndSupportViewController *helpAndSupportController = [BTHelpAndSupportViewController getHelpAndSupportViewController];
    helpAndSupportController.isLaunchingFromHomeScreen = YES;
    
    [self.navigationController pushViewController:helpAndSupportController animated:YES];
    
}

/*
 Handles navigating to Service status screen
 */
- (void)redirectToServiceStatusScreen {
    
    BTServiceStatusDashboardViewController *serviceStatusDashboradController = [BTServiceStatusDashboardViewController getServiceStatusDashboardViewController];
    NSArray *sortedArrayOfCugs = [self.viewModel.user.arrayOfCugs sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"indexInAPIResponse" ascending:YES]]];
    [serviceStatusDashboradController setCugs:sortedArrayOfCugs];
    [serviceStatusDashboradController setTitle:@"Service status"];
    
    [self.navigationController pushViewController:serviceStatusDashboradController animated:YES];
}

/*
 Handles navigating to track fault screen
 */
- (void)redirectToTrackFaultScreen {
    
    BTFaultTrackerDashboardViewController *faultDashboardViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"trackFaultDashboardScene"];
    
    faultDashboardViewController.firstPageOpenFaultDataDict = [_viewModel getFaultsDataDictionary];
    
    NSArray *sortedArrayOfCugs = [self.viewModel.user.arrayOfCugs sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"indexInAPIResponse" ascending:YES]]];
    [faultDashboardViewController setCugs:sortedArrayOfCugs];
    [self.navigationController pushViewController:faultDashboardViewController animated:YES];
}

- (void)redirectToHomeScreen {
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];
    BTHomeViewController *homeViewController = (BTHomeViewController *)[storyboard instantiateViewControllerWithIdentifier:kStoryboardViewHome];
    homeViewController.userDetails = _userDetails;
    [self.navigationController pushViewController:homeViewController animated:YES];
}

/*
 Handles navigating to track order screen
 */
- (void)redirectToTrackAnOrderScreen {
    
    //    BTOrderTrackerDashboardViewController *trackOrderViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"orderDashboardScene"];
    BTOrderTrackerDashboardViewController *trackOrderViewController = [BTOrderTrackerDashboardViewController getOrderTrackerDashboardViewController];
    //    BTOrderDashboardTableViewController *trackOrderViewController = [BTOrderDashboardTableViewController getOrderDashboard];
    trackOrderViewController.firstPageOpenOrderDataDict = [_viewModel getOrdersDataDictionary];
    
    DDLogInfo(@"Home: Pushing track order screen.");
    NSArray *sortedArrayOfCugs = [self.viewModel.user.arrayOfCugs sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"indexInAPIResponse" ascending:YES]]];
    trackOrderViewController.cugs = sortedArrayOfCugs;
    [trackOrderViewController setCugs:sortedArrayOfCugs];
    [self.navigationController pushViewController:trackOrderViewController animated:YES];
}

/*
 Handles navigating to Bill summary screen
 */
- (void)redirectToLatestBillScreen {
    
    BTBillsDashBoardViewController *billsDashBoardViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"billSummaryScene"];
    
    billsDashBoardViewController.billsDataDictionary = [_viewModel getBillsDataDictionary];
    billsDashBoardViewController.groupKey = self.viewModel.user.currentSelectedCug.groupKey;
    // NSArray *sortedArrayOfCugs = [self.viewModel.user.arrayOfCugs sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"indexInAPIResponse" ascending:YES]]];
    // [billsDashBoardViewController setCugs:sortedArrayOfCugs];
    
    [self.navigationController pushViewController:billsDashBoardViewController animated:NO];
    
}


- (void)redirectToSettingsScreen{
    
    BTSettingsViewController *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"settingScene"];
    
    settingsViewController.groupsArray = self.viewModel.user.arrayOfCugs;
    [self.navigationController pushViewController:settingsViewController animated:YES];
    
}

- (void)redirectToMoreBTBusinessAppsScreen{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CloudVoice" bundle:nil];
    BTSettingsViewController *settingsViewController = [storyboard instantiateViewControllerWithIdentifier:@"moreAppsScene"];
    [self.navigationController pushViewController:settingsViewController animated:YES];
    
}

- (void)redirectToLoggerScreen {
    
    UINavigationController *loggerController = [self.storyboard instantiateViewControllerWithIdentifier:@"loggerNavigationController"];
    [self presentViewController:loggerController animated:YES completion:nil];
}



- (void)redirectToUsageScreen
{
    BTUsageDashboardViewController *controller = [BTUsageDashboardViewController getUsageDashboardViewController];
    NSArray *sortedArrayOfCugs = [self.viewModel.user.arrayOfCugs sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"indexInAPIResponse" ascending:YES]]];
    NSString *groupKey = self.viewModel.user.currentSelectedCug.groupKey;
    if (!sortedArrayOfCugs) {
        if ([self.userDetails objectForKey:@"user"] && [[self.userDetails objectForKey:@"user"] isKindOfClass:[BTUser class]]) {
            BTUser *user = (BTUser *)[self.userDetails objectForKey:@"user"];
            sortedArrayOfCugs = [user.arrayOfCugs sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"indexInAPIResponse" ascending:YES]]];
        }
    }
    if (!groupKey) {
        if ([self.userDetails objectForKey:@"user"] && [[self.userDetails objectForKey:@"user"] isKindOfClass:[BTUser class]]) {
            BTUser *user = (BTUser *)[self.userDetails objectForKey:@"user"];
            groupKey = user.currentSelectedCug.groupKey;
            if (!groupKey && sortedArrayOfCugs) {
                groupKey = [(BTCug *)sortedArrayOfCugs[0] groupKey];
            }
        }
    }
    [controller setCugs:sortedArrayOfCugs];
    [controller setGroupKey:groupKey];
    [self.navigationController pushViewController:controller animated:NO];
}

- (void)redirectToSpeedTestScreen
{
    SpeedTestIntroductionViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"speedTestIntroductionScene"];
    [controller setCugs:self.viewModel.user.arrayOfCugs];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)redirectToPublicWifiScreen
{
    NSArray *sortedArrayOfCugs = [self.viewModel.user.arrayOfCugs sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"indexInAPIResponse" ascending:YES]]];
    
    BTAccountSelectTableViewController *accountsForPublicWifi = [[BTAccountSelectTableViewController alloc] initWithStyle:UITableViewStylePlain];
    [accountsForPublicWifi setContext:BTAccountSelectContextPublicWifi];
    [accountsForPublicWifi setCugs:sortedArrayOfCugs];
    [accountsForPublicWifi setCurrentUser:self.viewModel.user];
    [accountsForPublicWifi setGroupKey:self.viewModel.user.currentSelectedCug.groupKey];
    
    BTProductSelectTableViewController *productsForPublicWiFi = [[BTProductSelectTableViewController alloc] initWithStyle:UITableViewStylePlain];
    [productsForPublicWiFi setContext:BTAccountSelectContextPublicWifi];
    [productsForPublicWiFi setCurrentUser:self.viewModel.user];
    [productsForPublicWiFi setSelectedAsset:nil];
    [productsForPublicWiFi setGroupKey:self.viewModel.user.currentSelectedCug.groupKey];
    [productsForPublicWiFi setCugs:sortedArrayOfCugs];
    
    [self.navigationController pushViewController:productsForPublicWiFi animated:YES];
    
}

 - (void)redirectToReportaFault
{
   /* BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
   
    viewController.targetURLType = BTTargetURLTypeReportAFaultHomeScreen;
    
    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:NO andIsNeedCloseButton:YES];
    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{
        
    }];*/

            [OmnitureManager trackPage:OMNICLICK_HOME_REPORTAFAULT withContextInfo:[AppManager getOmniDictionary]];
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Select the product you’re having a problem with:" preferredStyle:UIAlertControllerStyleActionSheet];
            
            UIAlertAction *actionBroadBand = [UIAlertAction actionWithTitle:@"Broadband"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                                          [self showWebviewForLiveChatOptionsWith:@"Broadband"];
                                                                          
                                                                      }];
            [alert addAction:actionBroadBand];
            
            UIAlertAction *actionPhoneline = [UIAlertAction actionWithTitle:@"Phone line"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                                          [self showWebviewForLiveChatOptionsWith:@"Phone line"];
                                                                      }];
            [alert addAction:actionPhoneline];
            
            UIAlertAction *actionEmail = [UIAlertAction actionWithTitle:@"Email"
                                                                  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                      [alert dismissViewControllerAnimated:YES completion:nil];
                                                                      [self showWebviewForLiveChatOptionsWith:@"Email"];
                                                                  }];
            [alert addAction:actionEmail];
            
            UIAlertAction *actionBTCloudVoice = [UIAlertAction actionWithTitle:@"BT Cloud Voice"
                                                                         style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                                             [self showWebviewForLiveChatOptionsWith:@"BT Cloud Voice"];
                                                                         }];
            [alert addAction:actionBTCloudVoice];
            
            
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                             style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                                             }];
            [alert addAction:cancel];
            
            alert.popoverPresentationController.sourceRect = gridView.frame;
            alert.popoverPresentationController.canOverlapSourceViewRect = NO;
            alert.popoverPresentationController.sourceView = self.view;
            [self presentViewController:alert animated:YES completion:^{
                
            }];
}


- (void)showWebviewForLiveChatOptionsWith:(NSString *)screenName {
    
    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
    
    if([screenName isEqualToString:@"Broadband"]) {
        viewController.targetURLType = BTTargetURLTYPEReportAFaultBroadband;
    }
    else if([screenName isEqualToString:@"Phone line"]) {
        viewController.targetURLType = BTTargetURLTYPEReportAFaultPhoneline;
    }
    else if([screenName isEqualToString:@"Email"]) {
        viewController.targetURLType = BTTargetURLTYPEReportAFaultEmail;
    }
    else if([screenName isEqualToString:@"BT Cloud Voice"]) {
        viewController.targetURLType = BTTargetURLTYPEReportAFaultBTCloudVoice;
    }
    
    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:NO andIsNeedCloseButton:YES];
    
    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{
        
    }];
}


#pragma mark - button action methods

/*
 Handles navigating to Settings screen
 */
- (void)settingsButtonAction:(id)sender {
    
    BTSettingsViewController *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"settingScene"];
    
    settingsViewController.groupsArray = self.viewModel.user.arrayOfCugs;
    
    //  UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:settingsViewController];
    // [navController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    
    //  DDLogInfo(@"Home: Presenting view settings screen.");
    // [self presentViewController:navController animated:YES completion:nil];
    
    [self.navigationController pushViewController:settingsViewController animated:YES];
    
}


- (void)groupSelectionButtonPressed:(id)sender{
    
    //Track PAge
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = _pageNameForOmniture;
    [data setValue:@"Not Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
    
    [self showGroupSelection];
}

#pragma mark - company list button methods

/*
 Displaying user related group names in list
 */
- (void)showCompanyNameListButton:(id)sender {
    
    [self showGroupSelection];
}

#pragma mark UI Related Methods

/*
 Updates the UI after getting user details
 */
- (void)updateUserInterface {
    
    // (LP) Update navigation bar items
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor whiteColor]];
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
    
    self.title = @"BT Business";
    
    [self updateNavigationItemsForHomeScreen];
    
    [self hideAndShowTheControlsIsHide:NO];
    
    // (LP) Check stored user preference of cug selection
    
    NSString *groupName = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugName;
    NSUInteger groupIndex = -1;
    if(groupName != nil)
    {
        groupIndex = [self.viewModel.user indexOfCugWithCugName:groupName];
    }
    else
    {
        DDLogInfo(@"Home: User preference for group selection is not saved in persistence");
    }
    
    // (LP) initial company list table setup
    if (groupIndex == -1) {
        [self checkForSuperUserAndUpdateUIWithIndex:0];
    } else {
        [self checkForSuperUserAndUpdateUIWithIndex:groupIndex];
    }
    
}

/*
 Updates the currently selected group and UI on the basis of group selection
 */
- (void) checkForSuperUserAndUpdateUIWithIndex:(NSUInteger)index {
    
    // Get the sorted array
    NSArray *sortedArrayOfCugs = [self.viewModel.user.arrayOfCugs sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"indexInAPIResponse" ascending:YES]]];
    
    // (LP) Update currently selected group data in persistent settings
    BTCug *cugData = [sortedArrayOfCugs objectAtIndex:index];
    
    // (lp) Change current selected cug locally and in persistence.
    [self.viewModel changeSelectedCUGTo:cugData];
    
    DDLogInfo(@"Home: Updated the current selected cug in persistence");
    
    CDCug *cug = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug;
    
    if (_currentCug != nil) {
        if (![_currentCug.groupKey isEqualToString:cugData.groupKey]) {
            [self updateScreenTitle];
            _isErrorHandlingInProgressForAnotherAPI = NO;
        }
    }
    else
    {
        _currentCug = [[BTCug alloc] initWithCugName:cug.cugName cugID:cug.cugId groupKey:cug.groupKey cugRole:[cug.cugRole integerValue] indexInAPIResponse:[cug.indexInAPIResponse integerValue] andRefKey:cug.refKey andContactId:cug.contactId];
    }
    
}

/*
 Handles hiding of button views
 */
- (void) hideAndShowTheControlsIsHide:(BOOL)ishide {
    
    [self showGridView:!ishide];
    
}

#pragma mark APNS Methods
//Right now this method never gets called.
- (void) launchRespectiveScreenWithDictionary:(NSDictionary *)apnsDict {
    
    [self dismissPresentedController];
    if ([[apnsDict objectForKey:@"aps"] objectForKey:@"type"]) {
        
        if ([[[apnsDict objectForKey:@"aps"] objectForKey:@"type"] isEqualToString:@"Order"]) {
            BTOrderTrackerDetailsViewController *orderSummaryController = (BTOrderTrackerDetailsViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"OrderTrackerDetailViewController"];
            [orderSummaryController setOrderRef:@"BTDKN999"];
            [orderSummaryController setIsFromAPNS:YES];
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:orderSummaryController];
            [navController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
            [self presentViewController:navController animated:YES completion:NULL];
        } else if ([[[apnsDict objectForKey:@"aps"] objectForKey:@"type"] isEqualToString:@"Fault"]) {
            BTFaultTrackerSummaryViewController *faultSummaryController = (BTFaultTrackerSummaryViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"FaultSummaryScene"];
            [faultSummaryController setFaultReference:@"1-190258429"];
            [faultSummaryController setIsFromAPNS:YES];
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:faultSummaryController];
            [navController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
            [self presentViewController:navController animated:YES completion:NULL];
        }
        
    }
}

#pragma mark PrivateHelperMethods

//(VRK) Dismiss viewcontrollers which are already launched when pushnotification comes.
- (void) dismissPresentedController {
    
    if (self.presentedViewController) {
        [[self presentedViewController] dismissViewControllerAnimated:false completion:nil];
    }
    
}


- (void)populateHomeScreenUI{
    
    // (LP) hide loading retry buttons
    [self hideLoaderView];
    
    
    //(SL)showing grids
    [self showGridView:YES];
    
    // (LP) Update UI with fetched user details
    [self updateUserInterface];
    
    DDLogInfo(@"Home: User details fetched successfully");
    
    [self setUpGridsUI];
    [self.viewModel fetchGridData];
    
    self.isUIPopulated = YES;
    
}


//Method to handle the error of parrallel APIs which redirects to another screen
- (void)handleWebServiceError:(NLWebServiceError *)webServiceError{
    
    //Handle error cases of redirecting to login/pin screen if there is not scheduled redirection for another API
    if(!_isErrorHandlingInProgressForAnotherAPI){
        
        BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
        
        if(errorHandled){
            
            _isErrorHandlingInProgressForAnotherAPI = YES;
        }
    }
    
}

#pragma mark- HomeScreenGridViewDelegate Method

- (void)homeScreenGridView:(HomeScreenGridView *)view didSelectForTitle:(NSString *)titleString{
    if ([titleString isEqualToString:kTrackOrdersText])
    {
        [self trackOmnitureClickEvent:OMNICLICK_HOME_TRACK_ORDERS onPage:_pageNameForOmniture];
        [self redirectToTrackAnOrderScreen];
    }
    else if([titleString isEqualToString:kBillsText])
    {
        [self trackOmnitureClickEvent:OMNICLICK_HOME_BILLS onPage:_pageNameForOmniture];
        //[self redirectToLatestBillScreen];
        [self.tabBarController setSelectedIndex:billsTag];
    }
    else if([titleString isEqualToString:kTrackFaultsText])
    {
        [self trackOmnitureClickEvent:OMNICLICK_HOME_TRACK_FAULTS onPage:_pageNameForOmniture];
        [self redirectToTrackFaultScreen];
    }
    else if([titleString isEqualToString:kServiceStatusText])
    {
        [self trackOmnitureClickEvent:OMNICLICK_HOME_SERVICE onPage:_pageNameForOmniture];
        [self redirectToServiceStatusScreen];
    }
    else if([titleString isEqualToString:kBroadbandSpeedTest])
    {
        [self trackOmnitureClickEvent:OMNICLICK_HOME_BROADBAND_SPEED_TEST onPage:_pageNameForOmniture];
        [self redirectToSpeedTestScreen];
    }
    else if([titleString isEqualToString:kReportaFault])
    {
        [self trackOmnitureClickEvent:OMNICLICK_HOME_REPORT_A_FAULT onPage:_pageNameForOmniture];
        [self redirectToReportaFault];
    }
    else if([titleString isEqualToString:kPublicWiFiText])
    {
        [self trackOmnitureClickEvent:OMNICLICK_HOME_PUBLICWIFI onPage:_pageNameForOmniture];
        [self redirectToPublicWifiScreen];
    }
}



#pragma mark- BTUpdateContactInerceptViewControllerDelegate Method

- (void)didFinishSucessfullyOnBTUpdateContactInerceptViewController:(BTUpdateContactInerceptViewController *)controller{
    
    [self.viewModel checkForInterceptor];
}

- (void)makeUpdateContactInterceptorAPIRequestInBackgroundWithContactArray:(NSArray *)contactArray
{
    [self.viewModel makeUpdateContactInterceptorAPIRequestWithContactArray:contactArray];
}

#pragma mark - BTSecurityNumberViewControllerDelegate (1FA intercept)

- (void)successfullyUpdatedSecurityNumberOnSecurityNumberViewController:(BTSecurityNumberViewController *)securityNumberViewController
{
    [self.viewModel checkForInterceptor];
}


#pragma mark - DLMHomeScreenDelegate Methods

- (void)successfullyFetchedUserDataOnHomeScreen:(DLMHomeScreen *)homeScreen
{
    self.networkRequestInProgress = NO;
    
    if (self.viewModel.user.username) {
        
        [self.viewModel checkForInterceptor];
        
        //Comment below lines to start interceptot
        //[self populateHomeScreenUI];
        //[self fetchUserAccountNotifications];
        //[OmnitureManager trackPage:OMNIPAGE_HOME withContextInfo:data];
        
    } else {
        
        DDLogError(@"Home: Fetch User details failed");
        
        // (LP) Retry to fetch user details
        [self hideLoaderView];
        [self showRetryViewWithInternetStrip:NO];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_HOME];
    }
    
}



- (void)homeScreen:(DLMHomeScreen *)homeScreen didUpdateDataForGridViewModel:(HomeGridViewModel *)gridViewModel{
    
    
    if(gridViewModel){
        
        //Update Grids
        for(UIView *view in [self.topView subviews]){
            
            if([view isKindOfClass:[HomeScreenGridView class]]){
                
                if([gridViewModel.titleText isEqualToString:[(HomeScreenGridView *)view getTitle]]){
                    
                    [(HomeScreenGridView *)view updateWithGridViewModel:gridViewModel];
                    break;
                }
            }
            
        }
    }
    
    
}


- (void)homeScreen:(DLMHomeScreen *)homeScreen failedToFetchGridDataWithWebServiceError:(NLWebServiceError *)webServiceError{
    
    (webServiceError.error.code == BTNetworkErrorCodeAPINoDataFound)?[AppManager trackNoDataFoundErrorOnPage:OMNIPAGE_HOME]:[AppManager trackGenericAPIErrorOnPage:OMNIPAGE_HOME];
    [self handleWebServiceError:webServiceError];
    
}


- (void)homeScreen:(DLMHomeScreen *)homeScreen failedToFetchUserDataWithWebServiceError:(NLWebServiceError *)error
{
    self.networkRequestInProgress = NO;
    BOOL errorHandled = [self attemptVordelProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if(errorHandled == NO)
    {
        // (LP) Retry to fetch user details
        [self showRetryViewWithInternetStrip:NO];
        [self hideLoaderView];
    }
    [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_HOME];
}

- (void)homeScreen:(DLMHomeScreen *)homeScreen hasPendingIntercenptorWithType:(InterceptorType)interceptorType{
    
    switch (interceptorType) {
            
        case InterceptorTypeFirstTimeLogin:
        {
            //Set user name in Appmanager so that if First Time login intercepter come after login email will prefill in sign in screen. RLM
            [AppManager sharedAppManager].usernameForSignInIntercepter = [AppDelegate sharedInstance].viewModel.app.loggedInUser.username;
            NSString *question = [AppDelegate sharedInstance].viewModel.app.loggedInUser.securityQuestion;
            if (question && [question isEqualToString:@"1FA"]) {
                [self redirectToUpdate1FAInterceptor];
            } else {
                [self redirectToFirstTimeLoginIntercepter];
            }
        }
            break;
            
        case InterceptorTypeCreatePassword:
        {
            //Set user name in Appmanager so that if First Time login intercepter come after login email will prefill in sign in screen. //RLM
            [AppManager sharedAppManager].usernameForSignInIntercepter = [AppDelegate sharedInstance].viewModel.app.loggedInUser.username;
            
            [self redirectToCreatePasswordIntercepter];
        }
            break;
            
        case InterceptorTypeUpdateContact:
        {
            [self redirectToUpdateContactDetailsIntercepter];
        }
            break;
            
        case InterceptorTypeUpdateProfile:
        {
            [self redirectToUpdateProfileIntercepter];
        }
            break;
            
        case IntercepterType1FA:
        {
            // 1FA interceptor currently suppressed
            // uncomment following line AND break statement to re-enable
            //[self redirectToUpdate1FAInterceptor];
            [self redirectToUpdate1FAMigrationInterceptor];
        }
            break;
            
        case InterceptorTypeUnknown:
        {
            //if there is no pending interceptor or an unknown interceptor
            
            [self.interceptorNavigationController dismissViewControllerAnimated:YES completion:nil];
            self.interceptorNavigationController = nil;
            
            [self populateHomeScreenUI];
            [self fetchUserAccountNotifications];
            
        }
            break;
            
        default:
            break;
    }
}


- (void)successfullyFetchedNotificationDataOnHomeScreen:(DLMHomeScreen *)homeScreen
{
    
    if ([self.viewModel.arrayOfNotifications count] > 0) {
        
        for (BTNotification *notification in self.viewModel.arrayOfNotifications) {
            
            if (notification.notificationStatus == BTNotificationTypeUnRead) {
                if ([self.navigationItem.rightBarButtonItems count] > 0) {
                    UIBarButtonItem *item = [self.navigationItem.rightBarButtonItems objectAtIndex:0];
                    [item setImage:[[UIImage imageNamed:@"notification_bell"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
                }
                
                break;
            }
        }
        
    }
    
}

- (void)homeScreen:(DLMHomeScreen *)homeScreen failedToFetchNotificationsWithWebServiceError:(NLWebServiceError *)webServiceError
{
    [self handleWebServiceError:webServiceError];
    
    (webServiceError.error.code == BTNetworkErrorCodeAPINoDataFound)?[AppManager trackNoDataFoundErrorOnPage:OMNIPAGE_HOME]:[AppManager trackGenericAPIErrorOnPage:OMNIPAGE_HOME];
    
}

#pragma mark - Button click handling

- (IBAction)tappedGridButton:(id)sender
{
    if ([sender isKindOfClass:[UIButton class]]) {
        UIButton *button = (UIButton*)sender;
        [self tappedButtonWithTitle:button.titleLabel.text];
    }
}

- (void)tappedButtonWithTitle:(NSString*)titleString
{
    if([titleString isEqualToString:kAccountText])
    {
        [self trackOmnitureClickEvent:OMNICLICK_HOME_ACCOUNT onPage:_pageNameForOmniture];
        [self redirectToMyServiceScreen];
    }
    else if([titleString isEqualToString:kUsageText])
    {
        [self trackOmnitureClickEvent:OMNICLICK_HOME_USAGE onPage:_pageNameForOmniture];
        [self redirectToUsageScreen ];
    }
    else if([titleString isEqualToString:kHelpText])
    {
        [self trackOmnitureClickEvent:OMNICLICK_HOME_HELP onPage:_pageNameForOmniture];
        [self redirectToSupportArticlesScreen];
    }
    else if ([titleString isEqualToString:kPublicWiFiText])
    {
        [self trackOmnitureClickEvent:OMNICLICK_HOME_PUBLICWIFI onPage:_pageNameForOmniture];
        [self redirectToPublicWifiScreen];
    }
    else if ([titleString isEqualToString:kBroadbandSpeedTest])
    {
        [self trackOmnitureClickEvent:OMNICLICK_HOME_BROADBAND_SPEED_TEST onPage:_pageNameForOmniture];
        [self redirectToSpeedTestScreen];
    }
}


#pragma mark - Home Bottom Grid View Delegate

- (void)bottomHomeScreenView:(HomeBottomView *)view didSelectForTitle:(NSString *)titleString
{
    [self tappedButtonWithTitle:titleString];
}

#pragma mark - Home navigation controller delegate methods

- (void)redirectToDashboardScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController
{
    [self redirectToHomeScreen];
}

- (void)redirectToTrackOrdersScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController
{
    [self redirectToTrackAnOrderScreen];
}

- (void)redirectToBillsScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController
{
    [self redirectToLatestBillScreen];
}

- (void)redirectToServiceStatusScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController
{
    [self redirectToServiceStatusScreen];
}

- (void)redirectToTrackFaultsScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController
{
    [self redirectToTrackFaultScreen];
}

- (void)redirectToAccountScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController
{
    [self redirectToMyServiceScreen];
}

- (void)redirectToUsageScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController
{
    [self redirectToUsageScreen];
}

- (void)redirectToPublicWifiScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController
{
    [self redirectToPublicWifiScreen];
}

- (void)redirectToHelpScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController
{
    [self redirectToSupportArticlesScreen];
}

- (void)redirectToSettingsScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController
{
    [self redirectToSettingsScreen];
}

- (void)performLogOutActionForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController
{
    [self showAlertViewForLogOutUser];
}

- (void)redirectToLoggerScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController
{
    [self redirectToLoggerScreen];
}


- (void)redirectToTermsAndCondtitionScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController
{
    //    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
    //
    //    viewController.targetURLType = BTTargetURLTypeTermsAndConditions;
    //    viewController.isBillDownloading = NO;
    //
    //    //Set Navigation Icon
    //    [viewController updateInAppBrowserWithIsPresentedModally:NO isComingFromHomeScreen:YES isNeedBackButton:NO andIsNeedCloseButton:NO];
    //
    //    //Push
    //    [self.navigationController pushViewController:viewController animated:YES];
    //
    [AppManager openURL:kTermsAndConditionsUrl];
}

- (void)redirectToMoreBTBusinessAppsScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController
{
    [self redirectToMoreBTBusinessAppsScreen];
}


- (void)redirectToAboutTheAppScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController
{
    AboutTheAppTableViewController *objAboutTheAppTableViewController = [[AboutTheAppTableViewController alloc]init];
    [self.navigationController pushViewController:objAboutTheAppTableViewController animated:YES];
}

- (void)redirectToSpeedTestScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController
{
    [self redirectToSpeedTestScreen];
}

#pragma mark - BTOnBoardingViewControllerDelegate

- (void)userTappedOnLastOnboardingScreenOfOnBoardingViewController:(BTOnBoardingViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

#pragma mark -

- (void)userPressedOnRetryButtonForNotifications
{
    [self fetchUserAccountNotifications];
}

- (void)noUnReadItemsLeftInNotifications
{
    if ([self.navigationItem.rightBarButtonItems count] > 0) {
        UIBarButtonItem *item = [self.navigationItem.rightBarButtonItems objectAtIndex:0];
        [item setImage:[[UIImage imageNamed:@"notification_read_bell"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    }
}


#pragma mark - Logout related methods
- (void)showAlertViewForLogOutUser
{
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Log out?"  message:@"Are you sure you want to log out?"  preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Log out" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self logOutUser];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)logOutUser
{
    [_viewModel cancelAPIs];
    [[AppDelegate sharedInstance].viewModel logoutCurrentUser];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationUserLoggedOutFromApp object:nil userInfo:nil];
}



#pragma mark - Notification Methods

- (void)billPaidSuccessfully:(NSNotification *)notification {
    
    NSString *groupKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    if(!(self.currentCug.groupKey && ![self.currentCug.groupKey isEqualToString:groupKey])){
        
        _isBillViewNeedToUpdate = YES;
        [self.viewModel fetchBillPaymentHistoryData];
    }
    
}


- (void)checkReadyBillsAPICallStarted
{
    if(_isBillViewNeedToUpdate)
    {
        _isBillViewNeedToUpdate = NO;
        NSArray *gridModelArray = [self.viewModel gridModelArray];
        HomeGridViewModel *billGridViewModel = nil;
        
        for(HomeGridViewModel *gridModel in gridModelArray){
            
            if([[gridModel.titleText lowercaseString] isEqualToString:@"bills"])
            {
                billGridViewModel = gridModel;
            }
        }
        
        
        for(UIView *view in [self.topView subviews]){
            
            if([view isKindOfClass:[HomeScreenGridView class]]){
                
                HomeScreenGridView *gridView = (HomeScreenGridView *)view;
                if([[[gridView getTitle] lowercaseString] isEqualToString:@"bills"])
                {
                    if(billGridViewModel)
                        [gridView updateWithGridViewModel:billGridViewModel];
                }
            }
        }
        
        
    }
}

- (NSString*)welcomeStringForCurrentTime {
    // For calculating the current date
    NSDate *date = [NSDate date];
    
    // Make Date Formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"hh a"];
    
    // hh for hour mm for minutes and a will show you AM or PM
    NSString *str = [dateFormatter stringFromDate:date];
    NSLog(@"%@", str);
    
    // Sperate str by space i.e. you will get time and AM/PM at index 0 and 1 respectively
    NSArray *array = [str componentsSeparatedByString:@" "];
    
    // Now you can check it by 12. If < 12 means Its morning > 12 means its evening or night
    NSString *timeInHour = array[0];
    NSString *am_pm      = array[1];
    
    if([timeInHour integerValue] < 12 && [am_pm isEqualToString:@"AM"])
    {
        return [NSString stringWithFormat:@"Good morning"];
    }
    else if ([timeInHour integerValue] <= 5 && [am_pm isEqualToString:@"PM"])
    {
        return [NSString stringWithFormat:@"Good afternoon"];
    }
    else if ([timeInHour integerValue] == 12 && [am_pm isEqualToString:@"PM"])
    {
        return [NSString stringWithFormat:@"Good afternoon"];
    }
    else if ([timeInHour integerValue] > 5 && [am_pm isEqualToString:@"PM"])
    {
        return [NSString stringWithFormat:@"Good evening"];
    }
    else
    {
        return @"Hello";
    }
}

- (void)trackOmnitureClickEvent:(NSString *)clickEvent onPage:(NSString *)page
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",page,clickEvent] withContextInfo:data];
}

- (void)successfullyParsedSatusMatrixForHomeScreen:(NSString *)statusMatrix
{
    NSArray *sortedArrayOfCugs = [self.viewModel.user.arrayOfCugs sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"indexInAPIResponse" ascending:YES]]];
    
    NSString* contactID = self.viewModel.user.contactId;
    
    NSString *pageName;
    
    NSString *matrix = [NSString stringWithFormat:@"%@%@",STATE_PREFIX,statusMatrix];
    
    if([sortedArrayOfCugs count]>0)
        pageName  = OMNIPAGE_HOME;
    else
        pageName = OMNIPAGE_MULTICUG;
    
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:matrix forKey:kOmniContentViewed];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    if(contactID)
        [data setValue:contactID forKey:kContactID];
    [OmnitureManager trackPage:pageName withContextInfo:data];
}


@end

