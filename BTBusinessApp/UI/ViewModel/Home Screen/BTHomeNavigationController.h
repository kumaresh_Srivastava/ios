//
//  BTHomeNavigationController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 13/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTSlideMenuParentViewController.h"
#import "BTSideMenuViewController.h"

@class BTHomeNavigationController;

@protocol BTHomeNavigationControllerDelegate <UINavigationControllerDelegate>

- (void)redirectToDashboardScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController;
- (void)redirectToTrackOrdersScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController;
- (void)redirectToBillsScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController;
- (void)redirectToServiceStatusScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController;
- (void)redirectToTrackFaultsScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController;
- (void)redirectToAccountScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController;
- (void)redirectToUsageScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController;
- (void)redirectToPublicWifiScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController;
- (void)redirectToHelpScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController;
- (void)redirectToSettingsScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController;
- (void)performLogOutActionForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController;
- (void)redirectToLoggerScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController;
//- (void)redirectToTermsAndCondtitionScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController;
- (void)redirectToMoreBTBusinessAppsScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController;
- (void)redirectToAboutTheAppScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController;
- (void)redirectToSpeedTestScreenForHomeNavigationController:(BTHomeNavigationController *)homeNavigationController;

- (void)userPressedOnRetryButtonForNotifications;
- (void)noUnReadItemsLeftInNotifications;

@end

@interface BTHomeNavigationController : UINavigationController <BTSlideMenuParentViewControllerDelegate, BTSideMenuViewControllerDelegate, UITabBarDelegate>

@property (nonatomic, weak) id <BTHomeNavigationControllerDelegate> homeNavigationControllerDelegate;

@end
