//
//  BTHomeNavigationController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 13/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTHomeNavigationController.h"
#import "AppConstants.h"
#import "AppManager.h"
#import "OmnitureManager.h"

#import "BTHomeViewController.h"
#import "BTAssetsDashboardViewController.h"
#import "BTBillsDashBoardViewController.h"
#import "BTUsageDashboardViewController.h"
#import "BTSideMenuViewController.h"
#import "AppDelegate.h"

//#define homeTag 1
//#define billsTag 2
//#define usageTag 3
//#define accountTag 4
//#define moreTag 5

#define tabBarHeight 49
#define tabBarHeightLargeDevice 83

#define regularScreenHeight 667

@interface BTHomeNavigationController ()

@property (nonatomic) int currentTabTag;

@end

@implementation BTHomeNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    //[self createTabBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// ovderride pop to root method for tabs where we dont want home screen to show
- (NSArray<UIViewController *> *)popToRootViewControllerAnimated:(BOOL)animated
{
    //NSString* tabBarTitle = self.tabBarItem.title;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.isSingleBacCug == YES && ([self.tabBarItem.title.lowercaseString isEqualToString:kAccountTabName.lowercaseString] || [self.tabBarController.selectedViewController.tabBarItem.title.lowercaseString isEqualToString:kUsageTabName.lowercaseString]) ){
        
        NSArray<UIViewController *> * retVal;
        
        if (self.viewControllers.count > 2) {
            retVal = [self popToViewController:self.viewControllers[2] animated:YES];
        } else if (self.viewControllers.count == 2) {
            retVal = [self popToViewController:self.viewControllers[1] animated:YES];
        } else {
            retVal = [super popToRootViewControllerAnimated:animated];
        }
        
        if (self.navigationBarHidden) {
            [self setNavigationBarHidden:NO];
        }
        return retVal;
        
    } else{
    
    NSArray<UIViewController *> * retVal;
    if ([self.tabBarItem.title.lowercaseString isEqualToString:kHomeTabName.lowercaseString] || [self.tabBarController.selectedViewController.tabBarItem.title.lowercaseString isEqualToString:kHomeTabName.lowercaseString]) {
        retVal = [super popToRootViewControllerAnimated:animated];
    } else {
        if (self.viewControllers.count > 1) {
            retVal = [self popToViewController:self.viewControllers[1] animated:YES];
        } else {
            retVal = [super popToRootViewControllerAnimated:animated];
        }
    }
    if (self.navigationBarHidden) {
        [self setNavigationBarHidden:NO];
    }
    return retVal;
    }
}

#pragma mark - Slide menu parent view controller delegate methods
- (UIViewController *)viewControllerForMenuButton {
    return [self topViewController];
}

- (void)redirectToDashboardScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    [self.homeNavigationControllerDelegate redirectToDashboardScreenForHomeNavigationController:self];
}

- (void)redirectToTrackOrdersScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    [self.homeNavigationControllerDelegate redirectToTrackOrdersScreenForHomeNavigationController:self];
}

- (void)redirectToBillsScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    [self.homeNavigationControllerDelegate redirectToBillsScreenForHomeNavigationController:self];
}

- (void)redirectToServiceStatusScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    [self.homeNavigationControllerDelegate redirectToServiceStatusScreenForHomeNavigationController:self];
}

- (void)redirectToTrackFaultsScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    [self.homeNavigationControllerDelegate redirectToTrackFaultsScreenForHomeNavigationController:self];
}

- (void)redirectToAccountScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    [self.homeNavigationControllerDelegate redirectToAccountScreenForHomeNavigationController:self];
}

- (void)redirectToUsageScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    [self.homeNavigationControllerDelegate redirectToUsageScreenForHomeNavigationController:self];
}

- (void)redirectToPublicWifiScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController
{
    [self.homeNavigationControllerDelegate redirectToPublicWifiScreenForHomeNavigationController:self];
}

- (void)redirectToHelpScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    [self.homeNavigationControllerDelegate redirectToHelpScreenForHomeNavigationController:self];
}

- (void)redirectToSettingsScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    [self.homeNavigationControllerDelegate redirectToSettingsScreenForHomeNavigationController:self];
}

- (void)performLogOutActionForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    [self.homeNavigationControllerDelegate performLogOutActionForHomeNavigationController:self];
}

- (void)redirectToLoggerScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController
{
    [self.homeNavigationControllerDelegate redirectToLoggerScreenForHomeNavigationController:self];
}


//- (void)redirectToTermsAndConditonScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController
//{
//    [self.homeNavigationControllerDelegate redirectToTermsAndCondtitionScreenForHomeNavigationController:self];
//}

- (void)redirectToMoreBTBusinessAppsScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController
{
    [self.homeNavigationControllerDelegate redirectToMoreBTBusinessAppsScreenForHomeNavigationController:self];
}

- (void)redirectToOnBoardingScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController
{
    [self.homeNavigationControllerDelegate redirectToAboutTheAppScreenForHomeNavigationController:self];
}

- (void)redirectToSpeedTestScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController
{
    [self.homeNavigationControllerDelegate redirectToSpeedTestScreenForHomeNavigationController:self];
}

- (void)userPressedOnRetryButtonForNotifications
{
    [self.homeNavigationControllerDelegate userPressedOnRetryButtonForNotifications];
}

- (void)noUnReadItemsLeftInNotifications
{
    [self.homeNavigationControllerDelegate noUnReadItemsLeftInNotifications];
}

#pragma mark - tab bar
- (void) createTabBar {
    UITabBar *tabBar;
    if([[UIScreen mainScreen] bounds].size.height > regularScreenHeight) {
        tabBar = [[UITabBar alloc] initWithFrame:CGRectMake(0, [[UIScreen mainScreen] bounds].size.height - tabBarHeightLargeDevice, [[UIScreen mainScreen] bounds].size.width, tabBarHeightLargeDevice)];
    } else {
        tabBar = [[UITabBar alloc] initWithFrame:CGRectMake(0, [[UIScreen mainScreen] bounds].size.height - tabBarHeight, [[UIScreen mainScreen] bounds].size.width, tabBarHeight)];
    }
    
    
    tabBar.delegate = self;
    tabBar.backgroundColor = [BrandColours colourBtNeutral30];
    tabBar.tintColor = [BrandColours colourTextBTPurplePrimaryColor];
    
    UITabBarItem *homeItem = [[UITabBarItem alloc] initWithTitle:@"Home" image:[UIImage imageNamed:@"home_tab"] tag:homeTag];
    UITabBarItem *billingItem = [[UITabBarItem alloc] initWithTitle:@"Billing" image:[UIImage imageNamed:@"billing_tab"] tag:billsTag];
    UITabBarItem *usageItem = [[UITabBarItem alloc] initWithTitle:@"Usage" image:[UIImage imageNamed:@"usage_tab"] tag:usageTag];
    UITabBarItem *accountItem = [[UITabBarItem alloc] initWithTitle:@"Account" image:[UIImage imageNamed:@"account_tab"] tag:accountTag];
    UITabBarItem *moreItem = [[UITabBarItem alloc] initWithTitle:@"More" image:[UIImage imageNamed:@"more_tab"] tag:moreTag];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{
                                                        NSFontAttributeName:[UIFont fontWithName:kBtFontRegular size:13.0f]
                                                        } forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:@{
                                                        NSFontAttributeName:[UIFont fontWithName:kBtFontRegular size:13.0f]
                                                        } forState:UIControlStateSelected];
    
    NSMutableArray *tabBarItems = [[NSMutableArray alloc] init];
    [tabBarItems addObjectsFromArray:@[homeItem, billingItem, usageItem, accountItem, moreItem]];
    [tabBar setItems:tabBarItems];
    [self.view addSubview:tabBar];
    _currentTabTag = 1;
    [tabBar setSelectedItem:[tabBar.items objectAtIndex:0]];
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    if(_currentTabTag != item.tag) {
        _currentTabTag = (int) item.tag;
        if(item.tag == homeTag) {
            for(UIViewController *viewController in self.viewControllers) { // should always be a homeviewcontroller somewhere in the stack
                if([viewController isKindOfClass:[BTHomeViewController class]]) {
                    [self popToViewController:viewController animated:NO];
                }
            }
        }
        else if(item.tag == billsTag) {
            [self.homeNavigationControllerDelegate redirectToBillsScreenForHomeNavigationController:self];
        }
        else if(item.tag == usageTag) {
            [self.homeNavigationControllerDelegate redirectToUsageScreenForHomeNavigationController:self];
        }
        else if(item.tag == accountTag) {
            [self.homeNavigationControllerDelegate redirectToAccountScreenForHomeNavigationController:self];
        }
        else if(item.tag == moreTag) {
            BTSideMenuViewController *moreMenu = [BTSideMenuViewController getViewController];
            moreMenu.sideMenuViewControllerDelegate = self;
            [self pushViewController:moreMenu animated:NO];
        }
    } else {
        for(UIViewController *viewController in self.viewControllers) {
            if([viewController isKindOfClass:[BTHomeViewController class]] && _currentTabTag == homeTag) {
                [self popToViewController:viewController animated:YES];
            }
            else if([viewController isKindOfClass:[BTBillsDashBoardViewController class]] && _currentTabTag == billsTag) {
                [self popToViewController:viewController animated:YES];
            }
            else if([viewController isKindOfClass:[BTUsageDashboardViewController class]] && _currentTabTag == usageTag) {
                [self popToViewController:viewController animated:YES];
            }
            else if([viewController isKindOfClass:[BTAssetsDashboardViewController class]] && _currentTabTag == accountTag) {
                [self popToViewController:viewController animated:YES];
            }
            else if([viewController isKindOfClass:[BTSideMenuViewController class]] && _currentTabTag == moreTag) {
                [self popToViewController:viewController animated:YES];
            }
        }

    }
}

#pragma mark - more menu redirects
- (void)hideSideMenuViewWithSideMenuViewController:(UIViewController *)sideMenuViewController {
    // Do nothing
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedDashboardCellInSideMenuTableView:(UITableView *)tableView {
    // Do nothing
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedAccountCellInSideMenuTableView:(UITableView *)tableView {
    [self trackOmnitureClickEvent:OMNICLICK_HOME_DASHBOARD onPage:OMNIPAGE_NAVIGATE];
    [self.homeNavigationControllerDelegate redirectToAccountScreenForHomeNavigationController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedBillsCellInSideMenuTableView:(UITableView *)tableView {
    [self trackOmnitureClickEvent:OMNICLICK_HOME_BILLS onPage:OMNIPAGE_NAVIGATE];
    [self.homeNavigationControllerDelegate redirectToBillsScreenForHomeNavigationController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedDevDebuggerCellInSideMenuTableView:(UITableView *)tableView {
    [self.homeNavigationControllerDelegate redirectToLoggerScreenForHomeNavigationController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedHelpCellInSideMenuTableView:(UITableView *)tableView {
    [self trackOmnitureClickEvent:OMNICLICK_HOME_HELP onPage:OMNIPAGE_NAVIGATE];
    [self.homeNavigationControllerDelegate redirectToHelpScreenForHomeNavigationController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedLogOutCellInSideMenuTableView:(UITableView *)tableView {
    [self trackOmnitureClickEvent:OMNICLICK_HOME_LOGOUT onPage:OMNIPAGE_NAVIGATE];
    [self.homeNavigationControllerDelegate performLogOutActionForHomeNavigationController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedOnBoardingCellInSideMenuTableView:(UITableView *)tableView {
    [self.homeNavigationControllerDelegate redirectToAboutTheAppScreenForHomeNavigationController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedPublicWifiCellInSideMenuTableView:(UITableView *)tableView {
    [self trackOmnitureClickEvent:OMNICLICK_HOME_PUBLICWIFI onPage:OMNIPAGE_NAVIGATE];
    [self.homeNavigationControllerDelegate redirectToPublicWifiScreenForHomeNavigationController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedServiceStatusCellInSideMenuTableView:(UITableView *)tableView {
    [self trackOmnitureClickEvent:OMNICLICK_HOME_SERVICE onPage:OMNIPAGE_NAVIGATE];
    [self.homeNavigationControllerDelegate redirectToServiceStatusScreenForHomeNavigationController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedSettingsCellInSideMenuTableView:(UITableView *)tableView {
    [self trackOmnitureClickEvent:OMNICLICK_HOME_SETTINGS onPage:OMNIPAGE_NAVIGATE];
    [self.homeNavigationControllerDelegate redirectToSettingsScreenForHomeNavigationController:self];
}

//- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedTermsAndConditionsCellInSideMenuTableView:(UITableView *)tableView {
//    [self trackOmnitureClickEvent:OMNICLICK_HOME_SETTINGS onPage:OMNIPAGE_NAVIGATE];
//    [self.homeNavigationControllerDelegate redirectToTermsAndCondtitionScreenForHomeNavigationController:self];
//}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedTrackFaultsCellInSideMenuTableView:(UITableView *)tableView {
    [self trackOmnitureClickEvent:OMNICLICK_HOME_TRACK_FAULTS onPage:OMNIPAGE_NAVIGATE];
    [self.homeNavigationControllerDelegate redirectToTrackFaultsScreenForHomeNavigationController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedTrackOrdersCellInSideMenuTableView:(UITableView *)tableView {
    [self trackOmnitureClickEvent:OMNICLICK_HOME_TRACK_ORDERS onPage:OMNIPAGE_NAVIGATE];
    [self.homeNavigationControllerDelegate redirectToTrackOrdersScreenForHomeNavigationController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedUsageCellInSideMenuTableView:(UITableView *)tableView {
    [self trackOmnitureClickEvent:OMNICLICK_HOME_USAGE onPage:OMNIPAGE_NAVIGATE];
    [self.homeNavigationControllerDelegate redirectToUsageScreenForHomeNavigationController:self];
}

- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedMoreBTBusinessAppsCellInSideMenuTableView:(UITableView *)tableView {
    [self.homeNavigationControllerDelegate redirectToMoreBTBusinessAppsScreenForHomeNavigationController:self];
}


- (void)sideMenuViewController:(BTSideMenuViewController *)sideMenuViewController userPressedSpeedTestCellInSideMenuTableView:(UITableView *)tableView {
//    [self trackOmnitureClickEvent:OMNICLICK_HOME_SPEED_TEST onPage:OMNIPAGE_NAVIGATE];
    [self.homeNavigationControllerDelegate redirectToSpeedTestScreenForHomeNavigationController:self];
}

#pragma mark - omniture
//Added by (RM) To track click event
- (void)trackOmnitureClickEvent:(NSString *)clickEvent onPage:(NSString *)page
{
    NSString *pageName = page;
    NSString *linkTitle = clickEvent;
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:data];
}

@end
