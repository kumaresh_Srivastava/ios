//
//  BTServiceDashboardViewController.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 22/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//
#import <UIKit/UIKit.h>


@interface BTServiceStatusDashboardViewController : UIViewController

@property (nonatomic, copy) NSString *groupKey;
@property (nonatomic) NSArray *cugs;

+ (BTServiceStatusDashboardViewController *)getServiceStatusDashboardViewController;

@end
