//
//  BTServiceDashboardViewController.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 22/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTServiceStatusDashboardViewController.h"
#import "BTEmptyDashboardView.h"
#import "BTRetryView.h"
#import "DLMServiceStatusScreen.h"
#import "CustomSpinnerView.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "AppManager.h"
#import "BTAssetTableViewCell.h"
#import "BTCug.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "NLWebServiceError.h"

@interface BTServiceStatusDashboardViewController ()<DLMServiceStatusDashboardScreenDelegate,UITableViewDelegate,UITableViewDataSource,BTRetryViewDelegate> {

    UITableView *_assetsTableView;
    NSMutableArray *_assetsArray;
    BTEmptyDashboardView *_emptyDashboardView;
    BTRetryView *_retryView;
}

@property (nonatomic, readwrite) DLMServiceStatusScreen *serviceStatusDashboardScreenModel;
@property(nonatomic, assign) BOOL networkRequestInProgress;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *groupNameLabel;
@property (nonatomic,strong)  CustomSpinnerView *loadingView;

@end

@implementation BTServiceStatusDashboardViewController



- (void)viewDidLoad {

    [super viewDidLoad];

    // Do any additional setup after loading the view

    //[SD] View Model Initialization
    self.serviceStatusDashboardScreenModel = [[DLMServiceStatusScreen alloc] init];
    self.serviceStatusDashboardScreenModel.serviceStatusDashboardScreenDelegate = self;

    //[SD] Initial UI SetUP
    [self createInitialUI];
    [self updateGroupSelection];

    UINib *dataCell = [UINib nibWithNibName:@"BTAssetTableViewCell" bundle:nil];
    [_assetsTableView registerNib:dataCell forCellReuseIdentifier:@"BTAssetTableViewCell"];

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self createLoadingView];

    __weak typeof(self) selfWeak = self;

    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([selfWeak networkRequestInProgress]) {

            dispatch_async(dispatch_get_main_queue(), ^{

                [selfWeak hideLoadingItems:NO];
                selfWeak.navigationItem.rightBarButtonItem.enabled = NO;
                [selfWeak.loadingView startAnimatingLoadingIndicatorView];


            });
        }

        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
            dispatch_async(dispatch_get_main_queue(), ^{

                [selfWeak hideLoadingItems:YES];
                selfWeak.navigationItem.rightBarButtonItem.enabled = YES;
                [selfWeak.loadingView stopAnimatingLoadingIndicatorView];

            });
        }
    }];


    [self fetchAssetsDashboardAPI];


}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];

}

- (void)viewDidDisappear:(BOOL)animated {

    [super viewDidDisappear:animated];
    [self.serviceStatusDashboardScreenModel cancelServiceStatusDashboardAPIRequest];
    self.networkRequestInProgress = NO;

}


#pragma mark - Public Methods

+ (BTServiceStatusDashboardViewController *)getServiceStatusDashboardViewController {

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    BTServiceStatusDashboardViewController *controller = (BTServiceStatusDashboardViewController *)[storyboard instantiateViewControllerWithIdentifier:@"BTServiceStatusDashboardViewController"];

    return controller;


}


#pragma mark - InitialUI Methods

- (void)createInitialUI {

    CGSize screenSize = [UIScreen mainScreen].bounds.size;

    self.titleLabel.text = @"Accounts";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon_Home_White"] style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonAction)];

    self.view.backgroundColor = [UIColor colorWithRed:243.0/255.0 green:243.0/255.0 blue:243.0/255.0 alpha:1.0];

    //[SD] Strip for GugName if there is more than one cug
    if(self.cugs.count > 1) {

        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"cugSelection"] style:UIBarButtonItemStylePlain target:self action:@selector(groupSelectionAction)];
    }
    else {

        [self.groupNameLabel removeFromSuperview];
        UIFont *titleFont = self.titleLabel.font;
        self.titleLabel.font = [titleFont fontWithSize:20];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    //[SD]  UITableView Initialiation
    _assetsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,
                                                                     4,screenSize.width, screenSize.height-70) style:UITableViewStylePlain
                        ];
    _assetsTableView.backgroundColor = [UIColor colorWithRed:243.0/255.0 green:243.0/255.0 blue:243.0/255.0 alpha:1.0];
    _assetsTableView.delegate = self;
    _assetsTableView.dataSource = self;

    _assetsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    [self.view addSubview:_assetsTableView];


}





#pragma mark - api call methods

- (void)fetchAssetsDashboardAPI {

    if([AppManager isInternetConnectionAvailable]) {

        [self hideRetryItems:YES];
        self.networkRequestInProgress = YES;
        [self hideEmmptyDashBoardItems:YES];
        [self.serviceStatusDashboardScreenModel fetchServiceStatusDashboardDetails];
    }
    else {

        [self showRetryViewWithInternetStrip:YES];
    }
}





#pragma mark - UITableViewDataSource Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _assetsArray.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    BTAssetTableViewCell *assetCell = [tableView dequeueReusableCellWithIdentifier:@"BTAssetTableViewCell" forIndexPath:indexPath];
    assetCell.selectionStyle = UITableViewCellSelectionStyleNone;
    [assetCell updateAssetsCellWithAsset:[_assetsArray objectAtIndex:indexPath.row]];
    return assetCell;

}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    return 86;

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {


}


#pragma mark - Private Helper Methods
- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow {

    if(_retryView) {

        _retryView.hidden = NO;
        _retryView.retryViewDelegate = self;
        [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
        return;
    }
    _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
    _retryView.translatesAutoresizingMaskIntoConstraints = NO;
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
    _retryView.retryViewDelegate = self;

    [self.view addSubview:_retryView];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:64.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];


    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];

}

- (void)hideRetryItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to retry.
    [_retryView setHidden:isHide];

}

- (void)createLoadingView {

    if(!_loadingView){

        _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        _loadingView.translatesAutoresizingMaskIntoConstraints = NO;

        [self.view addSubview:_loadingView];
    }

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}

- (void)hideLoadingItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to loading.
    [_loadingView setHidden:isHide];

}


/*
 Updates the currently selected group and UI on the basis of group selection
 */
- (void) checkForSuperUserAndUpdateUIWithIndex:(NSUInteger)index {

    BTCug *cugData = [self.cugs objectAtIndex:index];

    // (SD) Change current selected cug locally and in persistence.
    [self.serviceStatusDashboardScreenModel changeSelectedCUGTo:cugData];

    if (![self.groupKey isEqualToString:cugData.groupKey]) {
        [self setGroupKey:cugData.groupKey];
        [self.groupNameLabel setText:cugData.cugName];
        [self resetDataAndUIAfterGroupChange];
    }
}


- (void)resetDataAndUIAfterGroupChange {

    BOOL needToRefresh = YES;

    if(_retryView) {

        if((![AppManager isInternetConnectionAvailable] && !_retryView.isHidden))
            needToRefresh = NO;
    }

    if(needToRefresh) {

        [self hideRetryItems:YES];
        _assetsTableView.hidden = YES;
        [self fetchAssetsDashboardAPI];
    }
    else {

        [_retryView updateRetryViewWithInternetStrip:YES];
    }

}



- (void)updateGroupSelection {
    if (self.cugs.count == 1) {
        return;
    }

    BTCug *cug = (BTCug *)[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug;

    [self setGroupKey:cug.groupKey];
    self.groupNameLabel.text = cug.cugName;
}

- (void)showEmptyDashBoardView {

    _emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTEmptyDashboardView" owner:nil options:nil] objectAtIndex:0];
    _emptyDashboardView.translatesAutoresizingMaskIntoConstraints = NO;
    [_emptyDashboardView updateEmptyDashboardViewWithImageName:@"package" title:@"None to display" detailText:@"Sorry, we're still setting up your account at the moment. Please try again later." andButtonTitle:nil];
    [self.view addSubview:_emptyDashboardView];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];

}

- (void)hideEmmptyDashBoardItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to retry.
    [_emptyDashboardView setHidden:isHide];
    [_emptyDashboardView removeFromSuperview];
    _emptyDashboardView = nil;

}


- (void)groupSelectionAction
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Select Group Name" preferredStyle:UIAlertControllerStyleActionSheet];

    DDLogVerbose(@"Bills: Total cugs belongs to user %lu.",(unsigned long)self.cugs.count);

    // (SD) Add group names in actionsheet
    if (self.cugs != nil && self.cugs.count > 0) {

        // (SD) Add action for each cug selection.
        int index = 0;
        for (BTCug *groupData in self.cugs) {

            __weak typeof(self) selfWeak = self;

            UIAlertAction *action = [UIAlertAction actionWithTitle:groupData.cugName
                                                             style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                 [selfWeak checkForSuperUserAndUpdateUIWithIndex:groupData.indexInAPIResponse];
                                                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                                             }];
            [alert addAction:action];

            index++;
        }
    }

    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    [alert addAction:cancel];

    [self presentViewController:alert animated:YES completion:^{

    }];

}



#pragma mark - Action Methods

- (void)doneButtonAction {

    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - DLMServiceStatusDashboardScreenDelegate

- (void)successfullyFetchedServiceStatusDashboardDataOnDLMServiceStatusDashboardScreen:(DLMServiceStatusScreen *)serviceStatusDashboardScreen {

    self.networkRequestInProgress = NO;
    _assetsTableView.hidden = NO;
    if(serviceStatusDashboardScreen.assetsArray.count > 0) {

        _assetsArray =  [serviceStatusDashboardScreen.assetsArray mutableCopy];
        [_assetsTableView reloadData];

    }
    else if(serviceStatusDashboardScreen.assetsArray.count == 0) {

        [self showEmptyDashBoardView];
    }
    else {

        DDLogError(@"Assets Dashboard: Assets not found.");
    }



}

- (void)serviceStatusDashboardScreen:(DLMServiceStatusScreen *)serviceStatusDashboardScreen failedToFetchServiceStatusDashboardDataWithWebServiceError:(NLWebServiceError *)webServiceError {

    self.networkRequestInProgress = NO;

    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];

    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO) {

        switch (webServiceError.error.code) {
                
            case BTNetworkErrorCodeAPINoDataFound: {
                
                errorHandled = YES;
                [self showEmptyDashBoardView];
                break;
            }
            default: {
                
                errorHandled = NO;
                break;
            }
        }
    }
    
    if(errorHandled == NO) {
        
        [self showRetryViewWithInternetStrip:NO];
    }
    
    
    
}


#pragma mark - RetryView Delgates

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView {
    
    [self fetchAssetsDashboardAPI];
}







@end
