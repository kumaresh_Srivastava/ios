//
//  DLMFAQsScreen.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 9/11/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "DLMFAQsScreen.h"
#import "AppManager.h"

@implementation DLMFAQsScreen

- (void)createFAQsDataArray
{
    NSDictionary *faqBroadband = [self createDictionaryWithTitle:@"Broadband & internet" description:@"Wi-fi, VoIP, BTnet, networking" imageName:@"faq_broadband" andURL:kBroadbandFAQUrl];
    
    NSDictionary *faqPhoneline = [self createDictionaryWithTitle:@"Phone line & services" description:@"BT Cloud Voice Express, Featureline, ISDN, calling features, call plans" imageName:@"faq_phoneline" andURL:kPhonelineFAQUrl];
    
    NSDictionary *faqOfficePhone = [self createDictionaryWithTitle:@"Office phones & systems" description:@"Avaya IP Office, BT Quantum, BT Cloud Voice, BT Cloud Phone, BCM" imageName:@"faq_officephone" andURL:kOfficePhoneFAQUrl];
    
    NSDictionary *faqEmail = [self createDictionaryWithTitle:@"Email, hosting & domains" description:@"Office 365, web hosting, domains, eShop" imageName:@"faq_email" andURL:kEmailFAQUrl];
    
    NSDictionary *faqMobile = [self createDictionaryWithTitle:@"Mobile services" description:@"Mobile phones, tablets, BT One Phone, mobile broadband" imageName:@"faq_mobile" andURL:kMobileFAQUrl];
    
    NSDictionary *faqBilling = [self createDictionaryWithTitle:@"Billing" description:@"Billing help & contact options" imageName:@"faq_billing" andURL:kBillingFAQUrl];
    
    _arrayOfFAQsData = [[NSArray alloc] initWithObjects:faqBroadband, faqPhoneline, faqOfficePhone, faqEmail, faqMobile, faqBilling, nil];
    
}

- (NSDictionary *)createDictionaryWithTitle:(NSString *)title description:(NSString *)description imageName:(NSString *)image andURL:(NSString *)url
{
    NSMutableDictionary *faqData = [NSMutableDictionary dictionary];
    [faqData setValue:title forKey:@"Title"];
    [faqData setValue:description forKey:@"Description"];
    [faqData setValue:image forKey:@"Image"];
    [faqData setValue:url forKey:@"URL"];
    
    return faqData;
}

@end
