//
//  BTFAQsViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 9/11/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTFAQsViewController.h"
#import "BTFAQsTableViewCell.h"
#import "DLMFAQsScreen.h"
#import "AppManager.h"
#import "AppConstants.h"

@interface BTFAQsViewController ()<UITableViewDelegate, UITableViewDataSource> {
    
}

@property (nonatomic, strong) UITableView *faqTableView;
@property (nonatomic, strong) DLMFAQsScreen *faqScreenViewModel;

@end

@implementation BTFAQsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Popular FAQs";
    
    //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleDone target:self action:@selector(closeButtonAction:)];
    
    _faqScreenViewModel = [[DLMFAQsScreen alloc] init];
    [_faqScreenViewModel createFAQsDataArray];
    
    [self createFAQTableView];
    [_faqTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods

- (void)createFAQTableView
{
    _faqTableView = [[UITableView alloc] init];
    _faqTableView.dataSource = self;
    _faqTableView.delegate = self;
    
    _faqTableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_faqTableView];
    
    NSDictionary *views = [[NSDictionary alloc] initWithObjectsAndKeys:_faqTableView, @"faqTableView", nil];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[faqTableView]|" options:NSLayoutFormatAlignAllCenterY metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[faqTableView]|" options:NSLayoutFormatAlignAllCenterX metrics:nil views:views]];
    
    _faqTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _faqTableView.rowHeight = UITableViewAutomaticDimension;
    _faqTableView.estimatedRowHeight = 101;
    
    UINib *dataCell = [UINib nibWithNibName:@"BTFAQsTableViewCell" bundle:nil];
    [_faqTableView registerNib:dataCell forCellReuseIdentifier:@"BTFAQsTableViewCell"];
}

- (void)openSafariBrowserWithURL:(NSString *)urlString
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}

- (void)closeButtonAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)showNoNetworkAlert
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kNoConnectionTitle message:kNoConnectionMessage preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:^{}];
}

#pragma mark - TableView Datasource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _faqScreenViewModel.arrayOfFAQsData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTFAQsTableViewCell *faqCell = [tableView dequeueReusableCellWithIdentifier:@"BTFAQsTableViewCell" forIndexPath:indexPath];
    [faqCell updateCellWithFAQCellData:_faqScreenViewModel.arrayOfFAQsData[indexPath.row]];
    
    return faqCell;
}

#pragma mark - TableView Delegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([AppManager isInternetConnectionAvailable])
    {
        [self openSafariBrowserWithURL:[_faqScreenViewModel.arrayOfFAQsData[indexPath.row] valueForKey:@"URL"]];
    }
    else
    {
        [self showNoNetworkAlert];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 101.0;
}

@end
