//
//  DLMFAQsScreen.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 9/11/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DLMFAQsScreen : NSObject

@property (nonatomic, strong) NSArray *arrayOfFAQsData;

- (void)createFAQsDataArray;

@end
