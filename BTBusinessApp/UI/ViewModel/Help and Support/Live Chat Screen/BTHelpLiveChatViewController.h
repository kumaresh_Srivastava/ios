//
//  BTHelpLiveChatViewController.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 10/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMHelpAndSupportScreen.h"

@interface BTHelpLiveChatViewController : UIViewController

@property (nonatomic, strong) DLMHelpAndSupportScreen *helpAndSupportScreenViewModel;

+ (BTHelpLiveChatViewController *)getHelpAndSupportChatViewController;

@end
