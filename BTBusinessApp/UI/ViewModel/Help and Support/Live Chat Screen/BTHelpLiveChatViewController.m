//
//  BTHelpLiveChatViewController.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 10/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTHelpLiveChatViewController.h"
#import "BTHelpLiveChatTableViewCell.h"
#import "BTLiveChatFAQTableViewCell.h"
#import "DLMHelpAndSupportScreen.h"
#import "AppConstants.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "BTInAppBrowserViewController.h"
#import "BTNavigationViewController.h"
#import "BTFAQsViewController.h"

@interface BTHelpLiveChatViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation BTHelpLiveChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    self.title = @"Live chat";
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //_tableView.allowsSelection = NO;

    UINib *dataCell = [UINib nibWithNibName:@"BTHelpLiveChatTableViewCell" bundle:nil];
    [_tableView registerNib:dataCell forCellReuseIdentifier:@"BTHelpLiveChatTableViewCell"];

    UINib *dataCell1 = [UINib nibWithNibName:@"BTLiveChatFAQTableViewCell" bundle:nil];
    [_tableView registerNib:dataCell1 forCellReuseIdentifier:@"BTLiveChatFAQTableViewCell"];


}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Class Methods

+ (BTHelpLiveChatViewController *)getHelpAndSupportChatViewController {

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    BTHelpLiveChatViewController *controller = (BTHelpLiveChatViewController *)[storyboard instantiateViewControllerWithIdentifier:@"BTHelpLiveChatViewController"];

    return controller;

}

#pragma mark - Private methods
- (void)redirectToInAppBrowserWithTargetType:(BTTargetURLType)targetURLType
{
    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
    
    viewController.targetURLType = targetURLType;
    
    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:NO andIsNeedCloseButton:YES];
    
    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{
        
    }];
}

- (void)redirectToFAQsScreen
{
    BTFAQsViewController *faqViewController = [[BTFAQsViewController alloc] init];
    
    [self.navigationController pushViewController:faqViewController animated:YES];
    
    //UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:faqViewController];
    
    //[self presentViewController:navController animated:YES completion:nil];
}

#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.helpAndSupportScreenViewModel.generalCategoryArray.count + 1;
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if(indexPath.row < self.helpAndSupportScreenViewModel.specificCategoryArray.count) {

        BTHelpLiveChatTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BTHelpLiveChatTableViewCell" forIndexPath:indexPath];
        [cell updateCellWithGeneralCategory:[self.helpAndSupportScreenViewModel.generalCategoryArray objectAtIndex:indexPath.row] specificCategory:[self.helpAndSupportScreenViewModel.specificCategoryArray objectAtIndex:indexPath.row] andCurrentTime:self.helpAndSupportScreenViewModel.currentTime];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else {

        BTLiveChatFAQTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BTLiveChatFAQTableViewCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;

    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == 0)
    {
        [self redirectToInAppBrowserWithTargetType:BTTargetURLTypeBillingChatScreen];
    }
    else if (indexPath.row == 1)
    {
        [self redirectToInAppBrowserWithTargetType:BTTargetURLTypeFaultsChatScreen];
    }
    else if (indexPath.row == 2)
    {
        [self redirectToInAppBrowserWithTargetType:BTTargetURLTypeOrdersChatScreen];
    }
    else if (indexPath.row == 3)
    {
        [self redirectToFAQsScreen];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    if(indexPath.row < self.helpAndSupportScreenViewModel.specificCategoryArray.count)
    {
        return 117;
    }
    else
    {
        return 105;
    }
}




#pragma mark - Action Methods

- (void)backButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Omniture Tracking
- (void)trackPage
{
    [OmnitureManager trackPage:OMNIPAGE_HELP_CHAT_CATEGORY withContextInfo:[AppManager getOmniDictionary]];
}

@end
