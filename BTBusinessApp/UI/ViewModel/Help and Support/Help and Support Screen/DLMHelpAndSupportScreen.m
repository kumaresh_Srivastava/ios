//
//  DLMHelpAndSupportScreen.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 09/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "DLMHelpAndSupportScreen.h"
#import "NLHelpAndSupportWebService.h"


@interface DLMHelpAndSupportScreen()<NLHelpAndSupportWebServiceDelegate>

@property (nonatomic, strong) NLHelpAndSupportWebService *helpAndSupportWebService;



@end

@implementation DLMHelpAndSupportScreen


#pragma mark - Public Methods

- (void)fetchHelpAndSupportScreenData {

    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd-MM-yy"];
    NSDate *now = [[NSDate alloc] init];
    NSString *dateString = [format stringFromDate:now];

    self.helpAndSupportWebService = [[NLHelpAndSupportWebService alloc] initWithDateText:dateString];
    self.helpAndSupportWebService.helpAndSupportWebServiceDelegate = self;
    [self.helpAndSupportWebService resume];

}

- (void)cancelHelpAndSupportApi {
    
    self.helpAndSupportWebService.helpAndSupportWebServiceDelegate = nil;
    [self.helpAndSupportWebService cancel];
    self.helpAndSupportWebService = nil;
}


#pragma mark - NLHelpAndSupportWebServiceDelegate

- (void)getHelpAndSupportWebService:(NLHelpAndSupportWebService *)webService successfullyFetchedHelpAndSupportSpecificCategory:(NSArray *)specificCategory generalCategory:(NSArray *)generalCategory andCurrentTime:(NSString *)currentTime {

    if(self.helpAndSupportWebService == webService) {

        if(specificCategory) {
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"categoryName" ascending:YES];
            self.specificCategoryArray = [specificCategory sortedArrayUsingDescriptors:@[sortDescriptor]];
        }

        if(generalCategory) {
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"categoryName" ascending:YES];
            self.generalCategoryArray = [generalCategory sortedArrayUsingDescriptors:@[sortDescriptor]];

        }

        if(currentTime)
            _currentTime = currentTime;

        [self.helpAndSupportScreenDelegate successfullyFetchedHelpAndSupportData:self];
        self.helpAndSupportWebService.helpAndSupportWebServiceDelegate = nil;
        self.helpAndSupportWebService = nil;
    }
    else {

        DDLogError(@"This delegate method gets called because of success of an object of NLHelpAndSupportWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLHelpAndSupportWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }

}
    


- (void)getHelpAndSupportWebService:(NLHelpAndSupportWebService *)webService failedToFetchHelpAndSupportWebServiceWuthError:(NLWebServiceError *)webServiceError {

    if(self.helpAndSupportWebService == webService){

        [self.helpAndSupportScreenDelegate failedToFetchHelpAndSupportScreenWithError:webServiceError];
        self.helpAndSupportWebService.helpAndSupportWebServiceDelegate = nil;
        self.helpAndSupportWebService = nil;
    }
    else{

        DDLogError(@"This delegate method gets called because of failure of an object of NLHelpAndSupportWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLHelpAndSupportWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
    

    
}


@end
