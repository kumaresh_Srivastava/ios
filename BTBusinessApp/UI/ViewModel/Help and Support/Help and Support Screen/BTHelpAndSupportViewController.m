//
//  HelpAndSupportViewController.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 03/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

//#define HELP_DESK_NUMEBR @"0800800152"
#define HELP_DESK_NUMBER @"0800800152"

#import "BTHelpAndSupportViewController.h"
#import "DLMHelpAndSupportScreen.h"
#import "NLWebServiceError.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "BTSpecificCategory.h"
#import "BTGeneralCategory.h"
#import "CustomSpinnerView.h"
#import "BTRetryView.h"
#import "AppManager.h"
#import "BTHelpLiveChatViewController.h"
#import "OmnitureManager.h"
#import "UIButton+BTButtonProperties.h"
#import "BTInAppBrowserViewController.h"
#import "BTNavigationViewController.h"
#import "BTFAQsViewController.h"

@interface BTHelpAndSupportViewController ()<DLMHelpAndSupportScreenDelegate, BTRetryViewDelegate, UITableViewDataSource, UITableViewDelegate>
{
    CustomSpinnerView *_loadingView;
    BTRetryView *_retryView;
    BOOL _isOrderTimeLies;
    BOOL _isBillTimeLies;
    BOOL _isFaultTimeLies;
    NSString *_pageNameForOmniture;
}


@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UITableView *tableViewOut;
@property (weak, nonatomic) IBOutlet UIButton *startLiveChatButton;

@property (weak, nonatomic) IBOutlet UIButton *callTheHelpDeskButton;
@property (weak, nonatomic) IBOutlet UIButton *reportAFaultButton;

@property (strong, nonatomic) DLMHelpAndSupportScreen *helpAndSupportScreenViewModel;

@end

@implementation BTHelpAndSupportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    // Do any additional setup after loading the view.
    _pageNameForOmniture = OMNIPAGE_HELP_HOME_START;
    self.containerView.hidden = YES;
    self.navigationItem.hidesBackButton = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    //[_startLiveChatButton getSuperButtonStyle];
    
    UIImage *backImage = nil;
    if (_isLaunchingFromHomeScreen)
    {
        backImage = [UIImage imageNamed:@"Icon_Home_White"];
    }
    else
    {
        backImage = [UIImage imageNamed:@"Back_Arrow"];
    }

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:backImage style:UIBarButtonItemStylePlain target:self action:@selector(backButtonAction:)];
    self.navigationItem.leftBarButtonItem = backButton;

    self.title = @"Help and support";
    self.view.backgroundColor = [UIColor whiteColor];
    
    _callTheHelpDeskButton.backgroundColor = [UIColor clearColor];
    _callTheHelpDeskButton.layer.borderWidth = 1.0;
    _callTheHelpDeskButton.layer.borderColor = [BrandColours colourTextBTPurplePrimaryColor].CGColor;
    
    _startLiveChatButton.backgroundColor = [UIColor clearColor];
    _startLiveChatButton.layer.borderWidth = 1.0;
    _startLiveChatButton.layer.borderColor = [BrandColours colourTextBTPurplePrimaryColor].CGColor;
    
    _tableViewOut.backgroundColor = [UIColor whiteColor];
    _tableViewOut.estimatedRowHeight = 101.0f;
    _tableViewOut.rowHeight = UITableViewAutomaticDimension;

    self.helpAndSupportScreenViewModel = [[DLMHelpAndSupportScreen alloc] init];
    self.helpAndSupportScreenViewModel.helpAndSupportScreenDelegate = self;

    if (_isDataAlreadyFetched)
    {
        self.helpAndSupportScreenViewModel.specificCategoryArray = _specificCategory;
        self.helpAndSupportScreenViewModel.generalCategoryArray = _generalCategory;
        self.helpAndSupportScreenViewModel.currentTime = _currentTime;
        
        [self updateUI];
    }
    else
    {
        [self fetchHelpAndSupportData];
    }

    [self trackPage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self.helpAndSupportScreenViewModel cancelHelpAndSupportApi];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


#pragma mark - Class Methods

+ (BTHelpAndSupportViewController *)getHelpAndSupportViewController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTHelpAndSupportViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"BTHelpAndSupportViewController"];

    return controller;
}

#pragma mark - Private Methods

- (void)trackPage
{
    NSDictionary *dict = [AppManager getOmniDictionary];
    [OmnitureManager trackPage:_pageNameForOmniture withContextInfo:dict];
}


- (void)trackClickForOmniture:(NSString *)clickEvent
{
    NSString *pageName = _pageNameForOmniture;
    NSString *linkTitle = clickEvent;
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:data];
    
}


- (void)fetchHelpAndSupportData {

    if([AppManager isInternetConnectionAvailable]) {
        [self hideRetryItems:YES];
        [self createLoadingView];
        [_loadingView startAnimatingLoadingIndicatorView];
        [self.helpAndSupportScreenViewModel fetchHelpAndSupportScreenData];
    }
    else {

        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:_pageNameForOmniture];
        [_startLiveChatButton setTitle:@"Live chat is currently unavailable" forState:UIControlStateNormal];
        _startLiveChatButton.enabled = NO;
        self.containerView.hidden = NO;
    }
}


- (void)updateUI {

    NSInteger holidayCount = 0;

    self.containerView.hidden = NO;

    for (BTSpecificCategory *specificCategoryObj in self.helpAndSupportScreenViewModel.specificCategoryArray) {
        if(specificCategoryObj.markedAsHoliday) {
            holidayCount = holidayCount + 1;
        }
    }

    if(holidayCount == 3) {
        [_startLiveChatButton setTitle:@"Live chat is currently closed" forState:UIControlStateNormal];
        _startLiveChatButton.enabled = NO;
        
        _pageNameForOmniture = OMNIPAGE_HELP_HOME_START_CLOSED;

    }
    else if(holidayCount < 3) {

        for (BTSpecificCategory *specificCategoryObj in self.helpAndSupportScreenViewModel.specificCategoryArray) {
            if(!specificCategoryObj.markedAsHoliday) {

                if([[specificCategoryObj.categoryName lowercaseString] isEqualToString:@"orders"]) {

                    if([self isCurrentTimeInBetweenStartTime:specificCategoryObj.startTime endTime:specificCategoryObj.endTime withCurrentTime:self.helpAndSupportScreenViewModel.currentTime])
                        _isOrderTimeLies = YES;


                }
                else if([[specificCategoryObj.categoryName lowercaseString] isEqualToString:@"faults"]) {

                    if([self isCurrentTimeInBetweenStartTime:specificCategoryObj.startTime endTime:specificCategoryObj.endTime withCurrentTime:self.helpAndSupportScreenViewModel.currentTime])
                        _isFaultTimeLies = YES;

                }
                if([[specificCategoryObj.categoryName lowercaseString] isEqualToString:@"billing"]) {

                    if([self isCurrentTimeInBetweenStartTime:specificCategoryObj.startTime endTime:specificCategoryObj.endTime withCurrentTime:self.helpAndSupportScreenViewModel.currentTime])
                        _isBillTimeLies = YES;

                }
            }
        }

        if(_isOrderTimeLies || _isFaultTimeLies || _isBillTimeLies)
        {
        [_startLiveChatButton setTitle:@"Start a live chat" forState:UIControlStateNormal];
            
            _pageNameForOmniture = OMNIPAGE_HELP_HOME_START;
        }
        else
        {
            [_startLiveChatButton setTitle:@"Live chat is currently closed" forState:UIControlStateNormal];
              _startLiveChatButton.enabled = NO;
            _pageNameForOmniture = OMNIPAGE_HELP_HOME_START_CLOSED;
        }
    }
    
    [self trackPage];

}




- (void)createLoadingView {

    if(!_loadingView) {

        _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        _loadingView.translatesAutoresizingMaskIntoConstraints = NO;

        [self.view addSubview:_loadingView];
    }

    _loadingView.hidden = NO;
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}

- (void)hideLoadingItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to loading.
    [_loadingView setHidden:isHide];

}



- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow  {

    if(_retryView) {

        _retryView.hidden = NO;
        _retryView.retryViewDelegate = self;
        [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
        return;
    }
    _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
    _retryView.translatesAutoresizingMaskIntoConstraints = NO;
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
    _retryView.retryViewDelegate = self;

    [self.view addSubview:_retryView];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];


    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];

}

- (void)hideRetryItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to retry.
    [_retryView setHidden:isHide];

}



-(BOOL)isCurrentTimeInBetweenStartTime:(NSString *)startTime endTime:(NSString *)endTime withCurrentTime:(NSString *)currentTime {

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];

    NSDate *sTime, *eTime,*cTime;
    sTime = [dateFormatter dateFromString:startTime];
    eTime = [dateFormatter dateFromString:endTime];
    cTime = [dateFormatter dateFromString:currentTime];

    if(([cTime compare:sTime] == NSOrderedDescending || [cTime compare:sTime] == NSOrderedSame) && ([cTime compare:eTime] ==  NSOrderedAscending || [cTime compare:eTime] == NSOrderedSame)) {

        return  YES;
    }

    return NO;

}

- (void)showWebviewForLiveChatOptionsWith:(NSString *)screenName {
    
    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
    
    if([screenName isEqualToString:@"Billing"])
    {
        viewController.targetURLType = BTTargetURLTypeBillingChatScreen;
    }
    else if ([screenName isEqualToString:@"Faults"])
    {
        viewController.targetURLType = BTTargetURLTypeFaultsChatScreen;
    }
    else if([screenName isEqualToString:@"Orders"])
    {
        viewController.targetURLType = BTTargetURLTypeOrdersChatScreen;
    }
    else if([screenName isEqualToString:@"Cloud Voice Express"]) {
        viewController.targetURLType = BTTargetURLTypeCloudVoiceExpressChatScreen;
    } // Newlly Added
    else if([screenName isEqualToString:@"Broadband"]) {
        viewController.targetURLType = BTTargetURLTYPEReportAFaultBroadband;
    }
    else if([screenName isEqualToString:@"Phone line"]) {
        viewController.targetURLType = BTTargetURLTYPEReportAFaultPhoneline;
    }
    else if([screenName isEqualToString:@"Email"]) {
        viewController.targetURLType = BTTargetURLTYPEReportAFaultEmail;
    }
    else if([screenName isEqualToString:@"BT Cloud Voice"]) {
        viewController.targetURLType = BTTargetURLTYPEReportAFaultBTCloudVoice;
    }
    
    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:NO andIsNeedCloseButton:YES];
    
    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{
        
    }];
}

- (void)redirectToFAQsScreen
{
    BTFAQsViewController *faqViewController = [[BTFAQsViewController alloc] init];
    
    [self.navigationController pushViewController:faqViewController animated:YES];
    
//    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:faqViewController];
//    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self presentViewController:navController animated:YES completion:nil];
//    });
    
}

#pragma mark - Action Methods


- (void)backButtonAction:(id)sender {

    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)reportaFaultAction:(UIButton *)sender {
    
    [self trackClickForOmniture:OMNICLICK_HELP_REPORTAFAULT];
    
    if(_isOrderTimeLies && _isFaultTimeLies && _isBillTimeLies) {
        
        [OmnitureManager trackPage:OMNIPAGE_HELP_CHAT_CATEGORY withContextInfo:[AppManager getOmniDictionary]];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Select the product you’re having a problem with:" preferredStyle:UIAlertControllerStyleActionSheet];
        
                UIAlertAction *actionBroadBand = [UIAlertAction actionWithTitle:@"Broadband"
                                                                 style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                                                       [self showWebviewForLiveChatOptionsWith:@"Broadband"];
                                                                     
                                                                 }];
                [alert addAction:actionBroadBand];
        
                UIAlertAction *actionPhoneline = [UIAlertAction actionWithTitle:@"Phone line"
                                                                 style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                                                      [self showWebviewForLiveChatOptionsWith:@"Phone line"];
                                                                 }];
                [alert addAction:actionPhoneline];
        
                UIAlertAction *actionEmail = [UIAlertAction actionWithTitle:@"Email"
                                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                                               [self showWebviewForLiveChatOptionsWith:@"Email"];
                                                                          }];
                [alert addAction:actionEmail];
        
                UIAlertAction *actionBTCloudVoice = [UIAlertAction actionWithTitle:@"BT Cloud Voice"
                                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                          [alert dismissViewControllerAnimated:YES completion:nil];
                                                                           [self showWebviewForLiveChatOptionsWith:@"BT Cloud Voice"];
                                                                      }];
                [alert addAction:actionBTCloudVoice];
 
        
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                         style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                         }];
                [alert addAction:cancel];
        
        alert.popoverPresentationController.sourceRect = self.reportAFaultButton.frame;
        alert.popoverPresentationController.canOverlapSourceViewRect = NO;
        alert.popoverPresentationController.sourceView = self.view;
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        
    }
    
}

- (IBAction)callTheHelpdeskAction:(id)sender {
    
    [self trackClickForOmniture:OMNICLICK_HELP_CALL_HELP_DESK];
    
    NSURL *helpdeskUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt://%@",HELP_DESK_NUMBER]];
    
    if ([[UIApplication sharedApplication] canOpenURL:helpdeskUrl]) {
        [[UIApplication sharedApplication] openURL:helpdeskUrl];
    }

}


- (IBAction)liveChatButtonAction:(id)sender {
    
    [self trackClickForOmniture:OMNICLICK_HELP_STARTLIVECHAT];
    
    if(_isOrderTimeLies && _isFaultTimeLies && _isBillTimeLies) {

        [OmnitureManager trackPage:OMNIPAGE_HELP_CHAT_CATEGORY withContextInfo:[AppManager getOmniDictionary]];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"What's your question about?" preferredStyle:UIAlertControllerStyleActionSheet];


        // (SD) Add group names in actionsheet
        if (self.helpAndSupportScreenViewModel.specificCategoryArray != nil && self.helpAndSupportScreenViewModel.specificCategoryArray.count > 0) {

            for (BTSpecificCategory *category in self.helpAndSupportScreenViewModel.specificCategoryArray) {
                
                if (![category.categoryName isEqualToString:@"Faults"]) {
                    
                    UIAlertAction *action = [UIAlertAction actionWithTitle:category.categoryName
                                                                     style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                                         
                                                                         
                                                                         UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Choose your service" preferredStyle:UIAlertControllerStyleActionSheet];
                                                                         
                                                                         
                                                                         // (SD) Add group names in actionsheet
                                                                         if ([category.categoryName isEqualToString:@"Billing"]) {
                                                                             UIAlertAction *action = [UIAlertAction actionWithTitle:@"Cloud Voice Express"
                                                                                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                                                                                  [alert dismissViewControllerAnimated:YES completion:nil];
                                                                                                                                  [self showWebviewForLiveChatOptionsWith:@"Cloud Voice Express"];
                                                                                                                              }];
                                                                             [alert addAction:action];
                                                                             
                                                                             UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"All other services"
                                                                                                                               style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                                                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                                                                                                                   [self showWebviewForLiveChatOptionsWith:category.categoryName];
                                                                                                                               }];
                                                                             [alert addAction:action1];
                                                                         }
                                                                         else if ([category.categoryName isEqualToString:@"Orders"]) {
                                                                             
                                                                             UIAlertAction *action = [UIAlertAction actionWithTitle:@"Cloud Voice Express"
                                                                                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                                                                                  [alert dismissViewControllerAnimated:YES completion:nil];
                                                                                                                                  [self showWebviewForLiveChatOptionsWith:@"Cloud Voice Express"];
                                                                                                                              }];
                                                                             [alert addAction:action];
                                                                             
                                                                             UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"All other services"
                                                                                                                               style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                                                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                                                                                                                   [self showWebviewForLiveChatOptionsWith:category.categoryName];
                                                                                                                               }];
                                                                             [alert addAction:action1];
                                                                         }
                                                                         else {
                                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                                             [self showWebviewForLiveChatOptionsWith:category.categoryName];
                                                                         }
                                                                         
                                                                         UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                                                                                          style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                                                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                                                                                          }];
                                                                         [alert addAction:cancel];
                                                                         
                                                                         alert.popoverPresentationController.sourceRect = self.startLiveChatButton.frame;
                                                                         alert.popoverPresentationController.canOverlapSourceViewRect = NO;
                                                                         alert.popoverPresentationController.sourceView = self.view;
                                                                         [self presentViewController:alert animated:YES completion:^{
                                                                             
                                                                         }];
                                                                         
                                                                     }];
                    [alert addAction:action];
                    
                }
            }
        }

        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                         style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                         }];
        [alert addAction:cancel];
        
        alert.popoverPresentationController.sourceView = self.view;
        alert.popoverPresentationController.sourceRect = self.startLiveChatButton.frame;
        alert.popoverPresentationController.canOverlapSourceViewRect = NO;
        
        [self presentViewController:alert animated:YES completion:^{

        }];


    }
    else {

        BTHelpLiveChatViewController *liveChatViewController = [BTHelpLiveChatViewController getHelpAndSupportChatViewController];
        liveChatViewController.helpAndSupportScreenViewModel = self.helpAndSupportScreenViewModel;
        [self.navigationController pushViewController:liveChatViewController animated:YES];
        
    }


}

#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *cellIdentifire = @"popularFAQCell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifire];

    if (cell == nil) {

        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifire];
    }

    cell.textLabel.text = @"";
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0 && indexPath.row == 0)
    {
        [self trackClickForOmniture:OMNICLICK_HELP_POPULAR_FAQ];
        
        [self redirectToFAQsScreen];
    }
}


#pragma mark - DLMHelpAndSupportScreenDelegate

- (void)successfullyFetchedHelpAndSupportData:(DLMHelpAndSupportScreen *)helpAndSupportModel {

    self.helpAndSupportScreenViewModel = helpAndSupportModel;
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];

    [self updateUI];

}

- (void)failedToFetchHelpAndSupportScreenWithError:(NLWebServiceError *)webServiceError {

    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];

    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO) {

        switch (webServiceError.error.code) {

            case BTNetworkErrorCodeAPINoDataFound: {
                [AppManager trackNoDataFoundErrorOnPage:_pageNameForOmniture];
                errorHandled = NO;
                break;
            }
            default: {
                errorHandled = NO;
                break;
            }
        }
    }

    if(errorHandled == NO) {

        [self hideLoadingItems:YES];
        [_loadingView stopAnimatingLoadingIndicatorView];
        [self showRetryViewWithInternetStrip:NO];
        [AppManager trackGenericAPIErrorOnPage:_pageNameForOmniture];
        [_startLiveChatButton setTitle:@"Live chat is currently unavailable" forState:UIControlStateNormal];
        _startLiveChatButton.enabled = NO;
    }
    
}



#pragma mark - BTRetryViewDelegate

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView {

    [self fetchHelpAndSupportData];
}

@end
