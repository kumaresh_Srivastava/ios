//
//  HelpAndSupportViewController.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 03/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BTHelpAndSupportViewController : UIViewController

@property (nonatomic) BOOL isLaunchingFromHomeScreen;
@property (nonatomic) BOOL isDataAlreadyFetched;

@property (nonatomic, strong) NSArray *specificCategory;
@property (nonatomic, strong) NSArray *generalCategory;
@property (nonatomic, strong) NSString *currentTime;

+ (BTHelpAndSupportViewController *)getHelpAndSupportViewController;

@end
