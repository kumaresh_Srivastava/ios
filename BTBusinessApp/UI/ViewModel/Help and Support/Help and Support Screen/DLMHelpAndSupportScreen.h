//
//  DLMHelpAndSupportScreen.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 09/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "DLMObject.h"


@class DLMHelpAndSupportScreen;
@class NLWebServiceError;

@protocol DLMHelpAndSupportScreenDelegate

- (void)successfullyFetchedHelpAndSupportData:(DLMHelpAndSupportScreen *)helpAndSupportModel;

- (void)failedToFetchHelpAndSupportScreenWithError:(NLWebServiceError *)webServiceError;

@end


@interface DLMHelpAndSupportScreen : DLMObject

@property (nonatomic, strong)NSArray *generalCategoryArray;
@property (nonatomic, strong)NSArray *specificCategoryArray;
@property (nonatomic, strong)NSString *currentTime;


@property (nonatomic, weak) id<DLMHelpAndSupportScreenDelegate> helpAndSupportScreenDelegate;


- (void)fetchHelpAndSupportScreenData;
- (void)cancelHelpAndSupportApi;

@end
