//
//  UpdateContactInerceptViewController.m
//  BTBusinessApp
//
//  Created by vectoscalar on 20/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTUpdateContactInerceptViewController.h"
#import "UpdateContactTableViewHeader.h"
#import "BTInterceptorUpdateContactTableViewCell.h"
#import "BTAlertBannerView.h"
#import "BTEditContactInerceptViewController.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "AppManager.h"
#import "NLWebServiceError.h"
#import "UIViewController+WebServiceErrorHandling.h"
#define kCellId @"BTInterceptorUpdateContactTableViewCellID"
#define kCellNib @"BTInterceptorUpdateContactTableViewCell"
#define kHeaderNib @"UpdateContactTableViewHeader"


#define kContactInterceptor  @"Update your details"
#define kAlertTitle @""
#define kAlertMessage @"Please fill all required fields."

#define kCancelDataAlertMessage @"Are you sure you want to discard the changes?"
#define kConfirmAlertMessage @"Are you sure you want to make the changes?"
#define kCancelDataAlertTitle @""

#define kEstimatedRowHeight 80
#define kTableViewYPos 0
#define kAlertBannerHeight 40


@interface BTUpdateContactInerceptViewController ()<UITableViewDelegate,UITableViewDataSource, DLMUpdateContactInerceptScreenDelegate,BTEditContactInterceptDelegate>
{
    BTAlertBannerView *_alertBannerView;
    NSMutableArray  * _contactDetailsArray;
}
@property (strong,nonatomic) UITableView *mTableView;
@property (strong,nonatomic) DLMUpdateContactInerceptScreen *updateContactScreenModel;

@end

@implementation BTUpdateContactInerceptViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.title = kContactInterceptor;
    self.navigationItem.hidesBackButton = YES;
    self.view.backgroundColor = [UIColor whiteColor];


    //Initializing View Model
    self.updateContactScreenModel = [[DLMUpdateContactInerceptScreen alloc] init];
    self.updateContactScreenModel.updateContactInerceptScreenDelegate = self;

    //Create and configure table view
    [self createAndConfigureTableView];


     //Manage Navigation
    [self addSkipOnRightNavigationItem];

    //Create Data
    [self createData];
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self setNetworkRequestInProgress:NO];
    [self hideProgressView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}


#pragma mark - Private helper method.
- (void)addConfirmOnRightNavigationItem
{
    self.navigationItem.rightBarButtonItem = nil;
    UIBarButtonItem *confirmButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Confirm" style:UIBarButtonItemStylePlain target:self action:@selector(confirmButtonPressed:)];
    
    [self.navigationItem setRightBarButtonItem:confirmButtonItem];
}


- (void)addSkipOnRightNavigationItem
{
     self.navigationItem.rightBarButtonItem = nil;
    UIBarButtonItem *confirmButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Skip" style:UIBarButtonItemStylePlain target:self action:@selector(skipButtonPressed)];
    
    [self.navigationItem setRightBarButtonItem:confirmButtonItem];
    
    
}

- (void)addCancelButtonOnLeftNavigationItem
{
    self.navigationItem.leftBarButtonItem = nil;
    UIBarButtonItem *cancelButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed)];
    
    [self.navigationItem setLeftBarButtonItem:cancelButtonItem];
    
    
}


- (void)createData
{
   if(!_contactDetailsArray)
       _contactDetailsArray = [[NSMutableArray alloc] init];
    
    _contactDetailsArray = [NSMutableArray arrayWithArray:[self.updateContactScreenModel getContactWrappers]];

    [self.mTableView reloadData];
}


- (void)createAndConfigureTableView
{
    if(!self.mTableView)
        self.mTableView = [[UITableView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height-64)];
    
    self.mTableView.delegate = self;
    self.mTableView.dataSource = self;
    self.mTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.mTableView.bounces = NO;
    //Get Header View
    self.mTableView.tableHeaderView  = [self getTableViewHeader];
    
    //Register for Automatic Dimenstion
    self.mTableView.rowHeight = UITableViewAutomaticDimension;
    self.mTableView.estimatedRowHeight = kEstimatedRowHeight;
    
    //Register cells
    UINib *nib = [UINib nibWithNibName:kCellNib bundle:nil];
    [self.mTableView registerNib:nib forCellReuseIdentifier:kCellId];
    
    [self.view addSubview:self.mTableView];
    
    [self.view bringSubviewToFront:_loadingView];
    
}



- (UpdateContactTableViewHeader *)getTableViewHeader
{
    UpdateContactTableViewHeader *headerView = [[[NSBundle mainBundle] loadNibNamed:kHeaderNib owner:nil options:nil] firstObject];
    
    return headerView;
}



- (void)resetAllChangedInfoToOriginalState
{
    [_contactDetailsArray removeAllObjects];
    [_contactDetailsArray addObjectsFromArray:[self.updateContactScreenModel getContactWrappers]];
    [self.mTableView reloadData];
    self.navigationItem.leftBarButtonItem = nil;
    [self addSkipOnRightNavigationItem];
    
}


- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
    }];
    
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:^{
        
    }];
    
}


- (void)showAlertForCancelRequestWithTitle:(NSString *)title andMessage:(NSString *)message
{
    
    __weak typeof(self) weakSelf = self;
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [weakSelf resetAllChangedInfoToOriginalState];
        
    }];
    
    [alertController addAction:okAction];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:^{
        
    }];
    
}

- (void)redirectToHomeScreen
{
    
    [self.delegate didFinishSucessfullyOnBTUpdateContactInerceptViewController:self];
   // [self dismissViewControllerAnimated:YES completion:nil];
    
}


- (void)showAlertForConfirmActionWithTitle:(NSString *)title andMessage:(NSString *)message
{
    __weak typeof(self) weakSelf = self;
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [weakSelf makeAPIRequest];
        
    }];
    
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:^{
        
    }];
    
}


#pragma mark - Table view delegate methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_contactDetailsArray count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTInterceptorUpdateContactTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellId];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSInteger index = indexPath.row;
    
    //Manage top separator view
    if(index == 0)
        [cell removeSeparator:NO];
    else
        [cell removeSeparator:YES];
    
    
    //Update Cell
    BTInterceptContactWrapper *contactWrapper = [_contactDetailsArray objectAtIndex:index];
    [cell updateDataWithContactModel:contactWrapper];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTEditContactInerceptViewController *editContactVC = (BTEditContactInerceptViewController *)[storyboard instantiateViewControllerWithIdentifier:@"BTEditContactInerceptViewControllerID"];
    editContactVC.delegate = self;
    editContactVC.contactwrapper = [_contactDetailsArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:editContactVC animated:YES];
}


#pragma mark - Green Strip Hide/Show methods
- (void)showAlertBannerWithMessage:(NSString *)alertMessage{
    
    if(_alertBannerView.alpha > 0.5)
        return;
    
    if(!_alertBannerView){
        
        _alertBannerView = [[[NSBundle mainBundle] loadNibNamed:@"BTAlertBannerView" owner:self options:nil] firstObject];
        
        _alertBannerView.frame = CGRectMake(0,
                                            kTableViewYPos,
                                            self.view.frame.size.width,
                                            kAlertBannerHeight);
        
        [self.view addSubview:_alertBannerView];
    }
    
    [_alertBannerView showAnimated:YES];
    
    [_alertBannerView setMessage:alertMessage];
    
    CGRect frame = self.mTableView.frame;
    frame.origin.y  = kTableViewYPos+kAlertBannerHeight;
    self.mTableView.frame = frame;
    [UIView animateWithDuration:kBannerAnimationTime animations:^{
        
        //[self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        [self performSelector:@selector(hideAlertBanner) withObject:nil afterDelay:3.0];
    }];
    
    
}


- (void)hideAlertBanner{
    
    [_alertBannerView hideAnimated:YES];
    
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:kBannerAnimationTime animations:^{
        
        CGRect frame = self.mTableView.frame;
        frame.origin.y  = kTableViewYPos;
        self.mTableView.frame = frame;
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
    }];
    
}



#pragma mark - Manage Navigation Action Methods

- (void)skipButtonPressed
{
    
    if(![AppManager isInternetConnectionAvailable])
    {
        [self showAlertWithTitle:kNoConnectionTitle andMessage:kNoConnectionMessage];

        return;
    }
    
    if(self.intercepterType == UpdateContactIntercepter)
    {
        [self.delegate makeUpdateContactInterceptorAPIRequestInBackgroundWithContactArray:_contactDetailsArray];
        [self redirectToHomeScreen];
        
    }
    else
    {
        //Check reuquired field are field or not
        NSString *errorMessage = [self.updateContactScreenModel getErrorMessageForRequiredField:_contactDetailsArray];
        if([errorMessage isEqualToString:@""])
        {
                //All fields are filled
            [self.delegate makeUpdateContactInterceptorAPIRequestInBackgroundWithContactArray:_contactDetailsArray];
             [self redirectToHomeScreen];
        }
        else
        {
            [self showAlertWithTitle:@"Message" andMessage:@"Please tell us your name and title!"];
        }
    }
}


- (void)confirmButtonPressed:(id)sender
{
    if(self.networkRequestInProgress)
       return;

    
    //If intercepter is Update contact just confirm blindly
    if(self.intercepterType == UpdateContactIntercepter)
    {
        NSString *sameEmailErrorMessage = [self.updateContactScreenModel getErrorMessageForSameEmailInContactArray:_contactDetailsArray];

        if(![sameEmailErrorMessage isEqualToString:@""])
        {
            //Email are not same
            [self showAlertWithTitle:@"Message" andMessage:sameEmailErrorMessage];
            return;
        }



        [self showAlertForConfirmActionWithTitle:@"" andMessage:kConfirmAlertMessage];
    }
    else
    { //Check reuquired field are field or not
        NSString *errorMessage = [self.updateContactScreenModel getErrorMessageForRequiredField:_contactDetailsArray];
        
        //No error is found
        if([errorMessage isEqualToString:@""])
        {
            //All fields are filled

            NSString *sameEmailErrorMessage = [self.updateContactScreenModel getErrorMessageForSameEmailInContactArray:_contactDetailsArray];

            if(![sameEmailErrorMessage isEqualToString:@""])
            {
                //Email are not same
                [self showAlertWithTitle:@"Message" andMessage:sameEmailErrorMessage];
                return;
            }

            [self showAlertForConfirmActionWithTitle:@"" andMessage:kConfirmAlertMessage];
        }
        else
        {
             [self showAlertWithTitle:kAlertTitle andMessage:errorMessage];
        }

    
    }
}

- (void)cancelButtonPressed
{
    if(self.networkRequestInProgress)
        return;
    
    [self showAlertForCancelRequestWithTitle:kCancelDataAlertTitle andMessage:kCancelDataAlertMessage];
}


#pragma mark - API Request method
- (void)makeAPIRequest
{
   if(![AppManager isInternetConnectionAvailable])
   {
       [self showAlertWithTitle:kNoConnectionTitle andMessage:kNoConnectionMessage];
       return;
   }
    
    NSString *password = [SAMKeychain passwordForService:kKeyChainServiceBTUserPassword account:[AppDelegate sharedInstance].viewModel.app.loggedInUser.username];
    
    NSDictionary *userDetailDict = [self.updateContactScreenModel getUserDataDictionaryForContactWrapperArray:_contactDetailsArray];
    [self showProgressView];
    self.networkRequestInProgress = YES;
    [self.updateContactScreenModel submitAndCreateContactDetailsModelWithContactDetailsDictionary:userDetailDict andPassword:password];

}



#pragma mark - DLMUpdateContactInerceptScreen Delegates

- (void)updateSuccessfullyFinishedByUpdateContactInerceptScreen:(DLMUpdateContactInerceptScreen *)updateContactInerceptScreen
{
    self.networkRequestInProgress = YES;
    [self hideProgressView];
  
    [self.updateContactScreenModel updateLocalDatabaseWithContacts:_contactDetailsArray];
    
    [self redirectToHomeScreen];

}


- (void)updateContactInerceptScreen:(DLMUpdateContactInerceptScreen *)updateContactInerceptScreen failedToSubmitUpdateContactInerceptScreenWithWebServiceError:(NLWebServiceError *)webServiceError
{
    self.networkRequestInProgress = NO;
    [self hideProgressView];
    BOOL errorHandled = [self attemptVordelProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
    
    if(errorHandled == NO)
    {
         [self showAlertWithTitle:@"Message" andMessage:kDefaultErrorMessage];
    }

}



#pragma mark - BTEditContactInterceptScreen Delegate

- (void)userUpdatedDataOnEditContactInerceptViewController:(BTInterceptContactWrapper *)updateContactWrapper andOldContact:(BTInterceptContactWrapper*)oldeContact
{
    NSInteger index = [_contactDetailsArray indexOfObject:oldeContact];
    [_contactDetailsArray removeObject:oldeContact];
    [_contactDetailsArray insertObject:updateContactWrapper atIndex:index];
    
    if([self.updateContactScreenModel isLocalInfoChangedFor:_contactDetailsArray])
    {
        [self addConfirmOnRightNavigationItem];
        [self addCancelButtonOnLeftNavigationItem];
        [self showAlertBannerWithMessage:[NSString stringWithFormat:@"%@ updated",updateContactWrapper.titleString]];
        
    }
    else
    {
        [self addSkipOnRightNavigationItem];
        self.navigationItem.leftBarButtonItem = nil;
    }
  
    [self.mTableView reloadData];

}


@end
