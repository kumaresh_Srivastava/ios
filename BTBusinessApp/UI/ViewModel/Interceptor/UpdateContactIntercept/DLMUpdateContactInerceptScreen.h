//
//  DLMUpdateContactInerceptScreen.h
//  BTBusinessApp
//
//  Created by vectoscalar on 20/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DLMObject.h"

typedef enum intercepterType
{
    UpdateContactIntercepter,
    UpdateProfileIntercepter
}IntercepterType;

#define kUserTitle @"Title"
#define kUserFirstName @"First name"
#define kUserLastName @"Last name"
#define kUserMobile @"Mobile"
#define kUserLandline @"Landline"
#define kUserEmail @"Email"
#define kUserAltEmail @"Alt. Email"



@class DLMUpdateContactInerceptScreen;
@class BTInterceptContactWrapper;
@class NLWebServiceError;

@protocol DLMUpdateContactInerceptScreenDelegate <NSObject>

- (void)updateSuccessfullyFinishedByUpdateContactInerceptScreen:(DLMUpdateContactInerceptScreen *)updateContactInerceptScreen;

- (void)updateContactInerceptScreen:(DLMUpdateContactInerceptScreen *)updateContactInerceptScreen failedToSubmitUpdateContactInerceptScreenWithWebServiceError:(NLWebServiceError *)webServiceError;

@end


@interface DLMUpdateContactInerceptScreen : DLMObject

- (NSArray *)getContactWrappers;
- (NSDictionary *)getUserDataDictionaryForContactWrapperArray:(NSArray*)contactWrapperArray;

- (BOOL)isLocalInfoChangedFor:(NSArray *)contactWrapperArray;
- (void)updateLocalDatabaseWithContacts:(NSArray *)contactArray;

@property (nonatomic, weak) id <DLMUpdateContactInerceptScreenDelegate> updateContactInerceptScreenDelegate;

- (void)submitAndCreateContactDetailsModelWithContactDetailsDictionary:(NSDictionary *)updatedContactDetailsDict andPassword:(NSString *)password;
- (NSString *)getErrorMessageForRequiredField:(NSArray *)contactWrapperArray;

- (NSString *)getErrorMessageForSameEmailInContactArray:(NSArray *)contactWrapperArray;

@end


@interface BTInterceptContactWrapper : NSObject {
    
}

@property(nonatomic, readonly) NSString *titleString;
@property(nonatomic, readonly) NSString *ValueString;
@property(nonatomic, assign) BOOL isRequired;
- (id)initWithTitle:(NSString *)title andValue:(NSString *)valueString andIsRequiredField:(BOOL)isRequired;


@end



