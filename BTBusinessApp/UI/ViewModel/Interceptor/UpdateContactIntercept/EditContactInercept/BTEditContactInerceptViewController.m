//
//  BTEditContactInerceptViewController.m
//  BTBusinessApp
//
//  Created by vectoscalar on 20/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTEditContactInerceptViewController.h"
#import "PasswordInputView.h"
#import "AppConstants.h"
#import "DLMEditContactInterceptScreen.h"
#import "UITextField+BTTextFieldProperties.h"

#define kTextfieldHeightConstraint 48

@interface BTEditContactInerceptViewController ()<UITextFieldDelegate>
{
    BOOL _isUILoaded;
}
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *valueTextfield;
@property (strong,nonatomic) PasswordInputView *textfieldView;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (strong,nonatomic) DLMEditContactInterceptScreen *editContactInterceptModel;
@property (strong,nonatomic) UIView *separatorViewForKeyboard;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textfieldTrailingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textfieldHeightConstraint;

@end

@implementation BTEditContactInerceptViewController
@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.editContactInterceptModel = [[DLMEditContactInterceptScreen alloc] init];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.valueTextfield getBTTextFieldStyle];
    
    //Manage Inital Population of Data
    [self manageInitialPopulationOfData];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if(!_isUILoaded)
    {
        _isUILoaded = YES;
        [self configureUI];
    }
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    self.separatorViewForKeyboard.hidden  =YES;
}

#pragma mark - Private helper methods
- (void)manageInitialPopulationOfData
{
    self.navigationItem.title = [self.editContactInterceptModel getNavigationTitleForString:self.contactwrapper.titleString];
    self.titleLabel.text = [self.editContactInterceptModel getTextFieldTitleForString:self.contactwrapper.titleString];
    self.valueTextfield.text = self.contactwrapper.ValueString;
    
    //Change the type of keyboard
    [self changeKeyboardTypeForTitle:self.contactwrapper.titleString];
    
    [self addTapGestureToView];
    [self.valueTextfield becomeFirstResponder];
    
    
    //Navigation
    [self addLeftNavigationItem];
    
}


- (void)configureUI
{
    //Code to change text field into dropdown
    if([[self.contactwrapper.titleString lowercaseString] containsString:@"title"])
    {
        
        self.textfieldHeightConstraint.constant = kTextfieldHeightConstraint;
        self.textfieldTrailingConstraint.constant = self.textfieldTrailingConstraint.constant + (kScreenSize.width - 1.6*self.textfieldTrailingConstraint.constant)/2;
        UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"downArrow_Gray"]];
        arrow.frame = CGRectMake(-5,
                                 0.0,
                                 arrow.image.size.width+10.0,
                                 arrow.image.size.height);
        arrow.contentMode = UIViewContentModeCenter;
        
        self.valueTextfield.rightView = arrow;
        self.valueTextfield.rightViewMode = UITextFieldViewModeAlways;
        
        self.valueTextfield.enabled = NO;
        
        CGRect frame = CGRectMake(self.valueTextfield.frame.origin.x, self.valueTextfield.frame.origin.y, self.textfieldTrailingConstraint.constant + (kScreenSize.width - 1.5*self.textfieldTrailingConstraint.constant)/2, self.valueTextfield.frame.size.height);
        
        UIButton *btn = [[UIButton alloc] initWithFrame:frame];
        // btn.backgroundColor = [UIColor redColor];
        [btn addTarget:self action:@selector(openTitleSelction) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        
    }
    
}


- (void)openTitleSelction{
    __weak typeof(self) weakSelf = self;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Select Title" preferredStyle:UIAlertControllerStyleActionSheet];
    
    NSArray *titlesArray = [self.editContactInterceptModel getTitlesArray];
    
    for(NSString *title in titlesArray){
        
        UIAlertAction *titleAction = [UIAlertAction actionWithTitle:title
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  
                                                                  weakSelf.valueTextfield.text = title;
                                                                  
                                                                  if([weakSelf.contactwrapper.ValueString isEqualToString:title])
                                                                  {//If user selected same value then remove the save button
                                                                      weakSelf.navigationItem.rightBarButtonItem = nil;
                                                                  }else
                                                                  {
                                                                      //user selected different item add save button
                                                                      [weakSelf addRightNavigationItem];
                                                                  }
                                                                  [alert dismissViewControllerAnimated:YES completion:nil];
                                                              }];
        
        [alert addAction:titleAction];
        
    }
    
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    [alert addAction:cancel];
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = self.valueTextfield.frame;
    alert.popoverPresentationController.canOverlapSourceViewRect = NO;
    
    [self presentViewController:alert animated:YES completion:^{
        
    }];
}



- (void)addTapGestureToView
{
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedIutside)];
    [self.view addGestureRecognizer:gesture];
}


- (void)userTappedIutside
{
    [self.view endEditing:YES];
}


- (void)removeErrorMessage
{
    self.errorLabel.text = @"";
    self.valueTextfield.layer.borderWidth = 1.0;
    self.valueTextfield.layer.borderColor = [BrandColours colourBtNeutral60].CGColor;
    self.titleLabel.textColor = [BrandColours colourBtNeutral90];
}


- (void)showErrorWithMessage:(NSString *)errorMessage{
    
    self.errorLabel.text = errorMessage;
    self.valueTextfield.layer.borderColor = [BrandColours colourMyBtRed].CGColor;
    self.valueTextfield.layer.borderWidth = 1.0;
    self.errorLabel.textColor = [BrandColours colourMyBtRed];
    self.titleLabel.textColor = [BrandColours colourMyBtRed];
}



- (void)manageSaveActionVisibilityForText:(NSString *)updatedString
{
    if(self.contactwrapper.isRequired)
    {
        if(updatedString.length >0 && ![updatedString isEqualToString:@""])
        {
            if([[updatedString lowercaseString] isEqualToString:[self.contactwrapper.ValueString lowercaseString]])
            {
                self.navigationItem.rightBarButtonItem = nil;
            }
            else
            {
                [self addRightNavigationItem];
            }
            
        }
        else
        {
            self.navigationItem.rightBarButtonItem = nil;
        }
        
    }
    else
    {
        if([[updatedString lowercaseString] isEqualToString:[self.contactwrapper.ValueString lowercaseString]])
        {
            self.navigationItem.rightBarButtonItem = nil;
        }
        else
        {
            [self addRightNavigationItem];
        }
    }
}


- (void)manageSeparatorForTextfield:(UITextField *)textField
{
    //We are adding A Line above keyboard if keyboard type is number Pad
    if(!self.separatorViewForKeyboard)
        self.separatorViewForKeyboard = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 1.0 / [UIScreen mainScreen].scale)];
    
    self.separatorViewForKeyboard.frame =CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 1.0 / [UIScreen mainScreen].scale);
    self.separatorViewForKeyboard.backgroundColor = [UIColor lightGrayColor];
    textField.inputAccessoryView =  self.separatorViewForKeyboard;
    
    if(textField.keyboardType == UIKeyboardTypePhonePad)
    {
        self.separatorViewForKeyboard.hidden  =NO;
    }
    else
    {
        self.separatorViewForKeyboard.hidden  =YES;
    }
    
}


- (void)changeKeyboardTypeForTitle:(NSString *)title
{
    if([[title lowercaseString] containsString:@"mobile"])
    {
        [self.valueTextfield setKeyboardType:UIKeyboardTypePhonePad];
    }
    else  if([[title lowercaseString] containsString:@"landline"])
    {
        [self.valueTextfield setKeyboardType:UIKeyboardTypePhonePad];
    }
    else  if([[title lowercaseString] containsString:@"email"])
    {
        [self.valueTextfield setKeyboardType:UIKeyboardTypeEmailAddress];
    }
    else
    {
        [self.valueTextfield setKeyboardType:UIKeyboardTypeDefault];
    }
}


- (BOOL)isInputValid
{
    BOOL isInputValid = YES;
    
    if(self.contactwrapper.isRequired)
    {
        if(![self.editContactInterceptModel isInputValid:self.valueTextfield.text forContactModel:self.contactwrapper])
        {
            [self showErrorWithMessage:[self getErrorMessageForTitle:self.contactwrapper.titleString]];
            isInputValid = NO;
        }
        else
        {
            isInputValid = YES;
        }
        
    }
    else if(self.valueTextfield.text.length > 0)
    {
        if(![self.editContactInterceptModel isInputValid:self.valueTextfield.text forContactModel:self.contactwrapper])
        {
            [self showErrorWithMessage:[self getErrorMessageForTitle:self.contactwrapper.titleString]];
            isInputValid = NO;
        }
        else
        {
            isInputValid = YES;
        }
        
    }
    return isInputValid;
}


- (NSString *)getErrorMessageForTitle:(NSString *)title
{
    NSString *errorMessage = @"";
    
    if([[title lowercaseString] isEqualToString:@"mobile"])
    {
        errorMessage= [NSString stringWithFormat:@"Please enter a valid %@ number",[self.contactwrapper.titleString lowercaseString]];
    }
    else  if([[title lowercaseString] isEqualToString:@"landline"])
    {
        errorMessage= [NSString stringWithFormat:@"Please enter a valid landline number"];
    }
    else if([[title lowercaseString] containsString:@"email"])
    {
        errorMessage = [NSString stringWithFormat:@"Please enter valid %@ address",[self.contactwrapper.titleString lowercaseString]];
    }
    else
        errorMessage = [NSString stringWithFormat:@"Please enter a valid %@",[self.contactwrapper.titleString lowercaseString]];
    
    
    return errorMessage;
}


#pragma mark - Navigation item handeling

- (void)addLeftNavigationItem
{
    UIBarButtonItem *confirmButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed)];
    
    [self.navigationItem setLeftBarButtonItem:confirmButtonItem];
}


- (void)addRightNavigationItem
{
    UIBarButtonItem *confirmButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonPressed)];
    
    [self.navigationItem setRightBarButtonItem:confirmButtonItem];
}


#pragma mark - manage navigation actions
- (void)cancelButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)saveButtonPressed
{
    if([self isInputValid])
    {
        [self removeErrorMessage];
    }
    else
    {
        return;
    }
    
    BTInterceptContactWrapper *updatedInfo = [[BTInterceptContactWrapper alloc] initWithTitle:self.contactwrapper.titleString andValue:self.valueTextfield.text andIsRequiredField:self.contactwrapper.isRequired];
    [delegate userUpdatedDataOnEditContactInerceptViewController:updatedInfo andOldContact:self.contactwrapper];
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark - Textfield Delegates
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    self.separatorViewForKeyboard.hidden  =YES;
    return YES;
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    [self manageSeparatorForTextfield:textField];
    [self.valueTextfield updateWithEnabledState];
    [self removeErrorMessage];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    self.separatorViewForKeyboard.hidden  =YES;
    [self.valueTextfield updateWithDisabledState];
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self removeErrorMessage];
    NSString *updatedString  = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self manageSaveActionVisibilityForText:updatedString];
    return YES;
}


@end
