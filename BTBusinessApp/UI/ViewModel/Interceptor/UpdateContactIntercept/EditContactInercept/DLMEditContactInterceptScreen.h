//
//  DLMEditContactInterceptScreen.h
//  BTBusinessApp
//
//  Created by vectoscalar on 20/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
#import "DLMUpdateContactInerceptScreen.h"


@interface DLMEditContactInterceptScreen : DLMObject


- (NSString *)getNavigationTitleForString:(NSString *)string;
- (NSString *)getTextFieldTitleForString:(NSString *)string;
- (BOOL)isInputValid:(NSString *)inputText forContactModel:(BTInterceptContactWrapper*)contactModel;
- (NSArray *)getTitlesArray;
@end
