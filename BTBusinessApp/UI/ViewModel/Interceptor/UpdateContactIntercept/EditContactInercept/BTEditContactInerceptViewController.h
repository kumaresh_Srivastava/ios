//
//  BTEditContactInerceptViewController.h
//  BTBusinessApp
//
//  Created by vectoscalar on 20/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMUpdateContactInerceptScreen.h"

@class BTEditContactInerceptViewController;

@protocol BTEditContactInterceptDelegate <NSObject>

- (void)userUpdatedDataOnEditContactInerceptViewController:(BTInterceptContactWrapper *)updateContactWrapper  andOldContact:(BTInterceptContactWrapper*)oldeContact;

@end


@interface BTEditContactInerceptViewController : UIViewController

@property (strong,nonatomic) BTInterceptContactWrapper *contactwrapper;
@property (weak,nonatomic) id <BTEditContactInterceptDelegate> delegate;


@end
