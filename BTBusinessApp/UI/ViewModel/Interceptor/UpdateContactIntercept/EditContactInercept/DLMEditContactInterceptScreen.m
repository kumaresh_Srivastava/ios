//
//  DLMEditContactInterceptScreen.m
//  BTBusinessApp
//
//  Created by vectoscalar on 20/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMEditContactInterceptScreen.h"
#import "AppConstants.h"
#import "AppManager.h"

#define kNameRegex @"^([a-zA-Z\\-]+(. )?[ \\']?)+$"
#define kMobileRegex @"^((00|\\+)44|0)7(\\d{8}|\\d{9})$"
#define kPhoneRegex @"^0[0-9]{9,10}$"
#define kEmailRegex @"^((([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+(\\.([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+)*)|((\\x22)((((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(([\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f]|\\x21|[\\x23-\\x5b]|[\\x5d-\\x7e]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(\\\\([\\x01-\\x09\\x0b\\x0c\\x0d-\\x7f]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF]))))*(((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(\\x22)))@((([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.)+(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.?$"


@implementation DLMEditContactInterceptScreen


#pragma mark- public methods

//Get Navigation Title
- (NSString *)getNavigationTitleForString:(NSString *)string
{
    NSString *title = [string lowercaseString];

    title = [NSString stringWithFormat:@"Edit %@",title];
    
    return title;
}


//Get Textfield Title
- (NSString *)getTextFieldTitleForString:(NSString *)string
{
    NSString *title = [string lowercaseString];
    
    if([title containsString:@"email"])
    {
        title = [NSString stringWithFormat:@"Contact %@ address",title];
    }
    else if([title containsString:@"mobile"])
    {
        title = [NSString stringWithFormat:@"Contact %@ number",title];
    }
    else if ([title containsString:@"line"])
    {
        title = [NSString stringWithFormat:@"Contact %@ number",title];
    }
    else
    {
        title = [NSString stringWithFormat:@"Contact %@",title];
    }
    
    return title;
}

//validate model

- (BOOL)isInputValid:(NSString *)inputText forContactModel:(BTInterceptContactWrapper *)contactModel
{
    BOOL isValid = NO;
    
    if([contactModel.titleString isEqualToString:kUserTitle])
    {
        isValid = YES;
    }
    else  if([contactModel.titleString isEqualToString:kUserFirstName])
    {
        isValid = [self isNameValid:inputText];
    }
    else  if([contactModel.titleString isEqualToString:kUserLastName])
    {
       isValid = [self isNameValid:inputText];
    }
    else  if([contactModel.titleString isEqualToString:kUserMobile])
    {
       isValid = [self isMobileValid:inputText];
    }
    else  if([contactModel.titleString isEqualToString:kUserLandline])
    {
       isValid = [self isPhoneValid:inputText];
    }
    else  if([contactModel.titleString isEqualToString:kUserEmail])
    {
       isValid = [self isEmailValid:inputText];
    }
    else  if([contactModel.titleString isEqualToString:kUserAltEmail])
    {
     isValid = [self isEmailValid:inputText];
    }

    return isValid;
}


- (NSArray *)getTitlesArray{
    
    NSArray *titlesArray;
    
    titlesArray = [AppManager getBTUserTitles];
    
    return titlesArray;
}




#pragma mark- Private Helper Methods

- (BOOL)isEmailValid:(NSString *)email
{
    if (!email || [email rangeOfString:kEmailRegex options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location == NSNotFound ) {
        
        return NO;
        
    }

    
    return YES;
}


- (BOOL)isPhoneValid:(NSString *)phone
{
    if (!phone || [phone rangeOfString:kPhoneRegex options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location == NSNotFound ) {
        
        return NO;
        
    }

    return YES;
}


- (BOOL)isMobileValid:(NSString *)mobile
{
    if (!mobile || [mobile rangeOfString:kMobileRegex options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location == NSNotFound ) {
        
        return NO;
        
    }
    
    return YES;
}


- (BOOL)isNameValid:(NSString *)name
{
    if (!name || [name rangeOfString:kNameRegex options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location == NSNotFound ) {
        
        return NO;
        
    }

    return YES;
}



@end
