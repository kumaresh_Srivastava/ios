//
//  UpdateContactInerceptViewController.h
//  BTBusinessApp
//
//  Created by vectoscalar on 20/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMUpdateContactInerceptScreen.h"
#import "BTBaseViewController.h"

@class BTUpdateContactInerceptViewController;

@protocol BTUpdateContactInerceptViewControllerDelegate <NSObject>

- (void)didFinishSucessfullyOnBTUpdateContactInerceptViewController:(BTUpdateContactInerceptViewController *)controller;

- (void)makeUpdateContactInterceptorAPIRequestInBackgroundWithContactArray:(NSArray *)contactArray;

@end

@interface BTUpdateContactInerceptViewController : BTBaseViewController
@property(nonatomic, assign) id<BTUpdateContactInerceptViewControllerDelegate>delegate;
@property (assign,nonatomic) IntercepterType intercepterType;

@end
