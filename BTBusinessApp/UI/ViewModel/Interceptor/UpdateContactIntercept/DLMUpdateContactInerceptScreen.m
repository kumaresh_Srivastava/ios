//
//  DLMUpdateContactInerceptScreen.m
//  BTBusinessApp
//
//  Created by vectoscalar on 20/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMUpdateContactInerceptScreen.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "CDAuthenticationToken.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "NSObject+APIResponseCheck.h"
#import "NLQueryUpdateContactWebService.h"
#import "NLWebServiceError.h"

//Used for making params for API request only
#define kSettingsUserTitle @"Title"
#define kSettingsUserFirstName @"FirstName"
#define kSettingsUserLastName @"LastName"
#define kSettingsUserMobile @"MobileNumber"
#define kSettingsUserLandline @"LandLineNumber"
#define kSettingsUserPriEmail @"PrimaryEmailAddress"
#define kSettingsUserUserAltEmail @"AlternateEmailAddress"

@interface DLMUpdateContactInerceptScreen()<NLQueryUpdateContactWebServiceDelegate>

@property (nonatomic) NLQueryUpdateContactWebService *queryUpdateContactWebService;


@end

@implementation DLMUpdateContactInerceptScreen

#pragma mark - Public helper methods

- (NSArray *)getContactWrappers{
    
    NSMutableArray *contailDetailRowWrappers = [NSMutableArray array];
    CDUser *user = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    
    
    //Title
    NSString *title = @"";
    if(user.titleInName &&![user.titleInName isEqualToString:@"-"])
        title = user.titleInName;
    
    
    //Check for Need updating
    if([[title lowercaseString] containsString:@"needs"] || [[title lowercaseString] containsString:@"updating"] || [[title lowercaseString] containsString:@"needs updating"])
    {
        title = @"";
    }
    
    BTInterceptContactWrapper *titleWrapper = [[BTInterceptContactWrapper alloc] initWithTitle:kUserTitle andValue:title andIsRequiredField:YES];
    [contailDetailRowWrappers addObject:titleWrapper];
    
    //First Name
    NSString *fName = @"";
    if(user.firstName &&![user.firstName isEqualToString:@"-"])
        fName = user.firstName;
    
    //Check for Need updating
    if([[fName lowercaseString] containsString:@"needs"] || [[fName lowercaseString] containsString:@"updating"] || [[fName lowercaseString] containsString:@"needs updating"])
    {
        fName = @"";
    }
    
    BTInterceptContactWrapper *firstNameWrapper = [[BTInterceptContactWrapper alloc] initWithTitle:kUserFirstName andValue:fName andIsRequiredField:YES];
    [contailDetailRowWrappers addObject:firstNameWrapper];
    
    //Last Name
    NSString *lName = @"";
    if(user.lastName &&![user.lastName isEqualToString:@"-"])
        lName = user.lastName;
    
    //Check for Need updating
    if([[lName lowercaseString] containsString:@"needs"] || [[lName lowercaseString] containsString:@"updating"] || [[lName lowercaseString] containsString:@"needs updating"])
    {
        lName = @"";
    }
    
    BTInterceptContactWrapper *lastNameWrapper = [[BTInterceptContactWrapper alloc] initWithTitle:kUserLastName andValue:lName andIsRequiredField:YES];
    [contailDetailRowWrappers addObject:lastNameWrapper];
    
    //User Mobile
    NSString *mobile = nil;
    if(user.mobileNumber && ![user.mobileNumber isEqualToString:@"-"])
        mobile = user.mobileNumber;
    
    //Check for Need updating
    if([[mobile lowercaseString] containsString:@"needs"] || [[mobile lowercaseString] containsString:@"updating"] || [[mobile lowercaseString] containsString:@"needs updating"])
    {
        mobile = nil;
    }
    
    
    BTInterceptContactWrapper *mobileRowWrapper = [[BTInterceptContactWrapper alloc] initWithTitle:kUserMobile andValue:mobile andIsRequiredField:NO];
    
    //Mandatory if coming from server
    if(mobile){
        
        mobileRowWrapper.isRequired = YES;
    }
    
    [contailDetailRowWrappers addObject:mobileRowWrapper];
    
    
    //User Landline
    NSString *landline = nil;
    if(user.landlineNumber &&![user.landlineNumber isEqualToString:@"-"])
        landline = user.landlineNumber;
    
    //Check for Need updating
    if([[landline lowercaseString] containsString:@"needs"] || [[landline lowercaseString] containsString:@"updating"] || [[landline lowercaseString] containsString:@"needs updating"])
    {
        landline = nil;
    }
    
    BTInterceptContactWrapper *landlineRowWrapper = [[BTInterceptContactWrapper alloc] initWithTitle:kUserLandline andValue:landline andIsRequiredField:NO];
    
    //Mandatory if coming from server
    if(landline){
        
        landlineRowWrapper.isRequired = YES;
    }
    
    
    [contailDetailRowWrappers addObject:landlineRowWrapper];
    
    //User Email
    NSString *email = @"";
    if(user.primaryEmailAddress &&![user.primaryEmailAddress isEqualToString:@"-"])
        email = user.primaryEmailAddress;
    
    //Check for Need updating
    if([[email lowercaseString] containsString:@"needs"] || [[email lowercaseString] containsString:@"updating"] || [[email lowercaseString] containsString:@"needs updating"])
    {
        email = @"";
    }
    
    BTInterceptContactWrapper *primaryEmailRowWrapper = [[BTInterceptContactWrapper alloc] initWithTitle:kUserEmail andValue:email andIsRequiredField:NO];
    [contailDetailRowWrappers addObject:primaryEmailRowWrapper];
    
    //User Alt Email
    NSString *altEmail = nil;
    if(user.alternativeEmailAddress &&![user.alternativeEmailAddress isEqualToString:@"-"])
        altEmail = user.alternativeEmailAddress;
    
    //Check for Need updating
    if([[altEmail lowercaseString] containsString:@"needs"] || [[altEmail lowercaseString] containsString:@"updating"] || [[altEmail lowercaseString] containsString:@"needs updating"])
    {
        altEmail = nil;
    }
    
    BTInterceptContactWrapper *altEmailRowWrapper = [[BTInterceptContactWrapper alloc] initWithTitle:kUserAltEmail andValue:altEmail andIsRequiredField:NO];
    [contailDetailRowWrappers addObject:altEmailRowWrapper];
    
    //Mandatory if coming from server
    if(altEmail){
        
        altEmailRowWrapper.isRequired = YES;
    }
    
    return contailDetailRowWrappers;
    
}

- (NSDictionary *)getUserDataDictionaryForContactWrapperArray:(NSArray*)contactWrapperArray{
    
    NSMutableDictionary *paramsDict = [[NSMutableDictionary alloc] init];
    
    for(BTInterceptContactWrapper *contactWrapper in contactWrapperArray)
    {
        if([contactWrapper.titleString isEqualToString:kUserTitle])
        {
            [paramsDict setObject:contactWrapper.ValueString forKey:kSettingsUserTitle];
        }
        else  if([contactWrapper.titleString isEqualToString:kUserFirstName])
        {
            [paramsDict setObject:contactWrapper.ValueString forKey:kSettingsUserFirstName];
        }
        else  if([contactWrapper.titleString isEqualToString:kUserLastName])
        {
            [paramsDict setObject:contactWrapper.ValueString forKey:kSettingsUserLastName];
        }
        else  if([contactWrapper.titleString isEqualToString:kUserMobile])
        {
            [paramsDict setObject:contactWrapper.ValueString forKey:kSettingsUserMobile];
        }
        else  if([contactWrapper.titleString isEqualToString:kUserLandline])
        {
            [paramsDict setObject:contactWrapper.ValueString forKey:kSettingsUserLandline];
        }
        else  if([contactWrapper.titleString isEqualToString:kUserEmail])
        {
            [paramsDict setObject:contactWrapper.ValueString forKey:kSettingsUserPriEmail];
        }
        else  if([contactWrapper.titleString isEqualToString:kUserAltEmail])
        {
            [paramsDict setObject:contactWrapper.ValueString forKey:kSettingsUserUserAltEmail];
        }
        
    }
    
    return [NSDictionary dictionaryWithDictionary:paramsDict];
}


- (BOOL)isLocalInfoChangedFor:(NSArray *)contactWrapperArray
{
    BOOL needToUpdate =  NO;
    
    for(BTInterceptContactWrapper *contactWrapper in contactWrapperArray)
    {
        NSString *updatedValue = [contactWrapper.ValueString lowercaseString];
        NSString *coreDataValue = [[self getCoreDataValueForContactWrapper:contactWrapper] lowercaseString];
        
        
        if([updatedValue isEqualToString:@""] || !updatedValue)
            updatedValue = nil;
        
        if([coreDataValue isEqualToString:@""] || !coreDataValue)
            coreDataValue = nil;
        
        
        
        if(updatedValue && coreDataValue)
        {
            if(![updatedValue isEqualToString:coreDataValue])
            {
                needToUpdate = YES;
                break;
            }
            
        }
        else
        {
          if(!updatedValue && !coreDataValue)
          {
             //Do nothing
          }
          else
          {
              needToUpdate = YES;
              break;
          }
        }
        
    }
    return needToUpdate;
    
}


- (void)updateLocalDatabaseWithContacts:(NSArray *)contactArray
{
    NSManagedObjectContext *context = [AppDelegate sharedInstance].managedObjectContext;
    CDApp *app = [CDApp appInManagedObjectContext:context];
    
    CDUser *user = app.loggedInUser;
    
    for(BTInterceptContactWrapper *contactWrapper in contactArray)
    {
        if([contactWrapper.titleString isEqualToString:kUserTitle])
        {
            user.titleInName  = contactWrapper.ValueString;
        }
        else  if([contactWrapper.titleString isEqualToString:kUserFirstName])
        {
            user.firstName  = contactWrapper.ValueString;;
        }
        else  if([contactWrapper.titleString isEqualToString:kUserLastName])
        {
            user.lastName  = contactWrapper.ValueString;
        }
        else  if([contactWrapper.titleString isEqualToString:kUserMobile])
        {
            user.mobileNumber  = contactWrapper.ValueString;
        }
        else  if([contactWrapper.titleString isEqualToString:kUserLandline])
        {
            user.landlineNumber  = contactWrapper.ValueString;
        }
        else  if([contactWrapper.titleString isEqualToString:kUserEmail])
        {
            user.primaryEmailAddress  = contactWrapper.ValueString;
        }
        else  if([contactWrapper.titleString isEqualToString:kUserAltEmail])
        {
            user.alternativeEmailAddress  = contactWrapper.ValueString;
        }
        
    }
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
}


- (void)submitAndCreateContactDetailsModelWithContactDetailsDictionary:(NSDictionary *)updatedContactDetailsDict andPassword:(NSString *)password
{
    NSMutableDictionary *updatedDictionary = [NSMutableDictionary dictionaryWithDictionary:updatedContactDetailsDict];
    
    [updatedDictionary setValue:[AppManager getRSAEncryptedString:password] forKey:@"Password"];
    
    // Get the currently logged in User
    CDUser *user = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    
    //(lp) Send the groupkey with text "Key"
    [updatedDictionary setValue:user.currentSelectedCug.groupKey forKey:@"Key"];
    
    self.queryUpdateContactWebService = [[NLQueryUpdateContactWebService alloc] initWithUpdatedContactDetailsDictionary:updatedDictionary];
    self.queryUpdateContactWebService.queryUpdateContactWebServiceDelegate = self;
    [self.queryUpdateContactWebService resume];
    
}


//Check Requred fields are fileed or not
- (NSString *)getErrorMessageForRequiredField:(NSArray *)contactWrapperArray
{
    NSString *errorMessage = @"";
    
    for(BTInterceptContactWrapper *contactWrapper in contactWrapperArray)
    {
        if(contactWrapper.isRequired)
        {
            //Check for non-empty and non-null
            NSString *filledvalue = [contactWrapper.ValueString lowercaseString];
            
            if([filledvalue isEqualToString:@""] || !filledvalue || filledvalue.length == 0)
            {
                errorMessage = [NSString stringWithFormat:@"%@ is required",contactWrapper.titleString];
                break;
            }
        }
    }
    
    return errorMessage;
}



//Check duplicacy of emails
- (NSString *)getErrorMessageForSameEmailInContactArray:(NSArray *)contactWrapperArray
{
    NSString *errorMessage = @"";
    
    NSString *email = @"";
    NSString *altEmail = @"";
    
    for(BTInterceptContactWrapper *contactWrapper in contactWrapperArray)
    {
        if([[contactWrapper.titleString lowercaseString] isEqualToString:[kUserEmail lowercaseString]])
        {
            email = contactWrapper.ValueString;
        }
        
        if([[contactWrapper.titleString lowercaseString] isEqualToString:[kUserAltEmail lowercaseString]])
        {
            altEmail = contactWrapper.ValueString;
        }
    }
    
    if([email isEqualToString:altEmail])
    {
        errorMessage  = @"Your primary and alternative email need to be different.";
    }
    
    return errorMessage;
}


#pragma mark - Private helper methods

- (NSString *)getCoreDataValueForContactWrapper:(BTInterceptContactWrapper *)contactWrapper
{
    CDUser *user = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    
    if([contactWrapper.titleString isEqualToString:kUserTitle])
    {
        return user.titleInName;
    }
    else  if([contactWrapper.titleString isEqualToString:kUserFirstName])
    {
        return user.firstName;
    }
    else  if([contactWrapper.titleString isEqualToString:kUserLastName])
    {
        return user.lastName;
    }
    else  if([contactWrapper.titleString isEqualToString:kUserMobile])
    {
        return user.mobileNumber;
    }
    else  if([contactWrapper.titleString isEqualToString:kUserLandline])
    {
        return user.landlineNumber;
    }
    else  if([contactWrapper.titleString isEqualToString:kUserEmail])
    {
        return user.primaryEmailAddress;
    }
    else  if([contactWrapper.titleString isEqualToString:kUserAltEmail])
    {
        return user.alternativeEmailAddress;
    }
    else
    {
        return  @"";
    }
    
}


#pragma mark - NLUpdatedContactWebService methods

- (void)queryUpdateContactDetailsSuccesfullyFinishedWithQueryUpdateContactWebService:(NLQueryUpdateContactWebService *)webService
{
    if(webService == _queryUpdateContactWebService) {
        [self.updateContactInerceptScreenDelegate updateSuccessfullyFinishedByUpdateContactInerceptScreen:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UserContactDetailsUpdatedSuccessfullyNotification" object:nil];
        _queryUpdateContactWebService.queryUpdateContactWebServiceDelegate = nil;
        _queryUpdateContactWebService = nil;
    } else {
        DDLogError(@"This delegate method gets called because of success of an object of NLQueryUpdateContactWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLQueryUpdateContactWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)queryUpdateContactWebService:(NLQueryUpdateContactWebService *)webService failedToUpdateContactDetailsWithWebServiceError:(NLWebServiceError *)error {
    if(webService == _queryUpdateContactWebService) {
        
        [self.updateContactInerceptScreenDelegate updateContactInerceptScreen:self failedToSubmitUpdateContactInerceptScreenWithWebServiceError:error];
        _queryUpdateContactWebService.queryUpdateContactWebServiceDelegate = nil;
        _queryUpdateContactWebService = nil;
        
    } else {
        DDLogError(@"This delegate method gets called because of failure of an object of NLQueryUpdateContactWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLQueryUpdateContactWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}



@end


@implementation BTInterceptContactWrapper

- (id)initWithTitle:(NSString *)title andValue:(NSString *)valueString andIsRequiredField:(BOOL)isRequired{
    
    self = [super init];
    
    if(self){
        
        _titleString = title;
        _ValueString = valueString;
        _isRequired = isRequired;
        
        if(!valueString || ![valueString validAndNotEmptyStringObject]){
            
            _ValueString = @"";
        }
    }
    
    return self;
}


@end
