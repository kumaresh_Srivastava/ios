//
//  BTSecurityCheckInterceptorViewController.h
//  BTBusinessApp
//
//  Created by vectoscalar on 07/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseViewController.h"
#import "BTSecurityNumberViewController.h"



@interface BTSecurityCheckInterceptorViewController : BTBaseViewController

@property (strong,nonatomic) NSString *questionFromSignInScreen;
@property (strong,nonatomic) NSString *cugKeyFromSignInScreen;

@property (nonatomic) id<BTSecurityNumberViewControllerDelegate> delegate;

@end
