//
//  DLMSecurityCheckScreen.m
//  BTBusinessApp
//
//  Created by vectoscalar on 07/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMSecurityCheckScreen.h"
#import "NLSecurityCheckIntercept.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "NLConstants.h"
#import "CDCug+CoreDataClass.h"
#import "NSObject+APIResponseCheck.h"

#define kUsernameKey @"UserId"
#define kPasswordKey @"Password"
#define kAnswerKey @"SecretAnswer"


@interface DLMSecurityCheckScreen () <NLSecurityCheckInterceptDelegate>

@property (nonatomic) NLSecurityCheckIntercept *securityCheckWebService;

@end



@implementation DLMSecurityCheckScreen

#pragma mark- public methods

- (void)fetchSecurityCheckScreenDataWithAnswer:(NSString *)answerText
{
    NSString *userName;
    NSString *password;
   if([[NSUserDefaults standardUserDefaults] objectForKey:kPasswordLoginIntercepter])
   {
        userName = [AppManager sharedAppManager].usernameForSignInIntercepter;
        password = [[NSUserDefaults standardUserDefaults] valueForKey:kPasswordLoginIntercepter];
   }
   else
   {
       userName = [AppDelegate sharedInstance].viewModel.app.loggedInUser.username;
       password = [SAMKeychain passwordForService:kKeyChainServiceBTUserPassword account:[AppDelegate sharedInstance].viewModel.app.loggedInUser.username];
   }
    
 
    NSMutableDictionary *paramsDict = [[NSMutableDictionary alloc] init];
    
    [paramsDict setObject:userName forKey:kUsernameKey];
    [paramsDict setObject:[AppManager getRSAEncryptedString:password] forKey:kPasswordKey];
    [paramsDict setObject:[AppManager getRSAEncryptedString:answerText] forKey:kAnswerKey];

    self.securityCheckWebService = [[NLSecurityCheckIntercept alloc] initWithSecurityCheckParamsDictionary:paramsDict];
    self.securityCheckWebService.getSecurityCheckIntercepterDelegate = self;
    [self.securityCheckWebService resume];
}

- (void)cancelSecurityCheckScreenRequest
{
    self.securityCheckWebService.getSecurityCheckIntercepterDelegate = nil;
    [self.securityCheckWebService cancel];
    self.securityCheckWebService = nil;
}

#pragma mark - Web Service Delegate Methods

- (void)getSecurityCheckWebService:(NLSecurityCheckIntercept *)webService failedTogetSecurityCheckDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    if(self.securityCheckWebService == webService){
        
        [self.securityCheckScreenDelegate failedToFetchDataOnSecurityCheckScreen:self withWebServiceError:webServiceError];
        
        self.securityCheckWebService.getSecurityCheckIntercepterDelegate = nil;
        self.securityCheckWebService = nil;
        
    }
    else{
        
        DDLogError(@"This delegate method gets called because of failure of an object of NLSecurityCheckIntercept but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLSecurityCheckIntercept but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}


- (void)getSecurityCheckWebService:(NLSecurityCheckIntercept *)webService successfullyFetchedSecurityCheckData:(BTSecurityCheck *)securityCheck
{
    
    if(self.securityCheckWebService == webService){
        
        self.securityCheckModel = securityCheck;
        [self.securityCheckScreenDelegate successfullyFetchedSecurityDataOnSecurityCheckScreen:self];
        
        self.securityCheckWebService.getSecurityCheckIntercepterDelegate = nil;
        self.securityCheckWebService = nil;
        
    }
    else{
        
        DDLogError(@"This delegate method gets called because of success of an object of NLSecurityCheckIntercept but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLSecurityCheckIntercept but this object is not the one stored in member variable of this class %@.", [self class]);
    }

}

@end
