//
//  DLMSecurityCheckScreen.h
//  BTBusinessApp
//
//  Created by vectoscalar on 07/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
#import "BTSecurityCheck.h"

@class DLMSecurityCheckScreen;
@class NLWebServiceError;

@protocol DLMSecurityCheckScreenDelegate <NSObject>

- (void)successfullyFetchedSecurityDataOnSecurityCheckScreen:(DLMSecurityCheckScreen *)securityCheckScreen;
- (void)failedToFetchDataOnSecurityCheckScreen:(DLMSecurityCheckScreen *)securiryCheckScreen withWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface DLMSecurityCheckScreen : DLMObject

@property (weak,nonatomic) id <DLMSecurityCheckScreenDelegate> securityCheckScreenDelegate;
@property (strong,nonatomic) BTSecurityCheck *securityCheckModel;

- (void)fetchSecurityCheckScreenDataWithAnswer:(NSString *)answerText;
- (void)cancelSecurityCheckScreenRequest;

@end

