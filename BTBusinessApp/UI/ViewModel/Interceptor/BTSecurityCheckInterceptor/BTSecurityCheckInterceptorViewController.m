//
//  BTSecurityCheckInterceptorViewController.m
//  BTBusinessApp
//
//  Created by vectoscalar on 07/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTSecurityCheckInterceptorViewController.h"
#import "DLMSecurityCheckScreen.h"
#import "AppConstants.h"
#import "NLConstants.h"
#import "BTSecurityQuestionsAndAnswerViewController.h"
#import "BTSecurityNumberViewController.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"
#define kScreenName @"Security check"
#define kTextCorrectionHeight 40
#import "NLWebServiceError.h"
#import "AppManager.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "UIButton+BTButtonProperties.h"
#import "UITextField+BTTextFieldProperties.h"

@interface BTSecurityCheckInterceptorViewController ()<UITextFieldDelegate,UIScrollViewDelegate,DLMSecurityCheckScreenDelegate,BTSecurityNumberViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *placeholderView;
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UITextField *answerTextField;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *errorLabelHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *messageLabelTopConstraint;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@property (strong,nonatomic) DLMSecurityCheckScreen *securityCheckModel;

//#define kButtonHighlightedColor [BrandColours colourBackgroundBTPurplePrimaryColor]
#define kButtonDisabledColor [BrandColours colourBtNeutral60]

@end

@implementation BTSecurityCheckInterceptorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.securityCheckModel = [[DLMSecurityCheckScreen alloc] init];
    
    self.securityCheckModel.securityCheckScreenDelegate = self;
    
    self.navigationItem.title = kScreenName;
    
    [self.answerTextField getBTTextFieldStyle];
    self.answerTextField.delegate = self;
    self.scrollView.delegate = self;
    
    //self.answerTextField.layer.borderColor = [BrandColours colourBtNeutral60].CGColor;
    
    [self customizeButton];
    [self addTapGestureToPlaceholderView];
    
    [UIView commitAnimations];
    [self hideMessageWhenLoadedFirstTime];
    
    self.navigationItem.hidesBackButton = YES;
    
    //Get question from user API
     NSString *question=@"";
    
    if(self.questionFromSignInScreen && self.questionFromSignInScreen.length >0)
    {
        question = self.questionFromSignInScreen;
    }
    else
    {
        question  = [AppDelegate sharedInstance].viewModel.app.loggedInUser.securityQuestion;
    }
    if(question && question.length >0)
        self.questionLabel.text = question;
    else
        self.questionLabel.text = @"-";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}



- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [self hideProgressView];
    self.networkRequestInProgress = NO;
    [self.securityCheckModel cancelSecurityCheckScreenRequest];
}

#pragma mark - Private helper method
- (void)hideMessageWhenLoadedFirstTime
{
    self.errorLabel.text = @"";
    self.errorLabel.hidden = YES;
    self.messageLabelTopConstraint.constant = -24;
    [self.view layoutIfNeeded];
}


//For testing only
- (void)addTapToQuestion
{
    UIButton *btn = [[UIButton alloc] initWithFrame:self.questionLabel.frame];
    
    [btn addTarget:self action:@selector(showQuestionScreen) forControlEvents:UIControlEventTouchUpInside];
    //Uncomment to add a dummy action to question label (RM)
   // [self.placeholderView addSubview:btn];
    
}

- (void)showQuestionScreen
{
    //Redirect to security question screen
    BTSecurityQuestionsAndAnswerViewController *changeSecurityQuestion = [self.storyboard instantiateViewControllerWithIdentifier:@"SecurityQuestionsAndAnswerScene"];
    changeSecurityQuestion.isScreenRelatedToInterceptorModule = YES;
    [self.navigationController pushViewController:changeSecurityQuestion animated:YES];
    
}

- (void)customizeButton
{
    self.submitButton.enabled = NO;
    [self.submitButton getBTButtonStyle];
    //self.submitButton.backgroundColor = kButtonDisabledColor;
    //self.submitButton.layer.cornerRadius = 4.0;
    //[self.submitButton addTarget:self action:@selector(buttonHighlight) forControlEvents:UIControlEventTouchDown];
    //[self.submitButton addTarget:self action:@selector(buttonNormal) forControlEvents:UIControlEventTouchUpInside];
}


- (void)addTapGestureToPlaceholderView
{
    //So that on outside tap keyboard hide
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(placeholderViewTapped)];
    [self.placeholderView addGestureRecognizer:gesture];
}


- (void)placeholderViewTapped
{
    [self.view endEditing:YES];
}


//- (void)buttonNormal
//{
//    if(self.answerTextField.text.length > 0)
//        self.submitButton.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
//}
//
//
//- (void)buttonHighlight
//{
//    if(self.answerTextField.text.length > 0)
//        self.submitButton.backgroundColor = kButtonHighlightedColor;
//}


- (void)showErrorWithMessageWithMessage:(NSString *)message
{
    if(!self.errorLabel.hidden)
    {
        self.errorLabel.text = message;
        return;
    }
    
    self.errorLabel.text = message;
    self.errorLabel.hidden = NO;
    
    // [UIView animateWithDuration:0.3 animations:^{
    
    [self.view layoutIfNeeded];
    
    // }];
    //[UIView commitAnimations];
    
    self.messageLabelTopConstraint.constant =   1;
}


- (void)removeErrorMessage
{
    self.errorLabel.text = @"";
    self.errorLabel.hidden = YES;
    self.messageLabelTopConstraint.constant = -24;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.view layoutIfNeeded];
    }];
    [UIView commitAnimations];
}

- (void)manageAlertViewWithKeyboardHeight:(CGFloat)keyboardHeight
{
    CGFloat keyboardPosition = self.view.frame.size.height - keyboardHeight;
    
    CGFloat visibleArea = self.placeholderView.frame.size.height - keyboardHeight;
    
    // Step 2: Adjust the bottom content inset of your scroll view by the keyboard height.
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0,visibleArea/1.3, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    
    // Step 3: Scroll the target text field into view.
    
    CGFloat textFieldPosition = self.answerTextField.frame.origin.y + self.answerTextField.frame.size.height;
    
    if(textFieldPosition > keyboardPosition)
    {
        [self performSelector:@selector(scrollToPosition:) withObject:[NSNumber numberWithFloat:textFieldPosition - keyboardPosition] afterDelay:0.0];
    }
    
}


- (void)scrollToPosition:(id)position
{
    CGFloat pos = [position floatValue];
    CGPoint scrollPoint = CGPointMake(0.0, pos);
    [self.scrollView setContentOffset:scrollPoint animated:YES];
}


- (void)redirectToHomeScreen
{
    
    
    
}


#pragma mark - Fire API Request

- (void)makeAPIRequestWithPassword:(NSString *)answerText
{
    [self showProgressView];

    [self.securityCheckModel fetchSecurityCheckScreenDataWithAnswer:answerText];
}



#pragma mark - Button Action method
- (IBAction)userPressedSubmitAnswerButton:(id)sender {
    
    [self.view endEditing:YES];
    
    if(![AppManager isInternetConnectionAvailable])
    {
        [self showErrorWithMessageWithMessage:kNoConnectionMessage];
        return;
    }
    [self removeErrorMessage];
    [self makeAPIRequestWithPassword:self.answerTextField.text];
}



#pragma mark - Text field Delegates

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *str  = [textField.text stringByReplacingCharactersInRange:range withString:string];
    BOOL isValid = NO;

    // update minimum length to 4 characters
    if(str.length > 3 || ![str isEqualToString:@""])
    {
        
        NSString *question=@"";
        
        if(self.questionFromSignInScreen && self.questionFromSignInScreen.length>0)
        {
            question = self.questionFromSignInScreen;
        }
        else
        {
            question  = [AppDelegate sharedInstance].viewModel.app.loggedInUser.securityQuestion;
        }

        // add condition answer != question (or any substring of question longer than 4 chars)
        if(question && question.length >0 && ![str isEqualToString:question])
        {
            NSString *qString = [self simplifiedStringFromString:question];
            NSString *aString = [self simplifiedStringFromString:str];
            
            if ([aString length] > 3 && ![aString isEqualToString:qString]) {
                isValid = YES;
                NSArray *strParts = [str componentsSeparatedByString:@" "];
                for (NSString *part in strParts) {
                    NSString *simplePart = [self simplifiedStringFromString:part];
                    if ([qString containsString:simplePart]) {
                        isValid = NO;
                    }
                }
            }
        }
    }
    
    if (isValid) {
        self.submitButton.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
        self.submitButton.enabled = YES;
    } else {
        self.submitButton.backgroundColor = kButtonDisabledColor;
        self.submitButton.enabled = NO;
    }
    
    return YES;
}

- (NSString*)simplifiedStringFromString:(NSString*)string
{
    // convenince method for complaring similar strings
    NSString *newString = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    newString = [newString stringByReplacingOccurrencesOfString:@" " withString:@""];
    newString = [newString stringByReplacingOccurrencesOfString:@"'" withString:@""];
    newString = [newString stringByReplacingOccurrencesOfString:@"?" withString:@""];
    return [newString lowercaseString];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [_answerTextField updateWithDisabledState];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [_answerTextField updateWithEnabledState];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    
    return YES;
}


#pragma mark - Keyboard Notifications
- (void)keyboardWillShow:(NSNotification *)n
{
    
    NSDictionary* userInfo = [n userInfo];
    
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    [self manageAlertViewWithKeyboardHeight:keyboardSize.height];
    
}



- (void)keyboardWillHide:(NSNotification *)n
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}


#pragma mark - DLMSecurity Check Delegate

- (void)successfullyFetchedSecurityDataOnSecurityCheckScreen:(DLMSecurityCheckScreen *)securityCheckScreen
{
    BTSecurityCheck *securityCheckModel = securityCheckScreen.securityCheckModel;
    [self hideProgressView];
    [self setNetworkRequestInProgress:false];
    
    if(securityCheckModel.status == 1)//Failed to validate
    {
        
        [self showErrorWithMessageWithMessage:kInvalidSecurityAnswerMessage];
    }
    else
    {
        //Code Validated
        [self removeErrorMessage];
      
//        BTSecurityQuestionsAndAnswerViewController *changeSecurityQuestion = [self.storyboard instantiateViewControllerWithIdentifier:@"SecurityQuestionsAndAnswerScene"];
//        changeSecurityQuestion.isScreenRelatedToInterceptorModule = YES;
//        changeSecurityQuestion.cugKeyFromSignInScreen = self.cugKeyFromSignInScreen;
//        [self.navigationController pushViewController:changeSecurityQuestion animated:YES];
        
        BTSecurityNumberViewController *changeSecurityNumber = [self.storyboard instantiateViewControllerWithIdentifier:@"BTSecurityNumberViewController"];
        changeSecurityNumber.delegate = self;
        changeSecurityNumber.isScreenRelatedToInterceptorModule = YES;
        changeSecurityNumber.isScreenrelatedTo1FAMigration = NO;
        changeSecurityNumber.cugKeyFromSignInScreen = self.cugKeyFromSignInScreen;
        [self.navigationController pushViewController:changeSecurityNumber animated:YES];
        
    }

}


- (void)failedToFetchDataOnSecurityCheckScreen:(DLMSecurityCheckScreen *)securiryCheckScreen withWebServiceError:(NLWebServiceError *)webServiceError
{
    [self hideProgressView];
    [self setNetworkRequestInProgress:false];
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
    
    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {
        
        switch (webServiceError.error.code)
        {
                
            case BTNetworkErrorCodeAPINoDataFound:
            {
                errorHandled = YES;
                [self showErrorWithMessageWithMessage:kInvalidSecurityAnswerMessage];
                break;
            }
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }
    
    if(errorHandled == NO)
    {
         [self showErrorWithMessageWithMessage:kDefaultErrorMessage];
    }
}

#pragma mark - BTSecurityNumberViewControllerDelegate

- (void)successfullyUpdatedSecurityNumberOnSecurityNumberViewController:(BTSecurityNumberViewController *)securityNumberViewController
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(successfullyUpdatedSecurityNumberOnSecurityNumberViewController:)]) {
        [self.delegate successfullyUpdatedSecurityNumberOnSecurityNumberViewController:securityNumberViewController];
    }
}

@end
