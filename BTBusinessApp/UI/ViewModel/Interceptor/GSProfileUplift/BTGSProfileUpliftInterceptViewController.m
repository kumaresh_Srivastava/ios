//
//  BTGSProfileUpliftInterceptViewController.m
//  BTBusinessApp
//
//  Created by kumaresh shrivastava on 10/05/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import "BTGSProfileUpliftInterceptViewController.h"
#import "AppManager.h"
#import "CustomSpinnerView.h"

@interface BTGSProfileUpliftInterceptViewController ()
{
    CustomSpinnerView *_loadingView;
}

@property (weak, nonatomic) IBOutlet UIWebView *gsProfileUpliftWebView;

@end

@implementation BTGSProfileUpliftInterceptViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self createCloseButton];

    //self.navigationController.disablesAutomaticKeyboardDismissal = NO;
    
    NSString* gsProfileUrlStr = [NSString stringWithFormat:@"https://%@%@", [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel], self.gsProfileUpliftUrl];
    
    self.title = @"Register";
    NSURL *targetURL = [NSURL URLWithString:gsProfileUrlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
   
   //_gsProfileUpliftWebView.hidden = YES;
     _gsProfileUpliftWebView.scalesPageToFit = YES;
    _gsProfileUpliftWebView.delegate = self;
     [_gsProfileUpliftWebView loadRequest:request];
    [self createLoadingView];
    
    _gsProfileUpliftWebView.keyboardDisplayRequiresUserAction = NO;
   
    //[[UIApplication sharedApplication] openURL:targetURL];
    
    /*WKWebViewConfiguration *theConfiguration = [[WKWebViewConfiguration alloc] init];
    WKWebView *webView = [[WKWebView alloc] initWithFrame:self.view.frame configuration:theConfiguration];
    webView.navigationDelegate = self;
   
    [webView loadRequest:request];
    [self.view addSubview:webView];*/
}

- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation{
    
    
    NSString *urlString = webView.URL.absoluteString;
    
    NSLog(@"%@", urlString);
}

- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation{
    
    NSString *urlString = webView.URL.absoluteString;
    
    NSLog(@"%@", urlString);
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    
    NSString *urlString = webView.URL.absoluteString;
    
    NSLog(@"%@", urlString);
}



- (void)createCloseButton
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(50, 0, 70, 30);
    [button setTitle:@"Close" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(closeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}

- (void)createSignButton
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(50, 0, 70, 30);
    [button setTitle:@"Sign In" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(closeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}

#pragma mark - Action Methods
- (void)closeButtonAction:(id)sender {
    [self deleteCookies];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    //Check if the web view loadRequest is sending an error message
   // DDLogError(@"Error : %@",error);
    [self toggleLoadingItems:YES];
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
  
    NSString *urlString = [[request URL] absoluteString];
    
   // NSLog(@"%@", urlString);
    
    if(urlString != nil  && ([urlString rangeOfString:@"journey=GSProfileRegistration" options:NSCaseInsensitiveSearch].location != NSNotFound)) {

        if(([urlString rangeOfString:@"BillingAccountsValidation" options:NSCaseInsensitiveSearch].location != NSNotFound)){
            
            self.title = @"Billing accounts";
          
        } else if(([urlString rangeOfString:@"RegistrationInterceptForGroups" options:NSCaseInsensitiveSearch].location != NSNotFound)){
            
            self.title = @"Billing accounts";
            
        } else if(([urlString rangeOfString:@"MyDetails" options:NSCaseInsensitiveSearch].location != NSNotFound)){
            
            self.title = @"My details";
            
        } else if(([urlString rangeOfString:@"showSecurePasswordSection" options:NSCaseInsensitiveSearch].location != NSNotFound)){
            
            self.title = @"My details";
            
        }
        else if(([urlString rangeOfString:@"TermsAndConditions" options:NSCaseInsensitiveSearch].location != NSNotFound)){
            
            self.title = @"Summary";
            
        } else if(([urlString rangeOfString:@"completion" options:NSCaseInsensitiveSearch].location != NSNotFound)){
            
            self.title = @"Complete";
            [self.navigationItem setRightBarButtonItem:nil];
            [self createSignButton];
        }
         return YES;
    }
    
    return true;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
      [self toggleLoadingItems:NO];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
   
   /* if(_isFirstTime == YES){
        NSString* gsProfileUrlStr = [NSString stringWithFormat:@"https://%@%@", [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel], self.gsProfileUpliftUrl];
        NSURL *targetURL = [NSURL URLWithString:gsProfileUrlStr];
        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
         [_gsProfileUpliftWebView loadRequest:request];
        _isFirstTime = NO;
        
    } else{*/
         [self toggleLoadingItems:YES];
   // }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self toggleLoadingItems:YES];
}

//- (BOOL)disablesAutomaticKeyboardDismissal {
//    return NO;
//   
//}

- (void)createLoadingView {
    
    if(!_loadingView) {
        
        _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [_loadingView.spinnerBackgroundView setBackgroundColor:[UIColor clearColor]];
        [_loadingView.loadingLabel setHidden:YES];
        [_loadingView setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:_loadingView];
    }
    
    _loadingView.hidden = NO;
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}


- (void)toggleLoadingItems:(BOOL)isHide {
    dispatch_async(dispatch_get_main_queue(), ^{
        if(isHide)
        {
            [self->_loadingView stopAnimatingLoadingIndicatorView];
            [self->_loadingView setHidden:isHide];
            self->_loadingView = nil;
        }
        else
        {
            [self createLoadingView];
            [self->_loadingView setHidden:isHide];
            [self->_loadingView startAnimatingLoadingIndicatorView];
        }
    });
}

- (void)deleteCookies
{
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
    
    for(NSHTTPCookie *cookie in cookies)
    {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
