//
//  BTGSProfileUpliftInterceptViewController.h
//  BTBusinessApp
//
//  Created by kumaresh shrivastava on 10/05/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseViewController.h"
#import "WebKit/WebKit.h"
NS_ASSUME_NONNULL_BEGIN

@interface BTGSProfileUpliftInterceptViewController : BTBaseViewController <UIWebViewDelegate,WKNavigationDelegate,WKUIDelegate>
@property (nonatomic, strong) NSString* gsProfileUpliftUrl;
@end

NS_ASSUME_NONNULL_END
