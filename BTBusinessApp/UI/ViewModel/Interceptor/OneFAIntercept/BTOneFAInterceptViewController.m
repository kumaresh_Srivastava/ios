//
//  BTOneFAInterceptViewController.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 06/08/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOneFAInterceptViewController.h"
#import "BTSecurityNumberViewController.h"
#import "AppConstants.h"
#import "BTUICommon.h"
#import <UIView+Shake/UIView+Shake.h>
#import "DLMPinUnlockScreen.h"
#import "DLMPinCreationScreen.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "DLMSecurityCheckScreen.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "CustomSpinnerAnimationView.h"
#import "NLWebServiceError.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "BTRetryView.h"
#import "AppConstants.h"

#define kScreenName @"Security check"
#define kWroungOneOffCode  @"You've entered an incorrect one-off code."
#define kNoAttempLeft @"You have no attempts left."
#define kAttemptLeft @"You have %ld attempts left. Try Again."
#define kOneAttemptLeft @"You have %ld attempt left. Try Again."
#define kTryAgain @"Try Again."
#define kChatWithSupport  @"https://btbusiness.custhelp.com/app/chat/chatForm1/q_id/140"
#define kCallSupport @"0800800154"

@interface BTOneFAInterceptViewController () <UITextFieldDelegate,BTSecurityNumberViewControllerDelegate,DLMSecurityCheckScreenDelegate>
{
    BOOL _isAnimationDone;
    UIView *_overlayView;
    CustomSpinnerAnimationView *_loaderView;
     BOOL _isTextFieldEnabled;
    BTRetryView *_retryView;
    UIGestureRecognizer *tapper;
}
@property (strong, nonatomic) IBOutlet UIButton *actionButton;
- (IBAction)buttonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *continue_ReturnToSign;

@property (strong,nonatomic) DLMSecurityCheckScreen *securityCheckModel;
@property (weak, nonatomic) IBOutlet UILabel *pinMessageLabel;
@property (weak, nonatomic) IBOutlet UILabel *pinMessageNumberOfAttemptLeftLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactUsLabel;
@property (weak, nonatomic) IBOutlet UITextView *securityMeasureDescription;

//@property (weak, nonatomic) IBOutlet UIView *pinErrorMessageView;

@property (nonatomic,strong) NSString *PIN;
//@property (strong, nonatomic) IBOutlet UILabel *promptLabel;
@property (strong, nonatomic) IBOutlet UIView *pinDotsContainerView;
@property (weak, nonatomic) IBOutlet UIView *InputPinContainerView;

@property (weak, nonatomic) IBOutlet UIView *pinDotBoxView01;
@property (weak, nonatomic) IBOutlet UIView *pinDotBoxView02;
@property (weak, nonatomic) IBOutlet UIView *pinDotBoxView04;
@property (weak, nonatomic) IBOutlet UIView *pinDotBoxView03;
@property (strong, nonatomic) IBOutlet UIImageView *animationImageView;
@property (strong, nonatomic) IBOutlet UIView *pinDotView01;
@property (strong, nonatomic) IBOutlet UIView *pinDotView02;
@property (strong, nonatomic) IBOutlet UIView *pinDotView03;
@property (strong, nonatomic) IBOutlet UIView *pinDotView04;
@property (strong, nonatomic) IBOutlet UITextField *pinEntryTextField;
@property (weak, nonatomic) IBOutlet UILabel *authnenticationLabel;
@property (strong, nonatomic) IBOutlet UIView *keyboardPlaceholderView;
@property (weak, nonatomic) IBOutlet UIView *spinnerView;
@property (strong,nonatomic) UIView *separatorViewForKeyboard;
@end



@implementation BTOneFAInterceptViewController

static NSInteger const kMaxNumPinEntryAttempts = 3;

static NSTimeInterval const kStateSwitchDelay = 0.25;//0.15 remove this comment after testing
static NSTimeInterval const kKickToAuthDelayInterval = 0.85;    // Minor delay after someone locks themselves out.
static NSTimeInterval const kAnimationDurationInterval = 0.68;  // 25 FPS.

static NSInteger const kNumPinDotShakes = 10;
static CGFloat const kPinDotShakeDelta = 8.0;
static NSTimeInterval const kPinDotShakeSpeed = 0.05;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    self.title = kScreenName;
    
    UIBarButtonItem *menuItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                              target:self
                                                                              action:@selector(moveToSignInScreen)];
    [self.navigationItem setRightBarButtonItem:menuItem];
    // Do any additional setup after loading the view.
     [self trackContentViewedForPage:OMNIPAGE_OneFA andNotificationPopupDetails:OMNIPAGE_SECURITY_CHECK];
}

- (void) moveToSignInScreen{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated{
     [self animatePinRoadblockScreenItems];
    
}
- (void)setupUI {
    
    self.securityCheckModel = [[DLMSecurityCheckScreen alloc] init];
    
    self.securityCheckModel.securityCheckScreenDelegate = self;
    // Alloc required objects.
    [self setPinDotContainers:[NSMutableArray new]];
    [self setPinDots:[NSMutableArray new]];
    [self hideMessageWhenLoadedFirstTime];
    // Load frames and configure animation.
    NSArray* animationFrames = [BTUICommon framesForAnimationWithName:@"animate_tick" andLength:17];
    
    [[self animationImageView] setImage:[animationFrames lastObject]];
    [[self animationImageView] setAnimationImages:animationFrames];
    [[self animationImageView] setAnimationRepeatCount:1];
    [[self animationImageView] setAnimationDuration:kAnimationDurationInterval];
    
    [[self pinDotContainers] addObject:[self pinDotBoxView01]];
    [[self pinDotContainers] addObject:[self pinDotBoxView02]];
    [[self pinDotContainers] addObject:[self pinDotBoxView03]];
    [[self pinDotContainers] addObject:[self pinDotBoxView04]];
    
    [[self pinDots] addObject:[self pinDotView01]];
    [[self pinDots] addObject:[self pinDotView02]];
    [[self pinDots] addObject:[self pinDotView03]];
    [[self pinDots] addObject:[self pinDotView04]];
    
    // Set delegates.
    [[self pinEntryTextField] setDelegate:self];
    [self setCurrentState:OneFA_StateRoadblockDefault];
    // Start monitoring network activity state changes.
    __weak typeof(self) weakSelf = self;
    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([weakSelf networkRequestInProgress]) {
            [weakSelf.authnenticationLabel setHidden:NO];
            [weakSelf.animationImageView setHidden:NO];
           //[weakSelf showLoaderView];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
        }
        
        else {
            [weakSelf.animationImageView setHidden:YES];
            [weakSelf.authnenticationLabel setHidden:YES];
            //[weakSelf hideLoaderView];
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
        }
    }];
    
    // Start monitoring state changes.
    [RACObserve(self, currentState) subscribeNext:^(NSNumber* state) {
        [weakSelf refreshViewForCurrentState];
    }];
    
    // Monitor PIN entry text field.
    [[[self pinEntryTextField] rac_textSignal] subscribeNext:^(id next) {
        // Normalise everything first.
        [weakSelf colourPinPlaceholderDotsWithColour:[UIColor clearColor]];
        
        // Colour the correct number of dots.
        for (NSInteger i = 0; i < [[[weakSelf pinEntryTextField] text] length]; i ++) {
            [[[weakSelf pinDots] objectAtIndex:i] setBackgroundColor:[UIColor blackColor]];
        }
        if ([[[weakSelf pinEntryTextField] text] length] == 4) {
            // Debug.
            DDLogInfo(@"Pin: PIN entered. Checking...");
            DDLogInfo(@"Pin: %@", [[weakSelf pinEntryTextField] text]);
            
            if (![weakSelf pinEntryTextFieldIsFrozen]) {
                //[weakSelf verifyPinAndUpdateStateForPin:[[weakSelf pinEntryTextField] text]];
                NSString* text = [[self pinEntryTextField] text];
                 weakSelf.PIN = text;
                 //[self pinEntryTextField].text = @"";

                self.continue_ReturnToSign.enabled = YES;
                [self.continue_ReturnToSign setTitle:@"Continue" forState:UIControlStateNormal];
                [self.continue_ReturnToSign setAlpha:1.0];
               
            }
            else {
                DDLogInfo(@"Pin: Ignoring check as PIN text field has been locked.");
            }
        }
    }];
    
    /*UIFont *regularFont = [UIFont fontWithName:kBtFontRegular size:15.0];
    //UIFont *boldFont = [UIFont fontWithName:kBtFontBold size:14.0];
    
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setAlignment:NSTextAlignmentCenter];
    [style setLineBreakMode:NSLineBreakByWordWrapping];
    
    NSMutableAttributedString *fullText = [[NSMutableAttributedString alloc] initWithString:@"Keeping My Account even more protected\n" attributes:@{NSFontAttributeName:regularFont,NSForegroundColorAttributeName:[UIColor whiteColor],NSParagraphStyleAttributeName:style}];
    NSMutableAttributedString *underlineText = [[NSMutableAttributedString alloc] initWithString:@"Set up your security number now" attributes:@{NSFontAttributeName:regularFont,NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),NSForegroundColorAttributeName:[UIColor whiteColor],NSUnderlineColorAttributeName:[UIColor whiteColor],NSParagraphStyleAttributeName:style}];
    [fullText appendAttributedString:underlineText];
    [self.actionButton.titleLabel setNumberOfLines:2];
    [self.actionButton.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [self.actionButton setAttributedTitle:fullText forState:UIControlStateNormal];*/
    
}

- (void) hideMessageWhenLoadedFirstTime{
    self.pinMessageLabel.hidden = YES;
    self.pinMessageNumberOfAttemptLeftLabel.hidden = YES;
    self.continue_ReturnToSign.enabled = NO;
    [self.continue_ReturnToSign setTitle:@"Continue" forState:UIControlStateNormal];
    [self.continue_ReturnToSign setAlpha:0.3];
}

- (void) viewWillAppear : (BOOL) animated {
    [super viewWillAppear:animated];
    //notification for keyboard
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appplicationDidBecomActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appplicationWillResignActive:)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    _isAnimationDone = NO;
     self.title = kScreenName;
    self.pinMessageNumberOfAttemptLeftLabel.hidden = true;
    [self colourPinDotContainersWithColour:[BrandColours colourMyBtMediumGrey]];

    
}
- (void)viewWillDisappear:(BOOL)animated{
     self.title = @"";
    [self pinEntryTextField].text = @"";
    [self hideMessageWhenLoadedFirstTime];
    [self colourPinDotContainersWithColour:[BrandColours colourMyBtMediumGrey]];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
   // [self hideProgressView];
    self.networkRequestInProgress = NO;
    [self.securityCheckModel cancelSecurityCheckScreenRequest];
    
}

#pragma mark - Application active status notification
- (void)appplicationDidBecomActive: (NSNotification *)notification {
    self.separatorViewForKeyboard.hidden = NO;
    if(self.currentState != OneFA_StateRoadblockOk && self.currentState != OneFA_StateSetupConfirmPinOk) {
        [[self pinEntryTextField] becomeFirstResponder];
        DDLogInfo(@"Pin: Pin is now first responder");
    }
}

- (void)appplicationWillResignActive: (NSNotification *)notification {
    self.separatorViewForKeyboard.hidden = YES;
    [[self pinEntryTextField] resignFirstResponder];
    
}

- (void)applicationWillEnterForeground: (NSNotification *)notification {
   
}

#pragma mark - animation handler

- (void)animatePinRoadblockScreenItems {
    DDLogInfo(@"Pin: Animating the PINScreen items after the view got loaded");
    [self.view layoutIfNeeded];
    if (!_isAnimationDone) {
        [UIView animateWithDuration:0.8 delay:0.0 options:0 animations:^{
            
            self.InputPinContainerView.hidden = NO;
            [self.pinDotsContainerView setHidden:NO];
            
            _isTextFieldEnabled = YES;
            
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
            _isAnimationDone = YES;
            
            [[self pinEntryTextField] becomeFirstResponder];
        }];
    }
}


- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow {
    
    if (!_retryView)
    {
        _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
        _retryView.translatesAutoresizingMaskIntoConstraints = NO;
        _retryView.retryViewDelegate = self;
        
        [self.view addSubview:_retryView];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    }
    
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
    
}

#pragma mark - Fire API Request


- (void)userFilledSecurityAnswer:(NSString*) answer {
    
    if(![AppManager isInternetConnectionAvailable])
    {
        [self showRetryViewWithInternetStrip:YES];
        return;
    }
    //[self removeErrorMessage];
    [self performSelector:@selector(makeAPIRequestWithPassword:) withObject:answer afterDelay:0.5];
}


- (void)makeAPIRequestWithPassword:(NSString *)answerText
{
    [self.view endEditing:YES];
    [self showProgressView];
    
    [self.securityCheckModel fetchSecurityCheckScreenDataWithAnswer:answerText];
}

#pragma mark PINState Methods

//Checking all the possible states for the PIN creation and Unlock with all the positive and negative states
- (void) refreshViewForCurrentState {
    // Setup - initial stage.
    if ([self currentState] == OneFA_StateSetupInit) {
        DDLogInfo(@"Pin: We're in the init state");
        [self setPinEntryTextFieldIsFrozen:false];
        
        //Omniture
        [self trackOmnitureEventForPage:OMNIPAGE_CREATEPIN];
        
        [self backgroundColourPinDotContainersWithColour:[UIColor whiteColor]];
        [self colourPinDotContainersWithColour:[UIColor grayColor]];
        
        [[self animationImageView] setHidden:true];
       // [self hideForgottenIdActionButtons];
        
        [[self pinEntryTextField] setText:@""];
        [[self pinEntryTextField] sendActionsForControlEvents:UIControlEventEditingChanged];
        [[self pinEntryTextField] becomeFirstResponder];
    }
    
    // Setup - confirm PIN entered in first stage.
    else if ([self currentState] == OneFA_StateSetupConfirmPin) {
        [self setPinEntryTextFieldIsFrozen:false];
        [self backgroundColourPinDotContainersWithColour:[UIColor whiteColor]];
        [self colourPinDotContainersWithColour:[UIColor grayColor]];
        
        //Omniture
        [self trackOmnitureEventForPage:OMNIPAGE_RECREATEPIN];
        
        [[self animationImageView] setHidden:true];
        // Accessibility.
       // UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, [self promptLabel]);
    }
    
    // Setup - temporary state to show the user that they entered their PIN incorrectly.
    else if ([self currentState] == OneFA_StateSetupConfirmPinFail) {
        [self setPinEntryTextFieldIsFrozen:true];
        
        [[self animationImageView] setHidden:true];
        
        [self trackOmnitureError:OMNIERROR_REGISTRATION_INCORRECT_PIN];
        
        [self colourPinDotContainersWithColour:[BrandColours colourMyBtRed]];
        [self colourPinPlaceholderDotsWithColour:[UIColor blackColor]];
        
        [[self pinDotView01] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{}];
        [[self pinDotView02] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{}];
        [[self pinDotView03] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{}];
        [[self pinDotView04] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, kStateSwitchDelay * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                
                [self setCurrentState:OneFA_StateSetupInit];
                [self setTmpPinForConfirmation:@""];
            });
        }];
    }
    
    // Setup - user failed to enter the same PIN again.
    // They'll need to stay here until they do this.
    else if ([self currentState] == OneFA_StateSetupConfirmPinFailTryAgain) {
        [self setPinEntryTextFieldIsFrozen:false];
        
       // [[self promptLabel] setHidden:true];
        
        [[self animationImageView] setHidden:true];
        //[self hideForgottenIdActionButtons];
        
        [self colourPinDotContainersWithColour:[BrandColours colourMyBtMediumGrey]];
        
        [[self pinEntryTextField] setText:@""];
        [[self pinEntryTextField] sendActionsForControlEvents:UIControlEventEditingChanged];
    }
    
    // Setup - all done!
    else if ([self currentState] == OneFA_StateSetupConfirmPinOk) {
        
        //Remove UserName from Appmanager
        [AppManager sharedAppManager].usernameForSignInIntercepter = nil;
        
        //Remove All Intercepter related changes if exist
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kPasswordLoginIntercepter];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kTokenLoginIntercepter];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kSMSessionLoginIntercepter];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kGroupKeyLoginIntercepter];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        [self setPinEntryTextFieldIsFrozen:true];
        [[self pinEntryTextField] endEditing:true];
        
        
        [self colourPinPlaceholderDotsWithColour:[UIColor blackColor]];
        [self colourPinDotContainersWithColour:[BrandColours colourMyBtGreen]];
        
        [[self animationImageView] setHidden:false];
        [[self animationImageView] startAnimating];
        
        // Accessibility.
        //UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, [self promptLabel]);
        
        // Debug.
        DDLogInfo(@"Pin: Setup complete!");
        
        [self trackOmnitureKeyTask];
        
    }
    
    // Roadblock - default screen. PIN Unlock Screen
    else if ([self currentState] == OneFA_StateRoadblockDefault) {
        [self setPinEntryTextFieldIsFrozen:false];
        
        [self.animationImageView setHidden:YES];
        
        //Omniture
        [self trackOmnitureEventForPage:OMNIPAGE_LOGINPIN];
        
        [[self animationImageView] setHidden:true];
        
        [self colourPinDotContainersWithColour:[UIColor grayColor]];//[BrandColours colourMyBtMediumGrey]
        [self backgroundColourPinDotContainersWithColour:[UIColor whiteColor]];
        
        [[self pinEntryTextField] setText:@""];
        [[self pinEntryTextField] sendActionsForControlEvents:UIControlEventEditingChanged];
        
        NSURL* url = [[NSURL alloc] initWithString:kChatWithSupport];
        NSString* phoneNumber = @"0800800154";
        
        NSString* contactText1 = [NSString stringWithFormat:@"This account was created for you by BT. As a security measure, a one-off code was provided to you over the phone or sent to you via SMS. If you do not have the one-off code, please call us on %@ or",phoneNumber];
        
        NSString* completeContactText = [NSString stringWithFormat:@"%@ contact us.",contactText1];
        NSRange phoneNumberCharRange = NSMakeRange(contactText1.length - phoneNumber.length - 3,phoneNumber.length);
        NSRange urlLinkCharRange = NSMakeRange(contactText1.length +1, 10);
        
        NSMutableAttributedString *description = [self getFormattedMessageWithContactPhoneAndUrlLinks:completeContactText phoneNumber:phoneNumber url:url withPhoneNumberCharRange:phoneNumberCharRange withUrlLinkCharRange:urlLinkCharRange];
        
        self.securityMeasureDescription.attributedText = description;
    }
    
    // Roadblock - temporary state to show the user they entered their PIN incorrectly.
    else if ([self currentState] == OneFA_StateRoadblockFail) {
        
        [self setPinEntryTextFieldIsFrozen:true];
        [self trackOmnitureError:OMNIERROR_LOGIN_INVALID_PIN];
        
        [[self animationImageView] setHidden:true];
        
        // Uncomment the below line to announce the error message on entering incorrect pin with accessibility
        //UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, kPinIncorrectErrorMessage);
        
        [self colourPinDotContainersWithColour:[BrandColours colourMyBtRed]];
        [self colourPinPlaceholderDotsWithColour:[UIColor blackColor]];
        
        [[self pinDotView01] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{}];
        [[self pinDotView02] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{}];
        [[self pinDotView03] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{}];
        [[self pinDotView04] shake:kNumPinDotShakes withDelta:kPinDotShakeDelta speed:kPinDotShakeSpeed completion:^{
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, kStateSwitchDelay * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                [self setCurrentState:OneFA_StateRoadblockFailTryAgain];
            });
        }];
    }
    
    // Roadblock - user got their PIN wrong.
    // They'll need to try again. (Number of attempts is handled outside this function)
    else if ([self currentState] == OneFA_StateRoadblockFailTryAgain) {
        
        [self setPinEntryTextFieldIsFrozen:false];
        
        [[self animationImageView] setHidden:true];

        self.continue_ReturnToSign.enabled = NO;
        [self.continue_ReturnToSign setAlpha:0.3];
        
        self.pinMessageLabel.hidden = NO;
        self.pinMessageNumberOfAttemptLeftLabel.hidden = NO;
        NSString* errorText = @"";
        long numberofAttempLeft = kMaxNumPinEntryAttempts - [self numPinEntryAttempts];

        if(numberofAttempLeft == 1){
             errorText = [NSString stringWithFormat:kOneAttemptLeft,(long)numberofAttempLeft];
        } else{
             errorText = [NSString stringWithFormat:kAttemptLeft,(long)numberofAttempLeft];
        }
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:errorText attributes:@{
                                                                                                    NSFontAttributeName: [UIFont fontWithName:@"BTFont-Regular" size: 14.0f],
                                                                                                    NSForegroundColorAttributeName: [UIColor colorForHexString:@"E60014"]
              
                                                                                                    }];
         if(numberofAttempLeft == 1){
             [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorForHexString:@"0A60FF"] range:NSMakeRange(kOneAttemptLeft.length - kTryAgain.length-2, kTryAgain.length)];
         } else {
               [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorForHexString:@"0A60FF"] range:NSMakeRange(kAttemptLeft.length - kTryAgain.length-2, kTryAgain.length)];
         }
       
        self.pinMessageNumberOfAttemptLeftLabel.attributedText = attributedString;//errorText;
        
        [self colourPinDotContainersWithColour:[BrandColours colourMyBtMediumGrey]];
        
        [[self pinEntryTextField] setText:@""];
        [[self pinEntryTextField] sendActionsForControlEvents:UIControlEventEditingChanged];
        [[self pinEntryTextField] becomeFirstResponder];
        
    }
    
    // Roadblock - temporary state that kicks users back to the sign in screen.
    else if ([self currentState] == OneFA_StateRoadblockFailKickToAuth) {
        
        [self trackOmnitureError:OMNIERROR_LOGIN_INVALID_PIN_3_TIMES];
        
        [self trackContentViewedForPage:OMNIPAGE_LOGINPIN andNotificationPopupDetails:OMNIPAGE_PIN_INCORRECT_OK];
        
        [self setPinEntryTextFieldIsFrozen:true];
        [[self view] endEditing:true];
        [[self animationImageView] setHidden:true];
        self.pinMessageLabel.hidden = NO;

        self.pinMessageLabel.text = kWroungOneOffCode;
        self.pinMessageNumberOfAttemptLeftLabel.hidden = NO;
        self.pinMessageNumberOfAttemptLeftLabel.text = kNoAttempLeft;
        
        [self.navigationItem setRightBarButtonItem:nil];
        
        self.contactUsLabel.hidden = NO;
        self.continue_ReturnToSign.enabled = YES;
        [self.continue_ReturnToSign setTitle:@"Return to sign in" forState:UIControlStateNormal];
        [self.continue_ReturnToSign setAlpha:1.0];
    
        [self colourPinDotContainersWithColour:[BrandColours colourMyBtRed]];
        [self colourPinPlaceholderDotsWithColour:[UIColor blackColor]];
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, kKickToAuthDelayInterval * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            
        });
    }
    
    // Roadblock - all good. PIN entered successfully.
    else if ([self currentState] == OneFA_StateRoadblockOk) {
        [self setPinEntryTextFieldIsFrozen:true];
        [[self pinEntryTextField] endEditing:true];

        [self colourPinPlaceholderDotsWithColour:[UIColor blackColor]];
        [self colourPinDotContainersWithColour:[BrandColours colourMyBtGreen]];
        
        [[self animationImageView] setHidden:false];
        [[self animationImageView] startAnimating];
        
        // Debug.
        DDLogInfo(@"Note. Roadblock cleared. Dismissing and performing initialisation if required.");
        
        if ([self initRequired]) {
            if([AppManager isInternetConnectionAvailable]) {
                self.initRequired = NO;
                [self setNetworkRequestInProgress:true];
            } else {
                [AppManager trackNoInternetErrorBeforeLoginOnPage:OMNIPAGE_LOGINPIN];
            }
        }
    }
}

- (NSMutableAttributedString *)getFormattedMessageWithContactPhoneAndUrlLinks:(NSString *)contactText phoneNumber:(NSString *)phoneNumber url:(NSURL *)url withPhoneNumberCharRange:(NSRange)phoneNumberCharRange withUrlLinkCharRange:(NSRange)urlLinkCharRange {
    
    NSMutableAttributedString *message = [[NSMutableAttributedString  alloc] initWithString:contactText];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentLeft];
    
    NSDictionary *textFormat = @{NSFontAttributeName : [UIFont fontWithName:kBtFontRegular size:14.0],
                                         NSForegroundColorAttributeName : [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0],
                                          NSParagraphStyleAttributeName: paragraphStyle};
    
    [message addAttributes:textFormat range:NSMakeRange(0, message.length)];
    
    [self addUrlHyperLinkForContact:message url:url urlLinkCharRange:&urlLinkCharRange];
    
    [self addHyperLinkToPhoneNumberForIphone:message phoneNumber:phoneNumber phoneNumberCharRange:&phoneNumberCharRange];
    
    return message;
}

- (void)addUrlHyperLinkForContact:(NSMutableAttributedString *)message url:(NSURL *)url urlLinkCharRange:(const NSRange *)urlLinkCharRange {
    
    NSDictionary *urlLinkAttributesChat = @{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle),
                                            NSLinkAttributeName : url,
                                            NSForegroundColorAttributeName: [BrandColours colourTextBlueSecondaryColor],
                                            NSFontAttributeName: [UIFont fontWithName:kBtFontBold size:16.0]
                                            };
    
    [message addAttributes:urlLinkAttributesChat range:*urlLinkCharRange];
}

- (void)addHyperLinkToPhoneNumberForIphone:(NSMutableAttributedString *)message phoneNumber:(NSString *)phoneNumber phoneNumberCharRange:(const NSRange *)phoneNumberCharRange {
    
    if (IS_IPHONE) {
        NSURL* urlTell = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"tel:%@",phoneNumber]];
        
        NSDictionary *linkAttributesTell = @{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle),
                                             NSLinkAttributeName : urlTell,
                                             NSForegroundColorAttributeName:[BrandColours colourTextBlueSecondaryColor],
                                             NSFontAttributeName:[UIFont fontWithName:kBtFontBold size:16.0]
                                             };
        
        [message addAttributes:linkAttributesTell range:*phoneNumberCharRange];
    }
}

- (void) verifyPinAndUpdateStateForPin : (NSString*) pin {
    // If we're in the setup process, move to the confirm stage.
    // We introduce a slight delay here so the user has time to see their final PIN
    // digit appearing.
    // here we need to send the service call to server to validate the entered Answer PIN
    
   
    if ([self currentState] == OneFA_StateSetupInit) {
        [self setTmpPinForConfirmation:[[self pinEntryTextField] text]];
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, kStateSwitchDelay * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            [[self pinEntryTextField] setText:@""];
            [[self pinEntryTextField] sendActionsForControlEvents:UIControlEventEditingChanged];
            
            [self setCurrentState:OneFA_StateSetupConfirmPin];
            
            //[OmnitureManager trackPage:OMNIPAGE_CONFIRM_PIN];
        });
    }
    
    // Confirmation.
    // This affects the setup journey and a normal roadblock.
    else if ([self currentState] == OneFA_StateSetupConfirmPin ||
             [self currentState] == OneFA_StateSetupConfirmPinFailTryAgain ||
             [self currentState] == OneFA_StateRoadblockDefault ||
             [self currentState] == OneFA_StateRoadblockFailTryAgain) {
        
         BOOL pinConfirmedOk = false;
           if ([self currentState] == OneFA_StateSetupConfirmPin || [self currentState] == OneFA_StateSetupConfirmPinFailTryAgain){
                // Reset counter.
                [self setNumPinEntryAttempts:0];
                // Set flag.
                pinConfirmedOk = true;
            }
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, kStateSwitchDelay * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            if (pinConfirmedOk) {
                DDLogInfo(@"Pin: Pin confirmed okay and is being saved.");
                // PIN ok and we're in the setup journey.
                if ([self currentState] == OneFA_StateSetupConfirmPin || [self currentState] == OneFA_StateSetupConfirmPinFailTryAgain) {
                    [self setPIN:[[self pinEntryTextField] text]];
                    [self setCurrentState:OneFA_StateSetupConfirmPinOk];
                    DDLogInfo(@"Pin: Pin saved.");
                }
                // PIN ok and we're on a roadblock.
                else {
                    [self setCurrentState:OneFA_StateRoadblockOk];
                }
            }
            else {
                // PIN failed and we're in the setup journey.
                if ([self currentState] == OneFA_StateSetupConfirmPin || [self currentState] == OneFA_StateSetupConfirmPinFailTryAgain) {
                    //[OmnitureManager trackError:OMNIERROR_PIN_MISMATCH onPageWithName:OMNIPAGE_SETUP_PIN];
                    
                    [self setCurrentState:OneFA_StateSetupConfirmPinFail];
                }
                
                // PIN failed and we're on a roadblock.
                else {
                    //[OmnitureManager trackError:OMNIERROR_PIN_INCORRECT onPageWithName:OMNIPAGE_PIN_LOGIN];
                    
                    [self setNumPinEntryAttempts:[self numPinEntryAttempts] + 1];
                    
                    if ([self numPinEntryAttempts] < kMaxNumPinEntryAttempts) {
                        [self setCurrentState:OneFA_StateRoadblockFail];
                    } else {
                        [self setCurrentState:OneFA_StateRoadblockFailKickToAuth];
                    }
                }
            }
        });
    }
}

- (void)pinSetupJourneyCompleted
{
    //(VRK) sending pin & user Details to Signin page
    NSMutableDictionary *pinInfoDict = [[NSMutableDictionary alloc] init];
    [pinInfoDict setObject:self.PIN forKey:@"pin"];
    [pinInfoDict setObject:[self.pinCreationInfoDic objectForKey:@"username"] forKey:@"username"];
    [pinInfoDict setObject:[self.pinCreationInfoDic objectForKey:@"password"] forKey:@"password"];
    [pinInfoDict setObject:[self.pinCreationInfoDic objectForKey:@"token"] forKey:@"token"];
    [pinInfoDict setObject:[self.pinCreationInfoDic objectForKey:@"smsession"] forKey:@"smsession"];
    [pinInfoDict setObject:[self.pinCreationInfoDic objectForKey:@"userDetails"] forKey:@"userDetails"];
   
}

#pragma ColorRelated Methods
//change the color of PIN dots
- (void) colourPinPlaceholderDotsWithColour : (UIColor*) colour {
    for (UIView* tmpDot in [self pinDots]) {
        [tmpDot setBackgroundColor:colour];
    }
}

//Change the color of the PIN containers
- (void) colourPinDotContainersWithColour : (UIColor*) colour {
    for (UIView* tmpDot in [self pinDotContainers]) {
        [[tmpDot layer] setBorderWidth:1.0];
        [[tmpDot layer] setBorderColor:[colour CGColor]];
    }
}

//change the background color and assign border width to the PIN dots
- (void) backgroundColourPinDotContainersWithColour : (UIColor*) colour {
    for (UIView* tmpDot in [self pinDotContainers]) {
        [[tmpDot layer] setBorderWidth:1.0];
        [tmpDot setBackgroundColor:colour];
    }
}

#pragma mark - Button Action method

- (IBAction)buttonPressed:(UIButton*)sender {
  
    NSString* titleLabel = sender.titleLabel.text;
    if([titleLabel isEqualToString:@"Continue"]){
        [self userFilledSecurityAnswer:self.PIN];
        self.continue_ReturnToSign.enabled = NO;
       [self.continue_ReturnToSign setAlpha:0.3];
    } else {
        [self moveToSignInScreen];
    }
}

#pragma mark KeyboardNotification Methods
//this method called when the keyboard comes into picture
- (void)keyboardWillShow:(NSNotification *)notification
{
   if ([self currentState] == OneFA_StateRoadblockFailKickToAuth){
      // self.separatorViewForKeyboard.hidden = YES;
        return;
    }
    [self.view layoutIfNeeded];
    
    self.separatorViewForKeyboard.hidden = NO;
    
    [UIView animateWithDuration:0.0 //[[notification.userInfo valueForKey:@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue]
                          delay:0.0
                        options:[[notification.userInfo valueForKey:@"UIKeyboardAnimationCurveUserInfoKey"] doubleValue]
                     animations:^{
                         
                         
                         if (IS_IPHONE_4_OR_LESS) {
                             //CGRect keyboardEndFrame = [[notification.userInfo valueForKey:@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
                             //                             [self.pinScrollView setContentOffset:CGPointMake(0, 30)];
                         } else if (IS_IPHONE_5) {
                             
                         }
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                     }];
}

//this method called when the keyboard Hides
- (void)keyboardWillHide:(NSNotification *)notification
{
    [self.view layoutIfNeeded];
    
   self.separatorViewForKeyboard.hidden = YES;
    
    [UIView animateWithDuration:0.0 //[[notification.userInfo valueForKey:@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue]
                          delay:0.0
                        options:[[notification.userInfo valueForKey:@"UIKeyboardAnimationCurveUserInfoKey"] doubleValue]
                     animations:^{
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                     }];
    
}

- (void) keyboardWillChange : (NSNotification*) notification {
    if ([self currentState] == OneFA_StateRoadblockFailKickToAuth){
        return;
    }
    CGRect keyboardRect = [[notification userInfo][UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardHeight = [[self view] convertRect:keyboardRect fromView:nil].size.height;
}

#pragma mark - Text field delegate methods.

- (BOOL) textField : (UITextField*) textField shouldChangeCharactersInRange : (NSRange) range replacementString : (NSString*) string {
    if ([self pinEntryTextFieldIsFrozen]) {
        return false;
    } else {
        NSUInteger pinLength = 4;
        
        NSUInteger oldLength = [[textField text] length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= pinLength || returnKey;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if ([self currentState] == OneFA_StateRoadblockFailKickToAuth){
        //self.separatorViewForKeyboard.hidden = YES;
        return NO;
    }
    
    if(!self.separatorViewForKeyboard)
        self.separatorViewForKeyboard = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 1.0 / [UIScreen mainScreen].scale)];
    
    self.separatorViewForKeyboard.frame =CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 1.0 / [UIScreen mainScreen].scale);
    self.separatorViewForKeyboard.backgroundColor = [UIColor lightGrayColor];
    textField.inputAccessoryView =  self.separatorViewForKeyboard;
    
    self.separatorViewForKeyboard.hidden  =NO;
    
    return _isTextFieldEnabled;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    self.separatorViewForKeyboard.hidden = YES;
    return YES;
}


#pragma mark - BTSecurityNumberViewControllerDelegate

- (void)successfullyFetchedSecurityDataOnSecurityCheckScreen:(DLMSecurityCheckScreen *)securityCheckScreen
{
    BTSecurityCheck *securityCheckModel = securityCheckScreen.securityCheckModel;
    [self hideProgressView];
    [self setNetworkRequestInProgress:false];
    
    if(securityCheckModel.status == 1){//Failed to validate
       
        [self changeStateForOneoffCodeError];
    }
    else{
        [self setNumPinEntryAttempts:0];
        [self setCurrentState:OneFA_StateRoadblockDefault];
        BTSecurityNumberViewController *updateSecurityNumber = [self.storyboard instantiateViewControllerWithIdentifier:@"BTSecurityNumberViewController"];
        updateSecurityNumber.delegate = self;
        updateSecurityNumber.isScreenrelatedTo1FAMigration = self.isScreenrelatedTo1FAMigration;
        updateSecurityNumber.isScreenRelatedToInterceptorModule = self.isScreenRelatedToInterceptorModule;
        [self.navigationController pushViewController:updateSecurityNumber animated:YES];
    }
}

- (void)failedToFetchDataOnSecurityCheckScreen:(DLMSecurityCheckScreen *)securiryCheckScreen withWebServiceError:(NLWebServiceError *)webServiceError
{
    [self hideProgressView];
    [self setNetworkRequestInProgress:false];
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
    
    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {
        switch (webServiceError.error.code)
        {
            case BTNetworkErrorCodeAPINoDataFound:
            {
                errorHandled = YES;
                [self changeStateForOneoffCodeError];
                break;
            }
            default:
            {
                errorHandled = NO;
                [self changeStateForOneoffCodeError];
                break;
            }
        }
    } else {
        [self changeStateForOneoffCodeError];
    }
}

- (void) changeStateForOneoffCodeError{
    [self setNumPinEntryAttempts:[self numPinEntryAttempts] + 1];
    
    if ([self numPinEntryAttempts] < kMaxNumPinEntryAttempts) {
        [self setCurrentState:OneFA_StateRoadblockFailTryAgain];
    }
    else {
        [self setCurrentState:OneFA_StateRoadblockFailKickToAuth];
    }
}



#pragma mark - Loading spinner view

- (void)createLoaderImageView
{
    _loaderView = [[CustomSpinnerAnimationView alloc] initWithText:@"" loadingImage:[UIImage imageNamed:@"Spinner"] andLoaderImageSize:20.0f];
    
    [self.spinnerView addSubview:_loaderView];
    
    [self.spinnerView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.spinnerView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [self.spinnerView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.spinnerView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    [_loaderView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:100]];
    [_loaderView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:155]];
    _loaderView.translatesAutoresizingMaskIntoConstraints = NO;
    
}

- (void)showLoaderView
{
    [self createLoaderImageView];
    
    _loaderView.hidden = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_loaderView startAnimation];
    });
}

- (void)hideLoaderView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [_loaderView stopAnimation];
    });
    
    _loaderView.hidden = YES;
    
    [_loaderView removeFromSuperview];
    
    _loaderView = nil;
    
}

#pragma mark Retry Delegate methods

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    [self hideRetryItems:YES];
    
}

- (void)hideRetryItems:(BOOL)isHide {
    
    // (SD) Hide or Show UI elements related to retry.
    [_retryView setRetryViewDelegate:nil];
    [_retryView removeFromSuperview];
    _retryView = nil;
    [self userFilledSecurityAnswer:_PIN];
}


#pragma mark Omniture methods

- (void)trackOmnitureClickEvent
{
    NSString *pageName = OMNIPAGE_OneFA;
    NSString *linkTitle = OMNICLICK_LOGIN_RETURN_TO_LOGIN;
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Not Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:data];
}

- (void)trackOmnitureEventForPage:(NSString *)page
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = page;
    [data setValue:@"Not Logged In" forKey:kOmniLoginStatus];
   
    [OmnitureManager trackPage:pageName withContextInfo:data];
}


- (void)trackOmnitureKeyTask
{

    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = OMNIPAGE_RECREATEPIN;
    [data setValue:@"Not Logged In" forKey:kOmniLoginStatus];
    [data setValue:OMNICLICK_LOGIN_PIN_CREATED forKey:kOmniKeyTask];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackOmnitureError:(NSString *)error
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Not Logged In" forKey:@"LoginStatus"];
    
    [OmnitureManager trackError:error onPageWithName:OMNIPAGE_LOGINPIN contextInfo:data];
}

- (void)trackContentViewedForPage:(NSString *)page andNotificationPopupDetails:(NSString *)notificationPopup
{

    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Not Logged In" forKey:kOmniLoginStatus];
    
    NSString *notificaiton = [NSString stringWithFormat:@"%@%@",STATE_PREFIX,notificationPopup];
    [data setValue:notificaiton forKey:kOmniContentViewed];
    
    [OmnitureManager trackPage:page withContextInfo:data];
}



@end
