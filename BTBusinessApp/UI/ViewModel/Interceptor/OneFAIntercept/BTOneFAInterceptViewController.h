//
//  BTOneFAInterceptViewController.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 06/08/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTSecurityNumberViewController.h"

@class BTOneFAInterceptViewController;

@protocol BTOneFAInterceptViewController <NSObject>

@optional

- (void)pinViewController:(BTOneFAInterceptViewController *)pinViewController userHasSuccesfullySetupThePINWithInfo:(NSDictionary *)infoDic;

- (void)userDecidedToEnterPasswordInsteadOfPinOnPinViewController:(BTOneFAInterceptViewController *)pinViewController;

- (void)userSuccesfullyEnteredTheCorrectPINAndUserAuthenticationDoneOnPinViewController:(BTOneFAInterceptViewController *)pinViewController;

- (void)userEnteredIncorrectPINThreeTimesOnPinViewController:(BTOneFAInterceptViewController *)pinViewController;


@end

@interface BTOneFAInterceptViewController : BTBaseViewController 

typedef NS_ENUM(NSUInteger, MyBtPinEntryStateOneFA) {
    OneFA_StateRoadblockDefault,
    OneFA_StateRoadblockFail,
    OneFA_StateRoadblockFailTryAgain,
    OneFA_StateRoadblockFailTryAgainTouchId,
    OneFA_StateRoadblockFailKickToAuth,
    OneFA_StateRoadblockOk,
    OneFA_StateRoadblockTouchId,
    OneFA_StateRoadblockFaceId,
    OneFA_StateSetupInit,
    OneFA_StateSetupConfirmPin,
    OneFA_StateSetupConfirmPinFail,
    OneFA_StateSetupConfirmPinFailTryAgain,
    OneFA_StateSetupConfirmPinOk
};


@property (nonatomic) id<BTSecurityNumberViewControllerDelegate> delegate;

@property (nonatomic, assign) BOOL isScreenRelatedToInterceptorModule;
@property (nonatomic, assign) BOOL isScreenrelatedTo1FAMigration;

@property (nonatomic) NSDictionary *pinCreationInfoDic;
@property (strong, nonatomic) NSMutableArray* pinDotContainers;
@property (strong, nonatomic) NSMutableArray* pinDots;
@property (strong, nonatomic) NSString* tmpPinForConfirmation;
@property (nonatomic) MyBtPinEntryStateOneFA currentState;
@property (nonatomic) NSInteger numPinEntryAttempts;
@property (nonatomic) NSInteger numRetries; // Auth / customer profile requests.
@property (nonatomic) BOOL isPinSetupJourney;   // Indicates if this is part of the setup journey. (Eg: For setting PIN)
@property (nonatomic) BOOL initRequired;    // Indicates if this view will be responsible for authentication / fetching customer profile.
@property (nonatomic) BOOL pinEntryTextFieldIsFrozen;
@property (nonatomic) BOOL networkRequestInProgress;

@end
