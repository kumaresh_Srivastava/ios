//
//  DLMCreateNewPasswordScreen.m
//  BTBusinessApp
//
//  Created by VectoScalar on 12/8/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMCreateNewPasswordScreen.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "CDAuthenticationToken.h"
#import "SAMKeychain.h"
#import "AppConstants.h"

@interface DLMCreateNewPasswordScreen()

@end

@implementation DLMCreateNewPasswordScreen

- (void)submitNewPassword:(NSString *)newPassword securityQuestion:(NSString *)question andSecurityAnswer:(NSString *)answer andGroupKeyFromSignInScreen:(NSString *)groupKeyFromSignInScreen{
    
    
    // Get the currently logged in User
    CDUser *user = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    
    NSString *password;
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:kPasswordLoginIntercepter])
    {
        password = [[NSUserDefaults standardUserDefaults] valueForKey:kPasswordLoginIntercepter];
    }
    else
    {
        NSError *errorReadingPassword;
      password = [SAMKeychain passwordForService:kKeyChainServiceBTUserPassword account:user.username error:&errorReadingPassword];
    }

    
    NSMutableDictionary *updatedDictionary = [NSMutableDictionary dictionary];
    [updatedDictionary setValue:[AppManager getRSAEncryptedString:newPassword] forKey:@"NewPassword"];
    [updatedDictionary setValue:[AppManager getRSAEncryptedString:password] forKey:@"Password"];
    [updatedDictionary setValue:[AppManager getRSAEncryptedString:question] forKey:@"SecretQuestion"];
    [updatedDictionary setValue:[AppManager getRSAEncryptedString:answer] forKey:@"SecretAnswer"];
    [updatedDictionary setValue:[NSNumber numberWithBool:YES] forKey:@"IsSecurityQuestionChanged"];
    
    //(lp) Send the groupkey with text "Key"
    
    if(groupKeyFromSignInScreen && groupKeyFromSignInScreen.length > 0)
    {
      [updatedDictionary setValue:groupKeyFromSignInScreen forKey:@"Key"];
    }
    else
    {
      [updatedDictionary setValue:user.currentSelectedCug.groupKey forKey:@"Key"];
    }
    
    self.queryUpdateProfileWebService.queryUpdateProfileWebServiceDelegate = nil;
    self.queryUpdateProfileWebService = [[NLQueryUpdateProfileWebService alloc] initWithUpdatedProfileDetailsDictionary:updatedDictionary];
    
    self.queryUpdateProfileWebService.queryUpdateProfileWebServiceDelegate = self;
    [self.queryUpdateProfileWebService resume];
    
}


@end
