//
//  BTCreateNewPasswordViewController.h
//  BTBusinessApp
//
//  Created by VectoScalar on 12/8/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseViewController.h"

@class BTCreateNewPasswordViewController;

@protocol BTCreateNewPasswordViewControllerDelegate <NSObject>

- (void)userPressedBackButtonOnBTCreateNewPasswordViewController:(BTCreateNewPasswordViewController *)controller;

@end

@interface BTCreateNewPasswordViewController : BTBaseViewController
@property(nonatomic, assign) id<BTCreateNewPasswordViewControllerDelegate>delegate;
@property(nonatomic, copy) NSString *securityQuestion;
@property(nonatomic, copy) NSString *securityAnswer;
@property (strong,nonatomic) NSString *cugKeyFromSignInScreen;

+ (BTCreateNewPasswordViewController *)getBTCreateNewPasswordViewController;

@end
