//
//  DLMCreateNewPasswordScreen.h
//  BTBusinessApp
//
//  Created by VectoScalar on 12/8/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
#import "DLMUpdatePasswordScreen.h"

@class DLMCreateNewPasswordScreen;

@protocol DLMCreateNewPasswordScreenDelegate <NSObject>


@end

@interface DLMCreateNewPasswordScreen : DLMUpdatePasswordScreen

- (void)submitNewPassword:(NSString *)newPassword securityQuestion:(NSString *)question andSecurityAnswer:(NSString *)answer andGroupKeyFromSignInScreen:(NSString *)groupKeyFromSignInScreen;

@end
