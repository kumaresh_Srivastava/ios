//
//  BTCreateNewPasswordViewController.m
//  BTBusinessApp
//
//  Created by VectoScalar on 12/8/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTCreateNewPasswordViewController.h"
#import "PasswordInputView.h"
#import "PasswordValidationView.h"
#import "NSObject+APIResponseCheck.h"
#import "DLMCreateNewPasswordScreen.h"
#import "BTNeedHelpViewController.h"
#import "AppConstants.h"
#import "NLWebServiceError.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "AppManager.h"
#import "UIButton+BTButtonProperties.h"
#import "BTSecurityNumberViewController.h"
#import "OmnitureManager.h"

#define kInputViewHeight 110
#define kValidationViewHeight 66

#define kPasswordTag 111
#define kReEnterPasswordTag 112
#define kNavigationHeightWithStatusBar 64

#define kButtonHighlightedColor [BrandColours colourBackgroundBTPurplePrimaryColor]
#define kButtonDisabledColor [BrandColours colourBtNeutral60]


@interface BTCreateNewPasswordViewController ()<DLMUpdatePasswordScreenDelegate, PasswordInputViewDelegate>{
    
    PasswordInputView *_passwordView;
    PasswordInputView *_reEnterPasswordView;
    PasswordValidationView *_passwordValidationView;
    DLMCreateNewPasswordScreen *_screenViewModel;
    UIButton *_completeButton;
    BOOL _isUILoaded;
    
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


@end

@implementation BTCreateNewPasswordViewController

+ (BTCreateNewPasswordViewController *)getBTCreateNewPasswordViewController{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    BTCreateNewPasswordViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"BTCreateNewPasswordViewControllerID"];
    
    return controller;
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Set password";
    // self.navigationItem.hidesBackButton = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    _screenViewModel = [[DLMCreateNewPasswordScreen alloc] init];
    _screenViewModel.updatePasswordScreenDelegate = self;
    
    
    //Tap Guesture to hide keyboard
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.cancelsTouchesInView = NO;
    [_scrollView addGestureRecognizer:singleTap];
    
     [self trackContentViewedForPage:OMNIPAGE_SET_PASSOWRD andNotificationPopupDetails:OMNIPAGE_SET_NEW_PASSWORD];
    
}

- (void)trackContentViewedForPage:(NSString *)page andNotificationPopupDetails:(NSString *)notificationPopup
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = page;
    [data setValue:@"Not Logged In" forKey:kOmniLoginStatus];
    
    NSString *notificaiton = [NSString stringWithFormat:@"%@%@",STATE_PREFIX,notificationPopup];
    [data setValue:notificaiton forKey:kOmniContentViewed];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
    
}

- (void)didMoveToParentViewController:(UIViewController *)parent{
    
    [self.delegate userPressedBackButtonOnBTCreateNewPasswordViewController:self];
    
}




- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if(!_isUILoaded)
    {
        _isUILoaded = YES;
        [self configureUI];
    }
}


- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    
    //Adding observer for keyboard notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}



- (void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
    //Removing keyboard notification observer
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark- helper method
//Creating and adding UI components
- (void)configureUI{
    
    
    _passwordView = [self getPasswordInputView];
    _passwordView.tag = kPasswordTag;
    [_passwordView setTitle:@"Password"];
    //[_passwordView addHelpButton];
    _passwordView.delegate = self;
    [self.scrollView addSubview:_passwordView];
    
    
    _reEnterPasswordView = [self getPasswordInputView];
    _reEnterPasswordView.tag = kReEnterPasswordTag;
    [_reEnterPasswordView setTitle:@"Re-enter password"];
    //[_reEnterPasswordView removeRightView];
    _reEnterPasswordView.delegate = self;
    [self.scrollView addSubview:_reEnterPasswordView];
    
    
    _completeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_completeButton getBTButtonStyle];
    [_completeButton setTintColor:[UIColor whiteColor]];
    [_completeButton setTitle:@"Complete" forState:UIControlStateNormal];
    //[_completeButton setBackgroundColor:kButtonDisabledColor];
    _completeButton.titleLabel.font = [UIFont fontWithName:kBtFontBold size:15.0];
    [_completeButton setEnabled:NO];
    [_completeButton addTarget:self action:@selector(completeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    _completeButton.layer.cornerRadius = 5.0;
    //[_completeButton addTarget:self action:@selector(completeButtonTouchDown:) forControlEvents:UIControlEventTouchDown];
    
    
    [self.scrollView addSubview:_completeButton];
    
    
    
    _passwordValidationView = [[[NSBundle mainBundle] loadNibNamed:@"PasswordValidationView" owner:self options:nil] firstObject];
    [self.scrollView addSubview:_passwordValidationView];
    
    
    UILabel *headerLabel = [[UILabel alloc] init];
    [headerLabel setText:@"Secure your account"];
    [headerLabel setFont:[UIFont fontWithName:kBtFontRegular size:20.0]];
    [self.scrollView addSubview:headerLabel];
    
    headerLabel.frame = CGRectMake(20, 10, _scrollView.frame.size.width - 2*20, 50);
    
    
//    _passwordView.frame = CGRectMake(0,
//                                     10,
//                                     _scrollView.frame.size.width,
//                                     kInputViewHeight - 15);
    
    _passwordView.frame = CGRectMake(0,
                                     headerLabel.frame.origin.y + headerLabel.frame.size.height + 10,
                                     _scrollView.frame.size.width,
                                     kInputViewHeight - 15);
    
    _passwordValidationView.frame = CGRectMake(0,
                                               _passwordView.frame.origin.y + _passwordView.frame.size.height,
                                               _scrollView.frame.size.width,
                                               kValidationViewHeight);
    
    _reEnterPasswordView.frame = CGRectMake(0,
                                            _passwordValidationView.frame.origin.y + _passwordValidationView.frame.size.height,
                                            _scrollView.frame.size.width,
                                            kInputViewHeight );
    
    
    
    
    _completeButton.frame = CGRectMake(20,
                                       _reEnterPasswordView.frame.origin.y + _reEnterPasswordView.frame.size.height, _scrollView.frame.size.width - 2*20,
                                       50);
    
    
    
    
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width,
                                         _completeButton.frame.origin.y + _completeButton.frame.size.height);
    
}


- (PasswordInputView *)getPasswordInputView{
    
    return [[[NSBundle mainBundle] loadNibNamed:@"PasswordInputView" owner:self options:nil] firstObject];
    
}



- (void)updateUIAsPerValidations{
    
    //Update UI as per validations
    BOOL isPassWordVarified = NO;
    BOOL isReEnteredPassVarified = NO;
    
    if([_screenViewModel checkNewPasswordValidtyForText:[_passwordView getInputText]]){
        
        isPassWordVarified = YES;
    }
    
    NSString *enteredPassword = [_passwordView getInputText];
    
    if(enteredPassword && [enteredPassword validAndNotEmptyStringObject]){
        
        NSString *reEnteredPassWord = [_reEnterPasswordView getInputText];
        
        if(reEnteredPassWord && [reEnteredPassWord isEqualToString:enteredPassword]){
            
            
            isReEnteredPassVarified = YES;
        }
    }
    
    
    if(isPassWordVarified && isReEnteredPassVarified){
        
        _completeButton.enabled = YES;
        //_completeButton.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
    }
    else{
        
        _completeButton.enabled = NO;
        //_completeButton.backgroundColor = kButtonDisabledColor;
    }
    
    
}


- (void)showErrorAlertWithMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Message" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}


- (void)hideKeyboard{
    
    [_passwordView resignFirstResponderForTextfield];
    [_reEnterPasswordView resignFirstResponderForTextfield];
}

#pragma mark- action method

- (void)completeButtonPressed:(id)sender{
    
    [self hideKeyboard];
    
    //_completeButton.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
    
    if (![AppManager isInternetConnectionAvailable]) {
    
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kNoConnectionTitle message:kNoConnectionMessage preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        NSString *passwprd = [_passwordView getInputText];
        [_screenViewModel submitNewPassword:passwprd securityQuestion:self.securityQuestion andSecurityAnswer:self.securityAnswer andGroupKeyFromSignInScreen:self.cugKeyFromSignInScreen];
        
        self.networkRequestInProgress = YES;
    }
    
}


- (void)completeButtonTouchDown:(id)sender{
    
    _completeButton.backgroundColor = kButtonHighlightedColor;
    
}


#pragma mark- Guesture Method
- (void)handleTap:(id)guesture{
    
    [self hideKeyboard];
}



#pragma mark- Keyboard Notifications Method

- (void)keyboardDidShow:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0,
                                                  0.0,
                                                  kbRect.size.height + 15,
                                                  0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification *)notification
{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    [self.scrollView setContentOffset:CGPointMake(0, 0)];
}




#pragma mark - PasswordInputViewDelegate method

- (void)passwordInputView:(PasswordInputView *)view didChangeWithText:(NSString *)inputTextString{
    
    if(view.tag == kPasswordTag){
        
        [_passwordValidationView updateWithUdateModel:_screenViewModel andInputText:inputTextString];
        
    }
    else if(view.tag == kReEnterPasswordTag){
        
        
    }
    
    [self updateUIAsPerValidations];
}


- (void)needHelpButtonPressedOnPasswordInputView:(PasswordInputView *)view{
    
    
    BTNeedHelpViewController *helpViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CreatePasswordNeedHelp"];
    [self presentViewController:helpViewController animated:YES completion:nil];
    
}

- (void)navigateToHomeScreen
{
    [self dismissViewControllerAnimated:YES completion:nil];
    if (self.delegate && [self.delegate isKindOfClass:[BTSecurityNumberViewController class]]) {
        BTSecurityNumberViewController *parentVC = (BTSecurityNumberViewController*)self.delegate;
        if (parentVC.delegate && [parentVC.delegate respondsToSelector:@selector(successfullyUpdatedSecurityNumberOnSecurityNumberViewController:)]) {
            [parentVC.delegate successfullyUpdatedSecurityNumberOnSecurityNumberViewController:parentVC];
        }
    }
    
}


#pragma mark- DLM Model delegate

- (void)updateSuccessfullyFinishedByUpdatePasswordScreen:(DLMUpdatePasswordScreen *)updatePasswordScreen{
    
    
    self.networkRequestInProgress = NO;
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Password changed" message:@"Please enter your username and password again" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self navigateToLoginScreen];
    }];
    
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}


- (void)updatePasswordScreen:(DLMUpdatePasswordScreen *)updatePasswordScreen failedToSubmitUpdatedProfileDetailsWithWebServiceError:(NLWebServiceError *)error{
    
    
    self.networkRequestInProgress = NO;
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if([error.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {
        errorHandled = YES;
        
        switch (error.error.code)
        {
            case BTNetworkErrorCodeVordelParameterInvalid:
            {
                [self showErrorAlertWithMessage:kDefaultErrorMessage];
                break;
            }
                
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }
    
    if(errorHandled == NO)
    {
        [self showErrorAlertWithMessage:kDefaultErrorMessage];
    }
    
}


- (void)navigateToLoginScreen
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kPasswordLoginIntercepter];
     [[NSUserDefaults standardUserDefaults] removeObjectForKey:kTokenLoginIntercepter];
     [[NSUserDefaults standardUserDefaults] removeObjectForKey:kSMSessionLoginIntercepter];
     [[NSUserDefaults standardUserDefaults] removeObjectForKey:kGroupKeyLoginIntercepter];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:@"needToPrefillEmail"];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationAppNeedsLockDownWithLoginScreenWithoutAnyPrompt object:nil userInfo:dictionary];
}



@end
