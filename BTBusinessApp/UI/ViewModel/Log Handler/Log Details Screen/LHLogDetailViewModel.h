//
//  LogDetailModelView.h
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 04/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LHLogFileItem;
@class LHLogDetailViewModel;

@protocol LHLogDetailViewModelDelegate <NSObject>

- (void)logDetailViewModel:(LHLogDetailViewModel *)viewModel successfullyRefreshedLogString:(NSString *)logString;

- (void)logDetailViewModel:(LHLogDetailViewModel *)viewModel errorWhileRefreshingLogString:(NSString *)errorString;

@end

@interface LHLogDetailViewModel : NSObject {

}

@property (nonatomic, readonly) id <LHLogDetailViewModelDelegate> delegate;
@property (nonatomic, readonly) LHLogFileItem *logFileItem;

- (instancetype)initWithLogFileItem:(LHLogFileItem *)fileItem andDelegate:(id<LHLogDetailViewModelDelegate>)delegateObject;

- (void)refreshAndReturnLogString;

@end
