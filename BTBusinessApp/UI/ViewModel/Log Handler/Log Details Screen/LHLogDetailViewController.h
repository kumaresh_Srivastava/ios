//
//  LogDetailViewController.h
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 04/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LHLogFileItem;

@interface LHLogDetailViewController : UIViewController {

}

@property (nonatomic) LHLogFileItem *logFileItem;

@end
