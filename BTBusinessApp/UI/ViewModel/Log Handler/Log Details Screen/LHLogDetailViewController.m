//
//  LogDetailViewController.m
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 04/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "LHLogDetailViewController.h"
#import "LHLogDetailViewModel.h"
#import "LHLogFileItem.h"
#import "LHSingleLogShareMailComposerViewModel.h"

@interface LHLogDetailViewController () <LHLogDetailViewModelDelegate, MailViewModelDelegate> {

    LHSingleLogShareMailComposerViewModel *_mailComposerViewModel;
}

@property (nonatomic, weak) IBOutlet UIWebView *webView;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, readonly) LHLogDetailViewModel *viewModel;

@end

@implementation LHLogDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    _viewModel = [[LHLogDetailViewModel alloc] initWithLogFileItem:self.logFileItem andDelegate:self];

    // (hd) Setting the title of screen
    NSString *titleString = [NSString stringWithFormat:@"%@ %@", [self.viewModel.logFileItem dateComponentFromName], [self.viewModel.logFileItem timeComponentFromName]];
    self.title = titleString;

    // (hd) Asking for data
    [_viewModel refreshAndReturnLogString];
    [self.activityIndicator startAnimating];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)share:(id)sender
{
    DDLogInfo(@"ViewLog: User tapped share button from navigation bar on LHLogDetailViewController.");

    BOOL canMailBeSent = [MailComposerViewModel canEmailBeSentFromThisDevice];
    if(canMailBeSent)
    {
        _mailComposerViewModel = [[LHSingleLogShareMailComposerViewModel alloc] initWithLogFileItem:self.viewModel.logFileItem];
        _mailComposerViewModel.delegate = self;
        [_mailComposerViewModel presentMailComposer];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Error"
                                              message:@"Email cannot be sent from this device. Please configure a mail account on this device."
                                              preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

            [alertController dismissViewControllerAnimated:YES completion:^{

            }];

        }];

        [alertController addAction:ok];

        [self presentViewController:alertController animated:YES completion:^{

            DDLogInfo(@"ViewLog: Error alert presented to user because device cannot send email.");
        }];
    }
}

#pragma mark - UIWebViewDelegate Methods

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.activityIndicator stopAnimating];
    self.activityIndicator.hidden = YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.activityIndicator stopAnimating];
    self.activityIndicator.hidden = YES;

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", error] preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        [self dismissViewControllerAnimated:YES completion:^{

        }];
    }];

    [alert addAction:okAction];

    [self presentViewController:alert animated:YES completion:^{

    }];
}

#pragma mark - LHLogDetailViewModelDelegate Methods

- (void)logDetailViewModel:(LHLogDetailViewModel *)viewModel successfullyRefreshedLogString:(NSString *)logString
{
    NSString *htmlString = [NSString stringWithFormat:@"<html><body>%@</body></html>", logString];
    [self.webView loadHTMLString:htmlString baseURL:nil];
}

- (void)logDetailViewModel:(LHLogDetailViewModel *)viewModel errorWhileRefreshingLogString:(NSString *)errorString
{
    NSString *htmlString = [NSString stringWithFormat:@"<html><body>%@</body></html>", errorString];
    [self.webView loadHTMLString:htmlString baseURL:nil];
}

#pragma mark -
#pragma mark MailComposerViewModelDelegate Methods

- (UIViewController *)presentationViewControllerForPresentingMailComposerRequestedByMailComposerViewModel:(MailComposerViewModel *)viewModel
{
    return self;
}

- (void)mailComposerViewModel:(MailComposerViewModel *)viewModel finishedMailComposerJourneyWithResult:(MFMailComposeResult)result
{
    if(viewModel == _mailComposerViewModel)
    {
        _mailComposerViewModel.delegate = nil;
        _mailComposerViewModel = nil;
    }
}


@end
