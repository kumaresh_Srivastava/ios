//
//  LogDetailModelView.m
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 04/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "LHLogDetailViewModel.h"
#import "LHLogFileItem.h"
#import <CocoaLumberjack/DDFileLogger.h>
#import "LHLogFileManagerCompleteLogs.h"
#import "LHLogFileManagerImportantLogs.h"

@interface LHLogDetailViewModel () {

    LHLogFileItem *_logFileItem;

    __weak id <LHLogDetailViewModelDelegate> _delegate;
}

@end

@implementation LHLogDetailViewModel

- (instancetype)initWithLogFileItem:(LHLogFileItem *)fileItem andDelegate:(id<LHLogDetailViewModelDelegate>)delegateObject
{
    self = [super init];
    if(self)
    {
        _logFileItem = fileItem;
        _delegate = delegateObject;
    }
    return self;
}

#pragma mark -
#pragma mark Public Methods

- (void)refreshAndReturnLogString
{
    // (hd) Find the right/correct DDFileLogger
    DDFileLogger *fileLoggerToBeUsed = nil;
    NSArray *arrayOfAllFileLoggers = [DDLog allLoggers];

    switch (_logFileItem.logFileType)
    {
        case LHLogFileTypeImportant:
        {
            for(DDAbstractLogger *logger in arrayOfAllFileLoggers)
            {
                if([logger isKindOfClass:[DDFileLogger class]])
                {
                    if([[(DDFileLogger *)logger logFileManager] isKindOfClass:[LHLogFileManagerImportantLogs class]])
                    {
                        fileLoggerToBeUsed = (DDFileLogger *)logger;
                    }
                }
            }
            break;
        }

        case LHLogFileTypeComplete:
        {
            for(DDAbstractLogger *logger in arrayOfAllFileLoggers)
            {
                if([logger isKindOfClass:[DDFileLogger class]])
                {
                    if([[(DDFileLogger *)logger logFileManager] isKindOfClass:[LHLogFileManagerCompleteLogs class]])
                    {
                        fileLoggerToBeUsed = (DDFileLogger *)logger;
                    }
                }
            }
            break;
        }

        case LHLogFileTypeNone:
        {
            DDLogError(@"ViewLogs: Trying to load log data for a LHLogFileTypeNone type of LHLogFileItem with file name as %@", _logFileItem.ddLogFileInfo.fileName);
            break;
        }

        default:
        {
            DDLogError(@"ViewLogs: Trying to load log data for an unknown type of LHLogFileItem with file name as %@", _logFileItem.ddLogFileInfo.fileName);
            break;
        }

    }

    if(fileLoggerToBeUsed)
    {
        dispatch_async(fileLoggerToBeUsed.loggerQueue, ^{

            NSError *error = nil;

            NSString *logString = [NSString stringWithContentsOfFile:_logFileItem.ddLogFileInfo.filePath
                                                           encoding:NSUTF8StringEncoding
                                                              error:&error];

            dispatch_async(dispatch_get_main_queue(), ^{

                if(error)
                {
                    DDLogError(@"ViewLogs: Unable to load log data from log file with name %@ with an error as %@", _logFileItem.ddLogFileInfo.fileName, error);
                    [self.delegate logDetailViewModel:self errorWhileRefreshingLogString:@"Unable to load log data."];
                }
                else
                {
                    [self.delegate logDetailViewModel:self successfullyRefreshedLogString:logString];
                }
            });
        });
    }
    else
    {
        DDLogError(@"ViewLogs: Unable to find DDFileLogger object while trying to load log data for log file with name %@", _logFileItem.ddLogFileInfo.fileName);
        [self.delegate logDetailViewModel:self errorWhileRefreshingLogString:@"Unable to load log data."];
    }
}



@end
