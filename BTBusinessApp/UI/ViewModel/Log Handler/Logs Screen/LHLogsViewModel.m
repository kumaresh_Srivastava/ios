//
//  LogsModelView.m
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 04/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "LHLogsViewModel.h"
#import "LHLogFileItem.h"
#import "LHLogFileManagerCompleteLogs.h"
#import "LHLogFileManagerImportantLogs.h"
#import <CocoaLumberjack/DDFileLogger.h>
#import "LHLogFileItem.h"

@interface LHLogsViewModel () {

    __weak id <LHLogsViewModelDelegate> _delegate;
}

- (void)fetchAndDeliverLogsForLogsViewFilterType:(LHLogsViewFilterType)logsViewFilterType;
- (NSArray *)logFileInfoObjectsForCompleteLogs;
- (NSArray *)logFileInfoObjectsForImportantLogs;

@end

@implementation LHLogsViewModel

- (instancetype)initWithSelectedLogsViewFilterType:(LHLogsViewFilterType)logsViewFilterType andDelegate:(id<LHLogsViewModelDelegate>)delegateObject
{
    self = [super init];
    if(self)
    {
        _delegate = delegateObject;
        _selectedLogsViewFilterType = logsViewFilterType;
    }
    return self;
}

#pragma mark -
#pragma mark Public Methods

- (void)userChangedLogsViewFilterSelectionTo:(LHLogsViewFilterType)newLogsViewFilterType
{
    DDLogInfo(@"User changed selection of log filter to %ld", (long)newLogsViewFilterType);
    
    _selectedLogsViewFilterType = newLogsViewFilterType;
    [self fetchAndDeliverLogsForLogsViewFilterType:_selectedLogsViewFilterType];
}

- (void)refreshData
{
    [self fetchAndDeliverLogsForLogsViewFilterType:_selectedLogsViewFilterType];
}


#pragma mark -
#pragma mark Private Helper Methods

- (void)fetchAndDeliverLogsForLogsViewFilterType:(LHLogsViewFilterType)logsViewFilterType
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{

        NSArray *resultArray = nil;

        NSMutableArray *mutableUnsortedArrayOfDDLogFileInfoObjects = [NSMutableArray array];

        switch (logsViewFilterType)
        {
            case LHLogsViewFilterTypeAll:
            {
                NSArray *completeLogFileInfoObjects = [self logFileInfoObjectsForCompleteLogs];

                for(DDLogFileInfo *fileInfo in completeLogFileInfoObjects)
                {
                    [mutableUnsortedArrayOfDDLogFileInfoObjects addObject:fileInfo];
                }

                NSArray *importantLogFileInfoObjects = [self logFileInfoObjectsForImportantLogs];

                for(DDLogFileInfo *fileInfo in importantLogFileInfoObjects)
                {
                    [mutableUnsortedArrayOfDDLogFileInfoObjects addObject:fileInfo];
                }

                break;
            }

            case LHLogsViewFilterTypeImportant:
            {
                NSArray *importantLogFileInfoObjects = [self logFileInfoObjectsForImportantLogs];

                for(DDLogFileInfo *fileInfo in importantLogFileInfoObjects)
                {
                    [mutableUnsortedArrayOfDDLogFileInfoObjects addObject:fileInfo];
                }

                break;
            }

            case LHLogsViewFilterTypeComplete:
            {
                NSArray *completeLogFileInfoObjects = [self logFileInfoObjectsForCompleteLogs];

                for(DDLogFileInfo *fileInfo in completeLogFileInfoObjects)
                {
                    [mutableUnsortedArrayOfDDLogFileInfoObjects addObject:fileInfo];
                }

                break;
            }

            case LHLogsViewFilterTypeFavorties:
            {
                break;
            }

            default:
                break;
        }

        NSArray *sortedArrayOfDDLogFileInfoObjects = [mutableUnsortedArrayOfDDLogFileInfoObjects sortedArrayUsingSelector:@selector(reverseCompareByCreationDate:)];

        NSMutableArray *mutableResultArray = [NSMutableArray array];

        for(DDLogFileInfo *logFileInfo in sortedArrayOfDDLogFileInfoObjects)
        {
            LHLogFileItem *fileItem = [[LHLogFileItem alloc] initWithLogFileInfo:logFileInfo];
            if(fileItem.logFileType == LHLogsViewFilterTypeComplete || fileItem.logFileType == LHLogsViewFilterTypeImportant)
            {
                [mutableResultArray addObject:fileItem];
            }
        }

        resultArray = [NSArray arrayWithArray:mutableResultArray];

        dispatch_async(dispatch_get_main_queue(), ^{

            // (hd) Checking if the filer selection has not changed while we were fetching this data of logs.
            if(self.selectedLogsViewFilterType == logsViewFilterType)
            {
                _arrayOfLogsToDisplay = resultArray;
                [self.delegate dataFetchedAndUpdatedForCurrentlySelectedFilterByModelView:self];
            }
        });
    });

}

- (NSArray *)logFileInfoObjectsForCompleteLogs
{
    NSArray *resultArray = nil;

    // (hd) Find an active local FileLogger for complete logs
    NSArray *arrayOfLoggers = [DDLog allLoggers];
    DDFileLogger *localFileLogger = nil;
    for(DDAbstractLogger *logger in arrayOfLoggers)
    {
        if([logger isKindOfClass:[DDFileLogger class]])
        {
            if([[(DDFileLogger *)logger logFileManager] isKindOfClass:[LHLogFileManagerCompleteLogs class]])
            {
                localFileLogger = (DDFileLogger *)logger;
                break;
            }
        }
    }

    if(localFileLogger)
    {
        NSArray *arrayOfFileInfoObjects = [[(DDFileLogger *)localFileLogger logFileManager] unsortedLogFileInfos];
        resultArray = arrayOfFileInfoObjects;
    }
    else
    {
        DDLogError(@"ViewLogs: Unable to find any local DDFileLogger for complete logs while trying to fetch local complete type logs files.");
    }

    return resultArray;
}

- (NSArray *)logFileInfoObjectsForImportantLogs
{

    NSArray *resultArray = nil;

    // (hd) Find an active local FileLogger for important logs
    NSArray *arrayOfLoggers = [DDLog allLoggers];
    DDFileLogger *localFileLogger = nil;
    for(DDAbstractLogger *logger in arrayOfLoggers)
    {
        if([logger isKindOfClass:[DDFileLogger class]])
        {
            if([[(DDFileLogger *)logger logFileManager] isKindOfClass:[LHLogFileManagerImportantLogs class]])
            {
                localFileLogger = (DDFileLogger *)logger;
                break;
            }
        }
    }

    if(localFileLogger)
    {
        NSArray *arrayOfFileInfoObjects = [[(DDFileLogger *)localFileLogger logFileManager] unsortedLogFileInfos];
        resultArray = arrayOfFileInfoObjects;
    }
    else
    {
        DDLogError(@"ViewLogs: Unable to find any local DDFileLogger for important logs while trying to fetch local important type logs files.");
    }

    return resultArray;
}

@end
