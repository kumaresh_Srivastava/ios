//
//  LogsModelView.h
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 04/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LHLogsViewModel;

typedef NS_ENUM(NSInteger, LHLogsViewFilterType) {
    LHLogsViewFilterTypeAll,
    LHLogsViewFilterTypeImportant,
    LHLogsViewFilterTypeComplete,
    LHLogsViewFilterTypeFavorties
};

@protocol LHLogsViewModelDelegate <NSObject>

- (void)dataFetchedAndUpdatedForCurrentlySelectedFilterByModelView:(LHLogsViewModel *)viewModel;

@end

@interface LHLogsViewModel : NSObject {

}

@property (nonatomic, readonly) LHLogsViewFilterType selectedLogsViewFilterType;
@property (nonatomic, readonly) NSArray *arrayOfLogsToDisplay;
@property (nonatomic, readonly) id <LHLogsViewModelDelegate> delegate;

- (instancetype)initWithSelectedLogsViewFilterType:(LHLogsViewFilterType)logsViewFilterType andDelegate:(id<LHLogsViewModelDelegate>)delegateObject;

- (void)userChangedLogsViewFilterSelectionTo:(LHLogsViewFilterType)newLogsViewFilterType;

- (void)refreshData;

@end
