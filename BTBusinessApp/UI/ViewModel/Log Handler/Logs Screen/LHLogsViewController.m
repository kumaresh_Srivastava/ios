//
//  LogsViewController.m
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 04/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "LHLogsViewController.h"
#import "LHLogsViewModel.h"
#import "LHLogFileItem.h"
#import "LHLogsTableViewCell.h"
#import "LHLogDetailViewController.h"
#import "LHSingleLogShareMailComposerViewModel.h"
#import "LHMultipleLogShareMailComposerViewModel.h"

@interface LHLogsViewController () <LHLogsViewModelDelegate, LHLogsTableViewCellDelegate, MailViewModelDelegate> {

    MailComposerViewModel *_mailComposerViewModel;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic, readonly) LHLogsViewModel *viewModel;

@end

@implementation LHLogsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    _viewModel = [[LHLogsViewModel alloc] initWithSelectedLogsViewFilterType:LHLogsViewFilterTypeAll andDelegate:self];
    [_viewModel refreshData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Action Methods

- (IBAction)selectionChangedOnFilterSegmentedControl:(id)sender
{
    DDLogInfo(@"ViewLog: User changed filter on LHLogsViewController.");

    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    [self.viewModel userChangedLogsViewFilterSelectionTo:segmentedControl.selectedSegmentIndex];
}

- (IBAction)done:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)share:(id)sender
{
    DDLogInfo(@"ViewLog: User tapped share button from navigation bar on LHLogsViewController.");

    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Share Logs"
                                          message:@"Please choose an option"
                                          preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction *shareAllVisibleLogs = [UIAlertAction actionWithTitle:@"Share All Visible Logs" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        DDLogInfo(@"ViewLog: User decided to share all visible logs on LHLogsViewController");


        BOOL canMailBeSent = [MailComposerViewModel canEmailBeSentFromThisDevice];
        if(canMailBeSent)
        {
            _mailComposerViewModel = [[LHMultipleLogShareMailComposerViewModel alloc] initWithArrayOfLogFileItems:self.viewModel.arrayOfLogsToDisplay];
            _mailComposerViewModel.delegate = self;
            [(LHMultipleLogShareMailComposerViewModel *)_mailComposerViewModel presentMailComposer];
        }
        else
        {
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Error"
                                                  message:@"Email cannot be sent from this device. Please configure a mail account on this device."
                                                  preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

                [alertController dismissViewControllerAnimated:YES completion:^{

                }];

            }];

            [alertController addAction:ok];

            [self presentViewController:alertController animated:YES completion:^{

                DDLogInfo(@"ViewLog: Error alert presented to user because device cannot send email.");
            }];
        }
    }];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {

        DDLogInfo(@"ViewLog: User cancelled share on LHLogsViewController");

        [alertController dismissViewControllerAnimated:YES completion:^{

        }];
    }];

    [alertController addAction:shareAllVisibleLogs];
    [alertController addAction:cancelAction];
    alertController.popoverPresentationController.sourceView = self.view;
    alertController.popoverPresentationController.sourceRect = self.view.frame;
    alertController.popoverPresentationController.canOverlapSourceViewRect = NO;

    [self presentViewController:alertController animated:YES completion:^{

    }];
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.viewModel.arrayOfLogsToDisplay count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = nil;

    LHLogFileItem *fileItem = [self.viewModel.arrayOfLogsToDisplay objectAtIndex:indexPath.row];
    switch (fileItem.logFileType)
    {
        case LHLogFileTypeNone:
        {
            NSAssert(NO, @"The app is trying to show a log file which has logFileType of LHLogFileTypeNone.");
            break;
        }

        case LHLogFileTypeComplete:
        {
            cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"LogCellComplete" forIndexPath:indexPath];
            break;
        }

        case LHLogFileTypeImportant:
        {
            cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"LogCellImportant" forIndexPath:indexPath];
            break;
        }

        default:
        {
            NSAssert(NO, @"The app is trying to show a log file which has unknown value of logFileType.");
            break;
        }
    }

    [(LHLogsTableViewCell *)cell updateCellWithLogFileItem:fileItem];
    return cell;
}



#pragma mark - LHLogsviewModelDelegate Methods

- (void)dataFetchedAndUpdatedForCurrentlySelectedFilterByModelView:(LHLogsViewModel *)viewModel
{
    [self.tableView reloadData];
}


#pragma mark - LHLogsTableViewCellDelegate Methods

- (void)userTappedShareButtonOnLogsTableViewCell:(LHLogsTableViewCell *)cell
{
    NSIndexPath *indexPathOfCell = [self.tableView indexPathForCell:cell];
    LHLogFileItem *fileItem = [self.viewModel.arrayOfLogsToDisplay objectAtIndex:indexPathOfCell.row];

    DDLogInfo(@"ViewLog: User tapped share button on cell for log file with name %@.", fileItem.ddLogFileInfo.fileName);

    _mailComposerViewModel = [[LHSingleLogShareMailComposerViewModel alloc] initWithLogFileItem:fileItem];
    _mailComposerViewModel.delegate = self;
    [(LHSingleLogShareMailComposerViewModel *)_mailComposerViewModel presentMailComposer];
}

- (void)userTappedFavoritesButtonOnLogsTableViewCell:(LHLogsTableViewCell *)cell
{
    NSIndexPath *indexPathOfCell = [self.tableView indexPathForCell:cell];
    LHLogFileItem *fileItem = [self.viewModel.arrayOfLogsToDisplay objectAtIndex:indexPathOfCell.row];

    DDLogInfo(@"ViewLog: User tapped favroite button on cell for log file with name %@.", fileItem.ddLogFileInfo.fileName);


    UIAlertController *alert =   [UIAlertController
                                  alertControllerWithTitle:@"Opps"
                                  message:@"Patience my friend!!! We have not implemented this feature yet."
                                  preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];

                         }];
    [alert addAction:ok];

    [self presentViewController:alert animated:YES completion:nil];
}

- (void)userTappedDeleteButtonOnLogsTableViewCell:(LHLogsTableViewCell *)cell
{
    NSIndexPath *indexPathOfCell = [self.tableView indexPathForCell:cell];
    LHLogFileItem *fileItem = [self.viewModel.arrayOfLogsToDisplay objectAtIndex:indexPathOfCell.row];


    DDLogInfo(@"ViewLog: User tapped delete button on cell for log file with name %@.", fileItem.ddLogFileInfo.fileName);

    UIAlertController *alert =   [UIAlertController
                                  alertControllerWithTitle:@"Opps"
                                  message:@"You think you can delete this log even though we are still writing the code for it :)"
                                  preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];

                         }];
    [alert addAction:ok];

    [self presentViewController:alert animated:YES completion:nil];

}

- (void)userTappedViewButtonOnLogsTableViewCell:(LHLogsTableViewCell *)cell
{
    NSIndexPath *indexPathOfCell = [self.tableView indexPathForCell:cell];
    LHLogFileItem *fileItem = [self.viewModel.arrayOfLogsToDisplay objectAtIndex:indexPathOfCell.row];

    DDLogInfo(@"ViewLog: User tapped view button on cell for log file with name %@.", fileItem.ddLogFileInfo.fileName);

    [self performSegueWithIdentifier:@"showLogDetail" sender:fileItem];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.

    if([segue.identifier isEqualToString:@"showLogDetail"])
    {
        LHLogDetailViewController *viewController = segue.destinationViewController;
        viewController.logFileItem = (LHLogFileItem *)sender;
    }
}


#pragma mark -
#pragma mark MailviewModelDelegate Methods


- (UIViewController *)presentationViewControllerForPresentingMailComposerRequestedByMailComposerViewModel:(MailComposerViewModel *)viewModel
{
    return self;
}

- (void)mailComposerViewModel:(MailComposerViewModel *)viewModel finishedMailComposerJourneyWithResult:(MFMailComposeResult)result
{
    if(viewModel == _mailComposerViewModel)
    {
        _mailComposerViewModel.delegate = nil;
        _mailComposerViewModel = nil;
    }
}

@end
