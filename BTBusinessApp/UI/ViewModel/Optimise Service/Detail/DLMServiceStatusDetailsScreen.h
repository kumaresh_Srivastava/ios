//
//  DLMServiceStatusDetailsScreen.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 2/22/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "DLMObject.h"
#import "BTLiveChatAvailabilityChecker.h"

@class DLMServiceStatusDetailsScreen;
@class BTServiceStatusDetail;
@class NLWebServiceError;

typedef NS_ENUM(NSInteger, SSActionType) {
    SSActionTypeReportAFault,
    SSActionTypePopularFAQs,
    SSActionTypeReportFaultToTrackProgress,
    SSActionTypeRestartHub,
    SSActionTypeHubHealthcheck
};

typedef NS_ENUM(NSInteger, SSIssueType) {
    SSIssueTypeNone,
    SSIssueTypeOngoingIssue,
    SSIssueTypeRecentlyResolvedIssue
};

typedef NS_ENUM(NSInteger, SS4GAssureStatus) {
    SS4GAssureUnavailable,
    SS4GAssureNewHubNeeded,
    SS4GAssureOn,
    SS4GAssureReady,
    SS4GAssureAvailable
};

@protocol DLMServiceStatusDetailsScreenDelegate <NSObject>

- (void)successfullyFetchedServiceStatusDetailsScreen:(DLMServiceStatusDetailsScreen *)serviceStatusDetailScreen;

- (void)serviceStatusDetailsScreen:(DLMServiceStatusDetailsScreen *)serviceStatusDetailScreen failedToFetchServiceDetailWithWebServiceError:(NLWebServiceError *)webServiceError;

- (void)successfullyFetchedWorkflowStatus:(DLMServiceStatusDetailsScreen*)serviceStatusDetailScreen;

- (void)serviceStatusDetailsScreen:(DLMServiceStatusDetailsScreen *)serviceStatusDetailScreen failedToFetchWorkFlowStatusWithWebServiceError:(NLWebServiceError *)webServiceError;

- (void)liveChatAvailableForFaultWithScreen:(DLMServiceStatusDetailsScreen *)serviceStatusDetailsScreen;
- (void)serviceStatusDetailsScreen:(DLMServiceStatusDetailsScreen *)serviceStatusDetailsScreen liveChatCurrentlyNotAvailableWithSpecificCategory:(NSArray *)specificCategory generalCategory:(NSArray *)generalCategory andCurrentTime:(NSString *)currentTime;
- (void)serviceStatusDetailsScreen:(DLMServiceStatusDetailsScreen *)serviceStatusDetailsScreen failedToCheckLiveChatAvailabilityWithWebServiceError:(NLWebServiceError *)error;

-(void)displayNetworkStatusIssue;

- (NSString*)pageNameForOmnitureTracking;

@end

@interface DLMServiceStatusDetailsScreen : DLMObject {
    
}

@property (nonatomic, weak) id <DLMServiceStatusDetailsScreenDelegate> serviceStatusDetailScreenDelegate;
@property (nonatomic, strong) NSArray *arrayOfActionList;
@property (nonatomic, strong) BTServiceStatusDetail *serviceStatusDetail;
@property (nonatomic, strong) NSString *takeActionTitleString;
@property (nonatomic, strong) NSString *issueTitleString;
@property (nonatomic, strong) NSString *moreString;
@property (nonatomic, assign) SSIssueType currentIssueType;
@property (nonatomic, assign) SS4GAssureStatus current4GAssureStatus;
@property (nonatomic, strong) NSMutableArray *resilientHubAssets;
@property (nonatomic, assign) NSDictionary *hubDetails;
@property (nonatomic, assign) NSString *postcode;
@property (nonatomic, assign) BOOL forSpeedTest;

- (void)createActionItemsForCurrentService;
- (void)fetchServiceStatusDetailWithServiceID:(NSString *)serviceID andServiceType:(NSString *)serviceType andIsNeedToPointModelAAlways:(BOOL)pointModelAAlways;
- (void)fetchResilientHubAsset:(NSString*)serviceID;
- (void)fetchEEOAuthToken;
- (void)cancelServiceStatusDetailAPI;
- (NSString *)getImageNameForCurrentService;
- (NSString *)getAffectedAreaTextForPotalCode:(NSString *)postalCode;

- (void)cancelChatAvailabilityCheckerAPI;
- (void)checkForLiveChatAvailibilityForFault;

@end
