//
//  DLMServiceStatusDetailsScreen.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 2/22/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "DLMServiceStatusDetailsScreen.h"
#import "NLServiceStatusDetailWebService.h"
#import "BTServiceStatusDetail.h"
#import "NLGetResilientHubAsset.h"
#import "NLGetHubStatus.h"
#import "BTResilientHubAsset.h"
#import "NLCheckWorkflowStatus.h"
#import "NLEEOAuthWebService.h"
#import "NLEEGeocodingWebService.h"
#import "NLEENetworkStatusWebService.h"
#import "NLVordelWebServiceError.h"

#define kOngoingIssueString @"Ongoing issue"
#define kNoIssuesFoundString @"No issues found"
#define kRecentlyResolvedIssueString @"Recently resolved issue"

#define kTakeActionString @"Useful links"
#define kStillHavingProblemsString @"How to fix problems"
#define k4GIssueString @"4G service status"
#define kMoreString @"More"

@interface DLMServiceStatusDetailsScreen()<NLServiceStatusDetailWebServiceDelegate, BTLiveChatAvailabilityCheckerDelegate, NLGetResilientHubAssetDelegate, NLGetHubStatusDelegate, NLCheckWorkflowStatusDelegate, NLEEOAuthWebServiceDelegate, NLEEGeocodingWebServiceDelegate, NLEENetworkStatusWebServiceDelegate> {
    
    BTLiveChatAvailabilityChecker *_liveChatAvailabityChecker;
}

@property (nonatomic) NLServiceStatusDetailWebService *getServiceDetailWebService;
@property (nonatomic) NLGetResilientHubAsset *getResilientHubAsset;
@property (nonatomic, strong) NLGetHubStatus *getHubStatus;
@property (nonatomic) NLCheckWorkflowStatus *checkWorkflowStatus;
@property (nonatomic) NLEEOAuthWebService *eeOAuthWebService;
@property (nonatomic) NLEEGeocodingWebService *eeGeocodingWebservice;
@property (nonatomic) NLEENetworkStatusWebService *eeNetworkStatusWebservice;
@property (nonatomic) BTAuthenticationToken *eeAuthToken;

@end

@implementation DLMServiceStatusDetailsScreen

- (NSMutableArray *)resilientHubAssets
{
    if (!_resilientHubAssets) {
        _resilientHubAssets = [NSMutableArray new];
    }
    return _resilientHubAssets;
}

#pragma mark- public methods

- (void)fetchServiceStatusDetailWithServiceID:(NSString *)serviceID andServiceType:(NSString *)serviceType andIsNeedToPointModelAAlways:(BOOL)pointModelAAlways
{
    self.getServiceDetailWebService.serviceStatusWebServiceDelegate = nil;
    self.getServiceDetailWebService = [[NLServiceStatusDetailWebService alloc] initWithServiceID:serviceID andServiceType:serviceType andIsNeedToPointModelAAlways:pointModelAAlways];
    self.getServiceDetailWebService.serviceStatusWebServiceDelegate = self;
    [self.getServiceDetailWebService resume];
}

-(void)fetchResilientHubAsset:(NSString*)serviceID {
    self.getServiceDetailWebService.serviceStatusWebServiceDelegate = nil;
    BTServiceStatusDetail *dummyDetails = [[BTServiceStatusDetail alloc] initWithServiceStatusDetailResponse:@{@"Product":@"BB",@"MSOFlag":@"No issues found"}];
    if (!self.getServiceDetailWebService) {
        self.getServiceDetailWebService = [[NLServiceStatusDetailWebService alloc] init];
    }
    [self getServiceStatusDetailWebService:self.getServiceDetailWebService successfullyFetchedServiceStatusDetail:dummyDetails];
    self.getResilientHubAsset = [[NLGetResilientHubAsset alloc] initWithServiceID:serviceID];
    self.getResilientHubAsset.getResilientHubAssetDelegate = self;
    [self.getResilientHubAsset resume];
}

-(void) fetchHubStatusExecutionId:(NSString*)hubSerialNumber {
    self.getHubStatus = [[NLGetHubStatus alloc] initWithHubSerialNumber:hubSerialNumber];
    self.getHubStatus.getHubStatusDelegate = self;
    [self.getHubStatus resume];
}

-(void)fetchEEOAuthToken {
    self.eeOAuthWebService = [[NLEEOAuthWebService alloc] init];
    self.eeOAuthWebService.eeOAuthServiceDelegate = self;
    [self.eeOAuthWebService resume];
}

-(void)fetchEENationalNetworkStatus:(BTAuthenticationToken*)token {
    self.eeNetworkStatusWebservice = [[NLEENetworkStatusWebService alloc] initWithToken:token];
    self.eeNetworkStatusWebservice.eeNetworkStatusServiceDelegate = self;
    [self.eeNetworkStatusWebservice resume];
}

-(void)fetchEENetworkStatus:(BTAuthenticationToken*)token latitude:(NSString*)latitude andLongitude:(NSString*)longitude {
    self.eeNetworkStatusWebservice = [[NLEENetworkStatusWebService alloc] initWithToken:token lat:latitude andLong:longitude];
    self.eeNetworkStatusWebservice.eeNetworkStatusServiceDelegate = self;
    [self.eeNetworkStatusWebservice resume];
}

-(void)fetchEEGeocode:(BTAuthenticationToken*)token {
    self.eeGeocodingWebservice = [[NLEEGeocodingWebService alloc] initWithToken:token andPostcode:_postcode];
    self.eeGeocodingWebservice.eeGeocodingServiceDelegate = self;
    [self.eeGeocodingWebservice resume];
}

- (void)checkForLiveChatAvailibilityForFault
{
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = [[BTLiveChatAvailabilityChecker alloc] init];
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = self;
    [_liveChatAvailabityChecker getLiveChatAvailableSlots];
}

- (void)createActionItemsForCurrentService
{
    NSMutableArray *arrayOfActionsList = [NSMutableArray array];
    
    NSDictionary *actionData1 = [NSDictionary dictionaryWithObjectsAndKeys:@"Report a fault", @"title", [NSNumber numberWithInteger:SSActionTypeReportAFault], @"type", nil];
    NSDictionary *actionData2 = [NSDictionary dictionaryWithObjectsAndKeys:@"Browse popular FAQs", @"title", [NSNumber numberWithInteger:SSActionTypePopularFAQs], @"type", nil];
    NSDictionary *actionData3 = [NSDictionary dictionaryWithObjectsAndKeys:@"Report fault to track progress", @"title", [NSNumber numberWithInteger:SSActionTypeReportFaultToTrackProgress], @"type", nil];
    NSDictionary *actionData4 = [NSDictionary dictionaryWithObjectsAndKeys:@"Restart this hub", @"title", [NSNumber numberWithInteger:SSActionTypeRestartHub], @"type", nil];
    NSDictionary *actionData5 = [NSDictionary dictionaryWithObjectsAndKeys:@"Hub healthcheck", @"title", [NSNumber numberWithInteger:SSActionTypeHubHealthcheck], @"type", nil];
    
    _issueTitleString = k4GIssueString;
    _moreString = kMoreString;
    
    if ([_serviceStatusDetail.product isEqualToString:@"BB"]) {
        [arrayOfActionsList addObject:actionData5];
    }
    
    if( [_serviceStatusDetail.product caseInsensitiveCompare:@"CLOUD"] == NSOrderedSame ) {
        [arrayOfActionsList addObject:actionData5];
    }
    
    if ([_serviceStatusDetail.mSOFlag isEqualToString:kNoIssuesFoundString])
    {
        _currentIssueType = SSIssueTypeNone;
        _takeActionTitleString = kStillHavingProblemsString;
        [arrayOfActionsList addObject:actionData2];
        [arrayOfActionsList addObject:actionData1];
    }
    else if ([_serviceStatusDetail.mSOFlag isEqualToString:kOngoingIssueString])
    {
        _currentIssueType = SSIssueTypeOngoingIssue;
        _takeActionTitleString = kStillHavingProblemsString;
       
        if( [_serviceStatusDetail.product caseInsensitiveCompare:@"CLOUD"] == NSOrderedSame ) {
            [arrayOfActionsList addObject:actionData2];
            [arrayOfActionsList addObject:actionData1];
        }
        else {
            [arrayOfActionsList addObject:actionData2];
            [arrayOfActionsList addObject:actionData3];
        }
        
        if ([_serviceStatusDetail.product isEqualToString:@"BB"])
        {
            //[arrayOfActionsList addObject:actionData4];
        }
    }
    else if ([_serviceStatusDetail.mSOFlag isEqualToString:kRecentlyResolvedIssueString])
    {
        _currentIssueType = SSIssueTypeRecentlyResolvedIssue;
        _takeActionTitleString = kStillHavingProblemsString;
        [arrayOfActionsList addObject:actionData2];
        [arrayOfActionsList addObject:actionData1];

    }
    
    _arrayOfActionList = [NSArray arrayWithArray:arrayOfActionsList];
}

- (void)cancelServiceStatusDetailAPI{
    
    self.getServiceDetailWebService.serviceStatusWebServiceDelegate = nil;
    [self.getServiceDetailWebService cancel];
    self.getServiceDetailWebService = nil;
}

- (void)cancelChatAvailabilityCheckerAPI
{
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    [_liveChatAvailabityChecker cancel];
    _liveChatAvailabityChecker = nil;
}

- (NSString *)getImageNameForCurrentService
{
    if ([_serviceStatusDetail.product isEqualToString:@"BB"])
    {
        if (_currentIssueType == SSIssueTypeNone || _currentIssueType == SSIssueTypeRecentlyResolvedIssue)
        {
            return @"broadband_no_issue";
        }
        else
        {
            return @"broadband_issue";
        }
    }
    else if ([_serviceStatusDetail.product isEqualToString:@"PSTN"])
    {
        if (_currentIssueType == SSIssueTypeNone || _currentIssueType == SSIssueTypeRecentlyResolvedIssue)
        {
            return @"pstn_no_issue";
        }
        else
        {
            return @"pstn_issue";
        }
    }
    return nil;
}


- (NSString *)getAffectedAreaTextForPotalCode:(NSString *)postalCode{
    
    NSString *affectedAreaText = @"";
    
    if(_currentIssueType == SSIssueTypeNone){
        if ([_serviceStatusDetail.product isEqualToString:@"BB"]){
            affectedAreaText = @"Our checks show no broadband problems in your area.";
            if(self.resilientHubAssets && [self.resilientHubAssets isKindOfClass:[NSArray class]] && self.resilientHubAssets.count) {
                BTResilientHubAsset *asset = self.resilientHubAssets[0];
                NSDictionary *assetAttributes = [asset attributes];
                if([assetAttributes valueForKey:@"attribute"]) {
                    NSDictionary *resilientFlag = [assetAttributes valueForKey:@"attribute"];
                    if([resilientFlag valueForKey:@"value"]) {
                        if( [[resilientFlag valueForKey:@"value"]isEqualToString:@"Y"]) {
                            affectedAreaText = @"Our checks show no broadband problems in your area. Make sure 4G Assure is plugged in to stay connected automatically if broadband fails.";
                        }
                    }
                }
            } else {
                affectedAreaText = @"Our checks show no broadband problems in your area.";
            }
        } else {
            affectedAreaText = @"Our checks did not find any issues affecting the location of this service.";//@"Our checks show no problems in your area.";
        }
    }
    else if (_currentIssueType == SSIssueTypeOngoingIssue){
        if ([_serviceStatusDetail.product isEqualToString:@"BB"]){
            affectedAreaText = [NSString stringWithFormat:@"We\'re really sorry but we\'ve got a problem at the moment in the %@, which means that some of our customers will be having trouble getting online. We\'re trying to fix the problem as quickly as we can.", postalCode];
        }
        else{
            //PSTN
            affectedAreaText = [NSString stringWithFormat:@"We\'re really sorry but we\'ve got a problem at the moment in the %@, which means that some of our customers will be having trouble with their telephone line. We\'re trying to fix the problem as quickly as we can.", postalCode];
        }
    }
    else if(_currentIssueType == SSIssueTypeRecentlyResolvedIssue){
        
        affectedAreaText = [NSString stringWithFormat:@"We\'ve fixed the problem in the %@. If your broadband or BT Infinity was also down and your connection has not come back on, try restarting your modem or router.", postalCode];
    }
    
    return affectedAreaText;
}

-(NSString *)getDetailTextWithHubDetails:(NSDictionary*)hubDetails andAssets:(NSDictionary*)assets {
    NSString *assureText = @"Our checks show no broadband problems in your area.";
    
    if (_current4GAssureStatus == SS4GAssureUnavailable) {
        assureText = @"We\'re really sorry, broadband and 4G are not available right now. We\'re working to put things right.";
    } else if (_current4GAssureStatus == SS4GAssureNewHubNeeded) {
        
    } else if (_current4GAssureStatus == SS4GAssureOn) {
        assureText = @"4G Assure is keeping you online. When your broadband's up and running, we'll switch to it automatically.";
    } else if (_current4GAssureStatus == SS4GAssureReady) {
        assureText = @"Our checks show no broadband problems in your area. Make sure 4G Assure is plugged in to stay connected automatically if broadband fails.";
    }
    
    return assureText;
}



#pragma mark- NLServiceStatusDetailWebServiceDelegate methods

- (void)getServiceStatusDetailWebService:(NLServiceStatusDetailWebService *)webService successfullyFetchedServiceStatusDetail:(BTServiceStatusDetail *)serviceStatusDetail
{
    
    if(webService == self.getServiceDetailWebService) {
        
        _serviceStatusDetail = serviceStatusDetail;
        
        [self createActionItemsForCurrentService];
        
        self.getServiceDetailWebService.serviceStatusWebServiceDelegate = nil;
        self.getServiceDetailWebService = nil;
        
        [self.serviceStatusDetailScreenDelegate successfullyFetchedServiceStatusDetailsScreen:self];
    }
    else {
        DDLogError(@"This delegate method gets called because of success of an object of NLServiceStatusDetailWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLServiceStatusDetailWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
    
    
    
}

- (void)getServiceStatusDetailWebService:(NLServiceStatusDetailWebService *)webService failedToFetchServiceStatusDetailWithWebServiceError:(NLWebServiceError *)webServiceError{
    
    
    if(webService == self.getServiceDetailWebService) {
        
        self.getServiceDetailWebService.serviceStatusWebServiceDelegate = nil;
        self.getServiceDetailWebService = nil;
        
        [self.serviceStatusDetailScreenDelegate serviceStatusDetailsScreen:self failedToFetchServiceDetailWithWebServiceError:webServiceError];
    }
    else {
        
        DDLogError(@"This delegate method gets called because of failure of an object of NLServiceStatusDetailWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLServiceStatusDetailWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
    
}

#pragma mark - BTLiveChatAvailabilityCheckerDelegate Methods
- (void)liveChatAvailabilityChecker:(BTLiveChatAvailabilityChecker *)liveChatAvailabilityChecker successfullyFetchedSlotsWithGeneralCategory:(NSArray *)generalCategoryArray specificeCategory:(NSArray *)specificCategory andCurrentTime:(NSString *)currentTime
{
    BOOL chatAvailable = [BTLiveChatAvailabilityChecker checkAvailabilityWithSpecificCategory:specificCategory currentTime:currentTime andChatType:BTChatTypeFault];
    
    if (chatAvailable)
    {
        [self.serviceStatusDetailScreenDelegate liveChatAvailableForFaultWithScreen:self];
    }
    else
    {
        [self.serviceStatusDetailScreenDelegate serviceStatusDetailsScreen:self liveChatCurrentlyNotAvailableWithSpecificCategory:specificCategory generalCategory:generalCategoryArray andCurrentTime:currentTime];
    }
    
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = nil;
}

- (void)liveChatAvailabilityChecker:(BTLiveChatAvailabilityChecker *)liveChatAvailabilityChecker failedToFetchSlotsWithWebServiceError:(NLWebServiceError *)webServiceError
{
    [self.serviceStatusDetailScreenDelegate serviceStatusDetailsScreen:self failedToCheckLiveChatAvailabilityWithWebServiceError:webServiceError];
    
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = nil;
}

#pragma mark - NLGetResilientHubAsset Delegate
- (void)getResilientHubAsset:(NLGetResilientHubAsset *)webService successfullyFetchedAssets:(NSMutableArray*)assets {
    [self.resilientHubAssets removeAllObjects];
    //[self createActionItemsForCurrentService];
    if(assets && assets.count) {
        [self.resilientHubAssets addObjectsFromArray:assets];
        BTResilientHubAsset *asset = (BTResilientHubAsset*) assets[0];
        NSString *serialNum = [asset hubSerialNumber];
        if (serialNum && ![serialNum isEqualToString:@""] && ![serialNum isKindOfClass:[NSNull class]]) {
            [self fetchHubStatusExecutionId:[asset hubSerialNumber]];
        }
    } else {
        [self.serviceStatusDetailScreenDelegate serviceStatusDetailsScreen:self failedToFetchServiceDetailWithWebServiceError:nil];
    }
}

- (void)getResilientHubAsset:(NLGetResilientHubAsset *)webService failedToFetchAssetWithWebServiceError: (NLWebServiceError *)error
{
    [self.resilientHubAssets removeAllObjects];
    NSString *errString = @"";
    if ([error isKindOfClass:[NLVordelWebServiceError class]]) {
        NLVordelWebServiceError *vordelError = (NLVordelWebServiceError*)error;
        errString = [NSString stringWithFormat:@"%@:%@",vordelError.errorCode,vordelError.errorDescription];
    } else {
        errString = [NSString stringWithFormat:@"%li",(long)error.error.code];
        if ([error.error.userInfo objectForKey:NSLocalizedDescriptionKey]) {
            NSString *info = [error.error.userInfo objectForKey:NSLocalizedDescriptionKey];
            errString = [errString stringByAppendingString:[NSString stringWithFormat:@":%@",info]];
        }
    }
    [AppManager trackError:errString FromAPI:webService.friendlyName OnPage:[self.serviceStatusDetailScreenDelegate pageNameForOmnitureTracking]];
    
    [self.serviceStatusDetailScreenDelegate serviceStatusDetailsScreen:self failedToFetchServiceDetailWithWebServiceError:error];
}

# pragma mark - GetHubStatus Delegate

- (void)getHubStatus:(NLGetHubStatus *)webService failedToFetchHubStatusWithWebServiceError:(NLWebServiceError *)error
{
    NSString *errString = @"";
    if ([error isKindOfClass:[NLVordelWebServiceError class]]) {
        NLVordelWebServiceError *vordelError = (NLVordelWebServiceError*)error;
        errString = [NSString stringWithFormat:@"%@:%@",vordelError.errorCode,vordelError.errorDescription];
    } else {
        errString = [NSString stringWithFormat:@"%li",(long)error.error.code];
        if ([error.error.userInfo objectForKey:NSLocalizedDescriptionKey]) {
            NSString *info = [error.error.userInfo objectForKey:NSLocalizedDescriptionKey];
            errString = [errString stringByAppendingString:[NSString stringWithFormat:@":%@",info]];
        }
    }
    [AppManager trackError:errString FromAPI:webService.friendlyName OnPage:[self.serviceStatusDetailScreenDelegate pageNameForOmnitureTracking]];
    
    [self.serviceStatusDetailScreenDelegate serviceStatusDetailsScreen:self failedToFetchServiceDetailWithWebServiceError:error];
}

- (void)getHubStatus:(NLGetHubStatus *)webService successfullyFetchedHubStatusExecutionId:(NSString *)executionId {
    self.checkWorkflowStatus = [[NLCheckWorkflowStatus alloc] initWithExecutionId:executionId];
    self.checkWorkflowStatus.checkWorkflowStatusDelegate = self;
    [self.checkWorkflowStatus resume];
}

#pragma mark - CheckWorkflowStatus Delegate

- (void)checkWorkflowStatusWebService:(NLCheckWorkflowStatus *)webService finishedWithResponse:(NSObject *)response
{
    if([response valueForKey:@"data"]){
        NSError *error = nil;
        NSData* jsonData = [[response valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *responseDict = [NSJSONSerialization
                                      JSONObjectWithData:jsonData
                                      options:0
                                      error:&error];
        
        if(!error) {
            if ([responseDict valueForKey:@"hubDetails"]) {
                _hubDetails = [responseDict valueForKey:@"hubDetails"];
            } else {
                NSString *errString = @"";
                NSString *errCode = [responseDict objectForKey:@"errorCode"];
                NSString *errMsg  = [responseDict objectForKey:@"errorMessgae"];
                if (errCode && errMsg) {
                    errString = [NSString stringWithFormat:@"%@:%@",errCode,errMsg];
                }
                [AppManager trackError:errString FromAPI:webService.friendlyName OnPage:[self.serviceStatusDetailScreenDelegate pageNameForOmnitureTracking]];
            }
        } else {
            NSLog(@"Error in parsing JSON");
        }
    }
    // Finally time to update the UI.
    if (self.serviceStatusDetailScreenDelegate) {
        [self.serviceStatusDetailScreenDelegate successfullyFetchedWorkflowStatus:self];
    }
}

- (void)checkWorkflowStatusWebService:(NLCheckWorkflowStatus *)webService failedWithWebServiceError:(NLWebServiceError *)error
{
    NSString *errString = @"";
    if ([error isKindOfClass:[NLVordelWebServiceError class]]) {
        NLVordelWebServiceError *vordelError = (NLVordelWebServiceError*)error;
        errString = [NSString stringWithFormat:@"%@:%@",vordelError.errorCode,vordelError.errorDescription];
    } else {
        errString = [NSString stringWithFormat:@"%li",(long)error.error.code];
        if ([error.error.userInfo objectForKey:NSLocalizedDescriptionKey]) {
            NSString *info = [error.error.userInfo objectForKey:NSLocalizedDescriptionKey];
            errString = [errString stringByAppendingString:[NSString stringWithFormat:@":%@",info]];
        }
    }
    if (self.serviceStatusDetailScreenDelegate) {
        [AppManager trackError:errString FromAPI:webService.friendlyName OnPage:[self.serviceStatusDetailScreenDelegate pageNameForOmnitureTracking]];
        [self.serviceStatusDetailScreenDelegate serviceStatusDetailsScreen:self failedToFetchServiceDetailWithWebServiceError:error];
    }
}

- (void)timedOutWithoutSuccessForCheckWorkflowStatusWebService:(NLCheckWorkflowStatus *)webService
{
    if (self.serviceStatusDetailScreenDelegate) {
        [AppManager trackError:@"workflow_timeout" FromAPI:webService.friendlyName OnPage:[self.serviceStatusDetailScreenDelegate pageNameForOmnitureTracking]];
        [self.serviceStatusDetailScreenDelegate serviceStatusDetailsScreen:self failedToFetchServiceDetailWithWebServiceError:nil];
    }
    
}

#pragma mark - EE OAuthWebService Delegate methods

- (void)eeOAuthWebService:(NLEEOAuthWebService *)service failedWithError:(NLWebServiceError *)error{
    // Handle EE failure here if necessary
}

- (void)eeOAuthWebService:(NLEEOAuthWebService *)service loginSuccesfulWithToken:(BTAuthenticationToken *)oauthToken{
    if(oauthToken != nil) {
        _eeAuthToken = oauthToken;
        [self fetchEENationalNetworkStatus:oauthToken];
    }
}

- (void)eeGeocodingWebService:(NLEEGeocodingWebService *)service failedWithError:(NLWebServiceError *)error {
    // Handle EE geocoding failure here
}

- (void)eeGeocodingWebService:(NLEEGeocodingWebService *)service successWithLat:(NSString *)latitude andLong:(NSString *)longitude {
    if(latitude != nil && longitude != nil) {
        [self fetchEENetworkStatus:_eeAuthToken latitude:latitude andLongitude:longitude];
    }
}

- (void)eeNetworkStatusWebService:(NLEENetworkStatusWebService *)service failedWithError:(NLWebServiceError *)error {
    // Handle EE network status failure here
}

- (void)eeNetworkStatusWebService:(NLEENetworkStatusWebService *)service successWithNetworkStatus:(EENetworkStatus *)network {
    NSString *eeStatusCode = [network statusCode];
    // Is this a national response?
    if(![network locationId]) {
        if(![network causeCodes] || [[network causeCodes] count] == 0) { // If no national cause codes, do a regional service call
            [self fetchEEGeocode:_eeAuthToken];
        } else {
            if([eeStatusCode isEqualToString:@"S01"] || [eeStatusCode isEqualToString:@"S02"]) {
                if (self.serviceStatusDetailScreenDelegate && [self.serviceStatusDetailScreenDelegate respondsToSelector:@selector(displayNetworkStatusIssue)]) {
                    [self.serviceStatusDetailScreenDelegate displayNetworkStatusIssue];
                }
            }
        }
    } else { // If regional...
        if([eeStatusCode isEqualToString:@"S01"] || [eeStatusCode isEqualToString:@"S02"]) {
            if (self.serviceStatusDetailScreenDelegate && [self.serviceStatusDetailScreenDelegate respondsToSelector:@selector(displayNetworkStatusIssue)]) {
                [self.serviceStatusDetailScreenDelegate displayNetworkStatusIssue];
            }
        }
    }
    
}

@end
