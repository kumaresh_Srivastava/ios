//
//  BTServiceStatusDetailsViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 2/22/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTServiceStatusDetailsViewController.h"
#import "BTServiceStatusTableViewCell.h"
#import "BT4GAssureTableViewCell.h"
#import "BTUpgradeHubTableViewCell.h"
#import "BTNetworkIssueTableViewCell.h"
#import "BTServiceStatusTakeActionTableViewCell.h"
#import "BTServiceStatusActionsHeaderView.h"
#import "DLMServiceStatusDetailsScreen.h"
#import "BTHelpAndSupportViewController.h"
#import "BTHubHealthcheckViewController.h"
#import "AppManager.h"
#import "BTRetryView.h"
#import "CustomCircularLoaderAnimationView.h"
#import "NLWebServiceError.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "BTServiceStatusDetail.h"
#import "BrandColours.h"
#import "OmnitureManager.h"
#import "CustomSpinnerView.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "BTInAppBrowserViewController.h"
#import "BTNavigationViewController.h"
#import "BTFAQsViewController.h"
#import "BTResilientHubAsset.h"
#import "EENetworkStatusViewController.h"
#import "BTPublicWifiUnavailableViewController.h"
#import "BTPublicWifiUpgradeTableViewCell.h"
#import "BTUnableToDetectHubTableViewCell.h"
#import "BTClientServiceInstance.h"
#import "NLVordelWebServiceError.h"
#import "BTOrderStatusLabel.h"
#import "BTHubTableViewCell.h"
#import "MoreAppsTableViewCell.h"
#import <SafariServices/SafariServices.h>
#import "BTHomeNavigationController.h"
#import <StoreKit/SKStoreProductViewController.h>
#import "AppDelegate.h"


#define kServiceTypePSTNString @"PSTN"
#define kServiceTypeCloudVoiceString @"cloud"
#define kServiceTypeBroadbandString @"BB"

@interface BTServiceStatusDetailsViewController () <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, SKStoreProductViewControllerDelegate, BTServiceStatusTakeActionTableViewCellDelegate, BT4GAssureTableViewCellDelegate, BTUpgradeHubTableViewCellDelegate, BTRetryViewDelegate, DLMServiceStatusDetailsScreenDelegate> {
    
    NSTimer *_dotsTimer;
    int _totalSeconds;
    BTRetryView *_retryView;
    CustomCircularLoaderAnimationView *_circularLoaderAnimationView;
    NSString * _pageNameForOmniture; //(RLM)
    CustomSpinnerView *_loadingView;
    UITextView *affectedAreaValueTextView;
}

@property (nonatomic, readwrite) DLMServiceStatusDetailsScreen *serviceStatusDetailsViewModel;
@property (weak, nonatomic) IBOutlet UIView *btHubHeaderView;
@property (weak, nonatomic) IBOutlet UIView *tableHeaderView;
@property (weak, nonatomic) IBOutlet UITableView *serviceStatusDetailsTableView;
@property (weak, nonatomic) IBOutlet UIView *headerWithStatusColorView;
@property (weak, nonatomic) IBOutlet UIImageView *headerServiceIconImageView;
@property (weak, nonatomic) IBOutlet UILabel *checkingServicesLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceIDLabel;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (weak, nonatomic) IBOutlet UIImageView *statusLoaderImageView;
@property (weak, nonatomic) IBOutlet UIButton *refreshButton;
@property (weak, nonatomic) IBOutlet UILabel *headerServiceIssueLocationLabel;
@property (weak, nonatomic) IBOutlet BTOrderStatusLabel *issueTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *affectedDiallingCodeLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBackArrowButtonWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintRefreshButtonWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintDiallingCodeHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeaderWithStatusColorViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBTHubHeaderViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerServiceIssueLocationHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerServiceTop;



@property (nonatomic) BOOL has4GAssure;
@property (nonatomic) BOOL suggestHubUpgrade;
@property (nonatomic) BOOL connectedBy4GAssure;
@property (nonatomic) BOOL noNetworkAvailable;
@property (nonatomic) BOOL networkRequestInProgress;

@property (nonatomic) BOOL isCloudVoiceExpress;


@property (nonatomic) int detailRows;
@property (nonatomic) int resilientRows;
@property (nonatomic) int upgradeRows;
@property (nonatomic) int issueRows;
@property (nonatomic) int issueRowHeight;

@property (nonatomic) EENetworkStatus *networkStatus;

@end


@implementation BTServiceStatusDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //_serviceStatusDetailsContainerView.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
    
    
    //Checking the selected item is cloud voice service or not
    
    if ([self.serviceID rangeOfString:@"cv" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        _isCloudVoiceExpress = YES;
        _productName = @"Cloud Voice Express"; // For setting Cloud voice title
    }
    else {
        _isCloudVoiceExpress = NO;
    }
    
    self.edgesForExtendedLayout = UIRectEdgeNone;

    _constraintHeaderWithStatusColorViewHeight.constant = 64;
    //self.title = @"Check Service Status";
    if ([UIScreen mainScreen].scale > 2.0f)
    {
        _constraintBackArrowButtonWidth.constant = 38;
        _constraintRefreshButtonWidth.constant = 38;
    }
    else
    {
        _constraintBackArrowButtonWidth.constant = 30;
        _constraintRefreshButtonWidth.constant = 30;
    }
    
    _detailRows = 1;
    _resilientRows = 1;
    _upgradeRows = 2;
    _issueRows = 0;
    _issueRowHeight = 0;
    
    //self.view.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
    
    if([_productName isEqualToString:@"Business BroadBand Access"]) {
        _productName = @"Business broadband";
    }

    self.titleLabel.text = _productName;
    
    if (_currentServiceType == BTServiceTypePSTN)
    {
        [self removeBroadbandCells];
        self.headerServiceIssueLocationLabel.text = [NSString stringWithFormat:@"BT Telephone Problem in the %@", _postCode];
    }
    else
    {
        self.headerServiceIssueLocationLabel.text = [NSString stringWithFormat:@"BT Broadband Problem in the %@", _postCode];
    }
    
    _serviceStatusDetailsViewModel = [[DLMServiceStatusDetailsScreen alloc] init];
    _serviceStatusDetailsViewModel.serviceStatusDetailScreenDelegate = self;
    
    // Service status table view setup
    self.serviceStatusDetailsTableView.delegate = self;
    self.serviceStatusDetailsTableView.dataSource = self;
    self.serviceStatusDetailsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.serviceStatusDetailsTableView.estimatedRowHeight = 50;
    self.serviceStatusDetailsTableView.rowHeight = UITableViewAutomaticDimension;
    
    _serviceIDLabel.text = self.serviceID;
    
    UINib *dataCell = [UINib nibWithNibName:@"BTServiceStatusTableViewCell" bundle:nil];
    UINib *actionCell = [UINib nibWithNibName:@"BTServiceStatusTakeActionTableViewCell" bundle:nil];
    UINib *resilientCell = [UINib nibWithNibName:@"BT4GAssureTableViewCell" bundle:nil];
    UINib *upgradeHubCell = [UINib nibWithNibName:@"BTUpgradeHubTableViewCell" bundle:nil];
    UINib *networkIssueCell = [UINib nibWithNibName:@"BTNetworkIssueTableViewCell" bundle:nil];
    UINib *unableToDetectCell = [UINib nibWithNibName:@"BTUnableToDetectHubTableViewCell" bundle:nil];
    UINib *upgradeNib = [UINib nibWithNibName:@"BTPublicWifiUpgradeTableViewCell" bundle:[NSBundle mainBundle]];
    UINib *hubImageCell = [UINib nibWithNibName:@"BTHubTableViewCell" bundle:[NSBundle mainBundle]];
    UINib *cloudVoiceAppCell = [UINib nibWithNibName:@"MoreAppsTableViewCell" bundle:nil];

    
    [self.serviceStatusDetailsTableView registerNib:dataCell forCellReuseIdentifier:@"BTServiceStatusTableViewCell"];
    [self.serviceStatusDetailsTableView registerNib:actionCell forCellReuseIdentifier:@"BTServiceStatusTakeActionTableViewCell"];
    [self.serviceStatusDetailsTableView registerNib:resilientCell forCellReuseIdentifier:@"BT4GAssureTableViewCell"];
    [self.serviceStatusDetailsTableView registerNib:upgradeHubCell forCellReuseIdentifier:@"BTUpgradeHubTableViewCell"];
    [self.serviceStatusDetailsTableView registerNib:networkIssueCell forCellReuseIdentifier:@"BTNetworkIssueTableViewCell"];
    [self.serviceStatusDetailsTableView registerNib:upgradeNib forCellReuseIdentifier:@"BTPublicWifiUpgradeTableViewCell"];
    [self.serviceStatusDetailsTableView registerNib:unableToDetectCell forCellReuseIdentifier:@"BTUnableToDetectHubTableViewCell"];
    [self.serviceStatusDetailsTableView registerNib:hubImageCell forCellReuseIdentifier:@"BTHubTableViewCell"];
    [self.serviceStatusDetailsTableView registerNib:cloudVoiceAppCell forCellReuseIdentifier:@"MoreAppsTableViewCell"];
    
    self.serviceStatusDetailsTableView.backgroundColor = [BrandColours colourBtNeutral30];
    
    __weak typeof(self) weakSelf = self;
    
    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([weakSelf networkRequestInProgress]) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
            
        }
        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
            
        }
    }];
    [self makeAPIcall];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    //[self resizeTableViewHeaderToFit];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChangeServiceDetail:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
   
    
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.isSingleBacCug == YES){
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if (self.isMovingFromParentViewController || self.isBeingDismissed) {
            appDelegate.isSingleBacCugBackNavButtonPressed = YES;
        }
        else{
            appDelegate.isSingleBacCugBackNavButtonPressed = NO;
        }
    }else {
        appDelegate.isSingleBacCugBackNavButtonPressed = NO;
    }
}

- (void)statusBarOrientationChangeServiceDetail:(NSNotification *)notification {
    // handle the interface orientation as needed
     [self createLoadingView];
    [self createLoadingViewWithBackgroundColor:_loadingView.backgroundColor andText:_loadingView.loadingLabel.text];
}

#pragma mark - API methods

- (void)makeAPIcall
{
    switch (_currentServiceType) {
        case BTServiceTypeBroadband:
            //[self fetchServiceStatusDetails];
            [self fetchResilientHubAsset];
            break;
            
        case BTServiceTypePSTN:
        default:
            [self fetchServiceStatusDetails];
            break;
    }
}

- (void)fetchServiceStatusDetails
{
    if([AppManager isInternetConnectionAvailable])
    {
        [self updateUIForAPICall];
        
        if (_currentServiceType == BTServiceTypePSTN)
        {
            if ([self.serviceID rangeOfString:@"cv" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                [_serviceStatusDetailsViewModel fetchServiceStatusDetailWithServiceID:self.serviceID andServiceType:kServiceTypeCloudVoiceString andIsNeedToPointModelAAlways:self.pointToModelAAlways];
            }
            else {
                [_serviceStatusDetailsViewModel fetchServiceStatusDetailWithServiceID:self.serviceID andServiceType:kServiceTypePSTNString andIsNeedToPointModelAAlways:self.pointToModelAAlways];
            }
            if (_isCloudVoiceExpress) {
                [self trackPage:OMNIPAGE_SERVICE_STATUS_CVE_SCAN_INPROGRESS andNeedToSendLoginStatus:YES];
            } else {
                [self trackPage:OMNIPAGE_SERVICE_STATUS_SCAN_INPROGRESS andNeedToSendLoginStatus:YES];
            }
        }
        else if (_currentServiceType == BTServiceTypeBroadband)
        {
            [_serviceStatusDetailsViewModel fetchServiceStatusDetailWithServiceID:self.serviceID andServiceType:kServiceTypeBroadbandString andIsNeedToPointModelAAlways:self.pointToModelAAlways];
            [self trackPage:OMNIPAGE_SERVICE_STATUS_CHECKING andNeedToSendLoginStatus:YES];
        }
    }
    else
    {
        //self.view.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
        _serviceStatusDetailsTableView.hidden = YES;
        _headerWithStatusColorView.hidden = YES;
        _refreshButton.hidden = YES;
        [self showRetryViewWithInternetStrip:YES];
        if (_currentServiceType == BTServiceTypePSTN) {
            [AppManager trackNoInternetErrorOnPage:OMNIPAGE_SERVICE_STATUS_SCAN_INPROGRESS];
        } else if (_currentServiceType == BTServiceTypeBroadband) {
            [AppManager trackNoInternetErrorOnPage:OMNIPAGE_SERVICE_STATUS_CHECKING];
        }
        
    }
}

-(void)fetchResilientHubAsset {
    [self updateUIForAPICall];
    [_serviceStatusDetailsViewModel fetchResilientHubAsset:self.serviceID];
}

-(void)fetchEEOAuthToken {
    [self updateUIForAPICall];
    [_serviceStatusDetailsViewModel fetchEEOAuthToken]; //TODO
}

#pragma mark - Private helper methods

/*
 Resize header view of table to update height of header view
 */
- (void)resizeTableViewHeaderToFit {
    UITableViewHeaderFooterView *headerView = (UITableViewHeaderFooterView *)self.serviceStatusDetailsTableView.tableHeaderView;
    
    [headerView setNeedsLayout];
    [headerView layoutIfNeeded];
    
    CGFloat height = [headerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CGRect frame = headerView.frame;
    frame.size.height = height;
    headerView.frame = frame;
    
    self.serviceStatusDetailsTableView.tableHeaderView = headerView;
}

/*
 Update loading texts while API call dynamically
 */

- (void)updateLoadingTexts:(id)sender {
    
    if ([_checkingServicesLabel.text isEqualToString:@"Checking your service"])
    {
        _checkingServicesLabel.text = @"Checking your service.";
    }
    else if ([_checkingServicesLabel.text isEqualToString:@"Checking your service."])
    {
        _checkingServicesLabel.text = @"Checking your service..";
    }
    else if ([_checkingServicesLabel.text isEqualToString:@"Checking your service.."])
    {
        _checkingServicesLabel.text = @"Checking your service...";
    }
    else if ([_checkingServicesLabel.text isEqualToString:@"Checking your service..."])
    {
        _checkingServicesLabel.text = @"Checking your service";
    }
    
    _totalSeconds--;
    
    if (_totalSeconds == 0)
    {
        [self updateUIAfterSuccessfulApiCall];
    }
    
}

/*
 Timer handling methods for loading dynamic text
 */
- (void)startTimerForApiCallLoadingText
{
    _totalSeconds = 5;
    
    if (!_dotsTimer)
    {
        _dotsTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateLoadingTexts:) userInfo:NULL repeats:YES];
    }
}

- (void)stopTimerForApiCallLoadingText
{
    if ([_dotsTimer isValid])
    {
        [_dotsTimer invalidate];
    }
    _dotsTimer = nil;
}

/*
 Update UI after API call
 */
- (void)updateUIAfterSuccessfulApiCall
{
    _affectedDiallingCodeLabel.hidden = NO;
    
    if (_serviceStatusDetailsViewModel.currentIssueType == SSIssueTypeOngoingIssue)
    {
        _issueTypeLabel.textColor = [BrandColours colourMyBtLightRed];
        _headerWithStatusColorView.backgroundColor = [BrandColours colourMyBtLightRed];
        
        if(self.currentServiceType == BTServiceTypeBroadband){
            _headerServiceIssueLocationLabel.text = [NSString stringWithFormat:@"BT Broadband problem in the\n%@", self.postCode];
            _affectedDiallingCodeLabel.text = [NSString stringWithFormat:@"Dialling codes affected: %@", [self.serviceID substringWithRange:NSMakeRange(0, 5)]];

        }
        else if (_isCloudVoiceExpress) {
            _headerServiceIssueLocationLabel.text = @"Fix in progress";
            _constraintDiallingCodeHeight.constant = 0;
        }
        else{ // Other Phone lines or PSTN products
            _headerServiceIssueLocationLabel.text = [NSString stringWithFormat:@"BT Telephone problem in the\n%@", self.postCode];
            _affectedDiallingCodeLabel.text = [NSString stringWithFormat:@"Dialling codes affected: %@", [self.serviceID substringWithRange:NSMakeRange(0, 5)]];
        }
        
    }
    else if (_serviceStatusDetailsViewModel.currentIssueType == SSIssueTypeNone){
        if(self.currentServiceType == BTServiceTypePSTN || self.currentServiceType == BTServiceTypeBroadband){
            _headerWithStatusColorView.backgroundColor = [BrandColours colourTrueGreen];
        }
        _headerServiceIssueLocationLabel.text = @"Working well";
        
        _affectedDiallingCodeLabel.text = nil;
        _affectedDiallingCodeLabel.hidden = YES;
        _constraintDiallingCodeHeight.constant = 0;
        //[_affectedDiallingCodeLabel removeFromSuperview];
    }
    else if (_serviceStatusDetailsViewModel.currentIssueType == SSIssueTypeRecentlyResolvedIssue){
        
        if(self.currentServiceType == BTServiceTypeBroadband){
            
            _headerServiceIssueLocationLabel.text = [NSString stringWithFormat:@"BT Broadband problem in the\n%@", self.postCode];
        }
        else{
            
            _headerServiceIssueLocationLabel.text = [NSString stringWithFormat:@"BT Telephone problem in the\n%@", self.postCode];
        }
        
        
        _affectedDiallingCodeLabel.text = [NSString stringWithFormat:@"Affecting Dialling Code: %@", [self.serviceID substringWithRange:NSMakeRange(0, 5)]];
    }
    
    NSString *issueText = @"";
    if ([_serviceStatusDetailsViewModel.serviceStatusDetail.mSOFlag compare:@"No issues found" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        //_issueTypeLabel.text = @"No problems found";
        issueText = @"No problems found";
    }
    else if ([_serviceStatusDetailsViewModel.serviceStatusDetail.mSOFlag compare:@"Ongoing issue" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        //_issueTypeLabel.text = @"Problems found";
        //[self.issueTypeLabel setMSOFlag:@"Problems found" forBroadband:NO];
        //[self.issueTypeLabel setOrderStatus:@"Problems found"];
        issueText = @"Problems found";
    }
    else {
        //_issueTypeLabel.text = _serviceStatusDetailsViewModel.serviceStatusDetail.mSOFlag;
        issueText = _serviceStatusDetailsViewModel.serviceStatusDetail.mSOFlag;
    }

    if(self.currentServiceType == BTServiceTypeBroadband) {
        //[_issueTypeLabel setMSOFlag:_serviceStatusDetailsViewModel.serviceStatusDetail.mSOFlag forBroadband:YES];
        [self.issueTypeLabel setMSOFlag:issueText forBroadband:YES];
    } else {
        //[_issueTypeLabel setMSOFlag:_serviceStatusDetailsViewModel.serviceStatusDetail.mSOFlag forBroadband:NO];
        [self.issueTypeLabel setMSOFlag:issueText forBroadband:NO];
    }
    
    //[self stopTimerForApiCallLoadingText];
    
    if(self.currentServiceType == BTServiceTypePSTN){
        self.view.backgroundColor = [BrandColours colourBtWhite];
        _serviceStatusDetailsTableView.hidden = NO;
        _headerWithStatusColorView.hidden = NO;
        _refreshButton.hidden = NO;
        _statusLoaderImageView.hidden = YES;
        _checkingServicesLabel.hidden = YES;
        //[self hideLoaderAnimation]; // if the user's on broadband, the loader shouldn't be hidden until the Resilient Hub checks have been done
        [self hideLoadingItems:YES];
    }
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
}

- (void)updateUIAfterSuccessfulWorkflowCall
{
    _affectedDiallingCodeLabel.hidden = NO;
    _headerWithStatusColorView.hidden = NO;
    
    NSArray *resilientHubAssets = [_serviceStatusDetailsViewModel resilientHubAssets];
    BTResilientHubAsset *asset = (BTResilientHubAsset*) resilientHubAssets[0];
    NSDictionary *hubAttributes = [asset attributes];
    NSString *serialNumber = [asset hubSerialNumber];
    
    CGRect tableFrame = _tableHeaderView.frame;
    tableFrame.size.height = 325;
    [_tableHeaderView setFrame:tableFrame];
    
    if (serialNumber && ![serialNumber isEqualToString:@""] && ![serialNumber isKindOfClass:[NSNull class]]) {
        
        NSDictionary *hubDetails = [_serviceStatusDetailsViewModel hubDetails];
        NSString *operationalMode = [hubDetails objectForKey:@"Operational Mode"];
        NSString *productClass = [hubDetails objectForKey:@"Product Class"];
        NSString *onlineStatus = [hubDetails objectForKey:@"CPE Online Status"];
        
        [self.issueTypeLabel setMSOFlag:_serviceStatusDetailsViewModel.serviceStatusDetail.mSOFlag forBroadband:YES];
        
        if (onlineStatus && [onlineStatus isEqualToString:@"OFFLINE"]) {
            self.view.backgroundColor = [BrandColours colourBtWhite];
            _serviceStatusDetailsTableView.hidden = NO;
            _refreshButton.hidden = NO;
            _statusLoaderImageView.hidden = YES;
            _checkingServicesLabel.hidden = YES;
            [self hideLoaderAnimation];
            BTClientServiceInstance *tmpInstance = [[BTClientServiceInstance alloc] initFromHubDetails:hubDetails withResilientAsset:asset];
            BTPublicWifiUnavailableViewController *errView = [[BTPublicWifiUnavailableViewController alloc] initWithClientServiceInstance:tmpInstance andContext:BTPublicWifiUnavailableContextGenericError4GAssure];
            [self.navigationController pushViewController:errView animated:NO];
            
        } else {
            // assume online
            NSString *omnitureContentViewed = @"";
            
            if(![productClass containsString:@"Hub 6"] && ![productClass containsString:@"Hub 5"] && ![productClass containsString:@"Smart"]) {
                _suggestHubUpgrade = TRUE;
                // don't show 4G assure upsell - covered by hub upgrade
                if(_resilientRows > 0) {
                    _resilientRows = 0;
                    //[_serviceStatusDetailsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:resilientIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                }
            } else {
                if(_upgradeRows > 0) {
                    _upgradeRows = 0;
                    //[_serviceStatusDetailsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:upgradeHubIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                }
            }
            
            if([hubAttributes valueForKey:@"attribute"]) {
                NSDictionary *resilientFlag = [hubAttributes valueForKey:@"attribute"];
                if([[resilientFlag valueForKey:@"value"] isEqualToString:@"N"]) {
                    _has4GAssure = FALSE;
                } else {
                    _has4GAssure = TRUE;
                    if(_resilientRows > 0) {
                        _resilientRows = 0;
                        //[_serviceStatusDetailsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:resilientIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                    }
                }
            }
            
            if([operationalMode isEqualToString:@"DSL"] || [operationalMode isEqualToString:@"VDSL"] || [operationalMode isEqualToString:@"G.Fast"] || [operationalMode isEqualToString:@"FIBRE"]) {
                _connectedBy4GAssure = FALSE;
                _headerWithStatusColorView.backgroundColor = [UIColor colorWithRed:0/255.0 green:170/255.0 blue:214/255.0 alpha:1.0];
                
                //_issueTypeLabel.text = @"Broadband connected";
                //_issueTypeLabel.textColor =  [UIColor colorWithRed:0/255.0 green:170/255.0 blue:214/255.0 alpha:1.0];
                [self.issueTypeLabel setMSOFlag:@"Broadband connected" forBroadband:YES];
                
                _headerServiceIssueLocationLabel.text = @"Working well";
                
                if(!_has4GAssure) {
                    _affectedDiallingCodeLabel.text = @"4G Assure: Available";
                    omnitureContentViewed = @"Broadband connected - Working well - 4G Assure Available";
                } else {
                    _affectedDiallingCodeLabel.text = @"4G Assure: Ready";
                    omnitureContentViewed = @"Broadband connected - Working well - 4G Assure Ready";
                }
                
                if([productClass containsString:@"Hub 6"] || [productClass containsString:@"Smart"]) {
                    _headerServiceIconImageView.image = [UIImage imageNamed:@"smarthub_blue"];
                } else if ([productClass containsString:@"Hub 5"] || [productClass containsString:@"Hub 4"]){
                    _headerServiceIconImageView.image = [UIImage imageNamed:@"businesshub_blue"];
                } else {
                    _headerServiceIconImageView.image = [UIImage imageNamed:@"broadband_no_issue_hub3"];
                    _affectedDiallingCodeLabel.text = @"4G Assure: New hub needed";
                    omnitureContentViewed = @"Broadband connected - Working well - 4G New hub needed";
                }
            } else if ([operationalMode isEqualToString:@"MOBILE"]) {
                _connectedBy4GAssure = TRUE;
                
                _headerWithStatusColorView.backgroundColor = [UIColor colorWithRed:100/255.0 green:0/255.0 blue:170/255.0 alpha:1.0];
                
                //_issueTypeLabel.text = @"4G Assure connected";
                //_issueTypeLabel.textColor = [UIColor colorWithRed:100/255.0 green:0/255.0 blue:170/255.0 alpha:1.0];
                [self.issueTypeLabel setMSOFlag:@"4G Assure connected" forBroadband:YES];
                
                _headerServiceIssueLocationLabel.text = @"Working with 4G";
                
                _affectedDiallingCodeLabel.text = @"4G Assure: On";
                
                omnitureContentViewed = @"4G Assure connected - Working on 4G - 4G Assure On";
                
                // If we're in the 'connected with mobile' state, check 4G upsell isn't displaying
                if(_resilientRows > 0) {
                    _resilientRows = 0;
                    //[_serviceStatusDetailsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:resilientIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                }
                
                if([productClass containsString:@"Hub 6"] || [productClass containsString:@"Smart"]) {
                    _headerServiceIconImageView.image = [UIImage imageNamed: @"smarthub_purple.png"];
                } else if ([productClass containsString:@"Hub 5"] || [productClass containsString:@"Hub 4"]){
                    _headerServiceIconImageView.image = [UIImage imageNamed: @"businesshub_purple.png"];
                } else {
                    _headerServiceIconImageView.image = [UIImage imageNamed:@"broadband_no_issue_hub3"];
                }
            } else if([operationalMode isEqualToString:@"UNKNOWN"] || [operationalMode isEqualToString:@"RESET"]) {
                _noNetworkAvailable = TRUE;
                _connectedBy4GAssure = FALSE;
                
                _headerWithStatusColorView.backgroundColor = [UIColor colorWithRed:230/255.0 green:0/255.0 blue:20/255.0 alpha:1.0];
                
//                _issueTypeLabel.text = @"Unable to connect";
//                _issueTypeLabel.textColor = [UIColor colorWithRed:230/255.0 green:0/255.0 blue:20/255.0 alpha:1.0];
                [self.issueTypeLabel setMSOFlag:@"Unable to connect" forBroadband:YES];
                
                _headerServiceIssueLocationLabel.text = @"Broadband";
                
                if(!_has4GAssure) {
                    _affectedDiallingCodeLabel.text = @"4G Assure: Available";
                    omnitureContentViewed = @"Unable to connected - Broadband - 4G Assure Available";
                } else {
                    //_affectedDiallingCodeLabel.hidden = YES;
                    [_affectedDiallingCodeLabel removeFromSuperview];
                    omnitureContentViewed = @"Unable to connected - Broadband and 4G - 4G Assure Unavailable";
//                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
                    if(_resilientRows > 0) {
                        _resilientRows = 0;
                        //[_serviceStatusDetailsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
                    }
                }
                
                if([productClass containsString:@"Hub 6"] || ![productClass containsString:@"Smart"]) {
                    _headerServiceIconImageView.image = [UIImage imageNamed:@"smarthub_red"];
                } else if ([productClass containsString:@"Hub 5"] || [productClass containsString:@"Hub 4"]){
                    _headerServiceIconImageView.image = [UIImage imageNamed:@"businesshub_red"];
                } else {
                    _headerServiceIconImageView.image = [UIImage imageNamed:@"broadband_issue_hub3"];
                }
            }
            
            [self updateDetailText];
            [self trackPageForOmnitureWithContentViewed:omnitureContentViewed];
            self.view.backgroundColor = [BrandColours colourBtWhite];
            [_serviceStatusDetailsTableView reloadData];
            _serviceStatusDetailsTableView.hidden = NO;
            _headerWithStatusColorView.hidden = NO;
            _refreshButton.hidden = NO;
            _statusLoaderImageView.hidden = YES;
            _checkingServicesLabel.hidden = YES;
            //[self hideLoaderAnimation];
            [self hideLoadingItems:YES];
        }
    } else {
        // hub serial number is null
        [self updateUIforNullHubSerialNumber];
    }
}

- (void)updateUIforNullHubSerialNumber
{
    _suggestHubUpgrade = TRUE;
    _has4GAssure = FALSE;
    _connectedBy4GAssure = FALSE;
    
    // don't show 4G assure upsell - covered by hub upgrade
    if(_resilientRows > 0) {
        _resilientRows = 0;
        //[_serviceStatusDetailsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:resilientIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
    if (_detailRows > 0) {
        _detailRows = 0;
        //[_serviceStatusDetailsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:detailIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
    _headerWithStatusColorView.backgroundColor = [UIColor colorWithRed:0/255.0 green:170/255.0 blue:214/255.0 alpha:1.0];
    //_issueTypeLabel.text = @"Unable to detect hub";
   //_issueTypeLabel.textColor =  [UIColor blueColor];//[UIColor colorWithRed:0/255.0 green:170/255.0 blue:214/255.0 alpha:1.0];
    [_issueTypeLabel setMSOFlag:@"Unable to detect hub" forBroadband:YES];
    _headerServiceIssueLocationLabel.text = @"No status available";
    _affectedDiallingCodeLabel.hidden = YES;
    _constraintDiallingCodeHeight.constant = 0;
    [self resizeTableViewHeaderToFit];
    
    [self.view updateConstraints];
    //[_affectedDiallingCodeLabel removeFromSuperview];
    _headerServiceIconImageView.image = [UIImage imageNamed:/*@"graphic_negative"*/@"error_red"];
    //_headerServiceIconImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [self trackPageForOmnitureWithContentViewed:@"Unsupported hub"];
    self.view.backgroundColor = [BrandColours colourBtWhite];
    [_serviceStatusDetailsTableView reloadData];
    _serviceStatusDetailsTableView.hidden = NO;
    _headerWithStatusColorView.hidden = NO;
    _refreshButton.hidden = NO;
    _statusLoaderImageView.hidden = YES;
    _checkingServicesLabel.hidden = YES;
    //[self hideLoaderAnimation];
    [self hideLoadingItems:YES];
}

- (void)updateUIAfterFailedApiCall
{
    _statusLoaderImageView.hidden = YES;
    _checkingServicesLabel.hidden = YES;
    [self.view bringSubviewToFront:_headerWithStatusColorView];
    [self.view bringSubviewToFront:_backButton];
    [self hideLoaderAnimation];
}

- (void)updateUIForAPICall
{
    //self.view.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
    _serviceStatusDetailsTableView.hidden = YES;
    _headerWithStatusColorView.hidden = YES;
    _refreshButton.hidden = YES;
    _statusLoaderImageView.hidden = YES;
    _checkingServicesLabel.hidden = YES;
    _checkingServicesLabel.text = @"Checking your service";
    //[self startTimerForApiCallLoadingText];
    //[self createAndDisplayLoaderAnimation];
    //[self createLoadingView];
    [self hideLoadingItems:NO];
    [self createLoadingViewWithBackgroundColor:[UIColor whiteColor] andText:@"Checking your service..."];
    [self hideRetryView];
    
}

-(void)removeBroadbandCells {
    _constraintBTHubHeaderViewHeight.constant = 0;
    CGRect tableFrame = _tableHeaderView.frame;
    tableFrame.size.height = 110;
    [_tableHeaderView setFrame:tableFrame];
    
   /* if(_resilientRows > 0 && _upgradeRows > 0 && _issueRows > 0) {
        _resilientRows = 0;
        _upgradeRows = 0;
        _issueRows = 0;
        NSIndexPath *resilientIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
        NSIndexPath *upgradeHubIndexPath = [NSIndexPath indexPathForRow:0 inSection:2];
        NSIndexPath *unableToDetectIndexPath = [NSIndexPath indexPathForRow:1 inSection:2];
        NSIndexPath *issueIndexPath = [NSIndexPath indexPathForRow:0 inSection:3];
        [_serviceStatusDetailsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:resilientIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        [_serviceStatusDetailsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:upgradeHubIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        [_serviceStatusDetailsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:issueIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        [_serviceStatusDetailsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:unableToDetectIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    }*/
    
    if(_resilientRows > 0 && _upgradeRows > 0 && _issueRows > 0) {
        
        NSIndexPath *unableToDetectIndexPath = [NSIndexPath indexPathForRow:1 inSection:2];
        [_serviceStatusDetailsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:unableToDetectIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    if(_resilientRows > 0 ) {
        _resilientRows = 0;
        NSIndexPath *resilientIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
        [_serviceStatusDetailsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:resilientIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    if(_upgradeRows > 0) {
        _upgradeRows = 0;
        NSIndexPath *upgradeHubIndexPath = [NSIndexPath indexPathForRow:0 inSection:2];
        [_serviceStatusDetailsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:upgradeHubIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    if(_issueRows > 0) {
        _issueRows = 0;
        NSIndexPath *issueIndexPath = [NSIndexPath indexPathForRow:0 inSection:3];
        [_serviceStatusDetailsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:issueIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    
}

- (void) createLoadingView{
    if(_loadingView == nil){
        _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        [self.view addSubview:_loadingView];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    }
}

- (void)createLoadingViewWithBackgroundColor:(UIColor*)color andText:(NSString*)text {
    
    if(_loadingView.isHidden == YES){
        return;
    }
  
    [_loadingView lazyLoadSpinView];
    _loadingView.backgroundColor = color;
    _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
    _loadingView.loadingLabel.text = text;
}

- (void)hideLoadingItems:(BOOL)isHide
{

    if (isHide)
    {
        [_loadingView setHidden:YES];
        //[_loadingView removeFromSuperview];
        //_loadingView = nil;
    }
    else
    {
        if (!_loadingView)
        {
            [self createLoadingView];
            [self createLoadingViewWithBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f] andText:@"Loading..."];
        }
        [_loadingView setHidden:NO];
    }
}

- (void)createAndDisplayLoaderAnimation
{
    if (!_circularLoaderAnimationView)
    {
        _circularLoaderAnimationView = [[CustomCircularLoaderAnimationView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height - 64)];
        _circularLoaderAnimationView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_circularLoaderAnimationView];
    }
    
    [_circularLoaderAnimationView startAnimation];
}

- (void)hideLoaderAnimation
{
    [_circularLoaderAnimationView removeFromSuperview];
    _circularLoaderAnimationView = nil;
}

- (void)checkChatAvailabilityAction
{
    if ([AppManager isInternetConnectionAvailable])
    {
        _networkRequestInProgress = YES;
        [self hideLoadingItems:NO];
        [_loadingView startAnimatingLoadingIndicatorView];
        [self.serviceStatusDetailsViewModel checkForLiveChatAvailibilityForFault];
    }
    else
    {
        [self showAlertWithTitle:kNoConnectionTitle andMessage:kNoConnectionMessage];
    }
}

- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }]];
    
    [self presentViewController:alertController animated:YES completion:^{
        
    }];
}

#pragma mark - Action Methods

- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)refreshButtonAction:(id)sender
{
//    [self fetchServiceStatusDetails];
//    if (_currentServiceType == BTServiceTypeBroadband) {
//        [self fetchResilientHubAsset];
//    }
    [self makeAPIcall];
}

-(void)moreAppButtonDownloadAction:(id)sender { // Button Cloud Voice App
    
    if (_serviceStatusDetailsViewModel.currentIssueType == SSIssueTypeNone) {
        [self trackOmniClickForCVEwithPageName:OMNIPAGE_SERVICE_STATUS_CVE_WORKING_WELL andLink:@"Cloud Voice app download"];
    }
    else {
        [self trackOmniClickForCVEwithPageName:OMNIPAGE_SERVICE_STATUS_CVE_WORKING_WELL_PREVIOUS_ISSUE andLink:@"Cloud Voice app download"];
    }
//    [AppManager openURL:@"https://itunes.apple.com/us/app/bt-cloud-voice-express/id1457358475?mt=8"];

    NSInteger iTuneID = 1457358475;//[[_urlPathArray objectAtIndex:[moreAppButton tag]] integerValue];
    [self openStoreProductViewControllerWithITunesItemIdentifier:iTuneID];
}

- (void)openStoreProductViewControllerWithITunesItemIdentifier:(NSInteger)iTunesItemIdentifier {
    
    SKStoreProductViewController *storeViewController = [[SKStoreProductViewController alloc] init];
    storeViewController.delegate = self;
    
    NSNumber *identifier = [NSNumber numberWithInteger:iTunesItemIdentifier];
    
    NSDictionary *parameters = @{ SKStoreProductParameterITunesItemIdentifier:identifier };
    [storeViewController loadProductWithParameters:parameters completionBlock:nil];
    
    [self presentViewController:storeViewController animated:YES completion:nil];
}

- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Class Methods

+ (BTServiceStatusDetailsViewController *)getServiceStatusDetailsViewController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"OptimiseServiceStatus" bundle:nil];
    BTServiceStatusDetailsViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"BTServiceStatusDetailsViewController"];
    
    return controller;
}

#pragma mark - Table View Datasource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (_isCloudVoiceExpress && _serviceStatusDetailsViewModel.currentIssueType != SSIssueTypeOngoingIssue) {
        return 6;
    }
    else return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)  {
        return _detailRows;
    } else if (section == 1) {
        return _resilientRows;
    } else if (section == 2) {
        return _upgradeRows;
    } else if (section == 3) {
        return _issueRows;
    } else if (section == 4) {
        return _serviceStatusDetailsViewModel.arrayOfActionList.count;
    }else
        return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // (gv) disabled network status view until cause code mapping available
//    if(indexPath.section == 3 && _networkStatus != nil) {
//        EENetworkStatusViewController *networkStatusViewController = [[EENetworkStatusViewController alloc] initWithNetworkStatus:_networkStatus];
//        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:networkStatusViewController];
//        [self presentViewController:navController animated:YES completion:nil];
//        [self trackOmniClick:OMNICLICK_SERVICE_STATUS_NETWORK_ISSUE];
//    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        BTServiceStatusTableViewCell *serviceStatusCell = [tableView dequeueReusableCellWithIdentifier:@"BTServiceStatusTableViewCell" forIndexPath:indexPath];
        NSString *affectedAreaText = @"";
        
        if (_isCloudVoiceExpress && _serviceStatusDetailsViewModel.currentIssueType != SSIssueTypeOngoingIssue) {
            
            NSURL *url = [[NSURL alloc] initWithString:@""];
            
            NSMutableAttributedString *hypLink = [[NSMutableAttributedString  alloc] initWithString:@"Our checks show no Cloud Voice Express problems in your area. Still having problems? Check your broadband"];
            
            NSDictionary *linkAttributes = @{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle),
                                             NSLinkAttributeName : url};
            
            [hypLink addAttributes:linkAttributes range:NSMakeRange(85,20)];
            
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setAlignment:NSTextAlignmentLeft];
            
            [hypLink addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,hypLink.length)];
            
            [hypLink addAttribute:NSForegroundColorAttributeName value:[BrandColours colourBtLightBlack] range:NSMakeRange(0, hypLink.length)];
            [hypLink addAttribute:NSForegroundColorAttributeName value:[BrandColours colourBackgroundBTPurplePrimaryColor] range:NSMakeRange(85,20)];
            [hypLink addAttribute:NSFontAttributeName value:[UIFont fontWithName:kBtFontRegular size:16.0] range:NSMakeRange(0, hypLink.length)];
            [serviceStatusCell updateCellWithServiceStatusDetailForCloudVoice:_serviceStatusDetailsViewModel.serviceStatusDetail andAffectedAreaText:hypLink];
            affectedAreaValueTextView = serviceStatusCell.affectedAreaValueTextView;
            affectedAreaValueTextView.linkTextAttributes = @{NSUnderlineColorAttributeName: [UIColor clearColor]};
            affectedAreaValueTextView.userInteractionEnabled = YES;
            affectedAreaValueTextView.allowsEditingTextAttributes = NO;
            affectedAreaValueTextView.scrollEnabled = NO;
            affectedAreaValueTextView.editable = NO;
            affectedAreaValueTextView.selectable = YES;
            affectedAreaValueTextView.delegate = self;
            
        }
//        else if (_isCloudVoiceExpress && _serviceStatusDetailsViewModel.currentIssueType == SSIssueTypeOngoingIssue){
//            
//            affectedAreaText = @"We\'re sorry, Cloud Voice Express is not available right now. We\'re working to put things right.";
//            [serviceStatusCell updateCellWithServiceStatusDetail:_serviceStatusDetailsViewModel.serviceStatusDetail andAffectedAreaText:affectedAreaText];
//        }
        else {
            affectedAreaText = [_serviceStatusDetailsViewModel getAffectedAreaTextForPotalCode:self.postCode];
            [serviceStatusCell updateCellWithServiceStatusDetail:_serviceStatusDetailsViewModel.serviceStatusDetail andAffectedAreaText:affectedAreaText];
        }
        
        return serviceStatusCell;
    }
    else if (indexPath.section == 1) //4G assure
    {
        BTPublicWifiUpgradeTableViewCell *upgradeCell = [tableView dequeueReusableCellWithIdentifier:@"BTPublicWifiUpgradeTableViewCell" forIndexPath:indexPath];
        [upgradeCell setupCellForContext:BTPublicWifiUpgradeType4GAssure];
        return upgradeCell;
    }
    else if (indexPath.section == 2) // Hub upgrade
    {
        if(indexPath.row == 0) {
            BTUnableToDetectHubTableViewCell *unableToDetectCell = [tableView dequeueReusableCellWithIdentifier:@"BTUnableToDetectHubTableViewCell" forIndexPath:indexPath];
            [unableToDetectCell setupCellForContext:BTUnableToDetect4GAssure];
            // set delegate
            return unableToDetectCell;
        } else {
            BTPublicWifiUpgradeTableViewCell *upgradeCell = [tableView dequeueReusableCellWithIdentifier:@"BTPublicWifiUpgradeTableViewCell" forIndexPath:indexPath];
            [upgradeCell setupCellForContext:BTPublicWifiUpgradeType4GUpgradeHub];
            return upgradeCell;
        }
    }
    else if (indexPath.section == 3) // 4G status
    {
        BTNetworkIssueTableViewCell *issueCell = [tableView dequeueReusableCellWithIdentifier:@"BTNetworkIssueTableViewCell" forIndexPath:indexPath];
        issueCell.postcode.text = _postCode;
        issueCell.hidden = YES;
        //issueCell.BTNetworkIssueTableViewCellDelegate = self;
        return issueCell;
    }
    else if (indexPath.section == 4)
    {
        BTServiceStatusTakeActionTableViewCell *serviceStatusTakeActionCell = [tableView dequeueReusableCellWithIdentifier:@"BTServiceStatusTakeActionTableViewCell" forIndexPath:indexPath];
        [serviceStatusTakeActionCell updateCellWithTitleText:[[_serviceStatusDetailsViewModel.arrayOfActionList objectAtIndex:indexPath.row] valueForKey:@"title"]];
        [serviceStatusTakeActionCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        serviceStatusTakeActionCell.serviceStatusTakeActionTableViewCellDelegate = self;
        return serviceStatusTakeActionCell;
    }
    else {
        MoreAppsTableViewCell *moreAppCell = [[[NSBundle mainBundle] loadNibNamed:@"MoreAppsTableViewCell" owner:nil options:nil] objectAtIndex:0];
        moreAppCell.selectionStyle = UITableViewCellSelectionStyleNone;
        moreAppCell.masterView.backgroundColor = [UIColor whiteColor];
        moreAppCell.backgroundColor = [UIColor colorWithRed:0.933 green:0.933 blue:0.933 alpha:1];
        moreAppCell.titleLabel.text = @"BT Cloud Voice Express";
        moreAppCell.descriptionLabel.text = @"The easy way to use your BT Cloud Voice Express service on the go.";
        [moreAppCell.btnMoreAppLogo setImage:[UIImage imageNamed:@"BTCloudVoiceAppLogo"] forState:UIControlStateNormal];
        [moreAppCell.actionButton setTitle:[NSString stringWithFormat:@"Download"] forState:UIControlStateNormal];
        [moreAppCell.actionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [moreAppCell.actionButton setTitleColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1] forState:UIControlStateNormal];
//        moreAppCell.containerButtonView.backgroundColor = [UIColor clearColor];
//        moreAppCell.innerButtonView.backgroundColor = [UIColor blueColor];
//        moreAppCell.innerButtonView.backgroundColor = [UIColor colorWithRed:0.392 green:0.0 blue:0.67 alpha:1];
//        moreAppCell.masterView.layer.cornerRadius = 7.0f;
        [moreAppCell.actionButton addTarget:self action:@selector(moreAppButtonDownloadAction:) forControlEvents:UIControlEventTouchUpInside];
        moreAppCell.layer.cornerRadius = 7.0f;

        
        return moreAppCell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (_currentServiceType == BTServiceTypeBroadband && section == 2 && _upgradeRows > 0)
    {
        BTServiceStatusActionsHeaderView *serviceStatusActionHeaderView = [[[NSBundle mainBundle] loadNibNamed:@"BTServiceStatusActionsHeaderView" owner:nil options:nil] objectAtIndex:0];
        [serviceStatusActionHeaderView updateTitleLabelWithTitleText:_serviceStatusDetailsViewModel.moreString];
        return serviceStatusActionHeaderView;
        
        //return nil;
    }
    if (section == 4)
    {
        BTServiceStatusActionsHeaderView *serviceStatusActionHeaderView = [[[NSBundle mainBundle] loadNibNamed:@"BTServiceStatusActionsHeaderView" owner:nil options:nil] objectAtIndex:0];
        [serviceStatusActionHeaderView updateTitleLabelWithTitleText:_serviceStatusDetailsViewModel.takeActionTitleString];
        return serviceStatusActionHeaderView;
    }
    return nil;
}


//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 0) {
//        return 88;
//    }else if (indexPath.section == 1 || indexPath.section == 2)
//    {
//        // upgrade cell
//        if(indexPath.section == 2 && indexPath.row == 0) {
//            return 200.0;
//        } else if (indexPath.section == 2 && indexPath.row == 1) {
//            return 320.0;
//        }
//        return 320.0;
//    } else if(indexPath.section == 3) {
//        return _issueRowHeight;
//    } else if(indexPath.section == 3) {
//        return 53.0;
//    } else if(indexPath.section == 4){
//        return tableView.rowHeight;
//    }
//    else {
//        if (_isCloudVoiceExpress && _serviceStatusDetailsViewModel.currentIssueType != SSIssueTypeOngoingIssue) {
//            return 161;
//        }
//        else return 0;
//    }
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat height = CGFLOAT_MIN;
    if (section == 0) {
        if (_detailRows > 0) {
            height = 1.0f;
        }
    } else if (section == 1) {
        if (_resilientRows > 0) {
            height = 10.0f;
        }
    } else if (section == 2) {
        if (_upgradeRows > 0) {
            height = 40.0f;
        }
    } else if (section == 3) {
        if (_issueRows > 0) {
            height = 10.0f;
        }
    } else if (section == 4){
        height = 40.0f;
    } else
        height = 0.0f;

    return height;
}



-(void)updateDetailText {
    NSString *detailMessage = @"Our checks show no broadband problems in your area.";
    
    if(_connectedBy4GAssure) {
        detailMessage = @"4G Assure is keeping you online. When your broadband's up and running, we'll switch you to it automatically.";
    } else if(_has4GAssure) {
        detailMessage = [detailMessage stringByAppendingString:@"Make sure 4G Assure is plugged in to stay connected automatically if broadband fails."];
    } else if(_has4GAssure && _noNetworkAvailable) {
        detailMessage = @"We're really sorry, broadband and 4G are not available right now. We're working to put things right.";
    } else if(_noNetworkAvailable) {
        detailMessage = @"We're really sorry, broadband is not available right now. We're working to put things right.";
    }
    
    BTServiceStatusTableViewCell *serviceStatusCell = [self.serviceStatusDetailsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [serviceStatusCell updateCellWithServiceStatusDetail:_serviceStatusDetailsViewModel.serviceStatusDetail andAffectedAreaText:detailMessage];
}

-(void)updateDetailTextNullSerialNumber {
    NSString *detailMessage = @"We're having trouble detecting your hub right now, which means you won't be able to see your service status. \n\nPlease note, if you have a hub older than the Business Hub 5 or the Business Smart Hub, you'll need to upgrade.";
    
    BTServiceStatusTableViewCell *serviceStatusCell = [self.serviceStatusDetailsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [serviceStatusCell updateCellWithServiceStatusDetail:_serviceStatusDetailsViewModel.serviceStatusDetail andAffectedAreaText:detailMessage];
}

#pragma mark - TextView delegate

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange interaction:(UITextItemInteraction)interaction
{
    NSRange range1 = NSMakeRange(85,20);//[text rangeOfString:@"Check your broadband"];
    BOOL allowable = NO;
    if (characterRange.location == range1.location) {
        allowable = YES;
    }
    if (URL) {
        if (_serviceStatusDetailsViewModel.currentIssueType == SSIssueTypeNone) {
            [self trackOmniClickForCVEwithPageName:OMNIPAGE_SERVICE_STATUS_CVE_WORKING_WELL andLink:@"Check your broadband"];
        }
        else {
            [self trackOmniClickForCVEwithPageName:OMNIPAGE_SERVICE_STATUS_CVE_WORKING_WELL_PREVIOUS_ISSUE andLink:@"Check your broadband"];
        }
        
        if (_isFromAccountFlow) {
            if (self.tabBarController) {
                UITabBarController *tabbar = self.tabBarController;
                BTHomeNavigationController *moreNavCon = [tabbar.viewControllers objectAtIndex:moreTag];
                [moreNavCon popToRootViewControllerAnimated:NO];
                [moreNavCon.homeNavigationControllerDelegate redirectToServiceStatusScreenForHomeNavigationController:moreNavCon];
                [tabbar setSelectedViewController:moreNavCon];
                [self.navigationController popViewControllerAnimated:NO];
            }
            _isFromAccountFlow = NO;
        }
        else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    return allowable;
}
- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange
{
    NSRange range1 = NSMakeRange(85,20);//[text rangeOfString:@"Check your broadband"];
    BOOL allowable = NO;
    if (characterRange.location == range1.location) {
        allowable = YES;
    }
    if (URL) {
        if (_serviceStatusDetailsViewModel.currentIssueType == SSIssueTypeNone) {
            [self trackOmniClickForCVEwithPageName:OMNIPAGE_SERVICE_STATUS_CVE_WORKING_WELL andLink:@"Check your broadband"];
        }
        else {
            [self trackOmniClickForCVEwithPageName:OMNIPAGE_SERVICE_STATUS_CVE_WORKING_WELL_PREVIOUS_ISSUE andLink:@"Check your broadband"];
        }
        
        if (_isFromAccountFlow) {
            if (self.tabBarController) {
                UITabBarController *tabbar = self.tabBarController;
                BTHomeNavigationController *moreNavCon = [tabbar.viewControllers objectAtIndex:moreTag];
                [moreNavCon popToRootViewControllerAnimated:NO];
                [moreNavCon.homeNavigationControllerDelegate redirectToServiceStatusScreenForHomeNavigationController:moreNavCon];
                [tabbar setSelectedViewController:moreNavCon];
                [self.navigationController popViewControllerAnimated:NO];
            }
            _isFromAccountFlow = NO;
        }
        else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    return allowable;
}

#pragma mark - BTServiceStatusTakeActionTableViewCellDelegate Methods
- (void)userPressedCellForServiceStatusTakeActionCell:(BTServiceStatusTakeActionTableViewCell *)serviceStatusTakeActionTableViewCell
{
    NSIndexPath *indexPath = [self.serviceStatusDetailsTableView indexPathForCell:serviceStatusTakeActionTableViewCell];
    NSInteger type = [[[_serviceStatusDetailsViewModel.arrayOfActionList objectAtIndex:indexPath.row] valueForKey:@"type"] integerValue];
    if (type == SSActionTypeReportAFault || type == SSActionTypeReportFaultToTrackProgress)
    {
        DDLogInfo(@"Report a fault button pressed");
        [self checkChatAvailabilityAction];
        if (self.currentServiceType == BTServiceTypePSTN) {
            if (_isCloudVoiceExpress && _serviceStatusDetailsViewModel.currentIssueType == SSIssueTypeNone) {
                [self trackOmniClickForCVEwithPageName:OMNIPAGE_SERVICE_STATUS_CVE_WORKING_WELL andLink:OMNICLICK_SERVICE_STATUS_CVE_REPORT_FAULT];
            }
            else if (_isCloudVoiceExpress && _serviceStatusDetailsViewModel.currentIssueType == SSIssueTypeRecentlyResolvedIssue){
                [self trackOmniClickForCVEwithPageName:OMNIPAGE_SERVICE_STATUS_CVE_WORKING_WELL_PREVIOUS_ISSUE andLink:OMNICLICK_SERVICE_STATUS_CVE_REPORT_FAULT];
            }
            else if (_isCloudVoiceExpress && _serviceStatusDetailsViewModel.currentIssueType == SSIssueTypeOngoingIssue){
                [self trackOmniClickForCVEwithPageName:OMNIPAGE_SERVICE_STATUS_CVE_WORKING_WELL_PREVIOUS_ISSUE andLink:OMNICLICK_SERVICE_STATUS_CVE_REPORT_FAULT];
            }
            else {
                [self trackOmniClick:OMNICLICK_SERVICE_STATUS_REPORT_FAULT onPage:_pageNameForOmniture];
            }
        } else if (self.currentServiceType == BTServiceTypeBroadband) {
            [self trackOmniClick:OMNICLICK_SERVICE_STATUS_FIX_REPORTAFAULT];
        }
    }
    else if (type == SSActionTypePopularFAQs)
    {
        DDLogInfo(@"Browse popular FAQs pressed");
        if (self.currentServiceType == BTServiceTypePSTN) {
            if (_isCloudVoiceExpress && _serviceStatusDetailsViewModel.currentIssueType == SSIssueTypeNone) {
                [self trackOmniClickForCVEwithPageName:OMNIPAGE_SERVICE_STATUS_CVE_WORKING_WELL andLink:OMNICLICK_SERVICE_STATUS_CVE_FAQ];
            }
            else if (_isCloudVoiceExpress && _serviceStatusDetailsViewModel.currentIssueType == SSIssueTypeRecentlyResolvedIssue){
                [self trackOmniClickForCVEwithPageName:OMNIPAGE_SERVICE_STATUS_CVE_WORKING_WELL_PREVIOUS_ISSUE andLink:OMNICLICK_SERVICE_STATUS_CVE_FAQ];
            }
            else if (_isCloudVoiceExpress && _serviceStatusDetailsViewModel.currentIssueType == SSIssueTypeOngoingIssue){
                [self trackOmniClickForCVEwithPageName:OMNIPAGE_SERVICE_STATUS_CVE_WORKING_WELL_PREVIOUS_ISSUE andLink:OMNICLICK_SERVICE_STATUS_CVE_FAQ];
            }
            else {
                [self trackOmniClick:OMNICLICK_SERVICE_STATUS_FAQ onPage:_pageNameForOmniture];
            }
        } else if (self.currentServiceType == BTServiceTypeBroadband) {
            [self trackOmniClick:OMNICLICK_SERVICE_STATUS_FIX_FAQS];
        }
        
        if (_isCloudVoiceExpress) {
            [AppManager openURL:kPhonelineFAQUrl];
        }
        else {
            BTFAQsViewController *faqViewController = [[BTFAQsViewController alloc] init];
            [self.navigationController pushViewController:faqViewController animated:YES];
//            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:faqViewController];
//            [self presentViewController:navController animated:YES completion:nil];
        }
    }
    else if(type == SSActionTypeHubHealthcheck)
    {
        DDLogInfo(@"Hub healthcheck pressed");
        
        if([_serviceStatusDetailsViewModel resilientHubAssets]) {
            BTHubHealthcheckViewController *healthcheckViewController = [[BTHubHealthcheckViewController alloc] initWithAssets:[_serviceStatusDetailsViewModel resilientHubAssets]];
            //UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:healthcheckViewController];
            //[self presentViewController:navController animated:YES completion:nil];
            
              // UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:healthcheckViewController];
                [self.navigationController pushViewController:healthcheckViewController animated:YES];
            
               //[self presentViewController:navController animated:YES completion:nil];
               //UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:faqViewController];
        
               //[self presentViewController:navController animated:YES completion:nil];

            
            
            if (_isCloudVoiceExpress && _serviceStatusDetailsViewModel.currentIssueType == SSIssueTypeNone) {
                [self trackOmniClickForCVEwithPageName:OMNIPAGE_SERVICE_STATUS_CVE_WORKING_WELL andLink:OMNICLICK_SERVICE_STATUS_FIX_CVE_HUBHEALTHCHECK];
            }
            else if (_isCloudVoiceExpress && _serviceStatusDetailsViewModel.currentIssueType == SSIssueTypeRecentlyResolvedIssue){
                [self trackOmniClickForCVEwithPageName:OMNIPAGE_SERVICE_STATUS_CVE_WORKING_WELL_PREVIOUS_ISSUE andLink:OMNICLICK_SERVICE_STATUS_FIX_CVE_HUBHEALTHCHECK];
            }
            else if (_isCloudVoiceExpress && _serviceStatusDetailsViewModel.currentIssueType == SSIssueTypeOngoingIssue){
                [self trackOmniClickForCVEwithPageName:OMNIPAGE_SERVICE_STATUS_CVE_WORKING_WELL_PREVIOUS_ISSUE andLink:OMNICLICK_SERVICE_STATUS_FIX_CVE_HUBHEALTHCHECK];
            }
            else {
                [self trackOmniClick:OMNICLICK_SERVICE_STATUS_FIX_HUBHEALTHCHECK];
            }
        }
    }
}

#pragma mark - BT4GAssureTableViewDelegate methods

-(void)userPressedCellFor4GAssure:(BT4GAssureTableViewCell *)BT4GAssureCell {
    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];

    viewController.targetURLType = BTTargetURLType4GAssureOrder;

    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:NO andIsNeedCloseButton:YES];

    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{

    }];
    [self trackOmniClick:OMNICLICK_SERVICE_STATUS_ORDERNOW];
    //[AppManager openURL:k4GAssureOrderNowUrl];
}

#pragma mark - BTUpgradeHubTableViewDelegate methods
-(void)userPressedCellForUpgradeHub:(BTUpgradeHubTableViewCell *)BTUpgradeHubCell {
    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
    
    viewController.targetURLType = BTTargetURLTypeUpgradeHub;
    
    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:NO andIsNeedCloseButton:YES];
    
    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{
        
    }];
    
//    SFSafariViewController *browserView = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:k4GAssureOrderNowUrl]];
//    [browserView setModalPresentationStyle:UIModalPresentationPopover];
//    [[UIApplication sharedApplication].keyWindow.rootViewController showViewController:browserView sender:nil];
    
    [self trackOmniClick:OMNICLICK_SERVICE_STATUS_UPGRADE];
//    [AppManager openURL:kUpgradeYourHubUrl];
}

#pragma mark - RetryView Methods
- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow {
    
    _headerWithStatusColorView.hidden = NO;

    if (!_retryView)
    {
        _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
        _retryView.translatesAutoresizingMaskIntoConstraints = NO;
        _retryView.retryViewDelegate = self;
        
        [self.view addSubview:_retryView];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_headerWithStatusColorView  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    }
    
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
    
}

- (void)showRetryViewForNoBillingRole {
    //_headerWithStatusColorView.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
    _headerWithStatusColorView.hidden = NO;
    [self.view bringSubviewToFront:_headerWithStatusColorView];
    [self.view bringSubviewToFront:_backButton];
    if (!_retryView)
    {
        _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
        _retryView.translatesAutoresizingMaskIntoConstraints = NO;
        _retryView.retryViewDelegate = self;
        
        [self.view addSubview:_retryView];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_headerWithStatusColorView  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    }
    NSString *errMsg = @"Please contact your administrator";
    NSString *detailMsg = @"You'll need billing access rights to continue.\n\nIf your account is currently being approved, please try again later.";
    [_retryView updateRetryViewWithInternetStrip:NO TryAgain:NO errorHeadline:errMsg errorDetail:detailMsg];
}

- (void)hideRetryView {
    
    // (SD) Hide UI elements related to retry.
    
    if (_retryView)
    {
        _retryView.retryViewDelegate = nil;
        [_retryView setHidden:YES];
        [_retryView removeFromSuperview];
        _retryView = nil;
    }
}

#pragma mark - Retry view delegate methods
- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    [self makeAPIcall];
}

#pragma mark- DLMServiceStatusDetailsScreen Delegate methods

- (void)successfullyFetchedServiceStatusDetailsScreen:(DLMServiceStatusDetailsScreen *)serviceStatusDetailScreen{
    
    if (_currentServiceType == BTServiceTypePSTN) {
        if (_isCloudVoiceExpress) {
            [self trackPageForIssueTypeOfCloudVoiceExpress:_serviceStatusDetailsViewModel.currentIssueType];
        }
        else {
            [self trackPageForIssueType:_serviceStatusDetailsViewModel.currentIssueType];
        }
//        [self updateUIAfterSuccessfulApiCall];
    }
    [self updateUIAfterSuccessfulApiCall];
    [_serviceStatusDetailsTableView reloadData];
}

- (void)serviceStatusDetailsScreen:(DLMServiceStatusDetailsScreen *)serviceStatusDetailScreen failedToFetchServiceDetailWithWebServiceError:(NLWebServiceError *)webServiceError{
    
    if (_currentServiceType == BTServiceTypePSTN) {
        [self trackPage:OMNIPAGE_SERVICE_STATUS_CHECKING_SERVICE_FAILED withKeyTask:OMNI_KEYTASK_SERVICE_STATUS_SCAN_FAILED];
    }
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
    if (webServiceError && [webServiceError isKindOfClass:[NLVordelWebServiceError class]]) {
        NLVordelWebServiceError *error = (NLVordelWebServiceError*)webServiceError;
        if (error.errorCode && [error.errorCode isEqualToString:@"MB5-01"]) {
            // interpret this as null serial number
            errorHandled = YES;
            [self updateUIforNullHubSerialNumber];
        } else if (error.errorCode && [error.errorCode isEqualToString:@"MBAF2-01"]) {
            // no admin/billing
            errorHandled = YES;
            [self updateUIAfterFailedApiCall];
            [self showRetryViewForNoBillingRole];
        }
    }
    
    if(!errorHandled)
    {
        [self updateUIAfterFailedApiCall];
        [self showRetryViewWithInternetStrip:NO];
        if (webServiceError && ![webServiceError isKindOfClass:[NLVordelWebServiceError class]]) {
            if (_currentServiceType == BTServiceTypePSTN) {
                if (_isCloudVoiceExpress) {
                    [AppManager trackWebServiceError:webServiceError FromAPI:@"GetServiceStatus" OnPage:OMNIPAGE_SERVICE_STATUS_CVE_SOMETHING_WENT_WRONG];
                } else {
                     [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_SERVICE_STATUS_SCAN_INPROGRESS];
                }
            }
        }
    }
}

- (void)liveChatAvailableForFaultWithScreen:(DLMServiceStatusDetailsScreen *)serviceStatusDetailsScreen
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
    
    if (_isCloudVoiceExpress) {
        viewController.targetURLType = BTTargetURLTypeCloudVoiceFaultsChatScreen;
    }
    else {
        viewController.targetURLType = BTTargetURLTypeFaultsChatScreen;
    }

    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:NO andIsNeedCloseButton:YES];

    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{

    }];
//    [AppManager openURL:kFaultsChatScreenUrl];
}

- (void)serviceStatusDetailsScreen:(DLMServiceStatusDetailsScreen *)serviceStatusDetailsScreen liveChatCurrentlyNotAvailableWithSpecificCategory:(NSArray *)specificCategory generalCategory:(NSArray *)generalCategory andCurrentTime:(NSString *)currentTime
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BTHelpAndSupportViewController *helpAndSupportController = [BTHelpAndSupportViewController getHelpAndSupportViewController];
    helpAndSupportController.isLaunchingFromHomeScreen = NO;
    helpAndSupportController.isDataAlreadyFetched = YES;
    helpAndSupportController.specificCategory = specificCategory;
    helpAndSupportController.generalCategory = generalCategory;
    helpAndSupportController.currentTime = currentTime;
    
    [self.navigationController pushViewController:helpAndSupportController animated:YES];
}

- (void)serviceStatusDetailsScreen:(DLMServiceStatusDetailsScreen *)serviceStatusDetailsScreen failedToCheckLiveChatAvailabilityWithWebServiceError:(NLWebServiceError *)error
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if(errorHandled == NO)
    {
        [self showAlertWithTitle:@"Message" andMessage:kDefaultErrorMessage];
        if (_isCloudVoiceExpress) {
            [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_SERVICE_STATUS_CVE_SOMETHING_WENT_WRONG];
        }
        else {
            [AppManager trackGenericAPIErrorOnPage:_pageNameForOmniture];
        }
    }
}

#pragma mark - Workflow Delegate methods
- (void)successfullyFetchedWorkflowStatus:(DLMServiceStatusDetailsScreen*)serviceStatusDetailScreen {
    _serviceStatusDetailsViewModel = serviceStatusDetailScreen;
    _serviceStatusDetailsViewModel.postcode = _postCode;
    [self fetchEEOAuthToken];
    [_serviceStatusDetailsTableView reloadData];
    [self updateUIAfterSuccessfulWorkflowCall];
}

- (void)serviceStatusDetailsScreen:(DLMServiceStatusDetailsScreen *)serviceStatusDetailScreen failedToFetchWorkFlowStatusWithWebServiceError:(NLWebServiceError *)webServiceError {
    // unused method?
    [self updateUIAfterFailedApiCall];
    if (_isCloudVoiceExpress) {
        [AppManager trackWebServiceError:webServiceError FromAPI:@"CheckWorkflowStatus" OnPage:OMNIPAGE_SERVICE_STATUS_CVE_SOMETHING_WENT_WRONG];
        //[AppManager trackGenericAPIErrorOnPage:OMNIPAGE_SERVICE_STATUS_CVE_SOMETHING_WENT_WRONG];
    }
    else {
        [AppManager trackGenericAPIErrorOnPage:[self pageNameForOmnitureTracking]];
    }
}

#pragma mark - Network status delegate method
-(void)displayNetworkStatusIssue:(EENetworkStatus*)networkStatus {
    _networkStatus = networkStatus;
    _issueRowHeight = 150;
    NSIndexPath *issueIndexPath = [NSIndexPath indexPathForRow:0 inSection:3];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:issueIndexPath, nil];
    [_serviceStatusDetailsTableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
    BTNetworkIssueTableViewCell *issueCell = [_serviceStatusDetailsTableView cellForRowAtIndexPath:issueIndexPath];
    issueCell.hidden = NO;
}

#pragma mark - Omniture Event tracking
- (void)trackPage:(NSString *)pageName andNeedToSendLoginStatus:(BOOL)sendLoginStatus
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    if(sendLoginStatus)
        [data setValue:@"Logged In" forKey:kOmniLoginStatus];

    [OmnitureManager trackPage:pageName withContextInfo:data];
    
}

- (void)trackPage:(NSString *)pageName withKeyTask:(NSString *)keyTask
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    [data setValue:keyTask forKey:kOmniKeyTask];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackOmniClick:(NSString *)linkTitle onPage:(NSString *)pageName
{
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle]];
}

- (void)trackPageForIssueType:(SSIssueType)issueType
{
    if(issueType == SSIssueTypeNone)
    {
        _pageNameForOmniture = [NSString stringWithFormat:@"%@%@:Service details",OMNIPAGE_SERVICE_STATUS_GOOD_SERVICE,_selectedServiceName];
        [self trackPage:_pageNameForOmniture andNeedToSendLoginStatus:NO];
    }
    else if(issueType == SSIssueTypeRecentlyResolvedIssue)
    {
        _pageNameForOmniture = [NSString stringWithFormat:@"%@%@:Service details",OMNIPAGE_SERVICE_STATUS_RECENTLY_FIXED_SERVICE,_selectedServiceName];
        [self trackPage:_pageNameForOmniture andNeedToSendLoginStatus:NO];
    }
    else
    {
        _pageNameForOmniture = [NSString stringWithFormat:@"%@%@:Service details",OMNIPAGE_SERVICE_STATUS_AFFECTED_SERVICE,_selectedServiceName];
        [self trackPage:_pageNameForOmniture andNeedToSendLoginStatus:NO];
    }
    
}

- (void)trackPageForIssueTypeOfCloudVoiceExpress:(SSIssueType)issueType
{
    if(issueType == SSIssueTypeNone)
    {
        [self trackPage:OMNIPAGE_SERVICE_STATUS_CVE_WORKING_WELL andNeedToSendLoginStatus:NO];
    }
    else if(issueType == SSIssueTypeRecentlyResolvedIssue)
    {
        [self trackPage:OMNIPAGE_SERVICE_STATUS_CVE_WORKING_WELL_PREVIOUS_ISSUE andNeedToSendLoginStatus:NO];
    }
    else
    {
        [self trackPage:OMNIPAGE_SERVICE_STATUS_CVE_ON_GOING_ISSUE andNeedToSendLoginStatus:NO];
    }
    
}

#pragma mark - New Omniture Methods

- (NSString*)pageNameForOmnitureTracking
{
    NSString *pageName = OMNIPAGE_SERVICE_STATUS_YOUR_SERVICE;
    return pageName;
}

- (void)trackPageForOmniture
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = [self pageNameForOmnitureTracking];;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    NSString *hubType = [_serviceStatusDetailsViewModel.hubDetails objectForKey:@"Product Class"];
    [data setValue:hubType forKey:kOmniHubType];
   
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackOmniClick:(NSString *)linkTitle
{
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",[self pageNameForOmnitureTracking],linkTitle]];
}

- (void)trackPageForOmnitureWithKeyTask:(NSString *)keyTask
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = [self pageNameForOmnitureTracking];

    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    NSString *hubType = [_serviceStatusDetailsViewModel.hubDetails objectForKey:@"Product Class"];
    [data setValue:hubType forKey:kOmniHubType];
   
    [data setValue:keyTask forKey:kOmniKeyTask];
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackPageForOmnitureWithContentViewed:(NSString *)contentViewed
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = [self pageNameForOmnitureTracking];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    if(_serviceStatusDetailsViewModel.hubDetails) {
        NSString *hubType = [_serviceStatusDetailsViewModel.hubDetails objectForKey:@"Product Class"];
        [data setValue:hubType forKey:kOmniHubType];
    }
    [data setValue:contentViewed forKey:kOmniContentViewed];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackOmniClickForCVEwithPageName:(NSString *)pageName andLink:(NSString *)linkTitle
{
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle]];
}



@end

