//
//  BTServiceStatusDetailsViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 2/22/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BTServiceStatusDetailsViewController;

typedef NS_ENUM(NSInteger, BTServiceType) {
    
    BTServiceTypePSTN,
    BTServiceTypeBroadband
};

@interface BTServiceStatusDetailsViewController : UIViewController

+ (BTServiceStatusDetailsViewController *)getServiceStatusDetailsViewController;

@property (nonatomic, assign) BTServiceType currentServiceType;
@property (nonatomic, strong) NSString *serviceID;
@property (nonatomic, strong) NSString *postCode;
@property (nonatomic, strong) NSString *productName;
@property (nonatomic,strong) NSString *selectedServiceName;
@property (nonatomic,assign) BOOL pointToModelAAlways;
@property (nonatomic,assign) BOOL isFromAccountFlow;

@property (strong, nonatomic) IBOutlet UIView *serviceStatusDetailsContainerView;
@property (weak, nonatomic) IBOutlet UIView *secondHeaderView;

@end
