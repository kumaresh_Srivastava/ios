//
//  EENetworkStatusViewController.h
//  BTBusinessApp
//
//  Created by Jim Purvis on 18/05/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EENetworkStatus.h"

@interface EENetworkStatusViewController : UIViewController

-(id)initWithNetworkStatus:(EENetworkStatus*)networkStatus;

@end
