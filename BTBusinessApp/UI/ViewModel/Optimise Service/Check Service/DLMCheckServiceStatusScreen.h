//
//  DLMCheckServiceStatusScreen.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 23/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "DLMObject.h"
#import "BTBroadbandAndPhoneService.h"

@class DLMCheckServiceStatusScreen;

@class  NLWebServiceError;
@protocol DLMCheckServiceStatusScreenDelegate <NSObject>

- (void)successfullyFetchedCheckServiceStatusDataOnDLMCheckServiceStatusScreenn:(DLMCheckServiceStatusScreen *)serviceStatusScreen;

- (void)serviceStatusScreen:(DLMCheckServiceStatusScreen *)serviceStatusScreen failedToFetchServiceStatusDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end


@interface DLMCheckServiceStatusScreen : DLMObject

@property (nonatomic, copy) NSArray<BTBroadbandAndPhoneService*> *servicesArray;

@property (nonatomic, weak) id <DLMCheckServiceStatusScreenDelegate> serviceStatusScreenDelegate;

@property (nonatomic) BOOL forSpeedTest;

- (void)fetchServiceStatusDetailsWithBACId:(NSString *)bacId;
- (void)cancelServiceStatusAPIRequest;


@end
