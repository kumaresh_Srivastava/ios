//
//  BTHubHealthcheckViewController.m
//  BTBusinessApp
//

#import "BTHubHealthcheckViewController.h"
#import "DLMFAQsScreen.h"
#import "AppManager.h"
#import "AppConstants.h"
#import "OmnitureManager.h"
#import "BTHubHealthCheckTableViewCell.h"
#import "DLMHealthcheckScreen.h"
#import "BTSelectHubTableViewCell.h"
#import "NLGetHubStatus.h"
#import "BTResilientHubAsset.h"
#import "NLCheckWorkflowStatus.h"

@interface BTHubHealthcheckViewController ()<UITableViewDelegate, UITableViewDataSource, ChangeHubHealthcheckDelegate> {
    
}

@property (nonatomic, strong) UITableView *healthcheckTableView;
@property (nonatomic, strong) DLMHealthcheckScreen *healthcheckScreenViewModel;
@property (nonatomic, strong) NSMutableArray<BTResilientHubAsset*> *resilientHubAssets;
@property (nonatomic, strong) NLGetHubStatus *getHubStatus;
@property (nonatomic) NLCheckWorkflowStatus *checkWorkflowStatusWebService;
@property (nonatomic) int hubIndex;

@end

@implementation BTHubHealthcheckViewController

-(id)initWithAssets:(NSMutableArray *)assets {
    self = [super init];
    if (self) {
        _resilientHubAssets = assets;
        
    }
    
    _hubIndex = 0;
    
    return self;
}

-(void) viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    [self createInitialUI];
    
   // self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleDone target:self action:@selector(closeButtonAction:)];
    
    _healthcheckScreenViewModel = [[DLMHealthcheckScreen alloc] init];
    [_healthcheckScreenViewModel createHealthcheckData];
    
    [self createHealthcheckTableView];
    [_healthcheckTableView reloadData];
    [self trackPageForOmniture];
}

#pragma mark - Private methods

- (void)createInitialUI
{
    //CGSize screenSize = [UIScreen mainScreen].bounds.size;
    
    UILabel *pageTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 208, 40)];
    pageTitleLabel.font = [UIFont fontWithName:kBtFontBold size:20.0];
    pageTitleLabel.textColor = [UIColor whiteColor];
    pageTitleLabel.textAlignment = NSTextAlignmentCenter;
    pageTitleLabel.text = @"Business broadband";
    
    NSString *subTitle = @"";
    if (self.resilientHubAssets && self.resilientHubAssets.count) {
        subTitle = self.resilientHubAssets[0].serviceID?self.resilientHubAssets[0].serviceID:@"";
    }
    
//    UILabel *pageSubTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 19, 208, 17.5)];
//    pageSubTitleLabel.font = [UIFont fontWithName:kBtFontRegular size:13.0];
//    pageSubTitleLabel.textColor = [UIColor whiteColor];
//    pageSubTitleLabel.textAlignment = NSTextAlignmentCenter;
//    pageSubTitleLabel.text = subTitle;
    
    UIView *titlesView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 208, 40)];
//    [titlesView addSubview:pageSubTitleLabel];
    [titlesView addSubview:pageTitleLabel];
    
    [self.navigationItem setTitleView:titlesView];
}

- (void)createHealthcheckTableView
{
    _healthcheckTableView = [[UITableView alloc] init];
    _healthcheckTableView.dataSource = self;
    _healthcheckTableView.delegate = self;
    
    _healthcheckTableView.allowsSelection = NO;
    _healthcheckTableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_healthcheckTableView];
    
    NSDictionary *views = [[NSDictionary alloc] initWithObjectsAndKeys:_healthcheckTableView, @"healthcheckTableView", nil];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[healthcheckTableView]|" options:NSLayoutFormatAlignAllCenterY metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[healthcheckTableView]|" options:NSLayoutFormatAlignAllCenterX metrics:nil views:views]];
    
    _healthcheckTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
    _healthcheckTableView.rowHeight = 280;
    
    UINib *headerCell = [UINib nibWithNibName:@"BTSelectHubTableViewCell" bundle:nil];
    [_healthcheckTableView registerNib:headerCell forCellReuseIdentifier:@"BTSelectHubTableViewCell"];
    
    UINib *dataCell = [UINib nibWithNibName:@"BTHubHealthCheckTableViewCell" bundle:nil];
    [_healthcheckTableView registerNib:dataCell forCellReuseIdentifier:@"BTHubHealthCheckTableViewCell"];
}

- (void)closeButtonAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)showNoNetworkAlert
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kNoConnectionTitle message:kNoConnectionMessage preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:^{}];
}

#pragma mark - TableView Datasource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return 1;
    } else {
        if(_hubIndex == 0) {
            return _healthcheckScreenViewModel.smartHubHealthcheckData.count;
        } else {
            return _healthcheckScreenViewModel.businessHubHealthcheckData.count;
        }
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0) {
        BTSelectHubTableViewCell *selectHubCell = [tableView dequeueReusableCellWithIdentifier:@"BTSelectHubTableViewCell"];
        selectHubCell.changeHubHealthcheckDelegate = self;
        selectHubCell.hubSegmentedControl.selectedSegmentIndex = _hubIndex;
        return selectHubCell;
    }
    else{
        BTHubHealthCheckTableViewCell *healthcheckCell = [tableView dequeueReusableCellWithIdentifier:@"BTHubHealthCheckTableViewCell" forIndexPath:indexPath];
        
        if(_hubIndex == 0) {
            NSDictionary *healthCheckData = (NSDictionary*) _healthcheckScreenViewModel.smartHubHealthcheckData[indexPath.row];
            [healthcheckCell updateCellWithHealthcheckCellData:healthCheckData];
        } else {
            NSDictionary *healthCheckData = (NSDictionary*) _healthcheckScreenViewModel.businessHubHealthcheckData[indexPath.row];
            [healthcheckCell updateCellWithHealthcheckCellData:healthCheckData];
        }
        
        return healthcheckCell;
    }
}

#pragma mark - TableView Delegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // do something
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if(indexPath.section == 0) {
        return 289
        ;
    } else {
        return 280;
    }
}

- (void)changeHubHealthcheck:(int)index {
    NSLog(@"Updating HHH view");
    _hubIndex = index;
    [_healthcheckTableView reloadData];
    if (index == 0) {
        // smarth hub
        [self trackOmniClick:OMNICLICK_SERVICE_STATUS_HUBSTATUS_HUB6];
    } else if (index == 1) {
        // hub 5
        [self trackOmniClick:OMNICLICK_SERVICE_STATUS_HUBSTATUS_HUB5];
    }
}

#pragma mark - Omniture Methods

- (NSString*)pageNameForOmnitureTracking
{
    NSString *pageName = OMNIPAGE_SERVICE_STATUS_HUB_STATUS;
    return pageName;
}

- (void)trackPageForOmniture
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = [self pageNameForOmnitureTracking];;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackOmniClick:(NSString *)linkTitle
{
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",[self pageNameForOmnitureTracking],linkTitle]];
}

@end
