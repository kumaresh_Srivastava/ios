//
//  DLMHealthcheckScreen.h
//  BTBusinessApp
//

#import <Foundation/Foundation.h>

@interface DLMHealthcheckScreen : NSObject

@property (nonatomic, strong) NSArray *smartHubHealthcheckData;
@property (nonatomic, strong) NSArray *businessHubHealthcheckData;

- (void) createHealthcheckData;

@end
