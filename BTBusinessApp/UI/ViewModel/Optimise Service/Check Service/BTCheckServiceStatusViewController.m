//
//  BTCheckServiceStatusViewController.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 22/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTCheckServiceStatusViewController.h"
#import "BTCheckServiceStatusTableViewCell.h"
#import "DLMCheckServiceStatusScreen.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "NLWebServiceError.h"
#import "BTEmptyDashboardView.h"
#import "BTRetryView.h"
#import "CustomSpinnerView.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "AppManager.h"
#import "BTServiceStatusDetailsViewController.h"
#import "BTBroadbandAndPhoneService.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "SpeedTestViewController.h"
#import "DLMAssetDashboardScreen.h"
#import "AppDelegate.h"
#import "SpeedTestIntroductionViewController.h"

@interface BTCheckServiceStatusViewController ()<DLMCheckServiceStatusScreenDelegate,BTRetryViewDelegate,UITableViewDataSource,DLMAssetDashboardScreenDelegate>
{
    BTEmptyDashboardView *_emptyDashboardView;
    BTRetryView *_retryView;
    
    NSMutableArray *_broadbandUsageArray;
    NSMutableArray *_phoneLineUsageArray;

    BOOL _isEmptyViewNeedToShowForPhoneLine;
    BOOL _isEmptyViewNeedToShowForBroadbandUsage;
    BOOL _isAllBACApiRequestDone;

}


@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) DLMCheckServiceStatusScreen *checkServiceStatusScreenViewModel;
@property (nonatomic,strong)  CustomSpinnerView *loadingView;
@property(nonatomic, assign) BOOL networkRequestInProgress;
@property (nonatomic, assign) BOOL broadbandHubNeededViewVisible;
@property (nonatomic, readwrite) DLMAssetDashboardScreen *assetDashboardScreenModel;

@end

@implementation BTCheckServiceStatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.

    _isEmptyViewNeedToShowForPhoneLine = NO;
    _isEmptyViewNeedToShowForBroadbandUsage = NO;
    _isAllBACApiRequestDone = YES;

    self.edgesForExtendedLayout = UIRectEdgeNone;

    //self.tableView.estimatedRowHeight = 110.0;
    self.tableView.rowHeight = 114.0;
    self.tableView.hidden = YES;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.backgroundColor = [BrandColours colourBtNeutral30];
    self.tableView.separatorColor = [UIColor colorWithRed:(245/255.0) green:(245/255.0) blue:(245/255.0) alpha:1];
    self.tableView.layoutMargins = UIEdgeInsetsZero;

    UINib *dataCell = [UINib nibWithNibName:@"BTCheckServiceStatusTableViewCell" bundle:nil];
    [self.tableView registerNib:dataCell forCellReuseIdentifier:@"BTCheckServiceStatusTableViewCell"];

    self.checkServiceStatusScreenViewModel = [[DLMCheckServiceStatusScreen alloc] init];
    self.checkServiceStatusScreenViewModel.serviceStatusScreenDelegate = self;
    self.checkServiceStatusScreenViewModel.forSpeedTest = _forSpeedTest;

    __weak typeof(self) selfWeak = self;

    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([selfWeak networkRequestInProgress]) {

            dispatch_async(dispatch_get_main_queue(), ^{

                [selfWeak hideLoadingItems:NO];
                selfWeak.navigationItem.rightBarButtonItem.enabled = NO;
                [selfWeak.loadingView startAnimatingLoadingIndicatorView];


            });
        }

        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
            dispatch_async(dispatch_get_main_queue(), ^{

                [selfWeak hideLoadingItems:YES];
                selfWeak.navigationItem.rightBarButtonItem.enabled = YES;
                [selfWeak.loadingView stopAnimatingLoadingIndicatorView];

            });
        }
    }];

    if(self.groupName) {
        //self.groupNameLabel.text = self.groupName;
    }
    else {
        UIFont *titleFont = self.titleLabel.font;
        self.titleLabel.font = [titleFont fontWithSize:20];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        //self.groupNameLabel.hidden = YES;
    }
    
    if(_forSpeedTest)
    {
        self.titleLabel.text = @"Broadband speed test";
    }

    [self fetchServiceStatusScreenDataWithBACId:self.bacID];
    
    [self trackPageForOmniture];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChangeCheckService:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }

}

- (void)viewDidAppear:(BOOL)animated{
    if(self.isSingleBacCUG == YES){
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if(appDelegate.isSingleBacCugBackNavButtonPressed == YES){
            appDelegate.isSingleBacCugBackNavButtonPressed  = NO;
            return;
        }
        if(_isAllBACApiRequestDone == NO){
            // Fetch ALLBAC to see if we receive more accounts
            self.assetDashboardScreenModel = [[DLMAssetDashboardScreen alloc] init];
            self.assetDashboardScreenModel.assetDashboardScreenDelegate = self;
            [self fetchAssetsDashboardAPI];
            self.tableView.hidden = YES;
        }
        _isAllBACApiRequestDone = NO;
    }
}

- (void)statusBarOrientationChangeCheckService:(NSNotification *)notification {
    // handle the interface orientation as needed
    //[self createLoadingView];
    [self performSelector:@selector(createLoadingView) withObject:nil afterDelay:0.0];
}

- (void)viewDidDisappear:(BOOL)animated {

    [super viewDidDisappear:animated];
    [self.checkServiceStatusScreenViewModel cancelServiceStatusAPIRequest];
    self.networkRequestInProgress = NO;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
    

}

- (void)viewWillDisappear:(BOOL)animated{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(self.isSingleBacCUG == YES){
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if (self.isMovingFromParentViewController || self.isBeingDismissed) {
            appDelegate.isSingleBacCugBackNavButtonPressed = YES;
            NSArray *array = [self.navigationController viewControllers];
            if(array.count > 1) {
                id controller = array[1];
                CATransition* transition = [CATransition animation];
                transition.duration = 1.0;
                transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                transition.type = kCATransitionFade; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
                //transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
                [self.navigationController.view.layer addAnimation:transition forKey:nil];
                if([controller isKindOfClass:[SpeedTestIntroductionViewController class]]){
                    [self.navigationController popToViewController:[array objectAtIndex:1] animated:NO];//SpeedTestIntroductionViewController
                } else {
                    [self.navigationController popToRootViewControllerAnimated:NO];
                }
            } else {
                 [self.navigationController popToRootViewControllerAnimated:NO];
            }
        }
        else{
            appDelegate.isSingleBacCugBackNavButtonPressed = NO;
        }
    }else {
        appDelegate.isSingleBacCugBackNavButtonPressed = NO;
    }
}

#pragma mark - Class Methods


+ (BTCheckServiceStatusViewController *) getCheckServiceStatusViewController {

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"OptimiseServiceStatus" bundle:nil];
    BTCheckServiceStatusViewController *controller = (BTCheckServiceStatusViewController *)[storyboard instantiateViewControllerWithIdentifier:@"BTCheckServiceStatusViewController"];

    return controller;

}


#pragma mark - Private Methods

- (NSInteger)getNumberOfRowsInSectionWithSection:(NSInteger)section
{
    if(section == 0)
    {
        if(!_isEmptyViewNeedToShowForBroadbandUsage)
            return _broadbandUsageArray.count;
        else
            if(!_isEmptyViewNeedToShowForPhoneLine)
                return _phoneLineUsageArray.count;
    }
    else
    {
        if(!_isEmptyViewNeedToShowForPhoneLine)
            return _phoneLineUsageArray.count;
        else
            if(!_isEmptyViewNeedToShowForBroadbandUsage)
                return _broadbandUsageArray.count;
    }
    return 0;
    
}

- (NSString *)getSectionHeaderTitleWithSection:(NSInteger)section
{
    NSString *sectionTitle = nil;
    if(section == 0)
    {
        if(!_isEmptyViewNeedToShowForBroadbandUsage)
            sectionTitle = @"Broadband";
        else if(!_isEmptyViewNeedToShowForPhoneLine)
            sectionTitle = @"Phone line";
    }
    else
    {
        if(!_isEmptyViewNeedToShowForPhoneLine)
            sectionTitle = @"Phone line";
        else if(!_isEmptyViewNeedToShowForBroadbandUsage)
            sectionTitle = @"Broadband";
    }
    
    return sectionTitle;
    
}

- (void)updateUI {

    if(_networkRequestInProgress)
        return;

//    [self hideLoadingItems:YES];
//    [self hideRetryItems:YES];
//    [self hideEmmptyDashBoardItems:YES];

    if(_isEmptyViewNeedToShowForPhoneLine && _isEmptyViewNeedToShowForBroadbandUsage && !_forSpeedTest)
    {
        [self showEmptyDashBoardView];
    }
    else
    {
        [self.tableView reloadData];
    }

}

- (void)fetchAssetsDashboardAPI {
    
    if([AppManager isInternetConnectionAvailable])
    {
        [self hideRetryItems:YES];
        [self createLoadingView];
        [self.assetDashboardScreenModel fetchAssetsDashboardDetails];
    }
    else
    {
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_ASSETS_ACCOUNT];
    }
}

- (void)fetchServiceStatusScreenDataWithBACId:(NSString *)bacId {

    if ([_bacID containsString:@"***"])
    {
        [self showEmptyDashBoardView];
    }
    else
    {
        self.tableView.hidden = YES;
        
        if([AppManager isInternetConnectionAvailable]) {
            
            //[self createLoadingView];
            [self performSelector:@selector(createLoadingView) withObject:nil afterDelay:0.0];
            [self hideRetryItems:YES];
            self.networkRequestInProgress = YES;
            [self hideEmmptyDashBoardItems:YES];
            [self.checkServiceStatusScreenViewModel fetchServiceStatusDetailsWithBACId:bacId];
        }
        else {
            
            [self showRetryViewWithInternetStrip:YES];
            [AppManager trackNoInternetErrorOnPage:OMNIPAGE_SERVICE_STATUS_ACCOUNT_ASSET];
        }
    }
}


- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow {

    if(_retryView) {

        _retryView.hidden = NO;
        _retryView.retryViewDelegate = self;
        [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
        return;
    }
    _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
    _retryView.translatesAutoresizingMaskIntoConstraints = NO;
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
    _retryView.retryViewDelegate = self;

    [self.view addSubview:_retryView];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:64.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    
    //FIXME: (saddam) second time setting top constraint.
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];


    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];

}

- (void)hideRetryItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to retry.
    [_retryView setHidden:isHide];

}

- (void)createLoadingView {

    if(_loadingView.isHidden){
        return;
    }
    
    if(!_loadingView){
        _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        [self.view addSubview:_loadingView];
    }
    _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
    [_loadingView lazyLoadSpinView];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}

- (void)hideLoadingItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to loading.
    [_loadingView setHidden:isHide];

}


- (void)showEmptyDashBoardView {

    //Track Error
    [OmnitureManager trackError:OMNIERROR_NO_ASSET_TO_DISPLAY onPageWithName:OMNIPAGE_SERVICE_STATUS_ACCOUNT_ASSET contextInfo:[AppManager getOmniDictionary]];
    
    _emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTEmptyDashboardView" owner:nil options:nil] objectAtIndex:0];
    _emptyDashboardView.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSURL *url;
    
    if ([kBTServerType isEqualToString:@"BTServerTypeProd"]) {
        url = [[NSURL alloc] initWithString:@"https://secure.business.bt.com/Account/Addbillingaccounts.aspx"];
    } else{
        url = [[NSURL alloc] initWithString:@"https://eric1-dmze2e-ukb.bt.com/Account/Addbillingaccounts.aspx"];
    }
    
    NSMutableAttributedString *hypLink = [[NSMutableAttributedString  alloc] initWithString:@"Your products and services will show here once your bill is ready to view online. If you have recently moved to a BT OneBill and you can no longer see your products and services, click here to add missing accounts."];
    
    NSDictionary *linkAttributes = @{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle),
                                      NSLinkAttributeName : url};
    
    [hypLink addAttributes:linkAttributes range:NSMakeRange(185,4)];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
    [hypLink addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,hypLink.length)];
    
    [hypLink addAttribute:NSForegroundColorAttributeName value:[BrandColours colourBtNeutral70] range:NSMakeRange(0, hypLink.length)];
    
    [hypLink addAttribute:NSFontAttributeName
                    value:[UIFont fontWithName:kBtFontRegular size:16.0] range:NSMakeRange(0, hypLink.length)];
    
    [_emptyDashboardView updateEmptyDashboardViewWithImageNameAndLink:@"package" title:@"None to display" detailText:hypLink andButtonTitle:nil];
    
    [self.view addSubview:_emptyDashboardView];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];

}

- (void) showBroadbandHubNeededView {
    _broadbandHubNeededViewVisible = YES;
    BTRetryView *errorView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
    errorView.retryViewDelegate = self;
    errorView.translatesAutoresizingMaskIntoConstraints = NO;
    
    /*NSMutableAttributedString *businessBroadbandNeeded = [[NSMutableAttributedString alloc] initWithString:@"You’ll need BT Business Broadband to run our speed test." attributes:@{
                                                                                                                                                                                    NSFontAttributeName: [UIFont fontWithName:@"BTFont-Regular" size: 16.0],
                                                                                                                                                                                    NSForegroundColorAttributeName: [UIColor colorForHexString:@"666666"],
                                                                                                                                                                                    
                                                                                                                                                                                                                                         }];
    
    [businessBroadbandNeeded addAttribute: NSLinkAttributeName value: @"https://business.bt.com/products/connectivity-hub/" range: NSMakeRange(12, 21)];

    [errorView updateRetryViewWithErrorHeadline:@"Sorry, we couldn’t test your speed" errorDetail:@"You’ll need BT Business Broadband to run our speed test." andButtonText:@"Done"];
    [errorView updateRetryViewDetailWithAttributedString:businessBroadbandNeeded];
    [self.view addSubview:errorView];*/
    
    
    ////
    NSURL* url = [[NSURL alloc] initWithString:@"https://business.bt.com/products/connectivity-hub/"];
   
    
    NSMutableAttributedString *hypLink = [[NSMutableAttributedString  alloc] initWithString:@"You’ll need BT Business Broadband to run your speed test"];
    
    NSDictionary *linkAttributes = @{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle),
                                     NSLinkAttributeName : url};
    
    [hypLink addAttributes:linkAttributes range:NSMakeRange(12,21)];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
    [hypLink addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,hypLink.length)];
    
    [hypLink addAttribute:NSForegroundColorAttributeName value:[UIColor colorForHexString:@"666666"] range:NSMakeRange(0, hypLink.length)];
    
    [hypLink addAttribute:NSFontAttributeName
                    value:[UIFont fontWithName:kBtFontRegular size:16.0] range:NSMakeRange(0, hypLink.length)];
    
    [errorView updateRetryViewWithErrorHeadline:@"Sorry, we couldn’t test your speed" errorDetail:@"You’ll need BT Business Broadband to run your speed test." andButtonText:@"Done"];
    
    [errorView updateRetryViewDetailWithAttributedString:hypLink];
    [self.view addSubview:errorView];
    
    /////
    
    
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:errorView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:64.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:errorView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:errorView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    
    //FIXME: (saddam) second time setting top constraint.
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:errorView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:errorView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
    
    
    [self trackPageForOmniture];
}

- (void)hideEmmptyDashBoardItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to retry.
    [_emptyDashboardView setHidden:isHide];
    [_emptyDashboardView removeFromSuperview];
    _emptyDashboardView = nil;

}



#pragma mark - table view datasource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(_isEmptyViewNeedToShowForBroadbandUsage || _isEmptyViewNeedToShowForPhoneLine)
        return 1;
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [self getNumberOfRowsInSectionWithSection:section];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Background color
    view.tintColor = [BrandColours colourBtNeutral30];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 40)];
    [headerView setBackgroundColor:[UIColor colorForHexString:@"eeeeee"]];//[BrandColours colourBtNeutral30]];
    
    NSString *sectionTitle = nil;
    
    if(section == 0)
    {
        sectionTitle = [self getSectionHeaderTitleWithSection:section];
    }
    else
    {
        sectionTitle = [self getSectionHeaderTitleWithSection:section];
    }
    
//    UIView *topSepView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, headerView.frame.size.width, 1)];
//    topSepView.backgroundColor = [BrandColours colourBtNeutral50];
//    [headerView addSubview:topSepView];
    
    UILabel *headerTitleLable = [[UILabel alloc] initWithFrame:CGRectMake(15, 8,tableView.bounds.size.width - 40, 25)];
    headerTitleLable.text = sectionTitle;
    headerTitleLable.textColor = [BrandColours colourBtLightBlack];
    headerTitleLable.font = [UIFont fontWithName:@"BTFont-Bold" size:14];
    headerTitleLable.backgroundColor = [UIColor clearColor];
    [headerView addSubview:headerTitleLable];
    
//    UIView *bottomSepView = [[UIView alloc] initWithFrame:CGRectMake(0, headerView.frame.size.height - 1, headerView.frame.size.width, 1)];
//    bottomSepView.backgroundColor = [BrandColours colourBtNeutral50];
//    [headerView addSubview:bottomSepView];
    
    return headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSMutableArray *dataArray = [[NSMutableArray alloc]init];
    
    if(indexPath.section == 0)
    {
        if(!_isEmptyViewNeedToShowForBroadbandUsage){
            dataArray = _broadbandUsageArray;
        }
        else {
            dataArray = _phoneLineUsageArray;
        }
    }
    else
    {
        if(!_isEmptyViewNeedToShowForPhoneLine){
            dataArray = _phoneLineUsageArray;
        }
        else {
            dataArray = _broadbandUsageArray;
        }
    }
    
    BTCheckServiceStatusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BTCheckServiceStatusTableViewCell" forIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell updateCheckServiceStatusTableViewCellWith:[dataArray objectAtIndex:indexPath.row]];
    cell.preservesSuperviewLayoutMargins = false;
    cell.separatorInset = UIEdgeInsetsZero;
    cell.layoutMargins = UIEdgeInsetsZero;
    CGRect frame = cell.frame;
    frame.size.width = [[UIScreen mainScreen]bounds].size.width;
    [cell setFrame:frame];
    return cell;
    
}


#pragma mark - table view delegate methods


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSMutableArray *dataArray = [[NSMutableArray alloc]init];
    

    if([_phoneLineUsageArray count] > 0 && [_broadbandUsageArray count] >0)
    {
        
        if(indexPath.section == 0)
        {
            BTBroadbandAndPhoneService *services = (BTBroadbandAndPhoneService *)[/*self.checkServiceStatusScreenViewModel.servicesArray*/_broadbandUsageArray objectAtIndex:indexPath.row];
            if ([services.productType isEqualToString:@"BB"] && !_forSpeedTest) {
                BTServiceStatusDetailsViewController *serviceStatusDetailsViewController = [BTServiceStatusDetailsViewController getServiceStatusDetailsViewController];
                serviceStatusDetailsViewController.currentServiceType = BTServiceTypeBroadband;
                serviceStatusDetailsViewController.serviceID = services.serviceId;
                serviceStatusDetailsViewController.postCode = services.postcode;
                serviceStatusDetailsViewController.selectedServiceName = @"Broadband";
                if([services.productName isEqualToString:@"Business BroadBand Access"]) {
                    serviceStatusDetailsViewController.productName = @"Business broadband";
                }
                serviceStatusDetailsViewController.productName = services.productName;
                [self.navigationController pushViewController:serviceStatusDetailsViewController animated:YES];
                [self trackOmniClick:OMNICLICK_SERVICE_STATUS_VIEW_DETAILS_BB];
            }
            else if([services.productType isEqualToString:@"BB"]) {
                SpeedTestViewController *speedTestViewController = [SpeedTestViewController getSpeedTestViewController];
                speedTestViewController.serviceID = services.serviceId;
                speedTestViewController.postcode = services.postcode;
                speedTestViewController.productName = services.productName;
                // pass relevant data
                [self.navigationController pushViewController:speedTestViewController animated:YES];
                // omniture tracking
                
            }
            /*
            BTBroadbandAndPhoneService *services = (BTBroadbandAndPhoneService *)[_broadbandUsageArray objectAtIndex:indexPath.row];
            BTServiceStatusDetailsViewController *serviceStatusDetailsViewController = [BTServiceStatusDetailsViewController getServiceStatusDetailsViewController];
            serviceStatusDetailsViewController.currentServiceType = BTServiceTypeBroadband;
            serviceStatusDetailsViewController.serviceID = services.serviceId;
            serviceStatusDetailsViewController.postCode = services.postcode;
            serviceStatusDetailsViewController.selectedServiceName = @"Broadband";
            if([services.productName isEqualToString:@"Business BroadBand Access"]) {
                serviceStatusDetailsViewController.productName = @"Business broadband";
            }
            serviceStatusDetailsViewController.productName = services.productName;
            [self.navigationController pushViewController:serviceStatusDetailsViewController animated:YES];
            [self trackOmniClick:OMNICLICK_SERVICE_STATUS_VIEW_DETAILS_BB];
             */
        }
        else {
            BTBroadbandAndPhoneService *services = (BTBroadbandAndPhoneService *)[_phoneLineUsageArray objectAtIndex:indexPath.row];
            BTServiceStatusDetailsViewController *serviceStatusDetailsViewController = [BTServiceStatusDetailsViewController getServiceStatusDetailsViewController];
            serviceStatusDetailsViewController.currentServiceType = BTServiceTypePSTN;
            serviceStatusDetailsViewController.serviceID = services.serviceId;
            //            serviceStatusDetailsViewController.serviceID = @"CV60041526"; //Hardcoded to test CV
            serviceStatusDetailsViewController.selectedServiceName = @"PSTN";
            serviceStatusDetailsViewController.postCode = services.postcode;
            serviceStatusDetailsViewController.productName = services.productName;
            [self.navigationController pushViewController:serviceStatusDetailsViewController animated:YES];
            
            if ([services.serviceId rangeOfString:@"cv" options:NSCaseInsensitiveSearch].location != NSNotFound) {
                [self trackPageForOmnitureCloudVoice: OMNICLICK_SERVICE_STATUS_VIEW_DETAILS_CVE];
            }
            else {
                [self trackOmniClick:OMNICLICK_SERVICE_STATUS_VIEW_DETAILS_PSTN];
            }
        }
    }
    else if (([_phoneLineUsageArray count] > 0))
    {
        BTBroadbandAndPhoneService *services = (BTBroadbandAndPhoneService *)[_phoneLineUsageArray objectAtIndex:indexPath.row];
        BTServiceStatusDetailsViewController *serviceStatusDetailsViewController = [BTServiceStatusDetailsViewController getServiceStatusDetailsViewController];
        serviceStatusDetailsViewController.currentServiceType = BTServiceTypePSTN;
        serviceStatusDetailsViewController.serviceID = services.serviceId;
        serviceStatusDetailsViewController.selectedServiceName = @"PSTN";
        serviceStatusDetailsViewController.postCode = services.postcode;
        serviceStatusDetailsViewController.productName = services.productName;
        [self.navigationController pushViewController:serviceStatusDetailsViewController animated:YES];
        
        if ([services.serviceId rangeOfString:@"cv" options:NSCaseInsensitiveSearch].location != NSNotFound) {
            [self trackPageForOmnitureCloudVoice: OMNICLICK_SERVICE_STATUS_VIEW_DETAILS_CVE];
        }
        else {
            [self trackOmniClick:OMNICLICK_SERVICE_STATUS_VIEW_DETAILS_PSTN];
        }
        
    }
    else if (([_broadbandUsageArray count] > 0))
    {
        BTBroadbandAndPhoneService *services = (BTBroadbandAndPhoneService *)[/*self.checkServiceStatusScreenViewModel.servicesArray*/_broadbandUsageArray objectAtIndex:indexPath.row];
        if ([services.productType isEqualToString:@"BB"] && !_forSpeedTest) {
            BTServiceStatusDetailsViewController *serviceStatusDetailsViewController = [BTServiceStatusDetailsViewController getServiceStatusDetailsViewController];
            serviceStatusDetailsViewController.currentServiceType = BTServiceTypeBroadband;
            serviceStatusDetailsViewController.serviceID = services.serviceId;
            serviceStatusDetailsViewController.postCode = services.postcode;
            serviceStatusDetailsViewController.selectedServiceName = @"Broadband";
            if([services.productName isEqualToString:@"Business BroadBand Access"]) {
                serviceStatusDetailsViewController.productName = @"Business broadband";
            }
            serviceStatusDetailsViewController.productName = services.productName;
            [self.navigationController pushViewController:serviceStatusDetailsViewController animated:YES];
            [self trackOmniClick:OMNICLICK_SERVICE_STATUS_VIEW_DETAILS_BB];
        }
        else if([services.productType isEqualToString:@"BB"]) {
            SpeedTestViewController *speedTestViewController = [SpeedTestViewController getSpeedTestViewController];
            speedTestViewController.serviceID = services.serviceId;
            speedTestViewController.postcode = services.postcode;
            speedTestViewController.productName = services.productName;
            // pass relevant data
            [self.navigationController pushViewController:speedTestViewController animated:YES];
            // omniture tracking
            
        }
        /*
        BTBroadbandAndPhoneService *services = (BTBroadbandAndPhoneService *)[_broadbandUsageArray objectAtIndex:indexPath.row];
        BTServiceStatusDetailsViewController *serviceStatusDetailsViewController = [BTServiceStatusDetailsViewController getServiceStatusDetailsViewController];
        serviceStatusDetailsViewController.currentServiceType = BTServiceTypeBroadband;
        serviceStatusDetailsViewController.serviceID = services.serviceId;
        serviceStatusDetailsViewController.postCode = services.postcode;
        serviceStatusDetailsViewController.selectedServiceName = @"Broadband";
        if([services.productName isEqualToString:@"Business BroadBand Access"]) {
            serviceStatusDetailsViewController.productName = @"Business broadband";
        }
        serviceStatusDetailsViewController.productName = services.productName;
        [self.navigationController pushViewController:serviceStatusDetailsViewController animated:YES];
        [self trackOmniClick:OMNICLICK_SERVICE_STATUS_VIEW_DETAILS_BB];
         */
    }

    
 /*
    if(indexPath.section == 0)
    {
        if(!_isEmptyViewNeedToShowForPhoneLine){
            dataArray = _phoneLineUsageArray;
        }
        else {
            dataArray = _broadbandUsageArray;
        }
    }
    else
    {
        if(!_isEmptyViewNeedToShowForBroadbandUsage){
            dataArray = _broadbandUsageArray;
        }
        else {
            dataArray = _phoneLineUsageArray;
        }
    }
    
    BTBroadbandAndPhoneService *services = (BTBroadbandAndPhoneService *)[dataArray objectAtIndex:indexPath.row];

    if ([services.productType isEqualToString:@"BB"]) {

        BTServiceStatusDetailsViewController *serviceStatusDetailsViewController = [BTServiceStatusDetailsViewController getServiceStatusDetailsViewController];
        serviceStatusDetailsViewController.currentServiceType = BTServiceTypeBroadband;
        serviceStatusDetailsViewController.serviceID = services.serviceId;
        serviceStatusDetailsViewController.postCode = services.postcode;
        serviceStatusDetailsViewController.selectedServiceName = @"Broadband";
        if([services.productName isEqualToString:@"Business BroadBand Access"]) {
            serviceStatusDetailsViewController.productName = @"Business broadband";
        }
        serviceStatusDetailsViewController.productName = services.productName;
        [self.navigationController pushViewController:serviceStatusDetailsViewController animated:YES];
        [self trackOmniClick:OMNICLICK_SERVICE_STATUS_VIEW_DETAILS_BB];
    }
    else if([services.productType isEqualToString:@"BB"]) {
        SpeedTestViewController *speedTestViewController = [SpeedTestViewController getSpeedTestViewController];
        speedTestViewController.serviceID = services.serviceId;
        speedTestViewController.postcode = services.postcode;
        speedTestViewController.productName = services.productName;
        // pass relevant data
        [self.navigationController pushViewController:speedTestViewController animated:YES];
        // omniture tracking
        
    }
    else if([services.productType isEqualToString:@"PSTN"]) {
        BTServiceStatusDetailsViewController *serviceStatusDetailsViewController = [BTServiceStatusDetailsViewController getServiceStatusDetailsViewController];
        serviceStatusDetailsViewController.currentServiceType = BTServiceTypePSTN;
        serviceStatusDetailsViewController.serviceID = services.serviceId;
        serviceStatusDetailsViewController.selectedServiceName = @"PSTN";
        serviceStatusDetailsViewController.postCode = services.postcode;
        serviceStatusDetailsViewController.productName = services.productName;
        [self.navigationController pushViewController:serviceStatusDetailsViewController animated:YES];
        [self trackOmniClick:OMNICLICK_SERVICE_STATUS_VIEW_DETAILS_PSTN];
    }
*/
}

#pragma mark - DLMAssetDashboardScreenDelegate ALLBAC API response
- (void)successfullyFetchedAssetsDashboardDataOnDLMAssetDashboardScreen:(DLMAssetDashboardScreen *)assetDashboardScreen
{
    
    if(assetDashboardScreen.assetsArray.count > 0)
    {
        NSArray* assetsArray =  [assetDashboardScreen.assetsArray mutableCopy];
        NSArray* cugs = [[AppDelegate sharedInstance] savedCugs];
        if(cugs.count > 1 || assetsArray.count > 1){
            self.isSingleBacCUG = NO;
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            appDelegate.isSingleBacCugBackNavButtonPressed = YES;
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [self fetchServiceStatusScreenDataWithBACId:self.bacID];
        }
    }
    else
    {
        DDLogError(@"Assets Dashboard: Assets not found.");
    }
}

- (void)assetDashboardScreen:(DLMAssetDashboardScreen *)assetDashboardScreen failedToFetchAssetsDashboardDataWithWebServiceError:(NLWebServiceError *)webServiceError {
    
     [self fetchServiceStatusScreenDataWithBACId:self.bacID];
}


#pragma mark - DLMCheckServiceStatusScreenDelegate

- (void)successfullyFetchedCheckServiceStatusDataOnDLMCheckServiceStatusScreenn:(DLMCheckServiceStatusScreen *)serviceStatusScreen {

    self.tableView.hidden = NO;
    self.networkRequestInProgress = NO;
    
    _isEmptyViewNeedToShowForPhoneLine = YES;
    _isEmptyViewNeedToShowForBroadbandUsage = YES;
    
    _phoneLineUsageArray = [[NSMutableArray alloc]init];
    _broadbandUsageArray = [[NSMutableArray alloc]init];

    self.checkServiceStatusScreenViewModel = serviceStatusScreen;

    if(serviceStatusScreen.servicesArray.count != 0) {
        
        for (int i = 0; i < serviceStatusScreen.servicesArray.count; i++) {
            BTBroadbandAndPhoneService *services = (BTBroadbandAndPhoneService *)[self.checkServiceStatusScreenViewModel.servicesArray objectAtIndex:i];
            if ([services.productType isEqualToString:@"PSTN"]) {
                _isEmptyViewNeedToShowForPhoneLine = NO;
                [_phoneLineUsageArray addObject:services];
            }
            else if ([services.productType isEqualToString:@"BB"]){
                _isEmptyViewNeedToShowForBroadbandUsage = NO;
                [_broadbandUsageArray addObject:services];
            }
        }
    }
    else if (_forSpeedTest) {
        [self showBroadbandHubNeededView];
    }
    else {
        [self showEmptyDashBoardView];
    }
    
    [self updateUI];


}

- (void)serviceStatusScreen:(DLMCheckServiceStatusScreen *)serviceStatusScreen failedToFetchServiceStatusDataWithWebServiceError:(NLWebServiceError *)webServiceError {

    self.networkRequestInProgress = NO;
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];

    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO) {

        switch (webServiceError.error.code) {

            case BTNetworkErrorCodeAPINoDataFound: {

                errorHandled = YES;
                [self showEmptyDashBoardView];
                [AppManager trackNoDataFoundErrorOnPage:OMNIPAGE_SERVICE_STATUS_ACCOUNT_ASSET];
                break;
            }
            default: {
                
                errorHandled = NO;
                break;
            }
        }
    }
    
    if(errorHandled == NO) {
        
        [self showRetryViewWithInternetStrip:NO];
         [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_SERVICE_STATUS_ACCOUNT_ASSET];
    }
}

- (void) filterPSTNResults {
    
}

#pragma mark - RetryView Delgates

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView {
    if ([retryView.retryButton.titleLabel.text.lowercaseString isEqualToString:@"done"]) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self fetchServiceStatusScreenDataWithBACId:self.bacID];
    }
}

#pragma mark - Omniture Methods

- (NSString*)pageNameForOmnitureTracking
{
    if(_forSpeedTest && _broadbandHubNeededViewVisible) {
        return OMNIPAGE_SPEED_TEST_NO_HUB_OR_BROADBAND;
    } else if(_forSpeedTest) {
        return OMNIPAGE_SPEED_TEST_SELECT_HUB;
    }
    else {
        return OMNIPAGE_SERVICE_STATUS_ACCOUNT_ASSET;
    }
}

- (void)trackPageForOmniture
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = [self pageNameForOmnitureTracking];;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];

    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackOmniClick:(NSString *)linkTitle
{
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",[self pageNameForOmnitureTracking],linkTitle]];
}

- (void)trackCUGChange
{
    NSString *pageName = [self pageNameForOmnitureTracking];
    NSString *linkTitle = OMNICLICK_CUG_CHANGE;
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:data];
}

- (void)trackPageForOmnitureCloudVoice:(NSString *)pageName
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

@end
