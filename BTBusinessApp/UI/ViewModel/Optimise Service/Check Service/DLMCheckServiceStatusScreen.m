//
//  DLMCheckServiceStatusScreen.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 23/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "DLMCheckServiceStatusScreen.h"
#import "NLCheckServiceStatusWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"

@interface DLMCheckServiceStatusScreen()<NLCheckServiceStatusWebServiceDelegate>

@property (nonatomic) NLCheckServiceStatusWebService *getCheckServiceStatusWebService;

@end


@implementation DLMCheckServiceStatusScreen

#pragma mark - Public Methods

- (void)fetchServiceStatusDetailsWithBACId:(NSString *)bacId {

    self.getCheckServiceStatusWebService = [[NLCheckServiceStatusWebService alloc] initWithBACId:bacId];
    self.getCheckServiceStatusWebService.checkServiceStatusWebServiceDelegate = self;
    [self.getCheckServiceStatusWebService resume];


}
- (void)cancelServiceStatusAPIRequest {

    self.getCheckServiceStatusWebService.checkServiceStatusWebServiceDelegate = nil;
    [self.getCheckServiceStatusWebService cancel];
    self.getCheckServiceStatusWebService = nil;
}

#pragma mark - NLCheckServiceStatusWebServiceDelegate


- (void)getCheckServiceStatusWebService:(NLCheckServiceStatusWebService *)webService successfullyFetchedCheckServiceStatusWithServiceArray:(NSArray *)serviceArray {

    if(webService == self.getCheckServiceStatusWebService) {
        // Filter out PSTN data for speed tests
        if(_forSpeedTest) {
            NSMutableArray *tempArray = [[NSMutableArray alloc] init];
            for(int i = 0; i < [serviceArray count]; i++) {
                BTBroadbandAndPhoneService *service = serviceArray[i];
                if([service.productType isEqualToString:@"BB"]) {
                    [tempArray addObject:service];
                }
            }
            serviceArray = tempArray;
        }
        
        self.servicesArray = serviceArray;

        [self.serviceStatusScreenDelegate successfullyFetchedCheckServiceStatusDataOnDLMCheckServiceStatusScreenn:self];
        self.getCheckServiceStatusWebService.checkServiceStatusWebServiceDelegate = nil;
        self.getCheckServiceStatusWebService = nil;
    }
    else {

        DDLogError(@"This delegate method gets called because of success of an object of NLCheckServiceStatusWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLCheckServiceStatusWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }



}


- (void)getCheckServiceStatusWebService:(NLCheckServiceStatusWebService *)webService failedToFetchCheckServiceStatusWithWebServiceError:(NLWebServiceError *)webServiceError {

    if(webService == self.getCheckServiceStatusWebService) {

        [self.serviceStatusScreenDelegate serviceStatusScreen:self failedToFetchServiceStatusDataWithWebServiceError:webServiceError];

        self.getCheckServiceStatusWebService.checkServiceStatusWebServiceDelegate = nil;
        self.getCheckServiceStatusWebService = nil;
    }
    else {

        DDLogError(@"This delegate method gets called because of failure of an object of NLCheckServiceStatusWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLCheckServiceStatusWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }

}
@end
