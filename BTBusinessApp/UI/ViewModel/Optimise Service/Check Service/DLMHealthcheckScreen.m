//
//  DLMHealthcheckScreen.m
//  BTBusinessApp
//

#import "DLMHealthcheckScreen.h"

@implementation DLMHealthcheckScreen

-(void) createHealthcheckData {
    // Get to populating the healthcheck data here
    NSDictionary *hubOff = [self createDictionaryWithTitle:@"The power is off or you've turned the lights off"
                                                     description:@"If you haven't turned the lights off, check that the power button is on, the power supply is plugged in correctly at the mains and the power is turned on. If there's still no light, call 0800 800 154."
                                                       imageName:@"smarthub_blank"];
    
    
    NSDictionary *hubStartingUp = [self createDictionaryWithTitle:@"The Hub is starting up"
                                                     description:@"Wait a couple of minutes for it to start."
                                                       imageName:@"smarthub_green"];
    
    NSDictionary *hubConnecting = [self createDictionaryWithTitle:@"The Hub is connecting to broadband"
                                                      description:@"Give it a minute or two to connect. The light will turn steady blue when your Hub is ready."
                                                        imageName:@"smarthub_orange"];
    
    NSDictionary *hubNoInternet = [self createDictionaryWithTitle:@"The Hub is working but isn't connected to the internet"
                                                      description:@"Give it a minute or two to connect. The light will turn steady blue when your Hub is ready."
                                                        imageName:@"smarthub_orange"];
    
    NSDictionary *hubFine = [self createDictionaryWithTitle:@"The Hub is working fine"
                                                description:@"You have a broadband connection – if you can't get online there might be a problem with your computer, tablet or mobile device. Turn it off and then on and try again."
                                                  imageName:@"smarthub_blue"];
    
    NSDictionary *hub4G = [self createDictionaryWithTitle:@"The Hub is connected to the 4G network via 4G Assure"
                                                description:@"Nothing - you are connected to the 4G network, your Hub will automatically connect to the broadband network when it's available."
                                                  imageName:@"smarthub_purple"];
    
    NSDictionary *hubProblemSomewhere = [self createDictionaryWithTitle:@"There's a problem somewhere"
                                              description:@"Using the Power button, turn your Hub off and then on again. If the light doesn't turn blue, use a paper clip to press your Hub's Factory Reset button. If this doesn't fix it call 0800800154. Make sure you're next to your Hub with a computer or device when you call us."
                                                imageName:@"smarthub_red"];
    
    NSDictionary *hubWPS = [self createDictionaryWithTitle:@""
                                                            description:@"If it's flashing blue, it's waiting for you to press the WPS button on your computer or device (you've got two minutes). If it's flashing red, you didn't connect. Give it a couple of minutes and try again. No light means that it's connected successfully."
                                                            imageName:@"smarthub_wps"];
    
    _smartHubHealthcheckData = [[NSArray alloc] initWithObjects: hubOff, hubStartingUp, hubConnecting, hubNoInternet, hubFine, hub4G, hubProblemSomewhere, hubWPS, nil];
    
    NSDictionary *businessHubOff = [self createDictionaryWithTitle:@"The power is off"
                                                description:@"If you haven't turned the lights off, check that the power button is on, the power supply is plugged in correctly at the mains and the power is turned on. If there's still no light, call 0800 800 154."
                                                  imageName:@"businesshub_blank"];
    
    NSDictionary *businessHubStartingUp = [self createDictionaryWithTitle:@"The Hub is starting up"
                                                      description:@"Wait a couple of minutes for it to start."
                                                        imageName:@"businesshub_green"];
    
    NSDictionary *businessHubProblem = [self createDictionaryWithTitle:@"There could be a problem" description:@"Check the broadband icons displayed in the silver strip and follow the help below." imageName:@"businesshub_orange"];
    
    NSDictionary *businessHubFine = [self createDictionaryWithTitle:@"The Hub is working fine" description:@"You have a broadband connection – if you can't get online there might be a problem with your computer, tablet or mobile device. Turn it off and then on and try again." imageName:@"businesshub_blue"];
    
    NSDictionary *businessHub4G = [self createDictionaryWithTitle:@"The Hub is connected to the 4G network via 4G Assure" description:@"Nothing - you are connected to the 4G network, your Hub will automatically connect to the broadband network when it is available." imageName:@"businesshub_purple"];
    
    NSDictionary *businessHubProblemSomewhere = [self createDictionaryWithTitle:@"There’s a problem somewhere" description:@"Using the power button, turn your Hub off and then on again. If the light doesn't turn blue, use a paper clip to press the Hub’s Factory Reset button. If this doesn't fix it call us on 0800800154." imageName:@"businesshub_red"];
    
    NSDictionary *businessHubAccountActivation = [self createDictionaryWithTitle:@"Your account might not be switched on yet" description:@"It can take up until midnight on the day we switch your broadband on for it to start working. If you've still got an orange light the next day, call us on 0800800154." imageName:@"businesshub_orange_b"];
    
    NSDictionary *businessHubUsernamePassword = [self createDictionaryWithTitle:@"The username and/or password is incorrect" description:@"Connect a device to your Hub using a cable or wi-fi. Open a web browser and follow the onscreen help wizard to get connected." imageName:@"businesshub_red_b"];
    
    NSDictionary *businessHubNoInternet = [self createDictionaryWithTitle:@"Your Hub is working, but isn't connected to the internet" description:@"Check the broadband cable (black cable with grey ends) - or if you have fibre to the premises it'll be the red ethernet cable (black cable with red ends) - is plugged in correctly and you're using a filter if needed." imageName:@"businesshub_red_b"];
    
    NSDictionary *businessHubWirelessSecurity = [self createDictionaryWithTitle:@"You've switched off wireless security for one or both wireless channels" description:@"Turn on your security in the wireless tab in your Hub Manager." imageName:@"businesshub_red_w"];
    
    NSDictionary *businessHubWPSAutoConnect = [self createDictionaryWithTitle:@"Your Hub is in WPS automatic connection mode" description:@"Click the button to connect (on your computer or adapter) without entering a pass key. The Hub will return to normal mode after a few minutes." imageName:@"businesshub_orange_w"];
    
    _businessHubHealthcheckData = [[NSArray alloc] initWithObjects:businessHubOff, businessHubStartingUp, businessHubProblem, businessHubFine, businessHub4G, businessHubProblemSomewhere, businessHubAccountActivation, businessHubUsernamePassword, businessHubNoInternet, businessHubWirelessSecurity, businessHubWPSAutoConnect, nil];
}

- (NSDictionary *)createDictionaryWithTitle:(NSString *)title description:(NSString *)description imageName:(NSString *)image
{
    NSMutableDictionary *healthcheckData = [NSMutableDictionary dictionary];
    [healthcheckData setValue:title forKey:@"Title"];
    [healthcheckData setValue:description forKey:@"Description"];
    [healthcheckData setValue:image forKey:@"Image"];
    
    return healthcheckData;
}


@end
