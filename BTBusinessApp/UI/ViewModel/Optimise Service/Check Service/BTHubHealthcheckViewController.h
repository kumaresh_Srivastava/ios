//
//  BTHubHealthcheckViewController.h
//  BTBusinessApp
//
//  Created by Jim Purvis on 11/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTHubHealthcheckViewController : UIViewController

-(id)initWithAssets:(NSMutableArray *)assets;

@end
