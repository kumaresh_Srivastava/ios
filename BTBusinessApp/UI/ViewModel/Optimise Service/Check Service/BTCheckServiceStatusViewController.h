//
//  BTCheckServiceStatusViewController.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 22/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTCheckServiceStatusViewController : UIViewController

@property (nonatomic, copy) NSString *groupName;
@property (nonatomic, copy) NSString *bacID;
@property (nonatomic) BOOL forSpeedTest;
@property (nonatomic,assign) BOOL isSingleBacCUG;

+ (BTCheckServiceStatusViewController *) getCheckServiceStatusViewController;

@end
