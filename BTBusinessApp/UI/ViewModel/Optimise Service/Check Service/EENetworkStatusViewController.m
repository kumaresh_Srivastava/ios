//
//  EENetworkStatusViewController.m
//  BTBusinessApp
//
//  Created by Jim Purvis on 18/05/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "EENetworkStatusViewController.h"
#import "AppManager.h"
#import "AppConstants.h"
#import "OmnitureManager.h"
#import "EENetworkStatusTableViewCell.h"

@interface EENetworkStatusViewController ()<UITableViewDelegate, UITableViewDataSource> {
    
}

@property (nonatomic, strong) EENetworkStatus* networkStatus;
@property (nonatomic, strong) UITableView *networkStatusTableView;

@end

@implementation EENetworkStatusViewController

-(id)initWithNetworkStatus:(EENetworkStatus *)networkStatus {
    self = [super init];
    if(self) {
        _networkStatus = networkStatus;
    }
    
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [self createInitialUI];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleDone target:self action:@selector(closeButtonAction:)];
    [self trackPageForOmniture];
}

- (void)createInitialUI
{
    //CGSize screenSize = [UIScreen mainScreen].bounds.size;
    
    UILabel *pageTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 208, 20)];
    pageTitleLabel.font = [UIFont fontWithName:kBtFontBold size:15.0];
    pageTitleLabel.textColor = [UIColor whiteColor];
    pageTitleLabel.textAlignment = NSTextAlignmentCenter;
    pageTitleLabel.text = @"Service status";

    UIView *titlesView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 208, 40)];
    [titlesView addSubview:pageTitleLabel];
    
    [self.navigationItem setTitleView:titlesView];
}

-(void)createNetworkStatusTableView {
    _networkStatusTableView = [[UITableView alloc] init];
    _networkStatusTableView.dataSource = self;
    _networkStatusTableView.delegate = self;
    _networkStatusTableView.allowsSelection = NO;
    [self.view addSubview:_networkStatusTableView];
    
    UINib *statusCell = [UINib nibWithNibName:@"EENetworkStatusTableViewCell" bundle:nil];
    [_networkStatusTableView registerNib:statusCell forCellReuseIdentifier:@"EENetworkStatusTableViewCell"];
}

- (void)closeButtonAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - TableView Datasource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EENetworkStatusTableViewCell *networkStatusCell = [tableView dequeueReusableCellWithIdentifier:@"EENetworkStatusTableViewCell"];
    networkStatusCell.title.text = [_networkStatus title];
    NSArray *causeCodes = [_networkStatus causeCodes];
    if(causeCodes && [causeCodes count] > 0) {
        NSString *causeCode = [causeCodes[0] valueForKey:@"causeCode"];
        networkStatusCell.issueDescription.text = [self textForCauseCode:causeCode];
    }
    return networkStatusCell;
}

-(NSString *)textForCauseCode:(NSString*)causeCode {
    NSString *message = @"We're really sorry, 4G isn't available right now. We're working to put things right.";
    if([causeCode isEqualToString:@"CCFT100001"] ||
       [causeCode isEqualToString:@"CCFT200001"] ||
       [causeCode isEqualToString:@"CCFT300001"] ) {
        return @"We’re on it. We expect good service within 4 hours, but some fixes take a little longer.";
    } else if([causeCode isEqualToString:@"CCFT100002"] ||
              [causeCode isEqualToString:@"CCFT200002"] ||
              [causeCode isEqualToString:@"CCFT300002"] ) {
        return @"We’re on it. We expect good service within 8 hours, but some fixes take a little longer.";
    } else if([causeCode isEqualToString:@"CCFT100003"] ||
              [causeCode isEqualToString:@"CCFT200003"] ||
              [causeCode isEqualToString:@"CCFT300003"] ) {
        return @"We’re on it. We expect good service within 16 hours, but some fixes take a little longer.";
    } else if([causeCode isEqualToString:@"CCFT100004"] ||
              [causeCode isEqualToString:@"CCFT200004"] ||
              [causeCode isEqualToString:@"CCFT300004"] ) {
        return @"We are aware of a problem with this site, but do not have an estimated time to fix it just yet.";
    } else if([causeCode isEqualToString:@"CCPT100101"] ||
              [causeCode isEqualToString:@"CCPT100091"] ||
              [causeCode isEqualToString:@"CCPT100201"] ||
              [causeCode isEqualToString:@"CCPT100031"]) {
        return @"We're working to get this site fixed. We expect good service within 16 hours.";
    }
    return message;
}

#pragma mark - Omniture Methods

- (NSString*)pageNameForOmnitureTracking
{
    NSString *pageName = OMNIPAGE_SERVICE_STATUS_4GSERVICE_STATUS;
    return pageName;
}

- (void)trackPageForOmniture
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = [self pageNameForOmnitureTracking];;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackOmniClick:(NSString *)linkTitle
{
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",[self pageNameForOmnitureTracking],linkTitle]];
}


@end
