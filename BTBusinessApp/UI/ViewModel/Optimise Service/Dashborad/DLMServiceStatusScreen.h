//
//  DLMServiceStatusScreen.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 22/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "DLMObject.h"
#import "BTAsset.h"
#import "BTCug.h"

@class DLMServiceStatusScreen;

@class  NLWebServiceError;
@protocol DLMServiceStatusDashboardScreenDelegate <NSObject>

- (void)successfullyFetchedServiceStatusDashboardDataOnDLMServiceStatusDashboardScreen:(DLMServiceStatusScreen *)serviceStatusDashboardScreen;

- (void)serviceStatusDashboardScreen:(DLMServiceStatusScreen *)serviceStatusDashboardScreen failedToFetchServiceStatusDashboardDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end



@interface DLMServiceStatusScreen : DLMObject

@property (nonatomic, copy) NSArray<BTAsset*> *assetsArray;
@property (nonatomic, weak) id <DLMServiceStatusDashboardScreenDelegate> serviceStatusDashboardScreenDelegate;

- (void)fetchServiceStatusDashboardDetails;
- (void)cancelServiceStatusDashboardAPIRequest;
- (void)changeSelectedCUGTo:(BTCug *)selectedCug;


@end
