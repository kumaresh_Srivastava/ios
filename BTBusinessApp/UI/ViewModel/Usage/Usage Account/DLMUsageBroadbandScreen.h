//
//  DLMUsageBroadbandScreen.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 26/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"

@class DLMUsageBroadbandScreen;
@class NLWebServiceError;

@protocol DLMUsageBroadbandScreenDelegate <NSObject>

- (void)successfullyFetchedUsageOnBroadbandUsageScreen:(DLMUsageBroadbandScreen *)broadbandUsageScreen withBroadbandUsageList:(NSArray *)broadbandUsageList;

- (void)usageBroadbandScreen:(DLMUsageBroadbandScreen *)usageBroadbandScreen failedToFetchUsageDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end




@interface DLMUsageBroadbandScreen : DLMObject

@property (nonatomic, weak) id<DLMUsageBroadbandScreenDelegate> usageBroadbandScreenDelegate;

- (void)fetchBroadbandUsageWithAccountNumber:(NSString *)accountNumber;
- (void)cancelBroadbandUsageApi;
@end


