//
//  DLMUsageBroadbandScreen.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 26/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMUsageBroadbandScreen.h"
#import "NLUsageBroadbandWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "CDAuthenticationToken+CoreDataProperties.h"

@interface DLMUsageBroadbandScreen() <NLUsageBroadbandWebServiceDelegate>

@property (nonatomic, strong) NLUsageBroadbandWebService *getUsageBroadbandWebService;

@end



@implementation DLMUsageBroadbandScreen

- (void)fetchBroadbandUsageWithAccountNumber:(NSString *)accountNumber
{
    self.getUsageBroadbandWebService = [[NLUsageBroadbandWebService alloc] initWithBAC:accountNumber];
    self.getUsageBroadbandWebService.usageBroadbandWebServiceDelegate = self;
    [self.getUsageBroadbandWebService resume];
}

- (void)cancelBroadbandUsageApi
{
    self.getUsageBroadbandWebService.usageBroadbandWebServiceDelegate = nil;
    [self.getUsageBroadbandWebService cancel];
    self.getUsageBroadbandWebService = nil;
}


#pragma mark - NLUsageBroadbandWebServiceDelegate

- (void)succesfullyFetchedBroadbandUsageOnUsageBroadbandWebService:(NLUsageBroadbandWebService *)webService withBroadbandUsageList:(NSArray *)broadbandUsageList
{
    if(webService == _getUsageBroadbandWebService)
    {
        if(self.usageBroadbandScreenDelegate && [self.usageBroadbandScreenDelegate respondsToSelector:@selector(successfullyFetchedUsageOnBroadbandUsageScreen:withBroadbandUsageList:)]) {
            [self.usageBroadbandScreenDelegate successfullyFetchedUsageOnBroadbandUsageScreen:self withBroadbandUsageList:broadbandUsageList];
        }
        self.getUsageBroadbandWebService.usageBroadbandWebServiceDelegate = nil;
        self.getUsageBroadbandWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLUsageBroadbandWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLUsageBroadbandWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }

}

- (void)usageBroadbandWebService:(NLUsageBroadbandWebService *)webService failedToFetchBroadbandUsageWithWebServiceError:(NLWebServiceError *)webServiceError
{
    if(webService == _getUsageBroadbandWebService)
    {
         if(self.usageBroadbandScreenDelegate && [self.usageBroadbandScreenDelegate respondsToSelector:@selector(usageBroadbandScreen:failedToFetchUsageDataWithWebServiceError:)]) {
             [self.usageBroadbandScreenDelegate usageBroadbandScreen:self failedToFetchUsageDataWithWebServiceError:webServiceError];
         }
        self.getUsageBroadbandWebService.usageBroadbandWebServiceDelegate = nil;
        self.getUsageBroadbandWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLUsageBroadbandWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLUsageBroadbandWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

@end
