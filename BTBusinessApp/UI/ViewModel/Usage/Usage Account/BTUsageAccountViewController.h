//
//  BTUsageAccountViewController.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 26/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTAsset.h"
#import "BTBaseViewController.h"

@interface BTUsageAccountViewController : UIViewController

@property (nonatomic, copy) NSString *groupName;
@property (nonatomic,copy) NSString *billingAccountNumber;
@property (nonatomic, copy) NSString *assetsName;
@property (nonatomic,strong) BTAsset *selectedAsset;
@property (nonatomic,assign) BOOL isSingleBacCUG;

+ (BTUsageAccountViewController *)getUsageAccountViewController;

@end
