//
//  DLMUsagePhoneScreen.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 29/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMUsagePhoneScreen.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "CDAuthenticationToken+CoreDataProperties.h"
#import "NLUsagePhoneLineWebService.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "BTUsagePhoneProduct.h"

@interface DLMUsagePhoneScreen() <NLUsagePhoneLineWebServiceDelegate>

@property (nonatomic, strong) NLUsagePhoneLineWebService *getUsagePhoneLineWebService;

@end

@implementation DLMUsagePhoneScreen

- (void)fetchPhoneLineUsageWithAccountNumber:(NSString *)accountNumber
{
    self.getUsagePhoneLineWebService = [[NLUsagePhoneLineWebService alloc] initWithBAC:accountNumber];
    self.getUsagePhoneLineWebService.getUsagePhoneLineWebServiceDelegate = self;
    [self.getUsagePhoneLineWebService resume];
}

- (void)cancelPhoneLineUsageApi
{
    self.getUsagePhoneLineWebService.getUsagePhoneLineWebServiceDelegate = nil;
    [self.getUsagePhoneLineWebService cancel];
    self.getUsagePhoneLineWebService = nil;
}

#pragma mark - NLUsagePhoneLineWebServiceDelegate

- (void)succesfullyFetchedUsageOnPhoneLineUsageWebService:(NLUsagePhoneLineWebService *)webService withPhoneLineUsageList:(id)responseObject

{
    if(webService == _getUsagePhoneLineWebService)
    {
        NSArray *phoneLineUsageList;
        if ([[responseObject valueForKey:kNLResponseKeyForResult] isKindOfClass:[NSArray class]]) {
            NSLog(@"Phoneline response");
            
            NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
            
            NSMutableArray *resultPhoneLineUsages = [[NSMutableArray alloc] init];
            for (NSDictionary *usageDict in resultDic) {
                
                BTUsagePhoneProduct *phoneLineUsage = [[BTUsagePhoneProduct alloc] initWithResponseDictionaryFromPhoneLineUsageDetailsAPIResponse:usageDict];
                
                [resultPhoneLineUsages addObject:phoneLineUsage];
            }
            
            phoneLineUsageList = [NSArray arrayWithArray:resultPhoneLineUsages];
        
        [self.usagePhoneScreenDelegate successfullyFetchedUsageOnPhoneUsageScreen:self withPhoneLineUsageList:phoneLineUsageList];
        self.getUsagePhoneLineWebService.getUsagePhoneLineWebServiceDelegate = nil;
        self.getUsagePhoneLineWebService = nil;
        }
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLUsagePhoneLineWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLUsagePhoneLineWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}
//- (void)succesfullyFetchedUsageOnPhoneLineUsageWebService:(NLUsagePhoneLineWebService *)webService withPhoneLineUsageList:(NSArray *)phoneLineUsageList
//{
//    if(webService == _getUsagePhoneLineWebService)
//    {
//        [self.usagePhoneScreenDelegate successfullyFetchedUsageOnPhoneUsageScreen:self withPhoneLineUsageList:phoneLineUsageList];
//        self.getUsagePhoneLineWebService.getUsagePhoneLineWebServiceDelegate = nil;
//        self.getUsagePhoneLineWebService = nil;
//    }
//    else
//    {
//        DDLogError(@"This delegate method gets called because of success of an object of NLUsagePhoneLineWebService but this object is not the one stored in member variable of this class %@.", [self class]);
//        NSAssert(NO, @"This delegate method gets called because of success of an object of NLUsagePhoneLineWebService but this object is not the one stored in member variable of this class %@.", [self class]);
//    }
//}

- (void)usagePhoneLineWebService:(NLUsagePhoneLineWebService *)webService failedToFetchPhoneLineUsageWithWebServiceError:(NLWebServiceError *)error
{
    if(webService == _getUsagePhoneLineWebService)
    {
        [self.usagePhoneScreenDelegate usagePhoneScreen:self failedToFetchUsageDataWithWebServiceError:error];
        self.getUsagePhoneLineWebService.getUsagePhoneLineWebServiceDelegate = nil;
        self.getUsagePhoneLineWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLUsagePhoneLineWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLUsagePhoneLineWebService but this object is not the one stored in member variable of this class %@.", [self class]);

    }
}

#pragma mark - Private Helper methods
- (void)fetchOfflineData
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"PhoneLineUsage" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    
    NSError *error;
    
    NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if([responseObject isValidSMSessionAPIResponseObject])
    {
        BOOL isSuccess = [[responseObject valueForKey:kNLResponseKeyForIsSuccess] boolValue];
        
        if(isSuccess)
        {
            NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
            
            NSMutableArray *resultPhoneLineUsages = [[NSMutableArray alloc] init];
            for (NSDictionary *usageDict in resultDic) {
                
                BTUsagePhoneProduct *phoneLineUsage = [[BTUsagePhoneProduct alloc] initWithResponseDictionaryFromPhoneLineUsageDetailsAPIResponse:usageDict];
                
                [resultPhoneLineUsages addObject:phoneLineUsage];
            }
            
            [self.usagePhoneScreenDelegate successfullyFetchedUsageOnPhoneUsageScreen:self withPhoneLineUsageList:resultPhoneLineUsages];
            
        }
        
    }
    
}

@end
