//
//  DLMUsagePhoneScreen.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 29/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"

@class DLMUsagePhoneScreen;
@class NLWebServiceError;

@protocol DLMUsagePhoneScreenDelegate <NSObject>

- (void)successfullyFetchedUsageOnPhoneUsageScreen:(DLMUsagePhoneScreen *)phoneUsageScreen withPhoneLineUsageList:(NSArray *)phoneLineUsageList;

- (void)usagePhoneScreen:(DLMUsagePhoneScreen *)usagePhoneScreen failedToFetchUsageDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface DLMUsagePhoneScreen : DLMObject

@property (nonatomic, weak) id<DLMUsagePhoneScreenDelegate> usagePhoneScreenDelegate;

- (void)fetchPhoneLineUsageWithAccountNumber:(NSString *)accountNumber;
- (void)cancelPhoneLineUsageApi;
@end
