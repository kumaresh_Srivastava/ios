//
//  BTUsageAccountViewController.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 26/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTUsageAccountViewController.h"
#import "BTPhoneUsageAccountTableViewCell.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDUser.h"
#import "CDApp.h"
#import "CDCug+CoreDataClass.h"
#import "BTCug.h"
#import "BTBroadbandUsageAccountTableViewCell.h"
#import "BTUsageBroadbandProduct.h"
#import "DLMUsageBroadbandScreen.h"
#import "CustomSpinnerView.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "BTRetryView.h"
#import "AppConstants.h"
#import "DLMUsagePhoneScreen.h"
#import "BTUsageRetryTableViewCell.h"
#import "BTEmptyDashboardView.h"
#import "BTBroadbandUsageViewController.h"
#import "BTPhoneUsageSummaryViewController.h"
#import "BTAccountLocationDropdownView.h"
#import "BTBroadbandUsageViewController.h"
#import "CustomSpinnerView.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "NLWebServiceError.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "DLMAssetDashboardScreen.h"

@interface BTUsageAccountViewController ()<UITableViewDataSource, UITableViewDelegate,DLMUsageBroadbandScreenDelegate,DLMUsagePhoneScreenDelegate, BTRetryViewDelegate,DLMAssetDashboardScreenDelegate>
{
    //UITableView *_accountTableView;
    NSArray *_broadbandUsageArray;
    NSArray *_phoneLineUsageArray;
    BTRetryView *_retryView;
    CustomSpinnerView *_loadingView;

    BOOL _isBroadbandApiFailed;
    BOOL _isPhoneLineApiFailed;
    BOOL _isEmptyViewNeedToShowForPhoneLine;
    BOOL _isEmptyViewNeedToShowForBroadbandUsage;
    BOOL _isCloudVoiceCell;

    BTEmptyDashboardView *_emptyDashboardView;

    BOOL _isPhoneLineApiInProgress;
    BOOL _isBroadbandApiInProgress;
    BOOL _isAllBACApiRequestDone;

}

@property (weak, nonatomic) IBOutlet UITableView *accountTableView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *groupNameLabel;
@property (nonatomic, strong) DLMUsageBroadbandScreen *usageBroadbandScreen;
@property (nonatomic, strong) DLMUsagePhoneScreen *usagePhoneLineScreen;
@property (nonatomic, readwrite) DLMAssetDashboardScreen *assetDashboardScreenModel;


@end

@implementation BTUsageAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configureInitialUI];

    UINib *phoneDataCell = [UINib nibWithNibName:@"BTPhoneUsageAccountTableViewCell" bundle:nil];
    [_accountTableView registerNib:phoneDataCell forCellReuseIdentifier:@"BTPhoneUsageAccountTableViewCell"];

    UINib *broadbandPhoneCell = [UINib nibWithNibName:@"BTBroadbandUsageAccountTableViewCell" bundle:nil];
    [_accountTableView registerNib:broadbandPhoneCell forCellReuseIdentifier:@"BTBroadbandUsageAccountTableViewCell"];

    UINib *broadbandRetryCell = [UINib nibWithNibName:@"BTUsageRetryTableViewCell" bundle:nil];
    [_accountTableView registerNib:broadbandRetryCell forCellReuseIdentifier:@"BTUsageRetryTableViewCell"];
    
    UINib *dropdownCell = [UINib nibWithNibName:@"BTOCSDropDownTableViewCell" bundle:nil];
    [_accountTableView registerNib:dropdownCell forCellReuseIdentifier:@"BTOCSDropDownTableViewCell"];

     self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

    self.usageBroadbandScreen = [[DLMUsageBroadbandScreen alloc] init];
    self.usageBroadbandScreen.usageBroadbandScreenDelegate = self;

    self.usagePhoneLineScreen = [[DLMUsagePhoneScreen alloc] init];
    self.usagePhoneLineScreen.usagePhoneScreenDelegate = self;
    _isAllBACApiRequestDone = YES;
    [self fetchUsageDataFromServer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

    [self.usagePhoneLineScreen cancelPhoneLineUsageApi];
    [self.usageBroadbandScreen cancelBroadbandUsageApi];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if( appDelegate.isSingleBacCug == YES){
        if (self.isMovingFromParentViewController || self.isBeingDismissed) { // BACK Button is PRESSED
            appDelegate.isSingleBacCugBackNavButtonPressed = YES;
        }
        else{
            appDelegate.isSingleBacCugBackNavButtonPressed = NO;
        }
    }
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    if(self.isSingleBacCUG == YES){
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationItem.leftBarButtonItem=nil;
        self.navigationItem.hidesBackButton=YES;
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if(appDelegate.isSingleBacCugBackNavButtonPressed == YES){
            appDelegate.isSingleBacCugBackNavButtonPressed = NO;
            return;
        }
        if(_isAllBACApiRequestDone == NO){
            // Fetch ALLBAC to see if we receive more accounts
            self.assetDashboardScreenModel = [[DLMAssetDashboardScreen alloc] init];
            self.assetDashboardScreenModel.assetDashboardScreenDelegate = self;
            [self fetchAssetsDashboardAPI];
           _accountTableView.hidden = YES;
        }
        _isAllBACApiRequestDone = NO;
    }
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = OMNIPAGE_USAGE_ASSET;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}


#pragma mark - Public Methods

+ (BTUsageAccountViewController *)getUsageAccountViewController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    BTUsageAccountViewController *controller = (BTUsageAccountViewController *)[storyboard instantiateViewControllerWithIdentifier:@"BTUsageAccountViewController"];

    return controller;

}

#pragma mark - Initial UI Methods

- (void)configureInitialUI
{
    if(!self.groupName)
    {
        UIFont *titleFont = self.titleLabel.font;
        self.titleLabel.font = [titleFont fontWithSize:20];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    self.titleLabel.text = self.assetsName;//@"Usage";
    self.groupNameLabel.text = self.groupName;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    _isEmptyViewNeedToShowForPhoneLine = NO;
    _isEmptyViewNeedToShowForBroadbandUsage = NO;

    self.view.backgroundColor = [UIColor colorForHexString:@"eeeeee"];

    _accountTableView.backgroundColor = [UIColor colorForHexString:@"eeeeee"];//[BrandColours colourBtNeutral30];
    _accountTableView.delegate = self;
    _accountTableView.dataSource = self;
    _accountTableView.showsVerticalScrollIndicator = NO;
    _accountTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:_accountTableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:_accountTableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:_accountTableView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:_accountTableView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]
                                ]];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];

}

//- (void)statusBarOrientationChangeDetailUsageDashboard:(NSNotification *)notification {
//    // handle the interface orientation as needed
//    //[self tabelViewOrientation];
//}
//
//
//- (void) tabelViewOrientation {
//
//    CGSize screenSize = [UIScreen mainScreen].bounds.size;
//    _accountTableView.frame = CGRectMake(0, 0,screenSize.width, screenSize.height - 64);
//    _accountTableView.contentInset = UIEdgeInsetsMake(0, 0, 120, 0);
//}


#pragma mark - UITableView Delegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(_isEmptyViewNeedToShowForBroadbandUsage || _isEmptyViewNeedToShowForPhoneLine)
        return 1;
    return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [self getNumberOfRowsInSectionWithSection:section];

}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Background color
    view.tintColor = [UIColor colorForHexString:@"eeeeee"];//[BrandColours colourBtNeutral30];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}


- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 40)];
    [headerView setBackgroundColor:[UIColor colorForHexString:@"eeeeee"]];//[BrandColours colourBtNeutral30]];

    NSString *sectionTitle = nil;

    if(section == 0)
    {
        sectionTitle = [self getSectionHeaderTitleWithSection:section];
    }
    else
    {
        sectionTitle = [self getSectionHeaderTitleWithSection:section];
    }

//    UIView *topSepView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, headerView.frame.size.width, 1)];
//    topSepView.backgroundColor = [BrandColours colourBtNeutral50];
//    [headerView addSubview:topSepView];

    UILabel *headerTitleLable = [[UILabel alloc] initWithFrame:CGRectMake(15, 8,tableView.bounds.size.width - 40, 25)];
    headerTitleLable.text = sectionTitle;
    headerTitleLable.textColor = [UIColor colorForHexString:@"333333"];//[BrandColours colourBtLightBlack];
    headerTitleLable.font = [UIFont fontWithName:@"BTFont-Bold" size:14];
    headerTitleLable.backgroundColor = [UIColor clearColor];
    [headerView addSubview:headerTitleLable];

//    UIView *bottomSepView = [[UIView alloc] initWithFrame:CGRectMake(0, headerView.frame.size.height - 1, headerView.frame.size.width, 1)];
//    bottomSepView.backgroundColor = [BrandColours colourBtNeutral50];
//    [headerView addSubview:bottomSepView];

    return headerView;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    _isCloudVoiceCell = NO;
    if(indexPath.section == 0)
    {
        if(!_isEmptyViewNeedToShowForBroadbandUsage)
        {
            BTBroadbandUsageAccountTableViewCell *broadbandAccountCell = [tableView dequeueReusableCellWithIdentifier:@"BTBroadbandUsageAccountTableViewCell" forIndexPath:indexPath];
            broadbandAccountCell.selectionStyle = UITableViewCellSelectionStyleNone;
            [broadbandAccountCell updateCellWithBroadbandUsage:[_broadbandUsageArray objectAtIndex:indexPath.row]];
            return broadbandAccountCell;
        }
        else
        {
            BTUsagePhoneProduct *phoneProduct = (BTUsagePhoneProduct *)[_phoneLineUsageArray objectAtIndex:indexPath.row];
            if ([phoneProduct.serviceId rangeOfString:@"cv" options:NSCaseInsensitiveSearch].location != NSNotFound) { // Cloud Voice details
                _isCloudVoiceCell = YES;
            }
            BTPhoneUsageAccountTableViewCell *phoneAccountCell = [tableView dequeueReusableCellWithIdentifier:@"BTPhoneUsageAccountTableViewCell" forIndexPath:indexPath];
            phoneAccountCell.selectionStyle = UITableViewCellSelectionStyleNone;
            [phoneAccountCell updateCellWithPhoneLineUsage:[_phoneLineUsageArray objectAtIndex:indexPath.row] withCloud:_isCloudVoiceCell];
            return phoneAccountCell;
        }
        
    }

    else
    {
        if(!_isEmptyViewNeedToShowForPhoneLine)
        {
            BTUsagePhoneProduct *phoneProduct = (BTUsagePhoneProduct *)[_phoneLineUsageArray objectAtIndex:indexPath.row];
            if ([phoneProduct.serviceId rangeOfString:@"cv" options:NSCaseInsensitiveSearch].location != NSNotFound) { // Cloud Voice details
                _isCloudVoiceCell = YES;
            }
            BTPhoneUsageAccountTableViewCell *phoneAccountCell = [tableView dequeueReusableCellWithIdentifier:@"BTPhoneUsageAccountTableViewCell" forIndexPath:indexPath];
            phoneAccountCell.selectionStyle = UITableViewCellSelectionStyleNone;
            [phoneAccountCell updateCellWithPhoneLineUsage:[_phoneLineUsageArray objectAtIndex:indexPath.row] withCloud:_isCloudVoiceCell];
            return phoneAccountCell;
        }
        else
        {
            BTBroadbandUsageAccountTableViewCell *broadbandAccountCell = [tableView dequeueReusableCellWithIdentifier:@"BTBroadbandUsageAccountTableViewCell" forIndexPath:indexPath];
            broadbandAccountCell.selectionStyle = UITableViewCellSelectionStyleNone;
            [broadbandAccountCell updateCellWithBroadbandUsage:[_broadbandUsageArray objectAtIndex:indexPath.row]];
            return broadbandAccountCell;
        }

    }
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        if(!_isEmptyViewNeedToShowForBroadbandUsage)
            return 114;
        else
            return 96;
    }
    else
    {
        if(!_isEmptyViewNeedToShowForPhoneLine)
            return 96;
        else
            return 114;
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if([_phoneLineUsageArray count] > 0 && [_broadbandUsageArray count] >0)
    {

        if(indexPath.section == 0)
        {
            [self trackOmnitureClickEventForService:OMNICLICK_USAGE_BROADBAND];
            BTUsageBroadbandProduct *broadbandProduct = (BTUsageBroadbandProduct *)[_broadbandUsageArray objectAtIndex:indexPath.row];
            BTBroadbandUsageViewController *controller = [[BTBroadbandUsageViewController alloc] init];
            controller.assetIntegrationID = broadbandProduct.assetIntegrationId;
            controller.selectedAsset = self.selectedAsset;
            [self.navigationController pushViewController:controller animated:YES];
            
        }
        
        else
        {
            [self trackOmnitureClickEventForService:OMNICLICK_USAGE_PHONELINE];
            BTUsagePhoneProduct *phoneProduct = (BTUsagePhoneProduct *)[_phoneLineUsageArray objectAtIndex:indexPath.row];
            BTPhoneUsageSummaryViewController *phoneUsageVC = [[BTPhoneUsageSummaryViewController alloc] init];
            //BTPhoneUsageSummaryViewController *phoneUsageVC = [[BTPhoneUsageSummaryViewController alloc] initWithStyle:UITableViewStylePlain];
//            phoneUsageVC.view.frame = self.accountTableView.frame;
            phoneUsageVC.phoneUsageModel = phoneProduct;
            phoneUsageVC.selectedAsset = self.selectedAsset;
            phoneUsageVC.serviceNumber = phoneProduct.serviceId;
            if ([phoneProduct.serviceId rangeOfString:@"cv" options:NSCaseInsensitiveSearch].location != NSNotFound) { // Cloud Voice details
                phoneUsageVC.isCloudVoiceExpress = YES;
                phoneUsageVC.navigationTitle = @"Cloud Voice Express usage";
            }
            else { // Other phone calls details
                phoneUsageVC.isCloudVoiceExpress = NO;
                phoneUsageVC.navigationTitle  = @"Phone usage summary";
            }
            [self.navigationController pushViewController:phoneUsageVC animated:YES];

        }
    }


    else  if([_phoneLineUsageArray count] > 0)
    {

        [self trackOmnitureClickEventForService:OMNICLICK_USAGE_PHONELINE];
        BTUsagePhoneProduct *phoneProduct = (BTUsagePhoneProduct *)[_phoneLineUsageArray objectAtIndex:indexPath.row];
        BTPhoneUsageSummaryViewController *phoneUsageVC = [[BTPhoneUsageSummaryViewController alloc] init];
        phoneUsageVC.phoneUsageModel = phoneProduct;
        phoneUsageVC.selectedAsset = self.selectedAsset;
        phoneUsageVC.serviceNumber = phoneProduct.serviceId;
        if ([phoneProduct.serviceId rangeOfString:@"cv" options:NSCaseInsensitiveSearch].location != NSNotFound) { // Cloud Voice details
            phoneUsageVC.isCloudVoiceExpress = YES;
            phoneUsageVC.navigationTitle = @"Cloud Voice Express usage";
        }
        else { // Other phone calls details
            phoneUsageVC.isCloudVoiceExpress = NO;
            phoneUsageVC.navigationTitle  = @"Phone usage summary";
        }
        [self.navigationController pushViewController:phoneUsageVC animated:YES];

    }

    else  if([_broadbandUsageArray count] > 0)
    {
        [self trackOmnitureClickEventForService:OMNICLICK_USAGE_BROADBAND];
        BTUsageBroadbandProduct *broadbandProduct = (BTUsageBroadbandProduct *)[_broadbandUsageArray objectAtIndex:indexPath.row];
        BTBroadbandUsageViewController *controller = [[BTBroadbandUsageViewController alloc] init];
        controller.assetIntegrationID = broadbandProduct.assetIntegrationId;
        controller.selectedAsset = self.selectedAsset;

        [self.navigationController pushViewController:controller animated:YES];
    }


}




#pragma mark - Private Helper Methods

- (void)updateUI {

    if(_isBroadbandApiInProgress || _isPhoneLineApiInProgress)
        return;
    
    [self hideLoadingItems:YES];
    [self hideRetryItems:YES];
    [self hideEmmptyDashBoardItems:YES];
    
    if((_isBroadbandApiFailed && _isPhoneLineApiFailed) || (_isBroadbandApiFailed && _isEmptyViewNeedToShowForPhoneLine) || (_isPhoneLineApiFailed && _isEmptyViewNeedToShowForBroadbandUsage))
    {
        [self hideRetryItems:NO];
        [self showRetryViewWithInternetStrip:NO];
    }
    else if(_isEmptyViewNeedToShowForPhoneLine && _isEmptyViewNeedToShowForBroadbandUsage)
    {
        [self showEmptyView];
    }
    else
    {
        _accountTableView.hidden = NO;
        [_accountTableView reloadData];
    }

}


- (NSInteger)getNumberOfRowsInSectionWithSection:(NSInteger)section
{
    if(section == 0)
    {
        if(!_isEmptyViewNeedToShowForBroadbandUsage)
            return _broadbandUsageArray.count;
        else
            if(!_isEmptyViewNeedToShowForPhoneLine)
                return _phoneLineUsageArray.count;
    }
    else
    {
        if(!_isEmptyViewNeedToShowForPhoneLine)
            return _phoneLineUsageArray.count;
        else
            if(!_isEmptyViewNeedToShowForBroadbandUsage)
                return _broadbandUsageArray.count;
    }
    return 0;

}



- (NSString *)getSectionHeaderTitleWithSection:(NSInteger)section
{
    NSString *sectionTitle = nil;
    if(section == 0)
    {
        if(!_isEmptyViewNeedToShowForBroadbandUsage)
            sectionTitle = @"Broadband";
        else if(!_isEmptyViewNeedToShowForPhoneLine)
            sectionTitle = @"Phone line";
    }
    else
    {
        if(!_isEmptyViewNeedToShowForPhoneLine)
            sectionTitle = @"Phone line";
        else if(!_isEmptyViewNeedToShowForBroadbandUsage)
            sectionTitle = @"Broadband";
    }

    return sectionTitle;

}

- (void)fetchAssetsDashboardAPI {
    
    if([AppManager isInternetConnectionAvailable])
    {
        [self hideRetryItems:YES];
        [self createLoadingView];
        [self.assetDashboardScreenModel fetchAssetsDashboardDetails];
    }
    else
    {
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_ASSETS_ACCOUNT];
    }
}

- (void)fetchUsageDataFromServer
{
    if ([_billingAccountNumber containsString:@"***"])
    {
        [self showEmptyView];
    }
    else
    {
        [self fetchUsageBroadbandAPI];
        [self fetchUsagePhoneLineAPI];
    }
}

- (void)fetchUsageBroadbandAPI
{

    if([AppManager isInternetConnectionAvailable]) {
        [self hideRetryItems:YES];
        _isBroadbandApiInProgress = YES;
        _accountTableView.hidden = YES;
        [self createLoadingView];
        [self.usageBroadbandScreen fetchBroadbandUsageWithAccountNumber:self.billingAccountNumber];

    }
    else {
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_USAGE_ASSET];
    }

}

- (void)fetchUsagePhoneLineAPI {

    if([AppManager isInternetConnectionAvailable])
    {
        [self hideRetryItems:YES];
        _accountTableView.hidden = YES;
        _isPhoneLineApiInProgress = YES;
        [self createLoadingView];
        [self.usagePhoneLineScreen fetchPhoneLineUsageWithAccountNumber:self.billingAccountNumber];
//        [self.usagePhoneLineScreen fetchPhoneLineUsageWithAccountNumber:@"GP00150789"];// To Do : Manik Hardcoding the BAC Number to test remove it later

    }
    else
    {
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_USAGE_ASSET];
    }
}




- (void)createLoadingView {

    if(!_loadingView)
    {
        _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
        [_loadingView startAnimatingLoadingIndicatorView];

        [self.view addSubview:_loadingView];

        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    }
}

- (void)hideLoadingItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to loading.

    [_loadingView stopAnimatingLoadingIndicatorView];
    [_loadingView setHidden:isHide];
    [_loadingView removeFromSuperview];
    _loadingView = nil;
}


- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow {

    if(_retryView)
    {
        _retryView.hidden = NO;
        _retryView.retryViewDelegate = self;
        [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
        return;
    }


    _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
    _retryView.translatesAutoresizingMaskIntoConstraints = NO;
    _retryView.retryViewDelegate = self;
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
    [self.view addSubview:_retryView];

   // [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:64.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-50.0]];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];

}

- (void)hideRetryItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to retry.
    [_retryView setHidden:isHide];


}


- (void)showEmptyView {

    
    [OmnitureManager trackError:OMNIERROR_NO_ASSET_TO_DISPLAY onPageWithName:OMNIPAGE_USAGE_ASSET contextInfo:[AppManager getOmniDictionary]];
    
    _emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTEmptyDashboardView" owner:nil options:nil] objectAtIndex:0];
    _emptyDashboardView.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSURL *url;
    
    if ([kBTServerType isEqualToString:@"BTServerTypeProd"]) {
        url = [[NSURL alloc] initWithString:@"https://secure.business.bt.com/Account/Addbillingaccounts.aspx"];
    } else{
        url = [[NSURL alloc] initWithString:@"https://eric1-dmze2e-ukb.bt.com/Account/Addbillingaccounts.aspx"];
    }
    
    NSMutableAttributedString *hypLink = [[NSMutableAttributedString  alloc] initWithString:@"Your products and services will show here once your bill is ready to view online. If you have recently moved to a BT OneBill and you can no longer see your products and services, click here to add missing accounts."];
    
    NSDictionary *linkAttributes = @{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle),
                                     NSLinkAttributeName : url};
    
    [hypLink addAttributes:linkAttributes range:NSMakeRange(185,4)];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
    [hypLink addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,hypLink.length)];
    
    [hypLink addAttribute:NSForegroundColorAttributeName value:[BrandColours colourBtNeutral70] range:NSMakeRange(0, hypLink.length)];
    
    [hypLink addAttribute:NSFontAttributeName
                    value:[UIFont fontWithName:kBtFontRegular size:16.0] range:NSMakeRange(0, hypLink.length)];
    
    [_emptyDashboardView updateEmptyDashboardViewWithImageNameAndLink:@"package" title:@"None to display" detailText:hypLink andButtonTitle:nil];
    
    _emptyDashboardView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_emptyDashboardView];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];

}


- (void)hideEmmptyDashBoardItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to retry.
    [_emptyDashboardView setHidden:isHide];
    [_emptyDashboardView removeFromSuperview];
    _emptyDashboardView = nil;

}
#pragma mark - DLMAssetDashboardScreenDelegate ALLBAC API response
- (void)successfullyFetchedAssetsDashboardDataOnDLMAssetDashboardScreen:(DLMAssetDashboardScreen *)assetDashboardScreen
{
    
    if(assetDashboardScreen.assetsArray.count > 0)
    {
        NSArray* assetsArray =  [assetDashboardScreen.assetsArray mutableCopy];
        NSArray* cugs = [[AppDelegate sharedInstance] savedCugs];
        if(cugs.count > 1 || assetsArray.count > 1){
            self.isSingleBacCUG = NO;
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            appDelegate.isSingleBacCugBackNavButtonPressed = YES;
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [self fetchUsageDataFromServer];
        }
    }
    else
    {
        DDLogError(@"Assets Dashboard: Assets not found.");
    }
    
    
    
}

- (void)assetDashboardScreen:(DLMAssetDashboardScreen *)assetDashboardScreen failedToFetchAssetsDashboardDataWithWebServiceError:(NLWebServiceError *)webServiceError {
    
    [self fetchUsageDataFromServer];
}

#pragma mark - DLMUsageBroadbandScreenDelegate

- (void)successfullyFetchedUsageOnBroadbandUsageScreen:(DLMUsageBroadbandScreen *)broadbandUsageScreen withBroadbandUsageList:(NSArray *)broadbandUsageList
{
    _isBroadbandApiInProgress = NO;
    _isBroadbandApiFailed = NO;
    _isEmptyViewNeedToShowForBroadbandUsage = NO;


    if(broadbandUsageList.count > 0)
    {
        _broadbandUsageArray = broadbandUsageList;
    }
    else if(broadbandUsageList.count == 0)
    {
        _isEmptyViewNeedToShowForBroadbandUsage = YES;
        
        //Remove this later
        NSLog(@"No data found!!!");
        

    }
    else
    {
        DDLogError(@"Usage Dashboard: Usage not found.");
    }


    [self updateUI];
}

- (void)usageBroadbandScreen:(DLMUsageBroadbandScreen *)usageBroadbandScreen failedToFetchUsageDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    _isEmptyViewNeedToShowForBroadbandUsage = NO;
    _isBroadbandApiInProgress = NO;

    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];

    if(errorHandled)
    {
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_USAGE_ASSET];
    }
    
    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {

        switch (webServiceError.error.code)
        {

            case BTNetworkErrorCodeAPINoDataFound:
            {
                [AppManager trackNoDataFoundErrorOnPage:OMNIPAGE_USAGE_ASSET];
                errorHandled = YES;
                _isEmptyViewNeedToShowForBroadbandUsage = YES;
                break;
            }
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }

    if(errorHandled == NO)
    {
        _isBroadbandApiFailed = YES;
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_USAGE_ASSET];
    }

    [self updateUI];
}


#pragma mark - DLMUsagePhoneScreenDelegate

- (void)successfullyFetchedUsageOnPhoneUsageScreen:(DLMUsagePhoneScreen *)phoneUsageScreen withPhoneLineUsageList:(NSArray *)phoneLineUsageList
{
    _isPhoneLineApiInProgress = NO;
    _isPhoneLineApiFailed = NO;
    _isEmptyViewNeedToShowForPhoneLine = NO;


    if(phoneLineUsageList.count > 0)
    {
        _phoneLineUsageArray = phoneLineUsageList;
    }
    else if(phoneLineUsageList.count == 0)
    {
        _isEmptyViewNeedToShowForPhoneLine = YES;
    }
    else
    {
        DDLogError(@"Usage Phone Line : Usage not found.");
    }

    [self updateUI];

}

- (void)usagePhoneScreen:(DLMUsagePhoneScreen *)usagePhoneScreen failedToFetchUsageDataWithWebServiceError:(NLWebServiceError *)webServiceError {

    _isEmptyViewNeedToShowForPhoneLine = NO;
    _isPhoneLineApiInProgress = NO;

    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];

    if(errorHandled)
    {
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_USAGE_ASSET];
    }
    
    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {

        switch (webServiceError.error.code)
        {

            case BTNetworkErrorCodeAPINoDataFound:
            {
                [AppManager trackNoDataFoundErrorOnPage:OMNIPAGE_USAGE_ASSET];
                errorHandled = YES;
                _isEmptyViewNeedToShowForPhoneLine = YES;
                break;
            }
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }

    if(errorHandled == NO)
    {
        _isPhoneLineApiFailed = YES;
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_USAGE_ASSET];
    }

    [self updateUI];

}

#pragma mark - Action Methods

- (IBAction)backButtonAction:(id)sender {

    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - BTRetryView Delegate

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    if(_isPhoneLineApiFailed)
    {
        [self fetchUsagePhoneLineAPI];
    }
    if(_isBroadbandApiFailed)
    {
        [self fetchUsageBroadbandAPI];
    }
    if(!_isBroadbandApiFailed && !_isPhoneLineApiFailed)
    {
        [self fetchUsagePhoneLineAPI];
        [self fetchUsageBroadbandAPI];
    }
}



#pragma mark - Omniture Methods

- (void)trackOmnitureClickEventForService:(NSString *)service
{
    NSString *pageName = OMNIPAGE_USAGE_ASSET;
    NSString *linkTitle = [NSString stringWithFormat:@"Service details %@",service];
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:data];
}

@end
