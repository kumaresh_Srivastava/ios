//
//  DLMBroadbandUsageScreen.m
//  BTBusinessApp
//
//  Created by VectoScalar on 12/22/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMBroadbandUsageScreen.h"
#import "NLBroadbandUsageWebService.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "CDAuthenticationToken.h"
#import "AppDelegateViewModel.h"
#import "AppDelegate.h"
#import <SAMKeychain/SAMKeychain.h>
#import "BTUser.h"
#import "BTCug.h"
#import "AppConstants.h"
#import "NSObject+APIResponseCheck.h"
#import "BTBroadbandUsage.h"
#import "BrandColours.h"
#define kUnlimitedText @"UNLIMITED"

#define kOmniUnlimitedUsage @"Usage:Broadband currently usage:unlimited"
#define kOmniUnderLimitUsage @"Usage:Broadband currently usage:limited under"
#define kOmniAlmostUsage @"Usage:Broadband currently usage:limited almost"
#define kOmniLimitOverUsage @"Usage:Broadband currently usage:limited over" //RM


@interface DLMBroadbandUsageScreen()<NLBroadbandUsageWebServiceDelegate>{
    
    NSArray *_currentUsageArray;
    NSArray *_monthlyUsageArray;
    BTBroadbandUsage *_broadbandUsage;
    
    NSString *_contentViewedString;
    NSString *_omniUsageString;//RM
}


@property(nonatomic, strong) NLBroadbandUsageWebService *broadbandWebService;

@end



@implementation DLMBroadbandUsageScreen


#pragma mark- public methods
- (void)fetchBroadbandUsageForAssetIntegrationID:(NSString *)assetIntegrationID
{
    
    self.broadbandWebService = [[NLBroadbandUsageWebService alloc] initWithBroadbandAssetIntegrationID:assetIntegrationID];
    self.broadbandWebService.broadbandWebServiceDelegate = self;
    [self.broadbandWebService resume];
    
}

- (void)cancelBroadbandUsageSevice
{
    
    self.broadbandWebService.broadbandWebServiceDelegate = nil;
    [self.broadbandWebService cancel];
    self.broadbandWebService = nil;
}


- (NSArray *)getCurrentUsageSectionArray{
    
    return _currentUsageArray;
}


- (NSArray *)getMonthlyUsageSectionArray{
    
    return _monthlyUsageArray;
}


#pragma mark- Private Helper methods

- (void)createData{
    
    //Current tab
    NSMutableArray *currentUsageSectionArray = [NSMutableArray array];
    NSMutableArray *monthlyUsageSectionArray = [NSMutableArray array];
    
    NSMutableArray *currentUsageWrapperArray = [NSMutableArray array];
    
    NSString *usageUnit = @"";
    if(_broadbandUsage && [_broadbandUsage.displayUnit validAndNotEmptyStringObject]){
        usageUnit = _broadbandUsage.displayUnit;
    }
    
    //***************Usage Summary******************
    BroadbandUsageRowWrapper *usageSummaryWrapper = [[BroadbandUsageRowWrapper alloc] init];
    usageSummaryWrapper.cellType = BroadbandUsageRowTypeUsageSummary;
    
    //Totoal Usage
    usageSummaryWrapper.subHeading1 = [NSString stringWithFormat:@"%.2f %@", _broadbandUsage.totalUsage, usageUnit];
    
    
    //Usage Limit
    if(_broadbandUsage && [_broadbandUsage.usageLimit validAndNotEmptyStringObject]){
        
        if([[_broadbandUsage.usageLimit uppercaseString] isEqualToString:kUnlimitedText]){
            
            usageSummaryWrapper.subHeading2 = [_broadbandUsage.usageLimit capitalizedString];
        }
        else{
            
            usageSummaryWrapper.subHeading2 = [NSString stringWithFormat:@"%@ %@", _broadbandUsage.usageLimit, usageUnit];
        }
        
    }
    else{
        
        usageSummaryWrapper.subHeading2 = @"-";
    }
    [currentUsageWrapperArray addObject:usageSummaryWrapper];
    
    
    
    //**************Current month usage progress******************
    BroadbandUsageRowWrapper *currentMonthUsageWrapper = [[BroadbandUsageRowWrapper alloc] init];
    currentMonthUsageWrapper.cellType = BroadbandUsageRowTypeMonthlyUsage;
    
    BOOL limitOrRemaingNotAvailable = NO;
    
    if(_broadbandUsage.usageLimit && [_broadbandUsage.usageLimit validAndNotEmptyStringObject]){
        
        
        if([[_broadbandUsage.usageLimit uppercaseString] isEqualToString:kUnlimitedText]){
            
            currentMonthUsageWrapper.usagePercentage = 0;
            
            currentMonthUsageWrapper.subHeading1 = nil;
        }
        else{
            
            currentMonthUsageWrapper.usagePercentage = [self usagaePercentageFromTotal:[_broadbandUsage.usageLimit doubleValue] andUsage:_broadbandUsage.totalUsage];
            
            //On current moth we show progress for remaining so reversing percentage
            if(currentMonthUsageWrapper.usagePercentage < 100){
                
                currentMonthUsageWrapper.usagePercentage = 100 - currentMonthUsageWrapper.usagePercentage;
            }
            
            currentMonthUsageWrapper.subHeading1 = [NSString stringWithFormat:@"%@ %@", _broadbandUsage.usageLimit, usageUnit]; //Limit
        }
        
    }
    else{
        
        currentMonthUsageWrapper.subHeading1 = @"-";
        limitOrRemaingNotAvailable = YES;
    }
    
    
    
    
    
    if([[_broadbandUsage.usageLimit uppercaseString] isEqualToString:kUnlimitedText]){
        
        _contentViewedString = @"Usage:Broadband currently usage:unlimited";
        
        currentMonthUsageWrapper.heading = [_broadbandUsage.usageLimit capitalizedString];
        currentMonthUsageWrapper.subHeading2 = @"As an unlimited broadband customer you don’t need to worry about how much data you’re using.";
        currentMonthUsageWrapper.subHeading3 = nil;
        
        _omniUsageString = kOmniUnlimitedUsage;
        
    }
    else{
        
        
        double remainingData = _broadbandUsage.bandWidthRemaining;
        
        if(remainingData < 0){
            
            remainingData = -remainingData;
        }
        
        if([_broadbandUsage.usageLimit doubleValue] > _broadbandUsage.totalUsage){
            
            _contentViewedString  = @"Usage:Broadband currently usage:limited under";
            
            _omniUsageString = kOmniUnderLimitUsage;
            
            currentMonthUsageWrapper.heading = [NSString stringWithFormat:@"%.2f %@ remaining", remainingData, usageUnit]; //Remaining
            
            if(_broadbandUsage.alertMessageStatus == AlertTypeAmber){
                
                currentMonthUsageWrapper.subHeading2 = @"You’ve almost reached your monthly allowance. If you go over we will start to charge you for the extra data.";
                
                _omniUsageString = kOmniAlmostUsage;
            }
            
        }
        else{
            
            currentMonthUsageWrapper.heading = [NSString stringWithFormat:@"%.2f %@ over allowance", remainingData, usageUnit];
            ///limitOrRemaingNotAvailable = YES;
            currentMonthUsageWrapper.subHeading2 = @"You’ve reached 100% of your monthly allowance. We started charging you for additional data usage.";//[self warningMessage];
            currentMonthUsageWrapper.alertType = AlertTypeRed;
            currentMonthUsageWrapper.subHeading3 = @"";
            
            _omniUsageString = kOmniLimitOverUsage;
        }
        
    }

    [self.broadbandUsageScreenDelegate broadbandUsageScreen:self didTrackUsage:_omniUsageString];
    
    if(limitOrRemaingNotAvailable){
        
        currentMonthUsageWrapper.usagePercentage = 100; //on 100% progress bar will be shown empty
    }
    else{
        //Means progress will be show so set the color of progress
        currentMonthUsageWrapper.progressColor = [self getColorFroCurrentStatus];
    }
    
    
    
    [currentUsageWrapperArray addObject:currentMonthUsageWrapper];
    
    
    
    //Data Cell
    BroadbandUsageRowWrapper *currentUsageInfoWrapper1 = [[BroadbandUsageRowWrapper alloc] init];
    currentUsageInfoWrapper1.cellType = BroadbandUsageRowTypeDataSummary;
    currentUsageInfoWrapper1.heading = @"Data";
    NSString *unit = @"";
    if(_broadbandUsage && [_broadbandUsage.displayUnit validAndNotEmptyStringObject]){
        unit = _broadbandUsage.displayUnit;
    } else {
        unit = @"GB";
    }
    NSString *downloadedData = [NSString stringWithFormat:@"%.2f %@", _broadbandUsage.totalDownload, unit];
    NSString *uploadedData = [NSString stringWithFormat:@"%.2f %@", _broadbandUsage.totalUpload, unit];
    currentUsageInfoWrapper1.subHeading1 = downloadedData;
    currentUsageInfoWrapper1.subHeading2 = uploadedData;
    [currentUsageWrapperArray addObject:currentUsageInfoWrapper1];
    
    
    if(_broadbandUsage.lastUsageRefreshDate){
        
        BroadbandUsageRowWrapper *currentUsageInfoWrapper2 = [[BroadbandUsageRowWrapper alloc] init];
        currentUsageInfoWrapper2.cellType = BroadbandUsageRowTypeCurrentUsageInfo;
        currentUsageInfoWrapper2.heading = @"Period";
        currentUsageInfoWrapper2.attributedTextString = [self getPeriodAttributedString];
        
        [currentUsageWrapperArray addObject:currentUsageInfoWrapper2];
        
    }
    
    
    
    NSMutableDictionary *sectionDict1 = [NSMutableDictionary dictionary];
    [sectionDict1 setValue:@"" forKey:kSectionTitleKey];
    [sectionDict1 setValue:currentUsageWrapperArray forKey:kSectionRowsKey];
    [currentUsageSectionArray addObject:sectionDict1];
    
    
    
    
    //Take Action row wrappers
    BroadbandUsageRowWrapper *viewLatestBillWrapper = [[BroadbandUsageRowWrapper alloc] init];
    viewLatestBillWrapper.cellType = BroadbandUsageRowTypeTakeAction;
    viewLatestBillWrapper.heading = kViewLatestBill;
    
    BroadbandUsageRowWrapper *viewAccountRowWrappper = [[BroadbandUsageRowWrapper alloc] init];
    viewAccountRowWrappper.cellType = BroadbandUsageRowTypeTakeAction;
    viewAccountRowWrappper.heading = kViewAccountSummary;
    
    
    NSMutableArray *takeAcrtionWrapperArray = [NSMutableArray array];
    [takeAcrtionWrapperArray addObject:viewLatestBillWrapper];
    [takeAcrtionWrapperArray addObject:viewAccountRowWrappper];
    
    
    NSMutableDictionary *sectionDict2 = [NSMutableDictionary dictionary];
    [sectionDict2 setValue:@"Useful links" forKey:kSectionTitleKey];
    [sectionDict2 setValue:takeAcrtionWrapperArray forKey:kSectionRowsKey];
    [currentUsageSectionArray addObject:sectionDict2];
    
    _currentUsageArray = currentUsageSectionArray;
    
    
    
    //Monthly Usage
    
    NSMutableArray *monthlyUsageWrapperArray = [NSMutableArray array];
    
    if(_broadbandUsage && _broadbandUsage.monthlyUsageArray && [_broadbandUsage.monthlyUsageArray count] > 0){
        
        
        for(BTMonthlyUsage *montlyUsage in _broadbandUsage.monthlyUsageArray){
            
            BroadbandUsageRowWrapper *month1Wrapper = [[BroadbandUsageRowWrapper alloc] init];
            month1Wrapper.cellType = BroadbandUsageRowTypeMonthlyUsage;
            month1Wrapper.heading = montlyUsage.monthName;
            month1Wrapper.subHeading1 = nil;
            
            NSString *usageString = [NSString stringWithFormat:@"%.2f %@", montlyUsage.totalUsage, usageUnit];
            month1Wrapper.attributedTextString = [self getMonthlyUsageAttribuittedDcitionaryForUsage:usageString];
            month1Wrapper.subHeading3 = [NSString stringWithFormat:@"%@ %@ limit", montlyUsage.productLimit, usageUnit];
            
            
            //Logic to show colr
            
            if([[montlyUsage.productLimit uppercaseString] isEqualToString:kUnlimitedText]){
                
                month1Wrapper.progressColor = [BrandColours colourMyBtGreen];
                month1Wrapper.subHeading3 = kUnlimitedText; //showing limit without unit
            }
            else if([montlyUsage.productLimit doubleValue] < montlyUsage.totalUsage){
                
                month1Wrapper.progressColor = [BrandColours colourMyBtRed];
            }
            else{
                
                month1Wrapper.progressColor = [BrandColours colourBtPrimaryColor];
            }
            
            
            //Percentage
            month1Wrapper.usagePercentage = [self usagaePercentageFromTotal:[montlyUsage.productLimit doubleValue] andUsage:montlyUsage.totalUsage];
            
            [monthlyUsageWrapperArray addObject:month1Wrapper];
        }
    }
    
    NSMutableDictionary *mothlyUsageSecDict1 = [NSMutableDictionary dictionary];
    [mothlyUsageSecDict1 setValue:@"" forKey:kSectionTitleKey];
    [mothlyUsageSecDict1 setValue:monthlyUsageWrapperArray forKey:kSectionRowsKey];
    [monthlyUsageSectionArray addObject:mothlyUsageSecDict1];
    
    
    NSMutableDictionary *sectionDict = [NSMutableDictionary dictionary];
    [sectionDict setValue:@"Useful links" forKey:kSectionTitleKey];
    [sectionDict setValue:takeAcrtionWrapperArray forKey:kSectionRowsKey];
    [monthlyUsageSectionArray addObject:sectionDict2];
    
    _monthlyUsageArray = monthlyUsageSectionArray;
    
}

- (double)usagaePercentageFromTotal:(double)total andUsage:(double)usage{
    
    if(total == 0){
        return 0;
    }
    
    if(total < usage){
        
        return 100;
    }
    
    return (usage/total * 100);
    
}


- (UIColor *)getColorFroCurrentStatus{
    
    UIColor *progressColor = [BrandColours colourMyBtGreen];
    
    if([[_broadbandUsage.usageLimit uppercaseString] isEqualToString:@"UNLIMITED"]){
        
        return progressColor;
    }
    
    
    if(_broadbandUsage){
        
        switch (_broadbandUsage.alertMessageStatus) {
                
            case AlertTypeGreen:
            {
                progressColor = [BrandColours colourTrueGreen];
            }
                break;
                
            case AlertTypeAmber:
            {
                progressColor = [BrandColours colourBTBusinessRed];
            }
                break;
                
            case AlertTypeRed:
            {
                progressColor = [BrandColours colourMyBtRed];
                
            }
                break;
                
            default:
                break;
        }
    }
    
    
    if(!progressColor){
        
        progressColor = [UIColor clearColor];
    }
    
    
    return progressColor;
}


- (NSString *)warningMessage{
    
    NSString *warningMessage = nil;
    
    if(_broadbandUsage){
        
        switch (_broadbandUsage.alertMessageStatus) {
                
            case 1:
            {
                
            }
                break;
                
            case 2:
            {
                
            }
                break;
                
            case 3:
            {
                warningMessage = @"You’ve reached 100% of your monthly allowance. We started charging you for additional data usage.";
                
            }
                break;
                
            default:
                break;
        }
    }
    
    return warningMessage;
    
}


- (NSAttributedString *)getMonthlyUsageAttribuittedDcitionaryForUsage:(NSString *)usageString{
    
    
    UIFont *textFont = [UIFont fontWithName:kBtFontBold size:13.0];
    NSDictionary *dataPartDict = [NSDictionary dictionaryWithObject:textFont forKey:NSFontAttributeName];
    
    textFont = [UIFont fontWithName:kBtFontRegular size:13.0];
    NSDictionary *textPartDict = [NSDictionary dictionaryWithObject:textFont forKey:NSFontAttributeName];
    
    
    //Download
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:usageString attributes:dataPartDict];
    
    NSMutableAttributedString *textString = [[NSMutableAttributedString alloc]initWithString:@" used in total" attributes:textPartDict];
    [aAttrString appendAttributedString:textString];
    
    return aAttrString;
    
}

- (NSAttributedString *)dataStringForUsage:(double)download andUpload:(double)upload{
    
    NSString *usageUnit = @"";
    if(_broadbandUsage && [_broadbandUsage.displayUnit validAndNotEmptyStringObject]){
        
        usageUnit = _broadbandUsage.displayUnit;
    }
    
    NSString *downloadedData = [NSString stringWithFormat:@"%.2f %@", download, usageUnit];
    
    NSString *uploadedData = [NSString stringWithFormat:@"%.2f %@", upload, usageUnit];
    
    UIFont *textFont = [UIFont fontWithName:kBtFontBold size:18.0];
    NSDictionary *dataPartDict = [NSDictionary dictionaryWithObject:textFont forKey:NSFontAttributeName];
    
    textFont = [UIFont fontWithName:kBtFontBold size:13.0];
    NSDictionary *textPartDict = [NSDictionary dictionaryWithObject:textFont forKey:NSFontAttributeName];
    
    //Download
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:downloadedData attributes:dataPartDict];
    
    NSMutableAttributedString *downloadText = [[NSMutableAttributedString alloc]initWithString:@" downloaded  " attributes:textPartDict];
    [aAttrString appendAttributedString:downloadText];
    
    //Upload
    NSMutableAttributedString *uploadDataAttString = [[NSMutableAttributedString alloc] initWithString:uploadedData attributes:dataPartDict];
    [aAttrString appendAttributedString:uploadDataAttString];
    
    NSMutableAttributedString *uploadText = [[NSMutableAttributedString alloc]initWithString:@" uploaded" attributes:textPartDict];
    [aAttrString appendAttributedString:uploadText];
    
    return aAttrString;
    
}


- (NSAttributedString *)getPeriodAttributedString{
    
    
    UIFont *textFont = [UIFont fontWithName:kBtFontRegular size:15.0];
    NSDictionary *textDict = [NSDictionary dictionaryWithObject:textFont forKey:NSFontAttributeName];
    
    
    NSString *monthString = [self monthStringFromDate:_broadbandUsage.lastUsageRefreshDate];
    
    NSString *yearString = [self yearStringFromDate:_broadbandUsage.lastUsageRefreshDate];
    
    NSString *dayString = [self dayOfTheMonthWithSuffixForDate:_broadbandUsage.lastUsageRefreshDate];
    
    NSString *timeString = [self timeStringFromDate:_broadbandUsage.lastUsageRefreshDate];
    
    
    NSString *textString = [NSString stringWithFormat:@"Current usage for %@. Last updated %@ %@ %@ at %@.", monthString, dayString, monthString, yearString, timeString];
    
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:textString attributes:textDict];
    
    
    
    return aAttrString;
    
}


- (NSString *)dayOfTheMonthWithSuffixForDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger dayOfMonth = [calendar component:NSCalendarUnitDay fromDate:date];
    NSString *suffix = @"";
    
    switch (dayOfMonth) {
        case 1:
        case 21:
        case 31: {
            
            suffix = @"st";
        }
            break;
            
        case 2:
        case 22:{
            
            suffix = @"nd";
        }
            break;
            
        case 3:
        case 23:
        {
            suffix = @"rd";
        }
            break;
            
        default:
            suffix = @"th";
    }
    
    
    if(dayOfMonth<10){
        
        return [NSString stringWithFormat:@"0%ld%@", (long)dayOfMonth, suffix];
    }
    
    return [NSString stringWithFormat:@"%ld%@", (long)dayOfMonth, suffix];
}


- (NSString *)monthStringFromDate:(NSDate *)date{
    
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    dateFormater.dateFormat = @"MMMM";
    
    return [dateFormater stringFromDate:date];
}

- (NSString *)yearStringFromDate:(NSDate *)date{
    
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    dateFormater.dateFormat = @"yyyy";
    
    return [dateFormater stringFromDate:date];
}


- (NSString *)timeStringFromDate:(NSDate *)date{
    
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    dateFormater.dateFormat = @"HH:mm";
    
    return [dateFormater stringFromDate:date];
}




#pragma mark- NLBroadbandUsageWebServiceDelegate methods

- (void)broadbandUsageWebService:(NLBroadbandUsageWebService *)webService successfullyFetchedBroadbandUsage:(BTBroadbandUsage *)broadbandUsage;
{
    
    
    if(self.broadbandWebService == webService){
        
        if(broadbandUsage){
            
            _broadbandUsage = broadbandUsage;
            
            if(broadbandUsage.serviceID && [broadbandUsage.serviceID validAndNotEmptyStringObject]){
                
                self.serviceID = [NSString stringWithFormat:@"%@", broadbandUsage.serviceID ];
                
            }
            else{
                
                self.serviceID = @"-";
            }
            
            
            if(broadbandUsage.productName && [broadbandUsage.productName validAndNotEmptyStringObject]){
                
                self.productName = broadbandUsage.productName;
            }
            
            self.alertStatus = _broadbandUsage.alertMessageStatus;
            
            [self createData];
            
        }
        
        [self.broadbandUsageScreenDelegate successfullyFetchedBroadbandUsageOnBroadbandUsageScreen:self];
        
        self.broadbandWebService.broadbandWebServiceDelegate = nil;
        self.broadbandWebService = nil;
        
    }
    else{
        
        DDLogError(@"This delegate method gets called because of success of an object of NLBroadbandUsageWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLBroadbandUsageWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
    
    
}

- (void)broadbandUsageWebService:(NLBroadbandUsageWebService *)webService failedToFetchUserDataWithWebServiceError:(NLWebServiceError *)webServiceError{
    
    
    if(self.broadbandWebService == webService){
        
        [self.broadbandUsageScreenDelegate broadbandUsageScreen:self failedToFetchBBUsageDataWithWebServiceError:webServiceError];
        
        self.broadbandWebService.broadbandWebServiceDelegate = nil;
        self.broadbandWebService = nil;
        
    }
    else{

        DDLogError(@"This delegate method gets called because of failure of an object of NLBroadbandUsageWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLBroadbandUsageWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
    
}



@end


@implementation BroadbandUsageRowWrapper

@end
