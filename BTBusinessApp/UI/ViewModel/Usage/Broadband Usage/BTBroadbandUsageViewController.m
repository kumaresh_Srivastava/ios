//
//  BTBroadbandUsageViewController.m
//  BTBusinessApp
//
//  Created by VectoScalar on 12/22/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBroadbandUsageViewController.h"
#import "BTBroadbandUsageHeaderView.h"
#import "DLMBroadbandUsageScreen.h"
#import "BTTakeActionTableViewCell.h"
#import "BTBroadbandDataUsageStatusCellTableViewCell.h"
#import "BTCurrentUsageInfoCell.h"
#import "BTCurrentMonthUsageSummaryCell.h"
#import "BTBroadbandDataDownloadUploadTableViewCell.h"
#import "BTTakeActionHeaderView.h"
#import "BTBillDetailViewController.h"
#import "BTViewAssetBACOverviewViewController.h"
#import "AppManager.h"
#import "OmnitureManager.h"
#import "NLWebServiceError.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "BTNoFaultSummaryView.h"
#import "AppDelegate.h"

#define kTakeActionCell @"BTTakeActionTableViewCell"
#define kTakeActionCellID @"BTTakeActionTableViewCellID"

#define kBTBroadbandDataUsageStatusCellTableViewCell @"BTBroadbandDataUsageStatusCellTableViewCell"
#define kBTBroadbandDataUsageStatusCellTableViewCellID @"BTBroadbandDataUsageStatusCellTableViewCellID"

#define kBTCurrentMonthUsageSummaryCell @"BTCurrentMonthUsageSummaryCell"
#define kBTCurrentMonthUsageSummaryCellID @"BTCurrentMonthUsageSummaryCellID"

#define kBTCurrentUsageInfoCell @"BTCurrentUsageInfoCell"
#define kBTCurrentUsageInfoCellID @"BTCurrentUsageInfoCellID"

#define kBTDataDownloadUploadCell @"BTBroadbandDataDownloadUploadTableViewCell"
#define kBTDataDownloadUploadCellID @"BTBroadbandDataDownloadUploadTableViewCellID"

#define kTakeActionColor [BrandColours colourBtLightBlack]

@interface BTBroadbandUsageViewController ()<UITableViewDataSource, UITableViewDelegate, BTBroadbandUsageHeaderViewDelegate, DLMBroadbandUsageScreenDelegate>{
    
    UITableView *_tableView;
    NSArray *_currentUsageDataArray;
    NSArray *_monthlyUsageDataArray;
    NSInteger _seclectedTabIndex;
    NSString *pageTitleForOmniture;
    
    BTBroadbandUsageHeaderView *_headerView;
    DLMBroadbandUsageScreen *_viewModel;
}

@end

@implementation BTBroadbandUsageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    pageTitleForOmniture = OMNIPAGE_USAGE_BROADBAND_CURRENTLY;
    [self configureUI];
    
    _viewModel = [[DLMBroadbandUsageScreen alloc] init];
    _viewModel.broadbandUsageScreenDelegate = self;
    [self fetchBroadbandUsageData];
}


- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];

    [_viewModel cancelBroadbandUsageSevice];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     if( appDelegate.isSingleBacCug == YES){
        if (self.isMovingFromParentViewController || self.isBeingDismissed) { // BACK Button is PRESSED
            appDelegate.isSingleBacCugBackNavButtonPressed = YES;
        }
        else{
            appDelegate.isSingleBacCugBackNavButtonPressed = NO;
        }
    }
    
}


- (void)viewDidLayoutSubviews{
    
    [self resizeTableViewHeaderToFit];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = pageTitleForOmniture;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

#pragma mark - Private Method

- (void)configureUI{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.title = @"Broadband usage";
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    _tableView = [[UITableView alloc] init];
    _tableView.frame = CGRectMake(0,
                                  0,
                                  screenSize.width,
                                  screenSize.height - 120);
    
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 150.0;
    _tableView.hidden = YES;
    _seclectedTabIndex = 0;
    [_tableView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [self.view addSubview:_tableView];
    
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:_tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:_tableView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:_tableView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:_tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]
                                ]];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
    //Nib Registration
    UINib *nib = [UINib nibWithNibName:kTakeActionCell bundle:nil];
    [_tableView registerNib:nib forCellReuseIdentifier:kTakeActionCellID];
    
    nib = [UINib nibWithNibName:kBTBroadbandDataUsageStatusCellTableViewCell bundle:nil];
    [_tableView registerNib:nib forCellReuseIdentifier:kBTBroadbandDataUsageStatusCellTableViewCellID];
    
    nib = [UINib nibWithNibName:kBTCurrentUsageInfoCell bundle:nil];
    [_tableView registerNib:nib forCellReuseIdentifier:kBTCurrentUsageInfoCellID];
    
    
    nib = [UINib nibWithNibName:kBTCurrentMonthUsageSummaryCell bundle:nil];
    [_tableView registerNib:nib forCellReuseIdentifier:kBTCurrentMonthUsageSummaryCellID];
    
    nib = [UINib nibWithNibName:kBTDataDownloadUploadCell bundle:nil];
    [_tableView registerNib:nib forCellReuseIdentifier:kBTDataDownloadUploadCellID];
    
    //Header
    _headerView= [[[NSBundle mainBundle] loadNibNamed:@"BTBroadbandUsageHeaderView" owner:self options:nil] firstObject];
    _headerView.delegate = self;

}


//- (void)showEmptyView
//{
//    BTNoFaultSummaryView *_emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTNoFaultSummaryView" owner:nil options:nil] objectAtIndex:0];
//    [self.view addSubview:_emptyDashboardView];
//    _emptyDashboardView.frame = CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height);
//    [_emptyDashboardView updateWithErrorMessage:@"No data found" andHideReportAFaultButton:YES];
//}
- (void)showEmptyView {
    
//    _headerWithStatusColorView.hidden = NO;
//    [self.view bringSubviewToFront:_headerWithStatusColorView];
//    [self.view bringSubviewToFront:_backButton];
    if (!_retryView)
    {
        _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
        _retryView.translatesAutoresizingMaskIntoConstraints = NO;
        _retryView.retryViewDelegate = self;
        
        [self.view addSubview:_retryView];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:64.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    }

    _retryView.titleLabel.text = @"Updating data";
    NSString *errMsg = @"Sorry, we can’t show your usage right now.";
    NSString *detailMsg = @"Please try again in 24 hours.";
    [_retryView updateRetryViewWithInternetStrip:NO TryAgain:NO errorHeadline:errMsg errorDetail:detailMsg];
}

- (void)fetchBroadbandUsageData{
    
    
    if(![AppManager isInternetConnectionAvailable]){
        
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:pageTitleForOmniture];
        return;
    }
    
    if(!_assetIntegrationID)
    {
        [self showRetryViewWithInternetStrip:NO];
        return;
    }
    
    [self hideRetryView];
    
    self.networkRequestInProgress = YES;
    [_viewModel fetchBroadbandUsageForAssetIntegrationID:self.assetIntegrationID];
    
}



#pragma mark- Helper Method

- (void)resizeTableViewHeaderToFit{
    
    [_headerView setNeedsLayout];
    [_headerView layoutIfNeeded];
    
    CGFloat height = [_headerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CGRect frame = _headerView.frame;
    frame.size.height = height;
    _headerView.frame = frame;
    
    _tableView.tableHeaderView = _headerView;
    
}




- (UITableViewCell *)cellForRowWrapper:(BroadbandUsageRowWrapper *)rowWrapper{
    
    UITableViewCell *cell = nil;
    
    switch (rowWrapper.cellType) {
            
        case BroadbandUsageRowTypeTakeAction:
        {
            cell = [_tableView dequeueReusableCellWithIdentifier:kTakeActionCellID];
            [(BTTakeActionTableViewCell *)cell updateWithTitle:rowWrapper.heading];
        }
            break;
            
        case BroadbandUsageRowTypeMonthlyUsage:
        {
            cell = [_tableView dequeueReusableCellWithIdentifier:kBTBroadbandDataUsageStatusCellTableViewCellID];
            [(BTBroadbandDataUsageStatusCellTableViewCell *)cell updateWithBroadbandUsageRowWrapper:rowWrapper forMonthlyUsage:YES];
        }
            break;
            
            
        case BroadbandUsageRowTypeCurrentUsageInfo:
        {
            cell = [_tableView dequeueReusableCellWithIdentifier:kBTCurrentUsageInfoCellID];
            [(BTCurrentUsageInfoCell *)cell updateWithBroadbandUsageRowWrapper:rowWrapper];
        }
            break;
            
            
        case BroadbandUsageRowTypeUsageSummary:
        {
            cell = [_tableView dequeueReusableCellWithIdentifier:kBTCurrentMonthUsageSummaryCellID];
            [(BTCurrentMonthUsageSummaryCell *)cell updateWithBroadbandUsageRowWrapper:rowWrapper];
        }
            break;
            
        case BroadbandUsageRowTypeDataSummary:
        {
            cell = [_tableView dequeueReusableCellWithIdentifier:kBTDataDownloadUploadCellID];
            [(BTBroadbandDataDownloadUploadTableViewCell *)cell updateWithBroadbandUsageRowWrapper:rowWrapper];
        }
            break;
            
        default:
            break;
    }
    
    
    return cell;
}



- (void)trackOmnitureEvent:(NSString *)event forPage:(NSString *)page//RLM
{
    NSString *pageName = page;
    NSString *linkTitle = event;
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:data];
}


- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow {
    
    if(!_retryView){
        
        _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
        _retryView.translatesAutoresizingMaskIntoConstraints = NO;
        _retryView.retryViewDelegate = self;
        [self.view addSubview:_retryView];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    }
    
    _retryView.hidden = NO;
    _isRetryViewShown = YES;
    _retryView.retryViewDelegate = self;
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
    
}


#pragma mark- UITabeleViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if(_seclectedTabIndex == 0){
        
        return [_currentUsageDataArray count];
    }
    
    return [_monthlyUsageDataArray count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSDictionary *sectionDict = nil;
    
    if(_seclectedTabIndex == 0){
        
        sectionDict = [_currentUsageDataArray objectAtIndex:section];
    }
    else{
        
        sectionDict = [_monthlyUsageDataArray objectAtIndex:section];
    }
    
    
    if(sectionDict && [sectionDict valueForKey:kSectionRowsKey]){
        
        NSArray *rowsArray = [sectionDict valueForKey:kSectionRowsKey];
        return [rowsArray count];
    }
    
    return 0;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    NSDictionary *sectionDict = nil;
    
    if(_seclectedTabIndex == 0){
        
        sectionDict = [_currentUsageDataArray objectAtIndex:indexPath.section];
    }
    else{
        
        sectionDict = [_monthlyUsageDataArray objectAtIndex:indexPath.section];
    }
    
    
    NSArray *rowsArray = [sectionDict valueForKey:kSectionRowsKey];
    BroadbandUsageRowWrapper *rowWrapper = [rowsArray objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [self cellForRowWrapper:rowWrapper];
    
    
    if((_seclectedTabIndex == 0 && [cell isKindOfClass:[BTBroadbandDataUsageStatusCellTableViewCell class]]) || (_seclectedTabIndex == 1 && [cell isKindOfClass:[BTBroadbandDataUsageStatusCellTableViewCell class]] && indexPath.row == [rowsArray count] - 1)){
        
        [(BTBroadbandDataUsageStatusCellTableViewCell *)cell hideSeparator];
    }
    
    return cell;
    
}




- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    NSDictionary *sectionDict = nil;
    
    if(_seclectedTabIndex == 0){
        
        sectionDict = [_currentUsageDataArray objectAtIndex:section];
    }
    else{
        
        sectionDict = [_monthlyUsageDataArray objectAtIndex:section];
    }

    
    
    if([[sectionDict valueForKey:kSectionTitleKey] isEqualToString:@""]){
        
        return nil;
    }
    
     
    UIView *sectionHeaderView = [[[NSBundle mainBundle] loadNibNamed:@"BTTakeActionHeaderView" owner:self options:nil] firstObject];
    
    [(BTTakeActionHeaderView *)sectionHeaderView updateLabelColorWithColor:kTakeActionColor];
    
    return sectionHeaderView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    NSDictionary *sectionDict = nil;
    
    if(_seclectedTabIndex == 0){
        
        sectionDict = [_currentUsageDataArray objectAtIndex:section];
    }
    else{
        
        sectionDict = [_monthlyUsageDataArray objectAtIndex:section];
    }
    
    
    
    if([[sectionDict valueForKey:kSectionTitleKey] isEqualToString:@""]){
        
        return 0.0;
    }

    
    return 40.0;
}


#pragma mark- UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSDictionary *sectionDict = nil;
    
    if(_seclectedTabIndex == 0){
        
        sectionDict = [_currentUsageDataArray objectAtIndex:indexPath.section];
    }
    else{
        
        sectionDict = [_monthlyUsageDataArray objectAtIndex:indexPath.section];
    }
    
    
    NSArray *rowsArray = [sectionDict valueForKey:kSectionRowsKey];
    AssetsDetailRowWrapper *rowWrapper = [rowsArray objectAtIndex:indexPath.row];
    
    if(rowWrapper.cellType == ServiceDetailRowTypeTakeAction){
        
        
        
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if([rowWrapper.heading isEqualToString:kViewLatestBill]){
            //RM
            [self trackOmnitureEvent:@"View latest bill" forPage:pageTitleForOmniture];
            BTBillDetailViewController *billDetailVC = (BTBillDetailViewController*)[storyboard instantiateViewControllerWithIdentifier:@"BTBillDetailViewController"];
            billDetailVC.btAsset = self.selectedAsset;
            
            [self.navigationController pushViewController:billDetailVC animated:YES];
            
            
            
        }
        else if([rowWrapper.heading isEqualToString:kViewAccountSummary]){
            
            //(RM)
            [self trackOmnitureEvent:@"View account summary" forPage:pageTitleForOmniture];
            BTViewAssetBACOverviewViewController *assetVC = (BTViewAssetBACOverviewViewController*)[storyboard instantiateViewControllerWithIdentifier:@"BTViewAssetBACOverviewViewController"];
            assetVC.userSelectedBTAsset  = self.selectedAsset;
            [self.navigationController pushViewController:assetVC animated:YES];

            
        }
      
    }
    
}



#pragma mark - RetryView Delegate

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    
    [self fetchBroadbandUsageData];
}





#pragma mark- BTBroadbandUsageHeaderViewDelegate Method

- (void)btBroadbandUsageHeaderView:(BTBroadbandUsageHeaderView *)view didSelectedSegmentAtIndex:(NSInteger)index{
    
    if(index == 0)
    {
        pageTitleForOmniture = OMNIPAGE_USAGE_BROADBAND_CURRENTLY;
    }
    else
    {
        pageTitleForOmniture =  OMNIPAGE_USAGE_BROADBAND_MONTHLY;
    }
    //Event tracker
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = pageTitleForOmniture;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
    
    _seclectedTabIndex = index;
    [_tableView reloadData];
    
}



#pragma mark- DLMBroadbandUsageScreenDelegate methods

- (void)successfullyFetchedBroadbandUsageOnBroadbandUsageScreen:(DLMBroadbandUsageScreen *)broadbandUsageScreen{
    
    self.networkRequestInProgress = NO;
    
    _currentUsageDataArray = [_viewModel getCurrentUsageSectionArray];
    _monthlyUsageDataArray = [_viewModel getMonthlyUsageSectionArray];
    [_headerView updateWithProducName:_viewModel.productName serviceID:_viewModel.serviceID andAlertStatus:_viewModel.alertStatus];
    
    _tableView.hidden = NO;
    [_tableView reloadData];
}


- (void)broadbandUsageScreen:(DLMBroadbandUsageScreen *)broadbandUsageScreen failedToFetchBBUsageDataWithWebServiceError:(NLWebServiceError *)webServiceError{
    
    self.networkRequestInProgress = NO;
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
    
    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {
        
        switch (webServiceError.error.code)
        {
            case BTNetworkErrorCodeAPINoDataFound:
            {
                errorHandled = YES;
                [self showEmptyView];
                [AppManager trackNoDataFoundErrorOnPage:pageTitleForOmniture];
                break;
            }
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }
    
    if(errorHandled == NO)
    {
        [_loadingView stopAnimatingLoadingIndicatorView];
        [self showRetryViewWithInternetStrip:NO];
         [AppManager trackGenericAPIErrorOnPage:pageTitleForOmniture];
    }
    
}

- (void)broadbandUsageScreen:(DLMBroadbandUsageScreen *)broadbandUsageScreen didTrackUsage:(NSString *)usageString{
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = pageTitleForOmniture;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    [data setValue:usageString forKey:kOmniContentViewed];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}



@end
