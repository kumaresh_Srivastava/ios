//
//  DLMBroadbandUsageScreen.h
//  BTBusinessApp
//
//  Created by VectoScalar on 12/22/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
@class DLMBroadbandUsageScreen;
@class NLWebServiceError;

typedef enum{

    BroadbandUsageRowTypeUnknown,
    BroadbandUsageRowTypeTakeAction,
    BroadbandUsageRowTypeMonthlyUsage,
    BroadbandUsageRowTypeCurrentUsageInfo,
    BroadbandUsageRowTypeUsageSummary,
    BroadbandUsageRowTypeDataSummary
    
}BroadbandUsageRowType;


typedef enum{
    AlertTypeUnknown,
    AlertTypeGreen,
    AlertTypeAmber,
    AlertTypeRed
    
}AlertType;

#define kSectionTitleKey @"title"
#define kSectionRowsKey @"rows"
#define kViewLatestBill @"View latest bill"
#define kViewAccountSummary @"View account summary"



@protocol DLMBroadbandUsageScreenDelegate <NSObject>

- (void)successfullyFetchedBroadbandUsageOnBroadbandUsageScreen:(DLMBroadbandUsageScreen *)broadbandUsageScreen;

- (void)broadbandUsageScreen:(DLMBroadbandUsageScreen *)broadbandUsageScreen failedToFetchBBUsageDataWithWebServiceError:(NLWebServiceError *)webServiceError;

- (void)broadbandUsageScreen:(DLMBroadbandUsageScreen *)broadbandUsageScreen didTrackUsage:(NSString *)usageString;

@end



@interface DLMBroadbandUsageScreen : DLMObject

@property(nonatomic, weak) id<DLMBroadbandUsageScreenDelegate>broadbandUsageScreenDelegate;
@property(nonatomic, copy) NSString *productName;
@property(nonatomic, copy) NSString *serviceID;
@property(nonatomic, assign) NSInteger alertStatus;

- (void)fetchBroadbandUsageForAssetIntegrationID:(NSString *)assetIntegrationID;
- (void)cancelBroadbandUsageSevice;
- (NSArray *)getCurrentUsageSectionArray;
- (NSArray *)getMonthlyUsageSectionArray;

@end



@interface BroadbandUsageRowWrapper : NSObject
@property(nonatomic, copy) NSString *heading;
@property(nonatomic, copy) NSString *subHeading1;
@property(nonatomic, copy) NSString *subHeading2;
@property(nonatomic, copy) NSString *subHeading3;
@property(nonatomic, assign) double usagePercentage;//To show the usage progress;

@property(nonatomic, strong) UIColor *progressColor;//To show progress bar color
@property(nonatomic, copy) NSAttributedString *attributedTextString;
@property(nonatomic, assign) BroadbandUsageRowType cellType;
@property(nonatomic, assign) AlertType alertType;//Used to do some warning specific things

@end
