//
//  BTBroadbandUsageViewController.h
//  BTBusinessApp
//
//  Created by VectoScalar on 12/22/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseViewController.h"
@class BTAsset;

@interface BTBroadbandUsageViewController : BTBaseViewController

@property(nonatomic, strong) NSString *assetIntegrationID;
@property (strong,nonatomic) BTAsset *selectedAsset;

@end
