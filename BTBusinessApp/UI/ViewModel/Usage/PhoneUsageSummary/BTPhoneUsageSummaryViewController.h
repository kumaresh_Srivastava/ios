//
//  BTPhoneUsageSummaryViewController.h
//  BTBusinessApp
//
//  Created by vectoscalar on 22/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTUsagePhoneProduct.h"
#import "BTAsset.h"

@interface BTPhoneUsageSummaryViewController : UIViewController
//@interface BTPhoneUsageSummaryViewController : UITableViewController

@property (strong,nonatomic) BTUsagePhoneProduct *phoneUsageModel;
@property (strong,nonatomic) NSArray *cloudVoiceUsageModelArray;
@property (strong,nonatomic) BTAsset *selectedAsset;
@property (strong,nonatomic) NSString *serviceNumber;
@property (strong,nonatomic) NSString *navigationTitle;
@property (nonatomic, assign) BOOL isCloudVoiceExpress;

@end
