//
//  DLMPhoneUsageSummaryScreeen.h
//  BTBusinessApp
//
//  Created by vectoscalar on 23/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
#import "BTUsagePhoneProduct.h"

@class BTPhoneUsageWrapper,DLMPhoneUsageSummaryScreeen,NLWebServiceError;

typedef enum phoneUsageCellType
{
    CallInfoCell,
    PeriodCell,
    UsageBreakDownWithHeaderCell,
    UsageBreakDownCell,
    TakeActionCell

}PhoneUsageCellType;

@protocol DLMPhoneUsageSummaryScreeenDelegate <NSObject>

- (void)successfullyFetchedSummaryOnPhoneUsageSummaryScreen:(DLMPhoneUsageSummaryScreeen *)phoneUsageSummaryScreen withPhoneLineProduct:(BTUsagePhoneProduct *)phoneUsageModel;
- (void)successfullyFetchedSummaryOnCloudVoiceUsageSummaryScreen:(DLMPhoneUsageSummaryScreeen *)phoneUsageSummaryScreen withCloudVoiceDetails:(NSArray *)cloudVoiceUsageModel;

- (void)phoneUsageSummaryScreen:(DLMPhoneUsageSummaryScreeen *)phoneUsageSummaryScreen failedToFetchUsageDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface DLMPhoneUsageSummaryScreeen : DLMObject
@property (nonatomic, weak) id<DLMPhoneUsageSummaryScreeenDelegate> phoneUsageSummaryScreenDelegate;
@property (strong, nonatomic) BTUsagePhoneProduct *phoneUsage;
- (NSArray *)getPhoneUsageWrapperObjectsWithModel:(BTUsagePhoneProduct*)phoneUsageModel;
- (NSArray *)getTakeActionSectionDataArray;
- (void)fetchPhoneLineUsageWithAccountNumber:(NSString *)accountNumber andServiceNumber:(NSString *)serviceNumber;
- (void)fetchCloudVoiceUsageWithAccountNumber:(NSString *)accountNumber andServiceNumber:(NSString *)serviceNumber;
- (void)cancelPhoneLineUsageApi;

@end


@interface BTPhoneUsageWrapper : NSObject

@property (assign,nonatomic) PhoneUsageCellType cellType;
@property (strong,nonatomic) NSString *totalCost;
@property (strong,nonatomic) NSString *totalCalls;
@property (strong,nonatomic) NSString *period;
@property (strong,nonatomic) NSString *usageBreakdownTitle;
@property (strong,nonatomic) NSString *usageBreakdownCalls;
@property (strong,nonatomic) NSString *usageBreakdownCost;
@property (strong,nonatomic) NSString *takeActionTitle;

@end
