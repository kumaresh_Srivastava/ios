//
//  DLMPhoneUsageSummaryScreeen.m
//  BTBusinessApp
//
//  Created by vectoscalar on 23/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMPhoneUsageSummaryScreeen.h"
#import "AppConstants.h"
#import "NLConstants.h"
#import "BTUsagePhoneLine.h"
#import "NLUsagePhoneLineWebService.h"
#import "NSObject+APIResponseCheck.h"

@interface DLMPhoneUsageSummaryScreeen() <NLUsagePhoneLineWebServiceDelegate>

@property (nonatomic, strong) NLUsagePhoneLineWebService *getUsagePhoneLineWebService;
@property (nonatomic, strong) NLUsagePhoneLineWebService *getUsageCVWebService;
@property (nonatomic, strong) NSString *serviceNumber;
@end

@implementation DLMPhoneUsageSummaryScreeen

- (void)fetchPhoneLineUsageWithAccountNumber:(NSString *)accountNumber andServiceNumber:(NSString *)serviceNumber
{
    self.serviceNumber = serviceNumber;
    self.getUsagePhoneLineWebService = [[NLUsagePhoneLineWebService alloc] initWithBAC:accountNumber];
    self.getUsagePhoneLineWebService.getUsagePhoneLineWebServiceDelegate = self;
    [self.getUsagePhoneLineWebService resume];
}
- (void)fetchCloudVoiceUsageWithAccountNumber:(NSString *)accountNumber andServiceNumber:(NSString *)serviceNumber
{
    self.serviceNumber = serviceNumber;
    self.getUsageCVWebService = [[NLUsagePhoneLineWebService alloc] initWithBAC:accountNumber andCloudVoiceServiceId:serviceNumber];
    self.getUsageCVWebService.getUsagePhoneLineWebServiceDelegate = self;
    [self.getUsageCVWebService resume];
}

- (void)cancelPhoneLineUsageApi
{
    self.getUsagePhoneLineWebService.getUsagePhoneLineWebServiceDelegate = nil;
    [self.getUsagePhoneLineWebService cancel];
    self.getUsagePhoneLineWebService = nil;
}


#pragma mark- Public methods

- (NSArray *)getPhoneUsageWrapperObjectsWithModel:(BTUsagePhoneProduct *)phoneUsageModel
{
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    
    //Phone usage call info
    BTPhoneUsageWrapper *callInfo = [[BTPhoneUsageWrapper alloc] init];
    callInfo.cellType = CallInfoCell;
    
    if(phoneUsageModel.totalCost && phoneUsageModel.totalCost > 0)
        callInfo.totalCost = [NSString stringWithFormat:@"%@",phoneUsageModel.totalCost];
    
    
    if(phoneUsageModel.totalCalls && phoneUsageModel.totalCalls > 0)
        callInfo.totalCalls = [NSString stringWithFormat:@"%@",phoneUsageModel.totalCalls];

    [dataArray addObject:callInfo];
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterNoStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    NSRange amRange = [dateString rangeOfString:[formatter AMSymbol]];
    NSRange pmRange = [dateString rangeOfString:[formatter PMSymbol]];
    BOOL is24h = (amRange.location == NSNotFound && pmRange.location == NSNotFound);
    NSLog(@"is24h --> %@\n",(is24h ? @"YES" : @"NO"));
    
    
    NSString *finalLastTimeString = @"";
    
    if (is24h) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"hh:mm a";
        
        NSDate *date = [dateFormatter dateFromString:phoneUsageModel.latestTime];
        dateFormatter.dateFormat = @"HH:mm";
        NSString *pmamDateString = [dateFormatter stringFromDate:date];
        finalLastTimeString = pmamDateString;
    }
    else {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"hh:mm a";
        
        NSDate *date = [dateFormatter dateFromString:phoneUsageModel.latestTime];
        dateFormatter.dateFormat = @"h:mm a";
        NSString *pmamDateString = [dateFormatter stringFromDate:date];
        finalLastTimeString = pmamDateString;
    }
    
    //Phone usage Period info
    BTPhoneUsageWrapper *periodInfo = [[BTPhoneUsageWrapper alloc] init];
    periodInfo.cellType = PeriodCell;
    NSString *numberOfdays = [NSString stringWithFormat:@"%ld",(long)phoneUsageModel.numberOfDays];
    NSString *latestTime = finalLastTimeString;//phoneUsageModel.latestTime;
    
    if(!numberOfdays || numberOfdays.length == 0)
        numberOfdays = @"-";
    
    if(!phoneUsageModel.latestTime || phoneUsageModel.latestTime.length == 0)
        latestTime = @"-";
    else
        latestTime = [latestTime lowercaseString];
    
    
    periodInfo.period =  [NSString stringWithFormat:@"Showing usage for %@ days since last billed date until %@ yesterday. Calls after this may not be included.",numberOfdays,latestTime];
    [dataArray addObject:periodInfo];
    
    //Phone usage usage break down with header
    
    
    if([phoneUsageModel.phoneLines count] >0)
    {
        
    int flag;
        
        for(flag = 0;flag <= [phoneUsageModel.phoneLines count]-1;flag++)
        {
            BTUsagePhoneLine *phoneUsage = [phoneUsageModel.phoneLines objectAtIndex:flag];
            BTPhoneUsageWrapper *usageBreakdown = [[BTPhoneUsageWrapper alloc] init];
            if(flag)
              usageBreakdown.cellType = UsageBreakDownCell;
            else
              usageBreakdown.cellType = UsageBreakDownWithHeaderCell;
            usageBreakdown.usageBreakdownTitle = phoneUsage.name;
            usageBreakdown.usageBreakdownCalls = [NSString stringWithFormat:@"%@",phoneUsage.calls];
            usageBreakdown.usageBreakdownCost = [NSString stringWithFormat:@"%@",phoneUsage.cost];
            [dataArray addObject:usageBreakdown];
        }
    }
    
    return dataArray;
}


- (NSArray *)getTakeActionSectionDataArray
{
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    
    //Phone usage call info
    BTPhoneUsageWrapper *viewLatestBill = [[BTPhoneUsageWrapper alloc] init];
    viewLatestBill.cellType = CallInfoCell;
    viewLatestBill.takeActionTitle = @"View latest bill";
    [dataArray addObject:viewLatestBill];
    
    BTPhoneUsageWrapper *viewAccountSummary = [[BTPhoneUsageWrapper alloc] init];
    viewAccountSummary.cellType = CallInfoCell;
    viewAccountSummary.takeActionTitle = @"View account summary";
    [dataArray addObject:viewAccountSummary];

    
    return dataArray;

}


#pragma mark - NLUsagePhoneLineWebServiceDelegate

- (void)succesfullyFetchedUsageOnPhoneLineUsageWebService:(NLUsagePhoneLineWebService *)webService withPhoneLineUsageList:(id)responseObject
{
    if(webService == _getUsagePhoneLineWebService)
    {
        NSArray *phoneLineUsageList;
        if ([[responseObject valueForKey:kNLResponseKeyForResult] isKindOfClass:[NSArray class]]) {
            NSLog(@"Phoneline response");
            
            NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
            
            NSMutableArray *resultPhoneLineUsages = [[NSMutableArray alloc] init];
            for (NSDictionary *usageDict in resultDic) {
                
                BTUsagePhoneProduct *phoneLineUsage = [[BTUsagePhoneProduct alloc] initWithResponseDictionaryFromPhoneLineUsageDetailsAPIResponse:usageDict];
                
                [resultPhoneLineUsages addObject:phoneLineUsage];
            }
            
            phoneLineUsageList = [NSArray arrayWithArray:resultPhoneLineUsages];
            
            BTUsagePhoneProduct *phoneProduct = nil;
            for (BTUsagePhoneProduct *phoneObject in phoneLineUsageList) {
                if ([phoneObject.serviceId isEqualToString:self.serviceNumber]) {
                    phoneProduct = phoneObject;
                    break;
                }
            }
            [self.phoneUsageSummaryScreenDelegate successfullyFetchedSummaryOnPhoneUsageSummaryScreen:self withPhoneLineProduct:phoneProduct];
            self.getUsagePhoneLineWebService.getUsagePhoneLineWebServiceDelegate = nil;
            self.getUsagePhoneLineWebService = nil;
        }
        else { // Cloud Voice Response
            
//            NSArray *unBilledCloudVoiceUsageArray;
//
//            NSArray *responseArray = [[responseObject valueForKey:kNLResponseKeyForResult]valueForKey:@"UnbilledUsageDetails"];
//            if(responseArray && responseArray.count > 0)
//            {
//                NSMutableArray *unbilledUsageObject = [NSMutableArray array];
//                for (NSDictionary *dict in responseArray) {
//                    BTUsagePhoneProduct *cloudVoiceUsage = [[BTUsagePhoneProduct alloc] initWithResponseDictionaryFromPhoneLineUsageDetailsAPIResponse:dict];
//                    [unbilledUsageObject addObject:cloudVoiceUsage];
//                }
//                unBilledCloudVoiceUsageArray = [NSArray arrayWithArray:unbilledUsageObject];
//            }
//
//            [self.phoneUsageSummaryScreenDelegate successfullyFetchedSummaryOnCloudVoiceUsageSummaryScreen:self withCloudVoiceDetails:unBilledCloudVoiceUsageArray];
//            self.getUsagePhoneLineWebService.getUsagePhoneLineWebServiceDelegate = nil;
//            self.getUsagePhoneLineWebService = nil;
        }
    } else if (webService == self.getUsageCVWebService) {
        NSArray *unBilledCloudVoiceUsageArray;
        
        NSArray *responseArray = [[responseObject valueForKey:kNLResponseKeyForResult]valueForKey:@"UnbilledUsageDetails"];
        if(responseArray && responseArray.count > 0)
        {
            NSMutableArray *unbilledUsageObject = [NSMutableArray array];
            for (NSDictionary *dict in responseArray) {
                BTUsagePhoneProduct *cloudVoiceUsage = [[BTUsagePhoneProduct alloc] initWithResponseDictionaryFromPhoneLineUsageDetailsAPIResponse:dict];
                [unbilledUsageObject addObject:cloudVoiceUsage];
            }
            unBilledCloudVoiceUsageArray = [NSArray arrayWithArray:unbilledUsageObject];
        }
        
        [self.phoneUsageSummaryScreenDelegate successfullyFetchedSummaryOnCloudVoiceUsageSummaryScreen:self withCloudVoiceDetails:unBilledCloudVoiceUsageArray];
        self.getUsageCVWebService.getUsagePhoneLineWebServiceDelegate = nil;
        self.getUsageCVWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLUsagePhoneLineWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLUsagePhoneLineWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}
//- (void)succesfullyFetchedUsageOnPhoneLineUsageWebService:(NLUsagePhoneLineWebService *)webService withPhoneLineUsageList:(NSArray *)phoneLineUsageList
//{
//    if(webService == _getUsagePhoneLineWebService)
//    {
//        BTUsagePhoneProduct *phoneProduct = nil;
//        for (BTUsagePhoneProduct *phoneObject in phoneLineUsageList) {
//            if ([phoneObject.serviceId isEqualToString:self.serviceNumber]) {
//                phoneProduct = phoneObject;
//                break;
//            }
//        }
//        [self.phoneUsageSummaryScreenDelegate successfullyFetchedSummaryOnPhoneUsageSummaryScreen:self withPhoneLineProduct:phoneProduct];
//        self.getUsagePhoneLineWebService.getUsagePhoneLineWebServiceDelegate = nil;
//        self.getUsagePhoneLineWebService = nil;
//    }
//    else
//    {
//        DDLogError(@"This delegate method gets called because of success of an object of NLUsagePhoneLineWebService but this object is not the one stored in member variable of this class %@.", [self class]);
//        NSAssert(NO, @"This delegate method gets called because of success of an object of NLUsagePhoneLineWebService but this object is not the one stored in member variable of this class %@.", [self class]);
//    }
//}
- (void)usagePhoneLineWebService:(NLUsagePhoneLineWebService *)webService failedToFetchPhoneLineUsageWithWebServiceError:(NLWebServiceError *)error
{
    if(webService == _getUsagePhoneLineWebService)
    {
        [self.phoneUsageSummaryScreenDelegate phoneUsageSummaryScreen:self
                     failedToFetchUsageDataWithWebServiceError:error];
        self.getUsagePhoneLineWebService.getUsagePhoneLineWebServiceDelegate = nil;
        self.getUsagePhoneLineWebService = nil;
    }
    else if (webService == _getUsageCVWebService)
    {
        [self.phoneUsageSummaryScreenDelegate phoneUsageSummaryScreen:self
                            failedToFetchUsageDataWithWebServiceError:error];
        self.getUsageCVWebService.getUsagePhoneLineWebServiceDelegate = nil;
        self.getUsageCVWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLUsagePhoneLineWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLUsagePhoneLineWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        
    }
}


#pragma mark - Private Helper methods
- (void)fetchOfflineData
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"PhoneLineUsage" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    
    NSError *error;
    
    NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if([responseObject isValidSMSessionAPIResponseObject])
    {
        BOOL isSuccess = [[responseObject valueForKey:kNLResponseKeyForIsSuccess] boolValue];
        
        if(isSuccess)
        {
            NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
            
            NSMutableArray *resultPhoneLineUsages = [[NSMutableArray alloc] init];
            for (NSDictionary *usageDict in resultDic) {
                
                BTUsagePhoneProduct *phoneLineUsage = [[BTUsagePhoneProduct alloc] initWithResponseDictionaryFromPhoneLineUsageDetailsAPIResponse:usageDict];
                
                [resultPhoneLineUsages addObject:phoneLineUsage];
            }
            
            BTUsagePhoneProduct *phoneProduct = nil;
            for (BTUsagePhoneProduct *phoneObject in resultPhoneLineUsages) {
                if ([phoneObject.serviceId isEqualToString:self.serviceNumber]) {
                    phoneProduct = phoneObject;
                    break;
                }
            }
            [self.phoneUsageSummaryScreenDelegate successfullyFetchedSummaryOnPhoneUsageSummaryScreen:self withPhoneLineProduct:phoneProduct];
            
        }
        
    }
    
}

@end


#pragma mark- BTPhoneUsageWrapper class

@implementation BTPhoneUsageWrapper


@end
