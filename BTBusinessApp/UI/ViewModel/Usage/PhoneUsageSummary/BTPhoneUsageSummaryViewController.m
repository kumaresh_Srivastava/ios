//
//  BTPhoneUsageSummaryViewController.m
//  BTBusinessApp
//
//  Created by vectoscalar on 22/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTPhoneUsageSummaryViewController.h"
#import "PhoneUsageSummaryTableViewHeader.h"
#import "BTTakeActionTableViewCell.h"
#import "BTTakeActionHeaderView.h"
#import "PhoneUsageBreakdownHeaderTableViewCell.h"
#import "PhoneUsageBreakdownTableViewCell.h"
#import "PhoneUsagePeriodCell.h"
#import "PhoneUsageCallInfoTableViewCell.h"
#import "DLMPhoneUsageSummaryScreeen.h"
#import "BTViewAssetBACOverviewViewController.h"
#import "BTBillDetailViewController.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "BTEmptyDashboardView.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "NLWebServiceError.h"
#import "BTCloudVoiceChooseNumberViewController.h"
#import "MoreAppsTableViewCell.h"
#import <StoreKit/SKStoreProductViewController.h>
#import "AppDelegate.h"

#define kTakeActionSectionNibName @"BTTakeActionHeaderView"
#define kTakeActionCell @"BTTakeActionTableViewCell"
#define kTakeActionCellID @"BTTakeActionTableViewCellID"


#define kPhoneUsageWithHeaderCell @"PhoneUsageBreakdownHeaderTableViewCell"
#define kPhoneUsageCell @"PhoneUsageBreakdownTableViewCell"
#define kPhoneUsagePeriodCell @"PhoneUsagePeriodCell"
#define kPhoneUsageCallInfoCell @"PhoneUsageCallInfoTableViewCell"
#define kMoreAppsCell @"MoreAppsTableViewCell"

#define kScreenSize [[UIScreen mainScreen] bounds].size
#define kNavSize 64
#define kSectionHeight 40
//#define kNavTitle @"Phone usage summary"
#define kHeaderTitle @"Business Phone line service"

#define kHeaderName @"PhoneUsageSummaryTableViewHeader"
#define kAccessoryImageNameForTakeAction @"arrow_right_purple"

#define kNumberOfSections 2

#define kTakeActionColor [BrandColours colourBtNeutral70]

@interface BTPhoneUsageSummaryViewController ()<UITableViewDelegate,UITableViewDataSource,SKStoreProductViewControllerDelegate,DLMPhoneUsageSummaryScreeenDelegate,BTRetryViewDelegate,BTCloudVoiceChooseNumberViewControllerDelegate>
{
    NSArray *_phoneUsageDataArray,*_takeActionDataArray;
    PhoneUsageSummaryTableViewHeader *_TableHeaderView;
    BTRetryView *_retryView;
    CustomSpinnerView *_loadingView;
    BTEmptyDashboardView *_emptyDashboardView;

    BOOL _isPhoneLineApiFailed;
    BOOL _isPhoneLineApiInProgress;
    
    BOOL _isCloudVoiceDetailsApiFailed;
    BOOL _isCloudVoiceDetailsApiInProgress;
    BOOL _isFromChooseNumberCV;

}
@property (strong,nonatomic) UITableView *tableView;
@property (strong,nonatomic) DLMPhoneUsageSummaryScreeen *phoneUsageSummaryScreenModel;

@end

@implementation BTPhoneUsageSummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = _navigationTitle;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
//    self.tableView.delegate = self;
//    self.tableView.dataSource = self;
//
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    self.tableView.backgroundColor = [UIColor colorForHexString:@"eeeeee"];
//
//    //Make TableView compatible for autolayout
//    self.tableView.rowHeight = UITableViewAutomaticDimension;
//    self.tableView.estimatedRowHeight = 110;
    
//    [self registerForCells];
    
//    [self setTableViewHeader];

    _isFromChooseNumberCV = NO;
    self.phoneUsageSummaryScreenModel = [[DLMPhoneUsageSummaryScreeen alloc] init];
    self.phoneUsageSummaryScreenModel.phoneUsageSummaryScreenDelegate = self;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    if(_isCloudVoiceExpress){
        if (self.phoneUsageModel) {
            [self fetchUsageCloudVoiceAPI];
        } else {
            [self fetchUsagePhoneLineAPI];
        }
    }
    else {
        if (self.phoneUsageModel) {
            [self createData];
        } else {
            _isPhoneLineApiInProgress = YES;
            _isPhoneLineApiFailed = NO;
            [self fetchUsagePhoneLineAPI];
        }
    }
    
//    if (!self.phoneUsageModel) {
//        _isPhoneLineApiInProgress = YES;
//        _isPhoneLineApiFailed = NO;
//        [self fetchUsagePhoneLineAPI];
//    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChangeUsagecreen:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSString *pageName = OMNIPAGE_USAGE_PHONE;

    if(_isCloudVoiceExpress && !_isFromChooseNumberCV){
        pageName = OMNIPAGE_USAGE_CLOUD_VOICE_EXPRESS_OVERVIEW;
    }
    else if(_isCloudVoiceExpress && _isFromChooseNumberCV){
        pageName = OMNIPAGE_USAGE_CLOUD_VOICE_EXPRESS_USAGE;
    }
    else {
        pageName = OMNIPAGE_USAGE_PHONE;
    }
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    [OmnitureManager trackPage:pageName withContextInfo:data];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self.phoneUsageSummaryScreenModel cancelPhoneLineUsageApi];
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if( appDelegate.isSingleBacCug == YES){
        if (self.isMovingFromParentViewController || self.isBeingDismissed) { // BACK Button is PRESSED
            appDelegate.isSingleBacCugBackNavButtonPressed = YES;
        }
        else{
            appDelegate.isSingleBacCugBackNavButtonPressed = NO;
        }
    }
    
}

#pragma mark - Private Helper Methods

- (void)statusBarOrientationChangeUsagecreen:(NSNotification *)notification {
    // handle the interface orientation as needed
    //[self performSelector:@selector(createLoadingView) withObject:nil afterDelay:0.1];
    if (self.phoneUsageModel) {
        self.tableView.tableHeaderView = nil;
        [self performSelector:@selector(handleHeaderView) withObject:nil afterDelay:0.1];
    }
}

- (void)handleHeaderView{
    [self setTableViewHeader];
    //Update Table header view
    if(self.phoneUsageModel.serviceId.length >0)
        [_TableHeaderView updateHeaderWithTitle:@"Number" andDescription:[NSString stringWithFormat:@"%@",self.phoneUsageModel.serviceId]];
    else
        [_TableHeaderView updateHeaderWithTitle:@"Number" andDescription:@"-"];
    
}

- (void)updateUI {
    
    if(_isPhoneLineApiInProgress)
        return;
    
    [self hideLoadingItems:YES];
    [self hideRetryItems:YES];
    [self hideEmmptyDashBoardItems:YES];

    if(_isPhoneLineApiFailed)
    {
        [self hideRetryItems:NO];
        [self showRetryViewWithInternetStrip:NO];
    }
    else if(!self.phoneUsageModel){
        [self showEmptyView];
    }
    else{
        [self createData];
    }
}

- (void)updateCloudVoiceUI {
    
    if(_isCloudVoiceDetailsApiInProgress)
        return;
    
    [self hideLoadingItems:YES];
    [self hideRetryItems:YES];
    [self hideEmmptyDashBoardItems:YES];
    
    if(_isCloudVoiceDetailsApiFailed)
    {
        [self hideRetryItems:NO];
        [self showRetryViewWithInternetStrip:NO];
    }
    else{
        [self createData];
    }
    
}

- (void)createData
{
     [self createAndConfigureTableView];
    _phoneUsageDataArray  = [NSArray arrayWithArray:[self.phoneUsageSummaryScreenModel getPhoneUsageWrapperObjectsWithModel:self.phoneUsageModel]];
    _takeActionDataArray = [NSArray arrayWithArray:[self.phoneUsageSummaryScreenModel getTakeActionSectionDataArray]];
    [self.tableView reloadData];
    
    //To do - Create a bool to check whether current selection is Phone line Or Cloud Voice

    //Update Table header view
    if (self.cloudVoiceUsageModelArray.count == 1) {
        self.phoneUsageModel = [self.cloudVoiceUsageModelArray objectAtIndex:0];
        [_TableHeaderView updateHeaderWithTitle:@"Cloud Voice Epress number" andDescription:[NSString stringWithFormat:@"%@",self.phoneUsageModel.serviceLineItem]];
    }
    else if(self.phoneUsageModel.serviceId.length >0)
        [_TableHeaderView updateHeaderWithTitle:@"Service ID" andDescription:[NSString stringWithFormat:@"%@",self.phoneUsageModel.serviceId]];
    else
        [_TableHeaderView updateHeaderWithTitle:@"Service ID" andDescription:@"-"];
    
    
    if (self.cloudVoiceUsageModelArray.count > 1) {
        _TableHeaderView.chooseButton.hidden = NO;
        [_TableHeaderView.chooseButton setTitle:[NSString stringWithFormat:@"Choose number"] forState:UIControlStateNormal];
        [_TableHeaderView.chooseButton addTarget:self action:@selector(chooseButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    else {
        _TableHeaderView.chooseButton.hidden = YES;
    }
    
    [self.tableView reloadData];
    
    [self.view layoutIfNeeded];
}

- (void)createCloudVoiceDetailsData:(BTUsagePhoneProduct *)cloudVoiceModel
{
    [self createAndConfigureTableView];
    _phoneUsageDataArray  = [NSArray arrayWithArray:[self.phoneUsageSummaryScreenModel getPhoneUsageWrapperObjectsWithModel:cloudVoiceModel]];
    _takeActionDataArray = [NSArray arrayWithArray:[self.phoneUsageSummaryScreenModel getTakeActionSectionDataArray]];
    //[self.mTableView reloadData];
    [self.tableView reloadData];
    
    //To do - Create a bool to check whether current selection is Phone line Or Cloud Voice
    
    //Update Table header view
    if(self.phoneUsageModel.serviceId.length >0)
        [_TableHeaderView updateHeaderWithTitle:@"Cloud Voice Epress number" andDescription:[NSString stringWithFormat:@"%@",cloudVoiceModel.serviceLineItem]];
    else
        [_TableHeaderView updateHeaderWithTitle:@"Service ID" andDescription:@"-"];
    
}

- (void)showEmptyView {
    
    _emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTEmptyDashboardView" owner:nil options:nil] objectAtIndex:0];
    _emptyDashboardView.translatesAutoresizingMaskIntoConstraints = NO;
    [_emptyDashboardView updateEmptyDashboardViewWithImageName:@"package" title:@"No data to display" detailText:nil andButtonTitle:nil];
    _emptyDashboardView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_emptyDashboardView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
}


- (void)hideEmmptyDashBoardItems:(BOOL)isHide {
    [_emptyDashboardView setHidden:isHide];
    [_emptyDashboardView removeFromSuperview];
    _emptyDashboardView = nil;
}

- (void)createAndConfigureTableView
{
    if(!self.tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
    }
    
    [self registerForCells];

    self.tableView.delegate = self;
    self.tableView.dataSource = self;

    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor colorForHexString:@"eeeeee"];

    //Make TableView compatible for autolayout
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 80;

    [self.tableView setTranslatesAutoresizingMaskIntoConstraints:NO];

//    if (self.tabBarController) {
//        self.mTableView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, CGRectGetHeight(self.tabBarController.tabBar.frame), 0.0f);
//    }

    [self.view addSubview:self.tableView];

    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]
                                ]];

//    if (@available(iOS 11.0, *)) {
//        [self.mTableView.bottomAnchor constraintEqualToAnchor:self.view.safeAreaLayoutGuide.bottomAnchor].active = YES;
//    } else {
//        // Fallback on earlier versions
//        [self.mTableView.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.bottomAnchor].active = YES;
//    }
    
    [self setTableViewHeader];

}


- (void)setTableViewHeader
{
    _TableHeaderView = [[[NSBundle mainBundle] loadNibNamed:kHeaderName owner:nil options:nil] firstObject];
    _TableHeaderView.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableView.tableHeaderView = _TableHeaderView;
    
    [self.view addConstraints:@[
                                      [NSLayoutConstraint constraintWithItem:_TableHeaderView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                      [NSLayoutConstraint constraintWithItem:_TableHeaderView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]
                                      ]];
}


- (void)registerForCells
{
    UINib *nib = [UINib nibWithNibName:kTakeActionCell bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kTakeActionCellID];

    //Phone usage breakdown cell with header
    nib = [UINib nibWithNibName:kPhoneUsageWithHeaderCell bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kPhoneUsageWithHeaderCell];

    //Phone usage breakdown cell
    nib = [UINib nibWithNibName:kPhoneUsageCell bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kPhoneUsageCell];

    //Phone usage period Cell
    nib = [UINib nibWithNibName:kPhoneUsagePeriodCell bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kPhoneUsagePeriodCell];

    //Phone usage call info Cell
    nib = [UINib nibWithNibName:kPhoneUsageCallInfoCell bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kPhoneUsageCallInfoCell];
    
    // More apps cell
    nib = [UINib nibWithNibName:kMoreAppsCell bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kMoreAppsCell];

}



- (UITableViewCell *)getTakeActionCellForIndexPath:(NSIndexPath *)indexPath forTableView:(UITableView*)tableView
{
    BTPhoneUsageWrapper *phoneUsageData = [_takeActionDataArray objectAtIndex:indexPath.row];
    BTTakeActionTableViewCell *actionCell = [tableView dequeueReusableCellWithIdentifier:kTakeActionCellID forIndexPath:indexPath];
    [actionCell updateWithTitle:phoneUsageData.takeActionTitle];
    return actionCell;

}


- (void)redirectToBillScreen
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTBillDetailViewController *billScreen = (BTBillDetailViewController*)[storyboard instantiateViewControllerWithIdentifier:@"BTBillDetailViewController"];
    billScreen.btAsset = self.selectedAsset;
    [self.navigationController pushViewController:billScreen animated:YES];
}


- (void)redirectToAssetScreen
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTViewAssetBACOverviewViewController *assetVC = (BTViewAssetBACOverviewViewController*)[storyboard instantiateViewControllerWithIdentifier:@"BTViewAssetBACOverviewViewController"];
    assetVC.userSelectedBTAsset  = self.selectedAsset;
    [self.navigationController pushViewController:assetVC animated:YES];
}


- (UITableViewCell *)getPhoneUsageSummaryCellForIndexPath:(NSIndexPath *)indexPath forTableView:(UITableView*)tableView
{
    BTPhoneUsageWrapper *phoneUsageData = [_phoneUsageDataArray objectAtIndex:indexPath.row];

    if(phoneUsageData.cellType == CallInfoCell)
    {
        PhoneUsageCallInfoTableViewCell *callInfoCell = [tableView dequeueReusableCellWithIdentifier:kPhoneUsageCallInfoCell forIndexPath:indexPath];
        [callInfoCell updateDataWithWrapper:phoneUsageData];
        return callInfoCell;

    }
    else if(phoneUsageData.cellType == PeriodCell)
    {
        PhoneUsagePeriodCell *phoneUsagePeriodCell = [tableView dequeueReusableCellWithIdentifier:kPhoneUsagePeriodCell forIndexPath:indexPath];
        [phoneUsagePeriodCell updateCellWithPeriodString:phoneUsageData.period];
        return phoneUsagePeriodCell;

    }
    else if(phoneUsageData.cellType == UsageBreakDownWithHeaderCell)
    {
        PhoneUsageBreakdownHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kPhoneUsageWithHeaderCell forIndexPath:indexPath];
        [cell updateDataWithWrapper:phoneUsageData];
        return cell;
    }
    else
    {
        PhoneUsageBreakdownTableViewCell *phoneUsageCell = [tableView dequeueReusableCellWithIdentifier:kPhoneUsageCell forIndexPath:indexPath];
        [phoneUsageCell updateDataWithWrapper:phoneUsageData];

        return phoneUsageCell;
    }

}

//- (CGFloat)getPhoneUsageSummaryCellHeightForIndexPath:(NSIndexPath *)indexPath forTableView:(UITableView*)tableView
//{
//    BTPhoneUsageWrapper *phoneUsageData = [_phoneUsageDataArray objectAtIndex:indexPath.row];
//
//    if(phoneUsageData.cellType == CallInfoCell)
//    {
//        return 87;
//    }
//    else if(phoneUsageData.cellType == PeriodCell)
//    {
//        return 143;
//    }
//    else if(phoneUsageData.cellType == UsageBreakDownWithHeaderCell)
//    {
//        return 130;
//    }
//    else
//    {
//        return 91;
//    }
//}

- (UITableViewCell*)getMoreAppsForIndexPath:(NSIndexPath*)indexPath forTableView:(UITableView*)tableView {
    //MoreAppsTableViewCell *moreAppCell = [[[NSBundle mainBundle] loadNibNamed:@"MoreAppsTableViewCell" owner:nil options:nil] objectAtIndex:0];
    MoreAppsTableViewCell *moreAppCell = [tableView dequeueReusableCellWithIdentifier:kMoreAppsCell forIndexPath:indexPath];
    moreAppCell.selectionStyle = UITableViewCellSelectionStyleNone;
    //moreAppCell.masterView.backgroundColor = [UIColor whiteColor];
    //moreAppCell.backgroundColor = [UIColor colorWithRed:0.933 green:0.933 blue:0.933 alpha:1];
    moreAppCell.contentView.backgroundColor = [UIColor colorForHexString:@"eeeeee"];
    moreAppCell.titleLabel.text = @"BT Cloud Voice Express";
    moreAppCell.descriptionLabel.text = @"The easy way to use your BT Cloud Voice Express service on the go.";
    [moreAppCell.btnMoreAppLogo setImage:[UIImage imageNamed:@"BTCloudVoiceAppLogo"] forState:UIControlStateNormal];
    [moreAppCell.actionButton setTitle:[NSString stringWithFormat:@"Download"] forState:UIControlStateNormal];
    [moreAppCell.actionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [moreAppCell.actionButton setTitleColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1] forState:UIControlStateNormal];
//    moreAppCell.containerButtonView.backgroundColor = [UIColor clearColor];
//    moreAppCell.innerButtonView.backgroundColor = [UIColor blueColor];
//    moreAppCell.innerButtonView.backgroundColor = [UIColor colorWithRed:0.392 green:0.0 blue:0.67 alpha:1];
//    moreAppCell.masterView.layer.cornerRadius = 7.0f;
    [moreAppCell.actionButton addTarget:self action:@selector(moreAppButtonDownloadAction:) forControlEvents:UIControlEventTouchUpInside];
//    moreAppCell.layer.cornerRadius = 7.0f;
    return moreAppCell;
}

- (void)fetchUsagePhoneLineAPI {
    
    if([AppManager isInternetConnectionAvailable])
    {
        [self hideRetryItems:YES];
        [self createLoadingView];
        [self.phoneUsageSummaryScreenModel fetchPhoneLineUsageWithAccountNumber:self.selectedAsset.billingAccountNumber andServiceNumber:self.serviceNumber];
    }
    else
    {
        [self showRetryViewWithInternetStrip:YES];
        
        if (_isCloudVoiceExpress) {
            [AppManager trackNoInternetErrorOnPage:OMNIPAGE_USAGE_CLOUD_VOICE_EXPRESS_OVERVIEW];
        }
        else {
            [AppManager trackNoInternetErrorOnPage:OMNIPAGE_USAGE_PHONE];
        }
    }
}

- (void)fetchUsageCloudVoiceAPI {
    
    if([AppManager isInternetConnectionAvailable])
    {
        [self hideRetryItems:YES];
        [self createLoadingView];
        [self.phoneUsageSummaryScreenModel fetchCloudVoiceUsageWithAccountNumber:self.selectedAsset.billingAccountNumber andServiceNumber:self.serviceNumber];
    }
    else
    {
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_USAGE_CLOUD_VOICE_EXPRESS_SELECT_SERVICE];
    }
}

- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow {
    
    if(_retryView)
    {
        _retryView.hidden = NO;
        _retryView.retryViewDelegate = self;
        [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
        return;
    }
    
    
    _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
    _retryView.translatesAutoresizingMaskIntoConstraints = NO;
    _retryView.retryViewDelegate = self;
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
    

    [self.view addSubview:_retryView];
    
    //[self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:64.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];

    
}

- (void)hideRetryItems:(BOOL)isHide {
    [_retryView setHidden:isHide];
}

- (void)hideLoadingItems:(BOOL)isHide {
    [_loadingView stopAnimatingLoadingIndicatorView];
    [_loadingView setHidden:isHide];
    [_loadingView removeFromSuperview];
    _loadingView = nil;
}

- (void)createLoadingView {
    
    if(!_loadingView)
    {
        _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
        [_loadingView startAnimatingLoadingIndicatorView];
    }
        [self.view addSubview:_loadingView];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
}

#pragma mark - Userdefined Methods

- (void)chooseButtonAction:(UIButton *)button {
    
    if ([button.titleLabel.text isEqualToString:@"Choose number"]) {
        [self trackOmnitureEvent:@"Choose number" forPage:OMNIPAGE_USAGE_CLOUD_VOICE_EXPRESS_OVERVIEW];
    }
    else {
        [self trackOmnitureEvent:@"Change number" forPage:OMNIPAGE_USAGE_CLOUD_VOICE_EXPRESS_USAGE];
    }
    if (self.cloudVoiceUsageModelArray.count > 1) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CloudVoice" bundle:nil];
        BTCloudVoiceChooseNumberViewController *chooseVC = [storyboard instantiateViewControllerWithIdentifier:@"chooseNumberScene"];
        chooseVC.cloudVoiceDataModelArray = self.cloudVoiceUsageModelArray;
        chooseVC.phoneUsageModel = self.phoneUsageModel;
        chooseVC.cloudVoiceChooseNumberScreenDelegate = self;
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:chooseVC];
        [self presentViewController:navController animated:YES completion:nil];
    }
}

-(void)moreAppButtonDownloadAction:(id)sender { // Button Cloud Voice App
    
    if (_isFromChooseNumberCV) {
        [self trackOmnitureEvent:@"Download Cloud Voice app" forPage:OMNIPAGE_USAGE_CLOUD_VOICE_EXPRESS_USAGE];
    }
    else {
        [self trackOmnitureEvent:@"Download Cloud Voice app" forPage:OMNIPAGE_USAGE_CLOUD_VOICE_EXPRESS_OVERVIEW];
    }
//    [AppManager openURL:@"https://itunes.apple.com/us/app/bt-cloud-voice-express/id1457358475?mt=8"];

    NSInteger iTuneID = 1457358475;//[[_urlPathArray objectAtIndex:[moreAppButton tag]] integerValue];
    [self openStoreProductViewControllerWithITunesItemIdentifier:iTuneID];
}

- (void)openStoreProductViewControllerWithITunesItemIdentifier:(NSInteger)iTunesItemIdentifier {
    
    SKStoreProductViewController *storeViewController = [[SKStoreProductViewController alloc] init];
    storeViewController.delegate = self;
    
    NSNumber *identifier = [NSNumber numberWithInteger:iTunesItemIdentifier];
    
    NSDictionary *parameters = @{ SKStoreProductParameterITunesItemIdentifier:identifier };
    [storeViewController loadProductWithParameters:parameters completionBlock:nil];
    
    [self presentViewController:storeViewController animated:YES completion:nil];
}

- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - DLMPhoneUsageSummaryScreenDelegate


- (void)successfullyFetchedSummaryOnPhoneUsageSummaryScreen:(DLMPhoneUsageSummaryScreeen *)phoneUsageSummaryScreen withPhoneLineProduct:(BTUsagePhoneProduct *)phoneUsageModel
{
    _isPhoneLineApiInProgress = NO;
    _isPhoneLineApiFailed = NO;
    if (phoneUsageModel) {
        self.phoneUsageModel = phoneUsageModel;
    } else {
        DDLogError(@"Phone usage summary: Data not found");
    }
    if (phoneUsageModel && _isCloudVoiceExpress) {
        [self fetchUsageCloudVoiceAPI];
    } else {
        [self updateUI];
    }
}

- (void)successfullyFetchedSummaryOnCloudVoiceUsageSummaryScreen:(DLMPhoneUsageSummaryScreeen *)phoneUsageSummaryScreen withCloudVoiceDetails:(NSArray *)cloudVoiceUsageModel
{
    _isCloudVoiceDetailsApiInProgress = NO;
    _isCloudVoiceDetailsApiFailed = NO;
    
    if (cloudVoiceUsageModel) {
        self.cloudVoiceUsageModelArray = cloudVoiceUsageModel; //[NSArray arrayWithObject:[cloudVoiceUsageModel firstObject]];
    }
    else {
        DDLogError(@"Cloud Voice usage details: Data not found");
    }
    
    [self updateCloudVoiceUI];
}

- (void)phoneUsageSummaryScreen:(DLMPhoneUsageSummaryScreeen *)phoneUsageSummaryScreen failedToFetchUsageDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
    if ([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO) {
        switch (webServiceError.error.code) {
                
            case BTNetworkErrorCodeAPINoDataFound: {
                
                if (_isCloudVoiceExpress) {
                    [AppManager trackNoDataFoundErrorOnPage:OMNIPAGE_USAGE_CLOUD_VOICE_EXPRESS_NO_DATA_FOUND];
                    _TableHeaderView.chooseButton.hidden = YES;
                    [self createData];
                }
                else {
                    [AppManager trackNoDataFoundErrorOnPage:OMNIPAGE_USAGE_PHONE];
                    [self showEmptyView];
                }
                errorHandled = YES;
                break;
            }
            default: {
                errorHandled = NO;
                break;
            }
        }
    }
    if (errorHandled == NO) {
        [_loadingView stopAnimatingLoadingIndicatorView];
        [self showRetryViewWithInternetStrip:NO];
        if (_isCloudVoiceExpress) {
            [AppManager trackWebServiceError:webServiceError FromAPI:@"GetUnbilledUsage" OnPage:OMNIPAGE_USAGE_CLOUD_VOICE_EXPRESS_SOMETHINGWENTWRONG];
            //[AppManager trackGenericAPIErrorOnPage:OMNIPAGE_USAGE_CLOUD_VOICE_EXPRESS_SOMETHINGWENTWRONG];
        }
        else {
            [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_USAGE_PHONE];
        }
    }
    
}

#pragma mark - BTCloudVoiceChooseNumberViewController Delegate

- (void)chosenIndexOnCloudVoiceChooseNumberViewController:(BTCloudVoiceChooseNumberViewController *)chooseNumberScreen withPhoneLineProduct:(BTUsagePhoneProduct *)phoneUsageModel andServiceId:(BOOL)isServiceId
{
    _isFromChooseNumberCV = NO;
    if (phoneUsageModel) {
        
        if (isServiceId) { // Called when service Id is selected instead of service line numbers
            [self createData];
            [_TableHeaderView.chooseButton setTitle:[NSString stringWithFormat:@"Choose number"] forState:UIControlStateNormal];
        }
        else {
            _isFromChooseNumberCV = YES;
            [self createCloudVoiceDetailsData:phoneUsageModel];
            [_TableHeaderView.chooseButton setTitle:[NSString stringWithFormat:@"Change number"] forState:UIControlStateNormal];
        }
        _TableHeaderView.chooseButton.hidden = NO;
        [_TableHeaderView.chooseButton addTarget:self action:@selector(chooseButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
}


#pragma mark - BTRetryView Delegate

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    if (_isCloudVoiceExpress) {
        [self fetchUsageCloudVoiceAPI];
    }
    else {
        [self fetchUsagePhoneLineAPI];
    }
}

#pragma mark - UITableView Data Source/Delegate

//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    if(!section)//For first Section
//        return  [_phoneUsageDataArray count];
//    else
//        return [_takeActionDataArray count];//Take action fields count
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 0;
    if (_phoneUsageDataArray.count > 0) {
        if(section == 0) { //For first Section
            rows = [_phoneUsageDataArray count];
        } else if (section == 1) { //Take action fields count
            rows =  [_takeActionDataArray count];
        } else {
            rows = 1;
        }
    }
    return rows;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    BTTakeActionHeaderView *sectionHeaderView = nil;
    if(section > 0) {
        sectionHeaderView = [[[NSBundle mainBundle] loadNibNamed:kTakeActionSectionNibName owner:self options:nil] firstObject];
    }
    return sectionHeaderView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat height = 0.0;
    if(section == 1) {
        height = kSectionHeight;
    }
    return height;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//     return 100.0;
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger sections = 0;
    if (_isCloudVoiceExpress && !_isFromChooseNumberCV){
        sections = 3;
    }
    else {
        sections = kNumberOfSections;
    }
    return sections;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if(indexPath.section == 0) {
        cell = [self getPhoneUsageSummaryCellForIndexPath:indexPath forTableView:tableView];
    } else if (indexPath.section == 1) {
        cell = [self getTakeActionCellForIndexPath:indexPath forTableView:tableView];
    } else { // Cloud voice section
        cell = [self getMoreAppsForIndexPath:indexPath forTableView:tableView];
    }
    return cell;
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if(indexPath.section == 1)
    {
        NSString *pageName = OMNIPAGE_USAGE_PHONE;
        if(_isCloudVoiceExpress && !_isFromChooseNumberCV){
            pageName = OMNIPAGE_USAGE_CLOUD_VOICE_EXPRESS_OVERVIEW;
        }
        else if(_isCloudVoiceExpress && _isFromChooseNumberCV){
            pageName = OMNIPAGE_USAGE_CLOUD_VOICE_EXPRESS_USAGE;
        }
        else {
            pageName = OMNIPAGE_USAGE_PHONE;
        }
        
        if(indexPath.row == 0)
        {
            //Push Bill Screen
            [self trackOmnitureEvent:@"View latest bill" forPage:pageName];
            [self redirectToBillScreen];
        }
        else
        {
            //Push Asset Screen
            [self trackOmnitureEvent:@"View account summary" forPage:pageName];
            [self redirectToAssetScreen];
        }

    }

}



#pragma mark - Omniture Tracking


- (void)trackOmnitureEvent:(NSString *)event forPage:(NSString *)page
{
    NSString *pageName = page;
    NSString *linkTitle = event;
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];

    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:data];
}



@end
