//
//  BTCloudVoiceChooseNumberViewController.h
//  BTBusinessApp
//
//  Created by Manik on 14/03/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseViewController.h"
#import "BTUsagePhoneProduct.h"


@class BTCloudVoiceChooseNumberViewController;

@protocol BTCloudVoiceChooseNumberViewControllerDelegate
- (void)chosenIndexOnCloudVoiceChooseNumberViewController:(BTCloudVoiceChooseNumberViewController *)chooseNumberScreen withPhoneLineProduct:(BTUsagePhoneProduct *)phoneUsageModel andServiceId:(BOOL)isServiceId;
@end

@interface BTCloudVoiceChooseNumberViewController : BTBaseViewController
@property (nonatomic, weak) id<BTCloudVoiceChooseNumberViewControllerDelegate> cloudVoiceChooseNumberScreenDelegate;
@property (strong,nonatomic) BTUsagePhoneProduct *phoneUsageModel;
@property (strong,nonatomic) NSArray *cloudVoiceDataModelArray;

@end

