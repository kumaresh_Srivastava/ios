//
//  BTCloudVoiceChooseNumberViewController.m
//  BTBusinessApp
//
//  Created by Manik on 14/03/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import "BTCloudVoiceChooseNumberViewController.h"
#import "BTOCSSimpleDisclosureTableViewCell.h"
#import "ChooseNumberTitleTableViewCell.h"
#import "OmnitureManager.h"
#import "AppManager.h"

#define kBTChooseNumberCell @"BTOCSSimpleDisclosureTableViewCell"
#define kBTChooseNumberTitleCell @"ChooseNumberTitleTableViewCell"

@interface BTCloudVoiceChooseNumberViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *chooseNumberTableView;

@end

@implementation BTCloudVoiceChooseNumberViewController

#pragma mark - View methods
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonAction)];
    
    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:@{ NSFontAttributeName: [UIFont fontWithName:kBtFontRegular size:20.0]} forState: UIControlStateNormal];

    self.title = @"Choose number";
    
    UINib *nib = [UINib nibWithNibName:kBTChooseNumberCell bundle:nil];
    [self.chooseNumberTableView registerNib:nib forCellReuseIdentifier:kBTChooseNumberCell];
    self.chooseNumberTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UINib *chooseNumberTitleNib = [UINib nibWithNibName:kBTChooseNumberCell bundle:nil];
    [self.chooseNumberTableView registerNib:chooseNumberTitleNib forCellReuseIdentifier:kBTChooseNumberCell];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSString *pageName = OMNIPAGE_USAGE_CLOUD_VOICE_EXPRESS_SELECT_SERVICE;
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
   
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

#pragma mark - table view datasource methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}


- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 39)];
    [headerView setBackgroundColor:[BrandColours colourBtLightGray]];
    
    NSString *sectionTitle = nil;
    
    if(section == 1)
    {
        sectionTitle = @"Numbers with usage after last bill date";
    }
   
    UILabel *headerTitleLable = [[UILabel alloc] initWithFrame:CGRectMake(15, 10,tableView.bounds.size.width - 15, 19)];
    headerTitleLable.text = sectionTitle;
    headerTitleLable.textColor = [BrandColours colourBtLightBlack];
    headerTitleLable.font = [UIFont fontWithName:@"BTFont-Bold" size:14];
    headerTitleLable.backgroundColor = [UIColor clearColor];
    [headerView addSubview:headerTitleLable];

    return headerView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return 0;
    else
        return 39;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 53;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    else {
        return [_cloudVoiceDataModelArray count];
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        BTOCSSimpleDisclosureTableViewCell *chooseNumberTitleCell = [tableView dequeueReusableCellWithIdentifier:kBTChooseNumberCell];
        chooseNumberTitleCell.selectionStyle = UITableViewCellSelectionStyleNone;
        chooseNumberTitleCell.label.text = _phoneUsageModel.serviceId;
//        [chooseNumberTitleCell.disclosureImgView setImage:[UIImage imageNamed:@"OCSRightArrowPurple"]];
        return chooseNumberTitleCell;
    }
    else{
        BTOCSSimpleDisclosureTableViewCell *chooseNumberCell = [tableView dequeueReusableCellWithIdentifier:kBTChooseNumberCell];
        chooseNumberCell.selectionStyle = UITableViewCellSelectionStyleNone;
//        [chooseNumberCell.disclosureIcon setImage:[UIImage imageNamed:@"OCSRightArrowPurple"]];
        chooseNumberCell.separatorLineView.hidden = NO;
        BTUsagePhoneProduct *objBTUsagePhoneProduct = [_cloudVoiceDataModelArray objectAtIndex:indexPath.row];
        chooseNumberCell.label.text = objBTUsagePhoneProduct.serviceLineItem;
        
        return chooseNumberCell;
    }
}

#pragma mark - table view delegate methods

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self trackOmnitureEvent:@"Details" forPage:OMNIPAGE_USAGE_CLOUD_VOICE_EXPRESS_SELECT_SERVICE];
    if (indexPath.section == 0) {
        [self.cloudVoiceChooseNumberScreenDelegate chosenIndexOnCloudVoiceChooseNumberViewController:self withPhoneLineProduct:_phoneUsageModel andServiceId:YES];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self.cloudVoiceChooseNumberScreenDelegate chosenIndexOnCloudVoiceChooseNumberViewController:self withPhoneLineProduct:[_cloudVoiceDataModelArray objectAtIndex:indexPath.row]andServiceId:NO];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    
}


#pragma mark - Action methods

- (void)closeButtonAction
{
//    [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Omniture Tracking

- (void)trackOmnitureEvent:(NSString *)event forPage:(NSString *)page
{
    NSString *pageName = page;
    NSString *linkTitle = event;
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:data];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
