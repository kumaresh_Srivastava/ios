//
//  DLMUsageDashboardScreen.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 23/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
#import "BTCug.h"

@class DLMUsageDashboardScreen;
@class NLWebServiceError;

@protocol DLMUsageDashboardScreenDelegate <NSObject>

- (void)successfullyFetchedUsageOnUsageDashboardScreen:(DLMUsageDashboardScreen *)usageDashboardScreen WithBACArray:(NSArray *)bacList;

- (void)usageDashboardScreen:(DLMUsageDashboardScreen *)usageDashboardScreen failedToFetchUsageDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end


@interface DLMUsageDashboardScreen : DLMObject

@property (nonatomic, assign) id<DLMUsageDashboardScreenDelegate> usageDashboardScreenDelegate;

- (void)fetchUsageDashboardDetails;
- (void)cancelUsageDashboardAPIRequest;
- (void)changeSelectedCUGTo:(BTCug *)selectedCug;

@end
