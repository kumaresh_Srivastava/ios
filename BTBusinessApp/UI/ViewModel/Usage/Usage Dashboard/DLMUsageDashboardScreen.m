//
//  DLMUsageDashboardScreen.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 23/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMUsageDashboardScreen.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "NLUsageDashboardWebService.h"
#import "CDAuthenticationToken+CoreDataProperties.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"

@interface DLMUsageDashboardScreen()<NLUsageDashboardWebServiceDelegate>

@property (nonatomic, strong)NLUsageDashboardWebService *usageDashboardWebService;

@end


@implementation DLMUsageDashboardScreen

- (void)fetchUsageDashboardDetails
{
    self.usageDashboardWebService = [[NLUsageDashboardWebService alloc] init];
    self.usageDashboardWebService.usageDashboardWebServiceDelegate = self;
    [self.usageDashboardWebService resume];

}

- (void)cancelUsageDashboardAPIRequest
{
    self.usageDashboardWebService.usageDashboardWebServiceDelegate = nil;
    [self.usageDashboardWebService cancel];
    self.usageDashboardWebService = nil;
}

#pragma mark PersistanceMethods

- (void)changeSelectedCUGTo:(BTCug *)selectedCug
{
    DDLogInfo(@"Saving %@ as selected cug into persistence.", selectedCug);

    NSManagedObjectContext *context = [AppDelegate sharedInstance].managedObjectContext;
    CDCug *newPersistenceCug = [CDCug newCugInManagedObjectContext:context];
    newPersistenceCug.groupKey = selectedCug.groupKey;
    newPersistenceCug.refKey = selectedCug.refKey;
    newPersistenceCug.cugName = selectedCug.cugName;
    newPersistenceCug.contactId = selectedCug.contactId;
    newPersistenceCug.cugId = selectedCug.cugID;
    newPersistenceCug.cugRole = @(selectedCug.cugRole);
    newPersistenceCug.indexInAPIResponse = @(selectedCug.indexInAPIResponse);

    // Get the currently logged in User
    CDUser *loggedInUser = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    CDCug *previouslySelectedCug = loggedInUser.currentSelectedCug;
    loggedInUser.currentSelectedCug = newPersistenceCug;

    if(previouslySelectedCug)
    {
        [context deleteObject:previouslySelectedCug];
    }

    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
}

#pragma mark - NLUsageDashBoardWebServiceDelegate Methods

- (void)succesfullyFetchedUsageOnUsageDashboardScreenWebService:(NLUsageDashboardWebService *)webService withUsageArray:(NSArray *)bacList
{
    if(webService == _usageDashboardWebService)
    {
        [self.usageDashboardScreenDelegate successfullyFetchedUsageOnUsageDashboardScreen:self WithBACArray:bacList];

        _usageDashboardWebService.usageDashboardWebServiceDelegate = nil;
        _usageDashboardWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLUsageDashboardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLUsageDashboardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)usageDashboardWebService:(NLUsageDashboardWebService *)webService failedToFetchUsageDataWithWebServiceError:(NLWebServiceError *)error
{
    if(webService == _usageDashboardWebService)
    {
        [self.usageDashboardScreenDelegate usageDashboardScreen:self failedToFetchUsageDataWithWebServiceError:error];
        _usageDashboardWebService.usageDashboardWebServiceDelegate = nil;
        _usageDashboardWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLUsageDashboardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLUsageDashboardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

@end
