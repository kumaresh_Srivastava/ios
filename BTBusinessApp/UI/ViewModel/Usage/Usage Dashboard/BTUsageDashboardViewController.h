//
//  BTUsageDashboardViewController.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 22/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BTUsageDashboardViewController : UIViewController

@property (nonatomic, copy) NSString *groupKey;
@property (nonatomic) NSArray *cugs;
 + (BTUsageDashboardViewController *)getUsageDashboardViewController;

@end
