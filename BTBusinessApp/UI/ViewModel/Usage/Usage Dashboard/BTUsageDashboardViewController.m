//
//  BTUsageDashboardViewController.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 22/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTUsageDashboardViewController.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDUser.h"
#import "CDApp.h"
#import "CDCug+CoreDataClass.h"
#import "BTCug.h"
#import "BTUsageTableViewCell.h"
#import "BTUsageAccountViewController.h"
#import "DLMUsageDashboardScreen.h"
#import "BTEmptyDashboardView.h"
#import "BTRetryView.h"
#import "CustomSpinnerView.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "AppConstants.h"
#import "BTRetryView.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "NLWebServiceError.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "BTAccountLocationDropdownView.h"

@interface BTUsageDashboardViewController ()<UITableViewDelegate, UITableViewDataSource,DLMUsageDashboardScreenDelegate, BTRetryViewDelegate>
{
    //UITableView *_usageTableView;
    NSArray *_bacArray;
    BTEmptyDashboardView *_emptyDashboardView;
    BTRetryView *_retryView;
    CustomSpinnerView *_loadingView;
}
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableTopConstraint;

@property (weak, nonatomic) IBOutlet UITableView *usageTableView;
@property(nonatomic, assign) BOOL networkRequestInProgress;
@property (weak, nonatomic) BTAccountLocationDropdownView *accountDropdownView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *groupSelectionButton;
@property (nonatomic, retain)DLMUsageDashboardScreen *usageDashboardScreen;
@property (nonatomic,strong)  CustomSpinnerView *loadingView;
@end

@implementation BTUsageDashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.usageDashboardScreen = [[DLMUsageDashboardScreen alloc] init];
    self.usageDashboardScreen.usageDashboardScreenDelegate = self;

    if(!self.cugs) {
        self.cugs = [[AppDelegate sharedInstance] savedCugs];
    }
    
    if (![AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug) {
        [self.usageDashboardScreen changeSelectedCUGTo:self.cugs[0]];
    }
    
    [self createInitialUI];
    [self updateGroupSelection];

    UINib *dataCell = [UINib nibWithNibName:@"BTUsageTableViewCell" bundle:nil];
    [_usageTableView registerNib:dataCell forCellReuseIdentifier:@"BTUsageTableViewCell"];

    self.navigationItem.hidesBackButton = YES;
    [self createLoadingView];

    __weak typeof(self) selfWeak = self;

    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([selfWeak networkRequestInProgress]) {

            dispatch_async(dispatch_get_main_queue(), ^{

                [selfWeak hideLoadingItems:NO];
                selfWeak.navigationItem.rightBarButtonItem.enabled = NO;
                [selfWeak.loadingView startAnimatingLoadingIndicatorView];
            });
        }

        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
            dispatch_async(dispatch_get_main_queue(), ^{

                [selfWeak hideLoadingItems:YES];
                selfWeak.navigationItem.rightBarButtonItem.enabled = YES;
                [selfWeak.loadingView stopAnimatingLoadingIndicatorView];

            });
        }
    }];

    
    
    [self trackPage];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidDisappear:(BOOL)animated{

    [super viewDidDisappear:animated];
    [self.usageDashboardScreen cancelUsageDashboardAPIRequest];
    self.networkRequestInProgress = NO;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    // if( appDelegate.isSingleBacCug == YES){
        if (self.isMovingFromParentViewController || self.isBeingDismissed) { // BACK Button is PRESSED
            appDelegate.isSingleBacCugBackNavButtonPressed = YES;
        }
        else{
            appDelegate.isSingleBacCugBackNavButtonPressed = NO;
        }
     //}
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
   _accountDropdownView.locationLabel.text = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugName;
    
    [self fetchUsageDashboardAPI];
}


#pragma mark - Public Methods

+ (BTUsageDashboardViewController *)getUsageDashboardViewController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    BTUsageDashboardViewController *controller = (BTUsageDashboardViewController *)[storyboard instantiateViewControllerWithIdentifier:@"BTUsageDashboardViewController"];

    return controller;

}



#pragma mark - InitialUI Methods

- (void)createInitialUI
{

    //CGSize screenSize = [UIScreen mainScreen].bounds.size;

    self.titleLabel.text = @"Usage";
    self.view.backgroundColor = [UIColor colorForHexString:@"eeeeee"];//[BrandColours colourBtNeutral30];
    [self setEdgesForExtendedLayout:UIRectEdgeNone];

    int cugSelectorOffset = 110;
    
    //[SD] Strip for GugName if there is more than one cug
    if(self.cugs.count > 1)
    {
        self.navigationItem.rightBarButtonItem = nil;
        
        _accountDropdownView = [[[NSBundle mainBundle] loadNibNamed:@"BTAccountLocationDropdownView" owner:self options:nil] firstObject];
        _accountDropdownView.translatesAutoresizingMaskIntoConstraints = NO;
        BTCug *firstCug = self.cugs[0];
        _accountDropdownView.locationLabel.text = firstCug.cugName;
        
        UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(grupSelectionButtonAction:)];
        [_accountDropdownView addGestureRecognizer:tapped];
        
        [self.view addSubview:_accountDropdownView];
        
        [self.view addConstraints:@[
                                    [NSLayoutConstraint constraintWithItem:_accountDropdownView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0],
                                    [NSLayoutConstraint constraintWithItem:_accountDropdownView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                    [NSLayoutConstraint constraintWithItem:_accountDropdownView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]
                                    ]];
    }
    else
    {
        self.navigationItem.rightBarButtonItem = nil;
        cugSelectorOffset = 0;
    }

    //[SD]  UITableView Initialiation
    //_usageTableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
    _usageTableView.backgroundColor = [UIColor colorForHexString:@"eeeeee"];//[BrandColours colourBtNeutral30];
    _usageTableView.delegate = self;
    _usageTableView.dataSource = self;
    _usageTableView.showsVerticalScrollIndicator = NO;
    _usageTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //_usageTableView.translatesAutoresizingMaskIntoConstraints = NO;

    //[self.view addSubview:_usageTableView];

    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:_usageTableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:_usageTableView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:_usageTableView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]
                                ]];
    
    if (cugSelectorOffset > 0) {
        [self.view removeConstraint:self.tableTopConstraint];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_usageTableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_accountDropdownView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    } else {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_usageTableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    }
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
}


//- (void)statusBarOrientationChangeUsageDashboard:(NSNotification *)notification {
//    // handle the interface orientation as needed
//    [self tabelViewOrientation];
//}
//
//
//- (void) tabelViewOrientation {
//
//    int cugSelectorOffset = 110;
//    if(self.cugs.count < 1){
//        cugSelectorOffset = 0;
//    }
//    CGSize screenSize = [UIScreen mainScreen].bounds.size;
//    _usageTableView.frame = CGRectMake(0, _usageTableView.frame.origin.y, screenSize.width, screenSize.height - (71 + cugSelectorOffset));
//    _usageTableView.contentInset = UIEdgeInsetsMake(0, 0, 120, 0);
//}

#pragma mark - Private Helper Methods

- (void)trackPage
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = OMNIPAGE_USAGE_ACCOUNT;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
  
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)fetchUsageDashboardAPI
{

    if([AppManager isInternetConnectionAvailable])
    {
        self.networkRequestInProgress = YES;
        [self hideEmmptyDashBoardItems:YES];
        [self hideRetryItems:YES];
        [self.usageDashboardScreen fetchUsageDashboardDetails];
    }
    else
    {
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_USAGE_ACCOUNT];
    }

}

- (void)createLoadingView {

    if(!_loadingView){

        _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addSubview:_loadingView];
    }

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}

- (void)hideLoadingItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to loading.
    [_loadingView setHidden:isHide];

}



- (void)showEmptyDashBoardView {

    //Error Tracker
    [OmnitureManager trackError:OMNIERROR_EMPTY_ACCOUNT_PROCESSSING onPageWithName:OMNIPAGE_USAGE_ACCOUNT contextInfo:[AppManager getOmniDictionary]];
    
    _emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTEmptyDashboardView" owner:nil options:nil] objectAtIndex:0];
    _emptyDashboardView.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSURL *url;
    
    if ([kBTServerType isEqualToString:@"BTServerTypeProd"]) {
        url = [[NSURL alloc] initWithString:@"https://secure.business.bt.com/Account/Addbillingaccounts.aspx"];
    } else{
        url = [[NSURL alloc] initWithString:@"https://eric1-dmze2e-ukb.bt.com/Account/Addbillingaccounts.aspx"];
    }
    
    NSMutableAttributedString *hypLink = [[NSMutableAttributedString  alloc] initWithString:@"There’s no information to show just yet. Your account is either pending approval by your admin, or you can add an account here."];
    
    NSDictionary *linkAttributes = @{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle),
                                     NSLinkAttributeName : url};
    
    [hypLink addAttributes:linkAttributes range:NSMakeRange(hypLink.length-5,4)];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
    [hypLink addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,hypLink.length)];
    
    [hypLink addAttribute:NSForegroundColorAttributeName value:[BrandColours colourBtNeutral70] range:NSMakeRange(0, hypLink.length)];
    
    [hypLink addAttribute:NSFontAttributeName
                    value:[UIFont fontWithName:kBtFontRegular size:16.0] range:NSMakeRange(0, hypLink.length)];
    
    [_emptyDashboardView updateEmptyDashboardViewWithImageNameAndLink:@"package" title:@"No accounts to display" detailText:hypLink andButtonTitle:nil];
    
    //_emptyDashboardView.backgroundColor = [UIColor redColor];
    [self.view addSubview:_emptyDashboardView];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    if(self.accountDropdownView){
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.accountDropdownView  attribute:NSLayoutAttributeTop multiplier:1.0 constant:self.accountDropdownView.frame.size.height]];
    } else{
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    }
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];

}

- (void)hideEmmptyDashBoardItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to retry.
    [_emptyDashboardView setHidden:isHide];
    [_emptyDashboardView removeFromSuperview];
    _emptyDashboardView = nil;

}



- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow {

    if(_retryView)
    {
        _retryView.hidden = NO;
        _retryView.retryViewDelegate = self;
        [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
    } else {
        _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
        _retryView.translatesAutoresizingMaskIntoConstraints = NO;
        [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
        _retryView.retryViewDelegate = self;
        
        [self.view addSubview:_retryView];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.usageTableView  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.usageTableView  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.usageTableView  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.usageTableView  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    }    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
}


- (void)hideRetryItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to retry.
    [_retryView setHidden:isHide];

}


- (void)updateGroupSelection {
    if (self.cugs.count == 1) {
        BTCug *cug = [self.cugs objectAtIndex:0];
        UIFont *titleFont = self.titleLabel.font;
        self.titleLabel.font = [titleFont fontWithSize:20];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        return;
    }
    
    BTCug *cug = (BTCug *)[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug;
    [self setGroupKey:cug.groupKey];
}


- (void) checkForSuperUserAndUpdateUIWithIndex:(NSUInteger)index {
    for(BTCug *cug in self.cugs) {
        if(index == cug.indexInAPIResponse) {
            [self setGroupKey:cug.groupKey];
            [self.usageDashboardScreen changeSelectedCUGTo:cug];
            _accountDropdownView.locationLabel.text = cug.cugName;
            [self resetDataAndUIAfterGroupChange];
        }
    }
}



- (void)resetDataAndUIAfterGroupChange
{
    BOOL needToRefresh = YES;

    if(_retryView)
    {
        if((![AppManager isInternetConnectionAvailable] && !_retryView.isHidden))
            needToRefresh = NO;
    }

    if(needToRefresh){
        [self hideRetryItems:YES];
        _usageTableView.hidden = YES;
        [self fetchUsageDashboardAPI];
    }
    else
    {
        [_retryView updateRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_USAGE_ACCOUNT];
    }

}



#pragma mark - Action methods


- (IBAction)homeButtonAction:(id)sender {

    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)grupSelectionButtonAction:(id)sender {


    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Select Group" preferredStyle:UIAlertControllerStyleActionSheet];

    // (SD) Add group names in actionsheet
    if (self.cugs != nil && self.cugs.count > 0) {

        __weak typeof(self) selfWeak = self;
        
        // (SD) Add action for each cug selection.
        int index = 0;
        for (BTCug *groupData in self.cugs) {

            UIAlertAction *action = [UIAlertAction actionWithTitle:groupData.cugName
                                                             style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                 //Track cug change

                                                                 NSString *pageName = OMNIPAGE_USAGE_ACCOUNT;
                                                                 NSString *linkTitle = OMNICLICK_CUG_CHANGE;
                                                                 NSMutableDictionary *data = [NSMutableDictionary dictionary];

                                                                 [data setValue:@"Logged In" forKey:kOmniLoginStatus];
                                                        
                                                                 [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:data];

                                                                 [selfWeak checkForSuperUserAndUpdateUIWithIndex:groupData.indexInAPIResponse];
                                                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                                             }];
            [alert addAction:action];

            index++;
        }
    }

    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    [alert addAction:cancel];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = _accountDropdownView.frame;
    alert.popoverPresentationController.canOverlapSourceViewRect = NO;

    [self presentViewController:alert animated:YES completion:^{

    }];
}






#pragma mark - UITableView Delegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 103.0;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _bacArray.count;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Background color
    view.tintColor = [BrandColours colourBtNeutral30];

}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTUsageTableViewCell *usageCell = [tableView dequeueReusableCellWithIdentifier:@"BTUsageTableViewCell" forIndexPath:indexPath];
    usageCell.selectionStyle = UITableViewCellSelectionStyleNone;

    [usageCell updateUsageCellWithAsset:[_bacArray objectAtIndex:indexPath.row]];
    return usageCell;
}


//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//    return 85;
//
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    BTAsset *assetsObject = [_bacArray objectAtIndex:indexPath.row];
    BTUsageAccountViewController *controller = [BTUsageAccountViewController getUsageAccountViewController];
    controller.assetsName = assetsObject.assetName;
    controller.isSingleBacCUG = NO;
    controller.selectedAsset = assetsObject;
    controller.billingAccountNumber = assetsObject.billingAccountNumber;
    [self.navigationController pushViewController:controller animated:YES];

}


#pragma mark - DLMUsageDashboardScreenDelegate

- (void)successfullyFetchedUsageOnUsageDashboardScreen:(DLMUsageDashboardScreen *)usageDashboardScreen WithBACArray:(NSArray *)bacList
{
    [self setNetworkRequestInProgress:false];
    _usageTableView.hidden = NO;

    if(bacList.count > 0)
    {
         _bacArray = bacList;
        //// Usages - CUG/BAC if one then navigate to next screen
        if(self.cugs.count == 1 && _bacArray.count == 1){
            
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            appDelegate.isSingleBacCug = YES;
            
            BTAsset *assetsObject = [_bacArray objectAtIndex:0];
            BTUsageAccountViewController *controller = [BTUsageAccountViewController getUsageAccountViewController];
            controller.assetsName = assetsObject.assetName;
            controller.isSingleBacCUG = YES;
            controller.selectedAsset = assetsObject;
            controller.billingAccountNumber = assetsObject.billingAccountNumber;
            
            [self.navigationController pushViewController:controller animated:NO];
            
        } else{
            [_usageTableView reloadData];
        }

    }
    else if(bacList.count == 0)
    {
        [self showEmptyDashBoardView];
    }
    else
    {
        DDLogError(@"Usage Dashboard: BAC not found.");
    }

}




- (void)usageDashboardScreen:(DLMUsageDashboardScreen *)usageDashboardScreen failedToFetchUsageDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    self.networkRequestInProgress = NO;

    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];

    if(errorHandled)
    {
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_USAGE_ACCOUNT];
    }
    
    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {
        
        switch (webServiceError.error.code)
        {
                
            case BTNetworkErrorCodeAPINoDataFound:
            {
                [AppManager trackNoDataFoundErrorOnPage:OMNIPAGE_USAGE_ACCOUNT];
                errorHandled = YES;
                [self showEmptyDashBoardView];
                break;
            }
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }
    
    if(errorHandled == NO)
    {
        [_loadingView stopAnimatingLoadingIndicatorView];
        [self showRetryViewWithInternetStrip:NO];
         [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_USAGE_ACCOUNT];
    }
}

#pragma mark - BTRetryView Delegate

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    [self fetchUsageDashboardAPI];
}



@end
