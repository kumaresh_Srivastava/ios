//
//  BTOrderTrackerHomeViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 23/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTOrderTrackerHomeViewController : UIViewController
@property (nonatomic) BOOL networkRequestInProgress;
@end
