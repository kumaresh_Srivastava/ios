//
//  DLMOrderSearchScreen.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 25/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"

@class DLMOrderSearchScreen;


@protocol DLMOrderSearchScreenDelegate <NSObject>

- (void)successfullyFetchedRecentSearchedOrderDataOnOrderSearchScreen:(DLMOrderSearchScreen *)orderSearchScreen;

@end

@interface DLMOrderSearchScreen : DLMObject

@property (nonatomic, copy) NSArray *arrayOfRecentSearchedOrders;
@property (nonatomic, weak) id <DLMOrderSearchScreenDelegate> orderSearchScreenDelegate;

- (void)fetchRecentSearchedOrders;

@end
