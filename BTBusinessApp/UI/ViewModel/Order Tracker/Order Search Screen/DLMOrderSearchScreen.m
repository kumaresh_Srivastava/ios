//
//  DLMOrderSearchScreen.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 25/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMOrderSearchScreen.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDRecentSearchedOrder.h"
#import "BTRecentSearchedOrder.h"

@interface DLMOrderSearchScreen ()

@end

@implementation DLMOrderSearchScreen

- (void)fetchRecentSearchedOrders {
    
    NSArray *recentSearchedOrders = [[AppDelegate sharedInstance].viewModel.app.loggedInUser.recentlySearchedOrders allObjects];
    
    NSArray *sortedRecentSearchedOrders = [recentSearchedOrders sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"lastSearchedDate" ascending:NO]]];
    
    NSMutableArray *orders = [NSMutableArray array];
    int i = 0;
    for (CDRecentSearchedOrder *recentSearchedOrder in sortedRecentSearchedOrders) {
        BTRecentSearchedOrder *searchedOrder = [[BTRecentSearchedOrder alloc] initWithCDRecentSearchedOrder:recentSearchedOrder];
        [orders addObject:searchedOrder];
        i++;
        if (i == 10) {
            break;
        }
    }
    
    _arrayOfRecentSearchedOrders = [NSArray arrayWithArray:orders];
    [self.orderSearchScreenDelegate successfullyFetchedRecentSearchedOrderDataOnOrderSearchScreen:self];
    
}

@end
