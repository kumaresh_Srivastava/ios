//
//  BTOrderTrackerHomeViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 23/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderTrackerHomeViewController.h"
//#import "BTOrderTrackerRecentlySearchedTableViewCell.h"
#import "BTOrderTrackerDashboardTableViewCell.h"
#import "BTOrderTrackerDetailsViewController.h"
#import "BTOrderTrackerGroupOrderSummaryViewController.h"
#import "DLMOrderSearchScreen.h"
#import "BTRecentSearchedOrder.h"
#import "UIButton+BTButtonProperties.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "AppManager.h"
#import "BTOrderSearchHelpViewController.h"
#import "OmnitureManager.h"
#import "UITextField+BTTextFieldProperties.h"
#import "BTOCSProject.h"
#import "BTOCSOrderDetailsTableViewController.h"


#define kRecentlyViewedText @"Recently viewed"
#define kNORecentlyViewedText @"No recent searched item found"

#define kTallDeviceHeight 812.0f
#define kHelpCardMovement 419.0f
#define kSpaceFromBottom 110.0f
#define kTallDeviceAdditionalSpacing 60.0f

@interface BTOrderTrackerHomeViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, DLMOrderSearchScreenDelegate> {
    BOOL _isGroupOrder;
    BOOL _isOCSOrder;
    BOOL _isValidOrder;
    
}

@property (nonatomic, readwrite) DLMOrderSearchScreen *viewModel;

@property (strong, nonatomic) IBOutlet UIView *orderIDInputContainerView;
@property (strong,nonatomic)IBOutlet UITextField *orderIDTextfield;
@property (strong,nonatomic)IBOutlet UIButton *trackOrderButton;
@property (strong,nonatomic)IBOutlet UITableView *recentlySearchedOrderTableView;

@property (weak, nonatomic) IBOutlet UILabel *recentlyViewedOrederLabel;
@property (weak, nonatomic) IBOutlet UIButton *needHelpButton;

@property(nonatomic,retain)UIPopoverPresentationController *popoverForiPadNeedHelp;

@property (weak, nonatomic) UIView *helpCard;

@property BOOL helpCardVisible;


@end

@implementation BTOrderTrackerHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // (lp) setup viewModel for order search screen
    self.viewModel = [[DLMOrderSearchScreen alloc] init];
    self.viewModel.orderSearchScreenDelegate = self;
    self.orderIDTextfield.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    //navigation bar appearence
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.title = @"Search orders";
    
    self.orderIDInputContainerView.layer.cornerRadius = 5.0f;
    self.orderIDInputContainerView.layer.borderWidth = 1.0f;
    self.orderIDInputContainerView.layer.borderWidth = 1.0f;
    self.orderIDInputContainerView.layer.borderColor = [UIColor colorForHexString:@"666666"].CGColor;
    self.orderIDInputContainerView.layer.cornerRadius = 5.0f;
    
    //table view
    self.recentlySearchedOrderTableView.delegate = self;
    self.recentlySearchedOrderTableView.dataSource = self;
    self.recentlySearchedOrderTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.recentlySearchedOrderTableView.backgroundColor = [UIColor colorForHexString:@"EEEEEE"];
    self.recentlySearchedOrderTableView.estimatedRowHeight = 50;
    self.recentlySearchedOrderTableView.rowHeight = UITableViewAutomaticDimension;
    
    self.orderIDTextfield.delegate = self;
    //[self.orderIDTextfield getBTTextFieldStyle];
    
    /*UIButton *questionHint = [[UIButton alloc] initWithFrame:CGRectMake(0.0,
                                                                        0.0,
                                                                        30, 30)];
    [questionHint setImage:[UIImage imageNamed:@"question"] forState:UIControlStateNormal];
    [questionHint addTarget:self action:@selector(helpButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    self.orderIDTextfield.rightView = questionHint;
    self.orderIDTextfield.rightViewMode = UITextFieldViewModeAlways;
    questionHint.hidden = NO;*/

    
    //[self.trackOrderButton getBTButtonStyle];
    
    __weak typeof(self) weakSelf = self;
    // Order reference textfield signal.
    RACSignal *orderIdSignal = [[[weakSelf orderIDTextfield] rac_textSignal] map:^id (NSString* orderId) {
        orderId = [AppManager stringByTrimmingWhitespaceInString:orderId];
        
        // Order reference textfield Validation.
        BOOL isValid = [orderId length] > 0;
        
        return [NSNumber numberWithBool:isValid];
    }];
    
    // Order Id is required field.
    RACSignal *shouldEnableTrackOrderButtonSignal = [RACSignal combineLatest:@[orderIdSignal] reduce:^id (NSNumber *orderIdIsValid) {
        return @([orderIdIsValid boolValue]);
    }];
    
    // Enable / disable button as appropriate.
    [shouldEnableTrackOrderButtonSignal subscribeNext:^(NSNumber* shouldEnable) {
        [[weakSelf trackOrderButton] setEnabled:[shouldEnable boolValue]];
        if([shouldEnable boolValue] == NO){
            [[weakSelf trackOrderButton] setBackgroundColor:[UIColor colorWithRed:114.0/255.0 green:64.0/255.0 blue:149.0/255.0 alpha:1]];
            [[weakSelf trackOrderButton] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        } else {
            [[weakSelf trackOrderButton] setBackgroundColor:[UIColor colorWithRed:100.0/255.0 green:0.0/255.0 blue:170.0/255.0 alpha:1]];
            [[weakSelf trackOrderButton] setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
    }];
    
    
    _isGroupOrder = NO;
    _isOCSOrder = NO;
    _isValidOrder = NO;
    
//    UINib *recentlySearchedDataCell = [UINib nibWithNibName:@"BTOrderTrackerRecentlySearchedTableViewCell" bundle:nil];
//
//    [self.recentlySearchedOrderTableView registerNib:recentlySearchedDataCell forCellReuseIdentifier:@"recentSearchOrderCell"];
    
    UINib *recentlySearchedDataCell = [UINib nibWithNibName:@"BTOrderTrackerDashboardTableViewCell" bundle:nil];
    
    [self.recentlySearchedOrderTableView registerNib:recentlySearchedDataCell forCellReuseIdentifier:[BTOrderTrackerDashboardTableViewCell reuseId]];
    
    //[self addHelpCard];
    
    [self trackOmniPage:OMNIPAGE_ORDER_SEARCH];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    self.orderIDTextfield.text = @"";
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.viewModel fetchRecentSearchedOrders];
        //[self.orderIDTextfield setText:@""];
        [self updateRecentlyViewedLabel];
    });
    
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.orderIDTextfield becomeFirstResponder];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //[self addHelpCard];
}

- (void)updateRecentlyViewedLabel
{
    if([self.viewModel.arrayOfRecentSearchedOrders count]==0)
    {
        self.recentlyViewedOrederLabel.text = kNORecentlyViewedText;
        self.recentlyViewedOrederLabel.textAlignment = NSTextAlignmentCenter;
        self.recentlyViewedOrederLabel.textColor = [UIColor colorForHexString:@"666666"];
        self.recentlyViewedOrederLabel.font = [UIFont fontWithName:kBtFontRegular size:16.0f];
    }
    else
    {
        self.recentlyViewedOrederLabel.text = kRecentlyViewedText;
        self.recentlyViewedOrederLabel.textAlignment = NSTextAlignmentLeft;
        self.recentlyViewedOrederLabel.textColor = [UIColor colorForHexString:@"333333"];
        self.recentlyViewedOrederLabel.font = [UIFont fontWithName:kBtFontBold size:14.0f];
    }
}

#pragma mark - DLMOrderSearchScreen Delegate methods
- (void)successfullyFetchedRecentSearchedOrderDataOnOrderSearchScreen:(DLMOrderSearchScreen *)orderSearchScreen
{
    
    [self.recentlySearchedOrderTableView reloadData];
}

#pragma mark - table view methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.viewModel.arrayOfRecentSearchedOrders count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    BTOrderTrackerRecentlySearchedTableViewCell *cell = (BTOrderTrackerRecentlySearchedTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"recentSearchOrderCell" forIndexPath:indexPath];
//
//    BTRecentSearchedOrder *recentSearched = [self.viewModel.arrayOfRecentSearchedOrders objectAtIndex:indexPath.row];
//
//    [cell updateCellWithRecentSearchedOrderData:recentSearched];
    
    BTOrderTrackerDashboardTableViewCell *cell = (BTOrderTrackerDashboardTableViewCell *)[tableView dequeueReusableCellWithIdentifier:[BTOrderTrackerDashboardTableViewCell reuseId] forIndexPath:indexPath];
    
    BTRecentSearchedOrder *recentSearched = [self.viewModel.arrayOfRecentSearchedOrders objectAtIndex:indexPath.row];
    
    [cell updateCellWithRecentSearchedOrderData:recentSearched];

    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    BTRecentSearchedOrder *recentSearched = [self.viewModel.arrayOfRecentSearchedOrders objectAtIndex:indexPath.row];
    NSString *orderID = [recentSearched.orderRef stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    orderID = [self validateOrderKey:orderID];
    
    [self.orderIDTextfield resignFirstResponder];
    if (!_isGroupOrder && !_isOCSOrder) {
        BTOrderTrackerDetailsViewController *orderTrackerDetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderTrackerDetailViewController"];
        [orderTrackerDetailsViewController setOrderRef:orderID];
        orderTrackerDetailsViewController.title = @"Order Summary";
        [self.navigationController pushViewController:orderTrackerDetailsViewController animated:YES];
    } else if (_isOCSOrder) {
        BTOCSProject *project = [[BTOCSProject alloc] initWithProjectRef:orderID];
        BTOCSOrderDetailsTableViewController *ocsViewController = [BTOCSOrderDetailsTableViewController getOCSOrderDetailsViewController];
        ocsViewController.project = project;
        [self.navigationController pushViewController:ocsViewController animated:YES];
    } else {
        BTOrderTrackerGroupOrderSummaryViewController *groupOrderSummaryViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderTrackerGroupOrderSummaryViewController"];
        [groupOrderSummaryViewController setOrderRef:orderID];
        groupOrderSummaryViewController.title = @"Group Order Summary";
        [self.navigationController pushViewController:groupOrderSummaryViewController animated:YES];
    }
}

#pragma mark - UITextFieldDelegateMethods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (![textField.text isEqualToString:@""])
    {
        [self trackOrderAction:textField];
    }
    
    return true;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{

    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

#pragma mark - naviagtion methods
- (IBAction)helpButtonAction:(id)sender {
        BTOrderSearchHelpViewController *helpViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"orderSearchHelpViewController"];
        [self.navigationController presentViewController:helpViewController animated:YES completion:nil];

}
/*
- (IBAction)helpButtonAction:(id)sender {
    
//    BTOrderSearchHelpViewController *helpViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"orderSearchHelpViewController"];
//    [self.navigationController presentViewController:helpViewController animated:YES completion:nil];
    
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        
        BTOrderSearchHelpViewController *helpViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"orderSearchHelpViewController"];
        
//        UINavigationController *destNav = [[UINavigationController alloc] initWithRootViewController:helpViewController];
        helpViewController.preferredContentSize = CGSizeMake(self.view.frame.size.width - 20, 525);
        helpViewController.modalPresentationStyle = UIModalPresentationPopover;
        _popoverForiPadNeedHelp = helpViewController.popoverPresentationController;
        _popoverForiPadNeedHelp.delegate = self;
        _popoverForiPadNeedHelp.sourceView = self.needHelpButton;
        _popoverForiPadNeedHelp.sourceRect = self.needHelpButton.frame;
        [self presentViewController:helpViewController animated:YES completion:nil];
    }
    else {
        if (helpPopoverController == nil)
        {
            UIView *btn = (UIView *)sender;
            
            BTOrderSearchHelpViewController *helpViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"orderSearchHelpViewController"];
            helpViewController.preferredContentSize = CGSizeMake(self.view.frame.size.width - 20, 525);
            
            helpViewController.modalInPopover = NO;
            helpPopoverController = [[WYPopoverController alloc] initWithContentViewController:helpViewController];
            helpPopoverController.delegate = self;
            helpPopoverController.passthroughViews = @[btn];
            helpPopoverController.popoverLayoutMargins = UIEdgeInsetsMake(10, 10, 10, 10);
            
            helpPopoverController.wantsDefaultContentAppearance = YES;
            [helpPopoverController presentPopoverFromRect:CGRectMake(btn.frame.origin.x, btn.frame.origin.y+56, self.view.frame.size.width - 20, 525)
                                                   inView:btn
                                 permittedArrowDirections:WYPopoverArrowDirectionDown
                                                 animated:YES
                                                  options:WYPopoverAnimationOptionFadeWithScale];
        }
        else
        {
            [self close:nil];
        }
    }
   
}

- (void)close:(id)sender
{
    [helpPopoverController dismissPopoverAnimated:YES completion:^{
        [self popoverControllerDidDismissPopover:helpPopoverController];
    }];
}

*/


- (IBAction)trackOrderAction:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    NSString *orderID = [self.orderIDTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    orderID = [orderID uppercaseString];
    
    orderID = [self validateOrderKey:orderID];
    
    if (orderID.length > 0 && _isValidOrder) {
        
        self.orderIDTextfield.text = @"";
        [self.orderIDTextfield resignFirstResponder];
        
        if (!_isGroupOrder && !_isOCSOrder) {
            BTOrderTrackerDetailsViewController *orderTrackerDetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderTrackerDetailViewController"];
            [orderTrackerDetailsViewController setOrderRef:orderID];
            orderTrackerDetailsViewController.title = @"Order Summary";
            [self.navigationController pushViewController:orderTrackerDetailsViewController animated:YES];
        } else if (_isOCSOrder) {
            // show OCS project summary
            BTOCSProject *project = [[BTOCSProject alloc] initWithProjectRef:orderID];
            BTOCSOrderDetailsTableViewController *ocsViewController = [BTOCSOrderDetailsTableViewController getOCSOrderDetailsViewController];
            ocsViewController.project = project;
            [self.navigationController pushViewController:ocsViewController animated:YES];
        } else {
            BTOrderTrackerGroupOrderSummaryViewController *groupOrderSummaryViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderTrackerGroupOrderSummaryViewController"];
            [groupOrderSummaryViewController setOrderRef:orderID];
            groupOrderSummaryViewController.title = @"Group Order Summary";
            [self.navigationController pushViewController:groupOrderSummaryViewController animated:YES];
        }
        
    } else {
        
        DDLogError(@"Error : Order reference isn't right.");
        
        UIAlertController *invalidOrderIdAlert = [UIAlertController alertControllerWithTitle:@"Sorry, that reference isn’t right." message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        NSMutableAttributedString *messageString = [[NSMutableAttributedString  alloc] initWithString:@"For guidance click on 'Need help?' or try again!"];
        [messageString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:NSMakeRange(0, messageString.length)];
        //[messageString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:NSMakeRange(23, 1)];
        
        [invalidOrderIdAlert setValue:messageString forKey:@"attributedMessage"];
        
        [self trackContentViewedForPage:OMNIPAGE_ORDER_SEARCH andNotificationPopupDetails:@"Sorry we didn’t recognise that order reference"];
        
        
        UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
              [weakSelf trackContentViewedForPage:OMNIPAGE_ORDER_SEARCH andNotificationPopupDetails:@"OK"];
        }];
        [invalidOrderIdAlert addAction:okButton];
        [self presentViewController:invalidOrderIdAlert animated:YES completion:nil];
    }
}


#pragma mark - validate order reference
- (NSString *)validateOrderKey:(NSString *)orderID
{
    _isValidOrder = NO;
    _isGroupOrder = NO;
    _isOCSOrder = NO;
    
    NSArray *regexArray = @[@"^[Bb][Oo][Ss][0-9]{8}$",
                            @"^[Vv][Oo][Ll][0123]{3}-[0-9]{10,12}$",
                            @"^[Bb][Tt][A-Za-z0-9]{6,6}$",
                            @"^[a-zA-z]{3}[0-9]{3}[a-zA-z]{2}$",
                            @"^(1-[0-9]+)$",
                            @"^[Gg][Bb][Tt][A-Ha-hJ-Nj-nP-Zp-z0-9]{5}$",
                            @"^[Bb][Oo][Ss][0-9]{7}$",
                            @"^[Pp][Xx][a-zA-Z0-9]{0,9}-[a-zA-Z0-9]{0,9}$"
                            ];
    
    for (NSString *currentString in regexArray) {
        if([self regexMatchesInString:orderID withComparableString:currentString])
        {
            _isValidOrder = YES;
            break;
        }
    }
    
    if([self regexMatchesInString:orderID withComparableString:regexArray[5]])
    {
        _isGroupOrder = YES;
    }
    
    if([self regexMatchesInString:orderID withComparableString:regexArray[7]])
    {
        _isOCSOrder = YES;
    }
    
    return orderID;
}

- (BOOL)regexMatchesInString:(NSString*)sourceString withComparableString:(NSString *)comparableString
{
    NSRange range = [sourceString rangeOfString:comparableString options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive];
    return range.location != NSNotFound && range.length > 0 && range.location == 0 ? YES:NO;
}

#pragma mark - Text field delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSRange lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
    
    if (lowercaseCharRange.location != NSNotFound) {
        textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                 withString:[string uppercaseString]];
        return NO;
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [_orderIDTextfield updateWithDisabledState];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [_orderIDTextfield updateWithEnabledState];
}

# pragma mark - help card
- (void) addHelpCard {
    _helpCardVisible = NO;
    _helpCard = [[[NSBundle mainBundle] loadNibNamed:@"OrderTrackerSearchHelp" owner:nil options:nil] objectAtIndex:0];
    CGRect helpCardFrame = _helpCard.frame;
    
    helpCardFrame.origin.x = ([UIScreen mainScreen].bounds.size.width / 2) - (helpCardFrame.size.width / 2);
    
    UITabBarController *tabBarController = [UITabBarController new];
    CGFloat tabBarHeight = tabBarController.tabBar.frame.size.height; // what's the height of a tabbarviewcontroller?
    float height = [UIScreen mainScreen].bounds.size.height;
    if(height == kTallDeviceHeight) {
        helpCardFrame.origin.y = [UIScreen mainScreen].bounds.size.height - (tabBarHeight + + kSpaceFromBottom + kTallDeviceAdditionalSpacing);
    } else {
        helpCardFrame.origin.y = [UIScreen mainScreen].bounds.size.height - (tabBarHeight + kSpaceFromBottom);
    }
    
    [_helpCard setFrame:helpCardFrame];
    
    UITapGestureRecognizer *tappedHelpCard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moveHelpCard)];
    [_helpCard addGestureRecognizer:tappedHelpCard];
    [self.view addSubview:_helpCard];
}

- (void) moveHelpCard {
    CGRect helpCardFrame = _helpCard.frame;
    
    if(_helpCardVisible){
        _helpCardVisible = NO;
        helpCardFrame.origin.y += kHelpCardMovement;
    }
    else {
        _helpCardVisible = YES;
        helpCardFrame.origin.y -= kHelpCardMovement;
    }
    
    _helpCard.frame = helpCardFrame;
}

- (void)trackOmniPage:(NSString *)page
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = page;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackContentViewedForPage:(NSString *)page andNotificationPopupDetails:(NSString *)notificationPopup
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = page;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    NSString *notificaiton = [NSString stringWithFormat:@"%@%@",STATE_PREFIX,notificationPopup];
    [data setValue:notificaiton forKey:kOmniContentViewed];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
    
}


@end
