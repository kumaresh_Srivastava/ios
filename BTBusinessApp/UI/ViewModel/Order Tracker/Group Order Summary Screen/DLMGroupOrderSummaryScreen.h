//
//  DLMGroupOrderSummaryScreen.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 05/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"

@class DLMGroupOrderSummaryScreen;
@class BTOrder;
@class NLWebServiceError;

@protocol DLMGroupOrderSummaryScreenDelegate <NSObject>

- (void)successfullyFetchedGroupOrderSummaryDataOnGroupOrderSummaryScreen:(DLMGroupOrderSummaryScreen *)groupOrderSummaryScreen;

- (void)groupOrderSummaryScreen:(DLMGroupOrderSummaryScreen *)groupOrderSummaryScreen failedToFetchGroupOrderSummaryDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface DLMGroupOrderSummaryScreen : DLMObject {
    
}

@property (nonatomic, weak) id <DLMGroupOrderSummaryScreenDelegate> groupOrderSummaryScreenDelegate;

@property (nonatomic, copy) NSArray *arrayOfOrders;
@property (nonatomic, copy) NSString *orderNumber;
@property (nonatomic, strong) NSDate *placedOnDate;

- (void)fetchGroupOrderSummaryForOrderReference:(NSString *)groupOrderRef;

@end
