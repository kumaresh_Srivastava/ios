//
//  DLMGroupOrderSummaryScreen.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 05/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMGroupOrderSummaryScreen.h"
#import "NLGetGroupOrderSummaryWebService.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDUser.h"
#import "CDApp.h"
#import "CDAuthenticationToken.h"
#import "AppConstants.h"
#import "BTOrder.h"
#import "NLWebServiceError.h"
#import "CDRecentSearchedOrder.h"


@interface DLMGroupOrderSummaryScreen () <NLGetGroupOrderSummaryWebServiceDelegate> {
    
}

@property (nonatomic) NLGetGroupOrderSummaryWebService *getGroupOrderSummaryWebService;

@end

@implementation DLMGroupOrderSummaryScreen

- (void)setupToFetchGroupOrderSummaryForOrderRef:(NSString *)groupOrderRef
{
    self.getGroupOrderSummaryWebService = [[NLGetGroupOrderSummaryWebService alloc] initWithGroupOrderRef:groupOrderRef];
    self.getGroupOrderSummaryWebService.getGroupOrderSummaryWebServiceDelegate = self;
}

- (void)fetchGroupOrderSummaryForCurrentGroupOrder
{
    [self.getGroupOrderSummaryWebService resume];
}

- (void)fetchGroupOrderSummaryForOrderReference:(NSString *)groupOrderRef
{
    [self setupToFetchGroupOrderSummaryForOrderRef:groupOrderRef];
    [self fetchGroupOrderSummaryForCurrentGroupOrder];
}

#pragma mark - NLGetGroupOrderSummaryWebServiceDelegate Methods

- (void)getGroupOrderSummaryWebService:(NLGetGroupOrderSummaryWebService *)webService successfullyFetchedGroupOrderSummaryDataWithOrders:(NSArray *)arrayOfOrders withOrderNumber:(NSString *)orderNumber withPlacedOnDate:(NSDate *)placedOnDate
{
    if(webService == _getGroupOrderSummaryWebService) {
        if (orderNumber != nil) {
            _arrayOfOrders = arrayOfOrders;
            _orderNumber = orderNumber;
            _placedOnDate = placedOnDate;
        }
        
        //Update Recently searched data
        NSManagedObjectContext *context = [AppDelegate sharedInstance].managedObjectContext;
        CDUser *persistenceUser = [CDUser userWithUsername:[AppDelegate sharedInstance].viewModel.app.loggedInUser.username inManagedObjectContext:context];
        
        NSArray *arrayOfRecentSearchedOrders = [persistenceUser.recentlySearchedOrders allObjects];
        
        CDRecentSearchedOrder *searchedOrder = nil;
        for (CDRecentSearchedOrder *recentSearchedOrder in arrayOfRecentSearchedOrders)
        {
            
            if(recentSearchedOrder.orderRef && orderNumber)
            {
                if ([recentSearchedOrder.orderRef isEqualToString:orderNumber])
                {
                    searchedOrder = recentSearchedOrder;
                }
            }
        }
        
        if (searchedOrder == nil) {
            searchedOrder = [CDRecentSearchedOrder newRecentSearchedOrderInManagedObjectContext:context];
            searchedOrder.orderRef = orderNumber;
            searchedOrder.orderStatus = @"";
            searchedOrder.orderDescription = @"Group of orders";
            searchedOrder.completionDate = nil;
            searchedOrder.placedOnDate = placedOnDate;
            searchedOrder.lastSearchedDate = [NSDate date];
            
            [persistenceUser addRecentlySearchedOrdersObject:searchedOrder];
        }
        else
        {
            searchedOrder.orderStatus = @"";
            searchedOrder.completionDate = nil;
            searchedOrder.lastSearchedDate = [NSDate date];
            
            [persistenceUser addRecentlySearchedOrdersObject:searchedOrder];
        }
        
        [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
        

        [self.groupOrderSummaryScreenDelegate successfullyFetchedGroupOrderSummaryDataOnGroupOrderSummaryScreen:self];
        _getGroupOrderSummaryWebService.getGroupOrderSummaryWebServiceDelegate = nil;
        _getGroupOrderSummaryWebService = nil;
    } else {
        DDLogError(@"This delegate method gets called because of success of an object of NLGetGroupOrderSummaryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLGetGroupOrderSummaryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}


- (void)getGroupOrderSummaryWebService:(NLGetGroupOrderSummaryWebService *)webService failedToFetchGroupOrderSummaryDataWithWebServiceError:(NLWebServiceError *)error {
    if(webService == _getGroupOrderSummaryWebService) {
        [self.groupOrderSummaryScreenDelegate groupOrderSummaryScreen:self failedToFetchGroupOrderSummaryDataWithWebServiceError:error];
        _getGroupOrderSummaryWebService.getGroupOrderSummaryWebServiceDelegate = nil;
        _getGroupOrderSummaryWebService = nil;
    } else {
        DDLogError(@"This delegate method gets called because of failure of an object of NLGetGroupOrderSummaryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLGetGroupOrderSummaryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}


@end
