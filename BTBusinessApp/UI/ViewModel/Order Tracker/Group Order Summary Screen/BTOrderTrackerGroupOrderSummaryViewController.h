//
//  BTOrderTrackerGroupOrderSummaryViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 07/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTOrderTrackerGroupOrderSummaryViewController : UIViewController

@property(nonatomic,strong)NSString *orderRef;
@property (nonatomic) BOOL networkRequestInProgress;

@end
