//
//  BTOrderTrackerGroupOrderSummaryViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 07/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderTrackerGroupOrderSummaryViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "BTOrderTrackerGroupOrderSummaryTableViewCell.h"
#import "BTOrderTrackerDetailsViewController.h"
#import "DLMGroupOrderSummaryScreen.h"
#import "BTRetryView.h"
#import "CustomSpinnerView.h"
#import "BTOrder.h"
#import "AppConstants.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "NLWebServiceError.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "BTEmptyDashboardView.h"

#define kOriginalHeaderViewHeight 70;

@interface BTOrderTrackerGroupOrderSummaryViewController ()<UITableViewDelegate, UITableViewDataSource, DLMGroupOrderSummaryScreenDelegate, BTRetryViewDelegate> {
    CustomSpinnerView *_loadingView;
    BTRetryView *_retryView;


}

@property (nonatomic, readwrite) DLMGroupOrderSummaryScreen *viewModel;

@property (weak, nonatomic) IBOutlet UIView *dateDetailsView;
@property (weak, nonatomic) IBOutlet UIView *bottomDescriptionView;
@property (weak, nonatomic) IBOutlet UIView *topDetailsContainerView;

@property (weak, nonatomic) IBOutlet UILabel *orderIdValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderPlacedDateLabel;
@property (weak, nonatomic) IBOutlet UITableView *groupOrderSummaryTableView;

@end

@implementation BTOrderTrackerGroupOrderSummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    //Set view model
    self.viewModel = [[DLMGroupOrderSummaryScreen alloc] init];
    self.viewModel.groupOrderSummaryScreenDelegate = self;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    //group order details table view config
    self.groupOrderSummaryTableView.delegate = self;
    self.groupOrderSummaryTableView.dataSource = self;
    self.groupOrderSummaryTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.groupOrderSummaryTableView.backgroundColor = [UIColor clearColor];
    self.groupOrderSummaryTableView.estimatedRowHeight = 90;
    self.groupOrderSummaryTableView.rowHeight = UITableViewAutomaticDimension;
    
    __weak typeof(self) weakSelf = self;
    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([weakSelf networkRequestInProgress]) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
        }
        
        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
        }
    }];
    
    UINib *dataCell = [UINib nibWithNibName:@"BTOrderTrackerGroupOrderSummaryTableViewCell" bundle:nil];
    
    [self.groupOrderSummaryTableView registerNib:dataCell forCellReuseIdentifier:@"BTOrderTrackerGroupOrderSummaryTableViewCell"];
    
    [self initialSetupForGroupOrderSummaryScreen];
    [self fetchGroupOrderSummaryDetails];
    
    [self trackOmniPage];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private helper method
/*
 Resize header view of table to update height of header view
 */
- (void)resizeTableViewHeaderToFit {
    UITableViewHeaderFooterView *headerView = (UITableViewHeaderFooterView *)self.groupOrderSummaryTableView.tableHeaderView;
    
    [headerView setNeedsLayout];
    [headerView layoutIfNeeded];
    
    CGFloat height = [headerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CGRect frame = headerView.frame;
    frame.size.height = height;
    headerView.frame = frame;
    
    self.groupOrderSummaryTableView.tableHeaderView = headerView;
    
}

- (void)initialSetupForGroupOrderSummaryScreen
{
    
    // (LP) initial setup for UI items
    self.title = @"Group order";
    self.orderPlacedDateLabel.text = @"";
    
    [self createLoadingView];
    
    [self hideorShowControls:YES];
}

- (void)createLoadingView {
    
    _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
    _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:_loadingView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}

- (void)hideLoadingItems:(BOOL)isHide {
    
    // (lp) Hide or Show UI elements related to loading.
    [_loadingView setHidden:isHide];
    
}

- (void)showRetryViewWithInernetStrip:(BOOL)needToShowInternetStrip {
    
    if(!_retryView)
    {
        _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
        _retryView.translatesAutoresizingMaskIntoConstraints = NO;
        _retryView.retryViewDelegate = self;
        [_retryView updateRetryViewWithInternetStrip:needToShowInternetStrip];
        [self.view addSubview:_retryView];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];

    }
    
    _retryView.hidden = NO;
    _retryView.retryViewDelegate = self;
    [_retryView updateRetryViewWithInternetStrip:needToShowInternetStrip];
    
}

- (void)hideRetryItems:(BOOL)isHide {
    
    // (lp) Hide or Show UI elements related to retry.
    [_retryView setRetryViewDelegate:nil];
    [_retryView setHidden:isHide];
    
}



- (void)trackOmniPage
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = OMNIPAGE_ORDER_GROUP_ORDER;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    [data setValue:self.orderRef forKey:kOmniOrderRef];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}


- (void) hideorShowControls:(BOOL)isHide {
    // (lp) Hide UI elements during loading time
    [self.topDetailsContainerView setHidden:isHide];
    [self.dateDetailsView setHidden:isHide];
    [self.bottomDescriptionView setHidden:isHide];
    [self.groupOrderSummaryTableView setHidden:isHide];
}


- (void)showEmptyViewWithTitle:(NSString *)titleText andNeedToShowImage:(BOOL)needToShowImage
{
    // No Order found
   BTEmptyDashboardView *emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTEmptyDashboardView" owner:nil options:nil] objectAtIndex:0];
    emptyDashboardView.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (needToShowImage)
    {
        [emptyDashboardView updateEmptyDashboardViewWithImageName:@"package" title:titleText detailText:nil andButtonTitle:nil];
    }
    else
    {
        [emptyDashboardView updateEmptyDashboardViewWithImageName:nil title:titleText detailText:nil andButtonTitle:nil];
    }
    
    
    [self.view addSubview:emptyDashboardView];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:emptyDashboardView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:emptyDashboardView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:10.0]];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:emptyDashboardView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}


- (void)trackOmniClick:(NSString *)clickEvent forPage:(NSString *)page
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:[AppManager getOmniDictionary]];
    NSString *pageName = page;
    NSString *linkTitle = clickEvent;
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:params];
}

#pragma mark - BTRetryView delegate methods
- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView {
    
   if(![AppManager isInternetConnectionAvailable])
   {
       [self showRetryViewWithInernetStrip:YES];
       [AppManager trackNoInternetErrorOnPage:OMNIPAGE_ORDER_GROUP_ORDER];
   }
    else
    {
         [self fetchGroupOrderSummaryDetails];
    }
   
}

#pragma mark - Fetch user details methods
- (void)fetchGroupOrderSummaryDetails {
    
    if(![AppManager isInternetConnectionAvailable])
    {
        [self showRetryViewWithInernetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_ORDER_GROUP_ORDER];
    }
    else
    {
        // (LP) Ask to viewModel to provide order summary details. The viewModel takes care of fetching data.
        [self.viewModel fetchGroupOrderSummaryForOrderReference:self.orderRef];
        self.networkRequestInProgress = YES;
        [self hideLoadingItems:NO];
        [self hideRetryItems:YES];
        [_loadingView startAnimatingLoadingIndicatorView];
    }

}

#pragma mark - table view methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.viewModel.arrayOfOrders.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTOrder *order = [self.viewModel.arrayOfOrders objectAtIndex:indexPath.row];
    BTOrderTrackerGroupOrderSummaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BTOrderTrackerGroupOrderSummaryTableViewCell" forIndexPath:indexPath];
    [cell updateGroupOrderCellWithOrderData:order];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTOrder *order = [self.viewModel.arrayOfOrders objectAtIndex:indexPath.row];
    
    NSString *linkName = [NSString stringWithFormat:@"%@ %@",order.orderStatus,OMNICLICK_ORDER_DETAILS];
    
    [self trackOmniClick:linkName forPage:OMNIPAGE_ORDER_GROUP_ORDER];
    
    BTOrderTrackerDetailsViewController *orderTrackerDetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderTrackerDetailViewController"];
    [orderTrackerDetailsViewController setOrderRef:order.orderRef];
    [self.navigationController pushViewController:orderTrackerDetailsViewController animated:YES];
}

#pragma mark - Update UI methods

- (void)updateUserInterface {
    
    // (LP) Update status color according to order status
    self.orderIdValueLabel.text = self.viewModel.orderNumber; //[NSString stringWithFormat:@"Ref: %@",self.viewModel.orderNumber];
    self.orderPlacedDateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:self.viewModel.placedOnDate];
    
    [self.groupOrderSummaryTableView reloadData];
    [self hideorShowControls:NO];
    [self hideRetryItems:YES];
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    [self resizeTableViewHeaderToFit];
}


#pragma mark - DLMGroupOrderSummaryScreenDelegate Methods

- (void)successfullyFetchedGroupOrderSummaryDataOnGroupOrderSummaryScreen:(DLMGroupOrderSummaryScreen *)groupOrderSummaryScreen
{
    self.networkRequestInProgress = NO;
    
    if (self.viewModel.orderNumber) {
    
        // (lp) Update UI with order summary data
        if ([self.viewModel.arrayOfOrders count] == 0) {
            // (lp) Retry to fetch order details
            [self hideLoadingItems:YES];
            [_loadingView stopAnimatingLoadingIndicatorView];
            [self showEmptyViewWithTitle:@"No order found" andNeedToShowImage:YES];
        }
        else
        {
            [self updateUserInterface];
        }
    } else {
        // (lp) Retry to fetch order details
        [self hideRetryItems:NO];
        [self hideLoadingItems:YES];
        [_loadingView stopAnimatingLoadingIndicatorView];
        
    }
    
}

- (void)groupOrderSummaryScreen:(DLMGroupOrderSummaryScreen *)groupOrderSummaryScreen failedToFetchGroupOrderSummaryDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    self.networkRequestInProgress = NO;
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
    
    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {
        
        switch (webServiceError.error.code)
        {
                
            case BTNetworkErrorCodeAPINoDataFound:
            {
                [AppManager trackNoDataFoundErrorOnPage:OMNIPAGE_ORDER_GROUP_ORDER];
                [self showEmptyViewWithTitle:@"No order found" andNeedToShowImage:YES];
                errorHandled = YES;
                break;
            }
            case BTNetworkErrorCodeUnsupportedOrder:
            {
                [self showEmptyViewWithTitle:@"Sorry, we can't show you this order just yet" andNeedToShowImage:NO];
                errorHandled = YES;
                break;
            }
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }
    
    if(errorHandled == NO)
    {
        [self showRetryViewWithInernetStrip:NO];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_ORDER_GROUP_ORDER];
    }
    
}



@end
