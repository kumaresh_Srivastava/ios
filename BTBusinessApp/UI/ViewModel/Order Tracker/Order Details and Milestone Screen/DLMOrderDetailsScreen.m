//
//  DLMOrderDetailsScreen.m
//  BTBusinessApp
//
//  Created by Accolite on 06/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMOrderDetailsScreen.h"
#import "NLOrderDetailsWebService.h"
#import "NLGetOrderMilestonesWebService.h"
#import "AppDelegate.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppDelegateViewModel.h"
#import "AppConstants.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"
#import "BTOrderMilestoneNodeModel.h"
#import "BTPricingDetails.h"
#import "BTBillingDetails.h"
#import "BTProductDetails.h"
#import "BTProduct.h"
#import "BTLiveChatAvailabilityChecker.h"

@interface DLMOrderDetailsScreen () <NLOrderDetailsWebServiceDelegate, NLGetOrderMilestonesWebServiceDelegate, BTLiveChatAvailabilityCheckerDelegate> {
    
    BTLiveChatAvailabilityChecker *_liveChatAvailabityChecker;
}

@property (nonatomic) NLOrderDetailsWebService *getOrderDetailsWebService;
@property (nonatomic) NLOrderDetailsWebService *getOrderDetailsWithPostCodeWebService;
@property (nonatomic) NLGetOrderMilestonesWebService *getOrderMilestonesWebService;
@property (nonatomic) NLGetOrderMilestonesWebService *getOrderMilestonesWithPostCodeWebService;

@end

@implementation DLMOrderDetailsScreen

#pragma mark Public Methods

- (void)setupToFetchOrderDetailsWithOrderRef:(NSString *)orderRef andItemRef:(NSString *)itemRef
{
    self.getOrderDetailsWebService = [[NLOrderDetailsWebService alloc] initWithOrderRef:orderRef itemRef:itemRef];
    self.getOrderDetailsWebService.getOrderDetailsWebServiceDelegate = self;
}

- (void)fetchOrderDetailsForCurrentOrder
{
    [self.getOrderDetailsWebService resume];
}

- (void)fetchOrderDetailsForOrderReference:(NSString *)orderRef andItemRef:(NSString *)itemRef
{
    DDLogInfo(@"Fetching order details from server for order ref %@ and item ref %@", orderRef, itemRef);
    
    [self setupToFetchOrderDetailsWithOrderRef:orderRef andItemRef:itemRef];
    [self fetchOrderDetailsForCurrentOrder];
}

- (void)setupToFetchCompleteOrderDetailsForOrderReference:(NSString *)orderRef ItemRef:(NSString *)itemRef postCode:(NSString *)postCode andErrorCount:(NSInteger)errorCount
{
    self.getOrderDetailsWithPostCodeWebService = [[NLOrderDetailsWebService alloc] initWithOrderRef:orderRef itemRef:itemRef postCode:postCode errorCount:errorCount];
    self.getOrderDetailsWithPostCodeWebService.getOrderDetailsWebServiceDelegate = self;
}

- (void)fetchCompleteOrderDetailsForCurrentOrder
{
    [self.getOrderDetailsWithPostCodeWebService resume];
}

- (void)fetchCompleteOrderDetailsForOrderReference:(NSString *)orderRef ItemRef:(NSString *)itemRef postCode:(NSString *)postCode andErrorCount:(NSInteger)errorCount
{
    DDLogInfo(@"Fetching order details from server for order ref %@ and item ref %@", orderRef, itemRef);
    
    [self setupToFetchCompleteOrderDetailsForOrderReference:orderRef ItemRef:itemRef postCode:postCode andErrorCount:errorCount];
    [self fetchCompleteOrderDetailsForCurrentOrder];
}

- (void)setupToFetchOrderMilestonesForOrderReference:(NSString *)orderRef andItemRef:(NSString *)itemRef
{
    self.getOrderMilestonesWebService = [[NLGetOrderMilestonesWebService alloc] initWithOrderRef:orderRef itemRef:itemRef];
    self.getOrderMilestonesWebService.getOrderMilestonesWebServiceDelegate = self;
}

- (void)fetchOrderMilestonesForCurrentOrder
{
    [self.getOrderMilestonesWebService resume];
}

- (void)fetchOrderMilestonesForOrderReference:(NSString *)orderRef andItemRef:(NSString *)itemRef
{
    DDLogInfo(@"Fetching order milestones from server for order ref %@ and item ref %@", orderRef, itemRef);
    
    [self setupToFetchOrderMilestonesForOrderReference:orderRef andItemRef:itemRef];
    [self fetchOrderMilestonesForCurrentOrder];
}

- (void)setupToFetchOrderMilestonesForOrderReference:(NSString *)orderRef ItemRef:(NSString *)itemRef postCode:(NSString *)postCode andErrorCount:(NSInteger)errorCount
{
    self.getOrderMilestonesWithPostCodeWebService = [[NLGetOrderMilestonesWebService alloc] initWithOrderRef:orderRef itemRef:itemRef postCode:postCode errorCount:errorCount];
    self.getOrderMilestonesWithPostCodeWebService.getOrderMilestonesWebServiceDelegate = self;
}

- (void)fetchCompleteOrderMilestonesForCurrentOrder
{
    [self.getOrderMilestonesWithPostCodeWebService resume];
}

- (void)fetchOrderMilestonesForOrderReference:(NSString *)orderRef ItemRef:(NSString *)itemRef postCode:(NSString *)postCode andErrorCount:(NSInteger)errorCount
{
    DDLogInfo(@"Fetching order milestones from server for order ref %@ and item ref %@ with Postcode %@", orderRef, itemRef, postCode);
    [self setupToFetchOrderMilestonesForOrderReference:orderRef ItemRef:itemRef postCode:postCode andErrorCount:errorCount];
    [self fetchCompleteOrderMilestonesForCurrentOrder];
}

- (void)setupToCheckLiveChatAvailability
{
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = [[BTLiveChatAvailabilityChecker alloc] init];
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = self;
}

- (void)getAvailableSlotsForChat
{
    [_liveChatAvailabityChecker getLiveChatAvailableSlots];
}

- (void)checkForLiveChatAvailibilityForOrder
{
    [self setupToCheckLiveChatAvailability];
    [self getAvailableSlotsForChat];
}



#pragma mark - Public Method

- (NSString *)getImageNameForProductIcon:(NSString *)productIcon
{
    NSString *imageName = nil;

    if ([productIcon isEqualToString:@"broadbandIcon"] || [productIcon isEqualToString:@"broadbandIconSmall"]) {
        imageName = @"broadband";
    } else if ([productIcon isEqualToString:@"phoneIcon"] || [productIcon isEqualToString:@"phoneIconSmall"]) {
        imageName = @"phoneline";
    } else if ([productIcon isEqualToString:@"hubIcon"] || [productIcon isEqualToString:@"hubIconSmall"]) {
        imageName = @"broadband";
    } else if ([productIcon isEqualToString:@"isdnIcon"] || [productIcon isEqualToString:@"isdnIconSmall"]) {
        imageName = @"phoneline";
    }

    return imageName;
}

#pragma mark Canceling

- (void) cancelCurrentRequest {
    self.getOrderDetailsWebService.getOrderDetailsWebServiceDelegate = nil;
    [self.getOrderDetailsWebService cancel];
    self.getOrderDetailsWebService = nil;
}

- (void)cancelGetOrderMilestonesRequest {
    self.getOrderMilestonesWebService.getOrderMilestonesWebServiceDelegate = nil;
    [self.getOrderMilestonesWebService cancel];
    self.getOrderMilestonesWebService = nil;
}

- (void)cancelChatAvailabilityCheckerAPI
{
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    [_liveChatAvailabityChecker cancel];
    _liveChatAvailabityChecker = nil;
}

#pragma mark - NLOrderDetailsWebServiceDelegate Methods

- (void)getOrderDetailsWebService:(NLOrderDetailsWebService *)webService successfullyFetchedOrderDetailsProductData:(BTProduct *)productSummary billingDetails:(BTBillingDetails *)billingDetails pricingDetails:(BTPricingDetails *)pricingDetails productDetails:(BTProductDetails *)productDetails milestoneData:(NSArray *)arrayOfOrderMilestoneData isAuthLevel:(NSInteger)authLevel isSIM2Order:(BOOL)isSIM2
{

   if(webService == _getOrderDetailsWebService)
    {
        [self setProductSummary:productSummary];
        [self setBillingDetails:billingDetails];
        [self setPricingDetails:pricingDetails];
        [self setProductDetails:productDetails];
        [self setAuthLevel:authLevel];
        self.isSIMOrder = isSIM2;
        //_isMoverOrder = false;
        
        if ((billingDetails.billingAddress != nil) || (billingDetails.billFrequency != nil) || (billingDetails.billType)) {
            _isBillingDetailsAvailable = YES;
        }

        if (authLevel != 1)
        {
            
            if (arrayOfOrderMilestoneData && arrayOfOrderMilestoneData.count > 0)
            {
                
                if (arrayOfOrderMilestoneData.count > 1)
                {
                    _isMoverOrder = YES;
                }
                
                NSMutableArray *arrayOfOrderMilestoneNodes = [NSMutableArray array];
                
                for (NSDictionary *orderMilestoneData in arrayOfOrderMilestoneData)
                {
                    if (orderMilestoneData != nil) {
                        
                        NSMutableArray *milestoneDetails = [NSMutableArray array];
                        
                        if ([orderMilestoneData valueForKey:@"MilestoneNodes"])
                        {
                            [milestoneDetails addObject:[orderMilestoneData valueForKey:@"MilestoneNodes"]];
                        }
                        
                        if ([orderMilestoneData valueForKey:@"DispatchDetails"])
                        {
                            [milestoneDetails addObject:[orderMilestoneData valueForKey:@"DispatchDetails"]];
                        }

                        
                        [arrayOfOrderMilestoneNodes addObject:milestoneDetails];
                        
                        if (!_isMoverOrder)
                        {
                            _isActivationDateAvailable = [[orderMilestoneData valueForKey:@"IsActivationDateAvailable"] boolValue];
                            if (_isActivationDateAvailable) {
                                _activationDate = [orderMilestoneData valueForKey:@"ActivationDate"];
                            }
                            _isEngineerAppointmentDateAvailable = [[orderMilestoneData valueForKey:@"IsEngineerAppointmentDateAvailable"] boolValue];
                            if (_isEngineerAppointmentDateAvailable) {
                                _engineerAppointmentDate = [orderMilestoneData valueForKey:@"EngineerAppointmentDate"];
                            }
                            _isSiteContactAvailable = [[orderMilestoneData valueForKey:@"IsSiteContactAvailable"] boolValue];
                            if (_isSiteContactAvailable) {
                                _siteContact = [orderMilestoneData valueForKey:@"SiteContact"];
                            }
                            _isAltSiteContactAvailable = [[orderMilestoneData valueForKey:@"IsAltSiteContactAvailable"] boolValue];
                            if (_isAltSiteContactAvailable) {
                                _altSiteContact = [orderMilestoneData valueForKey:@"AltSiteContact"];
                            }
                        }
                    }
                }
                
                _arrayOfMilestoneNodes = arrayOfOrderMilestoneNodes;
            }
        }
        else
        {
            
            _orderDetailSummaryType = BTOrderDetailSummaryTypeUnknown;
            
            if((productSummary.productType == 1 ||
               productSummary.productType == 2 ||
               productSummary.productType == 5) &&
               !(pricingDetails.journeyType == 2 || !productDetails.phoneNumber)
               ){
                //Phone Number
                _orderDetailSummaryType = BTOrderDetailSummaryTypePhone;

            }
            else if(productSummary.productType == 3){
                
                //Broadband
                _orderDetailSummaryType = BTOrderDetailSummaryTypeBroadband;
            }
            else if (productSummary.productType == 7 && pricingDetails.journeyType != 2)
            {
                
                //ISDN
                _orderDetailSummaryType = BTOrderDetailSummaryTypeISDN;
            }
            
            
            /*
            if (productDetails.networkUserId != nil) {
                if ([productDetails.networkUserId isEqualToString:@""] || (productDetails.networkUserId.length < 5)) {
                    _isBrodbandDetailsAvailable = NO;
                } else {
                    _isBrodbandDetailsAvailable = YES;
                }
            } else if (productDetails.phoneNumber != nil) {

                if ([productDetails.phoneNumber isEqualToString:@""] || (productDetails.phoneNumber.length < 5)) {
                    _isPhoneDetailsAvailable = NO;
                } else {
                    _isPhoneDetailsAvailable = YES;
                }
            }
             */
        }

        [self.orderDetailsDelegate successfullyFetchedOrderSummaryDataOnOrderDetailsScreen:self];

        self.getOrderDetailsWebService.getOrderDetailsWebServiceDelegate = nil;
        self.getOrderDetailsWebService = nil;
    }
    else if(webService == _getOrderDetailsWithPostCodeWebService)
    {
        [self setProductSummary:productSummary];
        [self setBillingDetails:billingDetails];
        [self setPricingDetails:pricingDetails];
        [self setProductDetails:productDetails];
        [self setAuthLevel:authLevel];
        
        //_isMoverOrder = false;
        
        if ((billingDetails.billingAddress != nil) || (billingDetails.billFrequency != nil) || (billingDetails.billType))
        {
            _isBillingDetailsAvailable = YES;
        }
        
        if (authLevel != 1)
        {
            if (arrayOfOrderMilestoneData && arrayOfOrderMilestoneData.count > 0)
            {
                if (arrayOfOrderMilestoneData.count > 1)
                {
                    _isMoverOrder = YES;
                }
                
                NSMutableArray *arrayOfOrderMilestoneNodes = [NSMutableArray array];
                
                for (NSDictionary *orderMilestoneData in arrayOfOrderMilestoneData)
                {
                    if (orderMilestoneData != nil) {
                        
                        NSMutableArray *milestoneDetails = [NSMutableArray array];
                        
                        if ([orderMilestoneData valueForKey:@"MilestoneNodes"])
                        {
                            [milestoneDetails addObject:[orderMilestoneData valueForKey:@"MilestoneNodes"]];
                        }
                        
                        if ([orderMilestoneData valueForKey:@"DispatchDetails"])
                        {
                            [milestoneDetails addObject:[orderMilestoneData valueForKey:@"DispatchDetails"]];
                        }

                        
                        [arrayOfOrderMilestoneNodes addObject:milestoneDetails];
                        
                        if (!_isMoverOrder)
                        {
                            _isActivationDateAvailable = [[orderMilestoneData valueForKey:@"IsActivationDateAvailable"] boolValue];
                            if (_isActivationDateAvailable) {
                                _activationDate = [orderMilestoneData valueForKey:@"ActivationDate"];
                            }
                            _isEngineerAppointmentDateAvailable = [[orderMilestoneData valueForKey:@"IsEngineerAppointmentDateAvailable"] boolValue];
                            if (_isEngineerAppointmentDateAvailable) {
                                _engineerAppointmentDate = [orderMilestoneData valueForKey:@"EngineerAppointmentDate"];
                            }
                            _isSiteContactAvailable = [[orderMilestoneData valueForKey:@"IsSiteContactAvailable"] boolValue];
                            if (_isSiteContactAvailable) {
                                _siteContact = [orderMilestoneData valueForKey:@"SiteContact"];
                            }
                            _isAltSiteContactAvailable = [[orderMilestoneData valueForKey:@"IsAltSiteContactAvailable"] boolValue];
                            if (_isAltSiteContactAvailable) {
                                _altSiteContact = [orderMilestoneData valueForKey:@"AltSiteContact"];
                            }
                        }
                    }
                    
                }
                
                _arrayOfMilestoneNodes = arrayOfOrderMilestoneNodes;
            }
        }
        else
        {
            
            _orderDetailSummaryType = BTOrderDetailSummaryTypeUnknown;
            
            if((productSummary.productType == 1 ||
                productSummary.productType == 2 ||
                productSummary.productType == 5) &&
               !(pricingDetails.journeyType == 2 || !productDetails.phoneNumber)
               ){
                
                //Phone Number
                _orderDetailSummaryType = BTOrderDetailSummaryTypePhone;
            }
            else if(productSummary.productType == 3){
                
                //Broadband
                _orderDetailSummaryType = BTOrderDetailSummaryTypeBroadband;
            }
            else if (productSummary.productType == 7 && pricingDetails.journeyType != 2){
                
                //ISDN
                _orderDetailSummaryType = BTOrderDetailSummaryTypeISDN;
            }
            
            
            /*
            if (productDetails.networkUserId != nil) {
                if ([productDetails.networkUserId isEqualToString:@""] || (productDetails.networkUserId.length < 5)) {
                    _isBrodbandDetailsAvailable = NO;
                } else {
                    _isBrodbandDetailsAvailable = YES;
                }
            } else if (productDetails.phoneNumber != nil) {
                
                if ([productDetails.phoneNumber isEqualToString:@""] || (productDetails.phoneNumber.length < 5)) {
                    _isPhoneDetailsAvailable = NO;
                } else {
                    _isPhoneDetailsAvailable = YES;
                }
            }
             */
        }
        
        [self.orderDetailsDelegate successfullyFetchedOrderSummaryDataOnOrderDetailsScreen:self];
        
        self.getOrderDetailsWithPostCodeWebService.getOrderDetailsWebServiceDelegate = nil;
        self.getOrderDetailsWithPostCodeWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLOrderDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLOrderDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)getOrderDetailsWebService:(NLOrderDetailsWebService *)webService failedToFetchOrderDetailsDataWithWebServiceError:(NLWebServiceError *)error {

    if(webService == _getOrderDetailsWebService)
    {
        [self.orderDetailsDelegate trackorderDetailsScreen:self failedToFetchOrderDetailsWithWebServiceError:error];

        self.getOrderDetailsWebService.getOrderDetailsWebServiceDelegate = nil;
        self.getOrderDetailsWebService = nil;
    }
    else if(webService == _getOrderDetailsWithPostCodeWebService)
    {
        [self.orderDetailsDelegate trackorderDetailsScreen:self failedToFetchOrderDetailsWithWebServiceError:error];
        
        self.getOrderDetailsWithPostCodeWebService.getOrderDetailsWebServiceDelegate = nil;
        self.getOrderDetailsWithPostCodeWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLOrderDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLOrderDetailsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

#pragma mark - NLGetOrderMilestonesWebServiceDelegate Methods

- (void)getOrderMilestonesWebService:(NLGetOrderMilestonesWebService *)webService successfullyFetchedOrderMilestonesData:(NSArray *)arrayOfMilestoneData
{
    if(webService == _getOrderMilestonesWebService)
    {
        //_isMoverOrder = false;
        
        if (arrayOfMilestoneData && arrayOfMilestoneData.count > 0)
        {
            if (arrayOfMilestoneData.count > 1)
            {
                _isMoverOrder = YES;
            }
            
            
            NSMutableArray *arrayOfOrderMilestoneNodes = [NSMutableArray array];
            
            for (NSDictionary *orderMilestoneData in arrayOfMilestoneData)
            {
                if (orderMilestoneData != nil)
                {
                    NSMutableArray *milestoneDetails = [NSMutableArray array];
                    
                    if ([orderMilestoneData valueForKey:@"MilestoneNodes"])
                    {
                        [milestoneDetails addObject:[orderMilestoneData valueForKey:@"MilestoneNodes"]];
                        
                        _authLevel = [(BTOrderMilestoneNodeModel *)[[milestoneDetails objectAtIndex:0] objectAtIndex:0] authLevel];
                    }
                    
                    if ([orderMilestoneData valueForKey:@"DispatchDetails"])
                    {
                        [milestoneDetails addObject:[orderMilestoneData valueForKey:@"DispatchDetails"]];
                    }
                    
                    [arrayOfOrderMilestoneNodes addObject:milestoneDetails];
                    
                    if (!_isMoverOrder)
                    {
                        _isActivationDateAvailable = [[orderMilestoneData valueForKey:@"IsActivationDateAvailable"] boolValue];
                        if (_isActivationDateAvailable) {
                            _activationDate = [orderMilestoneData valueForKey:@"ActivationDate"];
                        }
                        _isEngineerAppointmentDateAvailable = [[orderMilestoneData valueForKey:@"IsEngineerAppointmentDateAvailable"] boolValue];
                        if (_isEngineerAppointmentDateAvailable) {
                            _engineerAppointmentDate = [orderMilestoneData valueForKey:@"EngineerAppointmentDate"];
                        }
                        _isSiteContactAvailable = [[orderMilestoneData valueForKey:@"IsSiteContactAvailable"] boolValue];
                        if (_isSiteContactAvailable) {
                            _siteContact = [orderMilestoneData valueForKey:@"SiteContact"];
                        }
                        _isAltSiteContactAvailable = [[orderMilestoneData valueForKey:@"IsAltSiteContactAvailable"] boolValue];
                        if (_isAltSiteContactAvailable) {
                            _altSiteContact = [orderMilestoneData valueForKey:@"AltSiteContact"];
                        }
                    }
                }
            }
            _arrayOfMilestoneNodes = arrayOfOrderMilestoneNodes;
        }
        else
        {
            _authLevel = 1;
            _arrayOfMilestoneNodes = nil;
        }

        [self.orderDetailsDelegate successfullyFetchedOrderMilestonesDataOnOrderDetailsScreen:self];

        self.getOrderMilestonesWebService.getOrderMilestonesWebServiceDelegate = nil;
        self.getOrderMilestonesWebService = nil;
    }
    else if(webService == _getOrderMilestonesWithPostCodeWebService)
    {
        //_isMoverOrder = false;
        
        if (arrayOfMilestoneData && arrayOfMilestoneData.count > 0)
        {
            if (arrayOfMilestoneData.count > 1)
            {
                _isMoverOrder = YES;
            }
            
            
            NSMutableArray *arrayOfOrderMilestoneNodes = [NSMutableArray array];
            
            for (NSDictionary *orderMilestoneData in arrayOfMilestoneData)
            {
                if (orderMilestoneData != nil)
                {
                    NSMutableArray *milestoneDetails = [NSMutableArray array];
                    
                    if ([orderMilestoneData valueForKey:@"MilestoneNodes"])
                    {
                        [milestoneDetails addObject:[orderMilestoneData valueForKey:@"MilestoneNodes"]];
                        
                        _authLevel = [(BTOrderMilestoneNodeModel *)[[milestoneDetails objectAtIndex:0] objectAtIndex:0] authLevel];
                    }
                    
                    if ([orderMilestoneData valueForKey:@"DispatchDetails"])
                    {
                        [milestoneDetails addObject:[orderMilestoneData valueForKey:@"DispatchDetails"]];
                    }
                    
                    [arrayOfOrderMilestoneNodes addObject:milestoneDetails];
                    
                    if (!_isMoverOrder)
                    {
                        _isActivationDateAvailable = [[orderMilestoneData valueForKey:@"IsActivationDateAvailable"] boolValue];
                        if (_isActivationDateAvailable) {
                            _activationDate = [orderMilestoneData valueForKey:@"ActivationDate"];
                        }
                        _isEngineerAppointmentDateAvailable = [[orderMilestoneData valueForKey:@"IsEngineerAppointmentDateAvailable"] boolValue];
                        if (_isEngineerAppointmentDateAvailable) {
                            _engineerAppointmentDate = [orderMilestoneData valueForKey:@"EngineerAppointmentDate"];
                        }
                        _isSiteContactAvailable = [[orderMilestoneData valueForKey:@"IsSiteContactAvailable"] boolValue];
                        if (_isSiteContactAvailable) {
                            _siteContact = [orderMilestoneData valueForKey:@"SiteContact"];
                        }
                        _isAltSiteContactAvailable = [[orderMilestoneData valueForKey:@"IsAltSiteContactAvailable"] boolValue];
                        if (_isAltSiteContactAvailable) {
                            _altSiteContact = [orderMilestoneData valueForKey:@"AltSiteContact"];
                        }
                    }
                }
            }
            _arrayOfMilestoneNodes = arrayOfOrderMilestoneNodes;
        }
        else
        {
            _authLevel = 1;
            _arrayOfMilestoneNodes = nil;
        }
        
        [self.orderDetailsDelegate successfullyFetchedOrderMilestonesDataOnOrderDetailsScreen:self];
        
        self.getOrderMilestonesWithPostCodeWebService.getOrderMilestonesWebServiceDelegate = nil;
        self.getOrderMilestonesWithPostCodeWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLGetOrderMilestonesWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLGetOrderMilestonesWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)getOrderMilestonesWebService:(NLGetOrderMilestonesWebService *)webService failedToFetchOrderMilestonesDataWithWebServiceError:(NLWebServiceError *)error
{
    if(webService == _getOrderMilestonesWebService)
    {
        [self.orderDetailsDelegate orderDetailsScreen:self failedToFetchOrderMilestonesDataWithWebServiceError:error];
        self.getOrderMilestonesWebService.getOrderMilestonesWebServiceDelegate = nil;
        self.getOrderMilestonesWebService = nil;

    }
    else if(webService == _getOrderMilestonesWithPostCodeWebService)
    {
        [self.orderDetailsDelegate orderDetailsScreen:self failedToFetchOrderMilestonesDataWithWebServiceError:error];
        self.getOrderMilestonesWithPostCodeWebService.getOrderMilestonesWebServiceDelegate = nil;
        self.getOrderMilestonesWithPostCodeWebService = nil;
        
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLGetOrderMilestonesWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLGetOrderMilestonesWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

#pragma mark - BTLiveChatAvailabilityCheckerDelegate Methods
- (void)liveChatAvailabilityChecker:(BTLiveChatAvailabilityChecker *)liveChatAvailabilityChecker successfullyFetchedSlotsWithGeneralCategory:(NSArray *)generalCategoryArray specificeCategory:(NSArray *)specificCategory andCurrentTime:(NSString *)currentTime
{
    BOOL chatAvailable = [BTLiveChatAvailabilityChecker checkAvailabilityWithSpecificCategory:specificCategory currentTime:currentTime andChatType:BTChatTypeOrder];
    
    if (chatAvailable)
    {
        [self.orderDetailsDelegate liveChatAvailableForOrderWithScreen:self];
    }
    else
    {
        [self.orderDetailsDelegate orderDetailsScreen:self liveChatCurrentlyNotAvailableWithSpecificCategory:specificCategory generalCategory:generalCategoryArray andCurrentTime:currentTime];
    }
    
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = nil;
}

- (void)liveChatAvailabilityChecker:(BTLiveChatAvailabilityChecker *)liveChatAvailabilityChecker failedToFetchSlotsWithWebServiceError:(NLWebServiceError *)webServiceError
{
    [self.orderDetailsDelegate orderDetailsScreen:self failedToCheckLiveChatAvailabilityWithWebServiceError:webServiceError];
    
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = nil;
}


//On Lakhpat's Request we are preserving for the future use for example manual updation
/*
 - (void)updateMilestoneDataWithAmendedData
 {
 [self updateMilestoneDataWithEngineerAppointmentDate:self.engineerAppointmentDate];
 [self updateMilestoneDataWithActivationDate:self.activationDate];

 }


 - (void)updateMilestoneDataWithEngineerAppointmentDate:(NSDate *)engineerAppointmentDate
 {
 for (BTOrderMilestoneNodeModel *nodeData in _arrayOfMilestoneNodes) {
 if ([nodeData.nodeDisplayName isEqualToString:@"Engineer appointment"]) {
 [nodeData updateNodeDataWithEngineerAppointmentDate:engineerAppointmentDate];
 }
 }
 }

 - (void)updateMilestoneDataWithActivationDate:(NSDate *)activationDate
 {
 for (BTOrderMilestoneNodeModel *nodeData in _arrayOfMilestoneNodes) {
 if ([nodeData.nodeDisplayName isEqualToString:@"Completion"]) {
 [nodeData updateNodeDataWithActivationDate:activationDate];
 }
 }
 }
 */

@end
