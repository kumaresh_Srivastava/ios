//
//  BTOrderDetailsMilestoneViewController.h
//  BTBusinessApp
//
//  Created by Accolite on 07/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger, TrackOrderDetailsType) {
    OrderSummaryTab   = 0,
    OrderTrackerTab  = 1
};

@class BTOrderDetailsMilestoneViewController;

@protocol BTOrderDetailsMilestoneViewControllerDelegate <NSObject>

- (void)btOrderDetailsMilestoneViewController:(BTOrderDetailsMilestoneViewController *)controller didChangeData:(BOOL)dataChangeStatus;

@end


@interface BTOrderDetailsMilestoneViewController : UIViewController
@property(nonatomic, assign) id<BTOrderDetailsMilestoneViewControllerDelegate>delegate;

@property (nonatomic) NSString *orderRef;
@property (nonatomic) NSString *itemRef;
@property (nonatomic) NSString *productName;
@property (nonatomic) NSString *primaryProductName; 
@property (nonatomic) NSString *productDescription;
@property (nonatomic) NSString *primaryProductDescription;
@property (nonatomic) NSString *productIcon;
@property (nonatomic) NSString *primaryProductIcon;
@property (nonatomic) BOOL networkRequestInProgress;
@property (nonatomic) NSString *orderStatus;
@property (nonatomic) NSString *primaryOrderStatus;
@property (nonatomic) BOOL isShowAppointment;
@property (nonatomic) BOOL isSIM2Order;
@property (nonatomic) NSString *itemBBRef;
@property (nonatomic) BOOL isSecondaryOrder;
@end
