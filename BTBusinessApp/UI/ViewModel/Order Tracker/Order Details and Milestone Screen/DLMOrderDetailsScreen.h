//
//  DLMOrderDetailsScreen.h
//  BTBusinessApp
//
//  Created by Accolite on 06/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
@class DLMOrderDetailsScreen;
@class BTOrderSiteContactModel;
@class BTProduct;
@class BTBillingDetails;
@class BTPricingDetails;
@class BTProductDetails;
@class BTOrderMilestoneNodeModel;
@class NLWebServiceError;


typedef NS_ENUM(NSInteger, BTOrderDetailSummaryType ) {
    
    BTOrderDetailSummaryTypeUnknown,
    BTOrderDetailSummaryTypePhone,
    BTOrderDetailSummaryTypeBroadband,
    BTOrderDetailSummaryTypeISDN,
};



@protocol DLMOrderDetailsScreenDelegate <NSObject>

- (void)successfullyFetchedOrderSummaryDataOnOrderDetailsScreen:(DLMOrderDetailsScreen *)orderDetailsScreen;

- (void)trackorderDetailsScreen:(DLMOrderDetailsScreen *)orderDetailsScreen failedToFetchOrderDetailsWithWebServiceError:(NLWebServiceError *)error;

- (void)successfullyFetchedOrderMilestonesDataOnOrderDetailsScreen:(DLMOrderDetailsScreen *)orderDetailsScreen;

- (void)orderDetailsScreen:(DLMOrderDetailsScreen *)orderDetailsScreen failedToFetchOrderMilestonesDataWithWebServiceError:(NLWebServiceError *)error;

- (void)liveChatAvailableForOrderWithScreen:(DLMOrderDetailsScreen *)orderDetailsScreen;
- (void)orderDetailsScreen:(DLMOrderDetailsScreen *)orderDetailsScreen liveChatCurrentlyNotAvailableWithSpecificCategory:(NSArray *)specificCategory generalCategory:(NSArray *)generalCategory andCurrentTime:(NSString *)currentTime;
- (void)orderDetailsScreen:(DLMOrderDetailsScreen *)orderDetailsScreen failedToCheckLiveChatAvailabilityWithWebServiceError:(NLWebServiceError *)error;

@end

@interface DLMOrderDetailsScreen : DLMObject

@property (nonatomic, copy) NSString *orderRef;
@property (nonatomic, copy) NSString *itemRef;
@property (nonatomic, copy) NSString *productName;
@property (nonatomic, copy) NSString *productDescription;
@property (nonatomic, copy) NSString *productIcon;
@property (nonatomic, copy) NSArray *arrayOfMilestoneNodes;
@property (nonatomic, strong) NSDate *activationDate;
@property (nonatomic, assign) BOOL isActivationDateAvailable;
@property (nonatomic, strong) NSDate *engineerAppointmentDate;
@property (nonatomic, assign) BOOL isEngineerAppointmentDateAvailable;
@property (nonatomic, copy) NSArray *dispatchDetails;
@property (nonatomic, strong) BTOrderSiteContactModel *siteContact;
@property (nonatomic, assign) BOOL isSiteContactAvailable;
@property (nonatomic, strong) BTOrderSiteContactModel *altSiteContact;
@property (nonatomic, assign) BOOL isAltSiteContactAvailable;
@property (nonatomic, copy) NSString *postCode;
@property (nonatomic, weak) id<DLMOrderDetailsScreenDelegate> orderDetailsDelegate;

@property (nonatomic, strong) BTProduct *productSummary;
@property (nonatomic, strong) BTBillingDetails *billingDetails;
@property (nonatomic, strong) BTPricingDetails *pricingDetails;
@property (nonatomic, strong) BTProductDetails *productDetails;
@property (nonatomic, assign) NSInteger authLevel;
@property (nonatomic, assign) BOOL isSIMOrder;

//@property (nonatomic, assign) BOOL isBrodbandDetailsAvailable;
//@property (nonatomic, assign) BOOL isPhoneDetailsAvailable;
@property (nonatomic, assign) BOOL isBillingDetailsAvailable;
@property (nonatomic, readonly) BTOrderDetailSummaryType orderDetailSummaryType;

@property (nonatomic, assign) BOOL isMoverOrder;

- (void)fetchOrderDetailsForOrderReference:(NSString *)orderRef andItemRef:(NSString *)itemRef;
- (void)fetchCompleteOrderDetailsForOrderReference:(NSString *)orderRef ItemRef:(NSString *)itemRef postCode:(NSString *)postCode andErrorCount:(NSInteger)errorCount;
- (void) cancelCurrentRequest;
- (void)cancelChatAvailabilityCheckerAPI;

// Get Order Milestones Methods
- (void)fetchOrderMilestonesForOrderReference:(NSString *)orderRef andItemRef:(NSString *)itemRef;
- (void)fetchOrderMilestonesForOrderReference:(NSString *)orderRef ItemRef:(NSString *)itemRef postCode:(NSString *)postCode andErrorCount:(NSInteger)errorCount;
- (NSString *)getImageNameForProductIcon:(NSString *)productIcon;
- (void)checkForLiveChatAvailibilityForOrder;

//On Lakhpat's Request we are preserving for the future use for example manual updation
/*- (void)updateMilestoneDataWithAmendedData;
- (void)updateMilestoneDataWithEngineerAppointmentDate:(NSDate *)engineerAppointmentDate;
- (void)updateMilestoneDataWithActivationDate:(NSDate *)activationDate;
*/

@end
