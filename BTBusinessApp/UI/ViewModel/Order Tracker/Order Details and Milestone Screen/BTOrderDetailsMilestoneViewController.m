

//
//  BTOrderDetailsMilestoneViewController.m
//  BTBusinessApp
//
//  Created by Accolite on 07/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderDetailsMilestoneViewController.h"
#import "DLMOrderDetailsScreen.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "BTPricingDetails.h"
#import <EventKit/EventKit.h>
#import "BTProduct.h"
#import "BTOrderMilestoneNodeView.h"
#import "BTOrderSummaryView.h"
#import "BTOrderMilestoneNodeModel.h"
#import "AppConstants.h"
#import "CustomSpinnerView.h"
#import "BTOrderDetailsCompletedActionView.h"
#import "BTOrderDetailsTakeActionView.h"
#import "BTViewFullDetailsView.h"
#import "AppManager.h"
#import "BTOrderDetailsPriceListViewController.h"
#import "BTProduct.h"
#import "BTRetryView.h"
#import "BTEmptyOrderSummaryAndMilestoneView.h"
#import "BTOrderAmendViewController.h"
#import "BTAlertBannerView.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "CDEngineerAppointmentForOrder.h"
#import "BTCustomInputAlertView.h"
#import "BTChatViewController.h"
#import "CustomSpinnerAnimationView.h"
#import "OmnitureManager.h"
#import "NLWebServiceError.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "BTHelpAndSupportViewController.h"
#import "BTInAppBrowserViewController.h"
#import "BTNavigationViewController.h"
#import "BTOrderStatusLabel.h"

@interface BTOrderDetailsMilestoneViewController () <DLMOrderDetailsScreenDelegate, BTOrderSummaryViewDelegate, BTViewFullDetailsViewDelegate , BTRetryViewDelegate, BTOrderDetailsTakeActionViewDelegate, BTOrderMilestoneNodeViewDelegate, BTOrderAmendViewControllerDelegate,BTCustomInputAlertViewDelegate, BTOrderDetailsCompletedActionViewDelegate, BTEmptyOrderSummaryAndMilestoneViewDelegate> {
    UIView *_milestoneView;
    UIView *_summaryView;
    UIView *_bottomView;
    UIView *_trackerBottomView;
    BTOrderDetailsCompletedActionView *_completedActionView;
    BTViewFullDetailsView *_viewFullDetailsView;
    BTOrderDetailsTakeActionView *_takeActionView;
    NSString *_postCode;
    BOOL _isCompletedOrdersFetched, _isCompleteMilestonesFetched;
    BTOrderSummaryView *_orderSummaryView;
    BTEmptyOrderSummaryAndMilestoneView *_emptyOrderSummaryAndMilestoneView;
    BTRetryView *_retryView;
    BOOL _isPostalCodeRequired;
    BOOL _isRetryShown;
    BOOL _isSingleMileStoneLoadingView, _isSingleDetailsLoadingView;
    BOOL _isfetchingOrderDetailsAPI, _isfetchingMileStoneDetailsAPI, _isfetchingMilestoneFullDetailsAPI, _isfetchingfullOrderDetailsAPI, _isfetchFullOrderDetailsWithPostcodeOverlay;
    BOOL _isorderDetailsFail, _ismilestoneDetailsFail;
    
    BTAlertBannerView *_topAlertBanner;
    CGFloat _originalMainScrollTopConstraintValue;
    CGFloat _originalTakeActionViewHeight;
    
    BTCustomInputAlertView *_customlAlertView;
    CustomSpinnerAnimationView *_loaderView;
    
    BOOL _isFetchingFullDetails;//(RM)
    NSString *_pageNameForOmniture; //RM
    BOOL _addAppointmentToCalenderIsInProgress;
    BOOL issecondaryOrderProccesed;
    NSInteger primaryOrderApiCount;
    
    
}

@property (nonatomic, readwrite) DLMOrderDetailsScreen *orderDetailsViewModel;
@property (nonatomic, readwrite) DLMOrderDetailsScreen *primaryOrderDetailsViewModel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintPostCodeViewHeight;

@property (weak, nonatomic) IBOutlet UIImageView *orderImageView;
@property (weak, nonatomic) IBOutlet UIView *postCodeView;
@property (weak, nonatomic) IBOutlet UIButton *postCodeButton;
@property (weak, nonatomic) IBOutlet UIScrollView *mainContainerScrollView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *orderDetailsSegmentedControl;
@property (strong, nonatomic) IBOutlet UIView *segmentContainerView;
@property (weak, nonatomic) IBOutlet UILabel *orderReferenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderDescriptionLabel;
@property (weak, nonatomic) IBOutlet BTOrderStatusLabel *statusLabel;

@property (nonatomic, assign) NSInteger selectedTab;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainContainerScrollViewTopConstraint;
@property (assign, nonatomic) BOOL isDataChanged;
@property (strong, nonatomic) CustomSpinnerView *loadingView;


@end

@implementation BTOrderDetailsMilestoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _pageNameForOmniture = OMNIPAGE_ORDER_DETAILS;//RLM Set page name
    
    // (VRK) Setup viewModel for home screen
    self.orderDetailsViewModel = [[DLMOrderDetailsScreen alloc] init];
    self.orderDetailsViewModel.orderDetailsDelegate = self;
    self.orderDetailsViewModel.productName = self.productName;
    issecondaryOrderProccesed = NO;
    primaryOrderApiCount = 0;
    self.primaryOrderDetailsViewModel = nil;
    // (lp) Navigation bar back button
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    // (LP) Display loading view on the basis of network request.
    __weak typeof(self) weakSelf = self;
    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([weakSelf networkRequestInProgress]) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
            [weakSelf.loadingView startAnimatingLoadingIndicatorView];
        }
        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
            [weakSelf.loadingView stopAnimatingLoadingIndicatorView];
        }
    }];
    
    // (LP) Initial estup of views
    _milestoneView = [[UIView alloc] init];
    _summaryView = [[UIView alloc] init];
    _bottomView = nil;
    _trackerBottomView = nil;
    _postCode = @"";
    _isPostalCodeRequired = NO;
    
    // (LP) Default selection of tab
    [self setSelectedTab:OrderSummaryTab];
    [self initialUISetupForOrderDetails];
    
    
    _isRetryShown = NO;
    _isCompletedOrdersFetched = NO;
    _isCompleteMilestonesFetched = NO;
    _isSingleMileStoneLoadingView = NO;
    _isSingleDetailsLoadingView = NO;
    
    _isfetchingOrderDetailsAPI = NO;
    _isfetchingMileStoneDetailsAPI = NO;
    _isfetchingMilestoneFullDetailsAPI = NO;
    _isfetchingfullOrderDetailsAPI = NO;
    _isfetchFullOrderDetailsWithPostcodeOverlay = NO;
    _isorderDetailsFail = NO;
    _ismilestoneDetailsFail = NO;
    [self fetchOrderDetailsFromAPI];
    //    [_loadingView startAnimatingLoadingIndicatorView];
    
    //[_orderDetailsSegmentedControl setTintColor:[BrandColours colourBackgroundBTPurplePrimaryColor]];
    //[_orderDetailsSegmentedControl setTitleTextAttributes:[[AppManager sharedAppManager] fetchSegmentedControlFontDict] forState:UIControlStateNormal];
    
    _originalMainScrollTopConstraintValue = self.mainContainerScrollViewTopConstraint.constant;
    
    [self trackOmniPage:OMNIPAGE_ORDER_DETAILS];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChangeOrderDetailMilestone:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
        if (self.selectedTab == OrderSummaryTab && (_isCompletedOrdersFetched == YES || self.postCodeView.isHidden == NO)) {//KUMARESH
            
            [self updateAndShowOrderSummaryView];
        }
    }
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [_customlAlertView stopLoading];
    [_customlAlertView deRegisterKeyboardNotification];
    [_customlAlertView removeFromSuperview];
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
}


- (void)didMoveToParentViewController:(UIViewController *)parent{
    // To Fix #Crash Live build 2.0.2
    /*
     libobjc.A.dylib  objc_retain
     BTBusinessApp -[BTOrderDetailsMilestoneViewController didMoveToParentViewController:] BTOrderDetailsMilestoneViewController.m:200
     */
    if(parent){
        [self.delegate btOrderDetailsMilestoneViewController:self didChangeData:_isDataChanged];
    }
}


#pragma mark - Fetching Details

- (void)statusBarOrientationChangeOrderDetailMilestone:(NSNotification *)notification {
    // handle the interface orientation as needed
   // [self createLoadingView];
    [self performSelector:@selector(createLoadingView) withObject:nil afterDelay:0.1];
     if (self.selectedTab == OrderSummaryTab && (_isCompletedOrdersFetched == YES || self.postCodeView.isHidden == NO)) {//KUMARESH
        
        [self updateAndShowOrderSummaryView];
    }
}

- (void)createLoadingView {
    
    if(_customlAlertView) {
        _customlAlertView.frame = CGRectMake(0,64, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-64);
        [_customlAlertView startLoading];
        return;
    }
    
    if(_loadingView.isHidden){
        return;
    }
    if(_loadingView){
        [_loadingView removeFromSuperview];
        _loadingView = nil;
        
    }
    _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
    _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:_loadingView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
   
}


- (void)hideLoadingItems:(BOOL)isHide {
    
    // (lp) Hide or Show UI elements related to loading.
    [_loadingView setHidden:isHide];
}


// (VRK) fetching the order details
- (void)fetchOrderDetailsFromAPI {
    _isfetchingOrderDetailsAPI = YES;
    if([AppManager isInternetConnectionAvailable]) {
        [self hideLoadingItems:NO];
        [self.orderDetailsViewModel fetchOrderDetailsForOrderReference:self.orderRef andItemRef:self.itemRef];
        self.networkRequestInProgress = YES;
        
    } else {
        [self hideLoadingItems:YES];
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:_pageNameForOmniture];
        //self.orderDetailsSegmentedControl.enabled = NO;
        
    }
}

// (VRK) fetching the complete order details with postcode
- (void)fetchCompleteOrderDetailsData {
    if([AppManager isInternetConnectionAvailable]) {
        [self.orderDetailsViewModel fetchCompleteOrderDetailsForOrderReference:self.orderRef ItemRef:self.itemRef postCode:_postCode andErrorCount:0];
        
        self.networkRequestInProgress = YES;
        //[self hideLoadingItems:NO];
        //[_loadingView startAnimatingLoadingIndicatorView];
    } else {
        [self hideLoaderView];
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:_pageNameForOmniture];
        
    }
}

- (void)fetchOrderMilestonesData {
    if([AppManager isInternetConnectionAvailable])
    {
        // (LP) Ask to viewModel to provide order summary details. The viewModel takes care of fetching data.
        [self.orderDetailsViewModel fetchOrderMilestonesForOrderReference:self.orderRef andItemRef:self.itemRef];
        self.networkRequestInProgress = YES;
        [_takeActionView startLoaderView];
        _bottomView = _takeActionView;
        
        [self updateVisibilityOfEngineerAppointmentDetailsButton];
    }
    else
    {
        [self hideLoaderView];
        [_takeActionView stopLoaderView];
        _bottomView = _takeActionView;
        
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:_pageNameForOmniture];
        
    }
    
}

- (void)fetchCompleteOrderMilestonesData {
    if([AppManager isInternetConnectionAvailable])
    {
        // (LP) Ask to viewModel to provide order summary details. The viewModel takes care of fetching data.
        [self.orderDetailsViewModel fetchOrderMilestonesForOrderReference:self.orderRef ItemRef:self.itemRef postCode:_postCode andErrorCount:0];
        self.networkRequestInProgress = YES;
        [_takeActionView startLoaderView];
        
        [self updateVisibilityOfEngineerAppointmentDetailsButton];
    }
    else
    {
        [self hideLoaderView];
        [_takeActionView stopLoaderView];
        
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:_pageNameForOmniture];
        
    }
}

- (void)fetchDataWithPostalCode:(NSString *)postalCode
{
    _customlAlertView.frame = CGRectMake(0,64, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-64);
    [_customlAlertView startLoading];
    
    _postCode = postalCode;
    _isPostalCodeRequired = YES;
    
    if (self.orderDetailsViewModel.pricingDetails.journeyType == 2)
    {
        //(lp) Fetch only full order details for ceased order
        _isCompleteMilestonesFetched = YES;
        
        _isfetchFullOrderDetailsWithPostcodeOverlay = YES;
        _isFetchingFullDetails = YES;
        [self fetchCompleteOrderDetailsData];
    }
    else
    {
        if (self.selectedTab == 0)
        {
            _isfetchFullOrderDetailsWithPostcodeOverlay = YES;
            _isFetchingFullDetails = YES;
            [self fetchCompleteOrderDetailsData];
            
        } else if (self.selectedTab == 1)
        {
            _isfetchingMilestoneFullDetailsAPI = YES;
            [self fetchCompleteOrderMilestonesData];
        }
    }
}

#pragma mark Action Methods

- (IBAction)orderDetailsSegmentAction:(id)sender {
    
    long selectedSegment = (long)[self.orderDetailsSegmentedControl selectedSegmentIndex];
    switch (selectedSegment) {
        case OrderSummaryTab:// For summary tab
            [self setSelectedTab:OrderSummaryTab];
            
            [self trackOmniPage:OMNIPAGE_ORDER_DETAILS];
            _pageNameForOmniture = OMNIPAGE_ORDER_DETAILS;
            
            if (_isorderDetailsFail && _isRetryShown) {
                [_milestoneView removeFromSuperview];
                [_orderSummaryView removeFromSuperview];
                [_summaryView removeFromSuperview];
                [_bottomView removeFromSuperview];
                
                if ([AppManager isInternetConnectionAvailable])
                {
                    [self showRetryViewWithInternetStrip:NO];
                    [AppManager trackNoInternetErrorOnPage:_pageNameForOmniture];
                }
                else
                {
                    [self showRetryViewWithInternetStrip:YES];
                }
                
                return;
            }
            if (_ismilestoneDetailsFail && _isRetryShown) {
                [self hideRetryItems:YES];
                //(VRK) following is reenabled because if we click on milestone we have to reshow the retry view
                _isRetryShown = YES;
            }
            if (!((self.orderDetailsViewModel.productSummary == nil) || (self.orderDetailsViewModel.productSummary.orderKey == nil))) {
                [self updateAndShowOrderSummaryView];
            }
            
            if (_isSingleDetailsLoadingView) {
                
                [self showLoaderView];
                
            }
            if (_isSingleMileStoneLoadingView) {
                
                [self hideLoaderView];
                
            }
            
            break;
        case OrderTrackerTab:// for Order tracker tab
            [self setSelectedTab:OrderTrackerTab];
            
            [self trackOmniPage:OMNIPAGE_ORDER_DETAILS_TRACKER];
            _pageNameForOmniture = OMNIPAGE_ORDER_DETAILS_TRACKER;
            
            if (_ismilestoneDetailsFail && _isRetryShown) {
                [_summaryView removeFromSuperview];
                [[_milestoneView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
                [_bottomView removeFromSuperview];
                [_milestoneView removeFromSuperview];
                
                if ([AppManager isInternetConnectionAvailable]) {
                    [self showRetryViewWithInternetStrip:NO];
                    [AppManager trackNoInternetErrorOnPage:_pageNameForOmniture];
                }
                else
                {
                    [self showRetryViewWithInternetStrip:YES];
                }
                
                
                return;
            }
            if (_isorderDetailsFail && _isRetryShown) {
                [self hideRetryItems:YES];
                //(VRK) following is reenabled because if we click on milestone we have to reshow the retry view
                _isRetryShown = YES;
            }
            [self updateAndShowOrderMilestoneView];
            if (_isSingleDetailsLoadingView) {
                
                [self hideLoaderView];
            }
            if (_isSingleMileStoneLoadingView) {
                
                [self showLoaderView];
            }
            break;
    }
}

- (void)userClickedOnViewFullDetailsButtonOfViewFullDetailsView:(BTViewFullDetailsView *)viewFullDetailsView
{
    
    [self trackOmniClick:OMNICLICK_ORDER_VIEW_FULL_DETAILS forPage:_pageNameForOmniture];
    [self displayUIToEnterPostCodeForFullDetailsWithErrorMessageHidden:YES];
}

- (IBAction)postCodeValidationButtonActionMethod:(id)sender {
    
    [self displayUIToEnterPostCodeForFullDetailsWithErrorMessageHidden:YES];
}

- (void)doValidationOnPostCode:(NSString *)postCode
{
    NSString *postalCodePatternWithSpace = @"^[A-Z][A-Z0-9]{0,3}[ ][0-9][A-Z]{2}$";
    NSString *postalCodePatternWithoutSpace = @"^[A-Z][A-Z0-9]{0,3}[0-9][A-Z]{2}$";
    
    NSRange range1 = [postCode rangeOfString:postalCodePatternWithSpace options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive];
    NSRange range2 = [postCode rangeOfString:postalCodePatternWithoutSpace options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive];
    if ((range1.location != NSNotFound && range1.location == 0) || (range2.location != NSNotFound && range2.location == 0) ) {
        DDLogInfo(@"Postcode is valid!");
        
        if(![AppManager isInternetConnectionAvailable])
        {
            [_customlAlertView showErrorOnCustomInputWithWithErrro:kNoConnectionMessage];
            return;
        }
        
        
        [_customlAlertView removeErrorMeesageFromCustomInputAlertView];
        NSString *postCodeString = [postCode stringByReplacingOccurrencesOfString:@" " withString:@""];
        [self fetchDataWithPostalCode:postCodeString];
        
    } else {
        DDLogInfo(@"Postcode is invalid!");
        //[self displayUIToEnterPostCodeForFullDetailsWithErrorMessageHidden:NO];
        [_customlAlertView showErrorOnCustomInputWithWithErrro:@"Invalid postcode"];
    }
    
}

- (void)redirectToAmmendOrderScene {
    
    if(self.isSIM2Order == YES && self.isSecondaryOrder == YES && issecondaryOrderProccesed == NO){
         issecondaryOrderProccesed = YES;
        _isfetchingOrderDetailsAPI = YES;
        if([AppManager isInternetConnectionAvailable]) {
           // self.orderDetailsViewModel = nil;
            self.primaryOrderDetailsViewModel = [[DLMOrderDetailsScreen alloc] init];
            self.primaryOrderDetailsViewModel.orderDetailsDelegate = self;
           
            [self hideLoadingItems:NO];
            primaryOrderApiCount = 1;
            [self.primaryOrderDetailsViewModel fetchOrderDetailsForOrderReference:self.orderRef andItemRef:self.itemBBRef];
            self.networkRequestInProgress = YES;
        } else {
            [self hideLoadingItems:YES];
            [self showRetryViewWithInternetStrip:YES];
            [AppManager trackNoInternetErrorOnPage:_pageNameForOmniture];            
        }
        return;
        
    }
    //[SD]: If order is legacy
    if(!_orderDetailsViewModel.productSummary.isRelatedOrderStrategic)
    {
        [self checkChatAvailabilityAction];
        
    }
    else
    {
        
        [self trackOmniClick:OMNICLICK_ORDER_CHANGE_COMPLETION_DATE forPage:_pageNameForOmniture];
        
        BTOrderAmendViewController *amendViewController = [BTOrderAmendViewController getBTOrderAmendViewController];
        if(self.primaryOrderDetailsViewModel != nil){
             amendViewController.oderDetailModel = self.primaryOrderDetailsViewModel;
        } else {
            amendViewController.oderDetailModel = self.orderDetailsViewModel;
        }
        amendViewController.orderReference = self.orderRef;
        amendViewController.itemReference = self.itemRef;
        amendViewController.productName = self.orderDetailsViewModel.productSummary.productName;
        amendViewController.postalCode = _postCode;
        amendViewController.orderAmendDelegate = self;
        amendViewController.isShowAppointment = self.isShowAppointment;
        amendViewController.isSIM2Order = self.isSIM2Order;
        amendViewController.itemBBReferenceSIM2 = self.itemBBRef;
        amendViewController.isSecondaryOrder = self.isSecondaryOrder;
        [self.navigationController pushViewController:amendViewController animated:YES];
    }
}



//(Salman)
- (void)createCancelFaultBannerView{
    
    
    if(!_topAlertBanner){
        
        _topAlertBanner = [[[NSBundle mainBundle] loadNibNamed:@"BTAlertBannerView" owner:self options:nil] firstObject];
        
        _topAlertBanner.frame = CGRectMake(0,
                                           self.mainContainerScrollView.frame.origin.y,
                                           self.mainContainerScrollView.frame.size.width,
                                           40);
        [self.view addSubview:_topAlertBanner];
    }
}

//Salman
- (void)showAlertBannerWithMessage:(NSString *)message{
    
    [self createCancelFaultBannerView];
    
    [_topAlertBanner setMessage:message];
    
    [_topAlertBanner showAnimated:YES];
    
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:kBannerAnimationTime animations:^{
        
        self.mainContainerScrollViewTopConstraint.constant = self->_topAlertBanner.frame.size.height;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        [self performSelector:@selector(hideAlertBanner) withObject:nil afterDelay:3.0];
    }];
    
}

//Salman
- (void)hideAlertBanner{
    
    [_topAlertBanner hideAnimated:YES];
    
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:kBannerAnimationTime animations:^{
        
        self.mainContainerScrollViewTopConstraint.constant = self->_originalMainScrollTopConstraintValue;
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
    }];
    
}

- (void)createLoaderImageView
{
    _loaderView = [[CustomSpinnerAnimationView alloc] initWithText:@"" loadingImage:[UIImage imageNamed:@"Spinner"] andLoaderImageSize:40.0f];
    
    [self.mainContainerScrollView addSubview:_loaderView];
    
    [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.mainContainerScrollView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
//    [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.orderDetailsSegmentedControl attribute:NSLayoutAttributeBottom multiplier:1.0f constant:15.0f]];
    [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.segmentContainerView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:15.0f]];
    [_loaderView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:100]];
    [_loaderView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:155]];
    _loaderView.translatesAutoresizingMaskIntoConstraints = NO;
    
}

- (void)showLoaderView
{
    [self createLoaderImageView];
    
    _loaderView.hidden = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->_loaderView startAnimation];
    });
}

- (void)hideLoaderView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->_loaderView stopAnimation];
    });
    
    _loaderView.hidden = YES;
    
    [_loaderView removeFromSuperview];
    _loaderView = nil;
    
}


#pragma mark - BTOrderMilestoneNodeViewDelagte methods
- (void)userPressedChangeButtonOnOrderMilestoneNodeView:(BTOrderMilestoneNodeView *)orderMilestoneNodeView
{
    [self redirectToAmmendOrderScene];
    
    [self trackOmniClick:OMNICLICK_ORDER_CHANGE_ENGG_APPOINTMENT forPage:_pageNameForOmniture];
    [self trackOmniPage:OMNIPAGE_ORDER_CHANGE_ORDER_DETAIL];
    
}

- (void)orderMilestoneNodeView:(BTOrderMilestoneNodeView *)orderMilestoneNodeView userPressedTrackDeliveryButtonWithTrackReference:(NSString *)trackingReference andTrackingURL:(NSString *)trackingUrl
{
    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
    
    viewController.urlToBeLoaded = trackingUrl;
    viewController.targetURLType = BTTargetURLTypeTrackParcel;
    
    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:YES andIsNeedCloseButton:NO];
    
    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{
        
    }];
}

#pragma mark BTOrderSummarydelegateActionMethods

- (void)userPressedDetailsForEngineerAppointmentOfOrderSummaryView:(BTOrderSummaryView *)orderSummaryView {
    
    [self trackOmniPage:OMNIPAGE_ORDER_CHANGE_ORDER_DETAIL];
    [self trackOmniClick:OMNICLICK_ORDER_CHANGE_ENGG_APPOINTMENT_DETAILS forPage:OMNIPAGE_ORDER_BILLING_DETAILS];
    
    [self redirectToAmmendOrderScene];
}

- (void)userPressedDetailsForBillingDetailsOfOrderSummaryView:(BTOrderSummaryView *)orderSummaryView {
    
    [self trackOmniClick:OMNICLICK_ORDER_BILLING_DETAILS forPage:OMNIPAGE_ORDER_BILLING_DETAILS];
    [self trackOmniPage:OMNIPAGE_ORDER_BILLING_DETAILS];
    
    if (_isSingleDetailsLoadingView || _isSingleMileStoneLoadingView) {
        
        _isSingleMileStoneLoadingView = NO;
        _isSingleDetailsLoadingView = NO;
        
        [self hideLoaderView];
    }
    
    BTOrderDetailsPriceListViewController *priceListViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PricingDetailsScene"];
    [priceListViewController setOrderID:self.orderRef];
    [priceListViewController setIsRelatedOrderStrategic:_orderDetailsViewModel.productSummary.isRelatedOrderStrategic];
    [priceListViewController setPricingDetails:self.orderDetailsViewModel.pricingDetails];
    [priceListViewController setBillingDetails:self.orderDetailsViewModel.billingDetails];
    
    
    [self.navigationController pushViewController:priceListViewController animated:YES];
    
}

#pragma mark - Calendar Methods

// (lpm) On Main thread
- (CDEngineerAppointmentForOrder *)getEngineerAppointmentDataForCurrentOrderFromPersistentStore {
    // Fetch Persistence Object For Order to Local Calender Event ID
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"CDEngineerAppointmentForOrder" inManagedObjectContext:[[AppDelegate sharedInstance] managedObjectContext]]];
    
    NSPredicate *orderRefPredicate = [NSPredicate predicateWithFormat:@"itemRef = %@", self.itemRef];
    [fetchRequest setPredicate:orderRefPredicate];
    
    NSError *__autoreleasing fetchError = nil;
    NSArray *resultArray = [[[AppDelegate sharedInstance] managedObjectContext] executeFetchRequest:fetchRequest error:&fetchError];
    if (resultArray.count > 0) {
        
        CDEngineerAppointmentForOrder *appointmentOrder = [resultArray objectAtIndex:0];
        return appointmentOrder;
    }
    return nil;
}

// (lpm) On Main thread
- (void)deleteEngineerAppointmentForCurrentOrder:(CDEngineerAppointmentForOrder *)engineerAppointmentForOrder
{
    // (lpm) Delelte the current appointment for order.
    CDEngineerAppointmentForOrder *previousAppointment = engineerAppointmentForOrder;
    engineerAppointmentForOrder = nil;
    if(previousAppointment)
    {
        [[AppDelegate sharedInstance].managedObjectContext deleteObject:previousAppointment];
    }
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
}


- (BOOL)checkForAlreadyAddedAppointmentAndDeleteForCurrentOrder
{
    BOOL isAlreadyAdded = NO;
    
    CDEngineerAppointmentForOrder *engineerAppointmentData = [self getEngineerAppointmentDataForCurrentOrderFromPersistentStore];
    
    NSString *calendarEventIdentifier = engineerAppointmentData.localCalenderEventID;
    
    if (calendarEventIdentifier != nil)
    {
       // isAlreadyAdded = YES;
        
        EKEventStore *store = [[EKEventStore alloc] init];
        EKEvent *eventToRemove = [store eventWithIdentifier:calendarEventIdentifier];
        
        NSError *errorInRemove = nil;
        
        if (eventToRemove != nil)
        {
            [store removeEvent:eventToRemove span:EKSpanThisEvent error:&errorInRemove];
        }
        
        if (errorInRemove)
        {
            isAlreadyAdded = YES;
            DDLogError(@"Error in removing current event from calender");
        }
        else
        {
            isAlreadyAdded = NO;
            
            if ([NSThread isMainThread])
            {
                [self deleteEngineerAppointmentForCurrentOrder:engineerAppointmentData];
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self deleteEngineerAppointmentForCurrentOrder:engineerAppointmentData];
                });
            }
        }
    }
    
    return isAlreadyAdded;
}

#pragma mark - BTOrderDetailsCompletedActionViewDelegate Methods

- (void)userClickedOnContactUsButtonOfOrderDetailsCompletedActionView:(BTOrderDetailsCompletedActionView *)orderDetailsCompletedActionView
{
    [self trackOmniClick:OMNICLICK_ORDER_NEED_HELP forPage:_pageNameForOmniture];
    
    [self checkChatAvailabilityAction];
}

#pragma mark TakeActionDelegateActionMethods

- (void)userPressedAmendOrderDetailsOfTakeActionView:(BTOrderDetailsTakeActionView *)takeActionView {
    
    [self trackOmniPage:OMNIPAGE_ORDER_CHANGE_ORDER_DETAIL];
    [self trackOmniClick:OMNICLICK_ORDER_CHANGE_ORDER_DETAILS forPage:OMNIPAGE_ORDER_CHANGE_ORDER_DETAIL];
    [self redirectToAmmendOrderScene];
}

- (void)userPressedAddAppointmentToCalenderOfTakeActionView:(BTOrderDetailsTakeActionView *)takeActionView {
    
    if (_addAppointmentToCalenderIsInProgress) {
        return;
    }
    if(self.isShowAppointment == NO){
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Add appointment" message:@"Appointment date will be updated shortly." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:^{
            
            
        }];
        return;
    }
    _addAppointmentToCalenderIsInProgress = YES;
    
    [self trackOmniClick:OMNICLICK_ORDER_ADD_APPT_CALENDAR forPage:_pageNameForOmniture];
    
    BOOL isAlreadyAddedAndNotAbleToDelete = [self checkForAlreadyAddedAppointmentAndDeleteForCurrentOrder];
    
    if (isAlreadyAddedAndNotAbleToDelete)
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Message" message:kDefaultErrorMessage preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:^{
            
            _addAppointmentToCalenderIsInProgress = NO;
        }];
        
    }
    else
    {
        EKEventStore *store = [[EKEventStore alloc] init];
        
        __weak typeof(self) weakSelf = self;
        [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            if (!granted) {
                DDLogInfo(@"INFO:User declined calendar access.");
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Add appointment" message:@"Access to Calendar is disabled, please enable in the settings." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [alertController dismissViewControllerAnimated:YES completion:nil];
                }];
                [alertController addAction:okAction];
                [weakSelf presentViewController:alertController animated:YES completion:^{
                    
                    self->_addAppointmentToCalenderIsInProgress = NO;
                }];
                
            }
            
            EKEvent *event = [EKEvent eventWithEventStore:store];
            event.title = @"BT engineer appointment";
            event.startDate = self.orderDetailsViewModel.engineerAppointmentDate;
            
            
            //(VRK) calculating the end time from the start time based on the appointment actual slots
            NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:self.orderDetailsViewModel.engineerAppointmentDate];
            
            long startHour = components.hour;
            long endTimeInterval;
            if (startHour < 13) {
                endTimeInterval = 60*60*(13 - startHour);
            } else {
                
                if (startHour < 18)
                {
                    endTimeInterval = 60*60*(18 - startHour);
                }
                else
                {
                    endTimeInterval = 60*60*(24 - startHour);
                }
            }
            
            event.endDate = [event.startDate dateByAddingTimeInterval:endTimeInterval];  // Duration 1 hr
            event.alarms = [NSArray arrayWithObject:[EKAlarm alarmWithRelativeOffset:(-(60*60)*48)]];
            NSString *eventMessage = [NSString stringWithFormat:@"BT Engineer Appointment for %@", self.orderDetailsViewModel.productSummary.productName];
            event.notes = eventMessage;
            [event setCalendar:[store defaultCalendarForNewEvents]];
            NSError *errorInSaving = nil;
            [store saveEvent:event span:EKSpanThisEvent commit:YES error:&errorInSaving];
            
            if(errorInSaving)
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Message" message:kDefaultErrorMessage preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [alertController dismissViewControllerAnimated:YES completion:nil];
                }];
                [alertController addAction:okAction];
                [weakSelf presentViewController:alertController animated:YES completion:^{
                    
                    self->_addAppointmentToCalenderIsInProgress = NO;
                    
                }];
                
                return;
            }
            else
            {
                NSString* eventIdentifier = [[NSString alloc] initWithFormat:@"%@", event.eventIdentifier];
                
                CDEngineerAppointmentForOrder *newEngineerAppointmentForOrder = [CDEngineerAppointmentForOrder newEngineerAppointmentForOrderInManagedObjectContext:[[AppDelegate sharedInstance] managedObjectContext] withItemRef:self.itemRef andCalenderEventID:eventIdentifier];
                if(newEngineerAppointmentForOrder == nil)
                {
                    DDLogError(@"Error: Unable to create appointment data in persistent store.");
                    self->_addAppointmentToCalenderIsInProgress = NO;
                    return;
                }
                else
                {
                    [[AppDelegate sharedInstance] saveContext];
                    [self trackPageWithKeyTask:OMNI_KEYTASK_ORDER_TRACKER_APPOINTMENT_ADDED_TO_CALENDAR];
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"Appointment reminder added successfully." preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [alertController dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okAction];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf presentViewController:alertController animated:YES completion:^{
                            
                            self->_addAppointmentToCalenderIsInProgress = NO;
                            
                        }];
                    });
                }
            }
        }];
    }
}

- (void)userPressedContactUsButtonOfTakeActionView:(BTOrderDetailsTakeActionView *)takeActionView {
    
    [self trackOmniClick:OMNICLICK_ORDER_NEED_HELP forPage:_pageNameForOmniture];
    [self checkChatAvailabilityAction];
}

- (void)checkChatAvailabilityAction
{
    if ([AppManager isInternetConnectionAvailable])
    {
        _networkRequestInProgress = YES;
        [self hideLoadingItems:NO];
        [_loadingView startAnimatingLoadingIndicatorView];
        [self.orderDetailsViewModel checkForLiveChatAvailibilityForOrder];
    }
    else
    {
        [self showAlertWithTitle:kNoConnectionTitle andMessage:kNoConnectionMessage];
    }
}

- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }]];
    
    [self presentViewController:alertController animated:YES completion:^{
        
    }];
}

#pragma mark - UI Related methods

- (void)initialUISetupForOrderDetails
{
    self.title = @"Order details";
    
    self.orderReferenceLabel.text = self.orderRef; // [NSString stringWithFormat:@"Ref: %@",self.orderRef];
    self.orderDescriptionLabel.text = self.productDescription;
    NSString *statusString = [self.orderStatus lowercaseString];
    if(statusString)
    {
        //self.statusLabel.text = [statusString stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[statusString substringToIndex:1] uppercaseString]];
        [self.statusLabel setOrderStatus:[statusString stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[statusString substringToIndex:1] uppercaseString]]];
        ;
    }
    self.orderImageView.image = [UIImage imageNamed:[self.orderDetailsViewModel getImageNameForProductIcon:self.productIcon]];
    
//    // (LP) Update status color according to order status
//    if ([self.orderStatus isEqualToString:@"Completed"]) {
//        self.statusLabel.textColor = [BrandColours colourMyBtGreen];
//    } else if ([self.orderStatus isEqualToString:@"Delayed"] || [self.orderStatus isEqualToString:@"Cancelled"]) {
//        self.statusLabel.textColor = [BrandColours colourMyBtRed];
//    } else {
//        self.statusLabel.textColor = [BrandColours colourMyBtOrange];
//    }
    
   //[self createLoadingView];
    [self performSelector:@selector(createLoadingView) withObject:nil afterDelay:0.1];
    
    [self createOrderSummaryView];
    
    [self createViewFullDetailsView];
    [self createTakeActionView];
    [self createCompletedActionView];
    [self createEmptyOrderSummaryAndMilestoneView];
    
    [self hideTakeActionView];
    
    self.postCodeView.hidden = YES;
    self.constraintPostCodeViewHeight.constant = 0;
}

- (void)displayUIToEnterPostCodeForFullDetailsWithErrorMessageHidden:(BOOL)hideErrorMessage
{
    
    _customlAlertView = [[[NSBundle mainBundle] loadNibNamed:@"BTCustomInputAlertView" owner:nil options:nil] firstObject];
    _customlAlertView.frame = CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    _customlAlertView.delegate = self;
    
    [_customlAlertView updateCustomAlertViewWithTitle:@"Enter postcode"  andMessage:@"Enter the postcode of your billing, installation or delivery address to view full details of this order."];
    
    [_customlAlertView updateAlertButtonTitleForActionButton:@"View details" andCancelButtonTitle:@"Cancel"];
    
    [_customlAlertView updateTextFieldPlaceholderText:@"Postcode" andIsSecureEntry:NO andNeedToShowRightView:NO];
    [_customlAlertView updateLoadingMessage:@"Validating postcode"];
    
    if(_customlAlertView)
        [_customlAlertView showKeyboard];
    
    [[[UIApplication sharedApplication] keyWindow] addSubview:_customlAlertView];
    
    
    // UIAlertAction *viewDetails = [UIAlertAction actionWithTitle:@"View Details" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    
}

- (void)hideTakeActionView
{
    [_takeActionView hideAllTakeActionViewItems];
    
}

- (void)showTakeActionView
{
    if (self.orderDetailsViewModel.productDetails)
    {
        if (self.orderDetailsViewModel.arrayOfMilestoneNodes.count > 0)
        {
            int noOfMilestoneDetails;
            int totalNoOfMilestoneDetails = (int)self.orderDetailsViewModel.arrayOfMilestoneNodes.count;
            for (noOfMilestoneDetails = 0; noOfMilestoneDetails < totalNoOfMilestoneDetails; noOfMilestoneDetails++)
            {
                NSArray *milestoneDetails = [self.orderDetailsViewModel.arrayOfMilestoneNodes objectAtIndex:noOfMilestoneDetails];
                
                NSArray *arrayOfOrderMilestoneNodes;
                
                if (milestoneDetails && milestoneDetails.count > 0)
                {
                    arrayOfOrderMilestoneNodes = [milestoneDetails objectAtIndex:0];
                }
                
                for (BTOrderMilestoneNodeModel *node in arrayOfOrderMilestoneNodes)
                {
                    if (node.isAppointmentAmendSource || node.isActivationAmendSource || node.isSiteContactAmendSource || node.isAltSiteContactAmendSource)
                    {
                        [_takeActionView showAllTakeActionViewItems];
                        
                        if (![AppManager isValidDate:self.orderDetailsViewModel.productSummary.engineeringAppointmentDate] || ([self.orderDetailsViewModel.productSummary.engineeringAppointmentDate timeIntervalSinceNow] < 0.0) || self.isShowAppointment == NO) {
                            [_takeActionView hideAddAppointmentToCalenderView];
                        }
                        return;
                    }
                }
            }
        }
    }
}

- (void)createOrderSummaryView
{
    _orderSummaryView = [[[NSBundle mainBundle] loadNibNamed:@"BTOrderSummaryView" owner:nil options:nil] objectAtIndex:0];
    [_orderSummaryView setOrderSummaryViewDelegate:self];
    
}

- (void)createViewFullDetailsView
{
    _viewFullDetailsView = [[[NSBundle mainBundle] loadNibNamed:@"BTViewFullDetailsView" owner:nil options:nil] objectAtIndex:0];
}

- (void)createTakeActionView
{
    _takeActionView = [[[NSBundle mainBundle] loadNibNamed:@"BTOrderDetailsTakeActionView" owner:nil options:nil] objectAtIndex:0];
}

- (void)createCompletedActionView
{
    _completedActionView = [[[NSBundle mainBundle] loadNibNamed:@"BTOrderDetailsCompletedActionView" owner:nil options:nil] objectAtIndex:0];
    [_completedActionView setOrderDetailsCompletedActionViewDelegate:self];
}

- (void)createEmptyOrderSummaryAndMilestoneView
{
    _emptyOrderSummaryAndMilestoneView = [[[NSBundle mainBundle] loadNibNamed:@"BTEmptyOrderSummaryAndMilestoneView" owner:nil options:nil] objectAtIndex:0];
    [_emptyOrderSummaryAndMilestoneView setEmptyOrderSummaryAndMilestoneViewDelegate:self];
}

- (UIView *)getMilestoneDetailsHeaderViewWithText:(NSString *)titleString
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    UILabel *headerTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, headerView.frame.size.width - 30, 20)];
    headerTitleLabel.text = titleString;
    headerTitleLabel.font = [UIFont fontWithName:kBtFontBold size:14];
    headerTitleLabel.textColor = [UIColor colorForHexString:@"333333"];//[BrandColours colourBtNeutral70];
    [headerView addSubview:headerTitleLabel];
    
    //UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(20, 40, headerView.frame.size.width - 20, 1)];
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 40, headerView.frame.size.width, 1)];
    lineView.backgroundColor = [UIColor colorForHexString:@"eeeeee"];//[BrandColours colourBtNeutral50];
    [headerView addSubview:lineView];
    
    return headerView;
}

- (void)updateAndShowOrderMilestoneView
{
    [_summaryView removeFromSuperview];
    [[_milestoneView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [_bottomView removeFromSuperview];
    [_milestoneView removeFromSuperview];
    
    if (self.orderDetailsViewModel.arrayOfMilestoneNodes.count > 0)
    {
        BTOrderMilestoneNodeView *previousView = [[BTOrderMilestoneNodeView alloc] init];
        
        int noOfMilestoneDetails;
        int totalNoOfMilestoneDetails = (int)self.orderDetailsViewModel.arrayOfMilestoneNodes.count;
        for (noOfMilestoneDetails = 0; noOfMilestoneDetails < totalNoOfMilestoneDetails; noOfMilestoneDetails++)
        {
            NSArray *milestoneDetails = [self.orderDetailsViewModel.arrayOfMilestoneNodes objectAtIndex:noOfMilestoneDetails];
            
            NSArray *arrayOfOrderMilestoneNodes;
            
            if (milestoneDetails && milestoneDetails.count > 0)
            {
                arrayOfOrderMilestoneNodes = [milestoneDetails objectAtIndex:0];
            }
            
            UIView *milestoneDetailsHeaderView = nil;
            if (totalNoOfMilestoneDetails > 1)
            {
                if (noOfMilestoneDetails < (totalNoOfMilestoneDetails - 1))
                {
                    milestoneDetailsHeaderView = [self getMilestoneDetailsHeaderViewWithText:@"Status at old address"];
                }
                else
                {
                    milestoneDetailsHeaderView = [self getMilestoneDetailsHeaderViewWithText:@"Status at new address"];
                }
            }
            
            for (int i=0; i < arrayOfOrderMilestoneNodes.count; i++)
            {
                BTOrderMilestoneNodeModel *milestoneNode = [arrayOfOrderMilestoneNodes objectAtIndex:i];
                BTOrderMilestoneNodeView *milestoneNodeView = [[[NSBundle mainBundle] loadNibNamed:@"BTOrderMilestoneNodeView" owner:nil options:nil] objectAtIndex:0];
                milestoneNodeView.delegate = self;
                
                NSArray *dispatchDetails;
                if (milestoneNode.isDispatchDetailsAvailable)
                {
                    if (milestoneDetails.count > 1)
                    {
                        dispatchDetails = [milestoneDetails objectAtIndex:1];
                    }
                }
                
                if ( i==0 ) {
                    if (i==(arrayOfOrderMilestoneNodes.count-1)) {
                        [milestoneNodeView updateMilestoneNodeViewWithData:milestoneNode isFirstNode:YES isLastNode:YES isShowAppoitment:self.isShowAppointment andDispatchDetails:dispatchDetails];
                    } else {
                        [milestoneNodeView updateMilestoneNodeViewWithData:milestoneNode isFirstNode:YES isLastNode:NO isShowAppoitment:self.isShowAppointment andDispatchDetails:dispatchDetails];
                    }
                } else {
                    if (i==(arrayOfOrderMilestoneNodes.count-1)) {
                        [milestoneNodeView updateMilestoneNodeViewWithData:milestoneNode isFirstNode:NO isLastNode:YES isShowAppoitment:self.isShowAppointment andDispatchDetails:dispatchDetails];
                    } else {
                        [milestoneNodeView updateMilestoneNodeViewWithData:milestoneNode isFirstNode:NO isLastNode:NO isShowAppoitment:self.isShowAppointment andDispatchDetails:dispatchDetails];
                    }
                }
                
                if (self.orderDetailsViewModel.authLevel !=1 || [self.orderDetailsViewModel.productSummary.status isEqualToString:@"Completed"] || [self.orderDetailsViewModel.productSummary.status isEqualToString:@"Cancelled"] || self.orderDetailsViewModel.isMoverOrder /*|| self.isShowAppointment == NO*/) // KUMARESH 5JULY
                {
                    milestoneNodeView.changeButton.hidden = YES;
                }
                
                milestoneNodeView.translatesAutoresizingMaskIntoConstraints = NO;
                [_milestoneView addSubview:milestoneNodeView];
                [milestoneNodeView setNeedsLayout];
                
                NSDictionary *views = nil;
                if (milestoneDetailsHeaderView)
                {
                    views = @{@"milestoneNodeView":milestoneNodeView,@"previousView":previousView,@"milestoneView":_milestoneView,@"milestoneDetailsHeaderView":milestoneDetailsHeaderView};
                }
                else
                {
                    views = @{@"milestoneNodeView":milestoneNodeView,@"previousView":previousView,@"milestoneView":_milestoneView};
                }
                
                
                [_milestoneView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[milestoneNodeView(==milestoneView)]|" options:0 metrics:nil views:views]];
                
                if (i==0 ) {
                    
                    if (milestoneDetailsHeaderView)
                    {
                        milestoneDetailsHeaderView.translatesAutoresizingMaskIntoConstraints = NO;
                        [_milestoneView addSubview:milestoneDetailsHeaderView];
                        
                        [_milestoneView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[milestoneDetailsHeaderView(==milestoneView)]|" options:0 metrics:nil views:views]];
                        
                        if (noOfMilestoneDetails == 0)
                        {
                            [_milestoneView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[milestoneDetailsHeaderView(50)]" options:0 metrics:nil views:views]];
                            [_milestoneView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[milestoneDetailsHeaderView]-0-[milestoneNodeView]" options:0 metrics:nil views:views]];
                        }
                        else
                        {
                            [_milestoneView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[previousView]-0-[milestoneDetailsHeaderView(50)]" options:0 metrics:nil views:views]];
                            [_milestoneView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[milestoneDetailsHeaderView]-0-[milestoneNodeView]" options:0 metrics:nil views:views]];
                        }
                    }
                    else if (noOfMilestoneDetails == 0)
                    {
                        [_milestoneView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[milestoneNodeView]" options:0 metrics:nil views:views]];
                    }
                    else
                    {
                        [_milestoneView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[previousView]-0-[milestoneNodeView]" options:0 metrics:nil views:views]];
                    }
                }
                else
                {
                    [_milestoneView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[previousView]-0-[milestoneNodeView]" options:0 metrics:nil views:views]];
                }
                
                previousView = milestoneNodeView;
            }
        }
        
        
        if (self.orderDetailsViewModel.authLevel != 1) {
            
            _viewFullDetailsView.translatesAutoresizingMaskIntoConstraints = NO;
            _viewFullDetailsView.delegate = self;
            _bottomView = _viewFullDetailsView;
        } else {
            if ([self.orderDetailsViewModel.productSummary.status isEqualToString:@"Completed"] || [self.orderDetailsViewModel.productSummary.status isEqualToString:@"Cancelled"] || self.orderDetailsViewModel.isMoverOrder /*|| self.isShowAppointment == NO*/) {//KUMARESH 5JULY
                
                _completedActionView.translatesAutoresizingMaskIntoConstraints = NO;
                _bottomView = _completedActionView;
            } else {
                
                _takeActionView.translatesAutoresizingMaskIntoConstraints = NO;
                _takeActionView.delegate = self;
                
                if (![AppManager isValidDate:self.orderDetailsViewModel.productSummary.engineeringAppointmentDate] || ([self.orderDetailsViewModel.productSummary.engineeringAppointmentDate timeIntervalSinceNow] < 0.0) || self.isShowAppointment == NO) {
                    [_takeActionView hideAddAppointmentToCalenderView];
                }
                
                _originalTakeActionViewHeight = _takeActionView.frame.size.height;
                
                _bottomView = _takeActionView;
                
            }
        }
        
        previousView.translatesAutoresizingMaskIntoConstraints = NO;
        _bottomView.translatesAutoresizingMaskIntoConstraints = NO;
        _milestoneView.translatesAutoresizingMaskIntoConstraints = NO;
        [_milestoneView addSubview:_bottomView];
        
        [_milestoneView addConstraint:[NSLayoutConstraint constraintWithItem:_bottomView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:_milestoneView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [_milestoneView addConstraint:[NSLayoutConstraint constraintWithItem:_bottomView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:_milestoneView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [_milestoneView addConstraint:[NSLayoutConstraint constraintWithItem:_bottomView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:previousView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
        [_milestoneView addConstraint:[NSLayoutConstraint constraintWithItem:_milestoneView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_bottomView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
        
        [self.mainContainerScrollView addSubview:_milestoneView];
        [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_milestoneView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.mainContainerScrollView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0]];
        [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_milestoneView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.mainContainerScrollView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0]];
//        [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_milestoneView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.orderDetailsSegmentedControl attribute:NSLayoutAttributeBottom multiplier:1.0 constant:8.0]];
        [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_milestoneView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.segmentContainerView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
        [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:self.mainContainerScrollView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_milestoneView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    }
    else
    {
        if (_orderDetailsViewModel.arrayOfMilestoneNodes.count == 0 && !_isfetchingMileStoneDetailsAPI && !_isfetchingMilestoneFullDetailsAPI)
        {
            _emptyOrderSummaryAndMilestoneView.translatesAutoresizingMaskIntoConstraints = NO;
            [_milestoneView addSubview:_emptyOrderSummaryAndMilestoneView];
            
            [_milestoneView addConstraint:[NSLayoutConstraint constraintWithItem:_emptyOrderSummaryAndMilestoneView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:_milestoneView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
            [_milestoneView addConstraint:[NSLayoutConstraint constraintWithItem:_emptyOrderSummaryAndMilestoneView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:_milestoneView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
            [_milestoneView addConstraint:[NSLayoutConstraint constraintWithItem:_emptyOrderSummaryAndMilestoneView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_milestoneView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
            [_milestoneView addConstraint:[NSLayoutConstraint constraintWithItem:_milestoneView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_emptyOrderSummaryAndMilestoneView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
            
            _milestoneView.translatesAutoresizingMaskIntoConstraints = NO;
            [self.mainContainerScrollView addSubview:_milestoneView];
            
            [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_milestoneView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.mainContainerScrollView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0]];
            [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_milestoneView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.mainContainerScrollView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0]];
//            [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_milestoneView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.orderDetailsSegmentedControl attribute:NSLayoutAttributeBottom multiplier:1.0 constant:20.0]];
            [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_milestoneView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.segmentContainerView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
            [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:self.mainContainerScrollView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_milestoneView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
        }
    }
    [_milestoneView setNeedsLayout];
    [self.mainContainerScrollView setNeedsLayout];
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
}

- (void)updateAndShowOrderSummaryView
{
    [_milestoneView removeFromSuperview];
    [_orderSummaryView removeFromSuperview];
    [_summaryView removeFromSuperview];
    [_bottomView removeFromSuperview];

    [_orderSummaryView updateOrderSummaryViewWithProduct:self.orderDetailsViewModel.productSummary productOrderItems:self.orderDetailsViewModel.pricingDetails withAuthLevel:self.orderDetailsViewModel.authLevel isShowAppointment:self.isShowAppointment andPaymentModeType:_orderDetailsViewModel.pricingDetails.regularChargesView];
    
    [self updateVisibilityOfEngineerAppointmentDetailsButton];
    
    if (self.orderDetailsViewModel.authLevel != 1) {
        
        _viewFullDetailsView.translatesAutoresizingMaskIntoConstraints = NO;
        _viewFullDetailsView.delegate = self;
        _bottomView = _viewFullDetailsView;
    } else {

        if ([self.orderDetailsViewModel.productSummary.status isEqualToString:@"Completed"] || [self.orderDetailsViewModel.productSummary.status isEqualToString:@"Cancelled"] || self.orderDetailsViewModel.isMoverOrder) {
            
            _completedActionView.translatesAutoresizingMaskIntoConstraints = NO;
            _bottomView = _completedActionView;
        } else {
            
            _takeActionView.translatesAutoresizingMaskIntoConstraints = NO;
            _takeActionView.delegate = self;
            
            if (![AppManager isValidDate:self.orderDetailsViewModel.productSummary.engineeringAppointmentDate] || ([self.orderDetailsViewModel.productSummary.engineeringAppointmentDate timeIntervalSinceNow] < 0.0)) {
                [_takeActionView hideAddAppointmentToCalenderView];
            }
            
            _originalTakeActionViewHeight = _takeActionView.frame.size.height;
            
            _bottomView = _takeActionView;
            
        }
    }
    
    _orderSummaryView.translatesAutoresizingMaskIntoConstraints = NO;
    _summaryView.translatesAutoresizingMaskIntoConstraints = NO;
    _bottomView.translatesAutoresizingMaskIntoConstraints = NO;
    [_summaryView addSubview:_orderSummaryView];
    [_summaryView addSubview:_bottomView];
    
    [_summaryView addConstraint:[NSLayoutConstraint constraintWithItem:_orderSummaryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:_summaryView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [_summaryView addConstraint:[NSLayoutConstraint constraintWithItem:_orderSummaryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:_summaryView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [_summaryView addConstraint:[NSLayoutConstraint constraintWithItem:_orderSummaryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_summaryView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [_summaryView addConstraint:[NSLayoutConstraint constraintWithItem:_bottomView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:_summaryView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [_summaryView addConstraint:[NSLayoutConstraint constraintWithItem:_bottomView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:_summaryView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [_summaryView addConstraint:[NSLayoutConstraint constraintWithItem:_bottomView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_orderSummaryView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    [_summaryView addConstraint:[NSLayoutConstraint constraintWithItem:_summaryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_bottomView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
    [self.mainContainerScrollView addSubview:_summaryView];
    
    [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_summaryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.mainContainerScrollView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_summaryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.mainContainerScrollView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
//    [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_summaryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.orderDetailsSegmentedControl attribute:NSLayoutAttributeBottom multiplier:1.0 constant:20.0]];
    [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_summaryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.segmentContainerView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:self.mainContainerScrollView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_summaryView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:8.0]];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
}

- (void)updateVisibilityOfEngineerAppointmentDetailsButton
{
    if ((_orderDetailsViewModel.authLevel == 1) && ![self.orderDetailsViewModel.productSummary.status isEqualToString:@"Completed"] && ![self.orderDetailsViewModel.productSummary.status isEqualToString:@"Cancelled"] && !self.orderDetailsViewModel.isMoverOrder && !_isfetchingMileStoneDetailsAPI && !_isfetchingMilestoneFullDetailsAPI)
    {
        if (self.orderDetailsViewModel.arrayOfMilestoneNodes.count > 0)
        {
            int noOfMilestoneDetails;
            int totalNoOfMilestoneDetails = (int)self.orderDetailsViewModel.arrayOfMilestoneNodes.count;
            for (noOfMilestoneDetails = 0; noOfMilestoneDetails < totalNoOfMilestoneDetails; noOfMilestoneDetails++)
            {
                NSArray *milestoneDetails = [self.orderDetailsViewModel.arrayOfMilestoneNodes objectAtIndex:noOfMilestoneDetails];
                
                NSArray *arrayOfOrderMilestoneNodes;
                
                if (milestoneDetails && milestoneDetails.count > 0)
                {
                    arrayOfOrderMilestoneNodes = [milestoneDetails objectAtIndex:0];
                }
                
                for (BTOrderMilestoneNodeModel *node in arrayOfOrderMilestoneNodes)
                {
                    if (node.isAppointmentAmendSource) {
                        [_orderSummaryView showEngineerAppointmentDetailsButton];
                        return;
                    }
                }
            }
        }
    }
    
    [_orderSummaryView hideEngineerAppointmentDetailsButton];
}

#pragma mark ErrorHandling

- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow {
    
    _isRetryShown = YES;
    
    if (_retryView != nil)
    {
        _retryView.retryViewDelegate = self;
        _retryView.hidden = NO;
        [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
        return;
    }
    
    _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
    _retryView.translatesAutoresizingMaskIntoConstraints = NO;
    _retryView.retryViewDelegate = self;
    
    [self.mainContainerScrollView addSubview:_retryView];
    
    [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.mainContainerScrollView  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    
    [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.mainContainerScrollView  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    
    [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.segmentContainerView  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
    [self.mainContainerScrollView addConstraint:[NSLayoutConstraint constraintWithItem:self.mainContainerScrollView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_retryView  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
    
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
}

- (void)hideRetryItems:(BOOL)isHide {
    
    _isRetryShown = NO;
    // (lp) Hide or Show UI elements related to retry.
    [_retryView removeFromSuperview];
    [_retryView setRetryViewDelegate:nil];
    [_retryView setHidden:isHide];
    _retryView = nil;
    [self.view layoutIfNeeded];
}



#pragma mark- BTOrderAmendViewControllerDelegate method

- (void)btOrderAmendViewController:(BTOrderAmendViewController *)controller didAmendOrderForOrderDetailsModel:(DLMOrderDetailsScreen *)orderDetailModel isEngineerAppointmentChanged:(BOOL)isChanged {
    
    if(self.primaryOrderDetailsViewModel == nil){
        self.orderDetailsViewModel = orderDetailModel;
    }
      if (isChanged)
        {
            BOOL isAlreadyAddedAndNotAbleToDelete = [self checkForAlreadyAddedAppointmentAndDeleteForCurrentOrder];
            
            if (isAlreadyAddedAndNotAbleToDelete)
            {
                DDLogError(@"Error: Not able to remove appointment from calender");
            }
        }
        
        [self updateUIAfterOrderAmendedSuccessfully];
        
        _isDataChanged = YES;
   
    
}

- (void)updateUIAfterOrderAmendedSuccessfully
{
    [self showAlertBannerWithMessage:@"Order details updated"];
    
    
    if (self.selectedTab == OrderSummaryTab) {
        
        [self updateAndShowOrderSummaryView];
        
        [_orderSummaryView updateOrderSummaryViewWithProductDetails:self.orderDetailsViewModel.productDetails andSummaryType:self.orderDetailsViewModel.orderDetailSummaryType];
        
    }
    else{
        
        if (self.orderDetailsViewModel.arrayOfMilestoneNodes.count > 0) {
            
            [self updateAndShowOrderMilestoneView];
        }
    }
    
}

#pragma mark - BTEmptyOrderSummaryAndMilestoneViewDelegate Methods
- (void)userPressedNeedHelpButtonOnEmptyOrderSummaryAndMilestoneView:(BTEmptyOrderSummaryAndMilestoneView *)emptyOrderSummaryAndMilestoneView
{
    [self trackOmniClick:OMNICLICK_ORDER_NEED_HELP forPage:_pageNameForOmniture];
    [self checkChatAvailabilityAction];
}

#pragma mark - BTRetryView delegate methods
- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView {
    
    DDLogInfo(@"Retry to fecth details ");
    [self hideRetryItems:YES];
    self.orderDetailsSegmentedControl.enabled = YES;
    if (_isfetchingOrderDetailsAPI) {
        [self hideLoadingItems:NO];
        [_loadingView startAnimatingLoadingIndicatorView];
        
        [self fetchOrderDetailsFromAPI];
    }
    if (_isfetchFullOrderDetailsWithPostcodeOverlay) {
        [self hideLoadingItems:NO];
        [_loadingView startAnimatingLoadingIndicatorView];
        
        //[self showLoaderView];
        
        [self fetchCompleteOrderDetailsData];
    }
    
    if (_isfetchingMileStoneDetailsAPI && _postCode.length == 0) {
        _isSingleMileStoneLoadingView = YES;
        
        [self showLoaderView];
        [_takeActionView startLoaderView];
        
        [self fetchOrderMilestonesData];
    }
    if (_isfetchingMileStoneDetailsAPI && _postCode.length > 0) {
        _isSingleMileStoneLoadingView = YES;
        
        [self showLoaderView];
        [_takeActionView startLoaderView];
        
        [self fetchCompleteOrderMilestonesData];
    }
    if (_isfetchingfullOrderDetailsAPI) {
        _isSingleDetailsLoadingView = YES;
        
        [self showLoaderView];
        
        [self fetchCompleteOrderDetailsData];
    }
    
    //    [self fetchOrderDashBoardDetailsWithPageIndex:1];
}

#pragma mark DLMOrderDetailsScreenDelegate methods

- (void)successfullyFetchedOrderSummaryDataOnOrderDetailsScreen:(DLMOrderDetailsScreen *)orderDetailsScreen {
    
    _isfetchingOrderDetailsAPI = NO;
    
    _isorderDetailsFail = NO;
     if(self.isSIM2Order == YES && self.isSecondaryOrder == YES && issecondaryOrderProccesed == YES){
         
         if(primaryOrderApiCount == 1){
             if(_postCode != nil || _postCode.length > 1 ){
                 primaryOrderApiCount = 2;
                 [self.primaryOrderDetailsViewModel fetchCompleteOrderDetailsForOrderReference:self.orderRef ItemRef:self.itemBBRef postCode:_postCode andErrorCount:0];
             } else{
                 primaryOrderApiCount = 3;
                 //self.orderDetailsViewModel.arrayOfMilestoneNodes = nil;
                 [self.primaryOrderDetailsViewModel fetchOrderMilestonesForOrderReference:self.orderRef ItemRef:self.itemBBRef postCode:_postCode andErrorCount:0];
             }
             
         } else if (primaryOrderApiCount == 2) {
             primaryOrderApiCount = 3;
             if ([_postCode isEqualToString:@""] || (_postCode.length <= 0)) {
                     //self.orderDetailsViewModel.arrayOfMilestoneNodes = nil;
                     [self.primaryOrderDetailsViewModel fetchOrderMilestonesForOrderReference:self.orderRef andItemRef:self.itemBBRef];
                     self.networkRequestInProgress = YES;
                 
             } else{
                    //self.orderDetailsViewModel.arrayOfMilestoneNodes = nil;
                    [self.primaryOrderDetailsViewModel fetchOrderMilestonesForOrderReference:self.orderRef ItemRef:self.itemBBRef postCode:_postCode andErrorCount:0];
             }
         }
         self.networkRequestInProgress = YES;
       
         return;
     }
    
    self.isSIM2Order = orderDetailsScreen.isSIMOrder;
    
    if (self.selectedTab == OrderSummaryTab) {
        
        [self updateAndShowOrderSummaryView];
    }
    if (self.orderDetailsViewModel.authLevel == 1) {
        
        _isfetchingfullOrderDetailsAPI = NO;
        
        _isCompletedOrdersFetched = YES;
        _isfetchFullOrderDetailsWithPostcodeOverlay = NO;
        self.postCodeView.hidden = YES;
        self.constraintPostCodeViewHeight.constant = 0;
        
        _summaryView.hidden = NO;
       
        [_orderSummaryView updateOrderSummaryViewWithProductDetails:self.orderDetailsViewModel.productDetails andSummaryType:self.orderDetailsViewModel.orderDetailSummaryType];
        
        if (!_isCompleteMilestonesFetched) {
            
            if (self.orderDetailsViewModel.pricingDetails.journeyType == 2)
            {
                _isCompleteMilestonesFetched = YES;
                self.orderDetailsViewModel.arrayOfMilestoneNodes = nil;
            }
            else
            {
                if ([_postCode isEqualToString:@""] || (_postCode.length <= 0)) {
                    _isSingleMileStoneLoadingView = YES;
                    _isfetchingMileStoneDetailsAPI = YES;
                    
                    self.orderDetailsViewModel.arrayOfMilestoneNodes = nil;
                    
                    [self fetchOrderMilestonesData];
                } else {
                    _isSingleMileStoneLoadingView = YES;
                    _isfetchingMileStoneDetailsAPI = YES;
                    
                    self.orderDetailsViewModel.arrayOfMilestoneNodes = nil;
                    
                    [self fetchCompleteOrderMilestonesData];
                }
            }
        }
    } else { //RestrictedView
        self.postCodeView.hidden = NO;
        self.constraintPostCodeViewHeight.constant = 40;
        [self hideLoadingItems:YES];
        self.networkRequestInProgress = NO;
        
        [self trackOmniPage:OMNIPAGE_ORDER_RESTRICTED];
    }
    
    if (_isSingleDetailsLoadingView) {
        
        _isSingleDetailsLoadingView = NO;
        
        [self hideLoaderView];
        
        return;
    }
    
    [self hideLoadingItems:YES];
    self.networkRequestInProgress = NO;
    
    if (_isPostalCodeRequired) {
        
        [_customlAlertView stopLoading];
        [_customlAlertView deRegisterKeyboardNotification];
        [_customlAlertView removeFromSuperview];
        _customlAlertView = nil;
        
        if (self.orderDetailsViewModel.authLevel == 1) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Postcode validated" message:@"You're now able to view full details of this order." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [self trackContentViewedForPage:_pageNameForOmniture andNotificationPopupDetails:OMNINOTIFY_ORDER_POSTCODE_OK];
                
                [alertController dismissViewControllerAnimated:YES completion:nil];
            }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        } else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Message" message:@"The postcode doesn't belong to this order." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [alertController dismissViewControllerAnimated:YES completion:nil];
            }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        _isPostalCodeRequired = NO;
    }
    
}

- (void)trackorderDetailsScreen:(DLMOrderDetailsScreen *)orderDetailsScreen failedToFetchOrderDetailsWithWebServiceError:(NLWebServiceError *)error  {
    
    DDLogError(@"Order Details: Fetch order details failed");
    [self hideLoadingItems:YES];
    [_customlAlertView stopLoading];
    [self setNetworkRequestInProgress:false];
    
    
    if(_isFetchingFullDetails)
    {
        _isFetchingFullDetails = NO;
        
        [_customlAlertView stopLoading];
        BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
        if (errorHandled == NO) {
            
            _customlAlertView.frame = CGRectMake(0,0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
            [_customlAlertView showErrorOnCustomInputWithWithErrro:kDefaultErrorMessage];
            
        }
        else
        {
            [_customlAlertView stopLoading];
            [_customlAlertView removeFromSuperview];
            _customlAlertView = nil;
        }
        return;
    }
    // _summaryView.hidden = YES;
    [_customlAlertView deRegisterKeyboardNotification];
    [_customlAlertView removeFromSuperview];
    _customlAlertView= nil;
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    if([error.error.domain isEqualToString:BTNetworkErrorDomain] && (errorHandled == NO))
    {
        if (_isSingleDetailsLoadingView) {
            
            _isSingleDetailsLoadingView = NO;
            
            [self hideLoaderView];
            
            //                    return;
        }
        //Showretry
        if (_isfetchingOrderDetailsAPI || _isfetchingfullOrderDetailsAPI) {
            //                    [self showRetryView];
        }
        _isorderDetailsFail = YES;
        _isRetryShown = YES;
        if (self.selectedTab == OrderSummaryTab) {
            //self.orderDetailsSegmentedControl.enabled = NO;
            [self showRetryViewWithInternetStrip:NO];
        }
    }
    else if (errorHandled == NO)
    {
        [AppManager trackGenericAPIErrorOnPage:_pageNameForOmniture];
        
        if (_isSingleDetailsLoadingView) {
            
            _isSingleDetailsLoadingView = NO;
            
            [self hideLoaderView];
            //            return;
        }
        
        //Showretry
        if (_isfetchingOrderDetailsAPI || _isfetchingfullOrderDetailsAPI) {
            
        }
        
        _isorderDetailsFail = YES;
        _isRetryShown = YES;
        if (self.selectedTab == OrderSummaryTab) {
            //self.orderDetailsSegmentedControl.enabled = NO;
            [self showRetryViewWithInternetStrip:NO];
        }
        
    }
}


#pragma mark - DLMOrderMilestonesScreenDelegate Methods
- (void)successfullyFetchedOrderMilestonesDataOnOrderDetailsScreen:(DLMOrderDetailsScreen *)orderDetailsScreen
{

    if(self.isSIM2Order == YES && self.isSecondaryOrder == YES && issecondaryOrderProccesed == YES){
      
        _isfetchingMileStoneDetailsAPI = NO;
        _isfetchingMilestoneFullDetailsAPI = NO;
        
        _isCompleteMilestonesFetched = YES;
        [self hideLoadingItems:YES];
        self.networkRequestInProgress = NO;
        
       /*if (self.selectedTab == OrderTrackerTab) {
            [self updateAndShowOrderMilestoneView];
        }*/
        [self hideLoadingItems:YES];
        self.networkRequestInProgress = NO;
      
        if (_isSingleDetailsLoadingView) {
         
             _isSingleDetailsLoadingView = NO;
            
             [self hideLoaderView];
             [self redirectToAmmendOrderScene];
         return;
         }
        
        [self redirectToAmmendOrderScene];
        return;
    }
    _ismilestoneDetailsFail = NO;
    [_takeActionView stopLoaderView];
    _bottomView = _takeActionView;
    
    if (self.orderDetailsViewModel.authLevel == 1)
    {
        
        self.postCodeView.hidden = YES;
        self.constraintPostCodeViewHeight.constant = 0;
        
        if (!([self.orderDetailsViewModel.productSummary.status isEqualToString:@"Completed"] || [self.orderDetailsViewModel.productSummary.status isEqualToString:@"Cancelled"] || self.orderDetailsViewModel.isMoverOrder))
        {
           [self showTakeActionView];
        }
        
        _isfetchingMileStoneDetailsAPI = NO;
        _isfetchingMilestoneFullDetailsAPI = NO;
        
        _isCompleteMilestonesFetched = YES;
        [self hideLoadingItems:YES];
        self.networkRequestInProgress = NO;
        
        if (self.selectedTab == OrderTrackerTab) {
            [self updateAndShowOrderMilestoneView];
        }
        if (!_isCompletedOrdersFetched) {
            
            _summaryView.hidden = YES;
            _isfetchingfullOrderDetailsAPI = YES;
            _isSingleDetailsLoadingView = YES;
            [self fetchCompleteOrderDetailsData];
        } else {
            
            [self updateVisibilityOfEngineerAppointmentDetailsButton];
        }
        
        if (_isSingleMileStoneLoadingView) {
            
            _isSingleMileStoneLoadingView = NO;
            
            [self hideLoaderView];
        }
    }
    else
    {
        self.postCodeView.hidden = NO;
        self.constraintPostCodeViewHeight.constant = 40;
        [self hideLoadingItems:YES];
        self.networkRequestInProgress = NO;
    }
    
    
    if (_isPostalCodeRequired) {
        
        [_customlAlertView stopLoading];
        [_customlAlertView deRegisterKeyboardNotification];
        [_customlAlertView removeFromSuperview];
        _customlAlertView = nil;
        
        if (self.orderDetailsViewModel.authLevel == 1) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Postcode validated" message:@"You're now able to view full details of this order." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [alertController dismissViewControllerAnimated:YES completion:nil];
            }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        } else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Message" message:@"The postcode doesn't belong to this order." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [alertController dismissViewControllerAnimated:YES completion:nil];
            }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        _isPostalCodeRequired = NO;
    }
}

- (void)orderDetailsScreen:(DLMOrderDetailsScreen *)orderDetailsScreen failedToFetchOrderMilestonesDataWithWebServiceError:(NLWebServiceError *)error {
    
    DDLogError(@"Order Milestone: Fetch order Milestone failed");
    [self setNetworkRequestInProgress:false];
    [_customlAlertView stopLoading];
    [_takeActionView stopLoaderView];
    _bottomView = _takeActionView;
    
    if(_isfetchingMilestoneFullDetailsAPI)
    {
        _isfetchingMilestoneFullDetailsAPI = NO;
        
        [_customlAlertView stopLoading];
        BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
        
        if(errorHandled)
            [AppManager trackGenericAPIErrorOnPage:_pageNameForOmniture];
        
        if (errorHandled == NO) {
            
            _customlAlertView.frame = CGRectMake(0,0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
            [_customlAlertView showErrorOnCustomInputWithWithErrro:kDefaultErrorMessage];
        }
        else
        {
            [_customlAlertView removeFromSuperview];
            _customlAlertView = nil;
        }
        return;
    }
    [_customlAlertView deRegisterKeyboardNotification];
    [_customlAlertView removeFromSuperview];
    _customlAlertView = nil;
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if(errorHandled)
        [AppManager trackGenericAPIErrorOnPage:_pageNameForOmniture];
    
    if([error.error.domain isEqualToString:BTNetworkErrorDomain] && (errorHandled == NO) )
    {
        //Show retry View here
        if (_isSingleMileStoneLoadingView) {
            
            _isSingleMileStoneLoadingView = NO;
            
            [self hideLoaderView];
            //                    return;
        }
        
        //show retry
        if (_isfetchingMileStoneDetailsAPI || _isfetchingMilestoneFullDetailsAPI) {
            
        }
        _ismilestoneDetailsFail = YES;
        _isRetryShown = YES;
        if (self.selectedTab == OrderTrackerTab) {
            
            [self showRetryViewWithInternetStrip:NO];
        }
    }
    else if (errorHandled == NO)
    {
         [AppManager trackGenericAPIErrorOnPage:_pageNameForOmniture];
        //show retry view
        if (_isSingleMileStoneLoadingView) {
            
            _isSingleMileStoneLoadingView = NO;
            
            [self hideLoaderView];
            //            return;
        }
        
        //show retry
        if (_isfetchingMileStoneDetailsAPI || _isfetchingMilestoneFullDetailsAPI) {
            
        }
        _ismilestoneDetailsFail = YES;
        _isRetryShown = YES;
        if (self.selectedTab == OrderTrackerTab) {
            
            [self showRetryViewWithInternetStrip:NO];
        }
        
    }
}

- (void)liveChatAvailableForOrderWithScreen:(DLMOrderDetailsScreen *)orderDetailsScreen
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
    
    viewController.targetURLType = BTTargetURLTypeOrdersChatScreen;
    
    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:NO andIsNeedCloseButton:YES];
    
    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{
        
    }];
}

- (void)orderDetailsScreen:(DLMOrderDetailsScreen *)orderDetailsScreen liveChatCurrentlyNotAvailableWithSpecificCategory:(NSArray *)specificCategory generalCategory:(NSArray *)generalCategory andCurrentTime:(NSString *)currentTime
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BTHelpAndSupportViewController *helpAndSupportController = [BTHelpAndSupportViewController getHelpAndSupportViewController];
    helpAndSupportController.isLaunchingFromHomeScreen = NO;
    helpAndSupportController.isDataAlreadyFetched = YES;
    helpAndSupportController.specificCategory = specificCategory;
    helpAndSupportController.generalCategory = generalCategory;
    helpAndSupportController.currentTime = currentTime;
    
    [self.navigationController pushViewController:helpAndSupportController animated:YES];
}

- (void)orderDetailsScreen:(DLMOrderDetailsScreen *)orderDetailsScreen failedToCheckLiveChatAvailabilityWithWebServiceError:(NLWebServiceError *)error
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if(errorHandled == NO)
    {
        [self showAlertWithTitle:@"Message" andMessage:kDefaultErrorMessage];
        [AppManager trackGenericAPIErrorOnPage:_pageNameForOmniture];
    }
    
}

#pragma mark - BTCustom Alert View Delegate

- (void)userDidPressConfirmOnCustomInputAlertView:(BTCustomInputAlertView *)alertView withPassword:(NSString *)passwordText
{
    [self trackOmniClick:OMNICLICK_ENTER_PASSCODE forPage:_pageNameForOmniture];
    [self trackContentViewedForPage:_pageNameForOmniture andNotificationPopupDetails:OMNINOTIFY_ORDER_POSTCODE_VIEWDETAILS];
    NSString *postCode = passwordText;
    [self doValidationOnPostCode:postCode];
}

- (void)userDidPressCancelOnCustomInputAlertView:(BTCustomInputAlertView *)alertView
{
    [self trackContentViewedForPage:_pageNameForOmniture andNotificationPopupDetails:OMNINOTIFY_ORDER_POSTCODE_CANCEL];
    
    [_customlAlertView deRegisterKeyboardNotification];
    [_customlAlertView removeFromSuperview];
    _customlAlertView = nil;
}


- (void)trackOmniPage:(NSString *)page
{
    NSMutableDictionary *contextDict = [NSMutableDictionary dictionary];
    
    NSString *orderRef = [OmnitureManager getOrderRefFormattedString:self.orderRef];
    [contextDict setValue:orderRef forKey:kOmniOrderRef];
    
//    [OmnitureManager trackPage:page withContextInfo:contextDict];
    
    //Set KeyTask
    if([page isEqualToString:OMNIPAGE_ORDER_DETAILS])
    {
       [contextDict setValue:OMNI_KEYTASK_ORDER_TRACKER_SUMMARY forKey:kOmniKeyTask];
    }
    else
    {
       [contextDict setValue:OMNI_KEYTASK_ORDER_TRACKER_DETAILS forKey:kOmniKeyTask];
    }
    [OmnitureManager trackPage:page withContextInfo:contextDict];
}


- (void)trackOmniClick:(NSString *)clickEvent forPage:(NSString *)page
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSString *orderRef = [OmnitureManager getOrderRefFormattedString:self.orderRef];
    [params setValue:orderRef forKey:kOmniOrderRef];
    
    [params addEntriesFromDictionary:[AppManager getOmniDictionary]];
    NSString *pageName = page;
    NSString *linkTitle = clickEvent;
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:params];
}


- (void)trackContentViewedForPage:(NSString *)page andNotificationPopupDetails:(NSString *)notificationPopup
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = page;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    NSString *notificaiton = [NSString stringWithFormat:@"%@%@",STATE_PREFIX,notificationPopup];
    [data setValue:notificaiton forKey:kOmniContentViewed];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
    
}

- (void)trackPageWithKeyTask:(NSString *)keytask
{
    NSMutableDictionary *contextDict = [NSMutableDictionary dictionary];
    
    NSString *orderRef = [OmnitureManager getOrderRefFormattedString:self.orderRef];
    [contextDict setValue:orderRef forKey:kOmniOrderRef];
    
    
    NSString *pageName = OMNIPAGE_ORDER_DETAILS;
    [contextDict setValue:keytask forKey:kOmniKeyTask];
    [OmnitureManager trackPage:pageName withContextInfo:contextDict];
}



@end
