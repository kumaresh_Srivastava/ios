//
//  BTChatViewController.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 05/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTChatViewController.h"
#import "AppConstants.h"

@interface BTChatViewController ()

@end

@implementation BTChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.title = @"Chat View";

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Public Methods

+ (BTChatViewController *)getChatViewController
{

    UIStoryboard *storyboard= [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];

    BTChatViewController *chatViewController = (BTChatViewController *)[storyboard instantiateViewControllerWithIdentifier:@"BTChatViewController"];

    return chatViewController;
    
}

@end
