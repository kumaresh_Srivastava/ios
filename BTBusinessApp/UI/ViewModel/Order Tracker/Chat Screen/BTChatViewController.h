//
//  BTChatViewController.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 05/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTChatViewController : UIViewController


+ (BTChatViewController *)getChatViewController;

@end
