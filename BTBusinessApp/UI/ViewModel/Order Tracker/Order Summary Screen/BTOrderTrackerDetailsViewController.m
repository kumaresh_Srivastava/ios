//
//  BTOrderTrackerDetailsViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 23/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderTrackerDetailsViewController.h"
#import "BTOrderTrackerTableViewCell.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "BTOrderTrackerBundleSummaryViewController.h"
#import "DLMOrderSummaryScreen.h"
#import "BTOrder.h"
#import "BTProduct.h"
#import "AppManager.h"
#import "AppConstants.h"
#import "BTRetryView.h"
#import "CustomSpinnerView.h"
#import "OmnitureManager.h"
#import "NLWebServiceError.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "BTOrderDetailsMilestoneViewController.h"
#import "BTHelpAndSupportViewController.h"
#import "BTEmptyDashboardView.h"
#import "BTInAppBrowserViewController.h"
#import "BTOrderTrackerGroupOrderSummaryViewController.h"
#import "BTNavigationViewController.h"
#import "BTOrderStatusLabel.h"

@interface BTOrderTrackerDetailsViewController () <UITableViewDelegate, UITableViewDataSource, DLMOrderSummaryScreenDelegate, BTRetryViewDelegate, BTOrderDetailsMilestoneViewControllerDelegate, BTOrderTrackerBundleSummaryViewControllerDelegate> {
    CustomSpinnerView *_loadingView;
    BTRetryView *_retryView;
}

@property (nonatomic, readwrite) DLMOrderSummaryScreen *viewModel;


@property (weak, nonatomic) IBOutlet UIView *orderSummaryTopView;
@property (weak, nonatomic) IBOutlet UIView *orderSummaryDateContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *orderSummaryImageView;

@property (weak, nonatomic) IBOutlet UILabel *orderDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *expectedCompletionLabel;
@property (weak, nonatomic) IBOutlet UITableView *orderSummaryTableView;
@property (weak, nonatomic) IBOutlet UILabel *orderCompletionLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderPlacedLabel;
@property (weak, nonatomic) IBOutlet BTOrderStatusLabel *orderStatusLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelThisOrderButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orderDescriptionTopConstraint;
@property (weak, nonatomic) IBOutlet UILabel *orderRefLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *middleLabelHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *inlineButtonView;

@property (weak, nonatomic) IBOutlet UIButton *orderInlineButton;



@end

@implementation BTOrderTrackerDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // (lp) setup viewModel for order summary screen
    self.viewModel = [[DLMOrderSummaryScreen alloc] init];
    self.viewModel.orderSummaryScreenDelegate = self;
    
    // (lp) Navigation bar back button
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    // TODO: Check & remove this part of the code
    if (self.isFromAPNS) {
        [self setIsFromAPNS:NO];
        UIBarButtonItem *leftbarbutton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon_Home_White"] style:UIBarButtonItemStyleDone target:self action:@selector(homeButtonAction)];
        [self.navigationItem setLeftBarButtonItem:leftbarbutton];
    }
   //  self.edgesForExtendedLayout = UIRectEdgeNone;
    
    // Order details table view setup
    self.orderSummaryTableView.delegate = self;
    self.orderSummaryTableView.dataSource = self;
    self.orderSummaryTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.orderSummaryTableView.estimatedRowHeight = 124;
    self.orderSummaryTableView.rowHeight = UITableViewAutomaticDimension;
    
    UINib *dataCell = [UINib nibWithNibName:@"BTOrderTrackerTableViewCell" bundle:nil];
    [self.orderSummaryTableView registerNib:dataCell forCellReuseIdentifier:@"BTOrderTrackerTableViewCell"];
    
    __weak typeof(self) weakSelf = self;
    
    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([weakSelf networkRequestInProgress]) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
            
        }
        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
            
        }
    }];

    [self initialSetupForOrderSummaryScreen];
    [self fetchOrderSummaryDetails];

    [self trackOmniPage];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self resizeTableViewHeaderToFit];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChangeOrderDetail:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    self.networkRequestInProgress = NO;
    [self.viewModel cancelFetchOrderSummaryAPICall];
}


#pragma mark - UI setup related methods

- (void)statusBarOrientationChangeOrderDetail:(NSNotification *)notification {
    // handle the interface orientation as needed
    [self performSelector:@selector(createLoadingView) withObject:nil afterDelay:0.1];
    //[self createLoadingView];
}

/*
 Resize header view of table to update height of header view
 */
- (void)resizeTableViewHeaderToFit {
    UITableViewHeaderFooterView *headerView = (UITableViewHeaderFooterView *)self.orderSummaryTableView.tableHeaderView;
    
    [headerView setNeedsLayout];
    [headerView layoutIfNeeded];
    
    CGFloat height = [headerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CGRect frame = headerView.frame;
    frame.size.height = height;
    headerView.frame = frame;
    
    self.orderSummaryTableView.tableHeaderView = headerView;
    
}

- (void)initialSetupForOrderSummaryScreen
{
    
    // (LP) initial setup for UI items
    self.title = @"Order summary";[NSString stringWithFormat:@"Order %@",self.orderRef];
    self.orderRefLabel.text = [NSString stringWithFormat:@"%@",self.orderRef];
    self.orderStatusLabel.text = @"";
    self.orderCompletionLabel.text = @"";
    self.expectedCompletionLabel.text =@"";
    self.orderPlacedLabel.text = @"";
    
    [self.cancelThisOrderButton.layer setBorderColor:[UIColor darkGrayColor].CGColor];
    
   // [self createLoadingView];
   [self performSelector:@selector(createLoadingView) withObject:nil afterDelay:0.1];
    
    [self hideorShowControls:YES];
}

- (void)checkChatAvailabilityAction
{
    if ([AppManager isInternetConnectionAvailable])
    {
        _networkRequestInProgress = YES;
        [self hideLoadingItems:NO];
        [_loadingView startAnimatingLoadingIndicatorView];
        [self.viewModel checkForLiveChatAvailibilityForOrder];
    }
    else
    {
        [self showAlertWithTitle:kNoConnectionTitle andMessage:kNoConnectionMessage];
    }
}

- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }]];
    
    [self presentViewController:alertController animated:YES completion:^{
        
    }];
}

#pragma mark - Table view datasource methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.viewModel.arrayOfProductsNotRelatedToBundle count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTProduct *product = [self.viewModel.arrayOfProductsNotRelatedToBundle objectAtIndex:indexPath.row];
    BOOL isShowAppointment = self.viewModel.order.IsShowAppointment; 
    BTOrderTrackerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BTOrderTrackerTableViewCell" forIndexPath:indexPath];

    [cell updateCellWithProduct:product isShowAppointment:isShowAppointment];
    
    return cell;
    
}


#pragma mark - Table view delegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    BTProduct *product = [self.viewModel.arrayOfProductsNotRelatedToBundle objectAtIndex:indexPath.row];
    NSString* SIM2BBOrderRef = nil;
    NSString* primaryProductName = nil;
    NSString* primaryProductDescription = nil;
    NSString* primaryOrderStatus = nil;
    NSString* primaryProductIcon = nil;
    /* Logic to find BB orderReference we should identify by the flag IsFTTCSim2OrderSecondaryOli if it false it is BB orderReference / productType */
  
        for (BTProduct* prod in self.viewModel.arrayOfProductsNotRelatedToBundle)//BT Business Broadband
        {
            NSString *string = prod.productName;
            NSInteger productType = prod.productType;
            if ([string rangeOfString:@"Broadband"].location != NSNotFound || productType == 3) { //
                SIM2BBOrderRef = prod.orderItemReference;
                primaryProductName = prod.productName;
                primaryProductDescription = prod.productDescription;
                primaryOrderStatus = prod.status;
                primaryProductIcon = prod.productIcon;
                break;
            }
        }
    
    
    NSString *linkName = [NSString stringWithFormat:@"%@ %@",product.productName,OMNICLICK_ORDER_DETAILS];
    [self trackOmniClick:linkName forPage:OMNIPAGE_ORDER_SUMMARY];
    
    // (LP) Logic to go on different screens. If bundle order than go to bundle summary screen else go to order details scrren.
    if ([product.productIcon isEqualToString:@"bundleIcon"] || [product.productName isEqualToString:@"BT Business Bundle"]) {
        BTOrderTrackerBundleSummaryViewController *bundleOrderSummaryViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BundleOrderSummaryScene"];
        [bundleOrderSummaryViewController setOrderRef:_orderRef];
        [bundleOrderSummaryViewController setItemRef:product.orderItemReference];
        bundleOrderSummaryViewController.title = @"Bundle Order Summary";
        bundleOrderSummaryViewController.orderTrackerBundleSummaryViewControllerDelegate = self;
        bundleOrderSummaryViewController.isShowAppointment = self.viewModel.order.IsShowAppointment;
        [self.navigationController pushViewController:bundleOrderSummaryViewController animated:YES];
    } else {
        // (VRK) calling OrderDetailsMileStone Controller
        BTOrderDetailsMilestoneViewController *detailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailsMileStoneScene"];
        detailsViewController.delegate = self;
        [detailsViewController setOrderRef:_orderRef];
        if(product.productType !=3 ){
            detailsViewController.isSecondaryOrder = YES;
        } else {
            detailsViewController.isSecondaryOrder = NO;
        }
        [detailsViewController setItemRef:product.orderItemReference];
        [detailsViewController setProductName:product.productName];
        [detailsViewController setPrimaryProductName:primaryProductName];
        [detailsViewController setProductDescription:product.productDescription];
        [detailsViewController setPrimaryProductDescription:primaryProductDescription];
        [detailsViewController setProductIcon:product.productIcon];
        [detailsViewController setOrderStatus:product.status];
        [detailsViewController setPrimaryOrderStatus:primaryOrderStatus];
        [detailsViewController setPrimaryProductIcon:primaryProductIcon];
        [detailsViewController setIsShowAppointment:self.viewModel.order.IsShowAppointment];
        [detailsViewController setItemBBRef:SIM2BBOrderRef];
        
        [self.navigationController pushViewController:detailsViewController animated:YES];
    }
    
}

#pragma mark - Button Action methods

- (void) homeButtonAction {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelThisOrderButtonAction:(id)sender
{
    [self checkChatAvailabilityAction];
}

- (IBAction)userPressedOrderInlineButton:(id)sender {
    
    BTOrderTrackerGroupOrderSummaryViewController *groupOrderSummaryViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderTrackerGroupOrderSummaryViewController"];
    NSString *orderId = self.orderInlineButton.titleLabel.text;
    [groupOrderSummaryViewController setOrderRef:orderId];
    groupOrderSummaryViewController.title = @"Group Order Summary";
    [self.navigationController pushViewController:groupOrderSummaryViewController animated:YES];
}


#pragma mark - Private helper methods


/*
 Updates the UI after getting user details
 */

- (void)noOrderFoundViewWithTitle:(NSString *)titleText andNeedToShowImage:(BOOL)needToShowImage
{
    BTEmptyDashboardView *emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTEmptyDashboardView" owner:nil options:nil] objectAtIndex:0];
    emptyDashboardView.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (needToShowImage)
    {
        [emptyDashboardView updateEmptyDashboardViewWithImageName:@"package" title:titleText detailText:nil andButtonTitle:nil];
    }
    else
    {
        [emptyDashboardView updateEmptyDashboardViewWithImageName:nil title:titleText detailText:nil andButtonTitle:nil];
    }
    
    [self.view addSubview:emptyDashboardView];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:emptyDashboardView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:emptyDashboardView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:emptyDashboardView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}



- (void)updateUserInterface {
    //IsShowAppointment
    //Show or hide inline group order button
    if(self.viewModel.order.grouperOrderId && self.viewModel.order.grouperOrderId.length > 0)
    {

        [self.orderInlineButton setTitle:self.viewModel.order.grouperOrderId forState:UIControlStateNormal];
        
        //set the color of button for same state
        [self.orderInlineButton setTitleColor:[BrandColours colourTextBTPurplePrimaryColor] forState:UIControlStateHighlighted];
         [self.orderInlineButton setTitleColor:[BrandColours colourTextBTPurplePrimaryColor] forState:UIControlStateSelected];
        [self.orderInlineButton setTitleColor:[BrandColours colourTextBTPurplePrimaryColor] forState:UIControlStateNormal];
        [self.orderInlineButton setTitleColor:[BrandColours colourTextBTPurplePrimaryColor] forState:UIControlStateFocused];
        
    }
    else
    {
        [self hideGrouperInlineView];
    }
    
    if ([self.viewModel.order.orderStatus isEqualToString:@"Cancelled"] || [self.viewModel.order.orderStatus isEqualToString:@"Completed"]) {
        self.cancelThisOrderButton.hidden = YES;
    }
    
    self.orderDescriptionLabel.text = self.viewModel.order.orderDescription;

    [self.orderStatusLabel setOrderStatus:self.viewModel.order.orderStatus];
    if ([self.viewModel.order.orderStatus isEqualToString:@"Completed"]){
        self.expectedCompletionLabel.text = @"Completed on";
    } else {
        self.expectedCompletionLabel.text = @"Expected completion";
    }
    
   /* if ([AppManager isValidDate:self.viewModel.order.estimatedCompletionDate])
    {
         if (self.viewModel.order.IsShowAppointment == NO) {
             self.orderCompletionLabel.text = @"Will be updated shortly";
         } else {
             self.orderCompletionLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:self.viewModel.order.estimatedCompletionDate];
         }
    }
    else
    {
        self.orderCompletionLabel.text = @"-";
    }*/
    
    if (self.viewModel.order.IsShowAppointment == NO){
        self.orderCompletionLabel.text = @"Will be updated shortly";
    } else {
        if ([AppManager isValidDate:self.viewModel.order.estimatedCompletionDate]) {
            self.orderCompletionLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:self.viewModel.order.estimatedCompletionDate];
        }
        else {
            self.orderCompletionLabel.text = @"-";
        }
    }
    
    
    
    self.orderPlacedLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:self.viewModel.order.orderPlacedDate];
    
    [self.orderSummaryTableView reloadData];
    [self hideorShowControls:NO];
    [self hideRetryItems:YES];
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
}


- (void) hideorShowControls:(BOOL)isHide {
    
    // (lp) Hide UI elements during loading time
    [self.orderSummaryTopView setHidden:isHide];
    [self.orderSummaryDateContainerView setHidden:isHide];
    [self.orderSummaryImageView setHidden:isHide];
    [self.orderSummaryTableView setHidden:isHide];
}

- (void)showRetryWithInternetStrip:(BOOL)needToShowInternetStrip {
    
   if(!_retryView)
   {
       _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
       _retryView.translatesAutoresizingMaskIntoConstraints = NO;
       _retryView.retryViewDelegate = self;
       [_retryView updateRetryViewWithInternetStrip:needToShowInternetStrip];
       [self.view addSubview:_retryView];
       
       [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
       [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
       [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
       [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-10.0]];
   }
   
    _retryView.hidden = NO;
    _retryView.retryViewDelegate = self;
    [_retryView updateRetryViewWithInternetStrip:needToShowInternetStrip];
    
}

- (void)createLoadingView {
    
    
    if(_loadingView.isHidden == YES){
        return;
    }
    if(_loadingView){
        [_loadingView removeFromSuperview];
        _loadingView = nil;
    }
    _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
    _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
     [self.view addSubview:_loadingView];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}


- (void)hideRetryItems:(BOOL)isHide {
    
    // (lp) Hide or Show UI elements related to retry.
    [_retryView setRetryViewDelegate:nil];
    [_retryView setHidden:isHide];
    
}

- (void)hideLoadingItems:(BOOL)isHide {
    
    // (lp) Hide or Show UI elements related to loading.
    [_loadingView setHidden:isHide];
    
}


- (void)hideGrouperInlineView
{
    self.middleLabelHeightConstraint.constant = 0;
    self.orderDescriptionTopConstraint.constant = 0;
    self.inlineButtonView.hidden = YES;
    [self resizeTableViewHeaderToFit];
}


#pragma mark - Fetch user details methods
- (void)fetchOrderSummaryDetails {
    
    if(![AppManager isInternetConnectionAvailable])
    {
        [self showRetryWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_ORDER_SUMMARY];
    }
    else
    {
        // (LP) Ask to viewModel to provide order summary details. The viewModel takes care of fetching data.
        self.networkRequestInProgress = YES;
        [self hideLoadingItems:NO];
        [self hideRetryItems:YES];
        [_loadingView startAnimatingLoadingIndicatorView];
        [self.viewModel fetchOrderSummaryForOrderReference:self.orderRef];
    }
}



#pragma mark- BTOrderDetailsMilestoneViewControllerDelegate Method

- (void)btOrderDetailsMilestoneViewController:(BTOrderDetailsMilestoneViewController *)controller didChangeData:(BOOL)dataChangeStatus{
    
    if(dataChangeStatus == YES){
        
        [self fetchOrderSummaryDetails];
    }
    
}

- (void)btOrderTrackerBundleSummaryViewController:(BTOrderTrackerBundleSummaryViewController *)controller  didAmendData:(BOOL)isDataAmended{
    
    if(isDataAmended){
        
         [self fetchOrderSummaryDetails];
    }
}



#pragma mark - Event tracking methods
- (void)trackOmniPage
{
    NSMutableDictionary *contextDict = [NSMutableDictionary dictionary];
    
    NSString *orderRef = [OmnitureManager getOrderRefFormattedString:self.orderRef];
    [contextDict setValue:orderRef forKey:kOmniOrderRef];
    
    [OmnitureManager trackPage:OMNIPAGE_ORDER_SUMMARY withContextInfo:contextDict];
}

- (void)trackOmniClick:(NSString *)clickEvent forPage:(NSString *)page
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSString *orderRef = [OmnitureManager getOrderRefFormattedString:self.orderRef];
    [params setValue:orderRef forKey:kOmniOrderRef];
    [params addEntriesFromDictionary:[AppManager getOmniDictionary]];
    NSString *pageName = page;
    NSString *linkTitle = clickEvent;
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:params];
}


#pragma mark - DLMOrderSummaryScreenDelegate Methods

- (void)successfullyFetchedOrderSummaryDataOnOrderSummaryScreen:(DLMOrderSummaryScreen *)orderSummaryScreen
{
    self.networkRequestInProgress = NO;
    if (self.viewModel.order == nil) {
        [self hideRetryItems:NO];
        [self hideLoadingItems:YES];
        [_loadingView stopAnimatingLoadingIndicatorView];

    }
    else
    {
        if (self.viewModel.order.orderRef) {
            // (lp) Update UI with order summary data
            [self updateUserInterface];
            
        } else {
        
            // (lp) Retry to fetch order details
            [self hideRetryItems:NO];
            [self hideLoadingItems:YES];
            [_loadingView stopAnimatingLoadingIndicatorView];
            
        }
    }
}


- (void)orderSummaryScreen:(DLMOrderSummaryScreen *)orderSummaryScreen failedToFetchOrderSummaryDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    self.networkRequestInProgress = NO;
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
    
    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {
        
        switch (webServiceError.error.code)
        {
                
            case BTNetworkErrorCodeAPINoDataFound:
            {
                [AppManager trackNoDataFoundErrorOnPage:OMNIPAGE_ORDER_SUMMARY];
                [self noOrderFoundViewWithTitle:@"No order found" andNeedToShowImage:YES];
                errorHandled = YES;
                break;
            }
            case BTNetworkErrorCodeUnsupportedOrder:
            {
                [self noOrderFoundViewWithTitle:@"Sorry, we can't show you this order just yet" andNeedToShowImage:NO];
                errorHandled = YES;
                break;
            }
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }
    
    if(errorHandled == NO)
    {
        [self showRetryWithInternetStrip:NO];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_ORDER_SUMMARY];
    }
    
}

- (void)liveChatAvailableForOrderWithScreen:(DLMOrderSummaryScreen *)orderSummaryScreen
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
    
    viewController.targetURLType = BTTargetURLTypeOrdersChatScreen;
    
    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:NO andIsNeedCloseButton:YES];
    
    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{
        
    }];
}

- (void)orderSummaryScreen:(DLMOrderSummaryScreen *)orderSummaryScreen liveChatCurrentlyNotAvailableWithSpecificCategory:(NSArray *)specificCategory generalCategory:(NSArray *)generalCategory andCurrentTime:(NSString *)currentTime
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BTHelpAndSupportViewController *helpAndSupportController = [BTHelpAndSupportViewController getHelpAndSupportViewController];
    helpAndSupportController.isLaunchingFromHomeScreen = NO;
    helpAndSupportController.isDataAlreadyFetched = YES;
    helpAndSupportController.specificCategory = specificCategory;
    helpAndSupportController.generalCategory = generalCategory;
    helpAndSupportController.currentTime = currentTime;
    
    [self.navigationController pushViewController:helpAndSupportController animated:YES];
}

- (void)orderSummaryScreen:(DLMOrderSummaryScreen *)orderSummaryScreen failedToCheckLiveChatAvailabilityWithWebServiceError:(NLWebServiceError *)error
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if(errorHandled == NO)
    {
        [self showAlertWithTitle:@"Message" andMessage:kDefaultErrorMessage];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_ORDER_SUMMARY];
    }
    
}

#pragma mark - BTRetryView delegate methods
- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView {
    
    [self fetchOrderSummaryDetails];
    
}


@end
