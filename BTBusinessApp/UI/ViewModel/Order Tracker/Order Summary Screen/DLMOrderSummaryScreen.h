//
//  DLMOrderSummaryScreen.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 22/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"

@class DLMOrderSummaryScreen;
@class BTOrder;
@class NLWebServiceError;

@protocol DLMOrderSummaryScreenDelegate <NSObject>

- (void)successfullyFetchedOrderSummaryDataOnOrderSummaryScreen:(DLMOrderSummaryScreen *)orderSummaryScreen;

- (void)orderSummaryScreen:(DLMOrderSummaryScreen *)orderSummaryScreen failedToFetchOrderSummaryDataWithWebServiceError:(NLWebServiceError *)webServiceError;

- (void)liveChatAvailableForOrderWithScreen:(DLMOrderSummaryScreen *)orderSummaryScreen;
- (void)orderSummaryScreen:(DLMOrderSummaryScreen *)orderSummaryScreen liveChatCurrentlyNotAvailableWithSpecificCategory:(NSArray *)specificCategory generalCategory:(NSArray *)generalCategory andCurrentTime:(NSString *)currentTime;
- (void)orderSummaryScreen:(DLMOrderSummaryScreen *)orderSummaryScreen failedToCheckLiveChatAvailabilityWithWebServiceError:(NLWebServiceError *)error;

@end

@interface DLMOrderSummaryScreen : DLMObject {
    
}

@property (nonatomic, readonly) BTOrder *order;
@property (nonatomic, copy) NSArray *arrayOfProductsRelatedToBundle;
@property (nonatomic, copy) NSArray *arrayOfProductsNotRelatedToBundle;
@property (nonatomic, weak) id <DLMOrderSummaryScreenDelegate> orderSummaryScreenDelegate;

- (void)fetchOrderSummaryForOrderReference:(NSString *)orderRef;
- (void)cancelFetchOrderSummaryAPICall;
- (void)cancelChatAvailabilityCheckerAPI;

- (void)checkForLiveChatAvailibilityForOrder;

@end
