//
//  BTOrderTrackerDetailsViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 23/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTOrderTrackerDetailsViewController : UIViewController

@property(nonatomic,assign)BOOL isFromAPNS;
@property(nonatomic,strong)NSString *orderRef;
@property (nonatomic) BOOL networkRequestInProgress;
@property(nonatomic,assign)BOOL isSIM2Order;
@end
