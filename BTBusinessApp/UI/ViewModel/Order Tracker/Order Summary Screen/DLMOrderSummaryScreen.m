//
//  DLMOrderSummaryScreen.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 22/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMOrderSummaryScreen.h"
#import "NLGetOrderSummaryWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "BTOrder.h"
#import "BTProduct.h"
#import "CDRecentSearchedOrder.h"
#import "NLWebServiceError.h"
#import "BTLiveChatAvailabilityChecker.h"

@interface DLMOrderSummaryScreen () <NLGetOrderSummaryWebServiceDelegate, BTLiveChatAvailabilityCheckerDelegate> {
    
    BTLiveChatAvailabilityChecker *_liveChatAvailabityChecker;

}

@property (nonatomic) NLGetOrderSummaryWebService *getOrderSummaryWebService;

@end

@implementation DLMOrderSummaryScreen

- (void)fetchOrderSummaryForOrderReference:(NSString *)orderRef {
    
    [self setupToFetchOrderSummaryWithOrderRef:orderRef];
    [self fetchOrderSummaryForCurrentOrder];
}

- (void)setupToFetchOrderSummaryWithOrderRef:(NSString *)orderRef {
    
    DDLogInfo(@"Fetching order summary data from server for order ref %@", orderRef);
    self.getOrderSummaryWebService = [[NLGetOrderSummaryWebService alloc] initWithOrderRef:orderRef];
    self.getOrderSummaryWebService.getOrderSummaryWebServiceDelegate = self;
}

- (void)fetchOrderSummaryForCurrentOrder {
    
    [self.getOrderSummaryWebService resume];
}

- (void)checkForLiveChatAvailibilityForOrder
{
    [self setupToCheckLiveChatAvailability];
    [self getAvailableSlotsForChat];
}

- (void)setupToCheckLiveChatAvailability
{
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = [[BTLiveChatAvailabilityChecker alloc] init];
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = self;
}

- (void)getAvailableSlotsForChat
{
    [_liveChatAvailabityChecker getLiveChatAvailableSlots];
}

- (void)cancelFetchOrderSummaryAPICall
{
    self.getOrderSummaryWebService.getOrderSummaryWebServiceDelegate = nil;
    [self.getOrderSummaryWebService cancel];
    self.getOrderSummaryWebService = nil;
    
}

- (void)cancelChatAvailabilityCheckerAPI
{
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    [_liveChatAvailabityChecker cancel];
    _liveChatAvailabityChecker = nil;
}

#pragma mark - Private helper methods

- (void)getBundleRelatedInfoFromOrder {
    NSMutableArray *productsRelatedToBundle = [NSMutableArray array];
    NSMutableArray *productsNotRelatedToBundle = [NSMutableArray array];
    
    for (BTProduct *product in _order.arrayOfProducts) {
        if ((!product.isBundleProduct) || (product.isBundleProduct && product.bundleReference && ![product.bundleReference isEqualToString:product.orderKey]))
        {
            [productsNotRelatedToBundle addObject:product];
        }
        else
        {
            [productsRelatedToBundle addObject:product];
        }
    }
    _arrayOfProductsRelatedToBundle = [NSArray arrayWithArray:productsRelatedToBundle];
    _arrayOfProductsNotRelatedToBundle = [NSArray arrayWithArray:productsNotRelatedToBundle];
}


#pragma mark - NLGetOrderSummaryWebServiceDelegate Methods

- (void)getOrderSummaryWebService:(NLGetOrderSummaryWebService *)webService successfullyFetchedOrderSummaryData:(BTOrder *)order
{
    if(webService == _getOrderSummaryWebService) {
        if (order != nil) {
            // (LP) Save the latest data from web service into the persistence.
            
            DDLogInfo(@"Saving data from response of NLGetOrderSummaryWebServiceDelegate API Call into persistence in recent searched items for order %@.", order.orderRef);
            
            NSManagedObjectContext *context = [AppDelegate sharedInstance].managedObjectContext;
            CDUser *persistenceUser = [CDUser userWithUsername:[AppDelegate sharedInstance].viewModel.app.loggedInUser.username inManagedObjectContext:context];
            
            NSArray *arrayOfRecentSearchedOrders = [persistenceUser.recentlySearchedOrders allObjects];
            
            CDRecentSearchedOrder *searchedOrder = nil;
            for (CDRecentSearchedOrder *recentSearchedOrder in arrayOfRecentSearchedOrders) {
                if ([recentSearchedOrder.orderRef isEqualToString:order.orderRef]) {
                    searchedOrder = recentSearchedOrder;
                }
            }
            
            if (searchedOrder == nil) {
                searchedOrder = [CDRecentSearchedOrder newRecentSearchedOrderInManagedObjectContext:context];
                searchedOrder.orderRef = order.orderRef;
                searchedOrder.orderStatus = order.orderStatus;
                searchedOrder.orderDescription = order.orderDescription;
                searchedOrder.completionDate = order.estimatedCompletionDate;
                searchedOrder.placedOnDate = order.orderPlacedDate;
                searchedOrder.lastSearchedDate = [NSDate date];
                
                [persistenceUser addRecentlySearchedOrdersObject:searchedOrder];
            }
            else
            {
                searchedOrder.orderStatus = order.orderStatus;
                searchedOrder.completionDate = order.estimatedCompletionDate;
                searchedOrder.lastSearchedDate = [NSDate date];
                
                [persistenceUser addRecentlySearchedOrdersObject:searchedOrder];
            }
            
            [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
            
            _order = order;
            
            [self getBundleRelatedInfoFromOrder];
        }
        
        [self.orderSummaryScreenDelegate successfullyFetchedOrderSummaryDataOnOrderSummaryScreen:self];
        _getOrderSummaryWebService.getOrderSummaryWebServiceDelegate = nil;
        _getOrderSummaryWebService = nil;
        
    } else {
        DDLogError(@"This delegate method gets called because of success of an object of NLGetOrderSummaryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLGetOrderSummaryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}


- (void)getOrderSummaryWebService:(NLGetOrderSummaryWebService *)webService failedToFetchOrderSummaryDataWithWebServiceError:(NLWebServiceError *)error
{
    if(webService == _getOrderSummaryWebService) {
        
        [self.orderSummaryScreenDelegate orderSummaryScreen:self failedToFetchOrderSummaryDataWithWebServiceError:error];
        _getOrderSummaryWebService.getOrderSummaryWebServiceDelegate = nil;
        _getOrderSummaryWebService = nil;
        
    } else {
        
        DDLogError(@"This delegate method gets called because of failure of an object of NLGetOrderSummaryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLGetOrderSummaryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

#pragma mark - BTLiveChatAvailabilityCheckerDelegate Methods
- (void)liveChatAvailabilityChecker:(BTLiveChatAvailabilityChecker *)liveChatAvailabilityChecker successfullyFetchedSlotsWithGeneralCategory:(NSArray *)generalCategoryArray specificeCategory:(NSArray *)specificCategory andCurrentTime:(NSString *)currentTime
{
    BOOL chatAvailable = [BTLiveChatAvailabilityChecker checkAvailabilityWithSpecificCategory:specificCategory currentTime:currentTime andChatType:BTChatTypeOrder];
    
    if (chatAvailable)
    {
        [self.orderSummaryScreenDelegate liveChatAvailableForOrderWithScreen:self];
    }
    else
    {
        [self.orderSummaryScreenDelegate orderSummaryScreen:self liveChatCurrentlyNotAvailableWithSpecificCategory:specificCategory generalCategory:generalCategoryArray andCurrentTime:currentTime];
    }
    
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = nil;
}

- (void)liveChatAvailabilityChecker:(BTLiveChatAvailabilityChecker *)liveChatAvailabilityChecker failedToFetchSlotsWithWebServiceError:(NLWebServiceError *)webServiceError
{
    [self.orderSummaryScreenDelegate orderSummaryScreen:self failedToCheckLiveChatAvailabilityWithWebServiceError:webServiceError];
    
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = nil;
}


@end
