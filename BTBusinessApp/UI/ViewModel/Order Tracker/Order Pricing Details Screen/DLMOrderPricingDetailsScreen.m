//
//  DLMOrderPricingDetailsScreen.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 10/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMOrderPricingDetailsScreen.h"
#import "BTBillingDetails.h"
#import "BTPricingDetails.h"


#define kTerminationChargeQuantity 1

@implementation DLMOrderPricingDetailsScreen

- (void)updateWithBillingDetails:(BTBillingDetails *)billingDetails andPricingDetails:(BTPricingDetails *)pricingDetails
{
    _pricingDetails = pricingDetails;
    _billingDetails = billingDetails;
    
    if ((billingDetails.billingAddress != nil) || (billingDetails.billFrequency != nil) || (billingDetails.billType)) {
        _isBillingDetailsAvailable = YES;
    }
    
    NSMutableArray *arrayOfpricingDetail = [NSMutableArray array];
    
    NSArray *parentOrderitems = [NSArray arrayWithObject:pricingDetails.parentOrderItem];
    
    if([parentOrderitems count]>0)
    {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setValue:parentOrderitems forKey:kDataKey];
    [dict setValue:@"Services" forKey:kSectionTitleKey];
    
    [arrayOfpricingDetail addObject:dict];
    }
    
    
    //(RLM) 15 feb 2017 - First we check if isVisible key is yes in Order items Dictionary then only we add it to array
    //Check for order items
    NSMutableArray *orderItemArray =  [[NSMutableArray alloc] init];
    [orderItemArray removeAllObjects];
    for(NSDictionary *orderItemDict in pricingDetails.orderItems)
    {
       BOOL isOrderItemVisible = [[orderItemDict valueForKey:@"IsVisible"] boolValue];
        
        if(isOrderItemVisible)
        {
            [orderItemArray addObject:orderItemDict];
        }
    }
    
   //Check if orderItemArray contain any element or no
    if([orderItemArray count]>0)
    {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setValue:orderItemArray forKey:kDataKey];
    [dict setValue:@"Extra items" forKey:kSectionTitleKey];
    [arrayOfpricingDetail addObject:dict];
    }
    
    
    if(pricingDetails.journeyType == 2){
        
        //Cease order
        NSMutableArray *terminationChargeArray = [NSMutableArray array];
        
        
        NSMutableDictionary *terminationCharge = [NSMutableDictionary dictionary];
        [terminationCharge setValue:[NSNumber numberWithDouble:0.0] forKey:@"MonthlyPrice"];
        [terminationCharge setValue:[NSNumber numberWithDouble:pricingDetails.totalTerminationCharge] forKey:@"OneOffPrice"];
        [terminationCharge setValue:[NSString stringWithFormat:@"%d", kTerminationChargeQuantity]  forKey:@"Quantity"];
        [terminationCharge setValue:@"Termination charge" forKey:@"DisplayName"];
        [terminationChargeArray addObject:terminationCharge];
        
        NSMutableDictionary *httpCharge = [NSMutableDictionary dictionary];
        [httpCharge setValue:[NSNumber numberWithDouble:0.0] forKey:@"MonthlyPrice"];
        [httpCharge setValue:[NSNumber numberWithDouble:pricingDetails.totalHttpCharge] forKey:@"OneOffPrice"];
        [httpCharge setValue:[NSString stringWithFormat:@"%d", kTerminationChargeQuantity] forKey:@"Quantity"];
        [httpCharge setValue:@"Early termination charge" forKey:@"DisplayName"];
        
        [terminationChargeArray addObject:httpCharge];
        
        
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setValue:terminationChargeArray forKey:kDataKey];
        [dict setValue:@"Termination charge" forKey:kSectionTitleKey];
        [arrayOfpricingDetail addObject:dict];
       
        
        double totalOneOffCharge = kTerminationChargeQuantity*pricingDetails.totalHttpCharge + kTerminationChargeQuantity*pricingDetails.totalTerminationCharge;
     
        self.totalOneOffCharge = [NSString stringWithFormat:@"£%.2f", totalOneOffCharge];        
    }

    _arrayOfPricingDetails = arrayOfpricingDetail;
}

@end
