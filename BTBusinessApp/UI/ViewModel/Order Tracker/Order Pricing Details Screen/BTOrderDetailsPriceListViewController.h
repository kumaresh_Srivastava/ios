//
//  BTOrderDetailsPriceListViewController.h
//  BTBusinessApp
//
//  Created by Accolite on 17/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTBillingDetails;
@class BTPricingDetails;

@interface BTOrderDetailsPriceListViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *priceListTableView;

@property (nonatomic,strong) NSString *orderID;
@property (nonatomic,strong) BTBillingDetails *billingDetails;
@property (nonatomic,strong) BTPricingDetails *pricingDetails;
@property (nonatomic, assign) BOOL isRelatedOrderStrategic;

@end
