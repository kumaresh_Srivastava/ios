//
//  BTOrderDetailsPriceListViewController.m
//  BTBusinessApp
//
//  Created by Accolite on 17/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderDetailsPriceListViewController.h"
#import "BTOrderDetailsPriceListTableViewCell.h"
#import "AppConstants.h"
#import "BTCustomDisplayView.h"
#import "BTPricingDetails.h"
#import "BTBillingDetails.h"
#import "DLMOrderPricingDetailsScreen.h"

@interface BTOrderDetailsPriceListViewController () <UITableViewDelegate, UITableViewDataSource> {
    
}

@property (nonatomic, readwrite) DLMOrderPricingDetailsScreen *viewModel;

@property (weak, nonatomic) IBOutlet UILabel *OrderIdValue;
@property (weak, nonatomic) IBOutlet UILabel *oneOffLabel;
@property (weak, nonatomic) IBOutlet UILabel *regularLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *totalViewHeightConstraint;

@property (weak, nonatomic) IBOutlet UIView *totalView;
@end

@implementation BTOrderDetailsPriceListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // (LP) View model for pricing details screen
    self.viewModel = [[DLMOrderPricingDetailsScreen alloc] init];
    [self.viewModel updateWithBillingDetails:_billingDetails andPricingDetails:_pricingDetails];
   
    
    // (LP) Manually dealloc
    _billingDetails = nil;
    _pricingDetails = nil;
    
    if(self.viewModel.pricingDetails.regularChargesView)
        self.regularLabel.text = self.viewModel.pricingDetails.regularChargesView;
    else
        self.regularLabel.text = @"-";
    
    self.title = @"Billing details";
    
    UINib *dataCell = [UINib nibWithNibName:@"BTOrderDetailsPriceListTableViewCell" bundle:nil];
    [self.priceListTableView registerNib:dataCell forCellReuseIdentifier:@"BTOrderDetailsPriceTableViewCell"];
    [self.OrderIdValue setText:self.orderID];
    self.priceListTableView.delegate = self;
    self.priceListTableView.dataSource = self;
    self.priceListTableView.estimatedRowHeight = 73;
    self.priceListTableView.rowHeight = UITableViewAutomaticDimension;
    self.priceListTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    if (self.viewModel.isBillingDetailsAvailable) {
        
        [self addheaderToTableView];
    }
    [self addNoPriceDetailsView];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TableViewDelegate & DataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BTOrderDetailsPriceListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BTOrderDetailsPriceTableViewCell" forIndexPath:indexPath];
    if(self.viewModel.pricingDetails.regularChargesView && self.viewModel.pricingDetails.regularChargesView.length>0)
        cell.regularViewString = [self.viewModel.pricingDetails.regularChargesView lowercaseString];
    else
        cell.regularViewString = @"-";
    
    NSDictionary *dict = [self.viewModel.arrayOfPricingDetails objectAtIndex:indexPath.section];
    NSArray *dataArray = [dict valueForKey:kDataKey];
    
    NSDictionary *rowDict = [dataArray objectAtIndex:indexPath.row];
    [cell setValuesWithDictionary:rowDict andFrequency:self.viewModel.pricingDetails.regularChargesView];
  
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSDictionary *dict = [self.viewModel.arrayOfPricingDetails objectAtIndex:section];
    NSArray *dataArray = [dict valueForKey:kDataKey];

     return [dataArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.viewModel.arrayOfPricingDetails.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}


- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle = @"";

    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 40)];
    [headerView setBackgroundColor:[UIColor colorForHexString:@"eeeeee"]];//[BrandColours colourBtNeutral30]];

    NSDictionary *dict = [self.viewModel.arrayOfPricingDetails objectAtIndex:section];
    
    if([dict isKindOfClass:[NSDictionary class]])
    {
        sectionTitle = [dict valueForKey:kSectionTitleKey];
    }

//    UIView *topSepView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, headerView.frame.size.width, 1)];
//    topSepView.backgroundColor = [BrandColours colourBtNeutral50];
//    [headerView addSubview:topSepView];

    UILabel *headerTitleLable = [[UILabel alloc] initWithFrame:CGRectMake(15, 10,tableView.bounds.size.width - 30, 20)];
    headerTitleLable.text = sectionTitle;
     headerTitleLable.textColor = [UIColor colorForHexString:@"333333"];//[BrandColours colourBtNeutral70];
    headerTitleLable.font = [UIFont fontWithName:@"BTFont-Bold" size:14];
    headerTitleLable.backgroundColor = [UIColor clearColor];
    [headerView addSubview:headerTitleLable];

//    UIView *bottomSepView = [[UIView alloc] initWithFrame:CGRectMake(0, headerView.frame.size.height - 1, headerView.frame.size.width, 1)];
//    bottomSepView.backgroundColor = [BrandColours colourBtNeutral50];
//    [headerView addSubview:bottomSepView];

    return headerView;
}



#pragma mark PrivateHelperMethods

- (NSMutableAttributedString *)getAttributedStringFromString1:(NSString *)firstString String2:(NSString *)secondString {
    
    UIFont *firstStringFont = [UIFont fontWithName:@"BTFont-Bold" size:18.0];
    NSMutableDictionary *firstStringDict = [NSMutableDictionary dictionaryWithObject:firstStringFont forKey:NSFontAttributeName];
    [firstStringDict setObject:[UIColor colorForHexString:@"e60050"] forKey:NSForegroundColorAttributeName];
    NSMutableAttributedString *firstAttrString = [[NSMutableAttributedString alloc] initWithString:firstString attributes:firstStringDict];
    
    UIFont *secondStringFont = [UIFont fontWithName:kBtFontRegular size:14.0];
    NSMutableDictionary *secondStringDict = [NSMutableDictionary dictionaryWithObject:secondStringFont forKey:NSFontAttributeName];
    [secondStringDict setObject:[UIColor colorForHexString:@"666666"] forKey:NSForegroundColorAttributeName];
    NSMutableAttributedString *secondAttrString = [[NSMutableAttributedString alloc]initWithString:secondString attributes:secondStringDict];
    
    [firstAttrString appendAttributedString:secondAttrString];
    return firstAttrString;
}

#pragma mark - UI

- (void) addNoPriceDetailsView {
    if (self.viewModel.arrayOfPricingDetails.count == 0) {
        self.totalView.hidden = YES;
        BTCustomDisplayView *customDispalyView = [[[NSBundle mainBundle] loadNibNamed:@"BTCustomDisplayView" owner:nil options:nil] objectAtIndex:0];
        customDispalyView.translatesAutoresizingMaskIntoConstraints = NO;
        [customDispalyView.displayLabel setText:@"Pricing details are not available."];
        [self.view addSubview:customDispalyView];
        
        NSDictionary *customViews = @{@"customDispalyView":customDispalyView};
        //        [phoneServicesScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[customDispalyView]-|" options:0 metrics:nil views:customViews]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:customDispalyView
                                                                            attribute:NSLayoutAttributeCenterY
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.view
                                                                            attribute:NSLayoutAttributeCenterY
                                                                           multiplier:1.0
                                                                             constant:0.0]];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[customDispalyView]-|" options:0 metrics:nil views:customViews]];

    } else {
        self.totalView.hidden = NO;
        [self setValuesToTotalView];
    }
}

- (void) setValuesToTotalView {
    
    NSString *totaloneoffString = [NSString stringWithFormat:@"£%.2f", self.viewModel.pricingDetails.totalOneOffPrice];
    NSString *totalMonthlyOrQuarterlyString;
    if ([self.viewModel.pricingDetails.regularChargesView isEqualToString:@"Quarterly"]) {
        totalMonthlyOrQuarterlyString = [NSString stringWithFormat:@"£%.2f", self.viewModel.pricingDetails.totalQuarterlyPrice];
    }
    else {
        totalMonthlyOrQuarterlyString = [NSString stringWithFormat:@"£%.2f", self.viewModel.pricingDetails.totalMonthlyPrice];
    }
    
    if(self.viewModel.pricingDetails.journeyType == 2){
        
        totaloneoffString = self.viewModel.totalOneOffCharge;
    }
    
    self.oneOffLabel.attributedText = [self getAttributedStringFromString1:totaloneoffString String2:@" one-off"];
    NSString *regularText;
   if(self.viewModel.pricingDetails.regularChargesView && self.viewModel.pricingDetails.regularChargesView.length>0)
        regularText = [self.viewModel.pricingDetails.regularChargesView lowercaseString];
    else
        regularText = @"-";
    self.regularLabel.attributedText = [self getAttributedStringFromString1:totalMonthlyOrQuarterlyString String2:[NSString stringWithFormat:@" %@",regularText]];
}

- (void) addFooterToTableView {
    
    UIView *footerView = [[UIView alloc] init];
    [footerView setBackgroundColor:[UIColor clearColor]];
    
    UIImageView *footerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BOT-RoundRectangle"]];
    footerImageView.translatesAutoresizingMaskIntoConstraints = NO;
    [footerView addSubview:footerImageView];
    
    UIView *footerTotalView = [[UIView alloc] init];
    footerTotalView.translatesAutoresizingMaskIntoConstraints = NO;
    [footerTotalView setBackgroundColor:[UIColor whiteColor]];
    [footerView addSubview:footerTotalView];
    
    UILabel *totalLabel = [[UILabel alloc] init];
    totalLabel.translatesAutoresizingMaskIntoConstraints = NO;
    totalLabel.text = @"Total";
    [totalLabel setFont:[UIFont fontWithName:kBtFontRegular size:17]];
    [footerTotalView addSubview:totalLabel];
    //
    UILabel *oneoffTotalLabel = [[UILabel alloc] init];
    oneoffTotalLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSString *totaloneoffString = [NSString stringWithFormat:@"£ %.2f", self.viewModel.pricingDetails.totalOneOffPrice];
    [oneoffTotalLabel setFont:[UIFont fontWithName:kBtFontRegular size:17]];
    oneoffTotalLabel.text = totaloneoffString;
    [footerTotalView addSubview:oneoffTotalLabel];
    
    UILabel *monthlyTotalLabel = [[UILabel alloc] init];
    monthlyTotalLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSString *totalMonthlyString = [NSString stringWithFormat:@"£ %.2f", self.viewModel.pricingDetails.totalMonthlyPrice];
    [monthlyTotalLabel setFont:[UIFont fontWithName:kBtFontRegular size:17]];
    monthlyTotalLabel.text = totalMonthlyString;
    [footerTotalView addSubview:monthlyTotalLabel];
    
    NSDictionary *headerViews = @{@"footerImageView":footerImageView,
                                  @"totalView":footerTotalView,
                                  @"totalLabel":totalLabel,
                                  @"oneoffTotalLabel":oneoffTotalLabel,
                                  @"monthlyTotalLabel":monthlyTotalLabel
                                  };
    [footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[footerImageView]|" options:0 metrics:nil views:headerViews]];
    
    [footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(15)-[totalLabel]" options:0 metrics:nil views:headerViews]];
    [footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[footerImageView(8)]-15-[totalView(30)]" options:0 metrics:nil views:headerViews]];
    [footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[totalView]|" options:0 metrics:nil views:headerViews]];
    
    [footerTotalView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[totalLabel(21)]" options:0 metrics:nil views:headerViews]];
    
    [footerTotalView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[oneoffTotalLabel(21)]" options:0 metrics:nil views:headerViews]];
    [footerTotalView addConstraint:[NSLayoutConstraint constraintWithItem:oneoffTotalLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:footerTotalView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:1.0]];
    
    [footerTotalView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[monthlyTotalLabel]-|" options:0 metrics:nil views:headerViews]];
    [footerTotalView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[monthlyTotalLabel(21)]" options:0 metrics:nil views:headerViews]];
    
    float height = 2*8 + 21 + 8 + 15;
    [footerView setFrame:CGRectMake(0, 0, self.view.frame.size.width, height)];
    [self.priceListTableView setTableFooterView:footerView];
}

- (void) addheaderToTableView {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor whiteColor];  
    float height = 100;

    if(self.isRelatedOrderStrategic)
        height = 224;

    [headerView setFrame:CGRectMake(0, 0, self.view.frame.size.width, height)];
    
    NSDictionary *addressDict = self.viewModel.billingDetails.billingAddress;
    
    NSMutableArray *addressStringsArray = [NSMutableArray array];
    
    if (!([addressDict valueForKey:@"BuildingNumber"] == nil || ([[addressDict valueForKey:@"BuildingNumber"] isEqualToString:@""]))) {
        [addressStringsArray addObject:[addressDict valueForKey:@"BuildingNumber"]];
    }
    if (!([addressDict valueForKey:@"BuildingName"] == nil || ([[addressDict valueForKey:@"BuildingName"] isEqualToString:@""]))) {
        [addressStringsArray addObject:[addressDict valueForKey:@"BuildingName"]];
    }
    if (!([addressDict valueForKey:@"ThoroughfareName"] == nil || ([[addressDict valueForKey:@"ThoroughfareName"] isEqualToString:@""]))) {
        [addressStringsArray addObject:[addressDict valueForKey:@"ThoroughfareName"]];
    }
    if (!([addressDict valueForKey:@"PostTown"] == nil || ([[addressDict valueForKey:@"PostTown"] isEqualToString:@""]))) {
        [addressStringsArray addObject:[addressDict valueForKey:@"PostTown"]];
    }
    if (!([addressDict valueForKey:@"County"] == nil || ([[addressDict valueForKey:@"County"] isEqualToString:@""]))) {
        [addressStringsArray addObject:[addressDict valueForKey:@"County"]];
    }
    if (!([addressDict valueForKey:@"Postcode"] == nil || ([[addressDict valueForKey:@"Postcode"] isEqualToString:@""]))) {
        [addressStringsArray addObject:[addressDict valueForKey:@"Postcode"]];
    }
    
    NSString *addressString = [addressStringsArray componentsJoinedByString:@", "];
    
    UILabel *billingTypeHeaderLabel = [[UILabel alloc] init];
    billingTypeHeaderLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [billingTypeHeaderLabel setText:@"Type"];
    [billingTypeHeaderLabel setFont:[UIFont fontWithName:@"BTFont-Regular" size:13]];
    UILabel *billingTypeValueLabel = [[UILabel alloc] init];
    billingTypeValueLabel.translatesAutoresizingMaskIntoConstraints = NO;
    if ([self.viewModel.billingDetails.billType isEqualToString:@"Paper Free"]) {
        [billingTypeValueLabel setText:@"Paper-free"];
    } else {
        [billingTypeValueLabel setText:self.viewModel.billingDetails.billType];
    }
    [billingTypeValueLabel setFont:[UIFont fontWithName:@"BTFont-Regular" size:20]];
    
    UILabel *paymentMethodHeaderLabel = [[UILabel alloc] init];
    paymentMethodHeaderLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [paymentMethodHeaderLabel setText:@"Method"];
    [paymentMethodHeaderLabel setFont:[UIFont fontWithName:@"BTFont-Regular" size:13]];
    UILabel *paymentMethodValueLabel = [[UILabel alloc] init];
    paymentMethodValueLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [paymentMethodValueLabel setText:self.viewModel.billingDetails.paymentMethod];
    [paymentMethodValueLabel setFont:[UIFont fontWithName:@"BTFont-Regular" size:20]];
// add a separator line
    UILabel *separatorlineOne = [[UILabel alloc] init];
    separatorlineOne.translatesAutoresizingMaskIntoConstraints = NO;
    separatorlineOne.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0];
// end add separator line
    UILabel *billFrequencyHeaderLabel = [[UILabel alloc] init];
    billFrequencyHeaderLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [billFrequencyHeaderLabel setText:@"Frequency"];
    [billFrequencyHeaderLabel setFont:[UIFont fontWithName:@"BTFont-Regular" size:13]];
    UILabel *billFrequencyValueLabel = [[UILabel alloc] init];
    billFrequencyValueLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [billFrequencyValueLabel setText:self.viewModel.billingDetails.billFrequency];
    [billFrequencyValueLabel setFont:[UIFont fontWithName:@"BTFont-Regular" size:20]];
// add a separator line
    UILabel *separatorlineTwo = [[UILabel alloc] init];
    separatorlineTwo.translatesAutoresizingMaskIntoConstraints = NO;
    separatorlineTwo.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0];
// end add separator line
    UILabel *billingAddressHeaderLabel = [[UILabel alloc] init];
    billingAddressHeaderLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [billingAddressHeaderLabel setText:@"Address"];
    [billingAddressHeaderLabel setFont:[UIFont fontWithName:@"BTFont-Regular" size:13]];
    UILabel *billingAddressValueLabel = [[UILabel alloc] init];
    billingAddressValueLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [billingAddressValueLabel setText:addressString];
    [billingAddressValueLabel setFont:[UIFont fontWithName:@"BTFont-Regular" size:20]];
    billingAddressValueLabel.lineBreakMode = NSLineBreakByWordWrapping;
    billingAddressValueLabel.numberOfLines = 0;

    if(self.isRelatedOrderStrategic) {

    [headerView addSubview:billingTypeHeaderLabel];
    [headerView addSubview:billingTypeValueLabel];
    [headerView addSubview:paymentMethodHeaderLabel];
    [headerView addSubview:paymentMethodValueLabel];
    [headerView addSubview:separatorlineOne];
    [headerView addSubview:billFrequencyHeaderLabel];
    [headerView addSubview:billFrequencyValueLabel];
     
    [headerView addSubview:separatorlineTwo];
        
    [headerView addSubview:billingAddressHeaderLabel];
    [headerView addSubview:billingAddressValueLabel];
    
    NSDictionary *headerViews = @{@"billingTypeHeaderLabel":billingTypeHeaderLabel,
                                  @"billingTypeValueLabel":billingTypeValueLabel,
                                  @"paymentMethodHeaderLabel":paymentMethodHeaderLabel,
                                  @"paymentMethodValueLabel":paymentMethodValueLabel,
                                  @"separatorlineOne":separatorlineOne,
                                  @"billFrequencyHeaderLabel":billFrequencyHeaderLabel,
                                  @"billFrequencyValueLabel":billFrequencyValueLabel,
                                  @"separatorlineTwo":separatorlineTwo,
                                  @"billingAddressHeaderLabel":billingAddressHeaderLabel,
                                  @"billingAddressValueLabel":billingAddressValueLabel
                                  };
    
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[billingTypeHeaderLabel(140)]" options:0 metrics:nil views:headerViews]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[billingTypeValueLabel(140)]" options:0 metrics:nil views:headerViews]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[paymentMethodHeaderLabel(140)]-15-|" options:0 metrics:nil views:headerViews]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[paymentMethodValueLabel(140)]-15-|" options:0 metrics:nil views:headerViews]];
        
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[separatorlineOne]-0-|" options:0 metrics:nil views:headerViews]];
        
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[billFrequencyHeaderLabel(140)]" options:0 metrics:nil views:headerViews]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[billFrequencyValueLabel(140)]" options:0 metrics:nil views:headerViews]];
        
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[separatorlineTwo]-0-|" options:0 metrics:nil views:headerViews]];
        
        
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[billingAddressHeaderLabel(140)]" options:0 metrics:nil views:headerViews]];
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[billingAddressValueLabel]-15-|" options:0 metrics:nil views:headerViews]];

    
    [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-15-[paymentMethodHeaderLabel(21)][paymentMethodValueLabel(21)]-10-[separatorlineOne(1)]" options:0 metrics:nil views:headerViews]];
    //[headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-15-[billingTypeHeaderLabel(21)][billingTypeValueLabel(21)]-15-[billFrequencyHeaderLabel(21)][billFrequencyValueLabel(21)]-15-[billingAddressHeaderLabel(21)][billingAddressValueLabel(53)]" options:0 metrics:nil views:headerViews]];
        
         [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-15-[billingTypeHeaderLabel(21)][billingTypeValueLabel(21)]-20-[billFrequencyHeaderLabel(21)][billFrequencyValueLabel(21)]-13-[separatorlineTwo(1)]-15-[billingAddressHeaderLabel(21)][billingAddressValueLabel(53)]" options:0 metrics:nil views:headerViews]];
    }
    else {

        [headerView addSubview:billingAddressHeaderLabel];
        [headerView addSubview:billingAddressValueLabel];

        NSDictionary *headerViews = @{@"billingAddressHeaderLabel":billingAddressHeaderLabel,
                                      @"billingAddressValueLabel":billingAddressValueLabel
                                      };

        [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[billingAddressHeaderLabel(140)]" options:0 metrics:nil views:headerViews]];
        [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[billingAddressValueLabel]-15-|" options:0 metrics:nil views:headerViews]];

        [headerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-15-[billingAddressHeaderLabel(21)][billingAddressValueLabel(53)]" options:0 metrics:nil views:headerViews]];



    }
    [self.priceListTableView setTableHeaderView:headerView];

}

@end
