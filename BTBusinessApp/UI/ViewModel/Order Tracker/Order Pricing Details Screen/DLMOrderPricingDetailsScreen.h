//
//  DLMOrderPricingDetailsScreen.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 10/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"

#define kSectionTitleKey @"SectionTitle"
#define kDataKey @"TableData"

@class BTBillingDetails;
@class BTPricingDetails;

@interface DLMOrderPricingDetailsScreen : DLMObject

@property (nonatomic, assign) BOOL isBillingDetailsAvailable;
@property (nonatomic, strong) BTBillingDetails *billingDetails;
@property (nonatomic, strong) BTPricingDetails *pricingDetails;
@property (nonatomic, strong) NSString *totalOneOffCharge;


// (LP) Array to keep all pricing details together.
@property (nonatomic, copy) NSArray *arrayOfPricingDetails;

- (void)updateWithBillingDetails:(BTBillingDetails *)billingDetails andPricingDetails:(BTPricingDetails *)pricingDetails;


@end
