//
//  BTOrderSearchHelpViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 03/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderSearchHelpViewController.h"

@interface BTOrderSearchHelpViewController ()
@property (nonatomic, retain) UIView *statusBarView;

@end

@implementation BTOrderSearchHelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self trackOmniPage];
    // Do any additional setup after loading the view.


    [self createAndAddCustomStatusBarView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Private methods

- (void)createAndAddCustomStatusBarView
{
    _statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 20)];
    _statusBarView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:_statusBarView];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view
                                                                         attribute:NSLayoutAttributeTop
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:_statusBarView
                                                                         attribute:NSLayoutAttributeTop
                                                                        multiplier:1.0
                                                                          constant:0.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view
                                                                         attribute:NSLayoutAttributeLeading
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:_statusBarView
                                                                         attribute:NSLayoutAttributeLeading
                                                                        multiplier:1.0
                                                                          constant:0.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view
                                                                         attribute:NSLayoutAttributeHeight
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:_statusBarView
                                                                         attribute:NSLayoutAttributeHeight
                                                                        multiplier:1.0
                                                                          constant:20.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view
                                                                         attribute:NSLayoutAttributeTrailing
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:_statusBarView
                                                                         attribute:NSLayoutAttributeTrailing
                                                                        multiplier:1.0
                                                                          constant:0.0]];
    
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
}

#pragma mark - Rotation Methods

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id)coordinator {
    
    // before rotation
    
    [coordinator animateAlongsideTransition:^(id  _Nonnull context) {
        
        self.statusBarView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 20);
        
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        
    } completion:^(id  _Nonnull context) {
        
        // after rotation
        
    }];
}

#pragma mark - Omniture Methods

- (void)trackOmniPage
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = OMNIPAGE_ORDER_SEARCH_HELP;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}




@end
