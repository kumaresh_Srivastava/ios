//
//  BTBundlePricingDetailsViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 14/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBundlePricingDetailsViewController.h"
#import "BTBundlePricingTableViewCell.h"
#import "BTBundlePricingFooterView.h"
#import "AppConstants.h"
#import "BTBundlePricingHelpViewController.h"
#import "OmnitureManager.h"
#import "AppManager.h"

@interface BTBundlePricingDetailsViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *pricingDetailsTableView;

@end

@implementation BTBundlePricingDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.pricingDetailsTableView.delegate = self;
    self.pricingDetailsTableView.dataSource = self;
    self.pricingDetailsTableView.allowsSelection = NO;
    
    [self.pricingDetailsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    self.title = @"Bundle pricing";
    
    [self trackOmniPage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view datasource and delagate methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return 2;
    }else {
        return 1;
    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35;
}


- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35)];
    [headerView setBackgroundColor: [BrandColours colourBtNeutral30]];

    NSString *sectionTitle = nil;

    if(section == 0)
    {
        sectionTitle = @"Monthly charge";
    }
    else
    {
        sectionTitle = @"One-off charges";

    }

    UIView *topSepView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, headerView.frame.size.width, 1)];
    topSepView.backgroundColor = [BrandColours colourBtNeutral50];
    [headerView addSubview:topSepView];

    UILabel *headerTitleLable = [[UILabel alloc] initWithFrame:CGRectMake(20, 5,tableView.bounds.size.width - 40, 25)];
    headerTitleLable.text = sectionTitle;
    headerTitleLable.textColor = [BrandColours colourMyBtDarkGrey];
    headerTitleLable.font = [UIFont fontWithName:@"BTFont-Bold" size:15];
    headerTitleLable.backgroundColor = [UIColor clearColor];
    [headerView addSubview:headerTitleLable];

    UIView *bottomSepView = [[UIView alloc] initWithFrame:CGRectMake(0, headerView.frame.size.height - 1, headerView.frame.size.width, 1)];
    bottomSepView.backgroundColor = [BrandColours colourBtNeutral50];
    [headerView addSubview:bottomSepView];

    return headerView;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTBundlePricingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"bundlePricingCell" forIndexPath:indexPath];
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [cell updateCellWithTitle:@"Bundle" description:@"Standard bundle charge" pricingValue:self.bundleMonthlyCharge andPricingValue:@"/month"];
        } else {
            [cell updateCellWithTitle:@"Extras" description:@"Total charge for extras" pricingValue:self.bundleExtraCharge andPricingValue:@"/month"];
        }
    } else {
        [cell updateCellWithTitle:@"Setup" description:@"Installation and equipment" pricingValue:self.totalOneOffCharge andPricingValue:@"one-off"];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 87;
}



- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    BTBundlePricingFooterView *footerView = [[[NSBundle mainBundle] loadNibNamed:@"BTBundlePricingFooterView" owner:nil options:nil] objectAtIndex:0];
    if (section == 0) {
        [footerView updateViewWithPriceValue:(self.bundleExtraCharge + self.bundleMonthlyCharge) withPriceTypeLabel:@"/month"];
    } else {
        [footerView updateViewWithPriceValue:self.totalOneOffCharge withPriceTypeLabel:@"one-off"];
    }
    return footerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 56;
}


#pragma mark - Action methods
- (IBAction)needHelpButtonAction:(id)sender {
    BTBundlePricingHelpViewController *helpViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BTBundlePricingHelpViewController"];
    [self presentViewController:helpViewController animated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Private helper method
- (void)trackOmniPage
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = OMNIPAGE_ORDER_BUNDLE_PRICING_DETAILS;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}


@end
