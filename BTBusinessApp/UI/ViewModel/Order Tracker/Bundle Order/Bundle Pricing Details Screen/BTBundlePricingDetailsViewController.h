//
//  BTBundlePricingDetailsViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 14/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTBundlePricingDetailsViewController : UIViewController {
    
}

@property (nonatomic) double bundleMonthlyCharge;
@property (nonatomic) double bundleExtraCharge;
@property (nonatomic) double totalOneOffCharge;

@end
