//
//  DLMBundleOrderSummaryScreen.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 13/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMBundleOrderSummaryScreen.h"
#import "NLGetBundleOrderSummaryWebService.h"
#import <SAMKeychain/SAMKeychain.h>
#import "CDAuthenticationToken.h"
#import "AppConstants.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDCug+CoreDataClass.h"
#import "CDUser.h"
#import "BTBundleOrder.h"
#import "NLWebServiceError.h"
#import "BTLiveChatAvailabilityChecker.h"

@interface DLMBundleOrderSummaryScreen () <NLGetBundleOrderSummaryWebServiceDelegate, BTLiveChatAvailabilityCheckerDelegate> {
    
    BTLiveChatAvailabilityChecker *_liveChatAvailabityChecker;
}

@property (nonatomic) NLGetBundleOrderSummaryWebService *getBundleOrderSummaryWebService;

@end

@implementation DLMBundleOrderSummaryScreen

- (void)setupToFetchBundleOrderSummaryForBundleOrderRef:(NSString *)bundleOrderRef andItemRef:(NSString *)itemRef {
    
    self.getBundleOrderSummaryWebService = [[NLGetBundleOrderSummaryWebService alloc] initWithOrderRef:bundleOrderRef andItemRef:itemRef];
    self.getBundleOrderSummaryWebService.getBundleOrderSummaryWebServiceDelegate = self;
}

- (void)fetchBundleOrderSummaryForCurrentBundleOrder {
    
    [self.getBundleOrderSummaryWebService resume];
}

- (void)fetchBundleOrderSummaryForBundleOrderReference:(NSString *)bundleOrderRef andItemRef:(NSString *)itemRef {

    [self setupToFetchBundleOrderSummaryForBundleOrderRef:bundleOrderRef andItemRef:itemRef];
    [self fetchBundleOrderSummaryForCurrentBundleOrder];
}

- (void)checkForLiveChatAvailibilityForOrder
{
    [self setupToCheckLiveChatAvailability];
    [self getAvailableSlotsForChat];
}

- (void)setupToCheckLiveChatAvailability
{
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = [[BTLiveChatAvailabilityChecker alloc] init];
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = self;
}

- (void)getAvailableSlotsForChat
{
    [_liveChatAvailabityChecker getLiveChatAvailableSlots];
}

- (void)cancelChatAvailabilityCheckerAPI
{
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    [_liveChatAvailabityChecker cancel];
    _liveChatAvailabityChecker = nil;
}

#pragma mark - NLGetBundleOrderSummaryWebServiceDelegate methods
- (void)getBundleOrderSummaryWebService:(NLGetBundleOrderSummaryWebService *)webService successfullyFetchedBundleOrderSummaryData:(BTBundleOrder *)bundleOrder
{
    if(webService == _getBundleOrderSummaryWebService) {
        _bundleOrder = bundleOrder;
        [self.bundleOrderSummaryScreenDelegate successfullyFetchedBundleOrderSummaryDataOnBundleOrderSummaryScreen:self];
        _getBundleOrderSummaryWebService.getBundleOrderSummaryWebServiceDelegate = nil;
        _getBundleOrderSummaryWebService = nil;
    } else {
        DDLogError(@"This delegate method gets called because of success of an object of NLGetBundleOrderSummaryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLGetBundleOrderSummaryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)getBundleOrderSummaryWebService:(NLGetBundleOrderSummaryWebService *)webService failedToFetchBundleOrderSummaryDataWithWebServiceError:(NLWebServiceError *)error
{
    if(webService == _getBundleOrderSummaryWebService) {
        [self.bundleOrderSummaryScreenDelegate bundleOrderSummaryScreen:self failedToFetchBundleOrderSummaryWithWebServiceError:error];
        _getBundleOrderSummaryWebService.getBundleOrderSummaryWebServiceDelegate = nil;
        _getBundleOrderSummaryWebService = nil;
    } else {
        DDLogError(@"This delegate method gets called because of failure of an object of NLGetBundleOrderSummaryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLGetBundleOrderSummaryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

#pragma mark - BTLiveChatAvailabilityCheckerDelegate Methods
- (void)liveChatAvailabilityChecker:(BTLiveChatAvailabilityChecker *)liveChatAvailabilityChecker successfullyFetchedSlotsWithGeneralCategory:(NSArray *)generalCategoryArray specificeCategory:(NSArray *)specificCategory andCurrentTime:(NSString *)currentTime
{
    BOOL chatAvailable = [BTLiveChatAvailabilityChecker checkAvailabilityWithSpecificCategory:specificCategory currentTime:currentTime andChatType:BTChatTypeOrder];
    
    if (chatAvailable)
    {
        [self.bundleOrderSummaryScreenDelegate liveChatAvailableForOrderWithScreen:self];
    }
    else
    {
        [self.bundleOrderSummaryScreenDelegate bundleOrderSummaryScreen:self liveChatCurrentlyNotAvailableWithSpecificCategory:specificCategory generalCategory:generalCategoryArray andCurrentTime:currentTime];
    }
    
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = nil;
}

- (void)liveChatAvailabilityChecker:(BTLiveChatAvailabilityChecker *)liveChatAvailabilityChecker failedToFetchSlotsWithWebServiceError:(NLWebServiceError *)webServiceError
{
    [self.bundleOrderSummaryScreenDelegate bundleOrderSummaryScreen:self failedToCheckLiveChatAvailabilityWithWebServiceError:webServiceError];
    
    _liveChatAvailabityChecker.liveChatAvailabilityCheckerDelegate = nil;
    _liveChatAvailabityChecker = nil;
}

@end
