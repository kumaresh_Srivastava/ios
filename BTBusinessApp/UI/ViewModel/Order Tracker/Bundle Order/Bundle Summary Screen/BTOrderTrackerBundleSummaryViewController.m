//
//  BTOrderTrackerBundleSummaryViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 03/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderTrackerBundleSummaryViewController.h"
#import "BTOrderTrackerTableViewCell.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "DLMBundleOrderSummaryScreen.h"
#import "CustomSpinnerView.h"
#import "BTRetryView.h"
#import "BTBundleOrder.h"
#import "AppManager.h"
#import "AppConstants.h"
#import "BTProduct.h"
#import "BTBundlePricingDetailsViewController.h"
#import "BTOrderDetailsMilestoneViewController.h"
#import "OmnitureManager.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "NLWebServiceError.h"
#import "BTHelpAndSupportViewController.h"
#import "BTInAppBrowserViewController.h"
#import "BTNavigationViewController.h"
#import "BTOrderStatusLabel.h"

@interface BTOrderTrackerBundleSummaryViewController ()<UITableViewDataSource, UITableViewDelegate, DLMBundleOrderSummaryScreenDelegate, BTRetryViewDelegate, BTOrderDetailsMilestoneViewControllerDelegate> {
    CustomSpinnerView *_loadingView;
    BTRetryView *_retryView;
    BOOL _isDataChanged;
}

@property (nonatomic, readwrite) DLMBundleOrderSummaryScreen *viewModel;

@property (strong, nonatomic) IBOutlet UILabel *orderDescriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *orderReferenceIdLabel;
@property (strong, nonatomic) IBOutlet UILabel *expectedCompletionLabel;
@property (strong, nonatomic) IBOutlet BTOrderStatusLabel *orderStatusLabel;
@property (strong, nonatomic) IBOutlet UILabel *estimatedCompletionDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *orderPlacedOnDateLabel;
@property (weak, nonatomic) IBOutlet UIButton *billingDetailsButton;
@property (weak, nonatomic) IBOutlet UILabel *bundleMonthlyPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *bundleTotalOneOffPriceLabel;

@property (weak, nonatomic) IBOutlet UIButton *cancelThisOrderButton;


@property (strong, nonatomic) IBOutlet UITableView *bundleOrdersListTableView;

@end

@implementation BTOrderTrackerBundleSummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // (lp) setup viewModel for bundle order summary screen
    self.viewModel = [[DLMBundleOrderSummaryScreen alloc] init];
    self.viewModel.bundleOrderSummaryScreenDelegate = self;
    
    //navigation bar
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    //order details table view config
    self.bundleOrdersListTableView.delegate = self;
    self.bundleOrdersListTableView.dataSource = self;
    self.bundleOrdersListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.bundleOrdersListTableView.estimatedRowHeight = 124;
    self.bundleOrdersListTableView.rowHeight = UITableViewAutomaticDimension;
    
    UINib *dataCell = [UINib nibWithNibName:@"BTOrderTrackerTableViewCell" bundle:nil];
    [self.bundleOrdersListTableView registerNib:dataCell forCellReuseIdentifier:@"BTOrderTrackerTableViewCell"];
    
    __weak typeof(self) weakSelf = self;
    
    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([weakSelf networkRequestInProgress]) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
            
        }
        
        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
            
        }
    }];
    
    [self initialSetupForBundleOrderSummaryScreen];
    [self callGetBundleOrderSummaryAPI];
    
    DDLogInfo(@"Bundle Order summary: Initial setup for bundle order summary screen done.");
    
    [self trackOmniPage:OMNIPAGE_ORDER_BUNDLE_ORDER_SUMMARY];
    
}

- (void)viewWillAppear:(BOOL)animated{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChangeOrderBundleSummary:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
}

- (void)viewDidDisappear:(BOOL)animated{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
}

- (void)statusBarOrientationChangeOrderBundleSummary:(NSNotification *)notification {
    // handle the interface orientation as needed
    [self performSelector:@selector(createLoadingView) withObject:nil afterDelay:0.0];
    //[self createLoadingView];
}
- (void)didMoveToParentViewController:(UIViewController *)parent{
    
    [self.orderTrackerBundleSummaryViewControllerDelegate btOrderTrackerBundleSummaryViewController:self didAmendData:_isDataChanged];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI setup related methods
/*
 Resize header view of table to update height of header view
 */
- (void)resizeTableViewHeaderToFit {
    UITableViewHeaderFooterView *headerView = (UITableViewHeaderFooterView *)self.bundleOrdersListTableView.tableHeaderView;
    
    [headerView setNeedsLayout];
    [headerView layoutIfNeeded];
    
    CGFloat height = [headerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CGRect frame = headerView.frame;
    frame.size.height = height;
    headerView.frame = frame;
    
    self.bundleOrdersListTableView.tableHeaderView = headerView;
    
}


#pragma mark - Api call methods

- (void)callGetBundleOrderSummaryAPI {
    
    if([AppManager isInternetConnectionAvailable])
    {
        [self.viewModel fetchBundleOrderSummaryForBundleOrderReference:self.orderRef andItemRef:self.itemRef];
        [self setNetworkRequestInProgress:true];
        [self hideLoadingItems:NO];
        [self hideRetryItems:YES];
        [_loadingView startAnimatingLoadingIndicatorView];
    }
    else
    {
        [self hideLoadingItems:YES];
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_ORDER_BUNDLE_ORDER_SUMMARY];
    }
}


#pragma mark - Update view methods

- (void)initialSetupForBundleOrderSummaryScreen
{
    
    // (LP) initial setup for UI items
    self.title = @"BT Business Bundle";
    
   // [self createLoadingView];
    [self performSelector:@selector(createLoadingView) withObject:nil afterDelay:0.1];
    
    [self hideorShowControls:YES];
}


#pragma mark - table view methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.viewModel.bundleOrder.arrayOfProducts count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTProduct *product = [self.viewModel.bundleOrder.arrayOfProducts objectAtIndex:indexPath.row];
    BOOL isShowAppointment = self.isShowAppointment;//self.viewModel.bundleOrder.IsShowAppointment;
    BTOrderTrackerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BTOrderTrackerTableViewCell" forIndexPath:indexPath];
    
    [cell updateCellWithProduct:product isShowAppointment:isShowAppointment];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BTProduct *product = [self.viewModel.bundleOrder.arrayOfProducts objectAtIndex:indexPath.row];
    
    [self trackOmniPage:OMNIPAGE_ORDER_BUNDLE_ORDER_DETAILS];
    
    // (LP) Logic to go to order details screen
    BTOrderDetailsMilestoneViewController *detailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailsMileStoneScene"];
    detailsViewController.delegate = self;
    [detailsViewController setOrderRef:_orderRef];
    [detailsViewController setItemRef:product.orderItemReference];
    [detailsViewController setProductName:product.productName];
    [detailsViewController setProductDescription:product.productDescription];
    [detailsViewController setProductIcon:product.productIcon];
    [detailsViewController setOrderStatus:self.viewModel.bundleOrder.orderStatus];
    detailsViewController.isShowAppointment = self.isShowAppointment;
    
    [self.navigationController pushViewController:detailsViewController animated:YES];
}

#pragma mark - UI methods

- (void) hideorShowControls:(BOOL)isHide {
    [self.bundleOrdersListTableView setHidden:isHide];
}

/*
 Updates the UI after getting user details
 */
- (void)updateUserInterface {
  
    
    self.orderReferenceIdLabel.text = [NSString stringWithFormat:@"%@",self.viewModel.bundleOrder.orderRef];
    // (LP) Update status color according to order status
//    if ([self.viewModel.bundleOrder.orderStatus isEqualToString:@"Completed"]) {
//        self.orderStatusLabel.textColor = [BrandColours colourMyBtGreen];
//    } else if ([self.viewModel.bundleOrder.orderStatus isEqualToString:@"Delayed"]) {
//        self.orderStatusLabel.textColor = [BrandColours colourMyBtRed];
//    } else {
//        self.orderStatusLabel.textColor = [BrandColours colourMyBtOrange];
//    }
    
    if ([self.viewModel.bundleOrder.orderStatus isEqualToString:@"Cancelled"] || [self.viewModel.bundleOrder.orderStatus isEqualToString:@"Completed"]) {
        self.cancelThisOrderButton.hidden = YES;
    }
    
    self.orderDescriptionLabel.text = self.viewModel.bundleOrder.orderDescription;
    //self.orderStatusLabel.text = self.viewModel.bundleOrder.orderStatus;
    [self.orderStatusLabel setOrderStatus:self.viewModel.bundleOrder.orderStatus];
    if([self.viewModel.bundleOrder.orderStatus isEqualToString:@"Completed"]) {
        self.expectedCompletionLabel.text = @"Completed on";
    } else {
        self.expectedCompletionLabel.text = @"Expected completion";
    }
    
    if ([AppManager isValidDate:self.viewModel.bundleOrder.estimatedCompletionDate])
    {
        self.estimatedCompletionDateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:self.viewModel.bundleOrder.estimatedCompletionDate];
    }
    else
    {
        self.estimatedCompletionDateLabel.text = @"-";
    }
    
    self.orderPlacedOnDateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:self.viewModel.bundleOrder.orderPlacedDate];
    
    double oneOffPrice = self.viewModel.bundleOrder.bundleTotalOneOffCharge;
    double monthlyPrice = self.viewModel.bundleOrder.bundleExtraCharges + self.viewModel.bundleOrder.monthlyPrice;
    self.bundleTotalOneOffPriceLabel.text = [NSString stringWithFormat:@"£ %.2f", oneOffPrice];
    self.bundleMonthlyPriceLabel.attributedText = [self billingDetailsStringForOneOff:oneOffPrice andRegular:monthlyPrice];
    
    [self.bundleOrdersListTableView reloadData];
    [self hideorShowControls:NO];
    [self hideRetryItems:YES];
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    [self resizeTableViewHeaderToFit];
}


- (NSAttributedString *)billingDetailsStringForOneOff:(double)oneOff andRegular:(double)regular{
    
    NSString *oneOffData = [NSString stringWithFormat:@"£%.2f", oneOff];
    
    NSString *regularData = [NSString stringWithFormat:@"£%.2f", regular];
    
    
    
    
    UIFont *textFont = [UIFont fontWithName:kBtFontRegular size:18.0];
    NSDictionary *dataPartDict = [NSDictionary dictionaryWithObject:textFont forKey:NSFontAttributeName];
    
    textFont = [UIFont fontWithName:kBtFontRegular size:14.0];
    NSDictionary *textPartDict = [NSDictionary dictionaryWithObject:textFont forKey:NSFontAttributeName];
    
    //Regular
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:oneOffData attributes:dataPartDict];
    
    NSMutableAttributedString *oneOffText = [[NSMutableAttributedString alloc]initWithString:@" one-off\n" attributes:textPartDict];
    [aAttrString appendAttributedString:oneOffText];
    
    //OneOff
    NSMutableAttributedString *regularString = [[NSMutableAttributedString alloc] initWithString:regularData attributes:dataPartDict];
    [aAttrString appendAttributedString:regularString];
    
    NSMutableAttributedString *monthText = [[NSMutableAttributedString alloc]initWithString:@" /month  " attributes:textPartDict];
    [aAttrString appendAttributedString:monthText];
    
    return aAttrString;
    
}


- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow
{
    if (_retryView != nil) {
        
        _retryView.hidden = NO;
        _retryView.retryViewDelegate = self;
        [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
        return;
    }
    
    _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
    _retryView.translatesAutoresizingMaskIntoConstraints = NO;
    _retryView.retryViewDelegate = self;
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
    
    [self.view addSubview:_retryView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
}

- (void)createLoadingView {
    
    if(_loadingView.isHidden == YES){
        return;
    }
    if(_loadingView){
        [_loadingView removeFromSuperview];
        _loadingView = nil;
    }
    _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
    _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:_loadingView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}

- (void)hideRetryItems:(BOOL)isHide {
    
    // (lp) Hide or Show UI elements related to retry.
    [_retryView setRetryViewDelegate:nil];
    [_retryView setHidden:isHide];
    
}

- (void)hideLoadingItems:(BOOL)isHide {
    
    // (lp) Hide or Show UI elements related to loading.
    [_loadingView setHidden:isHide];
    
}

- (void)checkChatAvailabilityAction
{
    if ([AppManager isInternetConnectionAvailable])
    {
        _networkRequestInProgress = YES;
        [self hideLoadingItems:NO];
        [_loadingView startAnimatingLoadingIndicatorView];
        [self.viewModel checkForLiveChatAvailibilityForOrder];
    }
    else
    {
        [self showAlertWithTitle:kNoConnectionTitle andMessage:kNoConnectionMessage];
    }
}

- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }]];
    
    [self presentViewController:alertController animated:YES completion:^{
        
    }];
}


#pragma mark - BTRetryView delegate methods
- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView {
    
    DDLogInfo(@"Retry to fecth order summary details for order reference : %@",self.orderRef);
    [self callGetBundleOrderSummaryAPI];
}


#pragma mark - Action methods

- (IBAction)cancelThisOrderButtonAction:(id)sender
{
    [self checkChatAvailabilityAction];
}


- (IBAction)pricingDetailsButtonAction:(id)sender {
    BTBundlePricingDetailsViewController *pricingDetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BTBundlePricingDetailsViewController"];
    pricingDetailsViewController.bundleMonthlyCharge = self.viewModel.bundleOrder.monthlyPrice;
    pricingDetailsViewController.bundleExtraCharge = self.viewModel.bundleOrder.bundleExtraCharges;
    pricingDetailsViewController.totalOneOffCharge = self.viewModel.bundleOrder.bundleTotalOneOffCharge;
    [self.navigationController pushViewController:pricingDetailsViewController animated:YES];
}



#pragma mark- BTOrderDetailsMilestoneViewControllerDelegate Method

- (void)btOrderDetailsMilestoneViewController:(BTOrderDetailsMilestoneViewController *)controller didChangeData:(BOOL)dataChangeStatus{

    if(dataChangeStatus == YES){
        
        [self callGetBundleOrderSummaryAPI];
        _isDataChanged = dataChangeStatus;
    }
}


#pragma mark - DLMBundleOrderSummaryScreenDelagate methods
- (void)successfullyFetchedBundleOrderSummaryDataOnBundleOrderSummaryScreen:(DLMBundleOrderSummaryScreen *)bundleOrderSummaryScreen
{
    self.networkRequestInProgress = NO;
    
    if (self.viewModel.bundleOrder.orderRef) {
        
        DDLogInfo(@"Bundle Order Summary: Bundle order summary details fetched successfully");
        
        // (lp) Update UI with bundle order summary data
        [self updateUserInterface];
        
    } else {
        
        DDLogError(@"Order Summary: Fetch order summary details failed");
        
        // (lp) Retry to fetch order details
        [self hideRetryItems:NO];
        [self hideLoadingItems:YES];
        [_loadingView stopAnimatingLoadingIndicatorView];
        
    }

}

- (void)bundleOrderSummaryScreen:(DLMBundleOrderSummaryScreen *)bundleOrderSummaryScreen failedToFetchBundleOrderSummaryWithWebServiceError:(NLWebServiceError *)error {
    
    DDLogError(@"Bundle Order Summary: Fetch bundle order summary details failed");
    
    self.networkRequestInProgress = NO;
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if(errorHandled == NO)
    {
        // (lp) Retry to fetch bundle order summary
        [self hideLoadingItems:YES];
        [_loadingView stopAnimatingLoadingIndicatorView];
        [self showRetryViewWithInternetStrip:NO];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_ORDER_BUNDLE_ORDER_SUMMARY];
    }
    
}

- (void)liveChatAvailableForOrderWithScreen:(DLMBundleOrderSummaryScreen *)bundleOrderSummaryScreen
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
    
    viewController.targetURLType = BTTargetURLTypeOrdersChatScreen;
    
    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:NO andIsNeedCloseButton:YES];
    
    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{
        
    }];
}

- (void)bundleOrderSummaryScreen:(DLMBundleOrderSummaryScreen *)bundleOrderSummaryScreen liveChatCurrentlyNotAvailableWithSpecificCategory:(NSArray *)specificCategory generalCategory:(NSArray *)generalCategory andCurrentTime:(NSString *)currentTime
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BTHelpAndSupportViewController *helpAndSupportController = [BTHelpAndSupportViewController getHelpAndSupportViewController];
    helpAndSupportController.isLaunchingFromHomeScreen = NO;
    helpAndSupportController.isDataAlreadyFetched = YES;
    helpAndSupportController.specificCategory = specificCategory;
    helpAndSupportController.generalCategory = generalCategory;
    helpAndSupportController.currentTime = currentTime;
    
    [self.navigationController pushViewController:helpAndSupportController animated:YES];
}

- (void)bundleOrderSummaryScreen:(DLMBundleOrderSummaryScreen *)bundleOrderSummaryScreen failedToCheckLiveChatAvailabilityWithWebServiceError:(NLWebServiceError *)error
{
    _networkRequestInProgress = NO;
    
    [self hideLoadingItems:YES];
    [_loadingView stopAnimatingLoadingIndicatorView];
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if(errorHandled == NO)
    {
        [self showAlertWithTitle:@"Message" andMessage:kDefaultErrorMessage];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_ORDER_BUNDLE_ORDER_SUMMARY];
    }
    
}

#pragma mark - Private helper methods


- (void)trackOmniPage:(NSString *)page
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = page;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    [data setValue:self.orderRef forKey:kOmniOrderRef];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

@end
