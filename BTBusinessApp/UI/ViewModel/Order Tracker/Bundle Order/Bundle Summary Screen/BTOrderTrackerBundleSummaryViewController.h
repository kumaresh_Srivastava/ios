//
//  BTOrderTrackerBundleSummaryViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 03/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTOrderTrackerBundleSummaryViewController;

@protocol BTOrderTrackerBundleSummaryViewControllerDelegate <NSObject>

- (void)btOrderTrackerBundleSummaryViewController:(BTOrderTrackerBundleSummaryViewController *)controller  didAmendData:(BOOL)isDataAmended;

@end


@interface BTOrderTrackerBundleSummaryViewController : UIViewController
@property(nonatomic, weak) id<BTOrderTrackerBundleSummaryViewControllerDelegate> orderTrackerBundleSummaryViewControllerDelegate;

@property(nonatomic,strong) NSString *orderRef;
@property(nonatomic,strong) NSString *itemRef;

@property (nonatomic) BOOL networkRequestInProgress;
@property (nonatomic) BOOL isShowAppointment;

@end
