//
//  DLMBundleOrderSummaryScreen.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 13/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
@class DLMBundleOrderSummaryScreen;
@class BTBundleOrder;
@class NLWebServiceError;

@protocol DLMBundleOrderSummaryScreenDelegate <NSObject>

- (void)successfullyFetchedBundleOrderSummaryDataOnBundleOrderSummaryScreen:(DLMBundleOrderSummaryScreen *)bundleOrderSummaryScreen;

- (void)bundleOrderSummaryScreen:(DLMBundleOrderSummaryScreen *)bundleOrderSummaryScreen failedToFetchBundleOrderSummaryWithWebServiceError:(NLWebServiceError *)error;

- (void)liveChatAvailableForOrderWithScreen:(DLMBundleOrderSummaryScreen *)bundleOrderSummaryScreen;
- (void)bundleOrderSummaryScreen:(DLMBundleOrderSummaryScreen *)bundleOrderSummaryScreen liveChatCurrentlyNotAvailableWithSpecificCategory:(NSArray *)specificCategory generalCategory:(NSArray *)generalCategory andCurrentTime:(NSString *)currentTime;
- (void)bundleOrderSummaryScreen:(DLMBundleOrderSummaryScreen *)bundleOrderSummaryScreen failedToCheckLiveChatAvailabilityWithWebServiceError:(NLWebServiceError *)error;

@end

@interface DLMBundleOrderSummaryScreen : DLMObject {
    
}

@property (nonatomic, copy) NSString *bundleOrderNumber;
@property (nonatomic, strong) BTBundleOrder *bundleOrder;

@property (nonatomic, weak) id<DLMBundleOrderSummaryScreenDelegate>bundleOrderSummaryScreenDelegate;

- (void)fetchBundleOrderSummaryForBundleOrderReference:(NSString *)bundleOrderRef andItemRef:(NSString *)itemRef;

- (void)cancelChatAvailabilityCheckerAPI;
- (void)checkForLiveChatAvailibilityForOrder;

@end
