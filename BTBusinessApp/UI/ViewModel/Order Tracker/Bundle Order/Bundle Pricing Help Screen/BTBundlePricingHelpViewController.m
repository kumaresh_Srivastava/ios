//
//  BTBundlePricingHelpViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 16/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBundlePricingHelpViewController.h"
#import "AppManager.h"
#import "OmnitureManager.h"

@interface BTBundlePricingHelpViewController ()

@end

@implementation BTBundlePricingHelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self trackOmniPage];
    
    [self createAndAddCustomStatusBarView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
   
}

#pragma mark - Action methods
- (IBAction)downArrowButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Private methods

- (void)createAndAddCustomStatusBarView
{
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 20)];

    statusBarView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:statusBarView];
}

- (void)trackOmniPage
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = OMNIPAGE_ORDER_BUNDLE_PRICING_DETAILS_HELP;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];

    [OmnitureManager trackPage:pageName withContextInfo:data];
}


@end
