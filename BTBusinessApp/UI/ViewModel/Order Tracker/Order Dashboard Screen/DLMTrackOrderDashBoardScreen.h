//
//  DLMTrackOrderDashBoard.h
//  BTBusinessApp
//
//  Created by Accolite on 22/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
@class DLMTrackOrderDashBoardScreen;
@class BTCug;
@class NLWebServiceError;

@protocol DLMTrackOrderDashBoardScreenDelegate <NSObject>

- (void)successfullyFetchedOrderDashBoardDataOnTrackOrderDashBoardScreen:(DLMTrackOrderDashBoardScreen *)dashBoardScreen;

- (void)trackorderDashBoardScreen:(DLMTrackOrderDashBoardScreen *)dashBoardScreen failedToFetchOrderDashBoardDataWithWebServiceError:(NLWebServiceError *)error;

@end

@interface DLMTrackOrderDashBoardScreen : DLMObject

@property (nonatomic,assign)int pageSize;
@property (nonatomic,assign)int pageIndex;
@property (nonatomic,assign)int tabID;
@property (nonatomic,assign)int totalSize;
@property (nonatomic, weak) id<DLMTrackOrderDashBoardScreenDelegate> dashBoardDelegate;
@property (nonatomic) NSArray *orders;
@property (nonatomic) NSArray *projects;

- (id)initWithFirstPageOpenOrderDict:(NSDictionary *)firstPagerOpenOrderDict;
- (void)fetchOrderDashBoardDetailsForLoggedInUser;
- (void)cancelDashBoardAPIRequest;
- (void)changeSelectedCUGTo:(BTCug *)selectedCug;
- (int)getTotalNumberOfPagesToFetch;

@end
