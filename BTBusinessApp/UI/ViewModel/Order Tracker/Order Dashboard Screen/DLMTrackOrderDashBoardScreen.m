//
//  DLMTrackOrderDashBoard.m
//  BTBusinessApp
//
//  Created by Accolite on 22/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMTrackOrderDashBoardScreen.h"
#import "NLTrackOrderDashBoardWebService.h"
#import "AppDelegate.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppDelegateViewModel.h"
#import "AppConstants.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"

#import "BTCug.h"
#import "CDCug+CoreDataClass.h"
#import "BTUser.h"
#import "CDRecentSearchedFault.h"
#import "NLWebServiceError.h"

#import "NLGetProjectsByCUG.h"
#import "BTOCSProject.h"

@interface DLMTrackOrderDashBoardScreen () <NLTrackOrderDashBoardWebServiceDelegate,NLAPIGEEAuthenticationProtectedWebServiceDelegate>{
    
    NSDictionary *_firstPageOpenOrderDict;
    
    BOOL _isTrackOrderAPICompleted;
    BOOL _isOCSAPICompleted;
}

@property (nonatomic) NLTrackOrderDashBoardWebService *getOrderDashBoardWebService;
@property (nonatomic) NLGetProjectsByCUG *getProjectsByCUGWebService;
@end

@implementation DLMTrackOrderDashBoardScreen

#pragma mark Public Methods

- (id)initWithFirstPageOpenOrderDict:(NSDictionary *)firstPagerOpenOrderDict{
    
    self = [super init];
    
    if(self){
        
        _firstPageOpenOrderDict = firstPagerOpenOrderDict;
    }
    
    return self;
}

- (void)setProjects:(NSArray *)projects
{
    _projects = nil;
    if (projects) {
        _projects = [NSArray arrayWithArray:projects];
        _isOCSAPICompleted = YES;
    } else {
        _isOCSAPICompleted = NO;
    }
}

- (void)fetchOrderDashBoardDetailsForLoggedInUser
{
    
    //check for first page open order
    if(_firstPageOpenOrderDict && [_firstPageOpenOrderDict objectForKey:@"orders"] && self.pageIndex == 1 && self.tabID == 1){
        
        [self checkForAlreadyFetchedInitialOpenOrders];
    }
    else
    {
        [self setupToFetchOrderDashboardDetails];
        [self fetchOrderDashboardDetails];
    }
}

- (void)setupToFetchOrderDashboardDetails
{
    self.getOrderDashBoardWebService = [[NLTrackOrderDashBoardWebService alloc] initWithPageSize:self.pageSize pageIndex:self.pageIndex andTabID:self.tabID];
    self.getOrderDashBoardWebService.getTrackOrderDashBoardWebServiceDelegate = self;
    _isTrackOrderAPICompleted = NO;
    if (!_isOCSAPICompleted) {
        if (self.projects) {
            [self setProjects:@[]];
        }
        //NSString *cugName = [[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSString *cugId = [[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugId stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
//        self.getProjectsByCUGWebService = [[NLGetProjectsByCUG alloc] initWithCUG:cugName];
        self.getProjectsByCUGWebService = [[NLGetProjectsByCUG alloc] initWithCUGID:cugId];
        self.getProjectsByCUGWebService.delegate = self;
    }
}

- (void)fetchOrderDashboardDetails
{
    [self.getOrderDashBoardWebService resume];
    if (!_isOCSAPICompleted) {
        [self.getProjectsByCUGWebService resume];
    }
}

- (void)checkForAlreadyFetchedInitialOpenOrders
{
    NSArray *orders = [_firstPageOpenOrderDict valueForKey:@"orders"];
    int totalSize = [[_firstPageOpenOrderDict valueForKey:@"totalSize"] intValue];
    int tabID = [[_firstPageOpenOrderDict valueForKey:@"tabID"] intValue];
    int pageIndex = [[_firstPageOpenOrderDict valueForKey:@"pageIndex"] intValue];
    
    [self setOrders:orders];
    [self setTotalSize:totalSize];
    if (tabID == self.tabID) {
        if (pageIndex == self.pageIndex) {
            self.pageIndex ++;
        }
    }
    
    if ([_firstPageOpenOrderDict valueForKey:@"projects"] && [[_firstPageOpenOrderDict valueForKey:@"projects"] isKindOfClass:[NSArray class]]) {
        [self setProjects:[_firstPageOpenOrderDict valueForKey:@"projects"]];
    }
    
    [self.dashBoardDelegate successfullyFetchedOrderDashBoardDataOnTrackOrderDashBoardScreen:self];
}

- (int)getTotalNumberOfPagesToFetch
{
    int numberOfPages = self.totalSize / 3;
    int remaining = self.totalSize % 3;
    
    if (remaining > 0)
    {
        numberOfPages++;
    }
    
    return numberOfPages;
}

#pragma PersistanceMethods

- (void)changeSelectedCUGTo:(BTCug *)selectedCug
{
    DDLogInfo(@"Saving %@ as selected cug into persistence.", selectedCug);
    
    NSManagedObjectContext *context = [AppDelegate sharedInstance].managedObjectContext;
    CDCug *newPersistenceCug = [CDCug newCugInManagedObjectContext:context];
    newPersistenceCug.groupKey = selectedCug.groupKey;
    newPersistenceCug.refKey = selectedCug.refKey;
    newPersistenceCug.contactId = selectedCug.contactId;
    newPersistenceCug.cugName = selectedCug.cugName;
    newPersistenceCug.cugId = selectedCug.cugID;
    newPersistenceCug.cugRole = @(selectedCug.cugRole);
    newPersistenceCug.indexInAPIResponse = @(selectedCug.indexInAPIResponse);
    
    // Get the currently logged in User
    CDUser *loggedInUser = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    CDCug *previouslySelectedCug = loggedInUser.currentSelectedCug;
    loggedInUser.currentSelectedCug = newPersistenceCug;
    
    if(previouslySelectedCug)
    {
        [context deleteObject:previouslySelectedCug];
    }
    
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
    
    _firstPageOpenOrderDict = nil;
    _isTrackOrderAPICompleted = NO;
    _isOCSAPICompleted = NO;
}

#pragma mark CancellingDashBoard request methods

- (void) cancelDashBoardAPIRequest {
    self.getOrderDashBoardWebService.getTrackOrderDashBoardWebServiceDelegate = nil;
    [self.getOrderDashBoardWebService cancel];
    self.getOrderDashBoardWebService = nil;
    
    self.getProjectsByCUGWebService.delegate = nil;
    [self.getProjectsByCUGWebService cancel];
    self.getProjectsByCUGWebService = nil;
}

#pragma mark - NLTrackOrderDashBoardWebServiceDelegate Methods

- (void)getOrderDashBoardWebService:(NLTrackOrderDashBoardWebService *)webService successfullyFetchedOrderDashBoardData:(NSArray *)orders pageIndex:(int)pageIndex totalSize:(int)totalSize tabID:(int)tabID {

    if(webService == _getOrderDashBoardWebService)
    {
        [self setOrders:orders];
        [self setTotalSize:totalSize];
        if (tabID == self.tabID) {
            if (pageIndex == self.pageIndex) {
                self.pageIndex ++;
            }
        }

        _getOrderDashBoardWebService.getTrackOrderDashBoardWebServiceDelegate = nil;
        _getOrderDashBoardWebService = nil;
        
        _isTrackOrderAPICompleted = YES;
        if (_isOCSAPICompleted) {
            [self.dashBoardDelegate successfullyFetchedOrderDashBoardDataOnTrackOrderDashBoardScreen:self];
        }
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLTrackOrderDashBoardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLTrackOrderDashBoardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)getOrderDashBoardWebService:(NLTrackOrderDashBoardWebService *)webService failedToFetchOrderDashBoardDataWithWebServiceError:(NLWebServiceError *)error
{
    if(webService == _getOrderDashBoardWebService)
    {
        _getOrderDashBoardWebService.getTrackOrderDashBoardWebServiceDelegate = nil;
        _getOrderDashBoardWebService = nil;
        _isTrackOrderAPICompleted = YES;
        if (_isOCSAPICompleted) {
            [self.dashBoardDelegate trackorderDashBoardScreen:self failedToFetchOrderDashBoardDataWithWebServiceError:error];
        }
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLTrackOrderDashBoardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLTrackOrderDashBoardWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

#pragma mark - NLAPIGEEAuthenticationProtectedWebServiceDelegate (for getProjectsByCUG)

- (void)webService:(NLAPIGEEAuthenticatedWebService *)webService finshedWithSuccessResponse:(NSObject *)responseObject
{
    if (webService == self.getProjectsByCUGWebService) {
        //
        _isOCSAPICompleted = YES;
        
        NSMutableArray *projects = [NSMutableArray new];
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSObject *projectsObj = [(NSDictionary*)responseObject objectForKey:@"Projects"];
            if ([projectsObj isKindOfClass:[NSArray class]]) {
                for (NSObject *project in (NSArray*)projectsObj) {
                    if ([project isKindOfClass:[NSDictionary class]]) {
                        BTOCSProject *newProject = [[BTOCSProject alloc] initWithAPIResponse:(NSDictionary*)project];
                        [projects addObject:newProject];
                    }
                }
            } else if ([projectsObj isKindOfClass:[NSDictionary class]]) {
                BTOCSProject *newProject = [[BTOCSProject alloc] initWithAPIResponse:(NSDictionary*)projectsObj];
                [projects addObject:[newProject copy]];
            }
        }
        
        [self setProjects:projects];
        
        if (_isTrackOrderAPICompleted) {
            [self.dashBoardDelegate successfullyFetchedOrderDashBoardDataOnTrackOrderDashBoardScreen:self];
        }
        
        self.getProjectsByCUGWebService.delegate = nil;
        self.getProjectsByCUGWebService = nil;
    }
}

- (void)webService:(NLAPIGEEAuthenticatedWebService *)webService failedWithError:(NLWebServiceError *)error
{
    if (webService == self.getProjectsByCUGWebService) {
        //
        _isOCSAPICompleted = YES;
        
        //[self.homeScreenDelegate homeScreen:self failedToFetchGridDataWithWebServiceError:error];
        
        if (_isTrackOrderAPICompleted) {
            [self.dashBoardDelegate trackorderDashBoardScreen:self failedToFetchOrderDashBoardDataWithWebServiceError:error];
        }
        self.getProjectsByCUGWebService.delegate = nil;
        self.getProjectsByCUGWebService = nil;
    }
}

@end
