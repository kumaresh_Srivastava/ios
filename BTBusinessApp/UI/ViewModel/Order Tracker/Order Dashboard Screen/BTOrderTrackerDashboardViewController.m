//
//  BTOrderTrackerDashboardViewController.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 29/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderTrackerDashboardViewController.h"
#import "BTOrderTrackerDashboardTableViewCell.h"
#import "MBProgressHUD.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "BTUICommon.h"
#import "BTTrackOrderDashBoardModel.h"
#import "BTOrderTrackerDetailsViewController.h"
#import "DLMTrackOrderDashBoardScreen.h"
#import "BTOrder.h"
#import "BTRetryView.h"
#import "BTEmptyDashboardView.h"
#import "BTOrderTrackerHomeViewController.h"
#import "CustomSpinnerView.h"
#import "BTOrderDashBoardTableFooterView.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "BTCug.h"
#import "NLWebServiceError.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "AppConstants.h"
#import "BTSegmentedControl.h"
#import "BTAccountLocationDropdownView.h"
#import "BTOCSProject.h"
#import "BTOCSOrderDetailsTableViewController.h"

@interface BTOrderTrackerDashboardViewController ()<UITableViewDelegate, UITableViewDataSource, DLMTrackOrderDashBoardScreenDelegate, BTRetryViewDelegate, BTEmptyDashboardViewDelegate, BTOrderDashBoardFooterDelegate> {
    BTRetryView *_retryView;
    BTEmptyDashboardView *_emptyDashboardView;
    BTAccountLocationDropdownView *_accountDropdownView;
    BOOL _isRetryShown, _isEmptyViewShown;
    BOOL isTabSwitchingForMoreData;
    BOOL _isViewDidLoadCalled;
    BTOrderDashBoardTableFooterView *_orderDashBoardTableFooterView;
    BOOL _isFooterViewCreated;
    BOOL _isLazyLoadingInOpenOrder, _isLazyLoadingInCompleted;
    BOOL _isTabSwitch,_isLazyLoadingCompletedClosed, _islazyLoadingOpenClosed;
    Reachability *_reachability;
    BOOL _isFetchingWhileScroll;
    BOOL _isResetGroup;
    BOOL  _needToRemoveLoadingViewFromScreen;//RM
    NSString *_pageNameForOmniture; //RM
    BOOL _completedOrderAPIInProgress;
    BOOL _openOrderAPIInProgress;
}

@property (weak, nonatomic) IBOutlet UITableView *orderListTableView;
@property (weak, nonatomic) IBOutlet BTSegmentedControl *ordersSegmentedControl;

@property (nonatomic,strong) NSMutableArray *dummyOrderData;
@property (nonatomic,strong) NSMutableArray *openOrderData;
@property (nonatomic,strong) NSMutableArray *completedOrderData;
@property (nonatomic,assign) TrackOrderDashBoardType trackOrderType;

@property (weak, nonatomic) IBOutlet UILabel *groupName;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) CustomSpinnerView *loadingView;

@property (strong, nonatomic) IBOutlet UIView *segmentContainerView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *segmentContainerTopConstraint;

@property (nonatomic, readwrite) DLMTrackOrderDashBoardScreen *openOrderViewModel;
@property (nonatomic, readwrite) DLMTrackOrderDashBoardScreen *completedOrderViewModel;
@property (nonatomic,strong) NSString *currentGroupKey;
- (IBAction)orderTypeChanged:(id)sender;
@end

@implementation BTOrderTrackerDashboardViewController {
    BTTrackOrderDashBoardModel *trackOpenOrderDashBoardModel, *trackCompletedOrderDashBoardModel;
}

+ (BTOrderTrackerDashboardViewController *)getOrderTrackerDashboardViewController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTOrderTrackerDashboardViewController *trackOrderViewController = [storyboard instantiateViewControllerWithIdentifier:@"orderDashboardScene"];
    return trackOrderViewController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //BTOrderDashBoardTableFooterView
    _isViewDidLoadCalled = YES;
    isTabSwitchingForMoreData = NO;
    // (lp) Setup viewModel for home screen
    self.openOrderViewModel = [[DLMTrackOrderDashBoardScreen alloc] initWithFirstPageOpenOrderDict:self.firstPageOpenOrderDataDict];
    
    self.completedOrderViewModel = [[DLMTrackOrderDashBoardScreen alloc] init];
    [self.completedOrderViewModel setProjects:[self.firstPageOpenOrderDataDict objectForKey:@"projects"]];
    
    if(!self.cugs) {
        self.cugs = [[AppDelegate sharedInstance] savedCugs];
    }
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationBar_background"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                 NSForegroundColorAttributeName:[UIColor whiteColor],
                                                                 NSFontAttributeName:[UIFont fontWithName:kBtFontBold size:20                      ]
                                                                 }];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self updateGroupSelection];
    
    _isResetGroup = NO;
    _isTabSwitch = NO;
    _isLazyLoadingInOpenOrder = NO;
    _isLazyLoadingInCompleted = NO;
    _isFetchingWhileScroll = NO;
    //order details table view config
    self.orderListTableView.delegate = self;
    self.orderListTableView.dataSource = self;
    self.orderListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.orderListTableView.backgroundColor = [UIColor clearColor];
    self.orderListTableView.estimatedRowHeight = 114;
    self.orderListTableView.rowHeight = UITableViewAutomaticDimension;
  
    //[_ordersSegmentedControl setTintColor:[BrandColours colourBackgroundBTPurplePrimaryColor]];
    //[_ordersSegmentedControl setTitleTextAttributes:[[AppManager sharedAppManager] fetchSegmentedControlFontDict] forState:UIControlStateNormal];

    [self createLoadingView];
    //[self performSelector:@selector(createLoadingView) withObject:nil afterDelay:0.03];
    
    __weak typeof(self) weakSelf = self;
    
    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([weakSelf networkRequestInProgress]) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf createLoadingView];
                [weakSelf hideLoadingItems:NO];
                [weakSelf.loadingView startAnimatingLoadingIndicatorView];
                 //[weakSelf.loadingView startAnimation];
            });
        }
        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf hideLoadingItems:YES];
                [weakSelf.loadingView stopAnimatingLoadingIndicatorView];
               //[weakSelf.loadingView stopAnimation];
            });
        }
    }];
    
    self.trackOrderType = 0;//reset trackOrder
    _isRetryShown = false;
    _isEmptyViewShown = false;
    
    //Registering the NIbs
    UINib *dataCell = [UINib nibWithNibName:@"BTOrderTrackerDashboardTableViewCell" bundle:nil];
    [self.orderListTableView registerNib:dataCell forCellReuseIdentifier:@"orderTrackerDashboardTableViewCell"];
    
//   // if(self.cugs.count>1){
//    // Add the group picker
//        _accountDropdownView = [[[NSBundle mainBundle] loadNibNamed:@"BTAccountLocationDropdownView" owner:self options:nil] firstObject];
//        _accountDropdownView.locationLabel.text = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugName;
//        UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(groupBarButtonAction:)];
//        [_accountDropdownView addGestureRecognizer:tapped];
//        
//        // Separator line
//        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 109, self.view.frame.size.width, 1.0)];
//        separatorView.backgroundColor = [BrandColours colourBtNeutral30];
//        [_accountDropdownView addSubview:separatorView];
//        [self.view insertSubview:_accountDropdownView atIndex:0];
//       
//    /*} else{
//         self.ordersSegmentedControl.frame = CGRectMake(self.ordersSegmentedControl.frame.origin.x, self.ordersSegmentedControl.frame.origin.y - 110 , self.ordersSegmentedControl.frame.size.width, self.ordersSegmentedControl.frame.size.height);
//    }*/
    
    if (self.cugs.count > 1) {
        _accountDropdownView = [[[NSBundle mainBundle] loadNibNamed:@"BTAccountLocationDropdownView" owner:self options:nil] firstObject];
        BTCug *firstCug = self.cugs[0];
        _accountDropdownView.locationLabel.text = firstCug.cugName;
        UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(groupBarButtonAction:)];
        [_accountDropdownView addGestureRecognizer:tapped];
       
       // [self.view insertSubview:_accountDropdownView atIndex:0];
         //[self.view insertSubview:_accountDropdownView atIndex:0];
        [self.view addSubview:_accountDropdownView];
       
        _accountDropdownView.frame = CGRectMake(0,self.segmentContainerView.frame.size.height/2 + 25, _accountDropdownView.frame.size.width, _accountDropdownView.frame.size.height);
        //[self.view removeConstraint:self.segmentContainerTopConstraint];
        
        
        [self.view addConstraints:@[
                                  
                                     [NSLayoutConstraint constraintWithItem:_accountDropdownView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.segmentContainerView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-20.0],
                                    
                                    [NSLayoutConstraint constraintWithItem:_accountDropdownView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                    [NSLayoutConstraint constraintWithItem:_accountDropdownView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0],
                                     
                                      [NSLayoutConstraint constraintWithItem:_accountDropdownView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.orderListTableView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]
                                 
                                    
                                    ]];
        
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
        
    } else{
        _accountDropdownView = nil;
    }

    //(VRK)4 lines delete after the ViewModel implementation
    trackOpenOrderDashBoardModel = [[BTTrackOrderDashBoardModel alloc] init];
    trackCompletedOrderDashBoardModel = [[BTTrackOrderDashBoardModel alloc] init];
    self.openOrderData = [NSMutableArray array];
    self.completedOrderData = [NSMutableArray array];

    self.trackOrderType = TrackOrderDashBoardTypeOpenOrder;
    
    
    if (self.firstPageOpenOrderDataDict || [AppManager isInternetConnectionAvailable]) {
        
        [self fetchOrderDashBoardDetailsWithPageIndex:1];
        
    } else {
        
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
    }
    
    [self trackOmniPage:[self getPageNameForCurrentGroup]];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    if (!_isViewDidLoadCalled) {
        
        if (_isRetryShown) {
            [self hideRetryItems:YES];
        }
        if (_isEmptyViewShown) {
            [self hideEmmptyDashBoardItems:YES];
        }
        
        if (_ordersSegmentedControl.selectedSegmentIndex == 0) {
            self.trackOrderType = TrackOrderDashBoardTypeOpenOrder;
            if (self.openOrderCount == 0) {
                if ([AppManager isInternetConnectionAvailable]) {
                    
                    [self fetchOrderDashBoardDetailsWithPageIndex:1];
                } else {
                    [self showRetryViewWithInternetStrip:YES];
                    //Track No Internet
                    [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
                }
            } else {
                [self.orderListTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:0 animated:NO];
                [self fetchOpenOrdersAutomatically];
            }
        } else if (_ordersSegmentedControl.selectedSegmentIndex == 1) {
            self.trackOrderType = TrackOrderDashBoardTypeCompletedOrder;
            if (self.completedOrderCount == 0) {
                if ([AppManager isInternetConnectionAvailable]) {
                    
                    [self fetchOrderDashBoardDetailsWithPageIndex:1];
                } else {
                    [self showRetryViewWithInternetStrip:YES];
                    //Track No Internet
                    [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
                }
            } else {
                [self.orderListTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:0 animated:NO];
                [self fetchCompletedOrdersAutomatically];
            }
        }
    }
    
    _accountDropdownView.locationLabel.text = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugName;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChangeOrderTraker:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    _isViewDidLoadCalled = NO;
    if (_isFetchingWhileScroll) {
        _isFetchingWhileScroll = NO;
    }
    if (self.networkRequestInProgress) {
        [self setNetworkRequestInProgress:NO];
        if (self.trackOrderType == TrackOrderDashBoardTypeOpenOrder) {
            self.openOrderViewModel.dashBoardDelegate = nil;
            [self.openOrderViewModel cancelDashBoardAPIRequest];
        } else {
            self.completedOrderViewModel.dashBoardDelegate = nil;
            [self.completedOrderViewModel cancelDashBoardAPIRequest];
        }
    }
    if (_isLazyLoadingInOpenOrder) {
        [self cancelOpenOrderAPI];
    }
    if (_isLazyLoadingInCompleted) {
        [self cancelCompletedOrderAPI];
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
    
}

- (NSInteger)completedOrderCount
{
    NSInteger _orderCount = 0;
    for (NSObject *obj in self.completedOrderData) {
        if ([obj isKindOfClass:[BTOrder class]]) {
            _orderCount++;
        }
    }
    return _orderCount;
}

- (NSInteger)openOrderCount
{
    NSInteger _orderCount = 0;
    for (NSObject *obj in self.openOrderData) {
        if ([obj isKindOfClass:[BTOrder class]]) {
            _orderCount++;
        }
    }
    return _orderCount;
}

- (NSInteger)completedProjectCount
{
    NSInteger _projectCount = 0;
    for (NSObject *obj in self.completedOrderData) {
        if ([obj isKindOfClass:[BTOCSProject class]]) {
            _projectCount++;
        }
    }
    return _projectCount;
}

- (NSInteger)openProjectCount
{
    NSInteger _projectCount = 0;
    for (NSObject *obj in self.openOrderData) {
        if ([obj isKindOfClass:[BTOCSProject class]]) {
            _projectCount++;
        }
    }
    return _projectCount;
}

#pragma mark CUGRelated Methods

- (void)updateGroupSelection {
    
     [self hideGroupSelectionForOneAvailableGroup];
    if (self.cugs.count == 1) {
        _pageNameForOmniture =  OMNIPAGE_ORDER_OPEN;
       
    } else {
    
        //Salman
        _pageNameForOmniture =  OMNIPAGE_ORDER_OPEN_CUG;
        BTCug *cug = (BTCug *)[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug;
        [self setCurrentGroupKey:cug.groupKey];
       // self.groupName.text = cug.cugName;
    }
}

- (void) checkForSuperUserAndUpdateUIWithIndex:(NSUInteger)index {
    for(BTCug *cug in self.cugs) {
        if(index == cug.indexInAPIResponse) {
            [self setCurrentGroupKey:cug.groupKey];
            //[self.groupName setText:cug.cugName];
            [self.openOrderViewModel changeSelectedCUGTo:cug];
            if(_accountDropdownView){
                _accountDropdownView.locationLabel.text = cug.cugName;
            }
            [self resetDataAndUIAfterGroupChange];
        }
    }
    
    _pageNameForOmniture = [self getPageNameForCurrentGroup];
}

- (void) hideGroupSelectionForOneAvailableGroup {
   // [self.groupName removeFromSuperview];
    
    UIFont *titleFont = self.titleLabel.font;
    self.titleLabel.font = [titleFont fontWithSize:20];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
}

- (void)resetDataAndUIAfterGroupChange {
    if (self.openOrderData.count > 0) {
        
        [self.openOrderData removeAllObjects];
        [self.orderListTableView reloadData];
    }
    if (self.completedOrderData.count > 0) {
        
        [self.completedOrderData removeAllObjects];
        [self.orderListTableView reloadData];
    }
    
    //Check internet
    if([AppManager isInternetConnectionAvailable])
    {
        [self resetUIAndMakeAPIRequest];
    }
    else
    {
        self.orderListTableView.tableFooterView.hidden = YES;
        [self hideEmmptyDashBoardItems:YES];
        [self showRetryViewWithInternetStrip:YES];
        //Track No Internet
        [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
    }
}

- (void) resetUIAndMakeAPIRequest
{
    if (_isRetryShown) {
        self.orderListTableView.tableFooterView.hidden = NO;
        [self hideRetryItems:YES];
    }
    if (_isEmptyViewShown) {
        self.orderListTableView.tableFooterView.hidden = NO;
        [self hideEmmptyDashBoardItems:YES];
    }
    
    if (self.ordersSegmentedControl.selectedSegmentIndex == 0) {
        self.trackOrderType = TrackOrderDashBoardTypeOpenOrder;
        _isResetGroup = YES;
        [self cancelOpenOrderAPI];
        if (self.openOrderCount == 0) {
            self.networkRequestInProgress = true;
            self.orderListTableView.tableFooterView.hidden = YES;
            [self fetchOrderDashBoardDetailsWithPageIndex:1];
        }
    } else if (self.ordersSegmentedControl.selectedSegmentIndex == 1) {
        self.trackOrderType = TrackOrderDashBoardTypeCompletedOrder;
        _isResetGroup = YES;
        [self cancelCompletedOrderAPI];
        if (self.completedOrderCount == 0) {
            
            self.networkRequestInProgress = true;
            self.orderListTableView.tableFooterView.hidden = YES;
            [self fetchOrderDashBoardDetailsWithPageIndex:1];
        }
    }

}

#pragma mark- Helper Method

- (void)loadNextOpenOrders{
    
    if (!_isFooterViewCreated) {
        
        [self createFooterView];
        [_orderDashBoardTableFooterView createCustomLoadingIndicator];
    }
    
    if(self.openOrderData.count == 0){
        [self showEmptyOrdersView];
    }
    int pageIndex = _openOrderViewModel.pageIndex;
    int totalSize = _openOrderViewModel.totalSize;
    if ((self.openOrderCount < 9) && (((pageIndex - 1) * 3) < totalSize)) {
    
        self.orderListTableView.tableFooterView.hidden = NO;
        if ([AppManager isInternetConnectionAvailable]) {
            
            _isLazyLoadingInOpenOrder = YES;
            [self hideShowFooterIndicatorView:NO];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self fetchOrderDashBoardDetailsWithPageIndex:pageIndex];
            });
        } else {
            self.orderListTableView.tableFooterView.hidden = NO;
            [_orderDashBoardTableFooterView showRetryViewWithErrorMessage:kNoConnectionMessage];
            [_orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
            //Track No Internet
            [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
        }
    } else {
        self.orderListTableView.tableFooterView.hidden = YES;
    }
    
    if ((self.openOrderCount == totalSize) || (self.openOrderCount >= 9)) {
        
        self.orderListTableView.tableFooterView.hidden = NO;
        _isLazyLoadingInOpenOrder = NO;
        //self.orderListTableView.tableFooterView.hidden = YES;
        [self hideShowFooterIndicatorView:YES];
        [_orderDashBoardTableFooterView hideRetryView];
    }
}

#pragma mark UIMethods

- (void)createFooterView {
    
    _isFooterViewCreated = YES;
    _orderDashBoardTableFooterView = [[[NSBundle mainBundle] loadNibNamed:@"BTOrderDashBoardTableFooterView" owner:nil options:nil] objectAtIndex:0];
    _orderDashBoardTableFooterView.orderDashBoardFooterDelegate = self;
    
    [self.orderListTableView setTableFooterView:_orderDashBoardTableFooterView];
    self.orderListTableView.tableFooterView.hidden = NO;
}

- (void) updateUserInterfaceWithProjects:(NSArray *)projects {
    if (self.trackOrderType == TrackOrderDashBoardTypeOpenOrder) {
        NSMutableArray *toRemove = [NSMutableArray new];
        for (NSObject *obj in self.openOrderData) {
            if ([obj isKindOfClass:[BTOCSProject class]]) {
                [toRemove addObject:obj];
            }
        }
        if ([toRemove count]) {
            [self.openOrderData removeObjectsInArray:toRemove];
        }
        for (BTOCSProject *project in projects) {
            if ([project.status.lowercaseString isEqualToString:@"open"]) {
                [self.openOrderData addObject:project];
            }
        }
    } else {
        NSMutableArray *toRemove = [NSMutableArray new];
        for (NSObject *obj in self.completedOrderData) {
            if ([obj isKindOfClass:[BTOCSProject class]]) {
                [toRemove addObject:obj];
            }
        }
        if ([toRemove count]) {
            [self.completedOrderData removeObjectsInArray:toRemove];
        }
        for (BTOCSProject *project in projects) {
            if (![project.status.lowercaseString isEqualToString:@"open"]) {
                [self.completedOrderData addObject:project];
            }
        }
    }
    [self.orderListTableView reloadData];
}

- (void) updateUserInterfaceWithOrders:(NSArray *)orders {
    if (self.trackOrderType == TrackOrderDashBoardTypeOpenOrder) {
        
        [self.openOrderData addObjectsFromArray:orders];
    } else {
        
        [self.completedOrderData addObjectsFromArray:orders];
    }
    [self.orderListTableView reloadData];
}

- (void)hideShowFooterIndicatorView:(BOOL)isHide {
    if (!isHide) {
        _orderDashBoardTableFooterView.hidden = NO;
        [_orderDashBoardTableFooterView hideRetryView];
        [_orderDashBoardTableFooterView startLoadingIndicatorAnimation];
    } else {
        
        [_orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
    }
}

#pragma mark fetchDetails Methods

- (void)fetchOrderDashBoardDetailsWithPageIndex:(int)pageIndex {
    
    //Calling Dashboard API call
    if (!_isLazyLoadingInOpenOrder && !_isLazyLoadingInCompleted) {
        [self setNetworkRequestInProgress:true];
    }
    if (self.trackOrderType == TrackOrderDashBoardTypeOpenOrder) {
        
        [self.openOrderViewModel setPageSize:3];
        [self.openOrderViewModel setPageIndex:pageIndex];
        [self.openOrderViewModel setTabID:self.trackOrderType];
        self.openOrderViewModel.dashBoardDelegate = self;
        [self.openOrderViewModel fetchOrderDashBoardDetailsForLoggedInUser];
    } else {
        
        [self.completedOrderViewModel setPageSize:3];
        [self.completedOrderViewModel setPageIndex:pageIndex];
        [self.completedOrderViewModel setTabID:self.trackOrderType];
        self.completedOrderViewModel.dashBoardDelegate = self;
        [self.completedOrderViewModel fetchOrderDashBoardDetailsForLoggedInUser];
    }
}

- (void)fetchOpenOrdersAutomatically {
    int pageIndex = _openOrderViewModel.pageIndex;
    int totalSize = _openOrderViewModel.totalSize;
    [_orderDashBoardTableFooterView hideRetryView];
    if ((self.openOrderCount < 9) && (((pageIndex - 1) * 3) < totalSize)) {
        if([AppManager isInternetConnectionAvailable]) {
            _isLazyLoadingInOpenOrder = YES;
            [self hideShowFooterIndicatorView:NO];
            self.orderListTableView.tableFooterView.hidden = NO;
            [self fetchOrderDashBoardDetailsWithPageIndex:pageIndex];
        } else {
            self.orderListTableView.tableFooterView.hidden = NO;
            [_orderDashBoardTableFooterView showRetryViewWithErrorMessage:kNoConnectionMessage];
            [_orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
            //Track No Internet
            [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
        }
    }
}

- (void)fetchCompletedOrdersAutomatically {
    int pageIndex = _completedOrderViewModel.pageIndex;
    int totalSize = _completedOrderViewModel.totalSize;
    [_orderDashBoardTableFooterView hideRetryView];
    if ((self.completedOrderCount < 9) && (((pageIndex - 1) * 3) < totalSize)) {
        if([AppManager isInternetConnectionAvailable]) {
            _isLazyLoadingInCompleted = YES;
            [self hideShowFooterIndicatorView:NO];
            self.orderListTableView.tableFooterView.hidden = NO;
            [self fetchOrderDashBoardDetailsWithPageIndex:pageIndex];
        } else {
            self.orderListTableView.tableFooterView.hidden = NO;
            [_orderDashBoardTableFooterView showRetryViewWithErrorMessage:kNoConnectionMessage];
            [_orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
            //Track No Internet
            [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
        }

    }
}

#pragma mark CancellingAPIMethods

- (void) cancelOpenOrderAPI {
    if (_isLazyLoadingInOpenOrder) {
        _islazyLoadingOpenClosed = YES;
        _isLazyLoadingInOpenOrder = NO;
        if (_openOrderAPIInProgress) {
            _openOrderAPIInProgress = NO;
        }
        [self hideShowFooterIndicatorView:YES];
    }
    [self.openOrderViewModel cancelDashBoardAPIRequest];
}

- (void)cancelCompletedOrderAPI {
    if (_isLazyLoadingInCompleted) {
        _isLazyLoadingCompletedClosed = YES;
        _isLazyLoadingInCompleted = NO;
        if (_completedOrderAPIInProgress) {
            _completedOrderAPIInProgress = NO;
        }
        [self hideShowFooterIndicatorView:YES];
    }
    self.completedOrderViewModel.dashBoardDelegate = nil;
    [self.completedOrderViewModel cancelDashBoardAPIRequest];
}

#pragma mark - table view methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 0;
    if ([self.ordersSegmentedControl selectedSegmentIndex] == 0) {
        // open orders
        rows = self.openOrderData.count;
    } else if ([self.ordersSegmentedControl selectedSegmentIndex] == 1) {
        // completed orders
        rows = self.completedOrderData.count;
    }
    return rows;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTOrderTrackerDashboardTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"orderTrackerDashboardTableViewCell" forIndexPath:indexPath];
    [cell.infoButton addTarget:self action:@selector(infoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    NSInteger row = indexPath.row;
    
    if ([self.ordersSegmentedControl selectedSegmentIndex] == 0) {
        //For open Orders
        NSObject *openOrderData = [self.openOrderData objectAtIndex:row];
        if ([openOrderData isKindOfClass:[BTOCSProject class]]) {
            [cell updateCellWithProjectData:(BTOCSProject*)openOrderData isOpenOrder:YES];
        } else {
            [cell updateCellWithOrderData:(BTOrder*)openOrderData isOpenOrder:YES];
        }
    } else {
        //For Completed Orders
        NSObject *completedOrderData = [self.completedOrderData objectAtIndex:row];
        if ([completedOrderData isKindOfClass:[BTOCSProject class]]) {
            [cell updateCellWithProjectData:(BTOCSProject*)completedOrderData isOpenOrder:NO];
        } else {
            [cell updateCellWithOrderData:(BTOrder*)completedOrderData isOpenOrder:NO];
        }
    }
    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //Order details could not be fetched
    if (self.ordersSegmentedControl.selectedSegmentIndex == 0) {
        
        if (self.openOrderData.count ==  indexPath.row) {
            return indexPath;

        } else {
            BTOrderTrackerDashboardTableViewCell *cell = (BTOrderTrackerDashboardTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            // rows in section 0 should not be selectable
            
            if ([cell.orderDescription isEqualToString:@"Order details could not be fetched"]) return nil;
        }
    } else if (self.ordersSegmentedControl.selectedSegmentIndex == 1) {
        if (self.completedOrderData.count == indexPath.row) {
            return indexPath;
        } else {
            BTOrderTrackerDashboardTableViewCell *cell = (BTOrderTrackerDashboardTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            // rows in section 0 should not be selectable
            
            if ([cell.orderDescription isEqualToString:@"Order details could not be fetched"]) return nil;
        }
    }
    
    
    // By default, allow row to be selected
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.ordersSegmentedControl selectedSegmentIndex] == 0) {
        NSObject *orderData = [self.openOrderData objectAtIndex:indexPath.row];
        if ([orderData isKindOfClass:[BTOCSProject class]]) {
            // OCS screen
            BTOCSProject *project = (BTOCSProject*)orderData;
            BTOCSOrderDetailsTableViewController *vc = [BTOCSOrderDetailsTableViewController getOCSOrderDetailsViewController];
            vc.project = project;
            [self.navigationController pushViewController:vc animated:YES];
        } else {
            BTOrder *order = (BTOrder*)orderData;
            //Launch TrackOrderSummary screen for Open orders.
            //Track event
            NSString *linkName = [NSString stringWithFormat:@"%@ %@",order.orderStatus,OMNICLICK_ORDER_DETAILS];
            [self trackOmniClick:linkName forPage:_pageNameForOmniture];
            BTOrderTrackerDetailsViewController *orderTrackerDetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderTrackerDetailViewController"];
            [orderTrackerDetailsViewController setOrderRef:order.orderRef];
            // uncomment following line to hardcode order for testing
            // [orderTrackerDetailsViewController setOrderRef:@"BT23R3FD"];
            [self.navigationController pushViewController:orderTrackerDetailsViewController animated:YES];
        }
    } else {
        NSObject *orderData = [self.completedOrderData objectAtIndex:indexPath.row];
        if ([orderData isKindOfClass:[BTOCSProject class]]) {
            // OCS screen
            BTOCSProject *project = (BTOCSProject*)orderData;
            BTOCSOrderDetailsTableViewController *vc = [BTOCSOrderDetailsTableViewController getOCSOrderDetailsViewController];
            vc.project = project;
            [self.navigationController pushViewController:vc animated:YES];
        } else {
            //Launch TrackOrderSummary screen for Completed order.
            BTOrder *order = (BTOrder*)orderData;
            NSString *linkName = [NSString stringWithFormat:@"%@ %@",order.orderStatus,OMNICLICK_ORDER_DETAILS];
            [self trackOmniClick:linkName forPage:_pageNameForOmniture];
            
            BTOrderTrackerDetailsViewController *orderTrackerDetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderTrackerDetailViewController"];
            [orderTrackerDetailsViewController setOrderRef:order.orderRef];
            [self.navigationController pushViewController:orderTrackerDetailsViewController animated:YES];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark ActionMethods

- (void) infoButtonAction:(id)sender {
    UIButton *button = (UIButton *)sender;
    
    BTOrderTrackerDashboardTableViewCell *cell = (BTOrderTrackerDashboardTableViewCell *)[[[button superview] superview] superview];
    NSString *orderRef = cell.orderID;
    
    DDLogInfo(@"Info : Can't dispaly order %@ yet.", orderRef);
    NSString *alertMessage = [NSString stringWithFormat:@"We’re sorry but we can’t display your order %@ yet.", orderRef];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Please try again later" message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)home:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)searchOrderReference:(id)sender {
    BTOrderTrackerHomeViewController *orderTrackerInputViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderTrackerInputViewController"];
    [self trackOmniClick:OMNICLICK_ORDER_SEARCH forPage:_pageNameForOmniture];
    
    [self.navigationController pushViewController:orderTrackerInputViewController animated:YES];
}

- (IBAction)groupBarButtonAction:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Select Group" preferredStyle:UIAlertControllerStyleActionSheet];
    
    DDLogVerbose(@"Home: Total cugs belongs to user %lu.",(unsigned long)self.cugs.count);
    
    // (LP) Add group names in actionsheet
    if (self.cugs != nil && self.cugs.count > 0) {
        
        // (LP) Add action for each cug selection.
        int index = 0;
        for (BTCug *groupData in self.cugs) {
            __weak typeof(self) weakSelf = self;
            UIAlertAction *action = [UIAlertAction actionWithTitle:groupData.cugName
                                                             style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                 [weakSelf checkForSuperUserAndUpdateUIWithIndex:groupData.indexInAPIResponse];
                                                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                                             }];
            [alert addAction:action];
            
            index++;
        }
    }
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    [alert addAction:cancel];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = _accountDropdownView.frame;
    alert.popoverPresentationController.canOverlapSourceViewRect = NO;
    
    [self presentViewController:alert animated:YES completion:^{
        
    }];
}

- (IBAction)orderTypeChanged:(id)sender {
    //When clicked on the segment for loading the other set of records when open or completed order Arrays are empty.
    int index = (int)[self.ordersSegmentedControl selectedSegmentIndex];
    
    _pageNameForOmniture = [self getPageNameForCurrentGroup];
    [self trackOmniPage:_pageNameForOmniture];
    if(!index)
    {
        self.trackOrderType = TrackOrderDashBoardTypeOpenOrder;
    }
    else
    {
        self.trackOrderType = TrackOrderDashBoardTypeCompletedOrder;
    }
    
    
    if(self.trackOrderType == TrackOrderDashBoardTypeOpenOrder)
    {
        //If API is not executed control comes here
        if (_isRetryShown) {
            
            //self.faultDashboardTableView.tableFooterView.hidden = NO;
            [self hideRetryItems:YES];
            
        }
        
        if (_isEmptyViewShown) {
            
            //self.faultDashboardTableView.tableFooterView.hidden = NO;
            [self hideEmmptyDashBoardItems:YES];
        }
        
    }
    /*else
    {
       if(self.trackOrderType == TrackOrderDashBoardTypeOpenOrder)
        {
            
            if([self.openOrderData count] == 0)
            {
                [self showEmptyOrdersView];
            }
            else
            {
              //  self.faultDashboardTableView.tableFooterView.hidden = NO;
                [self hideEmmptyDashBoardItems:YES];
                [self hideRetryItems:YES];
            }
        }
    }*/

    
    //For second tab
    if(self.trackOrderType == TrackOrderDashBoardTypeCompletedOrder)
    {
        //If API is not executed control comes here
        if (_isRetryShown) {
            
           // self.faultDashboardTableView.tableFooterView.hidden = NO;
            [self hideRetryItems:YES];
            
        }
        
        if (_isEmptyViewShown) {
        
       //  self.faultDashboardTableView.tableFooterView.hidden = NO;
           [self hideEmmptyDashBoardItems:YES];
        }
        
    }
    /*else
    {
       if(self.trackOrderType == TrackOrderDashBoardTypeCompletedOrder)
        {
            if([self.completedOrderData count] == 0)
            {
                [self showEmptyOrdersView];
            }
            else
            {
              //  self.faultDashboardTableView.tableFooterView.hidden = NO;
                [self hideEmmptyDashBoardItems:YES];
                [self hideRetryItems:YES];
                
            }
        }
    }*/
    
    [self.orderListTableView reloadData];
    long selectedSegment = (long)[self.ordersSegmentedControl selectedSegmentIndex];
    switch (selectedSegment) {
        case 0:// for the OpenOrders
            _isTabSwitch = YES;
            [self cancelCompletedOrderAPI];
            if (_isFetchingWhileScroll) {
                _isFetchingWhileScroll = NO;
            }
            _needToRemoveLoadingViewFromScreen = NO;
            self.trackOrderType = TrackOrderDashBoardTypeOpenOrder;
            if (self.openOrderCount == 0) {
                self.orderListTableView.tableFooterView.hidden = YES;
                
                if([AppManager isInternetConnectionAvailable])
                {
                    [self fetchOrderDashBoardDetailsWithPageIndex:1];
                }
                else
                {
                    [self showRetryViewWithInternetStrip:YES];
                    //Track No Internet
                    [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
                }
                
            } else {
              [self fetchOpenOrdersAutomatically];
            }
            break;
        case 1:// for the CompletedOrders
            _isTabSwitch = YES;
            [self cancelOpenOrderAPI];
            if (_isFetchingWhileScroll) {
                _isFetchingWhileScroll = NO;
            }
            _needToRemoveLoadingViewFromScreen = YES;
            self.trackOrderType = TrackOrderDashBoardTypeCompletedOrder;
            if (self.completedOrderCount == 0) {
                self.orderListTableView.tableFooterView.hidden = YES;
                 if([AppManager isInternetConnectionAvailable])
                 {
                     [self fetchOrderDashBoardDetailsWithPageIndex:1];
                 }
                else
                {
                    [self showRetryViewWithInternetStrip:YES];
                    //Track No Internet
                    [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
                }
            } else {
                [self fetchCompletedOrdersAutomatically];
            }
            break;
    }
}



#pragma mark DLMTrackOrderDashBoardScreenDelegate Methods

- (void)successfullyFetchedOrderDashBoardDataOnTrackOrderDashBoardScreen:(DLMTrackOrderDashBoardScreen *)dashBoardScreen {
    _needToRemoveLoadingViewFromScreen = NO;
    [self setNetworkRequestInProgress:false];
    self.ordersSegmentedControl.enabled = true;
    if (_isFetchingWhileScroll) {
        _isFetchingWhileScroll = NO;
    }
        
    if (dashBoardScreen.orders.count == 0 && dashBoardScreen.projects.count == 0) {

        if (!_isLazyLoadingInCompleted && !_isLazyLoadingInOpenOrder) {
            
            //Tracker
            if (self.ordersSegmentedControl.selectedSegmentIndex == 0)
            {
                _openOrderAPIInProgress = NO;
                [self trackOmniPage:OMNIPAGE_ORDER_OPEN_EMPTY];
            }
            else
            {
                _completedOrderAPIInProgress = NO;
                 [self trackOmniPage:OMNIPAGE_ORDER_CLOSED_EMPTY];
            }//Tracker
            
            [self showEmptyOrdersView];
        }
    } else {
        
        if (dashBoardScreen.projects.count > 0) {
            [self updateUserInterfaceWithProjects:dashBoardScreen.projects];
        }
        if (dashBoardScreen.orders.count > 0) {
            [self updateUserInterfaceWithOrders:dashBoardScreen.orders];
        }
        
        
        if (self.ordersSegmentedControl.selectedSegmentIndex == 0) {

            _openOrderAPIInProgress = NO;
            [self performSelector:@selector(loadNextOpenOrders) withObject:nil afterDelay:0.03];
            
        } else if (self.ordersSegmentedControl.selectedSegmentIndex == 1) {
            
            _completedOrderAPIInProgress = NO;
            if (!_isFooterViewCreated) {
                
                [self createFooterView];
                [_orderDashBoardTableFooterView createCustomLoadingIndicator];
            }
            if(self.completedOrderData.count == 0){
                 [self showEmptyOrdersView];
            }
            self.orderListTableView.tableFooterView.hidden = NO;
            int pageIndex = _completedOrderViewModel.pageIndex;
            int totalSize = _completedOrderViewModel.totalSize;
            if ((self.completedOrderCount < 9) && (((pageIndex - 1) * 3) < totalSize)) {
                
                if ([AppManager isInternetConnectionAvailable]) {
                    _isLazyLoadingInCompleted = YES;
                    [self hideShowFooterIndicatorView:NO];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        _completedOrderAPIInProgress = YES;
                        [self fetchOrderDashBoardDetailsWithPageIndex:pageIndex];
                    });
                } else {
                    self.orderListTableView.tableFooterView.hidden = NO;
                    [_orderDashBoardTableFooterView showRetryViewWithErrorMessage:kNoConnectionMessage];
                    [_orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
                    //Track No Internet
                    [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
                }
            }
            
            if ((self.completedOrderCount == totalSize) || (self.completedOrderCount >= 9)) {
                
                _isLazyLoadingInCompleted = NO;
                [self hideShowFooterIndicatorView:YES];
                [_orderDashBoardTableFooterView hideRetryView];
            }
        }

    }
}

- (void)trackorderDashBoardScreen:(DLMTrackOrderDashBoardScreen *)dashBoardScreen failedToFetchOrderDashBoardDataWithWebServiceError:(NLWebServiceError *)error {
    DDLogError(@"Order DashBoard: Fetch order DashBoard details failed");
    
    if(self.ordersSegmentedControl.selectedSegmentIndex == 0)
    {
        _openOrderAPIInProgress = NO;
    }
    else if (self.ordersSegmentedControl.selectedSegmentIndex == 1)
    {
        _completedOrderAPIInProgress = NO;
    }
    
    [self trackOmnitureError:error.error.localizedDescription];
    self.ordersSegmentedControl.enabled = true;
    if (_isFetchingWhileScroll) {
        _isFetchingWhileScroll = NO;
    }

    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if(errorHandled)
        [AppManager trackGenericAPIErrorOnPage:[self getPageNameForCurrentGroup]];
    
    if ([error.error.domain isEqualToString:BTNetworkErrorDomain] && (errorHandled == NO)) {
        //(VRK) If more option api is cancelled while switching the tab the following condition will occur
        
        if (isTabSwitchingForMoreData) {
            isTabSwitchingForMoreData = NO;
            return;
        }
        if (self.ordersSegmentedControl.selectedSegmentIndex == 0) {
            
            if (_isTabSwitch && _isLazyLoadingCompletedClosed) {
                _isLazyLoadingCompletedClosed = NO;
                _isTabSwitch = NO;
                return;
            }
            if (_isResetGroup && _islazyLoadingOpenClosed) {
                _islazyLoadingOpenClosed = NO;
                _isResetGroup = NO;
                return;
            }
        } else if (self.ordersSegmentedControl.selectedSegmentIndex == 1) {
            
            if (_isTabSwitch && _islazyLoadingOpenClosed) {
                _islazyLoadingOpenClosed = NO;
                _isTabSwitch = NO;
                return;
            }
            if (_isResetGroup && _isLazyLoadingCompletedClosed) {
                _isLazyLoadingCompletedClosed = NO;
                _isResetGroup = NO;
                return;
            }
        }
        
        [self setNetworkRequestInProgress:false];
        // (lp) Retry to fetch order details
        if (!_isLazyLoadingInOpenOrder && !_isLazyLoadingInCompleted) {
            
            if (dashBoardScreen.projects.count > 0) {
                [self updateUserInterfaceWithProjects:dashBoardScreen.projects];
            }
            if (dashBoardScreen.orders.count > 0) {
                [self updateUserInterfaceWithOrders:dashBoardScreen.orders];
            }
            
            if (error.error.code == BTNetworkErrorCodeAPINoDataFound)
            {
                [AppManager trackNoDataFoundErrorOnPage:[self getPageNameForCurrentGroup]];
                if (self.ordersSegmentedControl.selectedSegmentIndex == 0) {
                    if (self.openOrderCount == 0 && self.openProjectCount == 0) {
                        [self showEmptyDashBoardView];
                    }
                } else if (self.ordersSegmentedControl.selectedSegmentIndex == 1) {
                    if (self.completedOrderCount == 0 && self.completedProjectCount == 0) {
                        [self showEmptyDashBoardView];
                    }
                }
            }
            else
            {
                [AppManager trackGenericAPIErrorOnPage:[self getPageNameForCurrentGroup]];
                if (self.ordersSegmentedControl.selectedSegmentIndex == 0) {
                    if (self.openOrderCount == 0 && self.openProjectCount == 0) {
                        [self showRetryViewWithInternetStrip:NO];
                    }
                } else if (self.ordersSegmentedControl.selectedSegmentIndex == 1) {
                    if (self.completedOrderCount == 0 && self.completedProjectCount == 0) {
                        [self showRetryViewWithInternetStrip:NO];
                    }
                }
            }
        }
        else
        {
            [AppManager trackGenericAPIErrorOnPage:[self getPageNameForCurrentGroup]];
            [_orderDashBoardTableFooterView showRetryView];
            [_orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
        }
    } else if (errorHandled == NO) {
        
        //(VRK) If more option api is cancelled while switching the tab the following condition will occur
        if (isTabSwitchingForMoreData) {
            isTabSwitchingForMoreData = NO;
            return;
        }
        if (self.ordersSegmentedControl.selectedSegmentIndex == 0) {
            
            if (_isTabSwitch && _isLazyLoadingCompletedClosed) {
                _isLazyLoadingCompletedClosed = NO;
                _isTabSwitch = NO;
                return;
            }
            if (_isResetGroup && _islazyLoadingOpenClosed) {
                _islazyLoadingOpenClosed = NO;
                _isResetGroup = NO;
                return;
            }
        } else if (self.ordersSegmentedControl.selectedSegmentIndex == 1) {
            
            if (_isTabSwitch && _islazyLoadingOpenClosed && !_needToRemoveLoadingViewFromScreen) {
                _islazyLoadingOpenClosed = NO;
                _isTabSwitch = NO;
                return;
            }
            if (_isResetGroup && _isLazyLoadingCompletedClosed) {
                _isLazyLoadingCompletedClosed = NO;
                _isResetGroup = NO;
                return;
            }
        }
        
        [self setNetworkRequestInProgress:false];
        // (lp) Retry to fetch order details
        if (!_isLazyLoadingInCompleted && !_isLazyLoadingInOpenOrder)
        {
            if (error.error.code == BTNetworkErrorCodeAPINoDataFound)
            {
                [AppManager trackNoDataFoundErrorOnPage:[self getPageNameForCurrentGroup]];
                [self showEmptyDashBoardView];
            }
            else
            {
                [AppManager trackGenericAPIErrorOnPage:[self getPageNameForCurrentGroup]];
                [self showRetryViewWithInternetStrip:NO];
            }
        } else {
            if (_isLazyLoadingInOpenOrder) {
                _isLazyLoadingInOpenOrder = NO;
            }
            if (_isLazyLoadingInCompleted) {
                _isLazyLoadingInCompleted = NO;
            }
            
            [AppManager trackGenericAPIErrorOnPage:[self getPageNameForCurrentGroup]];
            [_orderDashBoardTableFooterView showRetryView];
            [_orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
        }
    }
    
}

#pragma mark BTOrderDashBoardTableFooterDelegate

- (void) orderDashBoardTableFooterView:(BTOrderDashBoardTableFooterView *)orderDashBoardFooterView retryBUttonActionMethod:(id)sender {
    
    if(![AppManager isInternetConnectionAvailable]) {
        [orderDashBoardFooterView showRetryViewWithErrorMessage:kNoConnectionMessage];
        //Track No Internet
        [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
        return;
    }
    
    if (self.ordersSegmentedControl.selectedSegmentIndex == 0) {
        [orderDashBoardFooterView hideRetryView];
        [orderDashBoardFooterView showCustomLoadingIndicator];
        _isLazyLoadingInOpenOrder = YES;
        [self hideShowFooterIndicatorView:NO];
        _openOrderAPIInProgress = YES;
        [self fetchOrderDashBoardDetailsWithPageIndex:_openOrderViewModel.pageIndex];
    } else {
        [orderDashBoardFooterView hideRetryView];
        [orderDashBoardFooterView showCustomLoadingIndicator];
        _isLazyLoadingInCompleted = YES;
        [self hideShowFooterIndicatorView:NO];
        _completedOrderAPIInProgress = YES;
        [self fetchOrderDashBoardDetailsWithPageIndex:_completedOrderViewModel.pageIndex];
    }
}

#pragma mark ErrorHandling & Loading Methods



- (void)statusBarOrientationChangeOrderTraker:(NSNotification *)notification {
    // handle the interface orientation as needed
    [self performSelector:@selector(loadSpinner) withObject:nil afterDelay:0.1];
    if(_isLazyLoadingInOpenOrder == YES)
        [_orderDashBoardTableFooterView startLoadingIndicatorAnimation];
}

- (void)createLoadingView {
    
    if(_loadingView == nil){
        _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
    }
    [self performSelector:@selector(loadSpinner) withObject:nil afterDelay:0.1];
}

- (void) loadSpinner{
    
    if(_loadingView.isHidden == YES){
        return;
    }
   
    [_loadingView lazyLoadSpinView];
    _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:_loadingView];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}
- (void)hideLoadingItems:(BOOL)isHide {
    
    // (lp) Hide or Show UI elements related to loading.
    [_loadingView setHidden:isHide];
    
}

- (void)showRetryView {
    
    if(_isRetryShown)
        return;
    
    _isRetryShown = YES;
    
    _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
    _retryView.translatesAutoresizingMaskIntoConstraints = NO;
    _retryView.retryViewDelegate = self;
    
    [self.view addSubview:_retryView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.ordersSegmentedControl  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:10.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
}

- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow {
    
    //if(_isRetryShown)
       // return;
    
    //_isRetryShown = YES;
    
    self.orderListTableView.tableFooterView.hidden = YES;
    if(_retryView){
        [_retryView removeFromSuperview];
    }
    _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
    _retryView.translatesAutoresizingMaskIntoConstraints = NO;
    _retryView.retryViewDelegate = self;
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
    
    [self.view addSubview:_retryView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    if(internetStripNeedToShow==YES){
    
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    } else{
        
        //[self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_accountDropdownView  attribute:NSLayoutAttributeTop multiplier:1.0 constant:_accountDropdownView.frame.size.height]];
        if(_accountDropdownView){
            [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_accountDropdownView  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:10.0]];
        } else{
            [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.segmentContainerView  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:10.0]];
        }
    }
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
}


- (void)hideRetryItems:(BOOL)isHide {
    
    _isRetryShown = NO;
    // (lp) Hide or Show UI elements related to retry.
    [_retryView setRetryViewDelegate:nil];
    [_retryView setHidden:isHide];
    
}

- (void)showEmptyDashBoardView {
    
    if(_isEmptyViewShown)
        return;
    
    
    _isEmptyViewShown = YES;
    _emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTEmptyDashboardView" owner:nil options:nil] objectAtIndex:0];
    _emptyDashboardView.translatesAutoresizingMaskIntoConstraints = NO;
    _emptyDashboardView.emptyDashboardViewDelegate = self;
    
    NSURL *url;
    
    if ([kBTServerType isEqualToString:@"BTServerTypeProd"]) {
        url = [[NSURL alloc] initWithString:@"https://secure.business.bt.com/Account/Addbillingaccounts.aspx"];
    } else{
        url = [[NSURL alloc] initWithString:@"https://eric1-dmze2e-ukb.bt.com/Account/Addbillingaccounts.aspx"];
    }
    
    NSMutableAttributedString *hypLink = [[NSMutableAttributedString  alloc] initWithString:@"Can't see your order here? We are either still working on processing your order to view online, or you need to add your account by going here"];
    
    NSDictionary *linkAttributes = @{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle),
                                     NSLinkAttributeName : url};
    
    [hypLink addAttributes:linkAttributes range:NSMakeRange(hypLink.length-4,4)];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
    [hypLink addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,hypLink.length)];
    
    [hypLink addAttribute:NSForegroundColorAttributeName value:[BrandColours colourBtNeutral70] range:NSMakeRange(0, hypLink.length)];
    
    [hypLink addAttribute:NSFontAttributeName
                    value:[UIFont fontWithName:kBtFontRegular size:16.0] range:NSMakeRange(0, hypLink.length)];
    
    [hypLink addAttribute:NSFontAttributeName
                    value:[UIFont fontWithName:kBtFontRegular size:22.0] range:NSMakeRange(hypLink.length - 4, 4)];
    
    [_emptyDashboardView updateEmptyDashboardViewWithImageNameAndLink:@"package" title:@"No orders to display" detailText:hypLink andButtonTitle:@"showSearchButton"];
    
    [self.view addSubview:_emptyDashboardView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    if(_accountDropdownView){
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_accountDropdownView  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:10.0]];
    } else{
         [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.segmentContainerView  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:10.0]];
    }
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
}

- (void)hideEmmptyDashBoardItems:(BOOL)isHide {
    
    _isEmptyViewShown = NO;
    // (lp) Hide or Show UI elements related to retry.
    [_emptyDashboardView setEmptyDashboardViewDelegate:nil];
    [_emptyDashboardView setHidden:isHide];
    
}

- (void) showEmptyOrdersView {

    [self showEmptyDashBoardView];
}

#pragma mark - BTRetryView delegate methods
- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView {
    
    if([AppManager isInternetConnectionAvailable]) {
    
        [self trackOmniClick:OMNICLICK_ORDER_RETRY forPage:_pageNameForOmniture];
        
        DDLogInfo(@"Retry to fecth order Dashboard details ");
        [self hideRetryItems:YES];
        [self fetchOrderDashBoardDetailsWithPageIndex:1];
    } else {
        //[_retryView updateRetryViewWithInternetStrip:YES];
        //Track No Internet
        _isRetryShown = NO;
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
    }
}

#pragma mark - BTEmptyDashboardView delegate methods
- (void)userPressedSearchByOrderReferenceButtonOfEmptyDashboardView:(BTEmptyDashboardView *)retryView {
    [self hideEmmptyDashBoardItems:YES];
    BTOrderTrackerHomeViewController *orderSearchViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderTrackerInputViewController"];
    [self.navigationController pushViewController:orderSearchViewController animated:YES];
}

#pragma mark Scrollview Delegate methods 

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    //NSInteger result = maximumOffset - currentOffset;
    
    // Change 10.0 to adjust the distance from bottom
    if (maximumOffset - currentOffset <= 10.0) {
        
        if (!scrollView.dragging && !scrollView.decelerating)
        {
            
            //self.networkRequestInProgress = NO;
            if (self.ordersSegmentedControl.selectedSegmentIndex == 0) {
                
                int pageIndex = _openOrderViewModel.pageIndex;
                //int totalSize = _openOrderViewModel.totalSize;
                int numberOfPages = [_openOrderViewModel getTotalNumberOfPagesToFetch];
                
                if (pageIndex <= numberOfPages && self.openOrderCount >= 9)
                {
                    if (!_isFetchingWhileScroll && !_openOrderAPIInProgress) {

                        if ([AppManager isInternetConnectionAvailable]) {
                            
                            _isLazyLoadingInOpenOrder = YES;
                            _isFetchingWhileScroll = YES;
                            [self hideShowFooterIndicatorView:NO];
                            self.orderListTableView.tableFooterView.hidden = NO;
                            [self fetchOrderDashBoardDetailsWithPageIndex:pageIndex];
                        } else {
                            self.orderListTableView.tableFooterView.hidden = NO;
                            [_orderDashBoardTableFooterView showRetryViewWithErrorMessage:kNoConnectionMessage];
                            [_orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
                            //Track No Internet
                            [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
                        }
                    }
                }
            } else if (self.ordersSegmentedControl.selectedSegmentIndex == 1) {
                int pageIndex = _completedOrderViewModel.pageIndex;
                //int totalSize = _completedOrderViewModel.totalSize;
                int numberOfPages = [_completedOrderViewModel getTotalNumberOfPagesToFetch];
                
                if (pageIndex <= numberOfPages && self.completedOrderCount >= 9) {
                    if (!_isFetchingWhileScroll && !_completedOrderAPIInProgress) {
                        if ([AppManager isInternetConnectionAvailable]) {
                            
                            _isLazyLoadingInCompleted = YES;
                            _isFetchingWhileScroll = YES;
                            [self hideShowFooterIndicatorView:NO];
                            self.orderListTableView.tableFooterView.hidden = NO;
                            [self fetchOrderDashBoardDetailsWithPageIndex:pageIndex];
                        } else {
                            self.orderListTableView.tableFooterView.hidden = NO;
                            [_orderDashBoardTableFooterView showRetryViewWithErrorMessage:kNoConnectionMessage];
                            [_orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
                            //Track No Internet
                            [AppManager trackNoInternetErrorOnPage:[self getPageNameForCurrentGroup]];
                        }
                    }
                }
            }
        }
    }
}

#pragma mark Omniture related Methods

- (void)trackOmniPage:(NSString *)page
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:[AppManager getOmniDictionary]];
    
    [OmnitureManager trackPage:page withContextInfo:params];
}


- (NSString *)getPageNameForCurrentGroup
{
    NSString *pageName;
    int index = (int)[self.ordersSegmentedControl selectedSegmentIndex];
    if(self.cugs.count == 1)
    {
        if(index ==0)
           {
               pageName = OMNIPAGE_ORDER_OPEN;
           }
           else
           {
               pageName = OMNIPAGE_ORDER_COMPLETED;
               
           }
    }
    else
       {
           if(index == 0)
           {
               pageName = OMNIPAGE_ORDER_OPEN_CUG;
               
           }
           else
           {
               pageName = OMNIPAGE_ORDER_COMPLETED_CUG;
           }
       }
           
    return  pageName;
    
}

- (void)trackOmniClick:(NSString *)clickEvent forPage:(NSString *)page
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:[AppManager getOmniDictionary]];
    NSString *pageName = page;
    NSString *linkTitle = clickEvent;
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:params];
}

- (void)trackOmnitureError:(NSString *)error
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = [self getPageNameForCurrentGroup];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackError:error onPageWithName:pageName contextInfo:data];
}

@end
