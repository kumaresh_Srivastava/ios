//
//  BTOrderTrackerDashboardViewController.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 29/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger, TrackOrderDashBoardType) {
    TrackOrderDashBoardTypeOpenOrder   = 1,
    TrackOrderDashBoardTypeCompletedOrder  = 2
};

@interface BTOrderTrackerDashboardViewController : UIViewController
@property (nonatomic) BOOL networkRequestInProgress;
@property (nonatomic) NSArray *cugs;
@property (nonatomic) NSDictionary *firstPageOpenOrderDataDict;

+ (BTOrderTrackerDashboardViewController *)getOrderTrackerDashboardViewController;


@end
