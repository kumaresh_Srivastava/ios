//
//  DLMBillDetailsScreen.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 05/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
#import "BTProductCharge.h"

@class DLMBillDetailsScreen;
@class BTBill;
@class BTBillCharges;
@class NLWebServiceError;

@protocol DLMBillDetailsScreenDelegate <NSObject>

- (void)successfullyFetchedBillDetailsDataOnBillDetailsScreen:(DLMBillDetailsScreen *)billDetailsScreen;

- (void)billDetailsScreen:(DLMBillDetailsScreen *)billDetailsScreen failedToFetchBillDetailsDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end


typedef enum typeOfBillDetailCell
{
    DurationCell,
    BillingStatusCell,
    BillingSummaryCell,
    AccountInfoCell,
    TakeActionCell,
    ActionCell,
    ChargesCell,
    NoChargesCell
    
}TypeOfBillDetailCell;


@interface DLMBillDetailsScreen : DLMObject {
    
}

@property (nonatomic, strong) BTBill *bill;
@property (nonatomic, strong) BTBillCharges *billCharges;
@property (nonatomic, copy) NSArray<BTProductCharge*> *arrayOfProductCharges;
@property (nonatomic, weak) id <DLMBillDetailsScreenDelegate> billDetailScreenDelegate;

- (void)fetchBillDetailsDataWithBillingAccountNumber:(NSString *)BAC;
- (void)cancelBillDetailAPIRequest;
- (void)fetchBillDetailsDataWithBill:(BTBill *)bill;//used only in bill dashboard (RLM)

@end



@interface BillDetailsScreenWrapper : NSObject

@property (nonatomic, strong) NSString *billingAccountNo;
@property (nonatomic, strong) NSString *billDate;
@property (nonatomic, strong) NSString *billStatus;
@property (nonatomic, assign) double totalCharges;
@property (nonatomic, strong) NSString *accountName;
@property (nonatomic, strong) NSString *paymentDueDate;
@property (nonatomic, strong) NSString *nextBillDate;
@property (nonatomic, strong) NSString *billRef;
@property (nonatomic, strong) NSString *billType;
@property (nonatomic, strong) NSString *billingNameAndAddress;


@property (nonatomic,assign) BOOL isPaid;
@property (strong,nonatomic) NSString *status;
@property (strong,nonatomic) NSString *statusDesc;
@property (strong,nonatomic) NSString *billingAccountSystem;
@property (assign,nonatomic) NSInteger billingVersionNumber;

@property (nonatomic, assign) double adjustments;
@property (nonatomic, assign) double totalNotIncVat;
@property (nonatomic, assign) double totalVat;
@property (nonatomic, assign) double totalIncVat;

@property (nonatomic, strong) NSArray<BTProductCharge*> *arrayOfProductCharges;
@property (assign,nonatomic) BOOL needToShowDiscription;
@property (assign,nonatomic) BOOL needToShowSwitch;
@property (assign,nonatomic) BOOL isUserAllowedToDownloadPDF;

@property (strong,nonatomic) NSString *summaryOfChargesDiscription;
@property (assign,nonatomic) TypeOfBillDetailCell cellType;

@property (strong,nonatomic) NSString *takeActionText;
@property (strong,nonatomic) NSString *takeActionDetailedText;

//Properties for charges

@property (strong,nonatomic) NSString *chargesTitle;
@property (strong,nonatomic) NSString *chargesTitleDiscription;

@property (strong,nonatomic) NSString *chargesDetailedTitle;
@property (strong,nonatomic) NSString *chargesDetailedTitleDiscription;

@property (assign,nonatomic) BOOL needToShowHelpButton;
@property (assign,nonatomic) BOOL needToShowColoreData;


- (NSArray *)getChargesArrayForDLMDetailModel:(DLMBillDetailsScreen*)billDetailsScreen;
- (NSArray *)getTakeActionDataForDLMModelBillDetail:(DLMBillDetailsScreen*)billDetailsScreen;
- (NSArray *)getBillingSummaryArrayForDLMBillDetailModel:(DLMBillDetailsScreen*)billDetailsScreen;

@end
