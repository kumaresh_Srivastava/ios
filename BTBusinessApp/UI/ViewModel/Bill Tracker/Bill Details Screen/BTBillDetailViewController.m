//
//  BTBillDetailViewController.m
//  BTBusinessApp
//
//  Created by vectoscalar on 03/11/16.
//  Copyright © 2016 Accolite. All rights reserved.


#import "BTBillDetailViewController.h"
#import "BTBillDetailHeaderView.h"
#import "BTBillDetailChargesCell.h"
#import "BTBillDetailTakeActionCell.h"
#import "BTBillDetailDurationTableViewCell.h"
#import "BTBillDetailAccountInfromationTableViewCell.h"
#import "BTBillDetailSummaryStatusTableViewCell.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "DLMBillDetailsScreen.h"
#import "BTRetryView.h"
#import "CustomSpinnerView.h"
#import "BTUICommon.h"
#import "AppConstants.h"
#import "BTInAppBrowserViewController.h"
#import "BTBillCharges.h"
#import "BTProductCharge.h"
#import "BTNeedHelpViewController.h"
#import "BTEmptyDashboardView.h"
#import "BTBillNoChargesTableViewCell.h"
#import "BTBillBrowserViewController.h"
#import "OmnitureManager.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "NLWebServiceError.h"
#import "AppManager.h"
#import "BTNavigationViewController.h"

#import "UIButton+BTButtonProperties.h"

#define kBillDetailKey @"BillDetailSummaryDataKey"
#define kBillTakeActionKey @"BillDetailChargesData"
#define kFooterHeight 90
#define kPayByCard @"Pay by card"
#define kDownloadBill @"Download full bill as PDF"

#define kEstimatedRowheight 90

#define kBTDetailHeaderViewNIB @"BTBillDetailHeaderView"
//Charges Cell
#define kBTDetailChargesCell @"BTBillDetailChargesCell"
//Take Action Cell
#define kBTDetailTakeActionCell @"BTBillDetailTakeActionCell"
//duration Cell
#define kBTBillDetailDurationCell @"BTBillDetailDurationTableViewCell"
//Account Information Cell
#define kBTBillDetailAccountInfoCell @"BTBillDetailAccountInfromationTableViewCell"
//Billing Summary/Payment Status Cell
#define kBTBillDetailSummayStatusCell @"BTBillDetailSummaryStatusTableViewCell"
//No Chages Cell
#define kBTBillNOChargesCell @"BTBillNoChargesTableViewCell"

#define kSectionHeight 40
#define kXPadding 16

#define kScreenTitle @"Bill details"


@interface BTBillDetailViewController ()<UITableViewDelegate,UITableViewDataSource,DLMBillDetailsScreenDelegate,BTBillDetailHeaderViewDelegate,BTRetryViewDelegate,BTBillBillingAccountSummaryDelegate,UIScrollViewDelegate,BTBillDetailChargesCellDelegate>
{
    BTBillDetailHeaderView *headerView;
    //UIView *footerView;
    NSArray *_awaitingPaymentBillsArray;
    NSArray *_recentlyPaidBillsArray;
    NSInteger _paymentSegmentSelectedIndex;
    
    BTRetryView *_retryView;
    BOOL _isRetryViewShown,_isViewLoaded;
    NSString *_pageNameForOmniture;
    
    
    NSMutableDictionary *billDetailDict;
    NSMutableArray *billSummaryArray, *billDetailTakeActionArray, *billChargesArray;
    
    BTEmptyDashboardView * _emptyDashboardView;
}

@property (strong,nonatomic) DLMBillDetailsScreen *billDetailScreenModel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *payByCardButton;

@end

@implementation BTBillDetailViewController

//TODO:Rahul 2 Feb Need to merge the two DLMs into one


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout  = UIRectEdgeNone;
    
    self.navigationItem.title = kScreenTitle;
    
    self.navigationController.view.backgroundColor = [UIColor whiteColor];
    

    //UINavigationItem *item = self.navigationItem;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self.payByCardButton getSuperButtonStyle];

    // Set the model
    self.billDetailScreenModel = [[DLMBillDetailsScreen alloc] init];
    self.billDetailScreenModel.billDetailScreenDelegate = self;
    
    self.tableView.hidden = YES;

    [self.payByCardButton setTitle:@"Pay by card" forState:UIControlStateNormal];
  
    [self performSelector:@selector(fetchBillDetails) withObject:nil afterDelay:0.001];
    
    _pageNameForOmniture = OMNIPAGE_BILLS_BILL_SUMMARY;
    [self trackOmniturePage:_pageNameForOmniture];
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChangeBillDetail:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    self.networkRequestInProgress = NO;
    [self hideProgressView];
    [self.billDetailScreenModel cancelBillDetailAPIRequest];
    [_emptyDashboardView removeFromSuperview];
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if(!_isViewLoaded)
    {
        _isViewLoaded = YES;
        [self configureTableView];
    }
}

#pragma mark - Private helper methods

- (void)configureTableView
{
    
    self.payByCardButton.layer.cornerRadius = 4.0f;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.tableView.estimatedRowHeight = kEstimatedRowheight;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.tableView.separatorStyle  = UITableViewCellSeparatorStyleNone;
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    headerView = [[[NSBundle mainBundle] loadNibNamed:kBTDetailHeaderViewNIB owner:self options:nil] firstObject];
    //   [headerView updateDataWithBill:self.btBill];
    headerView.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableView.tableHeaderView = headerView;
    
    [self.view addConstraints:@[
                                     [NSLayoutConstraint constraintWithItem:headerView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.tableView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0],
                                     [NSLayoutConstraint constraintWithItem:headerView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                     [NSLayoutConstraint constraintWithItem:headerView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0],
                                     [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                     [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]
                                     ]];
    //tableView.tableHeaderView = your_headerView;
   //[self performSelector:@selector(setHeaderView) withObject:nil afterDelay:0.2];
   
    headerView.delegate = self;
    //[headerView setNeedsLayout];
   // [headerView layoutIfNeeded];
    
    //footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenSize.width, 85)];
    //footerView.backgroundColor = [UIColor whiteColor];
    //self.tableView.tableFooterView  = footerView;
    
    [self registerNibs];
    
}

- (void)statusBarOrientationChangeBillDetail:(NSNotification *)notification {
//    UIInterfaceOrientation orient = [notification.userInfo[UIApplicationStatusBarOrientationUserInfoKey] integerValue];
//    self.tableView.tableHeaderView = nil;
//    self.tableView.tableFooterView = nil;
//    if(orient == UIInterfaceOrientationPortrait || orient == UIInterfaceOrientationPortraitUpsideDown){
//        [self performSelector:@selector(setHeaderView) withObject:nil afterDelay:0.2];
//    } else {
//        //((orient == UIInterfaceOrientationLandscapeLeft) || (orient == UIInterfaceOrientationLandscapeRight)
//         [self performSelector:@selector(setHeaderView) withObject:nil afterDelay:0.2];
//    }
    // handle the interface orientation as needed
    [self.tableView reloadData];
    
    [headerView setNeedsLayout];
    [self.tableView setNeedsLayout];
    [self.view layoutIfNeeded];
    
    [self createLoadingView];
}

- (void) setHeaderView{
    
     self.tableView.tableHeaderView = headerView;
     //self.tableView.tableFooterView  = footerView;
    [self.tableView setNeedsLayout];
    [self.tableView layoutIfNeeded];
}

- (void)registerNibs
{
    //Charges Cell
    UINib *nib = [UINib nibWithNibName:kBTDetailChargesCell bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kBTDetailChargesCell];
    
    //Take Action Cell
    nib = [UINib nibWithNibName:kBTDetailTakeActionCell bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kBTDetailTakeActionCell];
    
    //Bill duration cell
    nib = [UINib nibWithNibName:kBTBillDetailDurationCell bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kBTBillDetailDurationCell];
    
    //Account Info Cell
    //Bill duration cell
    nib = [UINib nibWithNibName:kBTBillDetailAccountInfoCell bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kBTBillDetailAccountInfoCell];
    
    //Payment Status/Summary Cell
    nib = [UINib nibWithNibName:kBTBillDetailSummayStatusCell bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kBTBillDetailSummayStatusCell];
    
    //No Chagres  Cell
    nib = [UINib nibWithNibName:kBTBillNOChargesCell bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:kBTBillNOChargesCell];
}


- (void)manageHeaderViewHeight
{
//    //update the header's frame and set it again
//    CGRect headerFrame = headerView.frame;
//    headerFrame.size.height = headerFrame.size.height - 20;
//    headerView.frame = headerFrame;
//    self.tableView.tableHeaderView = headerView;
//
    [headerView setNeedsLayout];
    [headerView layoutIfNeeded];
}


#pragma mark - api call methods

- (void)fetchBillDetails {
    
    if(![AppManager isInternetConnectionAvailable])
    {
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:_pageNameForOmniture];
    }
    else
    {
        self.networkRequestInProgress = YES;
        //self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [self hideRetryItems:YES];
        [self showProgressView];
        
        if(self.btAsset)//user is comming from asset scren
            [self.billDetailScreenModel fetchBillDetailsDataWithBillingAccountNumber:self.btAsset.billingAccountNumber];
        
        else
        {
           //CHeck if user is coming from dashboard or not
           if(self.isComingFromBillDashboard)
           {
               [self.billDetailScreenModel fetchBillDetailsDataWithBill:self.btBill];
           }
            else
            {
                [self.billDetailScreenModel fetchBillDetailsDataWithBillingAccountNumber:self.btBill.billingAccountNo];
            }
        }

    }
    
}


#pragma mark - Table View datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[billDetailDict allKeys] count];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    //Return zero view if section is 0
    
    if(!section)
        return [UIView new];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,[[UIScreen mainScreen] bounds].size.width, kSectionHeight)];
    bgView.backgroundColor = [UIColor colorForHexString:@"eeeeee"];//[BrandColours colourBtNeutral30];
    
    /*UIView *topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0,[[UIScreen mainScreen] bounds].size.width, 1)];
    topLine.backgroundColor = [BrandColours colourBtNeutral50];
    [bgView addSubview:topLine];
    
    UIView *bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0,kSectionHeight-1,[[UIScreen mainScreen] bounds].size.width, 1)];
    bottomLine.backgroundColor = [BrandColours colourBtNeutral50];
    [bgView addSubview:bottomLine];*/
    
    
    
    UILabel *lblTakeAction = [[UILabel alloc] initWithFrame:CGRectMake(16, 10,2*kSectionHeight, kSectionHeight/2)];
    lblTakeAction.text = @"Useful links";
    lblTakeAction.font = [UIFont fontWithName:@"BTFont-Bold" size:15.0f];
    lblTakeAction.textColor = [UIColor colorForHexString:@"333333"];//[BrandColours colourBtNeutral70];
    
    [bgView addSubview:lblTakeAction];
    
    return bgView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(!section)
        return 0;
    
    return kSectionHeight;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if(section == 0)
    {
        NSArray *array = [billDetailDict valueForKey:kBillDetailKey];
        
        return [array count];
    }
    else
    {
        NSArray *array = [billDetailDict valueForKey:kBillTakeActionKey];
        
        return [array count];
        
    }
}

/*- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat height = 230.0;
    return height;
}*/


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSArray *billDetailWrapperArray ;
    
    if(indexPath.section == 0)
        billDetailWrapperArray = [billDetailDict objectForKey:kBillDetailKey];
    else
        billDetailWrapperArray = [billDetailDict objectForKey:kBillTakeActionKey];
    
    
    BillDetailsScreenWrapper *wrapper = [billDetailWrapperArray objectAtIndex:indexPath.row];
    
    if(wrapper.cellType == DurationCell)
    {
        BTBillDetailDurationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kBTBillDetailDurationCell];
        [cell updateDataWithBillDetail:wrapper];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else  if(wrapper.cellType == BillingStatusCell)
    {
        BTBillDetailSummaryStatusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kBTBillDetailSummayStatusCell];
        [cell updateDataWithBillDetail:wrapper];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if(wrapper.cellType == BillingSummaryCell)
    {
        BTBillDetailSummaryStatusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kBTBillDetailSummayStatusCell];
        cell.delegate = self;
        [cell updateDataWithBillDetail:wrapper];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if(wrapper.cellType == AccountInfoCell)
    {
        BTBillDetailAccountInfromationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kBTBillDetailAccountInfoCell];
        [cell updateDataWithBillDetail:wrapper];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if(wrapper.cellType == TakeActionCell)
    {
        BTBillDetailTakeActionCell *cell = [tableView dequeueReusableCellWithIdentifier:kBTDetailTakeActionCell];
        [cell updateTakeActionWithTittle:wrapper.takeActionText andNeedToShowSwitch:wrapper.needToShowSwitch andIsPDFAvailable:wrapper.isUserAllowedToDownloadPDF];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if(wrapper.cellType == ChargesCell)
    {
        BTBillDetailChargesCell *cell = [tableView dequeueReusableCellWithIdentifier:kBTDetailChargesCell];
        [cell updateDataWithBillDetail:wrapper];
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if(wrapper.cellType == NoChargesCell)
    {
        BTBillNoChargesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kBTBillNOChargesCell];
        [cell updateCellHeight:self.tableView.frame.size.height/2];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }

    else
    {
        BTBillDetailTakeActionCell *cell = [tableView dequeueReusableCellWithIdentifier:kBTDetailTakeActionCell];
        //  [cell updateDataWithBillDetail:wrapper];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}



//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    if(section == 1)
//    {
//        //If bill is paid then we have to remove extra padding beacuse pay by card button is not there
//        if(!self.payByCardButton.isHidden)
//        {
//           return kFooterHeight;
//        }
//        else
//        {
//            return 0;
//        }
//    }
//    else
//        return 0;
//}
//
//
//
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    UIView *view  =  [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, kFooterHeight)];
//    view.backgroundColor = [UIColor whiteColor];
//    return view;
//}

#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // What is the use of this code here
    //RM (We have different data for Different tabs(Charges and summay when User changes tab then billDetailDict is modified. Here data is fetching based on keys.)
    NSArray *billDetailWrapperArray ;
    
    if(indexPath.section == 0)
        billDetailWrapperArray = [billDetailDict objectForKey:kBillDetailKey];
    else
        billDetailWrapperArray = [billDetailDict objectForKey:kBillTakeActionKey];
    
    
    BillDetailsScreenWrapper *wrapper = [billDetailWrapperArray objectAtIndex:indexPath.row];
    
    if(wrapper.cellType == TakeActionCell)
    {
        if([wrapper.takeActionText isEqualToString:kPayByCard])
        {
            [self trackOmnitureClick:OMNICLICK_BILLS_PAY_BY_CARD];
            [self showPayByCardScreen];
        }
        else  if([wrapper.takeActionText isEqualToString:kDownloadBill])
        {
            if(wrapper.isUserAllowedToDownloadPDF)
            {
                [self downloadBill];
                [self trackOmnitureClick:OMNICLICK_BILLS_DOWNLOAD_BILLS];
            }
        }

    }

}


#pragma mark - BTBillDetailHeaderViewDelegate Delegate
- (void)userDidChangedBillDetailTypeWithIndex:(int)newIndex
{
    if(newIndex ==0)
    {
        [billDetailDict removeObjectForKey:kBillDetailKey];
        [billDetailDict setObject:billSummaryArray forKey:kBillDetailKey];
        _pageNameForOmniture = OMNIPAGE_BILLS_BILL_SUMMARY;
    }
    else
    {
        [billDetailDict removeObjectForKey:kBillDetailKey];
        [billDetailDict setObject:billChargesArray forKey:kBillDetailKey];
        
        if([billChargesArray count]==1)
        {
            UILabel *lblNoCharges = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, 200, 30)];
            lblNoCharges.text = @"NO CHARGES FOUND";
            lblNoCharges.font = [UIFont fontWithName:@"BTFont-Bold" size:20];
            lblNoCharges.textAlignment = NSTextAlignmentCenter;
            lblNoCharges.center = CGPointMake(self.view.center.x, self.view.center.y);
          //  [self.view addSubview:lblNoCharges];
        }
        
        _pageNameForOmniture = OMNIPAGE_BILLS_BILL_CHARGES;
        
    }
    
    [self trackOmniturePage:_pageNameForOmniture];
    [self.tableView reloadData];
}


#pragma mark - DLMBillDetailScreen Delegate
- (void)successfullyFetchedBillDetailsDataOnBillDetailsScreen:(DLMBillDetailsScreen *)billDetailsScreen
{
   self.networkRequestInProgress = NO;
   [self hideProgressView];
    
    //(RM) Here we are finding from where user has came. From asset or from Bills)
    if(self.btAsset)
    {
        
        if(!billDetailsScreen.bill.billRef || billDetailsScreen.bill.billRef.length == 0)
        {
            [self showEmptyDashBoardViewWithDetailText:@""];
            return;
        }
    }
    else
    {
       if(!billDetailsScreen || !billDetailsScreen.bill || !billDetailsScreen.billCharges)
       {
           [self showEmptyDashBoardViewWithDetailText:@""];
           return;
       }
    }
    
    self.tableView.hidden = NO;
    
    if([billDetailsScreen.bill.status isEqualToString:@"NO ACTION REQUIRED"] || [billDetailsScreen.bill.billRef isEqualToString:@""] || billDetailsScreen.bill.billRef == nil)
    {
        self.payByCardButton.hidden = YES;
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height + self.payByCardButton.frame.size.height+10);
        [self resizeTableIsPaidBill:YES];
    }
    else
    {
        if(billDetailsScreen.bill.isPaid)
        {
            self.payByCardButton.hidden = YES;
            self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height + self.payByCardButton.frame.size.height+10);
            
            [self resizeTableIsPaidBill:YES];
           
        }
        else
        {
            self.payByCardButton.hidden = NO;
            [self resizeTableIsPaidBill:NO];
        }
    }
    
    //Empty all array
    [billDetailTakeActionArray removeAllObjects];
    [billChargesArray removeAllObjects];
    [billSummaryArray removeAllObjects];
    [billDetailDict removeAllObjects];
    
    if(!billDetailDict)
        billDetailDict = [[NSMutableDictionary alloc] init];
    
    if(!billSummaryArray)
        billSummaryArray = [[NSMutableArray alloc] init];
    
    if(!billDetailTakeActionArray)
        billDetailTakeActionArray = [[NSMutableArray alloc] init];
    
    if(!billChargesArray)
        billChargesArray = [[NSMutableArray alloc] init];
    
    
    
    
    
    BTBill *bill = billDetailsScreen.bill;
    // bill.billingAccountNo = self.btBill.billingAccountNo;
    
    BillDetailsScreenWrapper *billDetailWrapper = [[BillDetailsScreenWrapper alloc] init];
    
    //Get summary array
    billSummaryArray = [NSMutableArray arrayWithArray:[billDetailWrapper getBillingSummaryArrayForDLMBillDetailModel:billDetailsScreen]];
    
    //Get Charges Array
    billChargesArray = [NSMutableArray arrayWithArray:[billDetailWrapper getChargesArrayForDLMDetailModel:billDetailsScreen]];
    
    //Take action array
    billDetailTakeActionArray = [NSMutableArray arrayWithArray:[billDetailWrapper getTakeActionDataForDLMModelBillDetail:billDetailsScreen]];
    
    
    ///ADd to dictionary
    [billDetailDict setObject:billSummaryArray forKey:kBillDetailKey];
    
    
    if(self.btBill)
    {
        if((billDetailsScreen.bill.billRef.length > 0 || billDetailsScreen.bill.billRef) && billDetailTakeActionArray.count >0)
        {
            [billDetailDict setObject:billDetailTakeActionArray forKey:kBillTakeActionKey];
        }
    }
    else
    {
      [billDetailDict setObject:billDetailTakeActionArray forKey:kBillTakeActionKey];
    }
    
    [self.tableView reloadData];
    
    NSString *totalAmount;
    if(billDetailsScreen.billCharges.totalIncVat)
        //totalAmount = [NSString stringWithFormat:@"Total : £%.2f",billDetailsScreen.billCharges.totalIncVat];
        totalAmount = [NSString stringWithFormat:@"£%.2f",billDetailsScreen.billCharges.totalIncVat];
    else
        //totalAmount = [NSString stringWithFormat:@"Total : £0.00"];
        totalAmount = [NSString stringWithFormat:@"£0.00"];
    
    //Update table header view
    if(self.btAsset)//user came from view asset
        [headerView updateDataWithAsset:self.btAsset withPaymentStatus:bill.status andPaymentText:bill.statusDesc andTotalAmount:totalAmount];
    
    else
        [headerView updateDataWithBill:self.btBill withPaymentStatus:bill.status andPaymentText:bill.statusDesc andTotalAmount:totalAmount];
    
    
    //as bill status is not present we have to subract the height
    if(bill.status.length == 0)
    {
        [self manageHeaderViewHeight];
    }

}

- (void) resizeTableIsPaidBill:(BOOL)isBillPayed{
    
    if(isBillPayed == YES){
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:headerView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.tableView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:headerView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:headerView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0],
                                [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]
                                ]];
    } else{
        [self.view addConstraints:@[
                                    [NSLayoutConstraint constraintWithItem:headerView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.tableView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0],
                                    [NSLayoutConstraint constraintWithItem:headerView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                    [NSLayoutConstraint constraintWithItem:headerView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0],
                                    [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0],
                                    [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]
                                    ]];
    }
    
    [self.tableView setNeedsLayout];
}

- (void)billDetailsScreen:(DLMBillDetailsScreen *)billDetailsScreen failedToFetchBillDetailsDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
 
    DDLogError(@"View Bills: View Bill failed");
    [self setNetworkRequestInProgress:false];
    [self hideProgressView];
    
    self.networkRequestInProgress = NO;
    
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
    
    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {
        
        switch (webServiceError.error.code)
        {
                
            case BTNetworkErrorCodeAPINoDataFound:
            {
                [self showEmptyDashBoardViewWithDetailText:@""];
                errorHandled = YES;
                [AppManager trackNoDataFoundErrorOnPage:_pageNameForOmniture];
                break;
            }
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }
    
    if(errorHandled == NO)
    {
        [self showRetryViewWithInternetStrip:NO];
        [AppManager trackGenericAPIErrorOnPage:_pageNameForOmniture];
    }

}



#pragma mark - Retry View delegate

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
  [self fetchBillDetails];
}

#pragma mark - Bill summary delegate
- (void)userPressedDetailOnBillSummaryCell
{
   [self scrollTableView];
    
   [headerView.segmentControl setSelectedSegmentIndex:1];
   [self performSelector:@selector(updateTableData) withObject:nil afterDelay:0.2];
}

- (void)scrollTableView
{
    [self.tableView scrollRectToVisible:CGRectMake(0, headerView.segmentControl.frame.origin.y-15, self.tableView.frame.size.width, self.tableView.frame.size.height) animated:YES];
}

- (void)updateTableData
{
       [self userDidChangedBillDetailTypeWithIndex:1];
}


#pragma mark  - Charges Cell Delegate

- (void)userDidPressedHelpButtonOnChargesCell
{
    //Show ToolTip
    [self trackOmnitureClick:OMNICLICK_BILLS_CHARGES_DETAILS];
    BTNeedHelpViewController *helpViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AdjustmentsHelpViewController"];
    [self presentViewController:helpViewController animated:YES completion:nil];
    
}




#pragma mark - Helper Methods


- (void)showEmptyDashBoardViewWithDetailText:(NSString *)detailText {
    
    _emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTEmptyDashboardView" owner:nil options:nil] objectAtIndex:0];
    _emptyDashboardView.translatesAutoresizingMaskIntoConstraints = NO;
    NSMutableAttributedString *detailedAttributedText = [[NSMutableAttributedString alloc] initWithString:@"No bill associated for this account."];
    
    [detailedAttributedText addAttribute:NSFontAttributeName
                                   value:[UIFont fontWithName:kBtFontRegular size:16.0] range:NSMakeRange(0, detailedAttributedText.length)];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
    [detailedAttributedText addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,detailedAttributedText.length)];
    
    [detailedAttributedText addAttribute:NSForegroundColorAttributeName value:[BrandColours colourBtNeutral70] range:NSMakeRange(0, detailedAttributedText.length)];
    [_emptyDashboardView updateEmptyDashboardViewWithImageName:@"No_Bills" title:@"No bills to display" detailText:detailedAttributedText andButtonTitle:nil];
    [self.view addSubview:_emptyDashboardView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}



- (void)showRetryViewWithInternetStrip:(BOOL)needToShowInternetStrip {
    
    _isRetryViewShown = YES;
 
    if(!_retryView)
    {
        _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
        _retryView.translatesAutoresizingMaskIntoConstraints = NO;
        _retryView.retryViewDelegate = self;
        
        [self.view addSubview:_retryView];
        
        //.tableView.tableFooterView = _retryView;
        [_retryView updateRetryViewWithInternetStrip:needToShowInternetStrip];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    }
    _retryView.hidden = NO;
    _retryView.retryViewDelegate = self;
    [_retryView updateRetryViewWithInternetStrip:needToShowInternetStrip];
    
}


- (void)hideRetryItems:(BOOL)isHide {
    
    // (SD) Hide or Show UI elements related to retry.
    [_retryView setRetryViewDelegate:nil];
    [_retryView setHidden:isHide];
    _isRetryViewShown = NO;
    
}


- (void)downloadBill
{
    NSString *baseURLString = [NSString stringWithFormat:@"https://%@", [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel]];
    NSString *endPointURLString;
    
    if (_btAsset) {
        endPointURLString = [NSString stringWithFormat:@"http://drive.google.com/viewerng/viewer?embedded=true&url=%@/account/api/v2/Bill/Download/%@/%@/%@/%@",baseURLString, _btAsset.billingAccountNumber, _billDetailScreenModel.bill.billRef, _billDetailScreenModel.bill.billingAccountSystem,[NSString stringWithFormat:@"%li",(long)_billDetailScreenModel.bill.billVersionNumber]];
    }
    else
    {
        endPointURLString = [NSString stringWithFormat:@"http://drive.google.com/viewerng/viewer?embedded=true&url=%@/account/api/v2/Bill/Download/%@/%@/%@/%@",baseURLString, _btBill.billingAccountNo, _billDetailScreenModel.bill.billRef, _billDetailScreenModel.bill.billingAccountSystem,[NSString stringWithFormat:@"%li",(long)_billDetailScreenModel.bill.billVersionNumber]];
    }
    
    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
    viewController.urlToBeLoaded = endPointURLString;
    viewController.targetURLType = BTTargetURLTypeDownloadBill;
    
    viewController.isBillDownloading = YES;
    
    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:YES andIsNeedCloseButton:NO];
    
    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{
        
    }];
    
    
}


- (void)showPayByCardScreen
{
    if(![AppManager isInternetConnectionAvailable])
    {
        [AppManager trackNoInternetErrorOnPage:_pageNameForOmniture];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kNoConnectionTitle message:kNoConnectionMessage preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        }];

        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:^{}];

    }
    else
    {

       NSString *baseURLString = [NSString stringWithFormat:@"https://%@", [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel]];

        NSString *URLString = [NSString stringWithFormat:@"%@/eserve/cust/loggedout/getMakeAPaymentDataForApp.html",baseURLString];
        
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];
        BTBillBrowserViewController *viewController = (BTBillBrowserViewController *)[storyboard instantiateViewControllerWithIdentifier:@"BTBillBrowserViewController"];
        viewController.urlToBeLoaded = URLString;
       
        CATransition* transition = [CATransition animation];
        transition.duration = 0.5;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionMoveIn;
        transition.subtype = kCATransitionFromTop;
        [self.navigationController.view.layer addAnimation:transition forKey:nil];

        [self.navigationController  pushViewController:viewController animated:NO];
    }
    
}


#pragma mark - Action Methods
- (IBAction)payByCardAction:(id)sender {

    [self trackOmnitureClick:OMNICLICK_BILLS_PAY_BY_CARD_PNKBUTTON];
    [self showPayByCardScreen];
}



#pragma mark - Event tracking
- (void)trackOmniturePage:(NSString *)page
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = page;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackOmnitureClick:(NSString*)clickevent
{
    NSString *pageName = _pageNameForOmniture;
    NSString *linkTitle = clickevent;
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:data];
    
}

@end
