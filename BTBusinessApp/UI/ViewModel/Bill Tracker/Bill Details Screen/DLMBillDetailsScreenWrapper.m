//
//  DLMBillDetailsScreenWrapper.m
//  BTBusinessApp
//
//  Created by vectoscalar on 05/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBill.h"
#import "BTBillCharges.h"
#import "DLMBillDetailsScreenWrapper.h"


#define kPayByCard @"Pay by card"
#define kDownloadBill @"Download full bill as PDF"
#define kBillingNotification @"Billing notifications"
#define kBillingNotificationDiscription @"Turn these on to hear about any overdue bills or over-usage."

@implementation DLMBillDetailsScreenWrapper


- (NSArray *)getChargesArrayForDLMDetailModel:(DLMBillDetailsScreen*)billDetailsScreen
{
    //Data for Charges
    
    
    if(!billDetailsScreen.bill.billRef || billDetailsScreen.bill.billRef.length == 0)
    {
        NSMutableArray *billChargesArray = [[NSMutableArray alloc] init];
        
        DLMBillDetailsScreenWrapper *firstCell = [[DLMBillDetailsScreenWrapper alloc] init];
        
        firstCell.cellType = NoChargesCell;
        [billChargesArray addObject:firstCell];
        
        return billChargesArray;

    }

    
    NSMutableArray *billChargesArray = [[NSMutableArray alloc] init];
    
    BTBillCharges *billCharges = billDetailsScreen.billCharges;
    
    
//    for(BTProductCharge *pCharges in billDetailsScreen.arrayOfProductCharges)
//    {
//        
//        DLMBillDetailsScreenWrapper *firstCell = [[DLMBillDetailsScreenWrapper alloc] init];
//        
//        if(pCharges.numberOfInstallations >= 2)
//        {
//            firstCell.chargesTitle = [NSString stringWithFormat:@"%ld %@",(long)pCharges.numberOfInstallations , pCharges.productName];
//        }
//        else
//        {
//            firstCell.chargesTitle = pCharges.productName;
//        }
//        
//        firstCell.chargesTitleDiscription = [NSString stringWithFormat:@"£%.2f",pCharges.totalCharges];
//        firstCell.cellType = ChargesCell;
//        [billChargesArray addObject:firstCell];
//        
//    }
    
    
    //Regular Charges
    if(billCharges.regularCharges > 0)
    {
        DLMBillDetailsScreenWrapper *regularChargesData = [[DLMBillDetailsScreenWrapper alloc] init];
        regularChargesData.chargesTitle = @"Regular charges";
        regularChargesData.chargesTitleDiscription = [NSString stringWithFormat:@"£%.2f",billCharges.regularCharges];
        regularChargesData.needToShowHelpButton = NO;
        regularChargesData.chargesDetailedTitle = nil;
        regularChargesData.chargesDetailedTitleDiscription =  nil;
        regularChargesData.cellType = ChargesCell;
        [billChargesArray addObject:regularChargesData];

    }
    
    //Usage Charges
    if(billCharges.usageCharges > 0)
    {
        DLMBillDetailsScreenWrapper *usageData = [[DLMBillDetailsScreenWrapper alloc] init];
        usageData.chargesTitle = @"Usage charges";
        usageData.chargesTitleDiscription = [NSString stringWithFormat:@"£%.2f",billCharges.usageCharges];
        usageData.needToShowHelpButton = NO;
        usageData.chargesDetailedTitle = nil;
        usageData.chargesDetailedTitleDiscription =  nil;
        usageData.cellType = ChargesCell;
        [billChargesArray addObject:usageData];
        
    }
    
    //One off charges bill
    if(billCharges.oneOffCharges > 0)
    {
        DLMBillDetailsScreenWrapper *oneOffChargesData = [[DLMBillDetailsScreenWrapper alloc] init];
        oneOffChargesData.chargesTitle = @"One-off charges and credits";
        oneOffChargesData.chargesTitleDiscription = [NSString stringWithFormat:@"£%.2f",billCharges.oneOffCharges];
        oneOffChargesData.needToShowHelpButton = NO;
        oneOffChargesData.chargesDetailedTitle = nil;
        oneOffChargesData.chargesDetailedTitleDiscription =  nil;
        oneOffChargesData.cellType = ChargesCell;
        [billChargesArray addObject:oneOffChargesData];
        
    }

    
    // Discount charges
    if(billCharges.discountCharges != 0)
    {
        DLMBillDetailsScreenWrapper *discountChargesData = [[DLMBillDetailsScreenWrapper alloc] init];
        discountChargesData.chargesTitle = @"Discounts for your whole account and specific services";
        discountChargesData.chargesTitleDiscription = [NSString stringWithFormat:@"£%.2f",billCharges.discountCharges];
        discountChargesData.needToShowHelpButton = NO;
        discountChargesData.chargesDetailedTitle = nil;
        discountChargesData.chargesDetailedTitleDiscription =  nil;
        discountChargesData.cellType = ChargesCell;
        [billChargesArray addObject:discountChargesData];
        
    }
    
    //Adjsutments
   if(billCharges.adjustments>0)
   {
       DLMBillDetailsScreenWrapper *adjsutmentData = [[DLMBillDetailsScreenWrapper alloc] init];
       adjsutmentData.chargesTitle = @"Adjustments";
       adjsutmentData.chargesTitleDiscription = [NSString stringWithFormat:@"£%.2f",billCharges.adjustments];
       adjsutmentData.needToShowHelpButton = YES;
       adjsutmentData.chargesDetailedTitle = @"Other account related items";
       adjsutmentData.chargesDetailedTitleDiscription =  [NSString stringWithFormat:@"£%.2f",billCharges.adjustments];
       adjsutmentData.cellType = ChargesCell;
       [billChargesArray addObject:adjsutmentData];
   }
    
    DLMBillDetailsScreenWrapper *totalNotInc = [[DLMBillDetailsScreenWrapper alloc] init];
    totalNotInc.chargesTitle = @"Total not inc. VAT";
    totalNotInc.chargesTitleDiscription = [NSString stringWithFormat:@"£%.2f",billCharges.totalNotIncVat];;
    totalNotInc.cellType = ChargesCell;
    [billChargesArray addObject:totalNotInc];
    
    DLMBillDetailsScreenWrapper *totalVat = [[DLMBillDetailsScreenWrapper alloc] init];
    totalVat.chargesTitle = @"Total VAT";
    totalVat.chargesTitleDiscription = [NSString stringWithFormat:@"£%.2f",billCharges.totalVat];
    totalVat.chargesDetailedTitle = @"VAT at 20%";
    totalVat.chargesDetailedTitleDiscription = [NSString stringWithFormat:@"£%.2f",billCharges.totalVat];;
    totalVat.cellType = ChargesCell;
    [billChargesArray addObject:totalVat];
    
    DLMBillDetailsScreenWrapper *totalInc = [[DLMBillDetailsScreenWrapper alloc] init];
    totalInc.chargesTitle = @"Total inc. VAT";
    totalInc.chargesTitleDiscription = [NSString stringWithFormat:@"=£%.2f",billCharges.totalIncVat];;
    totalInc.cellType = ChargesCell;
    totalInc.needToShowColoreData = YES;
    [billChargesArray addObject:totalInc];
    

    return billChargesArray;
}


- (NSArray *)getBillingSummaryArrayForDLMBillDetailModel:(DLMBillDetailsScreen*)billDetailsScreen
{
    //Duration Wrapper
    
    NSMutableArray *billSummaryArray = [[NSMutableArray alloc] init];
    
    DLMBillDetailsScreenWrapper *durationData = [[DLMBillDetailsScreenWrapper alloc] init];
    
    durationData.billDate = [self getDayDateMonthFormattedStringFromDate:billDetailsScreen.bill.billDate];
    durationData.paymentDueDate = [self getDayDateMonthFormattedStringFromDate:billDetailsScreen.bill.paymentDueDate];
    durationData.receivedDate = [self getDayDateMonthFormattedStringFromDate:billDetailsScreen.bill.receivedDate];
    durationData.isPaid = billDetailsScreen.bill.isPaid;
    ;
    durationData.cellType = DurationCell;
    
    [billSummaryArray addObject:durationData];
    
    //Billing Status Wrapper
    DLMBillDetailsScreenWrapper *paymentStatusData = [[DLMBillDetailsScreenWrapper alloc] init];
    
    paymentStatusData.billStatus  = billDetailsScreen.bill.billStatus;
    paymentStatusData.paymentDueDate = [self getDayDateMonthFormattedStringFromDate:billDetailsScreen.bill.paymentDueDate];
    paymentStatusData.isPaid = billDetailsScreen.bill.isPaid;
    paymentStatusData.receivedDate = @"Payment has been received";
    paymentStatusData.status = billDetailsScreen.bill.status;
    paymentStatusData.statusDesc = billDetailsScreen.bill.statusDesc;
    [billSummaryArray addObject:paymentStatusData];
    paymentStatusData.cellType = BillingStatusCell;
    
    //Billing Summary Wrapper
    DLMBillDetailsScreenWrapper *sumaryOfChargesData = [[DLMBillDetailsScreenWrapper alloc] init];
    
    
    sumaryOfChargesData.arrayOfProductCharges = billDetailsScreen.arrayOfProductCharges;
    sumaryOfChargesData.summaryOfChargesDiscription = @"Plus Extras and Vat";
    sumaryOfChargesData.cellType = BillingSummaryCell;
    [billSummaryArray addObject:sumaryOfChargesData];
    
    DLMBillDetailsScreenWrapper *accountInfoData = [[DLMBillDetailsScreenWrapper alloc] init];
    accountInfoData.billRef = billDetailsScreen.bill.billRef;
    accountInfoData.billType = billDetailsScreen.bill.billType;
    accountInfoData.accountName = [self trimWhiteSpacesAndNewLinesFromString:billDetailsScreen.bill.accountName];
    NSString *address = [self trimWhiteSpacesAndNewLinesFromString:billDetailsScreen.bill.billingNameAndAddress];
    accountInfoData.billingNameAndAddress = address;
    accountInfoData.isPaid = billDetailsScreen.bill.isPaid;
    accountInfoData.cellType = AccountInfoCell;
    [billSummaryArray addObject:accountInfoData];
    
    
    return billSummaryArray;
}


- (NSArray *)getTakeActionDataForDLMModelBillDetail:(DLMBillDetailsScreen*)billDetailsScreen
{
    
    NSMutableArray *billDetailTakeActionArray = [[NSMutableArray alloc] init];

    DLMBillDetailsScreenWrapper *payByCard = [[DLMBillDetailsScreenWrapper alloc] init];
    payByCard.takeActionText = kPayByCard;
    payByCard.cellType = TakeActionCell;
    
    DLMBillDetailsScreenWrapper *downloadBill = [[DLMBillDetailsScreenWrapper alloc] init];
    downloadBill.takeActionText = kDownloadBill;
    downloadBill.cellType = TakeActionCell;
    
    if(billDetailsScreen.bill.billRef)
    {
        downloadBill.isUserAllowedToDownloadPDF = YES;
    }
    else
    {
        downloadBill.isUserAllowedToDownloadPDF = NO;
    }
    
    DLMBillDetailsScreenWrapper *notification = [[DLMBillDetailsScreenWrapper alloc] init];
    notification.takeActionText = kBillingNotification;
    notification.takeActionDetailedText = kBillingNotificationDiscription;
    notification.cellType = TakeActionCell;
    notification.needToShowSwitch = YES;

    if(!billDetailsScreen.bill.isPaid)
    {
    [billDetailTakeActionArray addObject:payByCard];
    }
    [billDetailTakeActionArray addObject:downloadBill];
    
    //Uncomment this to add billing Notification option (RM)
   // [billDetailTakeActionArray addObject:notification];
    
    return billDetailTakeActionArray;

}


- (NSString *)getDayDateMonthFormattedStringFromDate:(NSDate *)date {
    NSString *dateFormat = @"dd MMM YYYY";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSString *dateString = nil;
    
    if ([dateFormatter stringFromDate:date] != nil) {
        
        dateString = [dateFormatter stringFromDate:date];
    }
    
    return dateString;
}


- (NSString*)trimWhiteSpacesAndNewLinesFromString:(NSString*)string
{
    NSString* newString =  [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return newString;
}

@end
