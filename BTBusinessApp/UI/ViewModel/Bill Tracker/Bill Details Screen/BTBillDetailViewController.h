//
//  BTBillDetailViewController.h
//  BTBusinessApp
//
//  Created by vectoscalar on 03/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBill.h"
#import "BTAsset.h"
#import "BTBaseViewController.h"
@interface BTBillDetailViewController : BTBaseViewController

@property (nonatomic, copy) NSString *groupName;
@property (nonatomic) NSArray *cugs;

@property (strong,nonatomic) NSString *bilAmount;
@property (strong,nonatomic) BTBill *btBill;
@property (strong,nonatomic) BTAsset *btAsset;
@property (assign,nonatomic) BOOL isComingFromBillDashboard;

@end
