//
//  DLMBillDetailsScreen.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 05/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMBillDetailsScreen.h"
#import "NLGetBillSummaryAndChargesWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "NLWebServiceError.h"
#import "BTBill.h"
#import "BTBillCharges.h"

@interface DLMBillDetailsScreen () <NLGetBillSummaryAndChargesWebServiceDelegate> {
    
}

@property (nonatomic) NLGetBillSummaryAndChargesWebService *getBillSummaryAndChargesWebService;

@end

@implementation DLMBillDetailsScreen

- (void)fetchBillDetailsDataWithBillingAccountNumber:(NSString *)BAC
{
    NSString *endPointURLParams = [NSString stringWithFormat:@"/bt-business-auth/v1/bills/%@",BAC];
    self.getBillSummaryAndChargesWebService = self.getBillSummaryAndChargesWebService = [[NLGetBillSummaryAndChargesWebService alloc] initWithEndPointURL:endPointURLParams andBAC:BAC];
    self.getBillSummaryAndChargesWebService.getBillSummaryAndChargesWebServiceDelegate = self;
    [self.getBillSummaryAndChargesWebService resume];


    //Uncomment this to fetch data from local json
   //[self.getBillSummaryAndChargesWebService fetchOfflineData];
}


- (void)fetchBillDetailsDataWithBill:(BTBill *)bill
{
   //Only called from Bill Dashboard screen
    NSString *endPointURLParams = [NSString stringWithFormat:@"/bt-business-auth/v1/bills/%@/%@/%@/%@",bill.billingAccountNo,bill.billRef,bill.billVersion,bill.billingAccountSystem];
    
    self.getBillSummaryAndChargesWebService = [[NLGetBillSummaryAndChargesWebService alloc] initWithEndPointURL:endPointURLParams andBAC:nil];
    self.getBillSummaryAndChargesWebService.getBillSummaryAndChargesWebServiceDelegate = self;
    [self.getBillSummaryAndChargesWebService resume];

}



- (void)cancelBillDetailAPIRequest
{
    self.getBillSummaryAndChargesWebService.getBillSummaryAndChargesWebServiceDelegate = nil;
    [self.getBillSummaryAndChargesWebService cancel];
    self.getBillSummaryAndChargesWebService  = nil;
}


#pragma mark - NLGetBillPaymentHistoryWebServiceDelegate Methods

- (void)getBillSummaryAndChargesWebService:(NLGetBillSummaryAndChargesWebService *)webService successfullyFetchedBillSummaryAndChargesDataWithBillSummary:(BTBill *)billSummary billCharges:(BTBillCharges *)billCharges andProductCharges:(NSArray *)arrayOfProductCharges
{
   if(webService == self.getBillSummaryAndChargesWebService)
   {
       _bill = billSummary;
       _billCharges = billCharges;
       _arrayOfProductCharges = arrayOfProductCharges;
       
       [self.billDetailScreenDelegate successfullyFetchedBillDetailsDataOnBillDetailsScreen:self];
       
       self.getBillSummaryAndChargesWebService.getBillSummaryAndChargesWebServiceDelegate = nil;
       self.getBillSummaryAndChargesWebService = nil;
   }
   else
   {
       DDLogError(@"This delegate method gets called because of success of an object of NLGetBillSummaryAndChargesWebService but this object is not the one stored in member variable of this class %@.", [self class]);
       NSAssert(NO, @"This delegate method gets called because of success of an object of NLGetBillSummaryAndChargesWebService but this object is not the one stored in member variable of this class %@.", [self class]);

   }
}


- (void)getBillSummaryAndChargesWebService:(NLGetBillSummaryAndChargesWebService *)webService failedToFetchPaymentHistoryDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    if(webService == self.getBillSummaryAndChargesWebService)
    {
       [self.billDetailScreenDelegate billDetailsScreen:self failedToFetchBillDetailsDataWithWebServiceError:webServiceError];
        
        self.getBillSummaryAndChargesWebService.getBillSummaryAndChargesWebServiceDelegate = nil;
        self.getBillSummaryAndChargesWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLGetBillSummaryAndChargesWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLGetBillSummaryAndChargesWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        
    }
    
}

@end



#define kPayByCard @"Pay by card"
#define kDownloadBill @"Download full bill as PDF"
#define kBillingNotification @"Billing notifications"
#define kBillingNotificationDiscription @"Turn these on to hear about any overdue bills or over-usage."

@implementation BillDetailsScreenWrapper


- (NSArray *)getChargesArrayForDLMDetailModel:(DLMBillDetailsScreen*)billDetailsScreen
{
    //Data for Charges
    
    
    if(!billDetailsScreen.bill.billRef || billDetailsScreen.bill.billRef.length == 0)
    {
        NSMutableArray *billChargesArray = [[NSMutableArray alloc] init];
        
        BillDetailsScreenWrapper *firstCell = [[BillDetailsScreenWrapper alloc] init];
        
        firstCell.cellType = NoChargesCell;
        [billChargesArray addObject:firstCell];
        
        return billChargesArray;
        
    }
    
    
    NSMutableArray *billChargesArray = [[NSMutableArray alloc] init];
    
    BTBillCharges *billCharges = billDetailsScreen.billCharges;
    
    
    //    for(BTProductCharge *pCharges in billDetailsScreen.arrayOfProductCharges)
    //    {
    //
    //        DLMBillDetailsScreenWrapper *firstCell = [[DLMBillDetailsScreenWrapper alloc] init];
    //
    //        if(pCharges.numberOfInstallations >= 2)
    //        {
    //            firstCell.chargesTitle = [NSString stringWithFormat:@"%ld %@",(long)pCharges.numberOfInstallations , pCharges.productName];
    //        }
    //        else
    //        {
    //            firstCell.chargesTitle = pCharges.productName;
    //        }
    //
    //        firstCell.chargesTitleDiscription = [NSString stringWithFormat:@"£%.2f",pCharges.totalCharges];
    //        firstCell.cellType = ChargesCell;
    //        [billChargesArray addObject:firstCell];
    //
    //    }
    
    
    //Regular Charges
    if(billCharges.regularCharges > 0)
    {
        BillDetailsScreenWrapper *regularChargesData = [[BillDetailsScreenWrapper alloc] init];
        regularChargesData.chargesTitle = @"Regular charges";
        regularChargesData.chargesTitleDiscription = [NSString stringWithFormat:@"£%.2f",billCharges.regularCharges];
        regularChargesData.needToShowHelpButton = NO;
        regularChargesData.chargesDetailedTitle = nil;
        regularChargesData.chargesDetailedTitleDiscription =  nil;
        regularChargesData.cellType = ChargesCell;
        [billChargesArray addObject:regularChargesData];
        
    }
    
    //Usage Charges
    if(billCharges.usageCharges > 0)
    {
        BillDetailsScreenWrapper *usageData = [[BillDetailsScreenWrapper alloc] init];
        usageData.chargesTitle = @"Usage charges";
        usageData.chargesTitleDiscription = [NSString stringWithFormat:@"£%.2f",billCharges.usageCharges];
        usageData.needToShowHelpButton = NO;
        usageData.chargesDetailedTitle = nil;
        usageData.chargesDetailedTitleDiscription =  nil;
        usageData.cellType = ChargesCell;
        [billChargesArray addObject:usageData];
        
    }
    
    //One off charges bill
    if(billCharges.oneOffCharges > 0)
    {
        BillDetailsScreenWrapper *oneOffChargesData = [[BillDetailsScreenWrapper alloc] init];
        oneOffChargesData.chargesTitle = @"One-off charges and credits";
        oneOffChargesData.chargesTitleDiscription = [NSString stringWithFormat:@"£%.2f",billCharges.oneOffCharges];
        oneOffChargesData.needToShowHelpButton = NO;
        oneOffChargesData.chargesDetailedTitle = nil;
        oneOffChargesData.chargesDetailedTitleDiscription =  nil;
        oneOffChargesData.cellType = ChargesCell;
        [billChargesArray addObject:oneOffChargesData];
        
    }
    
    
    // Discount charges
    if(billCharges.discountCharges != 0)
    {
        BillDetailsScreenWrapper *discountChargesData = [[BillDetailsScreenWrapper alloc] init];
        discountChargesData.chargesTitle = @"Discounts for your whole account and specific services";
        discountChargesData.chargesTitleDiscription = [NSString stringWithFormat:@"£%.2f",billCharges.discountCharges];
        discountChargesData.needToShowHelpButton = NO;
        discountChargesData.chargesDetailedTitle = nil;
        discountChargesData.chargesDetailedTitleDiscription =  nil;
        discountChargesData.cellType = ChargesCell;
        [billChargesArray addObject:discountChargesData];
        
    }
    
    //Adjsutments
    if(billCharges.adjustments>0)
    {
        BillDetailsScreenWrapper *adjsutmentData = [[BillDetailsScreenWrapper alloc] init];
        adjsutmentData.chargesTitle = @"Adjustments";
        adjsutmentData.chargesTitleDiscription = [NSString stringWithFormat:@"£%.2f",billCharges.adjustments];
        adjsutmentData.needToShowHelpButton = YES;
        adjsutmentData.chargesDetailedTitle = @"Other account related items";
        adjsutmentData.chargesDetailedTitleDiscription =  [NSString stringWithFormat:@"£%.2f",billCharges.adjustments];
        adjsutmentData.cellType = ChargesCell;
        [billChargesArray addObject:adjsutmentData];
    }
    
    BillDetailsScreenWrapper *totalNotInc = [[BillDetailsScreenWrapper alloc] init];
    totalNotInc.chargesTitle = @"Total not inc. VAT";
    totalNotInc.chargesTitleDiscription = [NSString stringWithFormat:@"£%.2f",billCharges.totalNotIncVat];;
    totalNotInc.cellType = ChargesCell;
    [billChargesArray addObject:totalNotInc];
    
    BillDetailsScreenWrapper *totalVat = [[BillDetailsScreenWrapper alloc] init];
    totalVat.chargesTitle = @"Total VAT";
    totalVat.chargesTitleDiscription = [NSString stringWithFormat:@"£%.2f",billCharges.totalVat];
    totalVat.chargesDetailedTitle = @"VAT at 20%";
    totalVat.chargesDetailedTitleDiscription = [NSString stringWithFormat:@"£%.2f",billCharges.totalVat];;
    totalVat.cellType = ChargesCell;
    [billChargesArray addObject:totalVat];
    
    BillDetailsScreenWrapper *totalInc = [[BillDetailsScreenWrapper alloc] init];
    totalInc.chargesTitle = @"Total inc. VAT";
    totalInc.chargesTitleDiscription = [NSString stringWithFormat:@"£%.2f",billCharges.totalIncVat];;
    totalInc.cellType = ChargesCell;
    totalInc.needToShowColoreData = YES;
    [billChargesArray addObject:totalInc];
    
    
    return billChargesArray;
}


- (NSArray *)getBillingSummaryArrayForDLMBillDetailModel:(DLMBillDetailsScreen*)billDetailsScreen
{
    //Duration Wrapper
    
    NSMutableArray *billSummaryArray = [[NSMutableArray alloc] init];
    
    BillDetailsScreenWrapper *durationData = [[BillDetailsScreenWrapper alloc] init];
    
    durationData.billDate = [self getDayDateMonthFormattedStringFromDate:billDetailsScreen.bill.billDate];
    durationData.paymentDueDate = [self getDayDateMonthFormattedStringFromDate:billDetailsScreen.bill.paymentDueDate];
    durationData.nextBillDate = [self getDayDateMonthFormattedStringFromDate:billDetailsScreen.bill.nextBillDate];
    durationData.isPaid = billDetailsScreen.bill.isPaid;
    ;
    durationData.cellType = DurationCell;
    
    [billSummaryArray addObject:durationData];
    
    //Billing Status Wrapper
    BillDetailsScreenWrapper *paymentStatusData = [[BillDetailsScreenWrapper alloc] init];
    
    paymentStatusData.billStatus  = billDetailsScreen.bill.billStatus;
    paymentStatusData.paymentDueDate = [self getDayDateMonthFormattedStringFromDate:billDetailsScreen.bill.paymentDueDate];
    paymentStatusData.isPaid = billDetailsScreen.bill.isPaid;
    paymentStatusData.status = billDetailsScreen.bill.status;
    paymentStatusData.statusDesc = billDetailsScreen.bill.statusDesc;
    [billSummaryArray addObject:paymentStatusData];
    paymentStatusData.cellType = BillingStatusCell;
    
    //Billing Summary Wrapper
    BillDetailsScreenWrapper *sumaryOfChargesData = [[BillDetailsScreenWrapper alloc] init];
    
    
    sumaryOfChargesData.arrayOfProductCharges = billDetailsScreen.arrayOfProductCharges;
    sumaryOfChargesData.summaryOfChargesDiscription = @"Plus Extras and Vat";
    sumaryOfChargesData.cellType = BillingSummaryCell;
    [billSummaryArray addObject:sumaryOfChargesData];
    
    BillDetailsScreenWrapper *accountInfoData = [[BillDetailsScreenWrapper alloc] init];
    accountInfoData.billRef = billDetailsScreen.bill.billRef;
    accountInfoData.billType = billDetailsScreen.bill.billType;
    accountInfoData.accountName = [self trimWhiteSpacesAndNewLinesFromString:billDetailsScreen.bill.accountName];
    NSString *address = [self trimWhiteSpacesAndNewLinesFromString:billDetailsScreen.bill.billingNameAndAddress];
    accountInfoData.billingNameAndAddress = address;
    accountInfoData.isPaid = billDetailsScreen.bill.isPaid;
    accountInfoData.cellType = AccountInfoCell;
    [billSummaryArray addObject:accountInfoData];
    
    
    return billSummaryArray;
}


- (NSArray *)getTakeActionDataForDLMModelBillDetail:(DLMBillDetailsScreen*)billDetailsScreen
{
    
    NSMutableArray *billDetailTakeActionArray = [[NSMutableArray alloc] init];
    
    BillDetailsScreenWrapper *payByCard = [[BillDetailsScreenWrapper alloc] init];
    payByCard.takeActionText = kPayByCard;
    payByCard.cellType = TakeActionCell;
    
    BillDetailsScreenWrapper *downloadBill = [[BillDetailsScreenWrapper alloc] init];
    downloadBill.takeActionText = kDownloadBill;
    downloadBill.cellType = TakeActionCell;
    
    if(billDetailsScreen.bill.billRef && ![billDetailsScreen.bill.billingAccountSystem isEqualToString:@"CSS"])
    {
        downloadBill.isUserAllowedToDownloadPDF = YES;
    }
    else
    {
        downloadBill.isUserAllowedToDownloadPDF = NO;
    }
    
    BillDetailsScreenWrapper *notification = [[BillDetailsScreenWrapper alloc] init];
    notification.takeActionText = kBillingNotification;
    notification.takeActionDetailedText = kBillingNotificationDiscription;
    notification.cellType = TakeActionCell;
    notification.needToShowSwitch = YES;
    if(![billDetailsScreen.bill.status isEqualToString:@"NO ACTION REQUIRED"])
    {
        if(!billDetailsScreen.bill.isPaid)
        {
            [billDetailTakeActionArray addObject:payByCard];
        }
    }
    
    if (![billDetailsScreen.bill.billingAccountSystem isEqualToString:@"CSS"])
    {
        [billDetailTakeActionArray addObject:downloadBill];
    }
    
    //Uncomment this to add billing Notification option (RM)
    // [billDetailTakeActionArray addObject:notification];
    
    return billDetailTakeActionArray;
    
}


- (NSString *)getDayDateMonthFormattedStringFromDate:(NSDate *)date {
    NSString *dateFormat = @"dd MMM yyyy";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSString *dateString = nil;
    
    if ([dateFormatter stringFromDate:date] != nil) {
        
        dateString = [dateFormatter stringFromDate:date];
    }
    
    return dateString;
}


- (NSString*)trimWhiteSpacesAndNewLinesFromString:(NSString*)string
{
    NSString* newString =  [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return newString;
}

@end
