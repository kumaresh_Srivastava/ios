//
//  BTBillSummaryViewController.h
//  BTBusinessApp
//
//  Created by Saddam Husain on 03/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTBillsDashBoardViewController : UIViewController

@property (nonatomic) BOOL networkRequestInProgress;
@property (nonatomic, copy) NSString *groupKey;
//@property (nonatomic) NSArray *cugs;
@property (nonatomic) NSDictionary *billsDataDictionary;

@end
