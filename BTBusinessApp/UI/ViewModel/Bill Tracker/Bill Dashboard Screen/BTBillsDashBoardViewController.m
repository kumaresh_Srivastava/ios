//
//  BTBillSummaryViewController.m
//  BTBusinessApp
//
//  Created by Saddam Husain on 01/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//


#import "BTBillsDashBoardViewController.h"
#import "BTBillSummaryTableViewCell.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "BTCug.h"
#import "BTBillsSummaryTableFooterView.h"
#import "BTEmptyDashboardView.h"
#import "BTRetryView.h"
#import "CustomSpinnerView.h"
//#import "CustomSpinnerAnimationView.h"
#import "DLMBillHistoryDashboardScreen.h"
#import "AppConstants.h"
#import "BTBillDetailViewController.h"
#import "BTBill.h"
#import "BTBillBrowserViewController.h"
#import "OmnitureManager.h"
#import "NLWebServiceError.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "AppManager.h"

#define kBTBillDetailViewControlelr @"BTBillDetailViewController"


@interface BTBillsDashBoardViewController ()<BTBillSummaryTableFooterDelegate, BTRetryViewDelegate,DLMBillHistoryDashboardScreenDelegate,UITableViewDelegate>
{
    NSArray *_awaitingPaymentBillsArray;
    NSArray *_recentlyPaidBillsArray;
    NSInteger _paymentSegmentSelectedIndex;
    BTBillsSummaryTableFooterView *billsSummaryTableFooterView;
    BTEmptyDashboardView *_emptyDashboardView;
    BTRetryView *_retryView;
    BOOL _isRetryViewShown;
    NSString *_pageNameForOmniture;
    //CustomSpinnerView *loadingView;
}

@property (nonatomic, readwrite) DLMBillHistoryDashboardScreen *billsHistoryScreenModel;

//@property (weak, nonatomic) IBOutlet UILabel *groupNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *spinnerBackgroundView;

@property (weak, nonatomic) IBOutlet UITableView *billDetailsTableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *billSegmentedControl;
//@property (weak, nonatomic) IBOutlet UIBarButtonItem *groupBarButtonItem;
@property(strong, nonatomic) CustomSpinnerView *loadingView;

@end

@implementation BTBillsDashBoardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self updateOmniturePageIndex:0];//set page name for omniture and track page
    self.edgesForExtendedLayout = UIRectEdgeNone;

    //[SD] View Model Initialization
    self.billsHistoryScreenModel = [[DLMBillHistoryDashboardScreen alloc] initWithBillDataDict:self.billsDataDictionary];
    self.billsHistoryScreenModel.billHistoryDashboardDelegate = self;
    //navigation bar appearence

    self.titleLabel.text = @"Bills";

    self.billDetailsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.billDetailsTableView.backgroundColor = [BrandColours colourBtNeutral30];

    self.billDetailsTableView.estimatedRowHeight = 118;
    self.billDetailsTableView.rowHeight = UITableViewAutomaticDimension;

    // [self updateGroupSelection];

    //[self createLoadingView];
    [self performSelector:@selector(createLoadingView) withObject:nil afterDelay:0.1];
    

    __weak typeof(self) weakSelf = self;

    [RACObserve(weakSelf, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([weakSelf networkRequestInProgress]) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:true];
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.billDetailsTableView.hidden = YES;
                [weakSelf hideLoadingItems:NO];
                //[weakSelf.loadingView startAnimation];
                [weakSelf.loadingView startAnimatingLoadingIndicatorView];
                //[loadingView startAnimation];

                //weakSelf.groupBarButtonItem.enabled = NO;

            });
        }

        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
            dispatch_async(dispatch_get_main_queue(), ^{

                //weakSelf.groupBarButtonItem.enabled = YES;
                weakSelf.billDetailsTableView.hidden = NO;
                [weakSelf hideLoadingItems:YES];

            });
        }
    }];


    UINib *dataCell = [UINib nibWithNibName:@"BTBillSummaryTableViewCell" bundle:nil];
    [self.billDetailsTableView registerNib:dataCell forCellReuseIdentifier:@"BTBillSummaryTableViewCell"];


    _awaitingPaymentBillsArray = [NSArray array];
    _recentlyPaidBillsArray = [NSArray array];

    //[_billSegmentedControl setTintColor:[BrandColours colourBackgroundBTPurplePrimaryColor]];
    //[_billSegmentedControl setTitleTextAttributes:[[AppManager sharedAppManager] fetchSegmentedControlFontDict] forState:UIControlStateNormal];
    
    // (SD) Call get bill summary API call
    [self fetchViewBillsAPI];

    //self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

}

- (void) viewWillAppear:(BOOL)animated {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChangeBillScreen:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
}

- (void)statusBarOrientationChangeBillScreen:(NSNotification *)notification {
    // handle the interface orientation as needed
    [self performSelector:@selector(createLoadingView) withObject:nil afterDelay:0.1];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self.billsHistoryScreenModel cancelBillHistoryAPIRequest];
    self.networkRequestInProgress = NO;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
}

#pragma mark - api call methods

- (void)fetchViewBillsAPI {

    if(![AppManager isInternetConnectionAvailable] && !self.billsDataDictionary)
    {
        self.networkRequestInProgress = NO;
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:_pageNameForOmniture];
    }
    else
    {
        [self hideRetryItems:YES];
        self.networkRequestInProgress = YES;
        [self hideEmmptyDashBoardItems:YES];
        self.billDetailsTableView.hidden = YES;
        [self.billsHistoryScreenModel fetchBillPaymentHistoryData];
    }
}




#pragma mark - table view action methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(_paymentSegmentSelectedIndex == 0)
        return [_awaitingPaymentBillsArray count];
    else
        return  [_recentlyPaidBillsArray count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BTBillSummaryTableViewCell *billDataCell = [tableView dequeueReusableCellWithIdentifier:@"BTBillSummaryTableViewCell" forIndexPath:indexPath];
    if(_paymentSegmentSelectedIndex == 0)
        [billDataCell updateCellWithBillSummaryData:[_awaitingPaymentBillsArray objectAtIndex:indexPath.row]];
    else
        [billDataCell updateCellWithBillSummaryData:[_recentlyPaidBillsArray objectAtIndex:indexPath.row]];

    return billDataCell;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    // (lpm) Need to show only in awaiting payment section
    if (_billSegmentedControl.selectedSegmentIndex == 0)
    {
        billsSummaryTableFooterView = [[[NSBundle mainBundle] loadNibNamed:@"BTBillsSummaryTableFooterView" owner:nil options:nil] firstObject];
        billsSummaryTableFooterView.billSummaryTableFooterDelegate = self;
        return billsSummaryTableFooterView;
    }
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (_billSegmentedControl.selectedSegmentIndex == 0)
    {
        return 100;
    }
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    BTBill *selectedBill ;

    if(_paymentSegmentSelectedIndex == 0)
        selectedBill = [_awaitingPaymentBillsArray objectAtIndex:indexPath.row];
    else
        selectedBill = [_recentlyPaidBillsArray objectAtIndex:indexPath.row];

    NSString *omnitureEvent = [NSString stringWithFormat:@"%@ %@",OMNICLICK_BILLS_BILL_DETAILS,[selectedBill.billStatus lowercaseString]];
    [self trackOmnitureClick:omnitureEvent];

    BTBillDetailViewController *billDetailViewController = (BTBillDetailViewController*)[self.storyboard instantiateViewControllerWithIdentifier:kBTBillDetailViewControlelr];
    billDetailViewController.btBill = selectedBill;
    billDetailViewController.isComingFromBillDashboard = YES;
    [self.navigationController pushViewController:billDetailViewController animated:YES];
}



#pragma mark - Helper Methods


- (void)hideLoadingItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to loading.
    [_loadingView setHidden:isHide];
   // [self.spinnerBackgroundView setHidden:isHide];

}

- (void)createLoadingView {

    if(_loadingView.isHidden == YES){
        return;
    }
    
    if(_loadingView){
        [_loadingView removeFromSuperview];
        _loadingView = nil;
    }
   /* loadingView = [[CustomSpinnerAnimationView alloc] initWithText:@"" loadingImage:[UIImage imageNamed:@"Spinner"] andLoaderImageSize:40.0f];
    
    loadingView.frame = CGRectMake(0, 0, self.spinnerBackgroundView.frame.size.width, self.spinnerBackgroundView.frame.size.height);
    loadingView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.spinnerBackgroundView addSubview:loadingView];*/
    
    
    _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
    _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    _loadingView.translatesAutoresizingMaskIntoConstraints = NO;

    [self.view addSubview:_loadingView];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
}


- (void)showRetryViewWithInternetStrip:(BOOL)needToShowInternetStrip {

    [self hideLoadingItems:YES];
    _isRetryViewShown = YES;

    if(!_retryView)
    {
        _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
        _retryView.translatesAutoresizingMaskIntoConstraints = NO;
        _retryView.retryViewDelegate = self;

        [_retryView updateRetryViewWithInternetStrip:needToShowInternetStrip];

        [self.view addSubview:_retryView];

        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        if(needToShowInternetStrip == YES){
            [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
        }else{
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.billSegmentedControl  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:self.billSegmentedControl.frame.size.height]];
        }
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    }
    _retryView.hidden = NO;
    _retryView.retryViewDelegate = self;
    [_retryView updateRetryViewWithInternetStrip:needToShowInternetStrip];

}


- (void)hideRetryItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to retry.
    [_retryView setRetryViewDelegate:nil];
    [_retryView setHidden:isHide];
    _isRetryViewShown = NO;

}


- (void)showEmptyDashBoardViewWithDetailText:(NSString *)detailText {

    _emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTEmptyDashboardView" owner:nil options:nil] objectAtIndex:0];
    _emptyDashboardView.translatesAutoresizingMaskIntoConstraints = NO;
    NSMutableAttributedString *detailedAttributedText = [[NSMutableAttributedString alloc] initWithString:detailText];
    
    [detailedAttributedText addAttribute:NSFontAttributeName
                    value:[UIFont fontWithName:kBtFontRegular size:16.0] range:NSMakeRange(0, detailedAttributedText.length)];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
    [detailedAttributedText addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,detailedAttributedText.length)];
    
    [detailedAttributedText addAttribute:NSForegroundColorAttributeName value:[BrandColours colourBtNeutral70] range:NSMakeRange(0, detailedAttributedText.length)];
    
    [_emptyDashboardView updateEmptyDashboardViewWithImageName:@"No_bills_dashboard" title:@"No bills to display" detailText:detailedAttributedText andButtonTitle:nil];
    self.billDetailsTableView.hidden = YES;
    [self.view addSubview:_emptyDashboardView];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.billSegmentedControl  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:10.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];

}

- (void)hideEmmptyDashBoardItems:(BOOL)isHide {

    // (SD) Hide or Show UI elements related to retry.
    [_emptyDashboardView setHidden:isHide];
    [_emptyDashboardView removeFromSuperview];
    _emptyDashboardView = nil;

}


- (void)showEmptyViewForAwaitingPayment
{
    self.billDetailsTableView.hidden = YES;
    [self showEmptyDashBoardViewWithDetailText:@"You haven't got any bills to pay."];
    [self trackOmniturePage:OMNIPAGE_BILLS_NO_BILLS];

}

- (void)showEmptyViewForPaidPayment
{
    self.billDetailsTableView.hidden = YES;
    [self showEmptyDashBoardViewWithDetailText:@"You currently have no bills recently paid."];
}

- (void)showEmptyViewForSelectedTabWithIndex:(int)index
{
    if(index == 0)
    {
        [self showEmptyViewForAwaitingPayment];
    }
    else
    {
        [self showEmptyViewForPaidPayment];
    }
}



- (void)showPayByCardScreen
{
    if(![AppManager isInternetConnectionAvailable])
    {
        [AppManager trackNoInternetErrorOnPage:_pageNameForOmniture];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kNoConnectionTitle message:kNoConnectionMessage preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        }];

        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:^{}];

    }
    else
    {

        NSString *baseURLString = [NSString stringWithFormat:@"https://%@", [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel]];

        NSString *URLString = [NSString stringWithFormat:@"%@/eserve/cust/loggedout/getMakeAPaymentDataForApp.html",baseURLString];
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];
        BTBillBrowserViewController *viewController = (BTBillBrowserViewController *)[storyboard instantiateViewControllerWithIdentifier:@"BTBillBrowserViewController"];
        viewController.urlToBeLoaded = URLString;

        CATransition* transition = [CATransition animation];
        transition.duration = 0.5;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionMoveIn;
        transition.subtype = kCATransitionFromTop;
        [self.navigationController.view.layer addAnimation:transition forKey:nil];

        [self.navigationController  pushViewController:viewController animated:NO];
    }

}



#pragma mark - Cug management methods
/*
 Updates the currently selected group and UI on the basis of group selection
 */
//- (void) checkForSuperUserAndUpdateUIWithIndex:(NSUInteger)index {
//
//    BTCug *cugData = [self.cugs objectAtIndex:index];
//    _pageNameForOmniture = cugData.cugName;
//
//    if (![self.groupKey isEqualToString:cugData.groupKey]) {
//
//        // (SD) Change current selected cug locally and in persistence.
//        [self.billsHistoryScreenModel changeSelectedCUGTo:cugData];
//
//        [self setGroupKey:cugData.groupKey];
//        [self.groupNameLabel setText:cugData.cugName];
//        [self resetDataAndUIAfterGroupChange];
//
//        self.billsDataDictionary = nil;
//    }
//}
//

//Checking for the number of groups, if 1 group is available hide topview and groupselection button
//- (void)hideGroupSelectionForOneAvailableGroup {
//    [self.groupBarButtonItem setEnabled:false];
//    [self.groupBarButtonItem setImage:nil];
//    self.billSegmentedControl.translatesAutoresizingMaskIntoConstraints = NO;
//    NSLayoutConstraint *segmentConstraint = [NSLayoutConstraint constraintWithItem:self.billSegmentedControl
//                                                                         attribute:NSLayoutAttributeTop
//                                                                         relatedBy:NSLayoutRelationEqual
//                                                                            toItem:self.view
//                                                                         attribute:NSLayoutAttributeTop
//                                                                        multiplier:1.0
//                                                                          constant:10.0];
//    [self.view addConstraint:segmentConstraint];
//
//    [self.groupNameLabel removeFromSuperview];
//
//    UIFont *titleFont = self.titleLabel.font;
//    self.titleLabel.font = [titleFont fontWithSize:20];
//    self.titleLabel.textAlignment = NSTextAlignmentCenter;
//}



//- (void)updateGroupSelection {
//    if (self.cugs.count == 1) {
//        [self hideGroupSelectionForOneAvailableGroup];
//        _pageNameForOmniture = OMNIPAGE_BILLS_AWAITING_PAYMENT;
//        return;
//    }
//
//    BTCug *cug = (BTCug *)[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug;
//    [self setGroupKey:cug.groupKey];
//    _pageNameForOmniture = [NSString stringWithFormat:@"%@",OMNIPAGE_BILLS_AWAITING_PAYMENT_CUG];
//    self.groupNameLabel.text = cug.cugName;
//}


//- (void)resetDataAndUIAfterGroupChange {
//
//    BOOL needToRefresh = YES;
//    if(_retryView)
//    {
//        if((![AppManager isInternetConnectionAvailable] && !_retryView.isHidden))
//            needToRefresh = NO;
//    }
//
//    if(needToRefresh){
//
//        [self hideRetryItems:YES];
//        [self fetchViewBillsAPI];
//    }
//    else
//    {
//        [_retryView updateRetryViewWithInternetStrip:YES];
//    }
//
//}



#pragma mark - Action methods

- (IBAction)homeButtonAction:(id)sender {

    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)switchSegment:(id)sender {

    _paymentSegmentSelectedIndex = self.billSegmentedControl.selectedSegmentIndex;
    [self hideEmmptyDashBoardItems:YES];

    //Change the omniturepagename
    [self updateOmniturePageIndex:(int)_paymentSegmentSelectedIndex];
    if(!_isRetryViewShown)
    {
        if(_paymentSegmentSelectedIndex == 0)
        {
            if(_awaitingPaymentBillsArray.count < 1)
            {

                [self showEmptyDashBoardViewWithDetailText:@"You haven't got any bills to pay."];
            }
            else
            {
                [self hideEmmptyDashBoardItems:NO];
                self.billDetailsTableView.hidden = NO;
            }


        }
        else
        {
            if(_recentlyPaidBillsArray.count < 1)
            {
                [self showEmptyDashBoardViewWithDetailText:@"You currently have no bills recently paid."];
            }
            else
            {
                [self hideEmmptyDashBoardItems:NO];
                self.billDetailsTableView.hidden = NO;
            }

        }

    }
    [self.billDetailsTableView reloadData];

}



//- (IBAction)groupButtonAction:(id)sender {
//
//    __weak typeof(self) weakSelf = self;
//
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Select Group Name" preferredStyle:UIAlertControllerStyleActionSheet];
//
//    DDLogVerbose(@"Bills: Total cugs belongs to user %lu.",(unsigned long)self.cugs.count);
//
//    // (SD) Add group names in actionsheet
//    if (self.cugs != nil && self.cugs.count > 0) {
//
//        // (SD) Add action for each cug selection.
//        int index = 0;
//        for (BTCug *groupData in self.cugs) {
//
//            UIAlertAction *action = [UIAlertAction actionWithTitle:groupData.cugName
//                                                             style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//                                                                 [weakSelf checkForSuperUserAndUpdateUIWithIndex:groupData.indexInAPIResponse];
//                                                                 [alert dismissViewControllerAnimated:YES completion:nil];
//                                                             }];
//            [alert addAction:action];
//
//            index++;
//        }
//    }
//
//    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
//                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
//                                                         [alert dismissViewControllerAnimated:YES completion:nil];
//                                                     }];
//    [alert addAction:cancel];
//
//    [self presentViewController:alert animated:YES completion:^{
//
//    }];
//
//
//
//}
//


#pragma mark - BTBillSummaryTableFooterDelegate

- (void)billsSummaryTableFooterView:(BTBillsSummaryTableFooterView *)billsSummaryTableFooterView payMultipleActionMethod:(id)sender
{

    [self trackOmnitureClick:OMNICLICK_BILLS_PAY_MULTIPLE_BILLS];
    [self showPayByCardScreen];

}



#pragma mark - RetryView Delegate

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    [self fetchViewBillsAPI];
}


#pragma mark - BTBillsHistoryScreenModel Delegates

- (void)successfullyFetchedBillPaymentHistoryDataOnBillHistoryDashboardScreen:(DLMBillHistoryDashboardScreen *)billHistoryDashboardScreen
{

    self.networkRequestInProgress = NO;
    [_loadingView stopAnimatingLoadingIndicatorView];
    self.spinnerBackgroundView.hidden = YES;

    //Create awaiting bill data
    if(billHistoryDashboardScreen.arrayOfAwaitingPayments && billHistoryDashboardScreen.arrayOfAwaitingPayments.count > 0)
    {
        _awaitingPaymentBillsArray = billHistoryDashboardScreen.arrayOfAwaitingPayments;

    }
    else
    {
        _awaitingPaymentBillsArray = nil;
    }

    //Create paid bill data
    if(billHistoryDashboardScreen.arrayOfRecentlyPaid && billHistoryDashboardScreen.arrayOfRecentlyPaid.count > 0)
    {

        _recentlyPaidBillsArray = billHistoryDashboardScreen.arrayOfRecentlyPaid;
    }
    else
    {
        _recentlyPaidBillsArray = nil;
    }


    //Update UI based on selected segment index
    dispatch_async(dispatch_get_main_queue(), ^{

        if(_paymentSegmentSelectedIndex == 0)
        {
            if(_awaitingPaymentBillsArray.count < 1)
            {
                self.billDetailsTableView.hidden = YES;
                [self showEmptyDashBoardViewWithDetailText:@"You haven't got any bills to pay."];
                [self trackOmniturePage:OMNIPAGE_BILLS_NO_BILLS];

            }
            else
            {
                [self.billDetailsTableView reloadData];
            }
        }

        else
        {
            if(_recentlyPaidBillsArray.count < 1)
            {
                self.billDetailsTableView.hidden = YES;
                [self showEmptyDashBoardViewWithDetailText:@"You currently have no bills recently paid."];

            }
            else
            {


                [self.billDetailsTableView reloadData];
            }
        }

    });
}


- (void)billHistoryDashboardScreen:(DLMBillHistoryDashboardScreen *)billHistoryDashboardScreen failedToFetchBillPaymentHistoryDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    self.networkRequestInProgress = NO;

    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];

    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {

        switch (webServiceError.error.code)
        {

            case BTNetworkErrorCodeAPINoDataFound:
            {
                [self showEmptyViewForSelectedTabWithIndex:(int)_paymentSegmentSelectedIndex];
                errorHandled = YES;
                [AppManager trackNoDataFoundErrorOnPage:_pageNameForOmniture];
                break;
            }
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }

    if(errorHandled == NO)
    {
        [self showRetryViewWithInternetStrip:NO];
        [AppManager trackGenericAPIErrorOnPage:_pageNameForOmniture];
    }
    
}


#pragma mark - Event tracking methods
- (void)trackOmniturePage:(NSString *)page
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = page;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];

    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackOmnitureClick:(NSString*)clickevent
{
    NSString *pageName = _pageNameForOmniture;
    NSString *linkTitle = clickevent;
    NSMutableDictionary *data = [NSMutableDictionary dictionary];

    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,linkTitle] withContextInfo:data];
}


- (void)updateOmniturePageIndex:(int)index
{
    if(index == 0) {

        _pageNameForOmniture = OMNIPAGE_BILLS_AWAITING_PAYMENT;
    }
    else {
        _pageNameForOmniture = OMNIPAGE_BILLS_PAID;
    }
    
    [self trackOmniturePage:_pageNameForOmniture];
    
}

@end
