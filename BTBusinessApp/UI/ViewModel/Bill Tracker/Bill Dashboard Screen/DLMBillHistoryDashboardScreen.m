//
//  DLMBillHistoryDashboardScreen.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMBillHistoryDashboardScreen.h"
#import "NLGetBillPaymentHistoryWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "CDCug+CoreDataClass.h"


@interface DLMBillHistoryDashboardScreen () <NLGetBillPaymentHistoryWebServiceDelegate> {
    

    NSDictionary *_billDataDict;
    
}

@property (nonatomic) NLGetBillPaymentHistoryWebService *getBillPaymentHistoryWebService;

@end

@implementation DLMBillHistoryDashboardScreen



- (id)initWithBillDataDict:(NSDictionary *)billDataDict{
    
    self = [super init];
    
    if(self){
        
        _billDataDict = billDataDict;
    }
    
    return self;
}


- (void)fetchBillPaymentHistoryData {
    
    
    if(_billDataDict){
        
        _arrayOfAwaitingPayments = [_billDataDict valueForKey:@"awaitingPayments"];
        _arrayOfRecentlyPaid = [_billDataDict valueForKey:@"recentlyPaid"];
        
        [self.billHistoryDashboardDelegate successfullyFetchedBillPaymentHistoryDataOnBillHistoryDashboardScreen:self];
        
        return;
    }

    
    self.getBillPaymentHistoryWebService = [[NLGetBillPaymentHistoryWebService alloc] init];
    self.getBillPaymentHistoryWebService.getBillPaymentHistoryWebServiceDelegate = self;
    [self.getBillPaymentHistoryWebService resume];
}


- (void)cancelBillHistoryAPIRequest
{
    self.getBillPaymentHistoryWebService.getBillPaymentHistoryWebServiceDelegate = nil;
    [self.getBillPaymentHistoryWebService cancel];
    self.getBillPaymentHistoryWebService = nil;
}

- (void)changeSelectedCUGTo:(BTCug *)selectedCug
{
    NSManagedObjectContext *context = [AppDelegate sharedInstance].managedObjectContext;
    CDCug *newPersistenceCug = [CDCug newCugInManagedObjectContext:context];
    newPersistenceCug.groupKey = selectedCug.groupKey;
    newPersistenceCug.cugName = selectedCug.cugName;
    newPersistenceCug.cugId = selectedCug.cugID;
    newPersistenceCug.refKey = selectedCug.refKey;
    newPersistenceCug.contactId = selectedCug.contactId;
    newPersistenceCug.cugRole = @(selectedCug.cugRole);
    newPersistenceCug.indexInAPIResponse = @(selectedCug.indexInAPIResponse);

    // Get the currently logged in User
    CDUser *loggedInUser = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    CDCug *previouslySelectedCug = loggedInUser.currentSelectedCug;
    loggedInUser.currentSelectedCug = newPersistenceCug;

    if(previouslySelectedCug)
    {
        [context deleteObject:previouslySelectedCug];
    }

    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
    _billDataDict = nil;
}


#pragma mark - NLGetBillPaymentHistoryWebServiceDelegate Methods

- (void)getBillPaymentHistoryWebService:(NLGetBillPaymentHistoryWebService *)webService successfullyFetchedPaymentHistoryDataWithAwaitingPayments:(NSArray *)arrayOfAwaitingPayments andRecentlyPaid:(NSArray *)arrayOfRecentlyPaid
{
   if(webService == self.getBillPaymentHistoryWebService)
   {
       _arrayOfAwaitingPayments = arrayOfAwaitingPayments;
       _arrayOfRecentlyPaid = arrayOfRecentlyPaid;
       
       [self.billHistoryDashboardDelegate successfullyFetchedBillPaymentHistoryDataOnBillHistoryDashboardScreen:self];
       
       self.getBillPaymentHistoryWebService.getBillPaymentHistoryWebServiceDelegate = nil;
       self.getBillPaymentHistoryWebService = nil;
       

   }
   else
   {
       DDLogError(@"This delegate method gets called because of success of an object of NLGetBillPaymentHistoryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
       NSAssert(NO, @"This delegate method gets called because of success of an object of NLGetBillPaymentHistoryWebService but this object is not the one stored in member variable of this class %@.", [self class]);

   }
}


- (void)getBillPaymentHistoryWebService:(NLGetBillPaymentHistoryWebService *)webService failedToFetchPaymentHistoryDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    if(webService == self.getBillPaymentHistoryWebService)
    {
        [self.billHistoryDashboardDelegate billHistoryDashboardScreen:self failedToFetchBillPaymentHistoryDataWithWebServiceError:webServiceError];
        
        self.getBillPaymentHistoryWebService.getBillPaymentHistoryWebServiceDelegate = nil;
        self.getBillPaymentHistoryWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLGetBillPaymentHistoryWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLGetBillPaymentHistoryWebService but this object is not the one stored in member variable of this class %@.", [self class]);

    }
}







@end
