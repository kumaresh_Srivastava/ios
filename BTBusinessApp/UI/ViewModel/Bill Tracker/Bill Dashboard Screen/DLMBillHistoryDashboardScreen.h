//
//  DLMBillHistoryDashboardScreen.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "DLMObject.h"
#import "BTCug.h"

@class DLMBillHistoryDashboardScreen;
@class NLWebServiceError;
@protocol DLMBillHistoryDashboardScreenDelegate <NSObject>

- (void)successfullyFetchedBillPaymentHistoryDataOnBillHistoryDashboardScreen:(DLMBillHistoryDashboardScreen *)billHistoryDashboardScreen;

- (void)billHistoryDashboardScreen:(DLMBillHistoryDashboardScreen *)billHistoryDashboardScreen failedToFetchBillPaymentHistoryDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end


@interface DLMBillHistoryDashboardScreen : DLMObject {
    
}

@property (nonatomic, copy) NSArray *arrayOfAwaitingPayments;
@property (nonatomic, copy) NSArray *arrayOfRecentlyPaid;
@property (nonatomic, weak) id <DLMBillHistoryDashboardScreenDelegate> billHistoryDashboardDelegate;

- (id)initWithBillDataDict:(NSDictionary *)billDataDict;
- (void)fetchBillPaymentHistoryData;
- (void)cancelBillHistoryAPIRequest;
- (void)changeSelectedCUGTo:(BTCug *)selectedCug;

@end
