//
//  CustomCircularLoaderAnimationView.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 2/28/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCircularLoaderAnimationView : UIView {
    
}

- (void)startAnimation;

@end
