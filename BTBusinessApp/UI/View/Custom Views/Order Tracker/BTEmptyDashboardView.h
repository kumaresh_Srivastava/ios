//
//  BTEmptyDashboardView.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 03/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTEmptyDashboardView;

@protocol BTEmptyDashboardViewDelegate <NSObject>

@required
- (void)userPressedSearchByOrderReferenceButtonOfEmptyDashboardView:(BTEmptyDashboardView *)retryView;

@end

@interface BTEmptyDashboardView : UIView

@property (weak, nonatomic) id <BTEmptyDashboardViewDelegate> emptyDashboardViewDelegate;


- (void)updateEmptyDashboardViewWithImageName:(NSString *)imageName title:(NSString *)title detailText:(NSAttributedString *)detailText andButtonTitle:(NSString *)buttonTitle;

- (void)updateEmptyDashboardViewWithImageNameAndLink:(NSString *)imageName title:(NSString *)title detailText:(NSMutableAttributedString *)detailText andButtonTitle:(NSString *)buttonTitle;
@end
