//
//  BTOrderMilestoneNodeView.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 09/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BTOrderMilestoneNodeModel;
@class BTOrderMilestoneNodeView;

@protocol BTOrderMilestoneNodeViewDelegate <NSObject>

- (void)userPressedChangeButtonOnOrderMilestoneNodeView:(BTOrderMilestoneNodeView *)orderMilestoneNodeView;
- (void)orderMilestoneNodeView:(BTOrderMilestoneNodeView *)orderMilestoneNodeView userPressedTrackDeliveryButtonWithTrackReference:(NSString *)trackingReference andTrackingURL:(NSString *)trackingUrl;

@end

@interface BTOrderMilestoneNodeView : UIView {
    
}

@property (weak, nonatomic) id <BTOrderMilestoneNodeViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTrackReferenceNumberButtonTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTrackReferenceNumberButtonLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTrackReferenceNumberButtonHeight;

@property (weak, nonatomic) IBOutlet UILabel *nodeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *nodeDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *trackReferenceNumberButton;
@property (weak, nonatomic) IBOutlet UILabel *dispatchDetailsLabel;
@property (weak, nonatomic) IBOutlet UIButton *changeButton;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *yearLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomEnabledLineView;
@property (weak, nonatomic) IBOutlet UIView *topEnabledLineView;
@property (weak, nonatomic) IBOutlet UIView *bottomDisabledLineView;
@property (weak, nonatomic) IBOutlet UIView *topDisabledLineView;
@property (weak, nonatomic) IBOutlet UIImageView *nodeCircleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *nodeCircleOverlayView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *changButtonTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *changeButtonBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dispatchDetailConstraintTop;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dispatchDetailBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dispatchButtonBottomConstraint;

//- (void)updateMilestoneNodeViewWithData:(BTOrderMilestoneNodeModel *)nodeData isFirstNode:(BOOL)isFirstNode isLastNode:(BOOL)isLastNode andDispatchDetails:(NSArray *)arrayOfDispatchDetails;
- (void)updateMilestoneNodeViewWithData:(BTOrderMilestoneNodeModel *)nodeData isFirstNode:(BOOL)isFirstNode isLastNode:(BOOL)isLastNode
                       isShowAppoitment:(BOOL)isShowAppoitment andDispatchDetails:(NSArray *)arrayOfDispatchDetails;

@end
