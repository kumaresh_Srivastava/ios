//
//  BTViewFullDetailsView.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 10/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTViewFullDetailsView.h"

@implementation BTViewFullDetailsView

- (void)drawRect:(CGRect)rect {
    // Drawing code
    self.viewFullDetailsButton.backgroundColor = [UIColor clearColor];
    self.viewFullDetailsButton.layer.borderColor = [BrandColours colourBackgroundBTPurplePrimaryColor].CGColor;
    self.viewFullDetailsButton.layer.borderWidth = 1.0f;
    self.viewFullDetailsButton.layer.cornerRadius = 5.0f;
}

- (IBAction)viewFullDetailsButtonAction:(id)sender {
    [self.delegate userClickedOnViewFullDetailsButtonOfViewFullDetailsView:self];
}

@end
