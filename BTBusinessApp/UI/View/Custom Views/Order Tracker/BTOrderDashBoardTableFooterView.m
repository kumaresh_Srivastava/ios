//
//  BTOrderDashBoardTableFooterView.m
//  BTBusinessApp
//
//  Created by Accolite on 22/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderDashBoardTableFooterView.h"

@implementation BTOrderDashBoardTableFooterView

#pragma mark Action methods

- (IBAction)retryButtonAction:(id)sender {
    if ([self.orderDashBoardFooterDelegate respondsToSelector:@selector(orderDashBoardTableFooterView:retryBUttonActionMethod:)]) {
        [self.orderDashBoardFooterDelegate orderDashBoardTableFooterView:self retryBUttonActionMethod:sender];
    }
}

#pragma mark public methods

- (void)createCustomLoadingIndicator {
    
    if(footerView){
        [footerView removeFromSuperview];
        footerView = nil;
        
    }
        self.loadingVIewbackground.hidden = NO;
        footerView = [[CustomSpinnerAnimationView alloc] initWithText:@"" withLoadingImage:[UIImage imageNamed:@"Spinner"]];
        footerView.frame = CGRectMake(0, 0, self.loadingVIewbackground.frame.size.width, self.loadingVIewbackground.frame.size.height);
        footerView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.loadingVIewbackground addSubview:footerView];
        
       // self.retryView.backgroundColor = UIColor.redColor;
        
        /*[self.retryView addConstraint:[NSLayoutConstraint constraintWithItem:footerView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.retryView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:-20.0]];
        [self.retryView addConstraint:[NSLayoutConstraint constraintWithItem:footerView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.retryView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:-30.0]];
        [_retryView addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:100]];
        [_retryView addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:155]];
        footerView.backgroundColor = [UIColor clearColor];
        [self setNeedsLayout];*/
   
}

- (void) hideRetryView {
    [self.errorLabel setHidden:YES];
    [self.retryButton setHidden:YES];
}

- (void) showRetryView {
    
    [self.errorLabel setHidden:NO];
    [self.retryButton setHidden:NO];
    [self.loadingVIewbackground setHidden:YES];
}

- (void)showRetryViewWithErrorMessage:(NSString *)error {
    
    self.errorLabel.text = error;
    [self.errorLabel setHidden:NO];
    [self.retryButton setHidden:NO];
    [self.loadingVIewbackground setHidden:YES];
    
    //_constraintRetryViewHeight.constant = kRetryViewOriginalHeight;
//    [self layoutIfNeeded];
}

- (void) hideCustomLoadingIndicator {
    [footerView setHidden:YES];
    [self.loadingVIewbackground setHidden:YES];
}

- (void) showCustomLoadingIndicator {
    [footerView setHidden:NO];
    [self.loadingVIewbackground setHidden:NO];
}

- (void) startLoadingIndicatorAnimation {
    
    [self stopCustomLoadingIndicatorAnimation];
    
    //[self createCustomLoadingIndicator];
    [self performSelector:@selector(createCustomLoadingIndicator) withObject:nil afterDelay:0.1];
    
    [footerView setIsStopped:NO];
    [self showCustomLoadingIndicator];
    [footerView startAnimation];
}

- (void) stopCustomLoadingIndicatorAnimation {
    [footerView stopAnimation];
    [self hideCustomLoadingIndicator];
    
    [footerView removeFromSuperview];
    footerView = nil;
}
@end
