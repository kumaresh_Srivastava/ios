//
//  BTOrderDetailsTakeActionView.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 09/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BTOrderDetailsTakeActionView;

@protocol BTOrderDetailsTakeActionViewDelegate <NSObject>

- (void)userPressedAmendOrderDetailsOfTakeActionView:(BTOrderDetailsTakeActionView *)takeActionView;
- (void)userPressedAddAppointmentToCalenderOfTakeActionView:(BTOrderDetailsTakeActionView *)takeActionView;
- (void)userPressedContactUsButtonOfTakeActionView:(BTOrderDetailsTakeActionView *)takeActionView;

@end

@interface BTOrderDetailsTakeActionView : UIView

@property (weak, nonatomic) id <BTOrderDetailsTakeActionViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintAddAppointmentToCalenderViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeaderViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintAmendOrderDetailsViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintOrderNotificationsViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintNeedHelpButtonHeight;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *amendOrderDetailsView;
@property (weak, nonatomic) IBOutlet UIButton *needHelpButton;
@property (weak, nonatomic) IBOutlet UIView *addAppointmentToCalenderView;
@property (weak, nonatomic) IBOutlet UIView *notificationsView;

- (void)hideAddAppointmentToCalenderView;
- (void)showAddAppointmentToCalenderView;
- (void)hideAllTakeActionViewItems;
- (void)showAllTakeActionViewItems;

- (void)startLoaderView;
- (void)stopLoaderView;

@end
