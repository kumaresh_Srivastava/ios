//
//  BTOrderSummaryView.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 10/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMOrderDetailsScreen.h"
@class BTOrderSummaryView;
@class BTProduct;
@class BTPricingDetails;
@class BTProductDetails;


#define kConstraintBroadbandDetailsHeightOriginal 223;//182;
#define kConstraintBroadbandDetailsHeightPhoneNumber 111;//70;

@protocol BTOrderSummaryViewDelegate <NSObject>

- (void)userPressedDetailsForEngineerAppointmentOfOrderSummaryView:(BTOrderSummaryView *)orderSummaryView;
- (void)userPressedDetailsForBillingDetailsOfOrderSummaryView:(BTOrderSummaryView *)orderSummaryView;

@end

@interface BTOrderSummaryView : UIView {
    
}

@property (weak, nonatomic) id <BTOrderSummaryViewDelegate> orderSummaryViewDelegate;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBroadbandDetailsHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintExpectedCompletionLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintEngineerAppointmentTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBillingDetailsTop;

@property (weak, nonatomic) IBOutlet UIView *engineerAppointmentView;
@property (weak, nonatomic) IBOutlet UIView *phoneOrBroadbandDetailsView;
@property (weak, nonatomic) IBOutlet UILabel *paymentDescriptionLabel;

@property (weak, nonatomic) IBOutlet UIButton *engineerAppointmentDetailsButton;

@property (weak, nonatomic) IBOutlet UILabel *expectedCompletionLabel;

@property (weak, nonatomic) IBOutlet UILabel *expectedDispatchLabel;
@property (weak, nonatomic) IBOutlet UILabel *expectedDispatchDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *expectedCompletionDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *engineerAppointmentDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *oneOffPriceValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *regularPriceValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneOrBroadbandDetailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneOrBroadbandLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneOrBroadbandNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *networkUserIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *networkUserIdValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *primaryEmailLabel;
@property (weak, nonatomic) IBOutlet UILabel *primaryEmailIDLabel;

//- (void)updateOrderSummaryViewWithProduct:(BTProduct *)product productOrderItems:(BTPricingDetails *)pricingDetails withAuthLevel:(NSInteger)authLevel andPaymentModeType:(NSString *)paymentModeType;
- (void)updateOrderSummaryViewWithProduct:(BTProduct *)product productOrderItems:(BTPricingDetails *)pricingDetails withAuthLevel:(NSInteger)authLevel isShowAppointment:(BOOL)isShowAppointment andPaymentModeType:(NSString *)paymentModeType;
- (void) updateOrderSummaryViewWithProductDetails:(BTProductDetails *)productDetails andSummaryType:(BTOrderDetailSummaryType)summaryType;

- (void)showEngineerAppointmentDetailsButton;
- (void)hideEngineerAppointmentDetailsButton;

@end
