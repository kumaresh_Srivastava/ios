//
//  BTEmptyOrderSummaryAndMilestoneView.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 3/3/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTEmptyOrderSummaryAndMilestoneView;

@protocol BTEmptyOrderSummaryAndMilestoneViewDelegate <NSObject>

- (void)userPressedNeedHelpButtonOnEmptyOrderSummaryAndMilestoneView:(BTEmptyOrderSummaryAndMilestoneView *)emptyOrderSummaryAndMilestoneView;

@end

@interface BTEmptyOrderSummaryAndMilestoneView : UIView

@property (nonatomic, weak) id <BTEmptyOrderSummaryAndMilestoneViewDelegate> emptyOrderSummaryAndMilestoneViewDelegate;

- (void)updateWithMessageString:(NSString *)messageString;

@end
