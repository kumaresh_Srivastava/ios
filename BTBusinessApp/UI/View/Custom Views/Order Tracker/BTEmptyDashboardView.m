//
//  BTEmptyDashboardView.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 03/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTEmptyDashboardView.h"
#import "NSObject+APIResponseCheck.h"

@interface BTEmptyDashboardView()

@property (weak, nonatomic) IBOutlet UIImageView *emptyImageView;
@property (weak, nonatomic) IBOutlet UITextView *detailedTextTextView;

@property (weak, nonatomic) IBOutlet UILabel *headingTextLabel;

@property (weak, nonatomic) IBOutlet UIButton *searchButton;

@end

@implementation BTEmptyDashboardView


- (void)awakeFromNib
{
    [super awakeFromNib];

    _searchButton.layer.cornerRadius = 5.0;
    _searchButton.layer.borderWidth = 1.0;
    _searchButton.layer.borderColor =  [BrandColours colourBackgroundBTPurplePrimaryColor].CGColor;

}
//[SD] Updating Empty Dashboard View
- (void)updateEmptyDashboardViewWithImageName:(NSString *)imageName title:(NSString *)title detailText:(NSAttributedString *)detailText andButtonTitle:(NSString *)buttonTitle
{

    if([imageName validAndNotEmptyStringObject])
    {
        UIImage *image = [UIImage imageNamed:imageName];
        if(image)
        {
            self.emptyImageView.image = image;
        }
        else
        {
            DDLogError(@"Empty Dashboard: image not exist");
        }
    }
    else
    {
        self.emptyImageView.hidden = YES;
        DDLogError(@"Empty Dashboard: image Name is nil");
    }

    if([title validAndNotEmptyStringObject])
    {
        self.headingTextLabel.text = title;
    }
    else
    {
        self.headingTextLabel.text = @"";
    }

    if(detailText && detailText.length>0)
    {
        self.detailedTextTextView.attributedText = detailText;
    }
    else
    {
        self.detailedTextTextView.attributedText = [[NSAttributedString alloc] initWithString:@""];
    }

    if([buttonTitle validAndNotEmptyStringObject])
    {
        self.searchButton.titleLabel.text = buttonTitle;
    }
    else
    {
        self.searchButton.hidden = YES;
    }


}

//MSC Updating empty dash board with links

- (void)updateEmptyDashboardViewWithImageNameAndLink:(NSString *)imageName title:(NSString *)title detailText:(NSAttributedString *)detailText andButtonTitle:(NSString *)buttonTitle
{
    
    if([imageName validAndNotEmptyStringObject])
    {
        UIImage *image = [UIImage imageNamed:imageName];
        if(image)
        {
            self.emptyImageView.image = image;
        }
        else
        {
            DDLogError(@"Empty Dashboard: image not exist");
        }
    }
    else
    {
        DDLogError(@"Empty Dashboard: image Name is nil");
    }
    
    if([title validAndNotEmptyStringObject])
    {
        self.headingTextLabel.text = title;
    }
    else
    {
        self.headingTextLabel.text = @"";
    }
    
    if(detailText && detailText.length>0)
    {
        self.detailedTextTextView.attributedText = detailText;
        self.detailedTextTextView.tintColor = [BrandColours colourTextBTPurplePrimaryColor];
        self.detailedTextTextView.editable = NO;
    }
    else
    {
        self.detailedTextTextView.attributedText = [[NSAttributedString alloc] initWithString:@""];
        self.detailedTextTextView.editable = NO;
    }
    
    if([buttonTitle validAndNotEmptyStringObject])
    {
        self.searchButton.titleLabel.text = buttonTitle;
    }
    else
    {
        self.searchButton.hidden = YES;
    }
    
    
}




- (IBAction)searchByOrderReferenceButtonAction:(id)sender {
    [self.emptyDashboardViewDelegate userPressedSearchByOrderReferenceButtonOfEmptyDashboardView:self];
}

@end
