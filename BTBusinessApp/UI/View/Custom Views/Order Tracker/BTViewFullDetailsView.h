//
//  BTViewFullDetailsView.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 10/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTViewFullDetailsView;

@protocol BTViewFullDetailsViewDelegate <NSObject>

- (void)userClickedOnViewFullDetailsButtonOfViewFullDetailsView:(BTViewFullDetailsView *)viewFullDetailsView;

@end

@interface BTViewFullDetailsView : UIView {
    
}

@property (weak, nonatomic) id <BTViewFullDetailsViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *viewFullDetailsButton;

@end
