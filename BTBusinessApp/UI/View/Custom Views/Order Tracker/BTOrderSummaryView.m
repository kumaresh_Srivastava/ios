//
//  BTOrderSummaryView.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 10/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderSummaryView.h"
#import "AppManager.h"
#import "BTProduct.h"
#import "BTPricingDetails.h"
#import "BTProductDetails.h"
#import "AppConstants.h"


@interface BTOrderSummaryView()
{
    NSString *_productName;
    BOOL _isDateViewHidden;
}
@end

@implementation BTOrderSummaryView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)updateOrderSummaryViewWithProduct:(BTProduct *)product productOrderItems:(BTPricingDetails *)pricingDetails withAuthLevel:(NSInteger)authLevel isShowAppointment:(BOOL)isShowAppointment andPaymentModeType:(NSString *)paymentModeType 
{
    // Write the code to update the view
    /*if ([AppManager isValidDate:product.expectedDispatchDate]) {
        self.expectedDispatchLabel.text = @"Expected Dispatch";
        if(isShowAppointment == NO){
            [self.expectedDispatchDateLabel setText:@"Will be updated shortly"];
            self.expectedDispatchDateLabel.font = [UIFont fontWithName:kBtFontRegular size:18.0f];
        }
        else{
             [self.expectedDispatchDateLabel setText:[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:product.expectedDispatchDate]];
            }
        self.constraintExpectedCompletionLeading.constant = 15;//[UIApplication sharedApplication].keyWindow.frame.size.width/2;
    } else {
        
        self.expectedDispatchLabel.hidden = YES;
        self.expectedDispatchDateLabel.hidden = YES;
        self.expectedDispatchLabel.text = @"";
        self.expectedCompletionDateLabel.text = @"";
        self.constraintExpectedCompletionLeading.constant = 15 - [UIApplication sharedApplication].keyWindow.frame.size.width/2;
    }*/
    if(isShowAppointment == NO){
        self.expectedDispatchLabel.text = @"Expected Dispatch";
        [self.expectedDispatchDateLabel setText:@"Will be updated shortly"];
        self.expectedDispatchDateLabel.font = [UIFont fontWithName:kBtFontRegular size:18.0f];
        self.constraintExpectedCompletionLeading.constant = 15;//[UIApplication sharedApplication].keyWindow.frame.size.width/2;
        
    } else {
        if ([AppManager isValidDate:product.expectedDispatchDate]) {
            self.expectedDispatchLabel.text = @"Expected Dispatch";
            [self.expectedDispatchDateLabel setText:[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:product.expectedDispatchDate]];
            self.constraintExpectedCompletionLeading.constant = 15;//[UIApplication sharedApplication].keyWindow.frame.size.width/2;
        } else {
            self.expectedDispatchLabel.hidden = YES;
            self.expectedDispatchDateLabel.hidden = YES;
            self.expectedDispatchLabel.text = @"";
            self.expectedCompletionDateLabel.text = @"";
            self.constraintExpectedCompletionLeading.constant = 15 - [UIApplication sharedApplication].keyWindow.frame.size.width/2;
        }
    }
    
    
    
    if ([AppManager isValidDate:product.productDuedate])
    {
        if ([product.status isEqualToString:@"Completed"]) {
            [self.expectedCompletionLabel setText:@"Completed on"];
        }
        if(isShowAppointment == NO){
            [self.expectedCompletionDateLabel setText:@"Will be updated shortly"];
            self.expectedCompletionDateLabel.font = [UIFont fontWithName:kBtFontRegular size:18.0f];
        }
        else{
            [self.expectedCompletionDateLabel setText:[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:product.productDuedate]];
        }
    }
    else
    {
        self.expectedCompletionLabel.hidden = YES;
        self.expectedCompletionDateLabel.hidden = YES;
        [self.expectedCompletionLabel setText:@""];
        [self.expectedCompletionDateLabel setText:@""];
        
    }
    
    if (![AppManager isValidDate:product.expectedDispatchDate] && ![AppManager isValidDate:product.productDuedate])
    {
        _isDateViewHidden = true;
        _constraintEngineerAppointmentTop.constant = 0;
    }
    else
    {
        _isDateViewHidden = false;
        _constraintEngineerAppointmentTop.constant = 85;
    }
    
    NSString *engineeringDate = @"";
    if(isShowAppointment == NO){
       engineeringDate = @"Will be updated shortly";
    }
    else{
        engineeringDate = [NSString stringWithFormat:@"%@, %@",[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:product.engineeringAppointmentDate],product.engineeringAppointmentSlot];
        engineeringDate =  [engineeringDate stringByReplacingOccurrencesOfString:@"(" withString:@""];
        engineeringDate = [engineeringDate  stringByReplacingOccurrencesOfString:@")" withString:@""];
    }
    [self.engineerAppointmentDateLabel setText:engineeringDate];
    
    //Commented by RM
    //NSString *oneOff = [NSString stringWithFormat:@"£%.2f",product.oneOffPrice];
    //NSString *regular = [NSString stringWithFormat:@"£%.2f",product.monthlyPrice];

   // [self.oneOffPriceValueLabel setText:oneOff];
   // [self.regularPriceValueLabel setText:@"£897234.342343"];
    
    
    //(RLM) 10 Feb 17 Now payment mode is static. It will always be regular.
    //To make it dymaninc uncomment this.
//    self.paymentDescriptionLabel.attributedText = [self billingDetailsStringForOneOff:product.oneOffPrice andRegular:product.monthlyPrice andRegularViewTitle:[paymentModeType lowercaseString]];
    
//    self.paymentDescriptionLabel.attributedText = [self billingDetailsStringForOneOff:pricingDetails.totalOneOffPrice regular:pricingDetails.totalMonthlyPrice regularViewTitle:@"regular" terminationCharges:(pricingDetails.totalTerminationCharge + pricingDetails.totalHttpCharge) andJourneyType:pricingDetails.journeyType];

    self.paymentDescriptionLabel.attributedText = [self billingDetailsStringForOneOff:pricingDetails.totalOneOffPrice regular:pricingDetails.totalMonthlyPrice regularViewTitle:@"/month" terminationCharges:(pricingDetails.totalTerminationCharge + pricingDetails.totalHttpCharge) andJourneyType:pricingDetails.journeyType];
    
    _productName = [product.productName lowercaseString];
    
    if (authLevel != 1) {
        
        [self hideBroadbandDetails:YES];
        
        if (![AppManager isValidDate:product.engineeringAppointmentDate])
        {
            [self.engineerAppointmentView removeFromSuperview];
            
            if (_isDateViewHidden)
            {
                _constraintBillingDetailsTop.constant = 0;
            }
            else
            {
                _constraintBillingDetailsTop.constant = 85;
            }
            
        } else
        {
            self.engineerAppointmentDetailsButton.hidden = YES;
        }
    } else {
        
        
        if (![AppManager isValidDate:product.engineeringAppointmentDate])
        {
            [self.engineerAppointmentView removeFromSuperview];
            
            if (_isDateViewHidden)
            {
                _constraintBillingDetailsTop.constant = 0;
            }
            else
            {
                _constraintBillingDetailsTop.constant = 85;
            }
            
        } else {
            self.engineerAppointmentDetailsButton.hidden = NO;
            
        }
    }
    
}


- (NSAttributedString *)billingDetailsStringForOneOff:(double)oneOff regular:(double)regular regularViewTitle:(NSString *)regularViewTitle terminationCharges:(double)terminationCharge andJourneyType:(int)journeyType
{
    
    if(!regularViewTitle || regularViewTitle.length == 0)
    {
      regularViewTitle = @"-";
    }
    
    NSString *usageUnit = @"";
    
    NSString *oneOffData = [NSString stringWithFormat:@"£%.2f %@", oneOff, usageUnit];
    
    NSString *regularData = [NSString stringWithFormat:@"£%.2f %@", regular, usageUnit];
    
    NSString *terminationData = [NSString stringWithFormat:@"£%.2f %@", terminationCharge, usageUnit];
    
    
    UIFont *textFont = [UIFont fontWithName:kBtFontRegular size:18.0];
    NSDictionary *dataPartDict = [NSDictionary dictionaryWithObject:textFont forKey:NSFontAttributeName];
    
    textFont = [UIFont fontWithName:kBtFontRegular size:14.0];
    NSDictionary *textPartDict = [NSDictionary dictionaryWithObject:textFont forKey:NSFontAttributeName];
    
    
    //OneOff
    NSAttributedString *oneOffDataAttString = [[NSAttributedString alloc] initWithString:oneOffData attributes:dataPartDict];
    
    NSAttributedString *downloadText = [[NSAttributedString alloc]initWithString:@" one-off \n" attributes:textPartDict];
    //[aAttrString appendAttributedString:downloadText];
    
    
    
    //Regular
    NSAttributedString *uploadDataAttString = [[NSAttributedString alloc] initWithString:regularData attributes:dataPartDict];
    //[aAttrString appendAttributedString:uploadDataAttString];
    
    NSAttributedString *uploadText = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@" %@  ",regularViewTitle] attributes:textPartDict];
    //[aAttrString appendAttributedString:uploadText];
    
    //Termination
    NSString* terminationNewLine = [NSString stringWithFormat:@"\n%@",terminationData];
    NSAttributedString *terminationDataAttString = [[NSAttributedString alloc] initWithString:terminationNewLine attributes:dataPartDict];
    //[aAttrString appendAttributedString:terminationDataAttString];
    
    NSAttributedString *terminationText = [[NSAttributedString alloc]initWithString:@"termination charges " attributes:textPartDict];
    //[aAttrString appendAttributedString:terminationText];
    
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] init];
    
    if (journeyType == 2)
    {
        [aAttrString appendAttributedString:uploadDataAttString];
        [aAttrString appendAttributedString:uploadText];
        [aAttrString appendAttributedString:terminationDataAttString];
        [aAttrString appendAttributedString:terminationText];
    }
    else
    {
        [aAttrString appendAttributedString:oneOffDataAttString];
        [aAttrString appendAttributedString:downloadText];
        [aAttrString appendAttributedString:uploadDataAttString];
        [aAttrString appendAttributedString:uploadText];
    }
    
    return aAttrString;
    
}

- (void) updateOrderSummaryViewWithProductDetails:(BTProductDetails *)productDetails andSummaryType:(BTOrderDetailSummaryType)summaryType;{
    
    
    switch (summaryType) {
            
        case BTOrderDetailSummaryTypeUnknown:
        case BTOrderDetailSummaryTypeISDN:
        {
            [self hideBroadbandDetails:YES];
        }
            break;
            
            
        case BTOrderDetailSummaryTypePhone:
        {
            if (productDetails.phoneNumber == nil || [productDetails.phoneNumber isEqualToString:@""]) {
                [self hideBroadbandDetails:YES];
            }
            else
            {
                [self hideBroadbandDetails:NO];
                [self.phoneOrBroadbandDetailsLabel setText:@"Phone line number"];
                self.phoneOrBroadbandLabel.text = @"";
                [self.phoneOrBroadbandNumberLabel setText:productDetails.phoneNumber];
                self.networkUserIdLabel.text = @"";
                self.networkUserIdValueLabel.text = @"";
                self.primaryEmailLabel.text = @"";
                self.primaryEmailIDLabel.text = @"";
                
                self.constraintBroadbandDetailsHeight.constant = kConstraintBroadbandDetailsHeightPhoneNumber;
                
                [self layoutIfNeeded];
            }

        }
            break;
            
        case BTOrderDetailSummaryTypeBroadband:
        {
            if ((productDetails.phoneNumber == nil || [productDetails.phoneNumber isEqualToString:@""]) && (productDetails.networkUserId == nil || [productDetails.networkUserId isEqualToString:@""]) && (productDetails.primaryEmailAddress == nil || [productDetails.primaryEmailAddress isEqualToString:@""])) {
                
                [self hideBroadbandDetails:YES];
            }
            else
            {
                [self hideBroadbandDetails:NO];
                [self.phoneOrBroadbandDetailsLabel setText:@"Broadband details"];
                
                self.constraintBroadbandDetailsHeight.constant = kConstraintBroadbandDetailsHeightOriginal;
                
                if (productDetails.phoneNumber == nil || [productDetails.phoneNumber isEqualToString:@""]) {
                    self.phoneOrBroadbandLabel.text = @"";
                    [self.phoneOrBroadbandNumberLabel setText:@""];
                    
                    self.constraintBroadbandDetailsHeight.constant -= 50;
                }
                else
                {
                    self.phoneOrBroadbandLabel.text = @"Broadband number";
                    [self.phoneOrBroadbandNumberLabel setText:productDetails.phoneNumber];
                }
                
                if (productDetails.networkUserId == nil || [productDetails.networkUserId isEqualToString:@""]) {
                    self.networkUserIdLabel.text = @"";
                    [self.networkUserIdValueLabel setText:@""];
                    
                    self.constraintBroadbandDetailsHeight.constant -= 50;
                }
                else
                {
                    self.networkUserIdLabel.text = @"Network user ID";
                    [self.networkUserIdValueLabel setText:productDetails.networkUserId];
                }
                
                if (productDetails.primaryEmailAddress == nil || [productDetails.primaryEmailAddress isEqualToString:@""]) {
                    self.primaryEmailLabel.text = @"";
                    [self.primaryEmailIDLabel setText:@""];
                    
                    self.constraintBroadbandDetailsHeight.constant -= 50;
                }
                else
                {
                    self.primaryEmailLabel.text = @"Primary Email";
                    [self.primaryEmailIDLabel setText:productDetails.primaryEmailAddress];
                }
                
                [self layoutIfNeeded];
            }

        }
            break;
            
        default:
            break;
    }

}

- (void)hideBroadbandDetails:(BOOL)isHide
{
    if (isHide) {
        self.phoneOrBroadbandDetailsLabel.text = @"";
        self.phoneOrBroadbandLabel.text = @"";
        self.phoneOrBroadbandNumberLabel.text = @"";
        self.networkUserIdLabel.text = @"";
        self.networkUserIdValueLabel.text = @"";
        self.primaryEmailLabel.text = @"";
        self.primaryEmailIDLabel.text = @"";
        
        self.phoneOrBroadbandDetailsView.hidden = YES;
        self.constraintBroadbandDetailsHeight.constant = 0;
        
        [self layoutIfNeeded];
        
    } else {
        
        self.phoneOrBroadbandDetailsView.hidden = NO;
        //self.constraintBroadbandDetailsHeight.constant = kConstraintBroadbandDetailsHeightOriginal;
        
        [self layoutIfNeeded];
    }
    
}

- (void)showEngineerAppointmentDetailsButton
{
    self.engineerAppointmentDetailsButton.hidden = NO;
}

- (void)hideEngineerAppointmentDetailsButton
{
    self.engineerAppointmentDetailsButton.hidden = YES;
}

- (IBAction)engineerAppointmentDetailsButtonAction:(id)sender {
    [self.orderSummaryViewDelegate userPressedDetailsForEngineerAppointmentOfOrderSummaryView:self];
}


- (IBAction)billingDetailsButtonAction:(id)sender {
    [self.orderSummaryViewDelegate userPressedDetailsForBillingDetailsOfOrderSummaryView:self];
}


@end
