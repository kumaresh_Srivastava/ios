//
//  BTBundlePricingFooterView.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 14/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTBundlePricingFooterView : UIView

@property (weak, nonatomic) IBOutlet UILabel *priceValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

- (void)updateViewWithPriceValue:(double )priceValue withPriceTypeLabel:(NSString *)priceTypeLabel;

@end
