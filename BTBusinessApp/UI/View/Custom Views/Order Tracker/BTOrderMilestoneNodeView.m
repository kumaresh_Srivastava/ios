//
//  BTOrderMilestoneNodeView.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 09/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderMilestoneNodeView.h"
#import "BTOrderMilestoneNodeModel.h"
#import "BrandColours.h"
#import "AppManager.h"
#import "BTOrderDispatchDetailsModel.h"
#import "AppConstants.h"

#define kOriginalDispatchDetailsLabelWidth 94;

@interface BTOrderMilestoneNodeView () {
    
    NSRange _dispatchStringTappableRange;
    BTOrderDispatchDetailsModel *_dispatchDetailsModel;
}

@end

@implementation BTOrderMilestoneNodeView

- (void)updateMilestoneNodeViewWithData:(BTOrderMilestoneNodeModel *)nodeData isFirstNode:(BOOL)isFirstNode isLastNode:(BOOL)isLastNode
                       isShowAppoitment:(BOOL)isShowAppoitment andDispatchDetails:(NSArray *)arrayOfDispatchDetails
{
    self.topEnabledLineView.backgroundColor = [UIColor colorForHexString:@"dddddd"];//[BrandColours colourBackgroundBTPurplePrimaryColor];
    self.bottomEnabledLineView.backgroundColor = [UIColor colorForHexString:@"dddddd"];//[BrandColours colourBackgroundBTPurplePrimaryColor];
    self.nodeTitleLabel.text = nodeData.nodeDisplayName;
    /*if([nodeData.nodeDisplayName isEqualToString:@"Engineer appointment"] || [nodeData.nodeDisplayName isEqualToString:@"Completion"] || [nodeData.nodeDisplayName isEqualToString:@"Equipment dispatched"] || [nodeData.nodeDisplayName isEqualToString:@"Expected Dispatch"] || [nodeData.nodeDisplayName isEqualToString:@"Expected dispatch"] || [nodeData.nodeDisplayName isEqualToString:@"Expected completion"] || [nodeData.nodeDisplayName isEqualToString:@"Expected Completion"] || [nodeData.nodeDisplayName isEqualToString:@"Changes to order details"])*/
         //if(nodeData.nodeType != 0 && nodeData.nodeType != 1)
        if(
           ([nodeData.nodeDisplayName compare:@"Engineer appointment" options:NSCaseInsensitiveSearch] == NSOrderedSame) ||
           ([nodeData.nodeDisplayName compare:@"Completion" options:NSCaseInsensitiveSearch] == NSOrderedSame) ||
           ([nodeData.nodeDisplayName compare:@"Equipment dispatched" options:NSCaseInsensitiveSearch] == NSOrderedSame) ||
           ([nodeData.nodeDisplayName compare:@"Expected Completion" options:NSCaseInsensitiveSearch] == NSOrderedSame)
           )
        {
            if(isShowAppoitment == NO){
                self.nodeDescriptionLabel.text = @"Will be updated shortly";
            }
            else {
                self.nodeDescriptionLabel.text = nodeData.nodeDescription;
             }
        } else {
            self.nodeDescriptionLabel.text = nodeData.nodeDescription;
        }
    
    if (arrayOfDispatchDetails)
    {
        BTOrderDispatchDetailsModel *dispatchDetails = [arrayOfDispatchDetails objectAtIndex:0];
        _dispatchDetailsModel = dispatchDetails;
        
        [self.trackReferenceNumberButton setTitle:dispatchDetails.trackingReferenceNumber forState:UIControlStateNormal];
        
        [self showDispatchDetails];
        
    }
    else
    {
        [self hideDispatchDetails];
    }
    
    if ([AppManager isValidDate:nodeData.date]) {
        
        NSString *dateString = @"";
       /*if([nodeData.nodeDisplayName isEqualToString:@"Engineer appointment"] || [nodeData.nodeDisplayName isEqualToString:@"Completion"] || [nodeData.nodeDisplayName isEqualToString:@"Equipment dispatched"] || [nodeData.nodeDisplayName isEqualToString:@"Expected Dispatch"] || [nodeData.nodeDisplayName isEqualToString:@"Expected dispatch"] || [nodeData.nodeDisplayName isEqualToString:@"Expected completion"] || [nodeData.nodeDisplayName isEqualToString:@"Expected Completion"] )*/
    //if(nodeData.nodeType != 0 && nodeData.nodeType != 1)
        if(
           ([nodeData.nodeDisplayName compare:@"Engineer appointment" options:NSCaseInsensitiveSearch] == NSOrderedSame) ||
           ([nodeData.nodeDisplayName compare:@"Completion" options:NSCaseInsensitiveSearch] == NSOrderedSame) ||
           ([nodeData.nodeDisplayName compare:@"Equipment dispatched" options:NSCaseInsensitiveSearch] == NSOrderedSame) ||
           ([nodeData.nodeDisplayName compare:@"Expected Completion" options:NSCaseInsensitiveSearch] == NSOrderedSame)
           )
           {
                if(isShowAppoitment == NO){
                    dateString = @"Will be updated shortly";
                    self.dateLabel.font = [UIFont fontWithName:kBtFontRegular size:12.0f];
                     self.dateLabel.textColor = [UIColor colorForHexString:@"666666"];
                }
                else {
                   dateString = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:nodeData.date];
                    self.dateLabel.font = [UIFont fontWithName:kBtFontBold size:16.0f];
                }
            } else {
                dateString = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:nodeData.date];
                self.dateLabel.font = [UIFont fontWithName:kBtFontBold size:16.0f];
            }

        self.dateLabel.text = dateString;
        
        self.dateLabel.textColor = [UIColor colorForHexString:@"333333"];
        self.yearLabel.text = @"";

    }
    else
    {
        self.dateLabel.text = @"Date not yet known";
        self.dateLabel.font = [UIFont fontWithName:kBtFontRegular size:12.0f];
        self.dateLabel.textColor = [UIColor colorForHexString:@"666666"];
        self.yearLabel.text = @"";
    }
    
    if (nodeData.isAppointmentAmendSource || nodeData.isActivationAmendSource) {
        [self.changeButton setHidden:NO];
        if(self.trackReferenceNumberButton.isHidden == YES){//BUG 16330
            
            [self removeConstraint:self.dispatchDetailConstraintTop];
            [self removeConstraint:self.dispatchDetailBottomConstraint];
            [self removeConstraint:self.dispatchButtonBottomConstraint];
             [self removeConstraint:self.changeButtonBottomConstraint];
             [self removeConstraint:self.changButtonTopConstraint];
             [self setNeedsLayout];
            
            
            NSLayoutConstraint* topCons = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.changeButton attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0.0f];
            [self addConstraint:topCons];
            
            NSLayoutConstraint* bottomCons = [NSLayoutConstraint constraintWithItem:self.nodeDescriptionLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.changeButton attribute:NSLayoutAttributeTop multiplier:1.0f constant:0.0f];
            [self addConstraint:bottomCons];
            
            
            NSLayoutConstraint* bottomCons1 = [NSLayoutConstraint constraintWithItem:self.nodeTitleLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.nodeDescriptionLabel attribute:NSLayoutAttributeTop multiplier:1.0f constant:-5.0f];
            [self addConstraint:bottomCons1];
            //self.backgroundColor = [UIColor lightGrayColor];
           
        }
    } else {
        [self.changeButton setHidden:YES];
    }
    
    if (isFirstNode) {
        self.topDisabledLineView.hidden = YES;
        self.topEnabledLineView.hidden = YES;
    }
    if (isLastNode) {
        self.bottomDisabledLineView.hidden = YES;
        self.bottomEnabledLineView.hidden = YES;
    }
    if (nodeData.isFirstActiveNode) {
        //self.nodeCircleOverlayView.hidden = NO;
        self.nodeCircleOverlayView.hidden = YES;
    } else {
        self.nodeCircleOverlayView.hidden = YES;
    }
    if (nodeData.isActiveNode) {
        if (nodeData.isFirstActiveNode) {
            //self.nodeCircleImageView.image = [UIImage imageNamed:@"empty_circle_enabled"];
            self.nodeCircleImageView.image = [UIImage imageNamed:@"greenTick"];
            self.bottomEnabledLineView.hidden = YES;
            self.topDisabledLineView.hidden = YES;
        } else {
            self.nodeCircleImageView.image = [UIImage imageNamed:@"empty_circle_disabled"];
            self.bottomEnabledLineView.hidden = YES;
            self.topEnabledLineView.hidden = YES;
            self.nodeDescriptionLabel.textColor = [BrandColours colourBtNeutral70];
            self.dispatchDetailsLabel.textColor = [BrandColours colourBtNeutral70];
            self.nodeTitleLabel.textColor = [BrandColours colourBtNeutral70];
            self.dateLabel.textColor = [BrandColours colourBtNeutral70];
        }
    } else {
        //self.nodeCircleImageView.image = [UIImage imageNamed:@"filled_circle"];
        self.nodeCircleImageView.image = [UIImage imageNamed:@"greenTick"];
        self.bottomDisabledLineView.hidden = YES;
        self.topDisabledLineView.hidden = YES;
    }
    [self setNeedsLayout];
}

- (IBAction)changeButtonAction:(id)sender {
    [self.delegate userPressedChangeButtonOnOrderMilestoneNodeView:self];
}

- (IBAction)trackingReferenceNumberButtonAction:(id)sender {
    [self.delegate orderMilestoneNodeView:self userPressedTrackDeliveryButtonWithTrackReference:_dispatchDetailsModel.trackingReferenceNumber andTrackingURL:_dispatchDetailsModel.trackingReferenceUrl];
}

- (void)showDispatchDetails
{
//    CGFloat remainingWidth = [self getRemainingWidthForTrackingReferenceNumberButton];
//    CGFloat requiredWidth = [self.trackReferenceNumberButton.titleLabel.text boundingRectWithSize:self.trackReferenceNumberButton.titleLabel.frame.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:([UIFont fontWithName:kBtFontBold size:13]) } context:nil].size.width + 8;
//
//    if (requiredWidth > remainingWidth)
//    {
//        self.constraintTrackReferenceNumberButtonTop.constant = 18;
//        self.constraintTrackReferenceNumberButtonLeading.constant = 0;
//    }
    self.constraintTrackReferenceNumberButtonHeight.constant = 19;
    [self setNeedsLayout];
}

- (void)hideDispatchDetails
{
    self.constraintTrackReferenceNumberButtonHeight.constant = 0;
    self.trackReferenceNumberButton.hidden = YES;
    self.dispatchDetailsLabel.text = @"";
    [self removeConstraint:self.dispatchDetailConstraintTop];
    [self removeConstraint:self.dispatchDetailBottomConstraint];
    [self removeConstraint:self.dispatchButtonBottomConstraint];
    [self removeConstraint:self.changeButtonBottomConstraint];
    [self removeConstraint:self.changButtonTopConstraint];
    [self setNeedsLayout];
    
    
    NSLayoutConstraint* topCons = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.changeButton attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0.0f];
    [self addConstraint:topCons];
    
    NSLayoutConstraint* bottomCons = [NSLayoutConstraint constraintWithItem:self.nodeDescriptionLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.changeButton attribute:NSLayoutAttributeTop multiplier:1.0f constant:0.0f];
    [self addConstraint:bottomCons];
    
    
    NSLayoutConstraint* bottomCons1 = [NSLayoutConstraint constraintWithItem:self.nodeTitleLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.nodeDescriptionLabel attribute:NSLayoutAttributeTop multiplier:1.0f constant:-5.0f];
    [self addConstraint:bottomCons1];
    [self setNeedsLayout];
}

- (CGFloat)getRemainingWidthForTrackingReferenceNumberButton
{
    CGFloat xDispatchLabel = self.dispatchDetailsLabel.frame.origin.x;
    CGFloat totalOccupiedWidth = xDispatchLabel + kOriginalDispatchDetailsLabelWidth;
    CGFloat remainingWidth = [UIScreen mainScreen].bounds.size.width - (totalOccupiedWidth + 20);
    
    return remainingWidth;
}

@end
