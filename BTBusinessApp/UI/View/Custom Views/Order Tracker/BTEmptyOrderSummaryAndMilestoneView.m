//
//  BTEmptyOrderSummaryAndMilestoneView.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 3/3/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTEmptyOrderSummaryAndMilestoneView.h"
#import "BrandColours.h"

@interface BTEmptyOrderSummaryAndMilestoneView () {
    
}

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (strong, nonatomic) IBOutlet UIButton *needHelpButton;


@end

@implementation BTEmptyOrderSummaryAndMilestoneView

- (void)drawRect:(CGRect)rect {
    // Drawing code
    self.needHelpButton.backgroundColor = [UIColor clearColor];
    self.needHelpButton.layer.borderColor = [BrandColours colourBackgroundBTPurplePrimaryColor].CGColor;
    self.needHelpButton.layer.borderWidth = 1.0f;
    self.needHelpButton.layer.cornerRadius = 5.0f;
}

- (void)updateWithMessageString:(NSString *)messageString
{
    _messageLabel.text = messageString;
}

- (IBAction)needHelpGetInTouchButtonAction:(id)sender
{
    [self.emptyOrderSummaryAndMilestoneViewDelegate userPressedNeedHelpButtonOnEmptyOrderSummaryAndMilestoneView:self];
}


@end
