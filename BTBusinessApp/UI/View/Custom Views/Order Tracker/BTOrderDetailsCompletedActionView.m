//
//  BTOrderDetailsCompletedActionView.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 10/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderDetailsCompletedActionView.h"
#import "BrandColours.h"

@implementation BTOrderDetailsCompletedActionView


- (void)drawRect:(CGRect)rect {
    // Drawing code
    self.contactUsButton.backgroundColor = [UIColor clearColor];
    self.contactUsButton.titleLabel.textColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
    self.contactUsButton.layer.borderColor = [BrandColours colourBackgroundBTPurplePrimaryColor].CGColor;
    self.contactUsButton.layer.borderWidth = 1.0f;
    self.contactUsButton.layer.cornerRadius = 5.0f;

}

- (IBAction)contactUsButtonAction:(id)sender {
    [self.orderDetailsCompletedActionViewDelegate userClickedOnContactUsButtonOfOrderDetailsCompletedActionView:self];
}


@end
