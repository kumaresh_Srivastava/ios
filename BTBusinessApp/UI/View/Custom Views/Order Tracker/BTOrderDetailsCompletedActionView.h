//
//  BTOrderDetailsCompletedActionView.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 10/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BTOrderDetailsCompletedActionView;

@protocol BTOrderDetailsCompletedActionViewDelegate <NSObject>

- (void)userClickedOnContactUsButtonOfOrderDetailsCompletedActionView:(BTOrderDetailsCompletedActionView *)orderDetailsCompletedActionView;

@end

@interface BTOrderDetailsCompletedActionView : UIView {
    
}

@property (weak, nonatomic) id <BTOrderDetailsCompletedActionViewDelegate> orderDetailsCompletedActionViewDelegate;
@property (weak, nonatomic) IBOutlet UIButton *contactUsButton;

@end
