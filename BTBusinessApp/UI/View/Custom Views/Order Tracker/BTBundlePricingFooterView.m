//
//  BTBundlePricingFooterView.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 14/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBundlePricingFooterView.h"

@implementation BTBundlePricingFooterView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)updateViewWithPriceValue:(double)priceValue withPriceTypeLabel:(NSString *)priceTypeLabel
{
    self.priceValueLabel.text = [NSString stringWithFormat:@"£%.2f", priceValue];
    self.priceLabel.text = priceTypeLabel;
}

@end
