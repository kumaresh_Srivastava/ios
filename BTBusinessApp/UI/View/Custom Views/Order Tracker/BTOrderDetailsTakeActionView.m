//
//  BTOrderDetailsTakeActionView.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 09/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderDetailsTakeActionView.h"
#import "CustomSpinnerAnimationView.h"

#define kOriginalHeaderHeight 40;
#define kOriginalAmendOrderHeight 55;
#define kOriginalAddAppointmentToCalenderHeight 55;
#define kOriginalNotificationsHeight 90;
#define kOriginalNeedHelpHeight 50;

@interface BTOrderDetailsTakeActionView () {
    
    CustomSpinnerAnimationView *_loaderView;
}

@end

@implementation BTOrderDetailsTakeActionView

- (void)drawRect:(CGRect)rect {
    
    self.needHelpButton.backgroundColor = [UIColor clearColor];
    self.needHelpButton.titleLabel.textColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
    self.needHelpButton.layer.borderColor = [BrandColours colourBackgroundBTPurplePrimaryColor].CGColor;
    self.needHelpButton.layer.borderWidth = 1.0f;
    self.needHelpButton.layer.cornerRadius = 5.0f;
    
}

#pragma mark - Private Helper Methods

- (void)createLoaderView
{
    if (!_loaderView)
    {
        _loaderView = [[CustomSpinnerAnimationView alloc] initWithText:@"" withLoadingImage:[UIImage imageNamed:@"Spinner"]];
        _loaderView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self addSubview:_loaderView];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:-80.0f]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0f constant:0.0f]];

    }
}

- (void)hideLoaderView
{
    [_loaderView setHidden:YES];
    [_loaderView removeFromSuperview];
    _loaderView = nil;
}

#pragma mark - Action Methods

- (IBAction)amendOrderDetailsButtonAction:(id)sender {
    [self.delegate userPressedAmendOrderDetailsOfTakeActionView:self];
}

- (IBAction)addAppointmentToCalender:(id)sender {
    [self.delegate userPressedAddAppointmentToCalenderOfTakeActionView:self];
}

- (IBAction)contactUsButtonAction:(id)sender {
    [self.delegate userPressedContactUsButtonOfTakeActionView:self];
}


#pragma mark - UI Handling Methods

- (void)hideAddAppointmentToCalenderView {
    self.constraintAddAppointmentToCalenderViewHeight.constant = 0;
    self.addAppointmentToCalenderView.hidden = YES;
    //[self layoutIfNeeded];
}

- (void)showAddAppointmentToCalenderView {
    self.constraintAddAppointmentToCalenderViewHeight.constant = kOriginalAddAppointmentToCalenderHeight;
    self.addAppointmentToCalenderView.hidden = NO;
    [self layoutIfNeeded];
}

- (void)hideAmendOrderDetailsView {
    self.constraintAmendOrderDetailsViewHeight.constant = 0;
    self.amendOrderDetailsView.hidden = YES;
    //[self layoutIfNeeded];
}

- (void)showAmendOrderDetailsView {
    self.constraintAmendOrderDetailsViewHeight.constant = kOriginalAmendOrderHeight;
    self.amendOrderDetailsView.hidden = NO;
    [self layoutIfNeeded];
}

- (void)hideNotificationsView {
    self.constraintOrderNotificationsViewHeight.constant = 0;
    self.notificationsView.hidden = YES;
    //[self layoutIfNeeded];
}

- (void)showNotificationsView {
    self.constraintOrderNotificationsViewHeight.constant = kOriginalNotificationsHeight;
    self.notificationsView.hidden = NO;
    [self layoutIfNeeded];
}

- (void)hideNeedHelpbutton {
    self.constraintNeedHelpButtonHeight.constant = 0;
    self.needHelpButton.hidden = YES;
    //[self layoutIfNeeded];
}

- (void)showNeedHelpbutton {
    self.constraintNeedHelpButtonHeight.constant = kOriginalNeedHelpHeight;
    self.needHelpButton.hidden = NO;
    [self layoutIfNeeded];
}

- (void)hideAllTakeActionViewItems {
    
    self.constraintHeaderViewHeight.constant = 0;
    self.headerView.hidden = YES;
    
    [self hideAmendOrderDetailsView];
    [self hideAddAppointmentToCalenderView];
    [self hideNotificationsView];
    
    [_loaderView startAnimation];
}

- (void)showAllTakeActionViewItems {
    
    self.constraintHeaderViewHeight.constant = kOriginalHeaderHeight;
    self.headerView.hidden = NO;
    
    [self showAmendOrderDetailsView];
    [self showAddAppointmentToCalenderView];
    [self showNotificationsView];
    
    [self layoutIfNeeded];
}

- (void)startLoaderView
{
    self.constraintHeaderViewHeight.constant = 60;
    [self createLoaderView];
    [_loaderView startAnimation];
}

- (void)stopLoaderView
{
    self.constraintHeaderViewHeight.constant = 0;
    [self hideLoaderView];
    [_loaderView stopAnimation];
}

@end
