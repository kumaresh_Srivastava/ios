//
//  BTOrderDashBoardTableFooterView.h
//  BTBusinessApp
//
//  Created by Accolite on 22/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomSpinnerAnimationView.h"

@class BTOrderDashBoardTableFooterView;

@protocol BTOrderDashBoardFooterDelegate <NSObject>

@required

- (void) orderDashBoardTableFooterView:(BTOrderDashBoardTableFooterView *)orderDashBoardFooterView retryBUttonActionMethod:(id)sender;
@end


@interface BTOrderDashBoardTableFooterView : UIView {
    CustomSpinnerAnimationView *footerView;
}
@property (weak, nonatomic) IBOutlet UIView *loadingVIewbackground;

@property (weak, nonatomic) id<BTOrderDashBoardFooterDelegate> orderDashBoardFooterDelegate;
@property (weak, nonatomic) IBOutlet UIView *retryView;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet UIButton *retryButton;

- (void)createCustomLoadingIndicator;
- (void) hideCustomLoadingIndicator;
- (void) showCustomLoadingIndicator;
- (void) startLoadingIndicatorAnimation;
- (void) stopCustomLoadingIndicatorAnimation;
- (void) hideRetryView;
- (void) showRetryView;
- (void)showRetryViewWithErrorMessage:(NSString *)error;
@end
