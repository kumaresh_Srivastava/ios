//
//  BTAssetServiceDetailHeaderView.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/9/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTAssetServiceDetailHeaderView.h"
#import "NSObject+APIResponseCheck.h"
#import "BTSegmentedControl.h"

#define kPhoneImage @"phoneline"
#define kBoradbandImage @"boradband"
#define kOtherImage @""


@interface BTAssetServiceDetailHeaderView()
@property (weak, nonatomic) IBOutlet UILabel *serviceNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceNumberLabel;


@end

@implementation BTAssetServiceDetailHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib
{
    [super awakeFromNib];
    //[_assetServiceDetailSegmentedControl setTintColor:[BrandColours colourBackgroundBTPurplePrimaryColor]];
}


//- (IBAction)segmendrControllChanged:(id)sender {
//    
//    if([self.delegate respondsToSelector:@selector(btAssetServiceDetailHeaderView:didSelectedSegmentAtIndex:)]){
//        
//        [self.delegate btAssetServiceDetailHeaderView:self didSelectedSegmentAtIndex:[(UISegmentedControl *)sender selectedSegmentIndex]];
//    }
//}

- (IBAction)segmentValueChanged:(id)sender {
    if([self.delegate respondsToSelector:@selector(btAssetServiceDetailHeaderView:didSelectedSegmentAtIndex:)]){
        
        [self.delegate btAssetServiceDetailHeaderView:self didSelectedSegmentAtIndex:[(BTSegmentedControl *)sender selectedSegmentIndex]];
    }
}

- (void)updateWithAssetsDetailCollection:(BTAssetDetailCollection *)collectionModel withAssetType:(NSString*)assetType{
    
    //Service name label is updated with the text "Service ID:"
    
//    if(collectionModel.serviceText && [collectionModel.serviceText validAndNotEmptyStringObject]){
//
//        self.serviceNameLabel.text = collectionModel.serviceText;
//    }
//    else{
//
//        self.serviceNameLabel.text = nil;
//    }
    
    
    if(collectionModel.serviceNumber && [collectionModel.serviceNumber validAndNotEmptyStringObject]){
        
        self.serviceNumberLabel.text = collectionModel.serviceNumber;
    }
    else{
        
        self.serviceNumberLabel.text = nil;
    }
}


- (UIImage *)getImageForAsset:(NSString *)assetName
{
    
    if([[assetName lowercaseString] containsString:@"phone"])
    {
        return  [UIImage imageNamed:@"phoneline"];
    }
    else if([[assetName lowercaseString] containsString:@"broadband"])
    {
        return   [UIImage imageNamed:@"broadband"];
    }
    else if([[assetName lowercaseString] containsString:@"mobile"])
    {
        return [UIImage imageNamed:@"mobile"];
    }
    else
    {
        return [UIImage imageNamed:@"bundle"];
    }
    
}




@end
