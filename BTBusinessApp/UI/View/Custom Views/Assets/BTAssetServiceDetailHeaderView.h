//
//  BTAssetServiceDetailHeaderView.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/9/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTAssetDetailCollection.h"

@class BTAssetServiceDetailHeaderView;

@protocol BTAssetServiceDetailHeaderViewDelegate <NSObject>

- (void)btAssetServiceDetailHeaderView:(BTAssetServiceDetailHeaderView *)view didSelectedSegmentAtIndex:(NSInteger)index;

@end

@interface BTAssetServiceDetailHeaderView : UIView

@property(nonatomic, assign) id<BTAssetServiceDetailHeaderViewDelegate>delegate;
@property (weak, nonatomic) IBOutlet UISegmentedControl *assetServiceDetailSegmentedControl;
- (IBAction)segmentValueChanged:(id)sender;


- (void)updateWithAssetsDetailCollection:(BTAssetDetailCollection *)collectionModel withAssetType:(NSString*)assetType;
@end
