//
//  ViewAssetBACOverviewTableHeaderView.m
//  BTBusinessApp
//
//  Created by vectoscalar on 08/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "ViewAssetBACOverviewTableHeaderView.h"

#define kEuroSymbol @"£"

@implementation ViewAssetBACOverviewTableHeaderView
@synthesize delegate;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
 
*/
- (void)drawRect:(CGRect)rect {
    
    // Drawing code
    [self customizeViewLatestBillButton];
    
}


- (void)customizeViewLatestBillButton
{
    self.viewLatestBillButton.layer.cornerRadius = 4.0f;
    self.viewLatestBillButton.layer.borderColor = [BrandColours colourTextBTPurplePrimaryColor].CGColor;
    self.viewLatestBillButton.layer.borderWidth = 1.0f;
    self.viewLatestBillButton.backgroundColor = [UIColor colorWithRed:100/255.0 green:0/255.0 blue:170/255.0 alpha:1.0];
}

- (void)updateDataWithAsset:(BTAsset *)asset
{
    self.accountLabel.text = asset.billingAccountNumber;
    
    if(asset.lastBillAmount)
    {
        self.amountLabel.text = [NSString stringWithFormat:@"%@%@",kEuroSymbol,asset.lastBillAmount];
    }
    else
    {
        self.amountLabel.text = @"-";
    }
    
}


- (IBAction)userPressedViewLatesetBullButton:(id)sender {
    
    [delegate userPressedViewLatestBill];
}

- (void)hideViewBillButton
{
    self.viewBillHeightConstraint.constant = 0;
    self.viewBillButtonBottomConstraint.constant = -3;
    self.viewLatestBillButton.hidden = YES;

}

@end
