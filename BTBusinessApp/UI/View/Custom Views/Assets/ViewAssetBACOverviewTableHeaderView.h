//
//  ViewAssetBACOverviewTableHeaderView.h
//  BTBusinessApp
//
//  Created by vectoscalar on 08/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTAsset.h"
#import "BTAssetModel.h"

@class ViewAssetBACOverviewTableHeaderView;

@protocol ViewAsserBACObverviewTableHeaderDelegate <NSObject>

- (void)userPressedViewLatestBill;

@end


@interface ViewAssetBACOverviewTableHeaderView : UIView

@property (weak, nonatomic) IBOutlet UILabel *accountLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UIButton *viewLatestBillButton;

@property (assign,nonatomic) id <ViewAsserBACObverviewTableHeaderDelegate> delegate;

- (IBAction)userPressedViewLatesetBullButton:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewBillHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewBillButtonBottomConstraint;

- (void)updateDataWithAsset:(BTAsset*)asset;

@end
