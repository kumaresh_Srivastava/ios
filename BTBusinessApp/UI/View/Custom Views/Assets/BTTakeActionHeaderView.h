//
//  TakeActionHeaderView.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/9/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTTakeActionHeaderView : UIView

@property (weak, nonatomic) IBOutlet UILabel *takeActionLabel;

- (void)updateLabelColorWithColor:(UIColor *)color;
- (void)updateLabelTitle:(NSString *)title;
@end
