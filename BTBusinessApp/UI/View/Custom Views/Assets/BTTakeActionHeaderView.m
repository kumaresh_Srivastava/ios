//
//  TakeActionHeaderView.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/9/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTTakeActionHeaderView.h"

@implementation BTTakeActionHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)updateLabelColorWithColor:(UIColor *)color
{
    self.takeActionLabel.textColor = color;
}

- (void)updateLabelTitle:(NSString *)title
{
    self.takeActionLabel.text = title;
}

@end
