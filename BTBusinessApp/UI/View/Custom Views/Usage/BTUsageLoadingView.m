//
//  BTUsageLoadingView.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 02/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTUsageLoadingView.h"
#import "CustomSpinnerAnimationView.h"

@interface BTUsageLoadingView () {
    CustomSpinnerAnimationView *_indicatorView;
}

@property(atomic) BOOL isAnimationInProgress;

@end

@implementation BTUsageLoadingView

- (void)awakeFromNib{

    [super awakeFromNib];

    _indicatorView = [[CustomSpinnerAnimationView alloc] initWithText:@"" withLoadingImage:[UIImage imageNamed:@"Spinner"]];
    _indicatorView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.spinnerView addSubview:_indicatorView];

    NSDictionary *indicstorViews = @{@"indicatorView":_indicatorView};
    [self.spinnerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-12-[indicatorView]-|" options:0 metrics:nil views:indicstorViews]];
    [self.spinnerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-12-[indicatorView]|" options:0 metrics:nil views:indicstorViews]];
    [self startAnimatingLoadingIndicatorView];


}


- (void)startAnimatingLoadingIndicatorView {

    if(self.isAnimationInProgress){
        return;
    }

    [_indicatorView setIsStopped:NO];
    self.isAnimationInProgress = YES;
    [_indicatorView startAnimation];
}

- (void)stopAnimatingLoadingIndicatorView {
    [_indicatorView setIsStopped:YES];
    self.isAnimationInProgress = NO;
    [_indicatorView stopAnimation];
}


@end
