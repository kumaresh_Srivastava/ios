//
//  BTUsageLoadingView.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 02/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTUsageLoadingView : UIView
@property (weak, nonatomic) IBOutlet UIView *spinnerView;

- (void)startAnimatingLoadingIndicatorView;
- (void)stopAnimatingLoadingIndicatorView;

@end
