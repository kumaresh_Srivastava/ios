//
//  BTAssetServiceDetailHeaderView.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/9/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBroadbandUsageHeaderView.h"
#import "NSObject+APIResponseCheck.h"

#define kBoradbandImage @"boradband"
#define kOtherImage @""


@interface BTBroadbandUsageHeaderView()
@property (weak, nonatomic) IBOutlet UILabel *serviceNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceNumberLabel;
@property (weak, nonatomic) IBOutlet UIImageView *assetImageView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *broadbandUsageHeaderViewSegmentedControl;


@end

@implementation BTBroadbandUsageHeaderView


- (void)awakeFromNib{
    
    [super awakeFromNib];
    //_broadbandUsageHeaderViewSegmentedControl.tintColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
    //self.serviceNameLabel.text = @"-";
    //self.serviceNumberLabel.text = @"Service ID: -";

    UIFont *font = [UIFont fontWithName:@"BTFont-Regular" size:16.0];
    
    NSDictionary* selectedAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[BrandColours colourBtWhite], NSForegroundColorAttributeName, font, NSFontAttributeName, nil];
    NSDictionary* normalAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[BrandColours colourBtLightBlack], NSForegroundColorAttributeName, font, NSFontAttributeName, nil];
    
    UIImage *unhighlightedImage = [[UIImage imageNamed:@"Segmented_Control_Left"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
    UIImage *highlightedImage = [[UIImage imageNamed:@"Segmented_Control_Right"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
    
    [_broadbandUsageHeaderViewSegmentedControl setTintColor:UIColor.clearColor];
    [_broadbandUsageHeaderViewSegmentedControl setBackgroundImage:unhighlightedImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [_broadbandUsageHeaderViewSegmentedControl setBackgroundImage:highlightedImage forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [_broadbandUsageHeaderViewSegmentedControl setDividerImage:[UIImage imageNamed:@"blank_divider"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [_broadbandUsageHeaderViewSegmentedControl setTitleTextAttributes:selectedAttributes forState:UIControlStateSelected];
    [_broadbandUsageHeaderViewSegmentedControl setTitleTextAttributes:normalAttributes forState:UIControlStateNormal];
}


- (IBAction)segmendrControllChanged:(id)sender {
 
   
    if([self.delegate respondsToSelector:@selector(btBroadbandUsageHeaderView:didSelectedSegmentAtIndex:)]){
                [self.delegate btBroadbandUsageHeaderView:self didSelectedSegmentAtIndex:[(UISegmentedControl *)sender selectedSegmentIndex]];
    }
}

- (void)updateWithProducName:(NSString *)productName serviceID:(NSString *)serviceID andAlertStatus:(NSInteger)alertStatus{
    
    //self.serviceNameLabel.text = productName;
    self.serviceNumberLabel.text = serviceID;
    
    //broadband
    
    if(alertStatus == 2){
        
        self.assetImageView.image = [UIImage imageNamed:@"broadband_usage_orange"];
    }
    else if(alertStatus == 3){
        
        self.assetImageView.image = [UIImage imageNamed:@"broadband_usage_red"];
    }
    else{
        
        self.assetImageView.image = [UIImage imageNamed:@"broadband"];
    }
}





@end
