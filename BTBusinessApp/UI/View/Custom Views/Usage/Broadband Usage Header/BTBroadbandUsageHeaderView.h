//
//  BTAssetServiceDetailHeaderView.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/9/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTBroadbandUsageHeaderView;

@protocol BTBroadbandUsageHeaderViewDelegate <NSObject>

- (void)btBroadbandUsageHeaderView:(BTBroadbandUsageHeaderView *)view didSelectedSegmentAtIndex:(NSInteger)index;

@end

@interface BTBroadbandUsageHeaderView : UIView

@property(nonatomic, assign) id<BTBroadbandUsageHeaderViewDelegate>delegate;

- (void)updateWithProducName:(NSString *)productName serviceID:(NSString *)serviceID andAlertStatus:(NSInteger)alertStatus;

@end
