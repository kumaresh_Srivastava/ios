//
//  PasswordValidationView.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/17/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMUpdatePasswordScreen.h"

@interface PasswordValidationView : UIView

- (void)updateWithUdateModel:(DLMUpdatePasswordScreen *)viewModel andInputText:(NSString *)inputText;

@end
