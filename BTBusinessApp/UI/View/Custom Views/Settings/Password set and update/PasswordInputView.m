//
//  PasswordInputView.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/17/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "PasswordInputView.h"
#import "AppConstants.h"
#import "BrandColours.h"
#import "UITextField+BTTextFieldProperties.h"

#define kGapBetweenContols 7.0

@interface PasswordInputView()<UITextFieldDelegate>{
    UITapGestureRecognizer *passwordViewTapGesture;
}
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *errorTextLabel;

@end

@implementation PasswordInputView


- (void)awakeFromNib{
    [super awakeFromNib];
    
    [self configureUI];
}


- (void)configureUI{
    
    _textField.delegate = self;
    _errorTextLabel.text = nil;
    
    [self.textField getBTTextFieldStyle];
    
    //Adding Event on text chenge
    [self.textField addTarget:self
                       action:@selector(textFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
    
    _titleLabel.textColor = [BrandColours colourBtNeutral90];
    _errorTextLabel.textColor = [BrandColours colourMyBtRed];
    
    _textField.rightView = [self getRightView];
    _textField.rightViewMode = UITextFieldViewModeAlways;
    //[_textField setTintColor:[BrandColours colourBtViolet]];
}



- (UIView *)getRightView{
    
    //icon_password_eye_gray
    
    UIImage *eyeImage = [UIImage imageNamed:@"icon_password_eye_gray"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:eyeImage forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"icon_password_eye_purple.png"] forState:UIControlStateSelected];
    
    
    button.frame = CGRectMake(0.0,
                              0.0,
                              eyeImage.size.width + kGapBetweenContols,
                              eyeImage.size.height + 5.0);
    
    button.imageEdgeInsets = UIEdgeInsetsMake(0,
                                              0,
                                              0,
                                              kGapBetweenContols);
    [button addTarget:self action:@selector(showTextButtonPressed:) forControlEvents:UIControlEventTouchUpInside];

    return button;
}


/*
- (UIImageView *)getCheckMarkView{
    
    
    UIImage *checkMarkImage = [UIImage imageNamed:@"checkmark_green"];
    
    UIImageView *checkMarkImageView = [[UIImageView alloc] init];
    checkMarkImageView.frame = CGRectMake(0,
                                          0,
                                          checkMarkImage.size.width,
                                          checkMarkImage.size.height);
    checkMarkImageView.image = checkMarkImage;

    return checkMarkImageView;
}



- (UIButton *)getHelpButton{
    
    UIImage *eyeImage = [UIImage imageNamed:@"question"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:eyeImage forState:UIControlStateNormal];

    button.frame = CGRectMake(0,
                              0.0,
                              eyeImage.size.width,
                              eyeImage.size.height);
    
    button.imageEdgeInsets = UIEdgeInsetsMake(0,
                                              0,
                                              0,
                                              kGapBetweenContols);
    [button addTarget:self action:@selector(helpButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
    
}
 */


- (void)setTitle:(NSString *)titleText{
    
    _titleLabel.text = titleText;
}


- (NSString *)getInputText{
    
    return _textField.text;
}


- (void)resignFirstResponderForTextfield{
    
    [_textField resignFirstResponder];
}

- (void)becomeFirstResponderForTextfield {
    [_textField becomeFirstResponder];
}

- (BOOL)textfieldIsFirstResponder {
    return [_textField isFirstResponder];
}


- (void)addEyeButton{
    
    _textField.rightView = [self getRightView];
}


- (void)textFieldClear {

    _textField.text = @"";
}
/*
- (void)addCheckMarkAndEyeButton{
    
    UIView *rightView = [[UIView alloc] init];
    rightView.backgroundColor = [UIColor clearColor];
    
    
    UIImageView *checkMarkView = [self getCheckMarkView];
    [rightView addSubview:checkMarkView];
    
    
    
    
    UIView *buttonView = [self getRightView];
    [rightView addSubview:buttonView];
    
    buttonView.frame = CGRectMake(checkMarkView.frame.origin.x + checkMarkView.frame.size.width + kGapBetweenContols,
                              0,
                              buttonView.frame.size.width,
                              buttonView.frame.size.height);
    
    rightView.frame = CGRectMake(0,
                                 0,
                                 buttonView.frame.origin.x + buttonView.frame.size.width,
                                 buttonView.frame.size.height);
    
    checkMarkView.frame = CGRectMake(checkMarkView.frame.origin.x,
                                     rightView.frame.size.height/2 - checkMarkView.frame.size.height/2,
                                     checkMarkView.frame.size.width,
                                     checkMarkView.frame.size.height);
    
    _textField.rightView = rightView;

}



- (void)addCheckMarkAndHelpRightView{
    
    
    UIView *rightView = [[UIView alloc] init];
    rightView.backgroundColor = [UIColor clearColor];
    

    UIImageView *checkMarkView = [self getCheckMarkView];
    [rightView addSubview:checkMarkView];
    
    
    

    UIButton *button = [self getHelpButton];
    [rightView addSubview:button];
    
    button.frame = CGRectMake(checkMarkView.frame.origin.x + checkMarkView.frame.size.width + kGapBetweenContols,
                              0,
                              button.frame.size.width + kGapBetweenContols,
                              button.frame.size.height + 5);
    
    rightView.frame = CGRectMake(0,
                                 0,
                                 checkMarkView.frame.size.width + button.frame.size.width + kGapBetweenContols + 5,
                                 button.frame.size.height);
    
    checkMarkView.frame = CGRectMake(checkMarkView.frame.origin.x,
                                     rightView.frame.size.height/2 - checkMarkView.frame.size.height/2,
                                     checkMarkView.frame.size.width,
                                     checkMarkView.frame.size.height);
    
    _textField.rightView = rightView;
    
    
}

 */

/*
- (void)addHelpButton{
    
    
    UIButton *button = [self getHelpButton];
    
    button.imageEdgeInsets = UIEdgeInsetsMake(0,
                                              0,
                                              0,
                                              kGapBetweenContols + 5);
    
    button.frame = CGRectMake(0,
                              0,
                              button.frame.size.width + kGapBetweenContols + 5,
                              button.frame.size.height + 5);

    
    _textField.rightView = button;
    
}

*/

/*
- (void)addCheckMarkImage{
    
    UIImageView *checkMarkView = [self getCheckMarkView];
    
    UIView *rightView = [[UIView alloc] init];
    rightView.backgroundColor = [UIColor clearColor];
    [rightView addSubview:checkMarkView];
    
    rightView.frame = CGRectMake(0,
                                 0,
                                 checkMarkView.frame.size.width + kGapBetweenContols + 5,
                                 checkMarkView.frame.size.height + 5);
    
    _textField.rightView = rightView;
}
 */


- (void)removeRightView{
    
    _textField.rightView = nil;
}


- (void)showErrorWithMessage:(NSString *)errorMessage{
    
    _errorTextLabel.text = errorMessage;
    _textField.layer.borderColor = [BrandColours colourMyBtRed].CGColor;
    _textField.layer.borderWidth = 1.0;
    _titleLabel.textColor = [BrandColours colourMyBtRed];
}

- (void)updateViewForNormalState
{
    _errorTextLabel.text = @"";
    _titleLabel.textColor = [BrandColours colourBtNeutral90];
    [self.textField updateWithEnabledState];
    [self.delegate passwordInputView:self didChangeWithText:_textField.text];
}

#pragma mark- Action Method

- (void)showTextButtonPressed:(id)sender{
    
    UIButton *button = (UIButton *)sender;
    button.selected = !button.selected;
    
    _textField.secureTextEntry = !_textField.isSecureTextEntry;
    if(self.keybaordVisibilityStatus) {
         [_textField becomeFirstResponder];
    }
}


- (void)helpButtonPressed:(id)sender{
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(needHelpButtonPressedOnPasswordInputView:)]){
        
        [self.delegate needHelpButtonPressedOnPasswordInputView:self];
    }
}



#pragma mark- UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self updateViewForNormalState];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.textField updateWithDisabledState];
}

#pragma mark - TextChange Event

- (void)textFieldDidChange:(UITextField *)textField{
    
    [self updateViewForNormalState];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self endEditing:YES];
    
    return YES;
}


@end
