
//
//  PasswordInputView.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/17/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PasswordInputView;

@protocol PasswordInputViewDelegate <NSObject>

- (void)passwordInputView:(PasswordInputView *)view didChangeWithText:(NSString *)inputTextString;

@optional

- (void)needHelpButtonPressedOnPasswordInputView:(PasswordInputView *)view;

@end

@interface PasswordInputView : UIView
@property(nonatomic, assign) id<PasswordInputViewDelegate>delegate;
@property(assign) BOOL keybaordVisibilityStatus;

- (void)setTitle:(NSString *)titleText;
- (NSString *)getInputText; 
- (void)resignFirstResponderForTextfield;
- (void)becomeFirstResponderForTextfield;
- (BOOL)textfieldIsFirstResponder;
- (void)showErrorWithMessage:(NSString *)errorMessage;
- (void)removeRightView;
- (void)textFieldClear;
@end
