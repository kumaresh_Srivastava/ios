//
//  PasswordValidationView.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/17/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "PasswordValidationView.h"
#import "NSObject+APIResponseCheck.h"
#import "BrandColours.h"

@interface PasswordValidationView()
@property (weak, nonatomic) IBOutlet UILabel *characterLengthLabel;
@property (weak, nonatomic) IBOutlet UILabel *oneUppercaseConditionLabel;
@property (weak, nonatomic) IBOutlet UILabel *oneLowercaseConditionLabel;
@property (weak, nonatomic) IBOutlet UILabel *oneNumberConditionLabel;

@end

@implementation PasswordValidationView

- (void)updateWithUdateModel:(DLMUpdatePasswordScreen *)viewModel andInputText:(NSString *)inputText{
    
    
    //Character Length
    if([inputText validAndNotEmptyStringObject] && [viewModel isLengthConditionTrueForText:inputText]){
        
        _characterLengthLabel.attributedText = [self getAttributtedStringForString:_characterLengthLabel.text];
        _characterLengthLabel.textColor = [BrandColours colourMyBtGreen];
    }
    else{
        
        _characterLengthLabel.text = _characterLengthLabel.text;
        _characterLengthLabel.textColor = [UIColor lightGrayColor];
    }
    
    
    //Atleasr one upper case
    if([inputText validAndNotEmptyStringObject] && [viewModel isUppercaseConditionTrueForText:inputText]){
        
        _oneUppercaseConditionLabel.attributedText = [self getAttributtedStringForString:_oneUppercaseConditionLabel.text];
        _oneUppercaseConditionLabel.textColor = [BrandColours colourMyBtGreen];
    }
    else{
        
        _oneUppercaseConditionLabel.text = _oneUppercaseConditionLabel.text;
        _oneUppercaseConditionLabel.textColor = [UIColor lightGrayColor];
    }
    
    
    //Atleasr one lower case
    if([inputText validAndNotEmptyStringObject] && [viewModel isLowerConditionTrueForText:inputText]){
        
        _oneLowercaseConditionLabel.attributedText = [self getAttributtedStringForString:_oneLowercaseConditionLabel.text];
        _oneLowercaseConditionLabel.textColor = [BrandColours colourMyBtGreen];
    }
    else{
        
        _oneLowercaseConditionLabel.text = _oneLowercaseConditionLabel.text;
        _oneLowercaseConditionLabel.textColor = [UIColor lightGrayColor];
    }
    
    //Atleasr one number
    if([inputText validAndNotEmptyStringObject] && [viewModel isNumberConditionTrueForText:inputText]){
        
        _oneNumberConditionLabel.attributedText = [self getAttributtedStringForString:_oneNumberConditionLabel.text];
        _oneNumberConditionLabel.textColor = [BrandColours colourMyBtGreen];
    }
    else{
        
        _oneNumberConditionLabel.text = _oneNumberConditionLabel.text;
        _oneNumberConditionLabel.textColor = [UIColor lightGrayColor];
    }
    
    
}


- (NSAttributedString *)getAttributtedStringForString:(NSString *)inputString{
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:inputString];
    [attributeString addAttribute:NSStrikethroughStyleAttributeName
                            value:@2
                            range:NSMakeRange(5,2)];
    
    
    NSDictionary *attributes = @{
                                 NSStrikethroughStyleAttributeName: [NSNumber numberWithInt:NSUnderlineStyleSingle]
                                 };
    
  //  NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:inputString attributes:attributes];
    
    [attributeString addAttributes:attributes range:NSMakeRange(4,[attributeString length]-4)];
    
    return attributeString;

}



@end
