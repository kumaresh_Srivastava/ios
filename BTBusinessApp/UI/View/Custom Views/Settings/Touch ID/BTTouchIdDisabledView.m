//
//  BTTouchIdDisabledView.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 19/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTTouchIdDisabledView.h"

@implementation BTTouchIdDisabledView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)settingsButtonAction:(id)sender {
    [self.touchIdDisabledViewDelegate redirectToDeviceSettingsWithTouchIDDisbaledView:self];
}

- (IBAction)cancelButtonAction:(id)sender {
    
    [self.touchIdDisabledViewDelegate cancelledEnrollTouchIDWithTouchIdDisabledView:self];
}

@end
