//
//  BTTouchIdDisabledView.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 19/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTTouchIdDisabledView;

@protocol BTTouchIdDisabledViewDelegate <NSObject>

@required
- (void)redirectToDeviceSettingsWithTouchIDDisbaledView:(BTTouchIdDisabledView *)touchIdDisabledView;

- (void)cancelledEnrollTouchIDWithTouchIdDisabledView:(BTTouchIdDisabledView *)touchIdDisabledView;

@end

@interface BTTouchIdDisabledView : UIView

@property (nonatomic, weak) id <BTTouchIdDisabledViewDelegate> touchIdDisabledViewDelegate;

@end
