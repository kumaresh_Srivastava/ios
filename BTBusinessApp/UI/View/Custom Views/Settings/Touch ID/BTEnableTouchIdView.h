//
//  BTEnableTouchIdView.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 19/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTEnableTouchIdView;

@protocol BTEnableTouchIdViewDelegate <NSObject>

@required
- (void)enableTouchIdView:(BTEnableTouchIdView *)enableTouchIdView userWantsToProceedEnableTouchIdWith:(BOOL)enable;

@end

@interface BTEnableTouchIdView : UIView

@property (nonatomic, weak) id <BTEnableTouchIdViewDelegate> enableTouchIdViewDelegate;

@end
