//
//  BTEnableTouchIdView.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 19/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTEnableTouchIdView.h"

@implementation BTEnableTouchIdView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)okButtonAction:(id)sender {
    [self.enableTouchIdViewDelegate enableTouchIdView:self userWantsToProceedEnableTouchIdWith:YES];
}

- (IBAction)noThanksButtonAction:(id)sender {
    [self.enableTouchIdViewDelegate enableTouchIdView:self userWantsToProceedEnableTouchIdWith:NO];
}

@end
