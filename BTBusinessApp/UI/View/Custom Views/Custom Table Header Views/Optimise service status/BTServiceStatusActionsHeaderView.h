//
//  BTServiceStatusActionsHeaderView.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 2/22/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTServiceStatusActionsHeaderView : UIView

- (void)updateTitleLabelWithTitleText:(NSString *)titleLabel;

@end
