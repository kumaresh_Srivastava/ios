//
//  BTServiceStatusActionsHeaderView.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 2/22/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTServiceStatusActionsHeaderView.h"

@interface BTServiceStatusActionsHeaderView () {
    
}

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;


@end

@implementation BTServiceStatusActionsHeaderView

- (void)updateTitleLabelWithTitleText:(NSString *)titleLabel
{
    _titleLabel.text = titleLabel;
}

@end
