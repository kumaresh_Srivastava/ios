//
//  PhoneUsageSummaryTableViewHeader.m
//  BTBusinessApp
//
//  Created by vectoscalar on 22/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "PhoneUsageSummaryTableViewHeader.h"

@implementation PhoneUsageSummaryTableViewHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)updateHeaderWithTitle:(NSString *)title andDescription:(NSString *)description
{
    if(title && title.length>0)
       self.titleLabel.text = title;
    else
        self.titleLabel.text = @"";
    
    
    if(description && description.length>0)
        self.descriptionLabel.text = description;
    else
        self.descriptionLabel.text = @"";
}

@end
