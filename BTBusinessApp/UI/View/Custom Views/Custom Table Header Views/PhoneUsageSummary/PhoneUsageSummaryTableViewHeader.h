//
//  PhoneUsageSummaryTableViewHeader.h
//  BTBusinessApp
//
//  Created by vectoscalar on 22/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneUsageSummaryTableViewHeader : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *chooseButton;

- (void)updateHeaderWithTitle:(NSString *)title andDescription:(NSString *)description;

@end
