//
//  BTFaultTrackerNodeView.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 20/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultTrackerNodeView.h"
#import "BTFaultMilestoneNode.h"
#import "BrandColours.h"
#import "AppManager.h"


@interface BTFaultTrackerNodeView()
{
    
    BTFaultMilestoneNode *_btFaultMilestoneNode;
}

@end

@implementation BTFaultTrackerNodeView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)updateMilestoneNodeViewWithData:(BTFaultMilestoneNode *)nodeData isFirstNode:(BOOL)isFirstNode isLastNode:(BOOL)isLastNode
{
    self.topEnabledLineView.backgroundColor = [UIColor colorForHexString:@"dddddd"];//[BrandColours colourBackgroundBTPurplePrimaryColor];
    self.bottomEnabledLineView.backgroundColor = [UIColor colorForHexString:@"dddddd"];//[BrandColours colourBackgroundBTPurplePrimaryColor];
    self.nodeTitleLabel.text = nodeData.milestoneName;
    
    _btFaultMilestoneNode = nodeData;
    
    
    if ([AppManager isValidDate:nodeData.milestoneDateTime]) {
        
        self.dateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:nodeData.milestoneDateTime];//[AppManager getDayMonthFromDate:nodeData.milestoneDateTime];
        self.timeLabel.text = [AppManager getTimeFromDate:nodeData.milestoneDateTime];
    }
    else
    {
        self.timeLabel.text = @"";
        self.dateLabel.text = @"";
    }
    
    if (nodeData.isActive && !nodeData.isFirstActive) {
        self.timeLabel.hidden = true;
        self.dateLabel.hidden = true;
    }
    
    if (nodeData.isMessageNode) {
        self.timeLabel.hidden = true;
        self.dateLabel.hidden = true;
        self.messageImageView.hidden = false;
        self.viewMessageButton.hidden = false;
        self.nodeDescriptionLabel.text = [NSString stringWithFormat:@"Sent on %@ at %@",[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:nodeData.milestoneDateTime], [AppManager getTimeFromDate:nodeData.milestoneDateTime]];
        self.nodeCircleImageView.hidden = true;
        self.nodeCircleOverlayView.hidden = true;
    }
    else
    {
        self.viewMessageButton.hidden = true;
        self.messageImageView.hidden = true;
        self.nodeDescriptionLabel.text = nodeData.milestoneText;
    }
    
    if (isFirstNode) {
        self.topDisabledLineView.hidden = YES;
        self.topEnabledLineView.hidden = YES;
    }
    if (isLastNode) {
        self.bottomDisabledLineView.hidden = YES;
        self.bottomEnabledLineView.hidden = YES;
    }
    if (nodeData.isFirstActive) {
        self.nodeCircleOverlayView.hidden = YES;
    } else {
        self.nodeCircleOverlayView.hidden = YES;
    }
    if (nodeData.isActive) {
        if (nodeData.isFirstActive) {
            //self.nodeCircleImageView.image = [UIImage imageNamed:@"empty_circle_enabled"];
            //self.nodeCircleImageView.image = [UIImage imageNamed:@"empty_circle_disabled"];
            self.nodeCircleImageView.image = [UIImage imageNamed:@"greenTick"];
            self.bottomEnabledLineView.hidden = YES;
            self.topDisabledLineView.hidden = YES;
        } else {
            self.nodeCircleImageView.image = [UIImage imageNamed:@"empty_circle_disabled"];
            self.bottomEnabledLineView.hidden = YES;
            self.topEnabledLineView.hidden = YES;
            self.nodeDescriptionLabel.textColor = [UIColor colorForHexString:@"666666"];//[BrandColours colourBtNeutral70];
            self.nodeTitleLabel.textColor = [UIColor colorForHexString:@"333333"];//[BrandColours colourBtNeutral70];
        }
    } else {
        //self.nodeCircleImageView.image = [UIImage imageNamed:@"filled_circle"];
        self.nodeCircleImageView.image = [UIImage imageNamed:@"greenTick"];
        self.bottomDisabledLineView.hidden = YES;
        self.topDisabledLineView.hidden = YES;
    }
}

- (IBAction)viewMessageButtonAction:(id)sender {
    [self.delegate userPressedViewMessageButtonOnFaultMilestoneNodeView:self withBTFaultMilestoneNode:_btFaultMilestoneNode];
}

@end
