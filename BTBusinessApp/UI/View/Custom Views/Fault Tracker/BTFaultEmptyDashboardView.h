//
//  BTFaultEmptyDashboardView.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 22/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTFaultEmptyDashboardView;

@protocol BTFaultEmptyDashboardViewDelegate <NSObject>

@required
- (void)userPressedSearchByFaultReferenceButtonOfEmptyDashboardView:(BTFaultEmptyDashboardView *)retryView;
- (void)userPressedCreateNewFaultButtonOfEmptyDashboardView:(BTFaultEmptyDashboardView *)retryView;

@end

@interface BTFaultEmptyDashboardView : UIView

@property (nonatomic, assign) id<BTFaultEmptyDashboardViewDelegate> delegate;

- (void)updateEmptyDashboardViewWithdetailText:(NSMutableAttributedString *)detailText;

@end
