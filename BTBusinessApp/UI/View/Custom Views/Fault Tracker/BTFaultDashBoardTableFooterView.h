//
//  BTFaultDashBoardTableFooterView.h
//  BTBusinessApp
//
//  Created by Accolite on 16/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomSpinnerAnimationView.h"
@class BTFaultDashBoardTableFooterView;

#define kRetryViewOriginalHeight 39;

@protocol BTFaultDashBoardFooterDelegate <NSObject>

@required

- (void) faultDashBoardTableFooterView:(BTFaultDashBoardTableFooterView *)faultDashBoardFooterView retryBUttonActionMethod:(id)sender;
- (void) faultDashBoardTableFooterView:(BTFaultDashBoardTableFooterView *)faultDashBoardFooterView reportNewFaultActionMethod:(id)sender;

@end

@interface BTFaultDashBoardTableFooterView : UIView {
    CustomSpinnerAnimationView *footerView;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintRetryViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet UIButton *retryButton;
@property (weak, nonatomic) IBOutlet UIView *retryView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *retryIndicatorView;
@property (weak, nonatomic) IBOutlet UIButton *reportFaultButton;

@property (nonatomic, weak) id<BTFaultDashBoardFooterDelegate> faultDashBoardFooterDelegate;
- (IBAction)searchFaultByReferencePressed:(id)sender;

- (void)createCustomLoadingIndicator;
- (void)hideCustomLoadingIndicator;
- (void)showCustomLoadingIndicator;
- (void)startLoadingIndicatorAnimation;
- (void)stopCustomLoadingIndicatorAnimation;
- (void)hideRetryView;
- (void)showRetryViewWithErrorMessage:(NSString *)error;
- (void)showRetryView;
- (void)updateRetryViewHeight;
- (void)showLoaderContainerView;
- (void)hideLoaderContainerView;

@end
