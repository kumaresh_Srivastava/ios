//
//  BTFaultDetailsTakeActionView.m
//  BTBusinessApp
//
//  Created by VectoScalar on 10/21/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultDetailsClosedActionView.h"

@implementation BTFaultDetailsClosedActionView


- (void)drawRect:(CGRect)rect {
    // Drawing code
    self.contactUsButton.backgroundColor = [UIColor clearColor];
    self.contactUsButton.layer.borderColor = [BrandColours colourBackgroundBTPurplePrimaryColor].CGColor;
    self.contactUsButton.layer.borderWidth = 1.0f;
    self.contactUsButton.layer.cornerRadius = 5.0f;
}

- (IBAction)contactUsButtonAction:(id)sender {
    [self.delegate userClickedOnContactUsButtonOfFaultDetailsClosedActionView:self];
}


@end
