//
//  BTFaultDetailsTakeActionView.h
//  BTBusinessApp
//
//  Created by VectoScalar on 10/21/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTFaultDetailsTakeActionView;

@protocol BTFaultDetailsTakeActionViewDelegate <NSObject>

@optional
- (void)userPressedAmendFaultDetailsOfTakeActionView:(BTFaultDetailsTakeActionView *)takeActionView;
- (void)userPressedAddAppointmentToCalenderOfTakeActionView:(BTFaultDetailsTakeActionView *)takeActionView;
- (void)userTogglesNotificationSwitchOfTakeActionView:(BTFaultDetailsTakeActionView *)takeActionView;
- (void)userPressCancelFaultOfTakeActionView:(BTFaultDetailsTakeActionView *)takeActionView;

@end

@interface BTFaultDetailsTakeActionView : UIView {
    
}

@property (weak, nonatomic) IBOutlet UIView *AmendFaultView;
@property (weak, nonatomic) IBOutlet UIButton *cancelThisFaultButton;
@property (weak, nonatomic) IBOutlet UIView *addAppointmentToCalenderView;
@property (weak, nonatomic) IBOutlet UIView *faultNotificationsBottomView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintCancelThisFaultButtonHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintAmendFaultViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintAddAppointmentToCalenderViewHeight;

@property(nonatomic, assign) id<BTFaultDetailsTakeActionViewDelegate>delegate;

- (void)updateTakeActionWithShowAmendView:(BOOL)showAmendFault andShowCancelFault:(BOOL)showCancelFault;
- (void)hideAddAppointmentToCalenderView;
- (void)showAddAppointmentToCalenderView;

@end
