//
//  BTNoFaultDashboardView.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 07/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTNoFaultDashboardView.h"

@implementation BTNoFaultDashboardView


- (void)drawRect:(CGRect)rect
{
    self.reportFaultButton.layer.cornerRadius = 4.0f;
    self.reportFaultButton.layer.borderWidth = 1.0f;
    self.reportFaultButton.layer.borderColor = [UIColor colorWithRed:228.0/255.0f green:228.0/255.0f blue:228.0/255.0f alpha:1.0f].CGColor;
}

- (IBAction)reportNewFaultAction:(id)sender {

    if([self.delegate respondsToSelector:@selector(noFaultDashboardView:reportNewFaultAction:)])
    {
    [self.delegate noFaultDashboardView:self reportNewFaultAction:sender];
    }
}

@end
