//
//  BTNoFaultDashboardView.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 07/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>


@class BTNoFaultDashboardView;

@protocol BTNoFaultDashboardViewDelegate <NSObject>

@required

- (void) noFaultDashboardView:(BTNoFaultDashboardView *)noFaultDashboardView reportNewFaultAction:(id)sender;

@end



@interface BTNoFaultDashboardView : UIView

@property (nonatomic, assign) id<BTNoFaultDashboardViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *reportFaultButton;

@end
