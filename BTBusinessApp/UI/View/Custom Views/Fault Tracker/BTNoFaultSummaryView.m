//
//  BTNoFaultDashboardView.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 07/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTNoFaultSummaryView.h"
#import "BrandColours.h"

@implementation BTNoFaultSummaryView


- (void)drawRect:(CGRect)rect
{
    self.reportFaultButton.layer.cornerRadius = 5.0f;
    self.reportFaultButton.layer.borderWidth = 1.0f;
    self.reportFaultButton.layer.borderColor = [BrandColours colourBackgroundBTPurplePrimaryColor].CGColor;
    self.reportFaultButton.titleLabel.textColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
    self.reportFaultButton.backgroundColor = [UIColor clearColor];
}


- (void)updateWarningLabelWithMessage:(NSString *)message
{
    self.warningLabel.text = message;
}


- (IBAction)reportNewFaultAction:(id)sender {

    if([self.delegate respondsToSelector:@selector(noFaultSummaryView:reportNewFaultAction:)])
    {
        [self.delegate noFaultSummaryView:self reportNewFaultAction:sender];
    }
}


- (void)updateWithErrorMessage:(NSString *)message andHideReportAFaultButton:(BOOL)hideButton
{
    self.warningLabel.text = message;
    
    if(hideButton)
    {
        self.reportFaultButton.hidden = YES;
        self.buttonHeightConstraint.constant = 0;
        [self layoutIfNeeded];
    }
}



@end
