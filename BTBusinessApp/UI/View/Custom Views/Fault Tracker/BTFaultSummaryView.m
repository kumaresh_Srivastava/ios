//
//  BTFaultSummaryView.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 21/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//


#import "BTFaultSummaryView.h"
#import "AppManager.h"
#import "NSObject+APIResponseCheck.h"
#import "BTFaultAppointmentDetail.h"

@interface BTFaultSummaryView()


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintEngineerAppointmentHeight;
@property (weak, nonatomic) IBOutlet UIView *engineerAppointmentView;

@property (weak, nonatomic) IBOutlet UILabel *reportedDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *expectedCompletionDateLabel;
@property (weak, nonatomic) IBOutlet UIView *faultStatusView;
@property (weak, nonatomic) IBOutlet UILabel *faultStatusHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *faultStatusDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *faultTypeDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *expectedCompletionOrCompletedLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *engineerAppointmentDateLabel;

@end


@implementation BTFaultSummaryView


- (void)updateFaultSummaryViewWithFault:(BTFault *)fault
{

    //(SD) : Updating Reported Date Label
    if ([AppManager isValidDate:fault.reportedOnDate]) {
        
        [self.reportedDateLabel setText:[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:fault.reportedOnDate]];
        
    } else {
        self.reportedDateLabel.hidden = YES;
    }

    //(SD) : Updating Completion Date Label
    if ([AppManager isValidDate:fault.targetFixDate]) {
        
        [self.expectedCompletionDateLabel setText:[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:fault.targetFixDate]];
        
    } else {

        self.expectedCompletionDateLabel.hidden = YES;
        
    }
    
    //(LP) : Updating Fault Engineer Appointment Details
    if (fault.faultAppointmentDetail.isHourAccessPresent)
    {
        if (![AppManager isValidDate:fault.faultAppointmentDetail.bookedStartDate] || ([self getHourFromText:fault.faultAppointmentDetail.earlierAccessTime] >= [self getHourFromText:fault.faultAppointmentDetail.latestAccessTime]))
        {
            //[self.engineerAppointmentView removeFromSuperview];
            self.engineerAppointmentView.hidden = YES;
            self.constraintEngineerAppointmentHeight.constant = 0;
        }
        else
        {
            self.engineerAppointmentView.hidden = NO;
            self.constraintEngineerAppointmentHeight.constant = 87;
            NSString *dateString = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:fault.faultAppointmentDetail.bookedStartDate];
            dateString = [NSString stringWithFormat:@"%@, %@-%@", dateString, [self getHourFromTimeText:fault.faultAppointmentDetail.earlierAccessTime], [self getHourFromTimeText:fault.faultAppointmentDetail.latestAccessTime]];
            self.engineerAppointmentDateLabel.text = dateString;
        }
    }
    else
    {
        if (![AppManager isValidDate:fault.faultAppointmentDetail.bookedEndDate])
        {
            //[self.engineerAppointmentView removeFromSuperview];
            self.engineerAppointmentView.hidden = YES;
            self.constraintEngineerAppointmentHeight.constant = 0;
        }
        else
        {
            self.engineerAppointmentView.hidden = NO;
            self.constraintEngineerAppointmentHeight.constant = 87;
            NSString *dateString = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:fault.faultAppointmentDetail.bookedEndDate];
            
            NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:fault.faultAppointmentDetail.bookedEndDate];
            
            long hourValue = components.hour;
            if (hourValue < 13)
            {
                dateString = [NSString stringWithFormat:@"%@, 8am-1pm", dateString];
            }
            else
            {
                dateString = [NSString stringWithFormat:@"%@, 1pm-6pm", dateString];
            }
            self.engineerAppointmentDateLabel.text = dateString;
        }
    }

    //(SD) : Updating Fault Status Header Label
    if ([fault.status validAndNotEmptyStringObject] != nil) {
        
        [self.faultStatusHeaderLabel setText:fault.status];
    } else {

        self.faultStatusHeaderLabel.hidden = YES;

    }

    //(SD) : Updating Fault Status Description Label
    if ([fault.currentMilestoneDescription validAndNotEmptyStringObject] != nil) {
        
      //(RM):: Remove space and new lines
        NSString *string = [[fault.currentMilestoneDescription componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
        string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [self.faultStatusDescriptionLabel setText:string];
    } else {
        
        self.faultStatusDescriptionLabel.text = @"";
        self.faultStatusDescriptionLabel.hidden = YES;
        
    }

    //(SD) : Updating Fault Type Description Label
    if ([fault.faultLocation validAndNotEmptyStringObject] != nil) {
        
        [self.faultTypeDescriptionLabel setText:fault.faultLocation];
    } else {

        self.faultTypeDescriptionLabel.hidden = YES;

    }
    
    //(lp) : Updating Fault Phone Number Label
    if ([fault.serviceID validAndNotEmptyStringObject] != nil) {
        
        [self.phoneNumberLabel setText:fault.serviceID];
    } else {
        
        self.phoneNumberLabel.hidden = YES;
        
    }
    
    //(MSC) : Updating the label over Completion date label
    NSString *faultStatus = [fault.status lowercaseString];

    if ([faultStatus isEqualToString:@"completed"]) {
        [self.expectedCompletionOrCompletedLabel setText:@"Completed on"];
    }
    else if ([faultStatus isEqualToString:@"resolved"]) {
        [self.expectedCompletionOrCompletedLabel setText:@"Resolved on"];

    }

    [self layoutSubviews];
    [self layoutIfNeeded];

}

- (NSString *)getHourFromTimeText:(NSString *)hourText
{
    //Removing colon and zeros after colon
    NSString *newString = [hourText stringByReplacingOccurrencesOfString:@":00" withString:@""];
    newString = [newString stringByReplacingOccurrencesOfString:@":00" withString:@""];
    
    //Removing leading zero
    NSRange range = [newString rangeOfString:@"^0*" options:NSRegularExpressionSearch];
    newString = [newString stringByReplacingCharactersInRange:range withString:@""];
    
    return newString;
}

- (NSInteger)getHourFromText:(NSString *)hourText
{
    //Removing colon and zeros after colon
    NSString *newString = [hourText stringByReplacingOccurrencesOfString:@":00" withString:@""];
    newString = [newString stringByReplacingOccurrencesOfString:@":00" withString:@""];
    
    //Removing leading zero
    NSRange range = [newString rangeOfString:@"^0*" options:NSRegularExpressionSearch];
    newString = [newString stringByReplacingCharactersInRange:range withString:@""];
    
    NSString *amPmText = [newString substringFromIndex:newString.length-2];
    
    NSString *hourString = [newString substringWithRange:NSMakeRange(0, newString.length-2)];
    
    if ([amPmText isEqualToString:@"am"])
    {
        NSInteger hourValue = [hourString integerValue];
        if (hourValue == 12)
        {
            return 0;
        }
        else
        {
            return hourValue;
        }
    }
    else
    {
        NSInteger hourValue = [hourString integerValue];
        if (hourValue == 12)
        {
            return 12;
        }
        else
        {
            return hourValue + 12;
        }
    }
}

@end
