//
//  BTFaultSummaryView.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 21/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTFault.h"

@interface BTFaultSummaryView : UIView


- (void)updateFaultSummaryViewWithFault:(BTFault *)fault;

@end
