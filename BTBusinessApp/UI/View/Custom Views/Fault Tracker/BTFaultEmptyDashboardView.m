//
//  BTFaultEmptyDashboardView.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 22/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultEmptyDashboardView.h"
#import "BrandColours.h"
@interface BTFaultEmptyDashboardView()
@property (weak, nonatomic) IBOutlet UIButton *searchByFaultRefButton;
@property (weak, nonatomic) IBOutlet UIButton *reportFaultButton;
@property (weak, nonatomic) IBOutlet UITextView *detailedTextTextView;

@end

@implementation BTFaultEmptyDashboardView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib
{
    [super awakeFromNib];

    _reportFaultButton.layer.cornerRadius = 5.0;
    _reportFaultButton.layer.borderWidth = 1.0;
    _reportFaultButton.layer.borderColor =  [BrandColours colourBackgroundBTPurplePrimaryColor].CGColor;


    _searchByFaultRefButton.layer.cornerRadius = 5.0;
    _searchByFaultRefButton.layer.borderWidth = 1.0;
    _searchByFaultRefButton.layer.borderColor =  [BrandColours colourBackgroundBTPurplePrimaryColor].CGColor;

}
- (IBAction)reportNewFaultAction:(id)sender {

    [_delegate userPressedCreateNewFaultButtonOfEmptyDashboardView:self];
}


- (IBAction)searchByFaultAction:(id)sender {

    [_delegate userPressedSearchByFaultReferenceButtonOfEmptyDashboardView:self];
}


- (void)dealloc
{
    _delegate = nil;
}

-(void)updateEmptyDashboardViewWithdetailText:(NSMutableAttributedString *)detailText
{
    
    
    if(detailText && detailText.length>0)
    {
        self.detailedTextTextView.attributedText = detailText;
        self.detailedTextTextView.tintColor = [BrandColours colourTextBTPurplePrimaryColor];
        self.detailedTextTextView.editable = NO;
    }
    else
    {
        self.detailedTextTextView.attributedText = [[NSAttributedString alloc] initWithString:@""];
        self.detailedTextTextView.editable = NO;
    }
}

@end
