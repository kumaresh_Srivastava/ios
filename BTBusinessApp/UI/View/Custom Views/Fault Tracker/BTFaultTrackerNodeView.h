//
//  BTFaultTrackerNodeView.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 20/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BTFaultTrackerNodeView;
@class BTFaultMilestoneNode;

@protocol BTFaultTrackerNodeViewDelegate <NSObject>

- (void)userPressedViewMessageButtonOnFaultMilestoneNodeView:(BTFaultTrackerNodeView *)faultMilestoneNodeView withBTFaultMilestoneNode:(BTFaultMilestoneNode *)faultMileStoneNode;

@end

@interface BTFaultTrackerNodeView : UIView {
    
}

@property (weak, nonatomic) id <BTFaultTrackerNodeViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *nodeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *nodeDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *viewMessageButton;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomEnabledLineView;
@property (weak, nonatomic) IBOutlet UIView *topEnabledLineView;
@property (weak, nonatomic) IBOutlet UIView *bottomDisabledLineView;
@property (weak, nonatomic) IBOutlet UIView *topDisabledLineView;
@property (weak, nonatomic) IBOutlet UIImageView *nodeCircleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *nodeCircleOverlayView;
@property (weak, nonatomic) IBOutlet UIImageView *messageImageView;

- (void)updateMilestoneNodeViewWithData:(BTFaultMilestoneNode *)nodeData isFirstNode:(BOOL)isFirstNode isLastNode:(BOOL)isLastNode;

@end
