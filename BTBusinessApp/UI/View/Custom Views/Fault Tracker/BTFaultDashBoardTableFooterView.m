//
//  BTFaultDashBoardTableFooterView.m
//  BTBusinessApp
//
//  Created by Accolite on 16/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultDashBoardTableFooterView.h"

@implementation BTFaultDashBoardTableFooterView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.*/

- (void)drawRect:(CGRect)rect {
    
    self.reportFaultButton.layer.cornerRadius = 5.0f;
    self.reportFaultButton.layer.borderWidth = 1.0f;
    self.reportFaultButton.layer.borderColor = [BrandColours colourBackgroundBTPurplePrimaryColor].CGColor;
    
    self.retryButton.layer.cornerRadius = 5.0f;
    self.retryButton.layer.borderWidth = 1.0f;
    self.retryButton.layer.borderColor = [BrandColours colourBackgroundBTPurplePrimaryColor].CGColor;
    
}

- (IBAction)reportNewFaultAction:(id)sender {
    [self.faultDashBoardFooterDelegate faultDashBoardTableFooterView:self reportNewFaultActionMethod:sender];
}

- (IBAction)retryAction:(id)sender {
    [self.faultDashBoardFooterDelegate faultDashBoardTableFooterView:self retryBUttonActionMethod:sender];
}

- (IBAction)searchFaultByReferencePressed:(id)sender {
}

- (void)updateRetryViewHeight {
    if (_retryButton.hidden && footerView.hidden) {
        _constraintRetryViewHeight.constant = 5;
    }
    else
    {
        _constraintRetryViewHeight.constant = kRetryViewOriginalHeight;
    }
}

- (void)showLoaderContainerView {
    _constraintRetryViewHeight.constant = kRetryViewOriginalHeight;
    [self updateConstraintsIfNeeded];
    [self layoutIfNeeded];
}

- (void)hideLoaderContainerView {
    _constraintRetryViewHeight.constant = 5;
    [self layoutIfNeeded];
    [self updateConstraintsIfNeeded];
    [self layoutSubviews];
}

#pragma mark public methods

- (void)hideRetryView {
    [self.errorLabel setHidden:YES];
    [self.retryButton setHidden:YES];
    
    [self updateConstraintsIfNeeded];
    [self layoutIfNeeded];
}

- (void)showRetryView {
    
    [self.errorLabel setHidden:NO];
    [self.retryButton setHidden:NO];
    
    [self updateConstraintsIfNeeded];
    [self layoutIfNeeded];
}

- (void)showRetryViewWithErrorMessage:(NSString *)error {
    
    self.errorLabel.text = error;
    [self.errorLabel setHidden:NO];
    [self.retryButton setHidden:NO];
    
    [self updateConstraintsIfNeeded];
    [self layoutIfNeeded];
}

- (void)createCustomLoadingIndicator {
    
    if(!footerView){
        
        footerView = [[CustomSpinnerAnimationView alloc] initWithText:@"" withLoadingImage:[UIImage imageNamed:@"Spinner"]];
        
        footerView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.retryView addSubview:footerView];
        [self.retryView addConstraint:[NSLayoutConstraint constraintWithItem:footerView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.retryView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:-20.0]];
        [self.retryView addConstraint:[NSLayoutConstraint constraintWithItem:footerView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.retryView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:-30.0]];
        [_retryView addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:100]];
        [_retryView addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:155]];
        footerView.backgroundColor = [UIColor clearColor];
        [self setNeedsLayout];
    }
}

- (void)hideCustomLoadingIndicator {
    [footerView setHidden:YES];
    
    [self layoutIfNeeded];
}

- (void)showCustomLoadingIndicator {
    [footerView setHidden:NO];
    
    [self layoutIfNeeded];
}

- (void)startLoadingIndicatorAnimation {
    
    [self stopCustomLoadingIndicatorAnimation];
    
    [self createCustomLoadingIndicator];
    
    [footerView setIsStopped:NO];
    [self showCustomLoadingIndicator];
    [footerView startAnimation];
}

- (void)stopCustomLoadingIndicatorAnimation {
    
    [footerView stopAnimation];
    [self hideCustomLoadingIndicator];
    
    [footerView removeFromSuperview];
    footerView = nil;
}

@end
