//
//  BTNoFaultDashboardView.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 07/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>


@class BTNoFaultSummaryView;

@protocol BTNoFaultSummaryViewDelegate <NSObject>

@required

- (void) noFaultSummaryView:(BTNoFaultSummaryView *)noFaultSummaryView reportNewFaultAction:(id)sender;

@end



@interface BTNoFaultSummaryView : UIView

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonHeightConstraint;
@property (nonatomic, assign) id<BTNoFaultSummaryViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *reportFaultButton;
@property (weak, nonatomic) IBOutlet UILabel *warningLabel;


- (void)updateWarningLabelWithMessage:(NSString *)message;
- (void)updateWithErrorMessage:(NSString *)message andHideReportAFaultButton:(BOOL)hideButton;

@end
