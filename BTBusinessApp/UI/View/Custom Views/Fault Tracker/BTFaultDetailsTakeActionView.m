//
//  BTFaultDetailsTakeActionView.m
//  BTBusinessApp
//
//  Created by VectoScalar on 10/21/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultDetailsTakeActionView.h"

#define kOriginalAddAppointmentToCalenderHeight 55;

@interface BTFaultDetailsTakeActionView () {
    
    BOOL _isAmendFaultButtonVisible;
    BOOL _isNotificationsButtonVisible;
    
}

@end

@implementation BTFaultDetailsTakeActionView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (IBAction)amendFaultButtonPressed:(id)sender {
    
    [self.delegate userPressedAmendFaultDetailsOfTakeActionView:self];
}

- (IBAction)notificationSwitchToggled:(id)sender {
    
    [self.delegate userTogglesNotificationSwitchOfTakeActionView:self];
}

- (IBAction)cancelFaultButtonPressed:(id)sender {
    
    [self.delegate userPressCancelFaultOfTakeActionView:self];
}

- (IBAction)addAppointmentToCalender:(id)sender {
    
    [self.delegate userPressedAddAppointmentToCalenderOfTakeActionView:self];
}

#pragma mark - UI Handling Methods

- (void)updateTakeActionWithShowAmendView:(BOOL)showAmendFault andShowCancelFault:(BOOL)showCancelFault
{
    if (!showAmendFault) {
        _isAmendFaultButtonVisible = NO;
        self.AmendFaultView.hidden = YES;
        self.constraintAmendFaultViewHeight.constant = 0;
        [self checkForNoButtonVisibleInView];
    }
    else
    {
        _isAmendFaultButtonVisible = YES;
        [self checkForNoButtonVisibleInView];
    }
    
    if (!showCancelFault) {
        self.cancelThisFaultButton.hidden = YES;
        self.constraintCancelThisFaultButtonHeight.constant = 0;
    } else {
        self.cancelThisFaultButton.layer.borderColor = [BrandColours colourBackgroundBTPurplePrimaryColor].CGColor;
    }
}

- (void)checkForNoButtonVisibleInView
{
    if (_isAmendFaultButtonVisible) {
        return;
    }
    else
    {
        self.hidden = YES;
    }
}

- (void)hideAddAppointmentToCalenderView {
    self.constraintAddAppointmentToCalenderViewHeight.constant = 0;
    self.addAppointmentToCalenderView.hidden = YES;
}

- (void)showAddAppointmentToCalenderView {
    self.constraintAddAppointmentToCalenderViewHeight.constant = kOriginalAddAppointmentToCalenderHeight;
    self.addAppointmentToCalenderView.hidden = NO;
    [self layoutIfNeeded];
}

@end
