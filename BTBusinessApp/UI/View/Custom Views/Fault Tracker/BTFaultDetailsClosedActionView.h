//
//  BTFaultDetailsTakeActionView.h
//  BTBusinessApp
//
//  Created by VectoScalar on 10/21/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BTFaultDetailsClosedActionView;

@protocol BTFaultDetailsClosedActionViewDelegate <NSObject>

- (void)userClickedOnContactUsButtonOfFaultDetailsClosedActionView:(BTFaultDetailsClosedActionView *)faultDetailsClosedActionView;

@end

@interface BTFaultDetailsClosedActionView : UIView {
    
}

@property (weak, nonatomic) id <BTFaultDetailsClosedActionViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *contactUsButton;

@end
