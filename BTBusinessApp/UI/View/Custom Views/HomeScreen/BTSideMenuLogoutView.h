//
//  BTSideMenuLogoutView.h
//  BTBusinessApp
//
//  Copyright © 2019 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BTSideMenuLogoutView : UIView
@property (weak, nonatomic) IBOutlet UILabel *logOutButton;
@end

NS_ASSUME_NONNULL_END
