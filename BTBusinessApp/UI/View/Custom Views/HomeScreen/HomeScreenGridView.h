//
//  HomeScreenGridView.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/23/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMHomeScreen.h"

@class HomeScreenGridView;

@protocol HomeScreenGridViewDelegate <NSObject>

- (void)homeScreenGridView:(HomeScreenGridView *)view didSelectForTitle:(NSString *)titleString;

@end

@interface HomeScreenGridView : UIView

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titieLabelTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *placeholderTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *spinnerImageTopConstraint;

@property (weak, nonatomic) IBOutlet UIView *bgView;

@property(nonatomic, assign) id<HomeScreenGridViewDelegate>delegate;
- (void)updateWithGridViewModel:(HomeGridViewModel *)gridViewModel;
- (CGFloat)height;
- (NSString *)getTitle;
- (void)updateGridViewConstraint;

@end
