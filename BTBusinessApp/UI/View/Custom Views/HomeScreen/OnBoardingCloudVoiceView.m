//
//  OnBoardingCloudVoiceView.m
//  BTBusinessApp
//
//  Created by Manik on 04/03/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import "OnBoardingCloudVoiceView.h"

@interface OnBoardingCloudVoiceView ()

@property (weak,nonatomic)IBOutlet UIView *btnGotItSuperView;
@property (weak,nonatomic)IBOutlet UIView *btnGotItContainerView;
@property (weak,nonatomic)IBOutlet UIButton *btnGotIt;



@end

@implementation OnBoardingCloudVoiceView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
//    _titleLabel.text = @"§tedfgsdf";
    self.btnGotItSuperView.layer.cornerRadius = 5.0;
    self.btnGotItContainerView.layer.cornerRadius = 5.0;
    self.btnGotIt.layer.cornerRadius = 5.0;

}


@end
