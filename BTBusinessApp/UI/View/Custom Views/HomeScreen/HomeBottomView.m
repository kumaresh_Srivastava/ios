//
//  HomeBottomView.m
//  BTBusinessApp
//
//  Created by vectoscalar on 26/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "HomeBottomView.h"

@implementation HomeBottomView
@synthesize delegate;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)didPressActionButton:(id)sender {
    
    [delegate bottomHomeScreenView:self didSelectForTitle:self.titleLabel.text];

}


- (void)updateWithGridViewModel:(HomeGridViewModel *)gridViewModel{
    
    self.hoverButton.accessibilityLabel = gridViewModel.titleText;
    self.titleLabel.text = gridViewModel.titleText;
    self.iconImageView.image = gridViewModel.iconImage;
    
    
    [self layoutIfNeeded];
    [self updateConstraints];
    
    imgHeight = self.iconImageView.frame.size.height;
}


- (CGFloat)height
{
    CGFloat newHeight = self.iconImageView.frame.size.height+26;
    
    return newHeight;
}

@end
