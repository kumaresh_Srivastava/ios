//
//  HomeBottomView.h
//  BTBusinessApp
//
//  Created by vectoscalar on 26/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMHomeScreen.h"


@class HomeBottomView;

@protocol HomeBottomViewDelegate <NSObject>

- (void)bottomHomeScreenView:(HomeBottomView *)view didSelectForTitle:(NSString *)titleString;

@end


@interface HomeBottomView : UIView
{
    CGFloat imgHeight;
}


@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UIButton *hoverButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

- (IBAction)didPressActionButton:(id)sender;
@property (assign,nonatomic) id <HomeBottomViewDelegate> delegate;
- (void)updateWithGridViewModel:(HomeGridViewModel *)gridViewModel;

- (CGFloat)height;

@end
