//
//  HomeScreenGridView.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/23/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "HomeScreenGridView.h"
#import <Lottie/Lottie.h>

#define kTitleLabelTopConstraint 10
#define kSpinnerTopConstraint 6
#define kSpinnerTopConstraintForiPhone5 4
#define IS_IPHONE5 ([[UIScreen mainScreen] bounds].size.height == 568.0)


@interface HomeScreenGridView(){
    
    BOOL _rotate;
    CGFloat imgHeight;
    
}
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (weak, nonatomic) IBOutlet UIView *valuePlaceholderView;
@property (weak, nonatomic) IBOutlet UIButton *hoverButton;
@property (nonatomic) LOTAnimationView *animationView;

@end

@implementation HomeScreenGridView

- (void)awakeFromNib{
    
    [super awakeFromNib];
    
    self.valueLabel.backgroundColor = [UIColor clearColor];
    self.valueLabel.layer.cornerRadius = 2;
    self.valueLabel.layer.borderWidth = 1.0;
    self.valueLabel.layer.borderColor = [BrandColours colourBtNeutral80].CGColor;
    
    [self createLoadingAnimation];
}

- (void)updateWithGridViewModel:(HomeGridViewModel *)gridViewModel{
    
    [self stopLoadingAnimation];
    
    self.hoverButton.accessibilityLabel = gridViewModel.titleText;
    self.titleLabel.text = gridViewModel.titleText;
    NSInteger value = gridViewModel.valueText.integerValue;
    if(value == 0){
        self.valueLabel.hidden = YES;
        self.valuePlaceholderView.hidden = YES;
    } else {
        self.valuePlaceholderView.hidden = NO;
        self.valueLabel.hidden = NO;
        self.valueLabel.text = [self notificationTextAndStyleForValue:gridViewModel.valueText withTitleText:gridViewModel.titleText];
    }
    self.valueLabel.text = [self notificationTextAndStyleForValue:gridViewModel.valueText withTitleText:gridViewModel.titleText];
    
    self.iconImageView.image = gridViewModel.iconImage;
    
    
    
    switch (gridViewModel.gridSate) {
        case HomeGridViewStateProgress:{
            [self startLoadingAnimation];
            break;
        }
        case HomeGridViewStateFailled:{
            self.valuePlaceholderView.hidden = YES;
            break;
        }
        case HomeGridViewStateCompleted:{
            break;
        }
        case HomeGridViewStateCompletedZero:{
//            self.valuePlaceholderView.layer.borderWidth = 1.0;
//            self.valuePlaceholderView.layer.borderColor = [[BrandColours colourBtWhite] colorWithAlphaComponent:0.2].CGColor;
            break;
        }
        default:
            break;
    }

    
    [self layoutIfNeeded];
    [self updateConstraints];
    
    imgHeight = self.iconImageView.frame.size.width;
    
}

- (CGFloat)height
{
    CGFloat height = self.frame.size.height;
    
    height += (imgHeight - 80);
    
    return height;
}

- (NSString *)getTitle{
    
    return self.titleLabel.text;
}
- (NSString*) notificationTextAndStyleForValue:(NSString*)value withTitleText:(NSString*)titleTxt{
    if([value length] <= 3) {
        if([value intValue] > 0) {
            if([titleTxt isEqualToString:@"Track orders"]){
                 return [NSString stringWithFormat:@"%@ active", value];
                
            } else if([titleTxt isEqualToString:@"Billing"]){
                 return [NSString stringWithFormat:@"%@ new", value];
            } else if([titleTxt isEqualToString:@"Track faults"]){
                return [NSString stringWithFormat:@"%@ open", value];
            } else{
                 return [NSString stringWithFormat:@"%@ active", value];
            }
           
        } else {
            self.valueLabel.layer.borderColor = [BrandColours colourBtNeutral50].CGColor;
            self.valueLabel.textColor = [BrandColours colourBtNeutral50];
            if([titleTxt isEqualToString:@"Track orders"]){
                 return @"0 active";
            } else if([titleTxt isEqualToString:@"Billing"]){
                return @"0 new";
            } else if([titleTxt isEqualToString:@"Track faults"]){
                return @"0 open";
            } else{
                return @"0 new";
            }
           
        }
    } else {
        return value;
    }
}

- (void) createLoadingAnimation {
    self.animationView = [LOTAnimationView animationNamed:@"Dotty-Loop"];
    [self.animationView setContentMode:UIViewContentModeScaleAspectFit];
    CGRect lottieFrame = CGRectMake(-10, -10, 100, 39);
    [self.animationView setFrame:lottieFrame];
    [self.animationView setBackgroundColor:[UIColor clearColor]];
    [self.animationView setLoopAnimation:YES];
    [self.animationView playToFrame:[NSNumber numberWithInteger:31]
                 withCompletion:^(BOOL animationFinished)
     {
         [self.animationView setLoopAnimation:true];
         [self.animationView playFromFrame:[NSNumber numberWithInteger:32]
                                      toFrame:[NSNumber numberWithInteger:93]
                               withCompletion:nil];
     }];
    [self.valuePlaceholderView addSubview:self.animationView];
}

- (void)startLoadingAnimation{
    self.animationView.hidden = NO;
    self.valueLabel.hidden = YES;
}


- (void)stopLoadingAnimation{
    self.animationView.hidden = YES;
    self.valueLabel.hidden = NO;
}

- (void)updateGridViewConstraint
{
    self.titieLabelTopConstraint.constant = kTitleLabelTopConstraint;
    
    if(IS_IPHONE5)
    {
        self.placeholderTopConstraint.constant = kSpinnerTopConstraintForiPhone5;
        self.spinnerImageTopConstraint.constant =  kSpinnerTopConstraintForiPhone5;
    }
    else
    {
        self.placeholderTopConstraint.constant = kSpinnerTopConstraint;
        self.spinnerImageTopConstraint.constant =  kSpinnerTopConstraint;
    }
    
    [self layoutIfNeeded];
    [self updateConstraints];
    
}

#pragma mark- Action Method

- (IBAction)gridButtonPressed:(id)sender {
    
    [self.delegate homeScreenGridView:self didSelectForTitle:self.titleLabel.text];
}



@end
