//
//  BTBillDetailHeaderView.h
//  BTBusinessApp
//
//  Created by vectoscalar on 03/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBill.h"
#import "BTAsset.h"
#import "BTOrderStatusLabel.h"

@class BTBillDetailHeaderView;

@protocol BTBillDetailHeaderViewDelegate <NSObject>

- (void)userDidChangedBillDetailTypeWithIndex:(int)newIndex;

@end

@interface BTBillDetailHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIView *containverView;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;

@property (weak, nonatomic) IBOutlet UILabel *accountIdLabel;

@property (weak, nonatomic) IBOutlet BTOrderStatusLabel *paymentStatusLabel;

@property (assign,nonatomic) id <BTBillDetailHeaderViewDelegate>delegate;

- (void)updateDataWithBill:(BTBill *)btBill withPaymentStatus:(NSString *)paymentStatus andPaymentText:(NSString *)paymentText  andTotalAmount:(NSString *)totalAmount;
- (void)updateDataWithAsset:(BTAsset *)btAsset withPaymentStatus:(NSString *)paymentStatus andPaymentText:(NSString *)paymentText  andTotalAmount:(NSString *)totalAmount;

- (IBAction)userChangedDeatilIndex:(id)sender;

@end
