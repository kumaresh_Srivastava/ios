//
//  BTBillDetailHeaderView.m
//  BTBusinessApp
//
//  Created by vectoscalar on 03/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBillDetailHeaderView.h"
#import "BTAsset.h"
#import "AppManager.h"

@implementation BTBillDetailHeaderView
@synthesize delegate;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib
{
    [super awakeFromNib];
    //[_segmentControl setTitleTextAttributes:[[AppManager sharedAppManager] fetchSegmentedControlFontDict] forState:UIControlStateNormal];
    //[_segmentControl setTintColor:[BrandColours colourBackgroundBTPurplePrimaryColor]];
}

- (void)updateDataWithBill:(BTBill *)btBill withPaymentStatus:(NSString *)paymentStatus andPaymentText:(NSString *)paymentText andTotalAmount:(NSString *)totalAmount
{
    self.containverView.backgroundColor = [UIColor whiteColor];//[BrandColours colourBtBackgroundColor];
    
    if(totalAmount && totalAmount.length >0)
        self.totalAmountLabel.text = totalAmount;
    else
        self.totalAmountLabel.text = @"-";
    
    if(btBill.billingAccountNo)
        self.accountIdLabel.text  = btBill.billingAccountNo;//[NSString stringWithFormat:@"Account %@",btBill.billingAccountNo];
    else
        self.accountIdLabel.text = @"-";//@"Account -";
        
    //self.paymentStatusLabel.text = paymentText;
    //self.paymentStatusLabel.textColor = [self getStatusLabelColorWithStatus:paymentStatus];
    [self.paymentStatusLabel setOrderStatus:paymentText];
}

- (void)updateDataWithAsset:(BTAsset *)btAsset withPaymentStatus:(NSString *)paymentStatus andPaymentText:(NSString *)paymentText andTotalAmount:(NSString *)totalAmount
{
    self.containverView.backgroundColor = [UIColor whiteColor];//[BrandColours colourBtBackgroundColor];
    
    if(totalAmount && totalAmount.length >0)
        self.totalAmountLabel.text = totalAmount;
    else
        self.totalAmountLabel.text = @"-";
    
    if(btAsset.billingAccountNumber)
        self.accountIdLabel.text  = btAsset.billingAccountNumber;//[NSString stringWithFormat:@"Account %@",btAsset.billingAccountNumber];
    else
        self.accountIdLabel.text = @"-";//@"Account -";
    
//    self.paymentStatusLabel.text = paymentText;
//    self.paymentStatusLabel.textColor = [self getStatusLabelColorWithStatus:paymentStatus];
    [self.paymentStatusLabel setOrderStatus:paymentText];
}



- (IBAction)userChangedDeatilIndex:(id)sender {
    
    UISegmentedControl * segmentedControl = (UISegmentedControl *)sender;
    
    [delegate userDidChangedBillDetailTypeWithIndex:(int)segmentedControl.selectedSegmentIndex];
    
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
//    self.totalAmountLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.totalAmountLabel.frame);
//    
//    self.accountIdLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.totalAmountLabel.frame);
//    
//    self.paymentStatusLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.paymentStatusLabel.frame);

}


- (UIColor *)getStatusLabelColorWithStatus:(NSString *)status
{
    //[SD] Default Color
    UIColor *statusLabelColor = [UIColor blackColor];
    if(status && ([[status lowercaseString] isEqualToString:@"overdue"]))
    {
        statusLabelColor = [BrandColours colourMyBtRed];
    }
    else if(status && ([[status lowercaseString] isEqualToString:@"ready"]))
    {
        statusLabelColor = [BrandColours colourMyBtOrange];
    }
    else if(status && ([[status lowercaseString] isEqualToString:@"paid"]))
    {
        statusLabelColor = [BrandColours colourMyBtGreen];
    }
    else if(status && ([[status lowercaseString] isEqualToString:@"due"]))
    {
        statusLabelColor = [BrandColours colourMyBtOrange];
    }
    else if (status && [[status lowercaseString] isEqualToString:@"no action required"])
    {
        statusLabelColor = [BrandColours colourMyBtGreen];
    }
    
    
    return statusLabelColor;
}

@end
