//
//  BTBillsDashBoardTableFooterView.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 03/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBillsSummaryTableFooterView.h"
#import "BrandColours.h"

@interface BTBillsSummaryTableFooterView()

@property (weak, nonatomic) IBOutlet UIButton *payMultipleBills;

@end

@implementation BTBillsSummaryTableFooterView

-(void)awakeFromNib
{
    [super awakeFromNib];
    _payMultipleBills.layer.borderColor = [BrandColours colourBackgroundBTPurplePrimaryColor].CGColor;
    _payMultipleBills.layer.borderWidth = 1.0;
    _payMultipleBills.layer.cornerRadius = 5.0f;
}

- (IBAction)payMultipleAction:(id)sender {

    if([self.billSummaryTableFooterDelegate respondsToSelector:@selector(billsSummaryTableFooterView:payMultipleActionMethod:)])
    {
        [self.billSummaryTableFooterDelegate billsSummaryTableFooterView:self payMultipleActionMethod:sender];
    }
}

@end
