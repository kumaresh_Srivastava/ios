//
//  BTBillsDashBoardTableFooterView.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 03/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTBillsSummaryTableFooterView;

@protocol BTBillSummaryTableFooterDelegate <NSObject>

@required

- (void) billsSummaryTableFooterView:(BTBillsSummaryTableFooterView *)billsSummaryTableFooterView payMultipleActionMethod:(id)sender;

@end


@interface BTBillsSummaryTableFooterView : UIView

@property (nonatomic, assign) id <BTBillSummaryTableFooterDelegate> billSummaryTableFooterDelegate;
@end
