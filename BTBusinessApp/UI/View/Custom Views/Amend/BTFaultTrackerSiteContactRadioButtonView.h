//
//  BTFaultTrackerSiteContactRadioButtonView.h
//  SampleViewApplication
//
//  Created by vectoscalar on 11/11/16.
//  Copyright © 2016 VS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTFaultTrackerSiteContactRadioButtonView;

@protocol BTFaultTrackerSiteContactRadioButtonViewDelegate <NSObject>

- (void)userDidSelectedOption:(NSString *)optionText atIndex:(int)index onView:(BTFaultTrackerSiteContactRadioButtonView*)view;

@end

@interface BTFaultTrackerSiteContactRadioButtonView : UIView

@property (assign,nonatomic) id <BTFaultTrackerSiteContactRadioButtonViewDelegate> delegate;
@property(nonatomic, assign) NSInteger section;

- (instancetype)initWithFrame:(CGRect)frame andOptionArray:(NSArray *)optionArray andTitleHeading:(NSString *)titleHeading andSelectedIndex:(NSInteger)selectedIndex;

+ (CGFloat)getFooterHeightForOptionArray:(NSArray *)optionArray withTitleText:(NSString *)title;

- (void)selectRadioButtonAtIndex:(NSInteger)index;

@end
