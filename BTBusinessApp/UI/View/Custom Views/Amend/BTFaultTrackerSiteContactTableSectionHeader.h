//
//  BTFaultTrackerSiteContactTableSectionHeader.h
//  SampleViewApplication
//
//  Created by vectoscalar on 11/11/16.
//  Copyright © 2016 VS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTFaultTrackerSiteContactTableSectionHeader : UIView

@property (weak, nonatomic) IBOutlet UILabel *sectionTitleLabel;

- (void)updateSectionTitle:(NSString*)title;

@end
