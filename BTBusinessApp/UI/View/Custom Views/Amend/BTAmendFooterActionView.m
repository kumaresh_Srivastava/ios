//
//  BTAmendFooterActionView.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/2/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTAmendFooterActionView.h"

@interface BTAmendFooterActionView()
@property (weak, nonatomic) IBOutlet UIButton *button;

@end

@implementation BTAmendFooterActionView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setTitle:(NSString *)titleText{
    
    [self.button setTitle:titleText forState:UIControlStateNormal];
}

- (IBAction)buttonPressed:(id)sender {
    
    if([self.delegate respondsToSelector:@selector(userPressedOnBTAmendFooterActionView:)]){
        
        [self.delegate userPressedOnBTAmendFooterActionView:self];
    }
}

@end
