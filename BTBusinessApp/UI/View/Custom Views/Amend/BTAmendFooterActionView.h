//
//  BTAmendFooterActionView.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/2/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTAmendFooterActionView;

@protocol BTAmendFooterActionViewDelegate <NSObject>

- (void)userPressedOnBTAmendFooterActionView:(BTAmendFooterActionView *)view;

@end

@interface BTAmendFooterActionView : UIView
@property(nonatomic, assign) id<BTAmendFooterActionViewDelegate>delegate;

- (void)setTitle:(NSString *)titleText;

@end
