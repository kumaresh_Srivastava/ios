//
//  BTFaultTrackerSiteContactTableSectionHeader.m
//  SampleViewApplication
//
//  Created by vectoscalar on 11/11/16.
//  Copyright © 2016 VS. All rights reserved.
//

#import "BTFaultTrackerSiteContactTableSectionHeader.h"

@implementation BTFaultTrackerSiteContactTableSectionHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (void)updateSectionTitle:(NSString *)title
{
    self.sectionTitleLabel.text = title;
    [self layoutIfNeeded];
}

@end
