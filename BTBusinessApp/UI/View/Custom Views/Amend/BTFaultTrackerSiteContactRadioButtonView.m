//
//  BTFaultTrackerSiteContactRadioButtonView.m
//  SampleViewApplication
//
//  Created by vectoscalar on 11/11/16.
//  Copyright © 2016 VS. All rights reserved.
//

#import "BTFaultTrackerSiteContactRadioButtonView.h"

#define kRadioUnselectedImage @"radio_unselected"
#define kRadioSelectedImage @"radio_selected"

#define kXPadding 16
#define kYPadding 0

#define kFontName @"BT-Regular"
#define kFontSize 16
#define kGap 6
#define kXGap 8

#define kRadioButtonSize 22

#define kBaseTag 78876

@implementation BTFaultTrackerSiteContactRadioButtonView
@synthesize delegate;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame andOptionArray:(NSArray *)optionArray andTitleHeading:(NSString *)titleHeading andSelectedIndex:(NSInteger)selectedIndex
{
    self = [super init];
    
    if(self)
    {
        [self updateViewWithArray:optionArray andTitle:titleHeading andSlectedIndex:selectedIndex andCGPoint:CGPointMake(frame.origin.x, frame.origin.y)];
        self.backgroundColor = [UIColor whiteColor];
    }
    return  self;
}


- (void)updateViewWithArray:(NSArray *)optionArray andTitle:(NSString *)title andSlectedIndex:(NSInteger)selectedIndex andCGPoint:(CGPoint)cgPoint
{
    
    //Create Title

    CGFloat maxWidth = [[UIScreen mainScreen] bounds].size.width - 2*kXPadding;
    
    CGFloat yPos = kYPadding;
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    titleLabel.numberOfLines = 2;
    
    titleLabel.text = title;
    titleLabel.font = [UIFont fontWithName:kFontName size:kFontSize];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    CGSize titleLabelSize = [BTFaultTrackerSiteContactRadioButtonView getTheSizeForText:title andFont:titleLabel.font toFitInWidth:maxWidth];
    titleLabel.frame = CGRectMake(kXPadding, yPos, maxWidth, titleLabelSize.height);
    
    [self addSubview:titleLabel];
    yPos += titleLabelSize.height;
    yPos += kGap;
    
    
    //Create Radio Buttons
   
    for(int i = 0;i<[optionArray count];i++)
    {
        NSString *optionText  = [optionArray objectAtIndex:i];
        
        CGFloat xPos = kXPadding;
        
        UIImageView *radioButtonImageView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, kRadioButtonSize, kRadioButtonSize)];
        radioButtonImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        radioButtonImageView.tag = kBaseTag+i;
        
        //Check if it is preselected or not
        if(i == selectedIndex)
        {
            radioButtonImageView.image = [UIImage imageNamed:kRadioSelectedImage];
        }
        else
        {
           radioButtonImageView.image = [UIImage imageNamed:kRadioUnselectedImage];
        }
        
        [self addSubview:radioButtonImageView];
        
        xPos += kRadioButtonSize+kXGap;
        
        //Create OPtion text Label
        
        UILabel *optionLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        CGFloat availableWidth = maxWidth - xPos;
        
        optionLabel.text = optionText;
        optionLabel.numberOfLines = 4;
        optionLabel.font = [UIFont fontWithName:kFontName size:kFontSize];
        optionLabel.textAlignment = NSTextAlignmentLeft;
        CGSize optionLabelSize = [BTFaultTrackerSiteContactRadioButtonView getTheSizeForText:optionLabel.text andFont:optionLabel.font toFitInWidth:availableWidth];
        optionLabel.frame = CGRectMake(xPos, yPos, availableWidth, optionLabelSize.height);
        
        [self addSubview:optionLabel];
        
        //Make a clickable UIBUtton above Content
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        //TODO::Manage Tapable Area if Needed
        button.frame = CGRectMake(kXPadding, yPos, optionLabelSize.width, optionLabelSize.height);
        
        
        [button setTitle:optionText forState:UIControlStateNormal];
        [button setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
        button.backgroundColor = [UIColor clearColor];
        [button addTarget:self action:@selector(userSelectedRadioButton:) forControlEvents:UIControlEventTouchUpInside];
         button.tag = kBaseTag+i;
        
        [self addSubview:button];
        
        yPos += optionLabelSize.height;
        
        if(optionLabelSize.height > kRadioButtonSize)
        {
               yPos += kGap;
        }
        else
        {
               yPos += 1.5*kGap;
        }
        
       
        
        

    }
    
    self.frame = CGRectMake(cgPoint.x ,cgPoint.y,[[UIScreen mainScreen] bounds].size.width, yPos);
    
}


#pragma mark - Button Action
- (void)userSelectedRadioButton:(id)sender
{
    UIView *senderView = sender;

    int selectedTag = (int)[senderView tag];
    
    UIButton *selectedButton = (UIButton *)sender;
    
    int selectedIndex = selectedTag%kBaseTag;
    
    
    for(UIView *view in self.subviews)
    {
        if([view isKindOfClass:[UIImageView class]])
        {
            UIImageView *imgView = (UIImageView*)view;
            
            if(imgView.tag == selectedTag)
            {
                imgView.image = [UIImage imageNamed:kRadioSelectedImage];
            }
            else
            {
                imgView.image = [UIImage imageNamed:kRadioUnselectedImage];
                
            }

        }
        
    }
    
    
    [delegate userDidSelectedOption:selectedButton.titleLabel.text atIndex:selectedIndex onView:self];
    
    
}


- (void)selectRadioButtonAtIndex:(NSInteger)index
{
    
    int selectedIndex = (int)index;

    
    for(UIView *view in self.subviews)
    {
        if([view isKindOfClass:[UIImageView class]])
        {
            UIImageView *imgView = (UIImageView*)view;
            
            int selectedTag = kBaseTag+selectedIndex;
            
            if(imgView.tag == selectedTag)
            {
                imgView.image = [UIImage imageNamed:kRadioSelectedImage];
                
            }
            else
            {
                imgView.image = [UIImage imageNamed:kRadioUnselectedImage];
    
                
            }
            
       
            
        }
        
    }

}


#pragma mark - Dynamic Text Size
+ (CGSize)getTheSizeForText:(NSString *)text andFont:(UIFont*)font toFitInWidth:(CGFloat)width
{
    
    CGRect textRect = [text boundingRectWithSize:CGSizeMake(width, 999)
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName:font}
                                         context:nil];
    
    return textRect.size;
}


+ (CGFloat)getFooterHeightForOptionArray:(NSArray *)optionArray withTitleText:(NSString *)title
{
    //Create Title
    
    CGFloat maxWidth = [[UIScreen mainScreen] bounds].size.width - 2*kXPadding;
    
    CGFloat yPos = kYPadding;
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    
    titleLabel.text = title;
    titleLabel.font = [UIFont fontWithName:kFontName size:kFontSize];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    CGSize titleLabelSize = [BTFaultTrackerSiteContactRadioButtonView getTheSizeForText:title andFont:titleLabel.font toFitInWidth:maxWidth];
    titleLabel.frame = CGRectMake(kXPadding, yPos, maxWidth, titleLabelSize.height);

    yPos += titleLabelSize.height;
    yPos += kGap;
    
    
    //Create Radio Buttons
    
    for(int i = 0;i<[optionArray count];i++)
    {
        NSString *optionText  = [optionArray objectAtIndex:i];
        
        CGFloat xPos = kXPadding;
        
        UIImageView *radioButtonImageView = [[UIImageView alloc] initWithFrame:CGRectMake(xPos, yPos, kRadioButtonSize, kRadioButtonSize)];
        radioButtonImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        radioButtonImageView.tag = kBaseTag+i;
        
        
        xPos += kRadioButtonSize+kXGap;
        
        //Create OPtion text Label
        
        UILabel *optionLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        CGFloat availableWidth = maxWidth - xPos;
        
        optionLabel.text = optionText;
        optionLabel.numberOfLines = 4;
        optionLabel.font = [UIFont fontWithName:kFontName size:kFontSize];
        optionLabel.textAlignment = NSTextAlignmentLeft;
        CGSize optionLabelSize = [BTFaultTrackerSiteContactRadioButtonView getTheSizeForText:optionLabel.text andFont:optionLabel.font toFitInWidth:availableWidth];
        optionLabel.frame = CGRectMake(xPos, yPos, availableWidth, optionLabelSize.height);
        

        
        //Make a clickable UIBUtton above Content
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        //TODO::Manage Tapable Area if Needed
        button.frame = CGRectMake(kXPadding, yPos, optionLabelSize.width, optionLabelSize.height);
        
        
        [button setTitle:optionText forState:UIControlStateNormal];
        [button setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
        button.backgroundColor = [UIColor clearColor];
        [button addTarget:self action:@selector(userSelectedRadioButton:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = kBaseTag+i;
        
        
        yPos += optionLabelSize.height;
        
        if(optionLabelSize.height > kRadioButtonSize)
        {
            yPos += kGap;
        }
        else
        {
            yPos += 1.5*kGap;
        }
        
        
    }

    return yPos;
    
}

@end
