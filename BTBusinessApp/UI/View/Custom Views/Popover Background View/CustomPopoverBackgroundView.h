//
//  CustomPopoverBackgroundView.h
//  BTBusinessApp
//
//  Created by VectoScalar on 1/12/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

//Salman:This class is implemented to hide popover arrow and to add shadow to the popver
#import <UIKit/UIKit.h>

@interface CustomPopoverBackgroundView : UIPopoverBackgroundView
@end
