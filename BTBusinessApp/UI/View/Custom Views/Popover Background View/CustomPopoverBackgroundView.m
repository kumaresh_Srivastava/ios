//
//  CustomPopoverBackgroundView.m
//  BTBusinessApp
//
//  Created by VectoScalar on 1/12/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "CustomPopoverBackgroundView.h"

@interface CustomPopoverBackgroundView()

@property (nonatomic, readwrite) UIPopoverArrowDirection arrowDirection;
@property (nonatomic, readwrite) CGFloat arrowOffset;

@end

@implementation CustomPopoverBackgroundView
@synthesize arrowDirection  = _arrowDirection;
@synthesize arrowOffset     = _arrowOffset;

-(id)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        //Adding shadow
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 4.0;
        self.layer.shadowRadius  = 3.0f;
        self.layer.shadowColor   = [BrandColours colourBtPrimaryColor].CGColor;
        self.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
        self.layer.shadowOpacity = 0.6f;
        self.layer.masksToBounds = NO;
        self.layer.borderWidth = 1.5f;
        self.layer.borderColor = [[BrandColours colourBtNeutral60] colorWithAlphaComponent:0.5].CGColor;
        self.arrowOffset = 0.0;
        
    }
    return self;
}



-(void)layoutSubviews{
    
    
}

- (void)setArrowDirection:(UIPopoverArrowDirection)arrowDirection
{
    _arrowDirection = arrowDirection;
    return;
}


- (void)setArrowOffset:(CGFloat)arrowOffset
{
    _arrowOffset = arrowOffset;
    return;
}


+ (CGFloat)arrowBase
{
    return 0.0;
}

+ (CGFloat)arrowHeight
{
    return 0.0;
}

+ (UIEdgeInsets)contentViewInsets
{
    return UIEdgeInsetsMake(1.0,1.0,1.0,1.0);
}


+ (BOOL)wantsDefaultContentAppearance
{
    return NO;
}

@end
