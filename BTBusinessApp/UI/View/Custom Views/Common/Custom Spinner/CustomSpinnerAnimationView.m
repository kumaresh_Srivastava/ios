//
//  CustomSpinnerAnimationView.m
//  BTBusinessApp
//
//  Created by Accolite on 09/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "CustomSpinnerAnimationView.h"
#import <QuartzCore/QuartzCore.h>

#define kLoadingRingSide 42
#define kGapBetweenRingAndText 10

@interface CustomSpinnerAnimationView()

@property(nonatomic, assign) CGFloat anglePendingInPhase1Begin;
@property(nonatomic, assign) CGFloat anglePendingInPhase1Middle;
@property(nonatomic, assign) CGFloat anglePendingInPhase1End;
@property(nonatomic, assign) CGFloat anglePendingInPhase2Begin;
@property(nonatomic, assign) CGFloat anglePendingInPhase2Middle;
@property(nonatomic, assign) CGFloat anglePendingInPhase2End;
@property(nonatomic, strong) UIImageView *ringImageView;

@end

@implementation CustomSpinnerAnimationView {
    BOOL _animate;
}

- (id)initWithText:(NSString *)loadingText withLoadingImage:(UIImage *)loadingRingImage
{
    self = [super initWithFrame:CGRectMake(0,
                                           0,
                                           0,
                                           0)];
    
    
    if(self)
    {
        [self setIsStopped:YES];
        _animate = NO;

        _animationView = [LOTAnimationView animationNamed:@"Dotty-Loop"];
        [_animationView setContentMode:UIViewContentModeScaleAspectFit];
        [_animationView setBackgroundColor:[UIColor clearColor]];
        //[_animationView setBackgroundColor:[UIColor greenColor]];
        [_animationView setLoopAnimation:false];
        
        [_animationView playToFrame:[NSNumber numberWithInteger:31]
                                 withCompletion:^(BOOL animationFinished)
         {
             [self->_animationView setLoopAnimation:true];
             [self-> _animationView playFromFrame:[NSNumber numberWithInteger:32]
                                               toFrame:[NSNumber numberWithInteger:93]
                                        withCompletion:nil];
         }];
        //_animationView.translatesAutoresizingMaskIntoConstraints = false;
        
        _animationOverlayView = [[UIView alloc]initWithFrame:CGRectMake(0,0, 400, 400)];
        //_animationOverlayView.translatesAutoresizingMaskIntoConstraints=false;
        [_animationOverlayView addSubview:_animationView];
        
        [self addSubview:_animationOverlayView];
        
        UIFont *labelTextFont = [UIFont fontWithName:@"BTFont-Regular" size:16.0];
        CGSize textLabelSize = [loadingText sizeWithAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:16.0]}];
        _loadingTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,
                                                                      _animationView.frame.origin.y + _animationView.frame.size.height + kGapBetweenRingAndText,
                                                                      textLabelSize.width,
                                                                      textLabelSize.height)];
        _loadingTextLabel.text = loadingText;
        _loadingTextLabel.backgroundColor = [UIColor clearColor];
        _loadingTextLabel.font = labelTextFont;
        _loadingTextLabel.textColor = [UIColor blackColor];
    }
    
    [self setFrame: CGRectMake(0,0,200,150)];
    
    return self;
}

- (id)initWithText:(NSString *)loadingText loadingImage:(UIImage *)loadingRingImage andLoaderImageSize:(CGFloat)ringSide
{
    self = [super initWithFrame:CGRectMake(0,
                                           0,
                                           0,
                                           0)];
    
    
    if(self)
    {
        [self setIsStopped:YES];
        _animate = NO;
        
        _animationView = [LOTAnimationView animationNamed:@"Dotty-Loop"];
        [_animationView setContentMode:UIViewContentModeScaleAspectFit];
        [_animationView setBackgroundColor:[UIColor clearColor]];
        //[_animationView setBackgroundColor:[UIColor blackColor]];
        [_animationView setLoopAnimation:false];
        
        [_animationView playToFrame:[NSNumber numberWithInteger:31]
                     withCompletion:^(BOOL animationFinished)
         {
             [self->_animationView setLoopAnimation:true];
             [self-> _animationView playFromFrame:[NSNumber numberWithInteger:32]
                                          toFrame:[NSNumber numberWithInteger:93]
                                   withCompletion:nil];
         }];
        //_animationView.translatesAutoresizingMaskIntoConstraints = false;
        
        _animationOverlayView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 400, 400)];
        //_animationOverlayView.translatesAutoresizingMaskIntoConstraints=false;
        [_animationOverlayView addSubview:_animationView];
        
        [self addSubview:_animationOverlayView];
        
        UIFont *labelTextFont = [UIFont fontWithName:@"BTFont-Regular" size:16.0];
        CGSize textLabelSize = [loadingText sizeWithAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:16.0]}];
        _loadingTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,
                                                                      _animationView.frame.origin.y + _animationView.frame.size.height + kGapBetweenRingAndText,
                                                                      textLabelSize.width,
                                                                      textLabelSize.height)];
        _loadingTextLabel.text = loadingText;
        _loadingTextLabel.backgroundColor = [UIColor clearColor];
        _loadingTextLabel.font = labelTextFont;
        _loadingTextLabel.textColor = [UIColor blackColor];
        [self addSubview:_loadingTextLabel];
        
    }
    
    return self;
}

- (void)startAnimation
{
//    self.isStopped = NO;
//    _animate = YES;
//    [self startAnimationFromBeginning];
}

- (void)stopAnimation
{
//    self.isStopped = YES;
//    _animate = NO;
//    [self.layer removeAllAnimations];
}

@end
