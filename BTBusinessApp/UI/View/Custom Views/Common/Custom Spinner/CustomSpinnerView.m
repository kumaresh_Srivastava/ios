//
//  CustomSpinnerView.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 03/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "CustomSpinnerView.h"
#import "CustomSpinnerAnimationView.h"

@interface CustomSpinnerView () {
    CustomSpinnerAnimationView *_indicatorView;
}

@property(atomic) BOOL isAnimationInProgress;

@end

@implementation CustomSpinnerView


- (void)awakeFromNib{
    
    [super awakeFromNib];

   
    [self lazyLoadSpinView];
   
}

- (void) lazyLoadSpinView{
    [self performSelector:@selector(loadingSpinView) withObject:nil afterDelay:0.03];
}

- (void) loadingSpinView{
    
    if(_indicatorView){
        [_indicatorView removeFromSuperview];
        _indicatorView = nil;
    }
    _indicatorView = [[CustomSpinnerAnimationView alloc] initWithText:@"" withLoadingImage:[UIImage imageNamed:@"Spinner"]];
    _indicatorView.translatesAutoresizingMaskIntoConstraints = NO;
    _indicatorView.frame = CGRectMake(0, 0, self.spinnerView.frame.size.width, self.spinnerView.frame.size.height);
   // NSDictionary *indicstorViews = @{@"indicatorView":_indicatorView};
    [self.spinnerView addSubview:_indicatorView];
   // [self.spinnerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-12-[indicatorView]-|" options:0 metrics:nil views:indicstorViews]];
    //[self.spinnerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-12-[indicatorView]|" options:0 metrics:nil views:indicstorViews]];
    
    
    [self stopAnimatingLoadingIndicatorView];
    [self startAnimatingLoadingIndicatorView];
}

- (void)startAnimatingLoadingIndicatorView {
    
    if(self.isAnimationInProgress){
        return;
    }
    
    [_indicatorView setIsStopped:NO];
    self.isAnimationInProgress = YES;
    [_indicatorView startAnimation];
}

- (void)stopAnimatingLoadingIndicatorView {
    [_indicatorView setIsStopped:YES];
    self.isAnimationInProgress = NO;
    [_indicatorView stopAnimation];
}

@end
