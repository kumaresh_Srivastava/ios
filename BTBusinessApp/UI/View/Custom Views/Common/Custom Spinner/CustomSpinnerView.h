//
//  CustomSpinnerView.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 03/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomSpinnerView : UIView

@property (weak, nonatomic) IBOutlet UIView *spinnerView;
@property (weak, nonatomic) IBOutlet UIView *spinnerBackgroundView;
@property (weak, nonatomic) IBOutlet UILabel *loadingLabel;

- (void)startAnimatingLoadingIndicatorView;
- (void)stopAnimatingLoadingIndicatorView;
- (void) lazyLoadSpinView;

@end
