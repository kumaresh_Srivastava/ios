//
//  CustomSpinnerAnimationView.h
//  BTBusinessApp
//
//  Created by Accolite on 09/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Lottie/Lottie.h>

@interface CustomSpinnerAnimationView : UIView {
    LOTAnimationView *_animationView;
    UIView *_animationOverlayView;
    UIImageView *_cupImageView;
    UILabel *_loadingTextLabel;
}

@property (nonatomic,assign) BOOL isStopped;
//- (id)initWithText:(NSString *)loadingText;
- (id)initWithText:(NSString *)loadingText withLoadingImage:(UIImage *)image;
- (id)initWithText:(NSString *)loadingText loadingImage:(UIImage *)loadingRingImage andLoaderImageSize:(CGFloat)ringSide;
- (void)startAnimation;
- (void)stopAnimation;

@end
