//
//  ViewWithRoundedHairline.h
//  My BT
//
//  Created by Sam McNeilly on 13/11/2015.
//  Copyright © 2015 BT. All rights reserved.
//

#import "ViewWithRoundedHairline.h"
#import "BrandColours.h"

@implementation ViewWithRoundedHairline

- (id) initWithCoder : (NSCoder*) decoder {
    self = [super initWithCoder:decoder];
    
    [[self layer] setCornerRadius:3.0];
    [[self layer] setBorderColor:[[BrandColours colourBtNeutral60] CGColor]];
    [[self layer] setBorderWidth:1.0];
    [[self layer] setBackgroundColor:[[UIColor whiteColor] CGColor]];
    
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];
}

@end
