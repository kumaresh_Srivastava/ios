//
//  CustomPageControl.m
//  BTBusinessApp
//
//  Created by vectoscalar on 22/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "CustomPageControl.h"

@implementation CustomPageControl

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        _activeImage = [UIImage imageNamed:@"selectedBullet"];
        _inactiveImage = [UIImage imageNamed:@"unselectedBullet"];
        self.pageIndicatorTintColor = [UIColor clearColor];
        self.currentPageIndicatorTintColor = [UIColor clearColor];
    }
    return self;
}

-(void)updateDotssss
{
    int i = 0;
    for (UIView *view in self.subviews)
    {
        if([view isKindOfClass:[UIImageView class]])
        {
        
        UIImageView* dot = [self.subviews objectAtIndex:i];
        dot.frame = CGRectMake(dot.frame.origin.x, dot.frame.origin.y, 14, 14.5);
        if (i == self.currentPage)
            dot.image = _activeImage;
        else
            dot.image = _inactiveImage;
        }
        i++;
    }
}

-(void) updateDots
{
    self.tintColor = [UIColor clearColor];
    
    for (int i = 0; i < [self.subviews count]; i++)
    {
        
        if (i == self.currentPage) {
            
            UIImageView* dot = [self.subviews objectAtIndex:i];
            
            for(UIView *v in dot.subviews)
            {
                [v removeFromSuperview];
            }
            
            dot.backgroundColor = [UIColor redColor];
            UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(-2, -2, 12, 12)];
           
                imgView.image = _activeImage;
                [dot addSubview:imgView];
          
        } else {
            
            UIImageView* dot = [self.subviews objectAtIndex:i];
            
            for(UIView *v in dot.subviews)
            {
                [v removeFromSuperview];
            }
            
            UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 7,7)];
          
                imgView.image = _inactiveImage;
                [dot addSubview:imgView];
            
        }
    }
}

-(void)setCurrentPage:(NSInteger)page
{
    [super setCurrentPage:page];
    [self updateDots];
}

@end
