//
//  TextFieldWithPlaceholderText.m
//  My BT
//
//  Created by Sam McNeilly on 13/11/2015.
//  Copyright © 2015 BT. All rights reserved.
//

#import "TextFieldWithPlaceholderText.h"

@implementation TextFieldWithPlaceholderText

- (id) initWithCoder : (NSCoder*) decoder {
    self = [super initWithCoder:decoder];
    
    [self setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:[self placeholder] attributes:@{ NSForegroundColorAttributeName : [BrandColours colourMyBtMediumGrey] }]];
    
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];
}

@end
