//
//  BTRetryView.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 02/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTRetryView;

@protocol BTRetryViewDelegate <NSObject>

@required
- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView;

@end

@interface BTRetryView : UIView {
    
}

@property (assign,nonatomic) BOOL isInternetStripShown;
@property (nonatomic, weak) id <BTRetryViewDelegate> retryViewDelegate;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *retryButton;

@property (weak, nonatomic) IBOutlet UIView *noInternetStrip;

- (void)updateRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow;

- (void)updateRetryViewWithInternetStrip:(BOOL)needInternetStrip TryAgain:(BOOL)needTryAgain errorHeadline:(NSString*)headline errorDetail:(NSString*)detail;
- (void)updateRetryViewWithErrorHeadline:(NSString*)headline errorDetail:(NSString*)detail andButtonText:(NSString*)buttonLabel;
- (void)updateRetryViewDetailWithAttributedString:(NSAttributedString*)attributedString;

@end
