//
//  BTRetryView.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 02/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTRetryView.h"
#import "BrandColours.h"
#import "AppManager.h"
#import "AppConstants.h"

#define kDefaultMessage @"Something went wrong"
#define kDefaultSubMessage @"Sorry, there's been an unexpected error.\nPlease try again later."

@interface BTRetryView()

@property (weak, nonatomic) IBOutlet UILabel *errorMessageLabel;
@property (weak, nonatomic) IBOutlet UITextView *errorDetailMessageTextView;

@end

@implementation BTRetryView

- (void)awakeFromNib
{
    [super awakeFromNib];

    self.noInternetStrip.hidden = YES;
    self.retryButton.hidden = YES;
    self.titleLabel.text = @"";
    self.errorMessageLabel.text = @"";
    self.errorDetailMessageTextView.text = @"";
}

- (void)drawRect:(CGRect)rect {
    // Drawing code
    self.retryButton.layer.borderColor = [BrandColours colourBtNeutral60].CGColor;
}



#pragma mark - Public Methods

- (void)updateRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow
{
    if(internetStripNeedToShow)
    {
        self.noInternetStrip.hidden = NO;
        self.retryButton.hidden = NO;
        self.titleLabel.text = @"Oops!";
        self.errorMessageLabel.text = kDefaultMessage;
        self.errorDetailMessageTextView.text = kNoConnectionMessage;
    }
    else
    {
        self.noInternetStrip.hidden = YES;
        self.retryButton.hidden = NO;
        self.titleLabel.text = @"Oops!";
        self.errorMessageLabel.text = kDefaultMessage;
        self.errorDetailMessageTextView.text = kDefaultSubMessage;
    }

}

- (void)updateRetryViewWithInternetStrip:(BOOL)needInternetStrip TryAgain:(BOOL)needTryAgain errorHeadline:(NSString *)headline errorDetail:(NSString *)detail
{
    self.noInternetStrip.hidden = !needInternetStrip;
    self.retryButton.hidden = !needTryAgain;
    self.titleLabel.text = @"";
    self.errorMessageLabel.text = headline?headline:@"";
    self.errorDetailMessageTextView.text = detail?detail:@"";
}

- (void)updateRetryViewWithErrorHeadline:(NSString*)headline errorDetail:(NSString*)detail andButtonText:(NSString*)buttonLabel{
    self.noInternetStrip.hidden = YES;
    self.retryButton.hidden = NO;
    self.errorMessageLabel.text = headline?headline:@"";
    self.errorDetailMessageTextView.text = detail?detail:@"";
    [self.retryButton setTitle:buttonLabel?buttonLabel:@"" forState:UIControlStateNormal];
}

- (void)updateRetryViewDetailWithAttributedString:(NSAttributedString*)attributedString {
    self.errorDetailMessageTextView.attributedText = attributedString;
    self.errorDetailMessageTextView.linkTextAttributes = @{NSForegroundColorAttributeName:[BrandColours colourTextBTPurplePrimaryColor]};
    //self.errorDetailMessageTextView.selectable = YES;
}

- (IBAction)retryButtonAction:(id)sender {
        [self.retryViewDelegate userPressedRetryButtonOfRetryView:self];
}

@end
