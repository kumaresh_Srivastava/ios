//
//  CustomPageControl.h
//  BTBusinessApp
//
//  Created by vectoscalar on 22/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPageControl : UIPageControl
{
//    UIImage* activeImage;
//    UIImage* inactiveImage;
}
@property(nonatomic, strong) UIImage* activeImage;
@property(nonatomic, strong) UIImage* inactiveImage;

@end
