//
//  TextFieldWithPlaceholderText.h
//  My BT
//
//  Created by Sam McNeilly on 13/11/2015.
//  Copyright © 2015 BT. All rights reserved.
//

#import <HTAutocompleteTextField/HTAutocompleteTextField.h>
#import <UIKit/UIKit.h>
#import "BrandColours.h"

@interface TextFieldWithPlaceholderText : HTAutocompleteTextField

@end
