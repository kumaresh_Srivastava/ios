//
//  BTAlertBannerView.m
//  BTBusinessApp
//
//  Created by VectoScalar on 10/24/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTAlertBannerView.h"
#import "BrandColours.h"

@interface BTAlertBannerView()
{

}
@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end

@implementation BTAlertBannerView

- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if(self){
        
        self.backgroundColor = [BrandColours colourMyBtGreen];
        self.alpha = 0.0;
    }
    
    
    return self;
}



- (void)setMessage:(NSString *)message{
    
    self.textLabel .text = message;
}


- (void)showAnimated:(BOOL)animated{
    
    self.alpha = 0.0;
    
    [UIView animateWithDuration:animated?kBannerAnimationTime:0.0 animations:^{
        
        self.alpha = 1.0;
        
    } completion:^(BOOL finished) {
        
    }];
    
}


- (void)hideAnimated:(BOOL)animated{
    

    [UIView animateWithDuration:animated?kBannerAnimationTime:0.0 animations:^{
        
        self.alpha = 0.0;
        
    } completion:^(BOOL finished) {
        
    }];

}

@end
