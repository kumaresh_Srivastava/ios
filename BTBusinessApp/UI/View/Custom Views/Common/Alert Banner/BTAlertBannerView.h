//
//  BTAlertBannerView.h
//  BTBusinessApp
//
//  Created by VectoScalar on 10/24/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>


#define kBannerAnimationTime 0.5

@interface BTAlertBannerView : UIView

- (void)showAnimated:(BOOL)animated;
- (void)hideAnimated:(BOOL)animated;

- (void)setMessage:(NSString *)message;

@end
