//
//  BTCustomInputAlertView.h
//  BTBusinessApp
//
//  Created by vectoscalar on 18/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomSpinnerAnimationView.h"

@class BTCustomInputAlertView;

@protocol BTCustomInputAlertViewDelegate <NSObject>

- (void)userDidPressCancelOnCustomInputAlertView:(BTCustomInputAlertView*)alertView;

- (void)userDidPressConfirmOnCustomInputAlertView:(BTCustomInputAlertView*)alertView withPassword:(NSString *)passwordText;

@end



@interface BTCustomInputAlertView : UIView <UITextFieldDelegate>
{
    CGFloat alertViewOriginalYConstant,keyboardOriginalHeight;
     CustomSpinnerAnimationView *loadingView;
    UIView *loadingContainerView;
}

@property (weak, nonatomic) IBOutlet UIView *alertView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *detailTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UIView *placeholderView;
@property (weak, nonatomic) IBOutlet UILabel *loadingLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertViewYConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *errorHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passwordTopConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *borderrTopConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *detailTitleHeightConstraint;

@property (assign,nonatomic) id <BTCustomInputAlertViewDelegate> delegate;




- (IBAction)userPressedCancelButton:(id)sender;

- (IBAction)userPressedSubmitButton:(id)sender;


//Public methods to manage alertView
- (void)showErrorOnCustomInputWithWithErrro:(NSString *)errorMessage;

- (void)removeErrorMeesageFromCustomInputAlertView;

- (void)updateCustomAlertViewWithTitle:(NSString *)title andMessage:(NSString*)message;

- (void)updateTextFieldPlaceholderText:(NSString *)placeholderText andIsSecureEntry:(BOOL)isSecureEntry andNeedToShowRightView:(BOOL)isRightViewVisible;

- (void)updateLoadingMessage:(NSString *)loadingMessage;

- (void)updateAlertButtonTitleForActionButton:(NSString *)actionButtonTitle andCancelButtonTitle:(NSString *)cancelButtonTitle;

- (void)makeTitleLabelForTextFieldWithTitle:(NSString *)title;

- (void)startLoading;

- (void)stopLoading;

- (void)showKeyboard;

- (void)deRegisterKeyboardNotification;
@end
