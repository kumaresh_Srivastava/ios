//
//  BTCustomDisplayView.h
//  BTBusinessApp
//
//  Created by Accolite on 30/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTCustomDisplayView : UIView
@property (weak, nonatomic) IBOutlet UILabel *displayLabel;

@end
