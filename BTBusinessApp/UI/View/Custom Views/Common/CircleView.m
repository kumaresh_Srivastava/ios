//
//  CircleView.m
//  My BT
//
//  Created by Sam McNeilly on 17/11/2015.
//  Copyright © 2015 BT. All rights reserved.
//

#import "CircleView.h"

@implementation CircleView

- (id) initWithCoder : (NSCoder*) decoder {
    self = [super initWithCoder:decoder];
    
    [[self layer] setCornerRadius:([self bounds].size.height / 2.0)];
    [[self layer] setMasksToBounds:true];
    
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];
}

@end
