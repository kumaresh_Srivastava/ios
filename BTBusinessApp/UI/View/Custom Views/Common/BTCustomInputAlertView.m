//
//  BTCustomInputAlertView.m
//  BTBusinessApp
//
//  Created by vectoscalar on 18/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTCustomInputAlertView.h"
#import "BrandColours.h"
#import "UITextField+BTTextFieldProperties.h"


@interface BTCustomInputAlertView()
{
    BOOL secureModeOn;
}


@end

@implementation BTCustomInputAlertView
@synthesize  delegate;
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 
 */

- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    self.alertView.layer.cornerRadius = 15.0f;
    self.placeholderView.layer.cornerRadius = 15.0f;
    self.placeholderView.backgroundColor = self.alertView.backgroundColor;
    self.passwordTextField.delegate = self;
    
    alertViewOriginalYConstant = self.alertViewYConstraint.constant;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    //Modify the carror color of UITextfield
    
    [self createLoadingView];
    
    [self addTapGestureToBlurView];
    
    [self customizeTextField];
    
}

- (void)deRegisterKeyboardNotification
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


- (void)updateLoadingMessage:(NSString *)loadingMessage
{
    self.loadingLabel.text = loadingMessage;
}


- (void)customizeTextField
{
    [self.passwordTextField getBTTextFieldStyle];
}

- (void)updateTextFieldPlaceholderText:(NSString *)placeholderText andIsSecureEntry:(BOOL)isSecureEntry andNeedToShowRightView:(BOOL)isRightViewVisible
{
    
    secureModeOn = isSecureEntry;
    self.passwordTextField.placeholder = placeholderText;
    [self.passwordTextField setSecureTextEntry:isSecureEntry];
    
    if(isRightViewVisible)
    {
        [self addRightViewToTextField];
    }
    
    if(!isSecureEntry)
    {
       self.passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    }
}


- (void)addRightViewToTextField
{
    UIImage *eyeImage = [UIImage imageNamed:@"icon_password_eye_gray"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:eyeImage forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"icon_password_eye_purple.png"] forState:UIControlStateSelected];
    
    
    button.frame = CGRectMake(0.0,
                              0.0,
                              37,
                              20);
    
    button.imageEdgeInsets = UIEdgeInsetsMake(0,
                                              0,
                                              0,
                                              7.0);
    [button addTarget:self action:@selector(showTextButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    self.passwordTextField.rightView = button;
    self.passwordTextField.rightViewMode = UITextFieldViewModeAlways;

}


- (void)showTextButtonPressed:(id)sender{
    
    UIButton *button = (UIButton *)sender;
    button.selected = !button.selected;
    
    ((UITextField *)[sender superview]).secureTextEntry = !((UITextField *)[sender superview]).isSecureTextEntry;
    
}


- (void)addTapGestureToBlurView
{
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOutside)];
    [self.blurView addGestureRecognizer:gesture];
}


- (void)userTappedOutside
{
    [self endEditing:YES];
}


#pragma mark - Update Alert Message
- (void)updateCustomAlertViewWithTitle:(NSString *)title andMessage:(NSString*)message
{
    
    self.titleLabel.text = title;
    if(!message || message.length == 0)
    {
        self.detailTitleHeightConstraint.constant = 0;
        self.detailTitleLabel.text = @"";
    }
    else
    {
        self.detailTitleLabel.text = message;
        
        CGSize messageHeight = [self getTheSizeForText:message andFont:[UIFont systemFontOfSize:14] toFitInWidth:self.detailTitleLabel.frame.size.width];
        
        self.detailTitleHeightConstraint.constant = messageHeight.height;
    }
    
    self.errorLabel.text = @"";
    self.errorHeightConstraint.constant = 0;
    
    [self updateConstraints];
    [self layoutIfNeeded];
    
}


- (void)showErrorOnCustomInputWithWithErrro:(NSString *)errorMessage
{
    self.errorLabel.text = errorMessage;
    
    CGSize errorHeight = [self getTheSizeForText:errorMessage andFont:self.errorLabel.font toFitInWidth:self.errorLabel.frame.size.width];
    
    self.errorHeightConstraint.constant =errorHeight.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        [self layoutIfNeeded];
    }];
    [UIView commitAnimations];
    
    [self manageAlertViewWithKeyboardHeight:keyboardOriginalHeight];
    
}


- (void)removeErrorMeesageFromCustomInputAlertView
{
    self.errorLabel.text = @"";
    self.errorHeightConstraint.constant = 0;
    [UIView animateWithDuration:0.3 animations:^{
        [self layoutIfNeeded];
    }];
    [UIView commitAnimations];
    
}

- (void)showKeyboard
{
    [self performSelector:@selector(makeFirstResponder) withObject:nil afterDelay:0.006];
}


#pragma mark - Button Actions
- (IBAction)userPressedCancelButton:(id)sender {
    [self endEditing:YES];
       [delegate userDidPressCancelOnCustomInputAlertView:self];
  
}

- (IBAction)userPressedSubmitButton:(id)sender {
    
    [delegate userDidPressConfirmOnCustomInputAlertView:self withPassword:self.passwordTextField.text];
    [self endEditing:YES];
    
}


- (CGSize)getTheSizeForText:(NSString *)text andFont:(UIFont*)font toFitInWidth:(CGFloat)width
{
    
    CGRect textRect = [text boundingRectWithSize:CGSizeMake(width, 999)
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName:font}
                                         context:nil];
    
    return textRect.size;
}

#pragma mark  - Keyboard Delegate


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self endEditing:YES];
    return YES;
}

- (void)keyboardWillHide:(NSNotification *)n
{
    
    self.alertViewYConstraint.constant = alertViewOriginalYConstant;
    
    [UIView animateWithDuration:0.2 animations:^{
        [self layoutIfNeeded];
    }];
    [UIView commitAnimations];
    
    loadingContainerView.center = self.alertView.center;
    
}

- (void)keyboardWillShow:(NSNotification *)n
{
    
    NSDictionary* userInfo = [n userInfo];
    
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    keyboardOriginalHeight = keyboardSize.height;
    
    [self manageAlertViewWithKeyboardHeight:keyboardSize.height];
    
}

- (void)manageAlertViewWithKeyboardHeight:(CGFloat)keyboardHeight
{
    CGFloat alertViewPostition = self.alertView.frame.origin.y+ self.alertView.frame.size.height;

    
    CGFloat visibleArea = [[UIScreen mainScreen] bounds].size.height - keyboardHeight;
    
    if(alertViewPostition > visibleArea)
    {
        CGFloat extraY = alertViewPostition - visibleArea+10;
        
        self.alertViewYConstraint.constant -= extraY;
        
    }
    [UIView animateWithDuration:0.2 animations:^{
        [self layoutIfNeeded];
    }];
    [UIView commitAnimations];
    

}



- (void)makeFirstResponder
{
   if(self.passwordTextField)
   {
       if([self.passwordTextField canBecomeFirstResponder]){
           [self.passwordTextField becomeFirstResponder];
       }
   }
}


#pragma mark - Loading VIew Methods
- (void)createLoadingView{
    
    if(!loadingView){
        
        
        loadingView = [[CustomSpinnerAnimationView alloc] initWithText:@"" withLoadingImage:[UIImage imageNamed:@"Spinner"]];
      
        
        loadingView.translatesAutoresizingMaskIntoConstraints = NO;
        [loadingView addConstraint:[NSLayoutConstraint constraintWithItem:loadingView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:155.0]];
        [loadingView addConstraint:[NSLayoutConstraint constraintWithItem:loadingView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:100.0]];
        
        loadingView.backgroundColor = [UIColor clearColor];
        // [_tableView setNeedsLayout];
    }
}


- (void)showLoadingView{
    
   
    [UIView commitAnimations];
    
    loadingView.hidden = YES;
    if(loadingContainerView){
        [loadingView removeFromSuperview];
        [loadingContainerView removeFromSuperview];
        loadingContainerView = nil;
    }
    //if(!loadingContainerView)
    loadingContainerView = [[UIView alloc] initWithFrame:self.alertView.frame];
    loadingContainerView.backgroundColor = [UIColor clearColor];
    loadingContainerView.center = self.alertView.center;
    [loadingContainerView addSubview:loadingView];
    
    
    [loadingContainerView addConstraint:[NSLayoutConstraint constraintWithItem:loadingView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:loadingContainerView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [loadingContainerView addConstraint:[NSLayoutConstraint constraintWithItem:loadingView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:loadingContainerView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    [self addSubview:loadingContainerView];
    
    loadingContainerView.hidden = YES;
    

    [self performSelector:@selector(makeLoadingVisible) withObject:nil afterDelay:0.3];
}

- (void)makeLoadingVisible
{
    loadingContainerView.hidden = NO;
    loadingView.hidden = NO;
    loadingContainerView.center = self.alertView.center;
}


- (void)startLoading
{
    [UIView commitAnimations];
    [self hideContent:YES];
    [self showLoadingView];
    [loadingView startAnimation];
    
}


- (void)stopLoading
{
    [self hideContent:NO];
    [loadingView stopAnimation];
    [loadingContainerView removeFromSuperview];
}


- (void)hideContent:(BOOL)isHidden
{
    self.placeholderView.hidden = !isHidden;
}


- (void)updateAlertButtonTitleForActionButton:(NSString *)actionButtonTitle andCancelButtonTitle:(NSString *)cancelButtonTitle
{
    
    
    [UIView performWithoutAnimation:^{
        [self.cancelButton setTitle:cancelButtonTitle forState:UIControlStateNormal];
        [self.confirmButton setTitle:actionButtonTitle forState:UIControlStateNormal];
        [self.confirmButton layoutIfNeeded];
        [self.cancelButton layoutIfNeeded];
    }];

}


- (void)makeTitleLabelForTextFieldWithTitle:(NSString *)title
{
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectZero];
    lblTitle.textColor = self.detailTitleLabel.textColor;
    lblTitle.font = self.detailTitleLabel.font;
    lblTitle.text = title;

    self.passwordTopConstraint.constant = 28;
    
     lblTitle.frame = CGRectMake(self.passwordTextField.frame.origin.x, self.passwordTextField.frame.origin.y, 200, 18);
    [self.alertView addSubview:lblTitle];
    [self.alertView sendSubviewToBack:lblTitle];
    [self.placeholderView sendSubviewToBack:lblTitle];
    [self.alertView bringSubviewToFront:self.placeholderView];
    
  
    
    
}

- (void)dealloc{
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
   
    if(!secureModeOn)
    {
        NSRange lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
        
        if (lowercaseCharRange.location != NSNotFound) {
            textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                     withString:[string uppercaseString]];
            return NO;
        }

    }
    
    return YES;
}

@end
