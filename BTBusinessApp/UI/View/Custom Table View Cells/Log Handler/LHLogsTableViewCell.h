//
//  LogsTableViewCell.h
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 04/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LHLogFileItem;
@class LHLogsTableViewCell;

@protocol LHLogsTableViewCellDelegate <NSObject>

- (void)userTappedShareButtonOnLogsTableViewCell:(LHLogsTableViewCell *)cell;
- (void)userTappedFavoritesButtonOnLogsTableViewCell:(LHLogsTableViewCell *)cell;
- (void)userTappedDeleteButtonOnLogsTableViewCell:(LHLogsTableViewCell *)cell;
- (void)userTappedViewButtonOnLogsTableViewCell:(LHLogsTableViewCell *)cell;

@end

@interface LHLogsTableViewCell : UITableViewCell {

}

@property (nonatomic, weak) IBOutlet id <LHLogsTableViewCellDelegate> logCellDelegate;

@property (nonatomic, weak) IBOutlet UILabel *logNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *ageLabel;
@property (nonatomic, weak) IBOutlet UILabel *fileSizeLabel;
@property (nonatomic, weak) IBOutlet UILabel *statusLabel;
@property (nonatomic, weak) IBOutlet UIImageView *statusImageView;
@property (nonatomic, weak) IBOutlet UIButton *favoriteButton;
@property (nonatomic, weak) IBOutlet UIButton *shareButton;
@property (nonatomic, weak) IBOutlet UIButton *deleteButton;
@property (nonatomic, weak) IBOutlet UIButton *viewButton;

- (void)updateCellWithLogFileItem:(LHLogFileItem *)fileItem;

@end
