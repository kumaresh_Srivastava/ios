//
//  LogsTableViewCell.m
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 04/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "LHLogsTableViewCell.h"
#import "LHLogFileItem.h"
#import "AppConstants.h"

@interface LHLogsTableViewCell ()

- (NSString *)displayStringForFileSize:(unsigned long long)fileSize;
- (NSString *)displayStringForAgeWithCreationDate:(NSDate *)creationDate;

@end

@implementation LHLogsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark -
#pragma mark Public Methods

- (void)updateCellWithLogFileItem:(LHLogFileItem *)fileItem
{
    // (hd) Setting up the name of the log.
    NSString *dateFromName = [fileItem dateComponentFromName];
    NSString *timeFromName = [fileItem timeComponentFromName];
    NSString *completeNameString = [NSString stringWithFormat:@"Created on %@ at %@", dateFromName, timeFromName];
    NSMutableAttributedString *completeNameAttributedString = [[NSMutableAttributedString alloc] initWithString:completeNameString];

    NSRange rangeOfDate = [completeNameString rangeOfString:dateFromName];
    [completeNameAttributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:kBtFontBold size:17] range:rangeOfDate];

    NSRange rangeOfTime = [completeNameString rangeOfString:timeFromName];
    [completeNameAttributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:kBtFontBold size:17] range:rangeOfTime];
    self.logNameLabel.attributedText = completeNameAttributedString;


    // (hd) Setting up the age of the log.
    NSString *ageString = [self displayStringForAgeWithCreationDate:fileItem.ddLogFileInfo.creationDate];
    self.ageLabel.text = ageString;


    // (hd) Setting up the file size of the log.
    NSString *fileSizeString = [self displayStringForFileSize:fileItem.ddLogFileInfo.fileSize];
    self.fileSizeLabel.text = fileSizeString;


    // (hd) Setting up the status of the log.
    if(fileItem.ddLogFileInfo.isArchived)
    {
        self.statusLabel.text = @"Archived";
        self.statusLabel.textColor = [UIColor blackColor];
        self.statusLabel.font = [UIFont fontWithName:kBtFontLight size:15];

        UIImage *archivedImage = [UIImage imageNamed:@"log_status_archived"];
        self.statusImageView.image = archivedImage;
    }
    else
    {
        self.statusLabel.text = @"In Progress";
        self.statusLabel.textColor = [UIColor greenColor];
        self.statusLabel.font = [UIFont fontWithName:kBtFontBold size:15];

        UIImage *activeImage = [UIImage imageNamed:@"log_status_active"];
        self.statusImageView.image = activeImage;
    }



    // (hd) Setting up the Delete Button. The user should not be able to delete the log file which is in progress.
    if(fileItem.ddLogFileInfo.isArchived)
    {
        self.deleteButton.hidden = NO;
    }
    else
    {
        self.deleteButton.hidden = YES;
    }
}


#pragma mark -
#pragma mark Action Methods

- (IBAction)shareButtonTapped:(id)sender
{
    [self.logCellDelegate userTappedShareButtonOnLogsTableViewCell:self];
}

- (IBAction)favoritesButtonTapped:(id)sender
{
    [self.logCellDelegate userTappedFavoritesButtonOnLogsTableViewCell:self];
}

- (IBAction)deleteButtonTapped:(id)sender
{
    [self.logCellDelegate userTappedDeleteButtonOnLogsTableViewCell:self];
}

- (IBAction)viewButtonTapped:(id)sender
{
    [self.logCellDelegate userTappedViewButtonOnLogsTableViewCell:self];
}



#pragma mark -
#pragma mark Private Helper Methods

- (NSString *)displayStringForFileSize:(unsigned long long)fileSize
{
    NSString *displayString = [NSString stringWithFormat:@"%llu bytes", fileSize];
    if(fileSize >= 1024)
    {
        int fileSizeInKBs = (int)round(fileSize / 1024);
        if(fileSizeInKBs >= 1024)
        {
            int fileSizeInMBs = (int)round(fileSizeInKBs / 1024);
            displayString = [NSString stringWithFormat:@"%d MB", fileSizeInMBs];
        }
        else
        {
            displayString = [NSString stringWithFormat:@"%d KB", fileSizeInKBs];
        }
    }

    return displayString;
}

- (NSString *)displayStringForAgeWithCreationDate:(NSDate *)creationDate;
{
    // Get the system calendar
    NSCalendar *sysCalendar = [NSCalendar currentCalendar];

    // Create the current NSDate
    NSDate *currentDate = [NSDate date];

    // Get conversion to months, days, hours, minutes
    NSCalendarUnit unitFlags = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitDay | NSCalendarUnitSecond;

    NSDateComponents *breakdownInfo = [sysCalendar components:unitFlags fromDate:creationDate  toDate:currentDate  options:0];


    NSString *displayString = @"unknown age";

    NSInteger numberOfDays = [breakdownInfo day];
    if(numberOfDays >= 1)
    {
        if(numberOfDays == 1)
        {
            displayString = @"1 day ago";
        }
        else
        {
            displayString = [NSString stringWithFormat:@"%ld days ago", (long)numberOfDays];
        }
    }
    else
    {
        NSInteger numberOfHours = [breakdownInfo hour];
        if(numberOfHours >= 1)
        {
            if(numberOfHours == 1)
            {
                displayString = @"1 hour ago";
            }
            else
            {
                displayString = [NSString stringWithFormat:@"%ld hours ago", (long)numberOfHours];
            }
        }
        else
        {
            NSInteger numberOfMinutes = [breakdownInfo minute];
            if(numberOfMinutes >= 1)
            {
                if(numberOfMinutes == 1)
                {
                    displayString = @"1 minute ago";
                }
                else
                {
                    displayString = [NSString stringWithFormat:@"%ld minutes ago", (long)numberOfMinutes];
                }
            }
            else
            {
                NSInteger numberOfSeconds = [breakdownInfo second];
                if(numberOfSeconds >= 1)
                {
                    if(numberOfSeconds == 1)
                    {
                        displayString = @"1 second ago";
                    }
                    else
                    {
                        displayString = [NSString stringWithFormat:@"%ld seconds ago", (long)numberOfSeconds];
                    }
                }
            }
        }
    }

    return displayString;
}

@end
