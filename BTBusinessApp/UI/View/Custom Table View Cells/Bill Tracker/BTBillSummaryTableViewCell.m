//
//  BTBillSummaryTableViewCell.m
//  BTBusinessApp
//
//  Created by Saddam Husain on 01/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBillSummaryTableViewCell.h"
#import "AppManager.h"
#import "NSObject+APIResponseCheck.h"
#import "BrandColours.h"
#import "BTOrderStatusLabel.h"

@interface BTBillSummaryTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *accountNumberLabel;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet BTOrderStatusLabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end




@implementation BTBillSummaryTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];

    [self setBackgroundColor:[UIColor clearColor]];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    self.containerView.layer.borderColor = [BrandColours colourBtNeutral50].CGColor;
    self.containerView.layer.borderWidth = 1.0;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCellWithBillSummaryData:(BTBill *)bill {

    //self.statusLabel.textColor = [self getStatusLabelColorWithStatus:bill.billStatus];
    [self.statusLabel setOrderStatus:bill.billStatus];

    if([bill.billingAccountNo validAndNotEmptyStringObject])
    {
        self.accountNumberLabel.text = bill.billingAccountNo;
    }
    else
    {
        self.accountNumberLabel.text = @"";
    }

    if([AppManager /*NSStringFromNSDateWithMonthNameWithHiphenFormat*/getDayMonthYearFromDate:bill.billDate])//BUG 17122
    {
        self.dateLabel.text = [AppManager /*NSStringFromNSDateWithMonthNameWithHiphenFormat*/getDayMonthYearFromDate:bill.billDate];
    }
    else
    {
        self.dateLabel.text = @"";
    }

    if([bill.billStatus validAndNotEmptyStringObject])
    {
        if (bill.billStatus) {
            //self.statusLabel.text = [NSString stringWithFormat:@"%@",[bill.billStatus capitalizedString]];
            [self.statusLabel setOrderStatus:[NSString stringWithFormat:@"%@",[bill.billStatus capitalizedString]]];

        } else
        {
          //self.statusLabel.text = @"";
            [self.statusLabel setOrderStatus:@""];
        }
    }
    else
    {
        //self.statusLabel.text = @"";
        [self.statusLabel setOrderStatus:@""];
    }

    if(bill.totalCharges || bill.totalCharges == 0.0)
    {
        self.priceLabel.text = [NSString stringWithFormat:@"£ %.2f", bill.totalCharges];
    }
    else
    {
        self.priceLabel.text = @"";
    }

}


- (UIColor *)getStatusLabelColorWithStatus:(NSString *)status
{
    //[SD] Default Color
    UIColor *statusLabelColor = [UIColor blackColor];
    if(status && ([[status lowercaseString] isEqualToString:@"overdue"]))
    {
        statusLabelColor = [BrandColours colourMyBtRed];
    }
    else if(status && ([[status lowercaseString] isEqualToString:@"ready"]))
    {
        statusLabelColor = [BrandColours colourMyBtOrange];
    }
    else if(status && ([[status lowercaseString] isEqualToString:@"paid"]))
    {
        statusLabelColor = [BrandColours colourMyBtGreen];
    }
    else if(status && ([[status lowercaseString] isEqualToString:@"due"]))
    {
        statusLabelColor = [BrandColours colourMyBtOrange];
    }
    else if (status && ([[status lowercaseString] isEqualToString:@"no action required"]))
    {
        statusLabelColor = [BrandColours colourMyBtGreen];
    }

    
    return statusLabelColor;
}

@end
