//
//  BTBillSummaryTableViewCell.h
//  BTBusinessApp
//
//  Created by Saddam Husain on 01/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBill.h"

@interface BTBillSummaryTableViewCell : UITableViewCell

- (void)updateCellWithBillSummaryData:(BTBill *)bill;

@end
