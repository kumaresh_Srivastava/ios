//
//  BTBillDetailChargesCell.m
//  BTBusinessApp
//
//  Created by vectoscalar on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBillDetailChargesCell.h"
#define kYPosition -16

@implementation BTBillDetailChargesCell
@synthesize delegate;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)updateDataWithBillDetail:(BillDetailsScreenWrapper *)billDetailWrapper
{
    [self resetValues];
    
   
    if(!billDetailWrapper.chargesTitle || billDetailWrapper.chargesTitle.length ==0)
    {
       billDetailWrapper.chargesTitle = @"-";
    }
    
    
    
    if(!billDetailWrapper.chargesTitleDiscription || billDetailWrapper.chargesTitleDiscription.length ==0)
    {
        billDetailWrapper.chargesTitleDiscription = @"-";
    }
    
    
    
    self.chargesTitleLabel.text = billDetailWrapper.chargesTitle;
    self.chargesDescriptionLabel.text = billDetailWrapper.chargesTitleDiscription;
    
    if(billDetailWrapper.chargesDetailedTitle)
    {
        self.discriptionLabel.text = billDetailWrapper.chargesDetailedTitle;
        NSString *string = billDetailWrapper.chargesDetailedTitle;
        if ([string rangeOfString:@"VAT"].location == NSNotFound) {
            self.discriptionLabel.hidden = NO;
            self.discriptionValueLabel.hidden = NO;
        } else {
             self.discriptionLabel.hidden = NO;
            self.discriptionValueLabel.hidden = YES;
        }
        self.discriptionValueLabel.text = billDetailWrapper.chargesDetailedTitleDiscription;
        self.separatorViewTopConstraint.constant = -kYPosition;
        
    }
    else
    {
        [self hideDiscriptionDetails];
    }
    
    if(billDetailWrapper.needToShowColoreData)
    {
        [self updateColor];
    }
    
    if(billDetailWrapper.needToShowHelpButton)
    {
        [self showHelpButton];
    }
}




//Set all properties to its default Values
- (void)resetValues
{
    self.chargesTitleLabel.textColor = [UIColor blackColor];
    self.chargesDescriptionLabel.textColor = [UIColor blackColor];
    self.helpButton.hidden = YES;
    self.buttonWidthConstraint.constant = 12;
    self.separatorView.backgroundColor = [BrandColours colourBtNeutral50];
    [self hideDiscriptionDetails];
}

- (IBAction)userDidPressedHelpButton:(id)sender {
    [delegate userDidPressedHelpButtonOnChargesCell];
    
}


//When Discription is not available hide the Discription Details
- (void)hideDiscriptionDetails
{
    self.separatorViewTopConstraint.constant = kYPosition;
    self.discriptionLabel.hidden = YES;
    self.discriptionValueLabel.hidden = YES;
    
}

//If its last row remove separator View
- (void)removeSeparatorView
{
    self.separatorView.hidden = YES;
}


//Update the color for last cell
- (void)updateColor
{
    //self.chargesTitleLabel.textColor = [BrandColours colourTextBTPurplePrimaryColor];
    //self.chargesDescriptionLabel.textColor = [BrandColours colourTextBTPurplePrimaryColor];
    //self.separatorView.backgroundColor = [UIColor clearColor];
    self.chargesDescriptionLabel.textColor = [UIColor colorForHexString:@"e60050"];
}

//show the question mark button  when Needed
- (void)showHelpButton
{
    self.helpButton.hidden = NO;
    self.buttonWidthConstraint.constant = 35;
}

@end
