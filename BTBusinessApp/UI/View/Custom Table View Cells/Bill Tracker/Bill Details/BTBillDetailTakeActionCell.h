//
//  BTBillDetailTakeActionCell.h
//  BTBusinessApp
//
//  Created by vectoscalar on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>


@class BTBillDetailTakeActionCell;

@protocol BTBillDetailTakeActionCellDelegate <NSObject>

- (void)userDidToggledNotificationOn:(BTBillDetailTakeActionCell*)cell withValue:(BOOL)switchStatus;

@end

@interface BTBillDetailTakeActionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *accessoryImageView;
@property (weak, nonatomic) IBOutlet UISwitch *toggleNotificaitonSwitch;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleTopConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *separatorViewTopConstraint;

@property (assign,nonatomic) id <BTBillDetailTakeActionCellDelegate> delegate;

- (IBAction)userToggledBillingNotificationSwitch:(id)sender;

- (void)showAccessoryView;
- (void)showSwitch;

- (void)updateTakeActionWithTittle:(NSString *)title andNeedToShowSwitch:(BOOL)needToShowSwitch andIsPDFAvailable:(BOOL)isPdfAvailable;

@end
