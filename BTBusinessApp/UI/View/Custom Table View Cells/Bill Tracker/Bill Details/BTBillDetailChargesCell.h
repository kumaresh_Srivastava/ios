//
//  BTBillDetailChargesCell.h
//  BTBusinessApp
//
//  Created by vectoscalar on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//
#import "DLMBillDetailsScreen.h"
#import <UIKit/UIKit.h>

@class BTBillDetailChargesCell;

@protocol BTBillDetailChargesCellDelegate <NSObject>

- (void)userDidPressedHelpButtonOnChargesCell;


@end

@interface BTBillDetailChargesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *chargesTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *chargesDescriptionLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonWidthConstraint;

@property (weak, nonatomic) IBOutlet UILabel *discriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *discriptionValueLabel;
@property (weak, nonatomic) IBOutlet UIButton *helpButton;

@property (weak, nonatomic) IBOutlet UIView *separatorView;

- (void)resetValues;

- (IBAction)userDidPressedHelpButton:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *separatorViewTopConstraint;

@property (assign,nonatomic) id <BTBillDetailChargesCellDelegate> delegate;

- (void)updateDataWithBillDetail:(BillDetailsScreenWrapper *)billDetailWrapper;


@end
