//
//  BTBillDetailDurationTableViewCell.h
//  BTBusinessApp
//
//  Created by vectoscalar on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMBillDetailsScreen.h"

@interface BTBillDetailDurationTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *paymentStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *paymentDateValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *paymentStatusValueLabel;


- (void)updateDataWithBillDetail:(BillDetailsScreenWrapper *)billDetailWrapper;

@end
