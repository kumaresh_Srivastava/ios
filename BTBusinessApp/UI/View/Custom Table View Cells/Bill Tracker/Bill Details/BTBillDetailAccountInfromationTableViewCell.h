//
//  BTBillDetailAccountInfromationTableViewCell.h
//  BTBusinessApp
//
//  Created by vectoscalar on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMBillDetailsScreen.h"

@interface BTBillDetailAccountInfromationTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *billRefLabel;
@property (weak, nonatomic) IBOutlet UILabel *billTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *accountNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *billingNameAddressLabel;


- (void)updateDataWithBillDetail:(BillDetailsScreenWrapper *)billDetailWrapper;

@end
