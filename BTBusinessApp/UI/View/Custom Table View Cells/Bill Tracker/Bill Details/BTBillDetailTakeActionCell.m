//
//  BTBillDetailTakeActionCell.m
//  BTBusinessApp
//
//  Created by vectoscalar on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBillDetailTakeActionCell.h"


#define kOriginalConstant 2
#define kOriginalConstantForTitle 20

#define kSixYPosition -10//-20
#define kFiveYPostion -14//-24
#define kSixPYPostion 6//-4


@implementation BTBillDetailTakeActionCell
@synthesize delegate;


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


- (void)updateTakeActionWithTittle:(NSString *)title andNeedToShowSwitch:(BOOL)needToShowSwitch andIsPDFAvailable:(BOOL)isPdfAvailable
{
    if(needToShowSwitch)
    {
        [self showSwitch];
    }
    else
    {
        [self showAccessoryView];
    }
    self.titleLabel.text = title;
    
    if([title containsString:@"pdf"] || [title containsString:@"PDF"])
    {
        if(!isPdfAvailable)
        {
            self.titleLabel.textColor = [UIColor lightGrayColor];
        }
        
        else
        {
            self.titleLabel.textColor = [UIColor blackColor];
            
        }
    }
}


- (void)showSwitch
{
    self.toggleNotificaitonSwitch.hidden = NO;
    self.descriptionLabel.hidden = NO;
    
    self.accessoryImageView.hidden = YES;
    
    self.separatorViewTopConstraint.constant = kOriginalConstant;
    self.titleTopConstraint.constant = kOriginalConstantForTitle;
    self.separatorView.hidden = NO;
    [self layoutIfNeeded];
}


- (void)showAccessoryView
{
    self.separatorView.hidden = NO;
    self.toggleNotificaitonSwitch.hidden = YES;
    self.detailTextLabel.text = @"";
    self.descriptionLabel.hidden = YES;
    
    self.accessoryImageView.hidden = NO;
    
    
    if([[UIScreen mainScreen] bounds].size.width == 414.0f)
    {
        self.separatorViewTopConstraint.constant = kSixPYPostion;
    }
    else if([[UIScreen mainScreen] bounds].size.width > 320.0f)
    {
       self.separatorViewTopConstraint.constant = kSixYPosition;
    }
    else
    {
        self.separatorViewTopConstraint.constant = kFiveYPostion;
        self.titleTopConstraint.constant = 6;
    }
    
    
    [self layoutIfNeeded];
}



- (IBAction)userToggledBillingNotificationSwitch:(id)sender {
    
    [delegate userDidToggledNotificationOn:self withValue:[sender isOn]];
    
}

@end
