//
//  BTBillNoChargesTableViewCell.m
//  BTBusinessApp
//
//  Created by vectoscalar on 12/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBillNoChargesTableViewCell.h"

@implementation BTBillNoChargesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)updateCellHeight:(CGFloat)height
{
    self.cellHeight.constant = height;
    [self layoutIfNeeded];
}

@end
