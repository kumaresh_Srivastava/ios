//
//  BTBillNoChargesTableViewCell.h
//  BTBusinessApp
//
//  Created by vectoscalar on 12/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTBillNoChargesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cellHeight;


- (void)updateCellHeight:(CGFloat)height;

@end
