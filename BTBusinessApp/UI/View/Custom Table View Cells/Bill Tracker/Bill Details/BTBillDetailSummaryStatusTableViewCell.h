//
//  BTBillDetailSummaryStatusTableViewCell.h
//  BTBusinessApp
//
//  Created by vectoscalar on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DLMBillDetailsScreen.h"

@class BTBillDetailAccountInfromationTableViewCell;

@protocol BTBillBillingAccountSummaryDelegate <NSObject>

- (void)userPressedDetailOnBillSummaryCell;

@end

@interface BTBillDetailSummaryStatusTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cellHeightConstraint;


@property (assign,nonatomic) BOOL isBillingCurrentStatusCell;
@property (assign,nonatomic) BOOL isPaymentDone;

@property (assign,nonatomic) id <BTBillBillingAccountSummaryDelegate
> delegate;
- (void)updatePaymentStatusWithStaus:(NSString *)paymentStatus andDescriptionInfo:(NSString*)descriptionInfo;

- (void)updateSummaryOfChargesWithArray:(NSArray *)chargesArray andDescriptionInfo:(NSString*)descriptionInfo;


- (void)updateDataWithBillDetail:(BillDetailsScreenWrapper *)billDetailWrapper;



@end
