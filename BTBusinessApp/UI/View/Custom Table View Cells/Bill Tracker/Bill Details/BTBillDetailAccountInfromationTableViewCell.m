//
//  BTBillDetailAccountInfromationTableViewCell.m
//  BTBusinessApp
//
//  Created by vectoscalar on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBillDetailAccountInfromationTableViewCell.h"

@implementation BTBillDetailAccountInfromationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateDataWithBillDetail:(BillDetailsScreenWrapper *)billDetailWrapper
{
   
    if(!billDetailWrapper.billRef || billDetailWrapper.billRef.length == 0)
    {
         billDetailWrapper.billRef = @"-";
    }
    
    if(!billDetailWrapper.billType || billDetailWrapper.billType.length == 0)
    {
        billDetailWrapper.billType = @"-";
    }
    
    if(!billDetailWrapper.accountName || billDetailWrapper.accountName.length == 0)
    {
        billDetailWrapper.accountName = @"-";
    }
    
    if(!billDetailWrapper.billingNameAndAddress || billDetailWrapper.billingNameAndAddress.length == 0)
    {
        billDetailWrapper.billingNameAndAddress = @"-";
    }
    
    billDetailWrapper.billingNameAndAddress = [billDetailWrapper.billingNameAndAddress stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
    billDetailWrapper.billingNameAndAddress = [billDetailWrapper.billingNameAndAddress stringByReplacingOccurrencesOfString:@"," withString:@", "];
    
    NSRange location = [billDetailWrapper.billingNameAndAddress rangeOfString:@"\n"];
    NSString* result = [billDetailWrapper.billingNameAndAddress stringByReplacingCharactersInRange:location withString:@""];
    
    self.billRefLabel.text = billDetailWrapper.billRef;
    self.billTypeLabel.text = billDetailWrapper.billType;
    self.accountNameLabel.text  = billDetailWrapper.accountName;
    self.billingNameAddressLabel.text= result;//billDetailWrapper.billingNameAndAddress;
}

@end
