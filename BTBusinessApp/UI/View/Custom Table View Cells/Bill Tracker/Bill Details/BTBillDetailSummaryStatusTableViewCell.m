//
//  BTBillDetailSummaryStatusTableViewCell.m
//  BTBusinessApp
//
//  Created by vectoscalar on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBillDetailSummaryStatusTableViewCell.h"
#import "BTProductCharge.h"

#define kBillingStatusTitle @"Current Status"
//#define kBillingSummaryTitle @"Summary of charges"
#define kBillingSummaryTitle @"This bill is for"

#define kAwatingPaymentText @"Awaiting payment"
#define kPaymentPaidText @"Paid"

#define kXPadding 15
#define kYPadding 10
#define kContentWidth ([[UIScreen mainScreen] bounds].size.width - 2*kXPadding)

//FOnts
#define kHeadingFontSize 14

#define kContentFontSize 18

#define kDiscriptionFontSize 14

#define kBoldFont @"BTFont-Bold"
#define kRegularFont @"BTFont-Regular"


#define kGap 10

#define kContentViewTag 8767

#define kDetailsButtonWidth 100

@implementation BTBillDetailSummaryStatusTableViewCell
@synthesize delegate;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
    
}


- (void)updateDataWithBillDetail:(BillDetailsScreenWrapper *)billDetailWrapper
{
    if(billDetailWrapper.cellType == BillingStatusCell)
    {
        if([billDetailWrapper.status isEqualToString:@"NO ACTION REQUIRED"])
        {
            [self updatePaymentStatusWithStaus:@"No action required" andDescriptionInfo:[NSString stringWithFormat:@"Your payment will be taken from your account on or just after %@", billDetailWrapper.paymentDueDate]];
        }
        else
        {
            if(billDetailWrapper.isPaid)
            {
                [self updatePaymentStatusWithStaus:@"Paid" andDescriptionInfo:@"Payment has been received, thank you."];
            }
            else
            {
                if(billDetailWrapper.paymentDueDate)
                    [self updatePaymentStatusWithStaus:@"Awaiting payment" andDescriptionInfo:[NSString stringWithFormat:@"Pay by %@",billDetailWrapper.paymentDueDate]];
                else
                    [self updatePaymentStatusWithStaus:@"Awaiting payment" andDescriptionInfo:@"-"];
            }
        }
    }
    else
    {
        [self updateSummaryOfChargesWithArray:billDetailWrapper.arrayOfProductCharges andDescriptionInfo:@"Plus extras and VAT"];
    }
}

- (void)updatePaymentStatusWithStaus:(NSString *)paymentStatus andDescriptionInfo:(NSString*)descriptionInfo
{
    
    [self removeAllViews];
    
    CGFloat yPos = kYPadding;
    CGFloat xPos = kXPadding;
    
    //SeparatorLine
    UIView *separatorLine = [[UIView alloc] initWithFrame:CGRectMake(kXPadding, yPos, kContentWidth, 1)];
    separatorLine.backgroundColor = [UIColor colorForHexString:@"ebebeb"];
    
    yPos += 1;
    
    [self.contentView addSubview:separatorLine];
    
    //Add Gap after Line
    yPos += kGap + kGap + 5;
    
    UILabel *headingLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headingLabel.text = kBillingStatusTitle;
    headingLabel.font = [UIFont fontWithName:kBoldFont size:kHeadingFontSize];
    headingLabel.textColor = [UIColor colorForHexString:@"333333"];
    
    headingLabel.frame = CGRectMake(xPos, yPos, kContentWidth/1.5, kHeadingFontSize+2);
    [self.contentView addSubview:headingLabel];
    
    
    yPos += headingLabel.frame.size.height;
    //Add Gap
    yPos += kGap/2;
    

    
    //    //Content Label
    UILabel *paymentStatusLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    paymentStatusLabel.text = paymentStatus;
    paymentStatusLabel.font = [UIFont fontWithName:kRegularFont size:kContentFontSize];
    paymentStatusLabel.alpha  =0.9;
    
    paymentStatusLabel.frame = CGRectMake(kXPadding, yPos, kContentWidth, kContentFontSize+3);
    [self.contentView addSubview:paymentStatusLabel];
    
    // Add Height and Gap
    yPos += paymentStatusLabel.frame.size.height + 3;
    
    //Add Discription Label
    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    descriptionLabel.text = descriptionInfo;
    descriptionLabel.font = [UIFont fontWithName:kRegularFont size:kDiscriptionFontSize];
    descriptionLabel.alpha = 0.8;
    descriptionLabel.numberOfLines = 0;
    CGSize descriptionSize = [self getTheSizeForText:descriptionLabel.text andFont:descriptionLabel.font toFitInWidth:kContentWidth];
    
    descriptionLabel.frame = CGRectMake(kXPadding, yPos, kContentWidth, descriptionSize.height);
    [self.contentView addSubview:descriptionLabel];
    
    //Add Hegiht to ypOs
    yPos += descriptionLabel.frame.size.height;
    
    //Add Bottom Margin
    
    yPos += 1.5*kGap;
    
    self.cellHeightConstraint.constant = yPos;
    [self layoutIfNeeded];
    
}



#pragma mark - Make Summary UI
- (void)updateSummaryOfChargesWithArray:(NSArray *)chargesArray andDescriptionInfo:(NSString*)descriptionInfo
{
    
    [self removeAllViews];
    
    CGFloat yPos = kYPadding;
    CGFloat xPos = kXPadding;
    
    //SeparatorLine
    UIView *separatorLine = [[UIView alloc] initWithFrame:CGRectMake(kXPadding, yPos, kContentWidth, 1)];
    separatorLine.backgroundColor = [UIColor colorForHexString:@"ebebeb"];
    
    yPos += 1;
    
    [self.contentView addSubview:separatorLine];
    
    //Add Gap after Line
    yPos = yPos + kGap + kGap/2 - 4;
    UILabel *headingLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headingLabel.text = kBillingSummaryTitle;
    headingLabel.font = [UIFont fontWithName:kBoldFont size:kHeadingFontSize];
    headingLabel.textColor = [UIColor colorForHexString:@"333333"];
    
    headingLabel.frame = CGRectMake(xPos, yPos, kContentWidth/1.5, kHeadingFontSize+2);
    [self.contentView addSubview:headingLabel];
    
    yPos += headingLabel.frame.size.height;
    //Add Gap
    yPos += kGap/2;
    

    //Content Label
    int counter = 0;
    for(BTProductCharge *pCharges in chargesArray)
    {
        NSString *charges;
        CGFloat paymentLabelWidth = kContentWidth;
        
        if(pCharges.numberOfInstallations >= 2)
        {
            charges = [NSString stringWithFormat:@"%@ (%ld)  £%.2f",pCharges.productName,(long)pCharges.numberOfInstallations,pCharges.totalCharges];
        }
        else
        {
            charges = [NSString stringWithFormat:@"%@  £%.2f",pCharges.productName,pCharges.totalCharges];
        }
        
        //If this is first Row Make Details button
        //Make Details Button
        
        UIButton *detailsButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [detailsButton setTitle:@"Details" forState:UIControlStateNormal];
        detailsButton.titleLabel.font = [UIFont fontWithName:kRegularFont size:14];
        
        CGSize size = [self getTheSizeForText:@"Details" andFont:detailsButton.titleLabel.font toFitInWidth:100];
        
        [detailsButton setTitleColor:[BrandColours colourTextBTPurplePrimaryColor] forState:UIControlStateNormal];
        detailsButton.frame = CGRectMake(kContentWidth- size.width+kXPadding, headingLabel.frame.origin.y ,  size.width, 18);//CGRectMake(kContentWidth- size.width+kXPadding, yPos+7, size.width, 18);//
        [detailsButton addTarget:self action:@selector(userPressedDetailButton) forControlEvents:UIControlEventTouchUpInside];
        if(!counter)
        {
            [self.contentView addSubview:detailsButton];
            paymentLabelWidth = kContentWidth - size.width-kXPadding;
        }
        //Make Products Label
        UILabel *paymentStatusLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        paymentStatusLabel.text = charges;
        paymentStatusLabel.font = [UIFont fontWithName:kRegularFont size:kContentFontSize];
        paymentStatusLabel.alpha  =0.9;
        paymentStatusLabel.numberOfLines = 4;
        
        size = [self getTheSizeForText:charges andFont:paymentStatusLabel.font toFitInWidth:paymentLabelWidth];
        
        
        paymentStatusLabel.frame = CGRectMake(kXPadding, yPos, paymentLabelWidth,size.height);
        [self.contentView addSubview:paymentStatusLabel];
        // Add Height and Gap
        yPos += paymentStatusLabel.frame.size.height + 2;
        counter++;
    }
    //Add a dash(-) if no data is available
    if([chargesArray count] == 0)
    {
        //Make Products Label
        UILabel *paymentStatusLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        paymentStatusLabel.text = @"-";
        paymentStatusLabel.font = [UIFont fontWithName:kRegularFont size:kContentFontSize];
        paymentStatusLabel.alpha  =0.9;
        paymentStatusLabel.numberOfLines = 4;
        
        CGSize PaymentLabelsize = [self getTheSizeForText:@"-" andFont:paymentStatusLabel.font toFitInWidth:50];
        
        
        paymentStatusLabel.frame = CGRectMake(kXPadding, yPos, PaymentLabelsize.width,PaymentLabelsize.height);
        [self.contentView addSubview:paymentStatusLabel];
        // Add Height and Gap
        yPos += paymentStatusLabel.frame.size.height + 1.5*kGap;
    }
    
    
    //Add Discription Label
    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    descriptionLabel.text = descriptionInfo;
    descriptionLabel.font = [UIFont fontWithName:kRegularFont size:kContentFontSize];//[UIFont fontWithName:kRegularFont size:kDiscriptionFontSize];
    descriptionLabel.alpha = 0.9;
    descriptionLabel.frame = CGRectMake(kXPadding, yPos + 5, kContentWidth, kDiscriptionFontSize);
    
    if([chargesArray count]> 0)
    {
        //Add Hegiht to ypOs
        [self.contentView addSubview:descriptionLabel];
        yPos += descriptionLabel.frame.size.height;
        //Add Bottom Margin
        yPos += 1.5*kGap;
    }
    
    
    
    
    self.cellHeightConstraint.constant = yPos;
    [self layoutIfNeeded];
    
}


- (void)userPressedDetailButton
{
    [delegate userPressedDetailOnBillSummaryCell];
}


- (void)removeAllViews
{
    for(UIView *view in self.contentView.subviews)
    {
        if(view.tag != kContentViewTag)
            [view removeFromSuperview];
    }
}

- (CGSize)getTheSizeForText:(NSString *)text andFont:(UIFont*)font toFitInWidth:(CGFloat)width
{
    
    CGRect textRect = [text boundingRectWithSize:CGSizeMake(width, 999)
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName:font}
                                         context:nil];
    
    return textRect.size;
}

@end
