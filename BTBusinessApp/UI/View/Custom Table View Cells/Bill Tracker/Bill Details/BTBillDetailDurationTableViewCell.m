//
//  BTBillDetailDurationTableViewCell.m
//  BTBusinessApp
//
//  Created by vectoscalar on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBillDetailDurationTableViewCell.h"

@implementation BTBillDetailDurationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)updateDataWithBillDetail:(BillDetailsScreenWrapper *)billDetailWrapper
{
    if(billDetailWrapper.isPaid)
    {
        self.paymentStatusLabel.text = @"Next bill date";
        self.paymentDateValueLabel.text = billDetailWrapper.billDate;
        
        if(billDetailWrapper.nextBillDate)
        self.paymentStatusValueLabel.text = billDetailWrapper.nextBillDate;
        else
        {
            self.paymentStatusValueLabel.text  = @"-";
        }
    }
    else
    {
        self.paymentStatusLabel.text = @"Due before";
        self.paymentDateValueLabel.text = billDetailWrapper.billDate;
        
        if(billDetailWrapper.paymentDueDate)
        {
         self.paymentStatusValueLabel.text = billDetailWrapper.paymentDueDate;
        }
        else
        {
        
          self.paymentStatusValueLabel.text = @"-";
        }
    }
}

@end
