//
//  DropDownTableViewCell.h
//  BTBusinessApp
//
//  Created by VectoScalar on 1/11/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DropDownTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end
