//
//  DropDownTableViewCell.m
//  BTBusinessApp
//
//  Created by VectoScalar on 1/11/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "DropDownTableViewCell.h"

#define kBackgroundColor [BrandColours colourBtBackgroundColor]


@implementation DropDownTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    //self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //Selction color
    UIView *cellBg = [[UIView alloc] init];
    cellBg.backgroundColor = kBackgroundColor;
    cellBg.layer.masksToBounds = YES;
    self.selectedBackgroundView = cellBg;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
