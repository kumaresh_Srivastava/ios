//
//  BTUsageTableViewCell.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 23/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTAsset.h"

@interface BTUsageTableViewCell : UITableViewCell


- (void)updateUsageCellWithAsset:(BTAsset *)asset;

@end
