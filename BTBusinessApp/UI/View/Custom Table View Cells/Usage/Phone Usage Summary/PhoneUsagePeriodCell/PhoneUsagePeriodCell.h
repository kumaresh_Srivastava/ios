//
//  PhoneUsagePeriodCell.h
//  BTBusinessApp
//
//  Created by vectoscalar on 23/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneUsagePeriodCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *periodDescriptionLabel;

- (void)updateCellWithPeriodString:(NSString *)periodString;
@end
