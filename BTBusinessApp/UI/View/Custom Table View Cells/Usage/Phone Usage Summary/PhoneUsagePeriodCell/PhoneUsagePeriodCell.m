//
//  PhoneUsagePeriodCell.m
//  BTBusinessApp
//
//  Created by vectoscalar on 23/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "PhoneUsagePeriodCell.h"

@implementation PhoneUsagePeriodCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
     self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Configure the view for the selected state
}

- (void)updateCellWithPeriodString:(NSString *)periodString
{
     if(periodString && periodString.length >0)
         self.periodDescriptionLabel.text  = periodString;
    else
        self.periodDescriptionLabel.text  =  @"";
}

@end
