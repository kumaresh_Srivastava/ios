//
//  PhoneUsageCallInfoTableViewCell.m
//  BTBusinessApp
//
//  Created by vectoscalar on 23/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "PhoneUsageCallInfoTableViewCell.h"

@implementation PhoneUsageCallInfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
     self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Configure the view for the selected state
}

- (void)updateDataWithWrapper:(BTPhoneUsageWrapper *)phoneUsage
{
    if(phoneUsage.totalCost && phoneUsage.totalCost.length>0)
        self.costLabel.text = [NSString stringWithFormat:@"£%@",phoneUsage.totalCost];
    else
        self.costLabel.text = @"-";
    
    if(phoneUsage.totalCalls && phoneUsage.totalCalls.length>0)
        self.callsLabel.text = [NSString stringWithFormat:@"%@",phoneUsage.totalCalls];
    else
        self.callsLabel.text = @"-";

}

@end
