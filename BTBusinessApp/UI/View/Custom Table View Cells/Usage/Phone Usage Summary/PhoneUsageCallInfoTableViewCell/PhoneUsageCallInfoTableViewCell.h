//
//  PhoneUsageCallInfoTableViewCell.h
//  BTBusinessApp
//
//  Created by vectoscalar on 23/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMPhoneUsageSummaryScreeen.h"

@interface PhoneUsageCallInfoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *callsLabel;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;


- (void)updateDataWithWrapper:(BTPhoneUsageWrapper*)phoneUsage;
@end


