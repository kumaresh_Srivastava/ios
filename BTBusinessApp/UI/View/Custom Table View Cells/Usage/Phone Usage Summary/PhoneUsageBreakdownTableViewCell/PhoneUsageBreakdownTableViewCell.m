//
//  PhoneUsageBreakdownTableViewCell.m
//  BTBusinessApp
//
//  Created by vectoscalar on 23/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "PhoneUsageBreakdownTableViewCell.h"

@implementation PhoneUsageBreakdownTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Configure the view for the selected state
}


- (void)updateDataWithWrapper:(BTPhoneUsageWrapper *)phoneUsage
{
    self.costLabel.text  = @"";
    self.callsLabel.text = @"";
    
    if(phoneUsage.usageBreakdownCost && phoneUsage.usageBreakdownCost.length > 0)
        self.costLabel.attributedText = [self getAttributedCostForString:phoneUsage.usageBreakdownCost];
    else
        self.costLabel.text  = @"-";
    
    
     if(phoneUsage.usageBreakdownCalls && phoneUsage.usageBreakdownCalls.length > 0)
         self.callsLabel.attributedText = [self getAttributedCallForString:phoneUsage.usageBreakdownCalls];
    else
        self.callsLabel.text  = @"-";
    
    if(phoneUsage.usageBreakdownTitle && phoneUsage.usageBreakdownTitle.length > 0)
          self.titleLabel.text = phoneUsage.usageBreakdownTitle;
    else
        self.titleLabel.text = @"-";
}

- (NSMutableAttributedString *)getAttributedCostForString:(NSString *)costString
{
    NSString *completeString = [NSString stringWithFormat:@"Cost: £%@",costString];
//    NSRange boldedRange = NSMakeRange(6, completeString.length-6);
    NSRange boldedRange = NSMakeRange(0, 5);

    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:completeString];
    
    [attrString beginEditing];
    [attrString addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:@"BTFont-Bold" size:14]
                       range:boldedRange];
    
    [attrString endEditing];
    
    return attrString;
}

- (NSMutableAttributedString *)getAttributedCallForString:(NSString *)callString
{
//    NSString *completeString = [NSString stringWithFormat:@"%@ calls",callString];
//    NSRange boldedRange = NSMakeRange(0, callString.length);
    
    NSString *completeString = [NSString stringWithFormat:@"Calls: %@",callString];
    NSRange boldedRange = NSMakeRange(0, 6);
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:completeString];
    
    [attrString beginEditing];
    [attrString addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:@"BTFont-Bold" size:14]
                       range:boldedRange];
    
    [attrString endEditing];
    
    return attrString;
}

@end
