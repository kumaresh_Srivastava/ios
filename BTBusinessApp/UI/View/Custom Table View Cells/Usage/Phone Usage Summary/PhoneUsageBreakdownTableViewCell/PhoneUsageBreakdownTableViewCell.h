//
//  PhoneUsageBreakdownTableViewCell.h
//  BTBusinessApp
//
//  Created by vectoscalar on 23/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMPhoneUsageSummaryScreeen.h"

@interface PhoneUsageBreakdownTableViewCell : UITableViewCell

- (void)updateDataWithWrapper:(BTPhoneUsageWrapper*)phoneUsage;


@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *callsLabel;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;
@end
