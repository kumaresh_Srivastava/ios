//
//  PhoneUsageBreakdownHeaderTableViewCell.m
//  BTBusinessApp
//
//  Created by vectoscalar on 23/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "PhoneUsageBreakdownHeaderTableViewCell.h"

@implementation PhoneUsageBreakdownHeaderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}


- (void)updateDataWithWrapper:(BTPhoneUsageWrapper *)phoneUsage
{
    self.costLabel.text  = @"";
    self.callLabel.text = @"";
    CGFloat breakDownCost = [phoneUsage.usageBreakdownCost floatValue];
    NSString *breakDownCostString = [NSString stringWithFormat:@"%.2f",breakDownCost];

    if(breakDownCostString && breakDownCostString.length>0)
        self.costLabel.attributedText = [self getAttributedCostForString:breakDownCostString];
    else
        self.costLabel.text = @"-";
    
   if(phoneUsage.usageBreakdownCalls && phoneUsage.usageBreakdownCalls.length>0)
       self.callLabel.attributedText = [self getAttributedCallForString:phoneUsage.usageBreakdownCalls];
    else
        self.callLabel.text = @"-";
    
    
    if(phoneUsage.usageBreakdownTitle && phoneUsage.usageBreakdownTitle.length > 0)
        self.titleLabel.text = phoneUsage.usageBreakdownTitle;
    else
        self.titleLabel.text = @"-";
}


- (NSMutableAttributedString *)getAttributedCostForString:(NSString *)costString
{
    NSString *completeString = [NSString stringWithFormat:@"Cost: £%@",costString];
//    NSRange boldedRange = NSMakeRange(6, completeString.length-6);
    NSRange boldedRange = NSMakeRange(0, 5);

    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:completeString];
    
    [attrString beginEditing];
    [attrString addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:@"BTFont-Bold" size:14]
                       range:boldedRange];
    
    [attrString endEditing];

    return attrString;
}

- (NSMutableAttributedString *)getAttributedCallForString:(NSString *)callString
{
//    NSString *completeString = [NSString stringWithFormat:@"%@ calls",callString];
//    NSRange boldedRange = NSMakeRange(0, callString.length);
    
    NSString *completeString = [NSString stringWithFormat:@"Calls: %@",callString];
    NSRange boldedRange = NSMakeRange(0, 6);

    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:completeString];
    
    [attrString beginEditing];
    [attrString addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:@"BTFont-Bold" size:14]
                       range:boldedRange];
    
    [attrString endEditing];
    
    return attrString;
}

@end
