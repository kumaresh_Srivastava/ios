//
//  PhoneUsageBreakdownHeaderTableViewCell.h
//  BTBusinessApp
//
//  Created by vectoscalar on 23/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMPhoneUsageSummaryScreeen.h"

@interface PhoneUsageBreakdownHeaderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *callLabel;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;


- (void)updateDataWithWrapper:(BTPhoneUsageWrapper*)phoneUsage;

@end
