//
//  CurrentMonthUsageSummaryCell.m
//  BTBusinessApp
//
//  Created by VectoScalar on 12/23/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTCurrentMonthUsageSummaryCell.h"
#import "DLMBroadbandUsageScreen.h"

@interface BTCurrentMonthUsageSummaryCell()
@property (weak, nonatomic) IBOutlet UILabel *thisMonthUsageLable;
@property (weak, nonatomic) IBOutlet UILabel *usageLimitLabel;

@end

@implementation BTCurrentMonthUsageSummaryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)updateWithBroadbandUsageRowWrapper:(BroadbandUsageRowWrapper *)wrapper{
    
    self.thisMonthUsageLable.text = wrapper.subHeading1;
    self.usageLimitLabel.text = wrapper.subHeading2;
    
}

@end
