//
//  BTBroadbandDataUsageStatusCellTableViewCell.m
//  BTBusinessApp
//
//  Created by VectoScalar on 12/22/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBroadbandDataUsageStatusCellTableViewCell.h"
#import "BrandColours.h"
#import "AppConstants.h"

@interface BTBroadbandDataUsageStatusCellTableViewCell()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *coloredProgressTraillingContraint;
@property (weak, nonatomic) IBOutlet UILabel *topLeftLabel;
@property (weak, nonatomic) IBOutlet UILabel *topRightLabel;
@property (weak, nonatomic) IBOutlet UIView *unSelectedProgressView;
@property (weak, nonatomic) IBOutlet UIView *selectedProgressView;//
@property (weak, nonatomic) IBOutlet UILabel *bottomLeftLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomRightLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;



@end

@implementation BTBroadbandDataUsageStatusCellTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.selectedProgressView.layer.cornerRadius = 6;
    self.unSelectedProgressView.layer.cornerRadius = 6;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateWithBroadbandUsageRowWrapper:(BroadbandUsageRowWrapper *)wrapper forMonthlyUsage:(BOOL)isMonthlyUsage {
    
    self.topLeftLabel.text = wrapper.heading;
    self.topRightLabel.text = wrapper.subHeading1;
    self.bottomLeftLabel.text = wrapper.subHeading2;
    self.bottomRightLabel.text = wrapper.subHeading3;
    
    self.separatorView.hidden = NO;
    double usagePercentage = wrapper.usagePercentage;
     if([[wrapper.heading uppercaseString] isEqualToString:@"UNLIMITED"] || [[wrapper.subHeading1 uppercaseString] isEqualToString:@"UNLIMITED"] || [[wrapper.subHeading2 uppercaseString] isEqualToString:@"UNLIMITED"] || [[wrapper.subHeading3 uppercaseString] isEqualToString:@"UNLIMITED"]){
          usagePercentage = 100;
     }
    CGFloat totalWidth = [[UIScreen mainScreen] bounds].size.width - 30;
    self.coloredProgressTraillingContraint.constant = 0 + (totalWidth*usagePercentage)/100;
    CGRect colorFrame = _selectedProgressView.frame;
    colorFrame.size.width = 0 + (totalWidth*usagePercentage)/100;
    _selectedProgressView.frame = colorFrame;
    //_selectedProgressView.frame.size.width = totalWidth - (totalWidth*wrapper.usagePercentage)/100;
    
    if(usagePercentage == 0 && !isMonthlyUsage){
        
        self.coloredProgressTraillingContraint.constant = 0;
    }
    
    self.selectedProgressView.backgroundColor = wrapper.progressColor;
    //self.unSelectedProgressView.backgroundColor = wrapper.progressColor;
    self.topLeftLabel.textColor = [BrandColours colourBtNeutral90];
    self.bottomLeftLabel.textColor = [BrandColours colourBtNeutral90];
    
    switch (wrapper.alertType) {
        case AlertTypeRed:
        {
            self.topLeftLabel.textColor = [BrandColours colourBTBusinessRed];
            self.bottomLeftLabel.textColor = [BrandColours colourBTBusinessRed];
        }
            break;
            
        case AlertTypeAmber:
        {
            
        }
            break;
            
        default:
            break;
    }
    

    
    //To handle Monthly Tab cell
    if(wrapper.attributedTextString){
        
        self.bottomLeftLabel.attributedText = wrapper.attributedTextString;
    }
    
}

- (void)hideSeparator{
    
    self.separatorView.hidden = YES;
}



@end
