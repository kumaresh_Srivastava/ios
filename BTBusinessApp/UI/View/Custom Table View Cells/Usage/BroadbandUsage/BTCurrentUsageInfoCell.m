//
//  BTCurrentUsageInfoCell.m
//  BTBusinessApp
//
//  Created by VectoScalar on 12/23/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTCurrentUsageInfoCell.h"
#import "DLMBroadbandUsageScreen.h"

@interface BTCurrentUsageInfoCell()
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;

@end

@implementation BTCurrentUsageInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateWithBroadbandUsageRowWrapper:(BroadbandUsageRowWrapper *)wrapper{
    
    self.headingLabel.text = wrapper.heading;
    self.valueLabel.attributedText = wrapper.attributedTextString;
    
}

@end
