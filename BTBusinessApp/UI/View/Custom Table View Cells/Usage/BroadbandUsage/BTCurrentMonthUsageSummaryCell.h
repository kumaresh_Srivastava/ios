//
//  CurrentMonthUsageSummaryCell.h
//  BTBusinessApp
//
//  Created by VectoScalar on 12/23/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BroadbandUsageRowWrapper;

@interface BTCurrentMonthUsageSummaryCell : UITableViewCell

- (void)updateWithBroadbandUsageRowWrapper:(BroadbandUsageRowWrapper *)wrapper;

@end
