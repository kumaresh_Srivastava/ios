//
//  BTBroadbandDataUsageStatusCellTableViewCell.h
//  BTBusinessApp
//
//  Created by VectoScalar on 12/22/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMBroadbandUsageScreen.h"

@interface BTBroadbandDataUsageStatusCellTableViewCell : UITableViewCell

- (void)updateWithBroadbandUsageRowWrapper:(BroadbandUsageRowWrapper *)wrapper forMonthlyUsage:(BOOL)isMonthlyUsage;
- (void)hideSeparator;

@end
