//
//  BTUsageTableViewCell.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 23/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTUsageTableViewCell.h"
#import "BrandColours.h"


@interface BTUsageTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *usageTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *usageAccountLabel;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end

@implementation BTUsageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
//    self.containerView.layer.borderColor = [BrandColours colourBtNeutral50].CGColor;
//    self.containerView.layer.borderWidth = 1.0;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


#pragma mark - Public Methods

- (void)updateUsageCellWithAsset:(BTAsset *)asset
{
    _usageTitleLabel.text = asset.assetName;
    _usageAccountLabel.text = asset.billingAccountNumber;
}
@end
