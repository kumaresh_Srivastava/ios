//
//  BTUsageAccountTableViewCell.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 26/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTPhoneUsageAccountTableViewCell.h"

@interface BTPhoneUsageAccountTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *phoneLineTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLineTitleValueLabel;

@end

@implementation BTPhoneUsageAccountTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Public Methods

- (void)updateCellWithPhoneLineUsage:(BTUsagePhoneProduct *)phoneProduct withCloud:(BOOL)isCloudVoice;
{
    if (isCloudVoice) {
        _phoneLineTitleLabel.text = @"Cloud Voice Express";
    }
    else {
        _phoneLineTitleLabel.text = @"Phone line service";
    }
    _phoneLineTitleValueLabel.text = phoneProduct.serviceId;
}


@end
