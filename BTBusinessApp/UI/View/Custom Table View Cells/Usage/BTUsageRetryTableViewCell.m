//
//  BLUsageRetryTableViewCell.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 02/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTUsageRetryTableViewCell.h"
#import "BTRetryView.h"
#import "BTUsageLoadingView.h"
#import "BTEmptyDashboardView.h"

@interface BTUsageRetryTableViewCell()<BTRetryViewDelegate>
{
    BTRetryView *_retryView;
    BTUsageLoadingView *_loadingView;
    BTEmptyDashboardView *_emptyDashboardView;
}


@end


@implementation BTUsageRetryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Private method

- (void)updateRetryTableViewCellWithSatate:(RetryViewState)state
{
    [_retryView removeFromSuperview];
    _retryView = nil;

    [_loadingView removeFromSuperview];
    _loadingView = nil;

    [_emptyDashboardView removeFromSuperview];
    _emptyDashboardView = nil;

    if(state == RetryViewStateLoading)
       [self createLoadingView];
    else if(state == RetryViewStateRetry)
        [self showRetryView];
    else if(state == RetryViewStateEmpty)
            [self showEmptyView];

}
- (void)showRetryView {


    _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
    _retryView.translatesAutoresizingMaskIntoConstraints = NO;
    _retryView.backgroundColor = [UIColor whiteColor];
    _retryView.retryViewDelegate = self;

    [self addSubview:_retryView];

    [self addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
           [self addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];


    [self addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
}


- (void)createLoadingView {

    if(!_loadingView){

        _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"BTUsageLoadingView" owner:nil options:nil] objectAtIndex:0];
        _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_loadingView];
    }

    [self addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];

}


- (void)showEmptyView {

    _emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTEmptyDashboardView" owner:nil options:nil] objectAtIndex:0];
    _emptyDashboardView.translatesAutoresizingMaskIntoConstraints = NO;
    [_emptyDashboardView updateEmptyDashboardViewWithImageName:@"package" title:@"No data to display" detailText:nil andButtonTitle:nil];
    _emptyDashboardView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_emptyDashboardView];

    [self addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
}


#pragma mark - BTRetryViewDelegate

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    [self.delegate userPressedRetryButtonOnRetryTableViewCell:self andIsForPhoneLine:_isForPhoneLine];
}

@end
