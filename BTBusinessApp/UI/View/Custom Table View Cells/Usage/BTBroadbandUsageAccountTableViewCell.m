//
//  BTBroadbandUsageAccountTableViewCell.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 26/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBroadbandUsageAccountTableViewCell.h"

@interface BTBroadbandUsageAccountTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *broadBandTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceIdValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *locatedValueLabel;

@end



@implementation BTBroadbandUsageAccountTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code


}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


#pragma mark - Public Methods

- (void)updateCellWithBroadbandUsage:(BTUsageBroadbandProduct *)broadbandProduct
{
    _broadBandTitleLabel.text = broadbandProduct.promotionProductName;
    _serviceIdValueLabel.text = broadbandProduct.serviceId;
    _locatedValueLabel.text = broadbandProduct.postcode;

}


@end
