//
//  BTUsageAccountTableViewCell.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 26/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTUsagePhoneProduct.h"
@interface BTPhoneUsageAccountTableViewCell : UITableViewCell

- (void)updateCellWithPhoneLineUsage:(BTUsagePhoneProduct *)phoneProduct withCloud:(BOOL)isCloudVoice;


@end
