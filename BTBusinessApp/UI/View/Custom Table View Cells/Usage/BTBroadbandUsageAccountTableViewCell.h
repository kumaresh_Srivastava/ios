//
//  BTBroadbandUsageAccountTableViewCell.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 26/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTUsageBroadbandProduct.h"

@interface BTBroadbandUsageAccountTableViewCell : UITableViewCell

- (void)updateCellWithBroadbandUsage:(BTUsageBroadbandProduct *)broadbandProduct;

@end
