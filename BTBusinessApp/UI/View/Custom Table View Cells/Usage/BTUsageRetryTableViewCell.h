//
//  BLUsageRetryTableViewCell.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 02/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTUsageRetryTableViewCell;

@protocol BTUsageRetryTableViewCellDelegate <NSObject>

- (void)userPressedRetryButtonOnRetryTableViewCell:(BTUsageRetryTableViewCell *)usageRetryTableViewCell andIsForPhoneLine:(BOOL)isPhoneLine;

@end



typedef enum retryViewState
{
    RetryViewStateLoading,
    RetryViewStateEmpty,
    RetryViewStateRetry
} RetryViewState;


@interface BTUsageRetryTableViewCell : UITableViewCell

@property (nonatomic, assign) id<BTUsageRetryTableViewCellDelegate> delegate;
@property (nonatomic, assign) BOOL isForPhoneLine;

- (void)updateRetryTableViewCellWithSatate:(RetryViewState)state;


@end
