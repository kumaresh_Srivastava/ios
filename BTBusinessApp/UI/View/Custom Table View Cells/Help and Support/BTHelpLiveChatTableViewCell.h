//
//  BTHelpLiveChatTableViewCell.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 10/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTGeneralCategory.h"
@interface BTHelpLiveChatTableViewCell : UITableViewCell

- (void)updateCellWithGeneralCategory:(BTGeneralCategory *)generalCategory specificCategory:(BTSpecificCategory *)specificCategory andCurrentTime:(NSString *)currentTime;

@end
