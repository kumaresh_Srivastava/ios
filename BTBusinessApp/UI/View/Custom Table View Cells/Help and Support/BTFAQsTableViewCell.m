//
//  BTFAQsTableViewCell.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 9/8/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTFAQsTableViewCell.h"
#import "NSObject+APIResponseCheck.h"
#import "BrandColours.h"

@interface BTFAQsTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *faqTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *faqDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomDividerView;

@end

@implementation BTFAQsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _bottomDividerView.backgroundColor = [BrandColours colourBtNeutral50];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Public method

- (void)updateCellWithFAQCellData:(NSDictionary *)faqCellData
{
    _faqTitleLabel.text = [[[faqCellData valueForKey:@"Title"] validAndNotEmptyStringObject] copy];
    //_faqTitleLabel.textColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
    
    _faqDescriptionLabel.text = [[[faqCellData valueForKey:@"Description"] validAndNotEmptyStringObject] copy];
    //_faqDescriptionLabel.textColor = [BrandColours colourMyBtVeryDarkGrey];
    
    //_faqImageView.image = [UIImage imageNamed:[faqCellData valueForKey:@"Image"]];
}

@end
