//
//  BTHelpLiveChatTableViewCell.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 10/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTHelpLiveChatTableViewCell.h"
#import "BrandColours.h"

@interface BTHelpLiveChatTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UIImageView *rightArrowImageView;

@end

@implementation BTHelpLiveChatTableViewCell

- (void)awakeFromNib {

    [super awakeFromNib];

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


#pragma mark - Public Methods


- (void)updateCellWithGeneralCategory:(BTGeneralCategory *)generalCategory specificCategory:(BTSpecificCategory *)specificCategory andCurrentTime:(NSString *)currentTime {

    self.titleLabel.text = generalCategory.categoryName;
    self.timeLabel.text = [NSString stringWithFormat:@"%@ to %@",generalCategory.startTime, generalCategory.endTime];
    self.dayLabel.text = [NSString stringWithFormat:@"%@ - %@",generalCategory.startDay, generalCategory.endDay];

    BOOL isEnabled = [self isCurrentTimeInBetweenStartTime:specificCategory.startTime endTime:specificCategory.endTime withCurrentTime:currentTime];

    NSString *imageStateString = @"enabled";

    if(isEnabled) {
        self.titleLabel.textColor = [BrandColours colourTextBTPurplePrimaryColor];
        self.timeLabel.textColor = [BrandColours colourBtNeutral90];
        self.dayLabel.textColor = [BrandColours colourBtNeutral90];
        self.descriptionLabel.textColor = [BrandColours colourBtNeutral90];

        _rightArrowImageView.image = [UIImage imageNamed:@"arrow_right"];
        self.userInteractionEnabled = YES;
    }
    else {

        self.titleLabel.textColor = [BrandColours colourBtNeutral70];
        self.timeLabel.textColor = [BrandColours colourBtNeutral70];
        self.dayLabel.textColor = [BrandColours colourBtNeutral70];
        self.descriptionLabel.textColor = [BrandColours colourBtNeutral70];

       _rightArrowImageView.image = [UIImage imageNamed:@"Help_disable_arrow_right"];

        imageStateString = @"disabled";
        self.userInteractionEnabled = NO;
    }


    if([[generalCategory.categoryName lowercaseString] isEqualToString:@"billing"]) {
        
        self.descriptionLabel.text = @"You'll need your account number";
        self.iconImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"billing_%@",imageStateString]];
    }
    else if([[generalCategory.categoryName lowercaseString] isEqualToString:@"faults"]) {
        self.descriptionLabel.text = @"Report faults without calling";
        self.iconImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"fault_%@",imageStateString]];
    }
    else if([[generalCategory.categoryName lowercaseString] isEqualToString:@"orders"]) {
        self.descriptionLabel.text = @"You'll need your order number";
        self.iconImageView.image = [UIImage imageNamed: [NSString stringWithFormat:@"order_%@",imageStateString]];
    }
    
}



#pragma mark - Private Helper Methods


-(BOOL)isCurrentTimeInBetweenStartTime:(NSString *)startTime endTime:(NSString *)endTime withCurrentTime:(NSString *)currentTime {

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];

    NSDate *sTime, *eTime,*cTime;
    sTime = [dateFormatter dateFromString:startTime];
    eTime = [dateFormatter dateFromString:endTime];
    cTime = [dateFormatter dateFromString:currentTime];

    if(([cTime compare:sTime] == NSOrderedDescending || [cTime compare:sTime] == NSOrderedSame) && ([cTime compare:eTime] ==  NSOrderedAscending || [cTime compare:eTime] == NSOrderedSame)) {

        return  true;
    }

    return false;
    
}


@end
