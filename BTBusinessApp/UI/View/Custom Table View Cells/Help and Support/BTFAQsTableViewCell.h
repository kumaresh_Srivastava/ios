//
//  BTFAQsTableViewCell.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 9/8/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTFAQsTableViewCell : UITableViewCell

- (void)updateCellWithFAQCellData:(NSDictionary *)faqCellData;

@end
