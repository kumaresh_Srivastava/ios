//
//  BTLiveChatFAQTableViewCell.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 10/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTLiveChatFAQTableViewCell.h"

@interface BTLiveChatFAQTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;


@end


@implementation BTLiveChatFAQTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    self.titleLabel.textColor = [BrandColours colourTextBTPurplePrimaryColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
