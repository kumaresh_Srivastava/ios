//
//  BTServiceStatusTableViewCell.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 2/22/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTServiceStatusTableViewCell.h"
#import "BTServiceStatusDetail.h"
#import "AppManager.h"

#define kOrigninalConstraintsDateTopViewHeight 75;

#define kOngoingIssueString @"Ongoing issue"
#define kNoIssuesFoundString @"No issues found"
#define kRecentlyResolvedIssueString @"Recently resolved issue"

@interface BTServiceStatusTableViewCell () <UITextViewDelegate>
    


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintsDateTopViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *affectedAreaViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cloudVoiceOngoingIssueViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *affectedAreaValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *cloudVoiceOngoingIssueLabel;



@property (weak, nonatomic) IBOutlet UIView *dateAreaTopView;
@property (weak, nonatomic) IBOutlet UILabel *resolvedLabel;
@property (weak, nonatomic) IBOutlet UILabel *loggedOnDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *resolvedDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *affectedAreaLabel;
@property (weak, nonatomic) IBOutlet UIView *dividerLineView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIView *cloudVoiceOngoingIssueView;

@end

@implementation BTServiceStatusTableViewCell

- (NSLayoutConstraint *)cloudVoiceOngoingIssueViewHeight
{
    if (!_cloudVoiceOngoingIssueViewHeight) {
        _cloudVoiceOngoingIssueViewHeight = [NSLayoutConstraint constraintWithItem:self.cloudVoiceOngoingIssueView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0.0];
    }
    return _cloudVoiceOngoingIssueViewHeight;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    [self hideDateAreaView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCellWithServiceStatusDetail:(BTServiceStatusDetail *)serviceStatusDetail andAffectedAreaText:(NSString *)affectedAreaText
{
//    self.affectedAreaValueLabel.text = affectedAreaText;
    
    self.affectedAreaValueLabel.hidden = NO;
    self.affectedAreaValueTextView.hidden = YES;

    
    [self.affectedAreaValueLabel setText:affectedAreaText];
    
    
    
    if ([serviceStatusDetail.mSOFlag isEqualToString:kNoIssuesFoundString])
    {
        _constraintsDateTopViewHeight.constant = 0;
        _affectedAreaViewHeight.constant = 0;
        [self.contentView addConstraint:self.cloudVoiceOngoingIssueViewHeight];

    }
    else if ([serviceStatusDetail.mSOFlag isEqualToString:kOngoingIssueString])
    {
        [self showDateAreaView];
        _affectedAreaLabel.hidden = NO;
        _resolvedLabel.text = @"Fixed";
        _resolvedDateLabel.text = [AppManager NSStringFromNSDateWithMonthHourDateFormat:serviceStatusDetail.expectedResolution];
        _loggedOnDateLabel.text = [AppManager NSStringFromNSDateWithMonthHourDateFormat:serviceStatusDetail.loggedOn];
        if ([serviceStatusDetail.product.lowercaseString isEqualToString:@"cloud"]) {
            [self.contentView removeConstraint:self.cloudVoiceOngoingIssueViewHeight];
        }
    }
    else if ([serviceStatusDetail.mSOFlag isEqualToString:kRecentlyResolvedIssueString])
    {
        //_cloudVoiceOngoingIssueLabelViewHeight.constant = 0;
        [self.contentView addConstraint:self.cloudVoiceOngoingIssueViewHeight];

        [self showDateAreaView];
        _affectedAreaLabel.hidden = NO;
        _resolvedLabel.text = @"Resolved on";
        _resolvedDateLabel.text = [AppManager NSStringFromNSDateWithMonthHourDateFormat:serviceStatusDetail.resolvedOn];
        _loggedOnDateLabel.text = [AppManager NSStringFromNSDateWithMonthHourDateFormat:serviceStatusDetail.loggedOn];
    }
}

- (void)updateCellWithServiceStatusDetailForCloudVoice:(BTServiceStatusDetail *)serviceStatusDetail andAffectedAreaText:(NSAttributedString *)affectedAreaText
{

    self.affectedAreaValueLabel.hidden = YES;
    self.affectedAreaValueTextView.hidden = NO;

    self.affectedAreaValueLabel.attributedText = affectedAreaText;
    self.affectedAreaValueTextView.attributedText = affectedAreaText;
    
    
    if ([serviceStatusDetail.mSOFlag isEqualToString:kNoIssuesFoundString])
    {
        _constraintsDateTopViewHeight.constant = 0;
        _affectedAreaViewHeight.constant = 0;
        //_cloudVoiceOngoingIssueLabelViewHeight.constant = 0;
        [self.contentView addConstraint:self.cloudVoiceOngoingIssueViewHeight];

    }
    else if ([serviceStatusDetail.mSOFlag isEqualToString:kOngoingIssueString])
    {

        [self showDateAreaView];
        _affectedAreaLabel.hidden = NO;
        _resolvedLabel.text = @"Fixed";
        _resolvedDateLabel.text = [AppManager NSStringFromNSDateWithMonthHourDateFormat:serviceStatusDetail.expectedResolution];
        _loggedOnDateLabel.text = [AppManager NSStringFromNSDateWithMonthHourDateFormat:serviceStatusDetail.loggedOn];
        [self.contentView removeConstraint:self.cloudVoiceOngoingIssueViewHeight];
        
    }
    else if ([serviceStatusDetail.mSOFlag isEqualToString:kRecentlyResolvedIssueString])
    {
        //_cloudVoiceOngoingIssueLabelViewHeight.constant = 0;
        [self.contentView addConstraint:self.cloudVoiceOngoingIssueViewHeight];

        [self showDateAreaView];
        _affectedAreaLabel.hidden = NO;
        _resolvedLabel.text = @"Resolved on";
        _resolvedDateLabel.text = [AppManager NSStringFromNSDateWithMonthHourDateFormat:serviceStatusDetail.resolvedOn];
        _loggedOnDateLabel.text = [AppManager NSStringFromNSDateWithMonthHourDateFormat:serviceStatusDetail.loggedOn];
    }
}

- (void)hideDateAreaView
{
    _constraintsDateTopViewHeight.constant = 0;
    _dateAreaTopView.hidden = YES;
    _dividerLineView.hidden = YES;
    _affectedAreaLabel.hidden = YES;
}

- (void)showDateAreaView
{
    _constraintsDateTopViewHeight.constant = kOrigninalConstraintsDateTopViewHeight;
    _dateAreaTopView.hidden = NO;
    _dividerLineView.hidden = NO;
    _affectedAreaLabel.hidden = NO;
}

- (void)removeAllConstraints
{
    UIView *superview = self.superview;
    while (superview != nil) {
        for (NSLayoutConstraint *c in superview.constraints) {
            if (c.firstItem == self || c.secondItem == self) {
                [superview removeConstraint:c];
            }
        }
        superview = superview.superview;
    }
    
    [self removeConstraints:self.constraints];
    self.translatesAutoresizingMaskIntoConstraints = YES;
}



@end
