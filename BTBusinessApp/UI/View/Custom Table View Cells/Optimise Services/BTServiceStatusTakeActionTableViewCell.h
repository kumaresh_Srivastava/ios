//
//  BTServiceStatusTakeActionTableViewCell.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 2/22/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BTServiceStatusTakeActionTableViewCell;

@protocol BTServiceStatusTakeActionTableViewCellDelegate <NSObject>

- (void)userPressedCellForServiceStatusTakeActionCell:(BTServiceStatusTakeActionTableViewCell *)serviceStatusTakeActionTableViewCell;

@end

@interface BTServiceStatusTakeActionTableViewCell : UITableViewCell {
    
}

@property (nonatomic, weak) id <BTServiceStatusTakeActionTableViewCellDelegate> serviceStatusTakeActionTableViewCellDelegate;

- (void)updateCellWithTitleText:(NSString *)titleString;

@end
