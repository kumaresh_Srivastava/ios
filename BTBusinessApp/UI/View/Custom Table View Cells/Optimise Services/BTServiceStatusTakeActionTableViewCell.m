//
//  BTServiceStatusTakeActionTableViewCell.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 2/22/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTServiceStatusTakeActionTableViewCell.h"

@interface BTServiceStatusTakeActionTableViewCell () {
    
}

@property (weak, nonatomic) IBOutlet UIButton *reportAFaultButton;

@end

@implementation BTServiceStatusTakeActionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCellWithTitleText:(NSString *)titleString
{
    [_reportAFaultButton setTitle:titleString forState:UIControlStateNormal];
}

#pragma mark - Button Action Methods

- (IBAction)userPressedCellButtonAction:(id)sender
{
    [self.serviceStatusTakeActionTableViewCellDelegate userPressedCellForServiceStatusTakeActionCell:self];
}


@end
