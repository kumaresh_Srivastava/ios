//
//  BTServiceStatusTableViewCell.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 2/22/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BTServiceStatusDetail;


@interface BTServiceStatusTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextView *affectedAreaValueTextView;

- (void)updateCellWithServiceStatusDetail:(BTServiceStatusDetail *)serviceStatusDetail andAffectedAreaText:(NSString *)affectedAreaText;
- (void)updateCellWithServiceStatusDetailForCloudVoice:(BTServiceStatusDetail *)serviceStatusDetail andAffectedAreaText:(NSAttributedString *)affectedAreaText;


@end
