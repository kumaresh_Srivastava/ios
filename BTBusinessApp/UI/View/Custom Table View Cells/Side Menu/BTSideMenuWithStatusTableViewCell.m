//
//  BTSideMenuWithStatusTableViewCell.m
//  CustomSlideMenu
//
//  Created by Lakhpat Meena on 12/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTSideMenuWithStatusTableViewCell.h"
#import "BTSideMenuItem.h"
#import "BTSideMenuCellDataWrapper.h"
#import "CustomSpinnerAnimationView.h"
#import "BTSideMenuLogoutView.h"

@interface BTSideMenuWithStatusTableViewCell () {
    CustomSpinnerAnimationView *_loaderView;
}

@end

@implementation BTSideMenuWithStatusTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createLoaderImageView
{
    _loaderView = [[CustomSpinnerAnimationView alloc] initWithText:@"" loadingImage:[UIImage imageNamed:@"menu_loader"] andLoaderImageSize:22.0f];
    
    //[self.contentView addSubview:_loaderView];
    
//    [_loaderView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:_loaderView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
//    [_loaderView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:_loaderView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
//    [_loaderView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:100]];
//    [_loaderView addConstraint:[NSLayoutConstraint constraintWithItem:_loaderView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:155]];
//    _loaderView.translatesAutoresizingMaskIntoConstraints = NO;
    
}

- (void)showLoaderView
{
    [self createLoaderImageView];
    
    _loaderView.hidden = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->_loaderView startAnimation];
    });
}

- (void)hideLoaderView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->_loaderView stopAnimation];
    });
    
    _loaderView.hidden = YES;
    
    [_loaderView removeFromSuperview];
    _loaderView = nil;
    
}


- (void)updateCellWithSideMenuItemData:(BTSideMenuCellDataWrapper *)sideMenuCellData
{
    [self hideLoaderView];
    if ([sideMenuCellData.sideMenuItem.title isEqualToString:@"Dashboard"])
    {
        _menuItemNameLabel.textColor = [BrandColours colourTextBTPurplePrimaryColor];
        self.backgroundColor = [BrandColours colourBtBackgroundColor];
    }
    else if ([sideMenuCellData.sideMenuItem.title isEqualToString:@"Log out"]){
//        self.contentView.hidden = YES;
//        BTSideMenuLogoutView *logOutView = [[[NSBundle mainBundle] loadNibNamed:@"BTSideMenuLogoutView" owner:self options:nil] objectAtIndex:0];
//        CGRect frame = logOutView.frame;
//        frame.size.width  = [UIScreen mainScreen].bounds.size.width;
//        [logOutView setFrame:frame];
//        logOutView.logOutButton.layer.masksToBounds = YES;
//        logOutView.logOutButton.layer.cornerRadius = 1.0f;
//        [self addSubview:logOutView];
    }
    else
    {
        self.backgroundColor = [BrandColours colourBtWhite];
        self.contentView.hidden = NO;
//        _menuItemNameLabel.textColor = [BrandColours colourBtNeutral80];
    }

    _menuItemNameLabel.text = sideMenuCellData.sideMenuItem.title;
    
    if (sideMenuCellData.sideMenuItem.statusType == SMSideMenuStatusTypeNone) {
        _openItemStatusView.hidden = YES;
        _tickImageView.hidden = YES;
        
        [self hideLoaderView];
        
    }
    else if (sideMenuCellData.sideMenuItem.statusType == SMSideMenuStatusTypeNumeric) {
        
        if (sideMenuCellData.needToShowIndicator)
        {
            [self showLoaderView];
            _openItemStatusView.hidden = YES;
            _tickImageView.hidden = YES;
        }
        else if (sideMenuCellData.needsToDisplayOpenItems)
        {
            // Code to display open items
            _openItemStatusView.hidden = NO;
            
            if (sideMenuCellData.sideMenuItem.openItems == 0)
            {
                _openItemsCountLabel.text = [NSString stringWithFormat:@"%ld", (long)sideMenuCellData.sideMenuItem.openItems];
                _openItemStatusView.layer.borderWidth = 1.0f;
                _openItemStatusView.layer.borderColor = [UIColor colorForHexString:@"666666"].CGColor;//[BrandColours colourBtNeutral50].CGColor;
                _openItemStatusView.backgroundColor = [UIColor clearColor];
                _openItemsCountLabel.textColor = [UIColor colorForHexString:@"666666"];//[BrandColours colourBtNeutral60];
            }
            else
            {
                _openItemsCountLabel.text = [NSString stringWithFormat:@"%ld", (long)sideMenuCellData.sideMenuItem.openItems];
                _openItemStatusView.backgroundColor = [UIColor clearColor];
                _openItemStatusView.layer.borderWidth = 1.0f;
                _openItemStatusView.layer.borderColor = [UIColor colorForHexString:@"666666"].CGColor;//[BrandColours colourBtNeutral30].CGColor;
                _openItemsCountLabel.textColor = [UIColor colorForHexString:@"666666"];//[BrandColours colourBtNeutral30];
            }
            
            _tickImageView.hidden = YES;
            [self hideLoaderView];
            
        }
        else
        {
            // Code to show nothing
            _openItemStatusView.hidden = YES;
            _tickImageView.hidden = YES;
            
            [self hideLoaderView];
        }
    }
    else if (sideMenuCellData.sideMenuItem.statusType == SMSideMenuStatusTypeExtendedNumeric)
    {
        
        if (sideMenuCellData.needToShowIndicator)
        {
            // Code to display indicator view
            [self showLoaderView];
            _openItemStatusView.hidden = YES;
            _tickImageView.hidden = YES;
        }
        else if (sideMenuCellData.needsToDisplayOpenItems)
        {
            // Code to display open items
            if (sideMenuCellData.sideMenuItem.openItems > 0)
            {
                _openItemStatusView.hidden = NO;
                _openItemsCountLabel.text = [NSString stringWithFormat:@"%ld", (long)sideMenuCellData.sideMenuItem.openItems];
                _tickImageView.hidden = YES;
                [self hideLoaderView];
                
            }
            else
            {
                _openItemStatusView.hidden = YES;
                _tickImageView.hidden = NO;
                [self hideLoaderView];
            }
            
        }
        else
        {
            // Code to show nothing
            [self hideLoaderView];
            _openItemStatusView.hidden = YES;
            _tickImageView.hidden = YES;
        }
        
    }
    
}

- (void)updateSideMenuCellOnSelectionWithCellData:(BTSideMenuCellDataWrapper *)cellData
{
    //self.backgroundColor = [BrandColours colourBtBackgroundColor];
    //_menuItemNameLabel.textColor = [BrandColours colourTextBTPurplePrimaryColor];
}

@end
