//
//  BTSideMenuWithStatusTableViewCell.h
//  CustomSlideMenu
//
//  Created by Lakhpat Meena on 12/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BTSideMenuItem;
@class BTSideMenuCellDataWrapper;

@interface BTSideMenuWithStatusTableViewCell : UITableViewCell {
    
}

@property (weak, nonatomic) IBOutlet UILabel *menuItemNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *openItemsCountLabel;
@property (weak, nonatomic) IBOutlet UIView *openItemStatusView;
@property (weak, nonatomic) IBOutlet UIImageView *tickImageView;

- (void)updateCellWithSideMenuItemData:(BTSideMenuCellDataWrapper *)sideMenuCellData;
- (void)updateSideMenuCellOnSelectionWithCellData:(BTSideMenuCellDataWrapper *)cellData;

@end
