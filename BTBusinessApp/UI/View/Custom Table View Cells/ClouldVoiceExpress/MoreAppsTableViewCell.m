//
//  MoreAppsTableViewCell.m
//  BTBusinessApp
//
//  Created by Manik on 05/03/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import "MoreAppsTableViewCell.h"
#import "BrandColours.h"

@implementation MoreAppsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    self.containerButtonView.layer.cornerRadius = 5.0;
//    self.innerButtonView.layer.cornerRadius = 5.0;
    self.masterView.layer.cornerRadius = 5.0f;
    self.btnMoreAppLogo.layer.cornerRadius = 15.0f;
    self.actionButton.layer.cornerRadius = 5.0f;
    self.actionButton.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
