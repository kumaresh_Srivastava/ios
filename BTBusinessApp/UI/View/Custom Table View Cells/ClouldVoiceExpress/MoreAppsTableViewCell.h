//
//  MoreAppsTableViewCell.h
//  BTBusinessApp
//
//  Created by Manik on 05/03/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MoreAppsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView   *masterView;
@property (weak, nonatomic) IBOutlet UIButton *btnMoreAppLogo;
@property (weak, nonatomic) IBOutlet UILabel  *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel  *descriptionLabel;
//@property (weak, nonatomic) IBOutlet UIView   *containerButtonView;
//@property (weak, nonatomic) IBOutlet UIView   *innerButtonView;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;

@end

NS_ASSUME_NONNULL_END
