//
//  ChooseNumberTitleTableViewCell.h
//  BTBusinessApp
//
//  Created by Manik on 15/03/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChooseNumberTitleTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel  *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel  *detailLabel1;
@property (weak, nonatomic) IBOutlet UILabel  *detailLabel2;
@property (weak, nonatomic) IBOutlet UIImageView  *disclosureImgView;

@end

NS_ASSUME_NONNULL_END
