//
//  BTInterceptorUpdateContactTableViewCell.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/15/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMUpdateContactInerceptScreen.h"
@interface BTInterceptorUpdateContactTableViewCell : UITableViewCell

- (void)removeSeparator:(BOOL)isRemove;

- (void)updateDataWithContactModel:(BTInterceptContactWrapper *)contactWrapper;



@end
