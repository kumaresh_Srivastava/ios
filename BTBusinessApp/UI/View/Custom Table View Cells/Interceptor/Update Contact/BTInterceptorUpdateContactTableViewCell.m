//
//  BTInterceptorUpdateContactTableViewCell.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/15/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTInterceptorUpdateContactTableViewCell.h"
#import "BrandColours.h"

@interface BTInterceptorUpdateContactTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *titleTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueTextLabel;
@property (weak, nonatomic) IBOutlet UIView *upperSeparator;
@property (weak, nonatomic) IBOutlet UIView *lowerSeparator;

@end

@implementation BTInterceptorUpdateContactTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)removeSeparator:(BOOL)isRemove{

    self.upperSeparator.hidden = isRemove;

}

- (void)updateDataWithContactModel:(BTInterceptContactWrapper *)contactWrapper
{
    self.titleTextLabel.text = contactWrapper.titleString;
    
    if(contactWrapper.ValueString && contactWrapper.ValueString.length >0)
        self.valueTextLabel.text = contactWrapper.ValueString;
    else
        self.valueTextLabel.text = @"";
}

@end
