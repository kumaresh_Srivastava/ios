
#import "BTTakeActionWithSwitchTableViewCell.h"
#import <LocalAuthentication/LocalAuthentication.h>

@implementation BTTakeActionWithSwitchTableViewCell
@synthesize delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    // Face ID or Touch ID?
    LAContext *context = [[LAContext alloc] init];
    NSError *localAuthError = nil;
    if (@available(iOS 11.0, *)) {
        if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&localAuthError]) {
            if(context.biometryType == LABiometryTypeFaceID) {
                [_takeActionTitleLabel setText:@"Enable Face ID"];
                [_takeActionDetailTitleLabel setText:@"Face ID uses facial recognition to log you in"];
            }
        }
    }
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (IBAction)takeActionSwitchChangedStatus:(id)sender {
    if([sender isOn]){
        [delegate userDidChangedSwitchValueOnTakeActionCell:self withSwitchStatus:YES];
    } else{
         [delegate userDidChangedSwitchValueOnTakeActionCell:self withSwitchStatus:NO];
    }
    
}

- (void)updateSwitchStatus:(BOOL)status
{
    DDLogInfo(@"Updating SWITCH STATUS");
    [self.takeActionSwich setOn:status];
}

@end
