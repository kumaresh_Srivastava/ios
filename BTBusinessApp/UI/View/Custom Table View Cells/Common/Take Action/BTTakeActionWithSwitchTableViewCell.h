//
//  BTTakeActionWithSwitchTableViewCell.h
//  SampleViewApplication
//
//  Created by vectoscalar on 15/11/16.
//  Copyright © 2016 VS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTTakeActionWithSwitchTableViewCell;

@protocol BTTakeActionSwitchTableViewCellDelegate <NSObject>

- (void)userDidChangedSwitchValueOnTakeActionCell:(BTTakeActionWithSwitchTableViewCell*)cell withSwitchStatus:(BOOL)switchStatus;

@end

@interface BTTakeActionWithSwitchTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *takeActionTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *takeActionDetailTitleLabel;
@property (weak, nonatomic) IBOutlet UISwitch *takeActionSwich;
@property (weak, nonatomic) IBOutlet UIView *takeActionSeparatorView;

@property (assign,nonatomic) id <BTTakeActionSwitchTableViewCellDelegate> delegate;

- (IBAction)takeActionSwitchChangedStatus:(id)sender;

- (void)updateSwitchStatus:(BOOL)status;


@end
