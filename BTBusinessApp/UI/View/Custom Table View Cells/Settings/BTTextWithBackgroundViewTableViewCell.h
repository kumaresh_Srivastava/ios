//
//  BTTextWithBackgroundViewTableViewCell.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 17/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BTTextWithBackgroundViewTableViewCell;

@protocol BTTextWithBackgroundViewTableViewCellDelegate <NSObject>

- (void)updatedSuccessfullyOnSelectionOnTextWithBackgroundViewTableViewCell:(BTTextWithBackgroundViewTableViewCell *)textWithBackgroundViewTableViewCell;

@end

@interface BTTextWithBackgroundViewTableViewCell : UITableViewCell {
    
}

@property (weak, nonatomic) IBOutlet UIView *textBackgroundView;
@property (weak, nonatomic) IBOutlet UILabel *cellTextLabel;
@property (weak, nonatomic) id<BTTextWithBackgroundViewTableViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *checkmarkPurpleImageView;

- (void)updateCellViewOnSelection;
- (void)removeSelection;
@end
