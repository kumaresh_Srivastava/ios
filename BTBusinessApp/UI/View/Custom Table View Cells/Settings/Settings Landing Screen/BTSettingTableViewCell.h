//
//  BTSettingTableViewCell.h
//  SampleViewApplication
//
//  Created by vectoscalar on 15/11/16.
//  Copyright © 2016 VS. All rights reserved.
//
#import "DLMSettingsScreen.h"
#import <UIKit/UIKit.h>

@class BTSettingTableViewCell ;


@protocol BTSettingsTableViewCellDelegate <NSObject>

- (void)userDidSelectSettingOptionFromCell:(BTSettingTableViewCell*)cell withTitleText:(NSString *)titleText;

@end

@interface BTSettingTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *settingsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *settingDetailTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *settingAccessoryImage;
@property (weak, nonatomic) IBOutlet UIView *separatorView;

@property (assign,nonatomic) id <BTSettingsTableViewCellDelegate> delegate;

- (void)updateInfoWithSettingsDLM:(DLMSettingsScreen*)dlmModel;

@end
