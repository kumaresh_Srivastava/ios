//
//  BTSettingTableViewCell.m
//  SampleViewApplication
//
//  Created by vectoscalar on 15/11/16.
//  Copyright © 2016 VS. All rights reserved.
//

#import "BTSettingTableViewCell.h"
#import "BTUICommon.h"
#import "BrandColours.h"


@implementation BTSettingTableViewCell
@synthesize delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateInfoWithSettingsDLM:(DLMSettingsScreen *)dlmModel
{
   if(dlmModel.title && dlmModel.title.length >0 )
   {
       self.settingsTitleLabel.text = dlmModel.title;
   }
   else
   {
      self.settingsTitleLabel.text = @"-";
   }
    
    if(dlmModel.subTitle && dlmModel.subTitle.length >0 )
    {
        self.settingDetailTitleLabel.text = dlmModel.subTitle;
    }
    else
    {
        self.settingDetailTitleLabel.text = @"-";
    }
    
    if(dlmModel.imgName)
    {
        //self.settingImageView.image = [UIImage imageNamed:dlmModel.imgName];
    }
}

@end
