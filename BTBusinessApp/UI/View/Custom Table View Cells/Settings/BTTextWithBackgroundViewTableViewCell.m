//
//  BTTextWithBackgroundViewTableViewCell.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 17/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTTextWithBackgroundViewTableViewCell.h"
#import "BrandColours.h"

@interface BTTextWithBackgroundViewTableViewCell () {
    
}

@end

@implementation BTTextWithBackgroundViewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _checkmarkPurpleImageView.hidden = YES;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCellViewOnSelection
{
    _textBackgroundView.layer.borderWidth = 1;
    _textBackgroundView.layer.borderColor = [BrandColours colourMyBtLightPurple].CGColor;
    
    _checkmarkPurpleImageView.hidden = NO;
    
    //[self.delegate updatedSuccessfullyOnSelectionOnTextWithBackgroundViewTableViewCell:self];
}

- (void)removeSelection
{
    _textBackgroundView.layer.borderWidth = 0;
    _textBackgroundView.layer.borderColor = [UIColor clearColor].CGColor;
    
    _checkmarkPurpleImageView.hidden = YES;
}

@end
