//
//  BTSettingsContactDetailTableViewCell.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/15/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMSettingsContactDetailScreen.h"

@interface BTSettingsContactDetailTableViewCell : UITableViewCell

- (void)updateWitContactWrapper:(BTContactRowWrapper *)rowWrapper;

@end
