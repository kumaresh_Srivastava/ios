//
//  BTSettingsContactDetailTableViewCell.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/15/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTSettingsContactDetailTableViewCell.h"

@interface BTSettingsContactDetailTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *titleTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueTextLabel;

@end

@implementation BTSettingsContactDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)updateWitContactWrapper:(BTContactRowWrapper *)rowWrapper{
    
    self.titleTextLabel.text = rowWrapper.titleString;
    self.valueTextLabel.text = rowWrapper.ValueString;
    
}


@end
