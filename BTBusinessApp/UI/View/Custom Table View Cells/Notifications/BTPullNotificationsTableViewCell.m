//
//  BTPullNotificationsTableViewCell.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 25/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTPullNotificationsTableViewCell.h"
#import "BTNotification.h"
#import "AppManager.h"
#import "BrandColours.h"

@implementation BTPullNotificationsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCellWithNotificationData:(BTNotification *)notification
{
    if (notification.notificationStatus == BTNotificationTypeUnRead)
    {
        _notificationLabel.text = notification.notificationSubject;
        _notificationTitleLabel.text = [NSString stringWithFormat:@"%@ sent", notification.displayNotificationMedium];
        _notificationTimeLabel.text = [NSString stringWithFormat:@"%@",[AppManager getDayMonthYearFromDate:notification.notificationDate]];
         self.contentView.backgroundColor = [BrandColours colourBtLightGray]; //BUG 16693
        _notificationLabel.textColor = [BrandColours colourTextBTPurplePrimaryColor];
    }
    else
    {        
        _notificationLabel.text = notification.notificationSubject;
        _notificationTitleLabel.text = [NSString stringWithFormat:@"%@ sent", notification.displayNotificationMedium];
        _notificationTimeLabel.text = [NSString stringWithFormat:@"%@",[AppManager getDayMonthYearFromDate:notification.notificationDate]];
         self.contentView.backgroundColor = [UIColor whiteColor];
        _notificationLabel.textColor = [BrandColours colourBtLightBlack];
    }
    
    
}

- (NSString *)getReadImageForNotificationType:(NSString *)notificationCategory
{
    NSString *imageName = @"";
    
    if ([notificationCategory isEqualToString:@"Your account"]) {
        imageName = @"notification_read_account";
    }
    else if ([notificationCategory isEqualToString:@"Orders"]) {
        imageName = @"notification_read_order";
    }
    else if ([notificationCategory isEqualToString:@"Faults"]) {
        imageName = @"notification_read_fault";
    }
    else if ([notificationCategory isEqualToString:@"Billing"]) {
        imageName = @"notification_read_bill";
    }
    else if ([notificationCategory isEqualToString:@"Hub Fault"]) {
        imageName = @"notification_read_hub";
    }
    
    return imageName;
}

- (NSString *)getUnReadImageForNotificationType:(NSString *)notificationCategory
{
    NSString *imageName = @"";
    
    if ([notificationCategory isEqualToString:@"Your account"]) {
        imageName = @"notification_account";
    }
    else if ([notificationCategory isEqualToString:@"Orders"]) {
        imageName = @"notification_order";
    }
    else if ([notificationCategory isEqualToString:@"Faults"]) {
        imageName = @"notification_fault";
    }
    else if ([notificationCategory isEqualToString:@"Billing"]) {
        imageName = @"notification_bill";
    }
    else if ([notificationCategory isEqualToString:@"Hub Fault"]) {
        imageName = @"notification_hub";
    }
    
    return imageName;
}

@end
