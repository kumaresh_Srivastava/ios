//
//  BTPullNotificationsTableViewCell.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 25/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTNotification;
@class BTPullNotificationsTableViewCell;

@protocol BTPullNotificationsTableViewCellDelegate <NSObject>

- (void)cellUIUpdatedOnPullNotificationsTableViewCell:(BTPullNotificationsTableViewCell *)notificationCell;

@end

@interface BTPullNotificationsTableViewCell : UITableViewCell {
    
}

@property (weak, nonatomic) IBOutlet UILabel *notificationLabel;
@property (weak, nonatomic) IBOutlet UILabel *notificationTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *notificationTimeLabel;

- (void)updateCellWithNotificationData:(BTNotification *)notification;

@end
