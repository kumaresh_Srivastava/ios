//
//  BTFaultTrackerSummaryTableViewCell.h
//  BTBusinessApp
//
//  Created by Saddam Husain on 26/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTFault.h"

@interface BTFaultTrackerSummaryTableViewCell : UITableViewCell

- (void)updateCellWithFaultData:(BTFault *)fault;

@end
