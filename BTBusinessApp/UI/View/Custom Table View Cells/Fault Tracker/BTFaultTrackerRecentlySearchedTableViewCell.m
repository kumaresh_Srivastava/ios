//
//  BTFaultTrackerRecentlySearchedTableViewCell.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 28/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultTrackerRecentlySearchedTableViewCell.h"
#import "BTRecentSearchedFault.h"
#import "AppManager.h"
#import "BTOrderStatusLabel.h"
#import "BrandColours.h"

@interface BTFaultTrackerRecentlySearchedTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *faultReferenceLabel;
@property (weak, nonatomic) IBOutlet UILabel *faultDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *reportedLabel;
@property (weak, nonatomic) IBOutlet UILabel *reportedDateLabel;
@property (weak, nonatomic) IBOutlet BTOrderStatusLabel *statusLabel;

@end

@implementation BTFaultTrackerRecentlySearchedTableViewCell

- (void)awakeFromNib {

    [super awakeFromNib];
    // Initialization code
    
    [self setBackgroundColor:[UIColor whiteColor]];//BUG 17114
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//table cell view actions
- (void)updateCellWithRecentSearchedFaultData:(BTRecentSearchedFault *)recentSearchedFault
{
    _faultReferenceLabel.text = recentSearchedFault.faultRef;
    _faultDetailLabel.text = recentSearchedFault.faultDescription;
    
    if (recentSearchedFault.reportedOnDate != nil) {
        _reportedDateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:recentSearchedFault.reportedOnDate];
        _reportedLabel.text = @"Reported";
    }
    else
    {
        _reportedDateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:recentSearchedFault.lastSearchedDate];
        _reportedLabel.text = @"Searched";
    }
    
    if ([recentSearchedFault.faultStatus rangeOfString:@"Fault" options:NSCaseInsensitiveSearch].location == 0) {
        [self.statusLabel setOrderStatus:[NSString stringWithFormat:@"%@",[[recentSearchedFault.faultStatus substringFromIndex:6] capitalizedString]]];
    } else
    {
        [self.statusLabel setOrderStatus:[recentSearchedFault.faultStatus capitalizedString]];
    }

}

@end
