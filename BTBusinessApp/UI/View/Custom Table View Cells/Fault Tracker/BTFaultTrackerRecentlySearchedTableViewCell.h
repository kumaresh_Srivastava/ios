//
//  BTFaultTrackerRecentlySearchedTableViewCell.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 28/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BTRecentSearchedFault;

@interface BTFaultTrackerRecentlySearchedTableViewCell : UITableViewCell

- (void)updateCellWithRecentSearchedFaultData:(BTRecentSearchedFault *)recentSearchedFault;

@end
