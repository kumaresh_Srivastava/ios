//
//  BTFaultTrackerDashboardTableViewCell.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 28/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BTFault;


@interface BTFaultTrackerDashboardTableViewCell : UITableViewCell

- (void)updateCellWithFaultData:(BTFault *)fault orderType:(int)orderType;

@end
