//
//  BTFaultTrackerDashboardTableViewCell.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 28/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultTrackerDashboardTableViewCell.h"
#import "AppManager.h"
#import "BTFault.h"
#import "BTOrderStatusLabel.h"
#import "BrandColours.h"

@interface BTFaultTrackerDashboardTableViewCell ()

@property (strong, nonatomic) IBOutlet UILabel *faultReferenceValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *reportedOnDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *lineOrBroadbandNumberValueLabel;
@property (strong, nonatomic) IBOutlet BTOrderStatusLabel *statusValueLabel;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end

@implementation BTFaultTrackerDashboardTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    [self setBackgroundColor:[UIColor clearColor]];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    self.containerView.layer.borderColor = [BrandColours colourBtNeutral50].CGColor;
    self.containerView.layer.borderWidth = 1.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//table cell view actions
- (void)updateCellWithFaultData:(BTFault *)fault orderType:(int)orderType
{
    _faultReferenceValueLabel.text = fault.faultReference;
    _lineOrBroadbandNumberValueLabel.text = fault.productName;
    _reportedOnDateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:fault.reportedOnDate];
    if ([fault.status isEqualToString:@"Fault resolved"]) {
        [_statusValueLabel setOrderStatus:@"Resolved"];
    } else if ([fault.status isEqualToString:@"Fault in progress"]) {
        [_statusValueLabel setOrderStatus:@"In progress"];
    } else {
        [_statusValueLabel setOrderStatus:fault.status];
    }
    
//    if ([fault.status isEqualToString:@"Fault resolved"]) {
//        //1 for OpenOrder
//        if (orderType == 1) {
//            _statusValueLabel.layer.borderWidth = 1.0f;
//            _statusValueLabel.layer.cornerRadius = 2.0f;
//            _statusValueLabel.layer.borderColor = [BrandColours colourMyBtOrange].CGColor;
//            [_statusValueLabel setTextColor:[BrandColours colourMyBtOrange]];
//        } else {
//            _statusValueLabel.layer.borderWidth = 1.0f;
//            _statusValueLabel.layer.cornerRadius = 2.0f;
//            _statusValueLabel.layer.borderColor = [BrandColours colourMyBtGreen].CGColor;
//            [_statusValueLabel setTextColor:[BrandColours colourMyBtGreen]];
//        }
//        _statusValueLabel.text = @"Resolved";
//    } else if ([fault.status isEqualToString:@"Fault in progress"]) {//Fault in progress
//        _statusValueLabel.layer.borderWidth = 1.0f;
//        _statusValueLabel.layer.cornerRadius = 2.0f;
//        _statusValueLabel.layer.borderColor = [BrandColours colourMyBtOrange].CGColor;
//        [_statusValueLabel setTextColor:[BrandColours colourMyBtOrange]];
//        _statusValueLabel.text = @"In progress";
//    } else {
//        _statusValueLabel.layer.borderWidth = 1.0f;
//        _statusValueLabel.layer.cornerRadius = 2.0f;
//        _statusValueLabel.layer.borderColor = [BrandColours colourMyBtOrange].CGColor;
//        [_statusValueLabel setTextColor:[BrandColours colourMyBtOrange]];
//
//        _statusValueLabel.text = fault.status;x
}


@end
