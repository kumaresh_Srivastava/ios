//
//  BTFaultTrackerSummaryTableViewCell.m
//  BTBusinessApp
//
//  Created by Saddam Husain on 26/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultTrackerSummaryTableViewCell.h"
#import "AppManager.h"
#import "BTOrderStatusLabel.h"
#import "BrandColours.h"

@interface BTFaultTrackerSummaryTableViewCell ()

@property (strong, nonatomic) IBOutlet UILabel *faultReferenceValueLabel;

@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;

@property (strong, nonatomic) IBOutlet BTOrderStatusLabel *statusValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *reportedOnDateLabel;


@end

@implementation BTFaultTrackerSummaryTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];

    [self setBackgroundColor:[UIColor clearColor]];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//table cell view actions
- (void)updateCellWithFaultData:(BTFault *)fault
{
    _faultReferenceValueLabel.text = fault.faultReference;
    _productNameLabel.text = fault.productName;
    _reportedOnDateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:fault.reportedOnDate];
    //self.statusValueLabel.text = [UIColor colo]
    if ([fault.statusOnUI rangeOfString:@"Fault" options:NSCaseInsensitiveSearch].location == 0) {
        [self.statusValueLabel setOrderStatus:[NSString stringWithFormat:@"%@",[[fault.statusOnUI substringFromIndex:6] capitalizedString]]];
    } else
    {
        [self.statusValueLabel setOrderStatus:[fault.statusOnUI capitalizedString]];
    }    
}

#pragma mark - Helper Methods

- (UIColor *)getTextColorWithColorStatus:(NSString *)colorStatus
{
    UIColor *color = [UIColor blackColor];
    if([colorStatus isEqualToString:@"status-orange"])
    {
        self.layer.borderWidth = 1.0f;
        self.layer.cornerRadius = 2.0f;
        self.layer.borderColor = [BrandColours colourMyBtOrange].CGColor;
        color = [BrandColours colourMyBtOrange];
    }
    else if([colorStatus isEqualToString:@"status-green"])
    {
        self.layer.borderWidth = 1.0f;
        self.layer.cornerRadius = 2.0f;
        self.layer.borderColor = [BrandColours colourMyBtGreen].CGColor;
        color = [BrandColours colourMyBtGreen];
    }


    return color;
}
@end
