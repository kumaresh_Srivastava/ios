//
//  AboutTheAppTableViewCell.h
//  BTBusinessApp
//
//  Created by Manik on 22/07/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AboutTheAppTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *label;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end

NS_ASSUME_NONNULL_END
