//
//  BTFaultAccessTimeSlotCell.h
//  BTBusinessApp
//
//  Created by VectoScalar on 1/4/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BTFaultAccessTimeSlotCell;

@protocol BTFaultAccessTimeSlotCellDelegate <NSObject>

- (void)btFaultAccessTimeSlotCell:(BTFaultAccessTimeSlotCell *)cell didSelectEarliestTimeView:(UIView *)earliestTimeView;
- (void)btFaultAccessTimeSlotCell:(BTFaultAccessTimeSlotCell *)cell didLatestTimeView:(UIView *)latestTimeView;

@end

@interface BTFaultAccessTimeSlotCell : UITableViewCell
@property(nonatomic, assign) id<BTFaultAccessTimeSlotCellDelegate>delegate;

- (void)updateWithSlotDate:(NSDate *)slotDate earlierTime:(NSString *)earlierTime latestTime:(NSString *)latestTime andShowDetail:(BOOL)showDetail;

@end
