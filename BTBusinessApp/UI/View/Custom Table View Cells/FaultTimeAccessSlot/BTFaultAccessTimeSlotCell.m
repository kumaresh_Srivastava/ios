//
//  BTFaultAccessTimeSlotCell.m
//  BTBusinessApp
//
//  Created by VectoScalar on 1/4/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTFaultAccessTimeSlotCell.h"
#import "BTFaultAppointment.h"

const float lowPriorityHeight = 250;
const float highPriorityHeight = 999;


#define kBorderColor [BrandColours colourBtNeutral50]
#define kBackgroundColor [BrandColours colourBtBackgroundColor]

@interface BTFaultAccessTimeSlotCell(){
    
    CGFloat detailViewHeight;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *detailViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *radioButton;
@property (weak, nonatomic) IBOutlet UIView *earliestTimeView;
@property (weak, nonatomic) IBOutlet UIView *latestTimeView;
@property (weak, nonatomic) IBOutlet UILabel *earliestTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *latestTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end

@implementation BTFaultAccessTimeSlotCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.radioButton.userInteractionEnabled = NO;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    detailViewHeight = self.detailViewHeightConstraint.constant;
    self.detailViewHeightConstraint.constant = 0;
    
    //Selction color
    UIView *cellBg = [[UIView alloc] init];
    cellBg.backgroundColor = kBackgroundColor;
    cellBg.layer.masksToBounds = YES;
    self.selectedBackgroundView = cellBg;
    
    _latestTimeView.layer.borderWidth = 1.0;
    _latestTimeView.layer.cornerRadius = 4.0;
    _latestTimeView.layer.borderColor = kBorderColor.CGColor;
    
    _earliestTimeView.layer.borderWidth = 1.0;
    _earliestTimeView.layer.cornerRadius = 4.0;
    self.earliestTimeView.layer.borderColor = kBorderColor.CGColor;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    //self.radioButton.selected = selected;
}



#pragma mark- public method

- (void)higlightEarlierTimeView{
    
    _earliestTimeView.layer.shadowRadius  = 1.8f;
    _earliestTimeView.layer.shadowColor   = [BrandColours colourBtPrimaryColor].CGColor;

    _earliestTimeView.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    _earliestTimeView.layer.shadowOpacity = 0.9f;
    _earliestTimeView.layer.masksToBounds = NO;
}

- (void)unhiglightEarlierTimeView{
    
    [UIView animateWithDuration:0.3 delay:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        _earliestTimeView.layer.shadowOpacity = 0.0f;
        
    } completion:^(BOOL finished) {
        
    }];

}

- (void)higlightLatestTimeView{
    
    _latestTimeView.layer.shadowRadius  =  1.8f;
    _latestTimeView.layer.shadowColor   = [BrandColours colourBtPrimaryColor].CGColor;
    _latestTimeView.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    _latestTimeView.layer.shadowOpacity = 0.9f;
    _latestTimeView.layer.masksToBounds = NO;
}

- (void)unhiglightLatestTimeView{
    
    [UIView animateWithDuration:0.5 animations:^{
        
        _latestTimeView.layer.shadowOpacity = 0;
        
    } completion:^(BOOL finished) {
        
    }];
    
}





#pragma mark- Helper Method

- (NSString *)getFormattedDateFromDate:(NSDate *)date{
    
    NSString *outputFormate = @"EEE dd MMM";
    NSString *outputDateString = @"";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if(date){
        
        [dateFormatter setDateFormat:outputFormate];
        outputDateString= [dateFormatter stringFromDate:date];
    }
    return outputDateString;
}



- (void)updateWithSlotDate:(NSDate *)slotDate earlierTime:(NSString *)earlierTime latestTime:(NSString *)latestTime andShowDetail:(BOOL)showDetail{
    
    if(showDetail){
        
        self.detailViewHeightConstraint.constant = 96;
        self.radioButton.selected = YES;
        self.backgroundColor = kBackgroundColor;
    }
    else{
        
        self.detailViewHeightConstraint.constant = 0;
        self.radioButton.selected = NO;
        [self setSelected:NO animated:YES];
        self.backgroundColor = [UIColor clearColor];
    }
    
    self.dateLabel.text = [self getFormattedDateFromDate:slotDate];
    self.latestTimeLabel.text = latestTime;
    self.earliestTimeLabel.text = earlierTime;
    
    [self unhiglightLatestTimeView];
    [self unhiglightEarlierTimeView];

}


- (IBAction)latestTimeButtonPressed:(id)sender {
    
    [self.delegate btFaultAccessTimeSlotCell:self didLatestTimeView:self.latestTimeView];
    [self higlightLatestTimeView];
}

- (IBAction)earliestTimeButtonPressed:(id)sender {
    
    [self.delegate btFaultAccessTimeSlotCell:self didSelectEarliestTimeView:self.earliestTimeView];
    [self higlightEarlierTimeView];
}

@end
