//
//  BTBundlePricingTableViewCell.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 14/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBundlePricingTableViewCell.h"

@implementation BTBundlePricingTableViewCell

- (void)awakeFromNib {

    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCellWithTitle:(NSString *)title description:(NSString *)description pricingValue:(double)pricingValue andPricingValue:(NSString *)pricingTypeLabel
{
    self.titleLabel.text = title;
    self.titleDescriptionLabel.text = description;
    self.pricingValueLabel.text = [NSString stringWithFormat:@"£%.2f", pricingValue];
    self.pricingLabel.text = pricingTypeLabel;
}

@end
