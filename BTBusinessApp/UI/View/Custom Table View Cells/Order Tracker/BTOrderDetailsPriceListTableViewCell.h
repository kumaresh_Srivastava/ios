//
//  BTOrderDetailsPriceListTableViewCell.h
//  BTBusinessApp
//
//  Created by Accolite on 08/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTOrderDetailsPriceListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *oneoffLabel;
@property (weak, nonatomic) IBOutlet UILabel *monthlyLabel;

@property (strong,nonatomic) NSString *regularViewString;
- (void)setValuesWithDictionary:(NSDictionary *)dict andFrequency:(NSString *)freq;
@end
