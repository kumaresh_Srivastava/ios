//
//  BTOrderTrackerTableViewCell.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 22/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTProduct;

@interface BTOrderTrackerTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *orderNameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *prdouctIconImage;
@property (strong, nonatomic) IBOutlet UILabel *oneOffValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *regularValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *estimatedCompletionTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *expectedCompletionLabel;

//- (void)updateCellWithProduct:(BTProduct *)product;
- (void)updateCellWithProduct:(BTProduct *)product isShowAppointment:(BOOL)isShowAppointment;
- (void)updateCellWithProductDict:(NSDictionary *)productDict;

@end
