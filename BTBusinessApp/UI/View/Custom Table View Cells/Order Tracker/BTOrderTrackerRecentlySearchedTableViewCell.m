//
//  BTOrderTrackerRecentlySearchedTableViewCell.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 28/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderTrackerRecentlySearchedTableViewCell.h"
#import "BTRecentSearchedOrder.h"
#import "BrandColours.h"
#import "AppManager.h"

@implementation BTOrderTrackerRecentlySearchedTableViewCell

- (void)awakeFromNib {

    [super awakeFromNib];
    
    // Initialization code
    
    [self setBackgroundColor:[UIColor clearColor]];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//table cell view actions
- (void)updateCellWithRecentSearchedOrderData:(BTRecentSearchedOrder *)recentSearchedOrderData {
    
    self.orderIDLabel.text = recentSearchedOrderData.orderRef;
    self.orderDetailLabel.text = recentSearchedOrderData.orderDescription;
    NSString *orderStatus = [recentSearchedOrderData.orderStatus lowercaseString];

    
    if(orderStatus.length>0)
        self.orderStatusLabel.text = [orderStatus stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[orderStatus substringToIndex:1] uppercaseString]];
    else
        self.orderStatusLabel.text = @"";
;
    
    if ([[orderStatus lowercaseString] isEqualToString:@"completed"]) {
        [self.orderStatusLabel setTextColor:[BrandColours colourMyBtGreen]];
        self.placedOrCompletedLabel.text = @"Completed";
        
        if ([AppManager isValidDate:recentSearchedOrderData.completionDate])
        {
            self.placedOrCompletedDateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:recentSearchedOrderData.completionDate];
        }
        else
        {
            self.placedOrCompletedDateLabel.text = @"-";
        }
        
        self.orderStatusLabel.text = @"Complete";
    } else if ([[orderStatus lowercaseString] isEqualToString:@"delayed"]) {
        [self.orderStatusLabel setTextColor:[BrandColours colourMyBtRed]];
        self.placedOrCompletedLabel.text = @"Ordered";
        self.placedOrCompletedDateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:recentSearchedOrderData.placedOnDate];
        
    }
    else if ([[orderStatus lowercaseString] isEqualToString:@"in progress"]) {
        [self.orderStatusLabel setTextColor:[BrandColours colourMyBtOrange]];
        self.placedOrCompletedLabel.text = @"Ordered";
        self.placedOrCompletedDateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:recentSearchedOrderData.placedOnDate];
        self.orderStatusLabel.text = @"In progress";
    }
    else {
        [self.orderStatusLabel setTextColor:[BrandColours colourMyBtRed]];
        self.placedOrCompletedLabel.text = @"Ordered";
        self.placedOrCompletedDateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:recentSearchedOrderData.placedOnDate];
    }
}




@end
