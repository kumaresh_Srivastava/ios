//
//  BTOrderTrackerTableViewCell.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 22/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderTrackerTableViewCell.h"
#import "BTProduct.h"
#import "AppManager.h"

@interface BTOrderTrackerTableViewCell ()


@end

@implementation BTOrderTrackerTableViewCell

- (void)awakeFromNib {

    [super awakeFromNib];
    
    // Initialization code
    
    [self setBackgroundColor:[UIColor whiteColor]];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

//table cell view actions
- (void)updateCellWithProduct:(BTProduct *)product isShowAppointment:(BOOL)isShowAppointment
{
    self.orderNameLabel.text = product.productName;
    NSString *OneOffPriceString = [NSString stringWithFormat:@"£ %.2f",product.oneOffPrice];
    self.oneOffValueLabel.text = OneOffPriceString;
    NSString *MonthlyPriceString = [NSString stringWithFormat:@"£ %.2f",product.monthlyPrice];
    self.regularValueLabel.text = MonthlyPriceString;
    if ([product.status isEqualToString:@"Completed"]){
        self.expectedCompletionLabel.text = @"Completed on";
    }
     if(isShowAppointment == NO){
          self.estimatedCompletionTimeLabel.text = @"Will be updated shortly";
     } else {
            if ([AppManager isValidDate:product.productDuedate]) {
                self.estimatedCompletionTimeLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:product.productDuedate];
            }
            else {
                self.estimatedCompletionTimeLabel.text = @"-";
            }
     }
    
    self.prdouctIconImage.image = [UIImage imageNamed:[self getImageWithProductName:product.productIcon]];
}

//table cell view actions
- (void)updateCellWithProductDict:(NSDictionary *)productDict
{
    self.orderNameLabel.text = [productDict objectForKey:@"ProductName"];
    NSNumber *OneOffPrice = [productDict objectForKey:@"OneOffPrice"];
    NSString *OneOffPriceString = [NSString stringWithFormat:@"£ %.2f",[OneOffPrice doubleValue]];
    self.oneOffValueLabel.text = OneOffPriceString;
    NSNumber *MonthlyPrice = [productDict objectForKey:@"MonthlyPrice"];
    NSString *MonthlyPriceString = [NSString stringWithFormat:@"£ %.2f",[MonthlyPrice doubleValue]];
    self.regularValueLabel.text = MonthlyPriceString;
    self.estimatedCompletionTimeLabel.text = [productDict objectForKey:@"ProductDuedate"];
    self.prdouctIconImage.image = [UIImage imageNamed:[self getImageWithProductName:[productDict objectForKey:@"ProductIcon"]]];
}

- (NSString *)getImageWithProductName:(NSString *)nameString
{
    NSString *imageName = nil;
    
    if ([nameString isEqualToString:@"bundleIcon"] || [nameString isEqualToString:@"bundleIconSmall"]) {
        imageName = @"bundle_small";
    } else if ([nameString isEqualToString:@"broadbandIcon"] || [nameString isEqualToString:@"broadbandIconSmall"]) {
        imageName = @"broadband_small";
    } else if ([nameString isEqualToString:@"phoneIcon"] || [nameString isEqualToString:@"phoneIconSmall"]) {
        imageName = @"phoneline_small";
    } else if ([nameString isEqualToString:@"hubIcon"] || [nameString isEqualToString:@"hubIconSmall"]) {
        imageName = @"broadband_small";
    } else if ([nameString isEqualToString:@"isdnIcon"] || [nameString isEqualToString:@"isdnIconSmall"]) {
        imageName = @"phoneline_small";
    }
    
    return imageName;
}



@end
