//
//  BTOrderTrackerRecentlySearchedTableViewCell.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 28/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BTRecentSearchedOrder;

@interface BTOrderTrackerRecentlySearchedTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *orderIDLabel;
@property (strong, nonatomic) IBOutlet UILabel *orderDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *placedOrCompletedLabel;
@property (strong, nonatomic) IBOutlet UILabel *placedOrCompletedDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *orderStatusLabel;

- (void)updateCellWithRecentSearchedOrderData:(BTRecentSearchedOrder *)recentSearchedOrderData;

@end
