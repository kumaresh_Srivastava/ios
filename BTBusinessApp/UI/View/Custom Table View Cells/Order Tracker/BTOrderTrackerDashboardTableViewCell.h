//
//  UIOrderTrackerDashboardTableViewCell.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 29/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseTableViewCell.h"

@class BTOCSProject;
@class BTOrder;
@class BTRecentSearchedOrder;

@interface BTOrderTrackerDashboardTableViewCell : BTBaseTableViewCell

@property (weak, nonatomic) IBOutlet BTOrderStatusLabel *orderStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderIdValue;
@property (weak, nonatomic) IBOutlet UILabel *placedOrCompletedLabel;
//@property (weak, nonatomic) IBOutlet UILabel *placedOrCompletedDateValue;
@property (weak, nonatomic) IBOutlet UIImageView *disclosureImageView;
@property (weak, nonatomic) IBOutlet UIButton *infoButton;
@property (weak, nonatomic) IBOutlet UIView *mainView;
//@property (weak, nonatomic) IBOutlet UILabel *noOrderLabel;

//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionLabelTopConstraint;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *placedOnDateTopConstraint;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *placedOnDateBottomConstraint;
@property (nonatomic,strong) NSString *orderID;
@property (nonatomic,strong) NSString *orderDescription;

@property (nonatomic, strong) NSObject *orederObj;

- (void)updateCellWithOrderData:(BTOrder *)orderData isOpenOrder:(BOOL)isOpenOrder;
- (void)updateCellWithProjectData:(BTOCSProject *)projectData isOpenOrder:(BOOL)isOpenOrder;
- (void)updateCellWithRecentSearchedOrderData:(BTRecentSearchedOrder *)recentSearchedOrderData;


//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orderDescriptionLabelHeightConstraint;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *noOrderLabelHeightConstraint;
@end
