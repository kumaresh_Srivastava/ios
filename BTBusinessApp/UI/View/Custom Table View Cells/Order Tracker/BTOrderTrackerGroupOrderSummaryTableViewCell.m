//
//  BTOrderTrackerGroupOrderSummaryTableViewCell.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 07/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderTrackerGroupOrderSummaryTableViewCell.h"
#import "AppManager.h"
#import "BTOrder.h"

@implementation BTOrderTrackerGroupOrderSummaryTableViewCell

- (void)awakeFromNib {

    [super awakeFromNib];
    // Initialization code


    [self setBackgroundColor:[UIColor clearColor]];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)updateGroupOrderCellWithOrderData:(BTOrder *)orderData
{
    self.orderIdValueLabel.text = orderData.orderRef;
    self.orderDetailsLabel.text = orderData.orderDescription;
    
    NSString *orderStatus = orderData.orderStatus;
    self.orderStatusLabel.text = orderStatus;
    
    if ([orderStatus isEqualToString:@"Completed"]) {
        [self.orderStatusLabel setTextColor:[BrandColours colourMyBtGreen]];
        self.placedOrCompletedLabel.text = @"Completed";
        
        if ([AppManager isValidDate:orderData.estimatedCompletionDate])
        {
            self.placedOrCompletedDateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:orderData.estimatedCompletionDate];
        }
        else
        {
            self.placedOrCompletedDateLabel.text = @"-";
        }
        
        self.orderStatusLabel.text = @"Complete";
    } else if ([orderStatus isEqualToString:@"Delayed"] || [orderStatus isEqualToString:@"Cancelled"]) {
        [self.orderStatusLabel setTextColor:[BrandColours colourMyBtRed]];
        self.placedOrCompletedLabel.text = @"Ordered";
        
        if ([AppManager isValidDate:orderData.orderPlacedDate])
        {
            self.placedOrCompletedDateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:orderData.orderPlacedDate];
        }
        else
        {
            self.placedOrCompletedDateLabel.text = @"-";
        }
    } else {
        [self.orderStatusLabel setTextColor:[BrandColours colourMyBtOrange]];
        self.placedOrCompletedLabel.text = @"Ordered";
        
        if ([AppManager isValidDate:orderData.orderPlacedDate])
        {
            self.placedOrCompletedDateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:orderData.orderPlacedDate];
        }
        else
        {
            self.placedOrCompletedDateLabel.text = @"-";
        }
    }
    
}


@end
