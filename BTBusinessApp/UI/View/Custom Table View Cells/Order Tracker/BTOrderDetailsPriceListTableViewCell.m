//
//  BTOrderDetailsPriceListTableViewCell.m
//  BTBusinessApp
//
//  Created by Accolite on 08/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderDetailsPriceListTableViewCell.h"
#import "AppConstants.h"

@implementation BTOrderDetailsPriceListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
//
- (void)setValuesWithDictionary:(NSDictionary *)dict andFrequency:(NSString *)freq{
    
    NSString *monthlyPriceString;
    if ([freq isEqualToString:@"Quarterly"]) {
        monthlyPriceString = [NSString stringWithFormat:@"£%.2f",[[dict objectForKey:@"QuarterlyPrice"] doubleValue]];
    } else {
        monthlyPriceString = [NSString stringWithFormat:@"£%.2f",[[dict objectForKey:@"MonthlyPrice"] doubleValue]];
    }
    NSString *oneoffPriceString = [NSString stringWithFormat:@"£%.2f",[[dict objectForKey:@"OneOffPrice"] doubleValue]];
    NSString *quantityPriceString = [NSString stringWithFormat:@"%@",[dict objectForKey:@"Quantity"]];
    
    [self.descriptionLabel setText:[dict objectForKey:@"DisplayName"]];
    [self.descriptionLabel setFont:[UIFont fontWithName:kBtFontRegular size:18.0f]];
    [self.quantityLabel setAttributedText:[self getAttributedStringFromString1:@"Qty "
                                                                       String2:quantityPriceString
                                                               firstStringFont:[UIFont fontWithName:kBtFontBold size:14]
                                                              secondStringFont:[UIFont fontWithName:kBtFontRegular size:14]]];
    [self.oneoffLabel setAttributedText:[self getAttributedStringFromString1:oneoffPriceString
                                                                     String2:@" one-off"
                                                             firstStringFont:[UIFont fontWithName:@"BTFont-Bold" size:14] secondStringFont:[UIFont fontWithName:@"BTFont-Regular" size:14]]];
    [self.monthlyLabel setAttributedText:[self getAttributedStringFromString1:monthlyPriceString
                                                                      String2:[NSString stringWithFormat: @" %@", self.regularViewString]
                                                             firstStringFont:[UIFont fontWithName:kBtFontRegular size:14] secondStringFont:[UIFont fontWithName:kBtFontBold size:14]]];

//    [self.oneoffLabel setText:oneoffPriceString];
//    [self.monthlyLabel setText:monthlyPriceString];
}

- (NSMutableAttributedString *)getAttributedStringFromString1:(NSString *)firstString String2:(NSString *)secondString firstStringFont:(UIFont *)firstFont secondStringFont:(UIFont *)secondFont {
    
    UIFont *firstStringFont = firstFont;//[UIFont fontWithName:@"BTFont-Bold" size:18.0];
    NSMutableDictionary *firstStringDict = [NSMutableDictionary dictionaryWithObject:firstStringFont forKey:NSFontAttributeName];
    [firstStringDict setObject:[UIColor colorForHexString:@"333333"] forKey:NSForegroundColorAttributeName];
    NSMutableAttributedString *firstAttrString = [[NSMutableAttributedString alloc] initWithString:firstString attributes:firstStringDict];
    
    UIFont *secondStringFont = secondFont;//[UIFont fontWithName:@"BTFont-Bold" size:13.0];
    NSMutableDictionary *secondStringDict = [NSMutableDictionary dictionaryWithObject:secondStringFont forKey:NSFontAttributeName];
    [secondStringDict setObject:[UIColor colorForHexString:@"666666"] forKey:NSForegroundColorAttributeName];
    NSMutableAttributedString *secondAttrString = [[NSMutableAttributedString alloc]initWithString:secondString attributes:secondStringDict];
    
    [firstAttrString appendAttributedString:secondAttrString];
    return firstAttrString;
}

@end
