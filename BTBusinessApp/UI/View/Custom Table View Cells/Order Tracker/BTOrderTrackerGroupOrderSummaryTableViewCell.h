//
//  BTOrderTrackerGroupOrderSummaryTableViewCell.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 07/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BrandColours.h"

@class BTOrder;

@interface BTOrderTrackerGroupOrderSummaryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *orderIdValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderDetailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *placedOrCompletedDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *placedOrCompletedLabel;

-(void)updateGroupOrderCellWithOrderData:(BTOrder *)orderData;

@end
