//
//  BTBundlePricingTableViewCell.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 14/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTBundlePricingTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *pricingLabel;
@property (weak, nonatomic) IBOutlet UILabel *pricingValueLabel;

- (void)updateCellWithTitle:(NSString *)title description:(NSString *)description pricingValue:(double)pricingValue andPricingValue:(NSString *)pricingTypeLabel;

@end
