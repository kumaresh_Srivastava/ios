//
//  UIOrderTrackerDashboardTableViewCell.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 29/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderTrackerDashboardTableViewCell.h"
#import "BrandColours.h"
#import "AppManager.h"
#import "AppConstants.h"
#import "BTOrder.h"
#import "BTOCSProject.h"
#import "BTRecentSearchedOrder.h"

@implementation BTOrderTrackerDashboardTableViewCell

+ (NSString *)reuseId
{
    return  @"orderTrackerDashboardTableViewCell";
}

- (CGFloat)heightForCell
{
    return UITableViewAutomaticDimension;
}

- (void)awakeFromNib {
    // Initialization code

    [super awakeFromNib];
    //self.mainView.layer.borderColor = [BrandColours colourBtNeutral50].CGColor;
    //self.mainView.layer.borderWidth = 1.0;
    self.orderStatusLabel.layer.borderWidth = 1.0f;
    self.orderStatusLabel.layer.cornerRadius = 2.0f;
    self.orderStatusLabel.layer.borderColor = [UIColor clearColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    // Configure the view for the selected state
    [super setSelected:selected animated:animated];

    [self setBackgroundColor:[UIColor whiteColor]];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

//table cell view actions
- (void)updateCellWithOrderData:(BTOrder *)orderData isOpenOrder:(BOOL)isOpenOrder {
    self.orederObj = orderData;
    [self setOrderID:orderData.orderRef];
    self.orderIdValue.text = orderData.orderRef;
    [self setOrderDescription:orderData.orderDescription];
    if ([orderData.orderDescription isEqualToString:@"Order details could not be fetched"]) {
        self.orderStatusLabel.hidden = YES;
        self.disclosureImageView.hidden = YES;
        self.infoButton.hidden = NO;
        //[self updateUIForNoOrderDetailsOrder];
        //[self.noOrderLabel setTextColor:[BrandColours colourMyBtOrange]];
        //self.noOrderLabel.text = @"You can't view this just yet";
        [self updateLabel:self.orderDescriptionLabel withBoldText:@"Status:" andRegularText:@"You can't view this just yet"];
    } else {
        //[self.noOrderLabel setHidden:YES];
        //self.orderDescriptionLabelHeightConstraint.constant = 18;
        //self.noOrderLabelHeightConstraint.constant = 0;
        //self.orderDescriptionLabel.text = orderData.orderDescription;
        [self updateLabel:self.orderDescriptionLabel withBoldText:@"Product:" andRegularText:orderData.orderDescription];
        self.orderStatusLabel.hidden = NO;
        self.disclosureImageView.hidden = NO;
        self.infoButton.hidden = YES;
    }
    
    NSString *orderStatus = orderData.orderStatus;
    
    if ([orderStatus isEqualToString:@"Completed"]) {
        [self.orderStatusLabel setTextColor:[UIColor whiteColor]];
        self.orderStatusLabel.layer.borderColor = [BrandColours colourMyBtGreen].CGColor;
        self.orderStatusLabel.backgroundColor = [BrandColours colourMyBtGreen];
        self.orderStatusLabel.text = orderStatus;
        
        if (isOpenOrder)
        {
            //self.placedOrCompletedLabel.text = @"Ordered";
            //self.placedOrCompletedDateValue.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:orderData.orderPlacedDate];
            if ([AppManager isValidDate:orderData.orderPlacedDate]) {
                [self updateLabel:self.placedOrCompletedLabel withBoldText:@"Ordered:" andRegularText:[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:orderData.orderPlacedDate]];
            } else {
                [self updateLabel:self.placedOrCompletedLabel withBoldText:@"Ordered:" andRegularText:@"-"];
            }
            
        }
        else
        {
            //self.placedOrCompletedLabel.text = @"Completed";
            
            if ([AppManager isValidDate:orderData.estimatedCompletionDate])
            {
                //self.placedOrCompletedDateValue.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:orderData.estimatedCompletionDate];
                [self updateLabel:self.placedOrCompletedLabel withBoldText:@"Completed:" andRegularText:[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:orderData.estimatedCompletionDate]];
            }
            else
            {
                //self.placedOrCompletedDateValue.text = @"-";
                [self updateLabel:self.placedOrCompletedLabel withBoldText:@"Completed:" andRegularText:@"-"];
            }
        }
    } else if ([orderStatus isEqualToString:@"Delayed"]) {
        self.orderStatusLabel.text = orderStatus;
        [self.orderStatusLabel setTextColor:[BrandColours colourMyBtRed]];
        self.orderStatusLabel.layer.borderColor = [BrandColours colourMyBtRed].CGColor;
        self.orderStatusLabel.backgroundColor = [UIColor clearColor];
        //self.placedOrCompletedLabel.text = @"Ordered";
        //self.placedOrCompletedDateValue.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:orderData.orderPlacedDate];
        [self updateLabel:self.placedOrCompletedLabel withBoldText:@"Ordered:" andRegularText:[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:orderData.orderPlacedDate]];
    } else if ([orderStatus isEqualToString:@"In Progress"]) {
        orderStatus = @"In progress";
        self.orderStatusLabel.text = orderStatus;
        [self.orderStatusLabel setTextColor:[BrandColours colourMyBtGreen]];
        self.orderStatusLabel.layer.borderColor = [BrandColours colourMyBtGreen].CGColor;
        self.orderStatusLabel.backgroundColor = [UIColor clearColor];
        //self.placedOrCompletedLabel.text = @"Ordered";
        //self.placedOrCompletedDateValue.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:orderData.orderPlacedDate];
        [self updateLabel:self.placedOrCompletedLabel withBoldText:@"Ordered:" andRegularText:[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:orderData.orderPlacedDate]];
    }
    else {
        self.orderStatusLabel.text = orderStatus;
        [self.orderStatusLabel setTextColor:[BrandColours colourMyBtOrange]];
        self.orderStatusLabel.layer.borderColor = [BrandColours colourMyBtOrange].CGColor;
        self.orderStatusLabel.backgroundColor = [UIColor clearColor];
        //self.placedOrCompletedLabel.text = @"Ordered";
        //self.placedOrCompletedDateValue.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:orderData.orderPlacedDate];
        [self updateLabel:self.placedOrCompletedLabel withBoldText:@"Ordered:" andRegularText:[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:orderData.orderPlacedDate]];
    }
}

- (void)updateCellWithProjectData:(BTOCSProject *)projectData isOpenOrder:(BOOL)isOpenOrder
{
    self.orederObj = projectData;
    [self setOrderID:projectData.projectRef];
    self.orderIdValue.text = projectData.projectRef;
    
    [self setOrderDescription:@"OCS"];
    
    //[self.noOrderLabel setHidden:YES];
    //self.orderDescriptionLabelHeightConstraint.constant = 18;
    //self.noOrderLabelHeightConstraint.constant = 0;
    //self.orderDescriptionLabel.text = projectData.productName;
    [self updateLabel:self.orderDescriptionLabel withBoldText:@"Product:" andRegularText:projectData.productName];
    self.orderStatusLabel.hidden = NO;
    self.disclosureImageView.hidden = NO;
    self.infoButton.hidden = YES;
    
    NSString *orderStatus = projectData.subStatus;
    
    if ([orderStatus isEqualToString:@"Completed"]) {
        [self.orderStatusLabel setTextColor:[UIColor whiteColor]];
        self.orderStatusLabel.layer.borderColor = [BrandColours colourMyBtGreen].CGColor;
        self.orderStatusLabel.backgroundColor = [BrandColours colourMyBtGreen];
        self.orderStatusLabel.text = orderStatus;
        
        if (isOpenOrder)
        {
            //self.placedOrCompletedLabel.text = @"Ordered";
            //self.placedOrCompletedDateValue.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:projectData.projectStartDate];
            [self updateLabel:self.placedOrCompletedLabel withBoldText:@"Ordered:" andRegularText:[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:projectData.projectStartDate]];
        }
        else
        {
            //self.placedOrCompletedLabel.text = @"Completed";
            
            if ([AppManager isValidDate:projectData.projectEndDate])
            {
                //self.placedOrCompletedDateValue.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:projectData.projectEndDate];
                [self updateLabel:self.placedOrCompletedLabel withBoldText:@"Completed:" andRegularText:[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:projectData.projectEndDate]];
                
            }
            else
            {
                //self.placedOrCompletedDateValue.text = @"-";
                [self updateLabel:self.placedOrCompletedLabel withBoldText:@"Completed:" andRegularText:@"-"];
            }
        }
    } else if ([orderStatus isEqualToString:@"Delayed"]) {
        self.orderStatusLabel.text = orderStatus;
        [self.orderStatusLabel setTextColor:[BrandColours colourMyBtRed]];
        self.orderStatusLabel.layer.borderColor = [BrandColours colourMyBtRed].CGColor;
        self.orderStatusLabel.backgroundColor = [UIColor clearColor];
        //self.placedOrCompletedLabel.text = @"Ordered";
        //self.placedOrCompletedDateValue.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:projectData.projectStartDate];
        [self updateLabel:self.placedOrCompletedLabel withBoldText:@"Ordered:" andRegularText:[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:projectData.projectStartDate]];
    } else if ([orderStatus isEqualToString:@"In Progress"]) {
        orderStatus = @"In progress";
        self.orderStatusLabel.text = orderStatus;
        [self.orderStatusLabel setTextColor:[BrandColours colourMyBtGreen]];
        self.orderStatusLabel.layer.borderColor = [BrandColours colourMyBtGreen].CGColor;
        self.orderStatusLabel.backgroundColor = [UIColor clearColor];
        //self.placedOrCompletedLabel.text = @"Ordered";
        //self.placedOrCompletedDateValue.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:projectData.projectStartDate];
        [self updateLabel:self.placedOrCompletedLabel withBoldText:@"Ordered:" andRegularText:[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:projectData.projectStartDate]];
    }
    else {
        self.orderStatusLabel.text = orderStatus;
        [self.orderStatusLabel setTextColor:[BrandColours colourMyBtOrange]];
        self.orderStatusLabel.layer.borderColor = [BrandColours colourMyBtOrange].CGColor;
        self.orderStatusLabel.backgroundColor = [UIColor clearColor];
        //self.placedOrCompletedLabel.text = @"Ordered";
        //self.placedOrCompletedDateValue.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:projectData.projectStartDate];
        [self updateLabel:self.placedOrCompletedLabel withBoldText:@"Ordered:" andRegularText:[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:projectData.projectStartDate]];
    }
}

- (void)updateCellWithRecentSearchedOrderData:(BTRecentSearchedOrder *)recentSearchedOrderData {
    self.orederObj = recentSearchedOrderData;
    self.orderIdValue.text = recentSearchedOrderData.orderRef;
    [self updateLabel:self.orderDescriptionLabel withBoldText:@"Product:" andRegularText:recentSearchedOrderData.orderDescription];
    NSString *orderStatus = [recentSearchedOrderData.orderStatus lowercaseString];
    
    if(orderStatus.length>0)
        self.orderStatusLabel.text = [orderStatus stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[orderStatus substringToIndex:1] uppercaseString]];
    else
        self.orderStatusLabel.text = @"";
    ;
    
    [self.orderStatusLabel setOrderStatus:orderStatus];
    
    if ([[orderStatus lowercaseString] isEqualToString:@"completed"]) {
        
//        [self.orderStatusLabel setTextColor:[UIColor whiteColor]];
//        self.orderStatusLabel.layer.borderColor = [BrandColours colourMyBtGreen].CGColor;
//        self.orderStatusLabel.backgroundColor = [BrandColours colourMyBtGreen];
//        self.orderStatusLabel.text = orderStatus;
        
        
        
        if ([AppManager isValidDate:recentSearchedOrderData.completionDate])
        {
            [self updateLabel:self.placedOrCompletedLabel withBoldText:@"Completed:" andRegularText:[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:recentSearchedOrderData.completionDate]];
        }
        else
        {
            [self updateLabel:self.placedOrCompletedLabel withBoldText:@"Completed:" andRegularText:@"-"];
        }
        
    } else if ([[orderStatus lowercaseString] isEqualToString:@"delayed"]) {
//        self.orderStatusLabel.text = orderStatus;
//        [self.orderStatusLabel setTextColor:[BrandColours colourMyBtRed]];
//        self.orderStatusLabel.layer.borderColor = [BrandColours colourMyBtRed].CGColor;
//        self.orderStatusLabel.backgroundColor = [UIColor clearColor];
        [self updateLabel:self.placedOrCompletedLabel withBoldText:@"Ordered:" andRegularText:[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:recentSearchedOrderData.placedOnDate]];
        
    }
    else if ([[orderStatus lowercaseString] isEqualToString:@"in progress"]) {
        orderStatus = @"In progress";
//        self.orderStatusLabel.text = orderStatus;
//        [self.orderStatusLabel setTextColor:[BrandColours colourMyBtGreen]];
//        self.orderStatusLabel.layer.borderColor = [BrandColours colourMyBtGreen].CGColor;
//        self.orderStatusLabel.backgroundColor = [UIColor clearColor];
        [self updateLabel:self.placedOrCompletedLabel withBoldText:@"Ordered:" andRegularText:[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:recentSearchedOrderData.placedOnDate]];
    }
    else {
//        self.orderStatusLabel.text = orderStatus;
//        [self.orderStatusLabel setTextColor:[BrandColours colourMyBtOrange]];
//        self.orderStatusLabel.layer.borderColor = [BrandColours colourMyBtOrange].CGColor;
//        self.orderStatusLabel.backgroundColor = [UIColor clearColor];
        [self updateLabel:self.placedOrCompletedLabel withBoldText:@"Ordered:" andRegularText:[AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:recentSearchedOrderData.placedOnDate]];
    }
}


- (void)updateLabel:(UILabel*)label withBoldText:(NSString*)boldPart andRegularText:(NSString*)regularPart
{
    NSString *baseString = [NSString stringWithFormat:@"%@ %@",boldPart,regularPart];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:baseString attributes:@{
                                                                                                                                  NSFontAttributeName: [UIFont fontWithName:kBtFontRegular size: 14.0f],
                                                                                                                                  NSForegroundColorAttributeName: [BrandColours colourBtLightBlack]
                                                                                                                                  }];
    [attributedString addAttributes:@{
                                      NSFontAttributeName: [UIFont fontWithName:kBtFontBold size: 14.0f],
                                      NSForegroundColorAttributeName: [BrandColours colourBtLightBlack]
                                      } range:NSMakeRange(0, boldPart.length)];
    
    [label setAttributedText:attributedString];
}

//- (void)updateUIForNoOrderDetailsOrder {
//    [self.noOrderLabel setHidden:NO];
//    self.noOrderLabelHeightConstraint.constant = 18;
//    self.orderDescriptionLabelHeightConstraint.constant = 0;    
//}

@end
