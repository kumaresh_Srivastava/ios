//
//  SiteContactTableViewCell.m
//  SampleActionViewProject
//
//  Created by VectoScalar on 10/26/16.
//  Copyright © 2016 VectoScalar. All rights reserved.
//

#import "SiteContactTableViewCell.h"
#import "NSObject+APIResponseCheck.h"
#import "AppConstants.h"
#import "AppManager.h"

@interface SiteContactTableViewCell()

@property (weak, nonatomic) IBOutlet UIButton *radioButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneAndEmailLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactNumberLabel;

@end

@implementation SiteContactTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIView *cellBg = [[UIView alloc] init];
    cellBg.backgroundColor = [BrandColours colourBtBackgroundColor];
    cellBg.layer.masksToBounds = YES;
    self.selectedBackgroundView = cellBg;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    self.radioButton.selected = selected;

    // Configure the view for the selected state
}




- (NSMutableAttributedString *)getAttributtedStringForText1:(NSString *)text1 andText2:(NSString *)text2{
    
    
    UIFont *textFont = [UIFont fontWithName:kBtFontBold size:17.0];
    NSDictionary *textDict = [NSDictionary dictionaryWithObject:textFont forKey:NSFontAttributeName];
    
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:text1 attributes: textDict];
    
    textFont = [UIFont fontWithName:kBtFontRegular size:17.0];
    NSDictionary *valueDict = [NSDictionary dictionaryWithObject:textFont forKey:NSFontAttributeName];
    NSMutableAttributedString *vAttrString = [[NSMutableAttributedString alloc]initWithString:text2 attributes:valueDict];
    
    [aAttrString appendAttributedString:vAttrString];
    
    
    return aAttrString;
    
}




#pragma mark- Public Method

- (void)updateWithBTContact:(BTOrderSiteContactModel *)contact{
    
    if(contact){
        
        NSString *complateName = @"";
        
        if([contact.firstName validAndNotEmptyStringObject]){
            
            complateName = contact.firstName;
        }
        
        if([contact.lastName validAndNotEmptyStringObject]){
            
            complateName = [complateName stringByAppendingString:[NSString stringWithFormat:@" %@", contact.lastName]];
        }
        self.nameLabel.text = complateName;
        
        NSString *phoneString = [AppManager getContactNumberFromSiteContact:contact];
        if([contact.phone validAndNotEmptyStringObject]){
            
            phoneString = contact.phone;
        }
        self.contactNumberLabel.text = phoneString;
        
        NSString *emailString = @"";
        
        if([contact.email validAndNotEmptyStringObject]){
            
            emailString = contact.email;
            
            if([phoneString validAndNotEmptyStringObject]){
                
                phoneString = [phoneString stringByAppendingString:@" | "];
                
            }
        }
        self.emailAddressLabel.text = contact.email;
        
       
        /*
        //Name
        NSString *complateName = @"";
        
        if([contact.firstName validAndNotEmptyStringObject]){
            
            complateName = contact.firstName;
        }
        
        if([contact.lastName validAndNotEmptyStringObject]){
            
            complateName = [complateName stringByAppendingString:[NSString stringWithFormat:@" %@", contact.lastName]];
        }
        self.nameLabel.text = complateName;
        
        
        NSString *phoneString = [AppManager getContactNumberFromSiteContact:contact];
        if([contact.phone validAndNotEmptyStringObject]){
            
            phoneString = contact.phone;
        }
        
        NSString *emailString = @"";
        
        if([contact.email validAndNotEmptyStringObject]){
            
            emailString = contact.email;
            
            if([phoneString validAndNotEmptyStringObject]){
                
                phoneString = [phoneString stringByAppendingString:@" | "];
            
            }
        }
        
        NSAttributedString *completeContactString = [self getAttributtedStringForText1:phoneString andText2:emailString];
        
        self.phoneAndEmailLabel.attributedText = completeContactString;
        */
        
    }
    
    
}



#pragma mark - action method

- (IBAction)radioButtonPressed:(id)sender {
    
    [self setSelected:[(UIButton *)sender isSelected] animated:YES];
    
    [self.delegate userPressedRatioButtonOnSiteContactTableViewCell:self];
}





@end
