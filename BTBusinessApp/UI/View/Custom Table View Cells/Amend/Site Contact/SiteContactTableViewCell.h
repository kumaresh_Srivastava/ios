//
//  SiteContactTableViewCell.h
//  SampleActionViewProject
//
//  Created by VectoScalar on 10/26/16.
//  Copyright © 2016 VectoScalar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTOrderSiteContactModel.h"

@class SiteContactTableViewCell;

@protocol SiteContactTableViewCellDelegate <NSObject>

- (void)userPressedRatioButtonOnSiteContactTableViewCell:(nonnull SiteContactTableViewCell *)cell;

@end

@interface SiteContactTableViewCell : UITableViewCell
@property(nonnull, assign) id<SiteContactTableViewCellDelegate>delegate;

- (void)updateWithBTContact:(nonnull BTOrderSiteContactModel *)contact;

@end
