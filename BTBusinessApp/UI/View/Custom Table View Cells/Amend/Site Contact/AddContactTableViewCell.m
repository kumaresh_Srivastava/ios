//
//  AddContactTableViewCell.m
//  SampleActionViewProject
//
//  Created by VectoScalar on 10/27/16.
//  Copyright © 2016 VectoScalar. All rights reserved.
//

#import "AddContactTableViewCell.h"
#import "NSObject+APIResponseCheck.h"
#import "AppConstants.h"
#import "UITextField+BTTextFieldProperties.h"

#define kScreenWidth [[UIScreen mainScreen] bounds].size.width

@interface AddContactTableViewCell()<UITextFieldDelegate>{
    
    BTTextFiledModel *_textFiledModel;
    CGFloat originalTraillingConstaintValue;
}
@property (weak, nonatomic) IBOutlet UILabel *fieldNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *errorMessageLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textFeildTraillingContraint;

@end

@implementation AddContactTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.errorMessageLabel.text = @"";
    self.textField.delegate = self;
    [self.textField getBTTextFieldStyle];
    
    originalTraillingConstaintValue = self.textFeildTraillingContraint.constant;
    
    //Adding Event on text chenge
    [self.textField addTarget:self
                  action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
    
    self.textField.layer.borderColor = [BrandColours colourBtNeutral80].CGColor;
    
    self.errorMessageLabel.textColor = [UIColor redColor];
    
    self.errorMessageLabel.adjustsFontSizeToFitWidth = YES;
    self.errorMessageLabel.minimumScaleFactor = 10.0/13.0;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)updateWithTextFieldModel:(BTTextFiledModel *)textFieldModel{
    
    _textFiledModel = textFieldModel;
    
    
    self.fieldNameLabel.text = textFieldModel.textFieldName;
    self.textField.text = textFieldModel.inputText;
    
    if(textFieldModel.textFieldType == TextFiledTypeDropDown && [textFieldModel.inputText isEqualToString:kDropDownDefaultText]){
        self.textField.text = kDropDownDefaultText;
        self.textField.textColor = [BrandColours colourTextBTPurplePrimaryColor];

    }
    else if(_textFiledModel.isDisabled){
        
        self.textField.textColor = [BrandColours colourBtNeutral80];
    }

    else{
        
        self.textField.textColor = [BrandColours colourBtNeutral90];
    }
    
    
    textFieldModel.correspondingTextField = self.textField;
    self.textField.keyboardType = textFieldModel.keyBoardType;
    self.textField.returnKeyType = textFieldModel.returnKeyType;
    
    
    if(textFieldModel.textFieldType == TextFiledTypeDropDown){
        //Adding right view for drop-down
        self.textFeildTraillingContraint.constant = originalTraillingConstaintValue + (kScreenSize.width - 2*originalTraillingConstaintValue)/2;
        
        /*UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"downArrow_Gray"]];
        arrow.frame = CGRectMake(0.0,
                                 0.0,
                                 arrow.image.size.width+20.0,
                                 arrow.image.size.height);
        arrow.contentMode = UIViewContentModeCenter;*/
        
        NSArray *dividingArrowArray = [[NSBundle mainBundle] loadNibNamed:@"DownArrowView" owner:self options:nil];
        UIView *dividingArrow = [dividingArrowArray objectAtIndex:0];
        dividingArrow.frame = CGRectMake(0.0,
                                                  0.0,
                                                  55.0,
                                                  dividingArrow.frame.size.height);
        dividingArrow.contentMode = UIViewContentModeCenter;
       
        UIButton *downArrowButton=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        downArrowButton.frame= dividingArrow.frame;
        downArrowButton.backgroundColor = [UIColor clearColor];
      
        [downArrowButton addTarget:self action:@selector(arrowButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [dividingArrow addSubview:downArrowButton];
        
        //self.textField.rightView = arrow;
        self.textField.rightView = dividingArrow;
        self.textField.rightViewMode = UITextFieldViewModeAlways;
    }
    else{
        self.textField.rightViewMode = UITextFieldViewModeNever;
        self.textField.rightView = nil;
        self.textFeildTraillingContraint.constant = originalTraillingConstaintValue;
    }
    
    
    //On Phone Keybord return button cant be canged so adding toolbar for next fuctionality
    if(textFieldModel.returnKeyType == UIReturnKeyNext && textFieldModel.keyBoardType == UIKeyboardTypePhonePad){
        
        UIToolbar *numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0,
                                                                              0,
                                                                              kScreenWidth,
                                                                              50)];
        
        numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil],
                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextButtonPressed)]];
        [numberToolbar sizeToFit];
        numberToolbar.backgroundColor = [BrandColours colourBtBackgroundColor];
        self.textField.inputAccessoryView = numberToolbar;
    }
    
    
    //[self updateUIAsPerValidity];
    
    [self layoutIfNeeded];
}

- (void)arrowButtonAction{
     [self.delegate addContactTableViewCell:self didSelectFiledForBTTextFiledModel:_textFiledModel andTextField:self.textField];
    if(_textFiledModel.textFieldType == TextFiledTypeDropDown || _textFiledModel.isDisabled)
    {
        [self updateUIWithValidState];
    }
}

- (void)updateUIAsPerValidity{
    
    if([_textFiledModel isValid]){
        
        [self updateUIWithValidState];
    }
    else{
        
        [self updateUIWithInvalidState];
        
    }
}

- (void)updateUIWithValidState
{
    self.errorMessageLabel.text = @"";
    self.textField.layer.borderColor = [BrandColours colourBtNeutral60].CGColor;
    self.fieldNameLabel.textColor = [BrandColours colourBtNeutral90];
}

- (void)updateUIWithInvalidState
{
    self.errorMessageLabel.text = [_textFiledModel errorMessage];
    DDLogError(@"Error Text = %@", [_textFiledModel errorMessage]);
    self.textField.layer.borderColor = [UIColor redColor].CGColor;
    self.fieldNameLabel.textColor = [UIColor redColor];
}

#pragma mark- UITextField Delegate Method


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    //Updating delegate about begin editing
    [self.delegate addContactTableViewCell:self didSelectFiledForBTTextFiledModel:_textFiledModel andTextField:self.textField];
    
    if(_textFiledModel.textFieldType == TextFiledTypeDropDown || _textFiledModel.isDisabled)
    {
        [self updateUIWithValidState];
    
        return NO;
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [textField updateWithEnabledState];
    [self updateUIWithValidState];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {

    [textField updateWithDisabledState];
    [self updateUIAsPerValidity];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self nextButtonPressed];
    
    return YES;
}


#pragma mark - Notification

- (void)textFieldDidChange:(UITextField *)textField{
    
    _textFiledModel.inputText = self.textField.text;
    
    if([self.delegate respondsToSelector:@selector(addContactTableViewCell:didChangeTextForMoldel:)]){
        
        [self.delegate addContactTableViewCell:self didChangeTextForMoldel:_textFiledModel];
    }
}


#pragma mark helper method

- (void)nextButtonPressed{
    
    [self.delegate retunKeyPressedForAddContactTableViewCell:self];
    
}


@end
