//
//  AddContactTableViewCell.h
//  SampleActionViewProject
//
//  Created by VectoScalar on 10/27/16.
//  Copyright © 2016 VectoScalar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTTextFiledModel.h"

@class AddContactTableViewCell;

@protocol AddContactTableViewCellDelegate <NSObject>

- (void)retunKeyPressedForAddContactTableViewCell:(AddContactTableViewCell *)cell;
- (void)addContactTableViewCell:(AddContactTableViewCell *)cell
didSelectFiledForBTTextFiledModel:(BTTextFiledModel *)textFieldModel andTextField:(UITextField*)textField;


@optional
- (void)addContactTableViewCell:(AddContactTableViewCell *)cell
didChangeTextForMoldel:(BTTextFiledModel *)textFieldModel;
- (void)addContactTableViewCell:(AddContactTableViewCell *)cell
         didEndEditingForMoldel:(BTTextFiledModel *)textFieldModel;


@end

@interface AddContactTableViewCell : UITableViewCell
@property(nonatomic, assign) id<AddContactTableViewCellDelegate>delegate;

- (void)updateWithTextFieldModel:(BTTextFiledModel *)textFieldModel;
- (void)updateUIAsPerValidity;

@end
