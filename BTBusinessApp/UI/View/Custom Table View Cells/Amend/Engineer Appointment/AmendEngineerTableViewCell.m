//
//  EngineerAppointmentTableViewCell.m
//  SampleActionViewProject
//
//  Created by VectoScalar on 10/26/16.
//  Copyright © 2016 VectoScalar. All rights reserved.
//

#import "AmendEngineerTableViewCell.h"

@interface AmendEngineerTableViewCell() {
    
    BTAppointment *_amendEngineedAppointmentModel;
    BTFaultAppointment *_faultAmendEngineedAppointmentModel;
}
@property (weak, nonatomic) IBOutlet UILabel *dateTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *firstShiftButton;
@property (weak, nonatomic) IBOutlet UIButton *secondShiftButton;

@end

@implementation AmendEngineerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)fisrtShiftButtonPressed:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    [self buttonPressed:button];
    
}

- (IBAction)secondShiftButtonPressed:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    [self buttonPressed:button];
    
}



- (void)buttonPressed:(UIButton *)button{
    
    
    if(button.selected)
        return;
    
    button.selected = !button.selected;
    
    
    if(button == self.firstShiftButton){
        
        _amendEngineedAppointmentModel.isAMSelected = YES;
        _amendEngineedAppointmentModel.isPMSelected = NO;
         self.secondShiftButton.selected = NO;
        
        _faultAmendEngineedAppointmentModel.isAmSelected = YES;
        _faultAmendEngineedAppointmentModel.isPmSelected = NO;
    }
    else{
        
        _amendEngineedAppointmentModel.isAMSelected = NO;
        _amendEngineedAppointmentModel.isPMSelected = YES;
        
        _faultAmendEngineedAppointmentModel.isAmSelected = NO;
        _faultAmendEngineedAppointmentModel.isPmSelected = YES;
        
        self.firstShiftButton.selected = NO;
    }

    
    if(!isFault)
    [self.delegate amendEngineerTableViewCell:self didToggleShiftForModel:_amendEngineedAppointmentModel];
    
    else
        [self.delegate amendFaultEngineerTableViewCell:self didToggleShiftForModel:_faultAmendEngineedAppointmentModel];
        
}


- (void)updateCellSelection{
    
    if(self.firstShiftButton.selected || self.secondShiftButton.selected){
        
        self.backgroundColor = [BrandColours colourBtBackgroundColor];
    }
    else{
        
        self.backgroundColor = [UIColor clearColor];
    }
}



#pragma mark- Public Method

- (void)updateWithBTEngineerAppointmentSlotModel:(BTAppointment *)model{
    
    if(model){
        
        _amendEngineedAppointmentModel = model;
        
        self.firstShiftButton.selected = model.isAMSelected;
        self.secondShiftButton.selected = model.isPMSelected;
        self.dateTextLabel.text = [self getFormattedDateFromDate:model.dateInfo];
        self.firstShiftButton.hidden = !model.hasAM;
        self.secondShiftButton.hidden = !model.hasPM;

        [self updateCellSelection];
    }
    
}



- (void)updateWithBTFaultEngineerAppointmentSlotModel:(BTFaultAppointment *)model{
    isFault = YES;
    if(model){
        
        _faultAmendEngineedAppointmentModel = model;
        
        self.firstShiftButton.selected = model.isAmSelected;
        self.secondShiftButton.selected = model.isPmSelected;
        self.dateTextLabel.text = [self getFormattedDateFromDate:model.dateInfo];
        
        self.firstShiftButton.hidden = !model.AMSlot;
        self.secondShiftButton.hidden = !model.PMSlot;
        
        [self updateCellSelection];
    }
    
}



- (NSString *)getFormattedDateFromDate:(NSDate *)date{
    
    NSString *outputFormate = @"EEE dd MMM";
    NSString *outputDateString = @"";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if(date){
        
        [dateFormatter setDateFormat:outputFormate];
        outputDateString= [dateFormatter stringFromDate:date];
    }
    return outputDateString;
}

@end
