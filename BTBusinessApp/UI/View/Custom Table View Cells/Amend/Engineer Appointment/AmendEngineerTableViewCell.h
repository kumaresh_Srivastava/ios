//
//  EngineerAppointmentTableViewCell.h
//  SampleActionViewProject
//
//  Created by VectoScalar on 10/26/16.
//  Copyright © 2016 VectoScalar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTEngineerAppointmentSlotModel.h"
#import "BTAppointment.h"
#import "BTFaultAppointment.h"

@class AmendEngineerTableViewCell;

@protocol AmendEngineerTableViewCellDelegate <NSObject>

- (void)amendEngineerTableViewCell:(AmendEngineerTableViewCell *)cell didToggleShiftForModel:(BTAppointment *)model;

- (void)amendFaultEngineerTableViewCell:(AmendEngineerTableViewCell *)cell didToggleShiftForModel:(BTFaultAppointment *)model;

@end

@interface AmendEngineerTableViewCell : UITableViewCell
{
    BOOL isFault;
}
@property(nonatomic, assign) id<AmendEngineerTableViewCellDelegate>delegate;


- (void)updateWithBTFaultEngineerAppointmentSlotModel:(BTFaultAppointment *)model;
- (void)updateWithBTEngineerAppointmentSlotModel:(BTAppointment *)model;

@end
