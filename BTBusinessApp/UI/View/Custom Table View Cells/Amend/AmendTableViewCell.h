//
//  AmendTableViewCell.h
//  SampleActionViewProject
//
//  Created by VectoScalar on 10/26/16.
//  Copyright © 2016 VectoScalar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AmendViewModel.h"


@interface AmendTableViewCell : UITableViewCell

- (void)updateWithAmend:(AmendViewModel *)amendModel;

@end
