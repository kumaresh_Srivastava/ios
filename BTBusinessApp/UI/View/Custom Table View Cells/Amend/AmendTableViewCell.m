//
//  AmendTableViewCell.m
//  SampleActionViewProject
//
//  Created by VectoScalar on 10/26/16.
//  Copyright © 2016 VectoScalar. All rights reserved.
//

#import "AmendTableViewCell.h"

@interface AmendTableViewCell(){
    
    __weak IBOutlet UILabel *headingLabel;
    
    __weak IBOutlet UIImageView *editIconImageView;
    __weak IBOutlet UILabel *subheading1;
    __weak IBOutlet UILabel *subHeading2;
}
@end


@implementation AmendTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    //self.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    UIView *cellBg = [[UIView alloc] init];
    cellBg.backgroundColor = [BrandColours colourBtBackgroundColor];
    cellBg.layer.masksToBounds = YES;
    self.selectedBackgroundView = cellBg;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)updateWithAmend:(AmendViewModel *)amendModel{
    
    headingLabel.text = amendModel.amendContext;
    subheading1.attributedText = amendModel.subHeading1;
    subHeading2.attributedText = amendModel.subHeading2;
    
    if([amendModel.amendContext isEqualToString:@""]){
        
        headingLabel.text = nil;
    }
    
    if([[amendModel.subHeading1 string] isEqualToString:@""]){
        
        subheading1.text = nil;
    }
    
    if([[amendModel.subHeading2 string] isEqualToString:@""]){
        
        subHeading2.text = nil;
    }
    NSAttributedString* updateText = [[NSAttributedString alloc] initWithString:@"Will be updated shortly"];
    if(amendModel.subHeading1 && [amendModel.subHeading1 isEqualToAttributedString:updateText]){
        editIconImageView.hidden = YES;
    } else {
        editIconImageView.hidden = !amendModel.isEditable;
    }
}

@end
