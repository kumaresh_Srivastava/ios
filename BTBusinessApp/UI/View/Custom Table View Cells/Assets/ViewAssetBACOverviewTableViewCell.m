//
//  ViewAssetBACOverviewTableViewCell.m
//  BTBusinessApp
//
//  Created by vectoscalar on 08/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "ViewAssetBACOverviewTableViewCell.h"

#define kFontSize 14
#define kFontBold @"BTFont-Bold"
#define kFontRegular @"BTFont-Regular"

@implementation ViewAssetBACOverviewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Configure the view for the selected state

}


- (void)updateDataWithAssetModel:(BTAssetModel *)assetModel
{
    self.servicesLabel.text = @"";
    NSString *serviceText;
    
    if(assetModel.noOfAssets >0)
    {
        if(assetModel.noOfAssets == 1)
        {
        serviceText = [NSString stringWithFormat:@"%ld service",(long)assetModel.noOfAssets];
        }
        else
        {
          serviceText = [NSString stringWithFormat:@"%ld services",(long)assetModel.noOfAssets];
        }
    }
    else
    {
        serviceText = @"-";
    }
    
    self.titleLabel.text = assetModel.name;
    
    self.servicesLabel.attributedText = [self boldFirstLetterOfString:serviceText withAssetCount:(int)assetModel.noOfAssets];
}


- (NSMutableAttributedString*)boldFirstLetterOfString:(NSString *)string withAssetCount:(int)assetCount
{
    
    int numOfChars = 0;//indicate how many numbers of characters to be bold
    
    while(assetCount) {
        numOfChars++;
        assetCount = assetCount/10;
    }
    
    NSDictionary *attrs = @{
                            NSFontAttributeName:[UIFont fontWithName:kFontBold size:kFontSize],
                            NSForegroundColorAttributeName:[UIColor colorForHexString:@"333333"]
                            };
    NSDictionary *subAttrs = @{
                               NSFontAttributeName:[UIFont fontWithName:kFontRegular size:kFontSize],
                               NSForegroundColorAttributeName:[UIColor colorForHexString:@"666666"]
                               };
    
    // Range of " 2012/10/14 " is (8,12). Ideally it shouldn't be hardcoded
    // This example is about attributed strings in one label
    // not about internationalisation, so we keep it simple :)
    // For internationalisation example see above code in swift
    const NSRange range = NSMakeRange(0,numOfChars);
    
    // Create the attributed string (text + attributes)
    NSMutableAttributedString *attributedText =
    [[NSMutableAttributedString alloc] initWithString:string
                                           attributes:subAttrs];
    [attributedText setAttributes:attrs range:range];
    
    // Set it in our UILabel and we are done!
    return attributedText;
}

@end
