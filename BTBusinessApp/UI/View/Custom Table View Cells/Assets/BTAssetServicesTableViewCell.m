//
//  BTAssetServicesTableViewCell.m
//  BTBusinessApp
//
//  Created by Accolite on 09/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTAssetServicesTableViewCell.h"
#import "BTAssetDetailCollection.h"

@implementation BTAssetServicesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateAssetServicesCell:(BTAssetDetailCollection *)assetDetailModel {
   
    if ([assetDetailModel.serviceNumber rangeOfString:@"cv" options:NSCaseInsensitiveSearch].location != NSNotFound) { // Cloud Voice details
        [self.titleLabel setText:@"Cloud Voice Express"];
    }
    else
    {
    [self.titleLabel setText:assetDetailModel.serviceText];
    }

    self.serviceNumberTopConstraint.constant = 8; //Set service number label top constraint to Original postion
    
    NSString *serviceNumber = assetDetailModel.serviceNumber;//[serviceDict objectForKey:@"ServiceNumber"];
    
    if ((serviceNumber == nil) || ([serviceNumber isEqualToString:@""])) {
        
        if(assetDetailModel.assetNumber)
        {
            self.serviceNumberLabel.text = assetDetailModel.assetNumber;
        }
        else
        {
            [self removeServiceNumberLabel];
        }
    }
    else
    {
         if(assetDetailModel.serviceText && assetDetailModel.serviceText.length > 0 )
         {
            [self.serviceNumberLabel setText:serviceNumber];
         }
         else
         {
             self.serviceNumberTopConstraint.constant = 0;
             [self.serviceNumberLabel setText:serviceNumber];
         }
    }
    
    
    NSString *locationString = assetDetailModel.installedAt;
    if ((locationString == nil) || ([locationString isEqualToString:@""])) {
        
        [self.locationLabel setText:@"---"];//[serviceDict objectForKey:@"installedAt"]];
    } else {
        [self.locationLabel setText:locationString];//[serviceDict objectForKey:@"installedAt"]];
    }
}

- (void)removeServiceNumberLabel {

    [self.serviceNumberLabel removeFromSuperview];
    NSDictionary *viewDict = @{@"titleLabel":self.titleLabel, @"locatedAtLabel":self.locatedAtLabel};
    [self.cellView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[titleLabel]-5-[locatedAtLabel]" options:0 metrics:nil views:viewDict]];
}

- (void)removeServiceTitleLabel
{
    
    
}

@end
