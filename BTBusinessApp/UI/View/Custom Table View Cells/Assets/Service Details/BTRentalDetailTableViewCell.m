//
//  RentalDetailTableViewCell.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/9/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTRentalDetailTableViewCell.h"

@interface BTRentalDetailTableViewCell() 
@property (weak, nonatomic) IBOutlet UILabel *itemDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemDescriptionLabel2;

@property (weak, nonatomic) IBOutlet UILabel *itemChargeLable;
@property (weak, nonatomic) IBOutlet UILabel *itemPeriodLabel;

@end

@implementation BTRentalDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)upadateWithRowWrapper:(AssetsDetailRowWrapper *)rowWrapper{
    
    self.itemDescriptionLabel.text = rowWrapper.heading;
    
    self.itemDescriptionLabel2.text = [rowWrapper.subHeading1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if(rowWrapper.subHeading2){
        
        self.itemChargeLable.attributedText = [rowWrapper attributedStringFotBoldString:rowWrapper.subHeading2 andBoldString:@" monthly"];
    }
    else{
        
        self.itemChargeLable.text = rowWrapper.subHeading2;
    }
    
    
    if(rowWrapper.subHeading3){
        
        self.itemPeriodLabel.attributedText = [rowWrapper getRentalAttributtedStringForSrting:rowWrapper.subHeading3];
    }
    else{
        
        self.itemPeriodLabel.text = rowWrapper.subHeading3;
    }
    
    
}

@end
