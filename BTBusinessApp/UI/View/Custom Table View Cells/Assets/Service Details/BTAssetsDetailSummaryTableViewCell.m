//
//  AssetsDetailSummaryTableViewCell.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/9/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTAssetsDetailSummaryTableViewCell.h"

@interface BTAssetsDetailSummaryTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *locationTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *contractTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *contractValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *rentalMessageLabel;
@property (weak, nonatomic) IBOutlet UILabel *rentalValue;
@property (weak, nonatomic) IBOutlet UILabel *contractEndDate;

@end

@implementation BTAssetsDetailSummaryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)upadateWithRowWrapper:(AssetsDetailRowWrapper *)rowWrapper{
    
    self.locationValueLabel.text = rowWrapper.heading;
    self.contractValueLabel.text = rowWrapper.subHeading1;
    
    NSString* monthText = @"/month";
    NSString *baseString = [NSString stringWithFormat:@"%@/month",rowWrapper.subHeading2];
    //BUG 14291
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:baseString attributes:@{
                                                                                                                            NSFontAttributeName: [UIFont fontWithName:@"BTFont-Regular" size: 18.0f],
                                                                                                                            NSForegroundColorAttributeName: [UIColor colorForHexString:@"333333"]
                                                                                                                            }];
    
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"BTFont-Regular" size: 14.0f] range:NSMakeRange(rowWrapper.subHeading2.length, monthText.length)];
    
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:102.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1.0] range:NSMakeRange(rowWrapper.subHeading2.length, monthText.length)];
    
    self.rentalValue.attributedText = attributedString;//rowWrapper.subHeading2;
    self.contractEndDate.text = rowWrapper.subHeading3;
    
}

@end
