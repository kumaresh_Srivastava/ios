//
//  BTTakeActionTableViewCell.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/9/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMAssetServiceDetailScreen.h"
#import "DLMMobileServiceAssetDetailScreen.h"

@interface BTTakeActionTableViewCell : UITableViewCell

- (void)upadateWithRowWrapper:(AssetsDetailRowWrapper *)rowWrapper;

- (void)updateWithTitle:(NSString *)title;

- (void)upadateWithMobileAssetsRowWrapper:(MobileAssetsDetailRowWrapper *)rowWrapper;

@end
