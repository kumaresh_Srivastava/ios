//
//  AssetsDetailSummaryTableViewCell.h
//  BTBusinessApp
//
//  Created by VectoScalar on 11/9/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMAssetServiceDetailScreen.h"

@interface BTAssetsDetailSummaryTableViewCell : UITableViewCell

- (void)upadateWithRowWrapper:(AssetsDetailRowWrapper *)rowWrapper;

@end
