//
//  BTTakeActionTableViewCell.m
//  BTBusinessApp
//
//  Created by VectoScalar on 11/9/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTTakeActionTableViewCell.h"

@interface BTTakeActionTableViewCell(){
    
}
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation BTTakeActionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)upadateWithRowWrapper:(AssetsDetailRowWrapper *)rowWrapper{
    
    self.titleLabel.text = rowWrapper.heading;
}

- (void)upadateWithMobileAssetsRowWrapper:(MobileAssetsDetailRowWrapper *)rowWrapper{
    
    self.titleLabel.text = rowWrapper.actionTitle;
}

- (void)updateWithTitle:(NSString *)title
{
    self.titleLabel.text = title;
}

@end
