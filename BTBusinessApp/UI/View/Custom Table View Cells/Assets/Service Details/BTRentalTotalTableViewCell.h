//
//  BTRentalTotalTableViewCell.h
//  BTBusinessApp
//
//  Created by VectoScalar on 12/1/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMAssetServiceDetailScreen.h"

@interface BTRentalTotalTableViewCell : UITableViewCell

- (void)upadateWithRowWrapper:(AssetsDetailRowWrapper *)rowWrapper;

@end
