//
//  BTRentalTotalTableViewCell.m
//  BTBusinessApp
//
//  Created by VectoScalar on 12/1/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTRentalTotalTableViewCell.h"
#import "BrandColours.h"

@interface BTRentalTotalTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *rentalValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *rentalTextLabel;

@end

@implementation BTRentalTotalTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.rentalTextLabel.textColor = [BrandColours colourTextBTPurplePrimaryColor];
    self.rentalValueLabel.textColor = [BrandColours colourTextBTPurplePrimaryColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)upadateWithRowWrapper:(AssetsDetailRowWrapper *)rowWrapper{
    
    self.rentalValueLabel.text = rowWrapper.heading;
}

@end
