//
//  BTAssetTableViewCell.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 08/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTAssetTableViewCell.h"
#import "NSObject+APIResponseCheck.h"
#import "BrandColours.h"

@interface BTAssetTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *accountName;
@property (weak, nonatomic) IBOutlet UILabel *accountNumber;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end

@implementation BTAssetTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _containerView.layer.borderColor = [BrandColours colourBtNeutral50].CGColor;
    _containerView.layer.borderWidth = 1.0;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


#pragma mark - Public Methods

- (void)updateAssetsCellWithAsset:(BTAsset *)asset
{

    //[SD] Updating assets name label
    if(asset.assetName && [asset.assetName validAndNotEmptyStringObject])
    {
        self.accountName.text = asset.assetName;
    }
    else
    {
        self.accountName.text = @"";
    }

     //[SD] Updating assets account number label
    if(asset.assetAccountNumber && [asset.assetAccountNumber validAndNotEmptyStringObject])
    {
        self.accountNumber.text = asset.assetAccountNumber;
    }
    else
    {
        self.accountNumber.text = @"";
    }




}
@end
