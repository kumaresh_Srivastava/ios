//
//  BTAssetTableViewCell.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 08/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTAsset.h"

@interface BTAssetTableViewCell : UITableViewCell


- (void)updateAssetsCellWithAsset:(BTAsset *)asset;
@end
