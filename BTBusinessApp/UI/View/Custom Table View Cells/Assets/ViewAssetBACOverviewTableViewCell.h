//
//  ViewAssetBACOverviewTableViewCell.h
//  BTBusinessApp
//
//  Created by vectoscalar on 08/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTAssetModel.h"

@interface ViewAssetBACOverviewTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *servicesLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

- (void)updateDataWithAssetModel:(BTAssetModel*)assetModel;


@end
