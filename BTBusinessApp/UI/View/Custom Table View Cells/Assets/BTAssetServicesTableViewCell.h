//
//  BTAssetServicesTableViewCell.h
//  BTBusinessApp
//
//  Created by Accolite on 09/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BTAssetDetailCollection;

@interface BTAssetServicesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *serviceNumberTopConstraint;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIView *cellView;
@property (weak, nonatomic) IBOutlet UILabel *locatedAtLabel;

- (void)updateAssetServicesCell:(BTAssetDetailCollection *)assetDetailModel;
@end
