//
//  NotificationNoInternetStripView.h
//  BTBusinessApp
//
//  Created by vectoscalar on 01/03/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NotificationNoInternetStripView;

@protocol NotificationNoInternetStripViewDelegate <NSObject>

- (void)userPressedRetryViewOnNotificationNoInternetStripView:(NotificationNoInternetStripView *)noInternetStripView;

@end


@interface NotificationNoInternetStripView : UIView

@property (weak,nonatomic) id <NotificationNoInternetStripViewDelegate> noInternetStripDelegate;

@end
