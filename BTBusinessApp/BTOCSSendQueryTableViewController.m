//
//  BTOCSSendQueryTableViewController.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 13/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSSendQueryTableViewController.h"

#import "BTOCSOrderDetailsTableViewController.h"

#import "NLSubmitProjectEnquiry.h"

#import "NSObject+APIResponseCheck.h"

#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"


@interface BTOCSSendQueryTableViewController () <BTOCSDropDownTableViewCellDelegate,BTOCSSimpleInputTableViewCellDelegate,BTOCSLargeTextEntryTableViewCellDelegate,NLSubmitProjectEnquiryDelegate>

@property (nonatomic, strong) BTOCSProjectInfoHeaderTableViewCell *headerCell;
@property (nonatomic, strong) BTOCSSimpleInputTableViewCell *nameCell;
@property (nonatomic, strong) BTOCSSimpleInputTableViewCell *emailCell;
@property (nonatomic, strong) BTOCSSimpleInputTableViewCell *phoneCell;
@property (nonatomic, strong) BTOCSSimpleInputTableViewCell *mobileCell;
@property (nonatomic, strong) BTOCSDropDownTableViewCell *subjectCell;
@property (nonatomic, strong) BTOCSLargeTextEntryTableViewCell *queryCell;
@property (nonatomic, strong) BTOCSSimpleButtonTableViewCell *sendQueryButtonCell;

@property (nonatomic, strong) NSString *selectedSubject;

@property (nonatomic, strong) NLSubmitProjectEnquiry *submitProjectEnquiryWebService;

@end

@implementation BTOCSSendQueryTableViewController

+ (BTOCSSendQueryTableViewController *)getOCSSendQueryViewController
{
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"BTOCSOrders" bundle:nil];
    //BTOCSSendQueryTableViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTOCSSendQueryTableViewController"];
    BTOCSSendQueryTableViewController *vc = [[BTOCSSendQueryTableViewController alloc] initWithStyle:UITableViewStylePlain];
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.title = @"Send query";
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (!self.existingEnquiry) {
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(dismissButtonAction)];
        self.navigationItem.leftBarButtonItem = cancelButton;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupTableCells
{
    [super setupTableCells];
    
    CDUser *user = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    
    if (!self.headerCell) {
        self.headerCell = (BTOCSProjectInfoHeaderTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSProjectInfoHeaderTableViewCell reuseId]];
        [self.headerCell updateWithPorjectRef:self.project.projectRef andProduct:self.project.productName andSite:self.selectedSite.name];
        [self.tableCells addObject:self.headerCell];
    }
    
    if (!self.nameCell) {
        self.nameCell = (BTOCSSimpleInputTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSSimpleInputTableViewCell reuseId]];
        self.nameCell.titleLabel.text = @"Your name";
        if (user) {
            self.nameCell.entryTextView.text = [NSString stringWithFormat:@"%@ %@",user.firstName,user.lastName];
        } else {
            self.nameCell.entryTextView.text = @"";
        }
        self.nameCell.delegate = self;
        self.nameCell.entryTextView.userInteractionEnabled = !self.project.hideContactPanel;
        [self.tableCells addObject:self.nameCell];
    }

    if (!self.emailCell) {
        self.emailCell = (BTOCSSimpleInputTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSSimpleInputTableViewCell reuseId]];
        self.emailCell.titleLabel.text = @"Email";
        if (user) {
            self.emailCell.entryTextView.text = user.primaryEmailAddress;
        } else {
            self.emailCell.entryTextView.text = @"";
        }
        self.emailCell.entryTextView.keyboardType = UIKeyboardTypeEmailAddress;
        self.emailCell.delegate = self;
        self.emailCell.entryTextView.userInteractionEnabled = !self.project.hideContactPanel;
        [self.tableCells addObject:self.emailCell];
    }

    if (!self.phoneCell) {
        self.phoneCell = (BTOCSSimpleInputTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSSimpleInputTableViewCell reuseId]];
        self.phoneCell.titleLabel.text = @"Phone number";
        if (user) {
            self.phoneCell.entryTextView.text = user.landlineNumber;
        } else {
            self.phoneCell.entryTextView.text = @"";
        }
        self.phoneCell.entryTextView.keyboardType = UIKeyboardTypePhonePad;
        self.phoneCell.delegate = self;
        self.phoneCell.entryTextView.userInteractionEnabled = !self.project.hideContactPanel;
        [self.tableCells addObject:self.phoneCell];
    }

    if (!self.mobileCell) {
        self.mobileCell = (BTOCSSimpleInputTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSSimpleInputTableViewCell reuseId]];
        self.mobileCell.titleLabel.text = @"Mobile number";
        if (user) {
            self.mobileCell.entryTextView.text = user.mobileNumber;
        } else {
            self.mobileCell.entryTextView.text = @"";
        }
        self.mobileCell.entryTextView.keyboardType = UIKeyboardTypePhonePad;
        self.mobileCell.delegate = self;
        self.mobileCell.entryTextView.userInteractionEnabled = !self.project.hideContactPanel;
        [self.tableCells addObject:self.mobileCell];
    }
    
    if (!self.subjectCell) {
        self.subjectCell = (BTOCSDropDownTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSDropDownTableViewCell reuseId]];
        if (self.existingEnquiry) {
            [self.subjectCell updateWithTitle:@"Subject" values:self.project.enquiryCategories selectedValue:self.existingEnquiry.enquiryCategory andPlaceHolder:nil];
            self.selectedSubject = self.existingEnquiry.enquiryCategory;
        } else {
            [self.subjectCell updateWithTitle:@"Subject" values:self.project.enquiryCategories selectedValue:@"" andPlaceHolder:@"Please select"];
        }
        self.subjectCell.delegate = self;
        if (self.existingEnquiry || self.project.hideContactPanel) {
            self.subjectCell.dropdownOutlineView.userInteractionEnabled = NO;
        } else {
            self.subjectCell.dropdownOutlineView.userInteractionEnabled = YES;
        }
        
        [self.tableCells addObject:self.subjectCell];
    }
    
//    if (self.existingEnquiry) {
//        BTOCSLargeTextEntryTableViewCell *prevEnquiry = (BTOCSLargeTextEntryTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSLargeTextEntryTableViewCell reuseId]];
//        prevEnquiry.questionLabel.text = @"";
//        prevEnquiry.entryTextView.text = self.existingEnquiry.enquiryDescription;
//        prevEnquiry.entryTextView.userInteractionEnabled = NO;
//        [self.tableCells addObject:prevEnquiry];
//    }
    
    if (!self.queryCell && !self.project.hideContactPanel) {
        self.queryCell = (BTOCSLargeTextEntryTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSLargeTextEntryTableViewCell reuseId]];
        self.queryCell.questionLabel.text = @"";
        if (self.existingEnquiry) {
            self.queryCell.questionLabel.text = self.existingEnquiry.enquiryDescription;
            //self.queryCell.entryTextView.text = self.existingEnquiry.enquiryDescription;
        }
        self.queryCell.maxLength = 1000;
        self.queryCell.delegate = self;
        [self.tableCells addObject:self.queryCell];
    }
    
    if (!self.project.hideContactPanel) {
        if (!self.sendQueryButtonCell) {
            self.sendQueryButtonCell = (BTOCSSimpleButtonTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSSimpleButtonTableViewCell reuseId]];
            self.sendQueryButtonCell.button.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
            NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:@"Send query" attributes:@{NSFontAttributeName:[UIFont fontWithName:kBtFontBold size:16.0],NSForegroundColorAttributeName:[UIColor whiteColor]}];
            [self.sendQueryButtonCell.button setAttributedTitle:attributedTitle forState:UIControlStateNormal];
            [self.sendQueryButtonCell.button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.sendQueryButtonCell.button addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
            [self.tableCells addObject:self.sendQueryButtonCell];
        }
    }
    
}

- (void)updateTableCells
{
    [super updateTableCells];
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
//    return 0;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
//    return 0;
//}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - input cell delegate


- (void)previousButtonPressedOnCell:(BTOCSSimpleInputTableViewCell *)cell
{
    if ([self.tableCells containsObject:cell]) {
        NSInteger idxOfCell = [self.tableCells indexOfObject:cell];
        if (idxOfCell > 0) {
            BTBaseTableViewCell *prevCell = [self.tableCells objectAtIndex:(idxOfCell-1)];
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:(idxOfCell-1) inSection:0] atScrollPosition:UITableViewScrollPositionNone animated:YES];
            if ([prevCell isKindOfClass:[BTOCSSimpleInputTableViewCell class]]) {
                [[(BTOCSSimpleInputTableViewCell*)prevCell entryTextView] becomeFirstResponder];
            } else if ([prevCell isKindOfClass:[BTOCSDropDownTableViewCell class]]) {
                [(BTOCSDropDownTableViewCell*)prevCell showOptions];
            } else if ([prevCell isKindOfClass:[BTOCSLargeTextEntryTableViewCell class]]) {
                [[(BTOCSLargeTextEntryTableViewCell*)prevCell entryTextView] becomeFirstResponder];
            }
        }
    }
}

- (void)nextButtonPressedOnCell:(BTOCSSimpleInputTableViewCell *)cell
{
    if ([self.tableCells containsObject:cell]) {
        NSInteger idxOfCell = [self.tableCells indexOfObject:cell];
        if (idxOfCell < self.tableCells.count) {
            BTBaseTableViewCell *nextCell = [self.tableCells objectAtIndex:(idxOfCell+1)];
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:(idxOfCell+1) inSection:0] atScrollPosition:UITableViewScrollPositionNone animated:YES];
            if ([nextCell isKindOfClass:[BTOCSSimpleInputTableViewCell class]]) {
                [[(BTOCSSimpleInputTableViewCell*)nextCell entryTextView] becomeFirstResponder];
            } else if ([nextCell isKindOfClass:[BTOCSDropDownTableViewCell class]]) {
                [(BTOCSDropDownTableViewCell*)nextCell showOptions];
            } else if ([nextCell isKindOfClass:[BTOCSLargeTextEntryTableViewCell class]]) {
                [[(BTOCSLargeTextEntryTableViewCell*)nextCell entryTextView] becomeFirstResponder];
            }
        }
    }
}

- (void)cell:(BTOCSSimpleInputTableViewCell *)cell finishedEditingWithValue:(NSString *)value
{
    
}

#pragma mark - dropdown cell delegate

- (void)cell:(BTOCSDropDownTableViewCell *)cell updatedSelection:(NSString *)selected
{
    self.selectedSubject = selected;
    // auto select next cell if there is one
    if ([self.tableCells containsObject:cell]) {
        NSInteger idxOfCell = [self.tableCells indexOfObject:cell];
        if (idxOfCell < self.tableCells.count) {
            BTBaseTableViewCell *nextCell = [self.tableCells objectAtIndex:(idxOfCell+1)];
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:(idxOfCell+1) inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            if ([nextCell isKindOfClass:[BTOCSSimpleInputTableViewCell class]]) {
                [[(BTOCSSimpleInputTableViewCell*)nextCell entryTextView] becomeFirstResponder];
            } else if ([nextCell isKindOfClass:[BTOCSDropDownTableViewCell class]]) {
                [(BTOCSDropDownTableViewCell*)nextCell showOptions];
            } else if ([nextCell isKindOfClass:[BTOCSLargeTextEntryTableViewCell class]]) {
                [[(BTOCSLargeTextEntryTableViewCell*)nextCell entryTextView] becomeFirstResponder];
            }
        }
    }
}

#pragma mark - large text entry delegate

- (void)cell:(BTOCSLargeTextEntryTableViewCell *)cell updatedTextEntry:(NSString *)text
{
    if ([self.tableCells containsObject:cell]) {
        NSInteger idxOfCell = [self.tableCells indexOfObject:cell];
        [self.tableView beginUpdates];
        [self.tableView endUpdates];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:idxOfCell inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
}

#pragma mark - close button/submit successful action

- (void)dismissButtonAction {
    [self trackOmniClick:OMNICLICK_OCS_ORDER_CLOSE];
    if (!self.existingEnquiry) {
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
            //
        }];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)dismissAction {
    if (!self.existingEnquiry) {
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
            if (self.delegate && [self.delegate respondsToSelector:@selector(querySentFromForm:)]) {
                [self.delegate querySentFromForm:self];
            }
        }];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
        if (self.delegate && [self.delegate respondsToSelector:@selector(querySentFromForm:)]) {
            [self.delegate querySentFromForm:self];
        }
    }
}

#pragma mark - button actions

- (void)submitAction {
    [self submitProjectEnquiry];
    [self trackOmniClick:self.sendQueryButtonCell.button.titleLabel.text];
}

#pragma mark - api methods

- (void)submitProjectEnquiry
{
    if (self.submitProjectEnquiryWebService) {
        [self.submitProjectEnquiryWebService cancel];
        self.submitProjectEnquiryWebService.submitProjectEnquiryDelegate = nil;
        self.submitProjectEnquiryWebService = nil;
    }
    
    // check if any input cells are empty/invalid
    NSMutableArray *invalidCells = [NSMutableArray new];
    
    if (!(self.nameCell.entryTextView.text.length>0)) {
        [invalidCells addObject:self.nameCell];
    }
    if (!(self.emailCell.entryTextView.text.length>0)) {
        [invalidCells addObject:self.emailCell];
    }
    if (!(self.phoneCell.entryTextView.text.length>0) || !(self.mobileCell.entryTextView.text.length>0)) {
        [invalidCells addObject:self.phoneCell];
    }
    if (!(self.selectedSubject)) {
        [invalidCells addObject:self.subjectCell];
    }
    
    if (invalidCells.count > 0) {
        NSInteger firstInvalidIdx = [self.tableCells indexOfObject:[invalidCells firstObject]];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:firstInvalidIdx inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    } else {
        NSString *nameString = self.nameCell.entryTextView.text?self.nameCell.entryTextView.text:@"";
        NSString *emailString = self.emailCell.entryTextView.text?self.emailCell.entryTextView.text:@"";
        NSString *phoneString = self.phoneCell.entryTextView.text?self.phoneCell.entryTextView.text:@"";
        NSString *mobleString = self.mobileCell.entryTextView.text?self.mobileCell.entryTextView.text:@"";
        NSString *enquiryString = self.queryCell.entryTextView.text?self.queryCell.entryTextView.text:@"";
        NSString *enquiryDescription = [NSString stringWithFormat:@"name:%@\nemail:%@\ntelephone:%@\nmobile:%@\n%@",nameString,emailString,phoneString,mobleString,enquiryString];
        if (self.project && self.project.projectRef) {
            if (self.existingEnquiry) {
                self.submitProjectEnquiryWebService = [[NLSubmitProjectEnquiry alloc] initWithProjectRef:self.project.projectRef category:self.selectedSubject enquiryDescription:enquiryDescription siteID:self.selectedSite.siteID andExistingEnquiryID:self.existingEnquiry.enquiryId];
            } else {
                self.submitProjectEnquiryWebService = [[NLSubmitProjectEnquiry alloc] initWithProjectRef:self.project.projectRef category:self.selectedSubject enquiryDescription:enquiryDescription siteID:self.selectedSite.siteID andExistingEnquiryID:0];
            }
            self.submitProjectEnquiryWebService.submitProjectEnquiryDelegate = self;
            if (!self.networkRequestInProgress) {
                self.networkRequestInProgress = YES;
            }
            [self.submitProjectEnquiryWebService resume];
        }
    }
}

- (void)submitProjectEnquiryWebService:(NLSubmitProjectEnquiry *)webService finishedWithResponse:(NSObject *)response
{
    self.networkRequestInProgress = NO;
    NSString *title = @"Query sent";
    NSString *message = @"";
    NSInteger queryID = 0;
//    if (self.project.contactMessage && ![self.project.contactMessage isEqualToString:@""]) {
//        message = self.project.contactMessage;
//    } else if ([response isKindOfClass:[NSDictionary class]]) {
//        message = [[(NSDictionary*)response objectForKey:@"Message"] validAndNotEmptyStringObject];
//        queryID = [[(NSDictionary*)response objectForKey:@"ID"] integerValue];
//        if (queryID != 0) {
//            message = [message stringByAppendingString:[NSString stringWithFormat:@" Please quote %ld if talking to us about your query.",(long)queryID]];
//        }
//    }
    //message = [[(NSDictionary*)response objectForKey:@"Message"] validAndNotEmptyStringObject];
    queryID = [[(NSDictionary*)response objectForKey:@"ID"] integerValue];
    if (queryID != 0) {
        title = [NSString stringWithFormat:@"Your query reference ID is: %li",(long)queryID];
        message = [NSString stringWithFormat:@"Please quote your project reference %@ and the above query reference number in all correspondence. ",self.project.projectRef];
    } else {
        message = [NSString stringWithFormat:@"Please quote your project reference %@ in all correspondence. ",self.project.projectRef];
    }
    message = [message stringByAppendingString:[[(NSDictionary*)response objectForKey:@"Message"] validAndNotEmptyStringObject]];
    
    
    /*
     let paragraphStyle = NSMutableParagraphStyle()
     paragraphStyle.alignment = NSTextAlignment.Left
     
     let messageText = NSMutableAttributedString(
     string: "The message you want to display",
     attributes: [
     NSParagraphStyleAttributeName: paragraphStyle,
     NSFontAttributeName : UIFont.preferredFontForTextStyle(UIFontTextStyleBody),
     NSForegroundColorAttributeName : UIColor.blackColor()
     ]
     )
     
     myAlert.setValue(messageText, forKey: "attributedMessage")
     */
    NSMutableParagraphStyle * paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.alignment = NSTextAlignmentLeft;
    NSMutableDictionary* dic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [dic setValue:paraStyle forKey:NSParagraphStyleAttributeName];
    [dic setValue:[UIFont fontWithName:kBtFontRegular size:13.0f] forKey:NSFontAttributeName];
    [dic setValue:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
    NSMutableAttributedString* messageText = [[NSMutableAttributedString alloc] initWithString:message attributes:dic];
    
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    __weak typeof(self) weakSelf = self;
    
    [alert setValue:messageText forKey:@"attributedMessage"];
    UIAlertAction *done = [UIAlertAction actionWithTitle:@"OK"
                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                         [weakSelf dismissAction];
                                                     }];
    [alert addAction:done];
    
    [self presentViewController:alert animated:YES completion:^{
        //
    }];
    
}

- (void)submitProjectEnquiryWebService:(NLSubmitProjectEnquiry *)webService failedWithWebServiceError:(NLWebServiceError *)error
{
    self.networkRequestInProgress = NO;
    [AppManager trackWebServiceError:error FromAPI:webService.friendlyName OnPage:self.pageNameForOmnitureTracking];
}

#pragma mark - omniture

- (NSString *)pageNameForOmnitureTracking
{
    NSString *pageName = OMNIPAGE_OCS_ORDER_SENDQUERY;
    if (self.existingEnquiry) {
        pageName = OMNIPAGE_OCS_ORDER_RECENTQUERY;
    }
    return pageName;
}

- (void)trackPageForOmniture
{
    if (self.project.productService && self.project.subStatus) {
        [self trackPageForOmnitureWithProductService:self.project.productService andOrderStatus:self.project.subStatus];
    } else {
        [super trackPageForOmniture];
    }
}

@end
