//
//  BTOCSRadioSelectCollectionViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 16/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BTOCSRadioSelectCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIView *bgView;
@property (strong, nonatomic) IBOutlet UIView *indicatorOutlineView;
@property (strong, nonatomic) IBOutlet UIView *indicatorView;
@property (strong, nonatomic) IBOutlet UILabel *optionTextLabel;

@end

NS_ASSUME_NONNULL_END
