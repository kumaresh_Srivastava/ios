//
//  NLGetProjectSummaryAnonymous.m
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLGetProjectSummaryAnonymous.h"
#import "NLVordelAuthenticationProtectedWebService+MbaasHeader.h"
#import "NSObject+APIResponseCheck.h"
#import "NLWebServiceError.h"
#import "NLConstants.h"
#import "NLWebService+BackEndAPIErrorHandling.h"

@implementation NLGetProjectSummaryAnonymous

- (instancetype)initWithProjectDetails:(NSDictionary*)projectDetails {
    
    NSDictionary *header = [self getMbaasHeader];
    
    NSDictionary *projectQuestions = [NSDictionary dictionaryWithObject:projectDetails forKey:@"getProjectSummaryAnonymousV1"];
    
    NSDictionary *payload = [NSDictionary dictionaryWithObjectsAndKeys:header, @"MobileServiceHeader", projectQuestions, @"Body", nil];
    
    NSError *errorWhileCreatingJSONData = nil;
    NSData *mbaasHeader = [NSJSONSerialization dataWithJSONObject:payload options:0 error:&errorWhileCreatingJSONData];
    
    NSString *jsonString = [[NSString alloc] initWithData:mbaasHeader encoding:4];
    
    NSString *endPointURLString = @"/ordermanagement/getprojectsummaryanonymous";
    
    self = [super initWithMethod:@"POST" parameters:jsonString andEndpointUrlString:endPointURLString];
    if(self)
    {
        
    }
    
    return self;
}

- (NSDictionary *)getMbaasHeader
{
    NSMutableDictionary *header = [NSMutableDictionary dictionaryWithDictionary:[super getMbaasHeader]];
    [header setObject:@"OCSOnline" forKey:@"App_Name"];
    return header;
}

- (NSString *)friendlyName
{
    return @"getProjectSummaryAnonymous";
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *resultDic = [[responseObject valueForKey:@"Body"] valueForKey:@"getProjectSummaryAnonymousResponse"];
    
    if ([resultDic valueForKey:@"Project"])
    {
        [self.getProjectSummaryAnonymousDelegate getProjectSummaryAnonymousWebService:self finishedWithResponse:responseObject];
    }
    else
    {
        NLWebServiceError *webServiceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponse];
        
        DDLogError(@"Invalid response json object in webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Invalid response json object in webservice with URL: %@", [[task currentRequest] URL]]));
        
        [self.getProjectSummaryAnonymousDelegate getProjectSummaryAnonymousWebService:self failedWithWebServiceError:webServiceError];
    }
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.getProjectSummaryAnonymousDelegate getProjectSummaryAnonymousWebService:self failedWithWebServiceError:webServiceError];
}

@end
