//
//  BTOCSProjectDetailsTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSProjectDetailsTableViewCell.h"

@interface BTOCSProjectDetailsTableViewCell ()
@property (strong, nonatomic) IBOutlet BTShadowedView *shadowedView;

@end

@implementation BTOCSProjectDetailsTableViewCell

+ (NSString *)reuseId
{
    return @"BTOCSProjectDetailsTableViewCell";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.shadowedView setElevation:2.0f];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
