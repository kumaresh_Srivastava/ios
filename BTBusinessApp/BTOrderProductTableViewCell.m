//
//  BTOrderProductTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 27/11/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOrderProductTableViewCell.h"

@implementation BTOrderProductTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateLabel:(UILabel *)label withBoldText:(NSString *)boldString andRegularText:(NSString *)regularString
{
    NSString *baseString = [NSString stringWithFormat:@"%@ %@",boldString,regularString];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:baseString attributes:@{
                                                                                                                                            NSFontAttributeName: [UIFont fontWithName:@"BTFont-Regular" size: 14.0f],
                                                                                                                                            NSForegroundColorAttributeName: [UIColor colorForHexString:@"666666"]
                                                                                                                                            }];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"BTFont-Bold" size: 14.0f] range:NSMakeRange(0, boldString.length)];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorForHexString:@"333333"] range:NSMakeRange(0, boldString.length)];
    [label setAttributedText:attributedString];
}

@end
