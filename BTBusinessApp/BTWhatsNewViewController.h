//
//  BTWhatsNewViewController.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 01/03/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTWhatsNewViewController;

@protocol BTWhatsNewViewControllerDelegate <NSObject>

-(void)userDismissedWhatsNewScreen:(BTWhatsNewViewController*)screen WithAction:(NSString*)action;

@end

@interface BTWhatsNewViewController : UIViewController
//@property (strong, nonatomic) IBOutlet UILabel *whatsNewHeader;
//@property (strong, nonatomic) IBOutlet UILabel *whatsNewTitle;
//@property (strong, nonatomic) IBOutlet UILabel *whatsNewDetail;
//@property (strong, nonatomic) IBOutlet UIImageView *whatsNewImageview;
//@property (strong, nonatomic) IBOutlet UIButton *whatsNewActionButton;
//@property (strong, nonatomic) IBOutlet UIButton *whatsNewSkipButton;

@property (weak,nonatomic)IBOutlet UIView *btnGotItSuperView;
@property (weak,nonatomic)IBOutlet UIView *btnGotItContainerView;
@property (weak,nonatomic)IBOutlet UIButton *btnGotIt;

@property (weak, nonatomic) id<BTWhatsNewViewControllerDelegate> delegate;
@property (strong, nonatomic) NSDictionary *userDetails;

- (IBAction)whatsNewButtonAction:(id)sender;

+ (BTWhatsNewViewController *)getWhatsNewViewController;

@end
