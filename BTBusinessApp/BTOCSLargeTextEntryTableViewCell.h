//
//  BTOCSLargeTextEntryTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 04/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@class BTBaseTableViewCell;
@class BTOCSLargeTextEntryTableViewCell;

@protocol BTOCSLargeTextEntryTableViewCellDelegate <NSObject>

- (void)cell:(BTOCSLargeTextEntryTableViewCell*)cell updatedTextEntry:(NSString*)text;
- (void)nextButtonPressedOnCell:(BTBaseTableViewCell*)cell;
- (void)previousButtonPressedOnCell:(BTBaseTableViewCell*)cell;

@end

@interface BTOCSLargeTextEntryTableViewCell : BTBaseTableViewCell
@property (strong, nonatomic) IBOutlet UILabel *questionLabel;

@property (strong, nonatomic) IBOutlet UITextView *entryTextView;
@property (strong, nonatomic) IBOutlet UILabel *charactersLabel;

@property (nonatomic) NSInteger maxLength;
@property (nonatomic) id<BTOCSLargeTextEntryTableViewCellDelegate> delegate;
@property (nonatomic) BOOL hidesToolbar;

@end

NS_ASSUME_NONNULL_END
