//
//  BTMobileServiceDetailHeaderViewTableViewCell.m
//  BTBusinessApp
//
//  Created by kumaresh shrivastava on 24/06/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import "BTMobileServiceDetailHeaderViewTableViewCell.h"
#import "BTSegmentedControl.h"

#import "NSObject+APIResponseCheck.h"

@implementation BTMobileServiceDetailHeaderViewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

/*
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}*/


- (void)updateWithAssetsDetailCollection:(BTAssetDetailCollection *)collectionModel withAssetType:(NSString*)assetType{
    
    if(collectionModel.serviceNumber && [collectionModel.serviceNumber validAndNotEmptyStringObject]){
        
        self.serviceIdLable.text = collectionModel.serviceNumber;
    }
    else{
        
        self.serviceIdLable.text = nil;
    }
}

-(IBAction)chooseNumberAction:(id)sender{
    [self.delegate btChooseNumberAction];
}

- (IBAction)segmentValueChanged:(id)sender {
    if([self.delegate respondsToSelector:@selector(btMobileServiceDetailHeaderView:didSelectedSegmentAtIndex:)]){
        
        [self.delegate btMobileServiceDetailHeaderView:self didSelectedSegmentAtIndex:[(BTSegmentedControl *)sender selectedSegmentIndex]];
    }
}



@end
