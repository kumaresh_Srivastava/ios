//
//  BTOCSAdditionalProjectDetailsTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 13/11/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSAdditionalProjectDetailsTableViewCell.h"

@implementation BTOCSAdditionalProjectDetailsTableViewCell

+ (NSString *)reuseId
{
    return @"BTOCSAdditionalProjectDetailsTableViewCell";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.shadowedView setElevation:2.0f];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
