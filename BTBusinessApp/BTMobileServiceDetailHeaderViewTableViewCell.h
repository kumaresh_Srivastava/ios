//
//  BTMobileServiceDetailHeaderViewTableViewCell.h
//  BTBusinessApp
//
//  Created by kumaresh shrivastava on 24/06/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTAssetDetailCollection.h"

NS_ASSUME_NONNULL_BEGIN

@class BTMobileServiceDetailHeaderViewTableViewCell;

@protocol BTMobileServiceDetailHeaderViewDelegate <NSObject>

- (void)btMobileServiceDetailHeaderView:(BTMobileServiceDetailHeaderViewTableViewCell *)view didSelectedSegmentAtIndex:(NSInteger)index;
- (void)btChooseNumberAction;

@end

@interface BTMobileServiceDetailHeaderViewTableViewCell : UIView

@property(nonatomic, assign) id<BTMobileServiceDetailHeaderViewDelegate>delegate;
@property (weak, nonatomic) IBOutlet UISegmentedControl *mobileAssetServiceDetailSegmentedControl;
- (IBAction)segmentValueChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *serviceIdLable;
- (void)updateWithAssetsDetailCollection:(BTAssetDetailCollection *)collectionModel withAssetType:(NSString*)assetType;

@end

NS_ASSUME_NONNULL_END
