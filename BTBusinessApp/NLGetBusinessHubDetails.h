//
//  NLGetBusinessHubDetails.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 13/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLVordelAuthenticationProtectedWebService.h"

@class NLGetBusinessHubDetails;
@class NLWebServiceError;

@protocol NLGetBusinessHubDetailsDelegate <NLVordelAuthenticationProtectedWebServiceDelegate>

- (void)getBusinessHubDetailsWebService:(NLGetBusinessHubDetails *)webService successfullyFetchedHubDetails:(NSObject *)details;

- (void)getBusinessHubDetailsWebService:(NLGetBusinessHubDetails *)webService failedToFetchHubDetailsWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLGetBusinessHubDetails : NLVordelAuthenticationProtectedWebService

@property (nonatomic, readonly) NSString *serialNumber;
@property (nonatomic, weak) id <NLGetBusinessHubDetailsDelegate> getBusinessHubDetailsDelegate;

- (instancetype)initWithSerialNumber:(NSString *)serialNumber;

@end
