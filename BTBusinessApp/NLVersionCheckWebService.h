//
//  NLVersionCheckWebService.h
//  BTBusinessApp
//
//  Created by VectoScalar on 3/16/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLWebService.h"
#import "NLEEWebService.h"

@class NLWebServiceError;
@class NLVersionCheckWebService;
@class BTVersionCheckResponse;

@protocol NLVersionCheckWebServiceDelegate <NSObject>

- (void)versionCheckWebService:(NLVersionCheckWebService *)webService succussfullyFinishedWithVersionResponse:(BTVersionCheckResponse *)versionCheckResponse;
- (void)versionCheckWebService:(NLVersionCheckWebService *)webService failedWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface NLVersionCheckWebService : NLEEWebService
@property(nonatomic, weak) id<NLVersionCheckWebServiceDelegate>versionCheckWebServiceDelegate;

@end
