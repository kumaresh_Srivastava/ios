//
//  NLGetProjectNotes.h
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLAPIGEEAuthenticatedWebService.h"

@class NLGetProjectNotes;
@class NLWebServiceError;

@protocol NLGetProjectNotesDelegate <NLAPIGEEAuthenticationProtectedWebServiceDelegate>

- (void)getProjectNotesWebService:(NLGetProjectNotes *)webService finishedWithResponse:(NSObject *)response;

- (void)getProjectNotesWebService:(NLGetProjectNotes *)webService failedWithWebServiceError:(NLWebServiceError *)error;

@optional
- (void)timedOutWithoutSuccessForGetProjectNotesWebService:(NLGetProjectNotes*)webService;

@end

@interface NLGetProjectNotes : NLAPIGEEAuthenticatedWebService

@property (nonatomic) NSDictionary* projectDetails;

@property (nonatomic, weak) id <NLGetProjectNotesDelegate> getProjectNotesDelegate;

- (instancetype)initWithProjectRef:(NSString*)projectRef;
- (instancetype)initWithProjectDetails:(NSDictionary*)projectDetails;

@end
