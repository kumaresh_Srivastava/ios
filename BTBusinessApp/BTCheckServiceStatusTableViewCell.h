//
//  BTCheckServiceStatusTableViewCell.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 22/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BTBroadbandAndPhoneService;

@interface BTCheckServiceStatusTableViewCell : UITableViewCell

- (void)updateCheckServiceStatusTableViewCellWith:(BTBroadbandAndPhoneService *)service;

@end
