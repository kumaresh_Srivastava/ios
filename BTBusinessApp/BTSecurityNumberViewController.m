//
//  BTSecurityNumberViewController.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 19/07/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTSecurityNumberViewController.h"
#import "AppManager.h"
#import "AppConstants.h"
#import "OmnitureManager.h"
#import "UIButton+BTButtonProperties.h"
#import "DLMSecurityQuestionsAndAnswerScreen.h"
#import "BTCustomInputAlertView.h"
#import "NLWebServiceError.h"
#import "NLVordelWebServiceError.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "BTCreateNewPasswordViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "DLMHomeScreen.h"

@interface BTSecurityNumberViewController () <UITextFieldDelegate,DLMSecurityQuestionsAndAnswerScreenDelegate,BTCustomInputAlertViewDelegate,BTCreateNewPasswordViewControllerDelegate,UIScrollViewDelegate> {
    BTCustomInputAlertView *_customlAlertView;
}

@property (nonatomic, readwrite) DLMSecurityQuestionsAndAnswerScreen *viewModel;

@property (strong, nonatomic) IBOutlet UIScrollView *containerScrollView;
@property (strong, nonatomic) IBOutlet UILabel *headerLabel;
@property (strong, nonatomic) IBOutlet UILabel *secNumEntryLabel;
@property (strong, nonatomic) IBOutlet UILabel *secNumReEntryLabel;
@property (strong, nonatomic) IBOutlet UIView *secNumEntryView;
@property (strong, nonatomic) IBOutlet UIView *secNumReEntryView;
@property (strong, nonatomic) IBOutlet UITextField *secNumEntryField;
@property (strong, nonatomic) IBOutlet UILabel *invalidEntryLabel;

@property (strong, nonatomic) NSMutableArray *secNumEntryContainerViews;
//@property (weak, nonatomic) IBOutlet UIButton *continueButton;

@property (nonatomic) NSUInteger pincount;

@end

@implementation BTSecurityNumberViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.pincount = 8;
    
    _viewModel = [[DLMSecurityQuestionsAndAnswerScreen alloc] init];
    _viewModel.securityQuestionAnswerDelegate = self;
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    [self createInitialUI];
    
    __weak typeof(self) weakSelf = self;
    [[self.secNumEntryField rac_textSignal] subscribeNext:^(id x) {
        // add/remove dots in entry field
        [weakSelf updateDotViews:weakSelf.secNumEntryContainerViews forTextField:weakSelf.secNumEntryField];
        // check validation
        [weakSelf validateEntry];
    }];
     [self trackContentViewedForPage:OMNIPAGE_SECURITY_NUMBER andNotificationPopupDetails:OMNIPAGE_SET_SECURITY_NUMBER];
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChangeSecurityNumber:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
    
}

- (void)trackContentViewedForPage:(NSString *)page andNotificationPopupDetails:(NSString *)notificationPopup
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Not Logged In" forKey:kOmniLoginStatus];
    
    NSString *notificaiton = [NSString stringWithFormat:@"%@%@",STATE_PREFIX,notificationPopup];
    [data setValue:notificaiton forKey:kOmniContentViewed];
    
    [OmnitureManager trackPage:page withContextInfo:data];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:OMNIPAGE_SETTINGS_CHANGE_SECURITY_NUMBER withContextInfo:data];
    
    [self.secNumEntryField becomeFirstResponder];
}

- (void)viewDidDisappear:(BOOL)animated {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
}

- (void)updateDotViews:(NSArray*)views forTextField:(UITextField*)textField
{
    for (int i=0; i<views.count; i++) {
        UIView *containerView = [views objectAtIndex:i];
        [containerView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        containerView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        if (i < textField.text.length) {
            CGFloat dotSize = MIN(containerView.bounds.size.height, containerView.frame.size.width)/3;
            CGFloat xPos = (containerView.bounds.size.width - dotSize)/2;
            CGFloat yPos = (containerView.bounds.size.height - dotSize)/2;
            UIView *dotView = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos, dotSize, dotSize)];
            dotView.backgroundColor = [UIColor blackColor];
            dotView.layer.cornerRadius = dotSize/2;
            [containerView addSubview:dotView];
            containerView.layer.borderColor = [[UIColor blackColor] CGColor];
        }
        if (i == textField.text.length && textField.isFirstResponder) {
            containerView.layer.borderColor = [[UIColor blackColor] CGColor];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//- (IBAction)continueButtonAction:(UIButton *)sender {
//    
//}

- (void)statusBarOrientationChangeSecurityNumber:(NSNotification *)notification {
    // handle the interface orientation as needed
    [self createInitialUI];
}

- (void)createInitialUI {
    
    self.navigationItem.title = @"Security number";
    
    //self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Back_Arrow"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonAction:)];
    
    if (self.isScreenrelatedTo1FAMigration || self.isScreenRelatedToInterceptorModule) {
        self.navigationItem.leftBarButtonItem = nil;
        self.headerLabel.text = @"Set up your security number";
    } else {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(backButtonAction:)];
        self.headerLabel.text = @"Update your security number";
    }
    
    if (!self.isScreenRelatedToInterceptorModule) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonAction:)];
    } else {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextButtonAction:)];
    }
   
   
    [self.secNumEntryView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [self.secNumReEntryView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    
    
    // initialize the hidden text fields for code entry
    //self.secNumEntryField.hidden = YES;
    self.secNumEntryField.keyboardType = UIKeyboardTypeNumberPad;
    self.secNumEntryField.delegate = self;
    
    // set up the secure view
    [self.secNumEntryView setBackgroundColor:[UIColor clearColor]];
    [self.secNumReEntryView setBackgroundColor:[UIColor clearColor]];
    
    [self.secNumEntryView setNeedsLayout];
    [self.secNumReEntryView setNeedsLayout];
    [self.secNumEntryView layoutIfNeeded];
    [self.secNumReEntryView layoutIfNeeded];
    
    self.secNumEntryContainerViews = [[NSMutableArray alloc] initWithCapacity:self.pincount*2];
    
    CGFloat boxPadding = 2.0;
    CGFloat boxWidth = MIN((self.secNumEntryView.bounds.size.width - (self.pincount +1)*boxPadding)/self.pincount, (self.secNumEntryView.bounds.size.height - (2*boxPadding)));
    CGFloat box2Width = MIN((self.secNumReEntryView.bounds.size.width - (self.pincount +1)*boxPadding)/self.pincount, (self.secNumReEntryView.bounds.size.height - (2*boxPadding)));
    
    CGFloat boxInset = (self.secNumEntryView.bounds.size.width - (boxWidth*self.pincount) - (boxPadding*(self.pincount+1)))/2;
    CGFloat boxInset2 = (self.secNumReEntryView.bounds.size.width - (box2Width*self.pincount) - (boxPadding*(self.pincount+1)))/2;
    
    for (int i=0; i<self.pincount; i++) {
      
        
        UIView *pinEntryContainer = [[UIView alloc] initWithFrame:CGRectMake(boxInset+boxPadding+(boxPadding*i)+(boxWidth*i), boxPadding, boxWidth, boxWidth)];
        pinEntryContainer.backgroundColor = [UIColor whiteColor];
        pinEntryContainer.layer.borderWidth = boxPadding/2;
        pinEntryContainer.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        pinEntryContainer.layer.cornerRadius = 5.0;
        [self.secNumEntryContainerViews addObject:pinEntryContainer];
        [self.secNumEntryView addSubview:pinEntryContainer];
        
        
         [ self.secNumEntryLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        
         [self.view addConstraint:[NSLayoutConstraint constraintWithItem:pinEntryContainer attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.secNumEntryLabel attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0]];
        
    }

    for (int i=0; i<self.pincount; i++) {
        UIView *pinReEntryContainer = [[UIView alloc] initWithFrame:CGRectMake(boxInset2+boxPadding+(boxPadding*i)+(box2Width*i), boxPadding, box2Width, box2Width)];
        pinReEntryContainer.backgroundColor = [UIColor whiteColor];
        pinReEntryContainer.layer.borderWidth = boxPadding/2;
        pinReEntryContainer.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        pinReEntryContainer.layer.cornerRadius = 5.0;
        [self.secNumEntryContainerViews addObject:pinReEntryContainer];
        [self.secNumReEntryView addSubview:pinReEntryContainer];
        
        [ self.secNumReEntryLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:pinReEntryContainer attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.secNumReEntryLabel attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0]];
        
    }
      [self.view updateConstraints];
    self.containerScrollView.contentSize = self.containerScrollView.frame.size;
    
    [self updateDotViews:self.secNumEntryContainerViews forTextField:self.secNumEntryField];
    // check validation
    [self validateEntry];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Validation

- (void)validateEntry {
    self.invalidEntryLabel.hidden = YES;
    BOOL isValid = NO;
    if (self.secNumEntryField.text.length >= self.pincount*2) {
        NSString *firstString = [self.secNumEntryField.text substringToIndex:self.pincount];
        NSString *secondString = [self.secNumEntryField.text substringFromIndex:self.pincount];
        if (firstString && (firstString.length == self.pincount)) {
            // first text field is full
            if (secondString && (secondString.length == self.pincount)) {
                // we have two entries of the same length
                if ([firstString isEqualToString:secondString]) {
                    // matched
                    if ([firstString rangeOfString:kSecurityNumberValidationRegularExpression options:NSRegularExpressionSearch].location != NSNotFound) {
                        isValid = YES;
                    } else {
                        // fails complexity validation
                        self.invalidEntryLabel.text = @"Please don’t use repeated or sequential numbers, e.g. 11111111, 23456789, etc.";
                        self.invalidEntryLabel.lineBreakMode = NSLineBreakByWordWrapping;
                        self.invalidEntryLabel.textAlignment = NSTextAlignmentJustified;
                        self.invalidEntryLabel.numberOfLines = 2;
                        self.invalidEntryLabel.hidden = NO;
                    }
                    //[phone rangeOfString:kPhoneRegex options:NSRegularExpressionSearch|NSRegularExpressionCaseInsensitive].location == NSNotFound
                } else {
                    // mismatched
                    self.invalidEntryLabel.text = @"Security numbers do not match";
                    self.invalidEntryLabel.lineBreakMode = NSLineBreakByWordWrapping;
                    self.invalidEntryLabel.textAlignment = NSTextAlignmentCenter;
                    self.invalidEntryLabel.numberOfLines = 1;
                    self.invalidEntryLabel.hidden = NO;
                }
            }
        }
    }

    if (isValid) {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    } else {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
}

#pragma mark - Action Methods

- (void)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)nextButtonAction:(id)sender {
    NSString *question  = @"1FA";
    NSString *answer = [AppManager stringByTrimmingWhitespaceInString:[self.secNumEntryField.text substringToIndex:self.pincount]];
    
    BTCreateNewPasswordViewController *newPasswordViewController = [BTCreateNewPasswordViewController getBTCreateNewPasswordViewController];
    newPasswordViewController.delegate = self;
    newPasswordViewController.securityQuestion = question;
    newPasswordViewController.securityAnswer = answer;
    newPasswordViewController.cugKeyFromSignInScreen = self.cugKeyFromSignInScreen;
    [self.navigationController  pushViewController:newPasswordViewController animated:YES];
}

- (void)saveButtonAction:(id)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    [self displayEnterPasswordViewWithErrorMessageHidden:YES andMessage:nil];
}

- (void)displayEnterPasswordViewWithErrorMessageHidden:(BOOL)hideErrorMessage andMessage:(NSString *)errorMessage
{
    
    _customlAlertView = [[[NSBundle mainBundle] loadNibNamed:@"BTCustomInputAlertView" owner:nil options:nil] firstObject];
    _customlAlertView.frame = CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    _customlAlertView.delegate = self;
    
    [_customlAlertView updateCustomAlertViewWithTitle:@"Enter your current password to confirm"  andMessage:nil];
    [_customlAlertView updateTextFieldPlaceholderText:@"Password" andIsSecureEntry:YES andNeedToShowRightView:YES];
    [_customlAlertView updateLoadingMessage:@"Validating password"];
    
    if(_customlAlertView)
        [_customlAlertView showKeyboard];
    
    [[[UIApplication sharedApplication] keyWindow] addSubview:_customlAlertView];
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL retVal = NO;
    NSCharacterSet *disallowed = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([string rangeOfCharacterFromSet:disallowed options:NSCaseInsensitiveSearch].location == NSNotFound) {
        NSUInteger oldLength = [[textField text] length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        retVal = newLength <= self.pincount*2;
    }
    return retVal;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self updateDotViews:self.secNumEntryContainerViews forTextField:self.secNumEntryField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self updateDotViews:self.secNumEntryContainerViews forTextField:self.secNumEntryField];
}

#pragma mark - BTCustom Alert View Delegate

- (void)userDidPressCancelOnCustomInputAlertView:(BTCustomInputAlertView *)alertView
{
    [_customlAlertView deRegisterKeyboardNotification];
    [_customlAlertView removeFromSuperview];
    _customlAlertView = nil;
    [self.secNumEntryField becomeFirstResponder];
}

- (void)userDidPressConfirmOnCustomInputAlertView:(BTCustomInputAlertView *)alertView withPassword:(NSString *)passwordText
{
    NSString *password = [AppManager stringByTrimmingWhitespaceInString:passwordText];
    [self doValidationOnPassword:password];
    
}

- (void)doValidationOnPassword:(NSString *)password
{
    if ([password length] > 0) {
        
        if (![AppManager isInternetConnectionAvailable]) {
            [_customlAlertView showErrorOnCustomInputWithWithErrro:kNoConnectionMessage];
            [AppManager trackNoInternetErrorOnPage:OMNIPAGE_SETTINGS_CHANGE_SECURITY_NUMBER];
        }
        else
        {
            [_customlAlertView removeErrorMeesageFromCustomInputAlertView];
            //[__NSCFConstantString substringToIndex:]: Index 8 out of bounds; string length 0  #Live Crash on 2.2.0 (2.2.0.14)
            if(self.secNumEntryField.text && (self.secNumEntryField.text.length > self.pincount)) {
            NSString *securityAnswer = [AppManager stringByTrimmingWhitespaceInString:[self.secNumEntryField.text substringToIndex:self.pincount]];
            
            //  self.networkRequestInProgress = YES;
                [_customlAlertView startLoading];
                
                [self.viewModel submitAndCreateProfileDetailsModelWithSecurityQuestion:@"1FA" securityAnswer:securityAnswer andPassword:password];
            } else {
                 [_customlAlertView showErrorOnCustomInputWithWithErrro:@"Please enter your password"];
            }
        }
    }
    else
    {
        //  [self displayEnterPasswordViewWithErrorMessageHidden:NO andMessage:@"Please enter your password"];
        [_customlAlertView showErrorOnCustomInputWithWithErrro:@"Please enter your password"];
    }
}

#pragma mark - DLMSecurityQuestionsAndAnswerScreenDelegate

- (void)successfullyFetchedSecurityQuestionsOnSecurityQuestionsAndAnswerScreen:(DLMSecurityQuestionsAndAnswerScreen *)securityQuestionsAndAnswerScreen
{
    // unused
}

- (void)updateSuccessfullyFinishedBySecurityQuestionsAndAnswerScreen:(DLMSecurityQuestionsAndAnswerScreen *)securityQuestionsAndAnswerScreen
{
    [_customlAlertView stopLoading];
    [_customlAlertView deRegisterKeyboardNotification];
    [_customlAlertView removeFromSuperview];
    if (self.delegate && [self.delegate respondsToSelector:@selector(successfullyUpdatedSecurityNumberOnSecurityNumberViewController:)]) {
        [self.delegate successfullyUpdatedSecurityNumberOnSecurityNumberViewController:self];
    }
    if (self.isScreenrelatedTo1FAMigration) {
        [self dismissViewControllerAnimated:YES completion:^{
            //
        }];
    } else {
        [self.navigationController popViewControllerAnimated:YES];

    }

}

- (void)securityQuestionsAndAnswerScreen:(DLMSecurityQuestionsAndAnswerScreen *)securityQuestionsAndAnswerScreen failedToSubmitUpdatedProfileDetailsWithWebServiceError:(NLWebServiceError *)webServiceError
{
    BOOL errorHandled = [self attemptVordelProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
    
    if (errorHandled == NO )
    {
        NSInteger errorCode = BTNetworkErrorCodeNone;
        
        if ([webServiceError isKindOfClass:[NLVordelWebServiceError class]]) {
            NLVordelWebServiceError *vordelError = (NLVordelWebServiceError*)webServiceError;
            errorCode = [NLVordelWebServiceError codeForVordelError:vordelError.errorCode];
        } else if ([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain]) {
            errorCode = webServiceError.error.code;
        }
        
        switch (errorCode)
        {
            case BTNetworkErrorCodeVordelParameterInvalid:
            {
                errorHandled = YES;
                [_customlAlertView stopLoading];
                [_customlAlertView showErrorOnCustomInputWithWithErrro:@"Please enter a valid password."];
                [OmnitureManager trackError:OMNIERROR_SECURITY_NUMBER_CHANGE_PASSWORD_INCORRECT onPageWithName:OMNIPAGE_SETTINGS_CHANGE_SECURITY_NUMBER contextInfo:[AppManager getOmniDictionary]];
                break;
            }
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }
//    else
//    {
//        [_customlAlertView stopLoading];
//        [_customlAlertView removeFromSuperview];
//
//    }
    
    if(errorHandled == NO)
    {
        [_customlAlertView stopLoading];
        [_customlAlertView showErrorOnCustomInputWithWithErrro:kDefaultErrorMessage];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_SETTINGS_CHANGE_SECURITY_NUMBER];
    }
}

#pragma mark - BTCreateNewPasswordViewControllerDelegate

- (void)userPressedBackButtonOnBTCreateNewPasswordViewController:(BTCreateNewPasswordViewController *)controller
{
    [self.secNumEntryField becomeFirstResponder];
}

@end


