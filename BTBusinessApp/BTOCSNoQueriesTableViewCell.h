//
//  BTOCSNoQueriesTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 18/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface BTOCSNoQueriesTableViewCell : BTBaseTableViewCell

@end

NS_ASSUME_NONNULL_END
