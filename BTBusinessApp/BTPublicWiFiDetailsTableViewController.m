//
//  BTPublicWiFiDetailsTableViewController.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 19/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <ReactiveCocoa/ReactiveCocoa.h>

#import "BTPublicWiFiDetailsTableViewController.h"

#import "AppConstants.h"
#import "AppManager.h"
#import "BrandColours.h"
#import "OmnitureManager.h"

#import "CustomSpinnerView.h"
#import "BTRetryView.h"

#import "BTPublicWifiUnavailableViewController.h"
#import "BTBaseTableViewCell.h"
#import "BTPublicWifiAlertBannerTableViewCell.h"
#import "BTPublicWifiNetworkNameTableViewCell.h"
#import "BTPublicWifiToggleTableViewCell.h"
#import "BTPublicWifiPostersTableViewCell.h"
#import "BTPublicWifiUpgradeTableViewCell.h"
#import "BTUnableToDetectHubTableViewCell.h"
#import "BTClientServiceInstance.h"

#import "NLTogglePublicWifi.h"
#import "NLVordelWebServiceError.h"


@interface BTPublicWiFiDetailsTableViewController () <UITableViewDelegate,UITableViewDataSource,BTPublicWifiToggleTableViewCellDelegate,NLTogglePublicWifiDelegate,BTRetryViewDelegate>

@property (nonatomic, assign) BOOL networkRequestInProgress;
@property (nonatomic,strong)  CustomSpinnerView *loadingView;
@property (strong, nonatomic) BTRetryView *retryView;

@property (nonatomic,strong) UILabel *pageTitleLabel;
@property (nonatomic,strong) UILabel *pageSubTitleLabel;

@property (nonatomic, assign) BTClientServiceInstance *instance;

@property (nonatomic) NLTogglePublicWifi *togglePublicWifiWebService;

@property (nonatomic, strong) NSMutableArray<BTBaseTableViewCell*> *tableCells;

@end

@implementation BTPublicWiFiDetailsTableViewController

- (instancetype)initWithClientServiceInstance:(BTClientServiceInstance *)instance
{
    self = [self initWithStyle:UITableViewStylePlain];
    if (self) {
        self.instance = instance;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self createInitialUI];
    [self setupTableCells];
    
    [self createLoadingView];
    
    __weak typeof(self) selfWeak = self;
    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([selfWeak networkRequestInProgress]) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [selfWeak.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
                [selfWeak.tableView setScrollEnabled:NO];
                [selfWeak hideLoadingItems:NO];
                [selfWeak.navigationController.navigationBar setUserInteractionEnabled:NO];
//                selfWeak.navigationItem.rightBarButtonItem.enabled = NO;
//                selfWeak.navigationItem.leftBarButtonItem.enabled = NO;
//                selfWeak.navigationItem.backBarButtonItem.enabled = NO;
//                selfWeak.navigationController.navigationItem.leftBarButtonItem.enabled = NO;
//                selfWeak.navigationController.navigationItem.rightBarButtonItem.enabled = NO;
//                selfWeak.navigationController.navigationItem.backBarButtonItem.enabled = NO;
                [selfWeak.loadingView startAnimatingLoadingIndicatorView];
            });
        }
        
        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
            dispatch_async(dispatch_get_main_queue(), ^{
                [selfWeak.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
                [selfWeak.tableView setScrollEnabled:YES];
                [selfWeak hideLoadingItems:YES];
                [selfWeak.navigationController.navigationBar setUserInteractionEnabled:YES];
//                selfWeak.navigationItem.rightBarButtonItem.enabled = YES;
//                selfWeak.navigationItem.leftBarButtonItem.enabled = YES;
//                selfWeak.navigationItem.backBarButtonItem.enabled = YES;
//                selfWeak.navigationController.navigationItem.leftBarButtonItem.enabled = YES;
//                selfWeak.navigationController.navigationItem.rightBarButtonItem.enabled = YES;
//                selfWeak.navigationController.navigationItem.backBarButtonItem.enabled = YES;
                [selfWeak.loadingView stopAnimatingLoadingIndicatorView];
                
            });
        }
    }];
    
    [self.tableView reloadData];
    [self trackPageForOmniture];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self cancelAPIcall];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createInitialUI
{
    //CGSize screenSize = [UIScreen mainScreen].bounds.size;
    
    self.pageTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 208, 20)];
    self.pageTitleLabel.font = [UIFont fontWithName:kBtFontBold size:15.0];
    self.pageTitleLabel.textColor = [UIColor whiteColor];
    self.pageTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.pageTitleLabel.text = self.instance.serviceName;
    
    self.pageSubTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 19, 208, 17.5)];
    self.pageSubTitleLabel.font = [UIFont fontWithName:kBtFontRegular size:13.0];
    self.pageSubTitleLabel.textColor = [UIColor whiteColor];
    self.pageSubTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.pageSubTitleLabel.text = self.instance.serviceId;
    
    UIView *titlesView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 208, 40)];
    [titlesView addSubview:self.pageSubTitleLabel];
    [titlesView addSubview:self.pageTitleLabel];
    
    [self.navigationItem setTitleView:titlesView];
    
    //self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon_Home_White"] style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonAction)];
    
    self.navigationItem.rightBarButtonItem = nil;
    
    //self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.view.backgroundColor = [UIColor whiteColor];//[BrandColours colourBtNeutral30];
    self.tableView.backgroundColor = [UIColor colorForHexString:@"eeeeee"];//[BrandColours colourBtNeutral30];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.separatorColor = [UIColor colorForHexString:@"eeeeee"];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.estimatedRowHeight = 250.0f;
    self.tableView.scrollsToTop = YES;
}

- (void)registerNibs
{
    // banner alerts cell
    UINib *alertNib = [UINib nibWithNibName:@"BTPublicWifiAlertBannerTableViewCell" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:alertNib forCellReuseIdentifier:@"BTPublicWifiAlertBannerTableViewCell"];
    
    // name cell
    UINib *nameNib = [UINib nibWithNibName:@"BTPublicWifiNetworkNameTableViewCell" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:nameNib forCellReuseIdentifier:@"BTPublicWifiNetworkNameTableViewCell"];
    
    // toggle cells
    UINib *toggleNib = [UINib nibWithNibName:@"BTPublicWifiToggleTableViewCell" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:toggleNib forCellReuseIdentifier:@"BTPublicWifiToggleTableViewCell"];
    
    // poster cells
    UINib *posterNib = [UINib nibWithNibName:@"BTPublicWifiPostersTableViewCell" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:posterNib forCellReuseIdentifier:@"BTPublicWifiPostersTableViewCell"];
    
    // upgrade your hub cell
    // guest wifi unavailable cell
    UINib *upgradeNib = [UINib nibWithNibName:@"BTPublicWifiUpgradeTableViewCell" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:upgradeNib forCellReuseIdentifier:@"BTPublicWifiUpgradeTableViewCell"];
    
    //Unable to detect hub cell
    UINib *unableToDetectHubNib = [UINib nibWithNibName:@"BTUnableToDetectHubTableViewCell" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:unableToDetectHubNib forCellReuseIdentifier:@"BTUnableToDetectHubTableViewCell"];
}

- (void)setupTableCells
{
    [self registerNibs];
    
    if (!self.tableCells) {
        self.tableCells = [[NSMutableArray alloc] initWithCapacity:6];
    }
    
    BTPublicWifiNetworkNameTableViewCell *nameCell = [self.tableView dequeueReusableCellWithIdentifier:@"BTPublicWifiNetworkNameTableViewCell"];
    [self.tableCells addObject:nameCell];
    
    BTPublicWifiUpgradeTableViewCell *upgradeCell = [self.tableView dequeueReusableCellWithIdentifier:@"BTPublicWifiUpgradeTableViewCell"];
    BTUnableToDetectHubTableViewCell *unableToDetectCell = [self.tableView dequeueReusableCellWithIdentifier:@"BTUnableToDetectHubTableViewCell"];
    
    if (self.instance.guestWifiFlag == BTGuestWiFiStatusFlagNewHubNeeded || self.instance.guestWifiFlag == BTGuestWiFiStatusFlagNULLSerialNum) {
        // show upgrade hub cell
        [nameCell updateCellWithNetworkType:@"Wi-Fi"];
        [nameCell updateCellWithNetworkName:self.instance.SSID?self.instance.SSID:@""];
        //[self.tableCells addObject:nameCell];
        [upgradeCell setupCellForContext:BTPublicWifiUpgradeTypeHub];
        [unableToDetectCell setupCellForContext:BTUnableToDetectPublicWifi];
        [self.tableCells addObject:unableToDetectCell];
        [self.tableCells addObject:upgradeCell];
    } else {
        // show public wi-fi cells
        BTPublicWifiToggleTableViewCell *guestWifiCell = [self.tableView dequeueReusableCellWithIdentifier:@"BTPublicWifiToggleTableViewCell"];
        [guestWifiCell setupCellForToggleType:BTPublicWifiToggleTypeBTGuestWiFi];
        [guestWifiCell setDelegate:self];
        
        BTPublicWifiToggleTableViewCell *btWifiCell = [self.tableView dequeueReusableCellWithIdentifier:@"BTPublicWifiToggleTableViewCell"];
        [btWifiCell setupCellForToggleType:BTPublicWifiToggleTypeBTWiFi];
        [btWifiCell setDelegate:self];
        
        if ([self.instance.currentWiFiStatus isEqualToString:@"GUEST_WIFI_ON"]) {
            [nameCell updateCellWithNetworkType:@"Guest Wi-Fi"];
            [nameCell updateCellWithNetworkName:self.instance.SSID?self.instance.SSID:@""];
            [guestWifiCell updateToggleState:YES];
            [btWifiCell updateToggleState:NO];
        } else if ([self.instance.currentWiFiStatus isEqualToString:@"BT_WIFI_ON"]) {
            [nameCell updateCellWithNetworkType:@"BT Wi-Fi"];
            [nameCell updateCellWithNetworkName:self.instance.SSID?self.instance.SSID:@""];
            [guestWifiCell updateToggleState:NO];
            [btWifiCell updateToggleState:YES];
        } else {
            [nameCell updateCellWithNetworkType:@"Wi-Fi"];
            [nameCell updateCellWithNetworkName:self.instance.SSID?self.instance.SSID:@""];
            [guestWifiCell updateToggleState:NO];
            [btWifiCell updateToggleState:NO];
        }
        
        
        if (self.instance.guestWifiFlag != BTGuestWiFiStatusFlagUnavailableBTWiFiOnly) {
            BTPublicWifiPostersTableViewCell *posterCell = [self.tableView dequeueReusableCellWithIdentifier:@"BTPublicWifiPostersTableViewCell"];
            [self.tableCells addObject:guestWifiCell];
            [self.tableCells addObject:btWifiCell];
            [self.tableCells addObject:posterCell];
        } else {
            [self.tableCells addObject:btWifiCell];
            // add guest wifi upgrade cell
            [upgradeCell setupCellForContext:BTPublicWifiUpgradeTypeGuestWifi];
            [self.tableCells addObject:upgradeCell];
        }
    }
}

- (void)refreshTableView
{
    [self.tableCells removeAllObjects];
    [self setupTableCells];
    [self.tableView reloadData];
}

#pragma mark - Private Helper Methods

- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow  {

    if(self.retryView)
    {
        self.retryView.hidden = NO;
        self.retryView.retryViewDelegate = self;
        [self.retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
    } else {
        self.retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
        self.retryView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
        self.retryView.retryViewDelegate = self;

        [self.view addSubview:self.retryView];

        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:64.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];

        [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.tableView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
        [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.tableView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    }
}

- (void)hideRetryItems:(BOOL)isHide {
    // (SD) Hide or Show UI elements related to retry.
    [self.retryView setHidden:isHide];
}

- (void)createLoadingView {
    
    if(!self.loadingView){
        
        self.loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        self.loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        self.loadingView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.loadingView setFrame:self.tableView.frame];
        
        [self.view addSubview:self.loadingView];
    }
    
    [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.tableView  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.tableView  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.tableView  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.tableView  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
    [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.tableView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.tableView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
}

- (void)hideLoadingItems:(BOOL)isHide {
    // (SD) Hide or Show UI elements related to loading.
    [self.loadingView setHidden:isHide];
}

#pragma mark - RetryView Delgates

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    [self hideAllAlertBanners];
    [self refreshTableView];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    NSInteger rows = 0;
    if (self.tableCells) {
        rows = self.tableCells.count;
    }
    return rows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 0.0f;
    BTBaseTableViewCell *cell = [self.tableCells objectAtIndex:indexPath.row];
    if (cell) {
        height = [cell heightForCell];
    }
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = nil;
    if (self.tableCells) {
        cell = [self.tableCells objectAtIndex:indexPath.row];
    }
    cell.preservesSuperviewLayoutMargins = false;
    cell.separatorInset = UIEdgeInsetsZero;
    cell.layoutMargins = UIEdgeInsetsZero;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - show/hide banner cells

- (void)showSuccessBannerForNetwork:(NSString*)network state:(NSString*)state
{
    NSString *networkName = @"";
    if ([[state lowercaseString] isEqualToString:@"off"]) {
        if ([network isEqualToString:@"BTWiFi"]) {
            networkName = @"BT Wi-Fi";
        } else if ([network isEqualToString:@"GuestWiFi"]) {
            networkName = @"Guest Wi-Fi";
        }
    } else if ([[state lowercaseString] isEqualToString:@"on"]) {
        networkName = self.instance.SSID;
    }
    
    BTPublicWifiAlertBannerTableViewCell *alertCell = [self.tableView dequeueReusableCellWithIdentifier:@"BTPublicWifiAlertBannerTableViewCell"];
    [alertCell.alertBannerImageView setImage:[UIImage imageNamed:@"banner_okay"]];
    NSString *keyTask = [NSString stringWithFormat:@"You've switched %@ %@",[state lowercaseString],networkName];
    [alertCell.alertBannerLabel setText:keyTask];
    //[alertCell.alertBannerLabel setTextAlignment:NSTextAlignmentCenter];
    [alertCell.contentView setBackgroundColor:[BrandColours colourTrueGreen]];
    [self showAlertBanner:alertCell];
    [self trackPageForOmnitureWithKeyTask:keyTask];
}

- (void)showFailureBannerForNetwork:(NSString*)network state:(NSString*)state
{
    NSString *networkName = @"";
    if ([network isEqualToString:@"BTWiFi"]) {
        networkName = @"BT Wi-Fi";
    } else if ([network isEqualToString:@"GuestWiFi"]) {
        networkName = @"Guest Wi-Fi";
    }
    BTPublicWifiAlertBannerTableViewCell *alertCell = [self.tableView dequeueReusableCellWithIdentifier:@"BTPublicWifiAlertBannerTableViewCell"];
    [alertCell.alertBannerImageView setImage:[UIImage imageNamed:@"banner_alert"]];
    NSString *keyTask = [NSString stringWithFormat:@"%@ did not switch %@ - please try again",networkName,[state lowercaseString]];
    [alertCell.alertBannerLabel setText:keyTask];
    [alertCell.contentView setBackgroundColor:[BrandColours colourBTBusinessRed]];
    [self showAlertBanner:alertCell];
    [self trackPageForOmnitureWithKeyTask:keyTask];
}

- (void)showPendingBannerForNetwork:(NSString*)network state:(NSString*)state
{
    NSString *networkName = @"";
    if ([network isEqualToString:@"BTWiFi"]) {
        networkName = @"BT Wi-Fi";
    } else if ([network isEqualToString:@"GuestWiFi"]) {
        networkName = @"Guest Wi-Fi";
    }
    BTPublicWifiAlertBannerTableViewCell *alertCell = [self.tableView dequeueReusableCellWithIdentifier:@"BTPublicWifiAlertBannerTableViewCell"];
    [alertCell.alertBannerImageView setImage:nil];
    
//    alertCell.alertBannerImageView.hidden = YES;
//    CGRect frame = alertCell.alertBannerImageView.frame;
//    frame.size.width = 0;
//    [alertCell.alertBannerImageView setFrame:frame];
    
    NSString *keyTask = [NSString stringWithFormat:@"Switching %@ %@",[state lowercaseString],networkName];
    //[alertCell.alertBannerLabel setText:[NSString stringWithFormat:@"%@ - this usually takes a few minutes but can be up to 24 hours",keyTask]];
    [alertCell.contentView setBackgroundColor:[BrandColours colourBackgroundBTBlueSecondaryColor]];
    [self showAlertBanner:alertCell];
    [self.loadingView.loadingLabel setText:[NSString stringWithFormat:@"Switching %@ %@...",[state lowercaseString],networkName]];
    [self trackPageForOmnitureWithKeyTask:keyTask];
}

- (void)showAlertBanner:(BTPublicWifiAlertBannerTableViewCell*)bannerCell
{
    [self hideAllAlertBanners];
    [self.tableCells insertObject:bannerCell atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
}

- (void)hideAllAlertBanners
{
    NSMutableArray *cellsToRemove = [[NSMutableArray alloc] initWithCapacity:1];
    for (UITableViewCell *cell in self.tableCells) {
        if ([cell isKindOfClass:[BTPublicWifiAlertBannerTableViewCell class]]) {
            BTPublicWifiAlertBannerTableViewCell *alertCell = (BTPublicWifiAlertBannerTableViewCell*)cell;
            [cellsToRemove addObject:alertCell];
        }
    }
    for (BTBaseTableViewCell *cell in cellsToRemove) {
        NSInteger index = [self.tableCells indexOfObject:cell];
        [self.tableCells removeObject:cell];
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
    }

}

#pragma mark - toggle cell delegate

- (void)BTPublicWiFiToggleCell:(BTPublicWifiToggleTableViewCell *)cell toggledToState:(BOOL)state
{
    [self hideAllAlertBanners];
    [self togglePublicWifi:cell.toggleType toState:state];
}

#pragma mark - api call

- (void)togglePublicWifi:(BTPublicWifiToggleType)wifiType toState:(BOOL)on
{
    if([AppManager isInternetConnectionAvailable])
    {
        [self createLoadingView];
        //[self hideRetryItems:YES];
        self.networkRequestInProgress = YES;
        NSString *wifi;
        NSString *state;
        switch (wifiType) {
            case BTPublicWifiToggleTypeBTGuestWiFi:
                wifi = @"GuestWiFi";
                break;
            case BTPublicWifiToggleTypeBTWiFi:
                wifi = @"BTWiFi";
                break;
                
            default:
                break;
        }
        if (on) {
            state = @"ON";
        } else {
            state = @"OFF";
        }
        if (wifi && state) {
            self.togglePublicWifiWebService = [[NLTogglePublicWifi alloc] initWithSerialNumber:self.instance.hubSerialNumber upadateWiFi:wifi updateWiFiValue:state];
            self.togglePublicWifiWebService.togglePublicWifiDelegate = self;
            [self.togglePublicWifiWebService resume];
        }
    }
    else
    {
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:[self pageNameForOmnitureTracking]];
    }
    
}

- (void)cancelAPIcall
{
    self.networkRequestInProgress = NO;
    if (self.togglePublicWifiWebService) {
        // Fixed in #Crash Live Build 2.1.1
        /*
         libobjc.A.dylib objc_retain
         BTBusinessApp -[BTPublicWiFiDetailsTableViewController trackPageForOmnitureWithKeyTask:] BTPublicWiFiDetailsTableViewController.m:697
         There is no need to show failure banner if user is moving back from the screen.
         */
        //[self showFailureBannerForNetwork:self.togglePublicWifiWebService.wifi state:self.togglePublicWifiWebService.state];
        self.togglePublicWifiWebService.togglePublicWifiDelegate = nil;
        [self.togglePublicWifiWebService cancel];
        self.togglePublicWifiWebService = nil;
    }
}

#pragma mark - toggle Public WiFi delegate

- (void)togglePublicWifiWebService:(NLTogglePublicWifi *)webService finishedWithResponse:(NSObject *)response
{
    self.networkRequestInProgress = NO;
    BOOL isErrorResponse = YES;
    NSObject *data = [(NSDictionary*)response objectForKey:@"data"];
    if ([data isKindOfClass:[NSString class]]) {
        NSError *error;
        NSData *stringData = [(NSString*)data dataUsingEncoding:NSUTF8StringEncoding];
        NSObject *theData = [NSJSONSerialization JSONObjectWithData:stringData options:NSJSONReadingMutableLeaves error:&error];
        if (!error && [theData isKindOfClass:[NSDictionary class]]) {
            NSLog(@"data: %@",theData);
            NSString *responseCode = [(NSDictionary*)theData objectForKey:@"responseCode"];
            NSString *responseMessage = [(NSDictionary*)theData objectForKey:@"responseMessage"];
            NSString *errorCode = [(NSDictionary*)theData objectForKey:@"errorCode"];
            NSString *errorMessage = [(NSDictionary*)theData objectForKey:@"errorMessage"];
            if ([responseCode isEqualToString:@"3"] && [responseMessage isEqualToString:@"SUCCESS"]) {
                isErrorResponse = NO;
                [self.instance updateWithToggleWifiResponse:(NSDictionary*)theData];
                [self refreshTableView];
                [self showSuccessBannerForNetwork:webService.wifi state:webService.state];
            } else {
                NSString *errString = @"";
                if (responseCode && responseMessage) {
                    errString = [NSString stringWithFormat:@"%@:%@",responseCode,responseMessage];
                } else if (errorCode && errorMessage) {
                    errString = [NSString stringWithFormat:@"%@:%@",errorCode,errorMessage];
                }
                [AppManager trackError:errString FromAPI:webService.friendlyName OnPage:[self pageNameForOmnitureTracking]];
            }
        }
    }
    if (isErrorResponse) {
        [self hideAllAlertBanners];
        [AppManager trackError:@"unexpected_response" FromAPI:webService.friendlyName OnPage:[self pageNameForOmnitureTracking]];
        [self refreshTableView];
        [self showFailureBannerForNetwork:webService.wifi state:webService.state];
    }
}

- (void)togglePublicWifiWebService:(NLTogglePublicWifi *)webService failedWithWebServiceError:(NLWebServiceError *)error
{
    self.networkRequestInProgress = NO;
    
    NSString *errString = @"";
    if ([error isKindOfClass:[NLVordelWebServiceError class]]) {
        NLVordelWebServiceError *vordelError = (NLVordelWebServiceError*)error;
        errString = [NSString stringWithFormat:@"%@:%@",vordelError.errorCode,vordelError.errorDescription];
    } else {
        errString = [NSString stringWithFormat:@"%li",(long)error.error.code];
        if ([error.error.userInfo objectForKey:NSLocalizedDescriptionKey]) {
            NSString *info = [error.error.userInfo objectForKey:NSLocalizedDescriptionKey];
            errString = [errString stringByAppendingString:[NSString stringWithFormat:@":%@",info]];
        }
    }
    [AppManager trackError:errString FromAPI:webService.friendlyName OnPage:[self pageNameForOmnitureTracking]];
    
    [self refreshTableView];
    [self showFailureBannerForNetwork:webService.wifi state:webService.state];
}

- (void)togglePublicWifiWebService:(NLTogglePublicWifi *)webService timedoutWithResponse:(NSObject *)response
{
    self.networkRequestInProgress = NO;
    [self hideAllAlertBanners];
    
    [AppManager trackError:@"workflow_timeout" FromAPI:webService.friendlyName OnPage:[self pageNameForOmnitureTracking]];
    
    BTPublicWifiUnavailableViewController *vc = [[BTPublicWifiUnavailableViewController alloc] initWithClientServiceInstance:self.instance context:BTPublicWifiUnavailableContextGuestWiFiInProgress wifiNetwork:webService.wifi andState:webService.state];
    [self.navigationController pushViewController:vc animated:YES];
    
    
}

#pragma mark - Omniture Methods

- (NSString*)pageNameForOmnitureTracking
{
    NSString *pageName = OMNIPAGE_PUBLICWIFI_MANAGE;
    switch (self.instance.guestWifiFlag) {
        case BTGuestWiFiStatusFlagNewHubNeeded:
            //pageName = OMNIPAGE_PUBLICWIFI_MANAGE_GWUNAVAILABLE12;
        case BTGuestWiFiStatusFlagUnavailableBTWiFiOnly:
            pageName = OMNIPAGE_PUBLICWIFI_MANAGE_GWUNAVAILABLE3;
            break;
        case BTGuestWiFiStatusFlagOn:
        case BTGuestWiFiStatusFlagAvailable:
        case BTGuestWiFiStatusFlagUnknown:
        case BTGuestWiFiStatusFlagCantConnect:
        case BTGuestWiFiStatusFlagUnavailableOn4GAssure:
        default:
            pageName = OMNIPAGE_PUBLICWIFI_MANAGE;
            break;
    }
    return pageName;
}

- (void)trackPageForOmniture
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = [self pageNameForOmnitureTracking];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    [data setValue:self.instance.productClass forKey:kOmniHubType];

    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackPageForOmnitureWithKeyTask:(NSString *)keyTask
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = [self pageNameForOmnitureTracking];
    
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    [data setValue:self.instance.productClass forKey:kOmniHubType];
    
    [data setValue:keyTask forKey:kOmniKeyTask];
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

@end
