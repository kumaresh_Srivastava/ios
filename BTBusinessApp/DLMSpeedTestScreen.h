#import "DLMObject.h"
#import "NLWebServiceError.h"
#import "DownloadSpeedCellType.h"

@class DLMSpeedTestScreen;

@protocol DLMSpeedTestScreenDelegate <NSObject>

- (void)successfullyPerformedSpeedTest:(DLMSpeedTestScreen *)speedTestScreen;

- (void)speedTestScreen:(DLMSpeedTestScreen *)speedTestScreen failedToPerformSpeedTestWithWebServiceError:(NLWebServiceError *)webServiceError;

- (void)speedTestCompleteShowUpgradeHubScreen;

- (void)speedTestCompleteShowUpdateHubScreen;

- (void)speedTestCompleteShowRestartHubScreen;

@end

@interface DLMSpeedTestScreen : DLMObject {
    
}

@property (nonatomic, weak) id <DLMSpeedTestScreenDelegate> speedTestScreenDelegate;

@property (nonatomic) BOOL pass;
@property (nonatomic) float downStreamSpeed;
@property (nonatomic) float upStreamSpeed;
@property (nonatomic) float downStreamNonSpeed;
@property (nonatomic) float upStreamNonSpeed;
@property (nonatomic) float mGals;
@property (nonatomic) float estimatedSpeed;
@property (nonatomic) NSString* ragStatus;
@property (nonatomic) NSString* errorMessage;
@property (nonatomic) int attemptsAtSpeedTest;
@property (nonatomic) CFAbsoluteTime timeTestBegan;
@property (nonatomic) DownloadSpeedCellType downloadResultType;

- (void)performSpeedTestWithServiceID:(NSString*)serviceID serialNumber:(NSString*)serialNumber hashCode:(NSString*)hashCode andForceExecute:(BOOL)forceExecute;

@end
