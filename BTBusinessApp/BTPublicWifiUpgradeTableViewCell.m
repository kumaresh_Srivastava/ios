//
//  BTPublicWifiUpgradeTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 10/05/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTPublicWifiUpgradeTableViewCell.h"
#import "AppConstants.h"
#import "AppManager.h"
#import "OmnitureManager.h"

@interface BTPublicWifiUpgradeTableViewCell () <UITextViewDelegate>

@end

@implementation BTPublicWifiUpgradeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _upgradeButton.layer.borderColor = [BrandColours colourTextBTPinkColor].CGColor;
    _upgradeButton.layer.borderWidth = 1;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
    [self.upgradeButton.layer setCornerRadius:self.upgradeButton.frame.size.height/10];
}

//- (CGFloat)heightForCell
//{
//    return 320.0;
//}

- (void)setupCellForContext:(BTPublicWifiUpgradeType)context
{
    
//    UIFont *regularFont = [UIFont fontWithName:kBtFontRegular size:12.0];
//    NSString *htmlString = @"With the latest <a href='https://business.bt.com/sales-comms/smart-hub/'>BT Business Smart Hub</a>, you can get even more benefits for your business:<li>Our most powerful wi-fi signal yet<li>Provide free wi-fi and protect your speed and security with <a href='https://business.bt.com/guest-wifi/'>Guest Wi-Fi<li>Add <a href='https://business.bt.com/4g-assure/'>4G Assure</a> to keep connected even if  broadband isn\'t available";
    
    UIFont *regularFont = [UIFont fontWithName:kBtFontRegular size:16.0];
    UIFont *boldFont = [UIFont fontWithName:kBtFontBold size:16.0];
    
    NSString *hubString = @"If you have a hub older than the Business Hub 5 or the Business Smart Hub, you'll need to upgrade.\n\nThe latest BT Business Smart Hub comes with our most powerful wi-fi signal yet and Guest Wi-Fi as standard. Add 4G Assure to keep connected even if broadband isn't available.";
    NSMutableAttributedString *mutableHubString = [[NSMutableAttributedString alloc] initWithString:hubString attributes:@{NSFontAttributeName:regularFont}];
    
    NSRange linkRange1 = [hubString rangeOfString:@"BT Business Smart Hub"];
    [mutableHubString addAttribute:NSLinkAttributeName value:[NSURL URLWithString:@"https://business.bt.com/sales-comms/smart-hub/"] range:linkRange1];
    
    NSRange linkRange2 = [hubString rangeOfString:@"Guest Wi-Fi"];
    [mutableHubString addAttribute:NSLinkAttributeName value:[NSURL URLWithString:@"https://business.bt.com/guest-wifi/"] range:linkRange2];
    
    NSRange linkRange3 = [hubString rangeOfString:@"4G Assure"];
    [mutableHubString addAttribute:NSLinkAttributeName value:[NSURL URLWithString:@"https://business.bt.com/4g-assure/"] range:linkRange3];
    
    NSString *guestString = @"Available with the BT Business Smart Hub and Hub 5.\n\nGet in touch today to upgrade - and enjoy the benefits of Guest Wi-Fi:\n\nSafe - risk-free wi-fi\nFast - your speeds protected\nFree - no extra cost for you or your guests";
    
    NSString *resilientString = @"4G Assure keeps you connected even if broadband isn't available.\n\n• Available with BT Smart Hub and Business Hub 5\n\n•  Hassle-free - just plug and play\n\n• Automatically switches to 4G to keep you working";
    
    self.context = context;
    switch (context) {
        case BTPublicWifiUpgradeTypeHub:
        case BTPublicWifiUpgradeType4GUpgradeHub:
            self.titleLable.text = @"Our latest hub";
           // self.upgradeButton.titleLabel.text = @"Upgrade - chat now";
            self.detailTextView.attributedText = mutableHubString;
            self.detailTextView.userInteractionEnabled = YES;
            self.detailTextView.allowsEditingTextAttributes = NO;
            self.detailTextView.scrollEnabled = NO;
            self.detailTextView.editable = NO;
            self.detailTextView.selectable = YES;
            self.detailTextView.delegate = self;
            break;
            
        case BTPublicWifiUpgradeTypeGuestWifi:
            self.titleLable.text = @"Guest Wi-Fi";
            self.upgradeButton.titleLabel.text = @"Start a live chat";
            self.detailTextView.text = guestString;
            self.detailTextView.userInteractionEnabled = NO;
            break;
            
        case BTPublicWifiUpgradeType4GAssure:
            self.titleLable.text = @"About 4G Assure";
            self.upgradeButton.titleLabel.text = @"Order Now";
            self.detailTextView.text = resilientString;
            self.detailTextView.userInteractionEnabled = NO;
            break;
            
        default:
            break;
    }
}

- (IBAction)upgradeButtonAction:(id)sender {
    NSString *urlString;
    NSString *omniString;
    switch (self.context) {
        case BTPublicWifiUpgradeTypeGuestWifi:
            urlString = kGuestWiFiOrderNowUrl; // @"https://btbusiness.custhelp.com/app/chat/chatForm7/q_id/239/scenario/business_app/?s_intcid=btb_intlink_busapp_upgrade_your_hub_chat_now";
            omniString = OMNICLICK_PUBLICWIFI_MANAGE_UPRGADE;
            break;
            
        case BTPublicWifiUpgradeTypeHub:
            urlString = kUpgradeYourHubUrl; // @"https://btbusiness.custhelp.com/app/chat/chatForm7/q_id/239/scenario/business_app/?s_intcid=btb_intlink_busapp_upgrade_your_hub_chat_now";
            omniString = OMNICLICK_PUBLICWIFI_MANAGE_UPRGADE;
            break;
            
        case BTPublicWifiUpgradeType4GUpgradeHub:
            urlString = kUpgradeYourHubUrl; // @"https://btbusiness.custhelp.com/app/chat/chatForm7/q_id/239/scenario/business_app/?s_intcid=btb_intlink_busapp_upgrade_your_hub_chat_now";
            omniString = OMNICLICK_SERVICE_STATUS_UPGRADE;
            break;
            
        case BTPublicWifiUpgradeType4GAssure:
            urlString = k4GAssureOrderNowUrl;
            omniString = OMNICLICK_SERVICE_STATUS_ORDERNOW;
            break;
            
        default:
            break;
    }
    if (urlString) {
        //[self openBrowserWithURL:urlString];
        [self trackOmniClick:omniString];
        [AppManager openURL:urlString];
    }
}

- (void)openBrowserWithURL:(NSString*)urlString
{
    if (urlString) {
        SFSafariViewController *browserView = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:urlString]];
        [browserView setModalPresentationStyle:UIModalPresentationPopover];
        [[UIApplication sharedApplication].keyWindow.rootViewController showViewController:browserView sender:nil];
    }
}

#pragma mark - Omniture Methods

- (NSString*)pageNameForOmnitureTracking
{
    NSString *pageName = @"";
    switch (self.context) {
        case BTPublicWifiUpgradeTypeHub:
            pageName = OMNIPAGE_PUBLICWIFI_MANAGE_GWUNAVAILABLE12;
            break;
        case BTPublicWifiUpgradeTypeGuestWifi:
            pageName = OMNIPAGE_PUBLICWIFI_MANAGE_GWUNAVAILABLE3;
            break;
        case BTPublicWifiUpgradeType4GUpgradeHub:
        case BTPublicWifiUpgradeType4GAssure:
            pageName = OMNIPAGE_SERVICE_STATUS_YOUR_SERVICE;
            break;
        default:
            pageName = OMNIPAGE_PUBLICWIFI_MANAGE;
            break;
    }
    return pageName;
}

- (void)trackOmniClick:(NSString *)linkTitle
{
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",[self pageNameForOmnitureTracking],linkTitle]];
}


#pragma mark - textView delegate

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange interaction:(UITextItemInteraction)interaction
{
    NSString *text = textView.text;
    NSRange range1 = [text rangeOfString:@"BT Business Smart Hub"];
    NSRange range2 = [text rangeOfString:@"Guest Wi-Fi"];
    NSRange range3 = [text rangeOfString:@"4G Assure"];
    BOOL allowable = NO;
    if (characterRange.location == range1.location || characterRange.location == range2.location || characterRange.location == range3.location) {
        allowable = YES;
    }
    if (interaction == UITextItemInteractionInvokeDefaultAction) {
        if (URL) {
            [self trackOmniClick:OMNICLICK_PUBLICWIFI_MANAGE_UPRGADE];
            NSString *urlString = URL.absoluteString;
            [AppManager openURL:urlString];
        }
    }
    return allowable;
}

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange
{
    NSString *text = textView.text;
    NSRange range1 = [text rangeOfString:@"BT Business Smart Hub"];
    NSRange range2 = [text rangeOfString:@"Guest Wi-Fi"];
    NSRange range3 = [text rangeOfString:@"4G Assure"];
    BOOL allowable = NO;
    if (characterRange.location == range1.location || characterRange.location == range2.location || characterRange.location == range3.location) {
        allowable = YES;
    }
    if (URL) {
        [self trackOmniClick:OMNICLICK_PUBLICWIFI_MANAGE_UPRGADE];
        NSString *urlString = URL.absoluteString;
        [AppManager openURL:urlString];
    }
    return allowable;
}

@end
