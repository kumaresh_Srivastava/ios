//
//  BTBackgroundMessageTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 07/11/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface BTBackgroundMessageTableViewCell : BTBaseTableViewCell
@property (strong, nonatomic) IBOutlet UILabel *largeTextLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionTextLabel;

- (void)updateWithLargeText:(NSString*)largeText andDescription:(NSString*)descriptionText;

- (void)updateWithLargeText:(NSString*)largeText andAttributedDescription:(NSString*)descriptionText;

@end

NS_ASSUME_NONNULL_END
