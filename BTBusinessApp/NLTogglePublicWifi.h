//
//  NLTogglePublicWifi.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 16/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLVordelAuthenticationProtectedWebService.h"

@class NLTogglePublicWifi;
@class NLWebServiceError;

@protocol NLTogglePublicWifiDelegate <NLVordelAuthenticationProtectedWebServiceDelegate>

- (void)togglePublicWifiWebService:(NLTogglePublicWifi *)webService finishedWithResponse:(NSObject *)response;

- (void)togglePublicWifiWebService:(NLTogglePublicWifi *)webService failedWithWebServiceError:(NLWebServiceError *)error;

- (void)togglePublicWifiWebService:(NLTogglePublicWifi *)webService timedoutWithResponse:(NSObject*)response;

@end

@interface NLTogglePublicWifi : NLVordelAuthenticationProtectedWebService

@property (nonatomic, readonly) NSString *serialNumber;
@property (nonatomic, readonly) NSString *wifi;
@property (nonatomic, readonly) NSString *state;
@property (nonatomic, weak) id <NLTogglePublicWifiDelegate> togglePublicWifiDelegate;

- (instancetype)initWithSerialNumber:(NSString *)serialNumber upadateWiFi:(NSString*)wifi updateWiFiValue:(NSString *)state;

@end
