//
//  BTNetworkIssueTableViewCell.h
//  BTBusinessApp
//
//  Created by Jim Purvis on 10/05/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTNetworkIssueTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *postcode;

@end
