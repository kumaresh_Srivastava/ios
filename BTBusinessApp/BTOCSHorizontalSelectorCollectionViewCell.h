//
//  BTOCSHorizontalSelectorCollectionViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 01/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTOCSHorizontalSelectorCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *textLabel;
@property (strong, nonatomic) IBOutlet UIView *bgView;
@property (strong, nonatomic) IBOutlet UIView *indicatorView;

@end

