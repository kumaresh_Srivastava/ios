//
//  BTVordelTokenHandler.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/10/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BTAuthenticationToken;
@class BTSMSession;

#define kSignInAuthenticationApiCallFailedNotification @"SignInAuthenticationApiCallFailedNotification"
#define kSignInAuthenticationApiCallSuccessfullyFinishedNotification @"SignInAuthenticationApiCallSuccessfullyFinishedNotification"


#define kReAuthenticationApiCallFailedNotification @"ReAuthenticationApiCallFailedNotification"
#define kReAuthenticationApiCallSuccessfullyFinishedNotification @"ReAuthenticationApiCallSuccessfullyFinishedNotification"


@interface BTAuthenticationManager : NSObject {

}

+ (BTAuthenticationManager *)sharedManager;

- (void)signInAuthenticateWithUsername:(NSString *)username andPassword:(NSString *)password;

- (void)reAuthenticateCurrentlyLoggedInUserWithPreviousToken:(BTAuthenticationToken *)previousToken withAlreadyHaveFreshTokenHandler:(void (^)(BTAuthenticationToken * token))alreadyHaveFreshTokenHandler andFetchFreshTokenHandler:(void (^) (void))fetchFreshTokenHandler;


- (void)resetPeriodicAuthenticationTimer;

- (void)turnOFFPeriodicAuthenticationTimer;

- (void)resumePeriodicAuthenticationTimer;

- (void)pausePeriodicAuthenticationTimer;


// Main Thread Only
- (BTAuthenticationToken *)authenticationTokenForCurrentLoggedInUser;

// Main Thread Only
- (BTSMSession *)smsessionForCurrentLoggedInUser;

// Main Thread Only
- (void)saveForCurrentlyLoggedInUserTheAuthenticationToken:(BTAuthenticationToken *)token smSession:(BTSMSession *)smSession andPassword:(NSString *)password;

// Main Thread Only
- (void)deleteAuthenticationTokenAndSMSEssionAndPasswordForUsername:(NSString *)username;

@end
