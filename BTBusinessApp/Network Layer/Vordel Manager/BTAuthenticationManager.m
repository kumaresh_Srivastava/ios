//
//  BTVordelTokenHandler.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/10/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTAuthenticationManager.h"
#import "NLVordelLoginService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"
#import "BTAuthenticationToken.h"
#import "BTSMSession.h"
#import "CDSMSession+CoreDataClass.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "NLWebServiceError.h"

@interface BTAuthenticationManager () <NLVordelLoginServiceDelegate> {

    BOOL *_getTokenRequestAlreadyInProgress;
    BTAuthenticationToken *_oauthToken;

    NLVordelLoginService *_signInAuthenticateLoginService;
    NLVordelLoginService *_reAuthenticateLoginService;
    NLVordelLoginService *_periodicPingAuthenticateLoginService;

    NSTimer *_periodicPingTimer;
    NSDate *_pausedPeriodicPingTimerFireDate;
}

// Main Thread Only
- (NSString *)usernameForCurrentLoggedInUser;

// Main Thread Only
- (NSString *)passwordForCurrentLoggedInUser;

- (void)invalidatePeriodicPingTimer;

- (void)triggerPeriodicPingTimerWithTimeInterval:(NSTimeInterval)timeInterval;

- (void)savePasswordForCurrentlyLoggedInUser:(NSString *)password;

- (void)saveAuthenticationTokenForCurrentlyLoggedInUser:(BTAuthenticationToken *)token;

- (void)saveSMSessionForCurrentlyLoggedInUser:(BTSMSession *)smSession;

- (void)deletePasswordForUsername:(NSString *)username;

- (void)deleteAuthenticationTokenForUsername:(NSString *)username;

- (void)deleteSMSessionForUsername:(NSString *)username;

- (NSTimeInterval)timeIntervalForVordelAuthenticationPeriodicTimer;

@end

@implementation BTAuthenticationManager

#pragma mark - Public methods

+ (BTAuthenticationManager *)sharedManager
{
    static BTAuthenticationManager *_sharedManager = nil;

    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        _sharedManager = [[BTAuthenticationManager alloc] init];
        _sharedManager->_getTokenRequestAlreadyInProgress = false;
    });

    return _sharedManager;
}



- (void)signInAuthenticateWithUsername:(NSString *)username andPassword:(NSString *)password
{
    if(_signInAuthenticateLoginService)
    {
        DDLogWarn(@"Another Sign In Authentication request recieved for username:%@ while there is already one running for username:%@.", username, _signInAuthenticateLoginService.username);
        return;
    }

    _signInAuthenticateLoginService = [[NLVordelLoginService alloc] initWithUsername:username andPassword:password];
    _signInAuthenticateLoginService.vordelLoginServiceDelegate = self;
    [_signInAuthenticateLoginService resume];
}

- (void)reAuthenticateCurrentlyLoggedInUserWithPreviousToken:(BTAuthenticationToken *)previousToken withAlreadyHaveFreshTokenHandler:(void (^)(BTAuthenticationToken * token))alreadyHaveFreshTokenHandler andFetchFreshTokenHandler:(void (^) (void))fetchFreshTokenHandler
{
    if(_reAuthenticateLoginService)
    {
        DDLogInfo(@"Reauthentication requested but it is already in progress.");
        fetchFreshTokenHandler();
    }
    else
    {
        if(previousToken)
        {
            BTAuthenticationToken *currentToken = [self authenticationTokenForCurrentLoggedInUser];

            //Check if already fetched new token, If already have new token, send it back otherwise take action to get new oauth token
            if ([currentToken.creationTime compare:previousToken.creationTime] == NSOrderedDescending)
            {
                alreadyHaveFreshTokenHandler(currentToken);
            }
            else
            {
                fetchFreshTokenHandler();

                NSString *username = [self usernameForCurrentLoggedInUser];
                NSString *password = [self passwordForCurrentLoggedInUser];

                _reAuthenticateLoginService = [[NLVordelLoginService alloc] initWithUsername:username andPassword:password];
                _reAuthenticateLoginService.vordelLoginServiceDelegate = self;
                [_reAuthenticateLoginService resume];
            }
        }
        else
        {
            fetchFreshTokenHandler();

            NSString *username = [self usernameForCurrentLoggedInUser];
            NSString *password = [self passwordForCurrentLoggedInUser];

            _reAuthenticateLoginService = [[NLVordelLoginService alloc] initWithUsername:username andPassword:password];
            _reAuthenticateLoginService.vordelLoginServiceDelegate = self;
            [_reAuthenticateLoginService resume];
        }
    }
}


// Main Thread Only
- (BTAuthenticationToken *)authenticationTokenForCurrentLoggedInUser
{
    CDApp *app = [AppDelegate sharedInstance].viewModel.app;
    CDAuthenticationToken *token = app.loggedInUser.token;
    return [[BTAuthenticationToken alloc] initWithCDAuthenticationToken:token];
}

// Main Thread Only
- (BTSMSession *)smsessionForCurrentLoggedInUser
{
    CDApp *app = [AppDelegate sharedInstance].viewModel.app;
    CDSMSession *smsession = app.loggedInUser.smsession;
    return [[BTSMSession alloc] initWithCDSMSession:smsession];
}

// Main Thread Only
- (void)saveForCurrentlyLoggedInUserTheAuthenticationToken:(BTAuthenticationToken *)token smSession:(BTSMSession *)smSession andPassword:(NSString *)password
{
    if ([NSThread isMainThread])
    {
        if(password)
            [self savePasswordForCurrentlyLoggedInUser:password];

        if(token)
            [self saveAuthenticationTokenForCurrentlyLoggedInUser:token];

        if(smSession)
            [self saveSMSessionForCurrentlyLoggedInUser:smSession];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{

            if(password)
                [self savePasswordForCurrentlyLoggedInUser:password];

            if(token)
                [self saveAuthenticationTokenForCurrentlyLoggedInUser:token];

            if(smSession)
                [self saveSMSessionForCurrentlyLoggedInUser:smSession];
        });
    }
}

// Main Thread Only
- (void)deleteAuthenticationTokenAndSMSEssionAndPasswordForUsername:(NSString *)username
{
    if ([NSThread isMainThread])
    {
        [self deletePasswordForUsername:username];
        [self deleteSMSessionForUsername:username];
        [self deleteAuthenticationTokenForUsername:username];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{

            [self deletePasswordForUsername:username];
            [self deleteSMSessionForUsername:username];
            [self deleteAuthenticationTokenForUsername:username];

        });
    }
}


- (void)resetPeriodicAuthenticationTimer
{
    DDLogInfo(@"Resetting Vordel Authentication Periodic Timer.");

    _pausedPeriodicPingTimerFireDate = nil;

    _periodicPingAuthenticateLoginService.vordelLoginServiceDelegate = nil;
    [_periodicPingAuthenticateLoginService cancel];
    _periodicPingAuthenticateLoginService = nil;

    if(_periodicPingTimer)
    {
        [self invalidatePeriodicPingTimer];
    }

    [self triggerPeriodicPingTimerWithTimeInterval:[self timeIntervalForVordelAuthenticationPeriodicTimer]];
}

- (void)turnOFFPeriodicAuthenticationTimer
{
    DDLogInfo(@"Turning OFF Vordel Authentication Periodic Timer.");

    _pausedPeriodicPingTimerFireDate = nil;

    _periodicPingAuthenticateLoginService.vordelLoginServiceDelegate = nil;
    [_periodicPingAuthenticateLoginService cancel];
    _periodicPingAuthenticateLoginService = nil;

    if(_periodicPingTimer)
    {
        [self invalidatePeriodicPingTimer];
    }
}

- (void)resumePeriodicAuthenticationTimer
{
    if(_pausedPeriodicPingTimerFireDate)
    {
        if(_pausedPeriodicPingTimerFireDate == [_pausedPeriodicPingTimerFireDate earlierDate:[NSDate date]])
        {
            DDLogInfo(@"Vordel Authentication Periodic Timer already missed while resuming. Creating a timer with 1 second of fire date from now.");

            _pausedPeriodicPingTimerFireDate = nil;

            _periodicPingAuthenticateLoginService.vordelLoginServiceDelegate = nil;
            [_periodicPingAuthenticateLoginService cancel];
            _periodicPingAuthenticateLoginService = nil;

            if(_periodicPingTimer)
            {
                [self invalidatePeriodicPingTimer];
            }

            [self triggerPeriodicPingTimerWithTimeInterval:5];
        }
        else
        {
            NSTimeInterval timeInterval = [_pausedPeriodicPingTimerFireDate timeIntervalSinceNow];

            DDLogInfo(@"Resuming Vordel Authentication Periodic Timer with time interval as %f seconds based on fire date when it was paused.", timeInterval);

            _pausedPeriodicPingTimerFireDate = nil;

            _periodicPingAuthenticateLoginService.vordelLoginServiceDelegate = nil;
            [_periodicPingAuthenticateLoginService cancel];
            _periodicPingAuthenticateLoginService = nil;

            if(_periodicPingTimer)
            {
                [self invalidatePeriodicPingTimer];
            }

            [self triggerPeriodicPingTimerWithTimeInterval:timeInterval];
        }
    }
    else
    {
        DDLogInfo(@"No paused fire date found for Vordel Authentication Periodic Timer while resuming. Hence resetting a new timer.");
        [self resetPeriodicAuthenticationTimer];
    }
}

- (void)pausePeriodicAuthenticationTimer
{
    DDLogInfo(@"Pausing Vordel Authentication Periodic Timer if active.");

    _pausedPeriodicPingTimerFireDate = _periodicPingTimer.fireDate;

    _periodicPingAuthenticateLoginService.vordelLoginServiceDelegate = nil;
    [_periodicPingAuthenticateLoginService cancel];
    _periodicPingAuthenticateLoginService = nil;

    if(_periodicPingTimer)
    {
        [self invalidatePeriodicPingTimer];
    }
}





#pragma mark - Private Helper methods

// Main Thread Only
- (NSString *)usernameForCurrentLoggedInUser
{
    CDApp *app = [AppDelegate sharedInstance].viewModel.app;
    NSString *username = app.loggedInUser.username;
    return username;
}

// Main Thread Only
- (NSString *)passwordForCurrentLoggedInUser
{
    CDApp *app = [AppDelegate sharedInstance].viewModel.app;
    NSString *username = app.loggedInUser.username;
    NSString *password = nil;

    if(username)
    {
        password = [SAMKeychain passwordForService:kKeyChainServiceBTUserPassword account:username];
    }

    return password;
}

- (void)triggerPeriodicPingTimerWithTimeInterval:(NSTimeInterval)timeInterval
{
    DDLogInfo(@"Triggering new Vordel Authentication Periodic Timer with timer interval %f seconds.", timeInterval);

    _periodicPingTimer = [NSTimer scheduledTimerWithTimeInterval:timeInterval
                                                          target:self
                                                        selector:@selector(timeToPingWithTimer:)
                                                        userInfo:nil
                                                         repeats:YES];

}

- (void)timeToPingWithTimer:(NSTimer *)timer
{
    if(!(_reAuthenticateLoginService || _signInAuthenticateLoginService))
    {
        DDLogInfo(@"Executing Vordel Authentication Periodic Timer Ping.");

        NSString *username = [self usernameForCurrentLoggedInUser];
        NSString *password = [self passwordForCurrentLoggedInUser];

        _periodicPingAuthenticateLoginService.vordelLoginServiceDelegate = nil;
        _periodicPingAuthenticateLoginService = [[NLVordelLoginService alloc] initWithUsername:username andPassword:password];
        _periodicPingAuthenticateLoginService.vordelLoginServiceDelegate = self;
        [_periodicPingAuthenticateLoginService resume];
    }
    else
    {
        DDLogInfo(@"Skipping Vordel Authentication Periodic Timer Ping because another authentication call is already active.");
    }

    if(timer.timeInterval != [self timeIntervalForVordelAuthenticationPeriodicTimer])
    {
        DDLogInfo(@"Time interval of Vordel Authentication Periodic Timer is different from what it should be. Adjusting and resetting the timer.");

        _pausedPeriodicPingTimerFireDate = nil;

        if(_periodicPingTimer)
        {
            [self invalidatePeriodicPingTimer];
        }

        [self triggerPeriodicPingTimerWithTimeInterval:[self timeIntervalForVordelAuthenticationPeriodicTimer]];
    }
}

- (void)invalidatePeriodicPingTimer
{
    [_periodicPingTimer invalidate];
    _periodicPingTimer = nil;
}

- (void)savePasswordForCurrentlyLoggedInUser:(NSString *)password
{
    //Save the password in the keychain
    CDUser *user = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    
    if (password == nil || [password isEqualToString:@""])
    {
        DDLogError(@"Error : Password is nil or empty string for username '%@'.", user.username);
        NSAssert(NO, @"Error : Password is nil or empty string for username '%@'.", user.username);
    }
    
    NSError *errorWhileSavingPassword = nil;
    BOOL savePasswordSuccess = [SAMKeychain setPassword:password forService:kKeyChainServiceBTUserPassword account:user.username error:&errorWhileSavingPassword];
    if(!savePasswordSuccess)
    {
        DDLogError(@"Failed to save password in keychain for username  '%@'", user.username);
    }
}

- (void)saveAuthenticationTokenForCurrentlyLoggedInUser:(BTAuthenticationToken *)token
{
    CDUser *user = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    if (user)
    {
        CDAuthenticationToken *newTokenObject = (CDAuthenticationToken *)[NSEntityDescription insertNewObjectForEntityForName:@"CDAuthenticationToken" inManagedObjectContext:[AppDelegate sharedInstance].managedObjectContext];

        newTokenObject.accessToken = token.accessToken;
        newTokenObject.tokenString = token.accessToken;
        newTokenObject.creationTime = token.creationTime;
        newTokenObject.expiresIn = token.expiresIn;
        newTokenObject.refreshToken = token.refreshToken;
        newTokenObject.tokenType = token.tokenType;
        user.token = newTokenObject;

        [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
    }
    else
    {
        DDLogError(@"Unable to save authentication token as no user is currently logged IN in database.");
    }
}

- (void)saveSMSessionForCurrentlyLoggedInUser:(BTSMSession *)smSession
{
    CDUser *user = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    if (user)
    {
        CDSMSession *newSmSessionObject = (CDSMSession *)[NSEntityDescription insertNewObjectForEntityForName:@"CDSMSession" inManagedObjectContext:[AppDelegate sharedInstance].managedObjectContext];

        newSmSessionObject.smsessionString = smSession.smsessionString;
        newSmSessionObject.creationTime = smSession.creationTime;
        user.smsession = newSmSessionObject;

        [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
    }
    else
    {
        DDLogError(@"Unable to save smsession as no user is currently logged IN in database.");
    }
}

- (void)deletePasswordForUsername:(NSString *)username
{
    // (hd) Delete the Password in the keychain for previous user
    BOOL removePasswordSuccess = [SAMKeychain deletePasswordForService:kKeyChainServiceBTUserPassword account:username];
    if(!removePasswordSuccess)
    {
        DDLogError(@"Failed to delete Password in keychain for previously logged in user with username  '%@'", username);
    }
}

- (void)deleteAuthenticationTokenForUsername:(NSString *)username
{
    // (hd) Delelte the authentication token for the previous user.
    CDUser *user = [CDUser userWithUsername:username inManagedObjectContext:[AppDelegate sharedInstance].managedObjectContext];
    CDAuthenticationToken *previousAuthTokenObject = user.token;
    user.token = nil;
    if(previousAuthTokenObject)
    {
        [[AppDelegate sharedInstance].managedObjectContext deleteObject:previousAuthTokenObject];
    }
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
}

- (void)deleteSMSessionForUsername:(NSString *)username
{
    // (hd) Delelte the authentication token for the previous user.
    CDUser *user = [CDUser userWithUsername:username inManagedObjectContext:[AppDelegate sharedInstance].managedObjectContext];
    CDSMSession *previousSMSession = user.smsession;
    user.smsession = nil;
    if(previousSMSession)
    {
        [[AppDelegate sharedInstance].managedObjectContext deleteObject:previousSMSession];
    }
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
}

- (NSTimeInterval)timeIntervalForVordelAuthenticationPeriodicTimer
{
    NSTimeInterval periodicPingTimeInterval = kPeriodicVordelAuthenticationPingDefaultTimeInterval;

    CDAuthenticationToken *authToken = [AppDelegate sharedInstance].viewModel.app.loggedInUser.token;
    if(authToken && authToken.expiresIn)
    {
        periodicPingTimeInterval = [authToken.expiresIn integerValue] - 60;
    }

    return periodicPingTimeInterval;
}

#pragma mark - NLVordelLoginServiceDelegate methods


- (void)vordelLoginService:(NLVordelLoginService *)service loginSuccesfulWithToken:(BTAuthenticationToken *)oauthToken smSession:(BTSMSession *)smSession forUsername:(NSString *)username
{
    if(_signInAuthenticateLoginService == service)
    {
        NSDictionary *credentialInfo = [NSDictionary dictionaryWithObjectsAndKeys:smSession, @"smsession", username, @"username", oauthToken, @"token", nil];

        [[NSNotificationCenter defaultCenter] postNotificationName:kSignInAuthenticationApiCallSuccessfullyFinishedNotification object:nil userInfo:credentialInfo];

        _signInAuthenticateLoginService.vordelLoginServiceDelegate = nil;
        _signInAuthenticateLoginService = nil;
    }
    else if(_reAuthenticateLoginService == service)
    {
        // Saving the received smsession and token into database.
        [self saveSMSessionForCurrentlyLoggedInUser:smSession];
        [self saveAuthenticationTokenForCurrentlyLoggedInUser:oauthToken];


        NSDictionary *credentialInfo = [NSDictionary dictionaryWithObjectsAndKeys:smSession, @"smsession", username, @"username", oauthToken, @"token", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kReAuthenticationApiCallSuccessfullyFinishedNotification object:nil userInfo:credentialInfo];

        _reAuthenticateLoginService.vordelLoginServiceDelegate = nil;
        _reAuthenticateLoginService = nil;

        [self resetPeriodicAuthenticationTimer];
    }
    else if(_periodicPingAuthenticateLoginService == service)
    {
        // Saving the received smsession and token into database.
        [self saveSMSessionForCurrentlyLoggedInUser:smSession];
        [self saveAuthenticationTokenForCurrentlyLoggedInUser:oauthToken];

        DDLogInfo(@"Vordel Authentication API Call succesful for Vordel Authentication Periodic Timer. Saved the latest token and smsession.");

        _periodicPingAuthenticateLoginService.vordelLoginServiceDelegate = nil;
        _periodicPingAuthenticateLoginService = nil;

        [self resetPeriodicAuthenticationTimer];
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLVordelLoginService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLVordelLoginService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)vordelLoginService:(NLVordelLoginService *)service failedWithError:(NLWebServiceError *)error forUsername:(NSString *)username
{
    if(_signInAuthenticateLoginService == service)
    {
        
        DDLogError(@"Sign In Authentication Api Call Failed with error code %ld", (long)error.error.code);
        
        NSDictionary *infoDic = [NSDictionary dictionaryWithObjectsAndKeys:error, @"error", username, @"username", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kSignInAuthenticationApiCallFailedNotification object:nil userInfo:infoDic];

        _signInAuthenticateLoginService.vordelLoginServiceDelegate = nil;
        _signInAuthenticateLoginService = nil;
    }
    else if(_reAuthenticateLoginService == service)
    {
        DDLogError(@"Sign In Re- Authentication Api Call Failed with error code %ld", (long)error.error.code);
        
        NSDictionary *infoDic = [NSDictionary dictionaryWithObjectsAndKeys:error, @"error", username, @"username", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kReAuthenticationApiCallFailedNotification object:nil userInfo:infoDic];

        _reAuthenticateLoginService.vordelLoginServiceDelegate = nil;
        _reAuthenticateLoginService = nil;
    }
    else if(_periodicPingAuthenticateLoginService == service)
    {
        DDLogError(@"Vordel Authentication API Call failed for Vordel Authentication Periodic Timer with error code %ld", (long)error.error.code);
        
        _periodicPingAuthenticateLoginService.vordelLoginServiceDelegate = nil;
        _periodicPingAuthenticateLoginService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLVordelLoginService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLVordelLoginService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}






@end
