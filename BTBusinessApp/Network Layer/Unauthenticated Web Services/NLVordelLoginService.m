//
//  NLVordelLoginService.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/12/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLVordelLoginService.h"
#import "AppConstants.h"
#import <NSString+URLEncode.h>
#import "AFNetworking.h"
#import "NLConstants.h"
#import "NSObject+APIResponseCheck.h"
#import "BTAuthenticationToken.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "BTSMSession.h"
#import "NLWebServiceError.h"


// (lpm) Vordel Client ID and Client Secret

unsigned char clientIdDebug[] =  {
    0x65, 0x62, 0x35, 0x31,
    0x31, 0x38, 0x66, 0x63,
    0x2d, 0x31, 0x65, 0x32,
    0x39, 0x2d, 0x34, 0x39,
    0x39, 0x32, 0x2d, 0x39,
    0x34, 0x63, 0x63, 0x2d,
    0x31, 0x36, 0x35, 0x37,
    0x38, 0x39, 0x33, 0x35,
    0x34, 0x35, 0x39, 0x61
};


unsigned char clientSecretDebug[] =  {
    0x61, 0x34, 0x35, 0x34,
    0x61, 0x64, 0x31, 0x37,
    0x2d, 0x61, 0x31, 0x64,
    0x34, 0x2d, 0x34, 0x37,
    0x38, 0x61, 0x2d, 0x62,
    0x30, 0x66, 0x38, 0x2d,
    0x33, 0x39, 0x66, 0x31,
    0x35, 0x61, 0x63, 0x35,
    0x63, 0x34, 0x37, 0x65
};


unsigned char clientIdLive[] =  {
    0x65, 0x34, 0x33, 0x38,
    0x37, 0x65, 0x63, 0x30,
    0x2d, 0x34, 0x66, 0x39,
    0x35, 0x2d, 0x34, 0x33,
    0x39, 0x30, 0x2d, 0x61,
    0x35, 0x36, 0x36, 0x2d,
    0x63, 0x35, 0x65, 0x65,
    0x39, 0x39, 0x30, 0x62,
    0x63, 0x64, 0x38, 0x33
};


unsigned char clientSecretLive[] =  {
    0x38, 0x34, 0x64, 0x66,
    0x34, 0x38, 0x33, 0x66,
    0x2d, 0x63, 0x36, 0x34,
    0x61, 0x2d, 0x34, 0x36,
    0x64, 0x37, 0x2d, 0x61,
    0x30, 0x61, 0x31, 0x2d,
    0x64, 0x35, 0x37, 0x39,
    0x64, 0x38, 0x36, 0x34,
    0x64, 0x39, 0x36, 0x33
};


@interface NLVordelLoginService () {

}

- (NSString *)smsessionCookieValueFromResponse:(NSHTTPURLResponse *)response;

- (NSString *)getOauthTokenDataUsername:(NSString *)username password:(NSString *)password;

@end

@implementation NLVordelLoginService

- (instancetype)initWithUsername:(NSString *)username andPassword:(NSString *)password
{
    NSString *parameterString = [self getOauthTokenDataUsername:username password:password];

    NSString *endPointURLString = [NSString stringWithFormat:@"/authentication/oauth"];
    //NSString *endPointURLString = [NSString stringWithFormat:@"/mbaas-wrapper/authentication/oauth?mode=token"];

    self = [super initWithMethod:@"POST" parameters:parameterString andEndpointUrlString:endPointURLString];
    if(self)
    {
        _username = username;
        _password = password;
    }

    DDLogVerbose(@"Vordel Login Service Object Number: %@", self);

    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask
- (AFHTTPSessionManager *)sessionManager
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];

    NSString *baseURLString = [NSString stringWithFormat:@"https://%@", [AppManager baseURLStringFromBaseURLType:_baseURLType]];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseURLString] sessionConfiguration:configuration];

    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    [manager.requestSerializer setTimeoutInterval:_timeOutTimeInterval];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];

    // (hd) Because the API call expects the parameters in raw format, hence we are setting an empty query string serializer so that it does not do any serialization.
    [manager.requestSerializer setQueryStringSerializationWithBlock:^NSString * _Nonnull(NSURLRequest * _Nonnull request, id  _Nonnull parameters, NSError * _Nullable __autoreleasing * _Nullable error) {

        return parameters;
    }];
    
    return manager;
}


#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    // (hds) Get SMSession Cookie String
    NSHTTPURLResponse *headerResponse = (NSHTTPURLResponse *)[task response];
    NSString *smsessionString = [self smsessionCookieValueFromResponse:headerResponse];
    BTSMSession *smSession = [[BTSMSession alloc] initWithSMSessionString:smsessionString];

    // (hds) Get Auth Token
    BTAuthenticationToken *oauthToken = [[BTAuthenticationToken alloc] initWithVordelTokenData:responseObject];

    [self.vordelLoginServiceDelegate vordelLoginService:self loginSuccesfulWithToken:oauthToken smSession:smSession forUsername:_username];

    DDLogVerbose(@"Vordel Login Service Suceeded for Object Number: %@", self);
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    DDLogError(@"Webservice with URL: %@ failed with error code: %ld.", [[task currentRequest] URL], (long)[webServiceError.error code]);
    
    [self.vordelLoginServiceDelegate vordelLoginService:self failedWithError:webServiceError forUsername:_username];

    DDLogVerbose(@"Vordel Login Service Failed for Object Number: %@", self);
}





#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andError:(NSError *)error
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andError:error];

    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.


        // (hds) Nothing to check the error cases here.
    }

    return errorToBeReturned;
}

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.

        if ([responseObject isValidVordelOauthTokenResponseObject])
        {
            NSHTTPURLResponse *headerResponse = (NSHTTPURLResponse *)[task response];

            NSString *smsessionString = [self smsessionCookieValueFromResponse:headerResponse];

            if (smsessionString == nil || [smsessionString isEqualToString:@""])
            {
                NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeVordelSMSessionMissingInHeader userInfo:nil];

                errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
            }
        }
        else
        {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidTokenResponseObject userInfo:nil];

            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
        }
    }

    return errorToBeReturned;
}


#pragma mark - Private Helper Methods
- (NSString *)smsessionCookieValueFromResponse:(NSHTTPURLResponse *)response
{
    NSDictionary *headerDictionary = [response allHeaderFields];

    NSString *smsessionString = nil;

    if ([headerDictionary valueForKey:@"Set-Cookie"])
    {
        NSString *setCookie = [headerDictionary valueForKey:@"Set-Cookie"];

        NSArray *arrayOfSetCookieFields = [setCookie componentsSeparatedByString:@"; "];

        if ([arrayOfSetCookieFields count] > 0)
        {
            NSString *smsessionStringWithTitle = [arrayOfSetCookieFields objectAtIndex:0];

            if ([smsessionStringWithTitle length] > 10 && [[smsessionStringWithTitle substringWithRange:NSMakeRange(0, 9)] isEqualToString:kSiteminderCookieName])
            {
                smsessionString = [smsessionStringWithTitle substringFromIndex:10];
            }
        }
    }
    return smsessionString;
}


- (NSString *)getOauthTokenDataUsername:(NSString *)username password:(NSString *)password {

    NSString *grantType = @"password";
    NSString *udid = [AppDelegate sharedInstance].viewModel.app.btInstallationID;
    NSString *make = @"Apple";
    NSString *model = [[UIDevice currentDevice] model];
    NSString *os = [[UIDevice currentDevice] systemName];
    NSString *osVersion = [[UIDevice currentDevice] systemVersion];

    username = [username URLEncodeUsingEncoding:NSUTF8StringEncoding];
    password = [password URLEncodeUsingEncoding:NSUTF8StringEncoding];
    grantType = [grantType URLEncodeUsingEncoding:NSUTF8StringEncoding];
    udid = [udid URLEncodeUsingEncoding:NSUTF8StringEncoding];
    make = [make URLEncodeUsingEncoding:NSUTF8StringEncoding];
    model = [model URLEncodeUsingEncoding:NSUTF8StringEncoding];
    os = [os URLEncodeUsingEncoding:NSUTF8StringEncoding];
    osVersion = [osVersion URLEncodeUsingEncoding:NSUTF8StringEncoding];

    NSString *clientID = nil;
    NSString *clientSecret = nil;
    
    if ([kBTServerType isEqualToString:@"BTServerTypeModelA"])
    {
        clientID = [[NSString alloc] initWithBytes:clientIdDebug length:36 encoding:NSASCIIStringEncoding];
        clientSecret = [[NSString alloc] initWithBytes:clientSecretDebug length:36 encoding:NSASCIIStringEncoding];
    }
    else
    {
        clientID = [[NSString alloc] initWithBytes:clientIdLive length:36 encoding:NSASCIIStringEncoding];
        clientSecret = [[NSString alloc] initWithBytes:clientSecretLive length:36 encoding:NSASCIIStringEncoding];
    }

    return [NSString stringWithFormat:@"grant_type=%@&username=%@&password=%@&client_id=%@"
            "&client_secret=%@&authenticate=btone&udid=%@&make=%@&model=%@&os=%@&osv=%@&smsession=true",
            grantType, username, password, clientID, clientSecret, udid, make, model, os, osVersion];
}

#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
//    NLWebServiceError *errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeHTTPTimeOut];
//    
//    return errorToBeReturned;
    
    return nil;
}


@end
