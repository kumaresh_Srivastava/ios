//
//  NLVordelLoginService.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/12/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLVordelWebService.h"
//#import "NLEEWebService.h"


@class BTAuthenticationToken;
@class BTSMSession;
@class NLWebServiceError;
@class NLVordelLoginService;

@protocol NLVordelLoginServiceDelegate <NSObject>

- (void)vordelLoginService:(NLVordelLoginService *)service loginSuccesfulWithToken:(BTAuthenticationToken *)oauthToken smSession:(BTSMSession *)smSession forUsername:(NSString *)username;

- (void)vordelLoginService:(NLVordelLoginService *)service failedWithError:(NLWebServiceError *)error forUsername:(NSString *)username;

@end

@interface NLVordelLoginService : NLVordelWebService {
//@interface NLVordelLoginService : NLEEWebService {

}

@property (nonatomic, readonly) NSString *username;
@property (nonatomic, readonly) NSString *password;
@property (nonatomic, weak) id <NLVordelLoginServiceDelegate> vordelLoginServiceDelegate;

- (instancetype)initWithUsername:(NSString *)username andPassword:(NSString *)password;

@end
