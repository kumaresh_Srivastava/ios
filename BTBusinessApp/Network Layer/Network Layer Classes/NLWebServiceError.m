//
//  NLWebServiceError.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 20/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLWebServiceError.h"

@implementation NLWebServiceError

- (id)initWithError:(NSError *)error andSourceError:(NLWebServiceError *)sourceError
{
    self = [super init];
    if(self)
    {
        _error = error;
        _sourceError = sourceError;
    }
    return self;
}

#pragma mark - Factory Methods

+ (NLWebServiceError *)webServiceNetworkErrorWithErrorDomain:(NSString *)networkErrorDomain andNetworkErrorCode:(BTNetworkErrorCode)networkErrorCode
{
    NSError *newErrorObject = [NSError errorWithDomain:networkErrorDomain code:networkErrorCode userInfo:nil];
    NLWebServiceError *webServiceError = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];

    return webServiceError;
}


@end
