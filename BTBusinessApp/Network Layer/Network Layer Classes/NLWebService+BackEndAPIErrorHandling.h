//
//  NLWebService+BackEndAPIErrorHandling.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 20/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLWebService.h"

@class NLWebServiceError;

@interface NLWebService (BackEndAPIErrorHandling)

- (NLWebServiceError *)errorFromBackendAPIAgreedResponseDic:(NSDictionary *)responseDic;

@end
