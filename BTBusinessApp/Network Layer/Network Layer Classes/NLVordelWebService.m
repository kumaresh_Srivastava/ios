//
//  NLVordelWebService.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 20/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLVordelWebService.h"
#import "NLWebServiceError.h"
#import "NLVordelWebServiceError.h"
#import "AFNetworking.h"
#import "AppConstants.h"
#import "NLConstants.h"

@implementation NLVordelWebService

- (instancetype)initWithMethod:(NSString *)method parameters:(NSString *)parameters andEndpointUrlString:(NSString *)endpointUrlString
{
    self = [super initWithMethod:method parameters:parameters endpointUrlString:endpointUrlString andBaseURLType:NLBaseURLTypeVordel];
    if(self)
    {
        
    }

    return self;
}

- (NSString *)friendlyName
{
    return @"VordelAPI";
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];

}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
}


#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andError:(NSError *)error
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andError:error];

    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.

        // (hds) Checking for Vordel Level Error Messages And Codes
        NSData *jsonData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        if(jsonData)
        {
            NSError *errorWhileCreatingJSON;
            NSDictionary* json = nil;
            json = [NSJSONSerialization JSONObjectWithData:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] options: NSJSONReadingMutableContainers error:&errorWhileCreatingJSON];

            if(json && [json valueForKey:@"code"])
            {
                NSString *errorCodeFromServer = [[json valueForKey:@"code"] lowercaseString];

                NSString *errCode = [json valueForKey:@"code"];
                NSString *errDesc = [json valueForKey:@"error"];
                
                if (errCode && errDesc) {
                    errorToBeReturned = [NLVordelWebServiceError VordelwebServiceNetworkErrorWithErrorCode:errCode andDescription:errDesc];
                }
                
                BTNetworkErrorCode errorCode = BTNetworkErrorCodeNone;

                if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForParameterMissing])
                {
                    errorCode = BTNetworkErrorCodeVordelParameterMissing;
                }
                else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForParameterInvalid])
                {
                    errorCode = BTNetworkErrorCodeVordelParameterInvalid;
                }
                else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForClientUnauthenticated])
                {
                    errorCode = BTNetworkErrorCodeVordelClientUnauthenticated;
                }
                else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForUserUnauthenticated])
                {
                    errorCode = BTNetworkErrorCodeVordelUserUnauthenticated;
                }
                else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForTokenInvalidOrExpired])
                {
                    errorCode = BTNetworkErrorCodeVordelTokenInvalidOrExpired;
                }
                else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForDeviceMismatch])
                {
                    errorCode = BTNetworkErrorCodeVordelDeviceMismatch;
                }
                else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForScopeInvalid])
                {
                    errorCode = BTNetworkErrorCodeVordelScopeInvalid;
                }
                else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForRefreshInvalidOrExpired])
                {
                    errorCode = BTNetworkErrorCodeVordelRefreshInvalidOrExpired;
                }
                else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForAccountLocked])
                {
                    errorCode = BTNetworkErrorCodeVordelAccountLocked;
                }
                else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForServiceDeprecated])
                {
                    errorCode = BTNetworkErrorCodeVordelServiceDeprecated;
                }
                else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForInvalidDataError])
                {
                    errorCode = BTNetworkErrorCodeVordelInvalidDataError;
                }
                else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForEntityTooLarge])
                {
                    errorCode = BTNetworkErrorCodeVordelEntityTooLarge;
                }
                else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForMbaasInternalException])
                {
                    errorCode = BTNetworkErrorCodeVordelMbassInternalExceptionError;
                }
                else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForPlannedServiceOutage])
                {
                    errorCode = BTNetworkErrorCodeVordelPlannedServiceOutage;
                }
                else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForUnplannedServiceOutage])
                {
                    errorCode = BTNetworkErrorCodeVordelUnplannedServiceOutage;
                }
                else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForMbaasTimeout])
                {
                    errorCode = BTNetworkErrorCodeVordelMbassTimeout;
                }
                else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForLegacyAccount])
                {
                    errorCode = BTNetworkErrorCodeVordelLegacyAccount;
                }

                if(errorCode != BTNetworkErrorCodeNone)
                {
                    if (!errorToBeReturned) {
                        NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:errorCode userInfo:nil];
                        
                        errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
                    }
                    
                }
            }
            else
            {
                NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeGeneralNetworkError userInfo:nil];

                errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
            }
        }
        else
        {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeGeneralNetworkError userInfo:nil];

            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
        }
    }

    return errorToBeReturned;
}

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        // (gv) generic MBaaS error handling
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *response = (NSDictionary*)responseObject;
            if ([response objectForKey:@"error"] || [response objectForKey:@"code"]) {
                NSString *errCode = [response objectForKey:@"code"]?[response objectForKey:@"code"]:@"";
                NSString *errDesc = [response objectForKey:@"error"]?[response objectForKey:@"error"]:@"";
                errorToBeReturned = [NLVordelWebServiceError VordelwebServiceNetworkErrorWithErrorCode:errCode andDescription:errDesc];
            }
        }
    }

    return errorToBeReturned;
}

- (BOOL)attemptErrorHandlingIfNeededForError:(NLWebServiceError *)webServiceError
{
    BOOL errorHandled = [super attemptErrorHandlingIfNeededForError:webServiceError];

    if(errorHandled == NO)
    {
        // (hds) If there was no error handling from superclass, then this class can check if it needs to handle the error or not.

        // (hds) Handling the MBaassTimeOut Error and taking action on it.
        if(webServiceError.error.domain == BTNetworkErrorDomain && webServiceError.error.code == BTNetworkErrorCodeVordelMbassTimeout)
        {
            if(_retryCount < kNLWebServiceTimeOutMaxRetryAttempts)
            {
                _retryCount++;
                [self resume];
                errorHandled = YES;
            }
        }
    }

    return errorHandled;
}

@end
