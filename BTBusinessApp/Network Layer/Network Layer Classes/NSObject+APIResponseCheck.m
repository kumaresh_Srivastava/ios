//
//  NSObject+APIResponseCheck.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 14/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"

@implementation NSObject (APIResponseCheck)

/**
 * Given an object, this function will safely put this into an array.
 * If already an array, it is simply returned, if nil, empty array will be given
 */
- (NSArray *)safeArrayFromJSONObject {
    if ( self == nil || [self isKindOfClass:[NSNull class]] ){
        return @[];
    }
    else if ( ![self isKindOfClass:[NSArray class]] ) {
        return @[self];
    }
    return (NSArray *) self;
}

- (NSString *)validAndNotEmptyStringObject
{
    NSString *validAndNotEmptyString = nil;

    if([self isKindOfClass:[NSString class]])
    {
        if(![[(NSString *)self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""])
        {
            return (NSString *)self;
        }
    }

    return validAndNotEmptyString;
}


- (BOOL)isValidVordelOauthTokenResponseObject
{
    BOOL isValid = NO;

    if ([self isKindOfClass:[NSDictionary class]])
    {
        if ([self valueForKey:@"access_token"] && ([[self valueForKey:@"access_token"] length] > 0))
        {
            isValid = YES;
        }
    }

    return isValid;
}


- (BOOL)isValidVordelAPIResponseObject
{
    BOOL isValid = NO;


    if([self isKindOfClass:[NSDictionary class]])
    {
        if([self valueForKey:@"Body"])
        {
            isValid = YES;
        } else if ([self valueForKey:@"MobileServiceResponse"])
        {
            //All responses should have 'Body' at the top level but MBaaS are violating their own rules...
            isValid = YES;
        }
    }

    return isValid;
}

- (BOOL)isValidSMSessionAPIResponseObject
{
    BOOL isValid = NO;

    if([self isKindOfClass:[NSDictionary class]])
    {
        if([self valueForKey:kNLResponseKeyForIsSuccess])
        {
            isValid = YES;
        }
    }
    
    return isValid;
}


@end
