//
//  NSObject+APIResponseCheck.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 14/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (APIResponseCheck)

- (NSArray *)safeArrayFromJSONObject;

- (NSString *)validAndNotEmptyStringObject;

- (BOOL)isValidVordelOauthTokenResponseObject;

- (BOOL)isValidVordelAPIResponseObject;

- (BOOL)isValidSMSessionAPIResponseObject;

@end
