//
//  NLWebServiceError.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 20/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NLWebServiceError.h"
#import "AppConstants.h"

@interface NLWebServiceError : NSObject {

}

@property (nonatomic, readonly) NSError *error;
@property (nonatomic, readonly) NLWebServiceError *sourceError;

- (id)initWithError:(NSError *)error andSourceError:(NLWebServiceError *)sourceError;

+ (NLWebServiceError *)webServiceNetworkErrorWithErrorDomain:(NSString *)networkErrorDomain andNetworkErrorCode:(BTNetworkErrorCode)networkErrorCode;

@end
