//
//  NLVordelWebService.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 20/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLWebService.h"

@interface NLVordelWebService : NLWebService {

}

@property (nonatomic, readonly) NSString *friendlyName;

- (instancetype)initWithMethod:(NSString *)method parameters:(NSString *)parameters andEndpointUrlString:(NSString *)endpointUrlString;

@end

