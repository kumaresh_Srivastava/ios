//
//  NLWebService.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 28/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppManager.h"

@class NLWebService;
@class AFHTTPSessionManager;
@class NLWebServiceError;


@interface NLWebService : NSObject {

    NSString *_httpMethodName;
    NSString *_urlEndPointString;
    NSString *_parameters;
    NLBaseURLType _baseURLType;
    NSInteger _maxTimeoutRetryAttempts;
    NSInteger _retryCount;
    NSTimeInterval _timeOutTimeInterval;

    NSURLSessionTask *_currentTask;
    AFHTTPSessionManager *_sessionManager;
}


// Final Methods


- (instancetype)initWithMethod:(NSString *)method parameters:(NSString *)parameters endpointUrlString:(NSString *)endpointUrlString andBaseURLType:(NLBaseURLType)baseURLType;


/* This method is called to start the web service. */
- (void)resume;

/* This method is called to cancel the web service. */
- (void)cancel;


/* This method is called to get sessionManager. Subclasses can override this method to allow custom creation of sessionManager. */
- (AFHTTPSessionManager *)sessionManager;

/* This method is called to get header fields. Subclasses can override this method by definately calling super first and then setting their own header fields. */
- (NSDictionary *)httpHeaderFields;

/* This method is declared in interface file for the visibility to subclasses and so that they can override it. This method should not be called explicitly by an external object. */
- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject;

/* This method is declared in interface file for the visibility to subclasses and so that they can override it. This method should not be called explicitly by an external object. */
- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError;


/* This method is declared in interface file for the visibility to subclasses and so that they can override it. This method should not be called explicitly by an external object. */
- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andError:(NSError *)error;


/* This method is declared in interface file for the visibility to subclasses and so that they can override it. This method should not be called explicitly by an external object. */
- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject;


/* This method is declared in interface file for the visibility to subclasses and so that they can override it. This method should not be called explicitly by an external object. */
- (BOOL)attemptErrorHandlingIfNeededForError:(NLWebServiceError *)webServiceError;


//Salman
//Override this method and return yes to anable caching for a particular webservice

- (NSString *)staticDataFileName;




@end
