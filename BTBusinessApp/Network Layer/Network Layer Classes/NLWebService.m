//
//  NLWebService.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 28/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLWebService.h"
#import "AFNetworking.h"
#import "NLConstants.h"
#import "AppConstants.h"
#import "NLWebServiceError.h"
#import "AppManager.h"


@interface NLWebService() {

}

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError;

- (NSString *)fileNameForParam:(NSString *)paramString;

@end

@implementation NLWebService


- (instancetype)initWithMethod:(NSString *)method parameters:(NSString *)parameters endpointUrlString:(NSString *)endpointUrlString andBaseURLType:(NLBaseURLType)baseURLType
{
    self = [super init];
    if(self)
    {
        _httpMethodName = [method copy];
        _parameters = [parameters copy];
        _urlEndPointString = [endpointUrlString copy];
        _baseURLType = baseURLType;
        _maxTimeoutRetryAttempts = kNLWebServiceTimeOutMaxRetryAttempts;
        _timeOutTimeInterval = kNLWebServiceRequestTimeOut;
    }
    return self;
}

- (void)dealloc
{
    [_sessionManager invalidateSessionCancelingTasks:YES];
    _sessionManager = nil;
}

#pragma mark - Public Methods


- (void)resume
{
    [self cancel];
    _currentTask = [self dataTask];

    if([AppManager isInternetConnectionAvailable])
    {
        if(_currentTask != nil)
        {
            DDLogInfo(@"Starting webservice with URL: %@", [[_currentTask currentRequest] URL]);
            [_currentTask resume];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{

                DDLogError(@"Unable to start webservice with URL: %@ because dataTask was not properly created.", [[_currentTask currentRequest] URL]);

                NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeGeneralNetworkError userInfo:nil];

                NLWebServiceError *webServiceError = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];

                [self handleFailureWithSessionDataTask:(NSURLSessionDataTask *)_currentTask andWebServiceError:webServiceError];

                [self cancel];
            });
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{

            DDLogError(@"Unable to start webservice with URL: %@ because internet connection is not there.", [[_currentTask currentRequest] URL]);

            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeNoInternet userInfo:nil];

            NLWebServiceError *webServiceError = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];

            [self handleFailureWithSessionDataTask:(NSURLSessionDataTask *)_currentTask andWebServiceError:webServiceError];

            [self cancel];
        });
    }
}

- (void)cancel
{
    if(_currentTask)
    {
        DDLogInfo(@"Cancelling webservice with URL: %@", [[_currentTask currentRequest] URL]);
        NSURLSessionTask *task = _currentTask;
        _currentTask = nil;
        [task cancel];
    }
}


#pragma mark - Helper Methods For Prepration Of The NSURLSessionTask

- (AFHTTPSessionManager *)sessionManager
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSString *baseURLString = [NSString stringWithFormat:@"https://%@", [AppManager baseURLStringFromBaseURLType:_baseURLType]];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseURLString] sessionConfiguration:configuration];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    [manager.requestSerializer setTimeoutInterval:_timeOutTimeInterval];

    return manager;
}

- (NSDictionary *)httpHeaderFields
{
    return nil;
}


#pragma mark - Helper Methods For Error Handling

- (BOOL)attemptErrorHandlingIfNeededForError:(NLWebServiceError *)webServiceError
{
    BOOL errorHandled = NO;

    if(webServiceError.error.domain == BTNetworkErrorDomain && webServiceError.error.code == BTNetworkErrorCodeHTTPTimeOut)
    {
        if(_retryCount < kNLWebServiceTimeOutMaxRetryAttempts)
        {
            _retryCount++;
            [self resume];
            errorHandled = YES;
        }
    }

    return errorHandled;
}



- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andError:(NSError *)error
{
    NLWebServiceError *webServiceError = nil;

    if(![AppManager isInternetConnectionAvailable])
    {
        NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeNoInternet userInfo:nil];

        webServiceError = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    }
    else if([error.domain isEqualToString:NSURLErrorDomain] && error.code == NSURLErrorTimedOut)
    {
        NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeHTTPTimeOut userInfo:nil];

        webServiceError = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    }

    return webServiceError;
}

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = nil;
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        NSDictionary *responseDict = (NSDictionary*)responseObject;
        if ([responseDict objectForKey:@"error"]) {
            NSString *errorDescription = [responseDict objectForKey:@"error"];
            NSInteger code = BTNetworkErrorCodeAPIErrorUnknown;
            if ([responseDict objectForKey:@"code"] && [[responseDict objectForKey:@"code"] isKindOfClass:[NSString class]]) {
                errorDescription = [NSString stringWithFormat:@"(%@) %@",[responseDict objectForKey:@"code"],errorDescription];
            } else {
                code = [[responseDict objectForKey:@"code"] integerValue];
            }
            NSDictionary<NSErrorUserInfoKey,id> *userinfo = @{NSLocalizedDescriptionKey:errorDescription};
            NSError * error = [NSError errorWithDomain:BTNetworkErrorDomain code:code userInfo:userinfo];
            errorToBeReturned = [[NLWebServiceError alloc] initWithError:error andSourceError:nil];
            DDLogError(@"Vordel error: %@ in response object for URL %@", errorDescription, [[task currentRequest] URL]);
        }
    }
    return errorToBeReturned;
}

#pragma mark - Private Helper Methods

- (NSURLSessionTask *)dataTask
{
    // (hd) Setting up AFHTTPSessionManager
    AFHTTPSessionManager *sessionManager = [self getSessionManagerForRequestTask];

    // (hd) Setting up header fields
    NSDictionary *headerFields = [self httpHeaderFields];
    if(headerFields)
    {
        for(NSString *key in headerFields)
        {
            [sessionManager.requestSerializer setValue:[headerFields valueForKey:key] forHTTPHeaderField:key];
        }
    }

    NSURLSessionTask *dataTask = [self taskWithSessionManager:sessionManager methodName:_httpMethodName urlEndPointString:_urlEndPointString parameters:_parameters];
    return dataTask;
}

- (AFHTTPSessionManager *)getSessionManagerForRequestTask
{
    if(_sessionManager == nil)
    {
        _sessionManager = [self sessionManager];
        
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
        
        _sessionManager.securityPolicy = policy;

        // (lp) Adding cetificates in security policy.
        _sessionManager.securityPolicy.pinnedCertificates = [AppManager getSecureCertificatesForPinning];
        
    }

    return _sessionManager;
}


- (NSURLSessionTask *)taskWithSessionManager:(AFHTTPSessionManager *)sessionManager methodName:(NSString *)methodName urlEndPointString:(NSString *)urlEndPointString parameters:(NSString *)parameters
{
    NSURLSessionTask *task = nil;

    //(Salman) Caching code

    BOOL needToCachResponse = NO;

    if(kIsDevMode){

        NSString *filePath = [self fileNameForParam:urlEndPointString];

        switch (kDataPoint) {

            case DataPointCache:
            {
                needToCachResponse = YES;
            }
                break;

            case DataPointStatic:
            {
                NSString *fileName = [self staticDataFileName];
                filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"json"];
                if(!filePath)
                    filePath = @"";//TO Avoid Crash in case of file not available
            }
                break;

            default:
                break;
        }

        if([[NSFileManager defaultManager] fileExistsAtPath:filePath]){

            NSError *error= NULL;
            NSData *data = [NSData dataWithContentsOfFile:filePath];
            id resultData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];

            [self handleSuccessWithSessionDataTask:nil andResponseObject:resultData];

            return task;
        }
    }


    if([methodName isEqualToString:@"GET"])
    {
        task = [sessionManager GET:urlEndPointString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

            if(task != self->_currentTask)
            {
                return ;
            }

            NLWebServiceError *webServiceError = [self processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

            // (hds) IMPORTANT: Following line of code is to simulate dummy error code while unit testing the error cases of your webservice code.
#ifdef kDummyUnitTestCaseCodeAllowed
            if([self dummyUnitTestCaseWebServiceError])
                webServiceError = [self dummyUnitTestCaseWebServiceError];
#endif

            if(!(([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && webServiceError.error.code == BTNetworkErrorCodeHTTPTimeOut) || ([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && webServiceError.error.code == BTNetworkErrorCodeVordelMbassTimeout)))
            {
                // Check if the error is not HTTP Time Out. If it is not, then make the retryCount variable back to 0.
                self->_retryCount = 0;
            }


            if(webServiceError)
            {
                BOOL neededErrorHandling = [self attemptErrorHandlingIfNeededForError:webServiceError];
                if(neededErrorHandling == NO)
                {
                    [self handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
                    [self cancel];
                }
            }
            else
            {
                [self handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
            }




            // (Salman) Saving response to disk
            if(needToCachResponse){

                [self saveResponse:responseObject forEndPointUrl:urlEndPointString];
            }


        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            //NSInteger errorCode = [[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
            
            //NSDictionary *userInfo = [error userInfo];
            
            if(task != self->_currentTask)
            {
                return ;
            }

            NLWebServiceError *webServiceError = [self processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andError:error];

            // (hds) IMPORTANT: Following line of code is to simulate dummy error code while unit testing the error cases of your webservice code.
#ifdef kDummyUnitTestCaseCodeAllowed
            if([self dummyUnitTestCaseWebServiceError])
                webServiceError = [self dummyUnitTestCaseWebServiceError];
#endif

            if(!(([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && webServiceError.error.code == BTNetworkErrorCodeHTTPTimeOut) || ([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && webServiceError.error.code == BTNetworkErrorCodeVordelMbassTimeout)))
            {
                // Check if the error is not HTTP Time Out. If it is not, then make the retryCount variable back to 0.
                self->_retryCount = 0;
            }


            BOOL neededErrorHandling = NO;

            if(webServiceError)
            {
                neededErrorHandling = [self attemptErrorHandlingIfNeededForError:webServiceError];
            }
            else
            {
                NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeGeneralNetworkError userInfo:nil];

                webServiceError = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
            }

            if(neededErrorHandling == NO)
            {
                [self handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
                [self cancel];
            }
        }];
    }
    else if([methodName isEqualToString:@"POST"])
    {
        task = [sessionManager POST:urlEndPointString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

            if(task != self->_currentTask)
            {
                return ;
            }

            NLWebServiceError *webServiceError = [self processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

            // (hds) IMPORTANT: Following line of code is to simulate dummy error code while unit testing the error cases of your webservice code.
#ifdef kDummyUnitTestCaseCodeAllowed
            if([self dummyUnitTestCaseWebServiceError])
                webServiceError = [self dummyUnitTestCaseWebServiceError];
#endif

            if(!(([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && webServiceError.error.code == BTNetworkErrorCodeHTTPTimeOut) || ([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && webServiceError.error.code == BTNetworkErrorCodeVordelMbassTimeout)))
            {
                // Check if the error is not HTTP Time Out. If it is not, then make the retryCount variable back to 0.
                self->_retryCount = 0;
            }

            if(webServiceError)
            {
                BOOL neededErrorHandling = [self attemptErrorHandlingIfNeededForError:webServiceError];
                if(neededErrorHandling == NO)
                {
                    [self handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
                    [self cancel];
                }
            }
            else
            {
                [self handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
            }

        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

            if(task != self->_currentTask)
            {
                return ;
            }

            NLWebServiceError *webServiceError = [self processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andError:error];

            // (hds) IMPORTANT: Following line of code is to simulate dummy error code while unit testing the error cases of your webservice code.
#ifdef kDummyUnitTestCaseCodeAllowed
            if([self dummyUnitTestCaseWebServiceError])
                webServiceError = [self dummyUnitTestCaseWebServiceError];
#endif

            if(!(([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && webServiceError.error.code == BTNetworkErrorCodeHTTPTimeOut) || ([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && webServiceError.error.code == BTNetworkErrorCodeVordelMbassTimeout)))
            {
                // Check if the error is not HTTP Time Out. If it is not, then make the retryCount variable back to 0.
                self->_retryCount = 0;
            }

            BOOL neededErrorHandling = NO;

            if(webServiceError)
            {
                neededErrorHandling = [self attemptErrorHandlingIfNeededForError:webServiceError];
            }
            else
            {
                NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeGeneralNetworkError userInfo:nil];

                webServiceError = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
            }

            if(neededErrorHandling == NO)
            {
                [self handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
                [self cancel];
            }
        }];
    }

    return task;
}


#pragma mark - Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    DDLogInfo(@"Webservice with URL: %@ finished.", [[task currentRequest] URL]);
}


- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    DDLogError(@"Webservice with URL: %@ failed with error code: %ld.", [[task currentRequest] URL], (long)[webServiceError.error code]);
    
    // If you are using name only, you can pass nil as properties.
    
    BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Webservice with URL: %@ failed with error code: %ld.", [[task currentRequest] URL], (long)[webServiceError.error code]]));
}





#pragma mark - Mock API Response Private Methods

- (NSString *)staticDataFileName
{
    return @"";
}


- (NSString *)fileNameForParam:(NSString *)paramString{

    NSString *documentDirectoryPath = [AppManager documentDirectoryPath];
    NSString *cacheDirectory = [documentDirectoryPath stringByAppendingPathComponent:kCacheDirectory];

    BOOL isDirectory = YES;

    if(![[NSFileManager defaultManager] fileExistsAtPath:cacheDirectory isDirectory:&isDirectory]){

        NSError *error = nil;
        BOOL success = [[NSFileManager defaultManager] createDirectoryAtPath:cacheDirectory
                                                 withIntermediateDirectories:NO
                                                                  attributes:nil
                                                                       error:&error];

        if(!success){

        }
    }


    paramString = [paramString stringByReplacingOccurrencesOfString:@"/" withString:@""];
    NSString *className = NSStringFromClass([self class]);
    NSString *fileName = [NSString stringWithFormat:@"%@_%@.json", className, paramString];
    NSString *filePath = [cacheDirectory stringByAppendingPathComponent:fileName];
    return filePath;
}



- (void)saveResponse:(id)response forEndPointUrl:(NSString *)urlString
{

    NSString *filePath = [self fileNameForParam:urlString];

    if(response && [response isKindOfClass:[NSDictionary class]]){

        NSError *error = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:response
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];

        if(![[NSFileManager defaultManager] fileExistsAtPath:filePath]){



            if([[NSFileManager defaultManager] createFileAtPath:filePath contents:nil attributes:nil]){

            }

        }

        [jsonData writeToFile:filePath atomically:YES];
    }

}





#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    return nil;
}






@end
