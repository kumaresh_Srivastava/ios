//
//  NLWebService+BackEndAPIErrorHandling.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 20/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLWebService+BackEndAPIErrorHandling.h"
#import "NLWebServiceError.h"
#import "NLConstants.h"
#import "AppConstants.h"

@implementation NLWebService (BackEndAPIErrorHandling)

- (NLWebServiceError *)errorFromBackendAPIAgreedResponseDic:(NSDictionary *)responseDic
{
    NLWebServiceError *errorToBeReturned = nil;

    if([responseDic valueForKey:kNLResponseKeyForCode])
    {
        NSInteger errorCodeFromResponse = [[responseDic valueForKey:kNLResponseKeyForCode] integerValue];

        BTNetworkErrorCode errorCode = BTNetworkErrorCodeAPIErrorUnknown;

        switch (errorCodeFromResponse)
        {
            case BTNetworkErrorCodeAPIUserProfileLocked:
            {
                errorCode = BTNetworkErrorCodeAPIUserProfileLocked;
                break;
            }

            case BTNetworkErrorCodeAPIInternalServiceError:
            {
                errorCode = BTNetworkErrorCodeAPIInternalServiceError;
                break;
            }

            case BTNetworkErrorCodeAPIServerError:
            {
                errorCode = BTNetworkErrorCodeAPIServerError;
                break;
            }

            case BTNetworkErrorCodeAPIProfileNotActivated:
            {
                errorCode = BTNetworkErrorCodeAPIProfileNotActivated;
                break;
            }

            case BTNetworkErrorCodeAPINoDataFound:
            {
                errorCode = BTNetworkErrorCodeAPINoDataFound;
                break;
            }
                
            case BTNetworkErrorCodeUnsupportedOrder:
            {
                errorCode = BTNetworkErrorCodeUnsupportedOrder;
                break;
            }

            default:
                break;
        }

        NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:errorCode userInfo:nil];
        
        errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    }

    return errorToBeReturned;
}

@end
