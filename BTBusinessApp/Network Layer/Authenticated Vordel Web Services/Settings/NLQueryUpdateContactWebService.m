//
//  NLQueryUpdateContactWebService.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/31/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLQueryUpdateContactWebService.h"
#import "NLVordelAuthenticationProtectedWebService+MbaasHeader.h"
#import "NSObject+APIResponseCheck.h"
#import "NLWebServiceError.h"
#import "NLConstants.h"
#import "NLWebService+BackEndAPIErrorHandling.h"

@implementation NLQueryUpdateContactWebService

- (instancetype)initWithUpdatedContactDetailsDictionary:(NSDictionary *)updatedDetailsDictionary
{
    NSDictionary *header = [self getMbaasHeader];
    
    NSDictionary *body = [NSDictionary dictionaryWithObject:updatedDetailsDictionary forKey:@"queryUpdateContact"];
    
    NSDictionary *payload = [NSDictionary dictionaryWithObjectsAndKeys:header, @"MobileServiceHeader", body, @"Body", nil];
    
    NSError *errorWhileCreatingJSONData = nil;
    NSData *mbaasHeader = [NSJSONSerialization dataWithJSONObject:payload options:0 error:&errorWhileCreatingJSONData];
    
    if (errorWhileCreatingJSONData)
    {
        DDLogError(@"Error while creating JSON data for queryUpdateContact");
    }
    
    NSString *jsonString = [[NSString alloc] initWithData:mbaasHeader encoding:4];
    
    NSString *endPointURLString = @"/managebusinessservice/queryupdatecontact";
    
    self = [super initWithMethod:@"POST" parameters:jsonString andEndpointUrlString:endPointURLString];
    if(self)
    {

    }
    return self;
    
}


#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *resultDic = [[[responseObject valueForKey:@"Body"] valueForKey:@"queryUpdateContactResponse"] valueForKey:kNLResponseKeyForResult];
    
    if ([resultDic valueForKey:@"Message"])
    {
        NSString *messageString = [resultDic valueForKey:@"Message"];
        if ([messageString isEqualToString:@"Changes saved"])
        {
            DDLogInfo(@"Successfully updated contact using webservice with URL: %@", [[task currentRequest] URL]);
            
            [self.queryUpdateContactWebServiceDelegate queryUpdateContactDetailsSuccesfullyFinishedWithQueryUpdateContactWebService:self];
        }
        else
        {
            NLWebServiceError *webServiceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeAPIErrorUnknown];
            
            DDLogError(@"Unknown error in response json object in webservice with URL: %@", [[task currentRequest] URL]);
            
            BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Unknown error in response json object in webservice with URL: %@", [[task currentRequest] URL]]));
            
            [self.queryUpdateContactWebServiceDelegate queryUpdateContactWebService:self failedToUpdateContactDetailsWithWebServiceError:webServiceError];
        }
    }
    else
    {
        NLWebServiceError *webServiceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponse];
        
        DDLogError(@"Invalid response json object in webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Invalid response json object in webservice with URL: %@", [[task currentRequest] URL]]));
        
        [self.queryUpdateContactWebServiceDelegate queryUpdateContactWebService:self failedToUpdateContactDetailsWithWebServiceError:webServiceError];
        
    }

}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.queryUpdateContactWebServiceDelegate queryUpdateContactWebService:self failedToUpdateContactDetailsWithWebServiceError:webServiceError];
}


#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (lpm) If there was no error from superclass, then this class can check for its level of errors.
        
        if([responseObject isValidVordelAPIResponseObject])
        {
            NSDictionary *body = [responseObject valueForKey:@"Body"];
            
            if ([body isKindOfClass:[NSDictionary class]])
            {
                if([body valueForKey:@"queryUpdateContactResponse"])
                {
                    NSDictionary *queryUpdateContactResponse = [body valueForKey:@"queryUpdateContactResponse"];
                    
                    if([queryUpdateContactResponse isValidSMSessionAPIResponseObject])
                    {
                        BOOL isSuccess = [[queryUpdateContactResponse valueForKey:kNLResponseKeyForIsSuccess] boolValue];
                        if(isSuccess)
                        {
                            NSDictionary *resultDic = [queryUpdateContactResponse valueForKey:kNLResponseKeyForResult];
                            if(![resultDic isKindOfClass:[NSDictionary class]])
                            {
                                errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
                                
                                DDLogError(@"'result' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]);
                                
                            }
                        }
                        else
                        {
                            NLWebServiceError *webServiceErrorFromBackendAPI = [self errorFromBackendAPIAgreedResponseDic:queryUpdateContactResponse];
                            if(webServiceErrorFromBackendAPI)
                            {
                                errorToBeReturned = webServiceErrorFromBackendAPI;
                                DDLogError(@"Error code: %ld in webservice with URL: %@", (long)webServiceErrorFromBackendAPI.error.code, [[task currentRequest] URL]);
                            }
                            else
                            {
                                errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
                                DDLogError(@"Error Code not found in response json object for webservice with URL: %@", [[task currentRequest] URL]);
                            }
                        }
                    }
                    else
                    {
                        errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
                        DDLogError(@"Invalid JSON Object in webservice with URL: %@", [[task currentRequest] URL]);
                    }
                }
                else
                {
                    errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
                    DDLogError(@"'queryUpdateContactResponse' element is missing in Body of response object for URL %@", [[task currentRequest] URL]);
                }
            }
            else
            {
                errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
                DDLogError(@"Invalid JSON Object in webservice with URL: %@", [[task currentRequest] URL]);
            }
        }
        else
        {
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
            DDLogError(@"Invalid Response Object for URL %@", [[task currentRequest] URL]);
        }
        
    }
    
    return errorToBeReturned;
}



#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;

    return nil;
}


@end
