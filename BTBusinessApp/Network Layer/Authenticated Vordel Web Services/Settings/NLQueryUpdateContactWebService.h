//
//  NLQueryUpdateContactWebService.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/31/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLVordelAuthenticationProtectedWebService.h"

@class NLQueryUpdateContactWebService;

@protocol NLQueryUpdateContactWebServiceDelegate <NLVordelAuthenticationProtectedWebServiceDelegate>

- (void)queryUpdateContactDetailsSuccesfullyFinishedWithQueryUpdateContactWebService:(NLQueryUpdateContactWebService *)webService;

- (void)queryUpdateContactWebService:(NLQueryUpdateContactWebService *)webService failedToUpdateContactDetailsWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLQueryUpdateContactWebService : NLVordelAuthenticationProtectedWebService {
    
}

@property (nonatomic, weak) id <NLQueryUpdateContactWebServiceDelegate> queryUpdateContactWebServiceDelegate;

- (instancetype)initWithUpdatedContactDetailsDictionary:(NSDictionary *)updatedDetailsDictionary;

@end
