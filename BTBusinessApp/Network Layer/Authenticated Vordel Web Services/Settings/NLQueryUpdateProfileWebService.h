//
//  NLQueryUpdateProfileWebService.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/31/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLVordelAuthenticationProtectedWebService.h"

@class NLQueryUpdateProfileWebService;

@protocol NLQueryUpdateProfileWebServiceDelegate <NLVordelAuthenticationProtectedWebServiceDelegate>

- (void)queryUpdateProfileDetailsSuccesfullyFinishedWithQueryUpdateProfileWebService:(NLQueryUpdateProfileWebService *)webService;

- (void)queryUpdateProfileWebService:(NLQueryUpdateProfileWebService *)webService failedToUpdateProfileDetailsWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface NLQueryUpdateProfileWebService : NLVordelAuthenticationProtectedWebService {
    
}

@property (nonatomic, weak) id <NLQueryUpdateProfileWebServiceDelegate> queryUpdateProfileWebServiceDelegate;

- (instancetype)initWithUpdatedProfileDetailsDictionary:(NSDictionary *)updatedProfileDictionary;

@end
