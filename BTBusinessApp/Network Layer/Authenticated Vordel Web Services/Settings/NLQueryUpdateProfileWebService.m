//
//  NLQueryUpdateProfileWebService.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/31/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLQueryUpdateProfileWebService.h"
#import "NLVordelAuthenticationProtectedWebService+MbaasHeader.h"
#import "NSObject+APIResponseCheck.h"
#import "NLWebServiceError.h"
#import "NLConstants.h"
#import "NLWebService+BackEndAPIErrorHandling.h"
#import "AppDelegate.h"
#import "CDApp.h"
#import "AppDelegateViewModel.h"

@implementation NLQueryUpdateProfileWebService

- (instancetype)initWithUpdatedProfileDetailsDictionary:(NSDictionary *)updatedProfileDictionary
{
    NSDictionary *header = [self getMbaasHeader];
    
    NSDictionary *body = [NSDictionary dictionaryWithObject:updatedProfileDictionary forKey:@"queryUpdateProfile"];
    
    NSDictionary *payload = [NSDictionary dictionaryWithObjectsAndKeys:header, @"MobileServiceHeader", body, @"Body", nil];
    
    NSError *errorWhileCreatingJSONData = nil;
    NSData *mbaasHeader = [NSJSONSerialization dataWithJSONObject:payload options:0 error:&errorWhileCreatingJSONData];
    
    if (errorWhileCreatingJSONData)
    {
        DDLogError(@"Error while creating JSON data for queryUpdateProfile");
    }
    
    NSString *jsonString = [[NSString alloc] initWithData:mbaasHeader encoding:4];
    
    NSString *endPointURLString = @"/managebusinessservice/queryupdateprofile";
    
    self = [super initWithMethod:@"POST" parameters:jsonString andEndpointUrlString:endPointURLString];
    if(self)
    {
        
    }
    return self;
    
}


- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
   
    //If intercepter is presented from Sign in screen then we have to use token from AppManager
    if([[NSUserDefaults standardUserDefaults] objectForKey:kTokenLoginIntercepter])
    {
        [finalHeaderFieldDic setObject:[NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] valueForKey:kTokenLoginIntercepter]] forKey:@"Authorization"];
    }
    else
    {
        [finalHeaderFieldDic setObject:[NSString stringWithFormat:@"Bearer %@", _authenticationToken.accessToken] forKey:@"Authorization"];
    }
    [finalHeaderFieldDic setObject:[AppDelegate sharedInstance].viewModel.app.btInstallationID forKey:@"udid"];
    
    return [finalHeaderFieldDic copy];
}



#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *resultDic = [[[responseObject valueForKey:@"Body"] valueForKey:@"queryUpdateProfileResponse"] valueForKey:kNLResponseKeyForResult];
    
    if ([resultDic valueForKey:@"Message"])
    {
        NSString *messageString = [resultDic valueForKey:@"Message"];
        if ([messageString isEqualToString:@"Changes saved"])
        {
            DDLogInfo(@"Successfully updated profile using webservice with URL: %@", [[task currentRequest] URL]);
            
            [self.queryUpdateProfileWebServiceDelegate queryUpdateProfileDetailsSuccesfullyFinishedWithQueryUpdateProfileWebService:self];
        }
        else
        {
            NLWebServiceError *webServiceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeAPIErrorUnknown];
            
            DDLogError(@"Unknown error in response json object in webservice with URL: %@", [[task currentRequest] URL]);
            
            BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Unknown error in response json object in webservice with URL: %@", [[task currentRequest] URL]]));
            
            [self.queryUpdateProfileWebServiceDelegate queryUpdateProfileWebService:self failedToUpdateProfileDetailsWithWebServiceError:webServiceError];
        }
    }
    else
    {
        NLWebServiceError *webServiceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponse];
        
        DDLogError(@"Invalid response json object in webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Invalid response json object in webservice with URL: %@", [[task currentRequest] URL]]));
        
        [self.queryUpdateProfileWebServiceDelegate queryUpdateProfileWebService:self failedToUpdateProfileDetailsWithWebServiceError:webServiceError];
        
    }
    
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.queryUpdateProfileWebServiceDelegate queryUpdateProfileWebService:self failedToUpdateProfileDetailsWithWebServiceError:webServiceError];
}


#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        
        // (lpm) If there was no error from superclass, then this class can check for its level of errors.
        
        if([responseObject isValidVordelAPIResponseObject])
        {
             NSDictionary *body = [responseObject valueForKey:@"Body"];
             
             if ([body isKindOfClass:[NSDictionary class]])
             {
                 if([body valueForKey:@"queryUpdateProfileResponse"])
                 {
                     NSDictionary *queryUpdateProfileResponse = [body valueForKey:@"queryUpdateProfileResponse"];
                     
                     if([queryUpdateProfileResponse isValidSMSessionAPIResponseObject])
                     {
                         BOOL isSuccess = [[queryUpdateProfileResponse valueForKey:kNLResponseKeyForIsSuccess] boolValue];
                         if(isSuccess)
                         {
                             NSDictionary *resultDic = [queryUpdateProfileResponse valueForKey:kNLResponseKeyForResult];
                             if(![resultDic isKindOfClass:[NSDictionary class]])
                             {
                                 errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
                                 
                                 DDLogError(@"'result' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]);
                                 
                             }
                         }
                         else
                         {
                             NLWebServiceError *webServiceErrorFromBackendAPI = [self errorFromBackendAPIAgreedResponseDic:queryUpdateProfileResponse];
                             if(webServiceErrorFromBackendAPI)
                             {
                                 errorToBeReturned = webServiceErrorFromBackendAPI;
                                 DDLogError(@"Error code: %ld in webservice with URL: %@", (long)webServiceErrorFromBackendAPI.error.code, [[task currentRequest] URL]);
                             }
                             else
                             {
                                 errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
                                 DDLogError(@"Error Code not found in response json object for webservice with URL: %@", [[task currentRequest] URL]);
                             }
                         }
                     }
                     else
                     {
                         errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
                         DDLogError(@"Invalid JSON Object in webservice with URL: %@", [[task currentRequest] URL]);
                     }
                 }
                 else
                 {
                     errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
                     DDLogError(@"'queryUpdateProfileResponse' element is missing in Body of response object for URL %@", [[task currentRequest] URL]);
                 }
             }
             else
             {
                 errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
                 DDLogError(@"Invalid JSON Object in webservice with URL: %@", [[task currentRequest] URL]);
             }
        }
        else
        {
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
            DDLogError(@"Invalid Response Object for URL %@", [[task currentRequest] URL]);
        }
    }
    
    return errorToBeReturned;
}



#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;
    
    return nil;
}

@end
