//
//  NLVordelAuthenticationProtectedWebService.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/15/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLVordelWebService.h"

@class NLVordelAuthenticationProtectedWebService;
@class BTAuthenticationToken;

@protocol NLVordelAuthenticationProtectedWebServiceDelegate <NSObject>

@end

@interface NLVordelAuthenticationProtectedWebService : NLVordelWebService {

    BTAuthenticationToken *_authenticationToken;
}

- (instancetype)initWithMethod:(NSString *)method parameters:(NSString *)parameters andEndpointUrlString:(NSString *)endpointUrlString;

@end
