//
//  NLVordelAuthenticationProtectedWebService+MbaasHeader.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 20/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLVordelAuthenticationProtectedWebService.h"

@interface NLVordelAuthenticationProtectedWebService (MbaasHeader)

- (NSDictionary *)getMbaasHeader;

@end
