//
//  NLVordelAuthenticationProtectedWebService+MbaasHeader.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 20/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLVordelAuthenticationProtectedWebService+MbaasHeader.h"

@implementation NLVordelAuthenticationProtectedWebService (MbaasHeader)

- (NSDictionary *)getMbaasHeader
{
    // Timestamp
    NSDate *now = [NSDate date];

    // Prefix
    NSDateFormatter *datePrefixFormat = [[NSDateFormatter alloc] init];
    [datePrefixFormat setDateFormat:@"yyyy-MM-dd"];
    [datePrefixFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];

    NSString *datePrefixString = [datePrefixFormat stringFromDate:now];

    // Suffix
    NSDateFormatter *dateSuffixFormat = [[NSDateFormatter alloc] init];
    [dateSuffixFormat setDateFormat:@"HH:mm:ss"];
    [dateSuffixFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    NSString *dateSuffixString = [dateSuffixFormat stringFromDate:now];

    // Assemble...
    NSString *finalDate = [NSString stringWithFormat:@"%@T%@", datePrefixString, dateSuffixString];
    
    // Version number
    NSString *currentVersion = [NSString stringWithFormat:@"v%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
    
    // UDID
    NSString *udid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];

    NSDictionary *header = [NSDictionary dictionaryWithObjectsAndKeys:@"BTBusinessApp", @"App_Name", @"Sync", @"Request_Type", currentVersion, @"ADN", finalDate, @"Timestamp", nil];
    return header;
}

@end
