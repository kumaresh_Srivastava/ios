//
//  NLVordelAuthenticationProtectedWebService.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/15/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLVordelAuthenticationProtectedWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "AFNetworking.h"
#import "NLConstants.h"
#import "BTAuthenticationToken.h"
#import "BTAuthenticationManager.h"
#import "NLWebServiceError.h"
#import "NLVordelWebServiceError.h"
#import "AppConstants.h"

@interface NLVordelAuthenticationProtectedWebService () {

    BOOL _hasAutoAuthenticationBeenAlreadyAttempted;
    NLWebServiceError *_webServiceErrorBeforeAutoAuthenticationAttempt;
}


@end

@implementation NLVordelAuthenticationProtectedWebService

- (instancetype)initWithMethod:(NSString *)method parameters:(NSString *)parameters andEndpointUrlString:(NSString *)endpointUrlString
{
    self = [super initWithMethod:method parameters:parameters andEndpointUrlString:endpointUrlString];

    if(self)
    {
        _authenticationToken = [[BTAuthenticationManager sharedManager] authenticationTokenForCurrentLoggedInUser];
    }
    return self;
}



#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (AFHTTPSessionManager *)sessionManager
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];

    NSString *baseURLString = [NSString stringWithFormat:@"https://%@", [AppManager baseURLStringFromBaseURLType:_baseURLType]];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseURLString] sessionConfiguration:configuration];

    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    [manager.requestSerializer setTimeoutInterval:_timeOutTimeInterval];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    // (hd) Because the API call expects the parameters in raw format, hence we are setting an empty query string serializer so that it does not do any serialization.
    [manager.requestSerializer setQueryStringSerializationWithBlock:^NSString * _Nonnull(NSURLRequest * _Nonnull request, id  _Nonnull parameters, NSError * _Nullable __autoreleasing * _Nullable error) {

        return parameters;
    }];

    return manager;
}

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];

    [finalHeaderFieldDic setObject:[NSString stringWithFormat:@"Bearer %@", _authenticationToken.accessToken] forKey:@"Authorization"];
    [finalHeaderFieldDic setObject:[AppDelegate sharedInstance].viewModel.app.btInstallationID forKey:@"udid"];

    return [finalHeaderFieldDic copy];
}



#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andError:(NSError *)error
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andError:error];

    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.


    }

    return errorToBeReturned;
}

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.


    }

    return errorToBeReturned;
}

- (BOOL)attemptErrorHandlingIfNeededForError:(NLWebServiceError *)webServiceError
{
    __block BOOL errorHandled = [super attemptErrorHandlingIfNeededForError:webServiceError];

    if(errorHandled == NO)
    {
        // (hds) If there was no error handling from superclass, then this class can check if it needs to handle the error or not.

        // (hds) Handling the Token Invalid and Refresh Token Invalid Error Cases and taking action on it.
        
        BOOL isTokenError = NO;
        
        if(webServiceError.error.domain == BTNetworkErrorDomain && (webServiceError.error.code == BTNetworkErrorCodeVordelTokenInvalidOrExpired || webServiceError.error.code == BTNetworkErrorCodeVordelRefreshInvalidOrExpired))
        {
            isTokenError = YES;
        }
        
        if (!isTokenError) {
            if ([webServiceError isKindOfClass:[NLVordelWebServiceError class]]) {
                NLVordelWebServiceError *vordelError = (NLVordelWebServiceError*)webServiceError;
                if ([vordelError.errorCode.lowercaseString isEqualToString:@"mb4-12"] || [vordelError.errorCode.lowercaseString isEqualToString:@"mb4-15"]) {
                    isTokenError = YES;
                }
            }
        }
        
        

//        if(webServiceError.error.domain == BTNetworkErrorDomain && (webServiceError.error.code == BTNetworkErrorCodeVordelTokenInvalidOrExpired || webServiceError.error.code == BTNetworkErrorCodeVordelRefreshInvalidOrExpired))
//        {
        if (isTokenError) {
            if(_hasAutoAuthenticationBeenAlreadyAttempted == NO)
            {
                errorHandled = YES;

                [[BTAuthenticationManager sharedManager] reAuthenticateCurrentlyLoggedInUserWithPreviousToken:_authenticationToken withAlreadyHaveFreshTokenHandler:^(BTAuthenticationToken *token) {

                    DDLogInfo(@"Handling the Vordel Authentication error by again attempting the API Call by using recently fetched new Vordel OAuth Token.");

                    _authenticationToken = token;
                    [self resume];

                } andFetchFreshTokenHandler:^{

                    DDLogInfo(@"Handling the Vordel Authentication error by attempting Auto Authentication Attempt.");

                    [self addObserverToNotificationForReAuthenticationCall];
                    _webServiceErrorBeforeAutoAuthenticationAttempt = webServiceError;
                    _hasAutoAuthenticationBeenAlreadyAttempted = YES;
                }];
            }
        }
    }

    return errorHandled;
}


#pragma mark - Private Helper Methods

- (void)addObserverToNotificationForReAuthenticationCall
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reAuthenticationAPICallSuccessfullyFinishedNotification:) name:kReAuthenticationApiCallSuccessfullyFinishedNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reAuthenticationAPICallFailedNotification:) name:kReAuthenticationApiCallFailedNotification object:nil];
}

- (void)removeObserverToNotificationForReAuthenticationCall
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReAuthenticationApiCallSuccessfullyFinishedNotification object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReAuthenticationApiCallFailedNotification object:nil];
}


#pragma mark - Notification Methods

- (void)reAuthenticationAPICallSuccessfullyFinishedNotification:(NSNotification *)notification
{
    [self removeObserverToNotificationForReAuthenticationCall];

    BTAuthenticationToken *token = [notification.userInfo valueForKey:@"token"];
    _authenticationToken = token;

    [self resume];
}

- (void)reAuthenticationAPICallFailedNotification:(NSNotification *)notification
{
    [self removeObserverToNotificationForReAuthenticationCall];

    NLWebServiceError *error = [notification.userInfo valueForKey:@"error"];

    NLWebServiceError *newErrorObject = [[NLWebServiceError alloc] initWithError:error.error andSourceError:_webServiceErrorBeforeAutoAuthenticationAttempt];
    [self handleFailureWithSessionDataTask:(NSURLSessionDataTask *)_currentTask andWebServiceError:newErrorObject];
    [self cancel];
}

@end
