//
//  NLQueryUserDetailsWebService.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/15/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLQueryUserDetailsWebService.h"
#import "BTAuthenticationManager.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "BTUser.h"
#import "AppConstants.h"
#import "NLVordelAuthenticationProtectedWebService+MbaasHeader.h"
#import "NLWebServiceError.h"
#import "NLWebService+BackEndAPIErrorHandling.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "AppManager.h"

@interface NLQueryUserDetailsWebService () {
    
    NSString *_accessToken;
    BOOL initiatedFromSignInPage;
}

@end

@implementation NLQueryUserDetailsWebService

- (instancetype)initWithUsername:(NSString *)username
{
    if (username == nil || [username isEqualToString:@""])
    {
        return nil;
    }
    
    NSDictionary *header = [self getMbaasHeader];
    
    initiatedFromSignInPage = NO;
    
    NSDictionary *body = [NSDictionary dictionaryWithObject:@"" forKey:@"queryUserDetailsV1"];
    //NSDictionary *body = [NSDictionary dictionaryWithObject:@"" forKey:@"queryUserDetails"];
    
    NSDictionary *payload = [NSDictionary dictionaryWithObjectsAndKeys:header, @"MobileServiceHeader", body, @"Body", nil];
    
    NSError *errorWhileCreatingJSONData = nil;
    NSData *mbaasHeader = [NSJSONSerialization dataWithJSONObject:payload options:0 error:&errorWhileCreatingJSONData];
    
    if(errorWhileCreatingJSONData)
    {
        DDLogError(@"Error while creating JSON data for queryUserDetails");
    }
    
    NSString *jsonString = [[NSString alloc] initWithData:mbaasHeader encoding:4];
    
    NSString *endPointURLString = @"/managebusinessservice/queryuserdetails";
    
    self = [super initWithMethod:@"POST" parameters:jsonString andEndpointUrlString:endPointURLString];
    
    if (self)
    {
        _username = [username copy];
    }
    return self;
}

- (instancetype)initWithUsername:(NSString *)username andAuthenticationToken:(NSString *)authenticationToken
{
    if (username == nil || [username isEqualToString:@""])
    {
        return nil;
    }
    
    if (authenticationToken == nil || [authenticationToken isEqualToString:@""])
    {
        return nil;
    }
    
    NSDictionary *header = [self getMbaasHeader];

    initiatedFromSignInPage = YES;
    
    NSMutableDictionary *queryUserDetailBody;
    
    if([AppManager isAppOpnedUsingDeepLinking])
    {
        queryUserDetailBody = [[NSMutableDictionary alloc] init];
        if([[AppManager sharedAppManager] getDeepLinkedActivationCode])
        {
         [queryUserDetailBody setObject:[[AppManager sharedAppManager] getDeepLinkedActivationCode] forKey:@"ActivationCode"];
        }
        else
        {
            [queryUserDetailBody setObject:@"" forKey:@"ActivationCode"];
        }
    }
    
    NSDictionary *body = [NSDictionary dictionaryWithObject:queryUserDetailBody?queryUserDetailBody:@"" forKey:@"queryUserDetailsV1"];
    //NSDictionary *body = [NSDictionary dictionaryWithObject:queryUserDetailBody?queryUserDetailBody:@"" forKey:@"queryUserDetails"];
    
    NSDictionary *payload = [NSDictionary dictionaryWithObjectsAndKeys:header, @"MobileServiceHeader", body, @"Body", nil];

    NSError *errorWhileCreatingJSONData = nil;
    NSData *mbaasHeader = [NSJSONSerialization dataWithJSONObject:payload options:0 error:&errorWhileCreatingJSONData];

    if(errorWhileCreatingJSONData)
    {
        DDLogError(@"Error while creating JSON data for queryUserDetails");
    }

    NSString *jsonString = [[NSString alloc] initWithData:mbaasHeader encoding:4];

    NSString *endPointURLString = @"/managebusinessservice/queryuserdetails";

    self = [super initWithMethod:@"POST" parameters:jsonString andEndpointUrlString:endPointURLString];
    
    if (self)
    {
        _username = [username copy];
        _accessToken = [authenticationToken copy];
    }
    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    if (initiatedFromSignInPage)
    {
        NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionary];
        
        [finalHeaderFieldDic setObject:[NSString stringWithFormat:@"Bearer %@", _accessToken] forKey:@"Authorization"];
        [finalHeaderFieldDic setObject:[AppDelegate sharedInstance].viewModel.app.btInstallationID forKey:@"udid"];
        return [finalHeaderFieldDic copy];
    }
    
    return [super httpHeaderFields];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];

    NSDictionary *resultDic = [[[responseObject valueForKey:@"Body"] valueForKey:@"queryUserDetailsResponseV1"] valueForKey:kNLResponseKeyForResult];
    //NSDictionary *resultDic = [[[responseObject valueForKey:@"Body"] valueForKey:@"queryUserDetailsResponse"] valueForKey:kNLResponseKeyForResult];

    BTUser *user = [[BTUser alloc] initWithUsername:self.username andResponseDictionaryFromQueryUserDetailsAPIResponse:resultDic];

    //Order is integer time in case of single interceptor
    if([[resultDic valueForKey:@"Order"] isKindOfClass:[NSNumber class]]){
        
        _order = [NSString stringWithFormat:@"%d", [[resultDic valueForKey:@"Order"] intValue]] ;
    }
    else if([[resultDic valueForKey:@"Order"] isKindOfClass:[NSString class]]){
        
        _order = [resultDic valueForKey:@"Order"];
    }
    
    _interfaces = [resultDic valueForKey:@"Interfaces"];

    if(user)
    {
        DDLogInfo(@"Successfully fetched user data for username '%@' using webservice with URL: %@", _username, [[task currentRequest] URL]);

        [self.queryUserDetailsWebServiceDelegate getUserWebService:self successfullyFetchedUserData:user];
    }
    else
    {
        NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
        NLWebServiceError *webServiceError = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];

        DDLogError(@"Unable to create BTUser model object using response json object in webservice with URL: %@", [[task currentRequest] URL]);
        
        if (!initiatedFromSignInPage)
        {
            BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Unable to create BTUser model object using response json object in webservice with URL: %@", [[task currentRequest] URL]]));
        }

        [self.queryUserDetailsWebServiceDelegate getUserWebService:self failedToFetchUserDataWithWebServiceError:webServiceError];
    }
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    if (initiatedFromSignInPage)
    {
        DDLogError(@"Webservice with URL: %@ failed with error code: %ld.", [[task currentRequest] URL], (long)[webServiceError.error code]);
    }
    else
    {
        [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    }

    [self.queryUserDetailsWebServiceDelegate getUserWebService:self failedToFetchUserDataWithWebServiceError:webServiceError];
}





#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.

        if([responseObject isValidVordelAPIResponseObject])
        {
            NSDictionary *body = [responseObject valueForKey:@"Body"];
            
            if([body valueForKey:@"queryUserDetailsResponseV1"])
            //if([body valueForKey:@"queryUserDetailsResponse"])
            {
                NSDictionary *queryUserDetailsResponse = [body valueForKey:@"queryUserDetailsResponseV1"];
                //NSDictionary *queryUserDetailsResponse = [body valueForKey:@"queryUserDetailsResponse"];

                if([queryUserDetailsResponse isValidSMSessionAPIResponseObject])
                {
                    BOOL isSuccess = [[queryUserDetailsResponse valueForKey:kNLResponseKeyForIsSuccess] boolValue];
                    if(isSuccess)
                    {
                        NSDictionary *resultDic = [queryUserDetailsResponse valueForKey:kNLResponseKeyForResult];
                        if(![resultDic isKindOfClass:[NSDictionary class]])
                        {
                            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
                            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];

                            DDLogError(@"'result' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]);
                            
                        }
                    }
                    else
                    {
                        NLWebServiceError *webServiceErrorFromBackendAPI = [self errorFromBackendAPIAgreedResponseDic:queryUserDetailsResponse];
                        if(webServiceErrorFromBackendAPI)
                        {
                            BTNetworkErrorCode errorCode = webServiceErrorFromBackendAPI.error.code;
                            if(webServiceErrorFromBackendAPI.error.code == BTNetworkErrorCodeAPIErrorUnknown)
                            {
                                switch([[queryUserDetailsResponse valueForKey:kNLResponseKeyForCode] integerValue])
                                {
                                    case BTNetworkErrorCodeIncorrectActivationCode:
                                    {
                                        errorCode = BTNetworkErrorCodeIncorrectActivationCode;
                                        break;
                                    }
                                    case BTNetworkErrorCodeEmailNotVerified:
                                    {
                                        errorCode = BTNetworkErrorCodeEmailNotVerified;
                                        break;
                                    }
                                    case BTNetworkErrorCodeEmailAlreadyVerified:
                                    {
                                        errorCode = BTNetworkErrorCodeEmailAlreadyVerified;
                                        break;
                                    }
                                    default:
                                    {
                                        errorCode = webServiceErrorFromBackendAPI.error.code;
                                        break;
                                    }
                                }
                                
                            }
                            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:errorCode userInfo:nil];
                            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
                            
                            DDLogError(@"Error code: %ld in webservice with URL: %@", (long)webServiceErrorFromBackendAPI.error.code, [[task currentRequest] URL]);
                            
                        }
                        else
                        {
                            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
                            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
                            DDLogError(@"Error Code not found in response json object for webservice with URL: %@", [[task currentRequest] URL]);
                            
                        }
                    }
                }
                else
                {
                    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
                    errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
                    DDLogError(@"Invalid JSON Object in webservice with URL: %@", [[task currentRequest] URL]);
                }
            }
            else
            {
                NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
                errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
                DDLogError(@"'queryUserDetailsResponse' element is missing in Body of response object for URL %@", [[task currentRequest] URL]);
            }
        }
        else
        {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];

            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
            DDLogError(@"Invalid Response Object for URL %@", [[task currentRequest] URL]);
        }
    }

    return errorToBeReturned;
}




#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    
//    NLWebServiceError *errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeVordelTokenInvalidOrExpired];
//
//    return errorToBeReturned;

    return nil;
}





@end
