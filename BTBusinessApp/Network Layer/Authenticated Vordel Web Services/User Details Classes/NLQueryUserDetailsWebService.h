//
//  NLQueryUserDetailsWebService.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/15/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLVordelAuthenticationProtectedWebService.h"

@class NLQueryUserDetailsWebService;
@class BTUser;
@class NLWebServiceError;

@protocol NLQueryUserDetailsWebServiceDelegate <NLVordelAuthenticationProtectedWebServiceDelegate>

- (void)getUserWebService:(NLQueryUserDetailsWebService *)webService successfullyFetchedUserData:(BTUser *)user;

- (void)getUserWebService:(NLQueryUserDetailsWebService *)webService failedToFetchUserDataWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLQueryUserDetailsWebService : NLVordelAuthenticationProtectedWebService {

}

@property (nonatomic, readonly) NSString *username;
@property (nonatomic, readonly) NSString *order;
@property (nonatomic, readonly) NSString *interfaces;

@property (nonatomic, weak) id <NLQueryUserDetailsWebServiceDelegate> queryUserDetailsWebServiceDelegate;

- (instancetype)initWithUsername:(NSString *)username;
- (instancetype)initWithUsername:(NSString *)username andAuthenticationToken:(NSString *)authenticationToken;

@end
