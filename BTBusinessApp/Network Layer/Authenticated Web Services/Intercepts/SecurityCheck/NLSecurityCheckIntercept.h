//
//  NLSecurityCheckIntercept.h
//  BTBusinessApp
//
//  Created by vectoscalar on 09/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "BTSecurityCheck.h"

@class  NLSecurityCheckIntercept;
@class NLWebServiceError;

@protocol NLSecurityCheckInterceptDelegate <NSObject>

- (void)getSecurityCheckWebService:(NLSecurityCheckIntercept *)webService successfullyFetchedSecurityCheckData:(BTSecurityCheck *)securityCheck;

- (void)getSecurityCheckWebService:(NLSecurityCheckIntercept *)webService failedTogetSecurityCheckDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end


@interface NLSecurityCheckIntercept : NLAuthenticationProtectedWebService

@property (assign,nonatomic) id<NLSecurityCheckInterceptDelegate> getSecurityCheckIntercepterDelegate;


- (instancetype)initWithSecurityCheckParamsDictionary:(NSDictionary *)paramsDict;


@end
