//
//  NLSecurityCheckIntercept.m
//  BTBusinessApp
//
//  Created by vectoscalar on 09/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLSecurityCheckIntercept.h"

#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "BTAsset.h"
#import "AppConstants.h"
#import "BTSMSession.h"
#import "BTAuthenticationManager.h"
#import "NLWebServiceError.h"
#import "AFHTTPSessionManager.h"
@implementation NLSecurityCheckIntercept

- (instancetype)initWithSecurityCheckParamsDictionary:(NSDictionary *)paramsDict
{
    NSError *errorWhileCreatingJSONData = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:paramsDict options:0 error:&errorWhileCreatingJSONData];
    
    if(errorWhileCreatingJSONData)
    {
        DDLogError(@"Failed to create JSON data for SecurityCheckParamsDictionary with error--%@",errorWhileCreatingJSONData.localizedDescription);
    }
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:4];
    
    NSString *endPointURLString = [NSString stringWithFormat:@"/Account/api/v4/ValidateSecurityDetails"];
    self = [super initWithMethod:@"POST" parameters:jsonString andEndpointUrlString:endPointURLString];
    if(self)
    {
        
    }
    return self;
}


#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask
- (AFHTTPSessionManager *)sessionManager
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSString *baseURLString = [NSString stringWithFormat:@"https://%@", [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel]];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseURLString] sessionConfiguration:configuration];
    
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    [manager.requestSerializer setTimeoutInterval:_timeOutTimeInterval];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // (hd) Because the API call expects the parameters in raw format, hence we are setting an empty query string serializer so that it does not do any serialization.
    [manager.requestSerializer setQueryStringSerializationWithBlock:^NSString * _Nonnull(NSURLRequest * _Nonnull request, id  _Nonnull parameters, NSError * _Nullable __autoreleasing * _Nullable error) {
        
        return parameters;
    }];
    
    return manager;
}


- (NSDictionary *)httpHeaderFields
{
    
    if (kDataPoint == DataPointStatic) {
        return [NSMutableDictionary dictionary];
    }
    else
    {
        NSDictionary *headerFieldsFromSuperClasses = nil;
        NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
        
        //Find smsession based on the entery point of the intercepter
        NSString *smsessionStr;
        NSString *groupKey;
        if([[NSUserDefaults standardUserDefaults] objectForKey:kSMSessionLoginIntercepter])
        {
            smsessionStr = [[NSUserDefaults standardUserDefaults] valueForKey:kSMSessionLoginIntercepter];
            groupKey = [[NSUserDefaults standardUserDefaults] valueForKey:kGroupKeyLoginIntercepter];
        }
        else
        {
            smsessionStr = _smSession.smsessionString;
            groupKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
        }
        
        
        NSDictionary *cookieProperties = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel], NSHTTPCookieDomain,
                                          @"SMSESSION", NSHTTPCookieName,
                                          @"\\", NSHTTPCookiePath,
                                          smsessionStr, NSHTTPCookieValue,
                                          nil];
        NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
        NSArray *arrayOfCookies = [NSArray arrayWithObject:cookie];
        NSDictionary *cookieHeaders = [NSHTTPCookie requestHeaderFieldsWithCookies:arrayOfCookies];
        
        for(NSString *key in cookieHeaders)
        {
            [finalHeaderFieldDic setValue:[cookieHeaders valueForKey:key] forKey:key];
        }
        
        
        [finalHeaderFieldDic setValue:groupKey forKey:@"_authKey"];
        
        return [finalHeaderFieldDic copy];
    }
    
    
}


#pragma mark - NLWebService Response Handling Private Methods


- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
    DDLogInfo(@"Successfully fetched check intercepter data using webservice with URL: %@", [[task currentRequest] URL]);
    
    BTSecurityCheck *securityCheck = [[BTSecurityCheck alloc] initWithResponseDictionaryFromSecurityCheckAPIResponse:resultDic];
    
    [self.getSecurityCheckIntercepterDelegate getSecurityCheckWebService:self successfullyFetchedSecurityCheckData:securityCheck];
}


- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.getSecurityCheckIntercepterDelegate  getSecurityCheckWebService:self failedTogetSecurityCheckDataWithWebServiceError:webServiceError];
}



#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (RLM) If there was no error from superclass, then this class can check for its level of errors.
        NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
        if(![resultDic isKindOfClass:[NSDictionary class]])
        {
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
            
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }
    
    return errorToBeReturned;
}

#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;
    
    return nil;
}

@end
