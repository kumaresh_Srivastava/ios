//
//  NLHelpAndSupportWebService.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 09/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLHelpAndSupportWebService.h"
#import "CDApp.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDUser.h"
#import "AppConstants.h"
#import "CDCug+CoreDataClass.h"
#import "NLConstants.h"
#import "NLWebServiceError.h"
#import "BTSpecificCategory.h"
#import "BTGeneralCategory.h"
#import "AppManager.h"

@implementation NLHelpAndSupportWebService

- (instancetype)initWithDateText:(NSString *)dateText {

    NSString *endPointURLString = nil;

    endPointURLString = [NSString stringWithFormat:@"/account/api/LiveChat/Slots/%@",dateText];

    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self) {

    }
    return self;
}



#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields {

    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];

    NSString *appInstallationKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:appInstallationKey forKey:@"_authKey"];

    return [finalHeaderFieldDic copy];
}


#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject {

    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];

    NSDictionary *resultDict = [responseObject valueForKey:kNLResponseKeyForResult];
    if(resultDict) {

        NSMutableArray *specificCategoryArray = [NSMutableArray array];
        NSMutableArray *generalCategoryArray =  [NSMutableArray array];
        NSString *ctTime = [resultDict valueForKey:@"CurrentTime"];

        for (NSDictionary *dict in [resultDict valueForKey:@"Specific"]) {
            BTSpecificCategory *specificCategory = [[BTSpecificCategory alloc] initWithResponseDictionaryFromHelpAndSupportAPIResponse:dict];
            if(specificCategory)
                [specificCategoryArray addObject:specificCategory];
        }

        for (NSDictionary *dict in [resultDict valueForKey:@"General"]) {

            BTGeneralCategory *generalCategory = [[BTGeneralCategory alloc] initWithResponseDictionaryFromHelpAndSupportAPIResponse:dict];
            if(generalCategory)
                [generalCategoryArray addObject:generalCategory];

        }

        [self.helpAndSupportWebServiceDelegate getHelpAndSupportWebService:self successfullyFetchedHelpAndSupportSpecificCategory:specificCategoryArray generalCategory:generalCategoryArray andCurrentTime:ctTime];

    }
    else {

        NLWebServiceError *errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];

        DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]]));
        
        [self.helpAndSupportWebServiceDelegate getHelpAndSupportWebService:self failedToFetchHelpAndSupportWebServiceWuthError:errorToBeReturned];

    }


}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError {

    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    [self.helpAndSupportWebServiceDelegate getHelpAndSupportWebService:self failedToFetchHelpAndSupportWebServiceWuthError:webServiceError];
}



#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject {

    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil) {

        NSDictionary *resultDict = [responseObject valueForKey:kNLResponseKeyForResult];
        if(![resultDict isKindOfClass:[NSDictionary class]]) {

            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];

            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
        }
    }

    return errorToBeReturned;
}


//- (void)resume
//{
//    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"HelpAndSupport" ofType:@"json"];
//    NSData *data = [NSData dataWithContentsOfFile:filePath];
//    id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
//
//    [self processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:nil andResponseObject:json];
//    [self handleSuccessWithSessionDataTask:nil andResponseObject:json];
//}

@end
