//
//  NLHelpAndSupportWebService.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 09/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"


@class NLHelpAndSupportWebService;
@class NLWebServiceError;

@protocol NLHelpAndSupportWebServiceDelegate

- (void)getHelpAndSupportWebService:(NLHelpAndSupportWebService *)webService successfullyFetchedHelpAndSupportSpecificCategory:(NSArray *)specificCategory generalCategory:(NSArray *)generalCategory andCurrentTime:(NSString *)currentTime;

- (void)getHelpAndSupportWebService:(NLHelpAndSupportWebService *)webService failedToFetchHelpAndSupportWebServiceWuthError:(NLWebServiceError *)webServiceError;

@end


@interface NLHelpAndSupportWebService : NLAuthenticationProtectedWebService

@property (nonatomic, weak) id<NLHelpAndSupportWebServiceDelegate> helpAndSupportWebServiceDelegate;

- (instancetype)initWithDateText:(NSString *)dateText;

@end
