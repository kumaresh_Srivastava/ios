//
//  NLGetBillSummaryAndChargesWebService.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLGetBillSummaryAndChargesWebService.h"

#import "NSObject+APIResponseCheck.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NLConstants.h"
#import "AppConstants.h"
#import "BTBill.h"
#import "BTBillCharges.h"
#import "BTProductCharge.h"
#import "NLWebServiceError.h"

@interface NLGetBillSummaryAndChargesWebService ()

{
    NSString *_BAC;
}

@end

@implementation NLGetBillSummaryAndChargesWebService

- (instancetype)initWithEndPointURL:(NSString *)endPointURL andBAC:(NSString *)BAC
{
    NSString *endPointURLString = [NSString stringWithFormat:@"%@/summary", endPointURL];
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    
    if(self)
    {
        _BAC = BAC;
    }
    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    NSString *groupKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:groupKey forKey:@"_authKey"];
    
    return [finalHeaderFieldDic copy];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    
    //TODO: (LP) Write DDLogInfo for successfull api call.
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
   
    BTBill *bill = nil;
    BTBillCharges *billCharges = nil;
    NSMutableArray *arrayOfProductCharges = [NSMutableArray array];
    
    NSDictionary *billSummaryDic = [resultDic valueForKey:@"BillSummary"];
    NSDictionary *billChargesDic = [resultDic valueForKey:@"BillCharges"];
    NSDictionary *productsDic = [resultDic valueForKey:@"Products"];
    
    if ([billSummaryDic isKindOfClass:[NSDictionary class]]) {
        bill = [[BTBill alloc] initBillWithBillSummaryAndChargesResponse:billSummaryDic];
    }
    
    if ([billChargesDic isKindOfClass:[NSDictionary class]]) {
        billCharges = [[BTBillCharges alloc] initBillChargesWithResponse:billChargesDic];
    }
    
    if ([productsDic isKindOfClass:[NSArray class]]) {
        for (NSDictionary *productChargeDic in productsDic) {
            BTProductCharge *productCharge = [[BTProductCharge alloc] initProductChargeWithData:productChargeDic];
            [arrayOfProductCharges addObject:productCharge];
        }
    }
    
    if(bill || billCharges || [arrayOfProductCharges count]>0)
    {
           DDLogInfo(@"Successfully fetched bill charges and summary with URL : %@", [[task currentRequest] URL]);
        
        [self.getBillSummaryAndChargesWebServiceDelegate getBillSummaryAndChargesWebService:self successfullyFetchedBillSummaryAndChargesDataWithBillSummary:bill billCharges:billCharges andProductCharges:arrayOfProductCharges];
    }
    else
    {
        NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
        NLWebServiceError *webServiceError = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
        
        DDLogError(@"Unable to create BTUser model object using response json object in webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Unable to create BTUser model object using response json object in webservice with URL: %@", [[task currentRequest] URL]]));
        
        [self.getBillSummaryAndChargesWebServiceDelegate getBillSummaryAndChargesWebService:self failedToFetchPaymentHistoryDataWithWebServiceError:webServiceError];
    }
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
     [self.getBillSummaryAndChargesWebServiceDelegate getBillSummaryAndChargesWebService:self failedToFetchPaymentHistoryDataWithWebServiceError:webServiceError];

}



#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        NSDictionary *resultDict = [responseObject valueForKey:kNLResponseKeyForResult];
        if([resultDict isKindOfClass:[NSDictionary class]])
        {
            // (RLM) Do Nothing Here.
        }
        else if ([responseObject objectForKey:@"code"]) {
            NSInteger code = [[responseObject objectForKey:@"code"] integerValue];
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:code userInfo:nil];
            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
        }
        else
        {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
            
            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
            
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }

    }
    
    return errorToBeReturned;
    
}

#pragma mark - Private helper method

- (void)fetchOfflineData
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"billDetailsDummyResponse1" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    
    NSError *error;
    
    NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if([responseObject isValidSMSessionAPIResponseObject])
    {
        BOOL isSuccess = [[responseObject valueForKey:kNLResponseKeyForIsSuccess] boolValue];
        
        if(isSuccess)
        {
            NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
            if([resultDic isKindOfClass:[NSDictionary class]])
            {
                BTBill *bill = nil;
                BTBillCharges *billCharges = nil;
                NSMutableArray *arrayOfProductCharges = [NSMutableArray array];
                
                NSDictionary *billSummaryDic = [resultDic valueForKey:@"BillSummary"];
                NSDictionary *billChargesDic = [resultDic valueForKey:@"BillCharges"];
                NSDictionary *productsDic = [resultDic valueForKey:@"Products"];
                
                if ([billSummaryDic isKindOfClass:[NSDictionary class]]) {
                    bill = [[BTBill alloc] initBillWithBillSummaryAndChargesResponse:billSummaryDic];
                }
                
                if ([billChargesDic isKindOfClass:[NSDictionary class]]) {
                    billCharges = [[BTBillCharges alloc] initBillChargesWithResponse:billChargesDic];
                }
                
                if ([productsDic isKindOfClass:[NSArray class]]) {
                    for (NSDictionary *productChargeDic in productsDic) {
                        BTProductCharge *productCharge = [[BTProductCharge alloc] initProductChargeWithData:productChargeDic];
                        [arrayOfProductCharges addObject:productCharge];
                    }
                }
                
                [self.getBillSummaryAndChargesWebServiceDelegate getBillSummaryAndChargesWebService:self successfullyFetchedBillSummaryAndChargesDataWithBillSummary:bill billCharges:billCharges andProductCharges:arrayOfProductCharges];
                
            }
        }
        
    }
    
}

#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;
    
    return nil;
}

@end
