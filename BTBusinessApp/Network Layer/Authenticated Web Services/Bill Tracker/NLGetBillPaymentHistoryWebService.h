//
//  NLGetBillPaymentHistoryWebService.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 03/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "NLAPIGEEAuthenticatedWebService.h"

@class NLGetBillPaymentHistoryWebService;
@class NLWebServiceError;
@protocol NLGetBillPaymentHistoryWebServiceDelegate 

- (void)getBillPaymentHistoryWebService:(NLGetBillPaymentHistoryWebService *)webService successfullyFetchedPaymentHistoryDataWithAwaitingPayments:(NSArray *)arrayOfAwaitingPayments andRecentlyPaid:(NSArray *)arrayOfRecentlyPaid;

- (void)getBillPaymentHistoryWebService:(NLGetBillPaymentHistoryWebService *)webService failedToFetchPaymentHistoryDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface NLGetBillPaymentHistoryWebService : NLAPIGEEAuthenticatedWebService {
    
}

@property (nonatomic, weak) id <NLGetBillPaymentHistoryWebServiceDelegate> getBillPaymentHistoryWebServiceDelegate;

@end
