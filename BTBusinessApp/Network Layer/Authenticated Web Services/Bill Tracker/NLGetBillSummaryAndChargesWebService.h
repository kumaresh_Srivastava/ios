//
//  NLGetBillSummaryAndChargesWebService.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "NLAPIGEEAuthenticatedWebService.h"

@class NLGetBillSummaryAndChargesWebService;
@class BTBill;
@class BTBillCharges;
@class NLWebServiceError;

@protocol NLGetBillSummaryAndChargesWebServiceDelegate 

- (void)getBillSummaryAndChargesWebService:(NLGetBillSummaryAndChargesWebService *)webService successfullyFetchedBillSummaryAndChargesDataWithBillSummary:(BTBill *)billSummary billCharges:(BTBillCharges *)billCharges andProductCharges:(NSArray *)arrayOfProductCharges;


- (void)getBillSummaryAndChargesWebService:(NLGetBillSummaryAndChargesWebService *)webService failedToFetchPaymentHistoryDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface NLGetBillSummaryAndChargesWebService : NLAPIGEEAuthenticatedWebService {
    
}


@property (nonatomic, weak) id <NLGetBillSummaryAndChargesWebServiceDelegate> getBillSummaryAndChargesWebServiceDelegate;

- (instancetype)initWithEndPointURL:(NSString *)endPointURL andBAC:(NSString *)BAC;
- (void)fetchOfflineData;

@end
