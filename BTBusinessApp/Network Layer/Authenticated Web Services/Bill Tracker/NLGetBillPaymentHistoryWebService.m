//
//  NLGetBillPaymentHistoryWebService.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 03/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLGetBillPaymentHistoryWebService.h"

#import "NSObject+APIResponseCheck.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NLConstants.h"
#import "AppConstants.h"
#import "BTBill.h"
#import "NLWebServiceError.h"
#import "NLWebService+BackEndAPIErrorHandling.h"

@implementation NLGetBillPaymentHistoryWebService

- (instancetype)init
{
    NSString *endPointURLString = @"/bt-business-auth/v1/bills/payment-history";
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];

    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    NSString *groupKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:groupKey forKey:@"_authKey"];
    
    return [finalHeaderFieldDic copy];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
    
     DDLogInfo(@"Successfully fetched bill payment history data using webservice with URL: %@", [[task currentRequest] URL]);
    
    NSMutableArray *arrayOfAwaitingPayments = [NSMutableArray array];
    NSMutableArray *arrayOfRecentlyPaid = [NSMutableArray array];
    
    NSArray *awaitingPayments = [resultDic valueForKey:@"AwaitingPayments"];
    
    for (NSDictionary *billData in awaitingPayments) {
        BTBill *bill = [[BTBill alloc] initBillWithBillPaymentHistoryResponse:billData];
        [arrayOfAwaitingPayments addObject:bill];
    }
    
    NSArray *recentlyPaid = [resultDic valueForKey:@"RecentlyPaid"];
    
    for (NSDictionary *billData in recentlyPaid) {
        BTBill *bill = [[BTBill alloc] initBillWithBillPaymentHistoryResponse:billData];
        [arrayOfRecentlyPaid addObject:bill];
    }
    
    [self.getBillPaymentHistoryWebServiceDelegate getBillPaymentHistoryWebService:self successfullyFetchedPaymentHistoryDataWithAwaitingPayments:arrayOfAwaitingPayments andRecentlyPaid:arrayOfRecentlyPaid];        
}



- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    if (webServiceError.error.code == BTNetworkErrorCodeAPINoDataFound) {
        [self.getBillPaymentHistoryWebServiceDelegate getBillPaymentHistoryWebService:self successfullyFetchedPaymentHistoryDataWithAwaitingPayments:@[] andRecentlyPaid:@[]];
    } else {
        [self.getBillPaymentHistoryWebServiceDelegate getBillPaymentHistoryWebService:self failedToFetchPaymentHistoryDataWithWebServiceError:webServiceError];
    }
}

#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        NSDictionary *resultDict = [responseObject valueForKey:kNLResponseKeyForResult];
        if([resultDict isKindOfClass:[NSDictionary class]])
        {
            // (RLM) Do Nothing Here.
        }
        else if([responseObject objectForKey:kNLResponseKeyForErrorMessage]) {
            if([[responseObject valueForKey:kNLResponseKeyForErrorMessage] isEqualToString:@"No data found"]) {
                [self.getBillPaymentHistoryWebServiceDelegate getBillPaymentHistoryWebService:self successfullyFetchedPaymentHistoryDataWithAwaitingPayments:@[] andRecentlyPaid:@[]];
            }
        }
        else {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
            
            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
            
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }
    
    return errorToBeReturned;
}


#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;
    
    return nil;
}


@end
