//
//  NLGetRefKeyWebService.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 13/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"

@class NLGetRefKeyWebService;
@class NLWebServiceError;
@protocol NLGetRefKeyWebServiceDelegate

- (void)getGetRefKeyWebService:(NLGetRefKeyWebService *)webService successfullyFetchedRefKey:(NSString *)refKey;

- (void)getRefKeyWebService:(NLGetRefKeyWebService *)webService failedToFetchRefKeyWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface NLGetRefKeyWebService : NLAuthenticationProtectedWebService

@property (nonatomic, weak) id<NLGetRefKeyWebServiceDelegate> getRefKeyWebServiceDelegate;

@end
