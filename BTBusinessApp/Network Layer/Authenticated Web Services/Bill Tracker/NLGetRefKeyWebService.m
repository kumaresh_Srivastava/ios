//
//  NLGetRefKeyWebService.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 13/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLGetRefKeyWebService.h"
#import "NSObject+APIResponseCheck.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NLConstants.h"
#import "AppConstants.h"
#import "NLWebServiceError.h"

@implementation NLGetRefKeyWebService


- (instancetype)init
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v3/User/refKey"];
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {

    }
    return self;
}



#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];

    NSString *appInstallationKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:appInstallationKey forKey:@"_authKey"];

    return [finalHeaderFieldDic copy];
}


#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];

    NSString *resultString = [responseObject valueForKey:kNLResponseKeyForResult];
    if(resultString)
    {

        [self.getRefKeyWebServiceDelegate getGetRefKeyWebService:self successfullyFetchedRefKey:resultString];

    }
    else
    {
        NLWebServiceError *errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];

        [self.getRefKeyWebServiceDelegate getRefKeyWebService:self failedToFetchRefKeyWithWebServiceError:errorToBeReturned];

    }


}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    [self.getRefKeyWebServiceDelegate getRefKeyWebService:self failedToFetchRefKeyWithWebServiceError:webServiceError];
}



#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil)
    {
        NSString *resultString = [responseObject valueForKey:kNLResponseKeyForResult];
        if(![resultString isKindOfClass:[NSString class]])
        {
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];

            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }
    
    return errorToBeReturned;
}





@end
