//
//  NLAuthenticationProtectedWebService.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 30/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLWebService.h"

@class NLAuthenticationProtectedWebService;
@class NLSiteMinderLoginService;
@class BTSMSession;

@interface NLAuthenticationProtectedWebService : NLWebService {

    BTSMSession *_smSession;
}

- (instancetype)initWithMethod:(NSString *)method parameters:(NSString *)parameters andEndpointUrlString:(NSString *)endpointUrlString;


@end
