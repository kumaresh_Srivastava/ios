//
//  NLAuthenticationProtectedWebService.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 30/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "CDUser.h"
#import "AppConstants.h"
#import "NSObject+APIResponseCheck.h"
#import "BTAuthenticationManager.h"
#import "NLWebServiceError.h"
#import "NLConstants.h"
#import "NLWebService+BackEndAPIErrorHandling.h"
#import "BTSMSession.h"

@interface NLAuthenticationProtectedWebService () {

    BOOL _hasAutoAuthenticationBeenAlreadyAttempted;
    NLWebServiceError *_webServiceErrorBeforeAutoAuthenticationAttempt;
}

@end

@implementation NLAuthenticationProtectedWebService

- (instancetype)initWithMethod:(NSString *)method parameters:(NSString *)parameters andEndpointUrlString:(NSString *)endpointUrlString
{
    self = [super initWithMethod:method parameters:parameters endpointUrlString:endpointUrlString andBaseURLType:NLBaseURLTypeNonVordel];
    if(self)
    {
        _smSession = [[BTAuthenticationManager sharedManager] smsessionForCurrentLoggedInUser];
    }
    return self;
}


#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    if (kDataPoint == DataPointStatic) {
        return [NSMutableDictionary dictionary];
    }
    else
    {
        NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
        NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];

        NSDictionary *cookieProperties = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel], NSHTTPCookieDomain,
                                          @"SMSESSION", NSHTTPCookieName,
                                          @"\\", NSHTTPCookiePath,
                                          _smSession.smsessionString, NSHTTPCookieValue,
                                          nil];
        NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
        if(cookie) {
            NSArray *arrayOfCookies = [NSArray arrayWithObject:cookie];
            NSDictionary *cookieHeaders = [NSHTTPCookie requestHeaderFieldsWithCookies:arrayOfCookies];

            for(NSString *key in cookieHeaders)
            {
                [finalHeaderFieldDic setValue:[cookieHeaders valueForKey:key] forKey:key];
            }
        }

        return [finalHeaderFieldDic copy];
    }
}


#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andError:(NSError *)error
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andError:error];

    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        if([[task response] isKindOfClass:[NSHTTPURLResponse class]])
        {
            NSInteger statusCode = [(NSHTTPURLResponse *)[task response] statusCode];
            if(statusCode == 200)
            {
                NSDictionary *allHTTPHeaders = [(NSHTTPURLResponse *)[task response] allHeaderFields];
                NSString *validSMSessionCookieValue = [self validSMSessionCookieValueFromServerResponseHTTPHeaderFields:allHTTPHeaders];
                if(validSMSessionCookieValue == nil)
                {
                    // (hd) Since we dont have a valid SMSession, hence we shall assume that the token/cookie got invalid. Hene it will be interpreted as an 'BTNetworkErrorCodeSMSessionUnauthenticaiton' error and will trigger auto authentication attempt in 'attemptErrorHandlingIfNeededForError' method.

                    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];

                    errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];

                    DDLogError(@"SMSession Cookie missing in the header of the response for SMSession protected API with URL %@.", [[task currentRequest] URL]);
                    
                }
                else
                {
                    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];

                    errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];

                    DDLogError(@"Response Object for API URL %@ is not valid but smsession cookie is still there in the response headers.", [[task currentRequest] URL]);
                    
                }
            }
        }
        else
        {
            DDLogError(@"The response of the SMSession API with URL %@ is not an object of NSHTTPURLResponse class.", [[task currentRequest] URL]);
        }
    }

    return errorToBeReturned;
}

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.

        if([responseObject isValidSMSessionAPIResponseObject])
        {
            BOOL isSuccess = [[responseObject valueForKey:kNLResponseKeyForIsSuccess] boolValue];
            if(isSuccess)
            {
                id result = [responseObject valueForKey:kNLResponseKeyForResult];
                if(result)
                {

                }
                else
                {
                    NLWebServiceError *webServiceError = [self errorFromBackendAPIAgreedResponseDic:responseObject];
                    
                    if (webServiceError)
                    {
                        if (webServiceError.error.code == BTNetworkErrorCodeAPINoDataFound)
                        {
                            errorToBeReturned = webServiceError;
                            
                            DDLogError(@"No data found for webservice with URL: %@", [[task currentRequest] URL]);
                        }
                        else if (webServiceError.error.code == BTNetworkErrorCodeUnsupportedOrder)
                        {
                            errorToBeReturned = webServiceError;
                            
                            DDLogError(@"No data found for webservice with URL: %@", [[task currentRequest] URL]);
                        }
                        else
                        {
                            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
                            
                            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
                            
                            DDLogError(@"'result' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]);
                        }
                    }
                    else
                    {
                        NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
                        
                        errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
                        
                        DDLogError(@"'result' and Error Code not found in response json object for webservice with URL: %@", [[task currentRequest] URL]);
                    }
                }
            }
            else
            {
                NLWebServiceError *webServiceErrorFromBackendAPI = [self errorFromBackendAPIAgreedResponseDic:responseObject];
                if(webServiceErrorFromBackendAPI)
                {
                    errorToBeReturned = webServiceErrorFromBackendAPI;
                    DDLogError(@"Error code: %ld in webservice with URL: %@", (long)webServiceErrorFromBackendAPI.error.code, [[task currentRequest] URL]);
                    
                }
                else
                {
                    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
                    errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
                    DDLogError(@"Error Code not found in response json object for webservice with URL: %@", [[task currentRequest] URL]);
                }
            }
        }
        else
        {
            if([[task response] isKindOfClass:[NSHTTPURLResponse class]])
            {
                NSInteger statusCode = [(NSHTTPURLResponse *)[task response] statusCode];
                if(statusCode == 200)
                {
                    NSDictionary *allHTTPHeaders = [(NSHTTPURLResponse *)[task response] allHeaderFields];
                    NSString *validSMSessionCookieValue = [self validSMSessionCookieValueFromServerResponseHTTPHeaderFields:allHTTPHeaders];
                    if(validSMSessionCookieValue == nil)
                    {
                        // (hd) Since we dont have a valid SMSession, hence we shall assume that the token/cookie got invalid. Hene it will be interpreted as an 'BTNetworkErrorCodeSMSessionUnauthenticaiton' error and will trigger auto authentication attempt in 'attemptErrorHandlingIfNeededForError' method.

                        NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];

                        errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];

                        DDLogError(@"SMSession Cookie missing in the header of the response for SMSession protected API with URL %@.", [[task currentRequest] URL]);
                    }
                    else
                    {
                        NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];

                        errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];

                        DDLogError(@"Response Object for API URL %@ is not valid but smsession cookie is still there in the response headers.", [[task currentRequest] URL]);
                    }
                }
                else
                {
                    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSuccessButNot200 userInfo:nil];

                    errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];

                    DDLogError(@"Unexpected Case: Webservice with URL %@ succeeded but the HTTP Status code of the response is %ld instead of 200.", [[task currentRequest] URL], (long)statusCode);
                    
                }
            }
            else
            {
                NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];

                errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];

                DDLogError(@"The response of the SMSession API with URL %@ is not an object of NSHTTPURLResponse class.", [[task currentRequest] URL]);
            }
        }
    }

    return errorToBeReturned;
}

- (BOOL)attemptErrorHandlingIfNeededForError:(NLWebServiceError *)webServiceError
{
    __block BOOL errorHandled = [super attemptErrorHandlingIfNeededForError:webServiceError];

    if(errorHandled == NO)
    {
        // (hds) If there was no error handling from superclass, then this class can check if it needs to handle the error or not.

        // (hds) Handling the case of 'BTNetworkErrorCodeSMSessionUnauthenticaiton' with auto attempt for authentication through BTAuthenticationManager.

        if(webServiceError.error.domain == BTNetworkErrorDomain && webServiceError.error.code == BTNetworkErrorCodeSMSessionUnauthenticaiton)
        {
            if(_hasAutoAuthenticationBeenAlreadyAttempted == NO)
            {
                DDLogInfo(@"Handling the SM Authentication error by attempting Auto Authentication Attempt.");

                errorHandled = YES;

                [[BTAuthenticationManager sharedManager] reAuthenticateCurrentlyLoggedInUserWithPreviousToken:nil withAlreadyHaveFreshTokenHandler:nil andFetchFreshTokenHandler:^{

                    [self addObserverToNotificationForReAuthenticationCall];
                    _webServiceErrorBeforeAutoAuthenticationAttempt = webServiceError;
                    _hasAutoAuthenticationBeenAlreadyAttempted = YES;
                }];
            }
        }
    }

    return errorHandled;
}


#pragma mark - Private Helper Methods

- (void)addObserverToNotificationForReAuthenticationCall
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reAuthenticationAPICallSuccessfullyFinishedNotification:) name:kReAuthenticationApiCallSuccessfullyFinishedNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reAuthenticationAPICallFailedNotification:) name:kReAuthenticationApiCallFailedNotification object:nil];
}

- (void)removeObserverToNotificationForReAuthenticationCall
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReAuthenticationApiCallSuccessfullyFinishedNotification object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReAuthenticationApiCallFailedNotification object:nil];
}


#pragma mark - Notification Methods

- (void)reAuthenticationAPICallSuccessfullyFinishedNotification:(NSNotification *)notification
{
    [self removeObserverToNotificationForReAuthenticationCall];

    BTSMSession *smSession = [notification.userInfo valueForKey:@"smsession"];
    _smSession = smSession;

    [self resume];
}

- (void)reAuthenticationAPICallFailedNotification:(NSNotification *)notification
{
    [self removeObserverToNotificationForReAuthenticationCall];

    NLWebServiceError *error = [notification.userInfo valueForKey:@"error"];

    NLWebServiceError *newErrorObject = [[NLWebServiceError alloc] initWithError:error.error andSourceError:_webServiceErrorBeforeAutoAuthenticationAttempt];
    [self handleFailureWithSessionDataTask:(NSURLSessionDataTask *)_currentTask andWebServiceError:newErrorObject];
    [self cancel];
}



#pragma mark - Private Helper Methods

- (NSString *)validSMSessionCookieValueFromServerResponseHTTPHeaderFields:(NSDictionary *)allHTTPHeaderFields
{
    NSString *validCookieValue = nil;

    id smSessionCookieValueFromServer = [allHTTPHeaderFields valueForKey:@"SMSESSION"];
    if(smSessionCookieValueFromServer)
    {
        if([smSessionCookieValueFromServer isKindOfClass:[NSString class]])
        {
            NSString *trimmedCookieValueFromServer = [smSessionCookieValueFromServer stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if(!([trimmedCookieValueFromServer isEqualToString:@""] || [[trimmedCookieValueFromServer lowercaseString] isEqualToString:@"loggedoff"] || [[trimmedCookieValueFromServer lowercaseString] isEqualToString:@"null"] || [[trimmedCookieValueFromServer lowercaseString] isEqualToString:@"nil"]))
            {
                validCookieValue = trimmedCookieValueFromServer;
            }
        }
    }


    return validCookieValue;
}



@end
