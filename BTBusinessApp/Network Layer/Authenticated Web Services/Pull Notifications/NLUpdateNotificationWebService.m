//
//  NLUpdateNotificationWebService.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 28/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLUpdateNotificationWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NLConstants.h"
#import "NLWebServiceError.h"

@implementation NLUpdateNotificationWebService

- (instancetype)initWithNotificationId:(NSString *)notificationId
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v3/Notification/Update/%@", notificationId];

    self = [super initWithMethod:@"POST" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
        
    }
    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    NSString *groupKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:groupKey forKey:@"_authKey"];
    
    return [finalHeaderFieldDic copy];
}


#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];

    BOOL result = [[responseObject valueForKey:kNLResponseKeyForResult] boolValue];

    if (result)
    {
        DDLogInfo(@"Successfully updated notification status using webservice with URL: %@", [[task currentRequest] URL]);
        
        [self.updateNotificationWebServiceDelegate successfullyUpdatedNotificationsDataByUpdateNotificationWebService:self];
    }
    else
    {
        DDLogError(@"Failed to update notification status using webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Failed to update notification status using webservice with URL: %@", [[task currentRequest] URL]]));
        
        NLWebServiceError *webServiceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeAPIErrorUnknown];
        
        [self.updateNotificationWebServiceDelegate updateNotificationWebService:self failedToUpdateNotificationsDataWithWebServiceError:webServiceError];
    }
    
}



- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];

    [self.updateNotificationWebServiceDelegate updateNotificationWebService:self failedToUpdateNotificationsDataWithWebServiceError:webServiceError];
}


#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.

        id result = [responseObject valueForKey:kNLResponseKeyForResult];
        if ([result isKindOfClass:[NSNumber class]])
        {
            if([result isEqual:[NSNumber numberWithBool:YES]]
               || [result isEqual:[NSNumber numberWithBool:NO]])
            {
                NSNumber *resultValue = result;
                if (resultValue.intValue)
                {
                    if (resultValue.intValue >= 0 && resultValue.intValue <= 1)
                    {
                        // (lpm) Do nothing here.
                    }
                    else
                    {
                        errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
                        
                        DDLogError(@"'result' element is not in correct format in the response object for URL %@", [[task currentRequest] URL]);
                        
                    }
                }
                else
                {
                    errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
                    
                    DDLogError(@"'result' element is not in correct format in the response object for URL %@", [[task currentRequest] URL]);
                    
                }
            }
            else
            {
                errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
                
                DDLogError(@"'result' element is not in correct format in the response object for URL %@", [[task currentRequest] URL]);
                
            }
        }
        else
        {
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
            
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }

    return errorToBeReturned;
}


#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;

    return nil;
}



@end
