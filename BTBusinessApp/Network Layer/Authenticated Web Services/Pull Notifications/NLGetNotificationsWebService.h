//
//  NLGetNotificationsWebService.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 27/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"

@class NLGetNotificationsWebService;
@class NLWebServiceError;

@protocol NLGetNotificationsWebServiceDelegate

- (void)getNotificationsWebService:(NLGetNotificationsWebService *)webService successfullyFetchedNotificationsData:(NSArray *)arrayOfNotifications;

- (void)getNotificationsWebService:(NLGetNotificationsWebService *)webService failedToFetchNotificationsDataWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLGetNotificationsWebService : NLAuthenticationProtectedWebService {
    
}

@property (nonatomic, weak) id <NLGetNotificationsWebServiceDelegate> getNotificationsWebServiceDelegate;

@end
