//
//  NLUpdateNotificationWebService.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 28/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"

@class NLUpdateNotificationWebService;
@class NLWebServiceError;

@protocol NLUpdateNotificationWebServiceDelegate

- (void)successfullyUpdatedNotificationsDataByUpdateNotificationWebService:(NLUpdateNotificationWebService *)webService;

- (void)updateNotificationWebService:(NLUpdateNotificationWebService *)webService failedToUpdateNotificationsDataWithWebServiceError:(NLWebServiceError *)error;

@end


@interface NLUpdateNotificationWebService : NLAuthenticationProtectedWebService {
    
}

@property (nonatomic, weak) id <NLUpdateNotificationWebServiceDelegate> updateNotificationWebServiceDelegate;

- (instancetype)initWithNotificationId:(NSString *)notificationId;

@end
