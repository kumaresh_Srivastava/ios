//
//  NLServerLogWebService.h
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 10/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"


@class NLServerLogWebService;
@class NLWebServiceError;

@protocol NLServerLogWebServiceDelegate 

- (void)serverLogWebService:(NLServerLogWebService *)webService finishedSubmittingLogsToServerWithArrayOfSuccessfullServerLogItems:(NSArray *)arrayOfSuccessServerLogItems;

- (void)serverLogWebService:(NLServerLogWebService *)webService failedToSubmitLogsToServerWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface NLServerLogWebService : NLAuthenticationProtectedWebService {

}

@property (nonatomic, weak) id <NLServerLogWebServiceDelegate> serverLogWebServiceDelegate;

- (instancetype)initWithArrayOfServerLogItems:(NSArray *)serverLogItems;

@end
