//
//  NLServerLogWebService.m
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 10/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLServerLogWebService.h"
#import "LHServerLogItem.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "AppConstants.h"
#import "AFNetworking.h"
#import "LHConstants.h"
#import <CocoaLumberjack/CocoaLumberjack.h>
#import <CocoaLumberjack/DDLog.h>
#import "AppManager.h"
#import "AppDelegate.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "AppDelegateViewModel.h"

#import "NLWebServiceError.h"

@interface NLServerLogWebService () {

    NSArray *_serverLogItems;
}

- (NSString *)parameterStringFromServerLogItems:(NSArray *)serverLogItems;

- (NSInteger)flagOfServerTypeFromFlagOfCocoaLumberjack:(NSUInteger)cocoaLumberjackLogFlag;

- (NSString *)commentStringFromServerLogItem:(LHServerLogItem *)logItem;

@end

@implementation NLServerLogWebService

- (instancetype)initWithArrayOfServerLogItems:(NSArray *)serverLogItems
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/Account/api/v1/BatchLog"];// /account/api/ExceptionLogger/BatchLog

    NSString *parameterString = [self parameterStringFromServerLogItems:serverLogItems];

    if(parameterString == nil)
    {
        return nil;
    }

    self = [super initWithMethod:@"POST" parameters:parameterString andEndpointUrlString:endPointURLString];
    if(self)
    {
        _serverLogItems = serverLogItems;
    }
    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask


- (AFHTTPSessionManager *)sessionManager
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];

    NSString *baseURLString = [NSString stringWithFormat:@"https://%@", [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel]];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseURLString] sessionConfiguration:configuration];

    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    [manager.requestSerializer setTimeoutInterval:_timeOutTimeInterval];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    // (hd) Because the API call expects the parameters in raw format, hence we are setting an empty query string serializer so that it does not do any serialization.
    [manager.requestSerializer setQueryStringSerializationWithBlock:^NSString * _Nonnull(NSURLRequest * _Nonnull request, id  _Nonnull parameters, NSError * _Nullable __autoreleasing * _Nullable error) {

        return parameters;
    }];
    
    return manager;
}


- (NSDictionary *)httpHeaderFields
{
    
    if (kDataPoint == DataPointStatic) {
        return [NSMutableDictionary dictionary];
    }
    else
    {
        NSDictionary *headerFieldsFromSuperClasses = nil;
        NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
        
        //Find smsession based on the entery point of the intercepter
        NSString *smsessionStr;
        NSString *groupKey;
        if([[NSUserDefaults standardUserDefaults] objectForKey:kSMSessionLoginIntercepter])
        {
            smsessionStr = [[NSUserDefaults standardUserDefaults] valueForKey:kSMSessionLoginIntercepter];
            groupKey = [[NSUserDefaults standardUserDefaults] valueForKey:kGroupKeyLoginIntercepter];
        }
        else
        {
            smsessionStr = _smSession.smsessionString;
            groupKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
        }
        
        
        NSDictionary *cookieProperties = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel], NSHTTPCookieDomain,
                                          @"SMSESSION", NSHTTPCookieName,
                                          @"\\", NSHTTPCookiePath,
                                          smsessionStr, NSHTTPCookieValue,
                                          nil];
        NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
        NSArray *arrayOfCookies = [NSArray arrayWithObject:cookie];
        NSDictionary *cookieHeaders = [NSHTTPCookie requestHeaderFieldsWithCookies:arrayOfCookies];
        
        for(NSString *key in cookieHeaders)
        {
            [finalHeaderFieldDic setValue:[cookieHeaders valueForKey:key] forKey:key];
        }
        
        
        [finalHeaderFieldDic setValue:groupKey forKey:@"_authKey"];
        
        return [finalHeaderFieldDic copy];
    }
    
    
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    DDLogInfo(@"Successfully submitted logs to server using webservice with URL: %@", [[task currentRequest] URL]);
    
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    DDLogError(@"Failed to submit logs to server with error: %@.", webServiceError.error);
}




#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (Sal) If there was no error from superclass, then this class can check for its level of errors.
        NSInteger resultValue = [[responseObject valueForKey:kNLResponseKeyForResult] integerValue];
        if(!resultValue)
        {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
            
            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
            
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }
    
    return errorToBeReturned;
}




#pragma mark - Private Helper Methods

- (NSString *)parameterStringFromServerLogItems:(NSArray *)serverLogItems
{
    NSMutableArray *finalArray = [NSMutableArray array];

    for(LHServerLogItem *logItem in serverLogItems)
    {
        NSMutableDictionary *logDic = [NSMutableDictionary dictionary];

        // (hd) Setting the Event Type
        [logDic setValue:LogHandlingServerLogEventType forKey:@"eventType"];

        // (hd) Setting the Event Name With Time Stamp
        [logDic setValue:[NSString stringWithFormat:@"%f", logItem.timeIntervalSinceReferenceDate] forKey:@"eventName"];
        

        // (hd) Setting the OrderKey with Username & BTInstallation ID Combo
        NSString *orderKeyString = nil;
        if(logItem.btInstallationID && logItem.loggedInUsername)
        {
            orderKeyString = [NSString stringWithFormat:@"%@||%@", logItem.loggedInUsername, logItem.btInstallationID];
        }
        else if(logItem.btInstallationID && logItem.loggedInUsername == nil)
        {
            orderKeyString = [NSString stringWithFormat:@"%@", logItem.btInstallationID];
        }
        else if(logItem.loggedInUsername && logItem.btInstallationID == nil)
        {
            orderKeyString = [NSString stringWithFormat:@"%@", logItem.loggedInUsername];
        }
        [logDic setValue:orderKeyString forKey:@"orderKey"];

        // (hd) Setting message with Error Message
        [logDic setValue:logItem.logMessage forKey:@"message"];


        // (hd) Setting comment with value of multiple other types of useful info
        [logDic setValue:logItem.comment forKey:@"comment"];


        // (hd) Setting Details with value of 1 signifying that is an error type of log
        [logDic setValue:@([self flagOfServerTypeFromFlagOfCocoaLumberjack:logItem.logFlag]) forKey:@"details"];

        [finalArray addObject:logDic];
    }



    NSError *errorWhileCreatingJSONData = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:finalArray options:0 error:&errorWhileCreatingJSONData];

    if(errorWhileCreatingJSONData)
    {
        return nil;
    }

    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:4];

    return jsonString;
}

- (NSInteger)flagOfServerTypeFromFlagOfCocoaLumberjack:(NSUInteger)cocoaLumberjackLogFlag
{
    NSUInteger serverFlag  = 3;

    switch (cocoaLumberjackLogFlag) {

        case DDLogFlagError:
        {
            serverFlag = 1;
            break;
        }

        case DDLogFlagWarning:
        {
            serverFlag = 1;
            break;
        }

        case DDLogFlagInfo:
        {
            serverFlag = 2;
            break;
        }

        case DDLogFlagDebug:
        {
            serverFlag = 3;
            break;
        }

        case DDLogFlagVerbose:
        {
            serverFlag = 3;
            break;
        }

        default:
            break;
    }

    return serverFlag;
}

- (NSString *)commentStringFromServerLogItem:(LHServerLogItem *)logItem
{
    NSMutableArray *infoArray = [NSMutableArray array];

    if(logItem.fileName)
    {
        [infoArray addObject:[NSString stringWithFormat:@"Filename: %@", logItem.fileName]];
    }

    if(logItem.method)
    {
        [infoArray addObject:[NSString stringWithFormat:@"Method: %@", logItem.method]];
    }

    if(logItem.line)
    {
        [infoArray addObject:[NSString stringWithFormat:@"Line: %ld", (unsigned long)logItem.line]];
    }

    if(logItem.threadName)
    {
        [infoArray addObject:[NSString stringWithFormat:@"Threadname: %@", logItem.threadName]];
    }

    if(logItem.queueLabel)
    {
        [infoArray addObject:[NSString stringWithFormat:@"Queuename: %@", logItem.queueLabel]];
    }

    if(logItem.selectedCUGName)
    {
        [infoArray addObject:[NSString stringWithFormat:@"Selected Cug: %@", logItem.selectedCUGName]];
    }

    NSMutableString *commentString = [NSMutableString stringWithFormat:@""];

    for(NSString *infoItemString in infoArray)
    {
        if([infoArray indexOfObject:infoItemString] == 0)
        {
            [commentString appendFormat:@"%@", infoItemString];
        }
        else
        {
            [commentString appendFormat:@"\n%@", infoItemString];
        }
    }

    return [commentString copy];
}

@end
