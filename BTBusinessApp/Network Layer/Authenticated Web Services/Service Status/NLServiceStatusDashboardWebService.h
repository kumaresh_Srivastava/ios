//
//  NLServiceStatusDashboardWebService.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 22/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"

@class NLServiceStatusDashboardWebService;
@class NLWebServiceError;

@protocol NLServiceStatusDashboardWebServiceDelegate

- (void)getServiceStatusDashboardWebService:(NLServiceStatusDashboardWebService *)webService successfullyFetchedServiceStatusDashBoardData:(NSArray *)assets;

- (void)getServiceStatusDashBoardWebService:(NLServiceStatusDashboardWebService *)webService failedToFetchServiceStatusDashBoardDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface NLServiceStatusDashboardWebService : NLAuthenticationProtectedWebService

@property (nonatomic, weak) id <NLServiceStatusDashboardWebServiceDelegate> serviceStatusDashBoardWebServiceDelegate;

@end
