//
//  NLCheckServiceStatusWebService.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 22/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "NLAPIGEEAuthenticatedWebService.h"

@class NLCheckServiceStatusWebService;
@class NLWebServiceError;

@protocol NLCheckServiceStatusWebServiceDelegate

- (void)getCheckServiceStatusWebService:(NLCheckServiceStatusWebService *)webService successfullyFetchedCheckServiceStatusWithServiceArray:(NSArray *)serviceArray;

- (void)getCheckServiceStatusWebService:(NLCheckServiceStatusWebService *)webService failedToFetchCheckServiceStatusWithWebServiceError:(NLWebServiceError *)webServiceError;

@end


@interface NLCheckServiceStatusWebService : NLAPIGEEAuthenticatedWebService

@property (nonatomic, weak) id <NLCheckServiceStatusWebServiceDelegate> checkServiceStatusWebServiceDelegate;

- (instancetype)initWithBACId:(NSString *)bacId;

@end
