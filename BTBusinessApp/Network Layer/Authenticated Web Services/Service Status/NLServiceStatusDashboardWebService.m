//
//  NLServiceStatusDashboardWebService.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 22/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLServiceStatusDashboardWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "BTAsset.h"
#import "AppConstants.h"
#import "NLWebServiceError.h"

@implementation NLServiceStatusDashboardWebService

- (instancetype)init {

    NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v2/User/%@/AllBAC",[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey];

    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];

    return self;
}


#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject {
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];

    NSArray *resultArray = [responseObject valueForKey:kNLResponseKeyForResult];
    BTAsset *asset = nil;
    NSMutableArray *assetsArray = [NSMutableArray array];

    for (NSDictionary *assetsDetailDict in resultArray) {

        if(assetsDetailDict) {

            asset = [[BTAsset alloc] initWithResponseDictionaryFromAssetsDashboardAPIResponse:assetsDetailDict];
            [assetsArray addObject:asset];
        }
    }

    DDLogInfo(@"Successfully fetched Assets BACs using webservice with URL: %@", [[task currentRequest] URL]);

    [self.serviceStatusDashBoardWebServiceDelegate getServiceStatusDashboardWebService:self successfullyFetchedServiceStatusDashBoardData:assetsArray];
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError {

    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];

    [self.serviceStatusDashBoardWebServiceDelegate getServiceStatusDashBoardWebService:self failedToFetchServiceStatusDashBoardDataWithWebServiceError:webServiceError];
}

#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject {

    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil) {

        // (hds) If there was no error from superclass, then this class can check for its level of errors.

        NSArray *resultArray = [responseObject valueForKey:kNLResponseKeyForResult];
        if([resultArray isKindOfClass:[NSArray class]]) {

            // (hds) Do Nothing Here.
        }
        else {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];

            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];

            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
        }
    }

    return errorToBeReturned;
}

#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError {

    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;
    
    return nil;
}



@end
