//
//  NLServiceStatusDetailWebService.h
//  BTBusinessApp
//
//  Created by VectoScalar on 3/1/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "NLAPIGEEAuthenticatedWebService.h"

@class NLServiceStatusDetailWebService;
@class BTServiceStatusDetail;
@class NLWebServiceError;

@protocol NLServiceStatusDetailWebServiceDelegate <NSObject>

- (void)getServiceStatusDetailWebService:(NLServiceStatusDetailWebService *)webService successfullyFetchedServiceStatusDetail:(BTServiceStatusDetail *)serviceStatusDetail;

- (void)getServiceStatusDetailWebService:(NLServiceStatusDetailWebService *)webService failedToFetchServiceStatusDetailWithWebServiceError:(NLWebServiceError *)webServiceError;

@end


@interface NLServiceStatusDetailWebService : NLAPIGEEAuthenticatedWebService

@property (nonatomic, weak) id <NLServiceStatusDetailWebServiceDelegate> serviceStatusWebServiceDelegate;
@property (nonatomic ,assign) BOOL needToPointToModelAAlways;

- (instancetype)initWithServiceID:(NSString *)serviceID andServiceType:(NSString *)serviceType andIsNeedToPointModelAAlways:(BOOL)needToPointModelA;

@end
