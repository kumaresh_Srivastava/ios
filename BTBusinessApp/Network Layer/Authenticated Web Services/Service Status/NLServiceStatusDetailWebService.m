//
//  NLServiceStatusDetailWebService.m
//  BTBusinessApp
//
//  Created by VectoScalar on 3/1/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLServiceStatusDetailWebService.h"
#import "NLConstants.h"
#import "AppConstants.h"
#import "NLWebServiceError.h"
#import "BTServiceStatusDetail.h"
#import "AppDelegate.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "CDApp.h"
#import "AppDelegateViewModel.h"
#import "AFNetworking.h"


@implementation NLServiceStatusDetailWebService

- (instancetype)initWithServiceID:(NSString *)serviceID andServiceType:(NSString *)serviceType andIsNeedToPointModelAAlways:(BOOL)needToPointModelA
{
    
    NSLog(@"Service ID is: %@", serviceID);
    
    NSString *endPointURLString = @"";
    
    //https://api-test1.ee.co.uk/bt-business-auth/v1/service/{serviceId}/{productType}/status
    if(needToPointModelA)
        endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/service/%@/%@/status", serviceID, serviceType];
    else
        endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/service/%@/%@/status", serviceID, serviceType];
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    
    if (self)
    {
        self.needToPointToModelAAlways = needToPointModelA;
    }
    
    return self;
}





#pragma mark - Helper Methods For Prepration Of The NSURLSessionTask
//(RLM) this method is overriden in order ot acheive the functionality that if we open service detail from tripple tapping on dashboard then it always points to Model A
- (AFHTTPSessionManager *)sessionManager
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSString *baseURLString ;
    if(_needToPointToModelAAlways)
       baseURLString = [NSString stringWithFormat:@"https://%@", kNLBaseURLNONVordelURLModelA];
    else
       baseURLString = [NSString stringWithFormat:@"https://%@", [AppManager baseURLStringFromBaseURLType:_baseURLType]];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseURLString] sessionConfiguration:configuration];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    [manager.requestSerializer setTimeoutInterval:_timeOutTimeInterval];
    
    return manager;
}


#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject {
    
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *resultDict = [responseObject valueForKey:kNLResponseKeyForResult];
    if(resultDict) {
        
        
        DDLogInfo(@"Successfully fetched service status detail using webservice with URL: %@", [[task currentRequest] URL]);
                
        BTServiceStatusDetail *serviceDetail = [[BTServiceStatusDetail alloc] initWithServiceStatusDetailResponse:resultDict];
        
        [self.serviceStatusWebServiceDelegate getServiceStatusDetailWebService:self successfullyFetchedServiceStatusDetail:serviceDetail];
    }
    else {
        
        NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
        
        NLWebServiceError *newWebServiceError = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
        
        DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]]));
        
        [self.serviceStatusWebServiceDelegate getServiceStatusDetailWebService:self failedToFetchServiceStatusDetailWithWebServiceError:newWebServiceError];

    }
    
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError {
    
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.serviceStatusWebServiceDelegate getServiceStatusDetailWebService:self failedToFetchServiceStatusDetailWithWebServiceError:webServiceError];
}

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    NSString *groupKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:groupKey forKey:@"_authKey"];
    
    return [finalHeaderFieldDic copy];
}


#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject {
    
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil) {
    
        
        NSDictionary *resultDict = [responseObject valueForKey:kNLResponseKeyForResult];
        if([resultDict isKindOfClass:[NSDictionary class]]) {
            
            //Do nothing
        
        }
        else {
            
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
            
            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
            
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }
    
    return errorToBeReturned;
}


#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError {
    
    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;
    
    return nil;
    
}


/*
//Temporary to load mock response
- (void)resume {
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"serviceStatusDetail" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    [self processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:nil andResponseObject:json];
    [self handleSuccessWithSessionDataTask:nil andResponseObject:json];
}
*/



@end
