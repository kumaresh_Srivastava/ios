//
//  NLCheckServiceStatusWebService.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 22/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLCheckServiceStatusWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "BTAsset.h"
#import "AppConstants.h"
#import "NLWebServiceError.h"
#import "BTBroadbandAndPhoneService.h"


@implementation NLCheckServiceStatusWebService

- (instancetype)initWithBACId:(NSString *)bacId {

    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/assets/%@/products",bacId];
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];

    return self;
}



#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];

    NSString *appInstallationKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;

    [finalHeaderFieldDic setValue:appInstallationKey forKey:@"_authKey"];

    return [finalHeaderFieldDic copy];
}





#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject {

    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];

    NSArray *resultArray = [responseObject valueForKey:kNLResponseKeyForResult];
    if(resultArray) {

        NSMutableArray *servicesArray = [NSMutableArray array];

            for (NSDictionary *serviceDetailDict in resultArray) {

                BTBroadbandAndPhoneService *service = [[BTBroadbandAndPhoneService alloc] initBTBroadbandAndPhoneServiceWithResponse:serviceDetailDict];
                if(service)
                    [servicesArray  addObject:service];

            }



        DDLogInfo(@"Successfully fetched CheckService status using webservice with URL: %@", [[task currentRequest] URL]);

        [self.checkServiceStatusWebServiceDelegate getCheckServiceStatusWebService:self successfullyFetchedCheckServiceStatusWithServiceArray:servicesArray];
    }
    else {

        NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];

        NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];

        DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]]));
        
        [self.checkServiceStatusWebServiceDelegate getCheckServiceStatusWebService:self failedToFetchCheckServiceStatusWithWebServiceError:errorToBeReturned];
    }

}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError {

    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];

    [self.checkServiceStatusWebServiceDelegate getCheckServiceStatusWebService:self failedToFetchCheckServiceStatusWithWebServiceError:webServiceError];
}

#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject {

    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil) {

        // (hds) If there was no error from superclass, then this class can check for its level of errors.

        NSArray *resultArray = [responseObject valueForKey:kNLResponseKeyForResult];
        if([resultArray isKindOfClass:[NSArray class]]) {

            // (hds) Do Nothing Here.
        }
        else {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];

            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];

            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
            
        }
    }

    return errorToBeReturned;
}

#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError {

    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;

    return nil;

}


//- (void)resume {
//
//    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"CheckServiceStatus" ofType:@"json"];
//    NSData *data = [NSData dataWithContentsOfFile:filePath];
//    id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
//    
//    [self processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:nil andResponseObject:json];
//    [self handleSuccessWithSessionDataTask:nil andResponseObject:json];
//}


@end
