//
//  NLUserLogoutWebService.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 15/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLUserLogoutWebService.h"
#import "NLConstants.h"
#import "NSObject+APIResponseCheck.h"
#import "NLWebServiceError.h"
#import "NSObject+APIResponseCheck.h"
#import "BTAuthenticationManager.h"
#import "BTSMSession.h"

@interface NLUserLogoutWebService () {

}

@end

@implementation NLUserLogoutWebService

- (instancetype)init
{
    NSString *endPointURLString = [NSString stringWithFormat:@"api/v1/User/Logout"];

    self = [super initWithMethod:@"POST" parameters:nil endpointUrlString:endPointURLString andBaseURLType:NLBaseURLTypeNonVordel];
    if(self)
    {
        
    }

    return self;
}


#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    BTSMSession *_smSession = [[BTAuthenticationManager sharedManager] smsessionForCurrentLoggedInUser];
    
    [finalHeaderFieldDic setValue:_smSession.smsessionString forKey:@"SMSESSION"];

    return [finalHeaderFieldDic copy];
}


#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    [self.userLogoutWebServiceDelegate apiRequestForLogoutSucceededWithUserLogoutWebService:self];
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    [self.userLogoutWebServiceDelegate userLogoutWebService:self failedToLogOutCurrentUserWithWebServiceError:webServiceError];
}

#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        
        NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
        if([resultDic isKindOfClass:[NSDictionary class]])
        {
            // (hds) Do Nothing Here.
        }
        else
        {
            DDLogError(@"'result' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]);
                        
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
            
        }
    }
    return errorToBeReturned;
}

@end
