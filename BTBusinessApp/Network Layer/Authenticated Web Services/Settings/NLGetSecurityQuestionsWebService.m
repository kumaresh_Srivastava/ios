//
//  NLGetSecurityQuestionsWebService.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 21/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLGetSecurityQuestionsWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "NLWebServiceError.h"
#import "AppConstants.h"
#import "NLConstants.h"

@interface NLGetSecurityQuestionsWebService ()

@end

@implementation NLGetSecurityQuestionsWebService

- (instancetype)init
{
    NSString *endPointURLString = @"/account/api/v3/Users/SecurityQuestions";

    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];

    if(self)
    {

    }
    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];

    NSString *appInstallationKey = [AppDelegate sharedInstance].viewModel.app.btInstallationID;
    [finalHeaderFieldDic setValue:appInstallationKey forKey:@"_authKey"];

    return [finalHeaderFieldDic copy];
}



#pragma mark - NLWebService Response Handling Private Methods
- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];

    NSArray *resultArray = [responseObject valueForKey:kNLResponseKeyForResult];
    
    if(resultArray)
    {
        NSMutableArray *arrayOfSecurityQuestions = [NSMutableArray array];

        for (NSString *securityQuestion in resultArray) {
            [arrayOfSecurityQuestions addObject:securityQuestion];
        }
        
        DDLogInfo(@"Successfully fetched security questions using webservice with URL: %@", [[task currentRequest] URL]);

        [self.getSecurityQuestionsWebServiceDelegate getSecurityQuestionsWebService:self successfullyFetchedSecurityQuestionsData:arrayOfSecurityQuestions];
    }
    else
    {
        NLWebServiceError *webServiceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];

        DDLogError(@"Invalid response object in response of webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Invalid response object in response of webservice with URL: %@", [[task currentRequest] URL]]));

        [self.getSecurityQuestionsWebServiceDelegate getSecurityQuestionsWebService:self failedWithWebServiceError:webServiceError];
    }
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];

    [self.getSecurityQuestionsWebServiceDelegate getSecurityQuestionsWebService:self failedWithWebServiceError:webServiceError];
}



#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.

        NSArray *resultArray = [responseObject valueForKey:kNLResponseKeyForResult];
        if([resultArray isKindOfClass:[NSArray class]])
        {

        }
        else
        {
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];

            DDLogError(@"Invalid response object in response of webservice with URL: %@", [[task currentRequest] URL]);
            
        }
    }

    return errorToBeReturned;
}




#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;
    
    return nil;
}

@end
