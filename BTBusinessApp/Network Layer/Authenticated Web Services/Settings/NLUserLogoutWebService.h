//
//  NLUserLogoutWebService.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 15/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLWebService.h"

@class NLUserLogoutWebService;
@class NLWebServiceError;

@protocol NLUserLogoutWebServiceDelegate <NSObject>

- (void)apiRequestForLogoutSucceededWithUserLogoutWebService:(NLUserLogoutWebService *)webService;

- (void)userLogoutWebService:(NLUserLogoutWebService *)webService failedToLogOutCurrentUserWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLUserLogoutWebService : NLWebService {

}

@property (assign,nonatomic) id <NLUserLogoutWebServiceDelegate> userLogoutWebServiceDelegate;

@end
