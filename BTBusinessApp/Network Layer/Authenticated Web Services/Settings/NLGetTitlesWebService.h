//
//  NLGetTitlesWebService.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 21/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"

@class NLGetTitlesWebService;
@class NLWebServiceError;

@protocol NLGetTitlesWebServiceDelegate <NSObject>

- (void)getTitlesWebService:(NLGetTitlesWebService *)webService successfullyFetchedTitlesData:(NSArray *)arrayOfTitles;

- (void)getTitlesWebService:(NLGetTitlesWebService *)webService failedWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLGetTitlesWebService : NLAuthenticationProtectedWebService {

}

@property (nonatomic, weak) id<NLGetTitlesWebServiceDelegate> getTitlesWebServiceDelegate;

@end
