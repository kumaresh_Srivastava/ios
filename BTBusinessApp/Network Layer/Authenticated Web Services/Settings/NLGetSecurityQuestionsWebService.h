//
//  NLGetSecurityQuestionsWebService.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 21/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"

@class NLGetSecurityQuestionsWebService;
@class NLWebServiceError;

@protocol NLGetSecurityQuestionsWebServiceDelegate <NSObject>

- (void)getSecurityQuestionsWebService:(NLGetSecurityQuestionsWebService *)webService successfullyFetchedSecurityQuestionsData:(NSArray *)arrayOfSecurityQuestions;

- (void)getSecurityQuestionsWebService:(NLGetSecurityQuestionsWebService *)webService failedWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLGetSecurityQuestionsWebService : NLAuthenticationProtectedWebService {

}

@property (nonatomic, weak) id<NLGetSecurityQuestionsWebServiceDelegate> getSecurityQuestionsWebServiceDelegate;

@end
