//
//  NLGetFaultSummaryWebService.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 16/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLGetFaultSummaryWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "AppConstants.h"

#import "BTFault.h"
#import "NLWebServiceError.h"

@implementation NLGetFaultSummaryWebService

- (instancetype)initWithFaultRef:(NSString *)faultRef
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v1/Fault/%@", faultRef];

    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {

        _faultRef = [faultRef copy];
    }
    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];

    NSString *groupKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:groupKey forKey:@"_authKey"];

    return [finalHeaderFieldDic copy];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];

    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
    BTFault *fault = nil;
    BOOL isAuthenticated = [[resultDic valueForKey:@"IsAuthenticated"] boolValue];
    NSString *productGroup;
    NSString *productImage;
    NSArray *faultList = [resultDic valueForKey:@"FaultListModels"];
    if ([faultList isKindOfClass:[NSArray class]]) {

        for (NSDictionary *faultDetail in faultList) {

            productGroup = [[[faultDetail valueForKey:@"productGroup"] validAndNotEmptyStringObject] copy];
            productImage = [[[faultDetail valueForKey:@"productImage"] validAndNotEmptyStringObject] copy];

            NSUInteger openFaultsCount = [[faultDetail valueForKey:@"openFaults"] count];
            if (openFaultsCount > 0) {

                NSDictionary *faultData = [[faultDetail valueForKey:@"openFaults"] objectAtIndex:0];
                fault = [[BTFault alloc] initWithResponseDictionaryFromFaultSummaryAPIResponse:faultData];
                break;
            }

            NSUInteger closedFaultsCount = [[faultDetail valueForKey:@"closedFaults"] count];
            if (closedFaultsCount > 0) {
                NSDictionary *faultData = [[faultDetail valueForKey:@"closedFaults"] objectAtIndex:0];
                fault = [[BTFault alloc] initWithResponseDictionaryFromFaultSummaryAPIResponse:faultData];
                break;
            }

        }
    }

    DDLogInfo(@"Successfully fetched FaultListModels data using webservice with URL: %@", [[task currentRequest] URL]);

    [self.getFaultSummaryWebServiceDelegate getFaultSummaryWebService:self successfullyFetchedFaultSummaryData:fault isAuthenticated:isAuthenticated productGroup:productGroup productImage:productImage];


}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];

    [self.getFaultSummaryWebServiceDelegate getFaultSummaryWebService:self failedToFetchFaultSummaryDataWithWebServiceError:webServiceError];
}


#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil)
    {
        // (SD) If there was no error from superclass, then this class can check for its level of errors.
        NSDictionary *resultDict = [responseObject valueForKey:kNLResponseKeyForResult];
        if(![resultDict isKindOfClass:[NSDictionary class]])
        {
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];

            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }

    return errorToBeReturned;
}



@end
