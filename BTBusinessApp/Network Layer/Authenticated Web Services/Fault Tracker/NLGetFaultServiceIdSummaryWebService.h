//
//  NLGetFaultServiceIdSummaryWebService.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 25/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"

@class NLGetFaultServiceIdSummaryWebService;
@class NLWebServiceError;

@protocol NLGetFaultServiceIdSummaryWebServiceDelegate 

- (void)getFaultSummaryWithServiceIdWebService:(NLGetFaultServiceIdSummaryWebService *)webService successfullyFetchedFaultSummaryWithServiceIdData:(NSArray *)arrayOfServiceIdData withIsAuthenticated:(BOOL)isAuthenticated;

- (void)getFaultSummaryWithServiceIdWebService:(NLGetFaultServiceIdSummaryWebService *)webService failedToFetchFaultSummaryDataWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLGetFaultServiceIdSummaryWebService : NLAuthenticationProtectedWebService {
    
}

@property (nonatomic, readonly) NSString *faultRef;
@property (nonatomic, weak) id <NLGetFaultServiceIdSummaryWebServiceDelegate> getFaultSummaryWithServiceIdWebServiceDelegate;

- (instancetype)initWithFaultRef:(NSString *)faultRef;

@end
