//
//  NLFaultServiceIDSummaryWebService.h
//  BTBusinessApp
//
//  Created by Accolite on 27/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
@class NLFaultServiceIDSummaryWebService;
@class BTFaultSummary;
@class NLWebServiceError;

@protocol NLFaultServiceIDSummaryWebServiceDelegate 

- (void)getFaultServiceIDSummaryWebService:(NLFaultServiceIDSummaryWebService *)webService successfullyFetchedFaultServiceIDSummaryData:(NSArray *)faultList isAuthenticated:(BOOL)isAuthenticated faultSummary:(BTFaultSummary *)faultSummary;

- (void)getFaultServiceIDSummaryWebService:(NLFaultServiceIDSummaryWebService *)webService failedToFetchFaultServiceIDSummaryDataWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLFaultServiceIDSummaryWebService : NLAuthenticationProtectedWebService
@property (nonatomic, weak) id <NLFaultServiceIDSummaryWebServiceDelegate> getFaultServiceIDSummaryWebServiceDelegate;

- (instancetype)initWithServiceID:(NSString *)serviceID;

@end
//https://eric1-dmze2e-ukb.bt.com/api/v1/Faults/01001129173
