//
//  NLGetFaultSummaryDetailsWebService.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 21/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"

@class NLGetFaultSummaryDetailsWebService;
@class BTFault;
@class NLWebServiceError;

@protocol NLGetFaultSummaryDetailsWebServiceDelegate 

- (void)getFaultSummaryDetailsWebService:(NLGetFaultSummaryDetailsWebService *)webService successfullyFetchedFaultSummaryDetailsData:(BTFault *)fault milestoneNodes:(NSArray *)arrayOfMilestoneNodes isAuthenticated:(BOOL)isAuthenticated productGroup:(NSString *)productGroup productImage:(NSString *)productImage;

- (void)getFaultSummaryDetailsWebService:(NLGetFaultSummaryDetailsWebService *)webService failedToFetchFaultSummaryDetailsDataWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLGetFaultSummaryDetailsWebService : NLAuthenticationProtectedWebService {
    
}

@property (nonatomic, readonly) NSString *faultRef;
@property (nonatomic, weak) id <NLGetFaultSummaryDetailsWebServiceDelegate> getFaultSummaryDetailsWebServiceDelegate;

- (instancetype)initWithFaultRef:(NSString *)faultRef;

@end
