//
//  NLFaultServiceIDSummaryWebService.m
//  BTBusinessApp
//
//  Created by Accolite on 27/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLFaultServiceIDSummaryWebService.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "AppConstants.h"

#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NLWebServiceError.h"
#import "BTFault.h"
#import "BTFaultSummary.h"

@implementation NLFaultServiceIDSummaryWebService

- (instancetype)initWithServiceID:(NSString *)serviceID {

    NSString *endPointURLString = [NSString stringWithFormat:@"/Account/api/v1/Faults/%@",serviceID];

    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
    }
    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];

    NSString *appInstallationKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:appInstallationKey forKey:@"_authKey"];

    return [finalHeaderFieldDic copy];
}


#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];

    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];

    BOOL isAuthenticated = NO;
    BTFaultSummary *faultSummary = nil;
    if (resultDic[@"IsAuthenticated"]) {
        NSNumber *authenticatedNumber = resultDic[@"IsAuthenticated"];
        isAuthenticated = [authenticatedNumber boolValue];
    }
    if (resultDic[@"FaultListModels"]) {

        NSArray *faultListModels = resultDic[@"FaultListModels"];
        NSMutableArray *faultList = [[NSMutableArray alloc] init];
        for (NSDictionary *modelDict in faultListModels) {
            if (modelDict) {
                faultSummary = [[BTFaultSummary alloc] initWithResponseDictionaryFromFaultServiceIDAPIResponse:modelDict];
                NSMutableArray *openFaults = [[NSMutableArray alloc] init];
                NSMutableArray *closedFaults = [[NSMutableArray alloc] init];
                if (modelDict[@"openFaults"]) {
                    NSArray *resultOpenFaults = modelDict[@"openFaults"];
                    for (NSDictionary *openFault in resultOpenFaults) {
                        BTFault *fault = [[BTFault alloc] initWithResponseDictionaryFromFaultServiceIDAPIResponse:openFault];
                        [openFaults addObject:fault];
                    }
                }
                if (modelDict[@"closedFaults"]) {
                    NSArray *resultClosedFaults = modelDict[@"closedFaults"];
                    for (NSDictionary *closedFault in resultClosedFaults) {
                        BTFault *fault = [[BTFault alloc] initWithResponseDictionaryFromFaultServiceIDAPIResponse:closedFault];
                        [closedFaults addObject:fault];
                    }
                }


                NSMutableDictionary *faultModelDict = [[NSMutableDictionary alloc] init];
                [faultModelDict setValue:faultSummary.productGroup forKey:@"productGroup"];
                [faultModelDict setValue:faultSummary.productImage forKey:@"productImage"];
                [faultModelDict setValue:openFaults forKey:@"openFaults"];
                [faultModelDict setValue:closedFaults forKey:@"closedFaults"];
                [faultList addObject:faultModelDict];
            }
        }
        DDLogInfo(@"Successfully fetched FaultServiceIDSummary data using webservice with URL: %@", [[task currentRequest] URL]);

        [self.getFaultServiceIDSummaryWebServiceDelegate getFaultServiceIDSummaryWebService:self successfullyFetchedFaultServiceIDSummaryData:faultList isAuthenticated:isAuthenticated faultSummary:faultSummary];
    }
    else
    {
        DDLogError(@"'FaultListModels' element missing in response json object in webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"'FaultListModels' element missing in response json object in webservice with URL: %@", [[task currentRequest] URL]]));

        [self.getFaultServiceIDSummaryWebServiceDelegate getFaultServiceIDSummaryWebService:self failedToFetchFaultServiceIDSummaryDataWithWebServiceError:[NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject]];
    }

}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];

    [self.getFaultServiceIDSummaryWebServiceDelegate getFaultServiceIDSummaryWebService:self failedToFetchFaultServiceIDSummaryDataWithWebServiceError:webServiceError];
}




#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil)
    {
        // (SD) If there was no error from superclass, then this class can check for its level of errors.
        NSDictionary *resultDict = [responseObject valueForKey:kNLResponseKeyForResult];
        if(![resultDict isKindOfClass:[NSDictionary class]])
        {
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];

            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }
    
    return errorToBeReturned;
}





@end
