//
//  NLGetFaultServiceIdSummaryWebService.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 25/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLGetFaultServiceIdSummaryWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NSObject+APIResponseCheck.h"
#import "AppConstants.h"
#import "NLConstants.h"
#import "BTFault.h"
#import "NLWebServiceError.h"

@implementation NLGetFaultServiceIdSummaryWebService

- (instancetype)initWithFaultRef:(NSString *)faultRef
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v1/Faults/%@", faultRef];
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
        
        _faultRef = [faultRef copy];
    }
    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    NSString *groupKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:groupKey forKey:@"_authKey"];
    
    return [finalHeaderFieldDic copy];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];

    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];

    BOOL isAuthenticated = [[resultDic valueForKey:@"IsAuthenticated"] boolValue];

    NSArray *faultList = [resultDic valueForKey:@"FaultListModels"];
    if ([faultList isKindOfClass:[NSArray class]]) {

        NSMutableArray *arrayOfServiceIdData = [NSMutableArray array];

        for (NSDictionary *faultDetail in faultList) {

            NSString *productGroup = [[[faultDetail valueForKey:@"productGroup"] validAndNotEmptyStringObject] copy];
            NSString *productImage = [[[faultDetail valueForKey:@"productImage"] validAndNotEmptyStringObject] copy];
            NSMutableArray *openFaults = [NSMutableArray array];
            NSMutableArray *closedFaults = [NSMutableArray array];

            NSUInteger openFaultsCount = [[faultDetail valueForKey:@"openFaults"] count];
            if (openFaultsCount > 0) {

                NSDictionary *faultData = [[faultDetail valueForKey:@"openFaults"] objectAtIndex:0];
                BTFault *openFault = [[BTFault alloc] initWithResponseDictionaryFromFaultSummaryAPIResponse:faultData];
                [openFaults addObject:openFault];
            }

            NSUInteger closedFaultsCount = [[faultDetail valueForKey:@"closedFaults"] count];
            if (closedFaultsCount > 0) {
                NSDictionary *faultData = [[faultDetail valueForKey:@"closedFaults"] objectAtIndex:0];
                BTFault *closedFault = [[BTFault alloc] initWithResponseDictionaryFromFaultSummaryAPIResponse:faultData];
                [closedFaults addObject:closedFault];
            }

            NSMutableDictionary *faultGroupData = [[NSMutableDictionary alloc] init];
            [faultGroupData setValue:productGroup forKey:@"productGroup"];
            [faultGroupData setValue:productImage forKey:@"productImage"];

            [arrayOfServiceIdData addObject:faultGroupData];
        }

        DDLogInfo(@"Successfully fetched GetFaultServiceIdSummary data using webservice with URL: %@", [[task currentRequest] URL]);
        [self.getFaultSummaryWithServiceIdWebServiceDelegate getFaultSummaryWithServiceIdWebService:self successfullyFetchedFaultSummaryWithServiceIdData:arrayOfServiceIdData withIsAuthenticated:isAuthenticated];
    }
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];

    [self.getFaultSummaryWithServiceIdWebServiceDelegate getFaultSummaryWithServiceIdWebService:self failedToFetchFaultSummaryDataWithWebServiceError:webServiceError];
}



#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil)
    {
        // (SD) If there was no error from superclass, then this class can check for its level of errors.
        NSDictionary *resultDict = [responseObject valueForKey:kNLResponseKeyForResult];
        if(![resultDict isKindOfClass:[NSDictionary class]])
        {
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];

            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }

    return errorToBeReturned;
}

@end
