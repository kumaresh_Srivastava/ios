//
//  NLFaultDashBoardWebService.h
//  BTBusinessApp
//
//  Created by Accolite on 14/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "NLAPIGEEAuthenticatedWebService.h"

@class NLFaultDashBoardWebService;

@protocol NLFaultDashBoardWebServiceDelegate 

- (void)getFaultDashBoardWebService:(NLFaultDashBoardWebService *)webService successfullyFetchedFaultDashBoardData:(NSArray *)faults pageIndex:(int)pageIndex totalSize:(int)totalSize andTabID:(int)tabID;

- (void)getFaultDashBoardWebService:(NLFaultDashBoardWebService *)webService failedToFetchFaultDashBoardDataWithWebServiceError:(NLWebServiceError *)error;

@end


@interface NLFaultDashBoardWebService : NLAPIGEEAuthenticatedWebService

@property (nonatomic, weak) id <NLFaultDashBoardWebServiceDelegate> getFaultDashBoardWebServiceDelegate;

- (instancetype)initWithPageSize:(int)pageSize pageIndex:(int)pageIndex andTabID:(int)tabID;

@end
