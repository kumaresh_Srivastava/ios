//
//  NLGetFaultSummaryWebService.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 16/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"

@class NLGetFaultSummaryWebService;
@class BTFault;
@class NLWebServiceError;

@protocol NLGetFaultSummaryWebServiceDelegate 

- (void)getFaultSummaryWebService:(NLGetFaultSummaryWebService *)webService successfullyFetchedFaultSummaryData:(BTFault *)fault isAuthenticated:(BOOL)isAuthenticated productGroup:(NSString *)productGroup productImage:(NSString *)productImage;

- (void)getFaultSummaryWebService:(NLGetFaultSummaryWebService *)webService failedToFetchFaultSummaryDataWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLGetFaultSummaryWebService : NLAuthenticationProtectedWebService {
    
}

@property (nonatomic, readonly) NSString *faultRef;
@property (nonatomic, weak) id <NLGetFaultSummaryWebServiceDelegate> getFaultSummaryWebServiceDelegate;

- (instancetype)initWithFaultRef:(NSString *)faultRef;

@end


/*
 https://{host}/Account/api/v1/Fault/{faultId}/{assetId} – Fault Detail
 https://{host}/Account/api/v1/Fault/{faultId}/{assetId}/{bac} – Fault Detail by BAC
*/
