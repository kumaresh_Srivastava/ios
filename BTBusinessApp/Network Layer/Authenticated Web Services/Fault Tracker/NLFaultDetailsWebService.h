//
//  NLFaultDetailsWebService.h
//  BTBusinessApp
//
//  Created by Accolite on 21/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "NLAPIGEEAuthenticatedWebService.h"

@class NLFaultDetailsWebService;
@class BTFault;
@class NLWebServiceError;

@protocol NLFaultDetailsWebServiceDelegate 

- (void)getFaultDetailsWebService:(NLFaultDetailsWebService *)webService successfullyFetchedFaultDetailsData:(BTFault *)fault milestoneNodes:(NSArray *)mileStoneNodes;

- (void)getFaultDetailsWebService:(NLFaultDetailsWebService *)webService failedToFetchFaultDetailsDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end


//@interface NLFaultDetailsWebService : NLAPIGEEAuthenticatedWebService
@interface NLFaultDetailsWebService : NLAuthenticationProtectedWebService

@property (nonatomic, weak) id <NLFaultDetailsWebServiceDelegate> getFaultDetailsWebServiceDelegate;

- (instancetype)initWithFaultID:(NSString *)faultID andAssetID:(NSString *)assetID;
- (instancetype)initWithFaultID:(NSString *)faultID assetID:(NSString *)assetID andBac:(NSString *)bac;

@end

//(salman)
@interface NLFaultDetailsCancelWebService : NLFaultDetailsWebService
- (instancetype)initWithFaultID:(NSString *)faultID assetID:(NSString *)assetID;
@end



/*
 https://{host}/Account/api/v1/Fault/{faultId}/{assetId} – Fault Detail
 https://{host}/Account/api/v1/Fault/{faultId}/{assetId}/{bac} – Fault Detail by BAC
 */

/*
Response
 
 
 {
 "isSuccess": true,
 "code": 600,
 "errorMessage": "",
 "result": {
 "FaultDetails": [
 {
 "milestoneDetail": {
 "milestoneName": "Fault Created",
 "milestoneDateTime": "2016-08-31T09:06:34Z",
 "milestoneClass": "icon__circle-tick overlay-grey",
 "milestoneColor": "wrap",
 "milestoneImage": "#icon-circle-tick",
 "milestoneDivClass": "row complete",
 "milestoneDivId": "milestone_0",
 "milestoneText": "You’ve told us about your fault"
 },
 "lstofFaultHistories": []
 },
 {
 "milestoneDetail": {
 "milestoneName": "Fault Submitted",
 "milestoneDateTime": "2016-08-31T09:11:15Z",
 "milestoneClass": "icon__circle-tick overlay-grey",
 "milestoneColor": "wrap",
 "milestoneImage": "#icon-circle-tick",
 "milestoneDivClass": "row complete",
 "milestoneDivId": "milestone_1",
 "milestoneText": "We've sent your details to our faults team"
 },
 "lstofFaultHistories": []
 },
 {
 "milestoneDetail": {
 "milestoneName": "Acknowledged",
 "milestoneDateTime": "2016-08-31T09:12:03Z",
 "milestoneClass": "icon__circle2 overlay-blue",
 "milestoneColor": "wrap panel callout",
 "milestoneImage": "#icon-circle2",
 "milestoneDivClass": "row incomplete",
 "milestoneDivId": "milestone_2",
 "milestoneText": "Our faults team are looking into your fault and deciding how they can fix it"
 },
 "lstofFaultHistories": []
 },
 {
 "milestoneDetail": {
 "milestoneName": "Engineer Assigned",
 "milestoneDateTime": "0001-01-01T00:00:00Z",
 "milestoneClass": "icon__circle2 overlay-blue",
 "milestoneColor": "wrap",
 "milestoneImage": "#icon-circle2",
 "milestoneDivClass": "row incomplete",
 "milestoneDivId": "milestone_3",
 "milestoneText": "We’ve given the fault to an engineer and they’re now fixing it. If we need access to your premises we’ll already have told you.  You cannot make any changes to appointments or contact details at this stage"
 }
 },
 {
 "milestoneDetail": {
 "milestoneName": "Fixed",
 "milestoneDateTime": "0001-01-01T00:00:00Z",
 "milestoneClass": "icon__circle2 overlay-blue",
 "milestoneColor": "wrap",
 "milestoneImage": "#icon-circle2",
 "milestoneDivClass": "row incomplete",
 "milestoneDivId": "milestone_4",
 "milestoneText": "We think we’ve fixed your fault. Sometimes we may need you to confirm that everything’s working OK"
 }
 },
 {
 "milestoneDetail": {
 "milestoneName": "Closed",
 "milestoneDateTime": "0001-01-01T00:00:00Z",
 "milestoneClass": "icon__circle2 overlay-blue",
 "milestoneColor": "wrap",
 "milestoneImage": "#icon-circle2",
 "milestoneDivClass": "row incomplete",
 "milestoneDivId": "milestone_5",
 "milestoneText": "Yes, we’ve fixed your fault and you’ve confirmed that you’re happy with it!"
 }
 },
 {
 "milestoneDetail": {
 "milestoneName": "Engineer Assigned",
 "milestoneDateTime": "0001-01-01T00:00:00Z",
 "milestoneClass": "icon__circle2 overlay-blue",
 "milestoneColor": "wrap",
 "milestoneImage": "#icon-circle2",
 "milestoneDivClass": "row incomplete",
 "milestoneDivId": "milestone_6",
 "milestoneText": "We’ve given the fault to an engineer and they’re now fixing it. If we need access to your premises we’ll already have told you.  You cannot make any changes to appointments or contact details at this stage"
 }
 },
 {
 "milestoneDetail": {
 "milestoneName": "Fixed",
 "milestoneDateTime": "0001-01-01T00:00:00Z",
 "milestoneClass": "icon__circle2 overlay-blue",
 "milestoneColor": "wrap",
 "milestoneImage": "#icon-circle2",
 "milestoneDivClass": "row incomplete",
 "milestoneDivId": "milestone_7",
 "milestoneText": "We think we’ve fixed your fault. Sometimes we may need you to confirm that everything’s working OK"
 }
 },
 {
 "milestoneDetail": {
 "milestoneName": "Closed",
 "milestoneDateTime": "0001-01-01T00:00:00Z",
 "milestoneClass": "icon__circle2 overlay-blue",
 "milestoneColor": "wrap",
 "milestoneImage": "#icon-circle2",
 "milestoneDivClass": "row incomplete",
 "milestoneDivId": "milestone_8",
 "milestoneText": "Yes, we’ve fixed your fault and you’ve confirmed that you’re happy with it!"
 }
 }
 ],
 "faultId": "1-190259869",
 "serviceId": "01005978839",
 "statusOnUI": "Fault in progress",
 "cugId": "h9v1FH6NMrMyAf3AfWsi7Q==",
 "colourforStatus": "status-orange",
 "targetFixDate": "02 Sep 2016",
 "faultLocation": "No fault found on BT's network",
 "currentMilestone": "Acknowledged",
 "textonUI": "We're still working on this fault, but you can amend your details if you need to.",
 "btnPrimaryText": "Amend",
 "btnSecondaryText": "Cancel",
 "btnPrimaryHide": false,
 "btnSecondaryHide": false,
 "btnPrimaryClickID": 8,
 "btnSecondaryClickID": 3,
 "currentMilestoneDesc": "The faults team are looking into your fault and deciding how they can fix it.",
 "nextMilestoneDesc": "\r\n      The faults team will assign an engineer to take care of your fault. Depending on what’s causing the fault, they may need to come out and visit your premises.\r\n    "
 }
 }

*/
