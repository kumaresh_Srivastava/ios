//
//  NLGetFaultSiteContactWebService.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 11/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLGetFaultContactDetailsWebService.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "AppConstants.h"

#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "BTFaultContactDetails.h"
#import "NLWebServiceError.h"

@implementation NLGetFaultContactDetailsWebService

- (instancetype)initWithFaultRef:(NSString *)faultRef
{
    //https://api-test1.ee.co.uk/bt-business-auth/v1/faults/{faultId}/contacts
    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/faults/%@/contacts", faultRef];
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
    }
    return self;
}


#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask
- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    NSString *appInstallationKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:appInstallationKey forKey:@"_authKey"];
    
    return [finalHeaderFieldDic copy];
}


#pragma mark - NLWebService Response Handling Private Methods
- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
    
    NSDictionary *contactModel = [resultDic valueForKey:@"contactModel"];
    
    BOOL showCallDivert = [[resultDic valueForKey:@"showCallDivert"] boolValue];
    BOOL showAppointment = [[resultDic valueForKey:@"showAppointment"] boolValue];
    
    DDLogInfo(@"Successfully fetched fault contact details data using webservice with URL: %@", [[task currentRequest] URL]);
    
    BTFaultContactDetails *contactDetails = [[BTFaultContactDetails alloc] initFaultContactDetailWithResponse:contactModel];
    
    [self.getContactDetailsWebServiceDelegate getContactDetailsWebService:self successfullyFetchedFaultContactDetails:contactDetails withShowCallDivert:showCallDivert andShowAppointment:showAppointment];
}


- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.getContactDetailsWebServiceDelegate getContactDetailsWebService:self failedToFetchSiteContactsWithWebServiceError:webServiceError];
}


#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (RLM) If there was no error from superclass, then this class can check for its level of errors.
        NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
        if([resultDic isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *contactModel = [resultDic valueForKey:@"contactModel"];
            
            if(contactModel && [contactModel isKindOfClass:[NSDictionary class]])
            {
                //Do nothing
            }
            else
            {
                errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
                
                DDLogError(@"'contactModel' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
                
            }
        }
        else
        {
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
            
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }
    
    return errorToBeReturned;
}

#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;
    
    return nil;
}

#pragma mark - Private Helper Methods
- (NSString *)staticDataFileName
{
    return @"faultContactDetailsResponse1";
}
@end
