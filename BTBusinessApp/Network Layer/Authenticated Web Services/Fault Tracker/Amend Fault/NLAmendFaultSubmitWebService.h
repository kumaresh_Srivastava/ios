//
//  NLAmendFaultSubmitWebService.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 12/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "NLAPIGEEAuthenticatedWebService.h"

@class NLAmendFaultSubmitWebService;
@class NLWebServiceError;

@protocol NLAmendFaultSubmitWebServiceDelegate 

- (void)faultAmendSubmissionSuccesfullyFinishedByAmendFaultSubmitWebService:(NLAmendFaultSubmitWebService *)webService;

- (void)amendFaultSubmitWebService:(NLAmendFaultSubmitWebService *)webService failedToSubmitAmendOrderWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface NLAmendFaultSubmitWebService : NLAPIGEEAuthenticatedWebService

@property (nonatomic, weak) id <NLAmendFaultSubmitWebServiceDelegate> amendFaultSubmitWebServiceDelegate;

- (instancetype)initWithAmendFaultRequestDictionary:(NSDictionary *)amendFaultRequestDic andFaultReference:(NSString *)faultRef;

@end
