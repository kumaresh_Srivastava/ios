//
//  NLGetFaultSiteContactWebService.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 11/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "NLAPIGEEAuthenticatedWebService.h"

@class NLGetFaultContactDetailsWebService;
@class BTFaultContactDetails;
@class NLWebServiceError;

@protocol NLGetFaultSiteContactWebServiceDelegate 

- (void)getContactDetailsWebService:(NLGetFaultContactDetailsWebService *)webService successfullyFetchedFaultContactDetails:(BTFaultContactDetails *)faultContactDetails withShowCallDivert:(BOOL)showCallDivert andShowAppointment:(BOOL)showAppointment;

- (void)getContactDetailsWebService:(NLGetFaultContactDetailsWebService *)webService failedToFetchSiteContactsWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface NLGetFaultContactDetailsWebService : NLAPIGEEAuthenticatedWebService

@property (nonatomic, weak) id <NLGetFaultSiteContactWebServiceDelegate> getContactDetailsWebServiceDelegate;

- (instancetype)initWithFaultRef:(NSString *)faultRef;

@end
