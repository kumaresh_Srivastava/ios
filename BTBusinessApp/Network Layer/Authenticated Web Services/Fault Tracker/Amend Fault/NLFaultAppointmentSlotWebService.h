//
//  NLFaultAppointmentSlotWebService.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 14/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "NLAPIGEEAuthenticatedWebService.h"
#import "BTFaultAppointmentSlotDetail.h"

@class NLFaultAppointmentSlotWebService;
@class NLWebServiceError;

@protocol NLFaultAppointmentSlotWebServiceDelegate 

- (void)faultAppointmentSlotWebService:(NLFaultAppointmentSlotWebService *)webService successfullyFetchedFaultAppointmentSlotWebServiceData:(BTFaultAppointmentSlotDetail *)faultAppointmentSlotDetail;

- (void)faultAppointmentWebService:(NLFaultAppointmentSlotWebService *)webService failedToFetchFaultAppointmentSlotWebServiceDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface NLFaultAppointmentSlotWebService : NLAPIGEEAuthenticatedWebService

@property (nonatomic, weak) id <NLFaultAppointmentSlotWebServiceDelegate> getfaultAppointmentSlotWebServiceDelegateDelegate;

- (instancetype)initWithFaultID:(NSString *)faultID  andInputDate:(NSString *)inputDate;

@end
