//
//  NLFaultAppointmentWebService.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 11/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLFaultAppointmentWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "AppConstants.h"
#import "BTFaultAppointmentDetail.h"
#import "AppManager.h"
#import "NSObject+APIResponseCheck.h"
#import "BTFaultAppointment.h"
#import "NLFaultAppointmentAccessSlotWebService.h"
#import "NLWebServiceError.h"

@implementation NLFaultAppointmentWebService


- (instancetype)initWithFaultID:(NSString *)faultID andInputDate:(NSString *)inputDate
{
    //https://api-test1.ee.co.uk/bt-business-auth/v1/faults/{faultId}/appointments?date={date}
    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/faults/%@/appointments?date=%@", faultID, inputDate];

    DDLogInfo(@"INFO: FaultAppointment:%@",endPointURLString);
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];

    if(self)
    {

    }

    return self;

}


#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];

    NSString *appInstallationKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:appInstallationKey forKey:@"_authKey"];

    return [finalHeaderFieldDic copy];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    BTFaultAppointmentDetail *faultAppointmentDetail = nil;
    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
    
     DDLogInfo(@"Successfully fetched NLFaultAppointmentWebService data using webservice with URL: %@", [[task currentRequest] URL]);
    
   NSDate *bookedStartDate = nil;
    NSDate *bookedEndDate = nil;
    //NSDate *presentDate = nil;
    NSArray *appointmentsDictArray = nil;
    NSMutableArray *appointmentsArray = [NSMutableArray array];
    bookedStartDate = [AppManager NSDateWithUTCFormatFromNSString:[[resultDic[@"apptBookedStartDate"] validAndNotEmptyStringObject] copy]];
    bookedEndDate = [AppManager NSDateWithUTCFormatFromNSString:[[resultDic[@"apptBookedEndDate"] validAndNotEmptyStringObject] copy]];
   // presentDate = [AppManager NSDateWithUTCFormatFromNSString:[[resultDic[@"presentDateTime"] validAndNotEmptyStringObject] copy]];
    
    BOOL isAccessTimeSlot = [[resultDic valueForKey:@"isHourAccessPresent"] boolValue];
    
    NSDate *currentAppointmentDate = bookedEndDate;
    
    if(isAccessTimeSlot){
        
        currentAppointmentDate = bookedStartDate;
    }
    
    appointmentsDictArray =  [resultDic valueForKey:@"lstApptSlots"];
    
    if(appointmentsDictArray && [appointmentsDictArray isKindOfClass:[NSArray class]])
    {
        for (NSDictionary *appointmentDict in appointmentsDictArray) {
            
            BTFaultAppointment *faultAppointment = [[BTFaultAppointment alloc] initWithResponseDictionaryFromFaultAppointmentsAPIResponse:appointmentDict];

            if(faultAppointment)
            {
                
                   if ([AppManager isDate:[NSDate date] isEqualTo:faultAppointment.dateInfo]){
                       
                       NSInteger hour = [AppManager getHourFromDate:[NSDate date]];
                       if (hour > 12) {
                           
                           [faultAppointment setAMSlot:NO];
                       }
                    }
    
                    [appointmentsArray addObject:faultAppointment];
                
            }
            
        }
    }
    
    faultAppointmentDetail = [[BTFaultAppointmentDetail alloc] initWithFaultAppointmentsArray:appointmentsArray andAPIResponseDict:resultDic];
    
    
    if(faultAppointmentDetail){
        
        [self.getfaultAppointmentWebServiceDelegateDelegate faultAppointmentWebService:self successfullyFetchedfaultAppointmentWebServiceData:faultAppointmentDetail];
    }
    else{
        
        DDLogError(@"Unable to create BTFaultAppointmentDetail model object using response json object in webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Unable to create BTFaultAppointmentDetail model object using response json object in webservice with URL: %@", [[task currentRequest] URL]]));
        
        NLWebServiceError *webServiceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];

         [self.getfaultAppointmentWebServiceDelegateDelegate faultAppointmentWebService:self failedToFetchFaultAppointmentWebServiceDataWithWebServiceError:webServiceError];

    }


}


- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];

   [self.getfaultAppointmentWebServiceDelegateDelegate faultAppointmentWebService:self failedToFetchFaultAppointmentWebServiceDataWithWebServiceError:webServiceError];
}



#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        //(Sal) If there was no error from superclass, then this class can check for its level of errors.
        NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
        if(![resultDic isKindOfClass:[NSDictionary class]])
        {

            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }
    
    return errorToBeReturned;
}


//Temporary to load mock response
/*
- (void)resume {
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"FaultAppointmentModel" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    [self processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:nil andResponseObject:json];
    [self handleSuccessWithSessionDataTask:nil andResponseObject:json];
}
 */

@end
