//
//  NLFaultAppointmentSlotWebService.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 14/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLFaultAppointmentSlotWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "AppConstants.h"
#import "BTFaultAppointmentSlotDetail.h"
#import "AppManager.h"
#import "NSObject+APIResponseCheck.h"
#import "BTFaultAppointment.h"

#import "NLWebServiceError.h"

@implementation NLFaultAppointmentSlotWebService


- (instancetype)initWithFaultID:(NSString *)faultID andInputDate:(NSString *)inputDate
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/faults/%@/appointment-slots?date=%@", faultID, inputDate];
   
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
        
    }
    
    return self;
    
}



#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask
- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    NSString *appInstallationKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:appInstallationKey forKey:@"_authKey"];
    
    return [finalHeaderFieldDic copy];
}


#pragma mark - NLWebService Response Handling Private Methods
- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    BTFaultAppointmentSlotDetail *faultAppointmentSlotDetail = nil;
    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
    
    DDLogInfo(@"Successfully fetched falt appointment slowt webservice data using webservice with URL: %@", [[task currentRequest] URL]);
    
    NSDate *previousSlotDate = nil;
    NSDate *laterSlotDate = nil;
    NSArray *appointmentsDictArray = nil;
    NSMutableArray *appointmentsArray = [NSMutableArray array];
    previousSlotDate = [AppManager NSDateWithUTCFormatFromNSString:[[resultDic[@"previousSlotDate"] validAndNotEmptyStringObject] copy]];
    laterSlotDate = [AppManager NSDateWithUTCFormatFromNSString:[[resultDic[@"laterSlotDate"] validAndNotEmptyStringObject] copy]];
    
    
    appointmentsDictArray =  [resultDic valueForKey:@"lstApptSlots"];
    if(appointmentsDictArray && [appointmentsDictArray isKindOfClass:[NSArray class]])
    {
        for (NSDictionary *appointmentDict in appointmentsDictArray) {
            
            BTFaultAppointment *faultAppointment = [[BTFaultAppointment alloc] initWithResponseDictionaryFromFaultAppointmentsAPIResponse:appointmentDict];
            if(faultAppointment)
            {
                [appointmentsArray addObject:faultAppointment];
            }
        }
    }
    
    //Fault Detail
    faultAppointmentSlotDetail = [[BTFaultAppointmentSlotDetail alloc] initWithFaultAppointmentSlotArray:appointmentsArray priviousSlotDate:previousSlotDate andLaterSlotDate:laterSlotDate];
    
    
    [self.getfaultAppointmentSlotWebServiceDelegateDelegate faultAppointmentSlotWebService:self successfullyFetchedFaultAppointmentSlotWebServiceData:faultAppointmentSlotDetail];
    
    
}


- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    [self.getfaultAppointmentSlotWebServiceDelegateDelegate faultAppointmentWebService:self failedToFetchFaultAppointmentSlotWebServiceDataWithWebServiceError:webServiceError];
}


#pragma mark - Override Helper Methods For Error Handling
- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (RLM) If there was no error from superclass, then this class can check for its level of errors.
        NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
        if(![resultDic isKindOfClass:[NSDictionary class]])
        {
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
            
            
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }
    
    return errorToBeReturned;
}


#pragma mark - Dummy Unit Testing Methods
- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;
    
    return nil;
}


#pragma mark - Private Helper Methods

- (NSString *)staticDataFileName
{
    return @"FaultAppointmentSlotsResponse1";
}



//Temporary to load mock response
/*
- (void)resume {
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"FaultAppointmentSlots" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    [self processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:nil andResponseObject:json];
    [self handleSuccessWithSessionDataTask:nil andResponseObject:json];
}
*/

@end
