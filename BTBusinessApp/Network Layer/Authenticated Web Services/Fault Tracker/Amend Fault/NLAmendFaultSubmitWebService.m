//
//  NLAmendFaultSubmitWebService.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 12/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAmendFaultSubmitWebService.h"

#import "AFNetworking.h"
#import "NLConstants.h"
#import "AppConstants.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "CDApp.h"
#import "NSObject+APIResponseCheck.h"
#import "NLWebServiceError.h"

@implementation NLAmendFaultSubmitWebService

- (instancetype)initWithAmendFaultRequestDictionary:(NSDictionary *)amendFaultRequestDic andFaultReference:(NSString *)faultRef
{
    NSError *errorWhileCreatingJSONData = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:amendFaultRequestDic options:0 error:&errorWhileCreatingJSONData];
    
    if(errorWhileCreatingJSONData)
    {
        DDLogError(@"Error while creating JSON data for AmendFaultRequestDictionary - %@",errorWhileCreatingJSONData.localizedDescription);
    }
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:4];
    
    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/faults/%@", faultRef];
    
    self = [super initWithMethod:@"PUT" parameters:jsonString andEndpointUrlString:endPointURLString];
    
    if(self)
    {
        
    }
    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (AFHTTPSessionManager *)sessionManager
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSString *baseURLString = [NSString stringWithFormat:@"https://%@", [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel]];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseURLString] sessionConfiguration:configuration];
    
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    [manager.requestSerializer setTimeoutInterval:_timeOutTimeInterval];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // (hd) Because the API call expects the parameters in raw format, hence we are setting an empty query string serializer so that it does not do any serialization.
    [manager.requestSerializer setQueryStringSerializationWithBlock:^NSString * _Nonnull(NSURLRequest * _Nonnull request, id  _Nonnull parameters, NSError * _Nullable __autoreleasing * _Nullable error) {
        
        return parameters;
    }];
    
    return manager;
}

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    NSString *groupKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:groupKey forKey:@"_authKey"];
    
    return [finalHeaderFieldDic copy];
}



#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSString *resultString = [responseObject valueForKey:kNLResponseKeyForResult];
    
    if([[resultString lowercaseString] isEqualToString:@"true"])
    {
        DDLogInfo(@"Successfully Amend Fault Submitted using webservice with URL: %@", [[task currentRequest] URL]);
        [self.amendFaultSubmitWebServiceDelegate faultAmendSubmissionSuccesfullyFinishedByAmendFaultSubmitWebService:self];
    }
    else
    {
        DDLogError(@"Amend failed using webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Amend failed using webservice with URL: %@", [[task currentRequest] URL]]));
        
        // TODO: (LP) 12/11/2016 Need to confirm with harman for error code
        NLWebServiceError *webServiceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
    
        [self.amendFaultSubmitWebServiceDelegate amendFaultSubmitWebService:self failedToSubmitAmendOrderWithWebServiceError:webServiceError];
    }
    
    
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.amendFaultSubmitWebServiceDelegate amendFaultSubmitWebService:self failedToSubmitAmendOrderWithWebServiceError:webServiceError];
}

#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (Sal) If there was no error from superclass, then this class can check for its level of errors.
        NSString *resultString = [responseObject valueForKey:kNLResponseKeyForResult];
        if(![resultString isKindOfClass:[NSString class]])
        {
            
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
            
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }
    
    return errorToBeReturned;
}



#pragma mark - Private Helper Methods
- (NSString *)staticDataFileName
{
    return @"";
}

@end
