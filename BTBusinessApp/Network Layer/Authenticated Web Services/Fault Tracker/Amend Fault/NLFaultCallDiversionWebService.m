//
//  NLFaultCallDiversionWebService.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 11/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLFaultCallDiversionWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "BTCallDiversionDetail.h"
#import "AppConstants.h"

#import "NLWebServiceError.h"

@interface NLFaultCallDiversionWebService()
{
    NSString *_faultID;
}
@end

@implementation NLFaultCallDiversionWebService


- (instancetype)initWithFaultID:(NSString *)faultID {
    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/faults/%@/call-diverts",faultID];

    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];

    if(self)
    {
        _faultID = [faultID copy];
    }

    return self;
}


#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];

    NSString *groupKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:groupKey forKey:@"_authKey"];

    return [finalHeaderFieldDic copy];
}


#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];


    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];

    BTCallDiversionDetail *callDiversionDetail = [[BTCallDiversionDetail alloc] initCallDiversionDetailWithResponse:resultDic];
    if(callDiversionDetail)
    {
        DDLogInfo(@"Successfully fetched Call Diversion data using webservice with URL: %@", [[task currentRequest] URL]);

        [self.getFaultCallDiversionWebServiceDelegate faultCallDiversionWebService:self successfullyFetchedFaultCallDiversionData:callDiversionDetail];
    }
    else
    {
        [self.getFaultCallDiversionWebServiceDelegate faultCallDiversionWebService:self failedToFetchFaultCallDiversionDataWithWebServiceError:[NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject]];
    }
}



- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];

    [self.getFaultCallDiversionWebServiceDelegate faultCallDiversionWebService:self failedToFetchFaultCallDiversionDataWithWebServiceError:webServiceError];
    
}


#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil)
    {
        // (SD) If there was no error from superclass, then this class can check for its level of errors.
        NSDictionary *resultDict = [responseObject valueForKey:kNLResponseKeyForResult];
        if(![resultDict isKindOfClass:[NSDictionary class]])
        {
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];

            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }

    return errorToBeReturned;
}




#pragma mark - Private Helper Methods

- (NSString *)staticDataFileName
{
    return @"FaultCallDiversionResponse1";
}





@end


