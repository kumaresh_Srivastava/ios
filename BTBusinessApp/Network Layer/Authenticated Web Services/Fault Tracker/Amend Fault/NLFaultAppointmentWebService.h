//
//  NLFaultAppointmentWebService.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 11/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "NLAPIGEEAuthenticatedWebService.h"
#import "BTFaultAppointmentDetail.h"

@class NLFaultAppointmentWebService;
@class NLWebServiceError;

@protocol NLFaultAppointmentWebServiceDelegate 

- (void)faultAppointmentWebService:(NLFaultAppointmentWebService *)webService successfullyFetchedfaultAppointmentWebServiceData:(BTFaultAppointmentDetail *)faultAppointmentDetail;

- (void)faultAppointmentWebService:(NLFaultAppointmentWebService *)webService failedToFetchFaultAppointmentWebServiceDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end



@interface NLFaultAppointmentWebService : NLAPIGEEAuthenticatedWebService

@property (nonatomic, weak) id <NLFaultAppointmentWebServiceDelegate> getfaultAppointmentWebServiceDelegateDelegate;

- (instancetype)initWithFaultID:(NSString *)faultID andInputDate:(NSString *)inputDate;

@end
