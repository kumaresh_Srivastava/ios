//
//  NLFaultCallDiversionWebService.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 11/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "NLAPIGEEAuthenticatedWebService.h"

@class NLFaultCallDiversionWebService;
@class BTCallDiversionDetail;
@class NLWebServiceError;

@protocol NLFaultCallDiversionWebServiceDelegate 

- (void)faultCallDiversionWebService:(NLFaultCallDiversionWebService *)webService successfullyFetchedFaultCallDiversionData:(BTCallDiversionDetail *)callDiversionDetail;

- (void)faultCallDiversionWebService:(NLFaultCallDiversionWebService *)webService failedToFetchFaultCallDiversionDataWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLFaultCallDiversionWebService : NLAPIGEEAuthenticatedWebService


@property (nonatomic, weak) id <NLFaultCallDiversionWebServiceDelegate> getFaultCallDiversionWebServiceDelegate;

- (instancetype)initWithFaultID:(NSString *)faultID;



@end
