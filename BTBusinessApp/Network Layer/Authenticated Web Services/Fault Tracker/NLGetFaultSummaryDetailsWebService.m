//
//  NLGetFaultSummaryDetailsWebService.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 21/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLGetFaultSummaryDetailsWebService.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "AppConstants.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "BTFault.h"
#import "BTFaultMilestoneNode.h"
#import "NLWebServiceError.h"
#import "BTFaultAppointmentDetail.h"

@implementation NLGetFaultSummaryDetailsWebService

- (instancetype)initWithFaultRef:(NSString *)faultRef {
    NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v2/Fault/%@", faultRef];

    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
        _faultRef = [faultRef copy];
    }
    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];

    NSString *groupKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:groupKey forKey:@"_authKey"];

    return [finalHeaderFieldDic copy];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];

    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];

    BTFault *fault = nil;
    BOOL isAuthenticated = [[resultDic valueForKey:@"IsAuthenticated"] boolValue];
    NSString *productGroup = [[[resultDic valueForKey:@"productGroup"] validAndNotEmptyStringObject] copy];
    NSString *productImage = [[[resultDic valueForKey:@"productImage"] validAndNotEmptyStringObject] copy];
    NSMutableArray *arrayToAddNodes = [NSMutableArray array];

    NSDictionary *faultDic = [resultDic valueForKey:@"faultSummary"];
    if ([faultDic isKindOfClass:[NSDictionary class]])
    {
        fault = [[BTFault alloc] initWithResponseDictionaryFromFaultSummaryAPIResponse:faultDic];
    }
    else
    {
        DDLogError(@"'faultSummary' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]);

        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"'faultSummary' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]]));
        
        [self.getFaultSummaryDetailsWebServiceDelegate getFaultSummaryDetailsWebService:self failedToFetchFaultSummaryDetailsDataWithWebServiceError:[NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponse]];
    }

    if (isAuthenticated) {
        NSDictionary *faultDetails = [resultDic valueForKey:@"faultDetail"];
        if ([faultDetails isKindOfClass:[NSDictionary class]]) {

            [fault updateFaultWithFullDetailsWithData:faultDetails];
            
            NSDictionary *appointmentDetail = [faultDetails valueForKey:@"appointmentDetail"];
            if ([appointmentDetail isKindOfClass:[NSDictionary class]])
            {
                BTFaultAppointmentDetail *appointmentDetails = [[BTFaultAppointmentDetail alloc] initWithFaultAppointmentDetail:appointmentDetail];
                [fault updateFaultWithAppointmentDetails:appointmentDetails];
            }

            NSArray *milestoneDetails = [faultDetails valueForKey:@"FaultDetails"];
            if ([milestoneDetails isKindOfClass:[NSArray class]]) {

                for (NSDictionary *milestoneData in milestoneDetails) {
                    BTFaultMilestoneNode *node = [[BTFaultMilestoneNode alloc] initMilestoneNodeWithResponseDic:[milestoneData valueForKey:@"milestoneDetail"]];
                    [arrayToAddNodes addObject:node];
                }
            }
            else
            {
                DDLogError(@"'FaultDetails' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]);
                
                BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"'FaultDetails' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]]));

                [self.getFaultSummaryDetailsWebServiceDelegate getFaultSummaryDetailsWebService:self failedToFetchFaultSummaryDetailsDataWithWebServiceError:[NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponse]];
            }
        }
        else
        {
            DDLogError(@"'faultDetail' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]);

            BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"'faultDetail' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]]));
            
            [self.getFaultSummaryDetailsWebServiceDelegate getFaultSummaryDetailsWebService:self failedToFetchFaultSummaryDetailsDataWithWebServiceError:[NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponse]];
        }

        DDLogInfo(@"Successfully fetched GetFaultSummaryDetails data using webservice with URL: %@", [[task currentRequest] URL]);

        [self.getFaultSummaryDetailsWebServiceDelegate getFaultSummaryDetailsWebService:self successfullyFetchedFaultSummaryDetailsData:fault milestoneNodes:arrayToAddNodes isAuthenticated:isAuthenticated productGroup:productGroup productImage:productImage];

    }




}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];

    [self.getFaultSummaryDetailsWebServiceDelegate getFaultSummaryDetailsWebService:self failedToFetchFaultSummaryDetailsDataWithWebServiceError:webServiceError];
}




#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil)
    {
        // (SD) If there was no error from superclass, then this class can check for its level of errors.
        NSDictionary *resultDict = [responseObject valueForKey:kNLResponseKeyForResult];
        if(![resultDict isKindOfClass:[NSDictionary class]])
        {
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];

            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }

    return errorToBeReturned;
}


- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    
//     NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeAPINoDataFound userInfo:nil];
//     
//     NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
//     
//     return errorToBeReturned;
    
    return nil;
}

@end
