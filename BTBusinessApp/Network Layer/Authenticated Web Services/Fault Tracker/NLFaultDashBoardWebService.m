//
//  NLFaultDashBoardWebService.m
//  BTBusinessApp
//
//  Created by Accolite on 14/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLFaultDashBoardWebService.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "AppConstants.h"

#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "BTFault.h"
#import "NLWebServiceError.h"
#import "AppManager.h"

@implementation NLFaultDashBoardWebService {
    int _tabID, _pageIndex;
}


- (instancetype)initWithPageSize:(int)pageSize pageIndex:(int)pageIndex andTabID:(int)tabID
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/faults/%@?pageSize=%d&index=%d&tabId=%d",[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey, pageSize, pageIndex, tabID];
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
        _tabID = tabID;
        _pageIndex = pageIndex;
    }
    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    NSString *groupKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:groupKey forKey:@"_authKey"];
    
    return [finalHeaderFieldDic copy];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
    
    if ([resultDic valueForKey:@"PageIndex"])
    {
        NSNumber *pageIndexNumber = resultDic[@"PageIndex"];
        _pageIndex = (int)pageIndexNumber.integerValue;
    }
    
    int totalSize = 0;
    if ([resultDic valueForKey:@"TotalSize"])
    {
        NSNumber *totalSizeNumber = resultDic[@"TotalSize"];
        totalSize = (int)totalSizeNumber.integerValue;
    }
    
    if ([resultDic valueForKey:@"Faults"])
    {
        NSArray *resultFaults = resultDic[@"Faults"];
        
        if ([resultFaults isKindOfClass:[NSArray class]])
        {
            NSMutableArray *faults = [[NSMutableArray alloc] init];
            
            for (NSDictionary *orderDict in resultFaults) {
                
                BTFault *fault = [[BTFault alloc] initWithResponseDictionaryFromFaultDashBoardAPIResponse:orderDict];
                [faults addObject:fault];
            }
            
            DDLogInfo(@"Successfully fetched FaultDashBoard data using webservice with URL: %@", [[task currentRequest] URL]);
            
            [self.getFaultDashBoardWebServiceDelegate getFaultDashBoardWebService:self
                                            successfullyFetchedFaultDashBoardData:faults
                                                                        pageIndex:_pageIndex
                                                                        totalSize:totalSize
                                                                         andTabID:_tabID];
        }
        else
        {
            NLWebServiceError *webServiceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
            
            DDLogError(@"'Faults' element is not in the correct format in response json object in webservice with URL: %@", [[task currentRequest] URL]);
            
            BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"'Faults' element is not in the correct format in response json object in webservice with URL: %@", [[task currentRequest] URL]]));
            
            [self.getFaultDashBoardWebServiceDelegate getFaultDashBoardWebService:self failedToFetchFaultDashBoardDataWithWebServiceError:webServiceError];
        }
    }
    else
    {
        NLWebServiceError *webServiceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
        
        DDLogError(@"'Faults' element missing in response json object in webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"'Faults' element missing in response json object in webservice with URL: %@", [[task currentRequest] URL]]));
        
        [self.getFaultDashBoardWebServiceDelegate getFaultDashBoardWebService:self failedToFetchFaultDashBoardDataWithWebServiceError:webServiceError];
    }
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.getFaultDashBoardWebServiceDelegate getFaultDashBoardWebService:self failedToFetchFaultDashBoardDataWithWebServiceError:webServiceError];
}


#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        
        NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
        if([resultDic isKindOfClass:[NSDictionary class]])
        {
            // (lpm) Do Nothing Here.
        }
        else
        {
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
            
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }
    
    return errorToBeReturned;
}



#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
//    NLWebServiceError *errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeAPINoDataFound];
//    
//    return errorToBeReturned;
    
    return nil;
}

@end
