//
//  NLFaultDetailsWebService.m
//  BTBusinessApp
//
//  Created by Accolite on 21/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLFaultDetailsWebService.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "AppConstants.h"
#import "BTFaultAppointmentDetail.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "BTFault.h"
#import "BTFaultMilestoneNode.h"
#import "NLWebServiceError.h"
#import "NLWebService+BackEndAPIErrorHandling.h"

@implementation NLFaultDetailsWebService

- (instancetype)initWithFaultID:(NSString *)faultID andAssetID:(NSString *)assetID {
//    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/faults/%@/%@",faultID,assetID];
    NSString *endPointURLString = [NSString stringWithFormat:@"/Account/api/v1/Fault/%@/%@",faultID,assetID];
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
    }
    return self;
}

- (instancetype)initWithFaultID:(NSString *)faultID assetID:(NSString *)assetID andBac:(NSString *)bac{
    
    NSString *endPointURLString = [NSString stringWithFormat:@"/Account/api/v1/Fault/%@/%@/%@",faultID,assetID,bac];
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
    }
    return self;
}


#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    NSString *appInstallationKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:appInstallationKey forKey:@"_authKey"];
    
    return [finalHeaderFieldDic copy];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    
    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
    
    BTFault *fault = nil;
    NSMutableArray *arrayToAddNodes = [NSMutableArray array];
    
    fault = [[BTFault alloc] initWithResponseDictionaryFromFaultDetailsAPIRespopnse:resultDic];
    
    NSDictionary *appointmentDetail = [resultDic valueForKey:@"appointmentDetail"];
    if ([appointmentDetail isKindOfClass:[NSDictionary class]])
    {
        BTFaultAppointmentDetail *appointmentDetails = [[BTFaultAppointmentDetail alloc] initWithFaultAppointmentDetail:appointmentDetail];
        [fault updateFaultWithAppointmentDetails:appointmentDetails];
    }
    
    NSArray *faultDetails = [resultDic valueForKey:@"FaultDetails"];
    if ([faultDetails isKindOfClass:[NSArray class]]) {
        
        for (NSDictionary *milestoneData in faultDetails) {
            
            BTFaultMilestoneNode *node = [[BTFaultMilestoneNode alloc] initMilestoneNodeWithResponseDic:[milestoneData valueForKey:@"milestoneDetail"]];
            [arrayToAddNodes addObject:node];
            
            NSArray *listOfFaultHistories = [milestoneData valueForKey:@"lstofFaultHistories"];
            if ([listOfFaultHistories count] > 0) {
                for (NSDictionary *messageData in listOfFaultHistories) {
                    if ([[messageData valueForKey:@"faultHistoryType"] isEqualToString:@"Notes"]) {
                        BTFaultMilestoneNode *messageNode = [[BTFaultMilestoneNode alloc] initMilestoneNodeWithMessageData:messageData andMilestoneNode:node];
                        [arrayToAddNodes addObject:messageNode];
                    }
                }
            }
            
        }
    }
    else
    {
        DDLogError(@"'faultDetails' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"'faultDetails' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]]));
    }
    
    [self.getFaultDetailsWebServiceDelegate getFaultDetailsWebService:self successfullyFetchedFaultDetailsData:fault milestoneNodes:arrayToAddNodes];


}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];

    [self.getFaultDetailsWebServiceDelegate getFaultDetailsWebService:self failedToFetchFaultDetailsDataWithWebServiceError:webServiceError];
}




#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        NSDictionary *resultDict = [responseObject valueForKey:kNLResponseKeyForResult];
        if([resultDict isKindOfClass:[NSDictionary class]])
        {
            // (RLM) Do Nothing Here.
        }
        else
        {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
            
            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
            
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
        
    }
    
    return errorToBeReturned;
}

#pragma mark - Private helper method


- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
//    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeAPINoDataFound userInfo:nil];
//    
//    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
//    
//    return errorToBeReturned;
    
    return nil;
}

@end


#pragma mark- Class

@implementation NLFaultDetailsCancelWebService

- (instancetype)initWithFaultID:(NSString *)faultID assetID:(NSString *)assetID{
    NSString *endPointURLString = [NSString stringWithFormat:@"/Account/api/v1/CancelFault/%@/%@",faultID,assetID];
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {

    }
    return self;
}

@end
