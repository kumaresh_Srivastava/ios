//
//  NLUpdateDNPWebService.h
//  BTBusinessApp
//
//  Created by Vikas Mishra on 17/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"

@class NLUpdateDNPWebService;
@class NLWebServiceError;

@protocol NLUpdateDNPSwebServiceDelegate


- (void)successfullyFetchedDNPDataWithGetUpdateDNPWebService:(NLUpdateDNPWebService *)webService;

- (void)getUpdateDNPWebService:(NLUpdateDNPWebService *)webService failedToFetchUpdateDNPDataWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLUpdateDNPWebService : NLAuthenticationProtectedWebService

@property(nonatomic, weak) id<NLUpdateDNPSwebServiceDelegate> updateDNPWebServiceDelegate;

-(instancetype)initWithSMSession:(NSString *)smsession withUsername:(NSString *)username;

@end
