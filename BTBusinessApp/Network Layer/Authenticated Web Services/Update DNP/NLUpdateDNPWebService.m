//
//  NLUpdateDNPWebService.m
//  BTBusinessApp
//
//  Created by Vikas Mishra on 17/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLUpdateDNPWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NLWebServiceError.h"
#import "NLConstants.h"
#import "AppConstants.h"
#import "AFNetworking.h"

@interface NLUpdateDNPWebService () {
    
    NSString *_smsession;
    NSString *_username;
    
}

@end

@implementation NLUpdateDNPWebService

- (instancetype)initWithSMSession:(NSString *)smsession withUsername:(NSString *)username
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v3/User/Activate"];
    self = [super initWithMethod:@"POST" parameters:nil andEndpointUrlString:endPointURLString];
    
    if (self)
    {
        _smsession = smsession;
        _username = username;
    }
    
    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

//- (AFHTTPSessionManager *)sessionManager
//{
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    
//    NSString *baseURLString = [NSString stringWithFormat:@"https://%@", [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel]];
//    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseURLString] sessionConfiguration:configuration];
//    
//    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringCacheData];
//    [manager.requestSerializer setTimeoutInterval:_timeOutTimeInterval];
//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    
//    // (hd) Because the API call expects the parameters in raw format, hence we are setting an empty query string serializer so that it does not do any serialization.
//    [manager.requestSerializer setQueryStringSerializationWithBlock:^NSString * _Nonnull(NSURLRequest * _Nonnull request, id  _Nonnull parameters, NSError * _Nullable __autoreleasing * _Nullable error) {
//        
//        return parameters;
//    }];
//    
//    return manager;
//}

- (NSDictionary *)httpHeaderFields
{
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionary];
    
    NSDictionary *cookieProperties = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel], NSHTTPCookieDomain,
                                      @"SMSESSION", NSHTTPCookieName,
                                      @"\\", NSHTTPCookiePath,
                                      _smsession, NSHTTPCookieValue,
                                      nil];
    NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
    NSArray *arrayOfCookies = [NSArray arrayWithObject:cookie];
    NSDictionary *cookieHeaders = [NSHTTPCookie requestHeaderFieldsWithCookies:arrayOfCookies];
    
    for(NSString *key in cookieHeaders)
    {
        [finalHeaderFieldDic setValue:[cookieHeaders valueForKey:key] forKey:key];
    }
    
    [finalHeaderFieldDic setValue:[AppManager getRSAEncryptedString:_username] forKey:@"userName"];
    
    return [finalHeaderFieldDic copy];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    [self.updateDNPWebServiceDelegate successfullyFetchedDNPDataWithGetUpdateDNPWebService:self];
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    [self.updateDNPWebServiceDelegate getUpdateDNPWebService:self failedToFetchUpdateDNPDataWithWebServiceError:webServiceError];
}

#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        
        id result = [responseObject valueForKey:kNLResponseKeyForResult];
        if ([result isKindOfClass:[NSNumber class]] && [result intValue] == 1)
        {
                //(VM) DO something on success
        }
        
        else
        {
            DDLogError(@"'result' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]);
            
            
            if([responseObject valueForKey:kNLResponseKeyForCode])
            {
                NSInteger errorCode = [[responseObject valueForKey:kNLResponseKeyForCode] integerValue];
                DDLogError(@"Error code: %ld in webservice with URL: %@", (long)errorCode, [[task currentRequest] URL]);
                
                if (errorCode == BTNetworkErrorCodeAPINoDataFound)
                {
                    errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:errorCode];
                }
                else
                {
                    errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
                }
            }
            else
            {
                errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
            }
        }
    }
    return errorToBeReturned;
}

@end
