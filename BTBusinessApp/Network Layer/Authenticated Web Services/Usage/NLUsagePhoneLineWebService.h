//
//  NLUsagePhoneLineWebService.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 29/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "NLAPIGEEAuthenticatedWebService.h"

@class NLWebServiceError;;
@class NLUsagePhoneLineWebService;

@protocol NLUsagePhoneLineWebServiceDelegate <NSObject>

//- (void)succesfullyFetchedUsageOnPhoneLineUsageWebService:(NLUsagePhoneLineWebService *)webService withPhoneLineUsageList:(NSArray *)phoneLineUsageList;
- (void)succesfullyFetchedUsageOnPhoneLineUsageWebService:(NLUsagePhoneLineWebService *)webService withPhoneLineUsageList:(id)responseObject;


- (void)usagePhoneLineWebService:(NLUsagePhoneLineWebService *)webService failedToFetchPhoneLineUsageWithWebServiceError:(NLWebServiceError *)error;

@end


@interface NLUsagePhoneLineWebService : NLAPIGEEAuthenticatedWebService
//@interface NLUsagePhoneLineWebService : NLAuthenticationProtectedWebService

@property (nonatomic, assign) id<NLUsagePhoneLineWebServiceDelegate> getUsagePhoneLineWebServiceDelegate;

- (instancetype)initWithBAC:(NSString *)bac;
- (instancetype)initWithBAC:(NSString *)bac andCloudVoiceServiceId:(NSString *)cloudVoiceServiceId;

@end
