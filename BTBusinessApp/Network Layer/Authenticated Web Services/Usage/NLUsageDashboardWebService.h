//
//  NLUsageDashboardWebService.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 23/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"


@class NLUsageDashboardWebService;
@class NLWebServiceError;

@protocol NLUsageDashboardWebServiceDelegate 

- (void)succesfullyFetchedUsageOnUsageDashboardScreenWebService:(NLUsageDashboardWebService *)webService withUsageArray:(NSArray *)bacList;
- (void)usageDashboardWebService:(NLUsageDashboardWebService *)webService failedToFetchUsageDataWithWebServiceError:(NLWebServiceError *)error;

@end



@interface NLUsageDashboardWebService : NLAuthenticationProtectedWebService

@property (nonatomic, assign) id<NLUsageDashboardWebServiceDelegate> usageDashboardWebServiceDelegate;

@end
