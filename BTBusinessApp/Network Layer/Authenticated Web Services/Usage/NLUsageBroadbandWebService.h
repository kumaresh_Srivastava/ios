//
//  NLUsageBroadbandWebService.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 26/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "NLAPIGEEAuthenticatedWebService.h"

@class NLUsageBroadbandWebService;
@class NLWebServiceError;

@protocol NLUsageBroadbandWebServiceDelegate 

- (void)succesfullyFetchedBroadbandUsageOnUsageBroadbandWebService:(NLUsageBroadbandWebService *)webService withBroadbandUsageList:(NSArray *)broadbandUsageList;;

- (void)usageBroadbandWebService:(NLUsageBroadbandWebService *)webService failedToFetchBroadbandUsageWithWebServiceError:(NLWebServiceError *)webServiceError;

@end



//@interface NLUsageBroadbandWebService : NLAPIGEEAuthenticatedWebService
@interface NLUsageBroadbandWebService : NLAuthenticationProtectedWebService

@property (nonatomic, assign) id<NLUsageBroadbandWebServiceDelegate> usageBroadbandWebServiceDelegate;

- (instancetype)initWithBAC:(NSString *)bac;

@end
