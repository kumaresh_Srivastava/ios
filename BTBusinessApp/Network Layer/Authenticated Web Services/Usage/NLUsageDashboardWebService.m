//
//  NLUsageDashboardWebService.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 23/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLUsageDashboardWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "BTAsset.h"
#import "AppConstants.h"

#import "NLWebServiceError.h"

@implementation NLUsageDashboardWebService

- (instancetype)init
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v2/User/%@/AllBAC",[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey];

    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];

    return self;

}


#pragma mark - NLWebService Response Handling Private Methods


- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];

    NSArray *resultArray = [responseObject valueForKey:kNLResponseKeyForResult];
    if(resultArray)
    {
        BTAsset *asset = nil;
        NSMutableArray *assetsArray = [NSMutableArray array];
        for (NSDictionary *assetsDetailDict in resultArray)
        {
            if(assetsDetailDict)
            {
                asset = [[BTAsset alloc] initWithResponseDictionaryFromUsageDashboardAPIResponse:assetsDetailDict];
                [assetsArray addObject:asset];
            }
        }

         DDLogInfo(@"Successfully fetched UsageDashBoard data using webservice with URL: %@", [[task currentRequest] URL]);

        [self.usageDashboardWebServiceDelegate succesfullyFetchedUsageOnUsageDashboardScreenWebService:self withUsageArray:[NSArray arrayWithArray:assetsArray]];
    }
    else
    {
         DDLogError(@"'Usage' element missing in response json object in webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"'Usage' element missing in response json object in webservice with URL: %@", [[task currentRequest] URL]]));

        [self.usageDashboardWebServiceDelegate usageDashboardWebService:self failedToFetchUsageDataWithWebServiceError:[NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject]];
    }

}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];

    [self.usageDashboardWebServiceDelegate usageDashboardWebService:self failedToFetchUsageDataWithWebServiceError:webServiceError];
}





#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil)
    {
        // (SD) If there was no error from superclass, then this class can check for its level of errors.
        NSArray *resultArray = [responseObject valueForKey:kNLResponseKeyForResult];
        if(![resultArray isKindOfClass:[NSArray class]])
        {
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];

            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }

    return errorToBeReturned;
}







#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;

    return nil;
}





@end
