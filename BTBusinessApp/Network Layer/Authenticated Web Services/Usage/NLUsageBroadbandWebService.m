//
//  NLUsageBroadbandWebService.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 26/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLUsageBroadbandWebService.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "BTOrder.h"
#import "AppConstants.h"

#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "BTUsageBroadbandProduct.h"
#import "NLWebServiceError.h"

@implementation NLUsageBroadbandWebService

- (instancetype)initWithBAC:(NSString *)bac
{
//    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/broadband-usage/products?bac=%@",bac];
    NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v3/BBUsage/Products/%@",bac];

    DDLogInfo(@"INFO: BroadbandURL:%@",endPointURLString);
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
    }
    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];

    NSString *appInstallationKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:appInstallationKey forKey:@"_authKey"];

    return [finalHeaderFieldDic copy];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{

    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSArray *resultArray = [responseObject valueForKey:kNLResponseKeyForResult];
    
    NSMutableArray *resultUsages = [[NSMutableArray alloc] init];
    
    for (NSDictionary *usageDict in resultArray) {
        
        BTUsageBroadbandProduct *usageProduct = [[BTUsageBroadbandProduct alloc] initWithResponseDictionaryFromBroadbandUsageDetailsAPIResponse:usageDict];
        
        [resultUsages addObject:usageProduct];
    }
    
    DDLogInfo(@"Successfully fetched BroadbandUsage data using webservice with URL: %@", [[task currentRequest] URL]);
    
    [self.usageBroadbandWebServiceDelegate succesfullyFetchedBroadbandUsageOnUsageBroadbandWebService:self withBroadbandUsageList:[NSArray arrayWithArray:resultUsages]];
    
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];

    [self.usageBroadbandWebServiceDelegate usageBroadbandWebService:self failedToFetchBroadbandUsageWithWebServiceError:webServiceError];
}



#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (Sal) If there was no error from superclass, then this class can check for its level of errors.
        NSArray *responseArray = [responseObject valueForKey:kNLResponseKeyForResult];
        if ([[responseObject valueForKey:@"code"]isEqualToNumber:[NSNumber numberWithInt:607]]) {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeAPINoDataFound userInfo:nil];
            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
        } else if(!responseArray || ![responseArray isKindOfClass:[NSArray class]])
        {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
            
            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
            
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
        }
    }
   
    return errorToBeReturned;
}




@end
