//
//  NLUsagePhoneLineWebService.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 29/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLUsagePhoneLineWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "BTUsagePhoneProduct.h"
#import "AppConstants.h"

#import "NLWebServiceError.h"

@implementation NLUsagePhoneLineWebService


- (instancetype)initWithBAC:(NSString *)bac
{
//    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/phone-usage?bac=%@",bac];//Old one
    //NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v3/PhoneUsage/%@",bac];

    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/unbilled-usage?bac=%@",bac]; //To Do : Manik change the new service here
    
 //   https://api-test1.ee.co.uk/bt-business-auth/v1/unbilled-usage?bac=GP00150789 //New test API from Pushkal
    
//    /bt-business-auth/v1/unbilled-usage?bac={bac}&serviceId={serviceId}

//    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/phone-usage?bac=%@",bac];
    //NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v3/PhoneUsage/%@",bac];
    
    //NSString *endPointURLString = @"https://api.myjson.com/bins/1f51l3";
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
    }
    return self;
}

// To fetch cloud voice details
- (instancetype)initWithBAC:(NSString *)bac andCloudVoiceServiceId:(NSString *)cloudVoiceServiceId
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/unbilled-usage?bac=%@&serviceId=%@",bac,cloudVoiceServiceId];
    //    https://api-test1.ee.co.uk/bt-business-auth/v1/unbilled-usage?bac=GP00132675&serviceId=CV60041526";
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
    }
    return self;
}


#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask
- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    NSString *appInstallationKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:appInstallationKey forKey:@"_authKey"];
    
    return [finalHeaderFieldDic copy];
}




#pragma mark - NLWebService Response Handling Private Methods
/*
- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    
//    if ([[responseObject valueForKey:kNLResponseKeyForResult] isKindOfClass:[NSDictionary class]]) {
//        NSLog(@"NSDictionary contains unbilled usage");
//    }
    
    if ([[responseObject valueForKey:kNLResponseKeyForResult] isKindOfClass:[NSArray class]]) {
        NSLog(@"Phoneline response");
        
        NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
        
        NSMutableArray *resultPhoneLineUsages = [[NSMutableArray alloc] init];
        for (NSDictionary *usageDict in resultDic) {
            
            BTUsagePhoneProduct *phoneLineUsage = [[BTUsagePhoneProduct alloc] initWithResponseDictionaryFromPhoneLineUsageDetailsAPIResponse:usageDict];
            
            [resultPhoneLineUsages addObject:phoneLineUsage];
        }
        
        [self.getUsagePhoneLineWebServiceDelegate succesfullyFetchedUsageOnPhoneLineUsageWebService:self withPhoneLineUsageList:[NSArray arrayWithArray:resultPhoneLineUsages]];
    }
    else {
        
        NSLog(@"Cloud voice response");

        NSDictionary *resultDic = [[responseObject valueForKey:kNLResponseKeyForResult]valueForKey:@"UnbilledUsageDetails"];
        
        NSMutableArray *resultPhoneLineUsages = [[NSMutableArray alloc] init];
        for (NSDictionary *usageDict in resultDic) {
            
            BTUsagePhoneProduct *phoneLineUsage = [[BTUsagePhoneProduct alloc] initWithResponseDictionaryFromPhoneLineUsageDetailsAPIResponse:usageDict];
            
            [resultPhoneLineUsages addObject:phoneLineUsage];
        }
        
        [self.getUsagePhoneLineWebServiceDelegate succesfullyFetchedUsageOnPhoneLineUsageWebService:self withPhoneLineUsageList:[NSArray arrayWithArray:resultPhoneLineUsages]];
    }
    
    
    DDLogInfo(@"Successfully fetched PhoneLineUsage data using webservice with URL: %@", [[task currentRequest] URL]);
    
}
*/

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    

    /*
     When user cancel (by pressing abck instantly) the request delegate is nil // Live crash version 2.3.0 (2.3.0.3)
     SIGABRT: -[__NSCFString succesfullyFetchedUsageOnPhoneLineUsageWebService:withPhoneLineUsageList:]: unrecognized selector sent to instance 0x280ca94d0
    */

    if(self.getUsagePhoneLineWebServiceDelegate && [self.getUsagePhoneLineWebServiceDelegate  respondsToSelector:@selector(succesfullyFetchedUsageOnPhoneLineUsageWebService:withPhoneLineUsageList:)]) {
        [self.getUsagePhoneLineWebServiceDelegate succesfullyFetchedUsageOnPhoneLineUsageWebService:self withPhoneLineUsageList:responseObject];
    
    DDLogInfo(@"Successfully fetched PhoneLineUsage data using webservice with URL: %@", [[task currentRequest] URL]);
    }
    
}

-(void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    /* when user cancel (by pressing abck instantly) the request delegate is nil // Live crash version 2.3.0 (2.3.0.3)
     SIGABRT: -[__NSCFString succesfullyFetchedUsageOnPhoneLineUsageWebService:withPhoneLineUsageList:]: unrecognized selector sent to instance 0x280ca94d0
     */
    if(self.getUsagePhoneLineWebServiceDelegate && [self.getUsagePhoneLineWebServiceDelegate respondsToSelector:@selector(usagePhoneLineWebService: failedToFetchPhoneLineUsageWithWebServiceError:)]) {
        [self.getUsagePhoneLineWebServiceDelegate usagePhoneLineWebService:self failedToFetchPhoneLineUsageWithWebServiceError:webServiceError];
      }
	}



#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (RLM) If there was no error from superclass, then this class can check for its level of errors.
        if ([[responseObject valueForKey:@"code"]isEqualToNumber:[NSNumber numberWithInt:607]]) {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeAPINoDataFound userInfo:nil];
            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
        } else if (![[responseObject valueForKey:@"code"]isEqualToNumber:[NSNumber numberWithInt:600]]) {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
            
            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
            
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
        }
    }
    
    return errorToBeReturned;
}
//- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
//{
//    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
//
//    if(errorToBeReturned == nil)
//    {
//        // (RLM) If there was no error from superclass, then this class can check for its level of errors.
//        NSArray *usageArray = [responseObject valueForKey:kNLResponseKeyForResult];
//
//        if(![usageArray isKindOfClass:[NSArray class]])
//        {
//            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
//
//            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
//
//            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
//        }
//    }
//
//    return errorToBeReturned;
//}


#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;
    
    return nil;
}


- (NSString *)staticDataFileName
{
    return @"PhoneLineUsage";
}

@end
