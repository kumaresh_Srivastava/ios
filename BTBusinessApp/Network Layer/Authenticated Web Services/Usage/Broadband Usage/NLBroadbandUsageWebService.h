//
//  NLBroadbandUsageWebService.h
//  BTBusinessApp
//
//  Created by VectoScalar on 12/23/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "NLAPIGEEAuthenticatedWebService.h"

@class NLBroadbandUsageWebService;
@class BTBroadbandUsage;
@class NLWebServiceError;

@protocol NLBroadbandUsageWebServiceDelegate 

- (void)broadbandUsageWebService:(NLBroadbandUsageWebService *)webService successfullyFetchedBroadbandUsage:(BTBroadbandUsage *)broadbandUsage;

- (void)broadbandUsageWebService:(NLBroadbandUsageWebService *)webService failedToFetchUserDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

//@interface NLBroadbandUsageWebService : NLAPIGEEAuthenticatedWebService
@interface NLBroadbandUsageWebService : NLAuthenticationProtectedWebService

@property(nonatomic, assign)id<NLBroadbandUsageWebServiceDelegate>broadbandWebServiceDelegate;

- (instancetype)initWithBroadbandAssetIntegrationID:(NSString *)assetIntegrationID;

@end
