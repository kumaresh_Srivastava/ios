//
//  NLBroadbandUsageWebService.m
//  BTBusinessApp
//
//  Created by VectoScalar on 12/23/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLBroadbandUsageWebService.h"
#import "NLConstants.h"
#import "AppConstants.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "CDApp.h"
#import "NSObject+APIResponseCheck.h"

#import "BTBroadbandUsage.h"
#import "NLWebServiceError.h"


@implementation NLBroadbandUsageWebService

- (instancetype)initWithBroadbandAssetIntegrationID:(NSString *)assetIntegrationID{
    
//    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/broadband-usage?assetIntegrationId=%@", assetIntegrationID];
    NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v3/BBUsage/%@", assetIntegrationID];
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    
    return self;
}



#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    NSString *appInstallationKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:appInstallationKey forKey:@"_authKey"];
    
    return [finalHeaderFieldDic copy];
}




#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *resultDict = [responseObject valueForKey:kNLResponseKeyForResult];

    BTBroadbandUsage *broadbandUsage = [[BTBroadbandUsage alloc] initWithResponseDictionaryFromBroadBandAPIResponse:resultDict];
    
    if(broadbandUsage){
        
        DDLogInfo(@"Successfully fetched BroadbandUsage data using webservice with URL: %@", [[task currentRequest] URL]);
        
        [self.broadbandWebServiceDelegate broadbandUsageWebService:self successfullyFetchedBroadbandUsage:broadbandUsage];
    }
    else{
        
        DDLogError(@"Unable to create BTBroadbandUsage model object using response json object in webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Unable to create BTBroadbandUsage model object using response json object in webservice with URL: %@", [[task currentRequest] URL]]));
        
        NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
        NLWebServiceError *webServiceError = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
        
        [self.broadbandWebServiceDelegate broadbandUsageWebService:self failedToFetchUserDataWithWebServiceError:webServiceError];
        
    }

    
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError{
    
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    [self.broadbandWebServiceDelegate broadbandUsageWebService:self failedToFetchUserDataWithWebServiceError:webServiceError];
}


#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (Sal) If there was no error from superclass, then this class can check for its level of errors.
        NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
        if(![resultDic isKindOfClass:[NSDictionary class]])
        {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
            
            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
            
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }
    
    return errorToBeReturned;
}


- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    /*
    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];

    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    
    return errorToBeReturned;
     
     */
    
    return nil;
}



@end
