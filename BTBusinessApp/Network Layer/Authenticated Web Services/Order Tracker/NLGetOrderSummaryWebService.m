//
//  NLGetOrderSummaryWebService.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 22/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLGetOrderSummaryWebService.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "AppConstants.h"

#import "BTOrder.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NLWebServiceError.h"

@implementation NLGetOrderSummaryWebService

- (instancetype)initWithOrderRef:(NSString *)orderRef
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business/v1/orders/%@", orderRef];
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
        _orderRef = [orderRef copy];
        _timeOutTimeInterval = 60.0;
    }
    return self;
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
    BTOrder *order = [[BTOrder alloc] initOrderWithResponseDictionaryFromOrderSummaryAPIResponse:resultDic];
    if(order)
    {
        DDLogInfo(@"Successfully fetched order summary data for order reference '%@' using webservice with URL: %@", _orderRef, [[task currentRequest] URL]);
        
        [self.getOrderSummaryWebServiceDelegate getOrderSummaryWebService:self successfullyFetchedOrderSummaryData:order];
    }
    else
    {
        DDLogError(@"Unable to create BTOrder model object using response json object in webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Unable to create BTOrder model object using response json object in webservice with URL: %@", [[task currentRequest] URL]]));
        
        [self.getOrderSummaryWebServiceDelegate getOrderSummaryWebService:self failedToFetchOrderSummaryDataWithWebServiceError:[NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject]];
    }
}


- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.getOrderSummaryWebServiceDelegate getOrderSummaryWebService:self failedToFetchOrderSummaryDataWithWebServiceError:webServiceError];
}


#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        
        NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
        if([resultDic isKindOfClass:[NSDictionary class]])
        {
            // (hds) Do Nothing Here.
        }
        else
        {
            DDLogError(@"'result' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]);
            
            
            if([responseObject valueForKey:kNLResponseKeyForCode])
            {
                NSInteger errorCode = [[responseObject valueForKey:kNLResponseKeyForCode] integerValue];
                DDLogError(@"Error code: %ld in webservice with URL: %@", (long)errorCode, [[task currentRequest] URL]);
                
                
                if (errorCode == BTNetworkErrorCodeAPINoDataFound)
                {
                    errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:errorCode];
                }
                else if (errorCode == BTNetworkErrorCodeUnsupportedOrder)
                {
                    errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:errorCode];
                }
                else
                {
                    errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
                }
            }
            else
            {
                errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
            }
        }
    }
    return errorToBeReturned;
}

#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;
    
    return nil;
}


@end
