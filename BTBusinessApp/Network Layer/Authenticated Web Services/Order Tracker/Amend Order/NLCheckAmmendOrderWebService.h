//
//  NLCheckAmmendOrderWebService.h
//  BTBusinessApp
//
//  Created by Accolite on 27/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
@class NLCheckAmmendOrderWebService;
@class NLWebServiceError;

@protocol NLCheckAmmendOrderWebServiceDelegate

- (void)ammendOrderCheckWebService:(NLCheckAmmendOrderWebService *)webService successfullyCheckedAmmendOrder:(NSDictionary *)ammendCheckDict;
- (void)ammendOrderCheckWebService:(NLCheckAmmendOrderWebService *)webService failedToCheckAmmendOrderWithWebServiceError:(NLWebServiceError *)webServiceError;

@end


@interface NLCheckAmmendOrderWebService : NLAuthenticationProtectedWebService
@property (nonatomic, weak) id <NLCheckAmmendOrderWebServiceDelegate> getCheckAmmendOrderWebServiceDelegate;

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef andAmmendAttribute:(NSString *)ammendAttribute;
@end
