//
//  NLSiteContactWebService.h
//  BTBusinessApp
//
//  Created by Accolite on 02/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
@class NLSiteContactWebService;
@class NLWebServiceError;

@protocol NLSiteContactWebServiceDelegate 

- (void)getSiteContactsWebService:(NLSiteContactWebService *)webService successfullyFetchedSiteContacts:(NSArray *)siteContacts currentSiteContactIndex:(int)siteContactIndex currentAltSiteContactIndex:(int)altSiteContactIndex;

- (void)getSiteContactsWebService:(NLSiteContactWebService *)webService failedToFetchSiteContactsWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface NLSiteContactWebService : NLAuthenticationProtectedWebService

@property (nonatomic, weak) id <NLSiteContactWebServiceDelegate> getSiteContactWebServiceDelegate;

- (instancetype)initWithOrderRef:(NSString *)orderRef andItemRef:(NSString *)itemRef;
- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef andPostCode:(NSString *)postCode;
@end

/*
 https://{host}/account/api/v2/Order/{orderRef}/{itemReference}/SiteContacts – Site Contacts
 2)      https://{host}/account/api/v2/Order/{orderRef}/{itemReference}/{postCode}/SiteContacts – Site Contacts with PostCode
*/
