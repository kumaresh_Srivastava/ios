//
//  NLHolidayListWebService.h
//  BTBusinessApp
//
//  Created by Accolite on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"

@class NLHolidayListWebService;
@class NLWebServiceError;

@protocol NLHolidayListWebServiceDelegate 

- (void)getHolidayListWebService:(NLHolidayListWebService *)webService successfullyFetchedHolidayList:(NSArray *)holidayList;

- (void)getHolidayListWebService:(NLHolidayListWebService *)webService failedToFetchHolidayListWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLHolidayListWebService : NLAuthenticationProtectedWebService

@property (nonatomic, weak) id <NLHolidayListWebServiceDelegate> getHolidayListWebServiceDelegate;

- (instancetype)initWithOrderRef:(NSString *)orderRef andItemRef:(NSString *)itemRef;

- (instancetype)initWithPostCode:(NSString *)postCode;

@end
