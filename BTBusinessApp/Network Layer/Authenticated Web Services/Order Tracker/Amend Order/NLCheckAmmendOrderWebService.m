//
//  NLCheckAmmendOrderWebService.m
//  BTBusinessApp
//
//  Created by Accolite on 27/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLCheckAmmendOrderWebService.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "AppConstants.h"

#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NLWebServiceError.h"


@implementation NLCheckAmmendOrderWebService
// https://{host}/account/api/v2/OrderStatus/{orderRef}/{itemReference}/{amendAttribute}

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef andAmmendAttribute:(NSString *)ammendAttribute{
    
    NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v3/OrderStatus/%@/%@/%@",orderRef,itemRef,ammendAttribute];
    
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
        _timeOutTimeInterval = 60.0;
    }
    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    NSString *appInstallationKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:appInstallationKey forKey:@"_authKey"];
    
    return [finalHeaderFieldDic copy];
}


#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *resultDict = [responseObject valueForKey:kNLResponseKeyForResult];
    DDLogInfo(@"INFO: AmmendCheck Succeesfull status : %@",resultDict);
    [self.getCheckAmmendOrderWebServiceDelegate ammendOrderCheckWebService:self successfullyCheckedAmmendOrder:resultDict];

}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    [self.getCheckAmmendOrderWebServiceDelegate ammendOrderCheckWebService:self failedToCheckAmmendOrderWithWebServiceError:webServiceError];
}



#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (Sal) If there was no error from superclass, then this class can check for its level of errors.
        //NSString *resultString = [responseObject valueForKey:kNLResponseKeyForResult];
        NSDictionary *resultDict = [responseObject valueForKey:kNLResponseKeyForResult];
        if(![resultDict isKindOfClass:[NSDictionary class]])
        {
            
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
            
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
        }
    }
    
    return errorToBeReturned;
}


@end
