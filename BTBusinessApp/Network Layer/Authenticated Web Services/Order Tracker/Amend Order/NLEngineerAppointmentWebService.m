//
//  NLEngineerAppointmentWebService.m
//  BTBusinessApp
//
//  Created by Accolite on 03/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLEngineerAppointmentWebService.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "AppConstants.h"

#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NLWebServiceError.h"

#import "BTAppointment.h"

@implementation NLEngineerAppointmentWebService

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef andAppointmentDate:(NSString *)appointmentDate
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/Account/api/v3.1/OrderAppointments/%@/%@/%@",orderRef, itemRef,appointmentDate];
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {

    }
    return self;
}

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef appointmentDate:(NSString *)appointmentDate andPCP:(BOOL)pcpOnly
{
    NSString *pcpFlag = @"false";
    if(pcpOnly) {
        pcpFlag = @"true";
    }
    NSString *endPointURLString = [NSString stringWithFormat:@"/Account/api/v3.2/OrderAppointments/%@/%@/%@/%@",orderRef, itemRef,appointmentDate, pcpFlag];
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
        
    }
    return self;
}

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef postCode:(NSString *)postCode andAppointmentDate:(NSString *)appointmentDate
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/Account/api/v3.1/OrderAppointments/%@/%@/%@/%@",orderRef, itemRef,postCode, appointmentDate];
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];

    if(self)
    {
        
    }
    return self;
}

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef postCode:(NSString *)postCode appointmentDate:(NSString *)appointmentDate andPCP:(BOOL)pcpOnly
{
    NSString *pcpFlag = @"false";
    if(pcpOnly) {
        pcpFlag = @"true";
    }
    NSString *endPointURLString = [NSString stringWithFormat:@"/Account/api/v3.2/OrderAppointments/%@/%@/%@/%@/%@",orderRef, itemRef,postCode, appointmentDate, pcpFlag];
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    
    if(self)
    {
        
    }
    return self;
}


#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    NSString *groupKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:groupKey forKey:@"_authKey"];
    
    return [finalHeaderFieldDic copy];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
    NSArray *resultAppointments = resultDic[@"Appointments"];
    
    NSMutableArray *appointments = [[NSMutableArray alloc] init];
    for (NSDictionary *appointmentDict in resultAppointments) {

        BTAppointment *appointment = [[BTAppointment alloc] initWithResponseDictionaryFromAppointmentsAPIResponse:appointmentDict];
        [appointments addObject:appointment];
    }

    DDLogInfo(@"Successfully fetched EngineerAppointments data using webservice with URL: %@", [[task currentRequest] URL]);

    [self.getEngineerAppointmentWebServiceDelegate getEngineerAppointmentsWebService:self successfullyFetchedEngineerAppointments:appointments];

}


- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];

    [self.getEngineerAppointmentWebServiceDelegate getEngineerAppointmentsWebService:self failedToFetchEngineerAppointmentsWithWebServiceError:webServiceError];
}



#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.

        NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];

        if([resultDic isKindOfClass:[NSDictionary class]])
        {
            NSArray *resultAppointments = resultDic[@"Appointments"];
            if([resultAppointments isKindOfClass:[NSArray class]])
            {

            }
            else
            {
                NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];

                errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];

                DDLogError(@"'Appointments' element missing in response json object in webservice with URL: %@", [[task currentRequest] URL]);
            }
        }
        else
        {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];

            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];

            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
        }
    }

    return errorToBeReturned;
}




#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;
    
    return nil;
}




@end

/*
/Account/api/v2/OrderAppointments/{orderRef}/{itemReference}/{appointmentDate}
*/
