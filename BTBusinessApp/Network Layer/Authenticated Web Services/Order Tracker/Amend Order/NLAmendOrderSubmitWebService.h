//
//  NLAmendOrderSubmitWebService.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 07/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "NLAPIGEEAuthenticatedWebService.h"

@class NLAmendOrderSubmitWebService;
@class BTProduct;
@class BTBillingDetails;
@class BTPricingDetails;
@class BTProductDetails;
@class NLWebServiceError;

@protocol NLAmendOrderSubmitWebServiceDelegate 

- (void)orderAmendSubmissionSuccesfullyFinishedByAmendOrderSubmitWebService:(NLAmendOrderSubmitWebService *)webService;

- (void)amendOrderSubmitWebService:(NLAmendOrderSubmitWebService *)webService orderAmendSubmissionSuccesfullyFinishedAndFetchedUpdatedProductData:(BTProduct *)productSummary billingDetails:(BTBillingDetails *)billingDetails pricingDetails:(BTPricingDetails *)pricingDetails productDetails:(BTProductDetails *)productDetails isAuthLevel:(NSInteger)authLevel;

- (void)amendOrderSubmitWebService:(NLAmendOrderSubmitWebService *)webService failedToSubmitAmendOrderWithWebServiceError:(NLWebServiceError *)webServiceError;

@end


@interface NLAmendOrderSubmitWebService : NLAPIGEEAuthenticatedWebService

@property (nonatomic, weak) id <NLAmendOrderSubmitWebServiceDelegate> amendOrderSubmitWebServiceDelegate;

- (instancetype)initWithAmendOrderRequestDictionary:(NSDictionary *)amendOrderRequestDic;
- (void)fetchOfflineData;

@end
