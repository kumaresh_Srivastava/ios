//
//  NLSiteContactWebService.m
//  BTBusinessApp
//
//  Created by Accolite on 02/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLSiteContactWebService.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "AppConstants.h"

#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "BTOrderSiteContactModel.h"
#import "BTContact.h"
#import "NLWebServiceError.h"
#import "AppManager.h"

@implementation NLSiteContactWebService

- (instancetype)initWithOrderRef:(NSString *)orderRef andItemRef:(NSString *)itemRef{
    
    NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v2/OrderSiteContacts/%@/%@",orderRef, itemRef];
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    
    if(self)
    {
    }
    return self;
}

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef andPostCode:(NSString *)postCode{

    NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v2/OrderSiteContacts/%@/%@/%@",orderRef, itemRef,postCode];
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
    }
    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    NSString *appInstallationKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:appInstallationKey forKey:@"_authKey"];
    
    return [finalHeaderFieldDic copy];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];

    
    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];

    NSNumber *sitecontactIndexNumber;
    NSNumber *altSiteitecontactIndexNumber;
    if (resultDic[@"CurrentSiteContactIndex"]) {
        sitecontactIndexNumber = resultDic[@"CurrentSiteContactIndex"];
    }
    if (resultDic[@"CurrentAltSiteContactIndex"]) {
        altSiteitecontactIndexNumber = resultDic[@"CurrentAltSiteContactIndex"];
    }
    int currentSiteContactIndex = (int)sitecontactIndexNumber.integerValue;
    int currentAltSiteContactIndex = (int)altSiteitecontactIndexNumber.integerValue;
    
    NSArray *resultSiteContacts;
    if (resultDic[@"Contacts"]) {
        resultSiteContacts = [NSArray arrayWithArray:resultDic[@"Contacts"]];
        
        NSMutableArray *siteContacts = [[NSMutableArray alloc] init];
        for (NSDictionary *siteContactDict in resultSiteContacts) {
            
            BTOrderSiteContactModel *siteContact = [[BTOrderSiteContactModel alloc] initSiteContactWithResponseDict:siteContactDict];
            [siteContacts addObject:siteContact];
            
        }
        
        DDLogInfo(@"Successfully fetched SiteContacts data using webservice with URL: %@", [[task currentRequest] URL]);
        
        [self.getSiteContactWebServiceDelegate getSiteContactsWebService:self successfullyFetchedSiteContacts:siteContacts currentSiteContactIndex:currentSiteContactIndex currentAltSiteContactIndex:currentAltSiteContactIndex];
    }
    else
    {
        DDLogError(@"'Contacts' element missing in response json object in webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"'Contacts' element missing in response json object in webservice with URL: %@", [[task currentRequest] URL]]));
        
        NLWebServiceError *webserviceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponse];
        [self.getSiteContactWebServiceDelegate getSiteContactsWebService:self failedToFetchSiteContactsWithWebServiceError:webserviceError];
    }

    
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.getSiteContactWebServiceDelegate getSiteContactsWebService:self failedToFetchSiteContactsWithWebServiceError:webServiceError];
}


#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (Sal) If there was no error from superclass, then this class can check for its level of errors.
        NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
        if(![resultDic isKindOfClass:[NSDictionary class]])
        {
            
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponse];
    
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
        }
    }
    
    return errorToBeReturned;
}





@end
