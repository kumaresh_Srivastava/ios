//
//  NLHolidayListWebService.m
//  BTBusinessApp
//
//  Created by Accolite on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLHolidayListWebService.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "AppConstants.h"

#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NLWebServiceError.h"

/*
 1)      https://eric1-dmze2e-ukb.bt.com/api/v2/HolidaysAndWeekends/BTTZ9137/BTTZ91371-1  -- WithoutPostcode
 2)      https://eric1-dmze2e-ukb.bt.com/api/v2/HolidaysAndWeekends/AB101HP -- WithPostcode
*/



@implementation NLHolidayListWebService

- (instancetype)initWithOrderRef:(NSString *)orderRef andItemRef:(NSString *)itemRef
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/Account/api/v2/HolidaysAndWeekends/%@/%@",orderRef, itemRef];

    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {

    }
    return self;
}

- (instancetype)initWithPostCode:(NSString *)postCode
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/Account/api/v2/HolidaysAndWeekends/%@",postCode];

    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];

    if(self)
    {

    }
    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    NSString *groupKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:groupKey forKey:@"_authKey"];
    
    return [finalHeaderFieldDic copy];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSArray *resultArray = [responseObject valueForKey:kNLResponseKeyForResult];
    NSArray *holidayList = [[NSArray alloc] initWithArray:resultArray];

    DDLogInfo(@"Successfully fetched Holidays list using webservice with URL: %@", [[task currentRequest] URL]);

    [self.getHolidayListWebServiceDelegate getHolidayListWebService:self successfullyFetchedHolidayList:holidayList];
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];

    [self.getHolidayListWebServiceDelegate getHolidayListWebService:self failedToFetchHolidayListWithWebServiceError:webServiceError];
}



#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.

        NSArray *resultArray = [responseObject valueForKey:kNLResponseKeyForResult];
        if([resultArray isKindOfClass:[NSArray class]])
        {

        }
        else
        {
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];

            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }

    return errorToBeReturned;
}




#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;
    
    return nil;
}





@end
