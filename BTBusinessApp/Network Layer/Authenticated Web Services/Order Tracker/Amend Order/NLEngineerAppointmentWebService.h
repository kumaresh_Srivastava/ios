//
//  NLEngineerAppointmentWebService.h
//  BTBusinessApp
//
//  Created by Accolite on 03/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"

@class NLEngineerAppointmentWebService;
@class NLWebServiceError;

@protocol NLEngineerAppointmentWebServiceDelegate 

- (void)getEngineerAppointmentsWebService:(NLEngineerAppointmentWebService *)webService successfullyFetchedEngineerAppointments:(NSArray *)appointments;

- (void)getEngineerAppointmentsWebService:(NLEngineerAppointmentWebService *)webService failedToFetchEngineerAppointmentsWithWebServiceError:(NLWebServiceError *)webServiceError;

@end


@interface NLEngineerAppointmentWebService : NLAuthenticationProtectedWebService

@property (nonatomic, weak) id <NLEngineerAppointmentWebServiceDelegate> getEngineerAppointmentWebServiceDelegate;

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef andAppointmentDate:(NSString *)appointmentDate;

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef postCode:(NSString *)postCode andAppointmentDate:(NSString *)appointmentDate;

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef appointmentDate:(NSString *)appointmentDate andPCP:(BOOL)pcpOnly;

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef postCode:(NSString *)postCode appointmentDate:(NSString *)appointmentDate andPCP:(BOOL)pcpOnly;

@end
