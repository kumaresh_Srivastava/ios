//
//  NLGetOrderMilestonesWebService.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 09/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLGetOrderMilestonesWebService.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "AppConstants.h"

#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "BTOrderMilestoneNodeModel.h"
#import "BTOrderDispatchDetailsModel.h"
#import "BTOrderSiteContactModel.h"
#import "NLWebServiceError.h"

@implementation NLGetOrderMilestonesWebService

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/orders/%@/%@/milestones", orderRef, itemRef];
    //NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v3/Order/%@/%@/MileStones/0", orderRef, itemRef];
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
        _orderRef = [orderRef copy];
        _itemRef = [itemRef copy];
        _timeOutTimeInterval = 60.0;
    }
    return self;
}

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef postCode:(NSString *)postCode errorCount:(NSInteger)errorCount
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business/v1/orders/%@/%@/milestones?postCode=%@", orderRef, itemRef, postCode];
    //NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v3/Order/%@/%@/MileStones/%@/0", orderRef, itemRef, postCode];
    ///http://m.e2e-ukb.nat.bt.com/account/api/v3/Order/BT25T5MC/BT25T5MC1-10/MileStones/AB101AU
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
        _orderRef = [orderRef copy];
        _itemRef = [itemRef copy];
        _timeOutTimeInterval = 60.0;
    }
    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    NSString *groupKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:groupKey forKey:@"_authKey"];
    
    return [finalHeaderFieldDic copy];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSArray *milestoneArray = [responseObject valueForKey:kNLResponseKeyForResult];
    
    if (milestoneArray == nil || milestoneArray.count == 0)
    {
        [self.getOrderMilestonesWebServiceDelegate getOrderMilestonesWebService:self successfullyFetchedOrderMilestonesData:nil];
    }
    else
    {
        NSMutableArray *arrayOfMilestoneData = [NSMutableArray array];
        
        for (NSDictionary *milestoneDataDic in milestoneArray)
        {
            if(milestoneDataDic && [milestoneDataDic isKindOfClass:[NSDictionary class]])
            {
                NSArray *nodes = [milestoneDataDic valueForKey:@"Nodes"];
                if ([nodes isKindOfClass:[NSArray class]])
                {
                    NSMutableArray<BTOrderMilestoneNodeModel *> *arrayToAddNodes = [NSMutableArray<BTOrderMilestoneNodeModel *> array];
                    NSDate *activationDate = nil;
                    BOOL isActivationDateAvailable = NO;
                    NSDate *engineerAppointmentDate = nil;
                    BOOL isEngineerAppointmentDateAvailable = NO;
                    NSArray *dispatchDetails = nil;
                    BTOrderSiteContactModel *siteContact = nil;
                    BOOL isSiteContactAvailable = NO;
                    BTOrderSiteContactModel *altSiteContact = nil;
                    BOOL isAltSiteContactAvailable = NO;
                    
                    for (NSDictionary *milestoneNode in nodes) {
                        BTOrderMilestoneNodeModel *nodeData = [[BTOrderMilestoneNodeModel alloc] initMilestoneNodeWithResponseDic:milestoneNode];
                        if (nodeData.isActivationAmendSource) {
                            activationDate = nodeData.date;
                            isActivationDateAvailable = YES;
                        }
                        if (nodeData.isAppointmentAmendSource) {
                            engineerAppointmentDate = nodeData.date;
                            isEngineerAppointmentDateAvailable = YES;
                        }
                        if (nodeData.isDispatchDetailsAvailable) {
                            
                            dispatchDetails =[[BTOrderDispatchDetailsModel alloc] getDispatchDetailsFrom:milestoneNode];
                        }
                        if (nodeData.isSiteContactAvailable) {
                            
                            siteContact = [[BTOrderSiteContactModel alloc] initSiteContactWithResponseDict:[milestoneNode objectForKey:@"SiteContact"]];
                            isSiteContactAvailable = YES;
                        }
                        if (nodeData.isAltSiteContactAvailable) {
                            
                            altSiteContact = [[BTOrderSiteContactModel alloc] initSiteContactWithResponseDict:[milestoneNode objectForKey:@"AlternateSiteContact"]];
                            isAltSiteContactAvailable = YES;
                        }
                        [arrayToAddNodes addObject:nodeData];
                    }
                    
                    NSMutableDictionary *orderMilestoneData = [NSMutableDictionary dictionary];
                    [orderMilestoneData setValue:arrayToAddNodes forKey:@"MilestoneNodes"];
                    [orderMilestoneData setValue:dispatchDetails forKey:@"DispatchDetails"];
                    [orderMilestoneData setValue:[NSNumber numberWithBool:isActivationDateAvailable] forKey:@"IsActivationDateAvailable"];
                    if (isActivationDateAvailable) {
                        [orderMilestoneData setValue:activationDate forKey:@"ActivationDate"];
                    }
                    [orderMilestoneData setValue:[NSNumber numberWithBool:isEngineerAppointmentDateAvailable] forKey:@"IsEngineerAppointmentDateAvailable"];
                    if (isEngineerAppointmentDateAvailable) {
                        [orderMilestoneData setValue:engineerAppointmentDate forKey:@"EngineerAppointmentDate"];
                    }
                    [orderMilestoneData setValue:[NSNumber numberWithBool:isSiteContactAvailable] forKey:@"IsSiteContactAvailable"];
                    if (isSiteContactAvailable) {
                        [orderMilestoneData setValue:siteContact forKey:@"SiteContact"];
                    }
                    [orderMilestoneData setValue:[NSNumber numberWithBool:isAltSiteContactAvailable] forKey:@"IsAltSiteContactAvailable"];
                    if (isAltSiteContactAvailable) {
                        [orderMilestoneData setValue:altSiteContact forKey:@"AltSiteContact"];
                    }
                    
                    [arrayOfMilestoneData addObject:orderMilestoneData];
                    
                }
                else
                {
                    //TODO: (LP) 15/03/2017 Need to discuss with Storyowner
                    DDLogError(@"'Nodes' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]);
                    
                    BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"'Nodes' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]]));
                }
        }
        }
        
        DDLogInfo(@"Successfully fetched order milestone data for order reference '%@' using webservice with URL: %@", _orderRef, [[task currentRequest] URL]);
        
        [self.getOrderMilestonesWebServiceDelegate getOrderMilestonesWebService:self successfullyFetchedOrderMilestonesData:arrayOfMilestoneData];
    }
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    [self.getOrderMilestonesWebServiceDelegate getOrderMilestonesWebService:self failedToFetchOrderMilestonesDataWithWebServiceError:webServiceError];
}


#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        
        NSArray *resultArray = [responseObject valueForKey:kNLResponseKeyForResult];
        if([resultArray isKindOfClass:[NSArray class]])
        {
            // (hds) Do Nothing Here.
        }
        else
        {
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }
    return errorToBeReturned;
}

#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;
    
    return nil;
}




@end
