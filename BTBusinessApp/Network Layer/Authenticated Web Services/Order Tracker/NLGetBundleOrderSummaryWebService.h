//
//  NLGetBundleOrderSummaryWebService.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 13/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAPIGEEAuthenticatedWebService.h"

@class NLGetBundleOrderSummaryWebService;
@class BTBundleOrder;

@protocol NLGetBundleOrderSummaryWebServiceDelegate 

- (void)getBundleOrderSummaryWebService:(NLGetBundleOrderSummaryWebService *)webService successfullyFetchedBundleOrderSummaryData:(BTBundleOrder *)bundleOrder;

- (void)getBundleOrderSummaryWebService:(NLGetBundleOrderSummaryWebService *)webService failedToFetchBundleOrderSummaryDataWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLGetBundleOrderSummaryWebService : NLAPIGEEAuthenticatedWebService

@property (nonatomic, weak) id <NLGetBundleOrderSummaryWebServiceDelegate> getBundleOrderSummaryWebServiceDelegate;

- (instancetype)initWithOrderRef:(NSString *)orderRef andItemRef:(NSString *)itemRef;

@end
