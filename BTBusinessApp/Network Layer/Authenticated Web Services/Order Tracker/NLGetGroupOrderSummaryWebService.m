//
//  NLGetGroupOrderSummaryWebService.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 05/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLGetGroupOrderSummaryWebService.h"
#import "NSObject+APIResponseCheck.h"

#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NLConstants.h"
#import "AppConstants.h"
#import "BTOrder.h"
#import "AppManager.h"
#import "NLWebServiceError.h"

@implementation NLGetGroupOrderSummaryWebService

- (instancetype)initWithGroupOrderRef:(NSString *)groupOrderRef
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business/v1/group-orders/%@", groupOrderRef];
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
        _groupOrderRef = [groupOrderRef copy];
    }
    return self;
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
    // TODO: (LP) 09/10/2016 Make Group order as subclass of BTOrder
    NSString *orderNumber = [[[resultDic valueForKey:@"OrderNumber"] validAndNotEmptyStringObject] copy];
    NSDate *placedOn = [AppManager NSDateWithUTCFormatFromNSString:[[[resultDic valueForKey:@"PlacedOn"] validAndNotEmptyStringObject] copy]];
    
    if (orderNumber != nil)
    {
        NSArray *resultOrders = nil;
        if ([resultDic valueForKey:@"Orders"]) {
            resultOrders = [resultDic valueForKey:@"Orders"];
            
            NSMutableArray *orders = [NSMutableArray array];
            for (NSDictionary *orderDict in resultOrders) {
                
                BTOrder *order = [[BTOrder alloc] initOrderWithResponseDictionaryFromGroupOrderSummaryAPIResponse:orderDict];
                [orders addObject:order];
            }
            DDLogInfo(@"Successfully fetched OrderDashBoard data using webservice with URL: %@", [[task currentRequest] URL]);
            
            [self.getGroupOrderSummaryWebServiceDelegate getGroupOrderSummaryWebService:self successfullyFetchedGroupOrderSummaryDataWithOrders:orders withOrderNumber:orderNumber withPlacedOnDate:placedOn];
            
        }
        else
        {
            DDLogError(@"'Orders' element is missing in response json object for webservice with URL: %@", [[task currentRequest] URL]);
            
            BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"'Orders' element is missing in response json object for webservice with URL: %@", [[task currentRequest] URL]]));
            
            [self.getGroupOrderSummaryWebServiceDelegate getGroupOrderSummaryWebService:self failedToFetchGroupOrderSummaryDataWithWebServiceError:[NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject]];
        }
    }
    else
    {
        DDLogError(@"'OrderNumber' element is missing in response json object for webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"'OrderNumber' element is missing in response json object for webservice with URL: %@", [[task currentRequest] URL]]));
        
        [self.getGroupOrderSummaryWebServiceDelegate getGroupOrderSummaryWebService:self failedToFetchGroupOrderSummaryDataWithWebServiceError:[NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject]];
    }
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.getGroupOrderSummaryWebServiceDelegate getGroupOrderSummaryWebService:self failedToFetchGroupOrderSummaryDataWithWebServiceError:webServiceError];
}


#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        
        NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
        if([resultDic isKindOfClass:[NSDictionary class]])
        {
            // (hds) Do Nothing Here.
        }
        else
        {
            DDLogError(@"'result' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]);
            
            
            if([responseObject valueForKey:kNLResponseKeyForCode])
            {
                NSInteger errorCode = [[responseObject valueForKey:kNLResponseKeyForCode] integerValue];
                DDLogError(@"Error code: %ld in webservice with URL: %@", (long)errorCode, [[task currentRequest] URL]);
                
                if (errorCode == BTNetworkErrorCodeAPINoDataFound) {
                    errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:errorCode];
                }
                else
                {
                    errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
                }
            }
            else
            {
                errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
            }
        }
    }
    return errorToBeReturned;
}


#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;
    
    return nil;
}

@end
