//
//  NL/Users/a606461564/Code/BTBusiness-iOS/BTBusinessApp.xcodeprojGetOrderSummaryWebService.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 22/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "NLAPIGEEAuthenticatedWebService.h"

@class NLGetOrderSummaryWebService;
@class BTOrder;
@class NLWebServiceError;

@protocol NLGetOrderSummaryWebServiceDelegate 

- (void)getOrderSummaryWebService:(NLGetOrderSummaryWebService *)webService successfullyFetchedOrderSummaryData:(BTOrder *)order;

- (void)getOrderSummaryWebService:(NLGetOrderSummaryWebService *)webService failedToFetchOrderSummaryDataWithWebServiceError:(NLWebServiceError *)error;

@end

//@interface NLGetOrderSummaryWebService : NLAuthenticationProtectedWebService {

@interface NLGetOrderSummaryWebService : NLAPIGEEAuthenticatedWebService {
    
}

@property (nonatomic, readonly) NSString *orderRef;
@property (nonatomic, weak) id <NLGetOrderSummaryWebServiceDelegate> getOrderSummaryWebServiceDelegate;

- (instancetype)initWithOrderRef:(NSString *)orderRef;
@end
