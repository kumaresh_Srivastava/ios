//
//  NLOrderDetailsWebService.h
//  BTBusinessApp
//
//  Created by Accolite on 05/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "NLAPIGEEAuthenticatedWebService.h"

@class NLOrderDetailsWebService;
@class BTOrder;
@class BTProduct;
@class BTBillingDetails;
@class BTPricingDetails;
@class BTProductDetails;
@class NLWebServiceError;

@protocol NLOrderDetailsWebServiceDelegate 

- (void)getOrderDetailsWebService:(NLOrderDetailsWebService *)webService successfullyFetchedOrderDetailsProductData:(BTProduct *)productSummary billingDetails:(BTBillingDetails *)billingDetails pricingDetails:(BTPricingDetails *)pricingDetails productDetails:(BTProductDetails *)productDetails milestoneData:(NSArray *)arrayOfOrderMilestoneData isAuthLevel:(NSInteger)authLevel isSIM2Order:(BOOL)isSIM2;

- (void)getOrderDetailsWebService:(NLOrderDetailsWebService *)webService failedToFetchOrderDetailsDataWithWebServiceError:(NLWebServiceError *)error;

@end


//@interface NLOrderDetailsWebService : NLAuthenticationProtectedWebService
@interface NLOrderDetailsWebService : NLAPIGEEAuthenticatedWebService

@property (nonatomic, weak) id <NLOrderDetailsWebServiceDelegate> getOrderDetailsWebServiceDelegate;

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef;
- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef postCode:(NSString *)postCode errorCount:(NSInteger )errorCount;

@end
