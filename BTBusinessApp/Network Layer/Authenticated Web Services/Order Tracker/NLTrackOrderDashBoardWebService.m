//
//  NLTrackOrderDashBoardWebService.m
//  BTBusinessApp
//
//  Created by Accolite on 22/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLTrackOrderDashBoardWebService.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "BTOrder.h"
#import "AppConstants.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NLWebServiceError.h"

@implementation NLTrackOrderDashBoardWebService {

    int _tabID, _pageIndex;
}


- (instancetype)initWithPageSize:(int)pageSize pageIndex:(int)pageIndex andTabID:(int)tabID
{
     NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v1/Order/%@/All/%d/%d/%d",[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey, pageSize, pageIndex, tabID];

    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
        _tabID = tabID;
        _pageIndex = pageIndex;
        _timeOutTimeInterval = 60.0;
//        if (kBTServerType == BTServerTypeModelA) {
//            _maxTimeoutRetryAttempts = 0;
//            _timeOutTimeInterval = 0.1f;
//        }
    }
    return self;
}



#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];

    NSString *groupKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:groupKey forKey:@"_authKey"];

    return [finalHeaderFieldDic copy];
}


#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];

    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
    if (resultDic[@"PageIndex"]) {
        NSNumber *pageIndexNumber = resultDic[@"PageIndex"];
        _pageIndex = (int)pageIndexNumber.integerValue;
    }
    int totalSize = 0;
    if (resultDic[@"TotalSize"]) {

        NSNumber *totalSizeNumber = resultDic[@"TotalSize"];
        totalSize = (int)totalSizeNumber.integerValue;

    }
    NSArray *resultOrders;
    if (resultDic[@"Orders"] || resultDic[@"PageIndex"]) {
        resultOrders = [NSArray arrayWithArray:resultDic[@"Orders"]];

        NSMutableArray *orders = [[NSMutableArray alloc] init];
        for (NSDictionary *orderDict in resultOrders) {

            BTOrder *order = [[BTOrder alloc] initWithResponseDictionaryFromOrderDashBoardAPIResponse:orderDict];
            [orders addObject:order];
        }

        DDLogInfo(@"Successfully fetched OrderDashBoard data using webservice with URL: %@", [[task currentRequest] URL]);

        [self.getTrackOrderDashBoardWebServiceDelegate getOrderDashBoardWebService:self
                                             successfullyFetchedOrderDashBoardData:orders
                                                                         pageIndex:_pageIndex
                                                                         totalSize:totalSize
                                                                             tabID:_tabID];

    }
    else
    {
        NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
        NLWebServiceError *webServiceError = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];

        DDLogError(@"'Orders' element missing in response json object in webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"'Orders' element missing in response json object in webservice with URL: %@", [[task currentRequest] URL]]));
        

        [self.getTrackOrderDashBoardWebServiceDelegate getOrderDashBoardWebService:self failedToFetchOrderDashBoardDataWithWebServiceError:webServiceError];
    }
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];

    [self.getTrackOrderDashBoardWebServiceDelegate getOrderDashBoardWebService:self failedToFetchOrderDashBoardDataWithWebServiceError:webServiceError];
}


#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.

        NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
        if([resultDic isKindOfClass:[NSDictionary class]])
        {
            // (hds) Do Nothing Here.
        }
        else
        {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];

            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];

            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }
    
    return errorToBeReturned;
}


#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
//        NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeAPINoDataFound userInfo:nil];
//    
//        NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    
//        return errorToBeReturned;
    
    return nil;
}



@end
