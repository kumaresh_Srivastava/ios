//
//  NLOrderDetailsWebService.m
//  BTBusinessApp
//
//  Created by Accolite on 05/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLOrderDetailsWebService.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "BTOrder.h"
#import "AppConstants.h"

#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"

#import "BTProduct.h"
#import "BTBillingDetails.h"
#import "BTPricingDetails.h"
#import "BTProductDetails.h"
#import "BTOrderMilestoneNodeModel.h"
#import "BTOrderDispatchDetailsModel.h"
#import "BTOrderSiteContactModel.h"
#import "NLWebServiceError.h"

@implementation NLOrderDetailsWebService 

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef {
    
    //NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v1/Order/%@/%@",orderRef,itemRef];
//    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/orders/%@/%@",orderRef,itemRef];
    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/orders/%@/%@",orderRef,itemRef];
    
    DDLogInfo(@"INFO: OrderDetailsURL:%@",endPointURLString);
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
         if ([kBTServerType isEqualToString:@"BTServerTypeModelA"]) {
                _timeOutTimeInterval = 60.0;//KUMARESH
         }
    }
    return self;
}


- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef postCode:(NSString *)postCode errorCount:(NSInteger )errorCount
{
    //NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v1/Order/%@/%@/%@/%ld",orderRef,itemRef,postCode,(long)errorCount];

    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business/v1/orders/%@/%@?postCode=%@",orderRef, itemRef, postCode];
    
    DDLogInfo(@"INFO: OrderDetailsURL:%@",endPointURLString);
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
        if ([kBTServerType isEqualToString:@"BTServerTypeModelA"]) {
            _timeOutTimeInterval = 60.0;//KUMARESH
        }
    }
    return self;
}


#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    NSString *appInstallationKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:appInstallationKey forKey:@"_authKey"];
    
    return [finalHeaderFieldDic copy];
}


#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
    NSInteger authLevel = -1;
    if (resultDic[@"AuthLevel"]) {
        NSNumber *authLevelNumber = resultDic[@"AuthLevel"];
        authLevel = authLevelNumber.integerValue;
    }
    BTProduct *product = nil;
    if (resultDic[@"ProductSummary"]) {
        product = [[BTProduct alloc] initProductWithResponse:resultDic[@"ProductSummary"] andIndexInAPIResponse:0];
    }
    
    if ([resultDic valueForKey:@"OrderMetaInfo"]) {
        [product updateIsProductRelatedToStrategicOrderWith:[resultDic valueForKey:@"OrderMetaInfo"]];
    }
    
    BTBillingDetails *billingDetails = nil;
    if (resultDic[@"BillingDetails"]) {
        billingDetails = [[BTBillingDetails alloc] initBillingDetailsWithResponse:resultDic[@"BillingDetails"]];
    }
    BTPricingDetails *pricingDetails = nil;
    if (resultDic[@"ProductOrderItems"]) {
        pricingDetails = [[BTPricingDetails alloc] initPricingDetailsWithResponse:resultDic[@"ProductOrderItems"]];
    }
    BTProductDetails *productDetails = nil;
    if (resultDic[@"ProductDetails"]) {
        productDetails = [[BTProductDetails alloc] initProductDetailsWithResponse:resultDic[@"ProductDetails"]];
    }
    
    NSString* linkedOrderReference = [[resultDic[@"LinkedOrderReference"] validAndNotEmptyStringObject] copy];
    BOOL isSIM2 = NO;
    if (linkedOrderReference && [linkedOrderReference rangeOfString:@"SIM2"].location != NSNotFound) {
        isSIM2 = YES;
    }
    
    NSArray *arrayOfOrderMilestoneData = nil;

    if (authLevel != 1) {
        
        NSArray *arrayOfTimeLineDetails = [resultDic valueForKey:@"TimeLineDetails"];
        if(arrayOfTimeLineDetails && [arrayOfTimeLineDetails count] >0)
        {
            NSMutableArray *arrayOfMilestoneData = [NSMutableArray array];
            
            for (NSDictionary *timeLineDetails in arrayOfTimeLineDetails)
            {
                if (timeLineDetails && [timeLineDetails isKindOfClass:[NSDictionary class]]) {
                    
                    NSArray *nodes = [timeLineDetails valueForKey:@"Nodes"];
                    if ([nodes isKindOfClass:[NSArray class]])
                    {
                        NSMutableArray *arrayToAddNodes = [NSMutableArray array];
                        NSDate *activationDate = nil;
                        BOOL isActivationDateAvailable = NO;
                        NSDate *engineerAppointmentDate = nil;
                        BOOL isEngineerAppointmentDateAvailable = YES;//NO;
                        NSArray *dispatchDetails = nil;
                        BTOrderSiteContactModel *siteContact = nil;
                        BOOL isSiteContactAvailable = NO;
                        BTOrderSiteContactModel *altSiteContact = nil;
                        BOOL isAltSiteContactAvailable = NO;
                        
                        for (NSDictionary *milestoneNode in nodes)
                        {
                            BTOrderMilestoneNodeModel *nodeData = [[BTOrderMilestoneNodeModel alloc] initMilestoneNodeWithResponseDic:milestoneNode];
                            if (nodeData.isActivationAmendSource) {
                                activationDate = nodeData.date;
                                isActivationDateAvailable = YES;
                            }
                            if (nodeData.isAppointmentAmendSource) {
                                engineerAppointmentDate = nodeData.date;
                                isEngineerAppointmentDateAvailable = YES;
                            }
                            if (nodeData.isDispatchDetailsAvailable) {
                                
                                dispatchDetails =[[BTOrderDispatchDetailsModel alloc] getDispatchDetailsFrom:milestoneNode];
                            }
                            if (nodeData.isSiteContactAvailable) {
                                
                                siteContact = [[BTOrderSiteContactModel alloc] initSiteContactWithResponseDict:[milestoneNode objectForKey:@"SiteContact"]];
                                isSiteContactAvailable = YES;
                            }
                            if (nodeData.isAltSiteContactAvailable) {
                                
                                altSiteContact = [[BTOrderSiteContactModel alloc] initSiteContactWithResponseDict:[milestoneNode objectForKey:@"AlternateSiteContact"]];
                                isAltSiteContactAvailable = YES;
                            }
                            [arrayToAddNodes addObject:nodeData];
                        }
                        
                        NSMutableDictionary *orderMilestoneData = [NSMutableDictionary dictionary];
                        [orderMilestoneData setValue:arrayToAddNodes forKey:@"MilestoneNodes"];
                        [orderMilestoneData setValue:dispatchDetails forKey:@"DispatchDetails"];
                        [orderMilestoneData setValue:[NSNumber numberWithBool:isActivationDateAvailable] forKey:@"IsActivationDateAvailable"];
                        if (isActivationDateAvailable) {
                            [orderMilestoneData setValue:activationDate forKey:@"ActivationDate"];
                        }
                        [orderMilestoneData setValue:[NSNumber numberWithBool:isEngineerAppointmentDateAvailable] forKey:@"IsEngineerAppointmentDateAvailable"];
                        if (isEngineerAppointmentDateAvailable) {
                            [orderMilestoneData setValue:engineerAppointmentDate forKey:@"EngineerAppointmentDate"];
                        }
                        [orderMilestoneData setValue:[NSNumber numberWithBool:isSiteContactAvailable] forKey:@"IsSiteContactAvailable"];
                        if (isSiteContactAvailable) {
                            [orderMilestoneData setValue:siteContact forKey:@"SiteContact"];
                        }
                        [orderMilestoneData setValue:[NSNumber numberWithBool:isAltSiteContactAvailable] forKey:@"IsAltSiteContactAvailable"];
                        if (isAltSiteContactAvailable) {
                            [orderMilestoneData setValue:altSiteContact forKey:@"AltSiteContact"];
                        }
                        
                        [arrayOfMilestoneData addObject:orderMilestoneData];
                    }
                    else
                    {
                        DDLogError(@"'Nodes' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]);
                        
                        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"'Nodes' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]]));
                        
                    }
                }
            }
            
            arrayOfOrderMilestoneData = [NSArray arrayWithArray:arrayOfMilestoneData];
        }
        else
        {
            DDLogError(@"'TimeLineDetails' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]);
            
            BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"'TimeLineDetails' element missing in response json object for webservice with URL: %@", [[task currentRequest] URL]]));
        }
        
    }
    
    if(product)
    {
        DDLogInfo(@"Successfully fetched OrderDetails data using webservice with URL: %@", [[task currentRequest] URL]);
        
        [self.getOrderDetailsWebServiceDelegate getOrderDetailsWebService:self successfullyFetchedOrderDetailsProductData:product billingDetails:billingDetails pricingDetails:pricingDetails productDetails:productDetails milestoneData:arrayOfOrderMilestoneData isAuthLevel:authLevel isSIM2Order:isSIM2];
    }
    else
    {
        DDLogError(@"Unable to create model object using response json object in webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Unable to create model object using response json object in webservice with URL: %@", [[task currentRequest] URL]]));
        
        [self.getOrderDetailsWebServiceDelegate getOrderDetailsWebService:self failedToFetchOrderDetailsDataWithWebServiceError:[NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject]];
    }
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    [self.getOrderDetailsWebServiceDelegate getOrderDetailsWebService:self failedToFetchOrderDetailsDataWithWebServiceError:webServiceError];
}


#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        
        NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
        if([resultDic isKindOfClass:[NSDictionary class]])
        {
            // (hds) Do Nothing Here.
        }
        else
        {
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }
    return errorToBeReturned;
}

#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;
    
    return nil;
}

@end
