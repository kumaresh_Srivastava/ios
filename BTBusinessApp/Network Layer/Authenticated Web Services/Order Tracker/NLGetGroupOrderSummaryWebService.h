//
//  NLGetGroupOrderSummaryWebService.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 05/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAPIGEEAuthenticatedWebService.h"

@class NLGetGroupOrderSummaryWebService;
@class BTOrder;
@class NLWebServiceError;

@protocol NLGetGroupOrderSummaryWebServiceDelegate 

- (void)getGroupOrderSummaryWebService:(NLGetGroupOrderSummaryWebService *)webService successfullyFetchedGroupOrderSummaryDataWithOrders:(NSArray *)arrayOfOrders withOrderNumber:(NSString *)orderNumber withPlacedOnDate:(NSDate *)placedOnDate;

- (void)getGroupOrderSummaryWebService:(NLGetGroupOrderSummaryWebService *)webService failedToFetchGroupOrderSummaryDataWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLGetGroupOrderSummaryWebService : NLAPIGEEAuthenticatedWebService {
}

@property (nonatomic, readonly) NSString *groupOrderRef;
@property (nonatomic, weak) id <NLGetGroupOrderSummaryWebServiceDelegate> getGroupOrderSummaryWebServiceDelegate;

- (instancetype)initWithGroupOrderRef:(NSString *)groupOrderRef;

@end
