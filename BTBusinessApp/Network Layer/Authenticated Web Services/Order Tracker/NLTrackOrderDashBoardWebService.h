//
//  NLTrackOrderDashBoardWebService.h
//  BTBusinessApp
//
//  Created by Accolite on 22/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"

@class NLTrackOrderDashBoardWebService;
@class BTOrder;
@class NLWebServiceError;

@protocol NLTrackOrderDashBoardWebServiceDelegate

- (void)getOrderDashBoardWebService:(NLTrackOrderDashBoardWebService *)webService successfullyFetchedOrderDashBoardData:(NSArray *)orders pageIndex:(int)pageIndex totalSize:(int)totalSize tabID:(int)tabID;

- (void)getOrderDashBoardWebService:(NLTrackOrderDashBoardWebService *)webService failedToFetchOrderDashBoardDataWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLTrackOrderDashBoardWebService : NLAuthenticationProtectedWebService

@property (nonatomic, weak) id <NLTrackOrderDashBoardWebServiceDelegate> getTrackOrderDashBoardWebServiceDelegate;

- (instancetype)initWithPageSize:(int)pageSize pageIndex:(int)pageIndex andTabID:(int)tabID;

@end
