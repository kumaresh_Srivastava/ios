//
//  NLGetBundleOrderSummaryWebService.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 13/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLGetBundleOrderSummaryWebService.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "AppConstants.h"

#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "BTBundleOrder.h"
#import "NLWebServiceError.h"

@implementation NLGetBundleOrderSummaryWebService

- (instancetype)initWithOrderRef:(NSString *)orderRef andItemRef:(NSString *)itemRef
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business/v1/bundle-orders/%@/%@", orderRef, itemRef];

    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];

    if(self)
    {
        _timeOutTimeInterval = kNLWebServiceRequestExtendedTimeOut;
    }
    return self;
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];

    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
    BTBundleOrder *bundleOrder = [[BTBundleOrder alloc] initBundleOrderWithResponseDictionaryFromBundleOrderSummaryAPIResponse:resultDic];

    DDLogInfo(@"Successfully fetched Bundle Order Summary with URL : %@", [[task currentRequest] URL]);

    [self.getBundleOrderSummaryWebServiceDelegate getBundleOrderSummaryWebService:self successfullyFetchedBundleOrderSummaryData:bundleOrder];

}



- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];

    [self.getBundleOrderSummaryWebServiceDelegate getBundleOrderSummaryWebService:self failedToFetchBundleOrderSummaryDataWithWebServiceError:webServiceError];
}



#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];

    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.

        NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
        if([resultDic isKindOfClass:[NSDictionary class]])
        {

        }
        else
        {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];

            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];

            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }

    return errorToBeReturned;
}

@end
