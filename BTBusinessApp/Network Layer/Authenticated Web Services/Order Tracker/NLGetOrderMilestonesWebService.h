//
//  NLGetOrderMilestonesWebService.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 09/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "NLAPIGEEAuthenticatedWebService.h"

@class NLGetOrderMilestonesWebService;
@class NLWebServiceError;

@protocol NLGetOrderMilestonesWebServiceDelegate 

- (void)getOrderMilestonesWebService:(NLGetOrderMilestonesWebService *)webService successfullyFetchedOrderMilestonesData:(NSArray *)arrayOfMilestoneData;

- (void)getOrderMilestonesWebService:(NLGetOrderMilestonesWebService *)webService failedToFetchOrderMilestonesDataWithWebServiceError:(NLWebServiceError *)error;

@end

//@interface NLGetOrderMilestonesWebService : /*NLAPIGEEAuthenticatedWebService*/NLAuthenticationProtectedWebService {
@interface NLGetOrderMilestonesWebService : NLAPIGEEAuthenticatedWebService {
    
}

@property (nonatomic, readonly) NSString *orderRef;
@property (nonatomic, readonly) NSString *itemRef;

@property (nonatomic, weak) id <NLGetOrderMilestonesWebServiceDelegate> getOrderMilestonesWebServiceDelegate;

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef;

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef postCode:(NSString *)postCode errorCount:(NSInteger)errorCount;
@end
