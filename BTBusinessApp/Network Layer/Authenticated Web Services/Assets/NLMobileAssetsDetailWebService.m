//
//  NLMobileAssetsDetailWebService.m
//  BTBusinessApp
//
//  Created by kumaresh shrivastava on 10/06/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import "NLMobileAssetsDetailWebService.h"

#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "BTAsset.h"
#import "AppConstants.h"
#import "BTSMSession.h"
#import "NLWebServiceError.h"


@implementation NLMobileAssetsDetailWebService

- (instancetype)initWithMobileSubscriptionDetailWithServiceID:(NSString *)mobileServiceId
{
    self.mobileServiceID = mobileServiceId;
    
    // NSString *endPointURLString = @"/bt-business-auth/v1/mobile-subscriptions/sims?status=Active";
    NSString *endPointURLString = @"/bt-business-auth/v1/mobile-subscriptions/products-summary";
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    
    return self;
    
}


#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
 {
     NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
     NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
     
     //NSString *appInstallationKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
     //[finalHeaderFieldDic setValue:appInstallationKey forKey:@"_authKey"];
     
     
     // NSString* temp = @"07997497058";
     if(self.mobileServiceID)
      [finalHeaderFieldDic setValue:self.mobileServiceID forKey:@"Mobile-Service-Id"];
     
     
     return [finalHeaderFieldDic copy];
 }
 



#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSArray *resultDic = [responseObject valueForKey:@"productsSummary"];
    BTAsset *btAsset = [[BTAsset alloc] initWithResponseFromMobileServiceApiResponse:resultDic];
    DDLogInfo(@"Successfully fetched AssetBacOverview data using webservice with URL: %@", [[task currentRequest] URL]);
    [self.mobileAssetsDetailWebServiceDelegate getMobileAssetsDetailWebService:self successfullyFetchedMobileAssetsDetailData:btAsset.assetModels];
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.mobileAssetsDetailWebServiceDelegate getMobileAssetsDetailWebService:self failedToFetchMobileAssetsDetailDataWithWebServiceError:webServiceError];
}

#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        
        NSArray *resultDic = [responseObject valueForKey:@"productsSummary"];
        if([resultDic isKindOfClass:[NSArray class]])
        {
            // (hds) Do Nothing Here.
        }
        else
        {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
            
            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
            
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }
    
    return errorToBeReturned;
}


@end
