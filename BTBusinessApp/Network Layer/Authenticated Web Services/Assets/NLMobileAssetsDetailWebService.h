//
//  NLMobileAssetsDetailWebService.h
//  BTBusinessApp
//
//  Created by kumaresh shrivastava on 10/06/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NLAPIGEEAuthenticatedWebService.h"
#import "BTAsset.h"
#import "BTAssetModel.h"
NS_ASSUME_NONNULL_BEGIN


@class NLMobileAssetsDetailWebService;
@class NLWebServiceError;


@protocol NLMobileAssetsDetailWebServiceDelegate

- (void)getMobileAssetsDetailWebService:(NLMobileAssetsDetailWebService *)webService successfullyFetchedMobileAssetsDetailData:(NSArray *)asset;

- (void)getMobileAssetsDetailWebService:(NLMobileAssetsDetailWebService *)webService failedToFetchMobileAssetsDetailDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface NLMobileAssetsDetailWebService : NLAPIGEEAuthenticatedWebService

- (instancetype)initWithMobileSubscriptionDetailWithServiceID:(NSString *)mobileServiceId;

@property (nonatomic, weak) id <NLMobileAssetsDetailWebServiceDelegate> mobileAssetsDetailWebServiceDelegate;
@property (nonatomic, strong) NSString* mobileServiceID;

@end

NS_ASSUME_NONNULL_END
