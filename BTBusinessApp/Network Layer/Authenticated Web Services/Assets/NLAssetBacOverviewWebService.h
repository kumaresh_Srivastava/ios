//
//  NLAssetBacOverviewWebService.h
//  BTBusinessApp
//
//  Created by vectoscalar on 09/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"
#import "NLAPIGEEAuthenticatedWebService.h"
#import "BTAsset.h"
#import "BTAssetModel.h"
#import "BTAssetCombinedRental.h"
#import "BTAssetDetailCollection.h"


@class NLAssetBacOverviewWebService;
@class NLWebServiceError;


@protocol NLAssetsBacOverviewWebServiceDelegate 

- (void)getAssetsBacOverviewWebService:(NLAssetBacOverviewWebService *)webService successfullyFetchedAssetsDashBoardData:(BTAsset *)asset;

- (void)getAssetsBACOverViewWebService:(NLAssetBacOverviewWebService *)webService failedToFetchAssetsBACOverviewDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end


@interface NLAssetBacOverviewWebService : NLAuthenticationProtectedWebService//NLAPIGEEAuthenticatedWebService

@property (nonatomic, weak) id <NLAssetsBacOverviewWebServiceDelegate> assetsBacViewWebServiceDelegate;

- (instancetype)initWithBillingAccountNumber:(NSString *)BAC;

@end
