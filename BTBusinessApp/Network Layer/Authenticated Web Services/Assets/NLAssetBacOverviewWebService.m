//
//  NLAssetBacOverviewWebService.m
//  BTBusinessApp
//
//  Created by vectoscalar on 09/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAssetBacOverviewWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "BTAsset.h"
#import "AppConstants.h"
#import "BTSMSession.h"

#import "NLWebServiceError.h"

@implementation NLAssetBacOverviewWebService

- (instancetype)initWithBillingAccountNumber:(NSString *)BAC
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v2/Assets/%@",BAC];
    
    //NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/assets/%@/products",BAC];
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    
    return self;
    
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

//- (NSDictionary *)httpHeaderFields
//{
//    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
//    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
//
//    NSString *appInstallationKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
//
//    [finalHeaderFieldDic setValue:appInstallationKey forKey:@"_authKey"];
//
//    return [finalHeaderFieldDic copy];
//}

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    NSString *groupKey = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey;
    [finalHeaderFieldDic setValue:groupKey forKey:@"_authKey"];
    [finalHeaderFieldDic setValue:_smSession.smsessionString forKey:@"SMSession"];
    return [finalHeaderFieldDic copy];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
    BTAsset *btAsset = [[BTAsset alloc] initWithResponseDictionaryFromAssetsBACAPIResponse:resultDic];
    DDLogInfo(@"Successfully fetched AssetBacOverview data using webservice with URL: %@", [[task currentRequest] URL]);
    [self.assetsBacViewWebServiceDelegate getAssetsBacOverviewWebService:self successfullyFetchedAssetsDashBoardData:btAsset];
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.assetsBacViewWebServiceDelegate getAssetsBACOverViewWebService:self failedToFetchAssetsBACOverviewDataWithWebServiceError:webServiceError];
}

#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        
        NSDictionary *resultDic = [responseObject valueForKey:kNLResponseKeyForResult];
        if([resultDic isKindOfClass:[NSDictionary class]])
        {
            // (hds) Do Nothing Here.
        }
        else
        {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
            
            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
            
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }
    
    return errorToBeReturned;
}

#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    //    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeSMSessionUnauthenticaiton userInfo:nil];
    //
    //    NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    //
    //    return errorToBeReturned;
    
    return nil;
}

@end
