//
//  NLAssetsDashboardWebService.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 08/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"


@class NLAssetsDashboardWebService;
@class NLWebServiceError;

@protocol NLAssetsDashBoardWebServiceDelegate 

- (void)getAssetsDashBoardWebService:(NLAssetsDashboardWebService *)webService successfullyFetchedAssetsDashBoardData:(NSArray *)assets;

- (void)getAssetsDashBoardWebService:(NLAssetsDashboardWebService *)webService failedToFetchAssetsDashBoardDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface NLAssetsDashboardWebService : NLAuthenticationProtectedWebService

@property (nonatomic, weak) id <NLAssetsDashBoardWebServiceDelegate> assetsDashBoardWebServiceDelegate;


@end
