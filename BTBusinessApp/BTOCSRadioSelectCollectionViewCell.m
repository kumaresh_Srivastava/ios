//
//  BTOCSRadioSelectCollectionViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 16/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSRadioSelectCollectionViewCell.h"

#import "AppConstants.h"
#import "BrandColours.h"

@implementation BTOCSRadioSelectCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.bgView.layer.cornerRadius = 5.0f;
    
    self.indicatorOutlineView.layer.borderColor = [BrandColours colourBtLightBlack].CGColor;
    self.indicatorOutlineView.layer.borderWidth = 1.0f;
    self.indicatorOutlineView.layer.cornerRadius = MAX(self.indicatorOutlineView.frame.size.height, self.indicatorOutlineView.frame.size.width)/2;
    
    self.indicatorView.layer.cornerRadius = MAX(self.indicatorView.frame.size.height, self.indicatorView.frame.size.width)/2;
}

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    if (selected) {
        self.indicatorView.hidden = NO;
        self.bgView.backgroundColor = [UIColor whiteColor];
        self.bgView.layer.borderColor = [BrandColours colourBtLightBlack].CGColor;
        self.bgView.layer.borderWidth = 1.0f;
    } else {
        self.indicatorView.hidden = YES;
        self.bgView.backgroundColor = [BrandColours colourBtLightGray];
        self.bgView.layer.borderColor = [BrandColours colourBtLightGray].CGColor;
        self.bgView.layer.borderWidth = 1.0f;
    }
    [self.contentView setNeedsLayout];
}

@end
