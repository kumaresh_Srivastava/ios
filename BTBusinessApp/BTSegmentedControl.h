//
//  BTSegmentedControl.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 21/12/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BTSegmentedControl : UISegmentedControl

@end

NS_ASSUME_NONNULL_END
