//
//  KeychainWrapper.h
//  BT FON
//
//  Created by Scott Runciman on 02/12/2013.
//  Copyright (c) 2013 BT PLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Security/Security.h>

typedef NS_ENUM(NSUInteger, kSecKeyType) {
    kPIN,
    kUser,
    kPassword
};

@interface KeychainWrapper : NSObject

+ (BOOL)setKeychainValue:(NSString *)value withKey:(kSecKeyType)key forKeychainIdentifier:(NSString *)identifier;

+ (void)deleteKeychainValue:(NSString *)identifier;

+ (NSData *)searchKeychainCopyMatching:(NSString *)identifier inAccessGroup:(NSString *)accessGroup;
+ (NSData *)searchKeychainCopyMatching:(NSString *)identifier;

+ (CFTypeRef)attrForkSecKeyType:(kSecKeyType) key;


@end
