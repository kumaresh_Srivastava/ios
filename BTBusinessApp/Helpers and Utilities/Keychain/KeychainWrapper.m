//
//  KeychainWrapper.m
//  BT FON
//
//  Created by Scott Runciman on 02/12/2013.
//  Copyright (c) 2013 BT PLC. All rights reserved.
//

#import "KeychainWrapper.h"

@implementation KeychainWrapper

//static NSString *serviceName = @"com.bt.WISPrKeychain";

+ (NSMutableDictionary *)newSearchDictionary:(NSString *)identifier inAccessgroup:(NSString *)accessGroup{
    NSMutableDictionary *searchDictionary = [[NSMutableDictionary alloc] init];

    [searchDictionary setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];

    [searchDictionary setObject:identifier forKey:(__bridge id)kSecAttrService];
    //[searchDictionary setObject:encodedIdentifier forKey:(__bridge id)kSecAttrGeneric];
    if (accessGroup !=nil) {
        [searchDictionary setObject:accessGroup forKey:(__bridge id)kSecAttrAccessGroup];
    }


    return searchDictionary;
}

+ (NSMutableDictionary *)newSearchDictionary:(NSString *)identifier {

    return [self newSearchDictionary:identifier inAccessgroup:nil];
}

+ (NSData *)searchKeychainCopyMatching:(NSString *)identifier inAccessGroup:(NSString *)accessGroup {
    NSMutableDictionary *searchDictionary = [self newSearchDictionary:identifier inAccessgroup:accessGroup];

    // Add search attributes
    [searchDictionary setObject:(__bridge id)kSecMatchLimitAll forKey:(__bridge id)kSecMatchLimit];

    // Add search return types
    [searchDictionary setObject:(id)kCFBooleanTrue forKey:(__bridge id)kSecReturnAttributes];
    [searchDictionary setObject:(id)kCFBooleanTrue forKey:(__bridge id<NSCopying>)(kSecReturnData)];

    NSData *result = nil;
    OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)searchDictionary,
                                          (void *)&result);
    if (status == errSecSuccess) {
        return result;
    }

    return nil;
}


+ (NSData *)searchKeychainCopyMatching:(NSString *)identifier {

    return [self searchKeychainCopyMatching:identifier inAccessGroup:nil];
}

+ (BOOL)setKeychainValue:(NSString *)value withKey:(kSecKeyType)key forKeychainIdentifier:(NSString *)identifier {

    if (value == nil) {
        [self deleteKeychainValue:identifier];
        return YES;
    }

    NSMutableDictionary *searchDictionary = [self newSearchDictionary:identifier];
    NSMutableDictionary *updateDictionary = [[NSMutableDictionary alloc] init];

    NSData *valueData = [value dataUsingEncoding:NSUTF8StringEncoding];
    [updateDictionary setObject:valueData forKey:[self attrForkSecKeyType:key]];

    OSStatus status = SecItemUpdate((__bridge CFDictionaryRef)searchDictionary,
                                    (__bridge CFDictionaryRef)updateDictionary);

    if (status == errSecSuccess) {
        return YES;
    } else if(status == errSecItemNotFound) {
        NSMutableDictionary *dictionary = [self newSearchDictionary:identifier];

        NSData *valueData = [value dataUsingEncoding:NSUTF8StringEncoding];
        [dictionary setObject:valueData forKey:(__bridge id)[self attrForkSecKeyType:key]];

        OSStatus status = SecItemAdd((__bridge CFDictionaryRef)dictionary, NULL);

        if (status == errSecSuccess) {
            return YES;
        }
        return NO;
    }
    return NO;
}

+ (void)deleteKeychainValue:(NSString *)identifier {

    NSMutableDictionary *searchDictionary = [KeychainWrapper newSearchDictionary:identifier];
    SecItemDelete((__bridge CFDictionaryRef)searchDictionary);
    // Changed line above to remove OSStatus status = SecItemDelete... because the return value was ignored.
}

+(CFTypeRef)attrForkSecKeyType:(kSecKeyType) key{
    
    // GAZZA - TO BE VERIFIED
    switch (key) {
        case kPIN:
            return kSecValueData;
        case kUser:
            return kSecAttrGeneric;
        case kPassword:
            return kSecAttrLabel;
        default:
            NSLog(@"Attempt to get Keychain attr for non defined kSecKeyType. This will probably cause trouble");
            break;
    }
}

@end
