//
//  BTUtils.h
//  BT Graduates
//
//  Created by Sam McNeilly on 24/07/2015.
//  Copyright © 2015 BT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTOrderSiteContactModel.h" //(SL)

@interface BTUtils : NSObject

//validateString not used anywhere
+ (BOOL)validateString:(NSString*)string withLength:(NSInteger)length;


+ (NSString*)stringByTrimmingWhitespaceInString:(NSString*)string;

//emailAddressIsValid not used anywhere
+ (BOOL)emailAddressIsValid:(NSString*)emailAddress;


//isPad not used anywhere
+ (BOOL)isPad;

+(BOOL)isValidDate:(NSDate *)date;

+ (NSDate *)NSDateWithMonthNameWithSpaceFormatFromNSString:(NSString *)dateString;
+ (NSString *)NSStringFromNSDateWithMonthNameWithSpaceeFormat:(NSDate *)date;
+ (NSString *)NSStringFromNSDateWithSlashFormat:(NSDate *)date;
+ (NSDate *)NSDateWithUTCFormatFromNSString:(NSString *)dateString;
+ (NSString *)NSStringWithOmnitureFormatFromNSDate : (NSDate *)date;
+ (NSString *)NSDateWithMonthNameFromNSString : (NSString*)string;
+(NSString *)getHourFromDateString:(NSString *)dateString;
+(NSString *)getMinuteFromDateString:(NSString *)dateString;
+(NSInteger)getHourFromDate:(NSDate *)date;
+(NSInteger)getMinuteFromDate:(NSDate *)date;
+ (NSString *)documentDirectoryPath;
+ (NSString *)getTimeFromDate:(NSDate *)date;
+ (NSString *)getDayMonthFromDate:(NSDate *)date;

//getDayDateMonthFormattedStringFromDate not used anywhere
+ (NSString *)getDayDateMonthFormattedStringFromDate:(NSDate *)date;

+ (NSString *)NSStringFromNSDateWithReverseDateFormat:(NSDate *)date;
//(SL)
+ (NSString *)getContactNumberFromSiteContact:(BTOrderSiteContactModel *)siteContact;
+ (NSString *)NSStringFromNSDateWithMonthNameWithHiphenFormat:(NSDate *)date;
+ (BOOL)isDate:(NSDate *)firstDate isEqualTo:(NSDate *)secondDate;
+ (BOOL)isInternetConnectionAvailable;
+ (NSArray *)getBTUserTitles;
+ (NSArray *)getBTUserSecurityQuestions;

+ (NSDictionary *)getOmniDictionary;
+ (void)trackOmnitureKeyTask:(NSString *)keyTask forPageName:(NSString *)pageName;

@end
