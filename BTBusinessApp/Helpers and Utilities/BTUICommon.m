//
//  OpenreachUICommon.m
//  A55
//
//  Created by Sam McNeilly on 17/09/2014.
//  Copyright (c) 2014 BT. All rights reserved.
//

#import "BTUICommon.h"
#import "AppDelegate.h"
#import "AppConstants.h"
#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface BTUICommon ()

@end

@implementation BTUICommon

- (id) initWithNibName : (NSString*) nibNameOrNil bundle : (NSBundle*) nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    return self;
}

- (void) viewDidLoad {
    [super viewDidLoad];
}

- (void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


+ (NSArray*) framesForAnimationWithName : (NSString*) name andLength : (NSInteger) numFrames {
    NSMutableArray* frames = [NSMutableArray new];
    
    for (NSInteger i = 0; i < numFrames; i ++) {
        NSString* thisFrameName = [NSString stringWithFormat:@"%@_%03ld", name, (long) i];
        UIImage* thisFrame = [UIImage imageNamed:thisFrameName];
        
        if (thisFrame) {
            [frames addObject:thisFrame];
        }
    }
    
    return frames;
}

+ (UIImage*) screenshotForCurrentView {
    UIScreen* screen = [UIScreen mainScreen];
    UIWindow* keyWindow = [[UIApplication sharedApplication] keyWindow];
    
    UIGraphicsBeginImageContextWithOptions(screen.bounds.size, false, 0);
    [keyWindow drawViewHierarchyInRect:[keyWindow bounds] afterScreenUpdates:false];
    UIImage* screenshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return screenshotImage;
}

+ (UIImage*) applyGaussianBlurToImage : (UIImage*) image withRadius : (float) radius {
    CIFilter* gaussianBlurFilter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [gaussianBlurFilter setDefaults];
    [gaussianBlurFilter setValue:[CIImage imageWithCGImage:[image CGImage]] forKey:kCIInputImageKey];
    [gaussianBlurFilter setValue:[NSNumber numberWithFloat:radius] forKey:kCIInputRadiusKey];
    
    CIImage* outputImage = [gaussianBlurFilter outputImage];
    CIContext* context = [CIContext contextWithOptions:nil];
    CGRect rect = [outputImage extent];
    
    // Ensure output image is the same size as input image.
    // rect.origin.x += (rect.size.width - image.size.width ) / 2;
    // rect.origin.y += (rect.size.height - image.size.height) / 2;
    // rect.size = image.size;
    
    CGImageRef cgimg     = [context createCGImage:outputImage fromRect:rect];
    UIImage* imageToReturn       = [UIImage imageWithCGImage:cgimg];
    CGImageRelease(cgimg);
    return imageToReturn;
}

+ (UIImage*) captureScreenshotForView : (UIView*) view {
    CGRect rect = [view bounds];
    
    
    
    UIGraphicsBeginImageContextWithOptions(rect.size, true, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:context];
    UIImage *capturedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return capturedImage;
}

+ (UIImage *)filledImageFrom:(UIImage *)source withColor:(UIColor *)color{
    
    // begin a new image context, to draw our colored image onto with the right scale
    UIGraphicsBeginImageContextWithOptions(source.size, NO, [UIScreen mainScreen].scale);
    
    // get a reference to that context we created
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // set the fill color
    [color setFill];
    
    // translate/flip the graphics context (for transforming from CG* coords to UI* coords
    CGContextTranslateCTM(context, 0, source.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGContextSetBlendMode(context, kCGBlendModeColorBurn);
    CGRect rect = CGRectMake(0, 0, source.size.width, source.size.height);
    CGContextDrawImage(context, rect, source.CGImage);
    
    CGContextSetBlendMode(context, kCGBlendModeSourceIn);
    CGContextAddRect(context, rect);
    CGContextDrawPath(context,kCGPathFill);
    
    // generate a new UIImage from the graphics context we drew onto
    UIImage *coloredImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //return the color-burned image
    return coloredImg;
}

// TODO: (lp) 23/09/2016 make view controller here and later change it to view controller only.
+ (void) showAlertWithShortMessage : (NSString*) message {
    
    //Sal - Removing depreciated Alert View
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                          }];
    [alert addAction:defaultAction];
    
    UIWindow *alertWindow = [(AppDelegate *)[[UIApplication sharedApplication] delegate] window];
    [alertWindow.rootViewController presentViewController:alert animated:YES completion:nil];

    
    
    /*
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle : message
                                                    message : nil
                                                   delegate : nil
                                          cancelButtonTitle : @"OK"
                                          otherButtonTitles : nil];
    [alert show];

*/


}

+ (void) showAlertWithTitle:(NSString*)title Message:(NSString *)message
{
    //Sal - Removing depreciated Alert View
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                          }];
    [alert addAction:defaultAction];
    
    UIWindow *alertWindow = [(AppDelegate *)[[UIApplication sharedApplication] delegate] window];
    [alertWindow.rootViewController presentViewController:alert animated:YES completion:nil];
}


+ (UIImage*) imageWithColor : (UIColor*) color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (void)updateNavigationBar:(UINavigationController *)navigationController {
    [navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    [navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationBar_background"] forBarMetrics:UIBarMetricsDefault];
    [navigationController.navigationBar setShadowImage:[UIImage new]];
    [navigationController.navigationBar setTranslucent:YES];
    [navigationController.navigationBar setTitleTextAttributes:@{
                                                                NSForegroundColorAttributeName:[UIColor whiteColor],
                                                                      NSFontAttributeName:[UIFont fontWithName:kBtFontBold size:20                      ]
                                                                      }];
    [navigationController.navigationBar setTintColor:[UIColor whiteColor]];
}

@end
