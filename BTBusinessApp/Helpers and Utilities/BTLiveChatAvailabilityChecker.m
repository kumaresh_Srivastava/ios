//
//  BTLiveChatAvailabilityChecker.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 5/11/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTLiveChatAvailabilityChecker.h"
#import "NLHelpAndSupportWebService.h"
#import "BTSpecificCategory.h"

@interface BTLiveChatAvailabilityChecker () <NLHelpAndSupportWebServiceDelegate>

@property (nonatomic, strong) NLHelpAndSupportWebService *helpAndSupportWebService;

@end

@implementation BTLiveChatAvailabilityChecker

- (void)getLiveChatAvailableSlots
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd-MM-yy"];
    NSDate *now = [[NSDate alloc] init];
    NSString *dateString = [format stringFromDate:now];
    
    self.helpAndSupportWebService = [[NLHelpAndSupportWebService alloc] initWithDateText:dateString];
    self.helpAndSupportWebService.helpAndSupportWebServiceDelegate = self;
    [self.helpAndSupportWebService resume];
}

- (void)cancel
{
    self.helpAndSupportWebService.helpAndSupportWebServiceDelegate = nil;
    [self.helpAndSupportWebService cancel];
    self.helpAndSupportWebService = nil;
}

+ (BOOL)isCurrentTimeInBetweenStartTime:(NSString *)startTime endTime:(NSString *)endTime withCurrentTime:(NSString *)currentTime {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    
    NSDate *sTime, *eTime,*cTime;
    sTime = [dateFormatter dateFromString:startTime];
    eTime = [dateFormatter dateFromString:endTime];
    cTime = [dateFormatter dateFromString:currentTime];
    
    if(([cTime compare:sTime] == NSOrderedDescending || [cTime compare:sTime] == NSOrderedSame) && ([cTime compare:eTime] ==  NSOrderedAscending || [cTime compare:eTime] == NSOrderedSame)) {
        
        return  YES;
    }
    
    return NO;
    
}

+ (BOOL)checkAvailabilityWithSpecificCategory:(NSArray *)specificCategory currentTime:(NSString *)currentTime andChatType:(BTChatType)chatType
{
    int holidayCount = 0;
    
    for (BTSpecificCategory *specificCategoryObj in specificCategory)
    {
        if(specificCategoryObj.markedAsHoliday)
        {
            holidayCount = holidayCount + 1;
        }
    }
    
    if(holidayCount == 3)
    {
        return false;
    }
    else if(holidayCount < 3)
    {
        for (BTSpecificCategory *specificCategoryObj in specificCategory)
        {
            if(!specificCategoryObj.markedAsHoliday)
            {
                switch (chatType)
                {
                    case BTChatTypeFault:
                        if([[specificCategoryObj.categoryName lowercaseString] isEqualToString:@"faults"])
                        {
                            if([BTLiveChatAvailabilityChecker isCurrentTimeInBetweenStartTime:specificCategoryObj.startTime endTime:specificCategoryObj.endTime withCurrentTime:currentTime])
                            {
                                return true;
                            }
                        }
                        break;
                        
                    case BTChatTypeOrder:
                        if([[specificCategoryObj.categoryName lowercaseString] isEqualToString:@"orders"])
                        {
                            if([BTLiveChatAvailabilityChecker isCurrentTimeInBetweenStartTime:specificCategoryObj.startTime endTime:specificCategoryObj.endTime withCurrentTime:currentTime])
                            {
                                return true;
                            }
                        }
                    case BTChatTypeBilling:
                        if([[specificCategoryObj.categoryName lowercaseString] isEqualToString:@"billing"])
                        {
                            if([BTLiveChatAvailabilityChecker isCurrentTimeInBetweenStartTime:specificCategoryObj.startTime endTime:specificCategoryObj.endTime withCurrentTime:currentTime])
                            {
                                return true;
                            }
                        }
                    default:
                        break;
                }
            }
        }
    }
    
    return false;
}

#pragma mark - NLHelpAndSupportWebServiceDelegate

- (void)getHelpAndSupportWebService:(NLHelpAndSupportWebService *)webService successfullyFetchedHelpAndSupportSpecificCategory:(NSArray *)specificCategory generalCategory:(NSArray *)generalCategory andCurrentTime:(NSString *)currentTime
{
    if(self.helpAndSupportWebService == webService) {
        
        if(specificCategory) {
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"categoryName" ascending:YES];
            self.specificCategoryArray = [specificCategory sortedArrayUsingDescriptors:@[sortDescriptor]];
        }
        
        if(generalCategory) {
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"categoryName" ascending:YES];
            self.generalCategoryArray = [generalCategory sortedArrayUsingDescriptors:@[sortDescriptor]];
            
        }
        
        if(currentTime)
            _currentTime = currentTime;
        
        [self.liveChatAvailabilityCheckerDelegate liveChatAvailabilityChecker:self successfullyFetchedSlotsWithGeneralCategory:self.generalCategoryArray specificeCategory:self.specificCategoryArray andCurrentTime:self.currentTime];
        self.helpAndSupportWebService.helpAndSupportWebServiceDelegate = nil;
        self.helpAndSupportWebService = nil;
    }
    else {
        
        DDLogError(@"This delegate method gets called because of success of an object of NLHelpAndSupportWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLHelpAndSupportWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
    
}

- (void)getHelpAndSupportWebService:(NLHelpAndSupportWebService *)webService failedToFetchHelpAndSupportWebServiceWuthError:(NLWebServiceError *)webServiceError {
    
    if(self.helpAndSupportWebService == webService){
        
        [self.liveChatAvailabilityCheckerDelegate liveChatAvailabilityChecker:self failedToFetchSlotsWithWebServiceError:webServiceError];
        self.helpAndSupportWebService.helpAndSupportWebServiceDelegate = nil;
        self.helpAndSupportWebService = nil;
    }
    else{
        
        DDLogError(@"This delegate method gets called because of failure of an object of NLHelpAndSupportWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLHelpAndSupportWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
    
    
    
}

@end
