//
//  BrandColors.h
//  My BT
//
//  Created by Sam McNeilly on 13/11/2015.
//  Copyright © 2015 BT. All rights reserved.
//

#import <UIColor-HNExtensions/UIColor+HNExtensions.h>

@interface BrandColours : NSObject

// New Brand refresh Oct 2019
+ (UIColor*)BTIndigo;
+ (UIColor*)BTPink;

// New (modified) methods for voilet and red color

+ (UIColor*)colourBackgroundBTPurplePrimaryColor;
+ (UIColor*)colourBackgroundBTPinkSuperColor;
+ (UIColor*)colourBackgroundBTBlueSecondaryColor;

+ (UIColor*)colourTextBTPurplePrimaryColor;
+ (UIColor*)colourTextBTPinkSuperColor;
+ (UIColor*)colourTextBlueSecondaryColor;



//Previous color's static method

+ (UIColor*)colourBtDarkViolet;
+ (UIColor*)colourBtYellow;

+ (UIColor*)colourMyBtDarkBlue;
+ (UIColor*)colourMyBtGreen;
+ (UIColor*)colourMyBtLightBlue;
+ (UIColor*)colourMyBtLightPink;
+ (UIColor*)colourMyBtPink;
+ (UIColor*)colourMyBtPinkDisabled;
+ (UIColor*)colourMyBtPinkHighlighted;
+ (UIColor*)colourMyBtLightRed;
+ (UIColor*)colourMyBtRed;
+ (UIColor*) colourBTBusinessRed;
+ (UIColor*) colourTrueGreen;
+ (UIColor*)colourTextBTPinkColor;

+ (UIColor*)colourMyBtVeryDarkGrey;
+ (UIColor*)colourMyBtDarkGrey;
+ (UIColor*)colourMyBtLightGrey;
+ (UIColor*)colourMyBtLightGreyAlt;
+ (UIColor*)colourMyBtMediumGrey;
+ (UIColor*)colourMyBtMediumGreyAlt;

+ (UIColor*)colourMyBtOrange;

+ (UIColor*)colourMyBtLightPurple;

+ (UIColor*)colourBtSuperColor;
+ (UIColor*)colourBtPrimaryColor;
+ (UIColor*)colourBtSecondaryColor;
+ (UIColor*)colourBtBackgroundColor;
+ (UIColor*)colourBtWhite;
+ (UIColor*)colourBtBlack;
+ (UIColor*)colourBtLightBlack;
+ (UIColor*)colourBtLightGray;
+ (UIColor*)colourBtNeutral90;
+ (UIColor*)colourBtNeutral80;
+ (UIColor*)colourBtNeutral70;
+ (UIColor*)colourBtNeutral60;
+ (UIColor*)colourBtNeutral50;
+ (UIColor*)colourBtNeutral40;
+ (UIColor*)colourBtNeutral30;
+ (UIColor*)colourBtSuccessColor;
+ (UIColor*)colourBtErrorColor;
+ (UIColor*)colourBtProgressColor;

+ (UIColor*)colourBtDisableInlineButton;

@end
