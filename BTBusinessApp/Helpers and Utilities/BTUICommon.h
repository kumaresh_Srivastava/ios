//
//  OpenreachUICommon.h
//  A55
//
//  Created by Sam McNeilly on 17/09/2014.
//  Copyright (c) 2014 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
//
//#import "DataModels.h"

@interface BTUICommon : UIViewController

+ (void)showAlertWithShortMessage:(NSString*)message;
+ (NSArray*)framesForAnimationWithName:(NSString*)name andLength:(NSInteger)numFrames;
+ (UIImage*)screenshotForCurrentView;
+ (UIImage*)applyGaussianBlurToImage:(UIImage*)image withRadius:(float)radius;
+ (UIImage*)captureScreenshotForView:(UIView*)view;
+ (void)showAlertWithTitle:(NSString*)title Message:(NSString*)message;
+ (UIImage*)imageWithColor:(UIColor*)color;
+ (void)updateNavigationBar:(UINavigationController *)navigationController;

@end
