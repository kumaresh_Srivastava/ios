//
//  BTLiveChatAvailabilityChecker.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 5/11/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BTLiveChatAvailabilityChecker;
@class NLWebServiceError;

typedef NS_ENUM(NSInteger, BTChatType) {
    
    BTChatTypeOrder,
    BTChatTypeFault,
    BTChatTypeBilling
};

@protocol BTLiveChatAvailabilityCheckerDelegate <NSObject>

- (void)liveChatAvailabilityChecker:(BTLiveChatAvailabilityChecker *)liveChatAvailabilityChecker successfullyFetchedSlotsWithGeneralCategory:(NSArray *)generalCategoryArray specificeCategory:(NSArray *)specificCategory andCurrentTime:(NSString *)currentTime;
- (void)liveChatAvailabilityChecker:(BTLiveChatAvailabilityChecker *)liveChatAvailabilityChecker failedToFetchSlotsWithWebServiceError:(NLWebServiceError *)webServiceError;

@end

@interface BTLiveChatAvailabilityChecker : NSObject

@property (nonatomic, weak) id <BTLiveChatAvailabilityCheckerDelegate> liveChatAvailabilityCheckerDelegate;

@property (nonatomic, strong)NSArray *generalCategoryArray;
@property (nonatomic, strong)NSArray *specificCategoryArray;
@property (nonatomic, strong)NSString *currentTime;

- (void)getLiveChatAvailableSlots;
+ (BOOL)checkAvailabilityWithSpecificCategory:(NSArray *)specificCategory currentTime:(NSString *)currentTime andChatType:(BTChatType)chatType;
+ (BOOL)isCurrentTimeInBetweenStartTime:(NSString *)startTime endTime:(NSString *)endTime withCurrentTime:(NSString *)currentTime;
- (void)cancel;

@end
