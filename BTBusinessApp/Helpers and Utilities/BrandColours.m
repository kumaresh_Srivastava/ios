//
//  BrandColors.m
//  My BT
//
//  Created by Sam McNeilly on 13/11/2015.
//  Copyright © 2015 BT. All rights reserved.
//

#import "BrandColours.h"

@implementation BrandColours

#pragma mark - New color methods

+ (UIColor *)BTIndigo
{
    return [UIColor colorForHexString:@"5514B4"];
}

+ (UIColor *)BTPink
{
    return [UIColor colorForHexString:@"FF6EFF"];
}

//Background color methods
+ (UIColor*)colourBackgroundBTPurplePrimaryColor
{
    return [UIColor colorForHexString:@"#6400AA"];
}

+ (UIColor*)colourBackgroundBTPinkSuperColor
{
    return [UIColor colorForHexString:@"#E60050"];
}

+ (UIColor*)colourBackgroundBTBlueSecondaryColor
{
    return [UIColor colorForHexString:@"#00AADC"];
}


//Text color methods
+ (UIColor*)colourTextBTPurplePrimaryColor
{
    return [UIColor colorForHexString:@"#55379B"];
}

+ (UIColor*)colourTextBTPinkSuperColor
{
    return [UIColor colorForHexString:@"#FF379B"];
}

+ (UIColor*)colourTextBTPinkColor
{
    return [UIColor colorForHexString:@"#e62950"];
}

//? where this will be used
+ (UIColor*)colourTextBlueSecondaryColor
{
    return [UIColor colorForHexString:@"#0295D4"];
}


#pragma mark - Old color methods

//What is the dark violet because not there in the style guide
+ (UIColor*) colourBtDarkViolet {
    return [UIColor colorForHexString:@"#321E5B"];
}

//As per design guide
+ (UIColor*) colourMyBtGreen {
    return [UIColor colorForHexString:@"#30AD63"];
}

+ (UIColor*) colourTrueGreen {
    return [UIColor colorForHexString:@"#008A00"];
}

//Used but Not mentioned in design guide
+ (UIColor*) colourMyBtLightRed {
    return [UIColor colorForHexString:@"#EC5840"];
}

//As per design guide
+ (UIColor*) colour4GAssure {
    return [UIColor colorForHexString:@"#6400aa"];
}

//As per design guide
+ (UIColor*) colourMyBtRed {
    return [UIColor colorForHexString:@"#C62332"];
}

//As per design guide
+ (UIColor*) colourBTBusinessRed {
    return [UIColor colorForHexString:@"#E60014"];
}

//In design guide but not used in code
+ (UIColor*) colourMyBtVeryDarkGrey {
    return [UIColor colorForHexString:@"#222222"];
}

//Used in code but not there in design guide but came from other design
+ (UIColor*) colourMyBtMediumGrey {
    return [UIColor colorForHexString:@"#767676"];
}

//As per design guide
+ (UIColor*) colourMyBtOrange {
    return [UIColor colorForHexString:@"#EA7F00"];
}

//Old color before branding
+ (UIColor*)colourBtSuperColor {
    return [UIColor colorForHexString:@"#FF379B"];
}

//(VRK) This has been used for shadows
+ (UIColor*)colourBtPrimaryColor {
    return [UIColor colorForHexString:@"#55379B"];
}

//Old color before branding
+ (UIColor*)colourBtSecondaryColor {
    return [UIColor colorForHexString:@"#0295D4"];
}

//As per design guide
+ (UIColor*)colourBtBackgroundColor {
    return [UIColor colorForHexString:@"#F1F0F6"];
}

//As per design guide
+ (UIColor*)colourBtWhite {
    return [UIColor colorForHexString:@"#FFFFFF"];
}

//as per design guide
+ (UIColor*)colourBtBlack {
    return [UIColor colorForHexString:@"#000000"];
}

+ (UIColor*)colourBtLightBlack {
    return [UIColor colorForHexString:@"#333333"];
}

+ (UIColor*)colourBtLightGray {
    return [UIColor colorForHexString:@"#EEEEEE"];
}

//as per design guide
+ (UIColor*)colourBtNeutral90 {
    return [UIColor colorForHexString:@"#222222"];
}

//as per design guide
+ (UIColor*)colourBtNeutral80 {
    return [UIColor colorForHexString:@"#666666"];
}

//as per design guide
+ (UIColor*)colourBtNeutral70 {
    return [UIColor colorForHexString:@"#888888"];
}

//as per design guide
+ (UIColor*)colourBtNeutral60 {
    return [UIColor colorForHexString:@"#C2C2C2"];
}

//as per design guide
+ (UIColor*)colourBtNeutral50 {
    return [UIColor colorForHexString:@"#E1E1E1"];
}

//as per design guide
+ (UIColor*)colourBtNeutral40 {
    return [UIColor colorForHexString:@"#EBEBEB"];
}

//as per design guide
+ (UIColor*)colourBtNeutral30 {
    return [UIColor colorForHexString:@"#F3F3F3"];
}

//In design guide but not used
+ (UIColor*)colourBtSuccessColor {
    return [UIColor colorForHexString:@"#30AD63"];
}

//as per design guide
+ (UIColor*)colourBtErrorColor {
    return [UIColor colorForHexString:@"#C62332"];
}

//In design guide but not used
+ (UIColor*)colourBtProgressColor {
    return [UIColor colorForHexString:@"#EA7F00"];
}

//Used but not is design guide
+ (UIColor*)colourBtDisableInlineButton {
    return [UIColor colorForHexString:@"#CCCCCC"];
}

#pragma mark Unused Methods

//(VRK)Not used
+ (UIColor*) colourBtYellow {
    return [UIColor colorForHexString:@"#FFCC00"];
}

//(VRK)Not used
+ (UIColor*) colourMyBtDarkBlue {
    return [UIColor colorForHexString:@"#144A95"];
}

//Not used & not mentioned in the guides
+ (UIColor*) colourMyBtLightBlue {
    return [UIColor colorForHexString:@"#027DB3"];
}

//Not used & not mentioned in the guides
+ (UIColor*) colourMyBtLightPink {
    return [UIColor colorForHexString:@"#CC2E79"];
}

//Not used & not mentioned in the guides
+ (UIColor*) colourMyBtPink {
    return [UIColor colorForHexString:@"#D43480"];
}

//Not used & not mentioned in the guides
+ (UIColor*) colourMyBtPinkDisabled {
    return [UIColor colorForHexString:@"#F2CDDE"];
}

//Not used & not mentioned in the guides
+ (UIColor*) colourMyBtPinkHighlighted {
    return [UIColor colorForHexString:@"#D43480"];
}

//Not used & not mentioned in the guides
+ (UIColor*) colourMyBtDarkGrey {
    return [UIColor colorForHexString:@"#333333"];
}

//Not used & not mentioned in the guides
+ (UIColor*) colourMyBtLightGrey {
    return [UIColor colorForHexString:@"#DDDDDD"];
}

//Not used & not mentioned in the guides
+ (UIColor*) colourMyBtLightGreyAlt {
    return [UIColor colorForHexString:@"#F6F6F6"];
}

//Mentioned in design guide but not used because another method uses the same color code
+ (UIColor*) colourMyBtMediumGreyAlt {
    return [UIColor colorForHexString:@"#888888"];
}

//Not used & not there in design guide
+ (UIColor*) colourMyBtLightPurple {
    return [UIColor colorForHexString:@"#9264CD"];
}

@end
