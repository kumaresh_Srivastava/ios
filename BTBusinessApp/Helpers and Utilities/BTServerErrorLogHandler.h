//
//  NLServerErrorLogger.h
//  BTBusinessApp
//
//  Created by Vikas Mishra on 12/05/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#define BTServerErrorLogger(url, message) [BTServerErrorLogHandler logAPIErrorOnServerWithURL:(url) withMessage:(message)];

#import <Foundation/Foundation.h>

@interface BTServerErrorLogHandler : NSObject

+(void)logAPIErrorOnServerWithURL:(NSString *)url withMessage: (NSString *)message;

@end
