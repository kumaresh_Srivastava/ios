//
//  BTUtils.m
//  BT Graduates
//
//  Created by Sam McNeilly on 24/07/2015.
//  Copyright © 2015 BT. All rights reserved.
//

#import "BTUtils.h"
#import <UIKit/UIKit.h>
#import "NSObject+APIResponseCheck.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "OmnitureManager.h"

@implementation BTUtils

+ (BOOL) validateString : (NSString*) string withLength : (NSInteger) length {
    if ([string length] >= length) {
        return true;
    }
    
    else {
        return false;
    }
}

+ (NSString*) stringByTrimmingWhitespaceInString : (NSString*) string {
    return [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+ (BOOL) emailAddressIsValid : (NSString*) emailAddress {
    // Note: This regular expression was nabbed from:
    // http://www.cocoawithlove.com/2009/06/verifying-that-string-is-email-address.html
    // It should comply with the RFC.
    
    NSString* emailRegEx =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    
    NSPredicate* regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    return [regExPredicate evaluateWithObject:emailAddress];
}

+ (BOOL)isPad
{
    UIDevice* thisDevice = [UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        return true;
    }
    else
    {
        return false;
    }
}

+ (NSString *)NSDateWithMonthNameFromNSString : (NSString*)string {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:[string substringWithRange:NSMakeRange(0, 10)]];
    if (![BTUtils isValidDate:date]) {
        return nil;
    }
    
    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    NSString * updatedString = [dateFormatter stringFromDate:date];
    return updatedString;
}

+ (NSString *)NSStringWithOmnitureFormatFromNSDate : (NSDate *)date {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSString * timestamp = [dateFormatter stringFromDate:date];
    return timestamp;
}

#pragma mark - Date format related helper methods

+ (NSDate *)NSDateWithMonthNameFormatFromNSString:(NSString *)dateString
{
    NSString *dateFormat = @"dd MMM yyyy";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSDate *date = [dateFormatter dateFromString:dateString];
    
    return date;
}

+ (NSDate *)NSDateWithMonthNameWithSpaceFormatFromNSString:(NSString *)dateString
{
    NSString *dateFormat = @"dd MMM yyyy";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSDate *date = nil;
    
    if ([dateFormatter dateFromString:dateString] != nil) {
        
        date = [dateFormatter dateFromString:dateString];
        return date;
    }
    
    return date;
}

+ (NSString *)NSStringFromNSDateWithMonthNameWithSpaceeFormat:(NSDate *)date
{
    NSString *dateFormat = @"dd MMM yyyy";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSString *dateString = nil;
    
    if ([dateFormatter stringFromDate:date] != nil) {
        
        dateString = [dateFormatter stringFromDate:date];
    }
    
    return dateString;
}

+ (NSString *)NSStringFromNSDateWithSlashFormat:(NSDate *)date
{
    NSString *dateFormat = @"dd/MM/yyyy";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSString *dateString = nil;
    
    if ([dateFormatter stringFromDate:date] != nil) {
        
        dateString = [dateFormatter stringFromDate:date];
    }
    
    return dateString;
}

+ (NSString *)NSStringFromNSDateWithMonthNameWithHiphenFormat:(NSDate *)date
{
    NSString *dateFormat = @"dd-MMM-yyyy";

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];

    NSString *dateString = nil;

    if ([dateFormatter stringFromDate:date] != nil) {

        dateString = [dateFormatter stringFromDate:date];
    }

    return dateString;
}


+ (NSString *)NSStringFromNSDateWithReverseDateFormat:(NSDate *)date
{
    NSString *dateFormat = @"yyyy-MM-dd";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSString *dateString = nil;
    
    if ([dateFormatter stringFromDate:date] != nil) {
        
        dateString = [dateFormatter stringFromDate:date];
    }
    
    return dateString;
}

+ (NSString *)getDayDateMonthFormattedStringFromDate:(NSDate *)date {
    NSString *dateFormat = @"EEE dd MMM";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSString *dateString = nil;
    
    if ([dateFormatter stringFromDate:date] != nil) {
        
        dateString = [dateFormatter stringFromDate:date];
    }
    
    return dateString;
}

+ (NSDate *)NSDateWithUTCFormatFromNSString:(NSString *)dateString{
    NSString *dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSDate *date = nil;
    
    if ([dateFormatter dateFromString:dateString] != nil) {
        
        date = [dateFormatter dateFromString:dateString];
        return date;
    }
    
    return date;
}

+ (NSString *)getTimeFromDate:(NSDate *)date
{
    NSString *dateFormat = @"HH:mm";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSString *dateString = nil;
    
    if ([dateFormatter stringFromDate:date] != nil) {
        
        dateString = [dateFormatter stringFromDate:date];
    }
    
    return dateString;
}

+ (NSString *)getDayMonthFromDate:(NSDate *)date
{
    NSString *dateFormat = @"dd MMM";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSString *dateString = nil;
    
    if ([dateFormatter stringFromDate:date] != nil) {
        
        dateString = [dateFormatter stringFromDate:date];
    }
    
    return dateString;
}

+(NSInteger)getHourFromDate:(NSDate *)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
    return [components hour];
}

+(NSInteger)getMinuteFromDate:(NSDate *)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
    return [components minute];
}

+(BOOL)isValidDate:(NSDate *)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    if ([components year]== 0001) {
        return false;
    }
    return true;
}

+ (BOOL)isDate:(NSDate *)firstDate isEqualTo:(NSDate *)secondDate
{
    unsigned int flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSDateComponents* components = [calendar components:flags fromDate:firstDate];
    
    NSDate* dateOnly = [calendar dateFromComponents:components];
    
    if ([dateOnly compare:secondDate] == NSOrderedSame) {
        return true;
    }
    
    return false;
}

+(NSString *)getHourFromDateString:(NSString *)dateString
{
    return [dateString substringWithRange:NSMakeRange(11, 2)];
}

+(NSString *)getMinuteFromDateString:(NSString *)dateString
{
    return [dateString substringWithRange:NSMakeRange(14, 2)];
}

//Salman
+ (NSString *)documentDirectoryPath{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}


+ (NSString *)getContactNumberFromSiteContact:(BTOrderSiteContactModel *)siteContact{
    
    
    //mobile ->work(with -extension) -> home
    if(!siteContact)
        return nil;
    
    NSString *contatNumber = @"";
    NSString *homePhone = siteContact.homePhone;
    NSString *workPhone = siteContact.workPhone;
    NSString *mobile = siteContact.mobile;
    NSString *phone = siteContact.phone;
    NSString *numberExtention = siteContact.extension;;
    
    if(siteContact.preferredContact && [siteContact.preferredContact validAndNotEmptyStringObject]){
        
        if([[siteContact.preferredContact lowercaseString]  isEqualToString:@"phone"]){
            
            contatNumber = phone;
        }
        else if([[siteContact.preferredContact lowercaseString]  isEqualToString:@"mobile"]){
            
            contatNumber = mobile;
        }
        else if([[siteContact.preferredContact lowercaseString]  isEqualToString:@"work"]){
            
            contatNumber = workPhone;
            
            if([numberExtention validAndNotEmptyStringObject]){
                
                contatNumber = [workPhone stringByAppendingString:[NSString stringWithFormat:@"-%@", numberExtention]];
            }

        }
        else if([[siteContact.preferredContact lowercaseString]  isEqualToString:@"home"]){
            
            contatNumber = homePhone;
        }
        
    }
    else{
        
        if([phone validAndNotEmptyStringObject]){
            
            contatNumber = phone;
        }
        else if([mobile validAndNotEmptyStringObject]){
            
            contatNumber = mobile;
        }
        else if([workPhone validAndNotEmptyStringObject]){
            
            contatNumber = workPhone;
            
            if([numberExtention validAndNotEmptyStringObject]){
                
                contatNumber = [workPhone stringByAppendingString:[NSString stringWithFormat:@"-%@", numberExtention]];
            }
            
        }
        else if([homePhone validAndNotEmptyStringObject]){
            
            contatNumber = homePhone;
        }

    }
    
    
    return contatNumber;
}

+ (BOOL)isInternetConnectionAvailable {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable) {
        return NO;
    } else {
        return YES;
    }
}

+ (NSArray *)getBTUserTitles {
    
    NSArray *titlesArray;
    
    titlesArray = [AppDelegate sharedInstance].viewModel.arrayOfTitles;
    
    if ((titlesArray != nil) && ([titlesArray count] > 0)) {
        
        return titlesArray;
    }
    else
    {
        NSError *error;
        NSPropertyListFormat format;
        NSString *plistPath;
        NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                  NSUserDomainMask, YES) objectAtIndex:0];
        plistPath = [rootPath stringByAppendingPathComponent:@"Data.plist"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
            plistPath = [[NSBundle mainBundle] pathForResource:@"Data" ofType:@"plist"];
        }
        
        NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
        
        NSDictionary *temp = (NSDictionary *)[NSPropertyListSerialization
                                              propertyListWithData:plistXML options:NSPropertyListMutableContainersAndLeaves format:&format error:&error];
        
        
        if (!temp) {
            NSLog(@"Error reading plist: %@, format: %lu", error.description, (unsigned long)format);
        }
        
        titlesArray = [NSArray arrayWithArray:[temp objectForKey:@"Titles"]];
        
        return titlesArray;
    }
    
}

+ (NSArray *)getBTUserSecurityQuestions {
    
    NSArray *securityQuestionsArray;
    
    securityQuestionsArray = [AppDelegate sharedInstance].viewModel.arrayOfSecurityQuestions;
    
    if ((securityQuestionsArray != nil) && (securityQuestionsArray > 0)) {
        
        return securityQuestionsArray;
    }
    else
    {
        NSError *error;
        NSPropertyListFormat format;
        NSString *plistPath;
        NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                  NSUserDomainMask, YES) objectAtIndex:0];
        plistPath = [rootPath stringByAppendingPathComponent:@"Data.plist"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
            plistPath = [[NSBundle mainBundle] pathForResource:@"Data" ofType:@"plist"];
        }
        
        NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
        
        NSDictionary *temp = (NSDictionary *)[NSPropertyListSerialization
                                              propertyListWithData:plistXML options:NSPropertyListMutableContainersAndLeaves format:&format error:&error];
        
        
        if (!temp) {
            NSLog(@"Error reading plist: %@, format: %lu", error.description, (unsigned long)format);
        }
        
        securityQuestionsArray = [NSArray arrayWithArray:[temp objectForKey:@"SecurityQuestions"]];
        
        return securityQuestionsArray;
    }
    
}


+ (void)trackOmnitureKeyTask:(NSString *)keyTask forPageName:(NSString *)pageName
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *timestamp = [BTUtils NSStringWithOmnitureFormatFromNSDate:[NSDate date]];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    [data setValue:timestamp forKey:kOmniTimeStamp];
    [data setValue:keyTask forKey:kOmniKeyTask];
    [OmnitureManager trackPage:pageName withContextInfo:data];
}


+ (NSDictionary *)getOmniDictionary
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *timestamp = [BTUtils NSStringWithOmnitureFormatFromNSDate:[NSDate date]];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    [data setValue:timestamp forKey:kOmniTimeStamp];
  
    
    return data;

}

@end
