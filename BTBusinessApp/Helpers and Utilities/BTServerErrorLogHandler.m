//
//  NLServerErrorLogger.m
//  BTBusinessApp
//
//  Created by Vikas Mishra on 12/05/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTServerErrorLogHandler.h"
#import "AppDelegate.h"
#import "CDApp.h"
#import "AppDelegateViewModel.h"
#import "CDUser+CoreDataProperties.h"
#import "NLServerLogWebService.h"
#import "LHServerLogItem.h"

@implementation BTServerErrorLogHandler

+(void)logAPIErrorOnServerWithURL:(NSString *)url withMessage: (NSString *)message
{
    /*
     Fix for #Crash Live Build 2.0.2
     SIGABRT: *** -[__NSPlaceholderDictionary initWithObjects:forKeys:count:]: attempt to insert nil object from objects[0]
     */
    NSString* timeInterval = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSinceReferenceDate]];
                              
    if((url != nil) && ([AppDelegate sharedInstance].viewModel.app.btInstallationID != nil) && ([AppDelegate sharedInstance].viewModel.app.loggedInUser.username != nil) && (timeInterval != nil) && (message != nil)) {
            NSDictionary *errorPayload = @{
                                           @"comment" : url,
                                           @"timeInterval" : timeInterval,
                                           @"BTInstallationID" : [AppDelegate sharedInstance].viewModel.app.btInstallationID,
                                           @"logMessage" : message,
                                           @"loggedInUser" : [NSString stringWithFormat:@"%@", [AppDelegate sharedInstance].viewModel.app.loggedInUser.username]
                                           };

            LHServerLogItem *logItem = [[LHServerLogItem alloc] initWithJSONDictionary:errorPayload];
        
            NSMutableArray *arrayLog = [[NSMutableArray alloc] init]; // [NSArray arrayWithObject:logItem];
            [arrayLog addObject:logItem];
            NLServerLogWebService *loggerWebService = [[NLServerLogWebService alloc] initWithArrayOfServerLogItems:arrayLog];
            [loggerWebService resume];
        
        
    }
    
}

@end
