//
//  BTOCSSimpleDisclosureTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSSimpleDisclosureTableViewCell.h"

@implementation BTOCSSimpleDisclosureTableViewCell

+ (NSString *)reuseId
{
    return @"BTOCSSimpleDisclosureTableViewCell";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
