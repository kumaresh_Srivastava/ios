//
//  NLSubmitProjectEnquiry.h
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLAPIGEEAuthenticatedWebService.h"

@class NLSubmitProjectEnquiry;
@class NLWebServiceError;

@protocol NLSubmitProjectEnquiryDelegate <NLAPIGEEAuthenticationProtectedWebServiceDelegate>

- (void)submitProjectEnquiryWebService:(NLSubmitProjectEnquiry *)webService finishedWithResponse:(NSObject *)response;

- (void)submitProjectEnquiryWebService:(NLSubmitProjectEnquiry *)webService failedWithWebServiceError:(NLWebServiceError *)error;

@optional
- (void)timedOutWithoutSuccessForSubmitProjectEnquiryWebService:(NLSubmitProjectEnquiry*)webService;

@end

@interface NLSubmitProjectEnquiry : NLAPIGEEAuthenticatedWebService

@property (nonatomic) NSDictionary* projectDetails;
@property (nonatomic) NSString* category;
@property (nonatomic) NSString* enquiryDescription;
@property (nonatomic) int siteID;
@property (nonatomic) NSString *existingEnquiryID;

@property (nonatomic, weak) id <NLSubmitProjectEnquiryDelegate> submitProjectEnquiryDelegate;

- (instancetype)initWithProjectRef:(NSString*)projectRef category:(NSString*)category enquiryDescription:(NSString*)enquiryDescription siteID:(NSInteger)siteID andExistingEnquiryID:(NSInteger)existingEnquiryID;

- (instancetype)initWithProjectDetails:(NSDictionary*)projectDetails category:(NSString*)category enquiryDescription:(NSString*)enquiryDescription siteID:(NSInteger)siteID andExistingEnquiryID:(NSInteger)existingEnquiryID;

@end
