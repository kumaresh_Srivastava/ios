//
//  EENetworkStatusTableViewCell.h
//  BTBusinessApp
//

#import <UIKit/UIKit.h>

@interface EENetworkStatusTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *issueDescription;
@end
