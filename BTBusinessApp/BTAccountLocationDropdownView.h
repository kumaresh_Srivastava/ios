//
//  BTAccountLocationDropdownView.h
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTAccountLocationDropdownView;

@protocol BTAccountLocationDropdownViewDelegate <NSObject>

- (void)dropdown:(BTAccountLocationDropdownView*)dropdown updatedSelection:(NSString*)selected;

@end

@interface BTAccountLocationDropdownView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *dropdownOutlineView;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet UIImageView *downArrow;

@property (strong, nonatomic) NSArray<NSString*> *values;
@property (strong, nonatomic) NSString *selectedValue;

@property (nonatomic) BOOL isOptionsShowing;

@property (nonatomic) id<BTAccountLocationDropdownViewDelegate> delegate;

- (void)updateWithTitle:(NSString*)title values:(NSArray<NSString*>*)values selectedValue:(NSString*)selected;
- (void)updateWithTitle:(NSString*)title values:(NSArray<NSString*>*)values selectedValue:(NSString*)selected andPlaceHolder:(NSString*)placeholder;

- (void)showOptions;

@end
