//
//  NLQueryClientProfileDetails.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 05/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLQueryClientProfileDetails.h"
#import "BTAuthenticationManager.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "BTUser.h"
#import "AppConstants.h"
#import "NLVordelAuthenticationProtectedWebService+MbaasHeader.h"
#import "NLWebServiceError.h"
#import "NLWebService+BackEndAPIErrorHandling.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "AppManager.h"
#import "BTClientProfileDetails.h"
#import "BTClientServiceInstance.h"

@interface NLQueryClientProfileDetails () {
    NSString *_accessToken;
}

@end

@implementation NLQueryClientProfileDetails

- (instancetype)initWithServiceCode:(NSString *)serviceCode
{
    if (serviceCode == nil || [serviceCode isEqualToString:@""])
    {
        return nil;
    }
    
    NSDictionary *header = [self getMbaasHeader];
    
    NSDictionary *serviceCodeDict = [NSDictionary dictionaryWithObject:serviceCode forKey:@"serviceCode"];
    NSDictionary *body = [NSDictionary dictionaryWithObject:serviceCodeDict forKey:@"queryClientProfileDetailsV1"];
    
    NSDictionary *payload = [NSDictionary dictionaryWithObjectsAndKeys:header, @"MobileServiceHeader", body, @"Body", nil];
    
    //NSDictionary *parentPayload = [NSDictionary dictionaryWithObject:payload forKey: @"MobileServiceRequest"];
    
    NSLog(@"CLIENT PROFILE DETAILS PAYLOAD IS:\n %@", payload);
    
    NSError *errorWhileCreatingJSONData = nil;
    NSData *mbaasHeader = [NSJSONSerialization dataWithJSONObject:payload options:0 error:&errorWhileCreatingJSONData];
    
    if(errorWhileCreatingJSONData)
    {
        DDLogError(@"Error while creating JSON data for queryClientProfileDetails");
    }
    
    NSString *jsonString = [[NSString alloc] initWithData:mbaasHeader encoding:NSUTF8StringEncoding];
    
    NSString *endPointURLString = @"/clientprofiledetails/queryclientprofiledetails";
    
    self = [super initWithMethod:@"POST" parameters:jsonString andEndpointUrlString:endPointURLString];
    
    
    if (self)
    {
        _serviceCode = [serviceCode copy];
    }
    return self;
}

- (NSString *)friendlyName
{
    return @"QueryClientProfileDetails";
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *resultDic = [[responseObject valueForKey:@"Body"] valueForKey:@"queryClientProfileDetailsResponseV1"];
    
    [self.queryClientProfileDetailsDelegate queryClientProfileDetailsWebService:self successfullyFetchedClientProfileDetails:[BTClientProfileDetails clientDetailsFromAPIResponse:resultDic]];    
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    [self.queryClientProfileDetailsDelegate queryClientProfileDetailsWebService:self failedToFetchClientProfileDetailsWithWebServiceError:webServiceError];
}

#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        
        if([responseObject isValidVordelAPIResponseObject])
        {
            NSDictionary *body = [responseObject valueForKey:@"Body"];
            if([body valueForKey:@"queryClientProfileDetailsResponseV1"])
            {
                NSDictionary *clientProfileDetailsResponse = [body valueForKey:@"queryClientProfileDetailsResponseV1"];
                if (!([clientProfileDetailsResponse objectForKey:@"identifierValue"] && [clientProfileDetailsResponse objectForKey:@"listOfClientServiceInstance"]))
                {
                    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
                    errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
                    DDLogError(@"'queryClientProfileDetailsResponseV1' missing required child elements in Body of response object for URL %@", [[task currentRequest] URL]);
                }
            }
            else
            {
                NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
                errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
                DDLogError(@"'queryClientProfileDetailsResponseV1' element is missing in Body of response object for URL %@", [[task currentRequest] URL]);
            }
        }
        else
        {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
            
            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
            DDLogError(@"Invalid Response Object for URL %@", [[task currentRequest] URL]);
        }
    }
    
    return errorToBeReturned;
}

@end
