
#import "LogOutViewController.h"
#import "OmnitureManager.h"

@interface LogOutViewController ()
@property (weak, nonatomic) IBOutlet UILabel *logOutScreenMessage;
@property (weak, nonatomic) IBOutlet UIButton *swichUserdButton;
@property (weak, nonatomic) IBOutlet UIButton *logInButton;

- (void)trackLoginActionForOmniture;
- (void)trackSwichUserActionForOmniture;
- (NSMutableDictionary *)getDirWithLoginStatus;

@end

@implementation LogOutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [self trackPageForOmniture];
    
    _logOutScreenMessage.text = [NSString stringWithFormat:@"Thanks for using BT Business, %@.\n To log in again, tap the button below.", _loggedInUser.firstName];
    
    [self customizeButtonsAppearance];
    [self trackPageForOmniture];
}

- (void) customizeButtonsAppearance {
    [self roundButtonCorners:_logInButton];
    
    _swichUserdButton.layer.borderWidth = 1.0;
     [self roundButtonCorners:_swichUserdButton];
    _swichUserdButton.layer.borderColor = [BrandColours colourTextBTPurplePrimaryColor].CGColor;
}

-(void)roundButtonCorners:(UIButton*) button
{
    button.layer.cornerRadius = 5.0f;
}

- (IBAction)logInAction:(id)sender {
    [self trackLoginActionForOmniture];
    
    [self.delegate userChoseToLogIn:self];
}

- (IBAction)switchUsersAction:(id)sender {
    [self trackSwichUserActionForOmniture];
    
    [self.delegate userChoseToSwichUsers:self];
}

#pragma mark - omniture methods
- (void)trackPageForOmniture
{
    [OmnitureManager trackPage:OMNIPAGE_LOGGED_OUT withContextInfo:[self getDirWithLoginStatus]];
}

- (void)trackLoginActionForOmniture {
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",OMNIPAGE_LOGGED_OUT, OMNICLICK_LOG_IN] withContextInfo:[self getDirWithLoginStatus]];
}

- (void)trackSwichUserActionForOmniture {
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",OMNIPAGE_LOGGED_OUT, OMNICLICK_SWICH_USER] withContextInfo:[self getDirWithLoginStatus]];
}

- (NSMutableDictionary *)getDirWithLoginStatus {
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Not Logged In" forKey:kOmniLoginStatus];
    return data;
}

@end
