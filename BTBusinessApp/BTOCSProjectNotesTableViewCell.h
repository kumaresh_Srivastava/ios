//
//  BTOCSProjectNotesTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

@protocol BTOCSProjectNotesTableViewCellDelegate <NSObject>

- (void)layoutChangeInCell:(BTBaseTableViewCell*)cell;

@end

@interface BTOCSProjectNotesTableViewCell : BTBaseTableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UIButton *showMoreButton;
@property (strong, nonatomic) IBOutlet UILabel *detailLabel;

@property (nonatomic) id<BTOCSProjectNotesTableViewCellDelegate> delegate;

@end
