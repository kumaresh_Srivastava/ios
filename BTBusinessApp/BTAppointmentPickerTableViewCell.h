//
//  BTAppointmentPickerTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 29/11/2017.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTAppointmentPickerTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *timeslotLabel;
@property (strong, nonatomic) IBOutlet UIImageView *selectedIndicator;

@end
