//
//  BT4GAssureTableViewCell.h
//  BTBusinessApp
//
//  Created by Jim Purvis on 06/03/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BT4GAssureTableViewCell;

@protocol BT4GAssureTableViewCellDelegate <NSObject>

- (void)userPressedCellFor4GAssure:(BT4GAssureTableViewCell*)BT4GAssureCell;

@end

@interface BT4GAssureTableViewCell : UITableViewCell {
    
}
@property (weak, nonatomic) IBOutlet UIButton *orderNowButton;
- (IBAction)orderNowPressed:(id)sender;

@property (nonatomic, weak) id <BT4GAssureTableViewCellDelegate> BT4GAssureTableViewCellDelegate;

@end
