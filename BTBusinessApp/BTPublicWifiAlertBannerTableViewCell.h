//
//  BTPublicWifiAlertBannerTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 18/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseTableViewCell.h"

@interface BTPublicWifiAlertBannerTableViewCell : BTBaseTableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *alertBannerImageView;
@property (strong, nonatomic) IBOutlet UILabel *alertBannerLabel;

@end
