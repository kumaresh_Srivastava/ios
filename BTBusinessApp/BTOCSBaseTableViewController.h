//
//  BTOCSBaseTableViewController.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 13/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppConstants.h"
#import "AppManager.h"
#import "BrandColours.h"
#import "OmnitureManager.h"

#import "CustomSpinnerView.h"
#import "BTRetryView.h"
#import "BTEmptyDashboardView.h"


#import "UIViewController+WebServiceErrorHandling.h"

#import "BTOCSDropDownTableViewCell.h"
#import "BTOCSProjectNotesTableViewCell.h"
#import "BTOCSSimpleButtonTableViewCell.h"
#import "BTOCSDetailsHeaderTableViewCell.h"
#import "BTOCSProjectDetailsTableViewCell.h"
#import "BTOCSSimpleDisclosureTableViewCell.h"
#import "BTOCSHorizontalSelectorTableViewCell.h"
#import "BTOCSRerquiredQuestionsTableViewCell.h"
#import "BTOCSProjectEnquiryTableViewCell.h"
#import "BTOCSCompletionDateTableViewCell.h"
#import "BTOCSProjectInfoHeaderTableViewCell.h"
#import "BTOCSProjectNotesTableViewCell.h"
#import "BTOCSSimpleInputTableViewCell.h"
#import "BTOCSLargeTextEntryTableViewCell.h"
#import "BTOCSSectionHeaderTableViewCell.h"
#import "BTOCSSummaryMilestoneTableViewCell.h"
#import "BTOCSRequiredQuestionsPromptTableViewCell.h"
#import "BTOCSRadioSelectTableViewCell.h"
#import "BTOCSDateSelectTableViewCell.h"
#import "BTOCSNoQueriesTableViewCell.h"
#import "BTOCSDateTimeSelectTableViewCell.h"
#import "BTOCSFileSelectTableViewCell.h"

#import "BTBaseTableViewCell.h"

#import "BTOCSProject.h"
#import "BTOCSSite.h"
#import "BTOCSMilestone.h"
#import "BTOCSContactDetail.h"
#import "BTOCSNote.h"
#import "BTOCSEnquiry.h"


@interface BTOCSBaseTableViewController : UITableViewController <UITableViewDelegate,UITableViewDataSource,BTRetryViewDelegate>

@property (nonatomic, assign) BOOL networkRequestInProgress;
@property (nonatomic) BOOL hidesTableWhileLoading;
@property (nonatomic,strong)  CustomSpinnerView *loadingView;
@property (strong, nonatomic) BTRetryView *retryView;

@property (nonatomic,strong) UILabel *pageTitleLabel;
@property (nonatomic,strong) UILabel *pageSubTitleLabel;

@property (nonatomic, strong) NSMutableArray<BTBaseTableViewCell*> *tableCells;

- (void)setupTableCells;
- (void)updateTableCells;
- (void)refreshTableView;
- (void)registerNibs;

- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow;
- (void)hideRetryItems:(BOOL)isHide;
- (void)showEmptyViewWithTitle:(NSString *)titleText andNeedToShowImage:(BOOL)needToShowImage;

- (NSString*)pageNameForOmnitureTracking;
- (void)trackOmniPage:(NSString *)page;
- (void)trackPageForOmniture;
- (void)trackPageForOmnitureWithProductService:(NSString *)product andOrderStatus:(NSString*)status;
- (void)trackPageForOmnitureWithName:(NSString*)pageName productService:(NSString *)product andOrderStatus:(NSString*)status;
- (void)trackOmniClick:(NSString *)linkTitle;
- (void)trackOmniClick:(NSString *)linkTitle forPage:(NSString*)pageName;



@end
