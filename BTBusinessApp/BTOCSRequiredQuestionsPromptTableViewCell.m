//
//  BTOCSRequiredQuestionsPromptTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 16/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSRequiredQuestionsPromptTableViewCell.h"

@implementation BTOCSRequiredQuestionsPromptTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    //self.message.text = @"Answer promptly to avoid delays";
    //[self.contentView setNeedsLayout];
    //[self.contentView layoutIfNeeded];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (NSString *)reuseId
{
    return @"BTOCSRequiredQuestionsPromptTableViewCell";
}

@end
