//
//  NLEEWebService.h
//  BTBusinessApp
//

#import "NLWebService.h"

@interface NLEEWebService : NLWebService {
    
}


- (instancetype)initWithMethod:(NSString *)method parameters:(NSString *)parameters andEndpointUrlString:(NSString *)endpointUrlString;

@end
