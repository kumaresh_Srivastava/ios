//
//  BTShadowedView.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 08/11/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MaterialShadowLayer.h"

NS_ASSUME_NONNULL_BEGIN

@interface BTShadowedView : UIView

- (MDCShadowLayer *)shadowLayer;
- (void)setElevation:(CGFloat)points;

@end

NS_ASSUME_NONNULL_END
