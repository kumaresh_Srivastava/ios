//
//  NLGetHubStatus.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/15/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLVordelAuthenticationProtectedWebService.h"

@class NLGetHubStatus;
@class BTUser;
@class NLWebServiceError;

@protocol NLGetHubStatusDelegate <NLVordelAuthenticationProtectedWebServiceDelegate>

- (void)getHubStatus:(NLGetHubStatus *)webService successfullyFetchedHubStatusExecutionId:(NSString *)executionId;

- (void)getHubStatus:(NLGetHubStatus *)webService failedToFetchHubStatusWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLGetHubStatus : NLVordelAuthenticationProtectedWebService {
    
}

@property (nonatomic, readonly) NSString *serialNumber;
@property (nonatomic, readonly) NSString *executionId;

@property (nonatomic, weak) id <NLGetHubStatusDelegate> getHubStatusDelegate;

- (instancetype)initWithHubSerialNumber:(NSString *)serialNumber;
- (instancetype)initWithHubSerialNumber:(NSString *)serialNumber andAuthenticationToken:(NSString *)authenticationToken;

@end


