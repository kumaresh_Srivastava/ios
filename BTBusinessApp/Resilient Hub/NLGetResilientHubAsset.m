//
//  NLGetResilientHubAsset.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/15/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLGetResilientHubAsset.h"
#import "BTAuthenticationManager.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "BTUser.h"
#import "AppConstants.h"
#import "NLVordelAuthenticationProtectedWebService+MbaasHeader.h"
#import "NLWebServiceError.h"
#import "NLWebService+BackEndAPIErrorHandling.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "AppManager.h"
#import "BTResilientHubAsset.h"

#define kGetAssetV1 @"getResilientHubAssetV1"
#define kGetAssetV2 @"getResilientHubAssetV2"

#define kIsV2API NO

@interface NLGetResilientHubAsset () {
    
    NSString *_accessToken;
    BOOL initiatedFromSignInPage;
}

@end

@implementation NLGetResilientHubAsset

- (instancetype)initWithServiceID:(NSString *)serviceID
{
    if (serviceID == nil || [serviceID isEqualToString:@""])
    {
        return nil;
    }
    
    NSDictionary *header = [self getMbaasHeader];
    
    initiatedFromSignInPage = NO;
    
    NSDictionary *service = [NSDictionary dictionaryWithObject:serviceID forKey:@"serviceId"];
    
    NSDictionary *body;
    
    if(kIsV2API) {
        body = [NSMutableDictionary dictionaryWithObject:service forKey:kGetAssetV2];
    } else {
        body = [NSMutableDictionary dictionaryWithObject:service forKey:kGetAssetV1];
    }
    
    
    NSDictionary *payload = [NSDictionary dictionaryWithObjectsAndKeys:header, @"MobileServiceHeader", body, @"Body", nil];
    
    NSError *errorWhileCreatingJSONData = nil;
    NSData *mbaasHeader = [NSJSONSerialization dataWithJSONObject:payload options:0 error:&errorWhileCreatingJSONData];
    
    if(errorWhileCreatingJSONData)
    {
        DDLogError(@"Error while creating JSON data for getResilientHubAsset");
    }
    
    NSString *jsonString = [[NSString alloc] initWithData:mbaasHeader encoding:4];
    
    NSLog(@"RESILIENT PAYLOAD IS: %@", jsonString);
    
    NSString *endPointURLString = @"/clientprofiledetails/getresilienthubasset";
    
    self = [super initWithMethod:@"POST" parameters:jsonString andEndpointUrlString:endPointURLString];
    
    
    if (self)
    {
        _serviceID = [serviceID copy];
    }
    return self;
}

- (instancetype)initWithServiceID:(NSString *)serviceID andAuthenticationToken:(NSString *)authenticationToken
{
    if (serviceID == nil || [serviceID isEqualToString:@""])
    {
        return nil;
    }
    
    if (authenticationToken == nil || [authenticationToken isEqualToString:@""])
    {
        return nil;
    }
    
    NSDictionary *header = [self getMbaasHeader];
    
    initiatedFromSignInPage = YES;
    
    NSMutableDictionary *queryUserDetailBody;
    
    if([AppManager isAppOpnedUsingDeepLinking])
    {
        queryUserDetailBody = [[NSMutableDictionary alloc] init];
        if([[AppManager sharedAppManager] getDeepLinkedActivationCode])
        {
            [queryUserDetailBody setObject:[[AppManager sharedAppManager] getDeepLinkedActivationCode] forKey:@"ActivationCode"];
        }
        else
        {
            [queryUserDetailBody setObject:@"" forKey:@"ActivationCode"];
        }
    }
    
    NSMutableDictionary *body = [NSMutableDictionary dictionaryWithObject:queryUserDetailBody?queryUserDetailBody:@"" forKey:@"getResilientHubAssetV1"];
    
    [body setObject:serviceID forKey:@"serviceId"];
    
    NSDictionary *payload = [NSDictionary dictionaryWithObjectsAndKeys:header, @"MobileServiceHeader", body, @"Body", nil];
    
    NSError *errorWhileCreatingJSONData = nil;
    NSData *mbaasHeader = [NSJSONSerialization dataWithJSONObject:payload options:0 error:&errorWhileCreatingJSONData];
    
    if(errorWhileCreatingJSONData)
    {
        DDLogError(@"Error while creating JSON data for resilientHubAsset");
    }
    
    NSString *jsonString = [[NSString alloc] initWithData:mbaasHeader encoding:4];
    
    NSLog(@"%@", jsonString);
    
    NSString *endPointURLString = @"/clientprofiledetails/getresilienthubasset";
    
    self = [super initWithMethod:@"POST" parameters:jsonString andEndpointUrlString:endPointURLString];
    
    if (self)
    {
        _serviceID = [serviceID copy];
        _accessToken = [authenticationToken copy];
    }
    return self;
}

- (NSString *)friendlyName
{
    return @"GetResilientHubAsset";
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    if (initiatedFromSignInPage)
    {
        NSLog(@"resilienthub Token is: %@", _accessToken);
        NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionary];
        
        [finalHeaderFieldDic setObject:[NSString stringWithFormat:@"Bearer %@", _accessToken] forKey:@"Authorization"];
        [finalHeaderFieldDic setObject:[AppDelegate sharedInstance].viewModel.app.btInstallationID forKey:@"udid"];
        return [finalHeaderFieldDic copy];
    }
    
    return [super httpHeaderFields];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *resultDic = [[[responseObject valueForKey:@"Body"] valueForKey:@"getResilientHubAssetResponseV1"] valueForKey:@"listOfAsset"];
    
    _assets = [[NSMutableArray alloc] init];
    for(id key in resultDic) {
        BTResilientHubAsset *asset = [[BTResilientHubAsset alloc] initWithResponseDictionary:[resultDic objectForKey:key]];
        [_assets addObject:asset];
    }
    
    // success?
    [self.getResilientHubAssetDelegate getResilientHubAsset:self successfullyFetchedAssets:_assets];
    //[self.getResilientHubAssetDelegate getResilientHubAsset:self failedToFetchAssetWithWebServiceError:nil];
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    if (initiatedFromSignInPage)
    {
        DDLogError(@"Webservice with URL: %@ failed with error code: %ld.", [[task currentRequest] URL], (long)[webServiceError.error code]);
    }
    else
    {
        [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    }
    

    [self.getResilientHubAssetDelegate getResilientHubAsset:self failedToFetchAssetWithWebServiceError:webServiceError];
}





#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NSLog(@"processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask");
    
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        
        NSError * err;
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:responseObject options:0 error:&err];
        NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        DDLogInfo(@"RESILIENT RESPONSE OBJECT:\n %@", myString);
        
        if([responseObject isValidVordelAPIResponseObject])
        {
            NSDictionary *body = [responseObject valueForKey:@"Body"];
            if([body valueForKey:@"getResilientHubAssetResponseV1"])
            {
                NSDictionary *resilientHubResponse = [body valueForKey:@"getResilientHubAssetResponseV1"];
                
                if([resilientHubResponse valueForKey:@"listOfAsset"])
                {
                    // Interrogate the object for further potential faults here...
                    errorToBeReturned = nil;
                }
                else
                {
                    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
                    errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
                    DDLogError(@"Invalid JSON Object in webservice with URL: %@", [[task currentRequest] URL]);
                }
            }
            else
            {
                NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
                errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
                DDLogError(@"'resilientHubResponse' element is missing in Body of response object for URL %@", [[task currentRequest] URL]);
            }
        }
        else
        {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
            
            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
            DDLogError(@"Invalid Response Object for URL %@", [[task currentRequest] URL]);
        }
    }
    
    return errorToBeReturned;
}




#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    
    //    NLWebServiceError *errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeVordelTokenInvalidOrExpired];
    //
    //    return errorToBeReturned;
    
    return nil;
}





@end

