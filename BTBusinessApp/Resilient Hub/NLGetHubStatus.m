//
//  NLGetHubStatus.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/15/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLGetHubStatus.h"
#import "BTAuthenticationManager.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "BTUser.h"
#import "AppConstants.h"
#import "NLVordelAuthenticationProtectedWebService+MbaasHeader.h"
#import "NLWebServiceError.h"
#import "NLWebService+BackEndAPIErrorHandling.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "AppManager.h"

@interface NLGetHubStatus () {
    
    NSString *_accessToken;
    BOOL initiatedFromSignInPage;
}

@end

@implementation NLGetHubStatus

- (instancetype)initWithHubSerialNumber:(NSString *)serialNumber
{
    if (serialNumber == nil || [serialNumber isEqualToString:@""])
    {
        return nil;
    }
    
    initiatedFromSignInPage = NO;
    
    NSDictionary *header = [self getMbaasHeader];
    
    NSDictionary *body = [NSDictionary dictionaryWithObject:serialNumber forKey:@"hubSerialNumber"];
    
    NSDictionary *payload = [NSDictionary dictionaryWithObject:body forKey:@"getHubStatus"];
    
    NSDictionary *parentPayload = [NSDictionary dictionaryWithObjectsAndKeys:header, @"MobileServiceHeader", payload, @"Body", nil];
    
    NSError *errorWhileCreatingJSONData = nil;
    NSData *mbaasHeader = [NSJSONSerialization dataWithJSONObject:parentPayload options:0 error:&errorWhileCreatingJSONData];
    
    if(errorWhileCreatingJSONData)
    {
        DDLogError(@"Error while creating JSON data for getHubStatus");
    }
    
    NSString *jsonString = [[NSString alloc] initWithData:mbaasHeader encoding:4];
    
    NSLog(@"HUB STATUS PAYLOAD IS: %@", jsonString);
    
    NSString *endPointURLString = @"/manageworkflows/gethubstatus";
    
    self = [super initWithMethod:@"POST" parameters:jsonString andEndpointUrlString:endPointURLString];
    
    if(self) {
        _serialNumber = serialNumber;
    }
    
    return self;
}

- (instancetype)initWithHubSerialNumber:(NSString *)serialNumber andAuthenticationToken:(NSString *)authenticationToken
{
    if (serialNumber == nil || [serialNumber isEqualToString:@""])
    {
        return nil;
    }
    
    if (authenticationToken == nil || [authenticationToken isEqualToString:@""])
    {
        return nil;
    }
    
    NSDictionary *header = [self getMbaasHeader];
    
    initiatedFromSignInPage = YES;
    
    NSMutableDictionary *queryUserDetailBody;
    
    if([AppManager isAppOpnedUsingDeepLinking])
    {
        queryUserDetailBody = [[NSMutableDictionary alloc] init];
        if([[AppManager sharedAppManager] getDeepLinkedActivationCode])
        {
            [queryUserDetailBody setObject:[[AppManager sharedAppManager] getDeepLinkedActivationCode] forKey:@"ActivationCode"];
        }
        else
        {
            [queryUserDetailBody setObject:@"" forKey:@"ActivationCode"];
        }
    }
    
    NSDictionary *body = [NSDictionary dictionaryWithObject:queryUserDetailBody?queryUserDetailBody:@"" forKey:@"getHubStatus"];
    
    NSDictionary *payload = [NSDictionary dictionaryWithObjectsAndKeys:header, @"MobileServiceHeader", body, @"Body", nil];
    
    NSError *errorWhileCreatingJSONData = nil;
    NSData *mbaasHeader = [NSJSONSerialization dataWithJSONObject:payload options:0 error:&errorWhileCreatingJSONData];
    
    if(errorWhileCreatingJSONData)
    {
        DDLogError(@"Error while creating JSON data for resilientHubAsset");
    }
    
    NSString *jsonString = [[NSString alloc] initWithData:mbaasHeader encoding:4];
    
    NSString *endPointURLString = @"/manageworkflows/gethubstatus";
    
    self = [super initWithMethod:@"POST" parameters:jsonString andEndpointUrlString:endPointURLString];
    
    if (self)
    {
        _serialNumber = serialNumber;
        _accessToken = [authenticationToken copy];
    }
    return self;
}

- (NSString *)friendlyName
{
    return @"GetHubStatus";
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (NSDictionary *)httpHeaderFields
{
    if (initiatedFromSignInPage)
    {
        NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionary];
        
        [finalHeaderFieldDic setObject:[NSString stringWithFormat:@"Bearer %@", _accessToken] forKey:@"Authorization"];
        [finalHeaderFieldDic setObject:[AppDelegate sharedInstance].viewModel.app.btInstallationID forKey:@"udid"];
        return [finalHeaderFieldDic copy];
    }
    
    return [super httpHeaderFields];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
//    _executionId = [[[responseObject valueForKey:@"MobileServiceResponse"] valueForKey:@"Body"] valueForKey:@"getHubStatusResponse"] valueForKey:@"executionId"];
    
    [self.getHubStatusDelegate getHubStatus:self  successfullyFetchedHubStatusExecutionId:_executionId];
    
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    if (initiatedFromSignInPage)
    {
        DDLogError(@"Webservice with URL: %@ failed with error code: %ld.", [[task currentRequest] URL], (long)[webServiceError.error code]);
    }
    else
    {
        [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    }
    
    [self.getHubStatusDelegate getHubStatus:self failedToFetchHubStatusWithWebServiceError:webServiceError];
}

#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        
        if([responseObject isValidVordelAPIResponseObject])
        {
            NSDictionary *response = [responseObject valueForKey:@"MobileServiceResponse"];
            NSDictionary *body = [response valueForKey:@"Body"];
            if([body valueForKey:@"getHubStatusResponse"])
            {
                NSDictionary *resilientHubResponse = [body valueForKey:@"getHubStatusResponse"];
                _executionId = [resilientHubResponse valueForKey:@"executionId"];
                errorToBeReturned = nil;
            }
            else
            {
                NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
                errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
                DDLogError(@"'resilientHubResponse' element is missing in Body of response object for URL %@", [[task currentRequest] URL]);
            }
        }
        else
        {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
            
            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
            DDLogError(@"Invalid Response Object for URL %@", [[task currentRequest] URL]);
        }
    }
    
    return errorToBeReturned;
}




#pragma mark - Dummy Unit Testing Methods

- (NLWebServiceError *)dummyUnitTestCaseWebServiceError
{
    
    //    NLWebServiceError *errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeVordelTokenInvalidOrExpired];
    //
    //    return errorToBeReturned;
    
    return nil;
}





@end


