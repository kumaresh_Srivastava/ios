//
//  NLGetResilientHubAsset.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/15/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLVordelAuthenticationProtectedWebService.h"

@class NLGetResilientHubAsset;
@class BTUser;
@class NLWebServiceError;

@protocol NLGetResilientHubAssetDelegate <NLVordelAuthenticationProtectedWebServiceDelegate>

- (void)getResilientHubAsset:(NLGetResilientHubAsset *)webService successfullyFetchedAssets:(NSMutableArray *)assets;

- (void)getResilientHubAsset:(NLGetResilientHubAsset *)webService failedToFetchAssetWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLGetResilientHubAsset : NLVordelAuthenticationProtectedWebService {
    
}

@property (nonatomic, readonly) NSString *serviceID;
@property (nonatomic, readonly) NSMutableArray *assets;

@property (nonatomic, weak) id <NLGetResilientHubAssetDelegate> getResilientHubAssetDelegate;

- (instancetype)initWithServiceID:(NSString *)serviceID;
- (instancetype)initWithServiceID:(NSString *)serviceID andAuthenticationToken:(NSString *)authenticationToken;

@end

