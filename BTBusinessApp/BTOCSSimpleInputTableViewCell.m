//
//  BTOCSSimpleInputTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 03/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSSimpleInputTableViewCell.h"

@interface BTOCSSimpleInputTableViewCell () <UITextViewDelegate>

@end

@implementation BTOCSSimpleInputTableViewCell

+ (NSString *)reuseId
{
    return @"BTOCSSimpleInputTableViewCell";
}

- (void)awakeFromNib {
    //[self.entryTextView addObserver:self forKeyPath:@"contentSize" options:(NSKeyValueObservingOptionNew) context:NULL];
    [super awakeFromNib];
    // Initialization code
    self.entryTextView.layer.borderWidth = 1.0f;
    self.entryTextView.layer.cornerRadius = 5.0f;
    self.entryTextView.layer.borderColor = [BrandColours colourBtNeutral80].CGColor;
    self.entryTextView.autocorrectionType = UITextAutocorrectionTypeNo;
    self.entryTextView.scrollEnabled = NO;
    self.entryTextView.delegate = self;
    self.entryTextView.contentInset = UIEdgeInsetsMake(2, 10, 0, 0);
    
    UIToolbar *keyboardAccesory = [[UIToolbar alloc] init];
    UIBarButtonItem *prevBtn = [[UIBarButtonItem alloc] initWithTitle:@"Previous" style:UIBarButtonItemStylePlain target:self action:@selector(prevButtonAction)];
    UIBarButtonItem *spacing = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *nextBtn = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextButtonAction)];
    keyboardAccesory.items = @[prevBtn,spacing,nextBtn];
    [keyboardAccesory sizeToFit];
    self.entryTextView.inputAccessoryView = keyboardAccesory;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prevButtonAction
{
    [self.entryTextView resignFirstResponder];
    if (self.delegate && [self.delegate respondsToSelector:@selector(previousButtonPressedOnCell:)]) {
        [self.delegate previousButtonPressedOnCell:self];
    }
}

- (void)nextButtonAction
{
    [self.entryTextView resignFirstResponder];
    if (self.delegate && [self.delegate respondsToSelector:@selector(nextButtonPressedOnCell:)]) {
        [self.delegate nextButtonPressedOnCell:self];
    }
}

#pragma mark - uitextview delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    BOOL retVal = YES;
    // text validation goes here
    if ([text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location != NSNotFound) {
        [textView resignFirstResponder];
        retVal = NO;
    }
    return retVal;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(cell:finishedEditingWithValue:)]) {
        [self.delegate cell:self finishedEditingWithValue:textView.text];
    }
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (object == self.entryTextView) {
        UITextView *tv = self.entryTextView;
        CGFloat topCorrect = ([tv bounds].size.height - [tv     contentSize].height * [tv zoomScale])/2.0;
        topCorrect = ( topCorrect < 0.0 ? 0.0 : topCorrect );
        //[tv setContentInset:UIEdgeInsetsMake(topCorrect,0,0,0)];
        [tv setTextContainerInset:UIEdgeInsetsMake(topCorrect,0,0,0)];
    }
}

@end
