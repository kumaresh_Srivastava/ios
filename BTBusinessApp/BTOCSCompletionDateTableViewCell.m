//
//  BTOCSCompletionDateTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 02/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSCompletionDateTableViewCell.h"

@implementation BTOCSCompletionDateTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (NSString *)reuseId
{
    return @"BTOCSCompletionDateTableViewCell";
}


@end
