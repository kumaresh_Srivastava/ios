//
//  BTHubHealthCheckTableViewCell.m
//  BTBusinessApp
//

#import "BTHubHealthCheckTableViewCell.h"

@implementation BTHubHealthCheckTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateCellWithHealthcheckCellData:(NSDictionary*)healthcheckData{
    _healthCheckTitle.text = [healthcheckData valueForKey:@"Title"];
    _healthCheckDescription.text = [healthcheckData valueForKey:@"Description"];
    _healthCheckHub.image = [UIImage imageNamed:[healthcheckData valueForKey:@"Image"]];
}

@end
