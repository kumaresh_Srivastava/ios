//
//  BTMobileServiceContractDetailTableViewCell.h
//  BTBusinessApp
//
//  Created by kumaresh shrivastava on 25/06/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMMobileServiceAssetDetailScreen.h"
NS_ASSUME_NONNULL_BEGIN

@interface BTMobileServiceContractDetailTableViewCell : UITableViewCell

- (void)upadateWithRowWrapper:(MobileAssetsDetailRowWrapper *)rowWrapper;

@end

NS_ASSUME_NONNULL_END
