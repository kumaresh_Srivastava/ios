//
//  BTOrderStatusLabel.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 23/11/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOrderStatusLabel.h"
#import "BrandColours.h"

@interface BTOrderStatusLabel ()

@property (nonatomic, readonly) UIEdgeInsets edgeInsets;

@end

@implementation BTOrderStatusLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (UIEdgeInsets)edgeInsets
{
    return UIEdgeInsetsMake(0, 15, 0, 15);
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

-(void)setOrderStatus:(NSString *)status
{
    if(status.length>0)
        self.text = [status stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[status substringToIndex:1] uppercaseString]];
    else
        self.text = @"";
    
    self.layer.borderWidth = 1.0f;
    self.layer.cornerRadius = 2.0f;
    
    if ([[status lowercaseString] isEqualToString:@"completed"] || [[status lowercaseString] isEqualToString:@"paid"] || [[status lowercaseString] isEqualToString:@"resolved"] || [[status lowercaseString] isEqualToString:@"fault resolved"]) {
        
        [self setTextColor:[UIColor whiteColor]];
        self.layer.borderColor = [BrandColours colourMyBtGreen].CGColor;
        self.backgroundColor = [BrandColours colourMyBtGreen];
        
    } else if ([[status lowercaseString] isEqualToString:@"delayed"] || [[status lowercaseString] isEqualToString:@"overdue"]) {
        [self setTextColor:[BrandColours colourMyBtRed]];
        self.layer.borderColor = [BrandColours colourMyBtRed].CGColor;
        self.backgroundColor = [UIColor clearColor];
        
    }
    else if ([[status lowercaseString] isEqualToString:@"in progress"] || [[status lowercaseString] isEqualToString:@"ready"]) {
        [self setTextColor:[BrandColours colourMyBtGreen]];
        self.layer.borderColor = [BrandColours colourMyBtGreen].CGColor;
        self.backgroundColor = [UIColor clearColor];
    } else if([[status lowercaseString] isEqualToString:@""] || [status lowercaseString] == nil) {
        [self setTextColor:[UIColor clearColor]];
        self.layer.borderColor = [UIColor clearColor].CGColor;
        self.backgroundColor = [UIColor clearColor];
    }
    else {
        [self setTextColor:[BrandColours colourMyBtOrange]];
        self.layer.borderColor = [BrandColours colourMyBtOrange].CGColor;
        self.backgroundColor = [UIColor clearColor];
    }
//    else {     //@Manik Fix
//        [self setTextColor:[UIColor clearColor]];
//        self.layer.borderColor = [UIColor clearColor].CGColor;
//        self.backgroundColor = [UIColor clearColor];
//    }
}

-(void)setMSOFlag:(NSString *)msoFlag forBroadband:(BOOL)broadband;
{
    
    if(msoFlag.length>0)
        self.text = [msoFlag stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[msoFlag substringToIndex:1] uppercaseString]];
    else
        self.text = @"";
    
    self.layer.borderWidth = 1.0f;
    self.layer.cornerRadius = 2.0f;
    
    //[self sizeToFit];
    
    if ([[msoFlag lowercaseString] isEqualToString:@"recently resolved issue"]) {
        [self setTextColor:[UIColor whiteColor]];
        self.layer.borderColor = [BrandColours colourTrueGreen].CGColor;
        self.backgroundColor = [BrandColours colourTrueGreen];
        
    } else if ([[msoFlag lowercaseString] isEqualToString:@"ongoing issue"] || [[msoFlag lowercaseString] isEqualToString:@"problems found"] || [[msoFlag lowercaseString] isEqualToString:@"unable to connect"]) {
        [self setTextColor:[BrandColours colourBTBusinessRed]];
        self.layer.borderColor = [BrandColours colourBTBusinessRed].CGColor;
        self.backgroundColor = [UIColor clearColor];
    }
    else if ([[msoFlag lowercaseString] isEqualToString:@"no issues found"]  || [[msoFlag lowercaseString] isEqualToString:@"no problems found"]) {
        if(broadband) {
            [self setTextColor:[BrandColours colourMyBtLightBlue]];
            self.layer.borderColor = [BrandColours colourMyBtLightBlue].CGColor;
            self.backgroundColor = [UIColor clearColor];
        } else {
            [self setTextColor:[BrandColours colourTrueGreen]];
            self.layer.borderColor = [BrandColours colourTrueGreen].CGColor;
            self.backgroundColor = [UIColor clearColor];
        }
    }
    else if ( [[msoFlag lowercaseString] isEqualToString:@"4G Assure connected".lowercaseString]) {
        self.text = @"Broadband connected";
        [self setTextColor:[BrandColours colourBackgroundBTPurplePrimaryColor]];
        self.layer.borderColor = [BrandColours colourBackgroundBTPurplePrimaryColor].CGColor;
        self.backgroundColor = [UIColor clearColor];
    }
    else if ([[msoFlag lowercaseString] isEqualToString:@"unable to detect hub"] || [[msoFlag lowercaseString] isEqualToString:@"broadband connected"]) {
        [self setTextColor:[BrandColours colourMyBtLightBlue]];
        self.layer.borderColor = [BrandColours colourMyBtLightBlue].CGColor;
        self.backgroundColor = [UIColor clearColor];
        
    }
    else {
        [self setTextColor:[BrandColours colourMyBtOrange]];
        self.layer.borderColor = [BrandColours colourMyBtOrange].CGColor;
        self.backgroundColor = [UIColor clearColor];
    }
//    [self sizeToFit];
}

- (void)drawTextInRect:(CGRect)rect {
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, self.edgeInsets)];
}

- (CGSize)intrinsicContentSize
{
    CGSize size = [super intrinsicContentSize];
    size.width  += self.edgeInsets.left + self.edgeInsets.right;
    size.height += self.edgeInsets.top + self.edgeInsets.bottom;
    return size;
}
    

@end
