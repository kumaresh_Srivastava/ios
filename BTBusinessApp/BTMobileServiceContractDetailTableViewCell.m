//
//  BTMobileServiceContractDetailTableViewCell.m
//  BTBusinessApp
//
//  Created by kumaresh shrivastava on 25/06/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import "BTMobileServiceContractDetailTableViewCell.h"
#import "AppManager.h"

@interface BTMobileServiceContractDetailTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *contractTermsTextLabel;

@property (weak, nonatomic) IBOutlet UILabel *contractTermsValueLabel;

@property (weak, nonatomic) IBOutlet UILabel *contractEndDateTextLabel;

@property (weak, nonatomic) IBOutlet UILabel *contractEndDateValueLabel;

@property (weak, nonatomic) IBOutlet UILabel *simSerialNumberValuLabel;
@property (weak, nonatomic) IBOutlet UILabel *yourRentalValueLabel;

@end

@implementation BTMobileServiceContractDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

/*
 @property(nonatomic, strong) NSString *mobilePlan;
 @property(nonatomic, strong) NSString *contractTerms;
 @property(nonatomic, strong) NSString *contractEndDate;
 @property(nonatomic, strong) NSString *simSerialNumber;
 @property(nonatomic, strong) NSString *totalRental;
 @property(nonatomic, strong) NSString *mobileDevice;
 @property(nonatomic, strong) NSString *imieNumber;
 
 */


- (void)upadateWithRowWrapper:(MobileAssetsDetailRowWrapper *)rowWrapper{
    
    self.contractTermsValueLabel.text = rowWrapper.contractTerms;
    
    
    NSDate* date = [AppManager dateFromString:rowWrapper.contractEndDate];
    if(date){
        self.contractEndDateValueLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:date];
    }
    else{
        self.contractEndDateValueLabel.text = @"-";
    }

    self.simSerialNumberValuLabel.text = rowWrapper.simSerialNumber;
    self.yourRentalValueLabel.text = rowWrapper.totalRental;
    
}


@end
