//
//  NLSubmitProjectQuestions.h
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLAPIGEEAuthenticatedWebService.h"

@class NLSubmitProjectQuestions;
@class NLWebServiceError;

@protocol NLSubmitProjectQuestionsDelegate <NLAPIGEEAuthenticationProtectedWebServiceDelegate>

- (void)submitProjectQuestionsWebService:(NLSubmitProjectQuestions *)webService finishedWithResponse:(NSObject *)response;

- (void)submitProjectQuestionsWebService:(NLSubmitProjectQuestions *)webService failedWithWebServiceError:(NLWebServiceError *)error;

@optional
- (void)timedOutWithoutSuccessForSubmitProjectQuestionsWebService:(NLSubmitProjectQuestions*)webService;

@end

@interface NLSubmitProjectQuestions : NLAPIGEEAuthenticatedWebService

@property (nonatomic) NSDictionary* projectDetails;

@property (nonatomic, weak) id <NLSubmitProjectQuestionsDelegate> submitProjectQuestionsDelegate;

- (instancetype)initWithProjectDetails:(NSDictionary*)projectDetails;
- (instancetype)initWithProjectRef:(NSString*)projectRef questionGroupActivitiyID:(NSInteger)activityID questionGroupID:(NSInteger)groupID questionResponses:(NSArray*)responses;

@end
