//
//  BTCheckServiceStatusTableViewCell.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 22/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTCheckServiceStatusTableViewCell.h"
#import "BTBroadbandAndPhoneService.h"

@interface BTCheckServiceStatusTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *serviceNameLabel;

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *serviceIdValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationValueLabel;

@end


@implementation BTCheckServiceStatusTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    CGRect frame = self.frame;
//    frame.size.width = [[UIScreen mainScreen] bounds].size.width;
//    self.frame = frame;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)updateCheckServiceStatusTableViewCellWith:(BTBroadbandAndPhoneService *)service {
//    _containerView.layer.borderColor = [UIColor colorWithRed:(221/255.0) green:(221/255.0) blue:(221/255.0) alpha:1].CGColor;
//    _containerView.layer.borderWidth = 1;
//    _containerView.layer.cornerRadius = 5;
    
    if([service.productName isEqualToString:@"Business BroadBand Access"]) {
        self.serviceNameLabel.text = @"Business broadband";
    }
    else {
        if ([service.serviceId rangeOfString:@"cv" options:NSCaseInsensitiveSearch].location != NSNotFound) {
            self.serviceNameLabel.text = @"Cloud Voice Express";
        }
        else {
            self.serviceNameLabel.text = service.productName;
        }
    }
    
    self.serviceIdValueLabel.text = service.serviceId;

    if([service.postcode isEqualToString:@""] || service.postcode == nil) {
        self.locationValueLabel.text = @"n/a";
    }
    else {
         self.locationValueLabel.text = service.postcode;
    }

//    if([service.productType isEqualToString: @"BB"]) {
//        self.serviceIconImageView.image = [UIImage imageNamed:@"broadband_small_new"];
//    }
//    else if([service.productType isEqualToString: @"PSTN"]) {
//        self.serviceIconImageView.image = [UIImage imageNamed:@"phoneline_small_new"];
//    }
    
    
}
@end
