//
//  BTAppointmentPickerViewController.m
//  ApptPicker
//
//  Created by Gareth Vaughan on 15/11/2017.
//  Copyright © 2017 Gareth Vaughan. All rights reserved.
//

#import "BTAppointmentPickerViewController.h"
#import "AppConstants.h"
#import "BrandColours.h"
#import "DLMAmmendEngineerAppointmentScreen.h"
#import "BTAmendFooterActionView.h"
#import "BTNeedHelpViewController.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "NLWebServiceError.h"
#import "CustomSpinnerAnimationView.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

#define kAppointmentMaxSize 90

@interface BTAppointmentPickerViewController () <DLMAmmendEngineerAppointmentScreenDelegate,BTAmendFooterActionViewDelegate>

@property (nonatomic, strong) NSDateFormatter *dayNameFormatter;
@property (nonatomic, strong) NSDateFormatter *dayMonthFormatter;
@property (strong, nonatomic) BTAppointment *selectedDay;
@property (nonatomic, strong) BTAppointment *updatedAppointment;
@property (nonatomic, strong) DLMAmmendEngineerAppointmentScreen *appointmentModel;

@property (nonatomic) BOOL isOpeningNeedHelp;
@property (nonatomic) BOOL isDataChanged;

@property (assign, nonatomic) BOOL isLoading;
@property (assign, nonatomic) BOOL hasMoreData;
@property (nonnull, strong) NSDate *maxDate;

@property (nonatomic, strong) BTAmendFooterActionView *amendFooterActionView;
@property (nonatomic, strong) CustomSpinnerAnimationView *spinner;

@end

@implementation BTAppointmentPickerViewController

+ (BTAppointmentPickerViewController *)getBTAppointmentPickerViewController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ApptPicker" bundle:nil];
    BTAppointmentPickerViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTAppointmentPickerViewControllerID"];
    return vc;
}

- (NSMutableArray *)appointments
{
    if (!_appointments) {
        _appointments = [NSMutableArray new];
    }
    return _appointments;
}

- (CustomSpinnerAnimationView *)spinner
{
    if (!_spinner) {
        _spinner = [[CustomSpinnerAnimationView alloc] initWithText:@"" withLoadingImage:[UIImage imageNamed:@"Spinner"]];
    }
    return _spinner;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if(_pcpOnly) {
        self.title = @"Activation date";
    } else {
        self.title = @"Engineer appointment";
    }
    
    self.navigationItem.leftBarButtonItem = [self getCloseButtonItem];
    
    // static data
//    NSError *error;
//    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"2HourApptResponse" ofType:@"json"];
//    NSData *stubResponse = [NSData dataWithContentsOfFile:filePath];
//    NSObject *jsonBlob = [NSJSONSerialization JSONObjectWithData:stubResponse options:NSJSONReadingMutableLeaves error:&error];
//    if (!error) {
//        NSDictionary *responseDic = [jsonBlob isKindOfClass:[NSDictionary class]]?(NSDictionary*)jsonBlob:nil;
//        NSArray* apptsArray = (NSArray*)[responseDic valueForKeyPath:@"result.Appointments"];
//        for (NSObject *appt in apptsArray) {
//            if ([appt isKindOfClass:[NSDictionary class]]) {
//                BTAppointment *appointment = [[BTAppointment alloc] initWithResponseDictionaryFromAppointmentsAPIResponse:(NSDictionary *)appt];
//                [self.appointments addObject:appointment];
//            }
//        }
//    }
    
    self.appointmentModel = [[DLMAmmendEngineerAppointmentScreen alloc] init];
    self.appointmentModel.appointmentDelegate = self;
    
    self.dayNameFormatter = [[NSDateFormatter alloc] init];
    [self.dayNameFormatter setDateFormat:@"EEE"];
    
    self.dayMonthFormatter = [[NSDateFormatter alloc] init];
    [self.dayMonthFormatter setDateFormat:@"dd MMM"];
    
    // set up collectionview
    self.dayPickerCollectionView.dataSource = self;
    self.dayPickerCollectionView.delegate = self;
    [self.dayPickerCollectionView setAllowsSelection:YES];
    
    // set up tableview
    self.timePickerTableView.dataSource = self;
    self.timePickerTableView.delegate = self;
    //self.timePickerTableView.rowHeight = UITableViewAutomaticDimension;
    //self.timePickerTableView.estimatedRowHeight = 60.0;
    //self.timePickerTableView.separatorColor = [UIColor colorForHexString:@"E1E1E1"];
    self.timePickerTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    // add footer view
    [self createFooterActionView];
    
    __weak typeof(self) weakSelf = self;
    
    [RACObserve(self, self.isDataChanged) subscribeNext:^(NSNumber* state) {
        
        if (weakSelf.isDataChanged) {
            
            weakSelf.navigationItem.rightBarButtonItem = [weakSelf getSaveButtonItem];
        }
        
        else {
            
            weakSelf.navigationItem.rightBarButtonItem = nil;
        }
    }];
    
    self.isDataChanged = NO;
    self.isLoading = NO;
    
    if (!self.isFaultAccessed) {
        [self trackOmniPage:OMNIPAGE_ORDER_AMEND_APPOINTMENT];
        [self trackPageWithKeyTask:OMNI_KEYTASK_ORDER_TRACKER_ENGINEER_APPOINTMENT_CHECKED];
    }
    
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setDay:90];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    self.maxDate = [calendar dateByAddingComponents:dateComponents toDate:self.appointmentDate options:0];
    
    self.hasMoreData = YES;
    [self makeAPIcall];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.isOpeningNeedHelp = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)createFooterActionView{
    
    //Footer actionview:Need Help Button
    
    if(!self.amendFooterActionView)
        self.amendFooterActionView = [[[NSBundle mainBundle] loadNibNamed:@"BTAmendFooterActionView" owner:self options:nil] firstObject];
    self.amendFooterActionView.translatesAutoresizingMaskIntoConstraints = NO;
    self.amendFooterActionView.delegate = self;
    [self.view addSubview:self.amendFooterActionView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.amendFooterActionView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.amendFooterActionView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.amendFooterActionView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:45.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.amendFooterActionView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.bottomLayoutGuide  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    
    // [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_amendFooterActionView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    
}

- (void)saveButtonPressed:(id)sender{
    [self.delegate btAmendEngineerViewController:self didSelectAppointment:self.updatedAppointment];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelInProgressAPI{
    
    if(!self.isOpeningNeedHelp){
        [self.appointmentModel cancelEngineerAppointmentsAPIRequest];
        [self.appointmentModel cancelEngineerPreAppointmentsAPIRequest];
    }
}

- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow{
    
    if(!_retryView){
        
        _retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
        _retryView.translatesAutoresizingMaskIntoConstraints = NO;
        _retryView.retryViewDelegate = self;
        [self.view addSubview:_retryView];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:_amendFooterActionView  attribute:NSLayoutAttributeTop multiplier:1.0 constant:5.0]];
    }
    
    _retryView.hidden = NO;
    _isRetryViewShown = YES;
    _retryView.retryViewDelegate = self;
    [_retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
}

- (void)makeAPIcall
{
    if(![AppManager isInternetConnectionAvailable]){
        
        [self.spinner setHidden:YES];
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_ORDER_AMEND_APPOINTMENT];
        if([self.appointments count] <= 0){
            [self showRetryViewWithInternetStrip:YES];
        }
        return;
    }
    
    if (!self.isLoading) {
        self.isLoading = YES;
        [self.spinner setHidden:NO];
        [self hideRetryView];
        
        if([self.appointments count] <= 0){
            self.networkRequestInProgress = YES;
        } else {
            [self.spinner startAnimation];
        }
        
        NSString *dateString = @"";
        if([self.appointments count] > 0){
            BTAppointment *appointment = [self.appointments lastObject];
            //Compare
            NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
            [dateComponents setDay:1];
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDate *nextToLastDate = [calendar dateByAddingComponents:dateComponents toDate:appointment.date options:0];
            dateString = [self getFormatedDateFromDate:nextToLastDate];
            if(self.isSIM2Order == YES){
                 [self.appointmentModel fetchEngineerAppointmentsForLoggedInUserWithOrderRef:self.orderReference itemRef:self.SIM2OrderRef appointmentDate:dateString andPCP:_pcpOnly];
            } else {
                [self.appointmentModel fetchEngineerAppointmentsForLoggedInUserWithOrderRef:self.orderReference itemRef:self.itemReference appointmentDate:dateString andPCP:_pcpOnly];
            }
        } else {
            // no appointments, so get prepone dates to begin
            dateString = [self getFormatedDateFromDate:self.appointmentDate];
            if(self.isSIM2Order == YES){
                 [self.appointmentModel fetchEngineerPreAppointmentsForLoggedInUserWithOrderRef:self.orderReference itemRef:self.SIM2OrderRef appointmentDate:dateString andPCP:_pcpOnly];
            } else {
                [self.appointmentModel fetchEngineerPreAppointmentsForLoggedInUserWithOrderRef:self.orderReference itemRef:self.itemReference appointmentDate:dateString andPCP:_pcpOnly];
            }
        }
    }
}

- (NSString *)getFormatedDateFromDate:(NSDate *)date{
    
    NSString *dateFormat = @"dd-MM-yyyy";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
}

- (void)showSelectedAppointment
{
    if (self.appointments && !self.selectedDay) {
        self.selectedDay = [self.appointments firstObject];
    }
    
    NSIndexPath *idx = [[NSIndexPath alloc] initWithIndex:[self.appointments indexOfObject:self.selectedDay]];
    [self.dayPickerCollectionView selectItemAtIndexPath:idx animated:NO scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
    [self.dayPickerCollectionView reloadData];
    [self.timePickerTableView reloadData];
    
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger items = self.appointments?self.appointments.count:0;
    if (self.appointments && self.appointments.count>0 && self.hasMoreData) {
        items += 1;
    }
    return items;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BTAppointmentPickerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BTAppointmentPickerCollectionViewCell" forIndexPath:indexPath];
    if (!cell) {
        cell = [[BTAppointmentPickerCollectionViewCell alloc] init];
    }
    if (self.appointments.count > 0 && indexPath.item < self.appointments.count) {
        BTAppointment *appointment = (BTAppointment*)[self.appointments objectAtIndex:indexPath.item];
        [cell.dayNameLabel setHidden:NO];
        [cell.dateMonthLabel setHidden:NO];
        [cell.selectedIndicator setHidden:NO];
        [cell.loadingView setHidden:YES];

        cell.dayNameLabel.text = [self.dayNameFormatter stringFromDate:appointment.date];
        cell.dayNameLabel.font = [UIFont fontWithName:kBtFontRegular size:20.0];
        cell.dateMonthLabel.text = [self.dayMonthFormatter stringFromDate:appointment.date];
        cell.dateMonthLabel.font = [UIFont fontWithName:kBtFontRegular size:20.0];

        if ([appointment.date isEqual:self.selectedDay.date]) {
            cell.selectedIndicator.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
        } else {
            cell.selectedIndicator.backgroundColor = [UIColor clearColor];
        }
    } else {
        [cell.dayNameLabel setHidden:YES];
        [cell.dateMonthLabel setHidden:YES];
        [cell.selectedIndicator setHidden:YES];
        [cell.loadingView setHidden:NO];
        [cell.loadingView addSubview:self.spinner];
    }
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    BOOL retVal = NO;
    if (self.appointments && self.appointments.count >0 && indexPath.item<self.appointments.count) {
        retVal = YES;
    }
    return retVal;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedDay = [self.appointments objectAtIndex:indexPath.item];
    [self.dayPickerCollectionView reloadData];
    [self.timePickerTableView reloadData];
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.item == self.appointments.count) {
        // load more days if available
        if (self.hasMoreData) {
            [self makeAPIcall];
        }
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 0;
    if (self.selectedDay) {
        rows = [self.selectedDay.appointmentSlots count];
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTAppointmentPickerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BTAppointmentPickerTableViewCellID"];
    if (!cell) {
        cell = [[BTAppointmentPickerTableViewCell alloc] init];
    }
    NSArray *slots = self.selectedDay.appointmentSlots;
    if (slots && slots.count > 0) {
        NSString *timeslot = [self.selectedDay.appointmentSlots objectAtIndex:indexPath.row];
        if(_pcpOnly) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"EEEE, d MMMM"];
            NSString *dayName = [dateFormatter stringFromDate:[self.selectedDay date]];
            cell.timeslotLabel.text = dayName;
            if(indexPath.row > 0) { // Only show the first row
                cell.hidden = YES;
            }
        } else {
            cell.timeslotLabel.text = timeslot;
        }
        cell.timeslotLabel.font = [UIFont fontWithName:kBtFontRegular size:17.0];
        UIView *selBgView = [[UIView alloc] init];
        [selBgView setBackgroundColor:[UIColor colorForHexString:@"f1f0f7"]];
        [cell setSelectedBackgroundView:selBgView];
        
        UIView *unselBgView = [[UIView alloc] init];
        [unselBgView setBackgroundColor:[UIColor clearColor]];
        
        BOOL isSelectedAppt = NO;
        if (self.updatedAppointment) {
            // check - has user selected this appt
            if ([[NSCalendar currentCalendar] isDate:self.selectedDay.date inSameDayAsDate:self.updatedAppointment.date]) {
                if ([self.updatedAppointment.selectedTimeslot isEqualToString:timeslot]) {
                    isSelectedAppt = YES;
                }
            }
        } else if (!self.isDataChanged) {
            // user hasn't changed appt
            if ([self.selectedDay isTimeslotSelected:timeslot]) {
                // check - is selected by API data
                isSelectedAppt = YES;
            }
            else if (!_pcpOnly && [[NSCalendar currentCalendar] isDate:self.initialAppointment.date inSameDayAsDate:self.selectedDay.date]) {
                if ([self.initialAppointment.selectedTimeslot isEqualToString:timeslot]) {
                    // check - is selected by initial appt
                    isSelectedAppt = YES;
                }
            }
        }
        
        if (isSelectedAppt) {
            // user selected timeslot (or initial timeslot if not supplied by api)
            cell.selectedIndicator.image = [UIImage imageNamed:@"radio_selected"];
            cell.backgroundView = selBgView;
        } else if ([self.selectedDay hasTimeslot:timeslot]) {
            // timeslot available but unselected
            cell.selectedIndicator.image = [UIImage imageNamed:@"radio_unselected"];
            cell.backgroundView = unselBgView;
        } else {
            // timeslot unavailable
            cell.selectedIndicator.image = [UIImage imageNamed:@"radio_unavailable"];
            cell.backgroundView = unselBgView;
        }
        
    } else {
        cell.timeslotLabel.text = @"No appointments to show";
        cell.selectedIndicator.image = [UIImage imageNamed:@"radio_unavailable"];
    }
    
    if (indexPath.row == 0) {
        // first cell
        UIView *topLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 1, cell.contentView.frame.size.width, 1)];
        [topLineView setBackgroundColor:[UIColor colorForHexString:@"f1f1f1"]];
        [cell.contentView addSubview:topLineView];
    } else if (indexPath.row == self.appointments.count-1) {
        // last cell
        UIView *bottomLineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.contentView.frame.size.height-2, cell.contentView.frame.size.width, 1)];
        [bottomLineView setBackgroundColor:[UIColor colorForHexString:@"f1f1f1"]];
        [cell.contentView addSubview:bottomLineView];
    }
    
    return cell;
}

#pragma mark - UITableViewViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *retVal = nil;
    if (self.appointments && self.appointments.count > 0) {
        NSString *timeslot = [self.selectedDay.appointmentSlots objectAtIndex:indexPath.row];
        if ([self.selectedDay hasTimeslot:timeslot]) {
            retVal = indexPath;
        }
    }
    return retVal;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.updatedAppointment = nil;
    NSString *timeslot = [self.selectedDay.appointmentSlots objectAtIndex:indexPath.row];
    if (![self.selectedDay isTimeslotSelected:timeslot] && [self.selectedDay hasTimeslot:timeslot]) {
        self.updatedAppointment = [[BTAppointment alloc] initWithDate:self.selectedDay.date andTimeslot:timeslot];
        self.isDataChanged = YES;
    } else {
        self.isDataChanged = NO;
    }
    [self.timePickerTableView reloadData];
        
}

#pragma mark - BTAmendFooterActionViewDelegate

- (void)userPressedOnBTAmendFooterActionView:(BTAmendFooterActionView *)view{
    
    
    [self trackOmniPage:OMNIPAGE_ORDER_AMEND_APPOINTMENT_HELP];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BTNeedHelpViewController *helpViewController = [storyboard instantiateViewControllerWithIdentifier:@"EngineerAppointmentNeedHelpViewController"];
    _isOpeningNeedHelp = YES;
    [self presentViewController:helpViewController animated:YES completion:nil];
}

#pragma mark - RetryView Delegate

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    [self makeAPIcall];
}

#pragma mark - DLMAmmendEngineerAppointmentScreenDelegate

- (NSDate *)getDateOnlyFromDate:(NSDate *)inputDate{
    
    unsigned int flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:flags fromDate:inputDate];
    return [calendar dateFromComponents:components];
}

- (void)successfullyFetchedUserDataOnAmmendEngineerAppointmentScreen:(DLMAmmendEngineerAppointmentScreen *)appointmentScreen
{
    [self.spinner stopAnimation];
    self.spinner.hidden = YES;
    self.networkRequestInProgress = NO;
    self.isLoading = NO;
    
    
    if (appointmentScreen && appointmentScreen.appointments.count > 0) {
        if (self.appointments.count == 0  ) {
            [self.appointments addObjectsFromArray:appointmentScreen.appointments];
            [self showSelectedAppointment];
        } else {
            [self.appointments addObjectsFromArray:appointmentScreen.appointments];
            [self.dayPickerCollectionView reloadData];
        }

    }
    
    if (self.appointments.count >= kAppointmentMaxSize) {
        self.hasMoreData = NO;
    } else {
        self.hasMoreData = YES;
    }
    
    if(self.hasMoreData){
        
        //Compare
        
        BTAppointment *appointment = [self.appointments lastObject];
        
        NSDate *date1 = [self getDateOnlyFromDate:appointment.date];
        NSDate *date2 = [self getDateOnlyFromDate:self.maxDate];
        
        if(([date1 compare:date2] != NSOrderedAscending)) //
        {
            self.hasMoreData = NO;
        }
    }
    
    
}

- (void)ammendEngineerAppointmentScreen:(DLMAmmendEngineerAppointmentScreen *)appointmentScreen failedToFetchUserDataWithWebServiceError:(NLWebServiceError *)webServiceError
{
    [self.spinner stopAnimation];
    [self.spinner setHidden:YES];
    self.networkRequestInProgress = NO;
    self.isLoading = NO;
    
    if(self.appointments && [self.appointments count] > 0)
    {
        BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
        if(errorHandled == NO)
        {
            [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_ORDER_AMEND_APPOINTMENT];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Message" message:kDefaultErrorMessage preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:^{}];
        }
    }
    else
    {
        //If no data show retry view
        [self handleWebServiceError:webServiceError];
    }
}

#pragma mark - Omniture reporting related methods

- (void)trackOmniPage:(NSString *)page
{
    NSMutableDictionary *contextDict = [NSMutableDictionary dictionary];
    
    NSString *orderRef = [OmnitureManager getOrderRefFormattedString:self.orderReference];
    [contextDict setObject:@"Logged In" forKey:kOmniLoginStatus];
    [contextDict setObject:orderRef forKey:kOmniOrderRef];
    
    [OmnitureManager trackPage:page withContextInfo:contextDict];
}

- (void)trackPageWithKeyTask:(NSString *)keytask
{
    NSMutableDictionary *contextDict = [NSMutableDictionary dictionary];
    NSString *orderRef = [OmnitureManager getOrderRefFormattedString:self.orderReference];
    [contextDict setObject:orderRef forKey:kOmniOrderRef];
    [contextDict setObject:keytask forKey:kOmniKeyTask];
    
    [OmnitureManager trackPage:OMNIPAGE_ORDER_AMEND_APPOINTMENT withContextInfo:contextDict];
}


@end
