//
//  NLGetProjectQuestions.h
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLAPIGEEAuthenticatedWebService.h"

@class NLGetProjectQuestions;
@class NLWebServiceError;

@protocol NLGetProjectQuestionsDelegate <NLAPIGEEAuthenticationProtectedWebServiceDelegate>

- (void)getProjectQuestionsWebService:(NLGetProjectQuestions *)webService finishedWithResponse:(NSObject *)response;

- (void)getProjectQuestionsWebService:(NLGetProjectQuestions *)webService failedWithWebServiceError:(NLWebServiceError *)error;

@optional
- (void)timedOutWithoutSuccessForGetProjectQuestionsWebService:(NLGetProjectQuestions*)webService;

@end

@interface NLGetProjectQuestions : NLAPIGEEAuthenticatedWebService

@property (nonatomic) NSDictionary* projectDetails;

@property (nonatomic, weak) id <NLGetProjectQuestionsDelegate> getProjectQuestionsDelegate;

- (instancetype)initWithProjectRef:(NSString*)projectRef;
- (instancetype)initWithProjectDetails:(NSDictionary*)projectDetails;

@end
