//
//  BTPublicWifiAlertBannerTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 18/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTPublicWifiAlertBannerTableViewCell.h"

@implementation BTPublicWifiAlertBannerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (CGFloat)heightForCell
{
    return 54.0f;
}

@end
