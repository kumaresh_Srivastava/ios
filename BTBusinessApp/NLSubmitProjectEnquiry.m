//
//  NLSubmitProjectEnquiry.m
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLSubmitProjectEnquiry.h"
#import "NSObject+APIResponseCheck.h"
#import "NLWebServiceError.h"
#import "NLConstants.h"
#import "NLWebService+BackEndAPIErrorHandling.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "BTCug.h"
#import "CDCug+CoreDataClass.h"

@implementation NLSubmitProjectEnquiry

- (instancetype)initWithProjectRef:(NSString *)projectRef category:(NSString *)category enquiryDescription:(NSString *)enquiryDescription siteID:(NSInteger)siteID andExistingEnquiryID:(NSInteger)existingEnquiryID
{
    NSString *contactId = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.contactId;
    
    NSMutableDictionary *projectDetails = [NSMutableDictionary new];
    [projectDetails setObject:projectRef forKey:@"projectRef"];
    [projectDetails setObject:@"BTBusinessApp" forKey:@"source"];
    [projectDetails setObject:@"" forKey:@"uniqueKey"];
    [projectDetails setObject:contactId forKey:@"customerContactId"];
    
    self = [self initWithProjectDetails:projectDetails category:category enquiryDescription:enquiryDescription siteID:siteID andExistingEnquiryID:existingEnquiryID];
    
    return self;
}

- (instancetype)initWithProjectDetails:(NSDictionary*)projectDetails category:(NSString*)category enquiryDescription:(NSString*)enquiryDescription siteID:(NSInteger)siteID andExistingEnquiryID:(NSInteger)existingEnquiryID {
    
    NSMutableDictionary *submitProjectEnquiry = [NSMutableDictionary new];
    [submitProjectEnquiry setObject:projectDetails forKey:@"projectDetails"];
    [submitProjectEnquiry setObject:category forKey:@"category"];
    [submitProjectEnquiry setObject:enquiryDescription forKey:@"enquiryDescription"];
    [submitProjectEnquiry setObject:[NSNumber numberWithInteger:siteID] forKey:@"siteID"];
    [submitProjectEnquiry setObject:[NSNumber numberWithInteger:existingEnquiryID] forKey:@"existingEnquiryID"];
    [submitProjectEnquiry setObject:[AppManager NSStringWithOCSFormatFromNSDate:[NSDate date]] forKey:@"submittedDateTime"];
    
    NSError *errorWhileCreatingJSONData = nil;
    
    NSData *parameters = [NSJSONSerialization dataWithJSONObject:submitProjectEnquiry options:0 error:&errorWhileCreatingJSONData];
    
    NSString *jsonString = [[NSString alloc] initWithData:parameters encoding:4];
    
    NSString *endPointURLString = @"/bt-business-auth/v1/ocs-orders/project-enquiry";
    
    self = [super initWithMethod:@"POST" parameters:jsonString andEndpointUrlString:endPointURLString];
    return self;
}

- (NSString *)friendlyName
{
    return @"submitProjectEnquiry";
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    if ([responseObject valueForKey:@"Message"])
    {
        [self.submitProjectEnquiryDelegate submitProjectEnquiryWebService:self finishedWithResponse:responseObject];
    }
    else
    {
        NLWebServiceError *webServiceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponse];
        
        DDLogError(@"Invalid response json object in webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Invalid response json object in webservice with URL: %@", [[task currentRequest] URL]]));
        
        [self.submitProjectEnquiryDelegate submitProjectEnquiryWebService:self failedWithWebServiceError:webServiceError];
    }
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.submitProjectEnquiryDelegate submitProjectEnquiryWebService:self failedWithWebServiceError:webServiceError];
}

@end
