//
//  ServiceStatusDetailTestingViewController.m
//  BTBusinessApp
//
//  Created by VectoScalar on 3/2/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "ServiceStatusDetailTestingViewController.h"
#import "BTServiceStatusDetailsViewController.h"

@interface ServiceStatusDetailTestingViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *serviceIDTextField;
@property (weak, nonatomic) IBOutlet UITextField *serviceTypeTextFiled;

@end

@implementation ServiceStatusDetailTestingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Check service status";
    self.serviceTypeTextFiled.keyboardType = UIKeyboardTypeDefault;
    self.serviceTypeTextFiled.delegate = self;
    self.serviceIDTextField.delegate = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submitButtonPressed:(id)sender {
    
    [self.view endEditing:YES];
    if (self.serviceIDTextField.text.length == 0 || self.serviceTypeTextFiled.text.length == 0 || !([self.serviceTypeTextFiled.text isEqualToString:@"BB"] || [self.serviceTypeTextFiled.text isEqualToString:@"PSTN"]))
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Message" message:@"Entered value is incorrect or missing." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:okAction];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
    }
    
    BTServiceStatusDetailsViewController *serviceStatusDetailsViewController = [BTServiceStatusDetailsViewController getServiceStatusDetailsViewController];
    
    if ([self.serviceTypeTextFiled.text compare:@"BB" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        serviceStatusDetailsViewController.currentServiceType = BTServiceTypeBroadband;
        serviceStatusDetailsViewController.serviceID = self.serviceIDTextField.text;
        serviceStatusDetailsViewController.pointToModelAAlways = YES;
        serviceStatusDetailsViewController.postCode = @"AB10 1HP";
        serviceStatusDetailsViewController.productName = @"Business broadband";
    }
    else if ([self.serviceTypeTextFiled.text compare:@"PSTN" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        serviceStatusDetailsViewController.currentServiceType = BTServiceTypePSTN;
        serviceStatusDetailsViewController.serviceID = self.serviceIDTextField.text;
        serviceStatusDetailsViewController.pointToModelAAlways = YES;
        serviceStatusDetailsViewController.postCode = @"AB10 1HP";
        serviceStatusDetailsViewController.productName = @"Business PSTN service";
    }
    
    [self.navigationController pushViewController:serviceStatusDetailsViewController animated:YES];
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{

    [self.view endEditing:YES];
    return YES;
}


@end
