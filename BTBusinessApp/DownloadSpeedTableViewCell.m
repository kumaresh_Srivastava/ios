#import "DownloadSpeedTableViewCell.h"
#import "OmnitureManager.h"
#import "AppManager.h"

#define kWiderDeviceWidth 375
#define kWideContainerWidth 163

@interface DownloadSpeedTableViewCell()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *actionButtonHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *downloadContainerWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *uploadContainerWidthConstraint;

@end

@implementation DownloadSpeedTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    _downloadContainerView.layer.borderWidth = 1;
    _downloadContainerView.layer.borderColor = [BrandColours colourMyBtLightGrey].CGColor;
    
    _uploadContainerView.layer.borderWidth = 1;
    _uploadContainerView.layer.borderColor = [BrandColours colourMyBtLightGrey].CGColor;
    
    if([[UIScreen mainScreen] bounds].size.width >= kWiderDeviceWidth) {
        _downloadContainerWidthConstraint.constant = kWideContainerWidth;
        _uploadContainerWidthConstraint.constant = kWideContainerWidth;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)setUpCellForContext:(DLMSpeedTestScreen*)speedTestScreen {
    _currentCellType = speedTestScreen.downloadResultType;
    switch(_currentCellType) {
        case MeetsMGALRequirement:
            //_actionButtonHeightConstraint.constant = 0;
            _downloadSpeedLabel.text = [NSString stringWithFormat:@"%.1f", speedTestScreen.downStreamSpeed];
            _uploadSpeedLabel.text = [NSString stringWithFormat:@"%.1f", speedTestScreen.upStreamSpeed];
            _mainDescriptionLabel.text = [NSString stringWithFormat:@"You're getting your guaranteed download speed of %.1fMbps.", speedTestScreen.mGals];
            [_actionButton removeFromSuperview];
            [self trackOmnitureContentViewedTag:OMNIPAGE_SPEED_TEST_CONTENT_DATA_SPEED_MET_GOOD];
            break;
        case DoesntMeetMGALRequirement1stAttempt:
            _downloadSpeedLabel.text = [NSString stringWithFormat:@"%.1f", speedTestScreen.downStreamSpeed];
            _uploadSpeedLabel.text = [NSString stringWithFormat:@"%.1f", speedTestScreen.upStreamSpeed];
            _mainDescriptionLabel.text = [NSString stringWithFormat:@"Sorry, you didn't get your guaranteed download speed of %.1fMbps.", speedTestScreen.mGals];
            [_actionButton setTitle:@"Re-test now" forState:UIControlStateNormal];
            [self trackOmnitureContentViewedTag:OMNIPAGE_SPEED_TEST_CONTENT_DATA_SPEED_NOT_MET_1ST_ATTEMPT];
            break;
        case DoesntMeetMGALRequirement2ndAttempt:
            _downloadSpeedLabel.text = [NSString stringWithFormat:@"%.1f", speedTestScreen.downStreamSpeed];
            _uploadSpeedLabel.text = [NSString stringWithFormat:@"%.1f", speedTestScreen.upStreamSpeed];
            [_actionButton setTitle:@"Report fault" forState:UIControlStateNormal];
            _mainDescriptionLabel.text = [NSString stringWithFormat:@"Sorry, you didn't get your guaranteed download speed of %.1fMbps.", speedTestScreen.mGals];
            [self trackOmnitureContentViewedTag:OMNIPAGE_SPEED_TEST_CONTENT_DATA_SPEED_NOT_MET_2ND_ATTEMPT];
            break;
        case UnknownMGAL:
            _downloadSpeedLabel.text = [NSString stringWithFormat:@"%.1f", speedTestScreen.downStreamSpeed];
            _uploadSpeedLabel.text = [NSString stringWithFormat:@"%.1f", speedTestScreen.upStreamSpeed];
            [_actionButton setTitle:@"Re-test now" forState:UIControlStateNormal];
            _mainDescriptionLabel.text = @"You can check your expected download speed on your order confirmation email.\n\nNot getting the download speed you expected? Try re-testing.";
            [self trackPageForOmniture:OMNIPAGE_SPEED_TEST_RESULTS_UNKNOWN];
            break;
        case NonMGAL1stAttempt:
            _downloadSpeedLabel.text = [NSString stringWithFormat:@"%.1f", speedTestScreen.downStreamSpeed];
            _uploadSpeedLabel.text = [NSString stringWithFormat:@"%.1f", speedTestScreen.upStreamSpeed];
            _mainDescriptionLabel.text = @"You can check your expected download speed on your order confirmation email.\n\nNot getting the download speed you expected? Try re-testing.";
            [_actionButton setTitle:@"Re-test now" forState:UIControlStateNormal];
            [self trackOmnitureContentViewedTag:OMNIPAGE_SPEED_TEST_CONTENT_DATA_1ST_ATTEMPT];
            break;
        case NonMGAL2ndAttempt:
            _downloadSpeedLabel.text = [NSString stringWithFormat:@"%.1f", speedTestScreen.downStreamSpeed];
            _uploadSpeedLabel.text = [NSString stringWithFormat:@"%.1f", speedTestScreen.upStreamSpeed];
            [_actionButton setTitle:@"Report fault" forState:UIControlStateNormal];
            _mainDescriptionLabel.text = @"You can check your expected download speed on your order confirmation email.\n\nStill not getting the download speed you expected? Report a fault to let us know.";
            [self trackOmnitureContentViewedTag:OMNIPAGE_SPEED_TEST_CONTENT_DATA_2ND_ATTEMPT];
            break;
    }
}

- (IBAction)takeActionButtonPressed:(id)sender {
    switch(_currentCellType) {
        case MeetsMGALRequirement:
            [self trackOmniClick:OMNICLICK_SPEED_TEST_NO_PERMISSION_LINK_DONE];
            [_delegate userPressedRetest];
            break;
        case DoesntMeetMGALRequirement1stAttempt:
            [self trackOmniClick:OMNICLICK_SPEED_TEST_RE_TEST_NOW];
            [_delegate userPressedRetest];
            break;
        case DoesntMeetMGALRequirement2ndAttempt:
            [self trackOmniClick:OMNICLICK_SPEED_TEST_REPORT_A_FAULT];
            [_delegate userPressedReportFault];
            break;
        case UnknownMGAL:
            [self trackOmniClick:OMNICLICK_SPEED_TEST_REPORT_A_FAULT];
            [_delegate userPressedReportFault];
            break;
        case NonMGAL1stAttempt:
            [self trackOmniClick:OMNICLICK_SPEED_TEST_RE_TEST_NOW];
            [_delegate userPressedRetest];
            break;
        case NonMGAL2ndAttempt:
            [self trackOmniClick:OMNICLICK_SPEED_TEST_REPORT_A_FAULT];
            [_delegate userPressedReportFault];
            break;
    }
}

#pragma mark - New Omniture Methods

- (NSString*)pageNameForOmnitureTracking {
    switch(_currentCellType) {
        case MeetsMGALRequirement:
            return OMNIPAGE_SPEED_TEST_RESULTS_MGAL_CONTENT_SPEED_MET_GOOD;
        case DoesntMeetMGALRequirement1stAttempt:
            return OMNIPAGE_SPEED_TEST_RESULTS_MGAL_CONTENT_SPEED_NOT_MET_1ST_ATTEMPT;
        case DoesntMeetMGALRequirement2ndAttempt:
            return OMNIPAGE_SPEED_TEST_RESULTS_MGAL_CONTENT_SPEED_NOT_MET_2ND_ATTEMPT;
        case UnknownMGAL:
            return OMNIPAGE_SPEED_TEST_RESULTS_UNKNOWN;
        case NonMGAL1stAttempt:
            return OMNIPAGE_SPEED_TEST_RESULTS_UNKNOWN_CONTENT_SPEED_NOT_MET_1ST_ATTEMPT;
        case NonMGAL2ndAttempt:
            return OMNIPAGE_SPEED_TEST_RESULTS_UNKNOWN_CONTENT_SPEED_NOT_MET_2ND_ATTEMPT;
        default:
            return OMNIPAGE_SPEED_TEST_RESULTS_UNKNOWN;
    }
}

- (void)trackPageForOmniture:(NSString*)pageName
{
    _currentOmniturePage = pageName;
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];

    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackOmniClick:(NSString *)linkTitle
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",[self pageNameForOmnitureTracking],linkTitle] withContextInfo:data];
}

- (void)trackOmnitureContentViewedTag:(NSString *)contentMessage {
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:contentMessage forKey:kOmniContentViewed];
    
    [OmnitureManager trackPage:[self pageNameForOmnitureTracking] withContextInfo:data];
}

@end
