//
//  NLCheckWorkflowStatus.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 16/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLVordelAuthenticationProtectedWebService.h"

@class NLCheckWorkflowStatus;
@class NLWebServiceError;

@protocol NLCheckWorkflowStatusDelegate <NLVordelAuthenticationProtectedWebServiceDelegate>

- (void)checkWorkflowStatusWebService:(NLCheckWorkflowStatus *)webService finishedWithResponse:(NSObject *)response;

- (void)checkWorkflowStatusWebService:(NLCheckWorkflowStatus *)webService failedWithWebServiceError:(NLWebServiceError *)error;

@optional
- (void)timedOutWithoutSuccessForCheckWorkflowStatusWebService:(NLCheckWorkflowStatus*)webService;

@end

@interface NLCheckWorkflowStatus : NLVordelAuthenticationProtectedWebService

@property (nonatomic, readonly) NSString *executionId;
@property (nonatomic, weak) id <NLCheckWorkflowStatusDelegate> checkWorkflowStatusDelegate;

- (instancetype)initWithExecutionId:(NSString *)executionId;

@end
