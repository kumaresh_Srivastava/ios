//
//  BTOCSDetailsHeaderTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSDetailsHeaderTableViewCell.h"

@implementation BTOCSDetailsHeaderTableViewCell

+ (NSString *)reuseId
{
    return @"BTOCSDetailsHeaderTableViewCell";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.prgressLabel.layer.borderWidth = 1.0f;
    self.prgressLabel.layer.cornerRadius = 2.0f;
    [self.prgressLabel setLayoutMargins:UIEdgeInsetsMake(0, 5, 0, 5)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateWithSubStatus:(NSString *)status
{
    [self.prgressLabel setOrderStatus:status];
//    if ([status.lowercaseString isEqualToString:@"in progress"]) {
//        [self updateWithProgress:status andProgressColour:[BrandColours colourMyBtGreen]];
//    } else if ([status.lowercaseString isEqualToString:@"delayed"]) {
//        [self updateWithProgress:status andProgressColour:[BrandColours colourMyBtRed]];
//    } else {
//        [self updateWithProgress:status andProgressColour:[BrandColours colourMyBtOrange]];
//    }
}

- (void)updateWithProgress:(NSString *)progress andProgressColour:(UIColor *)color
{
    self.prgressLabel.text = progress;
    self.prgressLabel.textColor = color;
    self.prgressLabel.layer.borderColor = color.CGColor;
}

- (void)updateWithProduct:(NSString *)product
{
    NSString *baseString = [NSString stringWithFormat:@"Product: %@",product];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:baseString attributes:@{NSFontAttributeName: [UIFont fontWithName:kBtFontRegular size: 14.0f],NSForegroundColorAttributeName: [UIColor blackColor]}];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:kBtFontBold size: 14.0f] range:NSMakeRange(0, 8)];
    self.productLabel.attributedText = attributedString;
}

@end
