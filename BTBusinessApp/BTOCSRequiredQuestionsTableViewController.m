//
//  BTOCSRequiredQuestionsTableViewController.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 13/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSRequiredQuestionsTableViewController.h"

#import "BTOCSOrderDetailsTableViewController.h"

#import "NLGetProjectQuestions.h"
#import "NLSubmitProjectQuestions.h"

#import "BTOCSQuestionGroup.h"
#import "BTOCSQuestion.h"
#import "BTOCSQuestionResponseOptions.h"
#import "NLWebServiceError.h"


@interface BTOCSRequiredQuestionsTableViewController () <NLGetProjectQuestionsDelegate,NLSubmitProjectQuestionsDelegate,BTOCSRadioSelectTableViewCellDelegate,BTOCSDropDownTableViewCellDelegate,BTOCSDateSelectTableViewCellDelegate,BTOCSDateTimeSelectTableViewCellDelegate,BTOCSFileSelectTableViewCellDelegate,BTOCSLargeTextEntryTableViewCellDelegate>

@property (nonatomic, strong) NLGetProjectQuestions *getProjectQuestionsWebService;
@property (nonatomic, strong) NLSubmitProjectQuestions *submitProjectQuestionsWebService;

@property (strong, nonatomic) BTOCSProjectInfoHeaderTableViewCell *headerCell;
@property (strong, nonatomic) BTOCSRequiredQuestionsPromptTableViewCell *promptCell;

@property (strong, nonatomic) BTOCSSimpleButtonTableViewCell *saveForLaterCell;
@property (strong, nonatomic) BTOCSSimpleButtonTableViewCell *submitCell;

@property (strong, nonatomic) NSMutableArray<BTBaseTableViewCell*> *questionCells;

@end

@implementation BTOCSRequiredQuestionsTableViewController

+ (BTOCSRequiredQuestionsTableViewController *)getOCSProjectQuestionsViewController
{
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"BTOCSOrders" bundle:nil];
    //BTOCSRequiredQuestionsTableViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTOCSRequiredQuestionsTableViewController"];
    BTOCSRequiredQuestionsTableViewController *vc = [[BTOCSRequiredQuestionsTableViewController alloc] initWithStyle:UITableViewStylePlain];
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.title = @"Required questions";
    [self getProjectQuestions];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupTableCells
{
    [super setupTableCells];
    if (!self.headerCell) {
        self.headerCell = (BTOCSProjectInfoHeaderTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSProjectInfoHeaderTableViewCell reuseId]];
        [self.headerCell updateWithPorjectRef:self.project.projectRef andProduct:self.project.productName andSite:self.selectedSite.name];
        [self.tableCells addObject:self.headerCell];
    }
    if (!self.promptCell) {
        self.promptCell = (BTOCSRequiredQuestionsPromptTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSRequiredQuestionsPromptTableViewCell reuseId]];
        [self.tableCells addObject:self.promptCell];
    }
}

- (void)updateTableCells
{
    [super updateTableCells];
    [self updateQuestionCells];
    [self.tableCells addObjectsFromArray:self.questionCells];
}

- (void)updateQuestionCells
{
    if (!self.questionCells) {
        self.questionCells = [NSMutableArray new];
    } else {
        [self.tableCells removeObjectsInArray:self.questionCells];
        [self.questionCells removeAllObjects];
    }
    NSUInteger questionIdx = 1;
    if (self.project.questionGroups) {
        for (BTOCSQuestionGroup *group in self.project.questionGroups) {
            if (group.questions) {
                for (BTOCSQuestion *question in group.questions) {
                    BTOCSSectionHeaderTableViewCell *questionHeader = (BTOCSSectionHeaderTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSSectionHeaderTableViewCell reuseId]];
                    [questionHeader.headerTextLabel setText:[NSString stringWithFormat:@"Question %lu",(unsigned long)questionIdx++]];
                    [self.questionCells addObject:questionHeader];
                    BTBaseTableViewCell *qCell = [self cellForQuestion:question];
                    if (qCell) {
                        [self.questionCells addObject:qCell];
                    }
                }
            }
        }
    }
    
    if (!self.saveForLaterCell) {
        self.saveForLaterCell = (BTOCSSimpleButtonTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSSimpleButtonTableViewCell reuseId]];
        self.saveForLaterCell.button.backgroundColor = [UIColor clearColor];
        [self.saveForLaterCell.button setTitle:@"Save for later" forState:UIControlStateNormal];
        [self.saveForLaterCell.button setTitleColor:[BrandColours colourBackgroundBTPurplePrimaryColor] forState:UIControlStateNormal];
        [self.saveForLaterCell.button addTarget:self action:@selector(saveForLaterAction) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (!self.submitCell) {
        self.submitCell = (BTOCSSimpleButtonTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSSimpleButtonTableViewCell reuseId]];
        [self.submitCell.button setTitle:@"Submit" forState:UIControlStateNormal];
        [self.submitCell.button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.submitCell.button.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
        
        [self.submitCell.button addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [self.questionCells addObject:self.saveForLaterCell];
    [self.questionCells addObject:self.submitCell];
    
}

- (BTBaseTableViewCell*)cellForQuestion:(BTOCSQuestion*)question
{
    BTBaseTableViewCell *cell = nil;
    
    // radio select
    if ([question.questionDataType.lowercaseString isEqualToString:@"radio"]) {
        BTOCSRadioSelectTableViewCell *radioCell = (BTOCSRadioSelectTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSRadioSelectTableViewCell reuseId]];
        NSMutableArray *optionsArray = [NSMutableArray new];
        for (BTOCSQuestionResponseOptions *option in question.questionResponseOptions) {
            [optionsArray addObject:option.optionText];
        }
        NSString *response = nil;
        if (question.questionResponse) {
            response = question.questionResponse;
        }
        [radioCell updateWithOptions:optionsArray andSelectedOption:response];
        NSString *questionText = question.questionName;
        if (question.questionDescription && ![question.questionDescription isEqualToString:@""]) {
            questionText = [questionText stringByAppendingString:[NSString stringWithFormat:@"\n%@",question.questionDescription]];
        }
        radioCell.descriptionLabel.text = questionText;
        radioCell.selectionCollectionView.userInteractionEnabled = !question.questionReadOnlyFlag;
        radioCell.delegate = self;
        cell = radioCell;
    }
    
    // dropdown
    else if ([question.questionDataType.lowercaseString isEqualToString:@"dropdown"]) {
        BTOCSDropDownTableViewCell *dropdownCell = (BTOCSDropDownTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSDropDownTableViewCell reuseId]];
        NSMutableArray *optionsArray = [NSMutableArray new];
        for (BTOCSQuestionResponseOptions *option in question.questionResponseOptions) {
            [optionsArray addObject:option.optionText];
        }
        
        NSString *questionText = question.questionName;
        if (question.questionDescription && ![question.questionDescription isEqualToString:@""]) {
            questionText = [questionText stringByAppendingString:[NSString stringWithFormat:@"\n%@",question.questionDescription]];
        }
        
    
        if (question.questionResponse) {
        [dropdownCell updateWithTitle:questionText values:optionsArray selectedValue:question.questionResponse];
    } else {
        [dropdownCell updateWithTitle:questionText values:optionsArray selectedValue:nil andPlaceHolder:@"Please select"];
    }
   
        [dropdownCell.titleLabel setFont:[UIFont fontWithName:kBtFontRegular size:16.0f]];
        dropdownCell.delegate = self;
        cell = dropdownCell;
    }
    
    // date picker
    else if ([question.questionDataType.lowercaseString isEqualToString:@"date"]) {
        BTOCSDateSelectTableViewCell *dateCell = (BTOCSDateSelectTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSDateSelectTableViewCell reuseId]];
        
        NSString *questionText = question.questionName;
        if (question.questionDescription && ![question.questionDescription isEqualToString:@""]) {
            questionText = [questionText stringByAppendingString:[NSString stringWithFormat:@"\n%@",question.questionDescription]];
        }
        
        
        [dateCell.questionLabel setText:questionText];
        //[dateCell.titleLabel setFont:[UIFont fontWithName:kBtFontRegular size:16.0f]];
        
        if (question.questionResponse) {
            NSDate *theDate = [AppManager dateFromString:question.questionResponse];
            dateCell.detailLabel.text = [AppManager NSStringFromNSDateWithSlashFormat:theDate];
            dateCell.dateSelected = theDate;
        }
        
        dateCell.delegate = self;
        cell = dateCell;
    }
    
    // date time picker
    else if ([question.questionDataType.lowercaseString isEqualToString:@"datetime"]) {
        BTOCSDateTimeSelectTableViewCell *dateTimeCell = (BTOCSDateTimeSelectTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSDateTimeSelectTableViewCell reuseId]];
        
        NSString *questionText = question.questionName;
        if (question.questionDescription && ![question.questionDescription isEqualToString:@""]) {
            questionText = [questionText stringByAppendingString:[NSString stringWithFormat:@"\n%@",question.questionDescription]];
        }
        
        
        [dateTimeCell.questionLabel setText:questionText];
        //[dateCell.titleLabel setFont:[UIFont fontWithName:kBtFontRegular size:16.0f]];
        
        if (question.questionResponse) {
            NSDate *theDate = [AppManager dateFromString:question.questionResponse];
            dateTimeCell.selectedDateLabel.text = [AppManager NSStringFromNSDateWithSlashFormat:theDate];
            dateTimeCell.timeField.text = [AppManager getTimeFromDate:theDate];
            dateTimeCell.selectedDateTime = theDate;
        }
        
        dateTimeCell.delegate = self;
        cell = dateTimeCell;
    }
    
    // text
    else if ([question.questionDataType.lowercaseString isEqualToString:@"textbox"]) {
        BTOCSLargeTextEntryTableViewCell *textCell = (BTOCSLargeTextEntryTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSLargeTextEntryTableViewCell reuseId]];
        textCell.hidesToolbar = YES;
        NSString *questionText = question.questionName;
        if (question.questionDescription && ![question.questionDescription isEqualToString:@""]) {
            questionText = [questionText stringByAppendingString:[NSString stringWithFormat:@"\n%@",question.questionDescription]];
        }
        textCell.maxLength = 1000;
        textCell.questionLabel.text = questionText;
        textCell.delegate = self;
        if (question.questionResponse) {
            textCell.entryTextView.text = question.questionResponse;
        }
        cell = textCell;
    }
    
    // file
    else if ([question.questionDataType.lowercaseString isEqualToString:@"file"]) {
        BTOCSFileSelectTableViewCell *fileCell = (BTOCSFileSelectTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSFileSelectTableViewCell reuseId]];
        
        NSString *questionText = question.questionName;
        if (question.questionDescription && ![question.questionDescription isEqualToString:@""]) {
            questionText = [questionText stringByAppendingString:[NSString stringWithFormat:@"\n%@",question.questionDescription]];
        }
        
        fileCell.questionLabel.text = questionText;
        fileCell.delegate = self;
        if (question.questionResponse) {
            NSData *fileData = [[NSData alloc] initWithBase64EncodedString:question.questionResponse options:NSDataBase64DecodingIgnoreUnknownCharacters];
            NSString *filename = @"file";
            if (question.questionResponseOptions && question.questionResponseOptions.count>0) {
                BTOCSQuestionResponseOptions *option = question.questionResponseOptions[0];
                filename = option.optionText;
            }
            [fileCell updateWithFilename:filename andFileData:fileData];
        } else {
            [fileCell updateWithNoFile];
        }
        cell = fileCell;
    }
    
    cell.tag = question.questionID;
    
    return cell;
}

#pragma mark - button actions

- (void)saveForLaterAction {
    [self trackOmniClick:self.saveForLaterCell.button.titleLabel.text];
    [self submitProjectQuestions];
}

- (void)submitAction {
    [self trackOmniClick:self.submitCell.button.titleLabel.text];
    if ([self validateQuestionResponses]) {
        [self submitProjectQuestions];
    }
}

#pragma mark - radio cell delegate

- (void)radioSelectCell:(BTOCSRadioSelectTableViewCell *)cell updatedSelection:(NSString *)selected
{
    //
}

#pragma mark - drop down delegate

- (void)cell:(BTOCSDropDownTableViewCell *)cell updatedSelection:(NSString *)selected
{
    //
}

#pragma mark - file cell delegate

- (void)layoutChangeInCell:(BTBaseTableViewCell *)cell
{
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
}

#pragma mark - large text entry delegate

- (void)cell:(BTOCSLargeTextEntryTableViewCell *)cell updatedTextEntry:(NSString *)text
{
    if ([self.tableCells containsObject:cell]) {
        NSInteger idxOfCell = [self.tableCells indexOfObject:cell];
        [self.tableView beginUpdates];
        [self.tableView endUpdates];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:idxOfCell inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
}

#pragma mark - api call

- (void)getProjectQuestions
{
    if (self.getProjectQuestionsWebService) {
        [self.getProjectQuestionsWebService cancel];
        self.getProjectQuestionsWebService.getProjectQuestionsDelegate = nil;
        self.getProjectQuestionsWebService = nil;
    }
    
    if ([AppManager isInternetConnectionAvailable]) {
        if (self.project && self.project.projectRef) {
            self.getProjectQuestionsWebService = [[NLGetProjectQuestions alloc] initWithProjectRef:self.project.projectRef];
            self.getProjectQuestionsWebService.getProjectQuestionsDelegate = self;
            if (!self.networkRequestInProgress) {
                self.networkRequestInProgress = YES;
            }
            [self.getProjectQuestionsWebService resume];
        }
    } else {
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:self.pageNameForOmnitureTracking];
    }
    
}

- (void)submitProjectQuestions
{
    if (self.submitProjectQuestionsWebService) {
        [self.submitProjectQuestionsWebService cancel];
        self.submitProjectQuestionsWebService.submitProjectQuestionsDelegate = nil;
        self.submitProjectQuestionsWebService = nil;
    }
    
    if (self.project && self.project.projectRef && self.questionCells && self.questionCells.count>0) {
        NSArray *responses = [self getQuestionResponses];
        if (responses && responses.count>0) {
            NSInteger activityID = self.project.questionGroups[0].questionGroupActivityID;
            NSInteger groupID = self.project.questionGroups[0].questionGroupID;
            
            self.submitProjectQuestionsWebService = [[NLSubmitProjectQuestions alloc] initWithProjectRef:self.project.projectRef questionGroupActivitiyID:activityID questionGroupID:groupID questionResponses:responses];
            
            self.submitProjectQuestionsWebService.submitProjectQuestionsDelegate = self;
            if (!self.networkRequestInProgress) {
                self.networkRequestInProgress = YES;
            }
            [self.submitProjectQuestionsWebService resume];
        }
    }
}

- (NSArray*)getQuestionResponses
{
    NSString *questionIdKey = @"questionId";
    NSString *responseValueKey = @"responseValue";
    NSString *responseValueExtraKey = @"responseValueExtra";
    
    NSMutableArray *responses = [NSMutableArray new];
    // grab the answers from the relevant cells
    for (BTBaseTableViewCell *cell in self.questionCells) {
        // radio cell
        if ([cell isKindOfClass:[BTOCSRadioSelectTableViewCell class]]) {
            BTOCSRadioSelectTableViewCell *radioCell = (BTOCSRadioSelectTableViewCell*)cell;
            NSMutableDictionary *responseDict = [NSMutableDictionary new];
            [responseDict setValue:[NSNumber numberWithInteger: cell.tag] forKey:questionIdKey];
            if (radioCell.selectedOption) {
                [responseDict setObject:radioCell.selectedOption forKey:responseValueKey];
            } else {
                [responseDict setObject:@"" forKey:responseValueKey];
            }
            
            [responseDict setObject:@"" forKey:responseValueExtraKey];
            
            [responses addObject:responseDict];
        }
        
        // drop down cell
        else if ([cell isKindOfClass:[BTOCSDropDownTableViewCell class]]) {
            BTOCSDropDownTableViewCell *ddCell = (BTOCSDropDownTableViewCell*)cell;
            NSMutableDictionary *responseDict = [NSMutableDictionary new];
            [responseDict setValue:[NSNumber numberWithInteger:cell.tag] forKey:questionIdKey];
            if (ddCell.selectedValue) {
                [responseDict setObject:ddCell.selectedValue forKey:responseValueKey];
            } else {
                [responseDict setObject:@"" forKey:responseValueKey];
            }
            [responseDict setObject:@"" forKey:responseValueExtraKey];
            
            [responses addObject:responseDict];
        }
        
        // text cell
        else if ([cell isKindOfClass:[BTOCSLargeTextEntryTableViewCell class]]) {
            BTOCSLargeTextEntryTableViewCell *textCell = (BTOCSLargeTextEntryTableViewCell*)cell;
            NSMutableDictionary *responseDict = [NSMutableDictionary new];
            [responseDict setValue:[NSNumber numberWithInteger:cell.tag] forKey:questionIdKey];
            if (textCell.entryTextView.text) {
                [responseDict setObject:textCell.entryTextView.text forKey:responseValueKey];
            } else {
                [responseDict setObject:@"" forKey:responseValueKey];
            }
            [responseDict setObject:@"" forKey:responseValueExtraKey];
            
            [responses addObject:responseDict];
        }
        
        // date cell
        else if ([cell isKindOfClass:[BTOCSDateSelectTableViewCell class]]) {
            BTOCSDateSelectTableViewCell *dateCell = (BTOCSDateSelectTableViewCell*)cell;
            NSMutableDictionary *responseDict = [NSMutableDictionary new];
            [responseDict setValue:[NSNumber numberWithInteger: cell.tag] forKey:questionIdKey];
            if (dateCell.dateSelected) {
                NSString *dateString = [AppManager NSStringFromNSDateWithSlashFormat:dateCell.dateSelected];
                [responseDict setObject:dateString forKey:responseValueKey];
            } else {
                [responseDict setObject:@"" forKey:responseValueKey];
            }
            [responseDict setObject:@"" forKey:responseValueExtraKey];
            
            [responses addObject:responseDict];
        }
        
        // datetime cell
        else if ([cell isKindOfClass:[BTOCSDateTimeSelectTableViewCell class]]) {
            BTOCSDateTimeSelectTableViewCell *dateTimeCell = (BTOCSDateTimeSelectTableViewCell*)cell;
            NSMutableDictionary *responseDict = [NSMutableDictionary new];
            [responseDict setValue:[NSNumber numberWithInteger:cell.tag] forKey:questionIdKey];
            if (dateTimeCell.selectedDateTime) {
                NSString *dateTimeString = [AppManager NSStringWithOCSFormatFromNSDate:dateTimeCell.selectedDateTime];
                [responseDict setObject:dateTimeString forKey:responseValueKey];
            } else {
                [responseDict setObject:@"" forKey:responseValueKey];
            }
            [responseDict setObject:@"" forKey:responseValueExtraKey];
            
            [responses addObject:responseDict];
        }
        
        // file cell
        else if ([cell isKindOfClass:[BTOCSFileSelectTableViewCell class]]) {
            BTOCSFileSelectTableViewCell *fileCell = (BTOCSFileSelectTableViewCell*)cell;
            NSMutableDictionary *responseDict = [NSMutableDictionary new];
            [responseDict setValue:[NSNumber numberWithInteger:cell.tag] forKey:questionIdKey];
            NSString *fileString = @"";
            NSString *fileName = @"";
            if (fileCell.fileData && fileCell.fileData.length > 0) {
            
                fileString = [fileCell.fileData base64EncodedStringWithOptions:0];
                fileName = fileCell.fileName;
            }
            [responseDict setObject:fileString forKey:responseValueKey];
            [responseDict setObject:fileName forKey:responseValueExtraKey];
            
            [responses addObject:responseDict];
        }
    }
    return responses;
}

- (BOOL)validateQuestionResponses
{
    BOOL valid = YES;
    NSMutableArray<BTBaseTableViewCell*> *invalidCells = [NSMutableArray new];
    // check the answers from the relevant cells
    for (BTBaseTableViewCell *cell in self.questionCells) {
        // radio cell
        if (valid && [cell isKindOfClass:[BTOCSRadioSelectTableViewCell class]]) {
            BTOCSRadioSelectTableViewCell *radioCell = (BTOCSRadioSelectTableViewCell*)cell;
            if ((!radioCell.selectedOption || [radioCell.selectedOption isEqualToString:@""]) && radioCell.options.count>0) {
                valid = NO;
                [invalidCells addObject:cell];
            }
        }
        
        // drop down cell
        else if (valid && [cell isKindOfClass:[BTOCSDropDownTableViewCell class]]) {
            BTOCSDropDownTableViewCell *ddCell = (BTOCSDropDownTableViewCell*)cell;
            if ((!ddCell.selectedValue || [ddCell.selectedValue isEqualToString:@""]) && ddCell.values.count>0) {
                valid = NO;
                [invalidCells addObject:cell];
            }
        }
        
        // text cell
        else if (valid && [cell isKindOfClass:[BTOCSLargeTextEntryTableViewCell class]]) {
            BTOCSLargeTextEntryTableViewCell *textCell = (BTOCSLargeTextEntryTableViewCell*)cell;
            if (!textCell.entryTextView.text || [textCell.entryTextView.text isEqualToString:@""]) {
                valid = NO;
                [invalidCells addObject:cell];
            }
        }
        
        // date cell
        else if (valid && [cell isKindOfClass:[BTOCSDateSelectTableViewCell class]]) {
            BTOCSDateSelectTableViewCell *dateCell = (BTOCSDateSelectTableViewCell*)cell;
            if (!dateCell.dateSelected) {
                valid = NO;
                [invalidCells addObject:cell];
            }
        }
        
        // datetime cell
        else if (valid && [cell isKindOfClass:[BTOCSDateTimeSelectTableViewCell class]]) {
            BTOCSDateTimeSelectTableViewCell *dateTimeCell = (BTOCSDateTimeSelectTableViewCell*)cell;
            
            if (!dateTimeCell.selectedDateTime) {
                valid = NO;
                [invalidCells addObject:cell];
            }
        }
        
        // file cell
        else if (valid && [cell isKindOfClass:[BTOCSFileSelectTableViewCell class]]) {
            BTOCSFileSelectTableViewCell *fileCell = (BTOCSFileSelectTableViewCell*)cell;
            if (!fileCell.fileData || fileCell.fileData.length == 0) {
                valid = NO;
                [invalidCells addObject:cell];            }
        }
    }
    
    // if there's an invalid cell scroll to it
    if (invalidCells.count > 0) {
        NSUInteger idx = [self.tableCells indexOfObject:[invalidCells firstObject]];
        if (idx > 0 && [[self.tableCells objectAtIndex:(idx-1)] isKindOfClass:[BTOCSSectionHeaderTableViewCell class]]) {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:(idx-1) inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        } else {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
    }
    
    return valid;
}

#pragma mark - getProjectQuestions delegate

- (void)getProjectQuestionsWebService:(NLGetProjectQuestions *)webService finishedWithResponse:(NSObject *)response
{
    // success
    self.networkRequestInProgress = NO;
    [self.project updateWithProjectQuestionsResponse:(NSDictionary*)response];
    [self refreshTableView];
}

- (void)getProjectQuestionsWebService:(NLGetProjectQuestions *)webService failedWithWebServiceError:(NLWebServiceError *)error
{
    // failure
    self.networkRequestInProgress = NO;
    [AppManager trackWebServiceError:error FromAPI:webService.friendlyName OnPage:self.pageNameForOmnitureTracking];
    BOOL errorHandled = [self attemptVordelProtectedAPIErrorHandlingOfWebServiceError:error];
    if (!errorHandled) {
        [self showRetryViewWithInternetStrip:NO];
    }
}

#pragma mark - retry view delegate

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    [self getProjectQuestions];
}

#pragma mark - submitProjectQuestions delegate

- (void)submitProjectQuestionsWebService:(NLSubmitProjectQuestions *)webService finishedWithResponse:(NSObject *)response
{
    self.networkRequestInProgress = NO;
   
    UIAlertController *successController = [UIAlertController alertControllerWithTitle:@"Info" message:@"Your's answers has been submitted successfully." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
        for (UIViewController *vc in self.navigationController.viewControllers) {
            if ([vc isKindOfClass:[BTOCSOrderDetailsTableViewController class]]) {
                [(BTOCSOrderDetailsTableViewController*)vc getProjectSummary];
            }
        }
    }];
    
    [successController addAction:okAction];
    
    [self presentViewController:successController animated:YES completion:^{
    }];
}

- (void)submitProjectQuestionsWebService:(NLSubmitProjectQuestions *)webService failedWithWebServiceError:(NLWebServiceError *)error
{
    self.networkRequestInProgress = NO;
    
    UIAlertController *errorController = [UIAlertController alertControllerWithTitle:@"Info" message:error.error.description preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
       
    }];

    [errorController addAction:okAction];
    
    [self presentViewController:errorController animated:YES completion:^{
    }];
    
    [AppManager trackWebServiceError:error FromAPI:webService.friendlyName OnPage:self.pageNameForOmnitureTracking];
}

#pragma mark - omniture

- (NSString *)pageNameForOmnitureTracking
{
    return OMNIPAGE_OCS_ORDER_REQUIREDQUESTIONS;
}

- (void)trackPageForOmniture
{
    if (self.project.productService && self.project.subStatus) {
        [self trackPageForOmnitureWithProductService:self.project.productService andOrderStatus:self.project.subStatus];
    } else {
        [super trackPageForOmniture];
    }
}

@end
