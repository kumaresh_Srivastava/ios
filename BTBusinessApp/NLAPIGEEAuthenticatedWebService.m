//
//  NLAPIGEEAuthenticatedWebService.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 19/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLAPIGEEAuthenticatedWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "AFNetworking.h"
#import "NLConstants.h"
#import "BTAuthenticationToken.h"
#import "BTAuthenticationManager.h"
#import "NLWebServiceError.h"
#import "NLAPIGEEWebServiceError.h"
#import "AppConstants.h"

unsigned char apigeeClientIdDebug[] =  {
    0x65, 0x62, 0x35, 0x31,
    0x31, 0x38, 0x66, 0x63,
    0x2d, 0x31, 0x65, 0x32,
    0x39, 0x2d, 0x34, 0x39,
    0x39, 0x32, 0x2d, 0x39,
    0x34, 0x63, 0x63, 0x2d,
    0x31, 0x36, 0x35, 0x37,
    0x38, 0x39, 0x33, 0x35,
    0x34, 0x35, 0x39, 0x61
};


unsigned char apigeeClientSecretDebug[] =  {
    0x61, 0x34, 0x35, 0x34,
    0x61, 0x64, 0x31, 0x37,
    0x2d, 0x61, 0x31, 0x64,
    0x34, 0x2d, 0x34, 0x37,
    0x38, 0x61, 0x2d, 0x62,
    0x30, 0x66, 0x38, 0x2d,
    0x33, 0x39, 0x66, 0x31,
    0x35, 0x61, 0x63, 0x35,
    0x63, 0x34, 0x37, 0x65
};


unsigned char apigeeClientIdLive[] =  {
    0x65, 0x34, 0x33, 0x38,
    0x37, 0x65, 0x63, 0x30,
    0x2d, 0x34, 0x66, 0x39,
    0x35, 0x2d, 0x34, 0x33,
    0x39, 0x30, 0x2d, 0x61,
    0x35, 0x36, 0x36, 0x2d,
    0x63, 0x35, 0x65, 0x65,
    0x39, 0x39, 0x30, 0x62,
    0x63, 0x64, 0x38, 0x33
};


unsigned char apigeeClientSecretLive[] =  {
    0x38, 0x34, 0x64, 0x66,
    0x34, 0x38, 0x33, 0x66,
    0x2d, 0x63, 0x36, 0x34,
    0x61, 0x2d, 0x34, 0x36,
    0x64, 0x37, 0x2d, 0x61,
    0x30, 0x61, 0x31, 0x2d,
    0x64, 0x35, 0x37, 0x39,
    0x64, 0x38, 0x36, 0x34,
    0x64, 0x39, 0x36, 0x33
};

@interface NLAPIGEEAuthenticatedWebService () {
    
    BOOL _hasAutoAuthenticationBeenAlreadyAttempted;
    NLWebServiceError *_webServiceErrorBeforeAutoAuthenticationAttempt;
}

@end

@implementation NLAPIGEEAuthenticatedWebService

- (instancetype)initWithMethod:(NSString *)method parameters:(NSString *)parameters andEndpointUrlString:(NSString *)endpointUrlString
{
    self = [super initWithMethod:method parameters:parameters andEndpointUrlString:endpointUrlString];
    
    if(self)
    {
        _authenticationToken = [[BTAuthenticationManager sharedManager] authenticationTokenForCurrentLoggedInUser];
    }
    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (AFHTTPSessionManager *)sessionManager
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSString *baseURLString = [NSString stringWithFormat:@"https://%@", [AppManager baseURLStringFromBaseURLType:_baseURLType]];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseURLString] sessionConfiguration:configuration];
    
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    [manager.requestSerializer setTimeoutInterval:_timeOutTimeInterval];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // (hd) Because the API call expects the parameters in raw format, hence we are setting an empty query string serializer so that it does not do any serialization.
    [manager.requestSerializer setQueryStringSerializationWithBlock:^NSString * _Nonnull(NSURLRequest * _Nonnull request, id  _Nonnull parameters, NSError * _Nullable __autoreleasing * _Nullable error) {

        return parameters;
    }];
    
    AFJSONResponseSerializer *jsonSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:(NSJSONReadingMutableLeaves|NSJSONReadingMutableContainers)];
    AFJSONResponseSerializer *fallbackJsonSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:(NSJSONReadingMutableLeaves|NSJSONReadingMutableContainers)];
    fallbackJsonSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", nil];
    AFHTTPResponseSerializer *fallbackDefaultSerializer = [AFHTTPResponseSerializer serializer];
    AFCompoundResponseSerializer *serializer = [AFCompoundResponseSerializer compoundSerializerWithResponseSerializers:@[jsonSerializer,fallbackJsonSerializer,fallbackDefaultSerializer]];
    manager.responseSerializer = serializer;
    
    return manager;
}


- (NSDictionary *)httpHeaderFields
{
    NSString *appID = @"BTBusinessApp";
    NSString *appUDID = [AppDelegate sharedInstance].viewModel.app.btInstallationID;
    NSString *requestID = [NSString stringWithFormat:@"%@-%@", appID, appUDID];
    NSString *authToken = [NSString stringWithFormat:@"Bearer %@", _authenticationToken.accessToken];
    BTSMSession *smsession = [[BTAuthenticationManager sharedManager] smsessionForCurrentLoggedInUser];
    
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *httpHeaders = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    [httpHeaders setObject:authToken forKey:@"Authorization"];
    [httpHeaders setObject:appUDID forKey:@"UDID"];
    [httpHeaders setObject:smsession.smsessionString forKey:@"SMSession"];
    [httpHeaders setObject:@"application/json, text/json, text/javascript" forKey:@"Accept"];
    //[httpHeaders setObject:requestID forKey:@"X-EE-EL-Tracking-Header"];
    //[httpHeaders setObject:appID forKey:@"X-EE-API-Originator"];
    
    NSString *clientID = nil;
    NSString *clientSecret = nil;
    
    if ([kBTServerType isEqualToString:@"BTServerTypeModelA"])
    {
        clientID = [[NSString alloc] initWithBytes:apigeeClientIdDebug length:36 encoding:NSASCIIStringEncoding];
        clientSecret = [[NSString alloc] initWithBytes:apigeeClientSecretDebug length:36 encoding:NSASCIIStringEncoding];
        //clientID = @"8ba289e5-1cf4-456c-8fd1-9f2cca7bbd4b";
        //clientSecret = @"790af81d-ad13-4d72-abff-fac5d343a3b9";
    }
    else
    {
        clientID = [[NSString alloc] initWithBytes:apigeeClientIdLive length:36 encoding:NSASCIIStringEncoding];
        clientSecret = [[NSString alloc] initWithBytes:apigeeClientSecretLive length:36 encoding:NSASCIIStringEncoding];
    }
    
    [httpHeaders setObject:requestID forKey:@"APIGW-Tracking-Header"];
    [httpHeaders setObject:clientID forKey:@"APIGW-Client-Id"];
    //[httpHeaders setObject:@"eb5118fc-1e29-4992-94cc-16578935459a" forKey:@"APIGW-Client-Id"];
    
    
    return [httpHeaders copy];
}

#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andError:(NSError *)error
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andError:error];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        
        
    }
    
    return errorToBeReturned;
}

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        NSDictionary *returnStatusInfo = [responseObject objectForKey:@"ReturnStatusInfo"];
        if (returnStatusInfo) {
            NSString *statusValue = [returnStatusInfo objectForKey:@"StatusValue"];
            NSString *statusInfo = [returnStatusInfo objectForKey:@"StatusInfo"];
            if (statusValue && ([statusValue integerValue] != 0)) {
                errorToBeReturned = [NLAPIGEEWebServiceError apigeeWebServiceNetworkErrorWithErrorCode:statusValue andDescription:statusInfo];
            }
        }
    }
    
    return errorToBeReturned;
}

- (BOOL)attemptErrorHandlingIfNeededForError:(NLWebServiceError *)webServiceError
{
    __block BOOL errorHandled = [super attemptErrorHandlingIfNeededForError:webServiceError];
    
    if(errorHandled == NO)
    {
        // (hds) If there was no error handling from superclass, then this class can check if it needs to handle the error or not.
        
        // (hds) Handling the Token Invalid and Refresh Token Invalid Error Cases and taking action on it.
        
        if(webServiceError.error.domain == BTNetworkErrorDomain && (webServiceError.error.code == BTNetworkErrorCodeVordelTokenInvalidOrExpired || webServiceError.error.code == BTNetworkErrorCodeVordelRefreshInvalidOrExpired))
        {
            if(_hasAutoAuthenticationBeenAlreadyAttempted == NO)
            {
                errorHandled = YES;
                
                [[BTAuthenticationManager sharedManager] reAuthenticateCurrentlyLoggedInUserWithPreviousToken:_authenticationToken withAlreadyHaveFreshTokenHandler:^(BTAuthenticationToken *token) {
                    
                    DDLogInfo(@"Handling the Vordel Authentication error by again attempting the API Call by using recently fetched new Vordel OAuth Token.");
                    
                    _authenticationToken = token;
                    [self resume];
                    
                } andFetchFreshTokenHandler:^{
                    
                    DDLogInfo(@"Handling the Vordel Authentication error by attempting Auto Authentication Attempt.");
                    
                    [self addObserverToNotificationForReAuthenticationCall];
                    _webServiceErrorBeforeAutoAuthenticationAttempt = webServiceError;
                    _hasAutoAuthenticationBeenAlreadyAttempted = YES;
                }];
            }
        }
    }
    
    return errorHandled;
}


#pragma mark - Private Helper Methods

- (void)addObserverToNotificationForReAuthenticationCall
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reAuthenticationAPICallSuccessfullyFinishedNotification:) name:kReAuthenticationApiCallSuccessfullyFinishedNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reAuthenticationAPICallFailedNotification:) name:kReAuthenticationApiCallFailedNotification object:nil];
}

- (void)removeObserverToNotificationForReAuthenticationCall
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReAuthenticationApiCallSuccessfullyFinishedNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReAuthenticationApiCallFailedNotification object:nil];
}


#pragma mark - Notification Methods

- (void)reAuthenticationAPICallSuccessfullyFinishedNotification:(NSNotification *)notification
{
    [self removeObserverToNotificationForReAuthenticationCall];
    
    BTAuthenticationToken *token = [notification.userInfo valueForKey:@"token"];
    _authenticationToken = token;
    
    [self resume];
}

- (void)reAuthenticationAPICallFailedNotification:(NSNotification *)notification
{
    [self removeObserverToNotificationForReAuthenticationCall];
    
    NLWebServiceError *error = [notification.userInfo valueForKey:@"error"];
    
    NLWebServiceError *newErrorObject = [[NLWebServiceError alloc] initWithError:error.error andSourceError:_webServiceErrorBeforeAutoAuthenticationAttempt];
    [self handleFailureWithSessionDataTask:(NSURLSessionDataTask *)_currentTask andWebServiceError:newErrorObject];
    [self cancel];
}


@end
