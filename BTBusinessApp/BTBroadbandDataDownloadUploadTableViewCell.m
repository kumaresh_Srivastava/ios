//
//  BTBroadbandDataDownloadUploadTableViewCell.m
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBroadbandDataDownloadUploadTableViewCell.h"
#import "DLMBroadbandUsageScreen.h"

@interface BTBroadbandDataDownloadUploadTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *dataDownloadedLabel;
@property (weak, nonatomic) IBOutlet UILabel *dataUploadedLabel;
@end

@implementation BTBroadbandDataDownloadUploadTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateWithBroadbandUsageRowWrapper:(BroadbandUsageRowWrapper *)wrapper{

    
    self.dataDownloadedLabel.text = wrapper.subHeading1;
    self.dataUploadedLabel.text = wrapper.subHeading2;
}

- (void)updateDataCellWithDataDownloaded:(NSString*)downloaded andUploaded:(NSString*)uploaded
{
    _dataDownloadedLabel.text = downloaded;
    _dataUploadedLabel.text = uploaded;
}

@end
