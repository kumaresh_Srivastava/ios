//
//  BTAccountLocationDropdownView.m
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTAccountLocationDropdownView.h"

@implementation BTAccountLocationDropdownView

- (void)awakeFromNib{
    [super awakeFromNib];
    
    CGRect frame = self.frame;
    frame.size.width = [[UIScreen mainScreen] bounds].size.width;
    self.frame = frame;
    
    self.dropdownOutlineView.layer.borderWidth = 1.0f;
    self.dropdownOutlineView.layer.cornerRadius = 5.0f;
    self.dropdownOutlineView.layer.borderColor = [BrandColours colourBtNeutral80].CGColor;
    
//    UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showOptions)];
//    [self.dropdownOutlineView addGestureRecognizer:tapped];
    
    self.locationLabel.text = @"None selected";
    
    self.isOptionsShowing = NO;
}

- (void)updateWithTitle:(NSString *)title values:(NSArray<NSString *> *)values selectedValue:(NSString *)selected
{
    self.titleLabel.text = title;
    if (values && values.count) {
        self.values = [NSArray arrayWithArray:values];
    }
    if (selected && ![selected isEqualToString:@""]) {
        self.selectedValue = selected;
        self.locationLabel.text = selected;
    } else {
        self.locationLabel.text = @"";
    }
    
}

- (void)updateWithTitle:(NSString *)title values:(NSArray<NSString *> *)values selectedValue:(NSString *)selected andPlaceHolder:(NSString *)placeholder
{
    self.titleLabel.text = title;
    if (values && values.count) {
        self.values = [NSArray arrayWithArray:values];
    }
    
    if (placeholder && ![placeholder isEqualToString:@""]) {
        self.locationLabel.text = placeholder;
    } else {
        self.locationLabel.text = @"";
    }
    
    if (selected && ![selected isEqualToString:@""]) {
        self.selectedValue = selected;
        if (!placeholder || [placeholder isEqualToString:@""]) {
            self.locationLabel.text = selected;
        }
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    //[super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)showOptions {
    if (self.values && self.values.count > 1) {
        //NSString *alertTitle = [NSString stringWithFormat:@"Select %@",self.titleLabel.text];
        //NSString *alertTitle = @"";
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        __weak typeof(self) weakSelf = self;
        for (NSString *value in self.values) {
            UIAlertAction *action = [UIAlertAction actionWithTitle:value style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [weakSelf selectedValue:value];
//                if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(cell:updatedSelection:)]) {
//                    [weakSelf.delegate cell:weakSelf updatedSelection:value];
//                }
                weakSelf.isOptionsShowing = NO;
                [alert dismissViewControllerAnimated:YES completion:nil];
            }];
            [alert addAction:action];
        }
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                         style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                             weakSelf.isOptionsShowing = NO;
                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                             
                                                         }];
        [alert addAction:cancel];
        alert.popoverPresentationController.sourceView = self;
        alert.popoverPresentationController.sourceRect = self.dropdownOutlineView.frame;
        alert.popoverPresentationController.canOverlapSourceViewRect = NO;
        
        if (self.delegate && [self.delegate isKindOfClass:[UIViewController class]]) {
            [(UIViewController*)self.delegate presentViewController:alert animated:YES completion:^{
                weakSelf.isOptionsShowing = YES;
            }];
        } else {
            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:^{
                weakSelf.isOptionsShowing = YES;
            }];
        }
    }
    
}

- (void)selectedValue:(NSString*)value {
    self.selectedValue = value;
    self.locationLabel.text = value;
}

@end
