//
//  BTOrderStatusLabel.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 23/11/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BTOrderStatusLabel : UILabel

- (void)setOrderStatus:(NSString*)status;
- (void)setMSOFlag:(NSString*)msoFlag forBroadband:(BOOL)broadband;

@end

NS_ASSUME_NONNULL_END
