//
//  NLAPIGEEAuthenticatedWebService.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 19/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLEEWebService.h"
#import "NLAPIGEEWebServiceError.h"

@class NLAPIGEEAuthenticatedWebService;
@class BTAuthenticationToken;

@protocol NLAPIGEEAuthenticationProtectedWebServiceDelegate <NSObject>

- (void)webService:(NLAPIGEEAuthenticatedWebService*)webService finshedWithSuccessResponse:(NSObject*)responseObject;
- (void)webService:(NLAPIGEEAuthenticatedWebService*)webService failedWithError:(NLWebServiceError*)error;

@end

@interface NLAPIGEEAuthenticatedWebService : NLEEWebService {
    BTAuthenticationToken *_authenticationToken;
}

@property (nonatomic, readonly) NSString *friendlyName;
@property (nonatomic, weak) id<NLAPIGEEAuthenticationProtectedWebServiceDelegate> delegate;

- (instancetype)initWithMethod:(NSString *)method parameters:(NSString *)parameters andEndpointUrlString:(NSString *)endpointUrlString;


@end
