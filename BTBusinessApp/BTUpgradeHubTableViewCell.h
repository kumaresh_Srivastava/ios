//
//  BTUpgradeHubTableViewCell.h
//  BTBusinessApp
//
//  Created by Jim Purvis on 07/03/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BTUpgradeHubTableViewCell;

@protocol BTUpgradeHubTableViewCellDelegate <NSObject>

-(void)userPressedCellForUpgradeHub:(BTUpgradeHubTableViewCell*)BTUpgradeHubCell;

@end

@interface BTUpgradeHubTableViewCell : UITableViewCell {
    
}
@property (weak, nonatomic) IBOutlet UIButton *upgradeButton;
- (IBAction)upgradePressed:(id)sender;

@property (nonatomic, weak) id <BTUpgradeHubTableViewCellDelegate> BTUpgradeHubTableViewCellDelegate;

@end
