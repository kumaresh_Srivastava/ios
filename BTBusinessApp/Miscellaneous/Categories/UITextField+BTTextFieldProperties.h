//
//  UITextField+BTTextFieldProperties.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 4/7/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (BTTextFieldProperties) {
    
}

- (void)getBTTextFieldStyle;
- (void)updateWithEnabledState;
- (void)updateWithDisabledState;

@end
