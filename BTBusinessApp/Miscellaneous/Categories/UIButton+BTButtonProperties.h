//
//  UIButton+BTButtonProperties.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 02/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (BTButtonProperties) {
    
}

- (void)getBTButtonStyle;

- (void)getSuperButtonStyle;

@end
