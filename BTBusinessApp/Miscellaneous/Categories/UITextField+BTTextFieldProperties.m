//
//  UITextField+BTTextFieldProperties.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 4/7/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "UITextField+BTTextFieldProperties.h"
#import "BrandColours.h"
#import "AppConstants.h"

@implementation UITextField (BTTextFieldProperties)

- (void)getBTTextFieldStyle
{
    self.font = [UIFont fontWithName:kBtFontRegular size:15];
    
    self.tintColor = [BrandColours colourBtPrimaryColor];
    self.textColor = [BrandColours colourBtNeutral90];
    self.backgroundColor = [UIColor whiteColor];
    
    self.layer.borderColor = [BrandColours colourBtNeutral60].CGColor;
    self.layer.borderWidth = 1.0f;
    self.layer.cornerRadius = 3.0f;
    
    UIView *hiddenLeftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, self.frame.size.height)];
    hiddenLeftView.backgroundColor = [UIColor clearColor];
    
    self.leftView = hiddenLeftView;
    self.leftViewMode = UITextFieldViewModeAlways;
}

- (void)updateWithEnabledState
{
    //Add shadow to textfield
    [self updateWithShadow:NO];
    self.clipsToBounds = NO;
    self.layer.borderColor = [BrandColours colourBtNeutral60].CGColor;
}

- (void)updateWithDisabledState
{
    [self updateWithShadow:NO];
}

- (void)updateWithShadow:(BOOL)shadowRequired
{
    if (shadowRequired)
    {
        self.layer.shadowOpacity = 1.0f;
        self.layer.shadowRadius = 2.0f;
        self.layer.shadowColor = [BrandColours colourBtPrimaryColor].CGColor;
        self.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    }
    else
    {
        self.layer.shadowOpacity = 0.0;
        self.layer.shadowRadius = 0.0;
        self.layer.shadowColor = [UIColor clearColor].CGColor;
    }
}

@end
