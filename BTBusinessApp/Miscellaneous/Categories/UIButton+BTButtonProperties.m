//
//  UIButton+BTButtonProperties.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 02/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "UIButton+BTButtonProperties.h"

@implementation UIButton (BTButtonProperties)

- (void)getBTButtonStyle {
    [self setBackgroundImage:[UIImage imageNamed:@"Normal.png"] forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage imageNamed:@"Highlighted.png"] forState:UIControlStateSelected];
    [self setBackgroundImage:[UIImage imageNamed:@"Disabled.png"] forState:UIControlStateDisabled];
}

- (void)getSuperButtonStyle
{
    [self setBackgroundImage:[UIImage imageNamed:@"Super_Button_Active"] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self setBackgroundImage:[UIImage imageNamed:@"Super_Button_Tapped"] forState:UIControlStateSelected];
    [self setBackgroundImage:[UIImage imageNamed:@"Super_Button_Tapped"] forState:UIControlStateHighlighted];
    [self setBackgroundImage:[UIImage imageNamed:@"Super_Button_Tapped"] forState:UIControlStateFocused];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateFocused];
    
    [self setBackgroundImage:[UIImage imageNamed:@"Super_Button_Disabled"] forState:UIControlStateDisabled];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
}

@end
