//
//  LHServerLogCustomFormatter.h
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 10/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CocoaLumberjack/DDLog.h>

@interface LHServerLogCustomFormatter : NSObject <DDLogFormatter> {
    
}

+ (NSString *)outputStringFromServerLogDictionary:(NSDictionary *)serverLogDic;

@end
