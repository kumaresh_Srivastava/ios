//
//  DDLogFileManagerCompleteLogs.m
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 04/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "LHLogFileManagerCompleteLogs.h"
#import "LHConstants.h"

@implementation LHLogFileManagerCompleteLogs

- (instancetype)init
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *baseDir = paths.firstObject;
    NSString *logsDirectory = [baseDir stringByAppendingPathComponent:@"Logs"];
    NSString *localLogsDirectory = [logsDirectory stringByAppendingPathComponent:@"local"];
    self = [super initWithLogsDirectory:localLogsDirectory];
    if(self)
    {

    }
    return self;
}

#pragma mark - Override Methods From DDLogFileManagerDefault

- (NSString *)newLogFileName {
    NSString *filePrefix = [self prefixForCompleteLogs];

    NSDateFormatter *dateFormatter = [self logFileDateFormatter];
    NSString *formattedDate = [dateFormatter stringFromDate:[NSDate date]];

    return [NSString stringWithFormat:@"%@ %@.log", filePrefix, formattedDate];
}

- (BOOL)isLogFile:(NSString *)fileName {
    NSString *filePrefix = [self prefixForCompleteLogs];

    BOOL hasProperPrefix = [fileName hasPrefix:filePrefix];
    BOOL hasProperSuffix = [fileName hasSuffix:@".log"];
    BOOL hasProperDate = NO;

    if (hasProperPrefix && hasProperSuffix) {
        NSUInteger lengthOfMiddle = fileName.length - filePrefix.length - @".log".length;

        // Date string should have at least 16 characters - " 2013-12-03 17-14"
        if (lengthOfMiddle >= 17) {
            NSRange range = NSMakeRange(filePrefix.length, lengthOfMiddle);

            NSString *middle = [fileName substringWithRange:range];
            NSArray *components = [middle componentsSeparatedByString:@" "];

            // When creating logfile if there is existing file with the same name, we append attemp number at the end.
            // Thats why here we can have three or four components. For details see createNewLogFile method.
            //
            // Components:
            //     "", "2013-12-03", "17-14"
            // or
            //     "", "2013-12-03", "17-14", "1"
            if (components.count == 3 || components.count == 4) {
                NSString *dateString = [NSString stringWithFormat:@"%@ %@", components[1], components[2]];
                NSDateFormatter *dateFormatter = [self logFileDateFormatter];

                NSDate *date = [dateFormatter dateFromString:dateString];

                if (date) {
                    hasProperDate = YES;
                }
            }
        }
    }

    return (hasProperPrefix && hasProperDate && hasProperSuffix);
}

#pragma mark - Helper Methods

- (NSString *)prefixForCompleteLogs
{
    return kPrefixNameForCompleteLogFiles;
}

- (NSDateFormatter *)logFileDateFormatter {
    NSMutableDictionary *dictionary = [[NSThread currentThread]
                                       threadDictionary];
    NSString *dateFormat = @"yyyy'-'MM'-'dd' 'HH'-'mm'";
    NSString *key = [NSString stringWithFormat:@"logFileDateFormatter.%@", dateFormat];
    NSDateFormatter *dateFormatter = dictionary[key];

    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
        [dateFormatter setDateFormat:dateFormat];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        dictionary[key] = dateFormatter;
    }
    
    return dateFormatter;
}

@end
