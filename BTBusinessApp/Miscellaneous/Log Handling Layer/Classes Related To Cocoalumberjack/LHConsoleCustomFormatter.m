//
//  LHConsoleCustomFormatter.m
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 10/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "LHConsoleCustomFormatter.h"

@interface LHConsoleCustomFormatter () {

    int _loggerCount;
    NSDateFormatter *_threadUnsafeDateFormatter;
}

@end

@implementation LHConsoleCustomFormatter

- (id)init {
    if((self = [super init])) {
        _threadUnsafeDateFormatter = [[NSDateFormatter alloc] init];
        [_threadUnsafeDateFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    }
    return self;
}


#pragma mark - DDLogFormatter Protocol Methods

- (NSString *)formatLogMessage:(DDLogMessage *)logMessage {
    NSString *logLevel;
    switch (logMessage->_flag)
    {
        case DDLogFlagError    : logLevel = @"E"; break;
        case DDLogFlagWarning  : logLevel = @"W"; break;
        case DDLogFlagInfo     : logLevel = @"I"; break;
        case DDLogFlagDebug    : logLevel = @"D"; break;
        default                : logLevel = @"V"; break;
    }

    NSString *dateAndTime = [_threadUnsafeDateFormatter stringFromDate:(logMessage->_timestamp)];

    return [NSString stringWithFormat:@"%@ || %@ || %@", dateAndTime, logLevel, logMessage->_message];
}

- (void)didAddToLogger:(id <DDLogger>)logger {
    _loggerCount++;
    NSAssert(_loggerCount <= 1, @"This logger isn't thread-safe");
}

- (void)willRemoveFromLogger:(id <DDLogger>)logger {
    _loggerCount--;
}

@end
