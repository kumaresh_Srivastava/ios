//
//  LHServerLogFileLogger.m
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 10/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "LHServerLogFileLogger.h"
#import "LHConstants.h"
#import "LHServerLogItem.h"
#import "NLServerLogWebService.h"
#import "LHServerLogCustomFormatter.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDAuthenticationToken.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "NLWebServiceError.h"

@interface LHServerLogFileLogger () <NLServerLogWebServiceDelegate> {

    NSTimer *_timer;
    NLServerLogWebService *_webService;
}

@end

@implementation LHServerLogFileLogger


#pragma mark - Public Methods

- (void)startLogSubmissionTimer
{
    DDLogInfo(@"Starting Log Submission Service.");

    // (hd) Exlusively making this call so that the first call is made right away and then later on timer calls will be there.
    [self timeToSubmitLogsToServer];

    dispatch_async(dispatch_get_main_queue(), ^{

        _timer = [NSTimer scheduledTimerWithTimeInterval:LogHandlingServerSubmissionTimeInterval target:self selector:@selector(timeToSubmitLogsToServer) userInfo:nil repeats:YES];
    });
}

- (void)stopLogSubmissionTimer
{
    DDLogInfo(@"Stopping Log Submission Service.");

    dispatch_async(dispatch_get_main_queue(), ^{

        if(_timer)
        {
            [_timer invalidate];
            _timer = nil;
        }
    });
}


#pragma mark - NSTimer Methods

- (void)timeToSubmitLogsToServer
{
    DDLogVerbose(@"timeToSubmitLogsToServer called by timer.");

    dispatch_async(self.loggerQueue, ^{

        if(_webService)
        {
            // (hd) If the previous _webService is still active, then ingore doing anything this time and next time when the timer calls this, then hopefully the previous _webService would have ended and we shall try and submit then.
            return;
        }

        NSMutableArray *mutableArrayOfServerLogObjects = [NSMutableArray array];

        NSArray *sortedLogFileInfoObjects = [self.logFileManager sortedLogFileInfos];
        for(DDLogFileInfo *logFileInfo in sortedLogFileInfoObjects)
        {
            NSError *errorWhileReadingFromFile = nil;
            NSString *logStringFromFile = [NSString stringWithContentsOfFile:logFileInfo.filePath encoding:4 error:&errorWhileReadingFromFile];
            NSArray *jsonComponents = [logStringFromFile componentsSeparatedByString:[NSString stringWithFormat:@"%@\n", kSeperatorPatternForServerLogs]];

            for(NSString *jsonComponentString in jsonComponents)
            {
                NSData *jsonData = [jsonComponentString dataUsingEncoding:4];
                NSError *errorWhileCreatingJSONObject = nil;
                id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&errorWhileCreatingJSONObject];
                if(errorWhileCreatingJSONObject)
                {
                    break;
                }

                if([jsonObject isKindOfClass:[NSDictionary class]])
                {
                    LHServerLogItem *logItem = [[LHServerLogItem alloc] initWithJSONDictionary:jsonObject];
                    if(logItem)
                    {
                        [mutableArrayOfServerLogObjects addObject:logItem];
                    }
                }
            }
        }

        if([mutableArrayOfServerLogObjects count] >= 1)
        {
            // Get the currently logged in User
            DDLogInfo(@"Subimtting logs to server.");

            _webService = [[NLServerLogWebService alloc] initWithArrayOfServerLogItems:[NSArray arrayWithArray:mutableArrayOfServerLogObjects]];
            _webService.serverLogWebServiceDelegate = self;
            [_webService resume];
        }
    });
}


#pragma mark - NLAuthenticationProtectedWebServiceDelegate Methods

- (NSString *)usernameNeededByAuthenticationProtectedWebService:(NLAuthenticationProtectedWebService *)webService
{
    return [AppDelegate sharedInstance].viewModel.app.loggedInUser.username;
}

- (NSString *)passwordNeededByAuthenticationProtectedWebService:(NLAuthenticationProtectedWebService *)webService
{
    return [SAMKeychain passwordForService:kKeyChainServiceBTUserPassword account:[AppDelegate sharedInstance].viewModel.app.loggedInUser.username];
}

- (void)authenticationProtectedWebService:(NLAuthenticationProtectedWebService *)webService freshTokenFetchedDuringWebService:(NSString *)freshToken
{
    // (hd) Save the latest token into the persistence.

    // (hd) Get the currently logged in User
    CDUser *user = [AppDelegate sharedInstance].viewModel.app.loggedInUser;

    DDLogInfo(@"Saving the authentication token received from success of NLSiteMinderLoginService for username %@ to persistence.", user.username);
    user.token.tokenString = freshToken;
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];



    //TODO: (hd) 19-09-2016 Temporarily saving the cookie value in NSUserDefaults. Remove this.
    NSString *trimmedToken = [freshToken stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [[NSUserDefaults standardUserDefaults] setValue:trimmedToken forKey:@"smsession"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - NLServerLogWebServiceDelegate Methods

- (void)serverLogWebService:(NLServerLogWebService *)webService finishedSubmittingLogsToServerWithArrayOfSuccessfullServerLogItems:(NSArray *)arrayOfSuccessServerLogItems
{
    dispatch_async(self.loggerQueue, ^{

        if(webService == _webService)
        {
            if([arrayOfSuccessServerLogItems count] >= 1)
            {
                // Then iterate each file one by one. Open it, remove and write back.
                NSArray *sortedLogFileInfoObjects = [self.logFileManager sortedLogFileInfos];
                for(DDLogFileInfo *logFileInfo in sortedLogFileInfoObjects)
                {
                    NSError *errorWhileReadingFromFile = nil;
                    NSString *logStringFromFile = [NSString stringWithContentsOfFile:logFileInfo.filePath encoding:4 error:&errorWhileReadingFromFile];
                    NSArray *jsonComponents = [logStringFromFile componentsSeparatedByString:[NSString stringWithFormat:@"%@\n", kSeperatorPatternForServerLogs]];

                    NSMutableArray *mutableArrayOfRemainingJSONComponents = [NSMutableArray array];

                    for(NSString *jsonComponentString in jsonComponents)
                    {
                        NSData *jsonData = [jsonComponentString dataUsingEncoding:4];
                        NSError *errorWhileCreatingJSONObject = nil;
                        id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&errorWhileCreatingJSONObject];
                        if(errorWhileCreatingJSONObject)
                        {
                            break;
                        }



                        if([jsonObject isKindOfClass:[NSDictionary class]])
                        {
                            BOOL shouldWeAddThisComponentBack = YES;

                            LHServerLogItem *logItem = [[LHServerLogItem alloc] initWithJSONDictionary:jsonObject];
                            if(logItem)
                            {
                                for(LHServerLogItem *successfulLogItem in arrayOfSuccessServerLogItems)
                                {
                                    if(logItem.logID == successfulLogItem.logID)
                                    {
                                        shouldWeAddThisComponentBack = NO;
                                        break;
                                    }
                                }
                            }

                            if(shouldWeAddThisComponentBack)
                            {
                                [mutableArrayOfRemainingJSONComponents addObject:jsonObject];
                            }
                        }
                    }

                    // Create the json string again and the output string and write it back to the file.
                    NSMutableString *mutableFinalStringToBeWritten = [NSMutableString string];

                    for(NSDictionary *logDic in mutableArrayOfRemainingJSONComponents)
                    {
                        NSString *jsonStringForLogDic = [LHServerLogCustomFormatter outputStringFromServerLogDictionary:logDic];
                        [mutableFinalStringToBeWritten appendString:[NSString stringWithFormat:@"%@\n", jsonStringForLogDic]];
                    }

                    NSError *errorWhileWritingFile = nil;
                    BOOL writeSuccess = [mutableFinalStringToBeWritten writeToFile:logFileInfo.filePath atomically:YES encoding:4 error:&errorWhileWritingFile];
                    if(writeSuccess)
                    {
                        DDLogInfo(@"Successfully removed server log items from local file after successfully submitting the logs to server.");
                    }
                    else if(errorWhileWritingFile)
                    {
                        DDLogError(@"Failed to remove server log items from local file after successfully submitting the logs to server with error: %@.", errorWhileWritingFile);
                        NSAssert(NO, @"Failed to remove server log items from local file after successfully submitting the logs to server.");
                    }
                }
            }
        }
        else
        {
            NSAssert(NO, @"LHServerLogFileLogger: This object should not have been the delegate of any other object of NLServerLogWebService other than its own member variable _webService.");
        }

        _webService.serverLogWebServiceDelegate = nil;
        _webService = nil;
    });
}

- (void)serverLogWebService:(NLServerLogWebService *)webService failedToSubmitLogsToServerWithWebServiceError:(NLWebServiceError *)webServiceError;
{
    DDLogError(@"Failed to submit logs to server with error: %@. Will try again afer %f seconds.", webServiceError.error, LogHandlingServerSubmissionTimeInterval);
    
    _webService.serverLogWebServiceDelegate = nil;
    _webService = nil;
}


@end
