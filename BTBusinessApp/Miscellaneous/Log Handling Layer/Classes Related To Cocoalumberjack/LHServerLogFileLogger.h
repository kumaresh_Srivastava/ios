//
//  LHServerLogFileLogger.h
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 10/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <CocoaLumberjack/CocoaLumberjack.h>

@interface LHServerLogFileLogger : DDFileLogger {

}

- (void)startLogSubmissionTimer;

- (void)stopLogSubmissionTimer;

@end
