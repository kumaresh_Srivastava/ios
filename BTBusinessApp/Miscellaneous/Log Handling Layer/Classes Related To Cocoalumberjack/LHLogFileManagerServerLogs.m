//
//  LHLogFileManagerServerLogs.m
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 10/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "LHLogFileManagerServerLogs.h"
#import "LHConstants.h"

@implementation LHLogFileManagerServerLogs

- (instancetype)init
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *baseDir = paths.firstObject;
    NSString *logsDirectory = [baseDir stringByAppendingPathComponent:@"Logs"];
    NSString *localLogsDirectory = [logsDirectory stringByAppendingPathComponent:@"server"];
    self = [super initWithLogsDirectory:localLogsDirectory];
    if(self)
    {

    }
    return self;
}

#pragma mark - Override Methods From DDLogFileManagerDefault

- (NSString *)newLogFileName {
    NSString *filePrefix = [self prefixForServerLogs];

    return [NSString stringWithFormat:@"%@.log", filePrefix];
}

- (BOOL)isLogFile:(NSString *)fileName {
    NSString *filePrefix = [self prefixForServerLogs];

    BOOL hasProperPrefix = [fileName hasPrefix:filePrefix];
    BOOL hasProperSuffix = [fileName hasSuffix:@".log"];

    return (hasProperPrefix && hasProperSuffix);
}

#pragma mark - Helper Methods

- (NSString *)prefixForServerLogs
{
    return kPrefixNameForServerLogFiles;
}

@end
