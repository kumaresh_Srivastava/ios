//
//  LHServerLogCustomFormatter.m
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 10/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "LHServerLogCustomFormatter.h"
#import "LHConstants.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"

@implementation LHServerLogCustomFormatter


#pragma mark - DDLogFormatter Protocol Methods

- (NSString *)formatLogMessage:(DDLogMessage *)logMessage {

    NSMutableDictionary *logDic = [NSMutableDictionary dictionary];

    long long logID = [[[NSUserDefaults standardUserDefaults] valueForKey:kUserDefaultKeyForServerLogIDGenerationCounter] longLongValue] + 1;
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithLongLong:logID] forKey:kUserDefaultKeyForServerLogIDGenerationCounter];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [logDic setValue:[NSNumber numberWithLongLong:logID] forKey:@"logID"];


    NSTimeInterval timeIntervalSinceReferenceDate = [logMessage->_timestamp timeIntervalSinceReferenceDate];
    [logDic setValue:[NSNumber numberWithDouble:timeIntervalSinceReferenceDate] forKey:@"timeInterval"];



    // (hd) We need loggedInUsername, btInstallationID and selectedCUGName which are stored in CoreData Objects. But core data objects should only be accessed on main thread as per design in this app. But this method does not gets called on the main thread. Hence we would have to create a temporary NSManagedObjectContext and fetch data using that.

    NSManagedObjectContext *temporaryContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    temporaryContext.persistentStoreCoordinator = [AppDelegate sharedInstance].persistentStoreCoordinator;

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"CDApp" inManagedObjectContext:temporaryContext]];

    NSError *__autoreleasing error = nil;
    NSArray *resultArray = [temporaryContext executeFetchRequest:fetchRequest error:&error];

    CDApp *app = nil;

    if([resultArray count] >= 1)
    {
        app = [resultArray objectAtIndex:0];
    }

    NSString *loggedInUsername = nil;
    NSString *btInstallationID = nil;
    NSString *selectedCUGName = nil;

    if(app)
    {
        loggedInUsername = app.loggedInUser.username;
        btInstallationID = app.btInstallationID;
        selectedCUGName = app.loggedInUser.currentSelectedCug.cugName;
    }

    if(loggedInUsername)
    {
        [logDic setValue:loggedInUsername forKey:@"loggedInUser"];
    }

    if(btInstallationID)
    {
        [logDic setValue:btInstallationID forKey:@"BTInstallationID"];
    }

    if(selectedCUGName)
    {
        [logDic setValue:selectedCUGName forKey:@"selectedCUG"];
    }



    

    [logDic setValue:logMessage->_message forKey:@"logMessage"];


    [logDic setValue:[NSNumber numberWithUnsignedInteger:logMessage->_flag] forKey:@"logFlag"];


    [logDic setValue:logMessage->_fileName forKey:@"fileName"];


    [logDic setValue:logMessage->_function forKey:@"method"];


    [logDic setValue:[NSNumber numberWithUnsignedInteger:logMessage->_line] forKey:@"line"];

    if(logMessage->_threadName)
    {
        [logDic setValue:logMessage->_threadName forKey:@"threadName"];
    }

    if(logMessage->_queueLabel)
    {
        [logDic setValue:logMessage->_queueLabel forKey:@"queueLabel"];
    }

    return [LHServerLogCustomFormatter outputStringFromServerLogDictionary:logDic];
}

#pragma mark - Helper Methods

+ (NSString *)outputStringFromServerLogDictionary:(NSDictionary *)serverLogDic
{
    NSError *errorWhileCreatingJSONData = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:serverLogDic options:0 error:&errorWhileCreatingJSONData];

    if(errorWhileCreatingJSONData)
    {
        return nil;
    }

    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:4];

    NSString *outputString = [NSString stringWithFormat:@"%@%@", jsonString, kSeperatorPatternForServerLogs];

    return outputString;
}



@end
