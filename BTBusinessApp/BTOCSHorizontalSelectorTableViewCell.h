//
//  BTOCSHorizontalSelectorTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

@class BTOCSHorizontalSelectorTableViewCell;

@protocol BTOCSHorizontalSelectorTableViewCellDelegate <NSObject>

- (void)horizontalSelectorCell:(BTOCSHorizontalSelectorTableViewCell*)cell updatedSelection:(NSString*)selected;

@end

@interface BTOCSHorizontalSelectorTableViewCell : BTBaseTableViewCell

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSArray<NSString*> *options;
@property (strong, nonatomic) NSString *selectedOption;
@property (nonatomic) id<BTOCSHorizontalSelectorTableViewCellDelegate>delegate;

- (void)updateWithOptions:(NSArray<NSString*>*)options andSelectedOption:(NSString*)selected;

@end
