//
//  BTVersionCheckViewController.m
//  BTBusinessApp
//
//  Created by VectoScalar on 3/17/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTVersionCheckViewController.h"
#import "DLMVersionCheckScreen.h"
#import "BTVersionCheckResponse.h"
#import "NLWebServiceError.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "AppConstants.h"
#import "AppManager.h"
#import "OmnitureManager.h"

@interface BTVersionCheckViewController ()

@end

@interface BTVersionCheckViewController()<DLMVersionCheckScreenDelegate>
{
    DLMVersionCheckScreen *_viewModel;
}
@property(nonatomic, assign) BOOL apiInProgress;
@end

@implementation BTVersionCheckViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.apiInProgress = NO;
    
    _viewModel = [[DLMVersionCheckScreen alloc] init];
    _viewModel.delegate = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appEnteredInForground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
}


- (void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}

#pragma mark- Helper Method

- (void)checkForVersionUpdate{
    if(!self.isVersionCheckInProgress){
        if([[AppManager sharedAppManager] getIsVersionCheckFinished]){
            
            //Version check has done for this launch
            return;
        }
        
        if(!_viewModel.versionCheckResponse){
            [_viewModel checkForVersionUpdate];
            self.isVersionCheckInProgress = YES;
        }
        else{
            
            [self handleVersionResponse:_viewModel.versionCheckResponse];
        }
        
    }
}


- (void)finishingAppVersionCheckProcess{
    
    
}


#pragma mark- private methos

- (void)appEnteredInForground{
    
    //[self checkForVersionUpdate];
}


- (void)showPopupWithMessage:(NSString *)message
                        link:(NSString *)link
                 andSeverity:(NSString *)severity
{
    
    
    
    NSString *cancelButtonTitle = nil;
    NSString *otherButtonTitle = nil;
    
    if ([severity isEqualToString:@"WARNING"]) {
        
        cancelButtonTitle = @"Cancel";
        if (link != nil) {
            
            otherButtonTitle = @"Update";
        }
        
    } else {
        
        if (link != nil) {
            otherButtonTitle = @"Update";
        }
    }
    
    
    UIAlertController *alertViewController = [UIAlertController alertControllerWithTitle:@"Latest update" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    
    if(cancelButtonTitle){
        
        UIAlertAction *cancelButtonAction = [UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            self.isVersionCheckInProgress = NO;
            //Updating version check status
            [[AppManager sharedAppManager] setIsVersionCheckFinished:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationVersionCheckCompleted object:nil];
            //[self finishingAppVersionCheckProcess];
            
        }];
        
        [alertViewController addAction:cancelButtonAction];
    }
    
    
    if(otherButtonTitle){
        
        UIAlertAction *otherButtonAction = [UIAlertAction actionWithTitle:otherButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            self.isVersionCheckInProgress = NO;
            
            if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:link]]){
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
            }
            else{
                [[AppManager sharedAppManager] setIsVersionCheckFinished:YES];
            }
            
            
        }];
        
        [alertViewController addAction:otherButtonAction];
    }
    
    [self presentViewController:alertViewController animated:YES completion:nil];
    
}


- (void)handleVersionResponse:(BTVersionCheckResponse *)versionResponse{
    
    switch(versionResponse.responseCode)
    {
        case VersionUpdateReminder:
            [self trackOmnitureContentViewedTag:OMNI_VERSION_UPDATE];
            [self showPopupWithMessage:versionResponse.errorMessage link:versionResponse.appStoreURL andSeverity:@"WARNING"];
            break;
        case UpdateMandatory:
            [self trackOmnitureContentViewedTag:OMNI_VERSION_MANDATORY_UPDATE];
            [self showPopupWithMessage:versionResponse.errorMessage link:versionResponse.appStoreURL andSeverity:@"MANDATORY"];
            break;
            
        case OSDeprecatedReminder:
        case OSDeprecatedMandatory:
        case SpecificOSNotSupportedMandatory:
        case DeviceNotSupported:
        case OSNotSupported:
            [self trackOmnitureContentViewedTag:OMNI_VERSION_UNSUPPORTED];
            [self showPopupWithMessage:versionResponse.errorMessage link:nil andSeverity:@"MANDATORY"];
            break;
            
        default:{
            
            self.isVersionCheckInProgress = NO;
            [self finishingAppVersionCheckProcess];
            
            //Updating version check status
            [[AppManager sharedAppManager] setIsVersionCheckFinished:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationVersionCheckCompleted object:nil];
        }
            break;
    }

}



#pragma mark- DLMVersionCheckScreenDelegate methods

- (void)versionCheckScreen:(DLMVersionCheckScreen *)versionCheckScreen successfullWithVersionResponse:(BTVersionCheckResponse *)versionResponse{
    [self handleVersionResponse:versionResponse];
}



- (void)versionCheckScreen:(DLMVersionCheckScreen *)versionCheckScreen failedWithWebServiceError:(NLWebServiceError *)webServiceError{
    
    self.isVersionCheckInProgress = NO;
    
    if(webServiceError && webServiceError.error){
        switch (webServiceError.error.code) {
                
            case BTNetworkErrorCodeNoInternet:
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationVersionCheckCompleted object:@{@"error" : @"Sorry, something has gone wrong"}];
            }
                break;
                
            default:{
                
                [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_LAUNCH];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationVersionCheckCompleted object:@{@"error" : @"Sorry, something has gone wrong"}];
                
            }
                break;
        }
    }
    
}

- (void)trackOmnitureContentViewedTag:(NSString *)contentMessage {
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = OMNIPAGE_LAUNCH;
    [data setValue:@"Not Logged In" forKey:kOmniLoginStatus];
    
    NSString *notificaiton = [NSString stringWithFormat:@"%@%@",STATE_PREFIX,contentMessage];
    [data setValue:notificaiton forKey:kOmniContentViewed];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}


@end
