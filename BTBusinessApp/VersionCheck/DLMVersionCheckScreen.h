//
//  DLMVersionCheckScreen.h
//  BTBusinessApp
//
//  Created by VectoScalar on 3/17/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "DLMObject.h"
@class DLMVersionCheckScreen;
@class NLWebServiceError;
@class BTVersionCheckResponse;

@protocol DLMVersionCheckScreenDelegate <NSObject>

- (void)versionCheckScreen:(DLMVersionCheckScreen *)versionCheckScreen successfullWithVersionResponse:(BTVersionCheckResponse *)versionResponse;
- (void)versionCheckScreen:(DLMVersionCheckScreen *)versionCheckScreen failedWithWebServiceError:(NLWebServiceError *)webServiceError;

@end


@interface DLMVersionCheckScreen : DLMObject
@property(nonatomic, weak) id<DLMVersionCheckScreenDelegate>delegate;
@property(nonatomic, readonly) BTVersionCheckResponse *versionCheckResponse;

- (void)checkForVersionUpdate;
- (void)cancelVersionCheckAPI;

@end
