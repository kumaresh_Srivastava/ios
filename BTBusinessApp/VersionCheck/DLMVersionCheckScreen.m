//
//  DLMVersionCheckScreen.m
//  BTBusinessApp
//
//  Created by VectoScalar on 3/17/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "DLMVersionCheckScreen.h"
#import "NLVersionCheckWebService.h"

@interface DLMVersionCheckScreen()<NLVersionCheckWebServiceDelegate>{
}

@property(nonatomic, strong) NLVersionCheckWebService *versionCheckWebService;

@end

@implementation DLMVersionCheckScreen


- (void)checkForVersionUpdate{

    self.versionCheckWebService = [[NLVersionCheckWebService alloc] initWithMethod:@"GET" parameters:nil endpointUrlString:@"/api/v3/forceupdate" andBaseURLType:NLBaseURLTypeNonVordel];
    self.versionCheckWebService.versionCheckWebServiceDelegate = self;
    
    [self.versionCheckWebService resume];
}



- (void)cancelVersionCheckAPI{
    
    
}



#pragma mark- NLVersionCheckWebServiceDelegate

- (void)versionCheckWebService:(NLVersionCheckWebService *)webService succussfullyFinishedWithVersionResponse:(BTVersionCheckResponse *)versionCheckResponse{
    
    _versionCheckResponse = versionCheckResponse;
    [self.delegate versionCheckScreen:self successfullWithVersionResponse:versionCheckResponse];
    
    
}

- (void)versionCheckWebService:(NLVersionCheckWebService *)webService failedWithWebServiceError:(NLWebServiceError *)webServiceError{
    
    [self.delegate versionCheckScreen:self failedWithWebServiceError:webServiceError];
}

@end
