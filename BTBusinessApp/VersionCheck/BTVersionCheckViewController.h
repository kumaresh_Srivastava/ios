//
//  BTVersionCheckViewController.h
//  BTBusinessApp
//
//  Created by VectoScalar on 3/17/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseViewController.h"

@interface BTVersionCheckViewController : UIViewController
@property(nonatomic, assign)  BOOL isVersionCheckInProgress;
- (void)checkForVersionUpdate;
@end
