//
//  BTOCSDropDownTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

@class BTOCSDropDownTableViewCell;

@protocol BTOCSDropDownTableViewCellDelegate <NSObject>

- (void)cell:(BTOCSDropDownTableViewCell*)cell updatedSelection:(NSString*)selected;

@end

@interface BTOCSDropDownTableViewCell : BTBaseTableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIView *dropdownOutlineView;
@property (strong, nonatomic) IBOutlet UILabel *detailLabel;
@property (strong, nonatomic) IBOutlet UIImageView *dropdownIcon;
@property (strong, nonatomic) IBOutlet UIView *detailSeparatorView;

@property (strong, nonatomic) NSArray<NSString*> *values;
@property (strong, nonatomic) NSString *selectedValue;

@property (nonatomic) BOOL isOptionsShowing;

@property (nonatomic) id<BTOCSDropDownTableViewCellDelegate> delegate;

- (void)updateWithTitle:(NSString*)title values:(NSArray<NSString*>*)values selectedValue:(NSString*)selected;
- (void)updateWithTitle:(NSString*)title values:(NSArray<NSString*>*)values selectedValue:(NSString*)selected andPlaceHolder:(NSString*)placeholder;

- (void)showOptions;

@end
