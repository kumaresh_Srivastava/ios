//
//  BTPublicWifiPostersTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 20/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseTableViewCell.h"

@interface BTPublicWifiPostersTableViewCell : BTBaseTableViewCell
@property (strong, nonatomic) IBOutlet UILabel *posterHeadlineLabel;
@property (strong, nonatomic) IBOutlet UITextView *posterDetailTextView;
@property (strong, nonatomic) IBOutlet UIButton *posterOrderButton;
- (IBAction)posterOrderAction:(id)sender;

@end
