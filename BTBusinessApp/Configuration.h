#import <Foundation/Foundation.h>

struct Config {
    NSString *environment;
    NSString *baseVordelUrl;
    NSString *baseSaasUrl;
    NSString *baseApigeeUrl;
};

@interface Configuration : NSObject

@property (nonatomic, retain) NSDictionary *plistDir;
@property struct Config config;

+ (instancetype)sharedInstance;
@end

