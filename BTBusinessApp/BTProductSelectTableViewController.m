//
//  BTProductSelectTableViewController.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 01/03/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <ReactiveCocoa/ReactiveCocoa.h>

#import "BTProductSelectTableViewController.h"
#import "BTProductSelectPublicWifiTableViewCell.h"
#import "AppConstants.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "AppManager.h"
#import "OmnitureManager.h"
#import "BTUser.h"
#import "BTCug.h"
#import "CustomSpinnerView.h"
#import "BTRetryView.h"
#import "BTAsset.h"
#import "BTAssetModel.h"
#import "BTEmptyDashboardView.h"

#import "BTPublicWifiUnavailableViewController.h"
#import "BTPublicWiFiDetailsTableViewController.h"

#import "NLQueryClientProfileDetails.h"
#import "NLGetBusinessHubDetails.h"
#import "NLVordelWebServiceError.h"

#import "BTClientProfileDetails.h"
#import "BTClientServiceInstance.h"

@interface BTProductSelectTableViewController () <UITableViewDataSource,UITableViewDelegate,BTRetryViewDelegate,NLQueryClientProfileDetailsDelegate,NLGetBusinessHubDetailsDelegate>

@property (nonatomic, assign) BOOL networkRequestInProgress;
@property (nonatomic,strong)  CustomSpinnerView *loadingView;
@property (strong, nonatomic) BTRetryView *retryView;
@property (strong, nonatomic) BTEmptyDashboardView *emptyDashboardView;

@property (nonatomic) NLQueryClientProfileDetails *queryClientProfileDetailsWebservice;
@property (nonatomic) NSMutableArray<NLGetBusinessHubDetails*> *getBusinessHubDetailsWebServices;

@property (nonatomic, strong) NSArray *broadbandAssets;

@property (nonatomic,strong) UILabel *pageTitleLabel;
@property (nonatomic,strong) UILabel *pageSubTitleLabel;

@property (nonatomic, strong) BTClientProfileDetails *clientProfileDetails;

@end

@implementation BTProductSelectTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    if(!self.cugs) {
        self.cugs = [[AppDelegate sharedInstance] savedCugs];
    }
    
    [self createInitialUI];
    
    UINib *dataCell = [UINib nibWithNibName:@"BTProductSelectPublicWifiTableViewCell" bundle:nil];
    [self.tableView registerNib:dataCell forCellReuseIdentifier:@"BTProductSelectPublicWifiTableViewCell"];
    
    [self createLoadingView];
    
    __weak typeof(self) selfWeak = self;
    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([selfWeak networkRequestInProgress]) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (([selfWeak.tableView numberOfSections] > 0) && ([selfWeak.tableView numberOfRowsInSection:0] > 0)) {
                    [selfWeak.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
                }
                [selfWeak hideLoadingItems:NO];
                selfWeak.navigationItem.rightBarButtonItem.enabled = NO;
                [selfWeak.tableView setScrollEnabled:NO];
                [selfWeak.loadingView startAnimatingLoadingIndicatorView];
            });
        }
        
        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
            dispatch_async(dispatch_get_main_queue(), ^{
                [selfWeak hideLoadingItems:YES];
                selfWeak.navigationItem.rightBarButtonItem.enabled = YES;
                [selfWeak.tableView setScrollEnabled:YES];
                [selfWeak.loadingView stopAnimatingLoadingIndicatorView];
                [selfWeak refreshTableView];
                
            });
        }
    }];
    
    [self makeAPIcall];
    [self trackPageForOmniture];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.clientProfileDetails && self.clientProfileDetails.clientServiceList) {
        [self refreshTableView];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSArray *)broadbandAssets
{
    if (!_broadbandAssets) {
        NSMutableArray *bAssets = [NSMutableArray arrayWithCapacity:self.selectedAsset.assetModels.count];
        for (BTAssetModel *model in self.selectedAsset.assetModels) {
            if ([[model.name lowercaseString] containsString:@"broadband"]) {
                [bAssets addObject:model];
            }
        }
        _broadbandAssets = [NSArray arrayWithArray:bAssets];
    }
    return _broadbandAssets;
}

#pragma mark - InitialUI Methods

- (void)createInitialUI
{
    //CGSize screenSize = [UIScreen mainScreen].bounds.size;
    
    if (self.context == BTAccountSelectContextPublicWifi) {
        self.pageTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 208, 37.5)];
        self.pageTitleLabel.font = [UIFont fontWithName:kBtFontBold size:20.0];
        self.pageTitleLabel.textColor = [UIColor whiteColor];
        self.pageTitleLabel.textAlignment = NSTextAlignmentCenter;
        self.pageTitleLabel.text = @"Public wi-fi";
        self.pageSubTitleLabel = nil;
    } else {
        self.pageTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 208, 20)];
        self.pageTitleLabel.font = [UIFont fontWithName:kBtFontBold size:20.0];
        self.pageTitleLabel.textColor = [UIColor whiteColor];
        self.pageTitleLabel.textAlignment = NSTextAlignmentCenter;
        self.pageTitleLabel.text = self.selectedAsset.assetName?self.selectedAsset.assetName:@"Public wi-fi";
        
        self.pageSubTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 19, 208, 17.5)];
        self.pageSubTitleLabel.font = [UIFont fontWithName:kBtFontRegular size:13.0];
        self.pageSubTitleLabel.textColor = [UIColor whiteColor];
        self.pageSubTitleLabel.textAlignment = NSTextAlignmentCenter;
        self.pageSubTitleLabel.text = self.selectedAsset.assetAccountNumber?self.selectedAsset.assetAccountNumber:self.currentUser.currentSelectedCug.cugName;
    }
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    UIView *titlesView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 208, 40)];
    [titlesView addSubview:self.pageSubTitleLabel];
    [titlesView addSubview:self.pageTitleLabel];
    
    [self.navigationItem setTitleView:titlesView];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon_Home_White"] style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonAction)];
    
    self.navigationItem.rightBarButtonItem = nil;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.view.backgroundColor = [BrandColours colourBtNeutral30];
    self.tableView.backgroundColor = [BrandColours colourBtNeutral30];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.estimatedRowHeight = 114.0;
    
    //self.tableView.refreshControl = [[UIRefreshControl alloc] init];
    //[self.tableView.refreshControl addTarget:self action:@selector(refreshAPICall) forControlEvents:UIControlEventValueChanged];
}

- (void)refreshTableView
{
    if (!self.networkRequestInProgress) {
        if (self.clientProfileDetails && self.clientProfileDetails.clientServiceList && (self.clientProfileDetails.clientServiceList.count > 0)) {
            [self.tableView reloadData];
        } else if (self.clientProfileDetails && self.clientProfileDetails.clientServiceList && (self.clientProfileDetails.clientServiceList.count == 0)) {
            //[self showEmptyDashBoardView];
            [self showEmptyAssetsView];
        } else {
            [self showRetryViewWithInternetStrip:NO];
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    NSInteger rows = 0;
//    if (self.broadbandAssets && [self.broadbandAssets count]) {
//        rows = self.broadbandAssets.count;
//    }
    if (self.context == BTAccountSelectContextPublicWifi && self.clientProfileDetails) {
        rows = [self.clientProfileDetails.clientServiceList count];
    }
    return rows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 114.0;
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BTProductSelectPublicWifiTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BTProductSelectPublicWifiTableViewCell" forIndexPath:indexPath];
    //BTAssetModel *assetModel = [self.broadbandAssets objectAtIndex:indexPath.row];
    
    BTClientServiceInstance *instance = [self.clientProfileDetails.clientServiceList objectAtIndex:indexPath.row];
    cell.contentView.backgroundColor = [BrandColours colourBtNeutral30];
    cell.serviceNameLabel.text = instance.serviceName;
    //cell.guestWifiStatusLabel.text = instance.guestWifiStatus;
    [cell updateCellWithGuestWiFiStatus:instance.guestWifiStatus];
    if (instance.serviceId && ![instance.serviceId isEqualToString:@""]) {
        //cell.serviceIDValueLabel.text = instance.serviceId;
        [cell updateCellWithServiceId:instance.serviceId];
    } else {
        //cell.serviceIDValueLabel.text = @"Unknown";
        [cell updateCellWithServiceId:@"Unknown"];
    }
    //cell.locationValueLabel.text = instance.postCode?instance.postCode:@"Unknown";
    [cell updateCellWithLocation:instance.postCode?instance.postCode:@"Unknown"];

    return cell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *nextView = nil;
    BTClientServiceInstance *selectedInstance = [self.clientProfileDetails.clientServiceList objectAtIndex:indexPath.row];
    
    switch (selectedInstance.guestWifiFlag) {
            
        case BTGuestWiFiStatusFlagUnavailableOn4GAssure:
            nextView = [[BTPublicWifiUnavailableViewController alloc] initWithClientServiceInstance:selectedInstance andContext:BTPublicWifiUnavailableContext4GAssureEnabled];
            break;
            
        case BTGuestWiFiStatusFlagCantConnect:
//            nextView = [[BTPublicWifiUnavailableViewController alloc] initWithClientServiceInstance:selectedInstance andContext:BTPublicWifiUnavailableContextNoConnection];
//            break;
            
        case BTGuestWiFiStatusFlagUnknown:
            nextView = [[BTPublicWifiUnavailableViewController alloc] initWithClientServiceInstance:selectedInstance andContext:BTPublicWifiUnavailableContextGenericError];
            break;
            
        case BTGuestWiFiStatusFlagNULLSerialNum:
        case BTGuestWiFiStatusFlagUnavailableBTWiFiOnly:
        case BTGuestWiFiStatusFlagNewHubNeeded:
        case BTGuestWiFiStatusFlagAvailable:
        case BTGuestWiFiStatusFlagOn:
        default:
            nextView = [[BTPublicWiFiDetailsTableViewController alloc] initWithClientServiceInstance:selectedInstance];
            break;

    }
    
    //nextView = [[BTPublicWiFiDetailsTableViewController alloc] initWithClientServiceInstance:selectedInstance];
    
    if (nextView) {
        [self.navigationController pushViewController:nextView animated:YES];
        [self trackOmniClick:OMNICLICK_PUBLICWIFI_BBVIEWDETAILS];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Private Helper Methods

- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow  {
    
    if(self.retryView)
    {
        self.retryView.hidden = NO;
        self.retryView.retryViewDelegate = self;
        [self.retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
    } else {
        self.retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
        self.retryView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
        self.retryView.retryViewDelegate = self;
        
        [self.view addSubview:self.retryView];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:64.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
        
        [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.tableView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
        [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.tableView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    }
}

- (void)hideRetryItems:(BOOL)isHide {
    // (SD) Hide or Show UI elements related to retry.
    [self.retryView setHidden:isHide];
}

- (void)createLoadingView {
    
    if(!self.loadingView){
        
        self.loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        self.loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        self.loadingView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.loadingView setFrame:self.tableView.frame];
        
        [self.view addSubview:self.loadingView];
    }
    
    [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.tableView  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.tableView  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.tableView  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.tableView  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
    [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.tableView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.tableView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
}

- (void)hideLoadingItems:(BOOL)isHide {
    // (SD) Hide or Show UI elements related to loading.
    [self.loadingView setHidden:isHide];
}


/*
 Updates the currently selected group and UI on the basis of group selection
 */
//- (void) checkForSuperUserAndUpdateUIWithIndex:(NSUInteger)index {
//
//    BTCug *cugData = [self.cugs objectAtIndex:index];
//    [self trackCUGChange];
//    // (SD) Change current selected cug locally and in persistence.
//    [self.assetDashboardScreenModel changeSelectedCUGTo:cugData];
//
//    if (![self.groupKey isEqualToString:cugData.groupKey]) {
//        [self setGroupKey:cugData.groupKey];
//        [self.pageTitleLabel setText:cugData.cugName];
//        [self.pageSubTitleLabel setText:cugData.groupKey];
//        [self resetDataAndUIAfterGroupChange];
//    }
//}

//- (void)resetDataAndUIAfterGroupChange
//{
//    BOOL needToRefresh = YES;
//    if(self.retryView)
//    {
//        if((![AppManager isInternetConnectionAvailable] && !_retryView.isHidden))
//            needToRefresh = NO;
//    }
//
//    if(needToRefresh){
//
//        [self hideRetryItems:YES];
//        //self.tableView.hidden = YES;
//        [self fetchAssetsDashboardAPI];
//    }
//    else
//    {
//        [self.retryView updateRetryViewWithInternetStrip:YES];
//        [AppManager trackNoInternetErrorOnPage:[self pageNameForOmnitureTracking]];
//    }
//}

#pragma mark - Action Methods

- (void)doneButtonAction
{
    if (self.networkRequestInProgress) {
        [self cancelAPIcall];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

//- (void)groupSelectionAction
//{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Select Group Name" preferredStyle:UIAlertControllerStyleActionSheet];
//    
//    DDLogVerbose(@"Bills: Total cugs belongs to user %lu.",(unsigned long)self.cugs.count);
//    
//    // (SD) Add group names in actionsheet
//    if (self.cugs != nil && self.cugs.count > 0) {
//        
//        // (SD) Add action for each cug selection.
//        int index = 0;
//        for (BTCug *groupData in self.cugs) {
//            
//            __weak typeof(self) selfWeak = self;
//            
//            UIAlertAction *action = [UIAlertAction actionWithTitle:groupData.cugName
//                                                             style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//                                                                 [selfWeak checkForSuperUserAndUpdateUIWithIndex:groupData.indexInAPIResponse];
//                                                                 [alert dismissViewControllerAnimated:YES completion:nil];
//                                                             }];
//            [alert addAction:action];
//            
//            index++;
//        }
//    }
//    
//    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
//                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
//                                                         [alert dismissViewControllerAnimated:YES completion:nil];
//                                                     }];
//    [alert addAction:cancel];
//    
//    [self presentViewController:alert animated:YES completion:^{
//        
//    }];
//}
//
//- (void)updateGroupSelection {
//    if (self.cugs.count == 1) {
//        return;
//    }
//    
//    AppDelegate *appDelegate = [AppDelegate sharedInstance];
//    BTCug *cug = appDelegate.viewModel.app.loggedInUser.currentSelectedCug;
//    [self setGroupKey:cug.groupKey];
//    self.pageSubTitleLabel.text = cug.cugName;
//}

- (void)refreshAPICall
{
    [self cancelAPIcall];
    [self makeAPIcall];
}

- (void)makeAPIcall
{
//    if ([self.tableView.refreshControl isRefreshing]) {
//        [self.tableView.refreshControl endRefreshing];
//    }
    if([AppManager isInternetConnectionAvailable])
    {
        [self createLoadingView];
        [self hideRetryItems:YES];
        [self hideEmptyDashBoardItems:YES];
        self.networkRequestInProgress = YES;
        //CDUser *user = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
        self.queryClientProfileDetailsWebservice = [[NLQueryClientProfileDetails alloc] initWithServiceCode:@"BT_BUSINESS_BROADBAND"];
        self.queryClientProfileDetailsWebservice.queryClientProfileDetailsDelegate = self;
        [self.queryClientProfileDetailsWebservice resume];
    }
    else
    {
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:[self pageNameForOmnitureTracking]];
    }

}

- (void)cancelAPIcall {
    self.networkRequestInProgress = NO;
    if (self.queryClientProfileDetailsWebservice) {
        self.queryClientProfileDetailsWebservice.queryClientProfileDetailsDelegate = nil;
        [self.queryClientProfileDetailsWebservice cancel];
        self.queryClientProfileDetailsWebservice = nil;
    }
}

#pragma mark - empty dashboard

- (void)showEmptyAssetsView {
    BTPublicWifiUnavailableViewController *vc = [[BTPublicWifiUnavailableViewController alloc] initWithClientServiceInstance:nil andContext:BTPublicWifiUnavailableContextContactAdmin];
    [self.navigationController pushViewController:vc animated:NO];
}

- (void)hideEmptyDashBoardItems:(BOOL)isHide {
    // Hide or Show UI elements related to retry.
    [self.emptyDashboardView setHidden:isHide];
    [self.emptyDashboardView removeFromSuperview];
    self.emptyDashboardView = nil;
}

- (void)showEmptyDashBoardView {
    
    //Error Tracker
    [OmnitureManager trackError:OMNIERROR_EMPTY_ACCOUNT_PROCESSSING onPageWithName:[self pageNameForOmnitureTracking] contextInfo:[AppManager getOmniDictionary]];
    
    self.emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTEmptyDashboardView" owner:nil options:nil] objectAtIndex:0];
    self.emptyDashboardView.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSURL *url;
    if ([kBTServerType isEqualToString:@"BTServerTypeProd"]) {
        url = [[NSURL alloc] initWithString:@"https://secure.business.bt.com/Account/Addbillingaccounts.aspx"];
    } else{
        url = [[NSURL alloc] initWithString:@"https://eric1-dmze2e-ukb.bt.com/Account/Addbillingaccounts.aspx"];
    }
    
    NSMutableAttributedString *hypLink = [[NSMutableAttributedString  alloc] initWithString:@"There’s no information to show just yet. Your account is either pending approval by your admin, or you can add an account here."];
    NSDictionary *linkAttributes = @{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle),
                                     NSLinkAttributeName : url};
    
    [hypLink addAttributes:linkAttributes range:NSMakeRange(hypLink.length-5,4)];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
    [hypLink addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,hypLink.length)];
    
    [hypLink addAttribute:NSForegroundColorAttributeName value:[BrandColours colourBtNeutral70] range:NSMakeRange(0, hypLink.length)];
    
    [hypLink addAttribute:NSFontAttributeName
                    value:[UIFont fontWithName:kBtFontRegular size:16.0] range:NSMakeRange(0, hypLink.length)];
    
    [self.emptyDashboardView updateEmptyDashboardViewWithImageNameAndLink:@"package" title:@"No accounts to display" detailText:hypLink andButtonTitle:nil];
    
    [self.view addSubview:self.emptyDashboardView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
}

#pragma mark - RetryView Delgates

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    [self makeAPIcall];
}

#pragma mark - API delegate methods

- (void)queryClientProfileDetailsWebService:(NLQueryClientProfileDetails *)webService successfullyFetchedClientProfileDetails:(BTClientProfileDetails *)details
{
    self.clientProfileDetails = details;
    if (!self.getBusinessHubDetailsWebServices) {
        self.getBusinessHubDetailsWebServices = [[NSMutableArray alloc] initWithCapacity:self.clientProfileDetails.clientServiceList.count];
    }
    
    // counter used to track the number of get hub details requests made
    NSUInteger counter = 0;
    
    for (BTClientServiceInstance *instance in self.clientProfileDetails.clientServiceList) {
        if (instance.hubSerialNumber && ![instance.hubSerialNumber isKindOfClass:[NSNull class]] && ![instance.hubSerialNumber isEqualToString:@""]) {
            counter++;
            NLGetBusinessHubDetails *hubDetailsWS = [[NLGetBusinessHubDetails alloc] initWithSerialNumber:instance.hubSerialNumber];
            hubDetailsWS.getBusinessHubDetailsDelegate = self;
            [self.getBusinessHubDetailsWebServices addObject:hubDetailsWS];
            [hubDetailsWS resume];
        } else {
            [instance updateGuestWiFiStatus:BTGuestWiFiStatusFlagNULLSerialNum];
            [instance setHubStatusFetched:YES];
            //[self refreshTableView];
        }
    }
    if (counter == 0) {
        // no additional requests were made
        self.networkRequestInProgress = NO;
        //[self refreshTableView];
    }
}

- (void)queryClientProfileDetailsWebService:(NLQueryClientProfileDetails *)webService failedToFetchClientProfileDetailsWithWebServiceError:(NLWebServiceError *)error
{
    self.networkRequestInProgress = NO;
    NSString *errString = @"";
    if ([error isKindOfClass:[NLVordelWebServiceError class]]) {
        NLVordelWebServiceError *vordelError = (NLVordelWebServiceError*)error;
        errString = [NSString stringWithFormat:@"%@:%@",vordelError.errorCode,vordelError.errorDescription];
        if (vordelError.errorCode && [vordelError.errorCode isEqualToString:@"MB5-06"]) {
            self.clientProfileDetails = [[BTClientProfileDetails alloc] initWithClientServiceList:@[]];
        }
    } else {
        errString = [NSString stringWithFormat:@"%li",(long)error.error.code];
        if ([error.error.userInfo objectForKey:NSLocalizedDescriptionKey]) {
            NSString *info = [error.error.userInfo objectForKey:NSLocalizedDescriptionKey];
            errString = [errString stringByAppendingString:[NSString stringWithFormat:@":%@",info]];
        }
    }
    [AppManager trackError:errString FromAPI:webService.friendlyName OnPage:[self pageNameForOmnitureTracking]];
    //[self refreshTableView];
}

- (void)getBusinessHubDetailsWebService:(NLGetBusinessHubDetails *)webService successfullyFetchedHubDetails:(NSObject *)details
{
    [self.getBusinessHubDetailsWebServices removeObject:webService];
    webService.getBusinessHubDetailsDelegate = nil;
//    if (self.getBusinessHubDetailsWebServices.count == 0) {
//        self.networkRequestInProgress = NO;
//    }
    
    NSObject *data = [(NSDictionary*)details objectForKey:@"data"];
    if ([data isKindOfClass:[NSString class]]) {
        NSError *error;
        NSData *stringData = [(NSString*)data dataUsingEncoding:NSUTF8StringEncoding];
        NSObject *theData = [NSJSONSerialization JSONObjectWithData:stringData options:NSJSONReadingMutableLeaves error:&error];
        if (!error && [theData isKindOfClass:[NSDictionary class]]) {
            NSLog(@"data: %@",theData);
            NSString *responseCode = [(NSDictionary*)theData objectForKey:@"responseCode"];
            NSString *responseMessage = [(NSDictionary*)theData objectForKey:@"responseMessage"];
            NSString *errorCode = [(NSDictionary*)theData objectForKey:@"errorCode"];
            NSString *errorMessage = [(NSDictionary*)theData objectForKey:@"errorMessage"];
            NSString *serialNum = [(NSDictionary*)theData objectForKey:@"serviceId"];
            NSDictionary *hubDetails = [(NSDictionary*)theData objectForKey:@"hubDetails"];
            if (hubDetails) {
                for (BTClientServiceInstance *instance in self.clientProfileDetails.clientServiceList) {
                    if ([instance.hubSerialNumber isEqualToString:serialNum]) {
                        if (hubDetails) {
                            [instance updateWithBusinessHubDetails:hubDetails];
                            [instance setHubStatusFetched:YES];
                        } else {
                            if (errorMessage && [errorMessage isEqualToString:@"EXECUTE_FAILED"]) {
                                [instance updateGuestWiFiStatus:BTGuestWiFiStatusFlagCantConnect];
                            } else {
                                [instance updateGuestWiFiStatus:BTGuestWiFiStatusFlagUnknown];
                            }
                            [instance setHubStatusFetched:YES];
                        }
                    }
                }
            } else {
                NSString *errString = @"";
                if (responseCode && responseMessage) {
                    errString = [NSString stringWithFormat:@"%@:%@",responseCode,responseMessage];
                } else if (errorCode && errorMessage) {
                    errString = [NSString stringWithFormat:@"%@:%@",errorCode,errorMessage];
                }
                [AppManager trackError:errString FromAPI:webService.friendlyName OnPage:[self pageNameForOmnitureTracking]];
            }
        }
    }
    
    BOOL allFetched = YES;
    for (BTClientServiceInstance *instance in self.clientProfileDetails.clientServiceList) {
        if ([instance.hubSerialNumber isEqualToString:webService.serialNumber]) {
            // failsafe in case of unknown error above
            if (!instance.gotHubStatus) {
                [instance setHubStatusFetched:YES];
//                if (instance.guestWifiFlag == BTGuestWiFiStatusFlagUnknown) {
//                    [instance updateGuestWiFiStatus:BTGuestWiFiStatusFlagCantConnect];
//                }
            }
        }
        if (!instance.gotHubStatus) {
            allFetched = NO;
        }
    }
    if (allFetched) {
        self.networkRequestInProgress = NO;
        //[self refreshTableView];
    }
}

- (void)getBusinessHubDetailsWebService:(NLGetBusinessHubDetails *)webService failedToFetchHubDetailsWithWebServiceError:(NLWebServiceError *)error
{
    [self.getBusinessHubDetailsWebServices removeObject:webService];
    webService.getBusinessHubDetailsDelegate = nil;
//    if (self.getBusinessHubDetailsWebServices.count == 0) {
//        self.networkRequestInProgress = NO;
//    }
    
    NSString *errString = @"";
    if ([error isKindOfClass:[NLVordelWebServiceError class]]) {
        NLVordelWebServiceError *vordelError = (NLVordelWebServiceError*)error;
        errString = [NSString stringWithFormat:@"%@:%@",vordelError.errorCode,vordelError.errorDescription];
    } else {
        errString = [NSString stringWithFormat:@"%li",(long)error.error.code];
        if ([error.error.userInfo objectForKey:NSLocalizedDescriptionKey]) {
            NSString *info = [error.error.userInfo objectForKey:NSLocalizedDescriptionKey];
            errString = [errString stringByAppendingString:[NSString stringWithFormat:@":%@",info]];
        }
    }
    [AppManager trackError:errString FromAPI:webService.friendlyName OnPage:[self pageNameForOmnitureTracking]];

    BOOL allFetched = YES;
    for (BTClientServiceInstance *instance in self.clientProfileDetails.clientServiceList) {
        if ([instance.hubSerialNumber isEqualToString:webService.serialNumber]) {
            [instance updateGuestWiFiStatus:BTGuestWiFiStatusFlagCantConnect];
            [instance setHubStatusFetched:YES];
        }
        if (!instance.gotHubStatus) {
            allFetched = NO;
        }
    }
    if (allFetched) {
        self.networkRequestInProgress = NO;
        //[self refreshTableView];
    }
}

#pragma mark - Omniture Methods

- (NSString*)pageNameForOmnitureTracking
{
    NSString *pageName = @"";
    switch (self.context) {
        case BTAccountSelectContextAccount:
            pageName = OMNIPAGE_ASSETS_ACCOUNT_ASSETS;
            break;
            
        case BTAccountSelectContextUsage:
            pageName = OMNIPAGE_USAGE_ASSET;
            break;
            
        case BTAccountSelectContextServiceStatus:
            pageName = OMNIPAGE_SERVICE_STATUS_ACCOUNT_ASSET;
            break;
            
        case BTAccountSelectContextPublicWifi:
            pageName = OMNIPAGE_PUBLICWIFI_ACCOUNT_ASSET;
            break;
            
        default:
            break;
    }
    return pageName;
}

- (void)trackPageForOmniture
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    NSString *pageName = [self pageNameForOmnitureTracking];
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackOmniClick:(NSString *)linkTitle
{
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",[self pageNameForOmnitureTracking],linkTitle]];
}

- (void)trackCUGChange
{
    NSString *pageName = [self pageNameForOmnitureTracking];
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,OMNICLICK_CUG_CHANGE] withContextInfo:data];
}

@end
