//
//  BTOCSProjectNotesTableViewController.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 07/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSProjectNotesTableViewController.h"

#import "NLGetProjectNotes.h"

@interface BTOCSProjectNotesTableViewController () <NLGetProjectNotesDelegate,BTOCSProjectNotesTableViewCellDelegate>

@property (strong, nonatomic) NLGetProjectNotes *getProjectNotesWebService;

@property (strong, nonatomic) BTOCSProjectInfoHeaderTableViewCell *headerCell;

@end

@implementation BTOCSProjectNotesTableViewController

+ (BTOCSProjectNotesTableViewController *)getOCSProjectNotesViewController
{
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"BTOCSOrders" bundle:nil];
    //BTOCSProjectNotesTableViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTOCSProjectNotesTableViewController"];
    BTOCSProjectNotesTableViewController *vc = [[BTOCSProjectNotesTableViewController alloc] initWithStyle:UITableViewStylePlain];
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.title = @"Project notes";
    [self getProjectNotes];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupTableCells
{
    [super setupTableCells];
    if (!self.headerCell) {
        self.headerCell = (BTOCSProjectInfoHeaderTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSProjectInfoHeaderTableViewCell reuseId]];
        [self.headerCell updateWithPorjectRef:self.project.projectRef andProduct:self.project.productName andSite:self.selectedSite.name];
        [self.tableCells addObject:self.headerCell];
    }
}

- (void)updateTableCells
{
    [super updateTableCells];
    [self.tableCells removeAllObjects];
    
    [self.tableCells addObject:self.headerCell];
    
    if (self.project.projectNotes && (self.project.projectNotes.count > 0)) {
        for (BTOCSNote *note in self.project.projectNotes) {
            BTOCSProjectNotesTableViewCell *cell = (BTOCSProjectNotesTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSProjectNotesTableViewCell reuseId]];
            cell.titleLabel.text = note.fullName;
            cell.dateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:note.noteDate];
            cell.timeLabel.text = [AppManager getTimeFromDate:note.noteDate];
            cell.detailLabel.text = note.note;
            
            cell.delegate = self;
            [self.tableCells addObject:cell];
        }
    }
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
//    return 0;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
//    return 0;
//}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 90.0;
}

#pragma mark - BTOCSProjectNotesTableViewCellDelegate

- (void)layoutChangeInCell:(BTBaseTableViewCell *)cell
{
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    [self trackOmniClick:OMNICLICK_OCS_ORDER_SHOWMORE];
}

#pragma mark - api

- (void)getProjectNotes
{
    if (self.getProjectNotesWebService) {
        [self.getProjectNotesWebService cancel];
        self.getProjectNotesWebService.getProjectNotesDelegate = nil;
        self.getProjectNotesWebService = nil;
    }
    
    if ([AppManager isInternetConnectionAvailable]) {
        if (self.project && self.project.projectRef) {
            self.getProjectNotesWebService = [[NLGetProjectNotes alloc] initWithProjectRef:self.project.projectRef];
            
            self.getProjectNotesWebService.getProjectNotesDelegate = self;
            if (!self.networkRequestInProgress) {
                self.networkRequestInProgress = YES;
            }
            [self.getProjectNotesWebService resume];
        }
    } else {
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:self.pageNameForOmnitureTracking];
    }
    
}

#pragma mark - API delegate

- (void)getProjectNotesWebService:(NLGetProjectNotes *)webService finishedWithResponse:(NSObject *)response
{
    self.networkRequestInProgress = NO;
    [self.project updateWithProjectNotesResponse:(NSDictionary*)response];
    [self refreshTableView];
}

- (void)getProjectNotesWebService:(NLGetProjectNotes *)webService failedWithWebServiceError:(NLWebServiceError *)error
{
    self.networkRequestInProgress = NO;
    [AppManager trackWebServiceError:error FromAPI:webService.friendlyName OnPage:self.pageNameForOmnitureTracking];
    BOOL errorHandled = [self attemptVordelProtectedAPIErrorHandlingOfWebServiceError:error];
    if (!errorHandled) {
        [self showRetryViewWithInternetStrip:NO];
    }
}

#pragma mark - retry view delegate

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    [self getProjectNotes];
}

#pragma mark - omniture

- (NSString *)pageNameForOmnitureTracking
{
    return OMNIPAGE_OCS_ORDER_PROJECTNOTES;
}

- (void)trackPageForOmniture
{
    if (self.project.productService && self.project.subStatus) {
        [self trackPageForOmnitureWithProductService:self.project.productService andOrderStatus:self.project.subStatus];
    } else {
        [super trackPageForOmniture];
    }
}

@end
