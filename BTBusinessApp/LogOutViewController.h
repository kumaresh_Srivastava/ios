#import <UIKit/UIKit.h>
#import "CDUser.h"

@class LogOutViewController;

@protocol LogOutViewControllerDelegate <NSObject>
-(void)userChoseToLogIn:(LogOutViewController *)logOutViewController;
-(void)userChoseToSwichUsers:(LogOutViewController *)LogOutViewController;
@end

@interface LogOutViewController : UIViewController

@property (nonatomic, weak) id<LogOutViewControllerDelegate> delegate;
@property (nonatomic, weak) CDUser *loggedInUser;
@end

