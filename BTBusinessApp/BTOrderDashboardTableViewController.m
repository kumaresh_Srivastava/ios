//
//  BTOrderDashboardTableViewController.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 06/11/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOrderDashboardTableViewController.h"

#import "BTOrderTrackerDashboardTableViewCell.h"
#import "MBProgressHUD.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "BTUICommon.h"
#import "BTTrackOrderDashBoardModel.h"
#import "BTOrderTrackerDetailsViewController.h"
#import "DLMTrackOrderDashBoardScreen.h"
#import "BTOrder.h"
#import "BTRetryView.h"
#import "BTEmptyDashboardView.h"
#import "BTOrderTrackerHomeViewController.h"
#import "CustomSpinnerView.h"
#import "BTOrderDashBoardTableFooterView.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "BTCug.h"
#import "NLWebServiceError.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "AppConstants.h"

#import "BTOrderTrackerDashboardViewController.h"

#import "BTOCSProject.h"
#import "BTOCSOrderDetailsTableViewController.h"
#import "BTOCSDropDownTableViewCell.h"
#import "BTOCSHorizontalSelectorTableViewCell.h"
#import "BTBackgroundMessageTableViewCell.h"
#import "BTOCSSimpleButtonTableViewCell.h"

@interface BTOrderDashboardTableViewController () <DLMTrackOrderDashBoardScreenDelegate,BTOrderDashBoardFooterDelegate,BTOCSDropDownTableViewCellDelegate,BTOCSHorizontalSelectorTableViewCellDelegate>

@property (nonatomic,strong) NSMutableArray *openOrderData;
@property (nonatomic,strong) NSMutableArray *completedOrderData;
@property (nonatomic,strong) NSMutableArray *openProjectData;
@property (nonatomic,strong) NSMutableArray *completedProjectData;
@property (nonatomic,assign) TrackOrderDashBoardType trackOrderType;

@property (nonatomic, readwrite) DLMTrackOrderDashBoardScreen *openOrderViewModel;
@property (nonatomic, readwrite) DLMTrackOrderDashBoardScreen *completedOrderViewModel;

@property (nonatomic, strong) BTOrderDashBoardTableFooterView *orderDashBoardTableFooterView;

@property (nonatomic, strong) BTOCSHorizontalSelectorTableViewCell *selectorCell;
@property (nonatomic, strong) BTOCSDropDownTableViewCell *cugSelectCell;

@end

@implementation BTOrderDashboardTableViewController

+ (BTOrderDashboardTableViewController *)getOrderDashboard
{
    BTOrderDashboardTableViewController *dashboard = [[BTOrderDashboardTableViewController alloc] initWithStyle:UITableViewStylePlain];
    return dashboard;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.title = @"Track orders";
    
    self.hidesTableWhileLoading = NO;
    
    self.openOrderViewModel = [[DLMTrackOrderDashBoardScreen alloc] initWithFirstPageOpenOrderDict:self.firstPageOpenOrderDataDict];
    self.completedOrderViewModel = [[DLMTrackOrderDashBoardScreen alloc] init];
    [self.completedOrderViewModel setProjects:[self.firstPageOpenOrderDataDict objectForKey:@"projects"]];
    
    if(!self.cugs) {
        self.cugs = [[AppDelegate sharedInstance] savedCugs];
    }
    
    self.openOrderData = [NSMutableArray new];
    self.completedOrderData = [NSMutableArray new];
    self.openProjectData = [NSMutableArray new];
    self.completedProjectData = [NSMutableArray new];
    
    self.trackOrderType = TrackFaultDashBoardTypeOpenOrder;
    
    self.orderDashBoardTableFooterView = [[[NSBundle mainBundle] loadNibNamed:@"BTOrderDashBoardTableFooterView" owner:nil options:nil] objectAtIndex:0];
    [self.orderDashBoardTableFooterView createCustomLoadingIndicator];
    [self.orderDashBoardTableFooterView hideCustomLoadingIndicator];
    [self.orderDashBoardTableFooterView hideRetryView];
    self.orderDashBoardTableFooterView.orderDashBoardFooterDelegate = self;
    
    [self.tableView setTableFooterView:self.orderDashBoardTableFooterView];
    self.tableView.tableFooterView.hidden = YES;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon_SearchButton_White"] style:UIBarButtonItemStylePlain target:self action:@selector(searchOrderReference:)];
    
    if (self.firstPageOpenOrderDataDict || [AppManager isInternetConnectionAvailable]) {
        
        [self fetchOrderDashBoardDetailsWithPageIndex:1];
        
    } else {
        
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:[self pageNameForOmnitureTracking]];
    }
    
}

- (void)registerNibs
{
    [super registerNibs];
    UINib *dataCell = [UINib nibWithNibName:@"BTOrderTrackerDashboardTableViewCell" bundle:nil];
    [self.tableView registerNib:dataCell forCellReuseIdentifier:@"orderTrackerDashboardTableViewCell"];
}

- (void)setupTableCells
{
    [super setupTableCells];

    if (!self.selectorCell) {
        self.selectorCell = (BTOCSHorizontalSelectorTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSHorizontalSelectorTableViewCell reuseId]];
        [self.selectorCell updateWithOptions:@[@"Open",@"Completed"] andSelectedOption:@"Open"];
        self.selectorCell.delegate = self;
        [self.tableCells addObject:self.selectorCell];
    }
    
    if (self.cugs && self.cugs.count>1) {
        self.cugSelectCell = (BTOCSDropDownTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSDropDownTableViewCell reuseId]];
        NSMutableArray *cugNames = [NSMutableArray new];
        NSString *currentCugName = @"";
        BTCug *currentCug = (BTCug *)[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug;
        for (BTCug *cug in self.cugs) {
            [cugNames addObject:cug.cugName];
            if ([cug.groupKey isEqualToString:currentCug.groupKey]) {
                currentCugName = cug.cugName;
            }
        }
        if (currentCugName && ![currentCugName isEqualToString:@""]) {
            [self.cugSelectCell updateWithTitle:@"Select group" values:cugNames selectedValue:currentCugName];
            self.cugSelectCell.delegate = self;
            [self.tableCells addObject:self.cugSelectCell];
        }
        
    }
}

- (void)updateTableCells
{
    [super updateTableCells];
    
    if (self.trackOrderType == TrackOrderDashBoardTypeOpenOrder) {
        self.selectorCell.selectedOption = @"Open";
    } else if (self.trackOrderType == TrackOrderDashBoardTypeCompletedOrder) {
        self.selectorCell.selectedOption = @"Completed";
    }
    
    NSMutableArray *remove = [NSMutableArray new];
    for (BTBaseTableViewCell *cell in self.tableCells) {
        if ([cell isKindOfClass:[BTOrderTrackerDashboardTableViewCell class]] || [cell isKindOfClass:[BTBackgroundMessageTableViewCell class]] || [cell isKindOfClass:[BTOCSSimpleButtonTableViewCell class]]) {
            [remove addObject:cell];
        }
    }
    if (remove.count > 0) {
        [self.tableCells removeObjectsInArray:remove];
    }
    
    if (self.trackOrderType == TrackOrderDashBoardTypeOpenOrder) {
        if (self.openOrderData.count > 0 || self.openProjectData.count > 0) {
            for (BTOCSProject *project in self.openProjectData) {
                BTOrderTrackerDashboardTableViewCell *newCell = (BTOrderTrackerDashboardTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOrderTrackerDashboardTableViewCell reuseId]];
                [newCell updateCellWithProjectData:project isOpenOrder:YES];
                [newCell.infoButton addTarget:self action:@selector(infoButton) forControlEvents:UIControlEventTouchUpInside];
                [self.tableCells addObject:newCell];
            }
            for (BTOrder *order in self.openOrderData) {
                BTOrderTrackerDashboardTableViewCell *newCell = (BTOrderTrackerDashboardTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOrderTrackerDashboardTableViewCell reuseId]];
                [newCell updateCellWithOrderData:order isOpenOrder:YES];
                [newCell.infoButton addTarget:self action:@selector(infoButton) forControlEvents:UIControlEventTouchUpInside];
                [self.tableCells addObject:newCell];
            }
        } else {
            BTBackgroundMessageTableViewCell *backgroundCell = (BTBackgroundMessageTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTBackgroundMessageTableViewCell reuseId]];
            [self.tableCells addObject:backgroundCell];
            BTOCSSimpleButtonTableViewCell *searchCell = (BTOCSSimpleButtonTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSSimpleButtonTableViewCell reuseId]];
            [searchCell.button setTitle:@"Search by order reference" forState:UIControlStateNormal];
            searchCell.button.backgroundColor = [UIColor clearColor];
            [searchCell.button setTitleColor:[BrandColours colourBackgroundBTPurplePrimaryColor] forState:UIControlStateNormal];
            [searchCell.button addTarget:self action:@selector(searchOrderReference:) forControlEvents:UIControlEventTouchUpInside];
            [self.tableCells addObject:searchCell];
        }
        
    } else if (self.trackOrderType == TrackOrderDashBoardTypeCompletedOrder) {
        if (self.completedOrderData.count > 0 || self.completedProjectData.count > 0) {
            for (BTOCSProject *project in self.completedProjectData) {
                BTOrderTrackerDashboardTableViewCell *newCell = (BTOrderTrackerDashboardTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOrderTrackerDashboardTableViewCell reuseId]];
                [newCell updateCellWithProjectData:project isOpenOrder:NO];
                [newCell.infoButton addTarget:self action:@selector(infoButton) forControlEvents:UIControlEventTouchUpInside];
                [self.tableCells addObject:newCell];
            }
            for (BTOrder *order in self.completedOrderData) {
                BTOrderTrackerDashboardTableViewCell *newCell = (BTOrderTrackerDashboardTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOrderTrackerDashboardTableViewCell reuseId]];
                [newCell updateCellWithOrderData:order isOpenOrder:NO];
                [newCell.infoButton addTarget:self action:@selector(infoButton) forControlEvents:UIControlEventTouchUpInside];
                [self.tableCells addObject:newCell];
            }
        } else {
            BTBackgroundMessageTableViewCell *backgroundCell = (BTBackgroundMessageTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTBackgroundMessageTableViewCell reuseId]];
            [self.tableCells addObject:backgroundCell];
            BTOCSSimpleButtonTableViewCell *searchCell = (BTOCSSimpleButtonTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSSimpleButtonTableViewCell reuseId]];
            [searchCell.button setTitle:@"Search by order reference" forState:UIControlStateNormal];
            searchCell.button.backgroundColor = [UIColor clearColor];
            [searchCell.button setTitleColor:[BrandColours colourBackgroundBTPurplePrimaryColor] forState:UIControlStateNormal];
            [searchCell.button addTarget:self action:@selector(searchOrderReference:) forControlEvents:UIControlEventTouchUpInside];
            [self.tableCells addObject:searchCell];
        }
        
    }
    
}


#pragma mark fetchDetails Methods

- (void)fetchOrderDashBoardDetailsWithPageIndex:(int)pageIndex {
    
    //Calling Dashboard API call
    if (self.trackOrderType == TrackOrderDashBoardTypeOpenOrder) {
        if (self.openOrderData.count == 0 && self.openProjectData.count == 0) {
            self.networkRequestInProgress = YES;
        } else {
            self.tableView.tableFooterView.hidden = NO;
            [self.orderDashBoardTableFooterView showCustomLoadingIndicator];
            [self.orderDashBoardTableFooterView startLoadingIndicatorAnimation];
        }
        [self.openOrderViewModel setPageSize:3];
        [self.openOrderViewModel setPageIndex:pageIndex];
        [self.openOrderViewModel setTabID:self.trackOrderType];
        self.openOrderViewModel.dashBoardDelegate = self;
        [self.openOrderViewModel fetchOrderDashBoardDetailsForLoggedInUser];
    } else {
        if (self.completedOrderData.count == 0 && self.completedProjectData.count == 0) {
            self.networkRequestInProgress = YES;
        } else {
            self.tableView.tableFooterView.hidden = NO;
            [self.orderDashBoardTableFooterView showCustomLoadingIndicator];
            [self.orderDashBoardTableFooterView startLoadingIndicatorAnimation];
        }
        [self.completedOrderViewModel setPageSize:3];
        [self.completedOrderViewModel setPageIndex:pageIndex];
        [self.completedOrderViewModel setTabID:self.trackOrderType];
        self.completedOrderViewModel.dashBoardDelegate = self;
        [self.completedOrderViewModel fetchOrderDashBoardDetailsForLoggedInUser];
    }
}

- (void)fetchOpenOrdersAutomatically {
    int pageIndex = _openOrderViewModel.pageIndex;
    int totalSize = _openOrderViewModel.totalSize;
    [self.orderDashBoardTableFooterView hideRetryView];
    if ((self.openOrderData.count < 9) && (((pageIndex - 1) * 3) < totalSize)) {
        if([AppManager isInternetConnectionAvailable]) {
            self.tableView.tableFooterView.hidden = NO;
            [self.orderDashBoardTableFooterView showCustomLoadingIndicator];
            [self.orderDashBoardTableFooterView startLoadingIndicatorAnimation];
            [self fetchOrderDashBoardDetailsWithPageIndex:pageIndex];
        } else {
            self.tableView.tableFooterView.hidden = NO;
            [self.orderDashBoardTableFooterView showRetryViewWithErrorMessage:kNoConnectionMessage];
            [self.orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
            //Track No Internet
            [AppManager trackNoInternetErrorOnPage:[self pageNameForOmnitureTracking]];
        }
    }
}

- (void)fetchCompletedOrdersAutomatically {
    int pageIndex = _completedOrderViewModel.pageIndex;
    int totalSize = _completedOrderViewModel.totalSize;
    [self.orderDashBoardTableFooterView hideRetryView];
    if ((self.completedOrderData.count < 9) && (((pageIndex - 1) * 3) < totalSize)) {
        if([AppManager isInternetConnectionAvailable]) {
            self.tableView.tableFooterView.hidden = NO;
            [self.orderDashBoardTableFooterView showCustomLoadingIndicator];
            [self.orderDashBoardTableFooterView startLoadingIndicatorAnimation];
            [self fetchOrderDashBoardDetailsWithPageIndex:pageIndex];
        } else {
            self.tableView.tableFooterView.hidden = NO;
            [self.orderDashBoardTableFooterView showRetryViewWithErrorMessage:kNoConnectionMessage];
            [self.orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
            //Track No Internet
            [AppManager trackNoInternetErrorOnPage:[self pageNameForOmnitureTracking]];
        }
        
    }
}

#pragma mark CancellingAPIMethods

- (void)cancelOpenOrderAPI {
    if (self.trackOrderType == TrackOrderDashBoardTypeOpenOrder) {
        self.tableView.tableFooterView.hidden = YES;
        [self.orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
        [self.orderDashBoardTableFooterView hideRetryView];
    }
    self.openOrderViewModel.dashBoardDelegate = nil;
    [self.openOrderViewModel cancelDashBoardAPIRequest];
}

- (void)cancelCompletedOrderAPI {
    if (self.trackOrderType == TrackOrderDashBoardTypeCompletedOrder) {
        self.tableView.tableFooterView.hidden = YES;
        [self.orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
        [self.orderDashBoardTableFooterView hideRetryView];
    }
    self.completedOrderViewModel.dashBoardDelegate = nil;
    [self.completedOrderViewModel cancelDashBoardAPIRequest];
}

- (void) updateUserInterfaceWithProjects:(NSArray *)projects {
    if (self.trackOrderType == TrackOrderDashBoardTypeOpenOrder) {
        [self.openProjectData removeAllObjects];
        for (BTOCSProject *project in projects) {
            if ([project.status.lowercaseString isEqualToString:@"open"]) {
                [self.openProjectData addObject:project];
            }
        }
    } else {
        [self.completedProjectData removeAllObjects];
        for (BTOCSProject *project in projects) {
            if (![project.status.lowercaseString isEqualToString:@"open"]) {
                [self.completedProjectData addObject:project];
            }
        }
    }
    [self refreshTableView];
}

- (void) updateUserInterfaceWithOrders:(NSArray *)orders {
    if (self.trackOrderType == TrackOrderDashBoardTypeOpenOrder) {
        
        [self.openOrderData addObjectsFromArray:orders];
    } else {
        
        [self.completedOrderData addObjectsFromArray:orders];
    }
    [self refreshTableView];
}

#pragma mark ActionMethods

- (void) infoButtonAction:(id)sender {
    UIButton *button = (UIButton *)sender;
    
    BTOrderTrackerDashboardTableViewCell *cell = (BTOrderTrackerDashboardTableViewCell *)[[[button superview] superview] superview];
    NSString *orderRef = cell.orderID;
    
    DDLogInfo(@"Info : Can't dispaly order %@ yet.", orderRef);
    NSString *alertMessage = [NSString stringWithFormat:@"We’re sorry but we can’t display your order %@ yet.", orderRef];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Please try again later" message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)home:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchOrderReference:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    BTOrderTrackerHomeViewController *orderTrackerInputViewController = [storyboard instantiateViewControllerWithIdentifier:@"OrderTrackerInputViewController"];
    [self trackOmniClick:OMNICLICK_ORDER_SEARCH forPage:self.pageNameForOmnitureTracking];
    
    [self.navigationController pushViewController:orderTrackerInputViewController animated:YES];
}

#pragma mark -

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL retVal = NO;
    BTBaseTableViewCell *cell = [self.tableCells objectAtIndex:indexPath.row];
    if ([cell isKindOfClass:[BTOrderTrackerDashboardTableViewCell class]]) {
        retVal = YES;
    }
    return retVal;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *retVal = nil;
    BTBaseTableViewCell *cell = [self.tableCells objectAtIndex:indexPath.row];
    if ([cell isKindOfClass:[BTOrderTrackerDashboardTableViewCell class]]) {
        retVal = indexPath;
    }
    return retVal;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTBaseTableViewCell *cell = [self.tableCells objectAtIndex:indexPath.row];
    if ([cell isKindOfClass:[BTOrderTrackerDashboardTableViewCell class]]) {
        BTOrderTrackerDashboardTableViewCell *orderCell = (BTOrderTrackerDashboardTableViewCell*)cell;
        NSObject *orderData = orderCell.orederObj;
        if ([orderData isKindOfClass:[BTOCSProject class]]) {
            // OCS screen
            BTOCSProject *project = (BTOCSProject*)orderData;
            BTOCSOrderDetailsTableViewController *vc = [BTOCSOrderDetailsTableViewController getOCSOrderDetailsViewController];
            vc.project = project;
            NSString *linkName = [NSString stringWithFormat:@"%@ %@",project.subStatus,OMNICLICK_ORDER_DETAILS];
            [self trackOmniClick:linkName forPage:self.pageNameForOmnitureTracking];
            [self.navigationController pushViewController:vc animated:YES];
        } else {
            BTOrder *order = (BTOrder*)orderData;
            //Launch TrackOrderSummary screen for Open orders.
            //Track event
            NSString *linkName = [NSString stringWithFormat:@"%@ %@",order.orderStatus,OMNICLICK_ORDER_DETAILS];
            [self trackOmniClick:linkName forPage:self.pageNameForOmnitureTracking];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            BTOrderTrackerDetailsViewController *orderTrackerDetailsViewController = [storyboard instantiateViewControllerWithIdentifier:@"OrderTrackerDetailViewController"];
            [orderTrackerDetailsViewController setOrderRef:order.orderRef];
            // uncomment following line to hardcode order for testing
            // [orderTrackerDetailsViewController setOrderRef:@"BT23R3FD"];
            [self.navigationController pushViewController:orderTrackerDetailsViewController animated:YES];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark DLMTrackOrderDashBoardScreenDelegate Methods

- (void)successfullyFetchedOrderDashBoardDataOnTrackOrderDashBoardScreen:(DLMTrackOrderDashBoardScreen *)dashBoardScreen
{
    [self setNetworkRequestInProgress:NO];
    
    if (dashBoardScreen.orders.count == 0 && dashBoardScreen.projects.count == 0) {
        
        if (self.trackOrderType == TrackOrderDashBoardTypeOpenOrder)
        {
            [self trackOmniPage:OMNIPAGE_ORDER_OPEN_EMPTY];
        } else if (self.trackOrderType == TrackOrderDashBoardTypeCompletedOrder) {
            [self trackOmniPage:OMNIPAGE_ORDER_CLOSED_EMPTY];
        }
    } else {
        
        if (dashBoardScreen.projects.count > 0) {
            [self updateUserInterfaceWithProjects:dashBoardScreen.projects];
        }
        if (dashBoardScreen.orders.count > 0) {
            [self updateUserInterfaceWithOrders:dashBoardScreen.orders];
        }
        
        if (self.trackOrderType == TrackOrderDashBoardTypeOpenOrder) {
        
            int pageIndex = self.openOrderViewModel.pageIndex;
            int totalSize = self.openOrderViewModel.totalSize;
            if ((self.openOrderData.count < 9) && (((pageIndex - 1) * 3) < totalSize)) {
                
                self.tableView.tableFooterView.hidden = NO;
                if ([AppManager isInternetConnectionAvailable]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self fetchOrderDashBoardDetailsWithPageIndex:pageIndex];
                    });
                } else {
                    self.tableView.tableFooterView.hidden = NO;
                    [self.orderDashBoardTableFooterView showRetryViewWithErrorMessage:kNoConnectionMessage];
                    [self.orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
                    //Track No Internet
                    [AppManager trackNoInternetErrorOnPage:[self pageNameForOmnitureTracking]];
                }
            } else {
                self.tableView.tableFooterView.hidden = YES;
                [self.orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
                [self.orderDashBoardTableFooterView hideRetryView];
            }
            
            if ((self.openOrderData.count == totalSize) || (self.openOrderData.count >= 9)) {
                
                self.tableView.tableFooterView.hidden = YES;
                [self.orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
                [self.orderDashBoardTableFooterView hideRetryView];
            }
            
            
        } else if (self.trackOrderType == TrackOrderDashBoardTypeCompletedOrder) {
            
            int pageIndex = self.completedOrderViewModel.pageIndex;
            int totalSize = self.completedOrderViewModel.totalSize;
            if ((self.completedOrderData.count < 9) && (((pageIndex - 1) * 3) < totalSize)) {
                
                self.tableView.tableFooterView.hidden = NO;
                if ([AppManager isInternetConnectionAvailable]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self fetchOrderDashBoardDetailsWithPageIndex:pageIndex];
                    });
                } else {
                    //Track No Internet
                    [AppManager trackNoInternetErrorOnPage:[self pageNameForOmnitureTracking]];
                }
            }  else {
                self.tableView.tableFooterView.hidden = YES;
                [self.orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
                [self.orderDashBoardTableFooterView hideRetryView];
            }
            
            if ((self.completedOrderData.count == totalSize) || (self.completedOrderData.count >= 9)) {
                self.tableView.tableFooterView.hidden = YES;
                [self.orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
                [self.orderDashBoardTableFooterView hideRetryView];
            }
        }
        
    }
}

- (void)trackorderDashBoardScreen:(DLMTrackOrderDashBoardScreen *)dashBoardScreen failedToFetchOrderDashBoardDataWithWebServiceError:(NLWebServiceError *)error {
    DDLogError(@"Order DashBoard: Fetch order DashBoard details failed");
    
    self.networkRequestInProgress = NO;
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:error];
    
    if(errorHandled) {
        [AppManager trackGenericAPIErrorOnPage:[self pageNameForOmnitureTracking]];
    } else {

        if (dashBoardScreen.projects.count > 0) {
            [self updateUserInterfaceWithProjects:dashBoardScreen.projects];
        }
        if (dashBoardScreen.orders.count > 0) {
            [self updateUserInterfaceWithOrders:dashBoardScreen.orders];
        }
        
        if (error.error.code == BTNetworkErrorCodeAPINoDataFound)
        {
            [AppManager trackNoDataFoundErrorOnPage:[self pageNameForOmnitureTracking]];
            self.tableView.tableFooterView.hidden = YES;
            [self.orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
            [self.orderDashBoardTableFooterView hideRetryView];
            [self refreshTableView];
        }
        else
        {
            if (self.trackOrderType == TrackOrderDashBoardTypeOpenOrder) {
                if (self.openOrderData.count == 0 && self.openProjectData.count == 0) {
                    [self showRetryViewWithInternetStrip:NO];
                    self.tableView.tableFooterView.hidden = YES;
                    [self.orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
                    [self.orderDashBoardTableFooterView hideRetryView];
                } else {
                    [self.orderDashBoardTableFooterView showRetryView];
                    [self.orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
                }
            } else if (self.trackOrderType == TrackOrderDashBoardTypeCompletedOrder) {
                if (self.completedOrderData.count == 0 && self.completedProjectData.count == 0) {
                    [self showRetryViewWithInternetStrip:NO];
                    self.tableView.tableFooterView.hidden = YES;
                    [self.orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
                    [self.orderDashBoardTableFooterView hideRetryView];
                } else {
                    [self.orderDashBoardTableFooterView showRetryView];
                    [self.orderDashBoardTableFooterView stopCustomLoadingIndicatorAnimation];
                }
            }
        }

    }
    
}

#pragma mark - BTRetryView delegate methods
- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView {
    
    if([AppManager isInternetConnectionAvailable]) {
        
        [self trackOmniClick:OMNICLICK_ORDER_RETRY forPage:self.pageNameForOmnitureTracking];
        
        DDLogInfo(@"Retry to fecth order Dashboard details ");
        [self hideRetryItems:YES];
        [self fetchOrderDashBoardDetailsWithPageIndex:1];
    } else {
        [self.retryView updateRetryViewWithInternetStrip:YES];
        //Track No Internet
        [AppManager trackNoInternetErrorOnPage:[self pageNameForOmnitureTracking]];
    }
}

#pragma mark - BTOrderDashBoardTableFooterDelegate

- (void) orderDashBoardTableFooterView:(BTOrderDashBoardTableFooterView *)orderDashBoardFooterView retryBUttonActionMethod:(id)sender {
    
    if(![AppManager isInternetConnectionAvailable]) {
        [orderDashBoardFooterView showRetryViewWithErrorMessage:kNoConnectionMessage];
        //Track No Internet
        [AppManager trackNoInternetErrorOnPage:[self pageNameForOmnitureTracking]];
        return;
    }
    
    if (self.trackOrderType == TrackOrderDashBoardTypeOpenOrder) {
        [orderDashBoardFooterView hideRetryView];
        [orderDashBoardFooterView showCustomLoadingIndicator];
        [self fetchOrderDashBoardDetailsWithPageIndex:self.openOrderViewModel.pageIndex];
    } else {
        [orderDashBoardFooterView hideRetryView];
        [orderDashBoardFooterView showCustomLoadingIndicator];
        [self fetchOrderDashBoardDetailsWithPageIndex:self.completedOrderViewModel.pageIndex];
    }
}

#pragma mark - BTOCSDropDownTableViewCellDelegate

- (void)cell:(BTOCSDropDownTableViewCell *)cell updatedSelection:(NSString *)selected {
    for (BTCug *cug in self.cugs) {
        if ([cug.cugName isEqualToString:selected]) {
            BTCug *currentCug = (BTCug *)[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug;
            if (![cug.groupKey isEqualToString:currentCug.groupKey]) {
                // change of cug; reset data and reload
                self.trackOrderType = TrackOrderDashBoardTypeOpenOrder;
                [self.openProjectData removeAllObjects];
                [self.openOrderData removeAllObjects];
                [self.completedProjectData removeAllObjects];
                [self.completedOrderData removeAllObjects];
                [self refreshTableView];
                [self.openOrderViewModel changeSelectedCUGTo:cug];
                if([AppManager isInternetConnectionAvailable])
                {
                    [self fetchOrderDashBoardDetailsWithPageIndex:1];
                }
                else
                {
                    self.tableView.tableFooterView.hidden = YES;
                    [self showRetryViewWithInternetStrip:YES];
                    //Track No Internet
                    [AppManager trackNoInternetErrorOnPage:[self pageNameForOmnitureTracking]];
                }
            }
            
        }
    }
}

#pragma mark - BTOCSHorizontalSelectorTableViewCellDelegate

- (void)horizontalSelectorCell:(BTOCSHorizontalSelectorTableViewCell *)cell updatedSelection:(NSString *)selected {
    if ([selected.lowercaseString isEqualToString:@"open"]) {
        self.trackOrderType = TrackOrderDashBoardTypeOpenOrder;
        [self refreshTableView];
    } else if ([selected.lowercaseString isEqualToString:@"completed"]) {
        self.trackOrderType = TrackOrderDashBoardTypeCompletedOrder;
        [self refreshTableView];
        if (self.completedOrderData.count == 0) {
            [self fetchOrderDashBoardDetailsWithPageIndex:1];
        }
    }
}

#pragma mark - omniture

- (NSString *)pageNameForOmnitureTracking
{
    NSString *pageName = @"";
    if(self.cugs.count == 1)
    {
        if(self.trackOrderType == TrackOrderDashBoardTypeOpenOrder)
        {
            pageName = OMNIPAGE_ORDER_OPEN;
        }
        else
        {
            pageName = OMNIPAGE_ORDER_COMPLETED;
            
        }
    }
    else
    {
        if(self.trackOrderType == TrackOrderDashBoardTypeOpenOrder)
        {
            pageName = OMNIPAGE_ORDER_OPEN_CUG;
            
        }
        else
        {
            pageName = OMNIPAGE_ORDER_COMPLETED_CUG;
        }
    }
    
    return  pageName;
    
}





@end
