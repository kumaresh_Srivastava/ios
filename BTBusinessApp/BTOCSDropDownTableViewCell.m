//
//  BTOCSDropDownTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSDropDownTableViewCell.h"

@interface BTOCSDropDownTableViewCell () <UIAlertViewDelegate>

@end

@implementation BTOCSDropDownTableViewCell

+ (NSString *)reuseId
{
    return @"BTOCSDropDownTableViewCell";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.dropdownOutlineView.layer.borderWidth = 1.0f;
    self.dropdownOutlineView.layer.cornerRadius = 5.0f;
    self.dropdownOutlineView.layer.borderColor = [BrandColours colourBtNeutral80].CGColor;
    
    UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showOptions)];
    [self.dropdownOutlineView addGestureRecognizer:tapped];
    
    self.detailLabel.text = @"None selected";
    
    self.isOptionsShowing = NO;
    
}

- (void)updateWithTitle:(NSString *)title values:(NSArray<NSString *> *)values selectedValue:(NSString *)selected
{
    self.titleLabel.text = title;
    if (values && values.count) {
        self.values = [NSArray arrayWithArray:values];
    }
    if (selected && ![selected isEqualToString:@""]) {
        self.selectedValue = selected;
        self.detailLabel.text = selected;
    } else {
        self.detailLabel.text = @"";
    }
    
}

- (void)updateWithTitle:(NSString *)title values:(NSArray<NSString *> *)values selectedValue:(NSString *)selected andPlaceHolder:(NSString *)placeholder
{
    self.titleLabel.text = title;
    if (values && values.count) {
        self.values = [NSArray arrayWithArray:values];
    }
    
    if (placeholder && ![placeholder isEqualToString:@""]) {
        self.detailLabel.text = placeholder;
    } else {
        self.detailLabel.text = @"";
    }
    
    if (selected && ![selected isEqualToString:@""]) {
        self.selectedValue = selected;
        if (!placeholder || [placeholder isEqualToString:@""]) {
            self.detailLabel.text = selected;
        }
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)showOptions {
    if (self.values && self.values.count > 1) {
        //NSString *alertTitle = [NSString stringWithFormat:@"Select %@",self.titleLabel.text];
        //NSString *alertTitle = @"";
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        __weak typeof(self) weakSelf = self;
        for (NSString *value in self.values) {
            UIAlertAction *action = [UIAlertAction actionWithTitle:value style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [weakSelf selectedValue:value];
                if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(cell:updatedSelection:)]) {
                    [weakSelf.delegate cell:weakSelf updatedSelection:value];
                }
                weakSelf.isOptionsShowing = NO;
                [alert dismissViewControllerAnimated:YES completion:nil];
            }];
            [alert addAction:action];
        }
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                         style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                             weakSelf.isOptionsShowing = NO;
                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                             
                                                         }];
        [alert addAction:cancel];
        alert.popoverPresentationController.sourceView = self;
        alert.popoverPresentationController.sourceRect = self.dropdownOutlineView.frame;
        alert.popoverPresentationController.canOverlapSourceViewRect = NO;
        
        if (self.delegate && [self.delegate isKindOfClass:[UIViewController class]]) {
            [(UIViewController*)self.delegate presentViewController:alert animated:YES completion:^{
                weakSelf.isOptionsShowing = YES;
            }];
        } else {
            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:^{
                weakSelf.isOptionsShowing = YES;
            }];
        }
    }
    
}

- (void)selectedValue:(NSString*)value {
    self.selectedValue = value;
    self.detailLabel.text = value;
}

@end
