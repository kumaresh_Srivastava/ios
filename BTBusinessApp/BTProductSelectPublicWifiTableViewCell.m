//
//  BTProductSelectPublicWifiTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 01/03/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTProductSelectPublicWifiTableViewCell.h"
#import "AppConstants.h"

@implementation BTProductSelectPublicWifiTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCellWithServiceId:(NSString *)serviceId
{
    if (serviceId) {
        UIFont *regularFont = [UIFont fontWithName:kBtFontRegular size:14.0];
        UIFont *boldFont = [UIFont fontWithName:kBtFontBold size:14.0];
        NSMutableAttributedString *fullText = [[NSMutableAttributedString alloc] initWithString:@"ID: " attributes:@{NSFontAttributeName:regularFont}];
        NSMutableAttributedString *boldText = [[NSMutableAttributedString alloc] initWithString:serviceId attributes:@{NSFontAttributeName:boldFont}];
        [fullText appendAttributedString:boldText];
        [self.serviceIDValueLabel setAttributedText:fullText];
    } else {
        [self.serviceIDValueLabel setText:@""];
    }
}

- (void)updateCellWithGuestWiFiStatus:(NSString *)status
{
    NSString *statusString;
    if (status && ![status isEqualToString:@""]) {
        statusString = status;
    } else {
        statusString = @"Can't connect";
    }
    UIFont *regularFont = [UIFont fontWithName:kBtFontRegular size:14.0];
    UIFont *boldFont = [UIFont fontWithName:kBtFontBold size:14.0];
    NSMutableAttributedString *fullText = [[NSMutableAttributedString alloc] initWithString:@"Guest Wi-Fi: " attributes:@{NSFontAttributeName:regularFont}];
    NSMutableAttributedString *boldText = [[NSMutableAttributedString alloc] initWithString:status attributes:@{NSFontAttributeName:boldFont}];
    [fullText appendAttributedString:boldText];
    [self.guestWifiStatusLabel setAttributedText:fullText];
}

- (void)updateCellWithLocation:(NSString *)location
{
    if (location) {
        UIFont *regularFont = [UIFont fontWithName:kBtFontRegular size:14.0];
        UIFont *boldFont = [UIFont fontWithName:kBtFontBold size:14.0];
        NSMutableAttributedString *fullText = [[NSMutableAttributedString alloc] initWithString:@"Location: " attributes:@{NSFontAttributeName:regularFont}];
        NSMutableAttributedString *boldText = [[NSMutableAttributedString alloc] initWithString:location attributes:@{NSFontAttributeName:boldFont}];
        [fullText appendAttributedString:boldText];
        [self.locationValueLabel setAttributedText:fullText];
    } else {
        [self.locationValueLabel setText:@""];
    }
}

@end
