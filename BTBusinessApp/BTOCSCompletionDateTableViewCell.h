//
//  BTOCSCompletionDateTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 02/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface BTOCSCompletionDateTableViewCell : BTBaseTableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;

@end

NS_ASSUME_NONNULL_END
