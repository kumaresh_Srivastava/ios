#ifndef DownloadSpeedCellType_h
#define DownloadSpeedCellType_h

typedef NS_ENUM(NSInteger, DownloadSpeedCellType) {
    MeetsMGALRequirement,
    DoesntMeetMGALRequirement1stAttempt,
    DoesntMeetMGALRequirement2ndAttempt,
    UnknownMGAL,
    NonMGAL1stAttempt,
    NonMGAL2ndAttempt
};

#endif /* DownloadSpeedCellType_h */
