//
//  BTSelectHubTableViewCell.m
//  BTBusinessApp
//

#import "BTSelectHubTableViewCell.h"

@implementation BTSelectHubTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    UIFont *font = [UIFont fontWithName:@"BTFont-Regular" size:16.0];
    
    NSDictionary* selectedAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[BrandColours colourBtWhite], NSForegroundColorAttributeName, font, NSFontAttributeName, nil];
    NSDictionary* normalAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[BrandColours colourBtLightBlack], NSForegroundColorAttributeName, font, NSFontAttributeName, nil];
    
    UIImage *unhighlightedImage = [[UIImage imageNamed:@"Segmented_Control_Left"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
    UIImage *highlightedImage = [[UIImage imageNamed:@"Segmented_Control_Right"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
    
    [_hubSegmentedControl setTintColor:UIColor.clearColor];
    [_hubSegmentedControl setBackgroundImage:unhighlightedImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [_hubSegmentedControl setBackgroundImage:highlightedImage forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [_hubSegmentedControl setDividerImage:[UIImage imageNamed:@"blank_divider"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [_hubSegmentedControl setTitleTextAttributes:selectedAttributes forState:UIControlStateSelected];
    [_hubSegmentedControl setTitleTextAttributes:normalAttributes forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onHubChanged:(id)sender {
    switch (_hubSegmentedControl.selectedSegmentIndex)
    {
    case 0:
        [_changeHubHealthcheckDelegate changeHubHealthcheck:0];
        break;
    case 1:
        [_changeHubHealthcheckDelegate changeHubHealthcheck:1];
        break;
    default:
        break;
    }
}
@end
