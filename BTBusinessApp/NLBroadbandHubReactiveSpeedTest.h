//
//  NLBroadbandHubReactiveSpeedTest.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 28/01/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import "NLAPIGEEAuthenticatedWebService.h"

NS_ASSUME_NONNULL_BEGIN

@interface NLBroadbandHubReactiveSpeedTest : NLAPIGEEAuthenticatedWebService

- (instancetype)initWithSerialNumber:(NSString*)serialNum serviceID:(NSString*)serviceId hashCode:(NSString*)hash andForceExecute:(BOOL)force;

@end

NS_ASSUME_NONNULL_END
