#import "Configuration.h"
@interface Configuration()
- (NSDictionary*)getPlistDir;
@end

@implementation Configuration 

+ (instancetype)sharedInstance {
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

-(id)init {
    
    self = [super init];
    
    NSDictionary *plistDir = [self getPlistDir];
    
    _config.environment = [plistDir objectForKey:@"environment"];
    _config.baseVordelUrl = [plistDir objectForKey:@"baseVordelUrl"];
    _config.baseSaasUrl = [plistDir objectForKey:@"baseSaasUrl"];
    _config.baseApigeeUrl = [plistDir objectForKey:@"baseApigeeUrl"];
    
    return self;
}

- (NSDictionary*)getPlistDir {
    
    NSString *infoPlistPath = [[NSBundle mainBundle] pathForResource: @"Info" ofType: @"plist"];
    NSDictionary *plistDir = [NSDictionary dictionaryWithContentsOfFile: infoPlistPath];
    NSString *configPlist = [plistDir objectForKey: @"PXLConfigurationPlist"];
    
    NSString *pathToConfigPlist = [[NSBundle mainBundle] pathForResource: [NSString stringWithFormat:@"%@", configPlist] ofType: @"plist"];
    
    return [NSDictionary dictionaryWithContentsOfFile: pathToConfigPlist];
}

@end
