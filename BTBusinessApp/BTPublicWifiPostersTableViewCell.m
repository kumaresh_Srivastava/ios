//
//  BTPublicWifiPostersTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 20/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTPublicWifiPostersTableViewCell.h"
#import "AppManager.h"
#import "OmnitureManager.h"

@implementation BTPublicWifiPostersTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.posterOrderButton.layer setCornerRadius:self.posterOrderButton.frame.size.height/10];
    self.posterOrderButton.layer.borderWidth = 1;
    self.posterOrderButton.layer.borderColor = [BrandColours colourTextBTPinkColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (CGFloat)heightForCell
{
    return 232.0f;
}

- (IBAction)posterOrderAction:(id)sender {
    [self trackOmniClick:OMNICLICK_PUBLICWIFI_MANAGE_ORDERNOW];
    [AppManager openURL:kGuestWiFiPostersUrl];
}

#pragma mark - Omniture Methods

- (void)trackOmniClick:(NSString *)linkTitle
{
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",OMNIPAGE_PUBLICWIFI_MANAGE,linkTitle]];
}

@end
