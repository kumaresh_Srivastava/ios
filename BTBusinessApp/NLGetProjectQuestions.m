//
//  NLGetProjectQuestions.m
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLGetProjectQuestions.h"
#import "NSObject+APIResponseCheck.h"
#import "NLWebServiceError.h"
#import "NLConstants.h"
#import "NLWebService+BackEndAPIErrorHandling.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "BTCug.h"
#import "CDCug+CoreDataClass.h"

@implementation NLGetProjectQuestions

- (instancetype)initWithProjectRef:(NSString*)projectRef {
    NSMutableDictionary *projectDetails = [NSMutableDictionary new];
    [projectDetails setObject:projectRef forKey:@"ProjectRef"];
    [projectDetails setObject:@"BTBusinessApp" forKey:@"Source"];
    [projectDetails setObject:@"" forKey:@"UniqueKey"];
    NSDictionary *projectDetailRequest = [NSDictionary dictionaryWithObjectsAndKeys:projectDetails,@"ProjectDetails",@"0",@"QuestionID", nil];
    
    self = [self initWithProjectDetails:projectDetailRequest];
    return self;
}

- (instancetype)initWithProjectDetails:(NSDictionary*)projectDetails {
    NSString *contactId = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.contactId;
    
    NSString *endPointURLString = [NSString stringWithFormat: @"/bt-business-auth/v1/ocs-orders/project-questions/%@?src=%@&Qid=%@&Cid=%@",
                                   projectDetails[@"ProjectDetails"][@"ProjectRef"],
                                   projectDetails[@"ProjectDetails"][@"Source"],
                                   projectDetails[@"QuestionID"],
                                   contactId];
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    return self;
}

- (NSString *)friendlyName
{
    return @"getProjectQuestions";
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    if ([responseObject valueForKeyPath:@"QuestionGroups"])
    {
        [self.getProjectQuestionsDelegate getProjectQuestionsWebService:self finishedWithResponse:responseObject];
    }
    else
    {
        NLWebServiceError *webServiceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponse];
        
        DDLogError(@"Invalid response json object in webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Invalid response json object in webservice with URL: %@", [[task currentRequest] URL]]));
        
        [self.getProjectQuestionsDelegate getProjectQuestionsWebService:self failedWithWebServiceError:webServiceError];
    }
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.getProjectQuestionsDelegate getProjectQuestionsWebService:self failedWithWebServiceError:webServiceError];
}

@end
