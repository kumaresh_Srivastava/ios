//
//  BTOCSDateTimeSelectTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 18/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSDateTimeSelectTableViewCell.h"
#import <JTCalendar/JTCalendar.h>
#import "AppManager.h"
#import "BTOCSBaseTableViewController.h"


@interface BTOCSDateTimeSelectTableViewCell () <JTCalendarDelegate>

@property (strong, nonatomic) JTCalendarMenuView *calanderMenuView;
@property (strong, nonatomic) JTCalendarManager *calendarManager;
@property (strong, nonatomic) JTVerticalCalendarView *calanderContentView;
@property (nonatomic, strong) UIViewController *calendarVC;

@property (nonatomic, strong) UIDatePicker *timePicker;

@end

@implementation BTOCSDateTimeSelectTableViewCell

+ (NSString *)reuseId
{
    return @"BTOCSDateTimeSelectTableViewCell";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.datePickOutlineView.layer.borderWidth = 1.0f;
    self.datePickOutlineView.layer.cornerRadius = 5.0f;
    self.datePickOutlineView.layer.borderColor = [BrandColours colourBtNeutral80].CGColor;
    
    UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDateOptions)];
    [self.datePickOutlineView addGestureRecognizer:tapped];
    
    self.timePickOutlineView.layer.borderWidth = 1.0f;
    self.timePickOutlineView.layer.cornerRadius = 5.0f;
    self.timePickOutlineView.layer.borderColor = [BrandColours colourBtNeutral80].CGColor;
    
    UITapGestureRecognizer *timeTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showTimeOptions)];
    [self.timePickOutlineView addGestureRecognizer:timeTapped];
    
    self.timePicker = [[UIDatePicker alloc] init];
    [self.timePicker setDatePickerMode:UIDatePickerModeTime];
    
    self.timeField.inputView = self.timePicker;
    
    UIToolbar *keyboardAccesory = [[UIToolbar alloc] init];
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonAction)];
    UIBarButtonItem *spacing = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonAction)];
    keyboardAccesory.items = @[cancelBtn,spacing,doneBtn];
    [keyboardAccesory sizeToFit];
    self.timeField.inputAccessoryView = keyboardAccesory;
    
    self.selectedDateLabel.text = @"Select date";
    self.selectedTimeLabel.text = @"Select time";
    self.timeField.text = @"Select time";
    
    [self createCalanderView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) createCalanderView {
    
    self.calendarManager = [JTCalendarManager new];
    self.calendarManager.delegate = self;
    
    self.calanderMenuView = [[JTCalendarMenuView alloc] initWithFrame:CGRectMake(0, 0, kScreenSize.width, 40)];
    self.calanderMenuView.backgroundColor = [UIColor whiteColor];
    
    self.calanderContentView = [[JTVerticalCalendarView alloc] init];
    self.calanderContentView.backgroundColor = [UIColor whiteColor];
    
    [self.calendarManager setMenuView:self.calanderMenuView];
    [self.calendarManager setContentView:self.calanderContentView];
    if (self.selectedDateTime) {
        [self.calendarManager setDate:self.selectedDateTime];
    } else {
        [self.calendarManager setDate:[NSDate date]];
    }
    
}

- (void)showDateOptions {
    if ([self.delegate isKindOfClass:[BTOCSBaseTableViewController class]]) {
        [(BTOCSBaseTableViewController*)self.delegate trackOmniClick:@"Calendar"];
    }
    self.calendarVC = [[UIViewController alloc] init];
    self.calendarVC.view = self.calanderContentView;
    if (self.selectedDateTime) {
        [self.calendarManager setDate:self.selectedDateTime];
        self.calendarVC.title = [AppManager NSStringFromNSDateWithMonthYearWithSpaceeFormat:self.selectedDateTime];
    } else {
        self.calendarVC.title = [AppManager NSStringFromNSDateWithMonthYearWithSpaceeFormat:[NSDate date]];
    }
    self.calendarVC.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.calendarVC.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveAndCloseCalendarView)];
    [[(UIViewController*)self.delegate navigationController] pushViewController:self.calendarVC animated:YES];
}

- (void)showTimeOptions {
    if (!self.timeField.isFirstResponder) {
        [self.timeField becomeFirstResponder];
    } else {
        [self.timeField resignFirstResponder];
    }
}

- (void)cancelButtonAction{
    [self.timeField resignFirstResponder];
}

- (void)doneButtonAction{
    [self updateWithDate:nil andTime:self.timePicker.date];
    [self.timeField resignFirstResponder];
}

#pragma mark - button action

- (void)saveAndCloseCalendarView {
    [self.calendarVC.navigationController popViewControllerAnimated:YES];
}

- (void)updateWithDate:(NSDate*)date andTime:(NSDate*)time
{
    NSDate *dayDate;
    NSDate *timeDate;
    
    if (date) {
        dayDate = date;
    } else if (self.selectedDateTime) {
        dayDate = [AppManager dateFromString:[AppManager getDayMonthYearFromDate:self.selectedDateTime]];
    } else {
        dayDate = [NSDate date];
    }
    
    if (time) {
        timeDate = time;
    } else if (self.selectedDateTime) {
        timeDate = self.selectedDateTime;
    } else {
        timeDate = [NSDate date];
    }
    
    NSDateComponents *dayComponents = [[NSCalendar currentCalendar] components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:dayDate];
    
    NSDateComponents *timeComponents = [[NSCalendar currentCalendar] components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:timeDate];
    
    NSDateComponents *combined = [[NSDateComponents alloc] init];
    combined.year = dayComponents.year;
    combined.month = dayComponents.month;
    combined.day = dayComponents.day;
    combined.hour = timeComponents.hour;
    combined.minute = timeComponents.minute;
    
    self.selectedDateTime = [[NSCalendar currentCalendar] dateFromComponents:combined];
    
    self.selectedDateLabel.text = [AppManager NSStringFromNSDateWithSlashFormat:self.selectedDateTime];
    self.timeField.text = [AppManager getTimeFromDate:self.selectedDateTime];
    
    [self.timePicker setDate:self.selectedDateTime];
}

#pragma mark - CalendarManager delegate

// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    
    [dayView.dotView setHidden:YES];
    [dayView.circleView setHidden:YES];
    [dayView.textLabel setFont:[UIFont fontWithName:kBtFontBold size:15.0]];
    // Today
    // Selected date
    //BOOL isDateHighlighted = NO;
    if(self.selectedDateTime && [self.calendarManager.dateHelper date:self.selectedDateTime isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        //isDateHighlighted = YES;
        dayView.circleView.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Other month
    else if(![self.calendarManager.dateHelper date:self.calanderContentView.date isTheSameMonthThan:dayView.date]){
        dayView.circleView.hidden = YES;
        dayView.textLabel.textColor = [BrandColours colourBtNeutral50];
    }
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
        dayView.textLabel.textColor = [BrandColours colourTextBTPurplePrimaryColor];
    }
    
    
    if([dayView isFromAnotherMonth]){
        dayView.circleView.hidden = YES;
        dayView.textLabel.textColor = [BrandColours colourMyBtLightPurple];;
    }
    
    NSDate *date = [NSDate date];
    if (([date compare:dayView.date] == NSOrderedDescending)) {
        dayView.textLabel.textColor = [BrandColours colourBtNeutral50];
    }
    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.day = 90;
    NSDate *newDate = [cal dateByAddingComponents:components toDate:date options:0];
    
    if (([newDate compare:dayView.date] == NSOrderedAscending)) {
        dayView.textLabel.textColor = [BrandColours colourBtNeutral50];
    }
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    if ([self.calendarManager.dateHelper date:dayView.date isEqualOrAfter:[NSDate date]] && ![self.calendarManager.dateHelper date:dayView.date isTheSameDayThan:[NSDate date]]) {
        [self updateWithDate:dayView.date andTime:nil];
        self.calendarVC.title = [AppManager NSStringFromNSDateWithMonthYearWithSpaceeFormat:self.selectedDateTime];
        // Animation for the circleView
        dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
        
        
        // Don't change page in week mode because block the selection of days in first and last weeks of the month
        if(self.calendarManager.settings.weekModeEnabled){
            return;
        }
        
        if(![self.calendarManager.dateHelper date:self.calanderContentView.date isTheSameMonthThan:dayView.date]){
            if([self.calanderContentView.date compare:dayView.date] == NSOrderedAscending){
                [self.calanderContentView loadNextPageWithAnimation];
            }
            else{
                [self.calanderContentView loadPreviousPageWithAnimation];
            }
        }
        
        
        [UIView transitionWithView:dayView
                          duration:.3
                           options:0
                        animations:^{
                            dayView.circleView.transform = CGAffineTransformIdentity;
                            [self.calendarManager reload];
                        } completion:nil];
    }
    
}

#pragma mark - CalendarManager delegate - Page mangement

// Used to limit the date for the calendar, optional
- (BOOL)calendar:(JTCalendarManager *)calendar canDisplayPageWithDate:(NSDate *)date
{
    return [self.calendarManager.dateHelper date:date isEqualOrAfter:[NSDate date]];
}

//if the following methods are not getting used delete them
- (void)calendarDidLoadNextPage:(JTCalendarManager *)calendar
{
    
}

- (void)calendarDidLoadPreviousPage:(JTCalendarManager *)calendar
{
    
}

@end
