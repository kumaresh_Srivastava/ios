//
//  BTOCSSectionHeaderTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 05/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSSectionHeaderTableViewCell.h"

@implementation BTOCSSectionHeaderTableViewCell

+ (NSString *)reuseId
{
    return @"BTOCSSectionHeaderTableViewCell";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
