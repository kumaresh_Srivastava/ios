//
//  BTMobileServiceTableViewCell.h
//  BTBusinessApp
//
//  Created by kumaresh shrivastava on 24/06/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BTMobileServiceTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *serviceIDLable;

- (void)updateMobileServicesCell:(NSString*)serviceIdString;

@end

NS_ASSUME_NONNULL_END
