//
//  BT4GAssureTableViewCell.m
//  BTBusinessApp
//

#import "BT4GAssureTableViewCell.h"

@implementation BT4GAssureTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    _orderNowButton.layer.borderColor = [BrandColours colourTextBTPinkColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)orderNowPressed:(id)sender {
    // when user presses 'Order now' ...
    NSLog(@"Calling 4G cell delegate.");
    [self.BT4GAssureTableViewCellDelegate userPressedCellFor4GAssure:self];
}
@end
