//
//  BTPublicWifiBaseTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 19/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SafariServices/SafariServices.h>

#import "AppConstants.h"
#import "BrandColours.h"

#import "BTShadowedView.h"
#import "BTOrderStatusLabel.h"

@interface BTBaseTableViewCell : UITableViewCell

+ (NSString*)reuseId;
- (CGFloat)heightForCell;

@end
