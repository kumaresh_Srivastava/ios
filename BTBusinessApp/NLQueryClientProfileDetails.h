//
//  NLQueryClientProfileDetails.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 05/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLVordelAuthenticationProtectedWebService.h"

@class NLQueryClientProfileDetails;
@class NLWebServiceError;
@class BTClientProfileDetails;

@protocol NLQueryClientProfileDetailsDelegate <NLVordelAuthenticationProtectedWebServiceDelegate>

- (void)queryClientProfileDetailsWebService:(NLQueryClientProfileDetails *)webService successfullyFetchedClientProfileDetails:(BTClientProfileDetails *)details;

- (void)queryClientProfileDetailsWebService:(NLQueryClientProfileDetails *)webService failedToFetchClientProfileDetailsWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLQueryClientProfileDetails : NLVordelAuthenticationProtectedWebService

@property (nonatomic, readonly) NSString *serviceCode;
@property (nonatomic, weak) id <NLQueryClientProfileDetailsDelegate> queryClientProfileDetailsDelegate;

- (instancetype)initWithServiceCode:(NSString *)serviceCode;

@end
