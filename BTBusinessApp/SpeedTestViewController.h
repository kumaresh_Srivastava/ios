#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SpeedTestViewController : UIViewController

+ (SpeedTestViewController *)getSpeedTestViewController;

@property (nonatomic, strong) NSString *serviceID;
@property (nonatomic, strong) NSString *postcode;
@property (nonatomic, strong) NSString *productName;
@property (nonatomic, strong) NSString *serialNumber;
@property (nonatomic, strong) NSString *hashCode;

@end

NS_ASSUME_NONNULL_END
