//
//  BTProductSelectPublicWifiTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 01/03/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTProductSelectPublicWifiTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UILabel *serviceNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *serviceIDValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *guestWifiStatusLabel;
@property (strong, nonatomic) IBOutlet UILabel *locationValueLabel;

- (void)updateCellWithServiceId:(NSString*)serviceId;
- (void)updateCellWithGuestWiFiStatus:(NSString*)status;
- (void)updateCellWithLocation:(NSString*)location;

@end
