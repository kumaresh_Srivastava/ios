//
//  BTHubHealthCheckTableViewCell.h
//  BTBusinessApp
//

#import <UIKit/UIKit.h>

@interface BTHubHealthCheckTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *healthCheckTitle;
@property (weak, nonatomic) IBOutlet UILabel *healthCheckDescription;
@property (weak, nonatomic) IBOutlet UIImageView *healthCheckHub;

-(void)updateCellWithHealthcheckCellData:(NSDictionary*)healthcheckData;

@end
