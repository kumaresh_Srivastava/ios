//
//  BTOCSSimpleDisclosureTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

@interface BTOCSSimpleDisclosureTableViewCell : BTBaseTableViewCell

@property (strong, nonatomic) IBOutlet UILabel *label;
@property (strong, nonatomic) IBOutlet UIImageView *disclosureIcon;
@property (strong, nonatomic) IBOutlet UIView *separatorLineView;

@end
