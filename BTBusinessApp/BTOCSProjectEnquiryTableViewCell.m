//
//  BTOCSProjectEnquiryTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 02/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSProjectEnquiryTableViewCell.h"
#import "BTOCSEnquiry.h"
#import "AppManager.h"

@implementation BTOCSProjectEnquiryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (NSString *)reuseId
{
    return @"BTOCSProjectEnquiryTableViewCell";
}

- (void)updateWithEnquiry:(BTOCSEnquiry*)enquiry
{
    self.dateLabel.text = [AppManager getDayMonthYearFromDate:enquiry.enquiryDate];
    self.timeLabel.text = [AppManager getTimeFromDate:enquiry.enquiryDate];
    self.subjectLabel.text = enquiry.enquiryCategory;
    self.indicatorImageView.image = nil;
    //[self.indicatorImageView addConstraint:[NSLayoutConstraint constraintWithItem:self.indicatorImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:0.0f]];
}

@end
