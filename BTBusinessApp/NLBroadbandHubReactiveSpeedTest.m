//
//  NLBroadbandHubReactiveSpeedTest.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 28/01/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import "NLBroadbandHubReactiveSpeedTest.h"

#define dummy YES

@interface NLBroadbandHubReactiveSpeedTest ()

@property (nonatomic, strong) NSString *hashCode;

@end

@implementation NLBroadbandHubReactiveSpeedTest

- (instancetype)initWithSerialNumber:(NSString *)serialNum serviceID:(NSString *)serviceId hashCode:(NSString *)hash andForceExecute:(BOOL)force
{
    NSString *forceEx = @"false";
    if (force)
        forceEx = @"true";
    if(hash)
        self.hashCode = hash;
    
    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/broadband-hub/%@/reactive-speedtest?serviceId=%@&sourceSystem=%@&forceTestExecution=%@",serialNum,serviceId,@"BTBusinessApp",forceEx];
    
    self = [super initWithMethod:@"GET" parameters:@"" andEndpointUrlString:endPointURLString];
    
    return self;
}

- (NSDictionary *)httpHeaderFields
{
    NSMutableDictionary *headers = [NSMutableDictionary dictionaryWithDictionary:[super httpHeaderFields]];
    if (self.hashCode) {
        [headers setObject:self.hashCode forKey:@"hashCode"];
    }
    [headers setObject:@"application/json" forKey:@"Accept"];
    return [headers copy];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *resultDic = [responseObject isKindOfClass:[NSDictionary class]]?(NSDictionary*)responseObject:nil;
    
    if (resultDic && [resultDic objectForKey:@"status"]) {
        NSString *statusOfRequest = [resultDic objectForKey:@"status"];
        if ([statusOfRequest.lowercaseString isEqualToString:@"pass"] || [statusOfRequest.lowercaseString isEqualToString:@"fail"] || [statusOfRequest.lowercaseString isEqualToString:@"nonmgal"]) {
            [self.delegate webService:self finshedWithSuccessResponse:resultDic];
        } else {
            [self.delegate webService:self failedWithError:[NLAPIGEEWebServiceError apigeeWebServiceNetworkErrorWithErrorCode:@"" andDescription:statusOfRequest]];
        }
    } else {
        NLWebServiceError *webServiceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponse];
        DDLogError(@"Invalid response json object in webservice with URL: %@", [[task currentRequest] URL]);
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Invalid response json object in webservice with URL: %@", [[task currentRequest] URL]]));
        [self.delegate webService:self failedWithError:webServiceError];
    }
    
//    if ([statusOfRequest.lowercaseString isEqualToString:@"undetermined"] || [statusOfRequest.lowercaseString isEqualToString:@"unknown"]) {
//        [self.delegate webService:self failedWithError:[NLAPIGEEWebServiceError apigeeWebServiceNetworkErrorWithErrorCode:@"0" andDescription:statusOfRequest]];
//    } else {
//        BOOL stillInProgess = FALSE;
//        if(statusOfRequest) {
//            stillInProgess = [statusOfRequest isEqualToString:@"REQUESTED"];
//        }
//
//        if (resultDic && !stillInProgess)
//        {
//            [self.delegate webService:self finshedWithSuccessResponse:resultDic];
//        }
//        else if(stillInProgess) {
//
//            NLWebServiceError *webServiceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponse];
//            [self.delegate webService:self failedWithError:webServiceError];
//        }
//        else
//        {
//            NLWebServiceError *webServiceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponse];
//
//            DDLogError(@"Invalid response json object in webservice with URL: %@", [[task currentRequest] URL]);
//
//            BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Invalid response json object in webservice with URL: %@", [[task currentRequest] URL]]));
//            [self.delegate webService:self failedWithError:webServiceError];
//        }
//    }
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    [self.delegate webService:self failedWithError:webServiceError];
}

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task
                                                                                                 andResponseObject:responseObject];
    if (!errorToBeReturned) {
        //local error checking
        NSDictionary *responseDic = (NSDictionary*)responseObject;
        if ([responseDic objectForKey:@"code"] && [responseDic objectForKey:@"message"]) {
            NSString *code = @"";
            if ([[responseDic objectForKey:@"code"] isKindOfClass:[NSString class]]) {
                code = [responseDic objectForKey:@"code"];
            } else if ([[responseDic objectForKey:@"code"] isKindOfClass:[NSNumber class]]) {
                code = [(NSNumber*)[responseDic objectForKey:@"code"] stringValue];
            }
            NSString *message = [responseDic objectForKey:@"message"];
            if (code.integerValue != 0) {
                errorToBeReturned = [NLAPIGEEWebServiceError apigeeWebServiceNetworkErrorWithErrorCode:code andDescription:message];
            }
        }
    }
    return errorToBeReturned;
}

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andError:(NSError *)error
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task
                                                                                                          andError:error];
    if (!errorToBeReturned) {
        if (task.response && [task.response isKindOfClass:[NSHTTPURLResponse class]]) {
            NSDictionary *headers = [(NSHTTPURLResponse*)task.response allHeaderFields];
            NSString *message = [headers objectForKey:@"APIGW-Exception"];
            NSString *code = @"";
            if ([[headers objectForKey:@"APIGW-Exception-Code"] isKindOfClass:[NSString class]]) {
                code = [headers objectForKey:@"APIGW-Exception-Code"];
            } else if ([[headers objectForKey:@"APIGW-Exception-Code"] isKindOfClass:[NSNumber class]]) {
                code = [(NSNumber*)[headers objectForKey:@"APIGW-Exception-Code"] stringValue];
            } else {
                code = [[NSNumber numberWithInteger:error.code] stringValue];
            }
            errorToBeReturned = [NLAPIGEEWebServiceError apigeeWebServiceNetworkErrorWithErrorCode:code andDescription:message];
        }
    }
    
    return errorToBeReturned;
}

@end
