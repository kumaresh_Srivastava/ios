#import <UIKit/UIKit.h>
#import "DLMSpeedTestScreen.h"
#import "DownloadSpeedCellType.h"


NS_ASSUME_NONNULL_BEGIN

@class DownloadSpeedTableViewCell;
@protocol DownloadSpeedTableViewCellDelegate

- (void)userPressedRetest;
- (void)userPressedReportFault;
- (void)userPressedDone;

@end

@interface DownloadSpeedTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *downloadSpeedLabel;
@property (weak, nonatomic) IBOutlet UILabel *uploadSpeedLabel;
@property (weak, nonatomic) IBOutlet UILabel *mainDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIView *downloadContainerView;
@property (weak, nonatomic) IBOutlet UIView *uploadContainerView;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;

@property (nonatomic) NSString* currentOmniturePage;

@property (nonatomic) DownloadSpeedCellType currentCellType;

@property (nonatomic) id<DownloadSpeedTableViewCellDelegate> delegate;

- (void)setUpCellForContext:(DLMSpeedTestScreen *)speedTestScreen;

@end

NS_ASSUME_NONNULL_END
