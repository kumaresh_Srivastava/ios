//
//  NLGetProjectSummary.h
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLAPIGEEAuthenticatedWebService.h"

@class NLGetProjectSummary;
@class NLWebServiceError;

@protocol NLGetProjectSummaryDelegate <NLAPIGEEAuthenticationProtectedWebServiceDelegate>

- (void)getProjectSummaryWebService:(NLGetProjectSummary *)webService finishedWithResponse:(NSObject *)response;

- (void)getProjectSummaryWebService:(NLGetProjectSummary *)webService failedWithWebServiceError:(NLWebServiceError *)error;

@optional
- (void)timedOutWithoutSuccessForGetProjectSummaryWebService:(NLGetProjectSummary*)webService;

@end

@interface NLGetProjectSummary : NLAPIGEEAuthenticatedWebService

@property (nonatomic) NSDictionary* projectDetails;
@property (nonatomic) NSString* returnDataType;

@property (nonatomic, weak) id <NLGetProjectSummaryDelegate> getProjectSummaryDelegate;

- (instancetype)initWithProjectRef:(NSString*)projectRef;
- (instancetype)initWithProjectDetails:(NSDictionary*)projectDetails andReturnDataType:(NSString*)returnDataType;

@end
