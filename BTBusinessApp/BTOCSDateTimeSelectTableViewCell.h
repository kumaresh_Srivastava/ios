//
//  BTOCSDateTimeSelectTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 18/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@class BTOCSDateTimeSelectTableViewCell;

@protocol BTOCSDateTimeSelectTableViewCellDelegate <NSObject>

- (void)cell:(BTOCSDateTimeSelectTableViewCell*)cell updatedDateTimeSelection:(NSDate*)selected;

@end

@interface BTOCSDateTimeSelectTableViewCell : BTBaseTableViewCell
@property (strong, nonatomic) IBOutlet UILabel *questionLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UITextField *timeField;
@property (strong, nonatomic) IBOutlet UILabel *selectedDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *selectedTimeLabel;
@property (strong, nonatomic) IBOutlet UIView *datePickOutlineView;
@property (strong, nonatomic) IBOutlet UIView *timePickOutlineView;

@property (strong, nonatomic) NSDate *selectedDateTime;
@property (strong, nonatomic) id<BTOCSDateTimeSelectTableViewCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
