//
//  BTOCSProjectInfoHeaderTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 03/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface BTOCSProjectInfoHeaderTableViewCell : BTBaseTableViewCell

@property (strong, nonatomic) IBOutlet UILabel *projectRefLabel;
@property (strong, nonatomic) IBOutlet UILabel *productLabel;
@property (strong, nonatomic) IBOutlet UILabel *siteLabel;

- (void)updateWithPorjectRef:(NSString*)projectRef andProduct:(NSString*)product andSite:(NSString*)site;

@end

NS_ASSUME_NONNULL_END
