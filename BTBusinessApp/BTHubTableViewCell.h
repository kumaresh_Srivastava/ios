//
//  BTHubTableViewCell.h
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTServiceStatusDetail.h"

NS_ASSUME_NONNULL_BEGIN

@interface BTHubTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *hubImageView;

- (void)updateCellWithServiceStatusDetail:(BTServiceStatusDetail *)serviceStatusDetail andAffectedAreaText:(NSString *)affectedAreaText;

@end

NS_ASSUME_NONNULL_END
