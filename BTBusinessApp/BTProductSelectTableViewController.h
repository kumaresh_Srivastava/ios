//
//  BTProductSelectTableViewController.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 01/03/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BTAccountSelectTableViewController.h"
#import "BTAsset.h"

@interface BTProductSelectTableViewController : UITableViewController

@property (nonatomic) BTAccountSelectContext context;
@property (nonatomic, assign) BTUser *currentUser;
@property (nonatomic, assign) BTAsset *selectedAsset;
@property (nonatomic, copy) NSString *groupKey;
@property (nonatomic) NSArray *cugs;

- (void)refreshAPICall;

@end
