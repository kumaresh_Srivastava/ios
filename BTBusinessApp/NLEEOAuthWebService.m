//
//  NLEEOAuthWebService.m
//  BTBusinessApp
//

#import "NLEEOAuthWebService.h"
#import "AppConstants.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "AFNetworking.h"
#import "CDApp.h"
#import "NLWebServiceError.h"

#import <NSString+URLEncode.h>



@interface NLEEOAuthWebService () {
    
}

@end

@implementation NLEEOAuthWebService

-(instancetype)init {
    NSString *endPointURLString = [NSString stringWithFormat:@"/oauth/accesstoken?grant_type=client_credentials"];
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    
    return self;
}

- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *httpHeaders = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    NSData *basicAuthCredentials = [[NSString stringWithFormat:@"%@:%@", @"860gYqi4QhAziXnlG3nOwEPbxAT0zaia", @"uBR7C0FPVbwFpWST"] dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64AuthCredentials = [basicAuthCredentials base64EncodedStringWithOptions:(NSDataBase64EncodingOptions)0];
    [httpHeaders setObject:[NSString stringWithFormat:@"Basic %@", base64AuthCredentials] forKey:@"Authorization"];
    return [httpHeaders copy];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    // PROCESS EE OAUTH SERVICE RESPONSE HERE
    BTAuthenticationToken *token = [[BTAuthenticationToken alloc] initWithEETokenData:responseObject];
    [_eeOAuthServiceDelegate eeOAuthWebService:self loginSuccesfulWithToken:token];
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [self.eeOAuthServiceDelegate eeOAuthWebService:self failedWithError:webServiceError];
}

#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andError:(NSError *)error
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andError:error];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        
        
        // (hds) Nothing to check the error cases here.
    }
    
    return errorToBeReturned;
}

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // PROCESS REQUEST AND DETERMINE ERROR
    }
    else
    {
        // HANDLE ERROR
    }
    
    return errorToBeReturned;
}

@end
