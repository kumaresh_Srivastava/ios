//
//  BTOCSFileSelectTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 19/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@class BTOCSFileSelectTableViewCell;

@protocol BTOCSFileSelectTableViewCellDelegate <NSObject>

- (void)cell:(BTBaseTableViewCell*)cell updatedSelection:(NSData*)selected;
- (void)layoutChangeInCell:(BTBaseTableViewCell*)cell;


@end

@interface BTOCSFileSelectTableViewCell : BTBaseTableViewCell
@property (strong, nonatomic) IBOutlet UILabel *questionLabel;
@property (strong, nonatomic) IBOutlet UIButton *uploadButton;
@property (strong, nonatomic) IBOutlet UIView *fileView;
@property (strong, nonatomic) IBOutlet UILabel *filenameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *deleteIcon;

@property (nonatomic, strong) NSString *fileName;
@property (nonatomic, strong) NSData *fileData;

@property (nonatomic) id<BTOCSFileSelectTableViewCellDelegate> delegate;

- (void)updateWithFilename:(NSString*)filename andFileData:(NSData*)data;
- (void)updateWithNoFile;

@end

NS_ASSUME_NONNULL_END
