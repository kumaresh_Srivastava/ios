//
//  BTOCSDateSelectTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@class BTOCSDateSelectTableViewCell;

@protocol BTOCSDateSelectTableViewCellDelegate <NSObject>

- (void)cell:(BTOCSDateSelectTableViewCell*)cell updatedSelection:(NSDate*)selected;

@end

@interface BTOCSDateSelectTableViewCell : BTBaseTableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIView *outlineView;
@property (strong, nonatomic) IBOutlet UILabel *detailLabel;
@property (strong, nonatomic) IBOutlet UIImageView *dateSelectIcon;
@property (strong, nonatomic) IBOutlet UILabel *questionLabel;

@property (nonatomic,strong) NSDate *dateSelected;

@property (nonatomic) id<BTOCSDateSelectTableViewCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
