//
//  NLGetProjectEnquiries.m
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLGetProjectEnquiries.h"
#import "NSObject+APIResponseCheck.h"
#import "NLWebServiceError.h"
#import "NLConstants.h"
#import "NLWebService+BackEndAPIErrorHandling.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "BTCug.h"
#import "CDCug+CoreDataClass.h"

@implementation NLGetProjectEnquiries

- (instancetype)initWithProjectRef:(NSString *)projectRef
{
    NSMutableDictionary *projectDetails = [NSMutableDictionary new];
    [projectDetails setObject:projectRef forKey:@"ProjectRef"];
    [projectDetails setObject:@"BTBusinessApp" forKey:@"Source"];
    [projectDetails setObject:@"" forKey:@"UniqueKey"];
    
    self = [self initWithProjectDetails:projectDetails];
    return self;
}

- (instancetype)initWithProjectDetails:(NSDictionary*)projectDetails {
    NSString *contactId = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.contactId;
    
    NSString *endPointURLString = [NSString stringWithFormat: @"/bt-business-auth/v1/ocs-orders/project-enquiry/%@?src=%@&Cid=%@",
                                   projectDetails[@"ProjectRef"],
                                   projectDetails[@"Source"],
                                   contactId];
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    return self;
}

- (NSString *)friendlyName
{
    return @"getProjectEnquiries";
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    [self.getProjectEnquiriesDelegate getProjectEnquiriesWebService:self finishedWithResponse:responseObject];
    
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.getProjectEnquiriesDelegate getProjectEnquiriesWebService:self failedWithWebServiceError:webServiceError];
}

@end
