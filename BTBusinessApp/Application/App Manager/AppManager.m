//
//  AppManager.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/12/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "AppManager.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "RSA.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "BTOrderSiteContactModel.h"
#import "NSObject+APIResponseCheck.h"
#import "OmnitureManager.h"
#import <AFNetworkReachabilityManager.h>
#import "Reachability.h"
#import "NLWebServiceError.h"
#import "NLVordelWebServiceError.h"
#import "NLAPIGEEWebServiceError.h"
#import "Configuration.h"

NSString *const kNLBaseURLNONVordelURLModelA = @"eric1-dmze2e-ukb.bt.com";
NSString *const kNLBaseURLNONVordelURLProd = @"secure.business.bt.com";
NSString *const kNLBaseURLVordelURLModelA = @"mapi-ref.robt.bt.co.uk:53082";
NSString *const kNLBaseURLVordelURLProd = @"mapi.robt.bt.co.uk:53082";
NSString *const kNLBaseURLEEURLProd = @"api.ee.co.uk";
NSString *const kNLBaseURLEEURLModelA = @"api-test1.ee.co.uk";
NSString *kBTServerType;
BTAuthenticationType const kBTAuthenticationType = BTAuthenticationTypeVordel;
NSString *const kAppCenterIDDevTest = @"482f48b9-3d3a-443c-947e-caf3e69d9fdc";
NSString *const kAppCenterIDLive = @"82f03547-4ba0-4ebe-acd8-71a37c1ff736";

NSString *const kForgotBtIdUrl = @"ForgotUserName/retrieve";
NSString *const kForgotPasswordUrl = @"ForgotPassword/retrieve";
NSString *const kRegistrationUrl = @"registration/retrieve";
BOOL const kIsMobileServiceAssetsEnabled = NO;
//Production Urls
//NSString *const kFaultsChatScreenUrl = @"https://btbusiness.custhelp.com/app/chat/chatForm3/flag/business_app/q_id/116";
//NSString *const kFaultsChatScreenUrl = @"https://secure.business.bt.com/faultmanagement/online/FaultReport.aspx";
NSString *const kCloudVoiceFaultsChatScreenUrl = @"https://secure.business.bt.com/faultmanagement/online/FaultReport.aspx?faultType=BTCV%20Service";
NSString *const kReportFaultUrl = @"https://secure.business.bt.com/FaultManagement/Online/FaultReport.aspx?faultType=Broadband&frompage=custpage";
NSString *const kReportaFaultHomeUrl = @"https://secure.business.bt.com/faultmanagement/online/FaultReport.aspx?";
NSString *const kFaultsChatScreenUrl = @"https://secure.business.bt.com/faultmanagement/online/FaultReport.aspx?s_intcid=btb_intlink_busapp_report_fault";
NSString *const kOrdersChatScreenUrl = @"https://btbusiness.custhelp.com/app/chat/chatForm2/flag/business_app/q_id/204";
NSString *const kBillsChatScreenUrl = @"https://btbusiness.custhelp.com/app/chat/chatForm1/flag/business_app/q_id/153";
NSString *const kCloudVoiceExpressChatScreenUrl = @"https://btbusiness.custhelp.com/app/chat/chatForm7/q_id/272/";
NSString *const kFAQChatScreenUrl = @"https://btbusiness.custhelp.com/app/home/";
NSString *const k4GAssureOrderNowUrl = @"https://business.bt.com/products/broadband-and-internet/4g-assure/?s_intcid=btb_intlink_New_4g_assure_presales";
NSString *const kUpgradeYourHubUrl = @"https://btbusiness.custhelp.com/app/chat/chatForm7/q_id/239/scenario/business_app?s_intcid=btb_intlink_myaccount_upgrade_your_hub_chat_now";
NSString *const kGuestWiFiOrderNowUrl = @"https://business.bt.com/guest-wifi/?s_intcid=btb_intlink_busapp_guest_wifi_presales";
NSString *const kGuestWiFiPostersUrl = @"http://www.btguestwifiprint.com";
NSString *const kSpeedTestGuaranteeUrl = @"https://business.bt.com/products/broadband-and-internet/speed-guarantee";
NSString *const kBroadbandSpeedTipsUrl = @"https://btbusiness.custhelp.com/app/answers/detail/a_id/13451/c/5107?s_intcid=btb_intlink_busapp_broadband_speed_tips";
NSString *const kWifiSpeedTipsUrl = @"https://btbusiness.custhelp.com/app/answers/detail/a_id/43366/c/5108?s_intcid=btb_intlink_busapp_wi-fi_speed_tips";
NSString *const kSmartHubUrl = @"https://business.bt.com/sales-comms/smart-hub/?s_intcid=btb_intlink_busapp_smart_hub_presale";
// Newlly added
NSString *const kReportAFaultBroadBandUrl = @"https://secure.business.bt.com/faultmanagement/online/FaultReport.aspx?faultType=Broadband";
NSString *const kReportAFaultPhoneUrl = @"https://secure.business.bt.com/faultmanagement/online/FaultReport.aspx?faultType=Business%20PSTN%20Service";
NSString *const kReportAFaultEmailUrl = @"https://secure.business.bt.com/faultmanagement/online/FaultReport.aspx?faultType=Broadband&fromPage=Email";
NSString *const kReportAFaultBtCloudVoiceServicelUrl = @"https://secure.business.bt.com/faultmanagement/online/FaultReport.aspx?faultType=BTCV%20Service";

//ModelA
/*NSString *const kFaultsChatScreenUrl = @"https://btbusiness--tst1.custhelp.com/app/chat/chatForm3/flag/business_app/q_id/116";
NSString *const kOrdersChatScreenUrl = @"https://btbusiness--tst1.custhelp.com/app/chat/chatForm2/flag/business_app/q_id/204";
NSString *const kBillsChatScreenUrl = @"https://btbusiness--tst1.custhelp.com/app/chat/chatForm1/flag/business_app/q_id/153";
NSString *const kFAQChatScreenUrl = @"https://btbusiness--tst1.custhelp.com/app/home/";
 */

NSString *const kTermsAndConditionsUrl = @"https://business.bt.com/terms/";//@"http://www2.bt.com/static/i/btretail/panretail/terms/pdfs/bt1165.pdf";
NSString *const kBroadbandFAQUrl = @"https://btbusiness.custhelp.com/app/categories/detail/c/5055";
NSString *const kPhonelineFAQUrl = @"https://btbusiness.custhelp.com/app/categories/detail/c/5056";
NSString *const kOfficePhoneFAQUrl = @"https://btbusiness.custhelp.com/app/categories/detail/c/5057";
NSString *const kEmailFAQUrl = @"https://btbusiness.custhelp.com/app/categories/detail/c/5059";
NSString *const kMobileFAQUrl = @"https://btbusiness.custhelp.com/app/categories/detail/c/5060";
NSString *const kBillingFAQUrl = @"https://btbusiness.custhelp.com/app/categories/detail/c/5058";

@interface AppManager()

@property (nonatomic) NSString *deepLinkedActivationCode;
@property (nonatomic) BOOL isDeeplinkAlertShownOnSignin;
@property (nonatomic) BOOL isVersionCheckFinished; //To perform version check once per app


@end

@implementation AppManager

- (id)init {
    self = [super init];

    if (self) {
        // Initialize Reachability
         [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }

    kBTServerType = [[Configuration sharedInstance] config].environment;

    return self;
}

- (void)dealloc {
    // Stop Notifier
  
         [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
    
}

#pragma mark Public Methods

+ (AppManager *)sharedAppManager
{
    static AppManager *_sharedAppManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedAppManager = [[AppManager alloc] init];
    });
    
    return _sharedAppManager;
}

+ (NSString *)baseURLStringFromBaseURLType:(NLBaseURLType)baseURLType
{
    NSString *baseURLString = nil;
        
        switch (baseURLType) {
                
            case NLBaseURLTypeVordel:
            {
                baseURLString = [[Configuration sharedInstance] config].baseVordelUrl;
                break;
            }
                
            case NLBaseURLTypeNonVordel:
            {
                baseURLString = [[Configuration sharedInstance] config].baseSaasUrl;
                break;
            }
            case NLBaseURLTypeEE:
            {
                baseURLString = [[Configuration sharedInstance] config].baseApigeeUrl;
                break;
            }
                
            default:
                break;
        }
    
    return baseURLString;
}

+ (NSString *)reportSpeedTestFaultForServiceID:(NSString *)serviceId
{
    return [NSString stringWithFormat:@"https://%@/FaultManagement/Online/FaultReport.aspx?faultType=Broadband&frompage=custpage&serviceID=%@",[AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel],serviceId];
}

+ (BOOL)isInternetConnectionAvailable {
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return NO;
    } else {
        return YES;
    }
}

+ (NSString *)getRSAEncryptedString:(NSString*)stringToBeEncrypted; {
    NSString *publicRSAKey = [SAMKeychain passwordForService:kKeyChainServiceBTPublicKey account:@"GlobalAccount"];
    NSString *encryptedString = [RSA encryptString:stringToBeEncrypted publicKey:publicRSAKey];
    return encryptedString;
}

+ (NSString*)stringByTrimmingWhitespaceInString : (NSString*) string {
    return [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+ (NSString *)documentDirectoryPath{

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}

-(NSDictionary *)fetchSegmentedControlFontDict
{
    UIFont *font = [UIFont fontWithName:kBtFontRegular size:16.0];
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
                                                           forKey:NSFontAttributeName];
    return attributes;
}

+ (NSString *)getSubstring:(NSString *)value betweenString:(NSString *)separator
{
    NSRange firstInstance = [value rangeOfString:separator];
    NSString* finalStr = [value substringFromIndex:firstInstance.location];
    return finalStr;
}

+ (NSString*)removeMiliSecondFromDateString:(NSString*)dateString {
    
    // get the substring till T then find for dot
    if ([dateString rangeOfString:@"T"].location == NSNotFound) {
        return dateString;
    } else {
        NSString *strTillT = [self getSubstring:dateString betweenString:@"T"];
        NSArray* subStrArray = [strTillT componentsSeparatedByString:@"."];
        if(subStrArray.count>0) {
            NSRange firstInstance = [dateString rangeOfString:@"T"];
            NSString* firstStr = [dateString substringToIndex:firstInstance.location];
            NSString* finalSTr = [firstStr stringByAppendingString:subStrArray[0]];
            return finalSTr;
        } else {
            return dateString;
        }
    }
   
}

#pragma mark - Date format related helper methods

+ (NSDate *)dateFromString:(NSString *)dateString
{
    NSDate *date = nil;
    // BUG 17190 and BUG 17189
    NSString* finalDateString = [self removeMiliSecondFromDateString:dateString];// To remove milisec which are not compatable to some lower version of iOS phones.
    date = [AppManager NSDateWithUTCFormatFromNSString:finalDateString];
   
    if (!date) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        date = [dateFormatter dateFromString:finalDateString];
    }
    if (!date) {
        date = [AppManager NSDateWithMonthNameWithSpaceFormatFromNSString:finalDateString];
    }
    
    if (!date) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        date = [dateFormatter dateFromString:finalDateString];
    }
    
    if (!date) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm"];
        date = [dateFormatter dateFromString:finalDateString];
    }
    if (!date) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SS'Z'"];
        date = [dateFormatter dateFromString:finalDateString];
    }
    if (!date) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSZ"];
        date = [dateFormatter dateFromString:finalDateString];
    }
    if (!date) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SS"];
        date = [dateFormatter dateFromString:finalDateString];
    }
    
    return date;
}

+ (NSDate *)NSDateWithMonthNameWithSpaceFormatFromNSString:(NSString *)dateString
{
    NSString *dateFormat = @"dd MMM yyyy";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:dateFormat];
    
    NSDate *date = nil;
    
    if ([dateFormatter dateFromString:dateString] != nil) {
        
        date = [dateFormatter dateFromString:dateString];
        return date;
    }
    
    return date;
}

+ (NSString *)NSStringFromNSDateWithMonthNameWithSpaceeFormat:(NSDate *)date
{
    NSString *dateFormat = @"dd MMM yyyy";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSString *dateString = nil;
    
    if ([dateFormatter stringFromDate:date] != nil) {
        
        dateString = [dateFormatter stringFromDate:date];
    }
    
    return dateString;
}

+ (NSString *)NSStringFromNSDateWithMonthYearWithSpaceeFormat:(NSDate *)date
{
    NSString *dateFormat = @"MMM yyyy";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSString *dateString = nil;
    
    if ([dateFormatter stringFromDate:date] != nil) {
        
        dateString = [dateFormatter stringFromDate:date];
    }
    
    return dateString;
}

+ (NSString *)NSStringFromNSDateWithSlashFormat:(NSDate *)date
{
    NSString *dateFormat = @"dd/MM/yyyy";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSString *dateString = nil;
    
    if ([dateFormatter stringFromDate:date] != nil) {
        
        dateString = [dateFormatter stringFromDate:date];
    }
    
    return dateString;
}

+ (NSString *)NSDateWithMonthNameFromNSString : (NSString*)string {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:[string substringWithRange:NSMakeRange(0, 10)]];
    if (![AppManager isValidDate:date]) {
        return nil;
    }
    
    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    NSString * updatedString = [dateFormatter stringFromDate:date];
    return updatedString;
}

+ (NSString *)NSStringWithOCSFormatFromNSDate : (NSDate *)date {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSString * timestamp = [dateFormatter stringFromDate:date];
    return timestamp;
}

+ (NSString *)NSStringWithOmnitureFormatFromNSDate : (NSDate *)date {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSString * timestamp = [dateFormatter stringFromDate:date];
    return timestamp;
}

+ (NSString *)NSStringFromNSDateWithMonthNameWithHiphenFormat:(NSDate *)date
{
    NSString *dateFormat = @"dd-MMM-yyyy";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSString *dateString = nil;
    
    if ([dateFormatter stringFromDate:date] != nil) {
        
        dateString = [dateFormatter stringFromDate:date];
    }
    
    return dateString;
}

+ (NSString *)NSStringFromNSDateWithMonthHourDateFormat:(NSDate *)date
{
    NSString *dateFormat = @"dd MMM, HH:mm";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSString *dateString = nil;
    
    if ([dateFormatter stringFromDate:date] != nil) {
        
        dateString = [dateFormatter stringFromDate:date];
    }
    
    return dateString;
}

+ (NSString *)NSStringFromNSDateWithReverseDateFormat:(NSDate *)date
{
    NSString *dateFormat = @"yyyy-MM-dd";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSString *dateString = nil;
    
    if ([dateFormatter stringFromDate:date] != nil) {
        
        dateString = [dateFormatter stringFromDate:date];
    }
    
    return dateString;
}

+ (NSString *)getDayDateMonthFormattedStringFromDate:(NSDate *)date {
    NSString *dateFormat = @"EEE dd MMM";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSString *dateString = nil;
    
    if ([dateFormatter stringFromDate:date] != nil) {
        
        dateString = [dateFormatter stringFromDate:date];
    }
    
    return dateString;
}

+ (NSDate *)NSDateWithUTCFormatFromNSString:(NSString *)dateString{
    NSString *dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:dateFormat];
    
    NSDate *date = nil;
    
    if ([dateFormatter dateFromString:dateString] != nil) {
        
        date = [dateFormatter dateFromString:dateString];
        return date;
    }
    
    return date;
}

+ (NSString *)getTimeFromDate:(NSDate *)date
{
    NSString *dateFormat = @"HH:mm";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSString *dateString = nil;
    
    if ([dateFormatter stringFromDate:date] != nil) {
        
        dateString = [dateFormatter stringFromDate:date];
    }
    
    return dateString;
}

+ (NSString *)getDayMonthYearFromDate:(NSDate *)date
{
    NSString *dateFormat = @"dd MMM yyyy";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSString *dateString = nil;
    
    if ([dateFormatter stringFromDate:date] != nil) {
        
        dateString = [dateFormatter stringFromDate:date];
    }
    
    return dateString;
}

+ (NSString *)getDayMonthFromDate:(NSDate *)date
{
    NSString *dateFormat = @"dd MMM";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSString *dateString = nil;
    
    if ([dateFormatter stringFromDate:date] != nil) {
        
        dateString = [dateFormatter stringFromDate:date];
    }
    
    return dateString;
}

+(NSInteger)getHourFromDate:(NSDate *)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
    return [components hour];
}

+(NSInteger)getMinuteFromDate:(NSDate *)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
    return [components minute];
}

+(BOOL)isValidDate:(NSDate *)date
{
    if (date == nil)
    {
        return false;
    }
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
    if ([components year]== 0001) {
        return false;
    }
    return true;
}

+ (BOOL)isDate:(NSDate *)firstDate isEqualTo:(NSDate *)secondDate
{
    if (firstDate == nil || secondDate == nil)
    {
        return false;
    }
    
    unsigned int flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSDateComponents* components = [calendar components:flags fromDate:firstDate];
    
    NSDate* dateOnly = [calendar dateFromComponents:components];
    
    if ([dateOnly compare:secondDate] == NSOrderedSame) {
        return true;
    }
    
    return false;
}

#pragma mark - Get User titles and security questions
+ (NSArray *)getBTUserTitles {
    
    NSArray *titlesArray;
    
    titlesArray = [AppDelegate sharedInstance].viewModel.arrayOfTitles;
    
    if ((titlesArray != nil) && ([titlesArray count] > 0)) {
        
        return titlesArray;
    }
    else
    {
        NSError *error;
        NSPropertyListFormat format;
        NSString *plistPath;
        NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                  NSUserDomainMask, YES) objectAtIndex:0];
        plistPath = [rootPath stringByAppendingPathComponent:@"Data.plist"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
            plistPath = [[NSBundle mainBundle] pathForResource:@"Data" ofType:@"plist"];
        }
        
        NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
        
        NSDictionary *temp = (NSDictionary *)[NSPropertyListSerialization
                                              propertyListWithData:plistXML options:NSPropertyListMutableContainersAndLeaves format:&format error:&error];
        
        
        if (!temp) {
            NSLog(@"Error reading plist: %@, format: %lu", error.description, (unsigned long)format);
        }
        
        titlesArray = [NSArray arrayWithArray:[temp objectForKey:@"Titles"]];
        
        return titlesArray;
    }
    
}

+ (NSArray *)getBTUserSecurityQuestions {
    
    NSArray *securityQuestionsArray;
    
    securityQuestionsArray = [AppDelegate sharedInstance].viewModel.arrayOfSecurityQuestions;
    
    if ((securityQuestionsArray != nil) && (securityQuestionsArray > 0)) {
        
        return securityQuestionsArray;
    }
    else
    {
        NSError *error;
        NSPropertyListFormat format;
        NSString *plistPath;
        NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                  NSUserDomainMask, YES) objectAtIndex:0];
        plistPath = [rootPath stringByAppendingPathComponent:@"Data.plist"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
            plistPath = [[NSBundle mainBundle] pathForResource:@"Data" ofType:@"plist"];
        }
        
        NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
        
        NSDictionary *temp = (NSDictionary *)[NSPropertyListSerialization
                                              propertyListWithData:plistXML options:NSPropertyListMutableContainersAndLeaves format:&format error:&error];
        
        
        if (!temp) {
            NSLog(@"Error reading plist: %@, format: %lu", error.description, (unsigned long)format);
        }
        
        securityQuestionsArray = [NSArray arrayWithArray:[temp objectForKey:@"SecurityQuestions"]];
        
        return securityQuestionsArray;
    }
    
}


#pragma mark - Related to site contact helper methods
+ (NSString *)getContactNumberFromSiteContact:(BTOrderSiteContactModel *)siteContact{
    
    
    //mobile ->work(with -extension) -> home
    if(!siteContact)
        return nil;
    
    NSString *contatNumber = @"";
    NSString *homePhone = siteContact.homePhone;
    NSString *workPhone = siteContact.workPhone;
    NSString *mobile = siteContact.mobile;
    NSString *phone = siteContact.phone;
    NSString *numberExtention = siteContact.extension;;
    
    if(siteContact.preferredContact && [siteContact.preferredContact validAndNotEmptyStringObject]){
        
        if([[siteContact.preferredContact lowercaseString]  isEqualToString:@"phone"]){
            
            contatNumber = phone;
        }
        else if([[siteContact.preferredContact lowercaseString]  isEqualToString:@"mobile"]){
            
            contatNumber = mobile;
        }
        else if([[siteContact.preferredContact lowercaseString]  isEqualToString:@"work"]){
            
            contatNumber = workPhone;
            
            if([numberExtention validAndNotEmptyStringObject]){
                
                contatNumber = [workPhone stringByAppendingString:[NSString stringWithFormat:@"-%@", numberExtention]];
            }
            
        }
        else if([[siteContact.preferredContact lowercaseString]  isEqualToString:@"home"]){
            
            contatNumber = homePhone;
        }
        
    }
    else{
        
        if([phone validAndNotEmptyStringObject]){
            
            contatNumber = phone;
        }
        else if([mobile validAndNotEmptyStringObject]){
            
            contatNumber = mobile;
        }
        else if([workPhone validAndNotEmptyStringObject]){
            
            contatNumber = workPhone;
            
            if([numberExtention validAndNotEmptyStringObject]){
                
                contatNumber = [workPhone stringByAppendingString:[NSString stringWithFormat:@"-%@", numberExtention]];
            }
            
        }
        else if([homePhone validAndNotEmptyStringObject]){
            
            contatNumber = homePhone;
        }
        
    }
    
    
    return contatNumber;
}

#pragma mark - Omniture related methods

+ (NSString *)ADBMobileConfigPath
{
    NSString *configPath=@"";
    if ([kBTServerType isEqualToString:@"BTServerTypeProd"]) {
        
        if (![[NSBundle mainBundle].bundleIdentifier.lowercaseString containsString:@"internal"]) {
            configPath = [[NSBundle mainBundle] pathForResource:@"ADBMobileConfig" ofType:@"json"];
        } else {
            configPath = [[NSBundle mainBundle] pathForResource:@"ADBMobileConfig-DEV" ofType:@"json"];
        }
    } else if([kBTServerType isEqualToString:@"BTServerTypeModelA"]) {
        configPath = [[NSBundle mainBundle] pathForResource:@"ADBMobileConfig-DEV" ofType:@"json"];
    } else {
        configPath = [[NSBundle mainBundle] pathForResource:@"ADBMobileConfig" ofType:@"json"];
    }
    return configPath;
}

+ (void)trackOmnitureKeyTask:(NSString *)keyTask forPageName:(NSString *)pageName
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    [data setValue:keyTask forKey:kOmniKeyTask];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}



+ (void)trackNoInternetErrorOnPage:(NSString *)pageName
{
   [OmnitureManager trackError:OMNIERROR_NO_INTERNET onPageWithName:pageName contextInfo:[AppManager getOmniDictionary]];
}

+ (void)trackNoInternetErrorBeforeLoginOnPage:(NSString *)pageName
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Not Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackError:OMNIERROR_NO_INTERNET onPageWithName:pageName contextInfo:data];
}

+ (void)trackGenericAPIErrorOnPage:(NSString *)pageName
{
     [OmnitureManager trackError:OMNIERROR_SOMETHING_WENT_WRONG onPageWithName:pageName contextInfo:[AppManager getOmniDictionary]];
    
     NSString* errorStr = [NSString stringWithFormat:@"%@:Page:%@",OMNIERROR_SOMETHING_WENT_WRONG,pageName];
    NSDictionary* errorDic = @{@"ErrorDetails":errorStr,@"LoginStatus":@"Logged In"};
    [MSAnalytics trackEvent:@"Service Error" withProperties:errorDic flags:MSFlagsDefault];
}

+ (void)trackError:(NSString *)errorDetail FromAPI:(NSString *)apiName OnPage:(NSString *)pageName
{
    NSString *errorString = [@"SE:" stringByAppendingString:apiName];
    if (errorDetail && ![errorDetail isEqualToString:@""]) {
        errorString = [NSString stringWithFormat:@"%@:%@",errorString,errorDetail];
    }
    // just in case - trim any trailing colon
    if ([[errorString substringFromIndex:(errorString.length-1)] isEqualToString:@":"]) {
        errorString = [errorString substringToIndex:errorString.length-1];
    }
    [OmnitureManager trackError:errorString onPageWithName:pageName contextInfo:[AppManager getOmniDictionary]];
    
    NSDictionary* errorDic = @{@"ErrorDetails":errorString,@"LoginStatus":@"loggedIn"};
    [MSAnalytics trackEvent:@"Service Error" withProperties:errorDic flags:MSFlagsDefault];
}

+ (void)trackWebServiceError:(NLWebServiceError *)error FromAPI:(NSString *)apiName OnPage:(NSString *)pageName {
    NSString *errString = @"";
    if ([error isKindOfClass:[NLVordelWebServiceError class]]) {
        NLVordelWebServiceError *vordelError = (NLVordelWebServiceError*)error;
        errString = [NSString stringWithFormat:@"%@:%@",vordelError.errorCode,vordelError.errorDescription];
    } else if ([error isKindOfClass:[NLAPIGEEWebServiceError class]]) {
        NLAPIGEEWebServiceError *apigeeError = (NLAPIGEEWebServiceError*)error;
        errString = [NSString stringWithFormat:@"%@:%@",apigeeError.errorCode,apigeeError.errorDescription];
    } else {
        errString = [NSString stringWithFormat:@"%li",(long)error.error.code];
        if ([error.error.userInfo objectForKey:NSLocalizedDescriptionKey]) {
            NSString *info = [error.error.userInfo objectForKey:NSLocalizedDescriptionKey];
            errString = [errString stringByAppendingString:[NSString stringWithFormat:@":%@",info]];
        }
    }
    [AppManager trackError:errString FromAPI:apiName OnPage:pageName];
}


+ (void)trackGenericAPIErrorBeforeLoginOnPage:(NSString *)pageName
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Not Logged In" forKey:kOmniLoginStatus];
   
    [OmnitureManager trackError:OMNIERROR_SOMETHING_WENT_WRONG onPageWithName:pageName contextInfo:data];
    
    NSString* errorStr = [NSString stringWithFormat:@"%@:Page:%@",OMNIERROR_SOMETHING_WENT_WRONG,pageName];
    
    NSDictionary* errorDic = @{@"ErrorDetails":errorStr,@"LoginStatus":@"Not Logged In"};
    [MSAnalytics trackEvent:@"Service Error" withProperties:errorDic flags:MSFlagsDefault];
}

+ (void)trackNoDataFoundErrorOnPage:(NSString *)pageName
{
    [OmnitureManager trackError:OMNIERROR_NO_DATA_FOUND onPageWithName:pageName contextInfo:[AppManager getOmniDictionary]];
}

+ (void)trackNoDataFoundErrorBeforeLoginOnPage:(NSString *)pageName
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Not Logged In" forKey:kOmniLoginStatus];
   
    [OmnitureManager trackError:OMNIERROR_NO_DATA_FOUND onPageWithName:pageName contextInfo:data];
}

+ (NSDictionary *)getOmniDictionary
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    return data;
}

#pragma mark - Pinned certificates

+ (NSSet<NSData *> *)getSecureCertificatesForPinning
{
    NSMutableArray<NSString*> *certNames = [NSMutableArray new];
    NSMutableArray<NSData*> *certData = [NSMutableArray new];

   if ([kBTServerType isEqualToString:@"BTServerTypeProd"]) {
        // names of certificates to be pinned in live
        [certNames addObject:@"mapi.live"];
        [certNames addObject:@"mapi.robt.bt.co.uk"];
        [certNames addObject:@"mapi.robt.bt.co.uk-Sep18"]; // new cert added Sep 18
        //[certNames addObject:@"secure.bt.business"];
        [certNames addObject:@"secure.business.bt.com"];
        [certNames addObject:@"api.ee.co.uk"];
        [certNames addObject:@"api.ee.co.uk-2020-02-27"];
   } else if ([kBTServerType isEqualToString:@"BTServerTypeModelA"]) {
        // names of certificates to be pinned in test (Model A)
        //[certNames addObject:@"mapi.modelA"];
        //[certNames addObject:@"eric1-dmze2e-ukb.bt.com"];
        //[certNames addObject:@"eric1-dmze2e-ukb_bt_com_ee"];
        [certNames addObject:@"eric1-dmze2e-ukb.bt.com-24Aug18"];
        //[certNames addObject:@"eric1-dmze2e-ukb.bt.com_Aug18"];
        //[certNames addObject:@"mapi-ref.robt.bt.co.uk"];
        [certNames addObject:@"mapi-ref.robt.bt.co.uk_Mar18"];
        [certNames addObject:@"api.ee.co.uk"];
        [certNames addObject:@"api-sandbox.ee.co.uk"];
        [certNames addObject:@"api-sandbox.ee.co.uk-2020-02-27"];
    }

    if (certNames && [certNames count] > 0) {
        for (NSString *name in certNames) {
            NSString *path = [[NSBundle mainBundle]pathForResource:name ofType:@"cer"];
            if (path) {
                NSData *data = [NSData dataWithContentsOfFile:path];
                if (data) {
                    [certData addObject:data];
                    //NSLog(@"PROBLEM: adding cert name: %@",name);
                } else {
                    NSLog(@"PROBLEM: no data for cert name: %@",name);
                }
            } else {
                NSLog(@"PROBLEM: cert not found for name: %@",name);
            }
        }
    }

    NSSet <NSData *> *certificates;

    if (certData && [certData count]>0) {
        certificates = [NSSet setWithArray:certData];
    } else {
        // something's probably gone wrong as no certs, but lets avoid a crash
        NSLog(@"PROBLEM: no certificates to pin :/");
        certificates = [NSSet new];
    }

    return certificates;
    
//    if (kBTServerType == BTServerTypeModelA)
//    {
//        NSString *pathToMapiModelACerti = [[NSBundle mainBundle]pathForResource:@"mapi.modelA" ofType:@"cer"];
//        NSString *pathToBTSecureModelACerti = [[NSBundle mainBundle]pathForResource:@"eric1-dmze2e-ukb.bt.com" ofType:@"cer"];
//
//        // New certificate added for Model A Non-vordel (eric1-dmze2e-ukb.bt.com) on 18-08-2017
//        NSString *pathToBTSecureModelANewCerti = [[NSBundle mainBundle]pathForResource:@"eric1-dmze2e-ukb_bt_com_ee" ofType:@"cer"];
//        NSString *pathToNewMapiModelACerti = [[NSBundle mainBundle]pathForResource:@"mapi-ref.robt.bt.co.uk" ofType:@"cer" ];
//        NSString *pathToNewerMapiModelACerti = [[NSBundle mainBundle]pathForResource:@"mapi-ref.robt.bt.co.uk_Mar18" ofType:@"cer" ];
//
//        NSString *pathToEEModelACerti = [[NSBundle mainBundle]pathForResource:@"api.ee.co.uk" ofType:@"cer" ];
//
//        NSData *localCertificateForMapiModelA = [NSData dataWithContentsOfFile:pathToMapiModelACerti];
//        NSData *localCertificateForBTSecureModelA = [NSData dataWithContentsOfFile:pathToBTSecureModelACerti];
//        NSData *localCertificateForNewBTSecureModelA = [NSData dataWithContentsOfFile:pathToBTSecureModelANewCerti];
//        NSData *localCertificateForNewMapiModelA = [NSData dataWithContentsOfFile:pathToNewMapiModelACerti];
//        NSData *localCertificateForNewerMapiModelA = [NSData dataWithContentsOfFile:pathToNewerMapiModelACerti];
//        NSData *localCertificateForEEModelA = [NSData dataWithContentsOfFile:pathToEEModelACerti];
//
//        NSSet <NSData *> *certificates = [[NSSet alloc] initWithObjects:localCertificateForMapiModelA, localCertificateForNewMapiModelA, localCertificateForNewerMapiModelA, localCertificateForNewBTSecureModelA, localCertificateForBTSecureModelA, localCertificateForEEModelA, nil];
//
//
//        return certificates;
//    }
//    else
//    {
//        NSString *pathToMapiLiveCerti = [[NSBundle mainBundle]pathForResource:@"mapi.live" ofType:@"cer"];
//        NSString *pathToNewMapiLiveCerti = [[NSBundle mainBundle]pathForResource:@"mapi.robt.bt.co.uk" ofType:@"cer" ];
//        NSString *pathToBTSecureLiveCerti = [[NSBundle mainBundle]pathForResource:@"secure.bt.business" ofType:@"cer"];
//        NSString *pathToNewBTSecureLiveCerti = [[NSBundle mainBundle]pathForResource:@"secure.business.bt.com" ofType:@"cer"];
//
//        NSString *pathToEELiveCerti = [[NSBundle mainBundle]pathForResource:@"api.ee.co.uk" ofType:@"cer" ];
//
//        NSData *localCertificateForMapiLive = [NSData dataWithContentsOfFile:pathToMapiLiveCerti];
//        NSData *localCertificateForNewMapiLive = [NSData dataWithContentsOfFile:pathToNewMapiLiveCerti];
//        NSData *localCertificateForBTSecureLive = [NSData dataWithContentsOfFile:pathToBTSecureLiveCerti];
//        NSData *localCertificateForNewBTSecureLive = [NSData dataWithContentsOfFile:pathToNewBTSecureLiveCerti];
//        NSData *localCertificateForEELive = [NSData dataWithContentsOfFile:pathToEELiveCerti];
//
//        NSSet <NSData *> *certificates = [[NSSet alloc] initWithObjects:localCertificateForNewMapiLive, localCertificateForMapiLive, localCertificateForBTSecureLive, localCertificateForNewBTSecureLive, localCertificateForEELive, nil];
//
//        return certificates;
//    }
}

#pragma mark - Methods related to deep linking
+ (BOOL)isAppOpnedUsingDeepLinking {
    return [[[NSUserDefaults standardUserDefaults] objectForKey:kIsAppOpenedFromEmailVerification] boolValue];
}

- (void)setDeepLinkedActivationCode:(NSString *)activationCode {
    _deepLinkedActivationCode = activationCode;
}

-(NSString*)getDeepLinkedActivationCode {
    return _deepLinkedActivationCode;
}

- (void)setDeeplinkAlertShownStatusOnSignin: (BOOL)status
{
    _isDeeplinkAlertShownOnSignin = status;
}

- (BOOL)getDeeplinkAlertShownStatusOnSignin
{
    return _isDeeplinkAlertShownOnSignin;
}

#pragma mark - App home screen shortcut methods

+ (NSArray *)appShortcutItems
{
    NSMutableArray<UIMutableApplicationShortcutItem *> *items = [NSMutableArray new];
    
    // Bills, Usage, Account, Service Status
    UIApplicationShortcutIcon *billsIcon = [UIApplicationShortcutIcon iconWithTemplateImageName:@"bills_disable"];
    UIMutableApplicationShortcutItem *bills = [[UIMutableApplicationShortcutItem alloc] initWithType:kShortcutTypeBill localizedTitle:@"Bills" localizedSubtitle:@"" icon:billsIcon userInfo:nil];
    [items addObject:bills];
    
    UIApplicationShortcutIcon *usageIcon = [UIApplicationShortcutIcon iconWithTemplateImageName:@"usage_disable"];
    UIMutableApplicationShortcutItem *usage = [[UIMutableApplicationShortcutItem alloc] initWithType:kShortcutTypeUsage localizedTitle:@"Usage" localizedSubtitle:@"" icon:usageIcon userInfo:nil];
    [items addObject:usage];
    
    UIApplicationShortcutIcon *accountIcon = [UIApplicationShortcutIcon iconWithTemplateImageName:@"account_disable"];
    UIMutableApplicationShortcutItem *account = [[UIMutableApplicationShortcutItem alloc] initWithType:kShortcutTypeAccount localizedTitle:@"Account" localizedSubtitle:@"" icon:accountIcon userInfo:nil];
    [items addObject:account];
    
    UIApplicationShortcutIcon *serviceStatusIcon = [UIApplicationShortcutIcon iconWithTemplateImageName:@"service_status_disable"];
    UIMutableApplicationShortcutItem *serviceStatus = [[UIMutableApplicationShortcutItem alloc] initWithType:kShortcutTypeServiceStatus localizedTitle:@"Service Status" localizedSubtitle:@"" icon:serviceStatusIcon userInfo:nil];
    [items addObject:serviceStatus];
    
    return items;
}

#pragma mark - Methods to set and get version performing completion status
- (void)setIsVersionCheckFinished:(BOOL)isVersionChecked
{
    _isVersionCheckFinished = isVersionChecked;
}
- (BOOL)getIsVersionCheckFinished
{
    return _isVersionCheckFinished;
}

#pragma mark - Open URLs in external browser

+ (void)openURL:(NSString*)url
{
    if (url && ![url isEqualToString:@""]) {
        NSURL *theURL = [NSURL URLWithString:url];
        if (theURL) {
            if ([[UIApplication sharedApplication] canOpenURL:theURL]) {
                if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
                    [[UIApplication sharedApplication] openURL:theURL options:@{} completionHandler:^(BOOL success) {
                        //
                    }];
                } else {
                    [[UIApplication sharedApplication] openURL:theURL];
                }
            }
        }
    }
}

@end
