//
//  AppManager.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/12/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTSMSession.h"
#import "BTAuthenticationToken.h"


//These are used when First Time login and reset password intercepter comes.
#define kPasswordLoginIntercepter @"PasswordWhenIntercepterIsPresentInLoginScreen"
#define kTokenLoginIntercepter @"TokenWhenIntercepterIsPresentInLoginScreen"
#define kSMSessionLoginIntercepter @"SMSessionWhenIntercepterIsPresentInLoginScreen"
#define kGroupKeyLoginIntercepter @"GroupKeyWhenIntercepterIsPresentInLoginScreen"



@class BTOrderSiteContactModel;
@class NLWebServiceError;

typedef NS_ENUM(NSInteger, NLBaseURLType) {
    NLBaseURLTypeVordel,
    NLBaseURLTypeNonVordel,
    NLBaseURLTypeEE
};

typedef NS_ENUM(NSInteger, BTAuthenticationType) {
    BTAuthenticationTypeVordel,
    BTAuthenticationTypeNonVordel
};

extern NSString *const kNLBaseURLNONVordelURLModelA;
extern NSString *const kNLBaseURLNONVordelURLProd;
extern NSString *const kNLBaseURLVordelURLModelA;
extern NSString *const kNLBaseURLVordelURLProd;

extern BTAuthenticationType const kBTAuthenticationType;
NSString *kBTServerType;

extern NSString *const kAppCenterIDDevTest;
extern NSString *const kAppCenterIDLive;

extern BOOL const kIsMobileServiceAssetsEnabled;

// (LP) URLs to use in App
extern NSString *const kForgotBtIdUrl;
extern NSString *const kForgotPasswordUrl;
extern NSString *const kRegistrationUrl;
extern NSString *const kFaultsChatScreenUrl;
extern NSString *const kCloudVoiceFaultsChatScreenUrl;
extern NSString *const kOrdersChatScreenUrl;
extern NSString *const kBillsChatScreenUrl;
extern NSString *const kCloudVoiceExpressChatScreenUrl;
extern NSString *const kFAQChatScreenUrl;
extern NSString *const kTermsAndConditionsUrl;
extern NSString *const kBroadbandFAQUrl;
extern NSString *const kPhonelineFAQUrl;
extern NSString *const kOfficePhoneFAQUrl;
extern NSString *const kEmailFAQUrl;
extern NSString *const kMobileFAQUrl;
extern NSString *const kBillingFAQUrl;
extern NSString *const kUpgradeYourHubUrl;
extern NSString *const k4GAssureOrderNowUrl;
extern NSString *const kGuestWiFiOrderNowUrl;
extern NSString *const kGuestWiFiPostersUrl;
NSString *const kBroadbandSpeedTipsUrl;
NSString *const kWifiSpeedTipsUrl;
NSString *const kSpeedTestGuaranteeUrl;
NSString *const kReportFaultUrl;
NSString *const kReportaFaultHomeUrl;
NSString *const kSmartHubUrl;
// newlly added
extern NSString *const kReportAFaultBroadBandUrl;
extern NSString *const kReportAFaultPhoneUrl;
extern NSString *const kReportAFaultEmailUrl;
extern NSString *const kReportAFaultBtCloudVoiceServicelUrl;

@interface AppManager : NSObject {

}

+ (AppManager *)sharedAppManager;

@property (strong,nonatomic) NSString *usernameForSignInIntercepter;

/* This method is called to get base url string for URL construction. */
+ (NSString *)baseURLStringFromBaseURLType:(NLBaseURLType)baseURLType;

+ (NSString *)reportSpeedTestFaultForServiceID:(NSString *)serviceId;

/* This method returns the current status of availability of internet. */
+ (BOOL)isInternetConnectionAvailable;

/* This method encrypts the string with RSA algorithm. */
+ (NSString *)getRSAEncryptedString:(NSString*)stringToBeEncrypted;

/* Trimming of whitespaces from string */
+ (NSString*)stringByTrimmingWhitespaceInString:(NSString*)string;

/* Date related methods */
+ (NSDate*)dateFromString:(NSString*)dateString;
+ (NSDate *)NSDateWithMonthNameWithSpaceFormatFromNSString:(NSString *)dateString;
+ (NSString *)NSStringFromNSDateWithMonthNameWithSpaceeFormat:(NSDate *)date;
+ (NSString *)NSStringFromNSDateWithMonthYearWithSpaceeFormat:(NSDate *)date;
+ (NSString *)NSStringFromNSDateWithSlashFormat:(NSDate *)date;
+ (NSDate *)NSDateWithUTCFormatFromNSString:(NSString *)dateString;
+ (NSString *)NSStringWithOmnitureFormatFromNSDate : (NSDate *)date;
+ (NSString *)NSStringWithOCSFormatFromNSDate : (NSDate *)date;
+ (NSString *)NSDateWithMonthNameFromNSString : (NSString*)string;
+ (NSInteger)getHourFromDate:(NSDate *)date;
+ (NSInteger)getMinuteFromDate:(NSDate *)date;
+ (NSString *)documentDirectoryPath;
+ (NSString *)getTimeFromDate:(NSDate *)date;
+ (NSString *)getDayMonthFromDate:(NSDate *)date;
+ (NSString *)getDayMonthYearFromDate:(NSDate *)date;
+ (NSString *)getDayDateMonthFormattedStringFromDate:(NSDate *)date;
+ (NSString *)NSStringFromNSDateWithMonthHourDateFormat:(NSDate *)date;
+ (NSString *)NSStringFromNSDateWithReverseDateFormat:(NSDate *)date;
+ (NSString *)NSStringFromNSDateWithMonthNameWithHiphenFormat:(NSDate *)date;
+ (BOOL)isDate:(NSDate *)firstDate isEqualTo:(NSDate *)secondDate;
+ (BOOL)isValidDate:(NSDate *)date;

/* Omniture related methods */
+(NSString*)ADBMobileConfigPath;
+ (NSDictionary *)getOmniDictionary;
+ (void)trackOmnitureKeyTask:(NSString *)keyTask forPageName:(NSString *)pageName;

+ (void)trackGenericAPIErrorBeforeLoginOnPage:(NSString *)pageName;
+ (void)trackGenericAPIErrorOnPage:(NSString *)pageName;
+ (void)trackError:(NSString *)errorDetail FromAPI:(NSString *)apiName OnPage:(NSString *)pageName;
+ (void)trackWebServiceError:(NLWebServiceError *)error FromAPI:(NSString *)apiName OnPage:(NSString *)pageName;
+ (void)trackNoInternetErrorBeforeLoginOnPage:(NSString *)pageName;
+ (void)trackNoInternetErrorOnPage:(NSString *)pageName;
+ (void)trackNoDataFoundErrorBeforeLoginOnPage:(NSString *)pageName;
+ (void)trackNoDataFoundErrorOnPage:(NSString *)pageName;

/* Get user titles and security questions */
+ (NSArray *)getBTUserTitles;
+ (NSArray *)getBTUserSecurityQuestions;

/* Related to site contact data. It should go in the specific class */
+ (NSString *)getContactNumberFromSiteContact:(BTOrderSiteContactModel *)siteContact;

/* Method to check whether app launch is initiated using deep link */
+ (BOOL)isAppOpnedUsingDeepLinking;
- (void)setDeepLinkedActivationCode: (NSString*)activationCode;
- (NSString*)getDeepLinkedActivationCode;
- (void)setDeeplinkAlertShownStatusOnSignin: (BOOL)status;
- (BOOL)getDeeplinkAlertShownStatusOnSignin;

/* Home screen shortcuts */
+ (NSArray *)appShortcutItems;

/* Methods to set and get version performing completion status */
- (void)setIsVersionCheckFinished:(BOOL)isVersionChecked;
- (BOOL)getIsVersionCheckFinished;

/* Method to fetch segmented control font */
-(NSDictionary *)fetchSegmentedControlFontDict;

/* Get certificates for certificate pinning */
+ (NSSet <NSData *> *)getSecureCertificatesForPinning;

/* Convenience method for opening an external browser */
+ (void)openURL:(NSString*)url;

@end
