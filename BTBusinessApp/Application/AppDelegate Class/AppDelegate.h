//
//  AppDelegate.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 06/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@import AppCenter;
@import AppCenterAnalytics;
@import AppCenterCrashes;


@class AppDelegateViewModel;
@class Reachability;


@interface AppDelegate : UIResponder <UIApplicationDelegate> {


}

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, readonly) AppDelegateViewModel *viewModel;
@property (strong, nonatomic) Reachability* internetReachability;
@property (nonatomic, strong) NSMutableDictionary *homeScreenData;
@property (assign, nonatomic) BOOL isSingleBacCugBackNavButtonPressed;
@property (assign, nonatomic) BOOL isSingleBacCug;


+ (AppDelegate *)sharedInstance;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (NSArray *) savedCugs;

@end

