//
//  AppDelegate.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 06/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "AppDelegate.h"
#import "BTSignInViewController.h"
#import "BTPinViewController.h"
#import "BTHomeViewController.h"
#import "BTSlideMenuParentViewController.h"
#import "BTHomeNavigationController.h"
#import "BTSideMenuViewController.h"
#import "BTPullNotificationsViewController.h"
#import "LogOutViewController.h"
#import "BTSplashViewController.h"
#import "BTOnBoardingViewController.h"
#import "BTWhatsNewViewController.h"
#import "LHLogFileManagerCompleteLogs.h"
#import "LHLogFileManagerImportantLogs.h"
#import "LHLogFileManagerServerLogs.h"
#import "LHConsoleCustomFormatter.h"
#import "LHLocalFileLogCustomFormatter.h"
#import "LHServerLogCustomFormatter.h"
#import "LHServerLogFileLogger.h"
#import <CocoaLumberjack/DDFileLogger.h>
#import "AppDelegateViewModel.h"
#import <SAMKeychain/SAMKeychain.h>
#import "AppConstants.h"
#import "OmnitureManager.h"
#import "BTAuthenticationToken.h"
#import "BTSMSession.h"
#import "AppManager.h"
#import "BTAuthenticationManager.h"
#import "AppConstants.h"
#import "BTUser.h"
#import "BTCug.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import <AFNetworkReachabilityManager.h>
#import "BrandColours.h"
#import "BTTabBarViewController.h"

@interface AppDelegate () <BTSplashViewControllerDelegate, AppDelegateViewModelDelegate, BTSignInViewControllerDelegate, BTOnBoardingViewControllerDelegate, BTPINViewControllerDelegate, LogOutViewControllerDelegate, BTWhatsNewViewControllerDelegate> {

    UIView* _secureView;
    NSDate *_dateSinceInactive;
    LHServerLogFileLogger *_serverLogsFileLogger;
    //NSMutableDictionary *homeScreenData;
}

@property (nonatomic, readwrite) AppDelegateViewModel *viewModel;

@property (nonatomic, strong) NSDictionary *apnsDict;

@property (nonatomic, strong) NSString *deferredShorcutAction;

@property (nonatomic) BOOL firstRunForVersion;


- (void)presentLoginFlowWithUsernameFromLoggedInUser:(BOOL)isUsernameFromLoggedInUser;

- (void)presentPINUnlockFlow;

@end



@implementation AppDelegate

+ (AppDelegate *)sharedInstance {
    return (AppDelegate*) [[UIApplication sharedApplication] delegate];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {


    // Initiating the AppManager
    [AppManager sharedAppManager];
    
    // set up appcenter monitoring
    NSString *appCenterID = @"";
    if ([kBTServerType isEqualToString:@"BTServerTypeProd"]) {
        if (![[NSBundle mainBundle].bundleIdentifier.lowercaseString containsString:@"internal"]) {
            appCenterID = kAppCenterIDLive;
        } else {
            appCenterID = kAppCenterIDDevTest;
        }
    } else {
        appCenterID = kAppCenterIDDevTest;
    }
    
    [MSAppCenter start:appCenterID withServices:@[[MSAnalytics class],[MSCrashes class]]];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:kIsAppOpenedFromEmailVerification];
    
    [self firstRunForVersionCheck];
    
    // (hd) Setting up logging for the app.
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    [[DDTTYLogger sharedInstance] setColorsEnabled:YES];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor blueColor] backgroundColor:nil forFlag:DDLogFlagInfo];
    [DDTTYLogger sharedInstance].logFormatter = [[LHConsoleCustomFormatter alloc] init];

    [DDLog addLogger:[DDASLLogger sharedInstance] withLevel:DDLogLevelWarning];

    LHLogFileManagerCompleteLogs *completeLogsFileManager = [[LHLogFileManagerCompleteLogs alloc] init];
    DDFileLogger *completeLogsFileLogger = [[DDFileLogger alloc] initWithLogFileManager:completeLogsFileManager];
    completeLogsFileLogger.rollingFrequency = 60 * 60 * 24;
    completeLogsFileLogger.logFileManager.maximumNumberOfLogFiles = 10;
    completeLogsFileLogger.logFormatter = [[LHLocalFileLogCustomFormatter alloc] init];
    [DDLog addLogger:completeLogsFileLogger];

    LHLogFileManagerImportantLogs *importantLogsFileManager = [[LHLogFileManagerImportantLogs alloc] init];
    DDFileLogger *importantLogsFileLogger = [[DDFileLogger alloc] initWithLogFileManager:importantLogsFileManager];
    importantLogsFileLogger.rollingFrequency = 60 * 60 * 24;
    importantLogsFileLogger.logFileManager.maximumNumberOfLogFiles = 10;
    importantLogsFileLogger.logFormatter = [[LHLocalFileLogCustomFormatter alloc] init];
    [DDLog addLogger:importantLogsFileLogger withLevel:DDLogLevelWarning];

//This code will be useful when we want to check while development
#if TARGET_OS_SIMULATOR
    /* LHLogFileManagerServerLogs *serverLogsFileManager = [[LHLogFileManagerServerLogs alloc] init];
    _serverLogsFileLogger = [[LHServerLogFileLogger alloc] initWithLogFileManager:serverLogsFileManager];
    _serverLogsFileLogger.logFormatter = [[LHServerLogCustomFormatter alloc] init];
    _serverLogsFileLogger.maximumFileSize = 0;
    [DDLog addLogger:_serverLogsFileLogger withLevel:DDLogLevelError]; */
#else
    /*LHLogFileManagerServerLogs *serverLogsFileManager = [[LHLogFileManagerServerLogs alloc] init];
    _serverLogsFileLogger = [[LHServerLogFileLogger alloc] initWithLogFileManager:serverLogsFileManager];
    _serverLogsFileLogger.logFormatter = [[LHServerLogCustomFormatter alloc] init];
    _serverLogsFileLogger.maximumFileSize = 0;
    [DDLog addLogger:_serverLogsFileLogger withLevel:DDLogLevelError];*/
#endif

    // (hd) Creating view model object for this class which shall remain with it for the life time of app run.
    self.viewModel = [[AppDelegateViewModel alloc] init];
    self.viewModel.delegate = self;
    [self.viewModel initializeAndSetupTheApp];
    
    //[[UINavigationBar appearance] setBackgroundColor:[AppConstants colourPurpleNavigationBar]];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"navigationBar_background"] forBarMetrics:UIBarMetricsDefault];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:kBtFontBold size:20], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    [[UINavigationBar appearance] setTitleTextAttributes:attributes];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-300, 0) forBarMetrics:UIBarMetricsDefault];
    
    NSDictionary *altAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:kBtFontBold size:20], NSFontAttributeName,
                                [BrandColours colourTextBTPurplePrimaryColor], NSForegroundColorAttributeName, nil];
    [[UINavigationBar appearanceWhenContainedInInstancesOfClasses:@[[UIDocumentPickerViewController class]]] setBackgroundImage:[UIImage imageNamed:@"navigationBar_background"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearanceWhenContainedInInstancesOfClasses:@[[UIDocumentPickerViewController class]]] setBackgroundColor:[BrandColours colourBackgroundBTPurplePrimaryColor]];
    [[UINavigationBar appearanceWhenContainedInInstancesOfClasses:@[[UIDocumentPickerViewController class]]] setTitleTextAttributes:altAttributes];
    [[UINavigationBar appearanceWhenContainedInInstancesOfClasses:@[[UIDocumentPickerViewController class]]] setTintColor:[UIColor blueColor]];
    [[UINavigationBar appearanceWhenContainedInInstancesOfClasses:@[[UIDocumentPickerViewController class]]] setShadowImage:[[UIImage alloc] init]];
 
    // (hd) BTSplashViewController is set as root view controller for intro animation from Main Storyboard. Hence setting its delegate so that it can communicate to app delegate about completion of its animation.
    if ([self.window.rootViewController isMemberOfClass:[BTSplashViewController class]]) {
        [(BTSplashViewController *)self.window.rootViewController setDelegate:self];
    }
    
    // Omniture handling
    [OmnitureManager overrideConfigPath:[AppManager ADBMobileConfigPath]];
    [OmnitureManager collectLifecycleData];
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    self.homeScreenData = [[NSMutableDictionary alloc] initWithCapacity:0];
    NSString *pageName = OMNIPAGE_LAUNCH;
    
    [data setValue:@"Not Logged In" forKey:@"LoginStatus"];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];

    self.isSingleBacCugBackNavButtonPressed = NO;
    self.isSingleBacCug = NO;
    BOOL retVal = YES;
    
    // Check if shortcuts available
    if ([[UIApplicationShortcutItem class] respondsToSelector:@selector(new)]) {
        [UIApplication sharedApplication].shortcutItems = [AppManager appShortcutItems];
        // check if app launched from shortcut
        UIApplicationShortcutItem *shortcut = [launchOptions objectForKeyedSubscript:UIApplicationLaunchOptionsShortcutItemKey];
        if (shortcut) {
            [self handleQuickLaunchForShortcutItem:shortcut];
            // return NO to prevent performActionForShortcutItem:completionHandler: from being called
            retVal = NO;
        }
    }
    
    return retVal;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    [application ignoreSnapshotOnNextApplicationLaunch];
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.

    [[BTAuthenticationManager sharedManager] pausePeriodicAuthenticationTimer];
    [_serverLogsFileLogger stopLogSubmissionTimer];


    // (hd) Saving the date for calculating the time interval of inactivity in case of resume.
    _dateSinceInactive = [NSDate date];
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
           [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationSecureView:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
       }
    DDLogInfo(@"Applying secure image before resigning active.");
    
       [self setSecureView];
}

- (void)statusBarOrientationSecureView:(NSNotification *)notification {
    // handle the interface orientation as needed
    [self setSecureView];
}

- (void) setSecureView {
    
      if(_secureView != nil) {
           [_secureView removeFromSuperview];
           _secureView = nil;
       }
          
        _secureView = [[UIView alloc] initWithFrame:[self.window frame]];
        UIImageView* secureImageView = [[UIImageView alloc]initWithFrame:[self.window frame]];
        secureImageView.image = [UIImage imageNamed:@"BTSplashImage"];
        secureImageView.contentMode = UIViewContentModeScaleToFill;
              
        UIImageView* secureLogoImageView = [[UIImageView alloc]initWithFrame:secureImageView.frame];
        secureLogoImageView.image = [UIImage imageNamed:@"BTLogoWhite"];
        secureLogoImageView.contentMode = UIViewContentModeCenter;
              
        [secureImageView addSubview:secureLogoImageView];
        [_secureView addSubview:secureImageView];
          
        [self.window endEditing:YES];
        [self.window addSubview:_secureView];
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[AppManager sharedAppManager] setDeeplinkAlertShownStatusOnSignin:YES];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.

}



- (void)applicationDidBecomeActive:(UIApplication *)application {

    // (hd) Check if _dateSinceInactive is nil or not. If nil, it means we are cominging here from a launch else we are coming from background or inactive state.
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
               [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
           }
    if(_secureView.superview == self.window)
    {
       
        DDLogInfo(@"Removing secure image while becoming active.");
        [_secureView removeFromSuperview];
        _secureView = nil;
    }
    
    if([AppManager isAppOpnedUsingDeepLinking])
    {
        [self presentSignInScreenAsNoUserIsLoggedInRequestedByAppDelegateViewModel:_viewModel];
        return;
    }
    
    if(_dateSinceInactive)
    {
        // (hd) Calculating Time Interval Of Inactivity while in background.
        NSTimeInterval timeIntervalOfInactivity = [[NSDate date] timeIntervalSinceDate:_dateSinceInactive];
        if(timeIntervalOfInactivity >= kIntervalAppLockDelay)
        {
            if([self.window.rootViewController isKindOfClass:[BTSlideMenuParentViewController class]])
            {
                DDLogInfo(@"Showing Pin Unlock screen as user has returned to foreground from background after more than %f number of seconds.", timeIntervalOfInactivity);
                [self presentPINUnlockFlow];
            }
        }


        // (hd) Start the Site Minder Refresh Cookie Service if the root is BTHomeViewController
        if([self.window.rootViewController isKindOfClass:[BTSlideMenuParentViewController class]])
        {
            [[BTAuthenticationManager sharedManager] resumePeriodicAuthenticationTimer];
            [_serverLogsFileLogger startLogSubmissionTimer];
        }

        _dateSinceInactive = nil;
    }
    [self.window endEditing:NO];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler {
    
    NSURL *testURL = userActivity.webpageURL;
    NSArray *array = [testURL pathComponents];
    NSString *activationCode = @"";
    if(array.count >= 4) {
        activationCode = [array objectAtIndex:3];
    }

    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:kIsAppOpenedFromEmailVerification];
    [[AppManager sharedAppManager] setDeepLinkedActivationCode:activationCode];
    [[AppManager sharedAppManager] setDeeplinkAlertShownStatusOnSignin:NO];
    return YES;
}

- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler
{
    BOOL handled = [self handleQuickActivateForShortcutItem:shortcutItem];
    completionHandler(handled);
}

- (void)firstRunForVersionCheck
{
    // check if this is the first run for this version
    self.firstRunForVersion = NO;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *currentVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    if ([defaults objectForKey:kLastKnownVersionKey] && [[defaults objectForKey:kLastKnownVersionKey] isKindOfClass:[NSString class]]) {
        NSString *lastVersion = [defaults objectForKey:kLastKnownVersionKey];
        if (![currentVersion isEqualToString:lastVersion]) {
            NSString *whatsNewVersion = @"2.2.0.0";
            if ([currentVersion isEqualToString:whatsNewVersion]) {
                self.firstRunForVersion = YES;
            } else if ([self isVersionString:currentVersion greaterThan:whatsNewVersion]) {
                if ([self isVersionString:whatsNewVersion greaterThan:lastVersion]) {
                    self.firstRunForVersion = YES;
                }
            }
            [defaults setObject:currentVersion forKey:kLastKnownVersionKey];
        }
    } else {
        self.firstRunForVersion = YES;
        [defaults setObject:currentVersion forKey:kLastKnownVersionKey];
    }
#warning disabled whats new screen
    self.firstRunForVersion = NO;
//#warning force show whats new
//    if (kBTServerType == BTServerTypeModelA) {
//        self.firstRunForVersion = YES;
//    }

}

- (BOOL)isVersionString:(NSString*)string1 greaterThan:(NSString*)string2 {
    BOOL retVal = NO;
    NSArray<NSString*> *comps1 = [string1 componentsSeparatedByString:@"."];
    NSArray<NSString*> *comps2 = [string2 componentsSeparatedByString:@"."];
    for (int i=0; i<comps1.count; i++) {
        NSInteger num1 = comps1[i].integerValue;
        if (!retVal) {
            if (comps2.count>i) {
                NSInteger num2 = comps2[i].integerValue;
                if (num1 > num2) {
                    retVal = YES;
                }
            } else {
                if (num1 >= 0) {
                    retVal = YES;
                }
            }
        }
    }
    return retVal;
}

- (BOOL)handleQuickLaunchForShortcutItem:(UIApplicationShortcutItem*)shortcutItem{
    // app launch from shortcut
    [self setDeferredShorcutAction:shortcutItem.type];
    return YES;
}

- (BOOL)handleQuickActivateForShortcutItem:(UIApplicationShortcutItem*)shortcutItem{
    // app running, activated from shortcut
    BOOL handled = NO;
    NSString *shortcutTypeToHandle = shortcutItem.type;
    if ([self.window.rootViewController isKindOfClass:[BTSlideMenuParentViewController class]]) {
        BTSlideMenuParentViewController *rootVC = (BTSlideMenuParentViewController*)self.window.rootViewController;
        if ([shortcutTypeToHandle isEqualToString:kShortcutTypeBill]) {
            handled = YES;
            [rootVC.delegate redirectToBillsScreenForSlideMenuParentViewController:rootVC];
            [OmnitureManager trackClick:OMNISHORTCUT_BILLS];
        } else if ([shortcutTypeToHandle isEqualToString:kShortcutTypeUsage]) {
            handled = YES;
            [rootVC.delegate redirectToUsageScreenForSlideMenuParentViewController:rootVC];
            [OmnitureManager trackClick:OMNISHORTCUT_USAGE];
        } else if ([shortcutTypeToHandle isEqualToString:kShortcutTypeAccount]) {
            handled = YES;
            [rootVC.delegate redirectToAccountScreenForSlideMenuParentViewController:rootVC];
            [OmnitureManager trackClick:OMNISHORTCUT_ACCOUNT];
        } else if ([shortcutTypeToHandle isEqualToString:kShortcutTypeServiceStatus]) {
            handled = YES;
            [rootVC.delegate redirectToServiceStatusScreenForSlideMenuParentViewController:rootVC];
            [OmnitureManager trackClick:OMNISHORTCUT_SERVICESTATUS];
        }
    } else if ([self.window.rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabBar = (UITabBarController*)self.window.rootViewController;
        if ([shortcutTypeToHandle isEqualToString:kShortcutTypeBill]) {
            //[homeNavigationController redirectToBillsScreenForSlideMenuParentViewController:slideMenuParentViewConroller];
            //[tabBar setSelectedViewController:billsHomeNavigationController];
            [tabBar setSelectedIndex:billsTag];
            if ([tabBar.selectedViewController isKindOfClass:[UINavigationController class]]) {
                [(UINavigationController*)tabBar.selectedViewController popToRootViewControllerAnimated:NO];
            }
            [OmnitureManager trackClick:OMNISHORTCUT_BILLS];
            handled = YES;
        } else if ([shortcutTypeToHandle isEqualToString:kShortcutTypeUsage]) {
            //[homeNavigationController redirectToUsageScreenForSlideMenuParentViewController:slideMenuParentViewConroller];
            //[tabBar setSelectedViewController:usageHomeNavigationController];
            [tabBar setSelectedIndex:usageTag];
            if ([tabBar.selectedViewController isKindOfClass:[UINavigationController class]]) {
                [(UINavigationController*)tabBar.selectedViewController popToRootViewControllerAnimated:NO];
            }
            [OmnitureManager trackClick:OMNISHORTCUT_USAGE];
            handled = YES;
        } else if ([shortcutTypeToHandle isEqualToString:kShortcutTypeAccount]) {
            //[homeNavigationController redirectToAccountScreenForSlideMenuParentViewController:slideMenuParentViewConroller];
            //[tabBar setSelectedViewController:accountHomeNavigationController];
            [tabBar setSelectedIndex:accountTag];
            if ([tabBar.selectedViewController isKindOfClass:[UINavigationController class]]) {
                [(UINavigationController*)tabBar.selectedViewController popToRootViewControllerAnimated:NO];
            }
            [OmnitureManager trackClick:OMNISHORTCUT_ACCOUNT];
            handled = YES;
        } else if ([shortcutTypeToHandle isEqualToString:kShortcutTypeServiceStatus]) {
            //[tabBar setSelectedViewController:homeNavigationController];
            [tabBar setSelectedIndex:homeTag];
            if ([tabBar.viewControllers[0] isKindOfClass:[BTSlideMenuParentViewController class]]) {
                BTSlideMenuParentViewController *slideMenuParentViewConroller = tabBar.viewControllers[0];
                [slideMenuParentViewConroller.delegate redirectToServiceStatusScreenForSlideMenuParentViewController:slideMenuParentViewConroller];
                [OmnitureManager trackClick:OMNISHORTCUT_SERVICESTATUS];
                handled = YES;
            }
        } else if ([self.deferredShorcutAction isEqualToString:kShortcutTypePublicWiFi]){
            //[tabBar setSelectedViewController:homeNavigationController];
            if ([tabBar.viewControllers[0] isKindOfClass:[BTSlideMenuParentViewController class]]) {
                [tabBar setSelectedIndex:homeTag];
                BTSlideMenuParentViewController *slideMenuParentViewConroller = tabBar.viewControllers[0];
                [slideMenuParentViewConroller.delegate redirectToPublicWifiScreenForSlideMenuParentViewController:slideMenuParentViewConroller];
                handled = YES;
                // TODO: omniture tag for whats new redirect
            }
        }
    } else {
        [self setDeferredShorcutAction:shortcutTypeToHandle];
    }
    
    return handled;
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.bt.BTBusinessApp" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"BTBusinessApp" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }

    // Create the coordinator and store

    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"BTBusinessApp.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }

    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }

    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            DDLogError(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - AppDelegateViewModel Delegate Methods

- (void)presentOnboardingRequestedByAppDelegateViewModel:(AppDelegateViewModel *)viewModel
{
    DDLogInfo(@"Presenting Onboarding Screen.");

    if(self.window.rootViewController.presentedViewController)
    {
        [self.window.rootViewController dismissViewControllerAnimated:NO completion:^{

            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];

            BTOnBoardingViewController *onBoardingViewController = (BTOnBoardingViewController *)[storyboard instantiateViewControllerWithIdentifier:@"OnBoardingScene"];
            onBoardingViewController.onBoardingDelegate = self;

            [[self window] setRootViewController:onBoardingViewController];
        }];
    }
    else
    {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];

        BTOnBoardingViewController *onBoardingViewController = (BTOnBoardingViewController *)[storyboard instantiateViewControllerWithIdentifier:@"OnBoardingScene"];
        onBoardingViewController.onBoardingDelegate = self;

        [[self window] setRootViewController:onBoardingViewController];
    }
}

- (void)presentSignInScreenAsNoUserIsLoggedInRequestedByAppDelegateViewModel:(AppDelegateViewModel *)viewModel
{
    DDLogInfo(@"Presenting Sign In Screen as no user is logged in.");

    if(self.window.rootViewController.presentedViewController)
    {
        [self.window.rootViewController dismissViewControllerAnimated:NO completion:^{

            [self presentLoginFlowWithUsernameFromLoggedInUser:NO];
        }];
    }
    else
    {
        [self presentLoginFlowWithUsernameFromLoggedInUser:NO];
    }
}


- (void)presentSignInScreenAsPasswardUpdatedAppDelegateViewModel:(AppDelegateViewModel *)viewModel andNeedToPrefillEmailField:(BOOL)needToPrefillEmailField
{
    [self presentLoginFlowWithUsernameFromLoggedInUser:needToPrefillEmailField];
}

- (void)appDelegateViewModel:(AppDelegateViewModel *)viewModel presentSignInScreenAsAppLockDownHasBeenRaisedWithAlertTitle:(NSString *)alertTitle andAlertMessage:(NSString *)alertMessage
{
    if(![self.window.rootViewController isKindOfClass:[BTSignInViewController class]])
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertTitle message:alertMessage preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

            DDLogInfo(@"Presenting Sign In Screen as App Lock Down has been triggered with message: %@", alertMessage);

            if(self.window.rootViewController.presentedViewController)
            {
                [self.window.rootViewController dismissViewControllerAnimated:NO completion:^{

                    [self presentLoginFlowWithUsernameFromLoggedInUser:YES];
                }];
            }
            else
            {
                [self presentLoginFlowWithUsernameFromLoggedInUser:YES];
            }
        }];

        [alertController addAction:okAction];


        UIViewController *presentationViewController = nil;
        UIViewController *currentViewController = self.window.rootViewController;
        while (presentationViewController ==  nil)
        {
            if(currentViewController.presentedViewController == nil)
            {
                presentationViewController = currentViewController;
            }
            else
            {
                currentViewController = currentViewController.presentedViewController;
            }
        }

        [presentationViewController presentViewController:alertController animated:YES completion:^{

        }];
    }
}


- (void)presentPINUnlockScreenAsUserIsLoggedInRequestedByAppDelegateViewModel:(AppDelegateViewModel *)viewModel
{
    // (hd) Now its time to show the PIN Unlock Screen.

    DDLogInfo(@"Presenting PIN Unlock Screen as user is logged in.");

    if(self.window.rootViewController.presentedViewController)
    {
        [self.window.rootViewController dismissViewControllerAnimated:NO completion:^{

            [self presentPINUnlockFlow];
        }];
    }
    else
    {
        [self presentPINUnlockFlow];
    }
}

- (void)appDelegateViewModel:(AppDelegateViewModel *)viewModel presentPINUnlockScreenAsAppLockDownHasBeenRaisedWithAlertTitle:(NSString *)alertTitle andAlertMessage:(NSString *)alertMessage
{
    if(![self.window.rootViewController isKindOfClass:[BTPINViewController class]])
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertTitle message:alertMessage preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

            DDLogInfo(@"Presenting PIN Unlock Screen as App Lock Down has been triggered with message: %@", alertMessage);

            if(self.window.rootViewController.presentedViewController)
            {
                [self.window.rootViewController dismissViewControllerAnimated:NO completion:^{

                    [self presentPINUnlockFlow];
                }];
            }
            else
            {
                [self presentPINUnlockFlow];
            }
        }];

        [alertController addAction:okAction];


        UIViewController *presentationViewController = nil;
        UIViewController *currentViewController = self.window.rootViewController;
        while (presentationViewController ==  nil)
        {
            if(currentViewController.presentedViewController == nil)
            {
                presentationViewController = currentViewController;
            }
            else
            {
                currentViewController = currentViewController.presentedViewController;
            }
        }

        [presentationViewController presentViewController:alertController animated:YES completion:^{

        }];
    }
}

- (void)presentPINUnlockScreenAsUserLoggedOutFromSettingsRequestedByAppDelegateViewModel:(AppDelegateViewModel *)viewModel
{
    DDLogInfo(@"Presenting Logged Out Screen as user has logged out from settings.");

    if(self.window.rootViewController.presentedViewController)
    {
        [self.window.rootViewController dismissViewControllerAnimated:NO completion:^{

            [self presentLoggedOutScreen];
        }];
    }
    else
    {
        [self presentLoggedOutScreen];
    }
}

#pragma mark - LogOutViewControllerDelegate method

-(void)userChoseToLogIn:(LogOutViewController *)logOutViewController
{
    [self presentPINUnlockFlow];
}

-(void)userChoseToSwichUsers:(LogOutViewController *)LogOutViewController
{
    DDLogInfo(@"Presenting Sign In Screen as user decided to swich users.");
    
    if(self.window.rootViewController.presentedViewController)
    {
        [self.window.rootViewController dismissViewControllerAnimated:NO completion:^{
            
            [self presentLoginFlowWithUsernameFromLoggedInUser:NO];
        }];
    }
    else
    {
        [self presentLoginFlowWithUsernameFromLoggedInUser:NO];
    }
}

#pragma mark - BTSplashViewControllerDelegate Methods

- (void)initialIntroAnimationFinishedBySplashViewController:(BTSplashViewController *)viewController {

    [self.viewModel splashAnimationHasFinished];
}

#pragma mark - BTSignInViewControllerDelegate Methods

- (void)signInViewController:(BTSignInViewController *)controller userSuccesfullyLoggedInAndCreatedPINWithInfo:(NSDictionary *)userInfo
{
    NSString *username = [userInfo valueForKey:@"username"];
    NSString *password = [userInfo valueForKey:@"password"];
    BTAuthenticationToken *token = [userInfo valueForKey:@"token"];
    BTSMSession *smsession = [userInfo valueForKey:@"smsession"];
    NSString *pin = [userInfo valueForKey:@"pin"];

    BOOL success = [self.viewModel logInUserWithUsername:username password:password token:token smSession:smsession andPIN:pin];
    if(!success)
    {
        DDLogError(@"Failed to log user in persistence for username %@", username);
        NSAssert(NO, @"Failed to log user in persistence for username %@.", username);
    }

    // (hd) 13-109-2016 Its time to present the Home Screen.
    DDLogInfo(@"Presenting Home Screen.");

    NSDictionary *userDetails = [userInfo valueForKey:@"userDetails"];
    
    if(self.window.rootViewController.presentedViewController)
    {
        [self.window.rootViewController dismissViewControllerAnimated:NO completion:^{

            [self presentHomeScreenWithUserDetails:userDetails];
        }];
    }
    else
    {
        [self presentHomeScreenWithUserDetails:userDetails];
    }
}

#pragma mark - BTOnBoardingViewControllerDelegate Methods

- (void)userTappedOnLastOnboardingScreenOfOnBoardingViewController:(BTOnBoardingViewController *)controller
{
    [self.viewModel onboardingHasFinished];
}

#pragma mark - BTWhatsNewViewControllerDelegate

- (void)userDismissedWhatsNewScreen:(BTWhatsNewViewController *)screen WithAction:(NSString *)action
{
    if (action) {
        self.deferredShorcutAction = action;
    }
    self.firstRunForVersion = NO;
    DDLogInfo(@"Presenting Home Screen.");
    if(self.window.rootViewController.presentedViewController)
    {
        [self.window.rootViewController dismissViewControllerAnimated:NO completion:^{
            
            [self presentHomeScreenWithUserDetails:screen.userDetails];
        }];
    }
    else
    {
        [self presentHomeScreenWithUserDetails:screen.userDetails];
    }
}

#pragma mark - BTPINViewControllerDelegate Methods

- (void)userDecidedToEnterPasswordInsteadOfPinOnPinViewController:(BTPINViewController *)pinViewController
{
    DDLogInfo(@"Presenting Sign In Screen as user decided to choose Enter Password from PIN Unlock Screen.");

    if(self.window.rootViewController.presentedViewController)
    {
        [self.window.rootViewController dismissViewControllerAnimated:NO completion:^{

            [self presentLoginFlowWithUsernameFromLoggedInUser:YES];
        }];
    }
    else
    {
        [self presentLoginFlowWithUsernameFromLoggedInUser:YES];
    }
}

- (void)userSuccesfullyEnteredTheCorrectPINAndUserAuthenticationDoneOnPinViewController:(BTPINViewController *)pinViewController
{
    DDLogInfo(@"Presenting Home Screen.");

    if(self.window.rootViewController.presentedViewController)
    {
        [self.window.rootViewController dismissViewControllerAnimated:NO completion:^{

            [self presentHomeScreenWithUserDetails:nil];
        }];
    }
    else
    {
        [self presentHomeScreenWithUserDetails:nil];
    }
}

- (void)userEnteredIncorrectPINThreeTimesOnPinViewController:(BTPINViewController *)pinViewController
{
    DDLogInfo(@"Presenting Sign In Screen as user entered incorrect pin 3 times.");

    if(self.window.rootViewController.presentedViewController)
    {
        [self.window.rootViewController dismissViewControllerAnimated:NO completion:^{

            [self presentLoginFlowWithUsernameFromLoggedInUser:NO];
        }];
    }
    else
    {
        [self presentLoginFlowWithUsernameFromLoggedInUser:NO];
    }
}


#pragma mark - Private Helper Methods

- (void)presentLoginFlowWithUsernameFromLoggedInUser:(BOOL)isUsernameFromLoggedInUser
{
    [self deleteLoggedInUserFromMemory];

    UIStoryboard *storyboard= [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];

    BTSignInViewController *signInViewController = (BTSignInViewController *)[storyboard instantiateViewControllerWithIdentifier:kStoryboardViewSignIn];
    signInViewController.delegate = self;
    signInViewController.shouldPopulateUsernameForLoggedInUser = isUsernameFromLoggedInUser;

    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:signInViewController];
    navController.navigationBarHidden = YES;

    [[self window] setRootViewController:navController];

    [[BTAuthenticationManager sharedManager] turnOFFPeriodicAuthenticationTimer];
}

- (void)deleteLoggedInUserFromMemory {
    [AppDelegate sharedInstance].viewModel.app.loggedInUser = nil;
    [self saveContext];
}

-(void)presentLoggedOutScreen
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"LogOutScreen" bundle:nil];
    LogOutViewController *logOutViewController = (LogOutViewController *)[storyboard instantiateViewControllerWithIdentifier:@"LogOutViewController"];
    
    logOutViewController.loggedInUser = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    logOutViewController.delegate = self;
    
    [[self window] setRootViewController:logOutViewController];
}

- (void)presentPINUnlockFlow
{
    // (hd) Now its time to show the PIN Unlock Screen.
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];
    BTPINViewController *pinRoadblock = (BTPINViewController *)[storyboard instantiateViewControllerWithIdentifier:kStoryboardViewPinRoadblock];
    pinRoadblock.pinControllerDelegate = self;
    [pinRoadblock setInitRequired:YES];
    [[self window] setRootViewController:pinRoadblock];
    
    [[BTAuthenticationManager sharedManager] turnOFFPeriodicAuthenticationTimer];
}

- (void)presentHomeScreenWithUserDetails:(NSDictionary *)userDetails
{
    if (!self.firstRunForVersion) {
        // Set respective view controllers of BTSlideMenuParentViewController.
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];
        
        // home
        BTHomeViewController *homeViewController = (BTHomeViewController *)[storyboard instantiateViewControllerWithIdentifier:kStoryboardViewHome];
        homeViewController.userDetails = userDetails;
        homeViewController.edgesForExtendedLayout = UIRectEdgeNone;
        BTSlideMenuParentViewController *slideMenuParentViewConroller = [[BTSlideMenuParentViewController alloc] init];
        BTHomeNavigationController *homeNavigationController = [[BTHomeNavigationController alloc] initWithRootViewController:homeViewController];
        homeNavigationController.homeNavigationControllerDelegate = homeViewController;
        slideMenuParentViewConroller.delegate = homeNavigationController;
        BTSideMenuViewController *sideMenuViewController = [BTSideMenuViewController getViewController];
        slideMenuParentViewConroller.screenViewController = homeNavigationController;
        slideMenuParentViewConroller.slideMenuViewController = sideMenuViewController;
        BTPullNotificationsViewController *pullNotificationsViewController = [BTPullNotificationsViewController getViewController];
        slideMenuParentViewConroller.pullNotificationsViewController = pullNotificationsViewController;
        slideMenuParentViewConroller.tabBarItem = [[UITabBarItem alloc] initWithTitle:kHomeTabName image:[UIImage imageNamed:@"home_tab"] tag:homeTag];
        
        // Billing
        BTHomeViewController *billsHomeViewController = (BTHomeViewController *)[storyboard instantiateViewControllerWithIdentifier:kStoryboardViewHome];
        billsHomeViewController.userDetails = userDetails;
        BTHomeNavigationController *billsHomeNavigationController = [[BTHomeNavigationController alloc] initWithRootViewController:billsHomeViewController];
        billsHomeNavigationController.homeNavigationControllerDelegate = billsHomeViewController;
        [billsHomeViewController redirectToBillsScreenForHomeNavigationController:billsHomeNavigationController];
        [billsHomeViewController.navigationItem setHidesBackButton:YES];
        [billsHomeViewController.navigationItem.backBarButtonItem setTitle:@""];
        billsHomeNavigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Billing" image:[UIImage imageNamed:@"billing_tab"] tag:billsTag];
        
        // Usage
        BTHomeViewController *usageHomeViewController = (BTHomeViewController *)[storyboard instantiateViewControllerWithIdentifier:kStoryboardViewHome];
        usageHomeViewController.userDetails = userDetails;
        usageHomeViewController.edgesForExtendedLayout = UIRectEdgeNone;
        BTHomeNavigationController *usageHomeNavigationController = [[BTHomeNavigationController alloc] initWithRootViewController:usageHomeViewController];
        usageHomeNavigationController.homeNavigationControllerDelegate = usageHomeViewController;
        [usageHomeViewController redirectToUsageScreenForHomeNavigationController:usageHomeNavigationController];
        [usageHomeViewController.navigationItem setHidesBackButton:YES];
        [usageHomeViewController.navigationItem.backBarButtonItem setTitle:@""];
        usageHomeNavigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Usage" image:[UIImage imageNamed:@"usage_tab"] tag:usageTag];
        
        // Account
        BTHomeViewController *accountHomeViewController = (BTHomeViewController *)[storyboard instantiateViewControllerWithIdentifier:kStoryboardViewHome];
        accountHomeViewController.userDetails = userDetails;
        BTHomeNavigationController *accountHomeNavigationController = [[BTHomeNavigationController alloc] initWithRootViewController:accountHomeViewController];
        accountHomeNavigationController.homeNavigationControllerDelegate = accountHomeViewController;
        [accountHomeViewController redirectToAccountScreenForHomeNavigationController:accountHomeNavigationController];
        [accountHomeViewController.navigationItem setHidesBackButton:YES];
        [accountHomeViewController.navigationItem.backBarButtonItem setTitle:@""];
        accountHomeNavigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Account" image:[UIImage imageNamed:@"account_tab"] tag:accountTag];
        
        // More
        BTHomeViewController *moreHomeViewController = (BTHomeViewController *)[storyboard instantiateViewControllerWithIdentifier:kStoryboardViewHome];
        moreHomeViewController.userDetails = userDetails;
        BTHomeNavigationController *moreHomeNavigationController = [[BTHomeNavigationController alloc] initWithRootViewController:moreHomeViewController];
        moreHomeNavigationController.homeNavigationControllerDelegate = moreHomeViewController;
        sideMenuViewController.sideMenuViewControllerDelegate = moreHomeNavigationController;
        [moreHomeViewController.navigationItem.backBarButtonItem setTitle:@""];
        [moreHomeNavigationController pushViewController:sideMenuViewController animated:NO];
        moreHomeNavigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"More" image:[UIImage imageNamed:@"more_tab"] tag:moreTag];
        
        
        // tab bar
        UITabBarController *tabBar = [[UITabBarController alloc] init];
        
        [[UITabBarItem appearance] setTitleTextAttributes:@{
                                                            NSFontAttributeName:[UIFont fontWithName:kBtFontRegular size:13.0f]
                                                            } forState:UIControlStateNormal];
        [[UITabBarItem appearance] setTitleTextAttributes:@{
                                                            NSFontAttributeName:[UIFont fontWithName:kBtFontRegular size:13.0f]
                                                            } forState:UIControlStateSelected];
        tabBar.tabBar.backgroundColor = [BrandColours colourBtNeutral30];
        tabBar.tabBar.tintColor = [BrandColours colourTextBTPurplePrimaryColor];
        tabBar.tabBar.opaque = YES;
        
        [tabBar setViewControllers:@[slideMenuParentViewConroller,billsHomeNavigationController,usageHomeNavigationController,accountHomeNavigationController,moreHomeNavigationController]];
        
        [self.window setRootViewController:tabBar];
        
        //[self.window setRootViewController:slideMenuParentViewConroller];
        
        if (self.deferredShorcutAction) {
            [homeNavigationController popToRootViewControllerAnimated:NO];
            if ([self.deferredShorcutAction isEqualToString:kShortcutTypeBill]) {
                //[homeNavigationController redirectToBillsScreenForSlideMenuParentViewController:slideMenuParentViewConroller];
                [tabBar setSelectedViewController:billsHomeNavigationController];
                [OmnitureManager trackClick:OMNISHORTCUT_BILLS];
            } else if ([self.deferredShorcutAction isEqualToString:kShortcutTypeUsage]) {
                //[homeNavigationController redirectToUsageScreenForSlideMenuParentViewController:slideMenuParentViewConroller];
                [tabBar setSelectedViewController:usageHomeNavigationController];
                [OmnitureManager trackClick:OMNISHORTCUT_USAGE];
            } else if ([self.deferredShorcutAction isEqualToString:kShortcutTypeAccount]) {
                //[homeNavigationController redirectToAccountScreenForSlideMenuParentViewController:slideMenuParentViewConroller];
                [tabBar setSelectedViewController:accountHomeNavigationController];
                [OmnitureManager trackClick:OMNISHORTCUT_ACCOUNT];
            } else if ([self.deferredShorcutAction isEqualToString:kShortcutTypeServiceStatus]) {
                [tabBar setSelectedViewController:moreHomeNavigationController];
                [moreHomeViewController redirectToServiceStatusScreenForHomeNavigationController:moreHomeNavigationController];
                //[homeNavigationController redirectToServiceStatusScreenForSlideMenuParentViewController:slideMenuParentViewConroller];
                [OmnitureManager trackClick:OMNISHORTCUT_SERVICESTATUS];
            } else if ([self.deferredShorcutAction isEqualToString:kShortcutTypePublicWiFi]){
                [tabBar setSelectedViewController:moreHomeNavigationController];
                [moreHomeViewController redirectToPublicWifiScreenForHomeNavigationController:moreHomeNavigationController];
                //[tabBar setSelectedViewController:homeNavigationController];
                //[homeNavigationController redirectToPublicWifiScreenForSlideMenuParentViewController:slideMenuParentViewConroller];
                // TODO: omniture tag for whats new redirect
            }
            self.deferredShorcutAction = nil;
        }
        
        [[BTAuthenticationManager sharedManager] resetPeriodicAuthenticationTimer];
        [_serverLogsFileLogger startLogSubmissionTimer];
    } else {
        // show whats new
        BTWhatsNewViewController *whatsNew = [BTWhatsNewViewController getWhatsNewViewController];
        whatsNew.userDetails = [userDetails copy];
        whatsNew.delegate = self;
        DDLogInfo(@"Presenting Whats New Screen");
        if(self.window.rootViewController.presentedViewController)
        {
            [self.window.rootViewController dismissViewControllerAnimated:NO completion:^{
                [self.window setRootViewController:whatsNew];
            }];
        }
        else
        {
            [self.window setRootViewController:whatsNew];
        }
    }
    
}

- (NSArray*) savedCugs {
    NSSet *cugSet = _viewModel.app.loggedInUser.cugs;
    NSArray *cugsArrayInPersistence = [cugSet allObjects];
    NSMutableArray *mutableCugs = [[NSMutableArray alloc] init];
    for(CDCug *persistenceCug in cugsArrayInPersistence)
    {
        NSString *cugName = persistenceCug.cugName;
        NSString *cugID = persistenceCug.cugId;
        NSString *groupKey = persistenceCug.groupKey;
        NSString *refKey = persistenceCug.refKey;
        NSInteger roleID = [persistenceCug.cugRole integerValue];
        NSInteger indexInResponse = [persistenceCug.indexInAPIResponse integerValue];
        NSString *contactId = persistenceCug.contactId;
        
        BTCug *cug = [[BTCug alloc] initWithCugName:cugName cugID:cugID groupKey:groupKey cugRole:roleID indexInAPIResponse:indexInResponse andRefKey:refKey andContactId: contactId];
        if(cug)
        {
            [mutableCugs addObject:cug];
        }
    }
    //[mutableCugs sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
   // NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    //[mutableCugs sortUsingDescriptors:[NSArray arrayWithObject:sort]];
     NSArray *sortedArrayOfCugs = [mutableCugs sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"indexInAPIResponse" ascending:YES]]];
    return sortedArrayOfCugs;
}
@end
