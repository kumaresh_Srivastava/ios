//
//  AppDelegateViewModel.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 28/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AppDelegateViewModel;
@class BTAuthenticationToken;
@class CDApp;
@class CDUser;
@class BTSMSession;

@protocol AppDelegateViewModelDelegate <NSObject>

- (void)presentOnboardingRequestedByAppDelegateViewModel:(AppDelegateViewModel *)viewModel;

- (void)presentSignInScreenAsNoUserIsLoggedInRequestedByAppDelegateViewModel:(AppDelegateViewModel *)viewModel;

- (void)appDelegateViewModel:(AppDelegateViewModel *)viewModel presentSignInScreenAsAppLockDownHasBeenRaisedWithAlertTitle:(NSString *)alertTitle andAlertMessage:(NSString *)alertMessage;

- (void)presentPINUnlockScreenAsUserIsLoggedInRequestedByAppDelegateViewModel:(AppDelegateViewModel *)viewModel;

- (void)appDelegateViewModel:(AppDelegateViewModel *)viewModel presentPINUnlockScreenAsAppLockDownHasBeenRaisedWithAlertTitle:(NSString *)alertTitle andAlertMessage:(NSString *)alertMessage;

- (void)presentPINUnlockScreenAsUserLoggedOutFromSettingsRequestedByAppDelegateViewModel:(AppDelegateViewModel *)viewModel;

- (void)presentSignInScreenAsPasswardUpdatedAppDelegateViewModel:(AppDelegateViewModel *)viewModel andNeedToPrefillEmailField:(BOOL)needToPrefillEmailField; //RLM

@end

@interface AppDelegateViewModel : NSObject {

}

@property (nonatomic, weak) id<AppDelegateViewModelDelegate> delegate;
@property (nonatomic, readonly) CDApp *app;
@property (nonatomic, readonly) NSArray *arrayOfTitles;
@property (nonatomic, readonly) NSArray *arrayOfSecurityQuestions;

- (void)initializeAndSetupTheApp;
- (void)splashAnimationHasFinished;
- (void)onboardingHasFinished;
- (BOOL)logInUserWithUsername:(NSString *)username password:(NSString *)password token:(BTAuthenticationToken *)token smSession:(BTSMSession *)smSession andPIN:(NSString *)pin;
- (void)logoutCurrentUser;
- (void)getUserDetailsAPICallSuccessfullyFinishedWithGroupKey:(NSString *)groupKey;
- (void)updatePINWithUpdatedPIN:(NSString *)pin;

@end
