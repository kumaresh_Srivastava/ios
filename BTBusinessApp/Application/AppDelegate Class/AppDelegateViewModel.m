//
//  AppDelegateViewModel.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 28/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "AppDelegate.h"
#import "CDAuthenticationToken.h"
#import <SAMKeychain/SAMKeychain.h>
#import "CDRecentSearchedOrder.h"
#import "CDRecentSearchedFault.h"
#import "AppConstants.h"
#import "NLGetSecurityQuestionsWebService.h"
#import "NLGetTitlesWebService.h"
#import "NLUpdateLastLoggedInWebService.h"
#import "BTAuthenticationToken.h"
#import "BTAuthenticationManager.h"
#import "NLUserLogoutWebService.h"

@interface AppDelegateViewModel () <NLGetSecurityQuestionsWebServiceDelegate, NLGetTitlesWebServiceDelegate, NLUserLogoutWebServiceDelegate, NLUpdateLastLoggedInWebServiceDelegate> {

    NLGetSecurityQuestionsWebService *_getSecurityQuestionsWebService;
    NLGetTitlesWebService *_getTitlesWebService;
    NLUpdateLastLoggedInWebService *_updateLastLoggedInWebService;
    NLUserLogoutWebService *_userLogoutWebService;
}

@property (nonatomic, readwrite) CDApp *app;

- (void)setupBTAppInstallationKeyIfRequiredInCDApp:(CDApp *)app;

@end

@implementation AppDelegateViewModel

- (void)dealloc {

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Public Methods

- (void)initializeAndSetupTheApp
{
    // Subscribe to notifications.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appNeedsLockDownWithLoginScreenNotification:) name:kNotificationAppNeedsLockDownWithLoginScreen object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appNeedsLockDownWithLoginScreenNotificationWithoutAnyPrompt:) name:kNotificationAppNeedsLockDownWithLoginScreenWithoutAnyPrompt object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appNeedsLockDownWithPINUnlockScreenNotification:) name:kNotificationAppNeedsLockDownWithPINUnlockScreen object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedOutFromSettingsScreenNotification:) name:kNotificationUserLoggedOutFromApp object:nil];

    // Saving public key in Keychain to encrypt sensitive data
    [SAMKeychain setPassword:@"-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtS20u+GidkWkkAuRdR8vT6gnc7m+puxnI+nHgFcYOBgw2GYmrxtAojwZChZyiL86SikRfdeqqmc84KxxVplwVSlePYoNOJj51BvoYUrwl5w+lsrIoDXzRdMUDJQhuGakzsdj0gsnki70LKg5dDjqTUVB4fUb5hBv/go4OiOrbQpAjDom7Nhfo1AYNT3XWKJJzgaSDPOPEkdM0CIY6rcUhWzMW09yY5qeuE1kTYn8E2Po150/upymSf9+VtewoXU9JGxOcmgMKDxJUK/vTv90piB4g3JW+wUKI+WI4lwToy3ZzP2SV3Q/wegcEp81YvQee7Gj1OWosh1Ch+hv/IePWQIDAQAB-----END PUBLIC KEY-----" forService:kKeyChainServiceBTPublicKey account:@"GlobalAccount"];

    // (hd) Setup the CDApp object for the App.
    NSManagedObjectContext *context = [AppDelegate sharedInstance].managedObjectContext;
    self.app = [CDApp appInManagedObjectContext:context];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];


    // (hd) This is also called App Installation Key. This is generated and stored at the first launch of the app after installation and it remains same untill app is deleted from the device. This App Installation Key is sent to the Server in the Get User Info API call only.
    [self setupBTAppInstallationKeyIfRequiredInCDApp:self.app];

    // (hd) Now we shall check if the app is being launched for the first time, then do all the neccessary things that needs to be done.


    // (hd) All the actions we need to do when it is the first launch of the app.
    if([self.app.isFirstAppLaunch boolValue])
    {
        DDLogInfo(@"Starting to perform actions for the first launch of the app.");


        // Clear the Keychain Values From A Previous Installation Of The BTApp
        DDLogInfo(@"Clearing the keychain for values left over from previous installation of the app on this device.");
        NSArray *allAccountsForBTPassword = [SAMKeychain accountsForService:kKeyChainServiceBTUserPassword];
        for(NSDictionary *accountDic in allAccountsForBTPassword)
        {
            NSString *accountName = [accountDic valueForKey:kSAMKeychainAccountKey];
            [SAMKeychain deletePasswordForService:kKeyChainServiceBTUserPassword account:accountName];
        }

        NSArray *allAccountsForBTPIN = [SAMKeychain accountsForService:kKeyChainServiceBTUserPIN];
        for(NSDictionary *accountDic in allAccountsForBTPIN)
        {
            NSString *accountName = [accountDic valueForKey:kSAMKeychainAccountKey];
            [SAMKeychain deletePasswordForService:kKeyChainServiceBTUserPIN account:accountName];
        }


        // After everything is done for the first launch, toggling the BOOL and saving it.
        self.app.isFirstAppLaunch = @NO;
        [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];

        DDLogInfo(@"Finished performing actions for the first launch of the app.");
    }
}

- (void)splashAnimationHasFinished
{

    // (hd) First we shall check if Onboarding needs to be displayed or not.
    if([self.app.needsOnboardingDisplay boolValue])
    {
        [self.delegate presentOnboardingRequestedByAppDelegateViewModel:self];
    }
    else
    {
        // (VM) We need to check whether app launch is initiated by deep linking
        if([AppManager isAppOpnedUsingDeepLinking])
        {
            [self.delegate presentSignInScreenAsNoUserIsLoggedInRequestedByAppDelegateViewModel:self];
        }
        else
        {
            // (hd) Now we shall check any user is already logged IN or not.
            if(self.app.loggedInUser)
            {
                [self.delegate presentPINUnlockScreenAsUserIsLoggedInRequestedByAppDelegateViewModel:self];
            }
            else
            {
                [self.delegate presentSignInScreenAsNoUserIsLoggedInRequestedByAppDelegateViewModel:self];
            }
        }
    }
}

- (void)onboardingHasFinished
{
    // (hd) Save that onboarding has been done.
    self.app.needsOnboardingDisplay = [NSNumber numberWithBool:NO];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];

    [self.delegate presentSignInScreenAsNoUserIsLoggedInRequestedByAppDelegateViewModel:self];
}

- (void)getUserDetailsAPICallSuccessfullyFinishedWithGroupKey:(NSString *)groupKey
{
    // (lp) Fetch User Titles and Security Questions From Server.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self submitLastLoggedInVersionWithGroupKey:groupKey];
        [self fetchBTUserSecurityQuestions];
        [self fetchBTUserTitles];
    });
}

- (void)updatePINWithUpdatedPIN:(NSString *)pin
{
    NSManagedObjectContext *context = [AppDelegate sharedInstance].managedObjectContext;
    self.app = [CDApp appInManagedObjectContext:context];

    CDUser *user = self.app.loggedInUser;

    //Save the PIN in the keychain
    NSError *errorWhileSavingPIN = nil;
    BOOL savePINSuccess = [SAMKeychain setPassword:pin forService:kKeyChainServiceBTUserPIN account:user.username error:&errorWhileSavingPIN];
    if(!savePINSuccess)
    {
        DDLogError(@"Failed to save PIN in keychain for username  '%@'", user.username);
    }
    else
    {
         [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
    }
}


- (void)fetchBTUserSecurityQuestions
{
    _getSecurityQuestionsWebService.getSecurityQuestionsWebServiceDelegate = nil;
    _getSecurityQuestionsWebService = [[NLGetSecurityQuestionsWebService alloc] init];
    _getSecurityQuestionsWebService.getSecurityQuestionsWebServiceDelegate = self;
    [_getSecurityQuestionsWebService resume];
}

- (void)fetchBTUserTitles
{
    _getTitlesWebService.getTitlesWebServiceDelegate = nil;
    _getTitlesWebService = [[NLGetTitlesWebService alloc] init];
    _getTitlesWebService.getTitlesWebServiceDelegate = self;
    [_getTitlesWebService resume];
}

- (void)submitLastLoggedInVersionWithGroupKey:(NSString *)groupkey
{
    _updateLastLoggedInWebService.updateLastLoggedInWebServiceDelegate = nil;
    NSString *buildVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    //(VRK) to be deleted later
    //[[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    NSMutableDictionary *versionDetails = [NSMutableDictionary dictionary];
    
    [versionDetails setValue:buildVersion forKey:@"AppVersion"];
    [versionDetails setValue:@"iOS" forKey:@"AppOSVersion"];

//    NSDictionary *versionDetails = [NSDictionary dictionaryWithObjectsAndKeys:buildVersion, @"AppVersion", @"iOS", @"AppOSVersion", nil];
    _updateLastLoggedInWebService = [[NLUpdateLastLoggedInWebService alloc] initWithVersionDetailsDictionary:versionDetails andGroupKey:groupkey];
    _updateLastLoggedInWebService.updateLastLoggedInWebServiceDelegate = self;
    [_updateLastLoggedInWebService resume];
}

- (BOOL)logInUserWithUsername:(NSString *)username password:(NSString *)password token:(BTAuthenticationToken *)token smSession:(BTSMSession *)smSession andPIN:(NSString *)pin
{
    DDLogInfo(@"Logging in new user with username '%@' in to persistence.", username);
    NSManagedObjectContext *context = [AppDelegate sharedInstance].managedObjectContext;

    CDUser *userObject = [CDUser userWithUsername:username inManagedObjectContext:context];
    if(userObject == nil)
    {
        DDLogError(@"Failed to fetch CDUser object for username  '%@'", username);
        return NO;
    }

    if(self.app.loggedInUser != userObject)
    {
        CDUser *previousLoggedInUser = self.app.loggedInUser;

        // (hd) Delete the PIN in the keychain for previous user
        BOOL removePINSuccess = [SAMKeychain deletePasswordForService:kKeyChainServiceBTUserPIN account:previousLoggedInUser.username];
        if(!removePINSuccess)
        {
            DDLogError(@"Failed to delete PIN in keychain for previously logged in user with username  '%@'", previousLoggedInUser.username);
        }

        [[BTAuthenticationManager sharedManager] deleteAuthenticationTokenAndSMSEssionAndPasswordForUsername:previousLoggedInUser.username];

        // (LP) Delete the recently searched data for previous user
        NSArray *previousRecentSearchedOrders = [previousLoggedInUser.recentlySearchedOrders allObjects];
        for (CDRecentSearchedOrder *searchedOrder in previousRecentSearchedOrders) {
            [previousLoggedInUser removeRecentlySearchedOrdersObject:searchedOrder];
            [context deleteObject:searchedOrder];
        }

        // (LP) Delete the recently searched data for previous user
        NSArray *previousRecentSearchedFaults = [previousLoggedInUser.recentlySearchedOrders allObjects];
        for (CDRecentSearchedFault *searchedFault in previousRecentSearchedFaults) {
            [previousLoggedInUser removeRecentlySearchedFaultsObject:searchedFault];
            [context deleteObject:searchedFault];
        }

        self.app.loggedInUser = userObject;

        DDLogInfo(@"New user with username '%@' successfully saved and logged In into the persistence.", username);
    }
    else
    {
        DDLogInfo(@"Currently logged In user with username '%@' again successfully saved and logged In into the persistence.", username);

    }

    //Save the PIN in the keychain
    if (pin == nil || [pin isEqualToString:@""])
    {
        DDLogError(@"Error : Pin is nil or empty string for username '%@'.", username);
        NSAssert(NO, @"Error : Pin is nil or empty string for username '%@'.", username);
    }
    
    NSError *errorWhileSavingPIN = nil;
    BOOL savePINSuccess = [SAMKeychain setPassword:pin forService:kKeyChainServiceBTUserPIN account:username error:&errorWhileSavingPIN];
    if(!savePINSuccess)
    {
        DDLogError(@"Failed to save PIN in keychain for username  '%@'", username);
        return NO;
    }

    //Save the password, token and SMSession
    [[BTAuthenticationManager sharedManager] saveForCurrentlyLoggedInUserTheAuthenticationToken:token smSession:smSession andPassword:password];

    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];

    return YES;


}


#pragma mark - NSNotification Methods


- (void)appNeedsLockDownWithLoginScreenNotificationWithoutAnyPrompt:(NSNotification*)notification
{
    NSDictionary *dictionary = [notification userInfo];
    
    BOOL needToPrefillEmail  = [[dictionary valueForKey:@"needToPrefillEmail"] boolValue];
    
    [self.delegate presentSignInScreenAsPasswardUpdatedAppDelegateViewModel:self andNeedToPrefillEmailField:needToPrefillEmail];
}

- (void)appNeedsLockDownWithLoginScreenNotification:(NSNotification *)notification
{
    NSString *alertTitle = [notification.userInfo valueForKey:@"title"];
    NSString *alertMessage = [notification.userInfo valueForKey:@"message"];

    [self.delegate appDelegateViewModel:self presentSignInScreenAsAppLockDownHasBeenRaisedWithAlertTitle:alertTitle andAlertMessage:alertMessage];
}

- (void)appNeedsLockDownWithPINUnlockScreenNotification:(NSNotification *)notification
{
    NSString *alertTitle = [notification.userInfo valueForKey:@"title"];
    NSString *alertMessage = [notification.userInfo valueForKey:@"message"];

    [self.delegate appDelegateViewModel:self presentPINUnlockScreenAsAppLockDownHasBeenRaisedWithAlertTitle:alertTitle andAlertMessage:alertMessage];
}

- (void)userLoggedOutFromSettingsScreenNotification:(NSNotification *)notification
{
    [self.delegate presentPINUnlockScreenAsUserLoggedOutFromSettingsRequestedByAppDelegateViewModel:self];
}

#pragma mark - Private Helper Methods

- (void)setupBTAppInstallationKeyIfRequiredInCDApp:(CDApp *)app
{
    if(app && (app.btInstallationID == nil))
    {
        NSString *uniqueIdentifier = [[NSUUID UUID] UUIDString];
        app.btInstallationID = uniqueIdentifier;
        [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
    }
}

- (void)logoutCurrentUser
{
    // Get the currently logged in User
    CDUser *user = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    
    DDLogInfo(@"Logging out user with username %@", user.username);
    
    _userLogoutWebService = [[NLUserLogoutWebService alloc] init];
    _userLogoutWebService.userLogoutWebServiceDelegate = self;
    
    [_userLogoutWebService resume];
    
}

#pragma mark - Log out Webservice delegate

- (void)apiRequestForLogoutSucceededWithUserLogoutWebService:(NLUserLogoutWebService *)webService
{
    DDLogInfo(@"User Logout Api was successful");
}

- (void)userLogoutWebService:(NLUserLogoutWebService *)webService failedToLogOutCurrentUserWithWebServiceError:(NLWebServiceError *)error
{
    DDLogError(@"User Logout Api failed");
}



#pragma mark - NLGetSecurityQuestionsWebServiceDelaget methods
- (void)getSecurityQuestionsWebService:(NLGetSecurityQuestionsWebService *)webService successfullyFetchedSecurityQuestionsData:(NSArray *)arrayOfSecurityQuestions
{
    if(webService == _getSecurityQuestionsWebService)
    {

        if (arrayOfSecurityQuestions != nil && ([arrayOfSecurityQuestions count] > 0)) {

            _arrayOfSecurityQuestions = arrayOfSecurityQuestions;
        }

        _getSecurityQuestionsWebService.getSecurityQuestionsWebServiceDelegate = nil;
        _getSecurityQuestionsWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLGetSecurityQuestionsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLGetSecurityQuestionsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
}

- (void)getSecurityQuestionsWebService:(NLGetSecurityQuestionsWebService *)webService failedWithWebServiceError:(NLWebServiceError *)error
{
    if(webService == _getSecurityQuestionsWebService)
    {
        _getSecurityQuestionsWebService.getSecurityQuestionsWebServiceDelegate = nil;
        _getSecurityQuestionsWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLGetSecurityQuestionsWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLGetSecurityQuestionsWebService but this object is not the one stored in member variable of this class %@.", [self class]);

    }
}

#pragma mark - NLGetTitlesWebServiceDelegate methods
- (void)getTitlesWebService:(NLGetTitlesWebService *)webService successfullyFetchedTitlesData:(NSArray *)arrayOfTitles
{
    if(_getTitlesWebService == webService)
    {

        if (arrayOfTitles != nil && ([arrayOfTitles count] > 0)) {

            _arrayOfTitles = arrayOfTitles;
        }

        _getTitlesWebService.getTitlesWebServiceDelegate = nil;
        _getTitlesWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of success of an object of NLGetTitlesWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLGetTitlesWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }

    
}

- (void)getTitlesWebService:(NLGetTitlesWebService *)webService failedWithWebServiceError:(NLWebServiceError *)error
{
    if(_getTitlesWebService == webService)
    {
        _getTitlesWebService.getTitlesWebServiceDelegate = nil;
        _getTitlesWebService = nil;
    }
    else
    {
        DDLogError(@"This delegate method gets called because of failure of an object of NLGetTitlesWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of failure of an object of NLGetTitlesWebService but this object is not the one stored in member variable of this class %@.", [self class]);

    }
}

#pragma mark - NLUpdateLastLoggedInWebServiceDelegate methods

-(void)versionSubmissionSuccesfullyFinished:(NLUpdateLastLoggedInWebService *)webService{
    
    if (webService == _updateLastLoggedInWebService) {
        
        _updateLastLoggedInWebService.updateLastLoggedInWebServiceDelegate = nil;
        _updateLastLoggedInWebService = nil;
    }
    else {
        DDLogError(@"This delegate method gets called because of success of an object of NLUpdateLastLoggedInWebService but this object is not the one stored in member variable of this class %@.", [self class]);
        NSAssert(NO, @"This delegate method gets called because of success of an object of NLUpdateLastLoggedInWebService but this object is not the one stored in member variable of this class %@.", [self class]);
    }
    
}

-(void)versionSubmitWebService:(NLUpdateLastLoggedInWebService *)webService failedToSubmitVersionDetailsWithWebServiceError:(NLWebServiceError *)error{
    
    if (webService == _updateLastLoggedInWebService) {
        _updateLastLoggedInWebService.updateLastLoggedInWebServiceDelegate = nil;
        _updateLastLoggedInWebService = nil;
    }
    else{
        
    }
    
}

@end
