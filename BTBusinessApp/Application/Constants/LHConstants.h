//
//  LHConstants.h
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 05/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#ifndef LHConstants_h
#define LHConstants_h

extern NSString *const kPrefixNameForCompleteLogFiles;
extern NSString *const kPrefixNameForImportantLogFiles;
extern NSString *const kPrefixNameForServerLogFiles;

extern NSString *const kUserDefaultKeyForServerLogIDGenerationCounter;

extern NSString *const kSeperatorPatternForServerLogs;

extern NSString *const LogHandlingServerLogEventType;

extern const NSTimeInterval LogHandlingServerSubmissionTimeInterval;

#endif /* LHConstants_h */
