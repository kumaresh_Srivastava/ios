//
//  AppConstants.h
//  My BT
//
//  Created by Sam McNeilly on 02/02/2015.
//  Copyright (c) 2015 BT. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppConstants : UIViewController


#define kDummyUnitTestCaseCodeAllowed 1

extern const NSInteger kPeriodicVordelAuthenticationPingDefaultTimeInterval;



// Endpoint addresses, files, folders, etc.

FOUNDATION_EXPORT NSString* const kEndpointPingService;

FOUNDATION_EXPORT NSString* const kEndpointRouteAuth;
FOUNDATION_EXPORT NSString* const kEndpointRouteBroadbandUsage;
FOUNDATION_EXPORT NSString* const kEndpointRouteCustomerProfile;
FOUNDATION_EXPORT NSString* const kEndpointRouteCustomerSummary;
FOUNDATION_EXPORT NSString* const kEndpointRouteServiceChecker;
FOUNDATION_EXPORT NSString* const kEndpointRouteServiceProfileHistory;
FOUNDATION_EXPORT NSString* const kEndpointRouteSiteminderPing;

FOUNDATION_EXPORT NSString* const kEndpointVordelDebug;
FOUNDATION_EXPORT NSString* const kEndpointVordelLive;

// Misc.
FOUNDATION_EXPORT NSString* const kVordelOAuthTokenExpiredMessage;
FOUNDATION_EXPORT NSString* const kVordelOAuthTokenMismatchMessage;
FOUNDATION_EXPORT NSString* const kSiteminderCookieName;
FOUNDATION_EXPORT NSString* const kSiteminderCookieDomain;
FOUNDATION_EXPORT NSString* const kSiteminderAlternateCookieDomain;
FOUNDATION_EXPORT NSInteger const kMaxLoginAttempts;

// App config.
FOUNDATION_EXPORT NSString* const kEndpointAppConfigDebug;
FOUNDATION_EXPORT NSString* const kEndpointAppConfigLive;
FOUNDATION_EXPORT NSString* const kBakedFileAppConfigDebug;
FOUNDATION_EXPORT NSString* const kBakedFileAppConfigLive;

// Storyboard nav and view controllers.
FOUNDATION_EXPORT NSString* const kStoryboardMainStoryboardFile;

FOUNDATION_EXPORT NSString* const kStoryboardNavDefault;
FOUNDATION_EXPORT NSString* const kStoryboardNavMainMenu;
FOUNDATION_EXPORT NSString* const kStoryboardNavPinRoadblock;
FOUNDATION_EXPORT NSString* const kStoryboardNavSignIn;
FOUNDATION_EXPORT NSString* const kStoryboardNavSwitchAccount;

FOUNDATION_EXPORT NSString* const kStoryboardViewAbout;
FOUNDATION_EXPORT NSString* const kStoryboardViewHelpMain;
FOUNDATION_EXPORT NSString* const kStoryboardViewBroadbandUsage;
FOUNDATION_EXPORT NSString* const kStoryboardViewGenericWebView;
FOUNDATION_EXPORT NSString* const kStoryboardViewHelpCategory;
FOUNDATION_EXPORT NSString* const kStoryboardViewHome;
FOUNDATION_EXPORT NSString* const kStoryboardViewIncognito;
FOUNDATION_EXPORT NSString* const kStoryboardViewMainMenu;
FOUNDATION_EXPORT NSString* const kStoryboardViewPinRoadblock;
FOUNDATION_EXPORT NSString* const kStoryboardViewRestartHub;
FOUNDATION_EXPORT NSString* const kStoryboardViewServiceStatus;
FOUNDATION_EXPORT NSString* const kStoryboardViewSignIn;
FOUNDATION_EXPORT NSString* const kStoryboardViewSpeedTest;
FOUNDATION_EXPORT NSString* const kStoryboardViewSwitchAccount;
FOUNDATION_EXPORT NSString* const kStoryboardViewTouchIdConfig;
FOUNDATION_EXPORT NSString* const kStoryboardViewUsageLanding;
FOUNDATION_EXPORT NSString* const kStoryboardViewUsageBroadband;
FOUNDATION_EXPORT NSString* const kStoryboardViewUsageMobile;
FOUNDATION_EXPORT NSString *const kStoryboardViewFaultDashboard;
FOUNDATION_EXPORT NSString *const kStoryboardViewFaultSearch;
FOUNDATION_EXPORT NSString *const kStoryboardViewFaultRestrictedDetails;
FOUNDATION_EXPORT NSString *const kStoryboardViewFaultDetailsMilestone;
FOUNDATION_EXPORT NSString *const kStoryboardViewFaultTrackerSummary;

// Timeouts and other intervals.
FOUNDATION_EXPORT NSTimeInterval const kIntervalHttpRequestTimeout;
FOUNDATION_EXPORT NSInteger const kIntervalSiteminderPing;
FOUNDATION_EXPORT NSInteger const kIntervalAppLockDelay;
FOUNDATION_EXPORT NSInteger const kIntervalSmartAppsCheckTimeout;
FOUNDATION_EXPORT NSInteger const kIntervalVersionCheckTimeout;


// Last resort stuff.
FOUNDATION_EXPORT NSString* const kMsgFatalErrorNoConfigFile;

// UI stuff.
extern CGFloat const kHeaderFontSizeDefault;
extern CGFloat const kHeaderFontSizeCompressed;
extern CGFloat const kBodyFontSizeDefault;
extern CGFloat const kBodyFontSizeCompressed;

extern NSString* const kBtFontBold;
extern NSString* const kBtFontExtraBold;
extern NSString* const kBtFontLight;
extern NSString* const kBtFontRegular;

// OS version checker stuff.
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


// (hd) Keychain Service Names

extern NSString *const kKeyChainServiceBTUserPassword;
extern NSString *const kKeyChainServiceBTUserPIN;
extern NSString *const kKeyChainServiceBTPublicKey;


// (hd) BT Error Domains And Codes

extern NSString *const BTNetworkErrorDomain;
extern NSString *const BTVordelErrorDomain;

typedef NS_ENUM(NSInteger, BTNetworkErrorCode ) {

    BTNetworkErrorCodeNone = 0,

    BTNetworkErrorCodeAPIUserProfileLocked = 602,
    BTNetworkErrorCodeAPIInternalServiceError = 604,
    BTNetworkErrorCodeAPIServerError = 605,
    BTNetworkErrorCodeAPIProfileNotActivated = 606,
    BTNetworkErrorCodeAPINoDataFound = 607,
    BTNetworkErrorCodeAmendAlreadyInProgress = 608,
    BTNetworkErrorCodeIncorrectActivationCode = 611,
    BTNetworkErrorCodeEmailNotVerified = 612,
    BTNetworkErrorCodeEmailAlreadyVerified = 613,
    BTNetworkErrorCodeUnsupportedOrder = 614,
    BTNetworkErrorCodeAPIErrorUnknown = 699,




    BTNetworkErrorCodeGeneralNetworkError = 2002,
    BTNetworkErrorCodeInvalidResponseObject = 2003,
    BTNetworkErrorCodeInvalidTokenResponseObject = 2004,
    BTNetworkErrorCodeHTTPTimeOut = 2005,
    BTNetworkErrorCodeNoInternet = 2006,
    BTNetworkErrorCodeSuccessButNot200 = 2007,


    BTNetworkErrorCodeVordelSMSessionMissingInHeader = 2007,
    BTNetworkErrorCodeSMSessionUnauthenticaiton = 2008,

    BTNetworkErrorCodeVordelEntityTooLarge = 3404,
    BTNetworkErrorCodeVordelParameterMissing = 3405,
    BTNetworkErrorCodeVordelParameterInvalid = 3406,
    BTNetworkErrorCodeVordelClientUnauthenticated = 3408,
    BTNetworkErrorCodeVordelUserUnauthenticated = 3411,
    BTNetworkErrorCodeVordelTokenInvalidOrExpired = 3412,
    BTNetworkErrorCodeVordelDeviceMismatch = 3413,
    BTNetworkErrorCodeVordelScopeInvalid = 3414,
    BTNetworkErrorCodeVordelRefreshInvalidOrExpired = 3415,
    BTNetworkErrorCodeVordelAccountLocked = 3416,
    BTNetworkErrorCodeVordelServiceDeprecated = 3418,
    BTNetworkErrorCodeVordelInvalidDataError = 3501,
    BTNetworkErrorCodeVordelMbassTimeout = 3502,
    BTNetworkErrorCodeVordelMbassInternalExceptionError = 3503,
    BTNetworkErrorCodeVordelPlannedServiceOutage = 3504,
    BTNetworkErrorCodeVordelUnplannedServiceOutage = 3505,
    BTNetworkErrorCodeVordelLegacyAccount = 3601,


    BTNetworkErrorCodeInvalidResponse = 9003,
    BTNetworkErrorCodeLoginInvalid = 9004,
    BTNetworkErrorCodeUserProfileLocked = 9005,
    BTNetworkErrorCodeNoDataFound = 9006,
    BTNetworkErrorCodeUserLoggedOff = 9007

};

// (hd) Notifications.
extern NSString *const kNotificationAppNeedsLockDownWithLoginScreenWithoutAnyPrompt;
extern NSString *const kNotificationAppNeedsLockDownWithLoginScreen;
extern NSString *const kNotificationAppNeedsLockDownWithPINUnlockScreen;
extern NSString *const kNotificationUserLoggedOutFromApp;
extern NSString *const kNotificationDeepLinkTriggered;
extern NSString *const kNotificationVersionCheckCompleted;




// (hd) NSUserDefault Key Constants.

extern NSString *const kNSUserDefaultKeyForStaleCookie;
extern NSString *const kIsAppOpenedFromEmailVerification;
extern NSString *const kEmailVerificationActivationCode;
extern NSString *const kLastKnownVersionKey;

// App shortcut constants
extern NSString* const kShortcutTypeBill;
extern NSString* const kShortcutTypeUsage;
extern NSString* const kShortcutTypeAccount;
extern NSString* const kShortcutTypeServiceStatus;
extern NSString* const kShortcutTypePublicWiFi;



// (LP) Messages to use in APP
extern NSString *const kInvalidCredentialsMessage;
extern NSString *const kProfileLockedMessage;
extern NSString *const kBTServiceOutageMessage;
extern NSString *const kDefaultErrorMessage;
extern NSString *const kSignErrorMessage;
extern NSString *const kDefaultErrorMessageWithLazyLoading;
extern NSString *const kNoConnectionMessage;
extern NSString *const kNoConnectionTitleMessage;
extern NSString *const kNoConnectionTitle;
extern NSString *const kCreatePinTitleMessage;
extern NSString *const kEnterPinTitleMessage;
extern NSString *const kReenterPinTitleMessage;
extern NSString *const kPinConfirmedTitleMessage;
extern NSString *const kPinIncorrectErrorMessage;
extern NSString *const kAppLockDownToLoginScreenAlertMessage1;
extern NSString *const kAppLockDownToLoginScreenAlertMessagePasswordChanged;
extern NSString *const kAppLockDownToPinUnlockScreenAlertMessageUnderMaintenance;
extern NSString *const kInvalidSecurityAnswerMessage;

// (VM) Messages to be used in account verification case
extern NSString *const kAccountVerificationPopupMessage;
extern NSString *const kEmailNotVerifiedErrorMessage;
extern NSString *const kEmailAlreadyVerifiedErrorMessage;
extern NSString *const kIncorrectActivationCodeErrorMessage;
extern NSString *const kHandcraftedURLMessage;
extern NSString *const kLegacyUserLoginErrorMessage;

//(Salman)


typedef NS_ENUM(NSUInteger, TrackFaultDashBoardType) {
    TrackFaultDashBoardTypeOpenOrder   = 1,
    TrackFaultDashBoardTypeCompletedOrder  = 2
};

typedef enum{
    
    DataPointServer,
    DataPointStatic,
    DataPointCache
    
}DataPoint;

#define kEmailRegularExpression @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$"
#define kNameRegularExpression @"^[a-zA-Z]+$"
#define kPhoneRegularExpression @"^[07][0-9]{9,10}$"
#define kPhoneOptionalRegularExpresstion @"^[0-9]{10,11}$"
#define kSecurityNumberValidationRegularExpression @"^(?!(.)\\1{3})(?!0123|1234|2345|3456|4567|5678|6789|7890|0987|9876|8765|7654|6543|5432|4321|3210)\\d{8}$"

#define kScreenSize [[UIScreen mainScreen] bounds].size

#define kIsDevMode NO

#define kDataPoint DataPointServer

#define kBillPaidSuccessfullyNotification @"BillPaidSuccessfullyNotification"


// (hds) Vordel Client ID and Secret
extern NSString *const kVordelClientID;
extern NSString *const kVordelClientSecret;


#define homeTag 0
#define billsTag 1
#define usageTag 2
#define accountTag 3
#define moreTag 4
#define kHomeTabName @"Home"
#define kAccountTabName @"Account"
#define kUsageTabName @"Usage"


//getting device related information
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH <= 568.0)




@end
