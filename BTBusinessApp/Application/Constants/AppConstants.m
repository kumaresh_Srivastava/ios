//
//  AppConstants.m
//  My BT
//
//  Created by Sam McNeilly on 02/02/2015.
//  Copyright (c) 2015 BT. All rights reserved.
//

#import "AppConstants.h"

@interface AppConstants ()

@end

@implementation AppConstants

// Endpoint addresses, files, folders, etc.


const NSInteger kPeriodicVordelAuthenticationPingDefaultTimeInterval = 15 * 60;



NSString* const kEndpointPingService = @"http://www.apple.com/library/test/success.html";

NSString* const kEndpointRouteAuth =                    @"/authentication/oauth";
NSString* const kEndpointRouteBroadbandUsage =          @"/getbroadbandusagesdetails/querybroadbandusages";
NSString* const kEndpointRouteCustomerProfile =         @"/manageoneidassets/queryoneidassets";
NSString* const kEndpointRouteCustomerSummary =         @"/getopenfaults/querycustomersummarytag";
NSString* const kEndpointRouteServiceChecker =          @"/manageservicestatus/getmsodetails";
NSString* const kEndpointRouteServiceProfileHistory =   @"/getserviceprofilehistory/queryserviceprofilehistory";
NSString* const kEndpointRouteSiteminderPing =          @"/siteminder/verify";

NSString* const kEndpointVordelDebug =  @"https://mapi-ref.robt.bt.co.uk:53082";
NSString* const kEndpointVordelLive =   @"https://mapi.robt.bt.co.uk:53082";

// App config.

NSString* const kEndpointAppConfigDebug =   @"http://ggdclab.no-ip.org/json/view.php?id=116";
NSString* const kEndpointAppConfigLive =    @"https://assets.bt.com/v1/btcomd/app/my_bt/app-config-v2.0.json"; // @"https://assets.bt.com/v1/btcomd/app/my_bt/app-config-v2.0.json";
NSString* const kBakedFileAppConfigDebug =  @"AppConfig-Test-01";
NSString* const kBakedFileAppConfigLive =   @"AppConfig-01";

// Misc.
NSString* const kVordelOAuthTokenExpiredMessage =   @"expired_token";
NSString* const kVordelOAuthTokenMismatchMessage =  @"token_mismatch";
NSString* const kSiteminderCookieName =             @"SMSESSION";
NSString* const kSiteminderCookieDomain =           @".bt.com";
NSString* const kSiteminderAlternateCookieDomain =  @"secure.business.bt.com";
NSInteger const kMaxLoginAttempts =                 3;

// Storyboard nav and view controllers.
NSString* const kStoryboardMainStoryboardFile =     @"Main";

NSString* const kStoryboardNavDefault =             @"NavigationController";
NSString* const kStoryboardNavMainMenu =            @"MainMenuNavigationController";
NSString* const kStoryboardNavPinRoadblock =        @"PINViewNavigationController";
NSString* const kStoryboardNavSignIn =              @"SignInNavigationController";
NSString* const kStoryboardNavSwitchAccount =       @"SwitchAccountNavigationController";

NSString* const kStoryboardViewBroadbandUsage =     @"BroadbandUsageMainViewController";
NSString* const kStoryboardViewHelpMain =           @"HelpViewController";
NSString* const kStoryboardViewHelpCategory =       @"HelpCategoryDetailViewController";
NSString* const kStoryboardViewHome =               @"HomeScene";
NSString* const kStoryboardViewIncognito =          @"IncognitoViewController";
NSString* const kStoryboardViewMainMenu =           @"MainMenuViewController";
NSString* const kStoryboardViewPinRoadblock =       @"PinRoadblockViewController";
NSString* const kStoryboardViewRestartHub =         @"RestartHubViewController";
NSString* const kStoryboardViewServiceStatus =      @"ServiceStatusViewController";
NSString* const kStoryboardViewSignIn =             @"SignInScene";
NSString* const kStoryboardViewSpeedTest =          @"SpeedTestViewController";
NSString* const kStoryboardViewSwitchAccount =      @"SwitchAccountViewController";
NSString* const kStoryboardViewGenericWebView =     @"GenericWebViewController";
NSString* const kStoryboardViewAbout =              @"AboutViewController";
NSString* const kStoryboardViewTouchIdConfig =      @"TouchIdConfigViewController";
NSString* const kStoryboardViewUsageLanding =       @"UsageLandingViewController";
NSString* const kStoryboardViewUsageBroadband =     @"BroadbandUsageViewController";
NSString* const kStoryboardViewUsageMobile =        @"MobileUsageViewController";
NSString *const kStoryboardViewFaultDashboard =     @"trackFaultDashboardScene";
NSString *const kStoryboardViewFaultSearch =        @"trackFaultSearchScene";
NSString *const kStoryboardViewFaultRestrictedDetails =     @"FaultRestrictedDetailsScene";
NSString *const kStoryboardViewFaultDetailsMilestone  =     @"faultDetailsMileStoneScene";
NSString *const kStoryboardViewFaultTrackerSummary    =     @"FaultSummaryScene";

// Timeouts and other intervals.
NSTimeInterval const kIntervalHttpRequestTimeout =  30.0;
NSInteger const kIntervalAppLockDelay =             60 * 5;//5F
NSInteger const kIntervalSiteminderPing =           60 * 18;
NSInteger const kIntervalSmartAppsCheckTimeout =    10;
NSInteger const kIntervalVersionCheckTimeout =      3;




// Last resort stuff.
NSString* const kMsgFatalErrorNoConfigFile =    @"We're really sorry, but it looks like your app is missing some required files. Please "\
                                                "download the latest version of My BT from the App Store.";


// UI stuff.
CGFloat const kHeaderFontSizeDefault = 38.0;
CGFloat const kHeaderFontSizeCompressed = 24.0;
CGFloat const kBodyFontSizeDefault = 17.0;
CGFloat const kBodyFontSizeCompressed = 15.0;

NSString* const kBtFontBold = @"BTFont-Bold";
NSString* const kBtFontExtraBold = @"BTFont-ExtraBold";
NSString* const kBtFontLight = @"BTFont-Light";
NSString* const kBtFontRegular = @"BTFont-Regular";


// (hd) Keychain Service Names

NSString *const kKeyChainServiceBTUserPassword = @"com.bt.btbusiness.keychainServiceForBTUserPassword";
NSString *const kKeyChainServiceBTUserPIN = @"com.bt.btbusiness.KeychainServiceForBTUserPIN";
NSString *const kKeyChainServiceBTPublicKey = @"com.bt.btbusiness.keychainServiceForPublicKey";

// (hd) BT Error Domains

NSString *const BTNetworkErrorDomain = @"BTNetworkErrorDomain";
NSString *const BTVordelErrorDomain = @"BTVordelErrorDomain";

// Notifications.

NSString *const kNotificationAppNeedsLockDownWithLoginScreenWithoutAnyPrompt = @"NotificationAppNeedsLockDownWithLoginScreenWithoutAnyPrompt";

NSString *const kNotificationAppNeedsLockDownWithLoginScreen = @"NotificationAppNeedsLockDownWithLoginScreen";

NSString *const kNotificationAppNeedsLockDownWithPINUnlockScreen = @"NotificationAppNeedsLockDownWithPINUnlockScreen";

NSString *const kNotificationUserLoggedOutFromApp = @"NotificationUserLoggedOutFromApp";

NSString *const kNotificationDeepLinkTriggered = @"NotificationDeepLinkTriggered";

NSString *const kNotificationVersionCheckCompleted = @"NotificationVersionCheckCompleted";


// (hd) NSUserDefault Key Constants.

NSString *const kNSUserDefaultKeyForStaleCookie = @"NSUserDefaultKeyForStaleCookie";
NSString *const kIsAppOpenedFromEmailVerification = @"IsAppOpenedFromEmailVerification";
NSString *const kEmailVerificationActivationCode = @"EmailVerificationActivationCode";
NSString *const kLastKnownVersionKey = @"BTBAppLastRunVersion";

// App shortcut constants
NSString* const kShortcutTypeBill = @"com.bt.internal.btbusinessapp.bill";
NSString* const kShortcutTypeUsage = @"com.bt.internal.btbusinessapp.usage";
NSString* const kShortcutTypeAccount = @"com.bt.internal.btbusinessapp.account";
NSString* const kShortcutTypeServiceStatus = @"com.bt.internal.btbusinessapp.servicestatus";
NSString* const kShortcutTypePublicWiFi = @"com.bt.internal.btbusinessapp.publicwifi";


// (LP) Messages to use in APP

//NSString *const kInvalidCredentialsMessage = @"Invalid username / password. If you make three invalid login attempts, your profile will be locked for 20 minutes." this is old message now it is replaced by Sorry, username or password error. Please try again.  - RLM 17 feb ;

NSString *const kInvalidCredentialsMessage = @"Invalid username and (or) password";

//NSString *const kProfileLockedMessage = @"Your profile has been locked for 20 minutes owing to incorrect password attempts. Please try again later.  - OLD MESSAGE - RLM 17 feb";

NSString *const kProfileLockedMessage = @"Because of 3 failed password attempts, your profile is now locked for 20 minutes. Please try again later.";

NSString *const kBTServiceOutageMessage = @"The app is under maintenance. Please try again later.";
NSString *const kDefaultErrorMessage = @"Sorry, something has gone wrong. Please try again later.";

NSString *const kSignErrorMessage = @"Sorry, username or password error. Please try again.";

NSString *const kDefaultErrorMessageWithLazyLoading = @"Something's gone wrong...";
NSString *const kNoConnectionMessage = @"Check your connection and try again.";
NSString *const kNoConnectionTitleMessage = @"Something went wrong";
//(Sal)
NSString *const kNoConnectionTitle = @"Message";

//(hds) 02-02 Fill up the proper messages here

NSString *const kAppLockDownToLoginScreenAlertMessage1 = @"Please enter your username and password again";//RLM 17 Feb

NSString *const kAppLockDownToLoginScreenAlertMessagePasswordChanged = @"Please enter your username and password again";//RLM 17 Feb


NSString *const kAppLockDownToPinUnlockScreenAlertMessageUnderMaintenance = @"The app is under maintenance. Please try after sometime.";

NSString *const kInvalidSecurityAnswerMessage = @"That's not the answer that we've got. Please try again.";

NSString *const kPinIncorrectErrorMessage = @"Invalid PIN please try again";

NSString *const kAccountVerificationPopupMessage = @"You just need to enter your username and password to verify your account.";

NSString *const kEmailNotVerifiedErrorMessage = @"Sorry, your account is still pending verification. Please click the verification link we have sent to you!";

NSString *const kLegacyUserLoginErrorMessage = @"Sorry, in order to complete account verification please log into bt.com/business.";

NSString *const kEmailAlreadyVerifiedErrorMessage = @"It looks like this account is already activated so press OK to continue or cancel"; //@"Account already verified, log in using your username and password!";

NSString *const kIncorrectActivationCodeErrorMessage = @"Sorry, that username does not match the activation link. Why not check the username in the email we sent you and try again!"; //@"Sorry, you are entering some incorrect details. Please check and try again!";

NSString *const kHandcraftedURLMessage = @"Sorry, something went wrong! Please click the link in the email to complete registration.";

@end
