//
//  NLConstants.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 14/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#ifndef NLConstants_h
#define NLConstants_h

extern NSString *const kNLResponseKeyForIsSuccess;
extern NSString *const kNLResponseKeyForCode;
extern NSString *const kNLResponseKeyForResult;
extern NSString *const kNLResponseKeyForErrorMessage;
extern NSString *const kCacheDirectory;

extern const NSTimeInterval kNLWebServiceRequestTimeOut;
extern const NSTimeInterval kNLWebServiceRequestExtendedTimeOut;

extern const NSInteger kNLWebServiceTimeOutMaxRetryAttempts;



extern NSString *const kNLVordelErrorCodeFromServerForParameterMissing;
extern NSString *const kNLVordelErrorCodeFromServerForParameterInvalid;
extern NSString *const kNLVordelErrorCodeFromServerForClientUnauthenticated;
extern NSString *const kNLVordelErrorCodeFromServerForUserUnauthenticated;
extern NSString *const kNLVordelErrorCodeFromServerForTokenInvalidOrExpired;
extern NSString *const kNLVordelErrorCodeFromServerForDeviceMismatch;
extern NSString *const kNLVordelErrorCodeFromServerForScopeInvalid;
extern NSString *const kNLVordelErrorCodeFromServerForRefreshInvalidOrExpired;
extern NSString *const kNLVordelErrorCodeFromServerForAccountLocked;
extern NSString *const kNLVordelErrorCodeFromServerForServiceDeprecated;
extern NSString *const kNLVordelErrorCodeFromServerForEntityTooLarge;
extern NSString *const kNLVordelErrorCodeFromServerForInvalidDataError;
extern NSString *const kNLVordelErrorCodeFromServerForMbaasInternalException;
extern NSString *const kNLVordelErrorCodeFromServerForPlannedServiceOutage;
extern NSString *const kNLVordelErrorCodeFromServerForUnplannedServiceOutage;
extern NSString *const kNLVordelErrorCodeFromServerForMbaasTimeout;
extern NSString *const kNLVordelErrorCodeFromServerForLegacyAccount;



#endif /* NLConstants_h */
