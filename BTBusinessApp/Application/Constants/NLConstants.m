//
//  NLConstants.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 14/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "NLConstants.h"

NSString *const kNLResponseKeyForIsSuccess = @"isSuccess";
NSString *const kNLResponseKeyForCode = @"code";
NSString *const kNLResponseKeyForResult = @"result";
NSString *const kNLResponseKeyForErrorMessage = @"errorMessage";
NSString *const kCacheDirectory = @"APIResponseCache";

const NSTimeInterval kNLWebServiceRequestTimeOut = 60.0;
const NSTimeInterval kNLWebServiceRequestExtendedTimeOut = 30.0;

const NSInteger kNLWebServiceTimeOutMaxRetryAttempts = 3;

NSString *const kNLVordelErrorCodeFromServerForParameterMissing = @"mb4-05";
NSString *const kNLVordelErrorCodeFromServerForParameterInvalid = @"mb4-06";
NSString *const kNLVordelErrorCodeFromServerForClientUnauthenticated = @"mb4-08";
NSString *const kNLVordelErrorCodeFromServerForUserUnauthenticated = @"mb4-11";
NSString *const kNLVordelErrorCodeFromServerForTokenInvalidOrExpired = @"mb4-12";
NSString *const kNLVordelErrorCodeFromServerForDeviceMismatch = @"mb4-13";
NSString *const kNLVordelErrorCodeFromServerForScopeInvalid = @"mb4-14";
NSString *const kNLVordelErrorCodeFromServerForRefreshInvalidOrExpired = @"mb4-15";
NSString *const kNLVordelErrorCodeFromServerForAccountLocked = @"mb4-16";
NSString *const kNLVordelErrorCodeFromServerForServiceDeprecated = @"mb4-18";
NSString *const kNLVordelErrorCodeFromServerForEntityTooLarge = @"mb4-04";
NSString *const kNLVordelErrorCodeFromServerForInvalidDataError = @"mb5-01";
NSString *const kNLVordelErrorCodeFromServerForMbaasInternalException = @"mb5-03";
NSString *const kNLVordelErrorCodeFromServerForPlannedServiceOutage = @"mb5-04";
NSString *const kNLVordelErrorCodeFromServerForUnplannedServiceOutage = @"mb5-05";
NSString *const kNLVordelErrorCodeFromServerForMbaasTimeout = @"mb5-02";
NSString *const kNLVordelErrorCodeFromServerForLegacyAccount = @"mbaf2-01";

