//
//  LHConstants.m
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 05/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "LHConstants.h"


NSString *const kPrefixNameForCompleteLogFiles = @"BTBMobileiOSCompleteLogs";
NSString *const kPrefixNameForImportantLogFiles = @"BTBMobileiOSImportantLogs";
NSString *const kPrefixNameForServerLogFiles = @"BTBMobileiOSServerLogs";

NSString *const kUserDefaultKeyForServerLogIDGenerationCounter = @"LHServerLogIDGenerationCounter";

NSString *const kSeperatorPatternForServerLogs = @"*---*---*---*---*";

NSString *const LogHandlingServerLogEventType = @"BTBMobileiOS";

//TODO: (hd) 21-09-2016 Change this time interval to something more once this code is tested and work perfectly well.
const NSTimeInterval LogHandlingServerSubmissionTimeInterval = 120.0;
