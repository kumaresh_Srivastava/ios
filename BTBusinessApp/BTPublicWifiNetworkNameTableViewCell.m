//
//  BTPublicWifiNetworkNameTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 18/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTPublicWifiNetworkNameTableViewCell.h"
#import "AppConstants.h"
#import "BrandColours.h"
@implementation BTPublicWifiNetworkNameTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCellWithNetworkType:(NSString *)type
{
    if (type) {
        [self.networkTypeLabel setText:type];
    } else {
        [self.networkTypeLabel setText:@"Wi-Fi"];
    }
    
}

- (void)updateCellWithNetworkName:(NSString *)name
{
    if (name && ![name isEqualToString:@""]) {
        UIFont *regularFont = [UIFont fontWithName:kBtFontRegular size:14.0];
        UIFont *boldFont = [UIFont fontWithName:kBtFontBold size:14.0];
        NSMutableAttributedString *fullNetworkName = [[NSMutableAttributedString alloc] initWithString:@"Network name: " attributes:@{NSFontAttributeName:boldFont}];
        NSMutableAttributedString *networkName = [[NSMutableAttributedString alloc] initWithString:name attributes:@{NSFontAttributeName:regularFont}];
        [fullNetworkName appendAttributedString:networkName];
        [self.networkNameLabel setAttributedText:fullNetworkName];
    } else {
        [self.networkNameLabel setText:@""];
    }
}

- (CGFloat)heightForCell
{
    return 100.0f;
}

@end
