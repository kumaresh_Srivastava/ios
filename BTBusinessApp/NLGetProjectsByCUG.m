//
//  NLGetProjectsByCUG.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 19/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLGetProjectsByCUG.h"
#import "NLAPIGEEWebServiceError.h"

@implementation NLGetProjectsByCUG

- (instancetype)initWithCUGID:(NSString *)cugId
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/ocs-orders?CUGID=%@&source=%@",cugId,@"BTBusinessApp"];
    
    self = [super initWithMethod:@"GET" parameters:@"" andEndpointUrlString:endPointURLString];
    if(self)
    {
        
    }
    
    return self;
}

- (instancetype)initWithCUG:(NSString *)cug
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/bt-business-auth/v1/ocs-orders?CUGName=%@&source=%@",cug,@"BTBusinessApp"];
    
    self = [super initWithMethod:@"GET" parameters:@"" andEndpointUrlString:endPointURLString];
    if(self)
    {
        
    }
    
    return self;
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *resultDic = (NSDictionary*)responseObject;
    
    if (resultDic)
    {
        [self.delegate webService:self finshedWithSuccessResponse:resultDic];
    }
    else
    {
        NLWebServiceError *webServiceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponse];
        
        DDLogError(@"Invalid response json object in webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Invalid response json object in webservice with URL: %@", [[task currentRequest] URL]]));
        [self.delegate webService:self failedWithError:webServiceError];
    }
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
//    if ([task.response isKindOfClass:[NSHTTPURLResponse class]]) {
//        NSHTTPURLResponse *response = (NSHTTPURLResponse*)task.response;
//        NSInteger statusCode = response.statusCode;
//    }
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.delegate webService:self failedWithError:webServiceError];
    
}

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task
                                                                                  andResponseObject:responseObject];
    if (!errorToBeReturned) {
        //local error checking
        NSDictionary *returnStatusInfo = [(NSDictionary*)responseObject valueForKey:@"ReturnStatusInfo"];
        if (returnStatusInfo) {
            NSString *statusValue = [returnStatusInfo valueForKey:@"StatusValue"];
            NSInteger statusValueCode = [[returnStatusInfo valueForKey:@"StatusValueCode"] integerValue];
            if (statusValue && statusValueCode != 0) {
                errorToBeReturned = [NLAPIGEEWebServiceError apigeeWebServiceNetworkErrorWithErrorCode:[NSString stringWithFormat:@"%li",(long)statusValueCode] andDescription:statusValue];
            }
            
        } else {
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponse];
        }
    }
    return errorToBeReturned;
}

@end
