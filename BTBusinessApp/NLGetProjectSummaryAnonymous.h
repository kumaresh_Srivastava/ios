//
//  NLGetProjectSummaryAnonymous.h
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLVordelAuthenticationProtectedWebService.h"

@class NLGetProjectSummaryAnonymous;
@class NLWebServiceError;

@protocol NLGetProjectSummaryAnonymousDelegate <NLVordelAuthenticationProtectedWebServiceDelegate>

- (void)getProjectSummaryAnonymousWebService:(NLGetProjectSummaryAnonymous *)webService finishedWithResponse:(NSObject *)response;

- (void)getProjectSummaryAnonymousWebService:(NLGetProjectSummaryAnonymous *)webService failedWithWebServiceError:(NLWebServiceError *)error;

@optional
- (void)timedOutWithoutSuccessForGetProjectSummaryAnonymousWebService:(NLGetProjectSummaryAnonymous*)webService;

@end

@interface NLGetProjectSummaryAnonymous : NLVordelAuthenticationProtectedWebService

@property (nonatomic) NSDictionary* projectDetails;

@property (nonatomic, weak) id <NLGetProjectSummaryAnonymousDelegate> getProjectSummaryAnonymousDelegate;

- (instancetype)initWithProjectDetails:(NSDictionary*)projectDetails;

@end
