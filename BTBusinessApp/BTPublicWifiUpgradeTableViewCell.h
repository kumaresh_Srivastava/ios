//
//  BTPublicWifiUpgradeTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 10/05/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

typedef NS_ENUM(NSInteger, BTPublicWifiUpgradeType) {
    BTPublicWifiUpgradeTypeGuestWifi,
    BTPublicWifiUpgradeTypeHub,
    BTPublicWifiUpgradeType4GUpgradeHub,
    BTPublicWifiUpgradeType4GAssure
};

@interface BTPublicWifiUpgradeTableViewCell : BTBaseTableViewCell
@property (nonatomic) BTPublicWifiUpgradeType context;
@property (strong, nonatomic) IBOutlet UILabel *titleLable;
@property (strong, nonatomic) IBOutlet UITextView *detailTextView;
@property (strong, nonatomic) IBOutlet UIButton *upgradeButton;
- (IBAction)upgradeButtonAction:(id)sender;

- (void)setupCellForContext:(BTPublicWifiUpgradeType)context;

@end
