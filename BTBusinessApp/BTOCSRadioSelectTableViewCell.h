//
//  BTOCSRadioSelectTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 16/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@class BTOCSRadioSelectTableViewCell;

@protocol BTOCSRadioSelectTableViewCellDelegate <NSObject>

- (void)radioSelectCell:(BTOCSRadioSelectTableViewCell*)cell updatedSelection:(NSString*)selected;

@end

@interface BTOCSRadioSelectTableViewCell : BTBaseTableViewCell

@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UICollectionView *selectionCollectionView;

@property (strong, nonatomic) NSArray<NSString*> *options;
@property (strong, nonatomic) NSString *selectedOption;
@property (nonatomic) id<BTOCSRadioSelectTableViewCellDelegate>delegate;

- (void)updateWithOptions:(NSArray<NSString*>*)options andSelectedOption:(NSString*)selected;

@end

NS_ASSUME_NONNULL_END
