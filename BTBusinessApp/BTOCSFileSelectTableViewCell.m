//
//  BTOCSFileSelectTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 19/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSFileSelectTableViewCell.h"
#import "UIImage+Resize.h"
#import "BTOCSBaseTableViewController.h"


@interface BTOCSFileSelectTableViewCell () <UIImagePickerControllerDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate>

@property (nonatomic, strong) UIImagePickerController *imagePicker;
@property (nonatomic, strong) UIDocumentPickerViewController *documentPicker;

@property (nonatomic, strong) NSLayoutConstraint *hideConstraint;
@property (nonatomic, strong) NSLayoutConstraint *showConstraint;

@end

@implementation BTOCSFileSelectTableViewCell

+ (NSString *)reuseId
{
    return @"BTOCSFileSelectTableViewCell";
}

- (UIImagePickerController *)imagePicker
{
    if (!_imagePicker) {
        _imagePicker = [UIImagePickerController new];
        _imagePicker.delegate = self;
    }
    return _imagePicker;
}

- (UIDocumentPickerViewController *)documentPicker
{
    if (!_documentPicker) {
        _documentPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:@[@"public.data"] inMode:UIDocumentPickerModeImport];
        _documentPicker.delegate = self;
        _documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    return _documentPicker;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.uploadButton.layer.cornerRadius = 5.0f;
    self.uploadButton.layer.borderWidth = 1.0f;
    self.uploadButton.layer.borderColor = [BrandColours colourBackgroundBTPurplePrimaryColor].CGColor;
    [self.uploadButton addTarget:self action:@selector(showOptions) forControlEvents:UIControlEventTouchUpInside];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"Upload document (max 500Kb)" attributes:@{
                                                                                                                                              NSFontAttributeName: [UIFont fontWithName:@"BTFont-Regular" size: 16.0f],
                                                                                                                                              NSForegroundColorAttributeName: [BrandColours colourBackgroundBTPurplePrimaryColor]
                                                                                                                                              }];
    [attributedString addAttributes:@{
                                      NSFontAttributeName: [UIFont fontWithName:@"BTFont-Regular" size: 14.0f],
                                      NSForegroundColorAttributeName: [BrandColours colourBtLightBlack]
                                      } range:NSMakeRange(16, 11)];
    
    [self.uploadButton setAttributedTitle:attributedString forState:UIControlStateNormal];
    
    self.fileView.layer.borderWidth = 1.0f;
    self.fileView.layer.borderColor = [BrandColours colourBtLightGray].CGColor;
    
    UITapGestureRecognizer *deleteTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deleteAction)];
    deleteTapped.numberOfTapsRequired = 1;
    self.deleteIcon.userInteractionEnabled = YES;
    [self.deleteIcon addGestureRecognizer:deleteTapped];
    
    self.hideConstraint = [NSLayoutConstraint constraintWithItem:self.fileView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:0.0f];
    
}

- (void)updateWithFilename:(NSString*)filename andFileData:(NSData*)data {
    if (filename && data) {
        self.fileName = filename;
        self.fileData = data;
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ (%3.2fKb)",filename,(data.length/1024.0f)] attributes:@{
                                                                                                                                                NSFontAttributeName: [UIFont fontWithName:@"BTFont-Regular" size: 14.0f],
                                                                                                                                                NSForegroundColorAttributeName: [UIColor colorForHexString:@"666666"]
                                                                                                                                                }];
        [attributedString addAttributes:@{
                                          NSFontAttributeName: [UIFont fontWithName:@"BTFont-Regular" size: 18.0f],
                                          NSForegroundColorAttributeName: [UIColor colorForHexString:@"333333"]
                                          } range:NSMakeRange(0, filename.length)];
        
        //self.filenameLabel.text = [NSString stringWithFormat:@"%@ (%3.2fKb)",filename,(data.length/1024.0f)];
        self.filenameLabel.attributedText = attributedString;
        if ([self.fileView.constraints containsObject:self.hideConstraint]) {
            [self.fileView removeConstraint:self.hideConstraint];
        }
        self.fileView.hidden = NO;
    } else {
        [self updateWithNoFile];
    }
}

- (void)updateWithNoFile
{
    [self.fileView addConstraint:self.hideConstraint];
    self.fileView.hidden = YES;
}

- (void)deleteAction
{
    
    UIAlertController *deleteAlert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Are you sure you want to delete %@?",self.fileName ] message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    __weak typeof(self) weakSelf = self;
    
    UIAlertAction *delAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        weakSelf.fileName = @"";
        weakSelf.fileData = [NSData data];
        [weakSelf updateWithNoFile];
        [weakSelf.delegate layoutChangeInCell:self];
        if ([weakSelf.delegate isKindOfClass:[BTOCSBaseTableViewController class]]) {
            [(BTOCSBaseTableViewController*)weakSelf.delegate trackOmniClick:@"Delete file"];
        }
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
        //weakSelf.isOptionsShowing = NO;
        [deleteAlert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [deleteAlert addAction:cancel];
    [deleteAlert addAction:delAction];
    
    [self showViewController:deleteAlert];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)showOptions {
        //NSString *alertTitle = [NSString stringWithFormat:@"Select %@",self.titleLabel.text];
        NSString *alertTitle = @"Upload documents from";
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:alertTitle preferredStyle:UIAlertControllerStyleActionSheet];
        __weak typeof(self) weakSelf = self;
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf.imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [weakSelf showViewController:weakSelf.imagePicker];
    }];
    
    UIAlertAction *photoLibrary = [UIAlertAction actionWithTitle:@"Photo library" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf.imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [weakSelf showViewController:weakSelf.imagePicker];
    }];
    
    UIAlertAction *document = [UIAlertAction actionWithTitle:@"Browse..." style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf showViewController:weakSelf.documentPicker];
    }];
        
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
        //weakSelf.isOptionsShowing = NO;
        [alert dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [alert addAction:camera];
    [alert addAction:photoLibrary];
    //[alert addAction:document];
    [alert addAction:cancel];
    
    alert.popoverPresentationController.sourceView = self;
    alert.popoverPresentationController.sourceRect = self.uploadButton.frame;
    alert.popoverPresentationController.canOverlapSourceViewRect = NO;
    
    [self showViewController:alert];
        
//        if (self.delegate && [self.delegate isKindOfClass:[UIViewController class]]) {
//            [(UIViewController*)self.delegate presentViewController:alert animated:YES completion:^{
//                //weakSelf.isOptionsShowing = YES;
//            }];
//        } else {
//            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:^{
//                //weakSelf.isOptionsShowing = YES;
//            }];
//        }
    
}

- (void)showViewController:(UIViewController*)viewController
{
    if (self.delegate && [self.delegate isKindOfClass:[UIViewController class]]) {
        [(UIViewController*)self.delegate presentViewController:viewController animated:YES completion:^{
            //
        }];
    } else {
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:viewController animated:YES completion:^{
            //
        }];
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info
{
    NSLog(@"file data: %@",info);
    UIImage *image = nil;
    if ([info objectForKey:UIImagePickerControllerEditedImage]) {
        image = [info objectForKey:UIImagePickerControllerEditedImage];
    } else if ([info objectForKey:UIImagePickerControllerOriginalImage]) {
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    if (image) {
        BOOL needCompress = NO;
        CGFloat maxSize = 500*1024.0f; // 500kb
        CGFloat compression = 1.0f;
        NSData *imageData = nil;;
        do {
            imageData = UIImageJPEGRepresentation(image, compression);
            //imageData = UIImagePNGRepresentation(image);
            if (imageData.length > maxSize) {
                needCompress = YES;
                //CGFloat factor = MIN(0.9, (maxSize/imageData.length));
                compression = compression - 0.1;
                //image = [image resizeWithScaleFactor:pow(compression, 0.5)];
                image = [image resizeWithScaleFactor:compression];
            } else {
                needCompress = NO;
            }
        } while (needCompress);
        
        [self updateWithFilename:@"photo.jpg" andFileData:imageData];
        [self.delegate layoutChangeInCell:self];
        if ([self.delegate isKindOfClass:[BTOCSBaseTableViewController class]]) {
            [(BTOCSBaseTableViewController*)self.delegate trackOmniClick:@"Upload document"];
        }
    }
    
    [picker dismissViewControllerAnimated:YES completion:^{
        //
    }];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

#pragma mark - UIDocumentPickerDelegate

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url
{
    if (controller.documentPickerMode == UIDocumentPickerModeImport) {
        NSString *fileName = url.lastPathComponent;
        NSData *fileData = [NSData dataWithContentsOfURL:url];
        [self updateWithFilename:fileName andFileData:fileData];
        [self.delegate layoutChangeInCell:self];
    }
}

@end
