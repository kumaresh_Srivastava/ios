//
//  BTOCSSimpleButtonTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

@interface BTOCSSimpleButtonTableViewCell : BTBaseTableViewCell

@property (strong, nonatomic) IBOutlet UIButton *button;

@end
