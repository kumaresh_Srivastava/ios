//
//  BTOCSRadioSelectTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 16/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSRadioSelectTableViewCell.h"
#import "BTOCSRadioSelectCollectionViewCell.h"

@interface BTOCSRadioSelectTableViewCell () <UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@end

@implementation BTOCSRadioSelectTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.selectionCollectionView.dataSource = self;
    self.selectionCollectionView.delegate = self;
    self.selectionCollectionView.allowsSelection = YES;
    self.selectionCollectionView.scrollEnabled = NO;
    [self.selectionCollectionView registerNib:[UINib nibWithNibName:@"BTOCSRadioSelectCollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"BTOCSRadioSelectCollectionViewCell"];
    
    if (@available(iOS 11.0, *)) {
        //UICollectionViewFlowLayout *flow = (UICollectionViewFlowLayout*)self.selectionCollectionView.collectionViewLayout;
        //flow.estimatedItemSize = CGSizeMake(self.selectionCollectionView.frame.size.width-30, 50);
       // flow.estimatedItemSize = CGSizeMake(kScreenSize.width-30, 50);
    } else {
        // Fallback on earlier versions
        
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChangeradio:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


+ (NSString *)reuseId
{
    return @"BTOCSRadioSelectTableViewCell";
}

- (void)updateWithOptions:(NSArray<NSString *> *)options andSelectedOption:(NSString *)selected
{
    self.options = [NSArray arrayWithArray:options];
    self.selectedOption = selected;
    
    CGFloat height = (self.options.count*60.0);
    [self.selectionCollectionView addConstraint:[NSLayoutConstraint constraintWithItem:self.selectionCollectionView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:height]];
    
   // CGFloat minWidth = kScreenSize.width-30;
   // [self.selectionCollectionView addConstraint:[NSLayoutConstraint constraintWithItem:self.selectionCollectionView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:minWidth]];
    
    [self.selectionCollectionView reloadData];
}

- (void)statusBarOrientationChangeradio:(NSNotification *)notification {
    // handle the interface orientation as needed
    [self.selectionCollectionView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    [self.contentView layoutIfNeeded];
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger items = 0;
    if (self.options && self.options.count) {
        items = self.options.count;
    }
    return items;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BTOCSRadioSelectCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BTOCSRadioSelectCollectionViewCell" forIndexPath:indexPath];
    if (!cell) {
        cell = [[BTOCSRadioSelectCollectionViewCell alloc] init];
    }
    if (self.options && indexPath.item < self.options.count) {
        NSString *title = [self.options objectAtIndex:indexPath.item];
        cell.optionTextLabel.text = title;
        if (self.selectedOption && [title isEqualToString:self.selectedOption]) {
            // this is the selected cell
            [cell setSelected:YES];
        } else {
            [cell setSelected:NO];
        }
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //[self.selectionCollectionView.superview layoutIfNeeded];
   // [self.selectionCollectionView layoutIfNeeded]; // avoids crash on ios10 on plus devices
    //return CGSizeMake(self.selectionCollectionView.frame.size.width-30, 50);
    return CGSizeMake(kScreenSize.width-30, 50);
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.options && indexPath.item < self.options.count) {
        self.selectedOption = [self.options objectAtIndex:indexPath.item];
        [self.selectionCollectionView reloadData];
        if (self.delegate && [self.delegate respondsToSelector:@selector(radioSelectCell:updatedSelection:)]) {
            [self.delegate radioSelectCell:self updatedSelection:self.selectedOption];
        }
    }
}


@end
