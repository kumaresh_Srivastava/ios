//
//  NLCheckWorkflowStatus.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 16/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLCheckWorkflowStatus.h"
#import "BTAuthenticationManager.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "BTUser.h"
#import "AppConstants.h"
#import "NLVordelAuthenticationProtectedWebService+MbaasHeader.h"
#import "NLWebServiceError.h"
#import "NLWebService+BackEndAPIErrorHandling.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "AppManager.h"
#import "NSObject+APIResponseCheck.h"

@interface NLCheckWorkflowStatus () {
    NSString *_executionId;
    NSInteger retryCount;
    NSInteger maxRetries;
    NSTimeInterval retryWait;
}

@end

@implementation NLCheckWorkflowStatus

- (instancetype)initWithExecutionId:(NSString *)executionId
{
    if (executionId == nil || [executionId isEqualToString:@""])
    {
        return nil;
    }
    
    NSDictionary *header = [self getMbaasHeader];
    
    NSDictionary *checkWorkflowDict = [NSDictionary dictionaryWithObject:executionId forKey:@"executionId"];
    NSDictionary *body = [NSDictionary dictionaryWithObject:checkWorkflowDict forKey:@"checkWorkflowStatus"];
    
    NSDictionary *payload = [NSDictionary dictionaryWithObjectsAndKeys:header, @"MobileServiceHeader", body, @"Body", nil];
    
    //NSDictionary *parentPayload = [NSDictionary dictionaryWithObject:payload forKey: @"MobileServiceRequest"];
    
    NSLog(@"CHECK WORKFLOW STATUS PAYLOAD IS: %@", payload);
    
    NSError *errorWhileCreatingJSONData = nil;
    NSData *mbaasHeader = [NSJSONSerialization dataWithJSONObject:payload options:0 error:&errorWhileCreatingJSONData];
    
    if(errorWhileCreatingJSONData)
    {
        DDLogError(@"Error while creating JSON data for checkWorkflowStatus");
    }
    
    NSString *jsonString = [[NSString alloc] initWithData:mbaasHeader encoding:NSUTF8StringEncoding];
    
    NSString *endPointURLString = @"/manageworkflows/checkworkflowstatus";
    
    self = [super initWithMethod:@"POST" parameters:jsonString andEndpointUrlString:endPointURLString];
    
    if (self)
    {
        _executionId = [executionId copy];
        retryCount = 0;
        maxRetries = 6;
        retryWait = 15.0;
    }
    return self;
}

- (NSString *)friendlyName
{
    return @"CheckWorkflowStatus";
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *response = [responseObject valueForKey:@"MobileServiceResponse"];
    NSDictionary *resultDic = [[response valueForKey:@"Body"] valueForKey:@"checkWorkflowStatusResponse"];
    
    NSString *stepId = [[resultDic objectForKey:@"stepID"] validAndNotEmptyStringObject];
    NSObject *content = [resultDic objectForKey:@"content"];

    if (stepId == NULL || [stepId isEqualToString:@""] || [stepId isEqual:[NSNull null]]) {
        retryCount++;
        if (retryCount <= maxRetries) {
            [self performSelector:@selector(resume) withObject:NULL afterDelay:retryWait];
        } else {
            if ([self.checkWorkflowStatusDelegate respondsToSelector:@selector(timedOutWithoutSuccessForCheckWorkflowStatusWebService:)]) {
                [self.checkWorkflowStatusDelegate timedOutWithoutSuccessForCheckWorkflowStatusWebService:self];
            } else {
                [self.checkWorkflowStatusDelegate checkWorkflowStatusWebService:self failedWithWebServiceError:nil];
            }
        }
    }
    else {
        [self.checkWorkflowStatusDelegate checkWorkflowStatusWebService:self finishedWithResponse:content];
    }
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    [self.checkWorkflowStatusDelegate checkWorkflowStatusWebService:self failedWithWebServiceError:webServiceError];
}

#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        
        if([responseObject isValidVordelAPIResponseObject])
        {
            //NSLog(@"workflow response body: %@", responseObject);
            NSDictionary *response = [responseObject valueForKey:@"MobileServiceResponse"];
            NSDictionary *body = [response valueForKey:@"Body"];
            if([body valueForKey:@"checkWorkflowStatusResponse"])
            {
                NSDictionary *checkWorkflowStatusResponse = [body valueForKey:@"checkWorkflowStatusResponse"];
                if (!([checkWorkflowStatusResponse objectForKey:@"stepID"] && [checkWorkflowStatusResponse objectForKey:@"content"]))
                {
                    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
                    errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
                    DDLogError(@"'checkWorkflowStatusResponse' missing required elements in Body of response object for URL %@", [[task currentRequest] URL]);
                }
            }
            else
            {
                NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
                errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
                DDLogError(@"'checkWorkflowStatusResponse' element is missing in Body of response object for URL %@", [[task currentRequest] URL]);
            }
        }
        else
        {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
            
            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
            DDLogError(@"Invalid Response Object for URL %@", [[task currentRequest] URL]);
        }
    }
    
    return errorToBeReturned;
}

@end
