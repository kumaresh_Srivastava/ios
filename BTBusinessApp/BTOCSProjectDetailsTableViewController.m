//
//  BTOCSProjectDetailsTableViewController.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 07/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSProjectDetailsTableViewController.h"

#import "NLGetProjectDetails.h"
#import "BTOCSKeyValueDescription.h"
#import "BTOCSAdditionalProjectDetailsTableViewCell.h"

#import "BTOCSSendQueryTableViewController.h"

@interface BTOCSProjectDetailsTableViewController () <NLGetProjectDetailsDelegate>

@property (strong, nonatomic) NLGetProjectDetails *getProjectDetailsWebService;

@property (strong, nonatomic) BTOCSSimpleButtonTableViewCell *needHelpButtonCell;
@property (strong, nonatomic) BTOCSSimpleButtonTableViewCell *closeButtonCell;

@end

@implementation BTOCSProjectDetailsTableViewController

+ (BTOCSProjectDetailsTableViewController *)getOCSProjectDetailsViewController
{
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"BTOCSOrders" bundle:nil];
    //BTOCSProjectDetailsTableViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTOCSProjectDetailsTableViewController"];
    BTOCSProjectDetailsTableViewController *vc = [[BTOCSProjectDetailsTableViewController alloc] initWithStyle:UITableViewStylePlain];
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.title = @"Project details";
    [self getProjectDetails];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupTableCells
{
    [super setupTableCells];
    // need help cell
    if (!self.needHelpButtonCell) {
        self.needHelpButtonCell = (BTOCSSimpleButtonTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSSimpleButtonTableViewCell reuseId]];
        self.needHelpButtonCell.button.titleLabel.text = @"Need help? Get in touch";
        self.needHelpButtonCell.button.backgroundColor = [UIColor clearColor];
        self.needHelpButtonCell.button.titleLabel.textColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
        [self.needHelpButtonCell.button addTarget:self action:@selector(needHelpAction) forControlEvents:UIControlEventTouchUpInside];
    }
    
    // need help cell
    if (!self.closeButtonCell) {
        self.closeButtonCell = (BTOCSSimpleButtonTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSSimpleButtonTableViewCell reuseId]];
        self.closeButtonCell.button.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:@"Close" attributes:@{NSFontAttributeName:[UIFont fontWithName:kBtFontBold size:16.0],NSForegroundColorAttributeName:[UIColor whiteColor]}];
        [self.closeButtonCell.button setAttributedTitle:attributedTitle forState:UIControlStateNormal];
        [self.closeButtonCell.button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.closeButtonCell.button addTarget:self action:@selector(closeAction) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)updateTableCells
{
    [super updateTableCells];
    [self.tableCells removeAllObjects];
    
    // project details cells
    if (self.project.contactDetails && (self.project.contactDetails.count > 0)) {
        for (BTOCSContactDetail *contact in self.project.contactDetails) {
            BTOCSProjectDetailsTableViewCell *cell = (BTOCSProjectDetailsTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSProjectDetailsTableViewCell reuseId]];
//            if ([contact.type isEqualToString:@"Site"]) {
//                cell.headerLabel.text = [self.selectedSite.name stringByAppendingString:@" contact"];
//            } else {
//                cell.headerLabel.text = [contact.type stringByAppendingString:@" contact"];
//            }
            cell.headerLabel.text = [contact.type stringByAppendingString:@" contact"];
            cell.nameLabel.text = contact.name;
            cell.emailLabel.text = [contact.email stringByReplacingOccurrencesOfString:@";" withString:@"\n"];
            cell.phoneLabel.text = contact.telephone;
            cell.mobileLabel.text = contact.mobile;
            
            cell.queryTitleLabel.hidden = YES;
            cell.queryLabel.hidden = YES;
            
            [self.tableCells addObject:cell];
        }
    }
    
    if (self.project.keyValueDescriptions && (self.project.keyValueDescriptions.count > 0)) {
        for (BTOCSKeyValueDescription *kvd in self.project.keyValueDescriptions) {
            BTOCSAdditionalProjectDetailsTableViewCell *kvdCell = (BTOCSAdditionalProjectDetailsTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSAdditionalProjectDetailsTableViewCell reuseId]];
            kvdCell.headerLabel.text = kvd.keyString;
            kvdCell.boldLabel.text = kvd.keyString;
            kvdCell.valueLabel.text = kvd.valueString;
            kvdCell.descriptionLabel.text = kvd.descriptionString;

            [self.tableCells addObject:kvdCell];
        }
    }
    
    // footer button cells
    if (!self.project.hideContactPanel) {
        [self.tableCells addObject:self.needHelpButtonCell];
    }
    [self.tableCells addObject:self.closeButtonCell];
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
//    return 0;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
//    return 0;
//}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - action button events

- (void)needHelpAction
{
    BTOCSSendQueryTableViewController *vc = [BTOCSSendQueryTableViewController getOCSSendQueryViewController];
    vc.project = self.project;
    vc.selectedSite = self.selectedSite;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
   // [nav setModalPresentationStyle:UIModalPresentationPopover];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    vc.view.frame = CGRectMake(0, 50, screenBounds.size.width, screenBounds.size.height);
    [self presentViewController:nav animated:YES completion:^{
        //
    }];
    [self trackOmniClick:self.needHelpButtonCell.button.titleLabel.text];
}

- (void)closeAction
{
    [self.navigationController popViewControllerAnimated:YES];
    [self trackOmniClick:OMNICLICK_OCS_ORDER_CLOSE];
}

#pragma mark - api call

- (void)getProjectDetails
{
    if (self.getProjectDetailsWebService) {
        [self.getProjectDetailsWebService cancel];
        self.getProjectDetailsWebService.getProjectDetailsDelegate = nil;
        self.getProjectDetailsWebService = nil;
    }
    
    if ([AppManager isInternetConnectionAvailable]) {
        if (self.project && self.project.projectRef) {
            NSString *siteId = [NSString stringWithFormat:@"%li",(long)self.selectedSite.siteID];
            self.getProjectDetailsWebService = [[NLGetProjectDetails alloc] initWithProjectRef:self.project.projectRef andSiteId:siteId];
            self.getProjectDetailsWebService.getProjectDetailsDelegate = self;
            if (!self.networkRequestInProgress) {
                self.networkRequestInProgress = YES;
            }
            [self.getProjectDetailsWebService resume];
        }
    } else {
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:self.pageNameForOmnitureTracking];
    }
}

#pragma mark - getProjectDetails delegate

- (void)getProjectDetailsWebService:(NLGetProjectDetails *)webService finishedWithResponse:(NSObject *)response
{
    self.networkRequestInProgress = NO;
    [self.project updateWithProjectDetailsResponse:(NSDictionary*)response];
    [self refreshTableView];
}

- (void)getProjectDetailsWebService:(NLGetProjectDetails *)webService failedWithWebServiceError:(NLWebServiceError *)error
{
    self.networkRequestInProgress = NO;
    
    
    [AppManager trackWebServiceError:error FromAPI:webService.friendlyName OnPage:self.pageNameForOmnitureTracking];
    BOOL errorHandled = [self attemptVordelProtectedAPIErrorHandlingOfWebServiceError:error];
    if (!errorHandled) {
        [self showRetryViewWithInternetStrip:NO];
    }
}

#pragma mark - retry view delegate

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    [self getProjectDetails];
}

#pragma mark - omniture

- (NSString *)pageNameForOmnitureTracking
{
    return OMNIPAGE_OCS_ORDER_PROJECTDETAILS;
}

- (void)trackPageForOmniture
{
    if (self.project.productService && self.project.subStatus) {
        [self trackPageForOmnitureWithProductService:self.project.productService andOrderStatus:self.project.subStatus];
    } else {
        [super trackPageForOmniture];
    }
}

@end
