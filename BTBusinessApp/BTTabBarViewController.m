//
//  BTTabBarViewController.m
//  BTBusinessApp
//
//  Copyright © 2019 BT. All rights reserved.
//

#import "BTTabBarViewController.h"
#import "AppConstants.h"

#import "BTHomeViewController.h"
#import "BTBillsDashBoardViewController.h"

@interface BTTabBarViewController ()

@property (nonatomic) BTHomeNavigationController *homeNavigationController;

@end

@implementation BTTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.delegate = self;
    
    self.tabBar.backgroundColor = [BrandColours colourBtNeutral30];
    self.tabBar.tintColor = [BrandColours colourTextBTPurplePrimaryColor];
    [[UITabBarItem appearance] setTitleTextAttributes:@{
                                                        NSFontAttributeName:[UIFont fontWithName:kBtFontRegular size:13.0f]
                                                        } forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:@{
                                                        NSFontAttributeName:[UIFont fontWithName:kBtFontRegular size:13.0f]
                                                        } forState:UIControlStateSelected];
    
    [self setSelectedIndex:1];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Public methods
+ (BTTabBarViewController *)getViewController {
    BTTabBarViewController *tabBarViewController = [[BTTabBarViewController alloc] init];
    return tabBarViewController;
}

- (id)initWithHomeNavigationController:(BTHomeNavigationController *)homeNavigationController {
    _homeNavigationController = homeNavigationController;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSMutableArray *tabViewControllers = [[NSMutableArray alloc] init];
    
    // Home
    [tabViewControllers addObject:homeNavigationController];
    
    // Bill
    BTBillsDashBoardViewController *billsDashboardViewController = [storyboard instantiateViewControllerWithIdentifier:@"billSummaryScene"];
    [tabViewControllers addObject:billsDashboardViewController];
    
    // Usage
    
    // Account
    
    // More
    
    [self setViewControllers:tabViewControllers];
    
    homeNavigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Home"
                                                                        image:[UIImage imageNamed:@"home_tab"]
                                                                          tag:1];
    
    billsDashboardViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Billing"
                                                                        image:[UIImage imageNamed:@"billing_tab"]
                                                                          tag:2];
    
    return self;
}

#pragma mark - tab view delegate methods
//- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
//    if([viewController isKindOfClass:[BTBillsDashBoardViewController class]]) {
//        // push billsdash
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        BTBillsDashBoardViewController *billsDashboardViewController = [storyboard instantiateViewControllerWithIdentifier:@"billSummaryScene"];
//        [_homeNavigationController pushViewController:billsDashboardViewController animated:YES];
//    }
//}

//- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
//    [self setSelectedIndex:2];
//    return NO;
//}

# pragma mark - slide menu delegate methods
- (void)noUnReadItemsLeftInNotifications {
    //
}

- (void)performLogOutActionForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    //
}

- (void)redirectToAccountScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    //
}

- (void)redirectToBillsScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    //
}

- (void)redirectToDashboardScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    //
}

- (void)redirectToHelpScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    //
}

- (void)redirectToLoggerScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    //
}

- (void)redirectToOnBoardingScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    //
}

- (void)redirectToPublicWifiScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    //
}

- (void)redirectToServiceStatusScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    //
}

- (void)redirectToSettingsScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    //
}

- (void)redirectToTermsAndConditonScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    //
}
- (void)redirectToMoreBTBusinessAppsScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    //
}

- (void)redirectToTrackFaultsScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    //
}

- (void)redirectToTrackOrdersScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    //
}

- (void)redirectToUsageScreenForSlideMenuParentViewController:(BTSlideMenuParentViewController *)slideMenuParentController {
    //
}

- (void)userPressedOnRetryButtonForNotifications {
    //
}

- (UIViewController *)viewControllerForMenuButton {
    return nil;
}

@end
