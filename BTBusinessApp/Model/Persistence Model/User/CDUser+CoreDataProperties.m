//
//  CDUser+CoreDataProperties.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 28/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CDUser+CoreDataProperties.h"

@implementation CDUser (CoreDataProperties)

@dynamic firstName;
@dynamic lastName;
@dynamic titleInName;
@dynamic username;
@dynamic securityQuestion;
@dynamic securityAnswer;
@dynamic mobileNumber;
@dynamic landlineNumber;
@dynamic primaryEmailAddress;
@dynamic alternativeEmailAddress;
@dynamic app;
@dynamic token;
@dynamic smsession;
@dynamic cugs;
@dynamic currentSelectedCug;
@dynamic recentlySearchedFaults;
@dynamic recentlySearchedOrders;
@dynamic contactId;
@end
