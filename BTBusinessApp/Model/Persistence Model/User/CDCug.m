//
//  CDCug.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 13/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "CDCug.h"

@implementation CDCug

// Insert code here to add functionality to your managed object subclass

+ (NSEntityDescription *)entityInManagedObjectContext:(NSManagedObjectContext *)context
{
    return [NSEntityDescription entityForName:@"CDCug" inManagedObjectContext:context];
}

+ (CDCug *)newCugInManagedObjectContext:(NSManagedObjectContext *)context
{
    CDCug *cug = (CDCug *)[NSEntityDescription insertNewObjectForEntityForName:@"CDCug" inManagedObjectContext:context];
    
    return cug;
}

@end
