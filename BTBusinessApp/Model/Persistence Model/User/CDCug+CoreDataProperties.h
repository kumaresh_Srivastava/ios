//
//  CDCug+CoreDataProperties.h
//  
//
//  Created by VS-Saddam Husain-MacBookPro on 10/01/17.
//
//

#import "CDCug+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CDCug (CoreDataProperties)

+ (NSFetchRequest<CDCug *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *cugName;
@property (nullable, nonatomic, copy) NSString *cugId;
@property (nullable, nonatomic, copy) NSNumber *cugRole;
@property (nullable, nonatomic, copy) NSString *groupKey;
@property (nullable, nonatomic, copy) NSNumber *indexInAPIResponse;
@property (nullable, nonatomic, copy) NSString *refKey;
@property (nullable, nonatomic, copy) NSString *contactId;
@property (nullable, nonatomic, retain) NSManagedObject *user;
@property (nullable, nonatomic, retain) NSManagedObject *userWhoHasSelected;

@end

NS_ASSUME_NONNULL_END
