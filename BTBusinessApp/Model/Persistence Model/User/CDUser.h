//
//  CDUser.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 28/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CDAuthenticationToken;
@class CDSMSession;
@class CDApp;
@class CDCug;
@class CDRecentSearchedFault;
@class CDRecentSearchedOrder;

NS_ASSUME_NONNULL_BEGIN

@interface CDUser : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

+ (nullable CDUser *)userWithUsername:(NSString *)username
      inManagedObjectContext:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "CDUser+CoreDataProperties.h"
