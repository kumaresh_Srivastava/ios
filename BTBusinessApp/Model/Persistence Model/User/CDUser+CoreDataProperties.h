//
//  CDUser+CoreDataProperties.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 28/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CDUser.h"

NS_ASSUME_NONNULL_BEGIN

@interface CDUser (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *firstName;
@property (nullable, nonatomic, retain) NSString *lastName;
@property (nullable, nonatomic, retain) NSString *titleInName;
@property (nullable, nonatomic, retain) NSString *username;
@property (nullable, nonatomic, retain) NSString *securityQuestion;
@property (nullable, nonatomic, retain) NSString *securityAnswer;
@property (nullable, nonatomic, retain) NSString *mobileNumber;
@property (nullable, nonatomic, retain) NSString *landlineNumber;
@property (nullable, nonatomic, retain) NSString *primaryEmailAddress;
@property (nullable, nonatomic, retain) NSString *alternativeEmailAddress;
@property (nullable, nonatomic, retain) NSString *contactId;
@property (nullable, nonatomic, retain) CDApp *app;
@property (nullable, nonatomic, retain) CDAuthenticationToken *token;
@property (nullable, nonatomic, retain) CDSMSession *smsession;
@property (nullable, nonatomic, retain) NSSet<CDCug *> *cugs;
@property (nullable, nonatomic, retain) CDCug *currentSelectedCug;
@property (nullable, nonatomic, retain) NSSet<CDRecentSearchedFault *> *recentlySearchedFaults;
@property (nullable, nonatomic, retain) NSSet<CDRecentSearchedOrder *> *recentlySearchedOrders;

@end


@interface NSManagedObject (CoreDataGeneratedAccessors)

- (void)addCugsObject:(CDCug *)value;
- (void)removeCugsObject:(CDCug *)value;
- (void)addCugs:(NSSet<CDCug *> *)values;
- (void)removeCugs:(NSSet<CDCug *> *)values;

- (void)addRecentlySearchedFaultsObject:(CDRecentSearchedFault *)value;
- (void)removeRecentlySearchedFaultsObject:(CDRecentSearchedFault *)value;
- (void)addRecentlySearchedFaults:(NSSet<CDRecentSearchedFault *> *)values;
- (void)removeRecentlySearchedFaults:(NSSet<CDRecentSearchedFault *> *)values;

- (void)addRecentlySearchedOrdersObject:(CDRecentSearchedOrder *)value;
- (void)removeRecentlySearchedOrdersObject:(CDRecentSearchedOrder *)value;
- (void)addRecentlySearchedOrders:(NSSet<CDRecentSearchedOrder *> *)values;
- (void)removeRecentlySearchedOrders:(NSSet<CDRecentSearchedOrder *> *)values;

@end

NS_ASSUME_NONNULL_END
