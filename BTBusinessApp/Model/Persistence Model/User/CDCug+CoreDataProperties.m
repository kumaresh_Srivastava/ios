//
//  CDCug+CoreDataProperties.m
//  
//
//  Created by VS-Saddam Husain-MacBookPro on 10/01/17.
//
//

#import "CDCug+CoreDataProperties.h"

@implementation CDCug (CoreDataProperties)

+ (NSFetchRequest<CDCug *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"CDCug"];
}

@dynamic cugName;
@dynamic cugRole;
@dynamic groupKey;
@dynamic indexInAPIResponse;
@dynamic refKey;
@dynamic contactId;
@dynamic user;
@dynamic userWhoHasSelected;


@end
