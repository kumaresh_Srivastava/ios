//
//  CDCug.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 13/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CDUser;

NS_ASSUME_NONNULL_BEGIN

@interface CDCug : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

+ (CDCug *)newCugInManagedObjectContext:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "CDCug+CoreDataProperties.h"
