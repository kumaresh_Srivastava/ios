//
//  CDCug+CoreDataClass.m
//  
//
//  Created by VS-Saddam Husain-MacBookPro on 10/01/17.
//
//

#import "CDCug+CoreDataClass.h"

@implementation CDCug

+ (NSEntityDescription *)entityInManagedObjectContext:(NSManagedObjectContext *)context
{
    return [NSEntityDescription entityForName:@"CDCug" inManagedObjectContext:context];
}

+ (CDCug *)newCugInManagedObjectContext:(NSManagedObjectContext *)context
{
    CDCug *cug = (CDCug *)[NSEntityDescription insertNewObjectForEntityForName:@"CDCug" inManagedObjectContext:context];

    return cug;
}

@end
