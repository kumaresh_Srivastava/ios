//
//  CDCug+CoreDataClass.h
//  
//
//  Created by VS-Saddam Husain-MacBookPro on 10/01/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface CDCug : NSManagedObject


+ (CDCug *)newCugInManagedObjectContext:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "CDCug+CoreDataProperties.h"
