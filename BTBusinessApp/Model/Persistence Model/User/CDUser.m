//
//  CDUser.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 28/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "CDUser.h"

@implementation CDUser

// Insert code here to add functionality to your managed object subclass


+ (NSEntityDescription *)entityInManagedObjectContext:(NSManagedObjectContext *)context
{
    return [NSEntityDescription entityForName:@"CDUser" inManagedObjectContext:context];
}

+ (nullable CDUser *)userWithUsername:(NSString *)username
        inManagedObjectContext:(NSManagedObjectContext *)context
{
    CDUser *user = nil;

    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[self entityInManagedObjectContext:context]];

    NSPredicate *usernamePredicate = [NSPredicate predicateWithFormat:@"username = %@", username];
    [fetchRequest setPredicate:usernamePredicate];

    NSError *__autoreleasing error = nil;
    NSArray *resultArray = [context executeFetchRequest:fetchRequest error:&error];

    if(error)
    {
        DDLogError(@"Persistence: There was an error trying to fetch the CDUser object from the database: %@", error);
    }
    else if([resultArray count] == 1)
    {
        user = [resultArray objectAtIndex:0];
    }
    else if([resultArray count] == 0 || resultArray == nil)
    {
        user = (CDUser *)[NSEntityDescription insertNewObjectForEntityForName:@"CDUser" inManagedObjectContext:context];
        user.username = username;
    }
    else if([resultArray count] > 1)
    {
        user = [resultArray objectAtIndex:0];

        DDLogError(@"Persistence: While trying to fetch CDUser from database, we have found more than 1 object for CDUser with same username which is something that should not happen.");
    }
    
    return user;
}


@end
