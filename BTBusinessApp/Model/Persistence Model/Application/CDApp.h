//
//  CDApp.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 28/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CDUser;

NS_ASSUME_NONNULL_BEGIN

@interface CDApp : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

+ (nullable CDApp *)appInManagedObjectContext:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "CDApp+CoreDataProperties.h"
