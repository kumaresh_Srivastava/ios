//
//  CDSMSession+CoreDataProperties.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 16/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "CDSMSession+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CDSMSession (CoreDataProperties)

+ (NSFetchRequest<CDSMSession *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *creationTime;
@property (nullable, nonatomic, copy) NSString *smsessionString;
@property (nullable, nonatomic, retain) CDUser *user;

@end

NS_ASSUME_NONNULL_END
