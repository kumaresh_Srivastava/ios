//
//  CDSMSession+CoreDataClass.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 16/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CDUser;

NS_ASSUME_NONNULL_BEGIN

@interface CDSMSession : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "CDSMSession+CoreDataProperties.h"
