//
//  CDAuthenticationToken+CoreDataProperties.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 13/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CDAuthenticationToken.h"

NS_ASSUME_NONNULL_BEGIN

@interface CDAuthenticationToken (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *accessToken;
@property (nullable, nonatomic, retain) NSNumber *expiresIn;
@property (nullable, nonatomic, retain) NSDate *creationTime;
@property (nullable, nonatomic, retain) NSString *refreshToken;
@property (nullable, nonatomic, retain) NSString *tokenString;
@property (nullable, nonatomic, retain) NSString *tokenType;
@property (nullable, nonatomic, retain) CDUser *user;

@end

NS_ASSUME_NONNULL_END
