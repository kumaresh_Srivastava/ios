//
//  CDAuthenticationToken+CoreDataProperties.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 13/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CDAuthenticationToken+CoreDataProperties.h"

@implementation CDAuthenticationToken (CoreDataProperties)

@dynamic accessToken;
@dynamic expiresIn;
@dynamic creationTime;
@dynamic refreshToken;
@dynamic tokenString;
@dynamic tokenType;
@dynamic user;

@end
