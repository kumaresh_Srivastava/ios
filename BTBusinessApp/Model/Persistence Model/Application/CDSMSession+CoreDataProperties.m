//
//  CDSMSession+CoreDataProperties.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 16/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "CDSMSession+CoreDataProperties.h"

@implementation CDSMSession (CoreDataProperties)

+ (NSFetchRequest<CDSMSession *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"CDSMSession"];
}

@dynamic creationTime;
@dynamic smsessionString;
@dynamic user;

@end
