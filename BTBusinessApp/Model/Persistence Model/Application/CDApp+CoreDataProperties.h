//
//  CDApp+CoreDataProperties.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 28/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CDApp.h"

NS_ASSUME_NONNULL_BEGIN

@interface CDApp (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *btInstallationID;
@property (nullable, nonatomic, retain) CDUser *loggedInUser;
@property (nullable, nonatomic, retain) NSNumber *needsOnboardingDisplay;
@property (nullable, nonatomic, retain) NSNumber *isFirstAppLaunch;
@property (nullable, nonatomic, retain) NSNumber *isTouchIDEnabledForUnlock;
@property (nullable, nonatomic, retain) NSNumber *isFaceIDEnabledForUnlock;

@end

NS_ASSUME_NONNULL_END
