//
//  CDApp.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 28/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "CDApp.h"
#import "CDUser.h"

@implementation CDApp

// Insert code here to add functionality to your managed object subclass

+ (NSEntityDescription *)entityInManagedObjectContext:(NSManagedObjectContext *)context
{
    return [NSEntityDescription entityForName:@"CDApp" inManagedObjectContext:context];
}

+ (nullable CDApp *)appInManagedObjectContext:(NSManagedObjectContext *)context
{
    CDApp *appObject = nil;

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[self entityInManagedObjectContext:context]];

    NSError *__autoreleasing error = nil;
    NSArray *resultArray = [context executeFetchRequest:fetchRequest error:&error];

    if(error)
    {
        DDLogError(@"Persistence: There was an error trying to fetch the CDApp object from the database: %@", error);
    }
    else if([resultArray count] == 1)
    {
        appObject = [resultArray objectAtIndex:0];
    }
    else if([resultArray count] == 0 || resultArray == nil)
    {
        // (hd) Creating a new object for the CDApp and this should happen only once after the app has been installed till it is unisntalled again.
        appObject = (CDApp *)[NSEntityDescription insertNewObjectForEntityForName:@"CDApp" inManagedObjectContext:context];

        // (hd) Onboarding needs to be displayed once when the user first installs the app. Our logic is such that this new object of CDApp should be created only once and that too when the app is first launched.
        appObject.needsOnboardingDisplay = [NSNumber numberWithBool:YES];

        appObject.isFirstAppLaunch = [NSNumber numberWithBool:YES];
        
        // (lp) Setting app unlock using touch id. User can unlock using pin or touch id. Initially this feature will be disabled but user can change this behaviour on first launch or in security settings of app.  
        appObject.isTouchIDEnabledForUnlock = [NSNumber numberWithBool:NO];
        
        // (lp) Setting app unlock using Face ID. User can unlock using pin or Face ID. Initially this feature will be disabled but user can change this behaviour on first launch or in security settings of app.
        appObject.isFaceIDEnabledForUnlock = [NSNumber numberWithBool:NO];
    }
    else if([resultArray count] > 1)
    {
        appObject = [resultArray objectAtIndex:0];

        DDLogError(@"Persistence: While trying to fetch CDApp from database, we have found more than 1 object for CDApp which is something that should not happen.");
    }
    
    return appObject;
}

@end
