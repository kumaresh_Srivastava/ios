//
//  CDEngineerAppointmentForOrder.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 06/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface CDEngineerAppointmentForOrder : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

+ (CDEngineerAppointmentForOrder *)newEngineerAppointmentForOrderInManagedObjectContext:(NSManagedObjectContext *)context withItemRef:(NSString *)itemRef andCalenderEventID:(NSString *)calenderEventID;

@end

NS_ASSUME_NONNULL_END

#import "CDEngineerAppointmentForOrder+CoreDataProperties.h"
