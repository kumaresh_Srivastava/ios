//
//  CDEngineerAppointmentForOrder+CoreDataProperties.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 06/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CDEngineerAppointmentForOrder+CoreDataProperties.h"

@implementation CDEngineerAppointmentForOrder (CoreDataProperties)

@dynamic itemRef;
@dynamic localCalenderEventID;

@end
