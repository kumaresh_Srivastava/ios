//
//  CDRecentSearchedOrder+CoreDataProperties.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 14/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CDRecentSearchedOrder+CoreDataProperties.h"

@implementation CDRecentSearchedOrder (CoreDataProperties)

@dynamic orderRef;
@dynamic lastSearchedDate;
@dynamic orderDescription;
@dynamic placedOnDate;
@dynamic completionDate;
@dynamic orderStatus;
@dynamic user;

@end
