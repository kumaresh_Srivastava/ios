//
//  CDRecentSearchedOrder.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 14/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "CDRecentSearchedOrder.h"

@implementation CDRecentSearchedOrder

// Insert code here to add functionality to your managed object subclass

+ (NSEntityDescription *)entityInManagedObjectContext:(NSManagedObjectContext *)context
{
    return [NSEntityDescription entityForName:@"CDRecentSearchedOrder" inManagedObjectContext:context];
}

+ (CDRecentSearchedOrder *)newRecentSearchedOrderInManagedObjectContext:(NSManagedObjectContext *)context
{
    CDRecentSearchedOrder *recentSearchedOrder = (CDRecentSearchedOrder *)[NSEntityDescription insertNewObjectForEntityForName:@"CDRecentSearchedOrder" inManagedObjectContext:context];
    
    return recentSearchedOrder;
}

@end
