//
//  CDRecentSearchedOrder+CoreDataProperties.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 14/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CDRecentSearchedOrder.h"

NS_ASSUME_NONNULL_BEGIN

@interface CDRecentSearchedOrder (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *orderRef;
@property (nullable, nonatomic, retain) NSDate *lastSearchedDate;
@property (nullable, nonatomic, retain) NSString *orderDescription;
@property (nullable, nonatomic, retain) NSDate *placedOnDate;
@property (nullable, nonatomic, retain) NSDate *completionDate;
@property (nullable, nonatomic, retain) NSString *orderStatus;
@property (nullable, nonatomic, retain) CDUser *user;

@end

NS_ASSUME_NONNULL_END
