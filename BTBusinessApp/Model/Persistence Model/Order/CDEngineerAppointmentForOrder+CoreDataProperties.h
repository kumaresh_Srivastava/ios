//
//  CDEngineerAppointmentForOrder+CoreDataProperties.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 06/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CDEngineerAppointmentForOrder.h"

NS_ASSUME_NONNULL_BEGIN

@interface CDEngineerAppointmentForOrder (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *itemRef;
@property (nullable, nonatomic, retain) NSString *localCalenderEventID;

@end

NS_ASSUME_NONNULL_END
