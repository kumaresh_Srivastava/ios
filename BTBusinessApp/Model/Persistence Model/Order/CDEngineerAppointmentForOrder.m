//
//  CDEngineerAppointmentForOrder.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 06/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "CDEngineerAppointmentForOrder.h"

@implementation CDEngineerAppointmentForOrder

// Insert code here to add functionality to your managed object subclass

+ (NSEntityDescription *)entityInManagedObjectContext:(NSManagedObjectContext *)context
{
    return [NSEntityDescription entityForName:@"CDEngineerAppointmentForOrder" inManagedObjectContext:context];
}

+ (CDEngineerAppointmentForOrder *)newEngineerAppointmentForOrderInManagedObjectContext:(NSManagedObjectContext *)context withItemRef:(NSString *)itemRef andCalenderEventID:(NSString *)calenderEventID
{
    if(itemRef == nil || calenderEventID == nil)
        return nil;

    CDEngineerAppointmentForOrder *engineerAppointmentForOrder = (CDEngineerAppointmentForOrder *)[NSEntityDescription insertNewObjectForEntityForName:@"CDEngineerAppointmentForOrder" inManagedObjectContext:context];

    engineerAppointmentForOrder.itemRef = itemRef;
    engineerAppointmentForOrder.localCalenderEventID = calenderEventID;

    return engineerAppointmentForOrder;
}

@end
