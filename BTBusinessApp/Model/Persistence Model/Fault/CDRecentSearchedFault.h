//
//  CDRecentSearchedFault.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 14/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CDUser;

NS_ASSUME_NONNULL_BEGIN

@interface CDRecentSearchedFault : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

+ (CDRecentSearchedFault *)newRecentSearchedFaultInManagedObjectContext:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "CDRecentSearchedFault+CoreDataProperties.h"
