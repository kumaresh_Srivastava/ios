//
//  CDEngineerAppointmentForFault.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 4/4/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "CDEngineerAppointmentForFault.h"

@implementation CDEngineerAppointmentForFault

// Insert code here to add functionality to your managed object subclass

+ (NSEntityDescription *)entityInManagedObjectContext:(NSManagedObjectContext *)context
{
    return [NSEntityDescription entityForName:@"CDEngineerAppointmentForFault" inManagedObjectContext:context];
}

+ (CDEngineerAppointmentForFault *)newEngineerAppointmentForFaultInManagedObjectContext:(NSManagedObjectContext *)context withFaultRef:(NSString *)faultRef andCalenderEventID:(NSString *)calenderEventID
{
    if(faultRef == nil || calenderEventID == nil)
    return nil;
    
    CDEngineerAppointmentForFault *engineerAppointmentForFault = (CDEngineerAppointmentForFault *)[NSEntityDescription insertNewObjectForEntityForName:@"CDEngineerAppointmentForFault" inManagedObjectContext:context];
    
    engineerAppointmentForFault.faultRef = faultRef;
    engineerAppointmentForFault.localCalenderEventID = calenderEventID;
    
    return engineerAppointmentForFault;
}

@end
