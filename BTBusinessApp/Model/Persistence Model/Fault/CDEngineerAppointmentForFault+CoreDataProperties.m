//
//  CDEngineerAppointmentForFault+CoreDataProperties.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 4/4/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "CDEngineerAppointmentForFault+CoreDataProperties.h"

@implementation CDEngineerAppointmentForFault (CoreDataProperties)

@dynamic faultRef;
@dynamic localCalenderEventID;

@end
