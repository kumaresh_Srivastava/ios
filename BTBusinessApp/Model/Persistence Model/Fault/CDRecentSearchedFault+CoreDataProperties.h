//
//  CDRecentSearchedFault+CoreDataProperties.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 14/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CDRecentSearchedFault.h"

NS_ASSUME_NONNULL_BEGIN

@interface CDRecentSearchedFault (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *faultRef;
@property (nullable, nonatomic, retain) NSString *faultDescription;
@property (nullable, nonatomic, retain) NSDate *reportedOnDate;
@property (nullable, nonatomic, retain) NSString *faultStatus;
@property (nullable, nonatomic, retain) NSString *colourWithStatus;
@property (nullable, nonatomic, retain) NSDate *lastSearchedDate;
@property (nullable, nonatomic, retain) CDUser *user;

@end

NS_ASSUME_NONNULL_END
