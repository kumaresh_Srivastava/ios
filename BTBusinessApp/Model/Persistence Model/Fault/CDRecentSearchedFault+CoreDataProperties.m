//
//  CDRecentSearchedFault+CoreDataProperties.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 14/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CDRecentSearchedFault+CoreDataProperties.h"

@implementation CDRecentSearchedFault (CoreDataProperties)

@dynamic faultRef;
@dynamic faultDescription;
@dynamic reportedOnDate;
@dynamic faultStatus;
@dynamic colourWithStatus;
@dynamic lastSearchedDate;
@dynamic user;

@end
