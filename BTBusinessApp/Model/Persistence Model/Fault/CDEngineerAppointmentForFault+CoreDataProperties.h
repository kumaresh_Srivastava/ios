//
//  CDEngineerAppointmentForFault+CoreDataProperties.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 4/4/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "CDEngineerAppointmentForFault.h"

NS_ASSUME_NONNULL_BEGIN

@interface CDEngineerAppointmentForFault (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *faultRef;
@property (nullable, nonatomic, retain) NSString *localCalenderEventID;

@end

NS_ASSUME_NONNULL_END
