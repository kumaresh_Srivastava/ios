//
//  CDEngineerAppointmentForFault.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 4/4/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface CDEngineerAppointmentForFault : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

+ (CDEngineerAppointmentForFault *)newEngineerAppointmentForFaultInManagedObjectContext:(NSManagedObjectContext *)context withFaultRef:(NSString *)faultRef andCalenderEventID:(NSString *)calenderEventID;

@end

NS_ASSUME_NONNULL_END

#import "CDEngineerAppointmentForFault+CoreDataProperties.h"
