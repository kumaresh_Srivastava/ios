//
//  CDRecentSearchedFault.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 14/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "CDRecentSearchedFault.h"

@implementation CDRecentSearchedFault

// Insert code here to add functionality to your managed object subclass

+ (NSEntityDescription *)entityInManagedObjectContext:(NSManagedObjectContext *)context
{
    return [NSEntityDescription entityForName:@"CDRecentSearchedFault" inManagedObjectContext:context];
}

+ (CDRecentSearchedFault *)newRecentSearchedFaultInManagedObjectContext:(NSManagedObjectContext *)context
{
    CDRecentSearchedFault *recentSearchedFault = (CDRecentSearchedFault *)[NSEntityDescription insertNewObjectForEntityForName:@"CDRecentSearchedFault" inManagedObjectContext:context];
    
    return recentSearchedFault;
}

@end
