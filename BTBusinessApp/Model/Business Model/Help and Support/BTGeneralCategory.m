//
//  BTGeneralCategory.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 09/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTGeneralCategory.h"
#import "NSObject+APIResponseCheck.h"

@implementation BTGeneralCategory

- (instancetype)initWithResponseDictionaryFromHelpAndSupportAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        self.categoryName = [[responseDic[@"CategoryName"] validAndNotEmptyStringObject] copy];
        self.startTime = [[responseDic[@"StartTime"] validAndNotEmptyStringObject] copy];
        self.endTime = [[responseDic[@"EndTime"] validAndNotEmptyStringObject] copy];
        self.markedAsHoliday = [responseDic[@"MarkedAsHoliday"]  boolValue];
        self.startDay = [[responseDic[@"StartDay"] validAndNotEmptyStringObject] copy];
        self.endDay = [[responseDic[@"EndDay"] validAndNotEmptyStringObject] copy];
    }
    return self;
}


@end
