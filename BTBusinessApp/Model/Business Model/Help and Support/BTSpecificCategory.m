//
//  BTSpecificCategory.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 09/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTSpecificCategory.h"
#import "NSObject+APIResponseCheck.h"

@implementation BTSpecificCategory

- (instancetype)initWithResponseDictionaryFromHelpAndSupportAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _categoryName = [[responseDic[@"CategoryName"] validAndNotEmptyStringObject] copy];
        _startTime = [[responseDic[@"StartTime"] validAndNotEmptyStringObject] copy];
        _endTime = [[responseDic[@"EndTime"] validAndNotEmptyStringObject] copy];
        _markedAsHoliday = [responseDic[@"MarkedAsHoliday"]  boolValue];

    }
    return self;
}

@end
