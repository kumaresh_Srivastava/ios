//
//  BTSpecificCategory.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 09/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTSpecificCategory : NSObject

@property (strong,nonatomic)  NSString *categoryName;
@property (strong,nonatomic)  NSString *startTime;
@property (strong,nonatomic)  NSString *endTime;
@property (assign,nonatomic)  BOOL markedAsHoliday;


- (instancetype)initWithResponseDictionaryFromHelpAndSupportAPIResponse:(NSDictionary *)responseDic;


@end
