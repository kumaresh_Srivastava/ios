//
//  BTVersionCheckResponse.h
//  BTBusinessApp
//
//  Created by VectoScalar on 3/16/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLWebService.h"


typedef enum VersionCheckResponses{
    NoUpgrade = 1,
    VersionUpdateReminder,
    UpdateMandatory,
    OSDeprecatedReminder,
    OSDeprecatedMandatory,
    SpecificOSNotSupportedMandatory,
    DeviceNotSupported,
    OSNotSupported,
    ErrorInvalidJSON,
    ErrorConnection,
    ErrorNoContext,
    ErrorUnknown,
    JSONLogicError
} VersionCheckResponseCode;

@interface BTVersionCheckResponse : NLWebService
@property(nonatomic, readonly) VersionCheckResponseCode responseCode;
@property(nonatomic, readonly) NSString *errorMessage;
@property(nonatomic, readonly) NSString *appStoreURL;

- (instancetype)initWithResponseDict:(NSDictionary *)responseDict;

@end
