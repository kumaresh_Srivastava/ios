//
//  BTVersionCheckResponse.m
//  BTBusinessApp
//
//  Created by VectoScalar on 3/16/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTVersionCheckResponse.h"
#import <UIKit/UIKit.h>


@interface BTVersionCheckResponse()

@property NSDictionary *versionData;
@property NSDictionary *parsedDictionary;
@property NSString *deviceType;
@property NSString *deviceOS;
@property NSString *appVersion;
@property NSString *appStoreURL;
@property NSString *currentVersion;
@property NSString *updateReminderMax;
@property NSString *updateReminderMin;
@property NSString *updateMandatoryMax;
@property NSString *updateMandatoryMin;
@property NSString *minOSSupported;
@property NSArray  *unsupportedOSs;
@property NSArray  *unsupportedDevices;
@property NSDictionary *errorMessages;
@property BOOL versionMatches;
@property BOOL UpdateReminder;
@property BOOL UpdateMandatory;
@property BOOL OSDeprecated;
@property BOOL SpecificOSNotSupported;
@property BOOL DeviceSupported;
@property NSString *enumString;
@property NSString *errorMessage;

@end

@implementation BTVersionCheckResponse

- (instancetype)initWithResponseDict:(NSDictionary *)responseDict{
    
    self = [super init];
    
    if(self){
        
        self.deviceType = [[UIDevice currentDevice] model];
        self.deviceOS = [[UIDevice currentDevice] systemVersion];
        self.appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
        [self parseDataFile:responseDict];
    }
    
    return self;
}



- (void)parseDataFile:(NSDictionary *)data {
    
    _responseCode = ErrorUnknown;
    
    if (data == nil) {
        
        _responseCode = ErrorInvalidJSON;
        self.errorMessage = @"Invalid JSON File";
        
    } else {
        
        
        NSDictionary *versionDataDict = data[@"Version"];
        
        NSDictionary *iOSVersionDataDict = versionDataDict[@"iOS"];
        
        self.appStoreURL = iOSVersionDataDict[@"AppStoreURL"];
        self.currentVersion = iOSVersionDataDict[@"CurrentVersion"];
        self.updateReminderMax = iOSVersionDataDict[@"UpdateReminder"][@"Max"];
        self.updateReminderMin = iOSVersionDataDict[@"UpdateReminder"][@"Min"];
        self.updateMandatoryMax = iOSVersionDataDict[@"UpdateMandatory"][@"Max"];
        self.updateMandatoryMin = iOSVersionDataDict[@"UpdateMandatory"][@"Min"];
        self.minOSSupported = iOSVersionDataDict[@"MinOSSupported"];
        self.unsupportedOSs = iOSVersionDataDict[@"UnsupportedOSs"];
        self.unsupportedDevices = iOSVersionDataDict[@"UnsupportedDevices"];
        self.errorMessages = versionDataDict[@"ErrorCodes"];
        
        
        //VersionMatches
        if (([[self compareVersion:self.appVersion withVersion:self.currentVersion] isEqualToString:@"MATCH"])) {
            self.versionMatches = YES;
        } else {
            self.versionMatches = NO;
        }
        
        //UpdateReminder
        if ((!([[self compareVersion:self.appVersion withVersion:self.updateReminderMin] isEqualToString:@"LOWER"])) && (!([[self compareVersion:self.appVersion withVersion:self.updateReminderMax] isEqualToString:@"HIGHER"]))) {
            self.UpdateReminder = YES;
        } else {
            self.UpdateReminder = NO;
        }
        
        //UpdateMandataory
        if ((!([[self compareVersion:self.appVersion withVersion:self.updateMandatoryMin] isEqualToString:@"LOWER"])) && (!([[self compareVersion:self.appVersion withVersion:self.updateMandatoryMax] isEqualToString:@"HIGHER"]))) {
            self.UpdateMandatory = YES;
        } else {
            self.UpdateMandatory = NO;
        }
        
        //OSDeprecated
        if ([[self compareVersion:self.deviceOS withVersion:self.minOSSupported] isEqualToString:@"LOWER"]) {
            self.OSDeprecated = YES;
        } else {
            self.OSDeprecated = NO;
        }
        
        //SpecificOSNotSupport
        self.SpecificOSNotSupported = NO;
        for (NSString *os in self.unsupportedOSs) {
            if ([[self compareVersion:self.deviceOS withVersion:os] isEqualToString:@"MATCH"]) {
                //OS Version is not supported
                self.SpecificOSNotSupported = YES;
            }
        }
        
        //DeviceSupported
        self.DeviceSupported = YES;
        for (NSString *device in self.unsupportedDevices) {
            if ([self.deviceType isEqualToString:device]) {
                //OS Version is not supported
                self.DeviceSupported = NO;
            }
        }
        
        if (self.DeviceSupported) {
            if (self.versionMatches) {
                if (self.OSDeprecated) {
                    _responseCode = OSNotSupported;
                } else {
                    if (self.SpecificOSNotSupported) {
                        _responseCode = OSNotSupported;
                    } else {
                        _responseCode = NoUpgrade;
                    }
                }
            } else {
                if ([[self compareVersion:self.appVersion withVersion:self.currentVersion] isEqualToString:@"HIGHER"])
                {
                    if (self.OSDeprecated) {
                        _responseCode = OSNotSupported;
                    } else {
                        if (self.SpecificOSNotSupported) {
                            _responseCode = OSNotSupported;
                        } else {
                            _responseCode = NoUpgrade;
                        }
                    }
                }
                else
                {
                    if (self.UpdateMandatory) {
                        if (self.OSDeprecated) {
                            _responseCode = OSDeprecatedMandatory;
                        } else {
                            if (self.SpecificOSNotSupported) {
                                _responseCode = SpecificOSNotSupportedMandatory;
                            } else {
                                _responseCode = UpdateMandatory;
                            }
                        }
                    } else {
                        if (self.UpdateReminder) {
                            if (self.OSDeprecated) {
                                _responseCode = OSDeprecatedReminder;
                            } else {
                                _responseCode = VersionUpdateReminder;
                            }
                        } else {
                            _responseCode = JSONLogicError;
                        }
                    }
                }
            }
        } else {
            _responseCode = DeviceNotSupported;
        }
        
        self.enumString = [self enumToString:(int) self.responseCode];
        self.errorMessage = self.errorMessages[self.enumString];
    }
}

- (NSString *)getFormattedVersion:(NSString *)version withSize:(NSInteger)size {
    
    NSArray *array = [version componentsSeparatedByString:@"."];
    if (array.count != size) {
        for (NSInteger i = array.count; i < size; i++) {
            version = [version stringByAppendingString:@".0"];
        }
    }
    return version;
}


- (NSString *)compareVersion:(NSString *)version1 withVersion:(NSString *)version2 {
    
    NSInteger currentVersionCompCount = [version1 componentsSeparatedByString:@"."].count;
    NSInteger withVersionCompCount = [version2 componentsSeparatedByString:@"."].count;
    
    NSInteger size = (currentVersionCompCount > withVersionCompCount) ? currentVersionCompCount: withVersionCompCount;
    
    NSString *fixedVersion1 = [self getFormattedVersion:version1 withSize:size];
    NSString *fixedVersion2 = [self getFormattedVersion:version2 withSize:size];
    NSArray *array1 = [fixedVersion1 componentsSeparatedByString:@"."];
    NSArray *array2 = [fixedVersion2 componentsSeparatedByString:@"."];
    
    for (NSInteger i = 0; i < array1.count; i++) {
        if (!([array1[i] integerValue] == [array2[i] integerValue])) {
            if ([array1[i] integerValue] < [array2[i] integerValue]) {
                return @"LOWER";
            } else {
                return @"HIGHER";
            }
        }
    }
    return @"MATCH";
}



- (NSString *) enumToString:(VersionCheckResponseCode )retCode {
    
    NSString *result = nil;
    
    switch(retCode)
    {
        case NoUpgrade:
            result = @"NoUpgrade";
            break;
        case VersionUpdateReminder:
            result = @"VersionUpdateReminder";
            break;
        case UpdateMandatory:
            result = @"UpdateMandatory";
            break;
        case OSDeprecatedReminder:
            result = @"OSDeprecatedReminder";
            break;
        case OSDeprecatedMandatory:
            result = @"OSDeprecatedMandatory";
            break;
        case SpecificOSNotSupportedMandatory:
            result = @"SpecificOSNotSupportedMandatory";
            break;
        case DeviceNotSupported:
            result = @"DeviceNotSupported";
            break;
        case OSNotSupported:
            result = @"OSNotSupported";
            break;
        case ErrorInvalidJSON:
            result = @"ErrorInvalidJSON";
            break;
        case ErrorConnection:
            result = @"ErrorConnection";
            break;
        case ErrorNoContext:
            result = @"ErrorNoContext";
            break;
        case ErrorUnknown:
            result = @"ErrorUnknown";
            break;
        case JSONLogicError:
            result = @"JSONLogicError";
            break;
        default:
            result = @"ErrorUnknown";
            break;
    }
    return result;
}



@end
