//
//  EENetworkStatus.m
//  BTBusinessApp
//
//  Created by Jim Purvis on 15/05/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "EENetworkStatus.h"

@implementation EENetworkStatus

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    _title = [dictionary valueForKey:@"title"];
    _desc = [dictionary valueForKey:@"description"];
    _causeCodes = [dictionary valueForKey:@"causeCode"];
    _statusCode = [dictionary valueForKey:@"statusCode"];
    _locationId = [dictionary valueForKey:@"locationId"];
    return self;
}

@end
