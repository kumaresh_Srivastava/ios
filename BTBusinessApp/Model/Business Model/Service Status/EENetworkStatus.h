//
//  EENetworkStatus.h
//  BTBusinessApp
//
//  Created by Jim Purvis on 15/05/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EENetworkStatus : NSObject

@property (nonatomic) NSString* locationId;
@property (nonatomic) NSString* statusCode;
@property (nonatomic) NSArray* causeCodes;
@property (nonatomic) NSString* title;
@property (nonatomic) NSString* desc;

-(instancetype) initWithDictionary:(NSDictionary*)dictionary;

@end
