//
//  BTServiceStatusDetail.m
//  BTBusinessApp
//
//  Created by VectoScalar on 3/1/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTServiceStatusDetail.h"
#import "AppManager.h"

@implementation BTServiceStatusDetail

- (instancetype)initWithServiceStatusDetailResponse:(NSDictionary *)responseDict{
    
    
    self = [super init];
    
    if(self){
        
        _mSOFlag = [responseDict valueForKey:@"MSOFlag"];
        _product = [responseDict valueForKey:@"Product"];
        _loggedOn = [AppManager NSDateWithUTCFormatFromNSString:[responseDict valueForKey:@"LoggedOn"]];
        _expectedResolution = [AppManager NSDateWithUTCFormatFromNSString:[responseDict valueForKey:@"ExpectedResolution"]];
        _resolvedOn = [AppManager NSDateWithUTCFormatFromNSString:[responseDict valueForKey:@"ResolvedOn"]];
    }
    
    return self;
}

@end
