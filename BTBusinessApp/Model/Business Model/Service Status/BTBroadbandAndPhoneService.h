//
//  BTBroadbandService.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 22/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTBroadbandAndPhoneService : NSObject

@property (nonatomic, readonly) NSString *assetIntegrationId;
@property (nonatomic, readonly) NSString *postcode;
@property (nonatomic, readonly) NSString *productName;
@property (nonatomic, readonly) NSString *productScode;
@property (nonatomic, readonly) NSString *serviceId;
@property (nonatomic, readonly) NSString *bBPromotionProductName;
@property (nonatomic, readonly) NSString *bBPromotionProductCode;
@property (nonatomic, readonly) NSString *productType;

- (instancetype)initBTBroadbandAndPhoneServiceWithResponse:(NSDictionary *)serviceDict;

@end
