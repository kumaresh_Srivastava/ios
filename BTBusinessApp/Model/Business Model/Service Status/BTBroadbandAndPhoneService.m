//
//  BTBroadbandService.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 22/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTBroadbandAndPhoneService.h"

@implementation BTBroadbandAndPhoneService

- (instancetype)initBTBroadbandAndPhoneServiceWithResponse:(NSDictionary *)serviceDict {

    self = [super init];
    if(self) {

        _assetIntegrationId = [serviceDict valueForKey:@"AssetIntegrationId"];
        _postcode = [serviceDict valueForKey:@"Postcode"];
        _productName = [serviceDict valueForKey:@"ProductName"];
        _productScode = [serviceDict valueForKey:@"ProductScode"];
        _serviceId = [serviceDict valueForKey:@"ServiceId"];
        _bBPromotionProductName = [serviceDict valueForKey:@"BBPromotionProductName"];
        _bBPromotionProductCode = [serviceDict valueForKey:@"BBPromotionProductCode"];
        _productType = [serviceDict valueForKey:@"ProductType"];
    }
    return self;
    
}
@end
