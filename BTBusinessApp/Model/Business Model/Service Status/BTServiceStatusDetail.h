//
//  BTServiceStatusDetail.h
//  BTBusinessApp
//
//  Created by VectoScalar on 3/1/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTServiceStatusDetail : NSObject

@property (nonatomic, readonly) NSString *mSOFlag;
@property (nonatomic, readonly) NSString *product;
@property (nonatomic, readonly) NSDate *loggedOn;
@property (nonatomic, readonly) NSDate *expectedResolution;
@property (nonatomic, readonly) NSDate *resolvedOn;

- (instancetype)initWithServiceStatusDetailResponse:(NSDictionary *)responseDict;

@end
