//
//  BTBillingDetails.m
//  BTBusinessApp
//
//  Created by Accolite on 09/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBillingDetails.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"

@implementation BTBillingDetails

- (instancetype)initBillingDetailsWithResponse:(NSDictionary *)billingDetailsDic
{
    self = [super init];
    if(self)
    {
        _billFrequency = [[billingDetailsDic[@"BillFrequency"] validAndNotEmptyStringObject] copy];
        _billType = [[billingDetailsDic[@"BillType"] validAndNotEmptyStringObject] copy];
        if (billingDetailsDic[@"BillingAddress"]) {
            
            _billingAddress = [billingDetailsDic[@"BillingAddress"] copy];
        }
        _paymentMethod = [[billingDetailsDic[@"PaymentMethod"] validAndNotEmptyStringObject] copy];
        NSNumber *billtypeAmendableNumber = billingDetailsDic[@"IsBillTypeAmendable"];
        _isBillTypeAmendable = [billtypeAmendableNumber boolValue];
    }
    return self;
}

@end
