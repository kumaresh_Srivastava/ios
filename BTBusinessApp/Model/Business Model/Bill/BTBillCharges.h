//
//  BTBillCharges.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTBillCharges : NSObject

@property (nonatomic, readonly) double adjustments;
//Newly added
@property (nonatomic, readonly) double discountCharges;
@property (nonatomic, readonly) double oneOffCharges;
@property (nonatomic, readonly) double regularCharges;
@property (nonatomic, readonly) double usageCharges;
//Ends
@property (nonatomic, readonly) double totalNotIncVat;
@property (nonatomic, readonly) double totalVat;
@property (nonatomic, readonly) double totalIncVat;

- (instancetype)initBillChargesWithResponse:(NSDictionary *)billChargesDic;

@end
