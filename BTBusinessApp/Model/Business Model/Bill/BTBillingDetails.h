//
//  BTBillingDetails.h
//  BTBusinessApp
//
//  Created by Accolite on 09/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTBillingDetails : NSObject

@property (nonatomic, readonly) NSString *billFrequency;
@property (nonatomic, readonly) NSString *billType;
@property (nonatomic, readonly) NSDictionary *billingAddress;
@property (nonatomic, readonly) NSString *paymentMethod;
@property (nonatomic, readonly) BOOL isBillTypeAmendable;
- (instancetype)initBillingDetailsWithResponse:(NSDictionary *)billingDetailsDic;
@end