//
//  BTBill.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBill.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"

@implementation BTBill

- (instancetype)initBillWithBillPaymentHistoryResponse:(NSDictionary *)billDic
{
    self = [super init];
    if(self)
    {
        _billingAccountNo = [[billDic[@"BillingAccountNo"] validAndNotEmptyStringObject] copy];
        
        if([billDic valueForKey:@"BillDate"])
        {
            _billDate = [AppManager NSDateWithUTCFormatFromNSString:[[billDic[@"BillDate"] validAndNotEmptyStringObject] copy]];
        }
        else
        {
            if([billDic valueForKey:@"ReceivedDate"])
            {
                _billDate = [AppManager NSDateWithUTCFormatFromNSString:[[billDic[@"ReceivedDate"] validAndNotEmptyStringObject] copy]];
            }
            else
            {
                _billDate = nil;
            }
        }
        
        _billStatus = [[billDic[@"Status"] validAndNotEmptyStringObject] copy];
        _billStatusDescription = [[billDic[@"StatusDesc"] validAndNotEmptyStringObject] copy];
        _totalCharges = [billDic[@"TotalCharges"] doubleValue];
        _billVersion =  billDic[@"BillVersion"];
        _billingAccountSystem =  [[billDic[@"BillingAccountSystem"] validAndNotEmptyStringObject] copy];
        _billRef = [[billDic[@"BillRef"] validAndNotEmptyStringObject] copy];

        
    }
    return self;
}

- (instancetype)initBillWithBillSummaryAndChargesResponse:(NSDictionary *)billDic
{
    self = [super init];
    if(self)
    {
        _accountName = [[billDic[@"AccountName"] validAndNotEmptyStringObject] copy];
        _billDate = [AppManager NSDateWithUTCFormatFromNSString:[[billDic[@"BillDate"] validAndNotEmptyStringObject] copy]];
        _paymentDueDate = [AppManager NSDateWithUTCFormatFromNSString:[[billDic[@"PaymentDueDate"] validAndNotEmptyStringObject] copy]];
        _nextBillDate = [AppManager NSDateWithUTCFormatFromNSString:[[billDic[@"NextBillDate"] validAndNotEmptyStringObject] copy]];
        _billStatus = [[billDic[@"Status"] validAndNotEmptyStringObject] copy];
        _billStatusDescription = [[billDic[@"StatusDesc"] validAndNotEmptyStringObject] copy];
        _billRef = [[billDic[@"BillRef"] validAndNotEmptyStringObject] copy];
        _billType = [[billDic[@"BillType"] validAndNotEmptyStringObject] copy];
        _billingNameAndAddress = [[billDic[@"BillingNameAndAddress"] validAndNotEmptyStringObject] copy];
        
        //Newly added models
        
         _status = [[billDic[@"Status"] validAndNotEmptyStringObject] copy];
        
        _statusDesc = [[billDic[@"StatusDesc"] validAndNotEmptyStringObject] copy];
        
        _isPaid = [[billDic valueForKey:@"IsPaid"] boolValue];
        
        _billingAccountSystem = [[billDic[@"BillingAccountSystem"] validAndNotEmptyStringObject] copy];
        
        _billVersionNumber = [[billDic [@"BillVersionNumber"] validAndNotEmptyStringObject] integerValue];
        
        
    }
    return self;
}


@end
