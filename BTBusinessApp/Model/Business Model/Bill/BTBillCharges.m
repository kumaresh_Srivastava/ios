//
//  BTBillCharges.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBillCharges.h"

@implementation BTBillCharges

- (instancetype)initBillChargesWithResponse:(NSDictionary *)billChargesDic
{
    self = [super init];
    if(self)
    {
        _adjustments = [billChargesDic[@"Adjustments"] doubleValue];
        _totalNotIncVat = [billChargesDic[@"TotalNotIncVat"] doubleValue];
        _totalVat = [billChargesDic[@"TotalVat"] doubleValue];
        _totalIncVat = [billChargesDic[@"TotalIncVat"] doubleValue];
        
        _discountCharges = [billChargesDic[@"DiscountCharges"] doubleValue];
        _oneOffCharges = [billChargesDic[@"OneOffCharges"] doubleValue];
        _regularCharges = [billChargesDic[@"RegularCharges"] doubleValue];
        _usageCharges = [billChargesDic[@"UsageCharges"] doubleValue];
        
    }
    return self;
}

@end
