//
//  BTBill.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTBill : NSObject

@property (nonatomic, readonly) NSString *billingAccountNo;
@property (nonatomic, readonly) NSDate *billDate;
@property (nonatomic, readonly) NSString *billStatus;
@property (nonatomic, readonly) NSString *billStatusDescription;
@property (nonatomic, readonly) double totalCharges;
@property (nonatomic, readonly) NSString *accountName;
@property (nonatomic, readonly) NSDate *paymentDueDate;
@property (nonatomic, readonly) NSDate *nextBillDate;
@property (nonatomic, readonly) NSString *billRef;
@property (nonatomic, readonly) NSString *billType;
@property (nonatomic, readonly) NSString *billingNameAndAddress;
@property (nonatomic,readonly) NSString *billVersion;
@property (nonatomic ,readonly) NSString *billingAccountSystem;
//New added models
@property (nonatomic,readonly) NSString *status;
@property (nonatomic,readonly) NSString *statusDesc;
@property (nonatomic,readonly) BOOL isPaid;
@property (nonatomic,readonly) NSInteger billVersionNumber;

- (instancetype)initBillWithBillPaymentHistoryResponse:(NSDictionary *)billDic;
- (instancetype)initBillWithBillSummaryAndChargesResponse:(NSDictionary *)billDic;

@end
