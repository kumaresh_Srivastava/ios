//
//  BTSecurityCheck.m
//  BTBusinessApp
//
//  Created by vectoscalar on 09/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTSecurityCheck.h"
#import "NSObject+APIResponseCheck.h"

@implementation BTSecurityCheck

- (instancetype)initWithResponseDictionaryFromSecurityCheckAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _status = [responseDic[@"Status"]  intValue];
        _message = [[responseDic[@"Message"] validAndNotEmptyStringObject] copy];
        
    }
    return self;
}

@end
