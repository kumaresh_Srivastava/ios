//
//  BTSecurityCheck.h
//  BTBusinessApp
//
//  Created by vectoscalar on 09/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTSecurityCheck : NSObject

@property (strong,nonatomic)  NSString *message;
@property (assign,nonatomic)  int status;


- (instancetype)initWithResponseDictionaryFromSecurityCheckAPIResponse:(NSDictionary *)responseDic;


@end
