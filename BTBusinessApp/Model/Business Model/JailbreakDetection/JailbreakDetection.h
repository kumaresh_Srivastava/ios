//
//  JailbreakDetection.h
//  BTBusinessApp
//
//  Created by Jim Purvis on 27/07/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JailbreakDetection : NSObject
BOOL isJailbroken();
BOOL isCracked();
BOOL isAppStore();
@end
