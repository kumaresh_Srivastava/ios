//
//  BTNotification.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 27/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTNotification.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"

@implementation BTNotification

- (instancetype)initWithNotificationData:(NSDictionary *)notificationDic
{
    self = [super init];
    if (self)
    {
        _notificationId = [[notificationDic valueForKey:@"NotificationId"] integerValue];
        _notificationCategory = [[[notificationDic valueForKey:@"NotificationCategory"] validAndNotEmptyStringObject] copy];
        _notificationSubject = [[[notificationDic valueForKey:@"NotificationSubject"] validAndNotEmptyStringObject] copy];
        _notificationBody = [[[notificationDic valueForKey:@"NotificationBody"] validAndNotEmptyStringObject] copy];
        _notificationDate = [AppManager NSDateWithUTCFormatFromNSString: [[[notificationDic valueForKey:@"NotificationDate"] validAndNotEmptyStringObject] copy]];
        _notificationMedium = [[notificationDic valueForKey:@"NotificationMedium"] integerValue];
        _displayNotificationMedium = [[[notificationDic valueForKey:@"DisplayNotificationMedium"] validAndNotEmptyStringObject] copy];
        _notificationStatus = [[notificationDic valueForKey:@"NotificationStatus"] integerValue];
        _isDeletable = [[notificationDic valueForKey:@"IsDeletable"] boolValue];
        _notificationLetterUri = [[[notificationDic valueForKey:@"NotificationLetterUri"] validAndNotEmptyStringObject] copy];
        _notificationKciMessageId = [[[notificationDic valueForKey:@"NotificationKciMessageId"] validAndNotEmptyStringObject] copy];
    }
    
    return self;
}

- (void)updateNotificationStatus
{
    _notificationStatus = BTNotificationTypeRead;
}

@end
