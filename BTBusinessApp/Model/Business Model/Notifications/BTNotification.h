//
//  BTNotification.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 27/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, BTNotificationType) {
    
    BTNotificationTypeUnRead,
    BTNotificationTypeRead
};

typedef NS_ENUM(NSInteger, BTNotificationMedium) {
    
    BTNotificationMediumEmail,
    BTNotificationMediumSMS,
    BTNotificationMediumLetter,
    BTNotificationMediumMon
};

@interface BTNotification : NSObject

@property (nonatomic, readonly) NSInteger notificationId;
@property (nonatomic, readonly) NSString *notificationCategory;
@property (nonatomic, readonly) NSString *notificationSubject;
@property (nonatomic, readonly) NSString *notificationBody;
@property (nonatomic, readonly) NSDate *notificationDate;
@property (nonatomic, readonly) NSInteger notificationMedium;
@property (nonatomic, readonly) NSString *displayNotificationMedium;
@property (nonatomic, readonly) NSInteger notificationStatus;
@property (nonatomic, readonly) BOOL isDeletable;
@property (nonatomic, readonly) NSString *notificationLetterUri;
@property (nonatomic, readonly) NSString *notificationKciMessageId;

- (instancetype)initWithNotificationData:(NSDictionary *)notificationDic;

- (void)updateNotificationStatus;

@end
