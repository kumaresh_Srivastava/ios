//
//  AssetCombinedRental.h
//  BTBusinessApp
//
//  Created by vectoscalar on 09/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTAssetCombinedRental : NSObject

@property (nonatomic,readonly) NSString *assetItemHeaderDesc;
@property (nonatomic,readonly) NSString *assetItemCharge;
@property (nonatomic,readonly) NSString *assetItemPeriod;

- (instancetype)initWithResponseDictionaryFromAssetsBACAPIResponse:(NSDictionary *)responseDic;

@end
