//
//  BTAsset.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 08/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTAsset.h"
#import "NSObject+APIResponseCheck.h"
#import "BTAssetModel.h"
#import "BTMobileServiceAssetModel.h"
#import "BTMobileSIMServiceAssetsModel.h"

@implementation BTAsset
{
    NSMutableArray *_assetModels;
}

- (instancetype)initWithResponseDictionaryFromAssetsDashboardAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _assetName = [[responseDic[@"Name"] validAndNotEmptyStringObject] copy];
        _assetAccountNumber = [[responseDic[@"AccountNumber"] validAndNotEmptyStringObject] copy];

    }
    return self;

}

- (instancetype)initWithResponseDictionaryFromUsageDashboardAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _assetName = [[responseDic[@"Name"] validAndNotEmptyStringObject] copy];
        _assetAccountNumber = [[responseDic[@"AccountNumber"] validAndNotEmptyStringObject] copy];
        _billingAccountNumber = [[responseDic[@"AccountNumber"] validAndNotEmptyStringObject] copy];

    }
    return self;
    
}


- (instancetype)initWithResponseDictionaryFromAssetsBACAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _billingAccountNumber = [[responseDic[@"BillingAccountNumber"] validAndNotEmptyStringObject] copy];
        _lastBillAmount = [[responseDic[@"LastBillAmount"] validAndNotEmptyStringObject] copy];
        _assetAccountNumber = [[responseDic[@"BillingAccountNumber"] validAndNotEmptyStringObject] copy];
        //_assetModels = [[responseDic[@"AssetModels"] validAndNotEmptyStringObject] copy];
        
        NSArray *assetModelArray = [responseDic valueForKey:@"AssetModels"];
        if([assetModelArray isKindOfClass:[NSArray class]])
        {
            for(NSDictionary *dic in assetModelArray)
            {
                if(![dic isKindOfClass:[NSDictionary class]])
                    continue;
        
                BTAssetModel *assetModel = [[BTAssetModel alloc] initWithResponseDictionaryFromAssetsBACAPIResponse:dic];
                
                assetModel.billingAccountNumber = _billingAccountNumber;
                if(assetModel)
                {
                    [self addAssetModel:assetModel];
                }
            }
        }
        else
        {
            DDLogError(@"'Products' element missing in order summary response ");
        }
        
    }
    return self;
    
}


- (instancetype)initWithResponseFromMobileServiceApiResponse:(NSArray *)responseArray
{
    self = [super init];
    if(self)
    {
        for(NSDictionary* dic in responseArray){
            BTMobileServiceAssetModel* mobileServiceAssetModel = [[BTMobileServiceAssetModel alloc] init];
            mobileServiceAssetModel.contractExpiry = [[dic[@"contractExpiry"] validAndNotEmptyStringObject] copy];
            mobileServiceAssetModel.productName =  [[dic[@"productName"] validAndNotEmptyStringObject] copy];
            mobileServiceAssetModel.productDisplayName =  [[dic[@"productDisplayName"] validAndNotEmptyStringObject] copy];
            mobileServiceAssetModel.type =  [[dic[@"type"] validAndNotEmptyStringObject] copy];
            [self addMobileServiceAssetModel:mobileServiceAssetModel];
        }
        
        if(responseArray.count == 0){

            DDLogError(@"'Products summary' element missing in mobile service response ");
        }
        
    }
    return self;
    
}


- (instancetype)initWithResponseFromMobileSIMServiceApiResponse:(NSArray *)responseArray
{
    self = [super init];
    if(self)
    {
        for(NSDictionary* dic in responseArray){
            BTMobileSIMServiceAssetsModel* mobileSIMServiceAssetModel = [[BTMobileSIMServiceAssetsModel alloc] init];
            mobileSIMServiceAssetModel.PUK1code = [[dic[@"PUK1code"] validAndNotEmptyStringObject] copy];
            mobileSIMServiceAssetModel.simSerialNumber =  [[dic[@"simSerialNumber"] validAndNotEmptyStringObject] copy];
            mobileSIMServiceAssetModel.status =  [[dic[@"status"] validAndNotEmptyStringObject] copy];
          
            [self addMobileSIMServiceAssetModel:mobileSIMServiceAssetModel];
        }
        
        if(responseArray.count == 0){
            
            DDLogError(@"'Products summary' element missing in mobile service response ");
        }
        
    }
    return self;
    
}


#pragma mark - Setter & Getter Methods

- (NSArray *)assetModels
{
    return [_assetModels copy];
}

#pragma mark - Public Methods

- (void)addAssetModel:(BTAssetModel *)assetModel
{
    if(_assetModels == nil)
    {
        _assetModels = [[NSMutableArray alloc] init];
    }
    
    if(assetModel == nil)
        return;
    
    [_assetModels addObject:assetModel];
}

- (void)addMobileServiceAssetModel:(BTMobileServiceAssetModel *)mobileServiceModel
{
    if(_assetModels == nil)
    {
        _assetModels = [[NSMutableArray alloc] init];
    }
    
    if(mobileServiceModel == nil)
        return;
    
    [_assetModels addObject:mobileServiceModel];
}

- (void)addMobileSIMServiceAssetModel:(BTMobileSIMServiceAssetsModel *)mobileSIMServiceModel
{
    if(_assetModels == nil)
    {
        _assetModels = [[NSMutableArray alloc] init];
    }
    
    if(mobileSIMServiceModel == nil)
        return;
    
    [_assetModels addObject:mobileSIMServiceModel];
}

@end
