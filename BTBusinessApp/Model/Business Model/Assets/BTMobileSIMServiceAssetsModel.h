//
//  BTMobileSIMServiceAssetsModel.h
//  BTBusinessApp
//
//  Created by kumaresh shrivastava on 26/06/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BTMobileSIMServiceAssetsModel : NSObject

@property (nonatomic,strong) NSString *PUK1code;
@property (nonatomic,strong) NSString *simSerialNumber;
@property (nonatomic,strong) NSString *status;

@end

NS_ASSUME_NONNULL_END
/*
 
 {
 PUK1code = 12345876;
 simSerialNumber = 110000019694;
 status = Active;
 }
 
 */
