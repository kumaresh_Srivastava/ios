//
//  BTResilientHubAsset.m
//  BTBusinessApp
//
//  Created by Jim Purvis on 10/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTResilientHubAsset.h"

@implementation BTResilientHubAsset

- (instancetype)initWithResponseDictionary:(NSDictionary *)responseDic {
    // Fixed for on #Crash Live build 2.0.2 reported :- SIGABRT: -[__NSCFNumber isEqualToString:]: unrecognized selector sent to instance 0xf15e81cfbb2ca532
    if([[responseDic objectForKey:@"hubSerialNum"] isKindOfClass:[NSNumber class]]){
        NSNumber* hubId = [responseDic objectForKey:@"hubSerialNum"];
        _hubSerialNumber = [hubId stringValue];
    } else{
        _hubSerialNumber = [responseDic objectForKey:@"hubSerialNum"];
    }
    _serviceID = [responseDic objectForKey:@"serviceId"];
    _attributes = [responseDic objectForKey:@"listOfAttribute"];
    _hashCode = [responseDic objectForKey:@"hashCode"];
    return self;
}

@end
