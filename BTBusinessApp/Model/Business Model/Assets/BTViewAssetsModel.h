//
//  BTViewAssetsModel.h
//  BTBusinessApp
//
//  Created by Accolite on 28/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTViewAssetsModel : NSObject

@property (nonatomic,strong) NSArray *viewAssetsArray;
@property (nonatomic,strong) NSDictionary *phoneServicesDictionary;
@property (nonatomic,strong) NSDictionary *broadBandServicesDictionary;
@property (nonatomic,strong) NSDictionary *othersDictionary;

@property (nonatomic,strong) NSArray *phoneServicesArray;
@property (nonatomic,strong) NSArray *broadBandServicesArray;
@property (nonatomic,strong) NSArray *othersArray;

@property (nonatomic,strong) NSString *BillingAccountNumber;
@property (nonatomic,strong) NSString *LastBillAmount;

@end


/*

 [
    {
     "NoOfAssets": "5",
     "Name": "Phone Services",
     "LastBillAmount": "233.68",
     "priority": 1,
     "accountSystem": "ANT",
     "AssetDetailCollection": [
     {
 "ServiceNumber": "01009637322",
 "ServiceText": "PSTN Aux Group",
 "installedAt": "AB10 7AR",
 "combinedRent": "£24.66",
 "contractTerm": "12",
 "contractEnd": "10/06/16",
 "assetID": "BTCWG0161-1",
 "assetNumber": "01009637322",
 "assetIntegrationID": "BTCWG0161-1",
 "CombinedRentalDesc": [
 {
 "AssetItemHeaderDesc": "Phone services - Regular charges- This is the charge in advance for your Prompt Care at £0.00 per month ",
 "AssetItemCharge": "£0.00",
 "AssetItemPeriod": "Rental for 01/03/16 - 31/03/16"
 },
 {
 "AssetItemHeaderDesc": "Phone services - Regular charges- This is the charge in advance for your Business Auxiliary line service at £12.33 per month",
 "AssetItemCharge": "£12.33",
 "AssetItemPeriod": "Rental for 01/03/16 - 31/03/16"
 },
 {
 "AssetItemHeaderDesc": "Phone services - Regular charges- This is the charge in advance for your Business Auxiliary line service at £12.33 per month",
 "AssetItemCharge": "£12.33",
 "AssetItemPeriod": "Rental for 01/03/16 - 31/03/16"
 }
 ]
 },
 {
 "ServiceNumber": "01004183555",
 "ServiceText": "Business PSTN Service",
 "installedAt": "HS1 2QJ",
 "combinedRent": "£0.00",
 "contractTerm": "24",
 "contractEnd": "27/09/17",
 "assetID": "BTCZF7031-10",
 "assetNumber": "01004183555",
 "assetIntegrationID": "BTCZF7031-10",
 "CombinedRentalDesc": [
 {
 "AssetItemHeaderDesc": "Phone services - Regular charges- The charge for your Business Line Service is included in your Bundle - BNDLMA03538",
 "AssetItemCharge": "£0.00",
 "AssetItemPeriod": "Rental for 01/03/16 - 31/03/16"
 },
 {
 "AssetItemHeaderDesc": "Phone services - Regular charges- This is the charge in advance for your Prompt Care at £0.00 per month ",
 "AssetItemCharge": "£0.00",
 "AssetItemPeriod": "Rental for 01/03/16 - 31/03/16"
 }
 ]
 },
 {
 "ServiceNumber": "01004540990",
 "ServiceText": "Business PSTN Service Critical",
 "installedAt": "HS1 2QJ",
 "combinedRent": "£24.25",
 "contractTerm": "24",
 "contractEnd": "23/01/17",
 "assetID": "BTCSP7481-1",
 "assetNumber": "01004540990",
 "assetIntegrationID": "BTCSP7481-1",
 "CombinedRentalDesc": [
 {
 "AssetItemHeaderDesc": "Phone services - Regular charges- This is the charge in advance for your Critical Care at £0.00 per month ",
 "AssetItemCharge": "£0.00",
 "AssetItemPeriod": "Rental for 01/03/16 - 31/03/16"
 },
 {
 "AssetItemHeaderDesc": "Phone services - Regular charges- This is the charge in advance for your BT Business Critical Phone Line at £24.25 per month",
 "AssetItemCharge": "£24.25",
 "AssetItemPeriod": "Rental for 01/03/16 - 31/03/16"
 }
 ]
 },
 {
 "ServiceNumber": "01003067658",
 "ServiceText": "Business PSTN Service",
 "installedAt": "AB15 7XY",
 "combinedRent": "£19.99",
 "contractTerm": "24",
 "contractEnd": "28/01/17",
 "assetID": "BTTS19091-10",
 "assetNumber": "01003067658",
 "assetIntegrationID": "BTTS19091-10",
 "CombinedRentalDesc": [
 {
 "AssetItemHeaderDesc": "Phone services - Regular charges- This is the charge in advance for your Prompt Care at £0.00 per month ",
 "AssetItemCharge": "£0.00",
 "AssetItemPeriod": "Rental for 01/03/16 - 31/03/16"
 },
 {
 "AssetItemHeaderDesc": "Phone services - Regular charges- This is the charge in advance for your Business Line Service at £19.99 per month",
 "AssetItemCharge": "£19.99",
 "AssetItemPeriod": "Rental for 01/03/16 - 31/03/16"
 }
 ]
 },
 {
 "ServiceNumber": "01000172378",
 "ServiceText": "Business PSTN Service",
 "installedAt": "HS1 2QJ",
 "combinedRent": "£0.00",
 "contractTerm": "24",
 "contractEnd": "27/09/17",
 "assetID": "BTCZF6241-10",
 "assetNumber": "01000172378",
 "assetIntegrationID": "BTCZF6241-10",
 "CombinedRentalDesc": [
 {
 "AssetItemHeaderDesc": "Phone services - Regular charges- This is the charge in advance for your Prompt Care at £0.00 per month ",
 "AssetItemCharge": "£0.00",
 "AssetItemPeriod": "Rental for 01/03/16 - 31/03/16"
 },
 {
 "AssetItemHeaderDesc": "Phone services - Regular charges- The charge for your Business Line Service is included in your Bundle - BNDLMA03532",
 "AssetItemCharge": "£0.00",
 "AssetItemPeriod": "Rental for 01/03/16 - 31/03/16"
 }
 ]
 }
 ]
 },
 {
 "NoOfAssets": "2",
 "Name": "Broadband & Internet",
 "LastBillAmount": "233.68",
 "priority": 2,
 "accountSystem": "ANT",
 "AssetDetailCollection": [
 {
 "ServiceNumber": "01004183555",
 "ServiceText": "Business Broadband",
 "installedAt": "HS1 2QJ",
 "combinedRent": "£0.00",
 "contractTerm": "24",
 "contractEnd": "04/10/17",
 "assetID": "BTCZF7031-2",
 "assetNumber": "01004183555",
 "assetIntegrationID": "BTCZF7031-2",
 "CombinedRentalDesc": [
 {
 "AssetItemHeaderDesc": "Broadband Service Rental- The charge for your Static IP is included in your Bundle - BNDLMA03538 ",
 "AssetItemCharge": "£0.00",
 "AssetItemPeriod": "Rental for 01/03/16 - 31/03/16"
 },
 {
 "AssetItemHeaderDesc": "Broadband Service Rental- The charge for your Broadband Service is included in your Bundle - %bundle id%",
 "AssetItemCharge": "£0.00",
 "AssetItemPeriod": "Rental for 01/03/16 - 31/03/16"
 }
 ]
 },
 {
 "ServiceNumber": "01000172378",
 "ServiceText": "Business Broadband",
 "installedAt": "HS1 2QJ",
 "combinedRent": "£0.00",
 "contractTerm": "24",
 "contractEnd": "04/10/17",
 "assetID": "BTCZF6241-2",
 "assetNumber": "01000172378",
 "assetIntegrationID": "BTCZF6241-2",
 "CombinedRentalDesc": [
 {
 "AssetItemHeaderDesc": "Broadband Service Rental- The charge for your Static IP is included in your Bundle - BNDLMA03532 ",
 "AssetItemCharge": "£0.00",
 "AssetItemPeriod": "Rental for 01/03/16 - 31/03/16"
 },
 {
 "AssetItemHeaderDesc": "Broadband Service Rental- The charge for your Broadband Service is included in your Bundle - %bundle id%",
 "AssetItemCharge": "£0.00",
 "AssetItemPeriod": "Rental for 01/03/16 - 31/03/16"
 }
 ]
 }
 ]
 },
 {
 "NoOfAssets": "4",
 "Name": "Others",
 "LastBillAmount": "233.68",
 "priority": 4,
 "accountSystem": "ANT",
 "AssetDetailCollection": [
 {
 "ServiceNumber": "",
 "ServiceText": "Broadband Standard Care",
 "installedAt": "",
 "combinedRent": "£0.00",
 "contractTerm": "",
 "contractEnd": "",
 "assetID": "BTCZF6241-9",
 "assetNumber": "GP00086210",
 "assetIntegrationID": "BTCZF6241-9",
 "CombinedRentalDesc": [
 {
 "AssetItemHeaderDesc": "Account - Regular charges- This is the charge in advance for your BT Broadband Standard Care at £0.00 per month",
 "AssetItemCharge": "£0.00",
 "AssetItemPeriod": "Rental for 01/03/16 - 31/03/16"
 }
 ]
 },
 {
 "ServiceNumber": "",
 "ServiceText": "BT Quantum Basic 8+16 (A) Maintenance",
 "installedAt": "",
 "combinedRent": "£19.83",
 "contractTerm": "60",
 "contractEnd": "10/05/20",
 "assetID": "BTCVR3501-2",
 "assetNumber": "GP00086210",
 "assetIntegrationID": "BTCVR3501-2",
 "CombinedRentalDesc": [
 {
 "AssetItemHeaderDesc": "Account - Regular charges- This is the charge in advance for your BT Quantum Basic 8+16 (A) 60 Months term with Prompt Care at £19.83 per month",
 "AssetItemCharge": "£19.83",
 "AssetItemPeriod": "Rental for 01/03/16 - 31/03/16"
 }
 ]
 },
 {
 "ServiceNumber": "BNDLMA03532",
 "ServiceText": "Business BB bundle Q4 test",
 "installedAt": "",
 "combinedRent": "£50.00",
 "contractTerm": "24",
 "contractEnd": "04/10/17",
 "assetID": "BTCZF6241-16",
 "assetNumber": "BNDLMA03532",
 "assetIntegrationID": "BTCZF6241-16",
 "CombinedRentalDesc": [
 {
 "AssetItemHeaderDesc": "Bundle Service Regular Charge- This is the charge in advance for your Business BB bundle Q4 test at £50.00 per month",
 "AssetItemCharge": "£50.00",
 "AssetItemPeriod": "Rental for 01/03/16 - 31/03/16"
 }
 ]
 },
 {
 "ServiceNumber": "BNDLMA03538",
 "ServiceText": "Business BB bundle Q4 test",
 "installedAt": "",
 "combinedRent": "£50.00",
 "contractTerm": "24",
 "contractEnd": "04/10/17",
 "assetID": "BTCZF7031-16",
 "assetNumber": "BNDLMA03538",
 "assetIntegrationID": "BTCZF7031-16",
 "CombinedRentalDesc": [
 {
 "AssetItemHeaderDesc": "Bundle Service Regular Charge- This is the charge in advance for your Business BB bundle Q4 test at £50.00 per month",
 "AssetItemCharge": "£50.00",
 "AssetItemPeriod": "Rental for 01/03/16 - 31/03/16"
 }
 ]
 }
 ]
 }
 ]
*/
