//
//  BTViewAssetsModel.m
//  BTBusinessApp
//
//  Created by Accolite on 28/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTViewAssetsModel.h"

@implementation BTViewAssetsModel

- (id) init {
    self = [super init];
    if (self) {
        self.viewAssetsArray = [NSArray array];
        self.phoneServicesDictionary = [NSDictionary dictionary];
        self.broadBandServicesDictionary = [NSDictionary dictionary];
        self.othersDictionary = [NSDictionary dictionary];
        
        self.phoneServicesArray = [NSArray array];
        self.broadBandServicesArray = [NSArray array];
        self.othersArray = [NSArray array];
    }
    return self;
}

@end
