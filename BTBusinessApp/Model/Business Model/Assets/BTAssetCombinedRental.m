//
//  AssetCombinedRental.m
//  BTBusinessApp
//
//  Created by vectoscalar on 09/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTAssetCombinedRental.h"
#import "NSObject+APIResponseCheck.h"

@implementation BTAssetCombinedRental

- (instancetype)initWithResponseDictionaryFromAssetsBACAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _assetItemCharge = [[responseDic[@"AssetItemCharge"] validAndNotEmptyStringObject] copy];
        _assetItemPeriod = [[responseDic[@"AssetItemPeriod"] validAndNotEmptyStringObject] copy];
        _assetItemHeaderDesc = [[responseDic[@"AssetItemHeaderDesc"] validAndNotEmptyStringObject] copy];
        
    }
    return self;
    
}

@end
