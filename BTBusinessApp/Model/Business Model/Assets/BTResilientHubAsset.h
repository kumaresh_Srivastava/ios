//
//  BTResilientHubAsset.h
//  BTBusinessApp
//
//  Created by Jim Purvis on 10/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTResilientHubAsset : NSObject

@property NSString *hubSerialNumber;
@property NSString *serviceID;
@property NSDictionary *attributes;
@property NSString *hashCode;

- (instancetype)initWithResponseDictionary:(NSDictionary *)responseDic;

@end
