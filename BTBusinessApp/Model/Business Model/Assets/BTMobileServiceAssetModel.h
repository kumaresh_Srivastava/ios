//
//  BTMobileServiceAssetModel.h
//  BTBusinessApp
//
//  Created by kumaresh shrivastava on 26/06/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BTMobileServiceAssetModel : NSObject

@property (nonatomic,strong) NSString *contractExpiry;
@property (nonatomic,strong) NSString *productDisplayName;
@property (nonatomic,strong) NSString *productName;
@property (nonatomic,strong) NSString *type;

@end

NS_ASSUME_NONNULL_END
/*
 {
 contractExpiry = "2019-11-28T00:00:00Z";
 productDisplayName = "BT Business Mobile SIM Only 1GB";
 productName = "SIM Only 1GB";
 type = "Mobile-Package";
 },
 {
 productDisplayName = "Unlimited Wi-Fi";
 productName = "Unlimited Wi-Fi";
 type = "BT-Wi-Fi";
 }
 
 */
