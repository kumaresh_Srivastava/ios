//
//  BTAsset.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 08/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTAsset : NSObject

//First Level Data
@property (nonatomic) NSString *assetName;
@property (nonatomic) NSString *assetAccountNumber;


//Second Level Data
@property (nonatomic,readonly) NSString *billingAccountNumber;
@property (nonatomic,readonly) NSString *lastBillAmount;
@property (nonatomic,readonly) NSArray *assetModels;

- (instancetype)initWithResponseDictionaryFromAssetsDashboardAPIResponse:(NSDictionary *)responseDic;

- (instancetype)initWithResponseDictionaryFromAssetsBACAPIResponse:(NSDictionary *)responseDic;

- (instancetype)initWithResponseDictionaryFromUsageDashboardAPIResponse:(NSDictionary *)responseDic;

- (instancetype)initWithResponseFromMobileServiceApiResponse:(NSArray *)responseArray;

- (instancetype)initWithResponseFromMobileSIMServiceApiResponse:(NSArray *)responseArray;

@end
