//
//  AssetDetailCollection.h
//  BTBusinessApp
//
//  Created by vectoscalar on 09/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTAssetDetailCollection : NSObject

@property (nonatomic,readonly) NSString *serviceNumber;
@property (nonatomic,readonly) NSString *serviceText;
@property (nonatomic,readonly) NSString *installedAt;
@property (nonatomic,readonly) NSString *combinedRent;
@property (nonatomic,readonly) NSString *contractTerm;
@property (nonatomic,readonly) NSString *contractEnd;
@property (nonatomic,readonly) NSString *assetID;
@property (nonatomic,readonly) NSString *assetNumber;
@property (nonatomic,readonly) NSString *assetIntegrationID;
@property (nonatomic,readonly) NSArray *combinedRentalDesc;


- (instancetype)initWithResponseDictionaryFromAssetsBACAPIResponse:(NSDictionary *)responseDic;

@end
