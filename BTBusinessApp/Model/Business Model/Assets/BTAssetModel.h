//
//  BTAssetModel.h
//  BTBusinessApp
//
//  Created by vectoscalar on 09/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTAssetModel : NSObject

@property (nonatomic,readonly) NSInteger noOfAssets;
@property (nonatomic,readonly) NSString *name;
@property (nonatomic,readonly) double lastBillAmount;
@property (nonatomic,readonly) NSInteger priority;
@property (nonatomic,readonly) NSString *accountSystem;
@property (nonatomic,readonly) NSArray *assetDetailCollection;
@property (nonatomic) NSString *billingAccountNumber;
- (instancetype)initWithResponseDictionaryFromAssetsBACAPIResponse:(NSDictionary *)responseDic;

@end
