//
//  BTAssetModel.m
//  BTBusinessApp
//
//  Created by vectoscalar on 09/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTAssetModel.h"
#import "NSObject+APIResponseCheck.h"
#import "BTAssetDetailCollection.h"

@implementation BTAssetModel
{
    NSMutableArray *_assetDetailCollection;
}


- (instancetype)initWithResponseDictionaryFromAssetsBACAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _name = [[responseDic[@"Name"] validAndNotEmptyStringObject] copy];
        _noOfAssets =[[responseDic valueForKey:@"NoOfAssets"] integerValue];
        _lastBillAmount = [[responseDic valueForKey:@"LastBillAmount"] doubleValue];
        _priority = [[responseDic valueForKey:@"priority"] integerValue];
        _accountSystem = [[responseDic[@"accountSystem"] validAndNotEmptyStringObject] copy];
      //  _assetDetailCollection = [[responseDic[@"AssetDetailCollection"] validAndNotEmptyStringObject] copy];
        NSArray *assetDetailCollectionArray = [responseDic valueForKey:@"AssetDetailCollection"];
        if([assetDetailCollectionArray isKindOfClass:[NSArray class]])
        {
            for(NSDictionary *dic in assetDetailCollectionArray)
            {
                if(![dic isKindOfClass:[NSDictionary class]])
                    continue;
                
                BTAssetDetailCollection *assetDetail = [[BTAssetDetailCollection alloc] initWithResponseDictionaryFromAssetsBACAPIResponse:dic];
                
                
                if(assetDetail)
                {
                    [self addAssetDetail:assetDetail];
                }
            }
        }
        else
        {
            DDLogError(@"'Products' element missing in order summary response ");
        }

    }
    return self;
    
}



#pragma mark - Setter & Getter Methods

- (NSArray *)assetDetailCollection
{
    return [_assetDetailCollection copy];
}

#pragma mark - Public Methods

- (void)addAssetDetail:(BTAssetDetailCollection *)assetDetail
{
    if(_assetDetailCollection == nil)
    {
        _assetDetailCollection = [[NSMutableArray alloc] init];
    }
    
    if(assetDetail == nil)
        return;
    
    [_assetDetailCollection addObject:assetDetail];
}




@end
