//
//  AssetDetailCollection.m
//  BTBusinessApp
//
//  Created by vectoscalar on 09/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTAssetDetailCollection.h"
#import "NSObject+APIResponseCheck.h"
#import "BTAssetCombinedRental.h"

@implementation BTAssetDetailCollection
{
   NSMutableArray *_combinedRentalDesc;
}

- (instancetype)initWithResponseDictionaryFromAssetsBACAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _serviceText = [[responseDic[@"ServiceText"] validAndNotEmptyStringObject] copy];
        _serviceNumber = [[responseDic[@"ServiceNumber"] validAndNotEmptyStringObject] copy];
        _installedAt = [[responseDic[@"installedAt"] validAndNotEmptyStringObject] copy];
        _combinedRent = [[responseDic[@"combinedRent"] validAndNotEmptyStringObject] copy];
        _contractTerm = [[responseDic[@"contractTerm"] validAndNotEmptyStringObject] copy];
        _contractEnd = [[responseDic[@"contractEnd"] validAndNotEmptyStringObject] copy];
        _assetID = [[responseDic[@"assetID"] validAndNotEmptyStringObject] copy];
        _assetNumber = [[responseDic[@"assetNumber"] validAndNotEmptyStringObject] copy];
        if(!_serviceNumber)
        {
            _serviceNumber = _assetNumber;
        }
        _assetIntegrationID = [[responseDic[@"assetIntegrationID"] validAndNotEmptyStringObject] copy];
        //_combinedRentalDesc = [[responseDic[@"CombinedRentalDesc"] validAndNotEmptyStringObject] copy];
        
        
        NSArray *combinedRentalArray = [responseDic valueForKey:@"CombinedRentalDesc"];
        if([combinedRentalArray isKindOfClass:[NSArray class]])
        {
            for(NSDictionary *dic in combinedRentalArray)
            {
                if(![dic isKindOfClass:[NSDictionary class]])
                    continue;
                
                BTAssetCombinedRental *cRental = [[BTAssetCombinedRental alloc] initWithResponseDictionaryFromAssetsBACAPIResponse:dic];
                
                
                if(cRental)
                {
                    [self addAssetRental:cRental];
                }
            }
        }
        else
        {
            DDLogError(@"'Products' element missing in order summary response ");
        }

        
    }
    return self;
    
}



#pragma mark - Setter & Getter Methods

- (NSArray *)combinedRentalDesc
{
    return [_combinedRentalDesc copy];
}

#pragma mark - Public Methods

- (void)addAssetRental:(BTAssetCombinedRental *)combinedRental
{
    if(_combinedRentalDesc == nil)
    {
        _combinedRentalDesc = [[NSMutableArray alloc] init];
    }
    
    if(combinedRental == nil)
        return;
    
    [_combinedRentalDesc addObject:combinedRental];
}



@end
