//
//  BTClientServiceInstance.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 09/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BTResilientHubAsset;

typedef NS_ENUM(NSInteger, BTGuestWiFiStatusFlag) {
    BTGuestWiFiStatusFlagAvailable,
    BTGuestWiFiStatusFlagOn,
    BTGuestWiFiStatusFlagNewHubNeeded,
    BTGuestWiFiStatusFlagCantConnect,
    BTGuestWiFiStatusFlagUnavailableOn4GAssure,
    BTGuestWiFiStatusFlagUnavailableBTWiFiOnly,
    BTGuestWiFiStatusFlagNULLSerialNum,
    BTGuestWiFiStatusFlagUnknown
};

@interface BTClientServiceInstance : NSObject

// properties returned/implied by queryClientProfileDetails API
@property (nonatomic, readonly) NSDictionary *refDict;
@property (nonatomic, readonly) NSString *hubSerialNumber;
@property (nonatomic, readonly) NSString *key;
@property (nonatomic, readonly) NSString *postCode;
@property (nonatomic, readonly) NSString *serviceCode;
@property (nonatomic, readonly) NSString *serviceId;
@property (nonatomic, readonly) NSString *serviceName;
@property (nonatomic, readonly) NSString *status;
@property (nonatomic, readonly) NSString *guestWifiStatus;
@property (nonatomic, readonly) BTGuestWiFiStatusFlag guestWifiFlag;
@property (nonatomic, readonly) BOOL resilientFlag;

// additional properties returned by getBusinessHubDetails API
@property (nonatomic, readonly) NSDictionary *hubDetailsDict;
@property (nonatomic, readonly) NSString *SSID;
@property (nonatomic, readonly) NSString *currentWiFiStatus;
@property (nonatomic, readonly) NSString *customerType;
@property (nonatomic, readonly) NSString *firmwareVersion;
@property (nonatomic, readonly) NSString *ipAddress;
@property (nonatomic, readonly) NSString *lastActivationTime;
@property (nonatomic, readonly) NSString *lastContactTime;
@property (nonatomic, readonly) NSString *operationalMode;
@property (nonatomic, readonly) NSString *oui;
@property (nonatomic, readonly) NSString *previousRequestFailureReason;
@property (nonatomic, readonly) NSString *previousRequestStatus;
@property (nonatomic, readonly) NSString *previousRequestTimestamp;
@property (nonatomic, readonly) NSString *previousWiFiRequest;
@property (nonatomic, readonly) NSString *productClass;
@property (readonly) BOOL gotHubStatus;

+ (NSArray<BTClientServiceInstance*>*)clientServiceInstancesFromAPIResponse:(id)response;
- (instancetype)initFromAPIResponse:(NSDictionary*)response;
- (instancetype)initFromHubDetails:(NSDictionary*)hubDetails withResilientAsset:(BTResilientHubAsset*)asset;
//- (void)updateGuestWiFiStatus:(NSString *)status;
- (void)updateGuestWiFiStatus:(BTGuestWiFiStatusFlag)status;
- (void)updateWithBusinessHubDetails:(NSDictionary*)details;
- (void)updateWithToggleWifiResponse:(NSDictionary*)response;
- (void)setHubStatusFetched:(BOOL)done;

@end
