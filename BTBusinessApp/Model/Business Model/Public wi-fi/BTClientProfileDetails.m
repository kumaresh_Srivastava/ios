//
//  BTClientProfileDetails.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 09/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTClientProfileDetails.h"
#import "BTClientServiceInstance.h"

@implementation BTClientProfileDetails

+ (BTClientProfileDetails *)clientDetailsFromAPIResponse:(NSDictionary *)response
{
    BTClientProfileDetails *details = nil;
    if (response) {
        details = [[BTClientProfileDetails alloc] initWithAPIResponse:response];
    }
    return details;
}

- (instancetype)initWithAPIResponse:(NSDictionary *)responseDict
{
    if (self = [super init]) {
        _refDict = [NSDictionary dictionaryWithDictionary:responseDict];
        _identifierValue = [responseDict objectForKey:@"identifierValue"];
        _clientServiceList = [BTClientServiceInstance clientServiceInstancesFromAPIResponse:[responseDict valueForKeyPath:@"listOfClientServiceInstance.clientServiceInstance"]];
        
    }
    return self;
}

- (instancetype)initWithClientServiceList:(NSArray<BTClientServiceInstance *> *)clientServiceList
{
    if (self = [super init]) {
        _refDict = @{};
        _identifierValue = @"";
        _clientServiceList = clientServiceList;
        
    }
    return self;
}

@end
