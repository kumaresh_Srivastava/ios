//
//  BTClientServiceInstance.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 09/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTClientServiceInstance.h"
#import "NSObject+APIResponseCheck.h"
#import "BTResilientHubAsset.h"

@implementation BTClientServiceInstance

+ (NSArray<BTClientServiceInstance *> *)clientServiceInstancesFromAPIResponse:(id)response
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    if ([response isKindOfClass:[NSDictionary class]]) {
        BTClientServiceInstance *instance = [[BTClientServiceInstance alloc] initFromAPIResponse:(NSDictionary*)response];
        [array addObject:instance];
    } else if ([response isKindOfClass:[NSArray class]]) {
        for (NSDictionary *infoDict in (NSArray*)response) {
            BTClientServiceInstance *instance = [[BTClientServiceInstance alloc] initFromAPIResponse:infoDict];
            [array addObject:instance];
        }
    }
    return array;
}

- (instancetype)initFromAPIResponse:(NSDictionary*)response
{
    if (self = [self init]) {
        _refDict = [NSDictionary dictionaryWithDictionary:response];
        _hubSerialNumber = [[response objectForKey:@"hubSerialNumber"] validAndNotEmptyStringObject];
        _key = [[response objectForKey:@"key"] validAndNotEmptyStringObject];
        _postCode = [[response objectForKey:@"postCode"] validAndNotEmptyStringObject];
        _serviceCode = [[response objectForKey:@"serviceCode"] validAndNotEmptyStringObject];
        if ([_serviceCode isEqualToString:@"BT_BUSINESS_BROADBAND"]) {
            _serviceName = @"Business broadband";
        } else {
            _serviceName = _serviceCode;
        }
        _serviceId = [[response objectForKey:@"serviceId"] validAndNotEmptyStringObject];
        _status = [[response objectForKey:@"status"] validAndNotEmptyStringObject];
        NSString *resilient = [[response objectForKey:@"Resilient"] validAndNotEmptyStringObject];
        if ([resilient isEqualToString:@"Y"]) {
            _resilientFlag = YES;
        } else if ([resilient isEqualToString:@"N"]) {
            _resilientFlag = NO;
        }
        _guestWifiStatus = @"Can't connect";
        _guestWifiFlag = BTGuestWiFiStatusFlagUnknown;
        _gotHubStatus = NO;
    }
    return self;
}

- (instancetype)initFromHubDetails:(NSDictionary *)hubDetails withResilientAsset:(BTResilientHubAsset *)asset
{
    if (self = [self init]) {
        _refDict = [NSDictionary dictionaryWithDictionary:hubDetails];
        _hubSerialNumber = [[hubDetails objectForKey:@"Serial Number"] validAndNotEmptyStringObject];
        _serviceCode = [[hubDetails objectForKey:@"Customer Type"] validAndNotEmptyStringObject];
        _serviceName = @"";
        if ([_serviceCode isEqualToString:@"BusinessBB"]) {
            _serviceName = @"Business broadband";
        } else {
            _serviceName = _serviceCode;
        }
        _serviceId = asset.serviceID;
        _productClass = [[hubDetails objectForKey:@"Product Class"] validAndNotEmptyStringObject];
        _operationalMode = [[hubDetails objectForKey:@"Operational Mode"] validAndNotEmptyStringObject];
        _oui = [[hubDetails objectForKey:@"oui"] validAndNotEmptyStringObject];

    }
    return self;
}

- (void)updateGuestWiFiStatus:(BTGuestWiFiStatusFlag)status
{
    _guestWifiFlag = status;
    switch (status) {
        case BTGuestWiFiStatusFlagAvailable:
            _guestWifiStatus = @"Available";
            break;
            
        case BTGuestWiFiStatusFlagOn:
            _guestWifiStatus = @"On";
            break;
            
        case BTGuestWiFiStatusFlagNewHubNeeded:
        case BTGuestWiFiStatusFlagUnavailableBTWiFiOnly:
            _guestWifiStatus = @"New hub needed";
            break;
            
        case BTGuestWiFiStatusFlagUnavailableOn4GAssure:
            _guestWifiStatus = @"Available";
            break;
        
        case BTGuestWiFiStatusFlagCantConnect:
        case BTGuestWiFiStatusFlagNULLSerialNum:
        case BTGuestWiFiStatusFlagUnknown:
        default:
            _guestWifiStatus = @"Can't connect";
            break;
    }
}

- (BOOL)guestWiFiSupported {
    BOOL retVal = NO;
    if ([self.productClass isEqualToString:@"Business Hub 50 Type A"] || [self.productClass isEqualToString:@"Business Hub 60 Type A"]) {
        retVal = YES;
    }
    return retVal;
}

- (BOOL)btWiFiSupported {
    BOOL retVal = [self guestWiFiSupported];
    if (!retVal) {
        retVal = [self.productClass isEqualToString:@"Business Hub 30 Type A"];
    }
    return retVal;
}

- (void)validateWiFiStatus
{
    if ([self.operationalMode isEqualToString:@"MOBILE"]) {
        [self updateGuestWiFiStatus:BTGuestWiFiStatusFlagUnavailableOn4GAssure];
    } else if ([self.operationalMode isEqualToString:@"UNKNOWN"] || [self.operationalMode isEqualToString:@"RESET"]) {
        [self updateGuestWiFiStatus:BTGuestWiFiStatusFlagUnknown];
    } else {
        // operational mode is DSL, VDSL, FIBRE, G.Fast
        // check on hub type
        if ([self guestWiFiSupported]) {
            // hub supports guest wifi, get guest wifi status
            if ([self.currentWiFiStatus isEqualToString:@"GUEST_WIFI_ON"]) {
                [self updateGuestWiFiStatus:BTGuestWiFiStatusFlagOn];
            } else {
                [self updateGuestWiFiStatus:BTGuestWiFiStatusFlagAvailable];
            }
        } else if ([self btWiFiSupported]) {
            // hub supports bt wifi only (i.e. Hub 3)
            //[self updateGuestWiFiStatus:BTGuestWiFiStatusFlagUnavailableBTWiFiOnly];
            // Nokia not currently supporting Hub 3 so show new hub screen
            [self updateGuestWiFiStatus:BTGuestWiFiStatusFlagNewHubNeeded];
        } else {
            // hub doesn not support any public wifi
            [self updateGuestWiFiStatus:BTGuestWiFiStatusFlagNewHubNeeded];
        }
    }
    
}

- (void)updateWithBusinessHubDetails:(NSDictionary *)details
{
    //
    _hubDetailsDict = [details copy];
    _SSID = [[details objectForKey:@"SSID"] validAndNotEmptyStringObject];
    _currentWiFiStatus = [[details objectForKey:@"currentWiFiStatus"] validAndNotEmptyStringObject];
    _customerType = [[details objectForKey:@"customerType"] validAndNotEmptyStringObject];
    _firmwareVersion = [[details objectForKey:@"firmwareVersion"] validAndNotEmptyStringObject];
    _ipAddress = [[details objectForKey:@"ipAddress"] validAndNotEmptyStringObject];
    _lastActivationTime = [[details objectForKey:@"lastActivationTime"] validAndNotEmptyStringObject];
    _lastContactTime = [[details objectForKey:@"lastContactTime"] validAndNotEmptyStringObject];
    _operationalMode = [[details objectForKey:@"operationalMode"] validAndNotEmptyStringObject];
    _oui = [[details objectForKey:@"oui"] validAndNotEmptyStringObject];
    _previousRequestFailureReason = [[details objectForKey:@"previousRequestFailureReason"] validAndNotEmptyStringObject];
    _previousRequestStatus = [[details objectForKey:@"previousRequestStatus"] validAndNotEmptyStringObject];
    _previousRequestTimestamp = [[details objectForKey:@"previousRequestTimestamp"] validAndNotEmptyStringObject];
    _previousWiFiRequest = [[details objectForKey:@"previousWiFiRequest"] validAndNotEmptyStringObject];
    _productClass = [[details objectForKey:@"productClass"] validAndNotEmptyStringObject];
    
    [self validateWiFiStatus];
}

- (void)updateWithToggleWifiResponse:(NSDictionary *)response
{
    _SSID = [[response objectForKey:@"SSID"] validAndNotEmptyStringObject];
    NSString *updateWifi = [[response objectForKey:@"updateWiFi"] validAndNotEmptyStringObject];
    NSString *updateWifiValue = [[response objectForKey:@"updateWiFiValue"] validAndNotEmptyStringObject];
    NSString *updatWifiTimestamp = [[response objectForKey:@"updateWiFiTimestamp"] validAndNotEmptyStringObject];
    if ([updateWifiValue isEqualToString:@"ON"]) {
        if ([updateWifi isEqualToString:@"GuestWiFi"]) {
            _currentWiFiStatus = @"GUEST_WIFI_ON";
        } else if ([updateWifi isEqualToString:@"BTWiFi"]) {
            _currentWiFiStatus = @"BT_WIFI_ON";
        }
    } else {
        _currentWiFiStatus = @"";
        _previousWiFiRequest = @"DISABLE_PUBLIC";
    }
    _previousRequestStatus = updatWifiTimestamp;
    _previousRequestStatus = @"completed";
    [self validateWiFiStatus];
}

- (void)setHubStatusFetched:(BOOL)done
{
    _gotHubStatus = done;
}

@end
