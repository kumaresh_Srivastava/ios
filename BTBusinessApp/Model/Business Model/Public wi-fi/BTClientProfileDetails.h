//
//  BTClientProfileDetails.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 09/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BTClientServiceInstance;

@interface BTClientProfileDetails : NSObject

@property (nonatomic, readonly) NSDictionary *refDict;
@property (nonatomic, readonly) NSString *identifierValue;
@property (nonatomic, readonly) NSArray<BTClientServiceInstance*> *clientServiceList;

+ (BTClientProfileDetails*)clientDetailsFromAPIResponse:(NSDictionary*)response;
- (instancetype)initWithAPIResponse:(NSDictionary*)responseDict;
- (instancetype)initWithClientServiceList:(NSArray<BTClientServiceInstance*>*)clientServiceList;

@end
