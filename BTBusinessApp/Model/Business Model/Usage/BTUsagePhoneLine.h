//
//  BTUsagePhoneLine.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 02/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTUsagePhoneLine : NSObject

@property (nonatomic, readonly) NSString *calls;
@property (nonatomic, readonly) NSString *cost;
@property (nonatomic, readonly) NSString *name;

- (instancetype)initWithResponseDict:(NSDictionary *)dict;

@end
