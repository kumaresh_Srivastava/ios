//
//  BTUsagePhoneProduct.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 26/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTUsagePhoneProduct.h"
#import "BTUsagePhoneLine.h"
#import "NSObject+APIResponseCheck.h"

@implementation BTUsagePhoneProduct


- (instancetype)initWithResponseDictionaryFromPhoneLineUsageDetailsAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _serviceLineItem = [[responseDic[@"ServiceLineItem"] validAndNotEmptyStringObject] copy];
        _serviceId = [[responseDic[@"ServiceId"] validAndNotEmptyStringObject] copy];
        _serviceType = [[responseDic[@"ServiceType"] validAndNotEmptyStringObject] copy];//To Do : Manik check whether empty validation is required or not
        _totalCalls = [responseDic valueForKey:@"TotalCalls"];
        _totalCost = [NSString stringWithFormat:@"%.2f",[[responseDic valueForKey:@"TotalCost"] floatValue]];
        
        _numberOfDays = [[responseDic valueForKey:@"NoOfDays"] integerValue];
        _latestTime = [responseDic valueForKey:@"LatestTime"];
        
        NSArray *phoneLineDictArray = [responseDic valueForKey:@"ServiceUsages"];


        if(phoneLineDictArray && phoneLineDictArray.count > 0)
        {
            NSMutableArray *phoneLineObjectsArray = [NSMutableArray array];
            for (NSDictionary *dict in phoneLineDictArray) {

                BTUsagePhoneLine *phoneLineObj = [[BTUsagePhoneLine alloc] initWithResponseDict:dict];
                if(phoneLineObj)
                   [phoneLineObjectsArray addObject:phoneLineObj];

            }

            _phoneLines = [NSArray arrayWithArray:phoneLineObjectsArray];

        }

    }

    return self;
}

/*
- (instancetype)initWithResponseDictionaryFromPhoneLineUsageDetailsAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _phonelineNumber = [[responseDic[@"PhonelineNumber"] validAndNotEmptyStringObject] copy];
        _totalCalls = [responseDic valueForKey:@"TotalCalls"];
        _totalCost = [NSString stringWithFormat:@"%.2f",[[responseDic valueForKey:@"TotalCost"] floatValue]];
        
        _numberOfDays = [[responseDic valueForKey:@"NoOfDays"] integerValue];
        _latestTime = [responseDic valueForKey:@"LatestTime"];
        
        NSArray *phoneLineDictArray = [responseDic valueForKey:@"phoneLines"];
        
        
        if(phoneLineDictArray && phoneLineDictArray.count > 0)
        {
            NSMutableArray *phoneLineObjectsArray = [NSMutableArray array];
            for (NSDictionary *dict in phoneLineDictArray) {
                
                BTUsagePhoneLine *phoneLineObj = [[BTUsagePhoneLine alloc] initWithResponseDict:dict];
                if(phoneLineObj)
                    [phoneLineObjectsArray addObject:phoneLineObj];
                
            }
            
            _phoneLines = [NSArray arrayWithArray:phoneLineObjectsArray];
            
        }
        
    }
    
    return self;
}
*/

@end
