//
//  BTBroadbandUsage.h
//  BTBusinessApp
//
//  Created by VectoScalar on 12/26/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BTMonthlyUsage <NSObject>

@end

@interface BTBroadbandUsage : NSObject
@property (nonatomic, readonly) NSString *productName;
@property (nonatomic, readonly) NSString *serviceID;
@property (nonatomic, readonly) NSString *displayUnit;
@property (nonatomic, readonly) NSString *usageLimit;
@property (nonatomic, readonly) NSDate *lastUsageRefreshDate;
@property (nonatomic, readonly) double totalDownload;
@property (nonatomic, readonly) double totalUpload;
@property (nonatomic, readonly) double totalUsage;
@property (nonatomic, readonly) double bandWidthRemaining;
@property (nonatomic, readonly) NSInteger alertMessageStatus;
@property (nonatomic, readonly, strong) NSArray*monthlyUsageArray;


- (instancetype)initWithResponseDictionaryFromBroadBandAPIResponse:(NSDictionary *)responseDict;

@end


@interface BTMonthlyUsage : NSObject
@property (nonatomic, readonly) NSString *monthName;
@property (nonatomic, readonly) double totalUsage;
@property (nonatomic, readonly) NSString *productLimit;
- (instancetype)initWithResponseDictionaryFromAPIResponse:(NSDictionary *)responseDict andProductLimit:(NSString *)productLimit;
@end
