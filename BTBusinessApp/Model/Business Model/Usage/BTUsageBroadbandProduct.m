//
//  BTUsageBroadbandProduct.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 26/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTUsageBroadbandProduct.h"
#import "NSObject+APIResponseCheck.h"

@implementation BTUsageBroadbandProduct

- (instancetype)initWithResponseDictionaryFromBroadbandUsageDetailsAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _promotionProductName = [[responseDic[@"PromotionProductName"] validAndNotEmptyStringObject] copy];
        _promotionProductScode = [[responseDic[@"PromotionProductScode"] validAndNotEmptyStringObject] copy];
        _serviceId = [[responseDic[@"ServiceId"] validAndNotEmptyStringObject] copy];
        _postcode = [[responseDic[@"Postcode"] validAndNotEmptyStringObject] copy];
        _assetIntegrationId = [[responseDic[@"AssetIntegrationId"] validAndNotEmptyStringObject] copy];
    }

    return self;
}


@end
