//
//  BTUsageBroadbandProduct.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 26/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTUsageBroadbandProduct : NSObject


@property (nonatomic, readonly) NSString *promotionProductName;
@property (nonatomic, readonly) NSString *promotionProductScode;
@property (nonatomic, readonly) NSString *serviceId;
@property (nonatomic, readonly) NSString *postcode;
@property (nonatomic, readonly) NSString *assetIntegrationId;

- (instancetype)initWithResponseDictionaryFromBroadbandUsageDetailsAPIResponse:(NSDictionary *)responseDic;


@end
