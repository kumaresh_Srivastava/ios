//
//  BTUsagePhoneLine.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 02/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTUsagePhoneLine.h"
#import "NSObject+APIResponseCheck.h"

@implementation BTUsagePhoneLine


- (instancetype)initWithResponseDict:(NSDictionary *)dict
{
    self = [super init];
    if(self)
    {
        _calls = [dict valueForKey:@"Calls"];
        _cost = [dict valueForKey:@"Cost"];
        _name = [[dict[@"Name"] validAndNotEmptyStringObject] copy];
    }
    return self;
}
@end
