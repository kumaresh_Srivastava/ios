//
//  BTUsagePhoneProduct.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 26/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTUsagePhoneProduct : NSObject

@property (nonatomic, readonly) NSString *serviceId;
@property (nonatomic, readonly) NSString *serviceType;
@property (nonatomic, readonly) NSString *totalCalls;
@property (nonatomic, readonly) NSString *totalCost;
@property (nonatomic,readonly) NSInteger numberOfDays;
@property (nonatomic,readonly) NSString *latestTime;
@property (nonatomic, readonly) NSArray *phoneLines;
@property (nonatomic, readonly) NSString *serviceLineItem;

- (instancetype)initWithResponseDictionaryFromPhoneLineUsageDetailsAPIResponse:(NSDictionary *)responseDic;

@end

/*
@interface BTUsagePhoneProduct : NSObject

@property (nonatomic, readonly) NSString *phonelineNumber;
@property (nonatomic, readonly) NSString *totalCalls;
@property (nonatomic, readonly) NSString *totalCost;
@property (nonatomic,readonly) NSInteger numberOfDays;
@property (nonatomic,readonly) NSString *latestTime;
@property (nonatomic, readonly) NSArray *phoneLines;

- (instancetype)initWithResponseDictionaryFromPhoneLineUsageDetailsAPIResponse:(NSDictionary *)responseDic;

@end
*/
