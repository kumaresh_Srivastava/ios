//
//  BTBroadbandUsage.m
//  BTBusinessApp
//
//  Created by VectoScalar on 12/26/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBroadbandUsage.h"
#import "AppManager.h"

@implementation BTBroadbandUsage

- (instancetype)initWithResponseDictionaryFromBroadBandAPIResponse:(NSDictionary *)responseDict{
    
    self = [super init];
    if(self)
    {
        
        if(responseDict){
            
            _serviceID = [responseDict valueForKey:@"serviceId"];
            _productName = [responseDict valueForKey:@"ProductName"];
            
            NSDictionary *broadbandUsageStatusDict = [responseDict valueForKey:@"BroadbandUsagePolicyStatus"];
            
            if(broadbandUsageStatusDict){
                
                _displayUnit = [broadbandUsageStatusDict valueForKey:@"DisplayUnit"];
                _usageLimit = [broadbandUsageStatusDict valueForKey:@"ProductLimit"];
                
            }
            
            
            NSDictionary *retailUsageDict = [responseDict valueForKey:@"RetailBroadbandUsage"];
            if(retailUsageDict){
                
                NSString *dateString = [retailUsageDict valueForKey:@"LastUsageRefreshDate"];
                if(dateString){
                    
                    _lastUsageRefreshDate = [AppManager NSDateWithUTCFormatFromNSString:dateString];
                }
                
                _totalUsage = [[retailUsageDict valueForKey:@"TotalUsage"] doubleValue];
                _totalUpload = [[retailUsageDict valueForKey:@"TotalUpload"] doubleValue];
                _totalDownload = [[retailUsageDict valueForKey:@"TotalDownload"] doubleValue];
                _alertMessageStatus = [[retailUsageDict valueForKey:@"aleartMsgStatus"] integerValue];
                _bandWidthRemaining = [[retailUsageDict valueForKey:@"BandwidthRemaining"] doubleValue];
                
                
                
                NSArray *mothlyUsageArray = [retailUsageDict valueForKey:@"MonthlyUsage"];
                
                NSMutableArray *monthyUsageModelArray = [NSMutableArray array];
                
                if(mothlyUsageArray && [mothlyUsageArray isKindOfClass:[NSArray class]]){
                    
                    
                    for(NSDictionary *dict in mothlyUsageArray){
                        
                        if(dict && [dict isKindOfClass:[NSDictionary class]]){
                            
                            BTMonthlyUsage *monthlyUsage = [[BTMonthlyUsage alloc] initWithResponseDictionaryFromAPIResponse:dict andProductLimit:_usageLimit];
                            [monthyUsageModelArray addObject:monthlyUsage];
                            
                        }
                    }
                    
                }
                
                _monthlyUsageArray = [NSArray arrayWithArray:monthyUsageModelArray];
            
            }
            
        }


    }
    
    return self;
    
}


@end



@implementation BTMonthlyUsage

- (instancetype)initWithResponseDictionaryFromAPIResponse:(NSDictionary *)responseDict andProductLimit:(NSString *)productLimit{
    
    self = [super init];
    
    if(self)
    {
        
        if(responseDict){
            
            _monthName = [responseDict valueForKey:@"MonthName"];
            
            /*
            NSCharacterSet *notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
            if ([_monthName rangeOfCharacterFromSet:notDigits].location != NSNotFound)
            {
                _monthName = [_monthName stringByAppendingString:@" 2016"];
            }
             */

            _totalUsage = [[responseDict valueForKey:@"TotalUsage"] doubleValue];
            _productLimit = productLimit;
        }
        
        
    }
    return self;

}

@end
