//
//  BTGeneralCategory.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 09/02/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTSpecificCategory.h"

@interface BTGeneralCategory : BTSpecificCategory

@property (strong,nonatomic)  NSString *startDay;
@property (strong,nonatomic)  NSString *endDay;

- (instancetype)initWithResponseDictionaryFromHelpAndSupportAPIResponse:(NSDictionary *)responseDic;

@end
