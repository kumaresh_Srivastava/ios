//
//  BTCug.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 14/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTCug.h"

@implementation BTCug

- (instancetype)initWithCugName:(NSString *)cugName cugID:(NSString*)cugID groupKey:(NSString *)groupKey cugRole:(NSInteger)cugRole andIndexInAPIResponse:(NSInteger)indexInAPIResponse andContactId:(NSString *)contactId
{
    self = [super init];
    if(self)
    {
        _cugName = [cugName copy];
        _cugID = [cugID copy];
        _groupKey = [groupKey copy];
        _cugRole = cugRole;
        _indexInAPIResponse = indexInAPIResponse;
        _refKey = @"";
        _contactId = contactId;
    }
    return self;
}

- (instancetype)initWithCugName:(NSString *)cugName cugID:(NSString*)cugID groupKey:(NSString *)groupKey cugRole:(NSInteger)cugRole indexInAPIResponse:(NSInteger)indexInAPIResponse andRefKey:(NSString *)refKey andContactId:(NSString *)contactId;
{
    self = [super init];
    if(self)
    {
        _cugName = [cugName copy];
        _cugID = [cugID copy];
        _groupKey = [groupKey copy];
        _cugRole = cugRole;
        _indexInAPIResponse = indexInAPIResponse;
        _refKey = refKey;
        _contactId = contactId;
    }
    return self;
}

- (NSString *)description {

    return [NSString stringWithFormat:@"%@(Role:%ld, Key:%@, Index:%ld, ContactId:%@)", _cugName, (long)_cugRole, _groupKey, (long)_indexInAPIResponse, _contactId];
}

@end
