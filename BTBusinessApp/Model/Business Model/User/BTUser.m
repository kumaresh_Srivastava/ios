//
//  BTUser.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 14/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTUser.h"
#import "NSObject+APIResponseCheck.h"
#import "BTCug.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"

@interface BTUser () {

    NSMutableArray *_arrayOfCugs;
}

@end

@implementation BTUser

- (instancetype)initWithUsername:(NSString *)username andResponseDictionaryFromQueryUserDetailsAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _username = username;
        
        _titleInName = [[responseDic[@"Title"] validAndNotEmptyStringObject] copy];
        _firstName = [[responseDic[@"FirstName"] validAndNotEmptyStringObject] copy];
        _lastName = [[responseDic[@"LastName"] validAndNotEmptyStringObject] copy];
        _securityQuestion = [[responseDic[@"SecurityQuestion"] validAndNotEmptyStringObject] copy];
        _securityAnswer = [[responseDic[@"SecurityAsnwer"] validAndNotEmptyStringObject] copy];
        _mobileNumber = [[responseDic[@"MobileNumber"] validAndNotEmptyStringObject] copy];
        _landlineNumber = [[responseDic[@"LandlineNumber"] validAndNotEmptyStringObject] copy];
        _primaryEmailAddress = [[responseDic[@"PrimaryEmailAddress"] validAndNotEmptyStringObject] copy];
        _alternativeEmailAddress = [[responseDic[@"AlternativeEmailAddress"] validAndNotEmptyStringObject] copy];
        _contactId = [[responseDic[@"ContactId"] validAndNotEmptyStringObject] copy];
        _nextStepURL = [[responseDic[@"NextStepURL"] validAndNotEmptyStringObject] copy];
        
        NSDictionary *dicOfGroups = [responseDic valueForKey:@"Groups"];
        if ([dicOfGroups isKindOfClass:[NSDictionary class]])
        {
            id groupObject = [dicOfGroups valueForKey:@"Group"];
            if([groupObject isKindOfClass:[NSArray class]])
            {
                for(NSDictionary *groupDic in (NSArray *)groupObject)
                {
                    if([groupDic isKindOfClass:[NSDictionary class]])
                    {
                        NSString *cugName;
                        if (groupDic[@"Name"])
                        {
                            cugName = [NSString stringWithFormat:@"%@",groupDic[@"Name"]];
                            if(!cugName)
                                cugName = @"";
                        }
                        else
                        {
                            cugName = @"";
                        }
                        
                        NSString *cugID = [groupDic[@"CugId"] validAndNotEmptyStringObject];
                        
                        NSString *groupKey = [groupDic[@"Key"] validAndNotEmptyStringObject];
                        if(!groupKey)
                            groupKey = @"";
                        
                        NSInteger roleID = [groupDic[@"RoleId"] integerValue];
                   
                        
                        NSInteger indexInResponse = [(NSArray *)groupObject indexOfObject:groupDic];
                        
                        NSString *contactId = [groupDic[@"ContactId"] validAndNotEmptyStringObject];

                        BTCug *cug = [[BTCug alloc] initWithCugName:cugName cugID:cugID groupKey:groupKey cugRole:roleID andIndexInAPIResponse:indexInResponse andContactId:contactId];
                        if(cug)
                        {
                            [self addCug:cug];
                        }
                    }
                }
            }
            else if([groupObject isKindOfClass:[NSDictionary class]])
            {
                NSString *cugName = [(NSDictionary *)groupObject[@"Name"] validAndNotEmptyStringObject];
                if(!cugName) {
                    cugName = @"";
                }
                
                NSString *cugID = [(NSDictionary *)groupObject[@"CugId"] validAndNotEmptyStringObject];
                if(!cugName) {
                    cugName = @"";
                }
                
                NSString *groupKey = [(NSDictionary *)groupObject[@"Key"] validAndNotEmptyStringObject];
                if(!groupKey) {
                    groupKey = @"";
                }
                
                NSString *contactId = [(NSDictionary *)groupObject[@"ContactId"] validAndNotEmptyStringObject];
                if (!contactId) {
                    contactId = @"";
                }
                
                NSInteger roleID = [[(NSDictionary *)groupObject valueForKey:@"RoleId"] integerValue];
                NSInteger indexInResponse = 0;

                BTCug *cug = [[BTCug alloc] initWithCugName:cugName cugID:cugID groupKey:groupKey cugRole:roleID andIndexInAPIResponse:indexInResponse andContactId:contactId];
                if(cug)
                {
                    [self addCug:cug];
                }
            }
        }
    }
    return self;
}

- (instancetype)initWithCDUser:(CDUser *)persistenceUser securityQuestion:(NSString *)question securityAnswer:(NSString *)answer mobileNumber:(NSString *)mobileNumber landlineNumber:(NSString *)landlineNumber primaryEmail:(NSString *)primaryEmail contactID:(NSString*)contactId andAlternativeEmail:(NSString *)alternativeEmail
{
    self = [super init];
    if(self)
    {
        _username = persistenceUser.username;
        _titleInName = persistenceUser.titleInName;
        _firstName = persistenceUser.firstName;
        _lastName = persistenceUser.lastName;
        _securityQuestion = [question copy];
        _securityAnswer = [answer copy];
        _mobileNumber = [mobileNumber copy];
        _landlineNumber = [landlineNumber copy];
        _primaryEmailAddress = [primaryEmail copy];
        _alternativeEmailAddress = [alternativeEmail copy];
        _contactId = [contactId copy];


        NSArray *cugsArrayInPersistence = [persistenceUser.cugs allObjects];
        for(CDCug *persistenceCug in cugsArrayInPersistence)
        {
            NSString *cugName = persistenceCug.cugName;
            NSString *cugID = persistenceCug.cugId;
            NSString *groupKey = persistenceCug.groupKey;
            NSString *refKey = persistenceCug.refKey;
            NSInteger roleID = [persistenceCug.cugRole integerValue];
            NSInteger indexInResponse = [persistenceCug.indexInAPIResponse integerValue];
            NSString *contactId  = persistenceCug.contactId;

            BTCug *cug = [[BTCug alloc] initWithCugName:cugName cugID:cugID groupKey:groupKey cugRole:roleID indexInAPIResponse:indexInResponse andRefKey:refKey andContactId:contactId];
            if(cug)
            {
                [self addCug:cug];
            }
        }

        if(persistenceUser.currentSelectedCug)
        {
            BTCug *newSelectedCug = [[BTCug alloc] initWithCugName:persistenceUser.currentSelectedCug.cugName cugID:persistenceUser.currentSelectedCug.cugId groupKey:persistenceUser.currentSelectedCug.groupKey cugRole:[persistenceUser.currentSelectedCug.cugRole integerValue] indexInAPIResponse:[persistenceUser.currentSelectedCug.indexInAPIResponse integerValue] andRefKey:persistenceUser.currentSelectedCug.refKey andContactId: persistenceUser.currentSelectedCug.contactId];
            _currentSelectedCug = newSelectedCug;
        }
        else
        {
            NSArray *sortedArrayOfCugs = [[persistenceUser.cugs allObjects] sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"indexInAPIResponse" ascending:YES]]];
            if([sortedArrayOfCugs count] >= 1)
            {
                CDCug *persistenceCugInFirstIndex = [sortedArrayOfCugs objectAtIndex:0];
                _currentSelectedCug = [[BTCug alloc] initWithCugName:persistenceCugInFirstIndex.cugName cugID:persistenceCugInFirstIndex.cugId groupKey:persistenceCugInFirstIndex.groupKey cugRole:[persistenceCugInFirstIndex.cugRole integerValue] indexInAPIResponse:[persistenceCugInFirstIndex.indexInAPIResponse integerValue] andRefKey:persistenceCugInFirstIndex.refKey andContactId:persistenceCugInFirstIndex.contactId];
            }
        }
    }
    return self;
}

#pragma mark - Setter & Getter Methods

- (NSArray *)arrayOfCugs
{
    return [_arrayOfCugs copy];
}

#pragma mark - Public Methods

- (void)addCug:(BTCug *)cug
{
    if(_arrayOfCugs == nil)
    {
        _arrayOfCugs = [[NSMutableArray alloc] init];
    }

    if(cug == nil)
        return;

    [_arrayOfCugs addObject:cug];
}

- (NSUInteger)indexOfCugWithCugName:(NSString *)cugName {
    
    NSUInteger index = -1;
    for (int i=0; i<self.arrayOfCugs.count; i++) {
        BTCug *groupData = [self.arrayOfCugs objectAtIndex:i];
        if ([[groupData cugName] isEqualToString:cugName]) {
            index = groupData.indexInAPIResponse;
            break;
        }
    }
    return index;
}



@end
