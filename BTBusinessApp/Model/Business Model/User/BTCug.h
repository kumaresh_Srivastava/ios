//
//  BTCug.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 14/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BTUser;

@interface BTCug : NSObject

@property (nonatomic, readonly) NSString *groupKey;
@property (nonatomic, readonly) NSString *cugName;
@property (nonatomic, readonly) NSString *cugID;
@property (nonatomic, readonly) NSString *refKey;
@property (nonatomic, readonly) NSInteger cugRole;
@property (nonatomic, readonly) NSInteger indexInAPIResponse;
@property (nonatomic, readonly) NSString *contactId;

- (instancetype)initWithCugName:(NSString *)cugName cugID:(NSString*)cugID groupKey:(NSString *)groupKey cugRole:(NSInteger)cugRole indexInAPIResponse:(NSInteger)indexInAPIResponse andRefKey:(NSString *)refKey andContactId:(NSString *)contactId;
- (instancetype)initWithCugName:(NSString *)cugName cugID:(NSString*)cugID groupKey:(NSString *)groupKey cugRole:(NSInteger)cugRole andIndexInAPIResponse:(NSInteger)indexInAPIResponse andContactId:(NSString *)contactId;

@end
