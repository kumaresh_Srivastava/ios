//
//  BTUser.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 14/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BTCug;
@class CDUser;

@interface BTUser : NSObject {

}

@property (nonatomic, readonly) NSString *firstName;
@property (nonatomic, readonly) NSString *lastName;
@property (nonatomic, readonly) NSString *titleInName;
@property (nonatomic, readonly) NSString *username;
@property (nonatomic, readonly) NSString *securityQuestion;
@property (nonatomic, readonly) NSString *securityAnswer;
@property (nonatomic, readonly) NSString *mobileNumber;
@property (nonatomic, readonly) NSString *landlineNumber;
@property (nonatomic, readonly) NSString *primaryEmailAddress;
@property (nonatomic, readonly) NSString *alternativeEmailAddress;
@property (nonatomic, readonly) NSString *contactId;
@property (nonatomic, readonly) NSArray *arrayOfCugs;
@property (nonatomic, readonly) BTCug *currentSelectedCug;
@property (nonatomic, readonly) NSString *nextStepURL;


- (instancetype)initWithUsername:(NSString *)username andResponseDictionaryFromQueryUserDetailsAPIResponse:(NSDictionary *)responseDic;

- (instancetype)initWithCDUser:(CDUser *)persistenceUser securityQuestion:(NSString *)question securityAnswer:(NSString *)answer mobileNumber:(NSString *)mobileNumber landlineNumber:(NSString *)landlineNumber primaryEmail:(NSString *)primaryEmail contactID:(NSString*)contactId andAlternativeEmail:(NSString *)alternativeEmail;

- (void)addCug:(BTCug *)cug;

- (NSUInteger)indexOfCugWithCugName:(NSString *)cugName;

@end
