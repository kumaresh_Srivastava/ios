//
//  BTSMSession.m
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 16/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTSMSession.h"
#import "CDSMSession+CoreDataClass.h"

@implementation BTSMSession

- (instancetype)initWithCDSMSession:(CDSMSession *)smSession
{
    self = [super init];
    if (self)
    {
        _smsessionString = smSession.smsessionString;
        _creationTime = smSession.creationTime;
    }
    return self;
}

- (instancetype)initWithSMSessionString:(NSString *)smsessionString
{
    self = [super init];
    if (self)
    {
        _smsessionString = smsessionString;
        _creationTime = [NSDate date];
    }
    return self;
}

@end
