//
//  BTSMSession.h
//  BTBusinessApp
//
//  Created by Harmandeep Singh on 16/01/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CDSMSession;

@interface BTSMSession : NSObject

@property (nonatomic, readonly) NSString *smsessionString;
@property (nonatomic, readonly) NSDate *creationTime;

- (instancetype)initWithCDSMSession:(CDSMSession *)smSession;
- (instancetype)initWithSMSessionString:(NSString *)smsessionString;

@end
