//
//  BTAuthenticationToken.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/13/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CDAuthenticationToken;

@interface BTAuthenticationToken : NSObject

@property (nonatomic, readonly) NSString *accessToken;
@property (nonatomic, readonly) NSNumber *expiresIn;
@property (nonatomic, readonly) NSString *refreshToken;
@property (nonatomic, readonly) NSString *tokenType;
@property (nonatomic, readonly) NSDate *creationTime;

- (instancetype)initWithVordelTokenData:(NSDictionary*)tokenData;
- (instancetype)initWithEETokenData:(NSDictionary*)tokenData;
- (instancetype)initWithCDAuthenticationToken:(CDAuthenticationToken *)authToken;

@end
