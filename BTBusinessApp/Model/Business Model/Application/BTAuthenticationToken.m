//
//  BTAuthenticationToken.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 1/13/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTAuthenticationToken.h"
#import "CDAuthenticationToken.h"

@implementation BTAuthenticationToken

- (instancetype)initWithVordelTokenData:(NSDictionary *)tokenData
{
    self = [super init];
    if (self)
    {
        _accessToken = [tokenData valueForKey:@"access_token"];
        _creationTime = [NSDate date];
        _expiresIn = [NSNumber numberWithDouble:[[tokenData valueForKey:@"expires_in"] doubleValue]];
        _refreshToken = [tokenData valueForKey:@"refresh_token"];
        _tokenType = [tokenData valueForKey:@"token_type"];
    }
    return self;
}

- (instancetype)initWithEETokenData:(NSDictionary *)tokenData
{
    self = [super init];
    if (self)
    {
        _accessToken = [tokenData valueForKey:@"accessToken"];
        _creationTime = [NSDate date];
        _expiresIn = [NSNumber numberWithDouble:[[tokenData valueForKey:@"expiresIn"] doubleValue]];
    }
    return self;
}

- (instancetype)initWithCDAuthenticationToken:(CDAuthenticationToken *)authToken
{
    self = [super init];
    if (self)
    {
        _accessToken = authToken.accessToken;
        _creationTime = authToken.creationTime;
        _expiresIn = authToken.expiresIn;
        _refreshToken = authToken.refreshToken;
        _tokenType = authToken.tokenType;
    }
    return self;
}

@end
