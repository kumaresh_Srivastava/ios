//
//  BTSideMenuItem.h
//  CustomSlideMenu
//
//  Created by Lakhpat Meena on 12/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, SMSideMenuStatusType) {
    
    SMSideMenuStatusTypeNone,
    SMSideMenuStatusTypeNumeric,
    SMSideMenuStatusTypeExtendedNumeric
};

@interface BTSideMenuItem : NSObject

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *imageName;
@property (nonatomic, readonly) SMSideMenuStatusType statusType;
@property (nonatomic, readonly) NSInteger openItems;

- (instancetype)initWithTitle:(NSString *)title imageName:(NSString *)imageName andStatusType:(SMSideMenuStatusType)statusType;

- (void)updateNumberOfOpenItems:(NSInteger)openItems;

@end
