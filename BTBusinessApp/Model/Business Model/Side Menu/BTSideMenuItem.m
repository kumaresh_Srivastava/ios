//
//  BTSideMenuItem.m
//  CustomSlideMenu
//
//  Created by Lakhpat Meena on 12/12/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTSideMenuItem.h"

@implementation BTSideMenuItem

- (instancetype)initWithTitle:(NSString *)title imageName:(NSString *)imageName andStatusType:(SMSideMenuStatusType)statusType
{
    self = [super init];
    if (self)
    {
        _title = title;
        _imageName = imageName;
        _statusType = statusType;
        
    }
    return self;
}

- (void)updateNumberOfOpenItems:(NSInteger)openItems
{
    _openItems = openItems;
}

@end
