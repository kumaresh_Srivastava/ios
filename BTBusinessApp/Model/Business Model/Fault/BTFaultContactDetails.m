//
//  BTFaultContactDetails.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 11/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultContactDetails.h"
#import "NSObject+APIResponseCheck.h"
#import "BTOrderSiteContactModel.h"

@implementation BTFaultContactDetails

- (instancetype)initFaultContactDetailWithResponse:(NSDictionary *)faultContactDetailDict
{
    self = [super init];
    if(self)
    {
        _reporterContacts = [[BTOrderSiteContactModel alloc] initWithResponseDictionaryFromFaultContactDetailsAPIResponse:[faultContactDetailDict valueForKey:@"reporterContacts"]];
        _siteContacts = [[BTOrderSiteContactModel alloc] initWithResponseDictionaryFromFaultContactDetailsAPIResponse:[faultContactDetailDict valueForKey:@"siteContacts"]];
        _reporterSameasSite = [[[faultContactDetailDict valueForKey:@"reporterSameasSite"] validAndNotEmptyStringObject] copy];
        _isNetworkFault = [[faultContactDetailDict valueForKey:@"isNetworkFault"] boolValue];
        _pageToRedirect = [[[faultContactDetailDict valueForKey:@"pageToRedirect"] validAndNotEmptyStringObject] copy];
        _moduleId = [[[faultContactDetailDict valueForKey:@"moduleId"] validAndNotEmptyStringObject] copy];
        
    }
    return self;
}

@end
