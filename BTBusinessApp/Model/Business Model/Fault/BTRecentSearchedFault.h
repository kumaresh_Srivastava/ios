//
//  BTRecentSearchedFault.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 19/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDRecentSearchedFault;

@interface BTRecentSearchedFault : NSObject {
    
}

@property (nonatomic, readonly) NSString *faultRef;
@property (nonatomic, readonly) NSDate *lastSearchedDate;
@property (nonatomic, readonly) NSString *faultDescription;
@property (nonatomic, readonly) NSDate *reportedOnDate;
@property (nonatomic, readonly) NSString *faultStatus;
@property (nonatomic, readonly) NSString *colourWithStatus;

- (instancetype)initWithCDRecentSearchedFault:(CDRecentSearchedFault *)recentSearchedFault;

@end
