//
//  BTFaultContactDetails.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 11/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BTOrderSiteContactModel;

@interface BTFaultContactDetails : NSObject

@property (nonatomic, readonly) BTOrderSiteContactModel *reporterContacts;
@property (nonatomic, readonly) BTOrderSiteContactModel *siteContacts;
@property (nonatomic, readonly) NSString *reporterSameasSite;
@property (nonatomic, readonly) BOOL isNetworkFault;
@property (nonatomic, readonly) NSString *pageToRedirect;
@property (nonatomic, readonly) NSString *moduleId;

- (instancetype)initFaultContactDetailWithResponse:(NSDictionary *)faultContactDetailDict;

@end
