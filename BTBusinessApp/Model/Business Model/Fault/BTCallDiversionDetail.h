//
//  BTCallDiversionDetail.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 11/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTCallDiversionDetail : NSObject

@property (nonatomic,readonly) NSString *callDivertFlag;
@property (nonatomic,readonly) BOOL showDivertFlag;
@property (nonatomic,readonly) NSDate *callDivertDate;
@property (nonatomic,readonly) NSString *customerCug;
@property (nonatomic,readonly) NSString *callDivertNumber;

- (void)setDivertFlag:(NSString *)flag;

- (instancetype)initCallDiversionDetailWithResponse:(NSDictionary *)callDiversionDetailDict;
@end
