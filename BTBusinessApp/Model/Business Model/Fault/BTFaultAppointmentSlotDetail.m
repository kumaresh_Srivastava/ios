//
//  BTFaultAppointmentSlotDetail.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 14/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultAppointmentSlotDetail.h"

@implementation BTFaultAppointmentSlotDetail


- (instancetype)initWithFaultAppointmentSlotArray:(NSArray *)faultAppointmentSlotArray priviousSlotDate:(NSDate *)priviousDate andLaterSlotDate:(NSDate *)laterSlotDate
{
    self = [super init];

    if(self)
    {
        _faultAppointmentSlotArray = [faultAppointmentSlotArray copy];
        _previousSlotDate = [priviousDate copy];
        _laterSlotDate = [laterSlotDate copy];
    }


    return self;

}


@end
