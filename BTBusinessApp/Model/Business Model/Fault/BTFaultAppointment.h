//
//  BTFaultAppointment.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 11/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTFaultAppointment : NSObject

@property (nonatomic, readonly) NSDate *dateInfo;
@property (nonatomic, assign) BOOL AMSlot;
@property (nonatomic, assign) BOOL PMSlot;
@property (nonatomic, readonly) NSString *morningDateShift;
@property (nonatomic, readonly) NSString *eveningDateShift;
@property (nonatomic, assign) BOOL isAmSelected;
@property (nonatomic, assign) BOOL isPmSelected;

//Sal : Adding to use with access time slot
//@property (nonatomic, copy) NSString *latestTime;
//@property (nonatomic, copy) NSString *earliestTime;

- (instancetype)initWithResponseDictionaryFromFaultAppointmentsAPIResponse:(NSDictionary *)responseDic;

- (void)updateFaultAppointmentWithCurrentAppointmentDate:(NSDate *)currentDate;

@end
