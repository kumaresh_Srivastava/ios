//
//  BTRecentSearchedFault.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 19/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTRecentSearchedFault.h"
#import "CDRecentSearchedFault.h"

@implementation BTRecentSearchedFault

- (instancetype)initWithCDRecentSearchedFault:(CDRecentSearchedFault *)recentSearchedFault
{
    self = [super init];
    if(self)
    {
        _faultRef = recentSearchedFault.faultRef;
        _faultStatus = recentSearchedFault.faultStatus;
        _faultDescription = recentSearchedFault.faultDescription;
        _reportedOnDate = recentSearchedFault.reportedOnDate;
        _lastSearchedDate = recentSearchedFault.lastSearchedDate;
        _colourWithStatus = recentSearchedFault.colourWithStatus;
        
    }
    return self;
}


@end
