//
//  BTFaultAppointment.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 11/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultAppointment.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"

@implementation BTFaultAppointment



- (instancetype)initWithResponseDictionaryFromFaultAppointmentsAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _dateInfo = [self getDateFromDateString:[[responseDic[@"slotDate"] validAndNotEmptyStringObject] copy]];
        _AMSlot = [responseDic[@"AMSlot"] boolValue];
        _PMSlot = [responseDic[@"PMSlot"] boolValue];
        _morningDateShift = [responseDic[@"slotMorningDateShift"] validAndNotEmptyStringObject];
        _eveningDateShift = [responseDic[@"slotEveningDateShift"] validAndNotEmptyStringObject];
        _isPmSelected = [responseDic[@"isPMSelected"] boolValue];;
        _isAmSelected = [responseDic[@"isAMSelected"] boolValue];;
        
    }
    return self;

}


- (NSDate *)getDateFromDateString:(NSString *)dateString{
    
    NSString *dateFormat = @"dd/MM/yyyy";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSDate *date = nil;
    
    if ([dateFormatter dateFromString:dateString] != nil) {
        
        date = [dateFormatter dateFromString:dateString];
        return date;
    }
    
    return date;
}


- (void)updateFaultAppointmentWithCurrentAppointmentDate:(NSDate *)currentDate
{
    NSInteger hour = [AppManager getHourFromDate:currentDate];
    if (hour<13) {
        _AMSlot = false;
    }
    else
    {
        _PMSlot = false;
    }
}

@end
