//
//  BTFaultSummary.m
//  BTBusinessApp
//
//  Created by Accolite on 27/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultSummary.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"

@implementation BTFaultSummary

- (instancetype)initWithResponseDictionaryFromFaultServiceIDAPIResponse:(NSDictionary *)fautltSummaryDic
{
    self = [super init];
    if(self)
    {
        _productGroup = [[fautltSummaryDic[@"productGroup"] validAndNotEmptyStringObject] copy];
        _productImage = [[fautltSummaryDic[@"productImage"] validAndNotEmptyStringObject] copy];
        _pagingID = [[fautltSummaryDic[@"pagingID"] validAndNotEmptyStringObject] copy];
    }
    return self;
}
@end
