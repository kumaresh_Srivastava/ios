//
//  BTCallDiversionDetail.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 11/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTCallDiversionDetail.h"
#import "AppManager.h"
#import "NSObject+APIResponseCheck.h"

@implementation BTCallDiversionDetail


- (instancetype)initCallDiversionDetailWithResponse:(NSDictionary *)callDiversionDetailDict
{
    self = [super init];
    if(self)
    {

        _callDivertFlag = [[callDiversionDetailDict[@"callDivertFlag"] validAndNotEmptyStringObject] copy];
        _showDivertFlag = [callDiversionDetailDict[@"showDivert"] boolValue];

        _callDivertDate = [AppManager NSDateWithUTCFormatFromNSString:[[callDiversionDetailDict[@"callDivertDate"] validAndNotEmptyStringObject] copy]];

        _customerCug = [[callDiversionDetailDict[@"CustCug"] validAndNotEmptyStringObject] copy];

        _callDivertNumber = [[callDiversionDetailDict[@"calldivertNumber"] validAndNotEmptyStringObject] copy];

    }
    return self;


}

- (void)setDivertFlag:(NSString *)flag{
    
    _callDivertFlag = flag;
    
}

@end
