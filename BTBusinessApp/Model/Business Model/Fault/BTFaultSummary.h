//
//  BTFaultSummary.h
//  BTBusinessApp
//
//  Created by Accolite on 27/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTFaultSummary : NSObject
@property (nonatomic,readonly) NSString *productGroup;
@property (nonatomic,readonly) NSString *productImage;
@property (nonatomic,readonly) NSString *pagingID;

- (instancetype)initWithResponseDictionaryFromFaultServiceIDAPIResponse:(NSDictionary *)fautltSummaryDic;
@end
