//
//  BTFaultMilestoneNode.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 21/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTFaultMilestoneNode : NSObject

@property (nonatomic, readonly) NSString *milestoneName;
@property (nonatomic, readonly) NSDate *milestoneDateTime;
@property (nonatomic, readonly) NSString *milestoneColor;
@property (nonatomic, readonly) NSString *milestoneText;
@property (nonatomic, readonly) NSString *milestoneDivClass;
@property (nonatomic, readonly) BOOL isActive;
@property (nonatomic, readonly) BOOL isFirstActive;
@property (nonatomic, readonly) BOOL isMessageNode;

- (instancetype)initMilestoneNodeWithResponseDic:(NSDictionary *)milestoneNode;
- (instancetype)initMilestoneNodeWithMessageData:(NSDictionary *)messageData andMilestoneNode:(BTFaultMilestoneNode *)nodeData;


@end
