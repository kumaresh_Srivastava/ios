//
//  BLFaultAppointmentDetail.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 11/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTFaultAppointmentDetail : NSObject

@property (nonatomic, readonly) NSArray *faultAppointmentsArray;
@property (nonatomic, readonly) NSArray *acessTimeOptionArray;
@property (nonatomic, readonly) NSDate *presentDateTime;
@property (nonatomic, readonly) NSDate *bookedStartDate;
@property (nonatomic, readonly) NSDate *bookedEndDate;
@property (nonatomic, readonly) NSString *earlierAccessTime;
@property (nonatomic, readonly) NSString *latestAccessTime;
@property (nonatomic, readonly) BOOL isHourAccessPresent;

- (instancetype)initWithFaultAppointmentsArray:(NSArray *)faultAppointmentsArray andAPIResponseDict:(NSDictionary *)responseDict;
- (instancetype)initWithFaultAppointmentDetail:(NSDictionary *)appointmentDetail;

- (void)updateBookedStartDateWithNSDate:(NSDate *)bookedStartDate;
- (void)updateBookedEndDateWithNSDate:(NSDate *)bookedEndDate;
- (void)updateearlierAccessTimeWithString:(NSString *)earlierAccessTimeString;
- (void)updateLatestAccessTimeWithString:(NSString *)latestAccessTimeString;

@end
