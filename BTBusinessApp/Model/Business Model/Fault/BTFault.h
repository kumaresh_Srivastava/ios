//
//  BTFault.h
//  BTBusinessApp
//
//  Created by Accolite on 14/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BTFaultAppointmentDetail;

@interface BTFault : NSObject

@property (nonatomic, readonly) NSString *faultReference;
@property (nonatomic, readonly) NSString *productName;
@property (nonatomic, readonly) NSDate *reportedOnDate;
@property (nonatomic, readonly) NSString *serviceID;
@property (nonatomic, readonly) NSString *status;
@property (nonatomic, readonly) NSString *customerId;
@property (nonatomic, readonly) NSString *assetId;
@property (nonatomic, readonly) NSString *colourwithStatus;
@property (nonatomic, readonly) NSDate *targetFixDate;
@property (nonatomic, readonly) NSString *faultLocation;
@property (nonatomic, readonly) NSString *currentMilestone;
@property (nonatomic, readonly) NSString *currentMilestoneDescription;
@property (nonatomic, readonly) NSString *subStatus;
@property (nonatomic, readonly) NSString *statusOnUI;
@property (nonatomic, readonly) NSString *lineNumber;
@property (nonatomic, readonly) NSInteger btnSecondaryClickID;
@property (nonatomic, readonly) NSInteger btnPrimaryClickID;
@property (nonatomic, readonly) BTFaultAppointmentDetail *faultAppointmentDetail;
@property (nonatomic, readonly,assign) BOOL isVissible;

- (instancetype)initWithResponseDictionaryFromFaultDetailsAPIRespopnse:(NSDictionary *)responseDic;
- (instancetype)initWithResponseDictionaryFromFaultDashBoardAPIResponse:(NSDictionary *)responseDic;
- (instancetype)initWithResponseDictionaryFromFaultSummaryAPIResponse:(NSDictionary *)responseDic;
- (instancetype)initWithResponseDictionaryFromFaultServiceIDAPIResponse:(NSDictionary *)fautltDic;

- (void)updateFaultWithFullDetailsWithData:(NSDictionary *)faultData;
- (void)updateReportedOnDate:(NSDate *)reportedOnDate;
- (void)updateFaultWithAppointmentDetails:(BTFaultAppointmentDetail *)appointmentDetails;

@end


/*{
    code = 600;
    errorMessage = "";
    isSuccess = 1;
    result =     {
        Faults =         (
                          {
                              FaultReference = "1-190258429";
                              ProductName = "PhoneLine Fault";
                              ReportedOn = "2016-08-05T10:50:11Z";
                              ServiceId = 01001830825;
                              Status = "Fault in progress";
                          }
                          );
        PageIndex = 1;
        PageSize = 3;
        TotalSize = 1;
    };
}*/

/*
 
 √"faultId": "1-190259869",
 √"serviceId": "01005978839",
 √"statusOnUI": "Fault in progress",
 "cugId": "h9v1FH6NMrMyAf3AfWsi7Q==",
 √-"colourforStatus": "status-orange",
 √"targetFixDate": "02 Sep 2016",
 √"faultLocation": "No fault found on BT's network",
 √"currentMilestone": "Acknowledged",
 "textonUI": "We're still working on this fault, but you can amend your details if you need to.",
 "btnPrimaryText": "Amend",
 "btnSecondaryText": "Cancel",
 "btnPrimaryHide": false,
 "btnSecondaryHide": false,
 "btnPrimaryClickID": 8,
 "btnSecondaryClickID": 3,
 √"currentMilestoneDesc": "The faults team are looking into your fault and deciding how they can fix it.",
 "nextMilestoneDesc": "\r\n      The faults team will assign an engineer to take care of your fault. Depending on what’s causing the fault, they may need to come out and visit your premises.\r\n    "
 */
