//
//  BLFaultAppointmentDetail.m
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 11/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultAppointmentDetail.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"

@interface BTFaultAppointmentDetail () <NSCopying>

@end

@implementation BTFaultAppointmentDetail
//Need to remove never used
/*- (instancetype)initWithFaultAppointmentsArray:(NSArray *)faultAppointmentsArray presentDate:(NSDate *)date bookedStartDate:(NSDate *)bookedStartDate andBookedEndDate:(NSDate *)bookedEndDate
{
    self = [super init];

    if(self)
    {
        _faultAppointmentsArray = [faultAppointmentsArray copy];
        _presentDateTime = [date copy];
        _bookedStartDate = [bookedStartDate copy];
        _bookedEndDate = [bookedEndDate copy];
        
    }
    return self;
    
}
*/
- (instancetype)initWithFaultAppointmentsArray:(NSArray *)faultAppointmentsArray andAPIResponseDict:(NSDictionary *)responseDict{
    
    self = [super init];
    
    if(self)
    {

        _faultAppointmentsArray = [faultAppointmentsArray copy];
        
        
        _bookedStartDate = [AppManager NSDateWithUTCFormatFromNSString:[[responseDict[@"apptBookedStartDate"] validAndNotEmptyStringObject] copy]];
        _bookedEndDate = [AppManager NSDateWithUTCFormatFromNSString:[[responseDict[@"apptBookedEndDate"] validAndNotEmptyStringObject] copy]];
        _presentDateTime = [AppManager NSDateWithUTCFormatFromNSString:[[responseDict[@"presentDateTime"] validAndNotEmptyStringObject] copy]];
        
        _acessTimeOptionArray = [responseDict valueForKey:@"accessTimesDDLOptns"];
        
        _isHourAccessPresent = [[responseDict valueForKey:@"isHourAccessPresent"] boolValue];
        
        _earlierAccessTime = [responseDict valueForKey:@"earlierAccessTime"];
        _latestAccessTime = [responseDict valueForKey:@"latestAccessTime"];

    }
    
    return self;
    
}

- (id)copyWithZone:(NSZone *)zone
{
    BTFaultAppointmentDetail *appointmentDetail = [[BTFaultAppointmentDetail allocWithZone:zone] init];
    
    if (appointmentDetail)
    {
        appointmentDetail->_faultAppointmentsArray = self->_faultAppointmentsArray;
        appointmentDetail->_acessTimeOptionArray = self->_acessTimeOptionArray;
        appointmentDetail->_presentDateTime = self->_presentDateTime;
        appointmentDetail->_bookedStartDate = self->_bookedStartDate;
        appointmentDetail->_bookedEndDate = self->_bookedEndDate;
        appointmentDetail->_earlierAccessTime = self->_earlierAccessTime;
        appointmentDetail->_latestAccessTime = self->_latestAccessTime;
        appointmentDetail->_isHourAccessPresent = self->_isHourAccessPresent;
    }
    
    return appointmentDetail;
}

- (instancetype)initWithFaultAppointmentDetail:(NSDictionary *)appointmentDetail
{
    self = [super init];
    if (self)
    {
        _bookedStartDate = [AppManager NSDateWithUTCFormatFromNSString:[[[appointmentDetail valueForKey:@"apptBookedStartDate"] validAndNotEmptyStringObject] copy]];
        _bookedEndDate = [AppManager NSDateWithUTCFormatFromNSString:[[[appointmentDetail valueForKey:@"apptBookedEndDate"] validAndNotEmptyStringObject] copy]];
        _isHourAccessPresent = [[appointmentDetail valueForKey:@"isHourAccessPresent"] boolValue];
        _earlierAccessTime = [appointmentDetail valueForKey:@"earlierAccessTime"];
        _latestAccessTime = [appointmentDetail valueForKey:@"latestAccessTime"];
    }
    
    return self;
}

- (void)updateBookedStartDateWithNSDate:(NSDate *)bookedStartDate
{
    _bookedStartDate = bookedStartDate;
}

- (void)updateBookedEndDateWithNSDate:(NSDate *)bookedEndDate
{
    _bookedEndDate = bookedEndDate;
}

- (void)updateearlierAccessTimeWithString:(NSString *)earlierAccessTimeString
{
    _earlierAccessTime = earlierAccessTimeString;
}

- (void)updateLatestAccessTimeWithString:(NSString *)latestAccessTimeString
{
    _latestAccessTime = latestAccessTimeString;
}

@end
