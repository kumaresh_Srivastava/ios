//
//  BTFaultAppointmentSlotDetail.h
//  BTBusinessApp
//
//  Created by VS-Saddam Husain-MacBookPro on 14/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTFaultAppointmentSlotDetail : NSObject

@property (nonatomic, readonly) NSArray *faultAppointmentSlotArray;
@property (nonatomic, readonly) NSDate *previousSlotDate;
@property (nonatomic, readonly) NSDate *laterSlotDate;

- (instancetype)initWithFaultAppointmentSlotArray:(NSArray *)faultAppointmentSlotArray priviousSlotDate:(NSDate *)priviousDate andLaterSlotDate:(NSDate *)laterSlotDate;

@end
