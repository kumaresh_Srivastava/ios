//
//  BTFaultMilestoneNode.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 21/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFaultMilestoneNode.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"

@implementation BTFaultMilestoneNode

- (instancetype)initMilestoneNodeWithResponseDic:(NSDictionary *)milestoneNode
{
    self = [super init];
    if (self) {
        _milestoneName = [[[milestoneNode valueForKey:@"milestoneName"] validAndNotEmptyStringObject] copy];
        _milestoneText = [[[milestoneNode valueForKey:@"milestoneText"] validAndNotEmptyStringObject] copy];
        _milestoneColor = [[[milestoneNode valueForKey:@"milestoneColor"] validAndNotEmptyStringObject] copy];
        _milestoneDateTime = [AppManager NSDateWithUTCFormatFromNSString:[[[milestoneNode valueForKey:@"milestoneDateTime"] validAndNotEmptyStringObject] copy]];
        _milestoneDivClass = [[[milestoneNode valueForKey:@"milestoneDivClass"] validAndNotEmptyStringObject] copy];
        if ([_milestoneDivClass isEqualToString:@"row complete"]) {
            _isActive = false;
        } else {
            _isActive = true;
        }
        if ([_milestoneColor isEqualToString:@"wrap panel callout"]) {
            _isFirstActive = true;
        } else {
            _isFirstActive = false;
        }
        _isMessageNode = false;
    }
    
    return self;
}

- (instancetype)initMilestoneNodeWithMessageData:(NSDictionary *)messageData andMilestoneNode:(BTFaultMilestoneNode *)nodeData
{
    self = [super init];
    if (self) {
        _milestoneName = @"Message from BT Agent";
        _milestoneText = [[[messageData valueForKey:@"faultHistoryContent"] validAndNotEmptyStringObject] copy];
        _milestoneDateTime = [AppManager NSDateWithUTCFormatFromNSString:[[[messageData valueForKey:@"faultHistDateTime"] validAndNotEmptyStringObject] copy]];
        _isFirstActive = false;
        _isActive = nodeData.isActive;
        _isMessageNode = true;
    }
    
    return self;
}


@end
