//
//  BTFault.m
//  BTBusinessApp
//
//  Created by Accolite on 14/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTFault.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"
#import "BTFaultAppointmentDetail.h"

@implementation BTFault

//This init method is related to fault details response with or without BAC
- (instancetype)initWithResponseDictionaryFromFaultDetailsAPIRespopnse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _faultReference = [[responseDic[@"faultId"] validAndNotEmptyStringObject] copy];
        _serviceID = [[responseDic[@"serviceId"] validAndNotEmptyStringObject] copy];
        _status = [[responseDic[@"statusOnUI"] validAndNotEmptyStringObject] copy];
        _colourwithStatus = [[responseDic[@"colourforStatus"] validAndNotEmptyStringObject] copy];
        _targetFixDate = [AppManager NSDateWithMonthNameWithSpaceFormatFromNSString:[[responseDic[@"targetFixDate"] validAndNotEmptyStringObject] copy]];
        _faultLocation = [[responseDic[@"faultLocation"] validAndNotEmptyStringObject] copy];
        _currentMilestone = [[responseDic[@"currentMilestone"] validAndNotEmptyStringObject] copy];
        _currentMilestoneDescription = [[responseDic[@"currentMilestoneDesc"] validAndNotEmptyStringObject] copy];
        _btnPrimaryClickID = [[responseDic valueForKey:@"btnPrimaryClickID"] integerValue];
        _btnSecondaryClickID = [[responseDic valueForKey:@"btnSecondaryClickID"] integerValue];
        
    }
    return self;
}


- (instancetype)initWithResponseDictionaryFromFaultDashBoardAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _faultReference = [[responseDic[@"FaultReference"] validAndNotEmptyStringObject] copy];
        _productName = [[responseDic[@"ProductName"] validAndNotEmptyStringObject] copy];
        _reportedOnDate = [AppManager NSDateWithUTCFormatFromNSString:[[responseDic[@"ReportedOn"] validAndNotEmptyStringObject] copy]];
        _status = [[responseDic[@"Status"] validAndNotEmptyStringObject] copy];
        
    }
    return self;
}

- (instancetype)initWithResponseDictionaryFromFaultSummaryAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _faultReference = [[responseDic[@"faultReference"] validAndNotEmptyStringObject] copy];
        _productName = [[responseDic[@"productName"] validAndNotEmptyStringObject] copy];
        _reportedOnDate = [AppManager NSDateWithUTCFormatFromNSString:[[responseDic[@"reportedOn"] validAndNotEmptyStringObject] copy]];
        _status = [[responseDic[@"statusOnUI"] validAndNotEmptyStringObject] copy];
        _customerId = [[responseDic[@"customerId"] validAndNotEmptyStringObject] copy];
        _serviceID = [[responseDic[@"lineNumber"] validAndNotEmptyStringObject] copy];
        _colourwithStatus = [[responseDic[@"colourwithStatus"] validAndNotEmptyStringObject] copy];
        _assetId = [[responseDic[@"assetId"] validAndNotEmptyStringObject] copy];
        
    }
    return self;
}

- (instancetype)initWithResponseDictionaryFromFaultServiceIDAPIResponse:(NSDictionary *)fautltDic
{
    self = [super init];
    if(self)
    {
        _faultReference = [[fautltDic[@"faultReference"] validAndNotEmptyStringObject] copy];
        _reportedOnDate = [AppManager NSDateWithUTCFormatFromNSString:[[fautltDic[@"reportedOn"] validAndNotEmptyStringObject] copy]];
        _status = [[fautltDic[@"status"] validAndNotEmptyStringObject] copy];
        _subStatus = [[fautltDic[@"subStatus"] validAndNotEmptyStringObject] copy];
        _statusOnUI = [[fautltDic[@"statusOnUI"] validAndNotEmptyStringObject] copy];
        _assetId = [[fautltDic[@"assetId"] validAndNotEmptyStringObject] copy];
        _productName = [[fautltDic[@"productName"] validAndNotEmptyStringObject] copy];
        _colourwithStatus = [[fautltDic[@"colourwithStatus"] validAndNotEmptyStringObject] copy];
        _lineNumber = [[fautltDic[@"lineNumber"] validAndNotEmptyStringObject] copy];
        NSNumber *visibleNumber = fautltDic[@"IsVisible"];
        _isVissible = [visibleNumber boolValue];
    }
    return self;
}

- (void)updateFaultWithFullDetailsWithData:(NSDictionary *)faultData
{
    _faultLocation = [[[faultData valueForKey:@"faultLocation"] validAndNotEmptyStringObject] copy];
    _targetFixDate = [AppManager NSDateWithMonthNameWithSpaceFormatFromNSString:[[[faultData valueForKey:@"targetFixDate"] validAndNotEmptyStringObject] copy]];
    _currentMilestone = [[[faultData valueForKey:@"currentMilestone"] validAndNotEmptyStringObject] copy];
    _currentMilestoneDescription = [[[faultData valueForKey:@"currentMilestoneDesc"] validAndNotEmptyStringObject] copy];
    _btnPrimaryClickID = [[faultData valueForKey:@"btnPrimaryClickID"] integerValue];
    _btnSecondaryClickID = [[faultData valueForKey:@"btnSecondaryClickID"] integerValue];
}

- (void)updateReportedOnDate:(NSDate *)reportedOnDate
{
    _reportedOnDate = reportedOnDate;
}

- (void)updateFaultWithAppointmentDetails:(BTFaultAppointmentDetail *)appointmentDetails
{
    _faultAppointmentDetail = appointmentDetails;
}

@end
