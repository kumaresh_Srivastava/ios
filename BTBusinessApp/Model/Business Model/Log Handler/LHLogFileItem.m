//
//  LHLog.m
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 05/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "LHLogFileItem.h"
#import "LHConstants.h"

@interface LHLogFileItem ()

- (NSArray *)nonPrefixComponentsFromFileNameWithoutExtension;

@end

@implementation LHLogFileItem

- (instancetype)initWithLogFileInfo:(DDLogFileInfo *)logFileInfo
{
    self = [super init];
    if(self)
    {
        _ddLogFileInfo = logFileInfo;


        // (hd) Logic to check what type of logFileType is this.
        NSRange rangeOfCompleteLogPrefix = [_ddLogFileInfo.fileName rangeOfString:kPrefixNameForCompleteLogFiles];
        if(rangeOfCompleteLogPrefix.location != NSNotFound)
        {
            _logFileType = LHLogFileTypeComplete;
        }
        else
        {
            NSRange rangeOfImportantLogPrefix = [_ddLogFileInfo.fileName rangeOfString:kPrefixNameForImportantLogFiles];
            if(rangeOfImportantLogPrefix.location != NSNotFound)
            {
                _logFileType = LHLogFileTypeImportant;
            }
            else
            {
                _logFileType = LHLogFileTypeNone;
            }
        }

    }
    return self;
}

#pragma mark -
#pragma mark Public Methods

- (NSString *)dateComponentFromName
{
    NSString *finalDateString = nil;

    NSArray *middleComponentsFromName = [self nonPrefixComponentsFromFileNameWithoutExtension];
    if (middleComponentsFromName.count == 3 || middleComponentsFromName.count == 4)
    {
        finalDateString = middleComponentsFromName[1];
    }

    return finalDateString;
}

- (NSString *)timeComponentFromName
{
    NSString *finalTimeString = nil;

    NSArray *middleComponentsFromName = [self nonPrefixComponentsFromFileNameWithoutExtension];
    if (middleComponentsFromName.count == 3 || middleComponentsFromName.count == 4)
    {
        finalTimeString = middleComponentsFromName[2];
    }

    return finalTimeString;
}


#pragma mark -
#pragma mark Private Helper Methods

- (NSArray *)nonPrefixComponentsFromFileNameWithoutExtension
{
    NSArray *nonPrefixComponents = nil;

    NSString *filePrefix = nil;

    switch (self.logFileType)
    {
        case LHLogFileTypeImportant:
        {
            filePrefix = kPrefixNameForImportantLogFiles;
            break;
        }

        case LHLogFileTypeComplete:
        {
            filePrefix = kPrefixNameForCompleteLogFiles;
            break;
        }

        default:
            filePrefix = @"";
            break;
    }

    BOOL hasProperPrefix = [self.ddLogFileInfo.fileName hasPrefix:filePrefix];

    if (hasProperPrefix)
    {
        NSArray *componentsSeperatedByDot = [self.ddLogFileInfo.fileName componentsSeparatedByString:@"."];
        if(componentsSeperatedByDot.count >= 1)
        {
            NSString *fileNameWithoutExtension = [componentsSeperatedByDot objectAtIndex:0];

            NSUInteger lengthOfMiddle = fileNameWithoutExtension.length - filePrefix.length;

            // (hd) Date string should have at least 16 characters - " 2013-12-03 17-14"
            if (lengthOfMiddle >= 17) {
                NSRange range = NSMakeRange(filePrefix.length, lengthOfMiddle);

                NSString *middle = [fileNameWithoutExtension substringWithRange:range];
                NSArray *components = [middle componentsSeparatedByString:@" "];

                // (hd) When creating logfile if there is existing file with the same name, we append attemp number at the end.
                // (hd) Thats why here we can have three or four components. For details see createNewLogFile method.
                // (hd) Components:
                //     "", "2013-12-03", "17-14"
                // or
                //     "", "2013-12-03", "17-14", "1"
                if (components.count == 3 || components.count == 4)
                {
                    nonPrefixComponents = components;
                }
            }
        }
    }
    
    return nonPrefixComponents;
}



@end
