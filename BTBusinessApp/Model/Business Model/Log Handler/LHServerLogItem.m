//
//  LHServerLogItem.m
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 10/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "LHServerLogItem.h"

@implementation LHServerLogItem

- (instancetype)initWithJSONDictionary:(NSDictionary *)jsonDic
{
    self = [super init];
    if(self)
    {
        _timeIntervalSinceReferenceDate = [[jsonDic valueForKey:@"timeInterval"] doubleValue];

        _loggedInUsername = [jsonDic valueForKey:@"loggedInUser"];

        _btInstallationID = [jsonDic valueForKey:@"BTInstallationID"];

        _selectedCUGName = [jsonDic valueForKey:@"selectedCUG"];

        _logMessage = [jsonDic valueForKey:@"logMessage"];

        _logFlag = [[jsonDic valueForKey:@"logFlag"] unsignedIntegerValue];

        _fileName = [jsonDic valueForKey:@"fileName"];

        _method = [jsonDic valueForKey:@"method"];

        _fileName = [jsonDic valueForKey:@"fileName"];

        _line = [[jsonDic valueForKey:@"line"] unsignedIntegerValue];

        _threadName = [jsonDic valueForKey:@"threadName"];

        _queueLabel = [jsonDic valueForKey:@"queueLabel"];
        
        _comment = [jsonDic valueForKey:@"comment"];
    }
    return self;
}

@end
