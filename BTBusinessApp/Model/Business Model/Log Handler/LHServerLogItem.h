//
//  LHServerLogItem.h
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 10/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LHServerLogItem : NSObject {

}

@property (nonatomic, readonly) long long logID;
@property (nonatomic, readonly) NSTimeInterval timeIntervalSinceReferenceDate;
@property (nonatomic, readonly) NSString *loggedInUsername;
@property (nonatomic, readonly) NSString *btInstallationID;
@property (nonatomic, readonly) NSString *selectedCUGName;
@property (nonatomic, readonly) NSString *logMessage;
@property (nonatomic, readonly) NSUInteger logFlag;
@property (nonatomic, readonly) NSString *fileName;
@property (nonatomic, readonly) NSString *method;
@property (nonatomic, readonly) NSUInteger line;
@property (nonatomic, readonly) NSString *threadName;
@property (nonatomic, readonly) NSString *queueLabel;
@property (nonatomic, readonly) NSString *comment;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithJSONDictionary:(NSDictionary *)jsonDic NS_DESIGNATED_INITIALIZER;

@end
