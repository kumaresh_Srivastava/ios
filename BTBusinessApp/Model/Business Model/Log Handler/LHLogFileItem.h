//
//  LHLog.h
//  BTLogHandleApp
//
//  Created by Harmandeep Singh on 05/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DDLogFileInfo;


typedef NS_ENUM(NSInteger, LHLogFileType) {

    LHLogFileTypeNone,
    LHLogFileTypeImportant,
    LHLogFileTypeComplete
};

@interface LHLogFileItem : NSObject {

}

@property (nonatomic, readonly) LHLogFileType logFileType;
@property (nonatomic, readonly) DDLogFileInfo *ddLogFileInfo;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithLogFileInfo:(DDLogFileInfo *)logFileInfo NS_DESIGNATED_INITIALIZER;

/* This method returns the date part from the filename. */
- (NSString *)dateComponentFromName;

/* This method returns the time part from the filename. */
- (NSString *)timeComponentFromName;

@end
