//
//  BTPlaceOrderModel.m
//  BTBusinessApp
//
//  Created by Accolite on 09/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTPlaceOrderModel.h"
//#import "ObjectiveC.runtime.h"
#import <objc/runtime.h>

@implementation BTPlaceOrderModel

- (id) init {
    self = [super init];
    if (self) {
        /*self.TrackedOrderKey = [NSString string];
        self.IsLegacy = false;
        self.ItemReference = [NSString string];
        
        //"PONRServicePointStatus": null,
        //"PONRServicePointSubStatus": null,
        self.IsAppointmentChanged = false;
        self.AppointmentDate = [NSString string];
        self.AppointmentSlot = [NSString string];
        self.IsSiteContactChanged = false;
        self.SiteContact = [NSDictionary dictionary];
        self.IsASCChanged = false;
        
        self.AlternateSiteContact= [NSDictionary dictionary];
        //"EngineeringNotes": null,
        //"EngineeringNotes2": null,
        self.ActivationDate = [NSString string];
        self.IsActivationDateChanged = false;
        
        self.MoveInDate = [NSString string];
        // "MoveInSlot": null,
        self.MoveOutDate = [NSString string];
        
        self.UserName = [NSString string];
        self.Appointment= [NSDictionary dictionary];*/

    }
    return self;
}

- (NSDictionary *)dictionaryReflectFromAttributes
{
    @autoreleasepool
    {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        unsigned int count = 0;
        objc_property_t *attributes = class_copyPropertyList([self class], &count);
        objc_property_t property;
        NSString *key, *value;
        
        for (int i = 0; i < count; i++)
        {
            property = attributes[i];
            key = [NSString stringWithUTF8String:property_getName(property)];
            value = [self valueForKey:key];
            [dict setObject:(value ? value : @"") forKey:key];
        }
        
        free(attributes);
        attributes = nil;
        
        return dict;
    }
}

- (NSString *)JSONString
{
    NSDictionary *dict = [self dictionaryReflectFromAttributes];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    if (jsonData.length > 0 && !error)
    {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return jsonString;
    }
    return nil;
}

@end
