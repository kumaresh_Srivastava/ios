//
//  BTProduct.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 22/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTProduct.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"

@implementation BTProduct

- (instancetype)initProductWithResponse:(NSDictionary *)productDic andIndexInAPIResponse:(NSInteger)indexInAPIResponse
{
    self = [super init];
    if(self)
    {
        _orderKey = [[productDic[@"OrderKey"] validAndNotEmptyStringObject] copy];
        _orderItemReference = [[productDic[@"OrderItemReference"] validAndNotEmptyStringObject] copy];
        _productName = [[productDic[@"ProductName"] validAndNotEmptyStringObject] copy];
        _productDescription = [[productDic[@"ProductDescription"] validAndNotEmptyStringObject] copy];
        _productIcon = [[productDic[@"ProductIcon"] validAndNotEmptyStringObject] copy];
        _bundleReference = [[productDic[@"BundleReference"] validAndNotEmptyStringObject] copy];
        _productDuedate = [AppManager NSDateWithMonthNameWithSpaceFormatFromNSString:[[productDic[@"ProductDuedate"] validAndNotEmptyStringObject] copy]];
        _oneOffPrice = [productDic[@"OneOffPrice"] doubleValue];
        _monthlyPrice = [productDic[@"MonthlyPrice"] doubleValue];
        _status = [[productDic[@"Status"] validAndNotEmptyStringObject] copy];
        _expectedDispatchDate = [AppManager NSDateWithUTCFormatFromNSString:[[productDic[@"ExpectedDispatchDate"] validAndNotEmptyStringObject] copy]];
        _engineeringAppointmentDate = [AppManager NSDateWithUTCFormatFromNSString:[[productDic[@"EngineeringAppointmentDate"] validAndNotEmptyStringObject] copy]];
        _engineeringAppointmentSlot = [[productDic[@"EngineeringAppointmentSlot"] validAndNotEmptyStringObject] copy];
        _isBundleProduct = [productDic[@"IsBundleProduct"] boolValue];
        _isCeaseOrCancelled = [productDic[@"isCeaseOrCancelled"] boolValue];
        _indexInAPIResponse = indexInAPIResponse;
        _productType = [productDic[@"ProductType"] intValue];
        _installType = [[productDic[@"InstallType"] validAndNotEmptyStringObject] copy];
       
        
    }
    return self;
}

- (void)updateProductDuedateWithNSDate:(NSDate *)productDueDate
{
    _productDuedate = productDueDate;
}

- (void)updateEngineeringAppointmentDateWithNSDate:(NSDate *)engineeringAppointmentDate
{
    _engineeringAppointmentDate = engineeringAppointmentDate;
}

- (void)updateEngineeringAppointmentSlotWithSlot:(NSString *)engineeringAppointmentSlot
{
    _engineeringAppointmentSlot = engineeringAppointmentSlot;
}

- (void)updateIsProductRelatedToStrategicOrderWith:(NSDictionary *)orderMetaInfo
{
    _isRelatedOrderStrategic = [[orderMetaInfo valueForKey:@"IsStrategic"] boolValue];
}

@end
