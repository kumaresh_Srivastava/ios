//
//  BTProductCharge.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTProductCharge.h"
#import "NSObject+APIResponseCheck.h"

@implementation BTProductCharge

- (instancetype)initProductChargeWithData:(NSDictionary *)productChargeDic
{
    self = [super init];
    if(self)
    {
        _productName = [[[productChargeDic valueForKey:@"ProductName"] validAndNotEmptyStringObject] copy];
        _productCode = [[[productChargeDic valueForKey:@"ProductCode"] validAndNotEmptyStringObject] copy];
        _numberOfInstallations = [[productChargeDic valueForKey:@"NumberOfInstallations"] integerValue];
        _totalCharges = [[productChargeDic valueForKey:@"TotalCharges"] doubleValue];
    }
    return self;
}

@end
