//
//  BTOrderSiteContactModel.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 21/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderSiteContactModel.h"
#import "NSObject+APIResponseCheck.h"

@implementation BTOrderSiteContactModel

- (instancetype) initSiteContactWithResponseDict:(NSDictionary *)siteContactDic
{
    self = [super init];
    if (self) {
        _contactContext = [[[siteContactDic objectForKey:@"ContactContext"] validAndNotEmptyStringObject] copy];
        _contactKey = [[[siteContactDic objectForKey:@"ContactKey"] validAndNotEmptyStringObject] copy];
        _email = [[[siteContactDic objectForKey:@"Email"] validAndNotEmptyStringObject] copy];
        _extension = [[[siteContactDic objectForKey:@"Extension"] validAndNotEmptyStringObject] copy];
        _firstName = [[[siteContactDic objectForKey:@"FirstName"] validAndNotEmptyStringObject] copy];
        _homePhone = [[[siteContactDic objectForKey:@"HomePhone"] validAndNotEmptyStringObject] copy];
        _lastName = [[[siteContactDic objectForKey:@"LastName"] validAndNotEmptyStringObject] copy];
        _mobile = [[[siteContactDic objectForKey:@"Mobile"] validAndNotEmptyStringObject] copy];
        _preferredContact = [[[siteContactDic objectForKey:@"PreferredContact"] validAndNotEmptyStringObject] copy];
        _title = [[[siteContactDic objectForKey:@"Title"] validAndNotEmptyStringObject] copy];
        _workPhone = [[[siteContactDic objectForKey:@"WorkPhone"] validAndNotEmptyStringObject] copy];
        _phone = [[[siteContactDic objectForKey:@"Phone"] validAndNotEmptyStringObject] copy];
    }
    return self;
}

- (instancetype)initWithResponseDictionaryFromSiteContactsAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _contactKey = [[responseDic[@"ContactKey"] validAndNotEmptyStringObject] copy];
        _email = [[responseDic[@"Email"] validAndNotEmptyStringObject] copy];
        _firstName = [[responseDic[@"FirstName"] validAndNotEmptyStringObject] copy];
        _lastName = [[responseDic[@"LastName"] validAndNotEmptyStringObject] copy];
        _title = [[responseDic[@"Title"] validAndNotEmptyStringObject] copy];
        _mobile = [[responseDic[@"Mobile"] validAndNotEmptyStringObject] copy];
        _phone = [[responseDic[@"Phone"] validAndNotEmptyStringObject] copy];
        _formattedContactLine = [[responseDic[@"ForamattedContactLine"] validAndNotEmptyStringObject] copy];
        
    }
    return self;
}

- (instancetype) initSiteContactWithAddedSiteContactDict:(NSDictionary *)siteContactDic
{
    self = [super init];
    if (self) {
        _contactContext = [[[siteContactDic objectForKey:@"ContactContext"] validAndNotEmptyStringObject] copy];
        _contactKey = [[[siteContactDic objectForKey:@"ContactKey"] validAndNotEmptyStringObject] copy];
        _email = [[[siteContactDic objectForKey:@"Email"] validAndNotEmptyStringObject] copy];
        _extension = [[[siteContactDic objectForKey:@"Extension"] validAndNotEmptyStringObject] copy];
        _firstName = [[[siteContactDic objectForKey:@"FirstName"] validAndNotEmptyStringObject] copy];
        _homePhone = [[[siteContactDic objectForKey:@"Phone"] validAndNotEmptyStringObject] copy];
        _lastName = [[[siteContactDic objectForKey:@"LastName"] validAndNotEmptyStringObject] copy];
        _mobile = [[[siteContactDic objectForKey:@"altPhone"] validAndNotEmptyStringObject] copy];
        _preferredContact = [[[siteContactDic objectForKey:@"PreferredContact"] validAndNotEmptyStringObject] copy];
        _title = [[[siteContactDic objectForKey:@"Title"] validAndNotEmptyStringObject] copy];
        _workPhone = [[[siteContactDic objectForKey:@"Phone"] validAndNotEmptyStringObject] copy];
        _phone = [[[siteContactDic objectForKey:@"Phone"] validAndNotEmptyStringObject] copy];
    }
    return self;
}

- (instancetype)initWithResponseDictionaryFromFaultContactDetailsAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _email = [[responseDic[@"EmailId"] validAndNotEmptyStringObject] copy];
        _firstName = [[responseDic[@"FirstName"] validAndNotEmptyStringObject] copy];
        _lastName = [[responseDic[@"LastName"] validAndNotEmptyStringObject] copy];
        _title = [[responseDic[@"Title"] validAndNotEmptyStringObject] copy];
        _mobile = [[responseDic[@"TextNo"] validAndNotEmptyStringObject] copy];
        _phone = [[responseDic[@"VoiceNo"] validAndNotEmptyStringObject] copy];
        _preferredContact = [[responseDic[@"PreferredContact"] validAndNotEmptyStringObject] copy];
        
    }
    return self;
}

//-(void)getsiteContactDicFrom:(NSDictionary *)siteContactDetails
//{
//    
//    if ([siteContactDetails objectForKey:@"ContactContext"]) {
//        self.contactContext = [siteContactDetails objectForKey:@"ContactContext"];
//    }
//    if ([siteContactDetails objectForKey:@"ContactKey"]) {
//        self.contactKey = [siteContactDetails objectForKey:@"ContactKey"];
//    }
//    if ([siteContactDetails objectForKey:@"Email"]) {
//        self.email = [siteContactDetails objectForKey:@"Email"];
//    }
//    if ([siteContactDetails objectForKey:@"Extension"]) {
//        self.extension = [siteContactDetails objectForKey:@"Extension"];
//    }
//    if ([siteContactDetails objectForKey:@"FirstName"]) {
//        self.firstName = [siteContactDetails objectForKey:@"FirstName"];
//    }
//    if ([siteContactDetails objectForKey:@"HomePhone"]) {
//        self.homePhone = [siteContactDetails objectForKey:@"HomePhone"];
//    }
//    if ([siteContactDetails objectForKey:@"LastName"]) {
//        self.lastName = [siteContactDetails objectForKey:@"LastName"];
//    }
//    if ([siteContactDetails objectForKey:@"Mobile"]) {
//        self.mobile = [siteContactDetails objectForKey:@"Mobile"];
//    }
//    if ([siteContactDetails objectForKey:@"PreferredContact"]) {
//        self.preferredContact = [siteContactDetails objectForKey:@"PreferredContact"];
//    }
//    if ([siteContactDetails objectForKey:@"Title"]) {
//        self.title = [siteContactDetails objectForKey:@"Title"];
//    }
//    if ([siteContactDetails objectForKey:@"WorkPhone"]) {
//        self.workPhone = [siteContactDetails objectForKey:@"WorkPhone"];
//    }
//    if ([siteContactDetails objectForKey:@"Phone"]) {
//        self.phone = [siteContactDetails objectForKey:@"Phone"];
//    }
//    
//}

@end
