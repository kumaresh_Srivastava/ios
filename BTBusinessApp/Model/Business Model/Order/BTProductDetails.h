//
//  BTProductDetails.h
//  BTBusinessApp
//
//  Created by Accolite on 10/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTProductDetails : NSObject

@property (nonatomic,readonly) NSString *phoneNumber;
@property (nonatomic,readonly) NSString *networkUserId;
@property (nonatomic,readonly) NSString *primaryEmailAddress;

- (instancetype)initProductDetailsWithResponse:(NSDictionary *)productDic;
@end
