//
//  BTTrackGroupOrderSummaryModel.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 07/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTTrackGroupOrderSummaryModel : NSObject

@property(nonatomic,strong) NSArray *orders;
@property(nonatomic,strong) NSString *orderNumber;
@property(nonatomic,strong) NSString *placedOn;

@end
