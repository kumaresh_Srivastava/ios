//
//  BTPlaceOrderModel.h
//  BTBusinessApp
//
//  Created by Accolite on 09/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTEngineeringAppointmentModel.h"
#import "BTSiteContactModel.h"
#import "BTAlternateSiteContactModel.h"

@interface BTPlaceOrderModel : NSObject


@property (nonatomic,strong)NSString *TrackedOrderKey;
@property (nonatomic,assign)BOOL IsLegacy;
@property (nonatomic,strong)NSString *ItemReference;

//"PONRServicePointStatus": null,
//"PONRServicePointSubStatus": null,
@property (nonatomic,assign)BOOL IsAppointmentChanged;
@property (nonatomic,strong)NSString *AppointmentDate;
@property (nonatomic,strong)NSString *AppointmentSlot;

@property (nonatomic,assign)BOOL IsSiteContactChanged;
@property (nonatomic,strong)BTSiteContactModel *SiteContact;
@property (nonatomic,assign)BOOL IsASCChanged;

@property (nonatomic,strong)BTSiteContactModel *AlternateSiteContact;
//"EngineeringNotes": null,
//"EngineeringNotes2": null,
@property (nonatomic,strong)NSString *ActivationDate;
@property (nonatomic,assign)BOOL IsActivationDateChanged;

@property (nonatomic,strong)NSString *MoveInDate;
// "MoveInSlot": null,
@property (nonatomic,strong)NSString *MoveOutDate;

@property (nonatomic,strong)NSString *UserName;
@property (nonatomic,strong)BTEngineeringAppointmentModel *Appointment;

- (NSString *)JSONString;
/*
 {
 "TrackedOrderKey": "BTDJS999",
 "IsLegacy": false,
 "ItemReference": "BTDJS9991-1",
 "PONRServicePointStatus": null,
 "PONRServicePointSubStatus": null,
 "IsAppointmentChanged": false,
 "AppointmentDate": "0001-01-01T00:00:00",
 "AppointmentSlot": null,
 "IsSiteContactChanged": true,
 "SiteContact": {
 "Title": "Miss",
 "FirstName": "Ad",
 "LastName": "Ad",
 "WorkPhone": "01002892373",
 "Extension": null,
 "Mobile": null,
 "HomePhone": "01002892373",
 "Email": "shruti.anand@bt.com",
 "ContactKey": "820ZDJK894",
 "ContactContext": null,
 "PreferredContact": null
 },
 "IsASCChanged": false,
 "AlternateSiteContact": null,
 "EngineeringNotes": null,
 "EngineeringNotes2": null,
 "ActivationDate": "0001-01-01T00:00:00",
 "IsActivationDateChanged": false,
 "MoveInDate": "0001-01-01T00:00:00",
 "MoveInSlot": null,
 "MoveOutDate": "0001-01-01T00:00:00",
 "UserName": "deepikaname",
 "Appointment": {
 "AppointmentDate": null,
 "AppointmentSlot": null,
 "AddressKey": null,
 "DBIDValue": null,
 "AppointmentBook": null
 }
 }
 */

@end
