//
//  BTTrackOrderMilestonesModel.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 21/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTTrackOrderMilestonesModel.h"
#import "BTOrderMilestoneNodeModel.h"

@implementation BTTrackOrderMilestonesModel

- (id) init {
    self = [super init];
    if (self) {
        self.milestoneNodes = [NSMutableArray array];
        self.dispatchDetails = [NSMutableArray array];
        self.siteContact = [BTOrderSiteContactModel new];
        self.altSiteContact = [BTOrderSiteContactModel new];
    }
    return self;
}

-(void)getOrderMilestoneModelFrom:(NSDictionary *)milestoneDetails
{
    NSArray *milestoneNodesArray = [milestoneDetails objectForKey:@"Nodes"];
    NSMutableArray *arrayToAddNodes = [[NSMutableArray alloc] init];
    for (NSDictionary *milestoneNode in milestoneNodesArray) {
        BTOrderMilestoneNodeModel *nodeData = [[BTOrderMilestoneNodeModel alloc] initMilestoneNodeWithResponseDic:milestoneNode];
        if (nodeData.isActivationAmendSource) {
            //[self setActivationDate:nodeData.date];
            self.isActivationDateAvailable = YES;
        }
        if (nodeData.isAppointmentAmendSource) {
            //[self setEngineerAppointmentDate:nodeData.date];
            self.isEngineerAppointmentDateAvailable = YES;
        }
        if (nodeData.isDispatchDetailsAvailable) {

            [self.dispatchDetails addObjectsFromArray:[[BTOrderDispatchDetailsModel alloc] getDispatchDetailsFrom:milestoneNode]];
        }
        if (nodeData.isSiteContactAvailable) {

            //[self.siteContact getSiteContactDetailsFrom:[milestoneNode objectForKey:@"SiteContact"]];
            self.isSiteContactAvailable = YES;
        }
        if (nodeData.isAltSiteContactAvailable) {
            
            //[self.altSiteContact getSiteContactDetailsFrom:[milestoneNode objectForKey:@"AlternateSiteContact"]];
            self.isAltSiteContactAvailable = YES;
        }
        [arrayToAddNodes addObject:nodeData];
    }
    self.milestoneNodes = arrayToAddNodes;
}

-(NSString *)getSiteContact
{
    NSMutableString *siteContact = [NSMutableString string];
    [siteContact appendFormat:@"%@ %@ %@", self.siteContact.title, self.siteContact.firstName, self.siteContact.lastName];
    if (![self.siteContact.workPhone isEqualToString:@""]) {
        [siteContact appendFormat:@"\nWork: %@", self.siteContact.workPhone];
    }
    if (![self.siteContact.mobile isEqualToString:@""]) {
        [siteContact appendFormat:@"\nMobile: %@", self.siteContact.mobile];
    }
    if (![self.siteContact.homePhone isEqualToString:@""]) {
        [siteContact appendFormat:@"\nHome: %@", self.siteContact.homePhone];
    }
    if (![self.siteContact.email isEqualToString:@""]) {
        [siteContact appendFormat:@"\n%@", self.siteContact.email];
    }
    return siteContact;
}

-(NSString *)getAltSiteContact
{
    NSMutableString *altSiteContact = [NSMutableString string];
    [altSiteContact appendFormat:@"%@ %@ %@", self.altSiteContact.title, self.altSiteContact.firstName, self.altSiteContact.lastName];
    if (![self.altSiteContact.workPhone isEqualToString:@""]) {
        [altSiteContact appendFormat:@"\nWork: %@", self.altSiteContact.workPhone];
    }
    if (![self.altSiteContact.mobile isEqualToString:@""]) {
        [altSiteContact appendFormat:@"\nMobile: %@", self.altSiteContact.mobile];
    }
    if (![self.altSiteContact.homePhone isEqualToString:@""]) {
        [altSiteContact appendFormat:@"\nHome: %@", self.altSiteContact.homePhone];
    }
    if (![self.altSiteContact.email isEqualToString:@""]) {
        [altSiteContact appendFormat:@"\n%@", self.altSiteContact.email];
    }
    return altSiteContact;
}

-(NSString *)getDispatchDetails
{
    NSMutableString *dispatchDetails = [NSMutableString string];
    for (BTOrderDispatchDetailsModel *dispatchDetail in self.dispatchDetails) {
        if (dispatchDetail == [self.dispatchDetails lastObject]) {
            [dispatchDetails appendFormat:@"We sent the %@ to your business on %@. Your tracking reference number is %@", dispatchDetail.equipmentNames, dispatchDetail.dispatchDate, dispatchDetail.trackingReferenceNumber];
            
        } else {
        [dispatchDetails appendFormat:@"We sent the %@ to your business on %@. Your tracking reference number is %@\n", dispatchDetail.equipmentNames, dispatchDetail.dispatchDate, dispatchDetail.trackingReferenceNumber];
        }
    }
    return dispatchDetails;
}

@end
