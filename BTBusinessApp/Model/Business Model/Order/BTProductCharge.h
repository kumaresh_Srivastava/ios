//
//  BTProductCharge.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 04/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTProductCharge : NSObject

@property (nonatomic, readonly) NSString *productName;
@property (nonatomic, readonly) NSString *productCode;
@property (nonatomic, readonly) NSInteger numberOfInstallations;
@property (nonatomic, readonly) double totalCharges;

- (instancetype)initProductChargeWithData:(NSDictionary *)productChargeDic;

@end
