//
//  BTEngineerAppointmentSlotModel.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 11/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTEngineerAppointmentSlotModel : NSObject

@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *dateInfo;
@property (nonatomic, strong) NSString *dayOfWeek;
@property (nonatomic) BOOL hasAM;
@property (nonatomic) BOOL hasPM;
@property (nonatomic) BOOL isAMSelected;
@property (nonatomic) BOOL isPMSelected;
@property (nonatomic) BOOL isInitialSlot;
@property (nonatomic) BOOL isLastSlot;

- (void)updateAppointmentSlotDataWith:(NSDictionary *)slotData isInitialSlot:(BOOL)isInitialSlot isLastSlot:(BOOL)isLastSlot;

@end
