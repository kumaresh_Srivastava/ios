//
//  BTPricingDetails.h
//  BTBusinessApp
//
//  Created by Accolite on 09/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTPricingDetails : NSObject

@property (nonatomic, readonly) NSArray *orderItems;
@property (nonatomic,readonly) NSDictionary *parentOrderItem;
@property (nonatomic, readonly) double totalOneOffPrice;
@property (nonatomic, readonly) double totalMonthlyPrice;
@property (nonatomic, readonly) double totalQuarterlyPrice;
@property (nonatomic, readonly) double totalHttpCharge;
@property (nonatomic, readonly) double totalTerminationCharge;
@property (strong,nonatomic) NSString * regularChargesView;
@property(nonatomic, readonly) int journeyType;

- (instancetype)initPricingDetailsWithResponse:(NSDictionary *)pricingDetailsDic;
@end
