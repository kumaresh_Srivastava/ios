//
//  BTContact.h
//  BTBusinessApp
//
//  Created by Accolite on 02/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTContact : NSObject

@property (nonatomic, readonly) NSString *contactKey;
@property (nonatomic, readonly) NSString *email;
@property (nonatomic, readonly) NSString *firstName;
@property (nonatomic, readonly) NSString *lastName;
@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *mobile;
@property (nonatomic, readonly) NSString *phone;
@property (nonatomic, readonly) NSString *formattedContactLine;

- (instancetype)initWithResponseDictionaryFromSiteContactsAPIResponse:(NSDictionary *)responseDic;
@end

/*
 {
 "ContactKey": "820ZDHS631",
 "Email": "vishnu.pandi@bt.com",
 "FirstName": "Yamini",
 "LastName": "Janjanam",
 "Title": "Miss",
 "Mobile": "07154564544",
 "Phone": "07154564544",
 "ForamattedContactLine": "Miss Yamini Janjanam - 07154564544 vishnu.pandi@bt.com"
 }
*/
