//
//  BTTrackOrderDashBoardModel.h
//  BTBusinessApp
//
//  Created by Accolite on 01/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTTrackOrderDashBoardModel : NSObject

@property(nonatomic,strong)NSArray *Orders;
@property(nonatomic,strong)NSNumber *PageIndex;
@property(nonatomic,strong)NSNumber *PageSize;
@property(nonatomic,strong)NSNumber *TotalSize;

@end


/*

 {
    Orders =     
    (
        {
         "<CompleteOrderStatus>k__BackingField" = "In Progress";
         "<CompletionDate>k__BackingField" = "2016-07-01T14:04:12.5820826+01:00";
         "<Description>k__BackingField" = "";
         "<LongDescription>k__BackingField" = "BT Business Broadband related order";
         "<OrderDate>k__BackingField" = "2015-01-23T13:22:28Z";
         "<OrderIdentifier>k__BackingField" = BTCSQ367;
         "<OrderStatus>k__BackingField" = 0;
         "<PlacedOnDate>k__BackingField" = "2015-01-23T13:22:28Z";
        },
        {
         "<CompleteOrderStatus>k__BackingField" = "In Progress";
         "<CompletionDate>k__BackingField" = "2015-10-13T00:00:00+01:00";
         "<Description>k__BackingField" = "";
         "<LongDescription>k__BackingField" = "BT Business Broadband related order";
         "<OrderDate>k__BackingField" = "2015-10-06T08:13:17+01:00";
         "<OrderIdentifier>k__BackingField" = BTCZP279;
         "<OrderStatus>k__BackingField" = 0;
         "<PlacedOnDate>k__BackingField" = "2015-10-06T08:13:17+01:00";
        }
    );
    PageIndex = 1;
    PageSize = 4;
    TotalSize = 2;
 }

*/