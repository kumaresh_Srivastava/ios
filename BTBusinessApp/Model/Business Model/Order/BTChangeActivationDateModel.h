//
//  BTChangeActivationDateModel.h
//  BTBusinessApp
//
//  Created by Accolite on 08/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTChangeActivationDateModel : NSObject

@property (nonatomic, strong)NSArray *holidaysArray;

@end
