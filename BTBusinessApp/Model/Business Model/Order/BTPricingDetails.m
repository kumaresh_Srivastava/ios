//
//  BTPricingDetails.m
//  BTBusinessApp
//
//  Created by Accolite on 09/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTPricingDetails.h"

@implementation BTPricingDetails
- (instancetype)initPricingDetailsWithResponse:(NSDictionary *)pricingDetailsDic
{
    self = [super init];
    if(self)
    {
        
        if (pricingDetailsDic[@"OrderItems"]) {
            
            _orderItems = [pricingDetailsDic[@"OrderItems"] copy];
        }
        
        if (pricingDetailsDic[@"ParentOrderItem"]) {
            
            _parentOrderItem = [pricingDetailsDic[@"ParentOrderItem"] copy];
        }
        
        _totalOneOffPrice = [pricingDetailsDic[@"TotalOneOffPrice"] doubleValue];
        _totalMonthlyPrice = [pricingDetailsDic[@"TotalMonthlyPrice"] doubleValue];
        _totalQuarterlyPrice = [pricingDetailsDic[@"TotalQuarterlyPrice"] doubleValue];
        _totalHttpCharge = [pricingDetailsDic[@"TotalHttCharge"] doubleValue];
        _totalTerminationCharge = [pricingDetailsDic[@"TotalTerminationCharge"] doubleValue];
        
        _regularChargesView = [pricingDetailsDic valueForKey:@"RegularChargesView"];
        
        _journeyType = [[pricingDetailsDic valueForKey:@"JourneyType"] intValue];
    }
    return self;
}

@end
