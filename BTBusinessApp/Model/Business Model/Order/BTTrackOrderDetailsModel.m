//
//  BTTrackOrderDetailsModel.m
//  BTBusinessApp
//
//  Created by Accolite on 12/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTTrackOrderDetailsModel.h"

@implementation BTTrackOrderDetailsModel
- (id) init {
    self = [super init];
    if (self) {
        self.priceOrderItems = [NSMutableArray array];
        self.groupPriceOrderItems = [NSMutableArray array];
        self.allOrderItems = [NSMutableArray array];
    }
    return self;
}

- (NSArray *)getGroupedOrderItems {
    for (NSDictionary *orderitemDict in self.OrderItems) {
        NSMutableArray *newGroupOrderDetails = [NSMutableArray array];
        BOOL isNewDictAvailable = YES;
        for (NSMutableArray *groupOrderDetails in self.priceOrderItems) {
            NSDictionary *groupOrderDict;
            if (groupOrderDetails.count > 0) {
            
                groupOrderDict = [groupOrderDetails objectAtIndex:0];
            }
            if (groupOrderDict != nil) {
                //ProductGroupName
                NSString *groupOrderDictProductName = [groupOrderDict objectForKey:@"ProductGroupName"];
                NSString *orderItemDictProductName = [orderitemDict objectForKey:@"ProductGroupName"];
                if ([groupOrderDictProductName isEqualToString:orderItemDictProductName]) {
                    isNewDictAvailable = NO;
                    [groupOrderDetails addObject:orderitemDict];
                }
            }
        }
        if ((self.priceOrderItems.count == 0) || isNewDictAvailable) {
            [newGroupOrderDetails addObject:orderitemDict];
            [self.priceOrderItems addObject:newGroupOrderDetails];
        }
        
    }

    return self.priceOrderItems;
}

- (NSInteger)getNumberOfRowsFromOrderItems {
    NSInteger rows = 0;
    for (NSArray *groupArray in self.priceOrderItems) {
        rows += groupArray.count;
    }
    return rows;
}

@end
