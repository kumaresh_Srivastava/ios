//
//  BTOrderDispatchDetailsModel.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 21/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTOrderDispatchDetailsModel : NSObject

@property (nonatomic, strong) NSString *dispatchDate;
@property (nonatomic, strong) NSString *equipmentNames;
@property (nonatomic, strong) NSString *trackingReferenceNumber;
@property (nonatomic, strong) NSString *trackingReferenceUrl;

- (NSMutableArray *)getDispatchDetailsFrom:(NSDictionary *)dispatchDetails;

@end
