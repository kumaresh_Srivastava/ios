//
//  BTTrackOrderSummaryModel.m
//  BTBusinessApp
//
//  Created by Accolite on 30/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTTrackOrderSummaryModel.h"

@implementation BTTrackOrderSummaryModel

- (NSDictionary *)getProductDictionaryFromProductsWithIndex:(NSUInteger)index {
    return [self.Products objectAtIndex:index];
}

@end
