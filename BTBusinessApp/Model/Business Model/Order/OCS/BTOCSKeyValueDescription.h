//
//  BTOCSKeyValueDescription.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 12/11/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BTOCSKeyValueDescription : NSObject

@property (strong, nonatomic) NSString *keyString;
@property (strong, nonatomic) NSString *valueString;
@property (strong, nonatomic) NSString *descriptionString;

+ (BTOCSKeyValueDescription*)keyValueDescriptionWithDict:(NSDictionary*)dict;


@end

NS_ASSUME_NONNULL_END
