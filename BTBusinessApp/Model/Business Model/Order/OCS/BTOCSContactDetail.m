//
//  BTOCSContactDetail.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 03/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSContactDetail.h"
#import "NSObject+APIResponseCheck.h"

@implementation BTOCSContactDetail

+ (BTOCSContactDetail *)contactDetailWithDict:(NSDictionary *)dict
{
    BTOCSContactDetail *contact = [[BTOCSContactDetail alloc] init];
    contact.email = [[dict objectForKey:@"ContactEmail"] validAndNotEmptyStringObject];
    contact.mobile = [[dict objectForKey:@"ContactMobile"] validAndNotEmptyStringObject];
    contact.name = [[dict objectForKey:@"ContactName"] validAndNotEmptyStringObject];
    contact.telephone = [[dict objectForKey:@"ContactTelephone"] validAndNotEmptyStringObject];
    contact.type = [[dict objectForKey:@"ContactType"] validAndNotEmptyStringObject];
    return contact;
}

@end
