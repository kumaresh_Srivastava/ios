//
//  BTOCSEnquiry.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 02/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSEnquiry.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"

@implementation BTOCSEnquiry

+ (BTOCSEnquiry *)enquiryWithDict:(NSDictionary *)enquiryDict
{
    BTOCSEnquiry *enquiry = [[BTOCSEnquiry alloc] init];
    enquiry.enquiryId = [[enquiryDict valueForKey:@"EnquiryID"] integerValue];
    enquiry.enquiryCategory = [[enquiryDict objectForKey:@"EnquiryCategory"] validAndNotEmptyStringObject];
    enquiry.enquiryDescription = [[enquiryDict objectForKey:@"EnquiryDescription"] validAndNotEmptyStringObject];
    enquiry.enquiryStatus = [[enquiryDict objectForKey:@"EnquiryStatus"] validAndNotEmptyStringObject];
    enquiry.enquiryDate = [AppManager dateFromString:[[enquiryDict objectForKey:@"EnquiryDate"] validAndNotEmptyStringObject]];
    enquiry.enquiryClosureDate = [AppManager dateFromString:[[enquiryDict objectForKey:@"EnquiryClosureDate"] validAndNotEmptyStringObject]];
    
    // extract notes info
    NSObject *notesInfo = [enquiryDict valueForKeyPath:@"EnquiryNotes.EnquiryNote"];
    NSMutableArray *notesArray = [NSMutableArray new];
    if ([notesInfo isKindOfClass:[NSArray class]]) {
        for (NSString *note in (NSArray*)notesInfo) {
            [notesArray addObject:note];
        }
    } else if ([notesInfo isKindOfClass:[NSString class]]) {
        [notesArray addObject:(NSString*)notesInfo];
    }
    enquiry.enquiryNotes = [NSArray arrayWithArray:notesArray];
    
    return enquiry;
}

@end
