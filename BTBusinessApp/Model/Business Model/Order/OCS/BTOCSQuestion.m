//
//  BTOCSQuestion.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 16/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSQuestion.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"
#import "BTOCSQuestionResponseOptions.h"

@implementation BTOCSQuestion

+ (BTOCSQuestion *)questionWithDict:(NSDictionary *)questionInfo
{
    BTOCSQuestion *question = [[BTOCSQuestion alloc] init];
    question.questionID = [[questionInfo valueForKey:@"QuestionID"] integerValue];
    question.questionParentID = [[questionInfo valueForKey:@"QuestionParentID"] integerValue];
    question.questionReadOnlyFlag = [[questionInfo valueForKey:@"QuestionReadOnlyFlag"] boolValue];

    question.questionName = [[questionInfo objectForKey:@"QuestionName"] validAndNotEmptyStringObject];
    question.questionDescription = [[questionInfo objectForKey:@"QuestionDescription"] validAndNotEmptyStringObject];
    question.questionDataType = [[questionInfo objectForKey:@"QuestionDataType"] validAndNotEmptyStringObject];
    question.questionResponse = [[questionInfo objectForKey:@"QuestionResponse"] validAndNotEmptyStringObject];
    question.questionResponseBy = [[questionInfo objectForKey:@"QuestionResponseBy"] validAndNotEmptyStringObject];

    question.questionResponseDate = [AppManager dateFromString:[[questionInfo objectForKey:@"QuestionResponseDate"] validAndNotEmptyStringObject]];
    
    NSObject *optionsInfo = [questionInfo valueForKeyPath:@"QuestionResponseOptions.QuestionResponseOption"];
    NSMutableArray *options = [NSMutableArray new];
    if (optionsInfo && ![optionsInfo isKindOfClass:[NSNull class]]) {
        for (NSDictionary *dict in [optionsInfo safeArrayFromJSONObject]) {
            
            [options addObject:[BTOCSQuestionResponseOptions optionWithDict:dict]];
        }
    }
    question.questionResponseOptions = [NSArray arrayWithArray:options];

    return question;
}

@end
