//
//  BTOCSKeyValueDescription.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 12/11/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSKeyValueDescription.h"
#import "NSObject+APIResponseCheck.h"

@implementation BTOCSKeyValueDescription

+ (BTOCSKeyValueDescription *)keyValueDescriptionWithDict:(NSDictionary *)dict
{
    BTOCSKeyValueDescription *kvd = [[BTOCSKeyValueDescription alloc] init];
    kvd.keyString = [[dict objectForKey:@"Key"] validAndNotEmptyStringObject];
    kvd.valueString = [[dict objectForKey:@"Value"] validAndNotEmptyStringObject];
    kvd.descriptionString = [[dict objectForKey:@"Description"] validAndNotEmptyStringObject];
    return kvd;
}

@end
