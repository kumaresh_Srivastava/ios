//
//  BTOCSMilestone.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 01/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BTOCSMilestone : NSObject

@property (nonatomic, strong) NSDate *completionDate;
@property (nonatomic, strong) NSDate *dueDate;
@property (nonatomic) NSInteger milestoneId;
@property (nonatomic, strong) NSString *information;
@property (nonatomic, strong) NSDate *lastUpdatedDate;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSDate *startDate;

+ (BTOCSMilestone*)milestoneWithDict:(NSDictionary*)dict;

@end

NS_ASSUME_NONNULL_END
