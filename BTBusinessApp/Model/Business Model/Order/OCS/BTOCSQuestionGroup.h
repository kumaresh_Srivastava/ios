//
//  BTOCSQuestionGroups.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 16/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BTOCSQuestion;

NS_ASSUME_NONNULL_BEGIN

@interface BTOCSQuestionGroup : NSObject

@property (nonatomic) NSInteger questionGroupID;
@property (nonatomic) NSInteger questionGroupActivityID;
@property (nonatomic, strong) NSString *questionGroupName;
@property (nonatomic, strong) NSDate *questionGroupRequestedDate;
@property (nonatomic, strong) NSDate *questionGroupResponseDate;
@property (nonatomic) BOOL questionGroupCompleted;
@property (nonatomic, strong) NSArray<BTOCSQuestion*> *questions;

+ (BTOCSQuestionGroup*)questionGroupWithDict:(NSDictionary*)dict;

@end

NS_ASSUME_NONNULL_END
