//
//  BTOCSSite.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 01/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class BTOCSMilestone;

@interface BTOCSSite : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic) NSInteger siteID;
@property (nonatomic, strong) NSArray<BTOCSMilestone*> *milestones;

+(BTOCSSite*)siteWithDict:(NSDictionary*)dict;

@end

NS_ASSUME_NONNULL_END
