//
//  BTOCSContactDetail.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 03/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BTOCSContactDetail : NSObject

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *telephone;
@property (nonatomic, strong) NSString *type;

+ (BTOCSContactDetail*)contactDetailWithDict:(NSDictionary*)dict;

@end
