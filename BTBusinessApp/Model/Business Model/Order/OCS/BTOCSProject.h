//
//  BTOCSProject.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BTOCSOrder;
@class BTOCSSite;
@class BTOCSEnquiry;
@class BTOCSContactDetail;
@class BTOCSKeyValueDescription;
@class BTOCSNote;
@class BTOCSQuestionGroup;

@interface BTOCSProject : NSObject

// base elements (getProjectsByCUG)
@property (strong, nonatomic) NSString *projectRef;
@property (strong, nonatomic) NSDate *projectStartDate;
@property (strong, nonatomic) NSDate *projectEndDate;
@property (strong, nonatomic) NSString *productName;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *subStatus;
@property (strong, nonatomic) NSArray<BTOCSOrder*> *orders;

// project summary elements (getProjectSummary)
@property (strong, nonatomic) NSString *customerName;
@property (strong, nonatomic) NSDate *targetDeliveryDate;
@property (strong, nonatomic) NSDate *dateClosed;
@property (strong, nonatomic) NSString *productService;
@property (strong, nonatomic) NSString *currentStatus;
@property (nonatomic) NSInteger intGroupId;
@property (strong, nonatomic) NSArray<NSString*> *enquiryCategories;
@property (strong, nonatomic) NSArray<NSString*> *contactEmails;
@property (strong, nonatomic) NSString *contactMessage;
@property (strong, nonatomic) NSArray<BTOCSSite*> *sites;
@property (nonatomic) BOOL hideContactPanel;

@property (nonatomic) BOOL openQuestionsFlag;
@property (nonatomic) BOOL importantInfoFlag;

// project enquiries elements (getProjectEnquiries)
@property (strong, nonatomic) NSArray<BTOCSEnquiry*> *enquiries;

// project details elements (getProjectDetail)
@property (strong, nonatomic) NSArray<BTOCSContactDetail*> *contactDetails;
@property (strong, nonatomic) NSArray<BTOCSKeyValueDescription*> *keyValueDescriptions;

// project notes element (getProjectotes)
@property (strong, nonatomic) NSArray<BTOCSNote*> *projectNotes;

// project questions element (getProjectQuestions)
@property (strong, nonatomic) NSArray<BTOCSQuestionGroup*> *questionGroups;

// initialisers and api response handlers

- (instancetype)initWithProjectRef:(NSString*)projectRef;
- (instancetype)initWithAPIResponse:(NSDictionary*)responseObject;

- (void)updateWithProjectSummaryResponse:(NSDictionary*)projectSummary;
- (void)updateWithProjectEnquiriesResponse:(NSDictionary*)projectEnquries;
- (void)updateWithProjectDetailsResponse:(NSDictionary*)projectDetail;
- (void)updateWithProjectNotesResponse:(NSDictionary*)projectNotes;
- (void)updateWithProjectQuestionsResponse:(NSDictionary*)projectQuestions;

@end
