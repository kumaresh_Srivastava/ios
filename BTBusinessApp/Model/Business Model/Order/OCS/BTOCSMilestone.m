//
//  BTOCSMilestone.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 01/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSMilestone.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"

@implementation BTOCSMilestone

+ (BTOCSMilestone *)milestoneWithDict:(NSDictionary *)dict
{
    BTOCSMilestone *milestone = [[BTOCSMilestone alloc] init];
    
    milestone.completionDate = [AppManager dateFromString:[[dict objectForKey:@"CompletionDate"] validAndNotEmptyStringObject]];
    milestone.dueDate = [AppManager dateFromString:[[dict objectForKey:@"DueDate"] validAndNotEmptyStringObject]];
    milestone.lastUpdatedDate = [AppManager dateFromString:[[dict objectForKey:@"LastUpdatedDate"] validAndNotEmptyStringObject]];
    milestone.startDate = [AppManager dateFromString:[[dict objectForKey:@"StartDate"] validAndNotEmptyStringObject]];
    
    milestone.name = [[dict objectForKey:@"Name"] validAndNotEmptyStringObject];
    milestone.information = [[dict objectForKey:@"Information"] validAndNotEmptyStringObject];
    milestone.milestoneId = [[dict valueForKey:@"ID"] integerValue];
    
    return milestone;
}

@end
