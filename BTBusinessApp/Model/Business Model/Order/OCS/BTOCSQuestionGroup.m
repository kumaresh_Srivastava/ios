//
//  BTOCSQuestionGroups.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 16/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSQuestionGroup.h"
#import "BTOCSQuestion.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"

@implementation BTOCSQuestionGroup

+ (BTOCSQuestionGroup *)questionGroupWithDict:(NSDictionary *)dict
{
    BTOCSQuestionGroup *questionGroup = [[BTOCSQuestionGroup alloc] init];
    questionGroup.questionGroupID = [[dict valueForKey:@"QuestionGroupID"] integerValue];
    questionGroup.questionGroupActivityID = [[dict valueForKey:@"QuestionGroupActivityID"] integerValue];
    questionGroup.questionGroupName = [[dict objectForKey:@"QuestionGroupName"] validAndNotEmptyStringObject];
    questionGroup.questionGroupRequestedDate = [AppManager dateFromString:[[dict objectForKey:@"QuestionGroupRequestedDate"] validAndNotEmptyStringObject]];
    questionGroup.questionGroupResponseDate = [AppManager dateFromString:[[dict objectForKey:@"QuestionGroupResponseDate"] validAndNotEmptyStringObject]];
    questionGroup.questionGroupCompleted = [[dict valueForKey:@"QuestionGroupCompleted"] boolValue];

    NSObject *questionsInfo = [dict valueForKeyPath:@"Questions.Question"];
    NSMutableArray *questions = [NSMutableArray new];
    for (NSDictionary *info in [questionsInfo safeArrayFromJSONObject]) {
        [questions addObject:[BTOCSQuestion questionWithDict:info]];
    }
    questionGroup.questions = [NSArray arrayWithArray:questions];
    
    return questionGroup;
}

@end
