//
//  BTOCSSite.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 01/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSSite.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"
#import "BTOCSMilestone.h"

@implementation BTOCSSite

+ (BTOCSSite *)siteWithDict:(NSDictionary *)dict
{
    BTOCSSite *site = [[BTOCSSite alloc] init];
    site.name = [[dict objectForKey:@"Name"] validAndNotEmptyStringObject];
    site.siteID = [[dict valueForKey:@"ID"] integerValue];
    
    // extract milestones info
    NSObject *milestonesInfo = [dict valueForKeyPath:@"Milestones.Milestone"];
    NSMutableArray *milestonesArray = [NSMutableArray new];
    if ([milestonesInfo isKindOfClass:[NSArray class]]) {
        for (NSDictionary *milestoneInfo in (NSArray*)milestonesInfo) {
            [milestonesArray addObject:[BTOCSMilestone milestoneWithDict:milestoneInfo]];
        }
    } else if ([milestonesInfo isKindOfClass:[NSDictionary class]]) {
        [milestonesArray addObject:[BTOCSMilestone milestoneWithDict:(NSDictionary*)milestonesInfo]];
    }
    site.milestones = [NSArray arrayWithArray:milestonesArray];
    
    return site;
}

@end
