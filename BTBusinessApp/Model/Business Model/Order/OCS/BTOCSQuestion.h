//
//  BTOCSQuestion.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 16/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class BTOCSQuestionResponseOptions;

@interface BTOCSQuestion : NSObject

@property (nonatomic) NSInteger questionID;
@property (nonatomic) NSInteger questionParentID;
@property (nonatomic, strong) NSString *questionName;
@property (nonatomic, strong) NSString *questionDescription;
@property (nonatomic, strong) NSString *questionDataType;
@property (nonatomic, strong) NSString *questionResponse;
@property (nonatomic, strong) NSString *questionResponseBy;
@property (nonatomic, strong) NSDate *questionResponseDate;
@property (nonatomic) BOOL questionReadOnlyFlag;
@property (nonatomic, strong) NSArray<BTOCSQuestionResponseOptions*> *questionResponseOptions;

+ (BTOCSQuestion*)questionWithDict:(NSDictionary*)questionInfo;


@end

NS_ASSUME_NONNULL_END
