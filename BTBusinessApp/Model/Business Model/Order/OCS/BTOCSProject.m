//
//  BTOCSProject.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSProject.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"
#import "BTOCSOrder.h"
#import "BTOCSSite.h"
#import "BTOCSEnquiry.h"
#import "BTOCSContactDetail.h"
#import "BTOCSKeyValueDescription.h"
#import "BTOCSNote.h"
#import "BTOCSQuestionGroup.h"

@implementation BTOCSProject

- (instancetype)initWithProjectRef:(NSString *)projectRef
{
    if (self = [self init]) {
        self.projectRef = projectRef;
    }
    return self;
}

- (instancetype)initWithAPIResponse:(NSDictionary *)responseObject
{
    if (self = [self init]) {
        self.projectRef = [[responseObject objectForKey:@"ProjectRef"] validAndNotEmptyStringObject];
        self.productName = [[responseObject objectForKey:@"ProductName"] validAndNotEmptyStringObject];
        self.status = [[responseObject objectForKey:@"Status"] validAndNotEmptyStringObject];
        self.subStatus = [[responseObject objectForKey:@"SubStatus"] validAndNotEmptyStringObject];
        
        self.projectStartDate = [AppManager dateFromString:[[responseObject objectForKey:@"ProjectStartDate"] validAndNotEmptyStringObject]];
        self.projectEndDate = [AppManager dateFromString:[[responseObject objectForKey:@"ProjectEndDate"] validAndNotEmptyStringObject]];

        if ([responseObject objectForKey:@"Orders"] && [[responseObject objectForKey:@"Orders"] isKindOfClass:[NSArray class]]) {
            NSArray *ordersArray = [[responseObject objectForKey:@"Orders"] safeArrayFromJSONObject];
            NSMutableArray<BTOCSOrder*> *tmpOrders = [[NSMutableArray alloc] initWithCapacity:ordersArray.count];
            for (NSDictionary *dict in ordersArray) {
                if ([dict objectForKey:@"OrderNumber"]) {
                    BTOCSOrder *order = [[BTOCSOrder alloc] init];
                    [order setOrderNumber:[dict objectForKey:@"OrderNumber"]];
                    [tmpOrders addObject:order];
                }
            }
            self.orders = [NSArray arrayWithArray:tmpOrders];
        } else if ([responseObject objectForKey:@"Orders"] && [[responseObject objectForKey:@"Orders"] isKindOfClass:[NSDictionary class]]) {
            BTOCSOrder *order = [[BTOCSOrder alloc] init];
            [order setOrderNumber:[[responseObject objectForKey:@"Orders"] objectForKey:@"OrderNumber"]];
            self.orders = [NSArray arrayWithObject:order];
        } else {
            self.orders = @[];
        }
        
        
    }
    return self;
}

- (void)updateWithProjectSummaryResponse:(NSDictionary *)projectSummary
{
    // root level objects from response
    NSDictionary *projectInfo = [projectSummary objectForKey:@"Project"];
    NSDictionary *contactInfo = [projectSummary objectForKey:@"ContactInfo"];
    //NSDictionary *relatedProjects = [projectSummary objectForKey:@"RelatedProjects"];
    
    // root level project info
    self.currentStatus = [[projectInfo objectForKey:@"CurrentStatus"] validAndNotEmptyStringObject];
    self.customerName = [[projectInfo objectForKey:@"CustomerName"] validAndNotEmptyStringObject];
    self.productService = [[projectInfo objectForKey:@"ProductService"] validAndNotEmptyStringObject];
    self.dateClosed = [AppManager dateFromString:[[projectInfo objectForKey:@"DateClosed"] validAndNotEmptyStringObject]];
    self.targetDeliveryDate = [AppManager dateFromString:[[projectInfo objectForKey:@"TargetDeliveryDate"] validAndNotEmptyStringObject]];
    self.intGroupId = [[projectInfo valueForKey:@"intGroupID"] integerValue];
    
    if (!self.productName) {
        self.productName = self.productService;
    }
    if (!self.subStatus) {
        self.subStatus = self.currentStatus;
    }
    
    if (!self.orders) {
        //if ([projectSummary objectForKey:@"Orders"] && [[projectSummary objectForKey:@"Orders"] isKindOfClass:[NSArray class]]) {
        if ([projectSummary objectForKey:@"Orders"]) {
            NSArray *ordersArray = [[projectSummary objectForKey:@"Orders"] safeArrayFromJSONObject];
            NSMutableArray<BTOCSOrder*> *tmpOrders = [[NSMutableArray alloc] initWithCapacity:ordersArray.count];
            for (NSDictionary *dict in ordersArray) {
                if ([dict objectForKey:@"OrderNumber"]) {
                    BTOCSOrder *order = [[BTOCSOrder alloc] init];
                    [order setOrderNumber:[dict objectForKey:@"OrderNumber"]];
                    [tmpOrders addObject:order];
                }
            }
            self.orders = [NSArray arrayWithArray:tmpOrders];
//        } else if ([projectSummary objectForKey:@"Orders"] && [[projectSummary objectForKey:@"Orders"] isKindOfClass:[NSDictionary class]]) {
//            BTOCSOrder *order = [[BTOCSOrder alloc] init];
//            [order setOrderNumber:[[projectSummary objectForKey:@"Orders"] objectForKey:@"OrderNumber"]];
//            self.orders = [NSArray arrayWithObject:order];
        } else {
            self.orders = @[];
        }
    }
    
    self.importantInfoFlag = [[projectInfo objectForKey:@"ImportantInfoFlag"] boolValue];
    self.openQuestionsFlag = [[projectInfo objectForKey:@"OpenQuestionsFlag"] boolValue];
    
    // extract contact emails info
    NSObject *contactsEmailInfo = [projectInfo valueForKeyPath:@"ContactEmails.ContactEmail"];
    NSMutableArray *emails = [NSMutableArray new];
//    if ([contactsEmailInfo isKindOfClass:[NSArray class]]) {
//        for (NSDictionary *emailInfo in (NSArray*)contactsEmailInfo) {
//            [emails addObject:[emailInfo objectForKey:@"EmailAddress"]];
//        }
//    } else if ([contactsEmailInfo isKindOfClass:[NSDictionary class]]) {
//        [emails addObject:[(NSDictionary*)contactsEmailInfo objectForKey:@"EmailAddress"]];
//    }
    if (contactsEmailInfo) {
        for (NSDictionary *emailInfo in [contactsEmailInfo safeArrayFromJSONObject]) {
            [emails addObject:[emailInfo objectForKey:@"EmailAddress"]];
        }

    }
    self.contactEmails = [NSArray arrayWithArray:emails];
    
    // extract enquiries categories
    NSObject *enquiriesCategoriesInfo = [contactInfo valueForKeyPath:@"EnquiryCategories.EnquiryCategory"];
    NSMutableArray *categories = [NSMutableArray new];
//    if ([enquiriesCategoriesInfo isKindOfClass:[NSArray class]]) {
//        for (NSDictionary *enquiryCategoriesInfo in (NSArray*)enquiriesCategoriesInfo) {
//            [categories addObject:[enquiryCategoriesInfo objectForKey:@"Category"]];
//        }
//    } else if ([enquiriesCategoriesInfo isKindOfClass:[NSDictionary class]]) {
//        [categories addObject:[(NSDictionary*)enquiriesCategoriesInfo objectForKey:@"Category"]];
//    }
    if (enquiriesCategoriesInfo) {
        for (NSDictionary *enquiryCategoriesInfo in [enquiriesCategoriesInfo safeArrayFromJSONObject]) {
            [categories addObject:[enquiryCategoriesInfo objectForKey:@"Category"]];
        }
    }
    self.enquiryCategories = [NSArray arrayWithArray:categories];
    
    self.contactMessage = [[contactInfo objectForKey:@"ContactMessage"] validAndNotEmptyStringObject];
    
    // extract sites info
    NSObject *sitesInfo = [projectInfo valueForKeyPath:@"Sites.Site"];
    NSMutableArray *sitesArray = [NSMutableArray new];
//    if ([sitesInfo isKindOfClass:[NSArray class]]) {
//        for (NSDictionary *siteInfo in (NSArray*)sitesInfo) {
//            [sitesArray addObject:[BTOCSSite siteWithDict:siteInfo]];
//        }
//    } else if ([sitesInfo isKindOfClass:[NSDictionary class]]) {
//        [sitesArray addObject:[BTOCSSite siteWithDict:(NSDictionary*)sitesInfo]];
//    }
    if (sitesInfo) {
        for (NSDictionary *siteInfo in [sitesInfo safeArrayFromJSONObject]) {
            [sitesArray addObject:[BTOCSSite siteWithDict:siteInfo]];
        }
    }
    self.sites = [NSArray arrayWithArray:sitesArray];
    
    // extra attributes
    self.hideContactPanel = NO;
    NSArray *extraAttributes = [[projectSummary valueForKeyPath:@"ExtraAttributes.ExtraAttribute"] safeArrayFromJSONObject];
    for (NSDictionary *attribute in extraAttributes) {
        if ([attribute objectForKey:@"AttributeName"] && [[attribute objectForKey:@"AttributeName"] isKindOfClass:[NSString class]]) {
            NSString *attributeName = (NSString*)[attribute objectForKey:@"AttributeName"];
            if ([attributeName.lowercaseString isEqualToString:@"HideContactPanel".lowercaseString]) {
                if ([[attribute valueForKey:@"AttributeTextValue"] isKindOfClass:[NSString class]]) {
                    NSString *attributeValue = (NSString*)[attribute valueForKey:@"AttributeTextValue"];
                    if ([attributeValue.lowercaseString isEqualToString:@"true"]) {
                        self.hideContactPanel = YES;
                    }
                } else if ([[attribute valueForKey:@"AttributeTextValue"] boolValue]) {
                    self.hideContactPanel = YES;
                }
            }
        }
    }
    
}

- (void)updateWithProjectEnquiriesResponse:(NSDictionary *)projectEnquries
{
    // extract enquiries info
    NSObject *enquiriesInfo = [projectEnquries valueForKeyPath:@"Enquiries.Enquiry"];
    NSMutableArray *enquiriesArray = [NSMutableArray new];
//    if ([enquiriesInfo isKindOfClass:[NSArray class]]) {
//        for (NSDictionary *enquiryInfo in (NSArray*)enquiriesInfo) {
//            [enquiriesArray addObject:[BTOCSEnquiry enquiryWithDict:enquiryInfo]];
//        }
//    } else if ([enquiriesInfo isKindOfClass:[NSDictionary class]]) {
//        [enquiriesArray addObject:[BTOCSEnquiry enquiryWithDict:(NSDictionary*)enquiriesInfo]];
//    }
    if (enquiriesInfo) {
        for (NSDictionary *enquiryInfo in [enquiriesInfo safeArrayFromJSONObject]) {
            [enquiriesArray addObject:[BTOCSEnquiry enquiryWithDict:enquiryInfo]];
        }
    }
    self.enquiries = [NSArray arrayWithArray:enquiriesArray];
}

- (void)updateWithProjectDetailsResponse:(NSDictionary *)projectDetail
{
    // extract contact detail info
    NSObject *detailsInfo = [projectDetail valueForKeyPath:@"ProjectContactDetails.ProjectContactDetail"];
    NSMutableArray *detailssArray = [NSMutableArray new];
//    if ([detailsInfo isKindOfClass:[NSArray class]]) {
//        for (NSDictionary *detailInfo in (NSArray*)detailsInfo) {
//            [detailssArray addObject:[BTOCSContactDetail contactDetailWithDict:detailInfo]];
//        }
//    } else if ([detailsInfo isKindOfClass:[NSDictionary class]]) {
//        [detailssArray addObject:[BTOCSContactDetail contactDetailWithDict:(NSDictionary*)detailsInfo]];
//    }
    if (detailsInfo) {
        for (NSDictionary *detailInfo in [detailsInfo safeArrayFromJSONObject]) {
            [detailssArray addObject:[BTOCSContactDetail contactDetailWithDict:detailInfo]];
        }
    }
    self.contactDetails = [NSArray arrayWithArray:detailssArray];
    
    NSObject *extraDetails = [projectDetail valueForKeyPath:@"KeyValueDescription.KeyValueDescription"];
    NSMutableArray *kvdArray = [NSMutableArray new];
    if (extraDetails) {
        for (NSDictionary *kvdInfo in [extraDetails safeArrayFromJSONObject]) {
            [kvdArray addObject:[BTOCSKeyValueDescription keyValueDescriptionWithDict:kvdInfo]];
        }
    }
    self.keyValueDescriptions = [NSArray arrayWithArray:kvdArray];
    
}

- (void)updateWithProjectNotesResponse:(NSDictionary *)projectNotes
{
    // extract project notes info
    NSObject *notesInfo = [projectNotes valueForKeyPath:@"ProjectNotes.Note"];
    NSMutableArray *notesArray = [NSMutableArray new];
//    if ([notesInfo isKindOfClass:[NSArray class]]) {
//        for (NSDictionary *noteInfo in (NSArray*)notesInfo) {
//            [notesArray addObject:[BTOCSNote noteWithDict:noteInfo]];
//        }
//    } else if ([notesInfo isKindOfClass:[NSDictionary class]]) {
//        [notesArray addObject:[BTOCSNote noteWithDict:(NSDictionary*)notesInfo]];
//    }
    if (notesInfo) {
        for (NSDictionary *noteInfo in [notesInfo safeArrayFromJSONObject]) {
            [notesArray addObject:[BTOCSNote noteWithDict:noteInfo]];
        }
    }
    self.projectNotes = [NSArray arrayWithArray:notesArray];
}

- (void)updateWithProjectQuestionsResponse:(NSDictionary *)projectQuestions
{
    NSObject *questionGroupInfo = [projectQuestions valueForKeyPath:@"QuestionGroups.QuestionGroup"];
    NSMutableArray *questionGroupArray = [NSMutableArray new];
    if (questionGroupInfo) {
        for (NSDictionary *info in [questionGroupInfo safeArrayFromJSONObject]) {
            [questionGroupArray addObject:[BTOCSQuestionGroup questionGroupWithDict:info]];
        }
    }
    self.questionGroups = [NSArray arrayWithArray:questionGroupArray];
}

@end
