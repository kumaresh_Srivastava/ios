//
//  BTOCSEnquiry.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 02/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTOCSEnquiry : NSObject

@property (nonatomic) NSInteger enquiryId;
@property (nonatomic, strong) NSString *enquiryCategory;
@property (nonatomic, strong) NSString *enquiryDescription;
@property (nonatomic, strong) NSString *enquiryStatus;
@property (nonatomic, strong) NSDate *enquiryDate;
@property (nonatomic, strong) NSDate *enquiryClosureDate;
@property (nonatomic, strong) NSArray<NSString*> *enquiryNotes;

+ (BTOCSEnquiry*)enquiryWithDict:(NSDictionary*)enquiryDict;

@end

