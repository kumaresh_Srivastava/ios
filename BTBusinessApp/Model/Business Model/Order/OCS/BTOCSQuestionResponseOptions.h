//
//  BTOCSQuestionResponseOptions.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 16/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BTOCSQuestionResponseOptions : NSObject

@property (nonatomic, strong) NSString *optionText;
@property (nonatomic, strong) NSString *optionValue;

+ (BTOCSQuestionResponseOptions*)optionWithDict:(NSDictionary*)options;

@end

NS_ASSUME_NONNULL_END
