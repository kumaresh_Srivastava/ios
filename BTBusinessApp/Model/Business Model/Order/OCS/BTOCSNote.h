//
//  BTOCSNote.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 03/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BTOCSNote : NSObject

@property (nonatomic, strong) NSDate *noteDate;
@property (nonatomic, strong) NSString *noteType;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *note;

+ (BTOCSNote*)noteWithDict:(NSDictionary*)noteDict;

@end

NS_ASSUME_NONNULL_END
