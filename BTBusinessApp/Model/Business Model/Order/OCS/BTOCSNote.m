//
//  BTOCSNote.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 03/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSNote.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"

@implementation BTOCSNote

+ (BTOCSNote *)noteWithDict:(NSDictionary *)noteDict
{
    BTOCSNote *note = [[BTOCSNote alloc] init];
    note.noteDate = [AppManager dateFromString:[[noteDict objectForKey:@"NoteDate"] validAndNotEmptyStringObject]];
    note.noteType = [[noteDict objectForKey:@"NoteType"] validAndNotEmptyStringObject];
    note.fullName = [[noteDict objectForKey:@"FullName"] validAndNotEmptyStringObject];
    note.note = [[noteDict objectForKey:@"Note"] validAndNotEmptyStringObject];

    return note;
}

@end
