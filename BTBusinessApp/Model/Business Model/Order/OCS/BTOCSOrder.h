//
//  BTOCSOrder.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 19/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BTOCSOrder : NSObject

@property (nonatomic, strong) NSString *orderNumber;

@end

