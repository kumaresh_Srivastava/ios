//
//  BTOCSQuestionResponseOptions.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 16/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSQuestionResponseOptions.h"
#import "NSObject+APIResponseCheck.h"

@implementation BTOCSQuestionResponseOptions

+ (BTOCSQuestionResponseOptions *)optionWithDict:(NSDictionary *)options
{
    BTOCSQuestionResponseOptions *option = [[BTOCSQuestionResponseOptions alloc] init];
    option.optionText = [[options objectForKey:@"OptionText"] validAndNotEmptyStringObject];
    option.optionValue = [[options objectForKey:@"OptionValue"] validAndNotEmptyStringObject];
    return option;
}

@end
