//
//  BTOrder.h
//  BTBusinessApp
//
//  Created by Accolite on 22/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BTProduct;


@interface BTOrder : NSObject

@property (nonatomic, readonly) NSString *orderRef;
@property (nonatomic, readonly) NSString *orderDescription;
@property (nonatomic, readonly) NSString *orderStatus;
@property (nonatomic, readonly) NSDate *estimatedCompletionDate;
@property (nonatomic, readonly) NSDate *orderPlacedDate;
@property (nonatomic, readonly) NSInteger orderProductsCount;
@property (nonatomic, readonly) NSArray *arrayOfProducts;
@property (nonatomic, readonly) NSDate *orderDate;
@property (nonatomic, readonly) BOOL IsStrategic;
@property (nonatomic,readonly)  NSString *grouperOrderId;
@property (nonatomic,readonly)  BOOL IsShowAppointment;

- (instancetype)initOrderWithResponseDictionaryFromGroupOrderSummaryAPIResponse:(NSDictionary *)responseDic;
- (instancetype)initWithResponseDictionaryFromOrderDashBoardAPIResponse:(NSDictionary *)responseDic;
- (instancetype)initOrderWithResponseDictionaryFromOrderSummaryAPIResponse:(NSDictionary *)responseDic;
- (instancetype)initOrderWithResponseDictionaryFromBundleOrderSummaryAPIResponse:(NSDictionary *)resultDic;

- (void)addProduct:(BTProduct *)product;

@end

/*
 {
 "isSuccess": true,
 "code": 600,
 "errorMessage": "",
 "result": {
 "PageIndex": 2,
 "TotalSize": 16,
 "PageSize": 3,
 "Orders": [
 {
 "<OrderDate>k__BackingField": "2016-08-18T11:29:29Z",
 "<OrderIdentifier>k__BackingField": "BTDLC987",
 "<Description>k__BackingField": "",
 "<OrderStatus>k__BackingField": false,
 "<LongDescription>k__BackingField": "BT Business PhoneLine related order",
 "<PlacedOnDate>k__BackingField": "2016-08-18T11:29:29Z",
 "<CompletionDate>k__BackingField": "2016-12-20T00:00:00Z",
 "<CompleteOrderStatus>k__BackingField": "In Progress"
 }
 ]
 }
 }
*/
