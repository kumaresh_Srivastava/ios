//
//  BTAppointment.m
//  BTBusinessApp
//
//  Created by Accolite on 03/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTAppointment.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"

@implementation BTAppointment

- (instancetype)initWithResponseDictionaryFromAppointmentsAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        
        _dateInfo = [self getDateFromDateString:[[responseDic[@"DateInfo"] validAndNotEmptyStringObject] copy]];
        _date = [AppManager NSDateWithUTCFormatFromNSString:[[responseDic[@"Date"] validAndNotEmptyStringObject] copy]];
        // get date from dateinfo for 2 hour appts
        if (!_date) {
            _date = [AppManager NSDateWithUTCFormatFromNSString:[[responseDic[@"DateInfo"] validAndNotEmptyStringObject] copy]];
        }
        _DayOfWeek = [[responseDic[@"DayOfWeek"] validAndNotEmptyStringObject] copy];
        _hasAM = [responseDic[@"HasAM"] boolValue];
        _hasPM = [responseDic[@"HasPM"] boolValue];
        _isAMSelected = [responseDic[@"IsAMSelected"] boolValue];
        _isPMSelected = [responseDic[@"IsPMSelected"] boolValue];
        
        //additional properties for 2 hour appts
        _hasAS1000 = [responseDic[@"HasAS1000"] boolValue];
        _hasAS1200 = [responseDic[@"HasAS1200"] boolValue];
        _hasAS1400 = [responseDic[@"HasAS1400"] boolValue];
        _hasAS1600 = [responseDic[@"HasAS1600"] boolValue];
        _hasAS1800 = [responseDic[@"HasAS1800"] boolValue];
        _hasAS2000 = [responseDic[@"HasAS2000"] boolValue];
        _isAS1000Selected = [responseDic[@"IsAS1000Selected"] boolValue];
        _isAS1200Selected = [responseDic[@"IsAS1200Selected"] boolValue];
        _isAS1400Selected = [responseDic[@"IsAS1400Selected"] boolValue];
        _isAS1600Selected = [responseDic[@"IsAS1600Selected"] boolValue];
        _isAS1800Selected = [responseDic[@"IsAS1800Selected"] boolValue];
        _isAS2000Selected = [responseDic[@"IsAS2000Selected"] boolValue];
        
        _hasAS0900 = [responseDic[@"HasAS0900"] boolValue];
        _hasAS1100 = [responseDic[@"HasAS1100"] boolValue];
        _hasAS1300 = [responseDic[@"HasAS1300"] boolValue];
        _hasAS1500 = [responseDic[@"HasAS1500"] boolValue];
        _hasAS1700 = [responseDic[@"HasAS1700"] boolValue];
        _hasAS1900 = [responseDic[@"HasAS1900"] boolValue];
        _isAS0900Selected = [responseDic[@"IsAS0900Selected"] boolValue];
        _isAS1100Selected = [responseDic[@"IsAS1100Selected"] boolValue];
        _isAS1300Selected = [responseDic[@"IsAS1300Selected"] boolValue];
        _isAS1500Selected = [responseDic[@"IsAS1500Selected"] boolValue];
        _isAS1700Selected = [responseDic[@"IsAS1700Selected"] boolValue];
        _isAS1900Selected = [responseDic[@"IsAS1900Selected"] boolValue];
    }
    return self;
}

- (instancetype)initWithDate:(NSDate *)date andTimeslot:(NSString *)timeslot
{
    self = [super init];
    if (self) {
        _date = date;
        _dateInfo = date;
        if ([timeslot isEqualToString:@"8am - 1pm"]) {
            _isAMSelected = YES;
        } else if ([timeslot isEqualToString:@"1pm - 6pm"]) {
            _isPMSelected = YES;
        } else if ([timeslot isEqualToString:@"7am - 9am"]) {
            _isAS0900Selected = YES;
        } else if ([timeslot isEqualToString:@"8am - 10am"]) {
            _isAS1000Selected = YES;
        } else if ([timeslot isEqualToString:@"9am - 11am"]) {
            _isAS1100Selected = YES;
        } else if ([timeslot isEqualToString:@"10am - 12pm"]) {
            _isAS1200Selected = YES;
        } else if ([timeslot isEqualToString:@"11am - 1pm"]) {
            _isAS1300Selected = YES;
        } else if ([timeslot isEqualToString:@"12pm - 2pm"]) {
            _isAS1400Selected = YES;
        } else if ([timeslot isEqualToString:@"1pm - 3pm"]) {
            _isAS1500Selected = YES;
        } else if ([timeslot isEqualToString:@"2pm - 4pm"]) {
            _isAS1600Selected = YES;
        } else if ([timeslot isEqualToString:@"3pm - 5pm"]) {
            _isAS1700Selected = YES;
        } else if ([timeslot isEqualToString:@"4pm - 6pm"]) {
            _isAS1800Selected = YES;
        } else if ([timeslot isEqualToString:@"5pm - 7pm"]) {
            _isAS1900Selected = YES;
        } else if ([timeslot isEqualToString:@"6pm - 8pm"]) {
            _isAS2000Selected = YES;
        }
    }
    return self;
}

- (NSDate *)getDateFromDateString:(NSString *)dateString{
    
    NSString *dateFormat = @"dd/MM/yyyy";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    
    NSDate *date = nil;
    
    if ([dateFormatter dateFromString:dateString] != nil) {
        
        date = [dateFormatter dateFromString:dateString];
        return date;
    }
    
    return date;
}

- (BOOL)isDaySelected
{
    return (self.isAMSelected||self.isPMSelected||self.isAS0900Selected||self.isAS1000Selected||self.isAS1100Selected||self.isAS1200Selected||self.isAS1300Selected||self.isAS1400Selected||self.isAS1500Selected||self.isAS1600Selected||self.isAS1700Selected||self.isAS1800Selected||self.isAS1900Selected||self.isAS2000Selected);
}

- (NSString *)selectedTimeslot
{
    NSString *timeslot = @"";
    if (self.isAMSelected) {
        timeslot = @"8am - 1pm";
    } else if (self.isPMSelected) {
        timeslot = @"1pm - 6pm";
    } else if (self.isAS0900Selected) {
        timeslot = @"7am - 9am";
    } else if (self.isAS1000Selected) {
        timeslot = @"8am - 10am";
    } else if (self.isAS1100Selected) {
        timeslot = @"9am - 11am";
    } else if (self.isAS1200Selected) {
        timeslot = @"10am - 12pm";
    } else if (self.isAS1300Selected) {
        timeslot = @"11am - 1pm";
    } else if (self.isAS1400Selected) {
        timeslot = @"12pm - 2pm";
    } else if (self.isAS1500Selected) {
        timeslot = @"1pm - 3pm";
    } else if (self.isAS1600Selected) {
        timeslot = @"2pm - 4pm";
    } else if (self.isAS1700Selected) {
        timeslot = @"3pm - 5pm";
    } else if (self.isAS1800Selected) {
        timeslot = @"4pm - 6pm";
    } else if (self.isAS1900Selected) {
        timeslot = @"5pm - 7pm";
    } else if (self.isAS2000Selected) {
        timeslot = @"6pm - 8pm";
    }
    return timeslot;
}

- (NSArray *)appointmentSlots
{
    NSMutableArray *appts = [NSMutableArray new];
    if (self.hasAM || self.hasPM) {
        [appts addObject:@"8am - 1pm"];
        [appts addObject:@"1pm - 6pm"];
    } else if (self.hasAS0900 || self.hasAS1100 || self.hasAS1300 || self.hasAS1500 || self.hasAS1700 || self.hasAS1900) {
        [appts addObject:@"7am - 9am"];
        [appts addObject:@"9am - 11am"];
        [appts addObject:@"11am - 1pm"];
        [appts addObject:@"1pm - 3pm"];
        [appts addObject:@"3pm - 5pm"];
        [appts addObject:@"5pm - 7pm"];
    } else if (self.hasAS1000 || self.hasAS1200 || self.hasAS1400 || self.hasAS1600 || self.hasAS1800 || self.hasAS2000) {
        [appts addObject:@"8am - 10am"];
        [appts addObject:@"10am - 12pm"];
        [appts addObject:@"12pm - 2pm"];
        [appts addObject:@"2pm - 4pm"];
        [appts addObject:@"4pm - 6pm"];
        [appts addObject:@"6pm - 8pm"];
    }
    
//    self.hasAM?[appts addObject:@"8am - 1pm"]:nil;
//    self.hasPM?[appts addObject:@"1pm - 6pm"]:nil;
//    self.hasAS0900?[appts addObject:@"7am - 9am"]:nil;
//    self.hasAS1000?[appts addObject:@"8am - 10am"]:nil;
//    self.hasAS1100?[appts addObject:@"9am - 11am"]:nil;
//    self.hasAS1200?[appts addObject:@"10am - 12pm"]:nil;
//    self.hasAS1300?[appts addObject:@"11am - 1pm"]:nil;
//    self.hasAS1400?[appts addObject:@"12pm - 2pm"]:nil;
//    self.hasAS1500?[appts addObject:@"1pm - 3pm"]:nil;
//    self.hasAS1600?[appts addObject:@"2pm - 4pm"]:nil;
//    self.hasAS1700?[appts addObject:@"3pm - 5pm"]:nil;
//    self.hasAS1800?[appts addObject:@"4pm - 6pm"]:nil;
//    self.hasAS1900?[appts addObject:@"5pm - 7pm"]:nil;
//    self.hasAS2000?[appts addObject:@"6pm - 8pm"]:nil;
    
    return appts;
}

- (BOOL)isTimeslotSelected:(NSString *)timeslot
{
    BOOL retVal = NO;
    if ([timeslot isEqualToString:@"8am - 1pm"]) {
        retVal = self.isAMSelected;
    } else if ([timeslot isEqualToString:@"1pm - 6pm"]) {
        retVal = self.isPMSelected;
    } else if ([timeslot isEqualToString:@"7am - 9am"]) {
        retVal = self.isAS0900Selected;
    } else if ([timeslot isEqualToString:@"8am - 10am"]) {
        retVal = self.isAS1000Selected;
    } else if ([timeslot isEqualToString:@"9am - 11am"]) {
        retVal = self.isAS1100Selected;
    } else if ([timeslot isEqualToString:@"10am - 12pm"]) {
        retVal = self.isAS1200Selected;
    } else if ([timeslot isEqualToString:@"11am - 1pm"]) {
        retVal = self.isAS1300Selected;
    } else if ([timeslot isEqualToString:@"12pm - 2pm"]) {
        retVal = self.isAS1400Selected;
    } else if ([timeslot isEqualToString:@"1pm - 3pm"]) {
        retVal = self.isAS1500Selected;
    } else if ([timeslot isEqualToString:@"2pm - 4pm"]) {
        retVal = self.isAS1600Selected;
    } else if ([timeslot isEqualToString:@"3pm - 5pm"]) {
        retVal = self.isAS1700Selected;
    } else if ([timeslot isEqualToString:@"4pm - 6pm"]) {
        retVal = self.isAS1800Selected;
    } else if ([timeslot isEqualToString:@"5pm - 7pm"]) {
        retVal = self.isAS1900Selected;
    } else if ([timeslot isEqualToString:@"6pm - 8pm"]) {
        retVal = self.isAS2000Selected;
    }
    return retVal;
}

- (BOOL)hasTimeslot:(NSString *)timeslot
{
    BOOL retVal = NO;
    if ([timeslot isEqualToString:@"8am - 1pm"]) {
        retVal = self.hasAM;
    } else if ([timeslot isEqualToString:@"1pm - 6pm"]) {
        retVal = self.hasPM;
    } else if ([timeslot isEqualToString:@"7am - 9am"]) {
        retVal = self.hasAS0900;
    } else if ([timeslot isEqualToString:@"8am - 10am"]) {
        retVal = self.hasAS1000;
    } else if ([timeslot isEqualToString:@"9am - 11am"]) {
        retVal = self.hasAS1100;
    } else if ([timeslot isEqualToString:@"10am - 12pm"]) {
        retVal = self.hasAS1200;
    } else if ([timeslot isEqualToString:@"11am - 1pm"]) {
        retVal = self.hasAS1300;
    } else if ([timeslot isEqualToString:@"12pm - 2pm"]) {
        retVal = self.hasAS1400;
    } else if ([timeslot isEqualToString:@"1pm - 3pm"]) {
        retVal = self.hasAS1500;
    } else if ([timeslot isEqualToString:@"2pm - 4pm"]) {
        retVal = self.hasAS1600;
    } else if ([timeslot isEqualToString:@"3pm - 5pm"]) {
        retVal = self.hasAS1700;
    } else if ([timeslot isEqualToString:@"4pm - 6pm"]) {
        retVal = self.hasAS1800;
    } else if ([timeslot isEqualToString:@"5pm - 7pm"]) {
        retVal = self.hasAS1900;
    } else if ([timeslot isEqualToString:@"6pm - 8pm"]) {
        retVal = self.hasAS2000;
    }
    return retVal;
}

@end

/*
 {
 "DateInfo": "2016-12-27T18:30:00Z",
 "Date": "28 / December",
 "DayOfWeek": "Wednesday",
 "HasAM": true,
 "HasPM": true,
 "IsAMSelected": false,
 "IsPMSelected": false
 },
 
 */
