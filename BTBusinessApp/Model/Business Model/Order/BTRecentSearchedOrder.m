//
//  BTRecentSearchedOrder.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 26/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTRecentSearchedOrder.h"
#import "CDRecentSearchedOrder.h"

@implementation BTRecentSearchedOrder

- (instancetype)initWithCDRecentSearchedOrder:(CDRecentSearchedOrder *)recentSearchedOrder
{
    self = [super init];
    if(self)
    {
        _orderRef = recentSearchedOrder.orderRef;
        _orderStatus = recentSearchedOrder.orderStatus;
        _orderDescription = recentSearchedOrder.orderDescription;
        _placedOnDate = recentSearchedOrder.placedOnDate;
        _completionDate = recentSearchedOrder.completionDate;
        _lastSearchedDate = recentSearchedOrder.lastSearchedDate;
        
    }
    return self;
}

@end
