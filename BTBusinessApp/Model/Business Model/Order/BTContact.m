//
//  BTContact.m
//  BTBusinessApp
//
//  Created by Accolite on 02/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTContact.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"

@implementation BTContact

- (instancetype)initWithResponseDictionaryFromSiteContactsAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _contactKey = [[responseDic[@"ContactKey"] validAndNotEmptyStringObject] copy];
        _email = [[responseDic[@"Email"] validAndNotEmptyStringObject] copy];
        _firstName = [[responseDic[@"FirstName"] validAndNotEmptyStringObject] copy];
        _lastName = [[responseDic[@"LastName"] validAndNotEmptyStringObject] copy];
        _title = [[responseDic[@"Title"] validAndNotEmptyStringObject] copy];
        _mobile = [[responseDic[@"Mobile"] validAndNotEmptyStringObject] copy];
        _phone = [[responseDic[@"Phone"] validAndNotEmptyStringObject] copy];
        _formattedContactLine = [[responseDic[@"ForamattedContactLine"] validAndNotEmptyStringObject] copy];
        
    }
    return self;
}

@end
