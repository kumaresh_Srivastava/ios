//
//  BTEngineerAppointmentSlotModel.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 11/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTEngineerAppointmentSlotModel.h"

@implementation BTEngineerAppointmentSlotModel

- (id) init {
    self = [super init];
    if (self) {
        self.date = [NSString string];
        self.dateInfo = [NSString string];
        self.dayOfWeek = [NSString string];
        self.hasAM = NO;
        self.hasPM = NO;
        self.isAMSelected = NO;
        self.isPMSelected = NO;
        self.isInitialSlot = NO;
        self.isLastSlot = NO;

    }
    return self;
}

- (void)updateAppointmentSlotDataWith:(NSDictionary *)slotData isInitialSlot:(BOOL)isInitialSlot isLastSlot:(BOOL)isLastSlot
{
    self.date = [slotData objectForKey:@"Date"];
    self.dateInfo = [slotData objectForKey:@"DateInfo"];
    self.dayOfWeek = [slotData objectForKey:@"DayOfWeek"];
    self.hasAM = [[slotData objectForKey:@"HasAM"] boolValue];
    self.hasPM = [[slotData objectForKey:@"HasPM"] boolValue];
    self.isAMSelected = [[slotData objectForKey:@"IsAMSelected"] boolValue];
    self.isPMSelected = [[slotData objectForKey:@"IsPMSelected"] boolValue];
    self.isInitialSlot = isInitialSlot;
    self.isLastSlot = isLastSlot;
}

@end
