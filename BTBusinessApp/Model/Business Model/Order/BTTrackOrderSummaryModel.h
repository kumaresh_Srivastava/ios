//
//  BTTrackOrderSummaryModel.h
//  BTBusinessApp
//
//  Created by Accolite on 30/06/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTTrackOrderSummaryModel : NSObject

@property(nonatomic,assign)NSUInteger AuthLevel;
@property(nonatomic,assign)NSUInteger IsBTB;
@property(nonatomic,assign)NSUInteger IsDelayedOrder;
@property(nonatomic,assign)NSUInteger IsPreAppointment;
@property(nonatomic,assign)NSUInteger IsPreAppointmentIndex;
@property(nonatomic,strong)NSDictionary *OrderMetaInfo;
@property(nonatomic,strong)NSArray *Products;
@property(nonatomic,strong)NSArray *BundleOrders;

- (NSDictionary *)getProductDictionaryFromProductsWithIndex:(NSUInteger)index;
@end


//Sample Response
/*

 {
    AuthLevel = 1;
    IsBTB = 1;
    IsDelayedOrder = 0;
    IsPreAppointment = 0;
    IsPreAppointmentIndex = 0;
    OrderMetaInfo =     
    {
        AuthLevel = 1;
        CurrentPage = 1;
        EstimatedCompletionDate = "06 Jul 2016";
        IsCONFOrder = 0;
        IsStrategic = 1;
        OrderDescription = "BT Business Broadband & BT Business phone line related order";
        OrderPlacedDate = "29 Jun 2016";
        OrderProductsCount = 2;
        OrderRef = BTDJC662;
        OrderStatus = Completed;
    };
    Products =     
    (
        {
            ActiveMilestone = Completion;
            AuthLevel = 1;
            BundleContractTerm = "24 month";
            EngineeringAppointmentDate = "2016-07-06T13:00:00+01:00";
            EngineeringAppointmentSlot = "(1pm - 6pm)";
            ExpectedDispatchDate = "2016-06-29T09:06:55+01:00";
            IsBundleProduct = 0;
            IsDelayedOrder = 0;
             IsPreAppointment = 0;
             MonthlyPrice = 40;
             OneOffPrice = 8;
             OrderItemReference = "BTDJC6621-2";
             OrderKey = BTDJC662;
             OrderStatusLongDescription = "Your order's now complete.";
             PercentageCompletion = 100;
             ProductDescription = "Provision of BT Business Fibre Broadband";
             ProductDuedate = "06 Jul 2016";
             ProductIcon = broadbandIcon;
             ProductName = "BT Business Broadband";
             ProductType = 3;
             Status = Completed;
             isCeaseOrCancelled = 0;
        },
        {
             ActiveMilestone = Completion;
             AuthLevel = 1;
             BundleContractTerm = "24 month";
             EngineeringAppointmentDate = "2016-07-06T13:00:00+01:00";
             EngineeringAppointmentSlot = "(1pm - 6pm)";
             ExpectedDispatchDate = "0001-01-01T00:00:00";
             IsBundleProduct = 0;
             IsDelayedOrder = 0;
             IsPreAppointment = 0;
             MonthlyPrice = "21.2";
             OneOffPrice = 120;
             OrderItemReference = "BTDJC6621-11";
             OrderKey = BTDJC662;
             OrderStatusLongDescription = "Your order's now complete.";
             PercentageCompletion = 100;
             ProductDescription = "Provision of BT Business Phone line";
             ProductDuedate = "06 Jul 2016";
             ProductIcon = phoneIcon;
             ProductName = "BT Business Phone line";
             ProductType = 1;
             Status = Completed;
             isCeaseOrCancelled = 0;
        }
    );
 }

*/