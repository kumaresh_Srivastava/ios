//
//  BTOrder.m
//  BTBusinessApp
//
//  Created by Accolite on 22/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrder.h"
#import "NSObject+APIResponseCheck.h"
#import "BTProduct.h"
#import "AppManager.h"


@interface BTOrder () {
    
    NSMutableArray *_arrayOfProducts;
}

@end

@implementation BTOrder

- (instancetype)initWithResponseDictionaryFromOrderDashBoardAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _orderDate = [AppManager dateFromString:[[responseDic[@"<OrderDate>k__BackingField"] validAndNotEmptyStringObject] copy]];
        _orderRef = [[responseDic[@"<OrderIdentifier>k__BackingField"] validAndNotEmptyStringObject] copy];
        _orderDescription = [[responseDic[@"<LongDescription>k__BackingField"] validAndNotEmptyStringObject] copy];
        _orderPlacedDate = [AppManager dateFromString:[[responseDic[@"<PlacedOnDate>k__BackingField"] validAndNotEmptyStringObject] copy] ];
        _estimatedCompletionDate = [AppManager dateFromString:[[responseDic[@"<CompletionDate>k__BackingField"] validAndNotEmptyStringObject] copy]];
        _orderStatus = [[responseDic[@"<CompleteOrderStatus>k__BackingField"] validAndNotEmptyStringObject] copy];
        
    }
    return self;
}

- (instancetype)initOrderWithResponseDictionaryFromOrderSummaryAPIResponse:(NSDictionary *)resultDic
{
    self = [super init];
    if(self)
    {
        if([resultDic[@"IsShowAppointment"] isEqual:[NSNull null]] || resultDic[@"IsShowAppointment"] == nil){
            _IsShowAppointment = YES;
        } else{
            _IsShowAppointment = [resultDic[@"IsShowAppointment"] boolValue];
        }
        
         //_IsShowAppointment = YES;
        
        NSDictionary *orderMetaInfo = [resultDic valueForKey:@"OrderMetaInfo"];
        if ([orderMetaInfo isKindOfClass:[NSDictionary class]]) {
            
            _orderRef = [[orderMetaInfo[@"OrderRef"] validAndNotEmptyStringObject] copy];
            _orderDescription = [[orderMetaInfo[@"OrderDescription"] validAndNotEmptyStringObject] copy];
            _orderStatus = [[orderMetaInfo[@"OrderStatus"] validAndNotEmptyStringObject] copy];
            _estimatedCompletionDate = [AppManager dateFromString:[[orderMetaInfo[@"EstimatedCompletionDate"] validAndNotEmptyStringObject] copy]];
            _orderPlacedDate = [AppManager dateFromString:[[orderMetaInfo[@"OrderPlacedDate"] validAndNotEmptyStringObject] copy]];
            _orderProductsCount = [orderMetaInfo[@"OrderProductsCount"] integerValue];
            _IsStrategic = [[orderMetaInfo valueForKey:@"IsStrategic"] boolValue];
            _grouperOrderId = [[orderMetaInfo[@"GrouperOrderId"] validAndNotEmptyStringObject] copy];
           
        }
        else
        {
            DDLogError(@"'OrderMetaInfo' element missing in order summary response ");
        }
        
        NSArray *productArray = [resultDic valueForKey:@"Products"];
        if([productArray isKindOfClass:[NSArray class]])
        {
            for(NSDictionary *productDic in productArray)
            {
                if(![productDic isKindOfClass:[NSDictionary class]])
                    continue;
                
                NSInteger indexInResponse = [productArray indexOfObject:productDic];
                
                BTProduct *product = [[BTProduct alloc] initProductWithResponse:productDic andIndexInAPIResponse:indexInResponse];
                if(product)
                {
                    [self addProduct:product];
                }
            }
        }
        else
        {
            DDLogError(@"'Products' element missing in order summary response ");
        }
    }
    return self;
}

- (instancetype)initOrderWithResponseDictionaryFromGroupOrderSummaryAPIResponse:(NSDictionary *)responseDic
{
    self = [super init];
    if(self)
    {
        _orderRef = [[responseDic[@"OrderNumber"] validAndNotEmptyStringObject] copy];
        _orderDescription = [[responseDic[@"Description"] validAndNotEmptyStringObject] copy];
        _orderPlacedDate = [AppManager dateFromString:[[responseDic[@"OrderDate"] validAndNotEmptyStringObject] copy] ];
        _estimatedCompletionDate = [AppManager dateFromString:[[responseDic[@"CompletionDate"] validAndNotEmptyStringObject] copy] ];
        _orderStatus = [[responseDic[@"Status"] validAndNotEmptyStringObject] copy];
        
    }
    return self;
}

- (instancetype)initOrderWithResponseDictionaryFromBundleOrderSummaryAPIResponse:(NSDictionary *)resultDic
{
    self = [super init];
    if(self)
    {
       /* if([resultDic[@"IsShowAppointment"] isEqual:[NSNull null]] || resultDic[@"IsShowAppointment"] == nil){
            _IsShowAppointment = YES;
        } else{
            _IsShowAppointment = [resultDic[@"IsShowAppointment"] boolValue];
        }*/
        NSDictionary *orderMetaInfo = [resultDic valueForKey:@"OrderMetaInfo"];
        if ([orderMetaInfo isKindOfClass:[NSDictionary class]]) {
            
            _orderRef = [[orderMetaInfo[@"OrderRef"] validAndNotEmptyStringObject] copy];
            _orderDescription = [[orderMetaInfo[@"OrderDescription"] validAndNotEmptyStringObject] copy];
            _orderStatus = [[orderMetaInfo[@"OrderStatus"] validAndNotEmptyStringObject] copy];
            _estimatedCompletionDate = [AppManager dateFromString:[[orderMetaInfo[@"EstimatedCompletionDate"] validAndNotEmptyStringObject] copy]];
            _orderPlacedDate = [AppManager dateFromString:[[orderMetaInfo[@"OrderPlacedDate"] validAndNotEmptyStringObject] copy]];
            _orderProductsCount = [orderMetaInfo[@"OrderProductsCount"] integerValue];
            
        }
        else
        {
            DDLogError(@"'OrderMetaInfo' element missing in bundle order summary response ");
        }
        
        NSArray *productArray = [resultDic valueForKey:@"BundleItemsSummary"];
        if([productArray isKindOfClass:[NSArray class]])
        {
            for(NSDictionary *productDic in productArray)
            {
                if(![productDic isKindOfClass:[NSDictionary class]])
                    continue;
                
                NSInteger indexInResponse = [productArray indexOfObject:productDic];
                
                BTProduct *product = [[BTProduct alloc] initProductWithResponse:productDic andIndexInAPIResponse:indexInResponse];
                if(product)
                {
                    [self addProduct:product];
                }
            }
        }
        else
        {
            DDLogError(@"'BundleItemsSummary' element missing in bundle order summary response ");
        }
    }
    return self;
}


#pragma mark - Setter & Getter Methods

- (NSArray *)arrayOfProducts
{
    return [_arrayOfProducts copy];
}

#pragma mark - Public Methods

- (void)addProduct:(BTProduct *)product
{
    if(_arrayOfProducts == nil)
    {
        _arrayOfProducts = [[NSMutableArray alloc] init];
    }
    
    if(product == nil)
        return;
    
    [_arrayOfProducts addObject:product];
}

@end
