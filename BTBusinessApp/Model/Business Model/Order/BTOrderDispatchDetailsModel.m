//
//  BTOrderDispatchDetailsModel.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 21/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderDispatchDetailsModel.h"

@implementation BTOrderDispatchDetailsModel

- (id) init {
    self = [super init];
    if (self) {
        self.dispatchDate = [NSString string];
        self.equipmentNames = [NSString string];
        self.trackingReferenceNumber = [NSString string];
        self.trackingReferenceUrl = [NSString string];
    }
    return self;
}

-(NSArray *)getDispatchDetailsFrom:(NSDictionary *)dispatchDetails
{
    
    NSArray *dispatchDetailsArray = [dispatchDetails objectForKey:@"DispatchDetails"];
    NSMutableArray *toAddDispatchDetailsArray = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dispatchDetail in dispatchDetailsArray) {
        BTOrderDispatchDetailsModel *dispatchDetailModel = [BTOrderDispatchDetailsModel new];
        if ([dispatchDetail objectForKey:@"DispatchDate"]) {
            dispatchDetailModel.dispatchDate = [dispatchDetail objectForKey:@"DispatchDate"];
        }
        if ([dispatchDetail objectForKey:@"EquipmentNames"]) {
            dispatchDetailModel.equipmentNames = [dispatchDetail objectForKey:@"EquipmentNames"];
        }
        if ([dispatchDetail objectForKey:@"TrackingReferenceNumber"]) {
            dispatchDetailModel.trackingReferenceNumber = [dispatchDetail objectForKey:@"TrackingReferenceNumber"];
        }
        if ([dispatchDetail objectForKey:@"TrackingReferenceUrl"]) {
            dispatchDetailModel.trackingReferenceUrl = [dispatchDetail objectForKey:@"TrackingReferenceUrl"];
        }
        
        [toAddDispatchDetailsArray addObject:dispatchDetailModel];
    }
    
    
    return toAddDispatchDetailsArray;
}

@end
