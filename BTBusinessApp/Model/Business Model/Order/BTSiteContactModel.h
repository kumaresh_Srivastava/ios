//
//  BTSiteContactModel.h
//  BTBusinessApp
//
//  Created by Accolite on 19/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTSiteContactModel : NSObject

@property (nonatomic,strong)NSString *Title;
@property (nonatomic,strong)NSString *FirstName;
@property (nonatomic,strong)NSString *LastName;
@property (nonatomic,strong)NSString *WorkPhone;
@property (nonatomic,strong)NSString *Extension;
@property (nonatomic,strong)NSString *Mobile;
@property (nonatomic,strong)NSString *HomePhone;
@property (nonatomic,strong)NSString *Email;
@property (nonatomic,strong)NSString *ContactKey;
@property (nonatomic,strong)NSString *ContactContext;
@property (nonatomic,strong)NSString *PreferredContact;

@end
/*
 "SiteContact": {
 "Title": "Miss",
 "FirstName": "Ad",
 "LastName": "Ad",
 "WorkPhone": "01002892373",
 "Extension": null,
 "Mobile": null,
 "HomePhone": "01002892373",
 "Email": "shruti.anand@bt.com",
 "ContactKey": "820ZDJK894",
 "ContactContext": null,
 "PreferredContact": null
 }
 */
