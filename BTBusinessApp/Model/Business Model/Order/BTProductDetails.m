//
//  BTProductDetails.m
//  BTBusinessApp
//
//  Created by Accolite on 10/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTProductDetails.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"

@implementation BTProductDetails


- (instancetype)initProductDetailsWithResponse:(NSDictionary *)productDetailsDic
{
    self = [super init];
    if(self)
    {
        if (productDetailsDic[@"PhoneNumber"]) {
            
            _phoneNumber = [[productDetailsDic[@"PhoneNumber"] validAndNotEmptyStringObject] copy];
        }
        if (productDetailsDic[@"NetworkUserId"]) {
            
            _networkUserId = [[productDetailsDic[@"NetworkUserId"] validAndNotEmptyStringObject] copy];
        }

        if (productDetailsDic[@"PrimaryEmailAddress"]) {
            
            _primaryEmailAddress = [[productDetailsDic[@"PrimaryEmailAddress"] validAndNotEmptyStringObject] copy];
        }
    }
    return self;
}


@end
