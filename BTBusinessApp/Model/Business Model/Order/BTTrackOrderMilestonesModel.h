//
//  BTTrackOrderMilestonesModel.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 21/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTOrderDispatchDetailsModel.h"
#import "BTOrderSiteContactModel.h"

@interface BTTrackOrderMilestonesModel : NSObject

@property (nonatomic, strong) NSMutableArray *milestoneNodes;
@property (nonatomic, strong) NSMutableArray *dispatchDetails;
@property (nonatomic, strong) BTOrderSiteContactModel *siteContact;
@property (nonatomic, strong) BTOrderSiteContactModel *altSiteContact;
@property (nonatomic,strong) NSString *engineerAppointmentDate;
@property (nonatomic,strong) NSString *activationDate;

@property (nonatomic) BOOL isSiteContactAvailable;
@property (nonatomic) BOOL isAltSiteContactAvailable;
@property (nonatomic) BOOL isEngineerAppointmentDateAvailable;
@property (nonatomic) BOOL isActivationDateAvailable;

-(void)getOrderMilestoneModelFrom:(NSDictionary *)milestoneDetails;

-(NSString *)getSiteContact;
-(NSString *)getAltSiteContact;
-(NSString *)getDispatchDetails;

@end
