//
//  BTTrackOrderDetailsModel.h
//  BTBusinessApp
//
//  Created by Accolite on 12/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTTrackOrderDetailsModel : NSObject
@property(nonatomic,strong)NSNumber *AuthLevel;
@property(nonatomic,assign)BOOL IsPostCodeValidated;
@property(nonatomic,strong)NSDictionary *OrderMetaInfo;
@property(nonatomic,strong)NSDictionary *ProductOrderItems;
@property(nonatomic,strong)NSDictionary *ParentOrderItem;
@property(nonatomic,strong)NSArray *OrderItems;
@property(nonatomic,strong)NSString *TotalMonthlyPrice;
@property(nonatomic,strong)NSNumber *TotalOneOffPrice;
@property(nonatomic,strong)NSDictionary *ProductSummary;

//Local
@property(nonatomic,strong) NSMutableArray *priceOrderItems;
@property(nonatomic,strong) NSMutableArray *groupPriceOrderItems;
@property(nonatomic,strong) NSMutableArray *allOrderItems;

//POSTALCODE VALIDATION
@property(nonatomic,strong)NSNumber *AuthLevel_postCode;
@property(nonatomic,strong)NSDictionary *BillingDetails;
@property(nonatomic,strong)NSDictionary *ProductDetails;

- (NSArray *)getGroupedOrderItems;
- (NSInteger)getNumberOfRowsFromOrderItems;
@end


/*

 {
 AuthLevel = 2;
 
 BillingDetails =     {
 IsBillTypeAmendable = 0;
 };
 
 BundleSummaryPartial =     {
 BundleDescription = "";
 BundleDueDate = "";
 BundleIconClass = "";
 BundleMonthlyPrice = "";
 BundleName = "";
 BundleOrderItemRef = "";
 BundleOrderRef = "";
 BundleStatus = "";
 ShowBundleInfo = 0;
 };
 
 IsPostCodeValidated = 0;
 
 OrderMetaInfo =     {
 AuthLevel = 2;
 CurrentPage = 2;
 EstimatedCompletionDate = "12 Jul 2016";
 IsCONFOrder = 0;
 IsStrategic = 1;
 OrderDescription = "BT Business Broadband & BT Business phone line related order";
 OrderPlacedDate = "29 Jun 2016";
 OrderProductsCount = 2;
 OrderRef = BTDJC662;
 OrderStatus = Completed;
 };
 
 ProductDetails =     {
 };
 
 ProductOrderItems =     {
 ContractProductName = "24 months";
 JourneyType = 0;
 
 OrderItems =         (
 {
 ActionText = Added;
 DisplayName = "Connection charge";
 IsToolTipDisplayable = 0;
 IsVisible = 1;
 MonthlyPrice = 0;
 OneOffPrice = 120;
 ProductCode = S0131485;
 ProductGroupName = "";
 Quantity = 1;
 QuarterlyPrice = 0;
 },
 {
 ActionText = Added;
 DisplayName = "TRC Parameters";
 IsToolTipDisplayable = 0;
 IsVisible = 0;
 MonthlyPrice = 0;
 OneOffPrice = 0;
 ProductCode = S0137054;
 ProductGroupName = "Additional Time Related Charges";
 Quantity = 1;
 QuarterlyPrice = 0;
 },
 {
 ActionText = Added;
 DisplayName = "Prompt Care";
 IsToolTipDisplayable = 0;
 IsVisible = 1;
 MonthlyPrice = 0;
 OneOffPrice = 0;
 ProductCode = S0131494;
 ProductGroupName = "Care plan";
 Quantity = 1;
 QuarterlyPrice = 0;
 },
 {
 ActionText = Added;
 DisplayName = "Directory Entry Business";
 IsToolTipDisplayable = 0;
 IsVisible = 1;
 MonthlyPrice = 0;
 OneOffPrice = 0;
 ProductCode = S0131475;
 ProductGroupName = "Directory listing";
 Quantity = 1;
 QuarterlyPrice = 0;
 }
 );
 
 ParentOrderItem =         {
 ActionText = Added;
 ContractProductDisplayName = "24 months";
 DisplayName = "BT Business Phone line";
 IsToolTipDisplayable = 0;
 IsVisible = 1;
 MonthlyPrice = "21.2";
 OneOffPrice = 0;
 ProductCode = S0144136;
 ProductGroupName = "BT Business PhoneLine";
 Quantity = 1;
 QuarterlyPrice = "63.59999999999999";
 };
 
 ProductDetailsTitle = "What you\U2019ve ordered";
 RegularChargesView = Monthly;
 TotalMonthlyPrice = "21.2";
 TotalOneOffPrice = 120;
 TotalQuarterlyPrice = "63.59999999999999";
 };
 
 
 ProductSummary =     {
 ActiveMilestone = Completion;
 AuthLevel = 0;
 BundleContractTerm = "24 month";
 EngineeringAppointmentDate = "2016-07-06T13:00:00+01:00";
 EngineeringAppointmentSlot = "(1pm - 6pm)";
 ExpectedDispatchDate = "0001-01-01T00:00:00";
 IsBundleProduct = 0;
 IsDelayedOrder = 0;
 IsPreAppointment = 0;
 MonthlyPrice = "21.2";
 OneOffPrice = 120;
 OrderItemReference = "BTDJC6621-11";
 OrderKey = BTDJC662;
 OrderStatusLongDescription = "Your order's now complete.";
 PercentageCompletion = 100;
 ProductDescription = "Provision of BT Business Phone line";
 ProductDuedate = "12 Jul 2016"; //CompletionDate
 ProductIcon = phoneIcon;
 ProductName = "BT Business Phone line";
 ProductType = 1;
 Status = Completed;
 isCeaseOrCancelled = 0;
 };
 
 TimeLineDetails =     (
 {
 Nodes =             (
 {
 AuthLevel = 2;
 Date = "2016-06-29T00:00:00";
 Description = "";
 DisplayName = "Order received";
 IsActiveNode = 0;
 IsFirstActiveNode = 0;
 NodeType = 0;
 },
 {
 AuthLevel = 2;
 Date = "2016-06-29T00:00:00";
 Description = "Your order is OK and is already checked by us.";
 DisplayName = "Order checks";
 IsActiveNode = 0;
 IsFirstActiveNode = 0;
 NodeType = 1;
 },
 {
 AltSiteContactAmendSource = 0;
 AppointmentAmendSource = 0;
 AuthLevel = 2;
 Date = "2016-07-06T13:00:00+01:00";
 Description = "The engineer finished the job on 06 Jul 2016";
 DisplayName = "Engineer appointment";
 IsActiveNode = 0;
 IsFirstActiveNode = 0;
 NodeType = 4;
 SiteContactAmendSource = 0;
 },
 {
 ActivationAmendSource = 0;
 AuthLevel = 2;
 Date = "0001-01-01T00:00:00";
 Description = "Order completed";
 DisplayName = Completion;
 IsActiveNode = 1;
 IsFirstActiveNode = 1;
 NodeType = 5;
 }
 );
 }
 );
 }

*/

/*
 {
 AuthLevel = 1;
 
 BillingDetails =     {
 BillFrequency = Monthly;
 BillType = Paper;
 BillingAddress =         {
 AddressIdentifier = R20154670566;
 BuildingNumber = 22;
 Country = "United Kingdom";
 County = Aberdeenshire;
 PostTown = Aberdeen;
 Postcode = "AB10 1AU";
 ThoroughfareName = Netherkirkgate;
 };
 IsBillTypeAmendable = 0;
 PaymentMethod = "Non direct debit";
 };
 
 BundleSummaryPartial =     {
 BundleDescription = "";
 BundleDueDate = "";
 BundleIconClass = "";
 BundleMonthlyPrice = "";
 BundleName = "";
 BundleOrderItemRef = "";
 BundleOrderRef = "";
 BundleStatus = "";
 ShowBundleInfo = 0;
 };
 
 IsPostCodeValidated = 1;
 
 OrderMetaInfo =     {
 AuthLevel = 1;
 CurrentPage = 2;
 EstimatedCompletionDate = "08 Jul 2016";
 IsCONFOrder = 0;
 IsStrategic = 1;
 OrderDescription = "BT Business Phone line related order";
 OrderPlacedDate = "05 Jul 2016";
 OrderProductsCount = 1;
 OrderRef = BTDJH319;
 OrderStatus = Completed;
 };
 
 ProductDetails =     {
 PhoneNumber = 01003656301;
 };
 
 ProductOrderItems =     {
 ContractProductName = "24 months";
 JourneyType = 0;
 OrderItems =         (
 {
 ActionText = Added;
 DisplayName = "Connection charge";
 IsToolTipDisplayable = 0;
 IsVisible = 1;
 MonthlyPrice = 0;
 OneOffPrice = 120;
 ProductCode = S0131485;
 ProductGroupName = "";
 Quantity = 1;
 QuarterlyPrice = 0;
 },
 {
 ActionText = Added;
 DisplayName = "TRC Parameters";
 IsToolTipDisplayable = 0;
 IsVisible = 0;
 MonthlyPrice = 0;
 OneOffPrice = 0;
 ProductCode = S0137054;
 ProductGroupName = "Additional Time Related Charges";
 Quantity = 1;
 QuarterlyPrice = 0;
 },
 {
 ActionText = Added;
 DisplayName = "Prompt Care";
 IsToolTipDisplayable = 0;
 IsVisible = 1;
 MonthlyPrice = 0;
 OneOffPrice = 0;
 ProductCode = S0131494;
 ProductGroupName = "Care plan";
 Quantity = 1;
 QuarterlyPrice = 0;
 },
 {
 ActionText = Added;
 DisplayName = "Directory Entry Business";
 IsToolTipDisplayable = 0;
 IsVisible = 1;
 MonthlyPrice = 0;
 OneOffPrice = 0;
 ProductCode = S0131475;
 ProductGroupName = "Directory listing";
 Quantity = 1;
 QuarterlyPrice = 0;
 }
 );
 ParentOrderItem =         {
 ActionText = Added;
 ContractProductDisplayName = "24 months";
 DisplayName = "BT Business Phone line";
 IsToolTipDisplayable = 0;
 IsVisible = 1;
 MonthlyPrice = "21.2";
 OneOffPrice = 0;
 ProductCode = S0144136;
 ProductGroupName = "BT Business PhoneLine";
 Quantity = 1;
 QuarterlyPrice = "63.59999999999999";
 };
 ProductDetailsTitle = "What you\U2019ve ordered";
 RegularChargesView = Monthly;
 TotalMonthlyPrice = "21.2";
 TotalOneOffPrice = 120;
 TotalQuarterlyPrice = "63.59999999999999";
 };
 
 ProductSummary =     {
 ActiveMilestone = Completion;
 AuthLevel = 0;
 BundleContractTerm = "24 month";
 EngineeringAppointmentDate = "2016-07-05T08:00:00+01:00";
 EngineeringAppointmentSlot = "(8am - 1pm)";
 ExpectedDispatchDate = "0001-01-01T00:00:00";
 IsBundleProduct = 0;
 IsDelayedOrder = 0;
 IsPreAppointment = 0;
 MonthlyPrice = "21.2";
 OneOffPrice = 120;
 OrderItemReference = "BTDJH3191-1";
 OrderKey = BTDJH319;
 OrderStatusLongDescription = "Your order's now complete.";
 PercentageCompletion = 100;
 ProductDescription = "Provision of BT Business Phone line";
 ProductDuedate = "08 Jul 2016";
 ProductIcon = phoneIcon;
 ProductName = "BT Business Phone line";
 ProductType = 1;
 Status = Completed;
 isCeaseOrCancelled = 0;
 };
 TimeLineDetails =     (
 {
 Nodes =             (
 {
 AuthLevel = 1;
 Date = "2016-07-05T00:00:00";
 Description = "";
 DisplayName = "Order received";
 IsActiveNode = 0;
 IsFirstActiveNode = 0;
 NodeType = 0;
 },
 {
 AuthLevel = 1;
 Date = "2016-07-05T00:00:00";
 Description = "Your order is OK and is already checked by us.";
 DisplayName = "Order checks";
 IsActiveNode = 0;
 IsFirstActiveNode = 0;
 NodeType = 1;
 },
 {
 AltSiteContactAmendSource = 0;
 AppointmentAmendSource = 0;
 AuthLevel = 1;
 Date = "2016-07-05T08:00:00+01:00";
 Description = "The engineer finished the job on 05 Jul 2016";
 DisplayName = "Engineer appointment";
 IsActiveNode = 0;
 IsFirstActiveNode = 0;
 NodeType = 4;
 SiteContactAmendSource = 0;
 },
 {
 ActivationAmendSource = 0;
 AuthLevel = 1;
 Date = "0001-01-01T00:00:00";
 Description = "Order completed";
 DisplayName = Completion;
 IsActiveNode = 1;
 IsFirstActiveNode = 1;
 NodeType = 5;
 }
 );
 }
 );
 }

*/