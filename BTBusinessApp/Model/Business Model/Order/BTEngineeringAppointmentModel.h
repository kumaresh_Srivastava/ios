//
//  BTEngineeringAppointmentModel.h
//  BTBusinessApp
//
//  Created by Accolite on 19/08/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTEngineeringAppointmentModel : NSObject

@property (nonatomic,strong)NSString *AppointmentDate;
@property (nonatomic,strong)NSString *AppointmentSlot;
@property (nonatomic,strong)NSString *AddressKey;
@property (nonatomic,strong)NSString *DBIDValue;
@property (nonatomic,strong)NSString *AppointmentBook;
@end
/*
 "Appointment": {
 "AppointmentDate": null,
 "AppointmentSlot": null,
 "AddressKey": null,
 "DBIDValue": null,
 "AppointmentBook": null
 }
 */
