//
//  BTOrderSiteContactModel.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 21/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTOrderSiteContactModel : NSObject

@property (nonatomic, readonly) NSString *contactContext;
@property (nonatomic, readonly) NSString *contactKey;
@property (nonatomic, readonly) NSString *email;
@property (nonatomic, readonly) NSString *extension;
@property (nonatomic, readonly) NSString *firstName;
@property (nonatomic, readonly) NSString *homePhone;
@property (nonatomic, readonly) NSString *lastName;
@property (nonatomic, readonly) NSString *mobile;
@property (nonatomic, readonly) NSString *preferredContact;
@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *workPhone;
@property (nonatomic, readonly) NSString *phone;
@property (nonatomic, readonly) NSString *formattedContactLine;

- (instancetype)initSiteContactWithResponseDict:(NSDictionary *)siteContactDic;
- (instancetype)initWithResponseDictionaryFromSiteContactsAPIResponse:(NSDictionary *)responseDic;
- (instancetype) initSiteContactWithAddedSiteContactDict:(NSDictionary *)siteContactDic;
- (instancetype)initWithResponseDictionaryFromFaultContactDetailsAPIResponse:(NSDictionary *)responseDic;

@end
