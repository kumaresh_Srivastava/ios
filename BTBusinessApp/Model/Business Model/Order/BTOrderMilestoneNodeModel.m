//
//  BTOrderMilestoneNodeModel.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 21/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrderMilestoneNodeModel.h"
#import "NSObject+APIResponseCheck.h"
#import "AppManager.h"

@implementation BTOrderMilestoneNodeModel

- (instancetype)initMilestoneNodeWithResponseDic:(NSDictionary *)milestoneNode
{
    self = [super init];
    if (self) {
        _authLevel = [[milestoneNode objectForKey:@"AuthLevel"] integerValue];
        _date = [AppManager NSDateWithUTCFormatFromNSString:[[[milestoneNode valueForKey:@"Date"] validAndNotEmptyStringObject] copy]];
        _nodeDescription = [[[milestoneNode valueForKey:@"Description"] validAndNotEmptyStringObject] copy];
        _nodeDisplayName = [[[milestoneNode valueForKey:@"DisplayName"] validAndNotEmptyStringObject] copy];
        _isActiveNode = [[milestoneNode valueForKey:@"IsActiveNode"] boolValue];
        _isFirstActiveNode = [[milestoneNode valueForKey:@"IsFirstActiveNode"] boolValue];
        _nodeType = [[milestoneNode valueForKey:@"NodeType"] integerValue];
        if ([milestoneNode valueForKey:@"DispatchDetails"]) {
            _isDispatchDetailsAvailable = YES;
        }
        if ([milestoneNode valueForKey:@"SiteContact"]) {
            _isSiteContactAvailable = YES;
        }
        if ([milestoneNode valueForKey:@"AlternateSiteContact"]) {
            _isAltSiteContactAvailable = YES;
        }
        if ([[milestoneNode valueForKey:@"AppointmentAmendSource"] integerValue] > 0) {
            _isAppointmentAmendSource = YES;
        }
        if ([[milestoneNode valueForKey:@"SiteContactAmendSource"] integerValue] > 0) {
            _isSiteContactAmendSource = YES;
        }
        if ([[milestoneNode valueForKey:@"AltSiteContactAmendSource"] integerValue] > 0) {
            _isAltSiteContactAmendSource = YES;
        }
        if ([[milestoneNode valueForKey:@"ActivationAmendSource"] integerValue] > 0) {
            _isActivationAmendSource = YES;
        }
    }
    
    return self;
}

- (void)updateNodeDataWithEngineerAppointmentDate:(NSDate *)engineerAppointmentDate
{
    _date = engineerAppointmentDate;
    
    NSString *slot = @"";
    
    NSInteger hour = [AppManager getHourFromDate:engineerAppointmentDate];
    
    if (hour < 13) {
        
         slot = @"(8am - 1pm)";
       
        
    } else{
        
        slot = @"(1pm - 6pm)";
        
    }
    
    _nodeDescription = [NSString stringWithFormat:@"The engineer visit is appointed to %@ %@. Ensure the address and contact information are correct.", [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:engineerAppointmentDate], slot];
}

- (void)updateNodeDataWithEngineerAppointment:(BTAppointment *)appointment
{
    _date = appointment.dateInfo;
    NSString *slot = appointment.selectedTimeslot;
    _nodeDescription = [NSString stringWithFormat:@"The engineer visit is appointed to %@ %@. Ensure the address and contact information are correct.", [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:appointment.dateInfo], slot];
}

- (void)updateNodeDataWithActivationDate:(NSDate *)activationDate
{
    _date = activationDate;
    
    _nodeDescription = [NSString stringWithFormat:@"Order is estimated to be completed at %@.", [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:activationDate]];
}





@end
