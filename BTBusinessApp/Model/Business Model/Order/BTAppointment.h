//
//  BTAppointment.h
//  BTBusinessApp
//
//  Created by Accolite on 03/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTAppointment : NSObject
@property (nonatomic, readonly) NSDate *dateInfo;
@property (nonatomic, readonly) NSDate *date;
@property (nonatomic, readonly) NSString *DayOfWeek;
@property (nonatomic, assign) BOOL hasAM;
@property (nonatomic, assign) BOOL hasPM;
@property (nonatomic, assign) BOOL isAMSelected;
@property (nonatomic, assign) BOOL isPMSelected;
//Salman: Adding this property to hide am selection
//@property (nonatomic, assign) BOOL isServerSelectedAM;

//additonal properties for 2 hour slots
@property (nonatomic, assign) BOOL hasAS1000;
@property (nonatomic, assign) BOOL hasAS1200;
@property (nonatomic, assign) BOOL hasAS1400;
@property (nonatomic, assign) BOOL hasAS1600;
@property (nonatomic, assign) BOOL hasAS1800;
@property (nonatomic, assign) BOOL hasAS2000;
@property (nonatomic, assign) BOOL isAS1000Selected;
@property (nonatomic, assign) BOOL isAS1200Selected;
@property (nonatomic, assign) BOOL isAS1400Selected;
@property (nonatomic, assign) BOOL isAS1600Selected;
@property (nonatomic, assign) BOOL isAS1800Selected;
@property (nonatomic, assign) BOOL isAS2000Selected;
@property (nonatomic, assign) BOOL hasAS0900;
@property (nonatomic, assign) BOOL hasAS1100;
@property (nonatomic, assign) BOOL hasAS1300;
@property (nonatomic, assign) BOOL hasAS1500;
@property (nonatomic, assign) BOOL hasAS1700;
@property (nonatomic, assign) BOOL hasAS1900;
@property (nonatomic, assign) BOOL isAS0900Selected;
@property (nonatomic, assign) BOOL isAS1100Selected;
@property (nonatomic, assign) BOOL isAS1300Selected;
@property (nonatomic, assign) BOOL isAS1500Selected;
@property (nonatomic, assign) BOOL isAS1700Selected;
@property (nonatomic, assign) BOOL isAS1900Selected;

@property (nonatomic, readonly) BOOL isDaySelected;
@property (nonatomic, readonly) NSString *selectedTimeslot;
@property (nonatomic, readonly) NSArray *appointmentSlots;

- (instancetype)initWithResponseDictionaryFromAppointmentsAPIResponse:(NSDictionary *)responseDic;
- (instancetype)initWithDate:(NSDate*)date andTimeslot:(NSString*)timeslot;
- (BOOL)isTimeslotSelected:(NSString*)timeslot;
- (BOOL)hasTimeslot:(NSString*)timeslot;

@end

