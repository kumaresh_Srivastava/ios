//
//  BTBundleOrder.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 13/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTOrder.h"

@interface BTBundleOrder : BTOrder

@property (nonatomic, readonly) double bundleTotalOneOffCharge;
@property (nonatomic, readonly) double bundleExtraCharges;
@property (nonatomic, readonly) double monthlyPrice;

- (instancetype)initBundleOrderWithResponseDictionaryFromBundleOrderSummaryAPIResponse:(NSDictionary *)responseDic;

@end
