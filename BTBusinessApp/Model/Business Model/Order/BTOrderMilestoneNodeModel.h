//
//  BTOrderMilestoneNodeModel.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 21/07/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTAppointment.h"

@interface BTOrderMilestoneNodeModel : NSObject

@property (nonatomic, readonly) NSInteger authLevel;
@property (nonatomic, readonly) NSDate *date;
@property (nonatomic, readonly) BOOL isActiveNode;
@property (nonatomic, readonly) BOOL isFirstActiveNode;
@property (nonatomic, readonly) NSString *nodeDescription;
@property (nonatomic, readonly) NSString *nodeDisplayName;
@property (nonatomic, readonly) NSInteger nodeType;
@property (nonatomic, readonly) BOOL isDispatchDetailsAvailable;
@property (nonatomic, readonly) BOOL isSiteContactAvailable;
@property (nonatomic, readonly) BOOL isAltSiteContactAvailable;
@property (nonatomic, readonly) BOOL isAppointmentAmendSource;
@property (nonatomic, readonly) BOOL isSiteContactAmendSource;
@property (nonatomic, readonly) BOOL isAltSiteContactAmendSource;
@property (nonatomic, readonly) BOOL isActivationAmendSource;

//-(void)getMilestoneNodeDetailsFrom:(NSDictionary *)nodeDetails;
- (instancetype)initMilestoneNodeWithResponseDic:(NSDictionary *)milestoneNode;
- (void)updateNodeDataWithEngineerAppointmentDate:(NSDate *)engineerAppointmentDate;
- (void)updateNodeDataWithEngineerAppointment:(BTAppointment *)appointment;
- (void)updateNodeDataWithActivationDate:(NSDate *)activationDate;


@end
