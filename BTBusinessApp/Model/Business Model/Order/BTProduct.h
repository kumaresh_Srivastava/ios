//
//  BTProduct.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 22/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTProduct : NSObject {
    
}

@property (nonatomic, readonly) NSString *orderKey;
@property (nonatomic, readonly) NSString *orderItemReference;
@property (nonatomic, readonly) NSString *productName;
@property (nonatomic, readonly) NSString *productDescription;
@property (nonatomic, readonly) NSString *productIcon;
@property (nonatomic, readonly) NSString *bundleReference;
@property (nonatomic, readonly) NSDate *productDuedate;
@property (nonatomic, readonly) double oneOffPrice;
@property (nonatomic, readonly) double monthlyPrice;
@property (nonatomic, readonly) NSString *status;
@property (nonatomic, readonly) NSDate *expectedDispatchDate;
@property (nonatomic, readonly) NSDate *engineeringAppointmentDate;
@property (nonatomic, readonly) NSString *engineeringAppointmentSlot;
@property (nonatomic, readonly) BOOL isBundleProduct;
@property (nonatomic, readonly) BOOL isCeaseOrCancelled;
@property (nonatomic, readonly) BOOL isRelatedOrderStrategic;
@property (nonatomic, readonly) NSInteger indexInAPIResponse;
@property (nonatomic, readonly) int productType;
@property (nonatomic, readonly) NSString *installType;


- (instancetype)initProductWithResponse:(NSDictionary *)productDic andIndexInAPIResponse:(NSInteger)indexInAPIResponse;
- (void)updateProductDuedateWithNSDate:(NSDate *)productDueDate;
- (void)updateEngineeringAppointmentDateWithNSDate:(NSDate *)engineeringAppointmentDate;
- (void)updateEngineeringAppointmentSlotWithSlot:(NSString*)engineeringAppointmentSlot;

- (void)updateIsProductRelatedToStrategicOrderWith:(NSDictionary *)orderMetaInfo;

@end
