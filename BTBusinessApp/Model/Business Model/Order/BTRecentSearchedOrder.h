//
//  BTRecentSearchedOrder.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 26/09/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDRecentSearchedOrder;

@interface BTRecentSearchedOrder : NSObject {
    
}

@property (nonatomic, readonly) NSString *orderRef;
@property (nonatomic, readonly) NSDate *lastSearchedDate;
@property (nonatomic, readonly) NSString *orderDescription;
@property (nonatomic, readonly) NSDate *placedOnDate;
@property (nonatomic, readonly) NSDate *completionDate;
@property (nonatomic, readonly) NSString *orderStatus;

- (instancetype)initWithCDRecentSearchedOrder:(CDRecentSearchedOrder *)recentSearchedOrder;

@end
