//
//  BTBundleOrder.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 13/10/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import "BTBundleOrder.h"

@implementation BTBundleOrder

- (instancetype)initBundleOrderWithResponseDictionaryFromBundleOrderSummaryAPIResponse:(NSDictionary *)responseDic
{
    self = [super initOrderWithResponseDictionaryFromBundleOrderSummaryAPIResponse:responseDic];
    if (self) {
        _bundleExtraCharges = [[responseDic valueForKey:@"BundleExtraCharges"] doubleValue];
        _bundleTotalOneOffCharge = [[responseDic valueForKey:@"BundleTotalOneOffCharge"] doubleValue];
        _monthlyPrice = [[[responseDic valueForKey:@"BundleSummary"] valueForKey:@"MonthlyPrice"] doubleValue];
    }
    
    return self;
}

@end
