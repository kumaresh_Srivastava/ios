//
//  NLGetBusinessHubDetails.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 13/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLGetBusinessHubDetails.h"
#import "BTAuthenticationManager.h"
#import "NSObject+APIResponseCheck.h"
#import "NLConstants.h"
#import "BTUser.h"
#import "AppConstants.h"
#import "NLVordelAuthenticationProtectedWebService+MbaasHeader.h"
#import "NLWebServiceError.h"
#import "NLWebService+BackEndAPIErrorHandling.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "AppManager.h"
#import "BTClientProfileDetails.h"
#import "BTClientServiceInstance.h"

#import "NLCheckWorkflowStatus.h"

@interface NLGetBusinessHubDetails () <NLCheckWorkflowStatusDelegate> {
    NSString *_serialNumber;
}

@property (nonatomic) NLCheckWorkflowStatus *checkWorkflowStatusWebService;

@end

@implementation NLGetBusinessHubDetails

- (instancetype)initWithSerialNumber:(NSString *)serialNumber
{
    if (serialNumber == nil || [serialNumber isEqualToString:@""])
    {
        return nil;
    }
    
    NSDictionary *header = [self getMbaasHeader];
    
    NSDictionary *serialNumDict = [NSDictionary dictionaryWithObject:serialNumber forKey:@"hubSerialNumber"];
    NSDictionary *body = [NSDictionary dictionaryWithObject:serialNumDict forKey:@"getBusinessHubDetails"];
    
    NSDictionary *payload = [NSDictionary dictionaryWithObjectsAndKeys:header, @"MobileServiceHeader", body, @"Body", nil];
    
    //NSDictionary *parentPayload = [NSDictionary dictionaryWithObject:payload forKey: @"MobileServiceRequest"];
    
    NSLog(@"GET BUSINESS HUB DETAILS PAYLOAD IS:\n %@", payload);
    
    NSError *errorWhileCreatingJSONData = nil;
    NSData *mbaasHeader = [NSJSONSerialization dataWithJSONObject:payload options:0 error:&errorWhileCreatingJSONData];
    
    if(errorWhileCreatingJSONData)
    {
        DDLogError(@"Error while creating JSON data for getBusinessHubDetails");
    }
    
    NSString *jsonString = [[NSString alloc] initWithData:mbaasHeader encoding:NSUTF8StringEncoding];
    
    NSString *endPointURLString = @"/manageworkflows/getbusinesshubdetails";
    
    self = [super initWithMethod:@"POST" parameters:jsonString andEndpointUrlString:endPointURLString];
    
    if (self)
    {
        _serialNumber = [serialNumber copy];
    }
    return self;
}

- (NSString *)friendlyName
{
    return @"GetBusinessHubDetails";
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSString *executionID = [(NSDictionary*)responseObject valueForKeyPath:@"MobileServiceResponse.Body.getBusinessHubDetailsResponse.executionId"];
    if (executionID && ![executionID isEqualToString:@""]) {
        self.checkWorkflowStatusWebService = [[NLCheckWorkflowStatus alloc] initWithExecutionId:executionID];
        self.checkWorkflowStatusWebService.checkWorkflowStatusDelegate = self;
        [self.checkWorkflowStatusWebService resume];
    } else {
        NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
        NLWebServiceError *errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
        DDLogError(@"'getBusinessHubDetailsResponse' missing execution ID in Body of response object for URL %@", [[task currentRequest] URL]);
        [self.getBusinessHubDetailsDelegate getBusinessHubDetailsWebService:self failedToFetchHubDetailsWithWebServiceError:errorToBeReturned];
    }    
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    [self.getBusinessHubDetailsDelegate getBusinessHubDetailsWebService:self failedToFetchHubDetailsWithWebServiceError:webServiceError];
}

#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        
        if([responseObject isValidVordelAPIResponseObject])
        {
            NSDictionary *response = [responseObject valueForKeyPath:@"MobileServiceResponse.Body.getBusinessHubDetailsResponse"];
            if (response) {
                if (![response objectForKey:@"executionId"])
                {
                    NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
                    errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
                    DDLogError(@"'getBusinessHubDetailsResponse' missing execution ID in Body of response object for URL %@", [[task currentRequest] URL]);
                }
            } else {
                NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
                errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
                DDLogError(@"'getBusinessHubDetailsResponse' element is missing or format is unexpected in Body of response object for URL %@", [[task currentRequest] URL]);
            }
        }
        else
        {
            NSError *newErrorObject = [NSError errorWithDomain:BTNetworkErrorDomain code:BTNetworkErrorCodeInvalidResponseObject userInfo:nil];
            
            errorToBeReturned = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
            DDLogError(@"Invalid Response Object for URL %@", [[task currentRequest] URL]);
        }
    }
    
    return errorToBeReturned;
}

#pragma mark - overwrite cancel method to ensure child webservice also cancelled

- (void)cancel
{
    if (self.checkWorkflowStatusWebService) {
        [self.checkWorkflowStatusWebService cancel];
        self.checkWorkflowStatusWebService.checkWorkflowStatusDelegate = nil;
        self.checkWorkflowStatusWebService = nil;
    }
    [super cancel];
}

#pragma mark - CheckWorkflowStatus Delegate

- (void)checkWorkflowStatusWebService:(NLCheckWorkflowStatus *)webService finishedWithResponse:(NSObject *)response
{
    [self.getBusinessHubDetailsDelegate getBusinessHubDetailsWebService:self successfullyFetchedHubDetails:response];
}

- (void)checkWorkflowStatusWebService:(NLCheckWorkflowStatus *)webService failedWithWebServiceError:(NLWebServiceError *)error
{
    [self.getBusinessHubDetailsDelegate getBusinessHubDetailsWebService:self failedToFetchHubDetailsWithWebServiceError:error];
}

@end
