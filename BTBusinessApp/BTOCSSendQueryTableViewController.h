//
//  BTOCSSendQueryTableViewController.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 13/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTOCSBaseTableViewController.h"

@class BTOCSSendQueryTableViewController;

@protocol BTOCSSendQueryTableViewControllerDelegate <NSObject>

@optional
- (void)querySentFromForm:(BTOCSSendQueryTableViewController*)queryForm;

@end

@interface BTOCSSendQueryTableViewController : BTOCSBaseTableViewController

@property (strong, nonatomic) BTOCSProject *project;
@property (strong, nonatomic) BTOCSSite *selectedSite;
@property (strong, nonatomic) BTOCSEnquiry *existingEnquiry;
@property (nonatomic) id<BTOCSSendQueryTableViewControllerDelegate> delegate;

+ (BTOCSSendQueryTableViewController *)getOCSSendQueryViewController;

@end
