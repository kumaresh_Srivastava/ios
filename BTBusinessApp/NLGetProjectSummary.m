//
//  NLGetProjectSummary.m
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLGetProjectSummary.h"
#import "NSObject+APIResponseCheck.h"
#import "NLWebServiceError.h"
#import "NLConstants.h"
#import "NLWebService+BackEndAPIErrorHandling.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "BTCug.h"
#import "CDCug+CoreDataClass.h"

@implementation NLGetProjectSummary

- (instancetype)initWithProjectRef:(NSString *)projectRef
{
    NSMutableDictionary *projectDetails = [NSMutableDictionary new];
    [projectDetails setObject:projectRef forKey:@"ProjectRef"];
    [projectDetails setObject:@"BTBusinessApp" forKey:@"Source"];
    [projectDetails setObject:@"" forKey:@"UniqueKey"];
    
    self = [self initWithProjectDetails:projectDetails andReturnDataType:@"2"];
    return self;
}

- (instancetype)initWithProjectDetails:(NSDictionary*)projectDetails andReturnDataType:(NSString *)returnDataType {
    NSString *contactId = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.contactId;
    
    NSString *endPointURLString =[NSString stringWithFormat:@"/bt-business-auth/v1/ocs-orders/project-summary/%@?src=%@&rtd=%@&Cid=%@",
                                  projectDetails[@"ProjectRef"],
                                  projectDetails[@"Source"],
                                  returnDataType,
                                  contactId];
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    
    return self;
}

- (NSString *)friendlyName
{
    return @"getProjectSummary";
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    if ([responseObject valueForKey:@"Project"])
    {
        [self.getProjectSummaryDelegate getProjectSummaryWebService:self finishedWithResponse:responseObject];
    }
    else
    {
        NLWebServiceError *webServiceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponse];
        
        DDLogError(@"Invalid response json object in webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Invalid response json object in webservice with URL: %@", [[task currentRequest] URL]]));
        
        [self.getProjectSummaryDelegate getProjectSummaryWebService:self failedWithWebServiceError:webServiceError];
    }
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.getProjectSummaryDelegate getProjectSummaryWebService:self failedWithWebServiceError:webServiceError];
}

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *error = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task
                                                                                     andResponseObject:responseObject];
    return error;
}



@end
