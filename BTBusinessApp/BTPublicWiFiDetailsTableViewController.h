//
//  BTPublicWiFiDetailsTableViewController.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 19/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTClientServiceInstance;

@interface BTPublicWiFiDetailsTableViewController : UITableViewController

- (instancetype)initWithClientServiceInstance:(BTClientServiceInstance*)instance;

@end
