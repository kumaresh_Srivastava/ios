//
//  NLSubmitProjectQuestions.m
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLSubmitProjectQuestions.h"
#import "NSObject+APIResponseCheck.h"
#import "NLWebServiceError.h"
#import "NLConstants.h"
#import "NLWebService+BackEndAPIErrorHandling.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "BTCug.h"
#import "CDCug+CoreDataClass.h"

@implementation NLSubmitProjectQuestions

- (instancetype)initWithProjectRef:(NSString*)projectRef questionGroupActivitiyID:(NSInteger)activityID questionGroupID:(NSInteger)groupID questionResponses:(NSArray*)responses
{
    NSString *contactId = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.contactId;
    
    NSMutableDictionary *submitProjectQuestions = [NSMutableDictionary new];
    [submitProjectQuestions setObject:projectRef forKey:@"projectRef"];
    [submitProjectQuestions setObject:@"BTBusinessApp" forKey:@"source"];
    [submitProjectQuestions setObject:@"" forKey:@"uniqueKey"];
    [submitProjectQuestions setObject:contactId forKey:@"customerContactId"];
    
    NSDictionary *questionResponses = @{@"questionResponse":responses};
    
    NSMutableDictionary *projectDetailRequest = [NSMutableDictionary new];
    [projectDetailRequest setObject:submitProjectQuestions forKey:@"submitProjectQuestions"];
    [projectDetailRequest setObject:[NSNumber numberWithInteger: activityID] forKey:@"questionGroupActivityId"];
    [projectDetailRequest setObject:[NSNumber numberWithInteger:groupID] forKey:@"questionGroupId"];
    [projectDetailRequest setObject:questionResponses forKey:@"questionResponses"];
    
    self = [self initWithProjectDetails:projectDetailRequest];
    return self;
}
- (instancetype)initWithProjectDetails:(NSDictionary*)projectDetailRequest {
    
    NSError *errorWhileCreatingJSONData = nil;
    NSData *payload = [NSJSONSerialization dataWithJSONObject:projectDetailRequest options:0 error:&errorWhileCreatingJSONData];
    
    NSString *jsonString = [[NSString alloc] initWithData:payload encoding:NSUTF8StringEncoding];

    NSString *endPointURLString = @"/bt-business-auth/v1/ocs-orders/project-questions";
    
    self = [super initWithMethod:@"POST" parameters:jsonString andEndpointUrlString:endPointURLString];
    return self;
}

- (NSString *)friendlyName
{
    return @"submitProjectQuestions";
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSString *statusValue = [[responseObject valueForKey:@"ReturnStatusInfo"] valueForKey:@"StatusValue"];
    
    if ([statusValue  isEqual: @"Success"])
    {
        [self.submitProjectQuestionsDelegate submitProjectQuestionsWebService:self finishedWithResponse:responseObject];
    }
    else
    {
        NLWebServiceError *webServiceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponse];
        
        DDLogError(@"Unsuccessful response from webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Unsuccessful response from webservice with URL: %@", [[task currentRequest] URL]]));
        
        [self.submitProjectQuestionsDelegate submitProjectQuestionsWebService:self failedWithWebServiceError:webServiceError];
    }
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.submitProjectQuestionsDelegate submitProjectQuestionsWebService:self failedWithWebServiceError:webServiceError];
}

@end
