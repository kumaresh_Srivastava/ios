//
//  BTShadowedView.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 08/11/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTShadowedView.h"

@implementation BTShadowedView

+ (Class)layerClass {
    return [MDCShadowLayer class];
}

- (MDCShadowLayer *)shadowLayer {
    return (MDCShadowLayer *)self.layer;
}

- (void)setElevation:(CGFloat)points {
    [(MDCShadowLayer *)self.layer setElevation:points];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
