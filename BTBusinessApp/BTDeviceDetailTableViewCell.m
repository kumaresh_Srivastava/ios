//
//  BTDeviceDetailTableViewCell.m
//  BTBusinessApp
//
//  Created by kumaresh shrivastava on 04/07/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import "BTDeviceDetailTableViewCell.h"

@implementation BTDeviceDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)upadateWithRowWrapper:(MobileAssetsDetailRowWrapper *)rowWrapper{
    
    self.mobileDeviceNameValueLable.text = rowWrapper.mobileDevice;
    self.mobileDeviceIMEIValueLable.text = rowWrapper.imieNumber;
}

@end
