//
//  BTOCSProjectNotesTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSProjectNotesTableViewCell.h"

@interface BTOCSProjectNotesTableViewCell ()

@property (strong, nonatomic) IBOutlet BTShadowedView *shadowedView;
@property (nonatomic, strong) NSLayoutConstraint *collapseConstraint;
@property (nonatomic) BOOL isShowMoreExpanded;

@end

@implementation BTOCSProjectNotesTableViewCell

+ (NSString *)reuseId
{
    return @"BTOCSProjectNotesTableViewCell";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.shadowedView setElevation:2.0f];
    [self.showMoreButton addTarget:self action:@selector(showMoreButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.collapseConstraint = [NSLayoutConstraint constraintWithItem:self.detailLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationLessThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:105.0f];
    [self.detailLabel addConstraint:self.collapseConstraint];
    self.isShowMoreExpanded = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)showMoreButtonAction
{
    if (self.isShowMoreExpanded) {
        [self.showMoreButton setTitle:@"Show more" forState:UIControlStateNormal];
        [self.detailLabel addConstraint:self.collapseConstraint];
        self.isShowMoreExpanded = NO;
    } else {
        [self.showMoreButton setTitle:@"Show less" forState:UIControlStateNormal];
        [self.detailLabel removeConstraint:self.collapseConstraint];
        self.isShowMoreExpanded = YES;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(layoutChangeInCell:)]) {
        [self.delegate layoutChangeInCell:self];
    }
}

@end
