//
//  NLGetProjectsByCUG.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 19/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLAPIGEEAuthenticatedWebService.h"

NS_ASSUME_NONNULL_BEGIN

@interface NLGetProjectsByCUG : NLAPIGEEAuthenticatedWebService

- (instancetype)initWithCUGID:(NSString*)cugId;
- (instancetype)initWithCUG:(NSString*)cug;

@end

NS_ASSUME_NONNULL_END
