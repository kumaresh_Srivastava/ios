//
//  NLGetProjectDetails.h
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLAPIGEEAuthenticatedWebService.h"

@class NLGetProjectDetails;
@class NLWebServiceError;

@protocol NLGetProjectDetailsDelegate <NLAPIGEEAuthenticationProtectedWebServiceDelegate>

- (void)getProjectDetailsWebService:(NLGetProjectDetails *)webService finishedWithResponse:(NSObject *)response;

- (void)getProjectDetailsWebService:(NLGetProjectDetails *)webService failedWithWebServiceError:(NLWebServiceError *)error;

@optional
- (void)timedOutWithoutSuccessForGetProjectDetailsWebService:(NLGetProjectDetails*)webService;

@end

@interface NLGetProjectDetails : NLAPIGEEAuthenticatedWebService

@property (nonatomic) NSDictionary* projectDetails;

@property (nonatomic, weak) id <NLGetProjectDetailsDelegate> getProjectDetailsDelegate;

- (instancetype)initWithProjectRef:(NSString *)projectRef andSiteId:(NSString*)siteId;

- (instancetype)initWithProjectDetails:(NSDictionary*)projectDetails;

@end
