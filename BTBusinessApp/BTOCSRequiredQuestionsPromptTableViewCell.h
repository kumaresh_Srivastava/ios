//
//  BTOCSRequiredQuestionsPromptTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 16/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface BTOCSRequiredQuestionsPromptTableViewCell : BTBaseTableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *icon;
@property (strong, nonatomic) IBOutlet UILabel *message;

@end

NS_ASSUME_NONNULL_END
