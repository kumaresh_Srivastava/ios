//
//  BTUnableToDetectHubTableViewCell.m
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTUnableToDetectHubTableViewCell.h"
#import "AppConstants.h"
#import "AppManager.h"

@interface BTUnableToDetectHubTableViewCell () <UITextViewDelegate>

@end

@implementation BTUnableToDetectHubTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)upgradePressed:(id)sender {
    [self.BTUnableToDetectHubTableViewCellDelegate userPressedCellForUnableToDetectHub:self];
}

- (void)setupCellForContext:(BTUnableToDetectType)context
{
    UIFont *regularFont = [UIFont fontWithName:kBtFontRegular size:16.0];
    
    NSString *findOutMoreString;
    
    switch(context) {
        case BTUnableToDetect4GAssure:
            findOutMoreString = @"We're having trouble detecting your hub right now, which means you won't be able to see your service status.\n\nFind out more";
            break;
        case BTUnableToDetectPublicWifi:
            findOutMoreString = @"We're having trouble detecting your hub right now, which means you won't be able to manage your Public Wi-Fi.\n\nFind out more";
            break;
        default:
            findOutMoreString = @"We're having trouble detecting your hub right now, which means you won't be able to manage your service status.\n\nFind out more";
            break;
    }
    
    NSMutableAttributedString *mutableFindOutMoreString = [[NSMutableAttributedString alloc] initWithString:findOutMoreString attributes:@{NSFontAttributeName:regularFont}];
    NSRange findOutMoreRange = [findOutMoreString rangeOfString:@"Find out more"];
    [mutableFindOutMoreString addAttribute:NSLinkAttributeName value:[NSURL URLWithString:@"https://btbusiness.custhelp.com/app/answers/detail/a_id/46674?s_intcid=btb_intlink_busapp_find_out_more"] range:findOutMoreRange];
    
    // Make find out more into a link
    self.detailTextView.attributedText = mutableFindOutMoreString;
    self.detailTextView.scrollEnabled = NO;
    self.detailTextView.editable = NO;
    self.detailTextView.selectable = YES;
    self.detailTextView.delegate = self;
    self.detailTextView.userInteractionEnabled = YES;
    self.detailTextView.allowsEditingTextAttributes = NO;
}

@end
