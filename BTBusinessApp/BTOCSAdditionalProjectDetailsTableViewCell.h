//
//  BTOCSAdditionalProjectDetailsTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 13/11/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface BTOCSAdditionalProjectDetailsTableViewCell : BTBaseTableViewCell
@property (strong, nonatomic) IBOutlet UILabel *headerLabel;
@property (strong, nonatomic) IBOutlet UILabel *boldLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *valueLabel;
@property (strong, nonatomic) IBOutlet BTShadowedView *shadowedView;

@end

NS_ASSUME_NONNULL_END
