//
//  BTOCSLargeTextEntryTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 04/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSLargeTextEntryTableViewCell.h"

@interface BTOCSLargeTextEntryTableViewCell () <UITextViewDelegate>

@end

@implementation BTOCSLargeTextEntryTableViewCell

+ (NSString *)reuseId
{
    return @"BTOCSLargeTextEntryTableViewCell";
}

- (void)setHidesToolbar:(BOOL)hidesToolbar
{
    _hidesToolbar = hidesToolbar;
    if (hidesToolbar) {
        self.entryTextView.inputAccessoryView = nil;
    } else {
        if (!self.entryTextView.inputAccessoryView) {
            UIToolbar *keyboardAccesory = [[UIToolbar alloc] init];
            UIBarButtonItem *prevBtn = [[UIBarButtonItem alloc] initWithTitle:@"Previous" style:UIBarButtonItemStylePlain target:self action:@selector(prevButtonAction)];
            UIBarButtonItem *spacing = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
            UIBarButtonItem *nextBtn = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextButtonAction)];
            keyboardAccesory.items = @[prevBtn,spacing,nextBtn];
            [keyboardAccesory sizeToFit];
            self.entryTextView.inputAccessoryView = keyboardAccesory;
        }
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.entryTextView.layer.borderWidth = 1.0f;
    self.entryTextView.layer.borderColor = [BrandColours colourBtLightBlack].CGColor;
    self.entryTextView.text = @"";
    self.entryTextView.scrollEnabled = NO;
    self.entryTextView.delegate = self;
    
    if (!self.hidesToolbar) {
        UIToolbar *keyboardAccesory = [[UIToolbar alloc] init];
        UIBarButtonItem *prevBtn = [[UIBarButtonItem alloc] initWithTitle:@"Previous" style:UIBarButtonItemStylePlain target:self action:@selector(prevButtonAction)];
        UIBarButtonItem *spacing = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *nextBtn = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextButtonAction)];
        keyboardAccesory.items = @[prevBtn,spacing,nextBtn];
        [keyboardAccesory sizeToFit];
        self.entryTextView.inputAccessoryView = keyboardAccesory;
    }
    
    
    [self updateCharactersLabel];
}

- (void)setMaxLength:(NSInteger)maxLength
{
    _maxLength = maxLength;
    [self updateCharactersLabel];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCharactersLabel
{
    if (self.maxLength && (self.maxLength > 0)) {
        if (self.charactersLabel.hidden) {
            self.charactersLabel.hidden = NO;
        }
        self.charactersLabel.text = [NSString stringWithFormat:@"%li/%li",self.entryTextView.text.length,self.maxLength];
    } else {
        self.charactersLabel.hidden = YES;
    }
}

- (void)prevButtonAction
{
    [self.entryTextView resignFirstResponder];
    if (self.delegate && [self.delegate respondsToSelector:@selector(previousButtonPressedOnCell:)]) {
        [self.delegate previousButtonPressedOnCell:self];
    }
}

- (void)nextButtonAction
{
    [self.entryTextView resignFirstResponder];
    if (self.delegate && [self.delegate respondsToSelector:@selector(nextButtonPressedOnCell:)]) {
        [self.delegate nextButtonPressedOnCell:self];
    }
}

#pragma mark - uitextview delegate

- (void)textViewDidChange:(UITextView *)textView
{
    [self updateCharactersLabel];
    //[self.contentView setNeedsLayout];
    if (self.delegate && [self.delegate respondsToSelector:@selector(cell:updatedTextEntry:)]) {
        [self.delegate cell:self updatedTextEntry:self.entryTextView.text];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    BOOL retVal = YES;
    
    // check max length
    if (self.maxLength && (self.maxLength > 0)) {
        NSString *newString = [self.entryTextView.text stringByReplacingCharactersInRange:range withString:text];
        if (newString.length > self.maxLength) {
            retVal = NO;
        }
    }
    
    return retVal;
}

@end
