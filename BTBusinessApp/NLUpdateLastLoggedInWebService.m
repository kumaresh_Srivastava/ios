//
//  NLUpdateLastLoggedInWebService.m
//  BTBusinessApp
//
//  Created by SaiCharan Movva on 25/04/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLUpdateLastLoggedInWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "NLWebServiceError.h"
#import "AFNetworking.h"


@interface NLUpdateLastLoggedInWebService ()

@property (weak, nonatomic) NSString *firstGroupKey;

@end


@implementation NLUpdateLastLoggedInWebService


-(instancetype)initWithVersionDetailsDictionary:(NSDictionary *)versionDetailsDictionary andGroupKey:(NSString *)groupKey{
    
    self.firstGroupKey = groupKey;
    
    NSError *errorWhileCreatingJSONData = nil;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:versionDetailsDictionary options:0 error:&errorWhileCreatingJSONData];
    
    if(errorWhileCreatingJSONData)
    {
        DDLogError(@"Error while creating JSON data for VersionDetailsDictionary - %@",errorWhileCreatingJSONData.localizedDescription);
    }
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:4];
    NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v3/ServiceApp/Update"];
    
    self = [super initWithMethod:@"POST" parameters:jsonString andEndpointUrlString:endPointURLString];
    
    if (self) {
        
    }
    return self;
}

#pragma mark - Override Helper Methods For Prepration Of The NSURLSessionTask

- (AFHTTPSessionManager *)sessionManager
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSString *baseURLString = [NSString stringWithFormat:@"https://%@", [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel]];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseURLString] sessionConfiguration:configuration];
    
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    [manager.requestSerializer setTimeoutInterval:_timeOutTimeInterval];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // (hd) Because the API call expects the parameters in raw format, hence we are setting an empty query string serializer so that it does not do any serialization.
    [manager.requestSerializer setQueryStringSerializationWithBlock:^NSString * _Nonnull(NSURLRequest * _Nonnull request, id  _Nonnull parameters, NSError * _Nullable __autoreleasing * _Nullable error) {
        
        return parameters;
    }];
    
    return manager;
}


- (NSDictionary *)httpHeaderFields
{
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *finalHeaderFieldDic = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    
    [finalHeaderFieldDic setValue:self.firstGroupKey forKey:@"_authKey"];
    
    return [finalHeaderFieldDic copy];
}


#pragma mark - NLWebService Response Handling Private Methods
- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    NSDictionary *resultDict = [responseObject valueForKey:@"result"];
    
    if(resultDict)
    {
        DDLogInfo(@"Successfully submitted version details using webservice with URL: %@", [[task currentRequest] URL]);
        
        [self.updateLastLoggedInWebServiceDelegate versionSubmissionSuccesfullyFinished:self];
    }
    else {
        NLWebServiceError *webServiceError = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
        
        DDLogError(@"Invalid response object in response of webservice with URL: %@", [[task currentRequest] URL]);
        
        BTServerErrorLogger(([[[task currentRequest] URL] absoluteString]), ([NSString stringWithFormat:@"Invalid response object in response of webservice with URL: %@", [[task currentRequest] URL]]));
        
        [self.updateLastLoggedInWebServiceDelegate versionSubmitWebService:self failedToSubmitVersionDetailsWithWebServiceError:webServiceError];
    }
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.updateLastLoggedInWebServiceDelegate versionSubmitWebService:self failedToSubmitVersionDetailsWithWebServiceError:webServiceError];
}


#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    
    NLWebServiceError *errorToBeReturned = nil;
    errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        NSDictionary *resultDict = [responseObject valueForKey:@"result"];
        if(![resultDict isKindOfClass:[NSDictionary class]])
        {
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
            
            DDLogError(@"'result' element is either missing or not in correct format in the response object for URL %@", [[task currentRequest] URL]);
            
        }
    }
    return errorToBeReturned;
}

@end
