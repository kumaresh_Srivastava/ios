//
//  BTAppointmentPickerViewController.h
//  ApptPicker
//
//  Created by Gareth Vaughan on 15/11/2017.
//  Copyright © 2017 Gareth Vaughan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseViewController.h"
#import "BTAppointmentPickerCollectionViewCell.h"
#import "BTAppointmentPickerTableViewCell.h"
#import "BTAppointment.h"

@class BTAppointmentPickerViewController;

@protocol BTAppointmentPickerViewControllerDelegate <NSObject>

@optional
- (void)btAmendEngineerViewController:(BTAppointmentPickerViewController *)controller didSelectAppointment:(BTAppointment *)appointment;

@end

@interface BTAppointmentPickerViewController : BTBaseViewController <UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView *dayPickerCollectionView;
@property (strong, nonatomic) IBOutlet UITableView *timePickerTableView;

@property (nonatomic, assign) id<BTAppointmentPickerViewControllerDelegate> delegate;
@property (nonatomic, assign) BTAppointment *initialAppointment;
@property (strong, nonatomic) NSMutableArray *appointments;
@property (strong, nonatomic) NSDate *appointmentDate;

@property (nonatomic,assign) BOOL isFaultAccessed;
@property (nonatomic,assign) BOOL pcpOnly;
@property (strong, nonatomic) NSString *SIM2OrderRef;
@property (nonatomic,assign) BOOL isSIM2Order;


+ (BTAppointmentPickerViewController*)getBTAppointmentPickerViewController;

@end
