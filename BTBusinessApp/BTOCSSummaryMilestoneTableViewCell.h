//
//  BTOCSSummaryMilestoneTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 05/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@protocol BTOCSSummaryMilestoneTableViewCellDelegate <NSObject>

- (void)layoutChangeInCell:(BTBaseTableViewCell*)cell;

@end

@interface BTOCSSummaryMilestoneTableViewCell : BTBaseTableViewCell

@property (strong, nonatomic) IBOutlet UILabel *headerLabel;
@property (strong, nonatomic) IBOutlet UIButton *actionButton;

@property (strong, nonatomic) IBOutlet UIView *topSeparatorView;
@property (strong, nonatomic) IBOutlet UIView *bottomSeparatorView;

@property (strong, nonatomic) IBOutlet UIView *expandedView;
@property (strong, nonatomic) IBOutlet UILabel *expandedLabel;
@property (strong, nonatomic) IBOutlet UIView *triangleView;

@property (strong, nonatomic) IBOutlet UILabel *targetDateTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *targetDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *completionDateTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *completionDateLabel;
@property (strong, nonatomic) IBOutlet BTOrderStatusLabel *statusLabel;

@property (nonatomic) id<BTOCSSummaryMilestoneTableViewCellDelegate> delegate;

- (void)updateWithStartDate:(NSDate*)startDate dueDate:(NSDate*)dueDate completionDate:(NSDate*)completionDate;

@end

NS_ASSUME_NONNULL_END
