//
//  BTBackgroundMessageTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 07/11/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBackgroundMessageTableViewCell.h"

@implementation BTBackgroundMessageTableViewCell

+ (NSString *)reuseId
{
    return @"BTBackgroundMessageTableViewCell";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateWithLargeText:(NSString *)largeText andDescription:(NSString *)descriptionText
{
    self.largeTextLabel.text = largeText;
    self.descriptionTextLabel.text = descriptionText;
}

- (void)updateWithLargeText:(NSString *)largeText andAttributedDescription:(NSAttributedString *)descriptionText
{
    self.largeTextLabel.text = largeText;
    self.descriptionTextLabel.attributedText = descriptionText;
}

@end
