#import "SpeedTestIntroductionViewController.h"
#import "SpeedTestUpgradeHubViewController.h"
#import "BTRetryView.h"
#import "CustomSpinnerView.h"
#import "BTServiceStatusDashboardViewController.h"
#import "OmnitureManager.h"
#import "AppManager.h"

@interface SpeedTestIntroductionViewController () <BTRetryViewDelegate>

@property (nonatomic,strong)  CustomSpinnerView *loadingView;

@end

@implementation SpeedTestIntroductionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"Broadband speed test";
    
    [self trackPageForOmniture:OMNIPAGE_SPEED_TEST_TEST_BROADBAND_SPEED];
}

#pragma mark - UI methods

- (void)showErrorViewWithDoneButton {
    BTRetryView *errorView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
    errorView.retryViewDelegate = self;
    errorView.translatesAutoresizingMaskIntoConstraints = NO;
    [errorView updateRetryViewWithErrorHeadline:@"Please contact your administrator" errorDetail:@"You'll need billing access rights to continue.\n\nIf your account is currently being approved, please try again later." andButtonText:@"Done"];
    
    [self.view addSubview:errorView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:errorView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:errorView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:errorView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}

- (void) showLoadingView {
    if(!_loadingView){
        _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.view addSubview:_loadingView];
    }
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}

- (void) hideLoadingView {
    [_loadingView setHidden:YES];
}

- (IBAction)dontHaveSmartHubPressed:(id)sender {
    SpeedTestUpgradeHubViewController *upgradeHubViewController = [SpeedTestUpgradeHubViewController getSpeedTestUpgradeHubViewController];
    [self.navigationController pushViewController:upgradeHubViewController animated:YES];
}

# pragma mark - UI response methods

- (IBAction)testMySpeedButtonPressed:(id)sender {
    BTServiceStatusDashboardViewController *dashboardController = [BTServiceStatusDashboardViewController getServiceStatusDashboardViewController];
    NSArray *sortedArrayOfCugs = [_cugs sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"indexInAPIResponse" ascending:YES]]];
    [dashboardController setCugs:sortedArrayOfCugs];
    [dashboardController setForSpeedTest:YES];
    [dashboardController setTitle:@"Broadband speed test"];
    [dashboardController.navigationItem.leftBarButtonItem setTitle:@""];
    [self.navigationController pushViewController:dashboardController animated:YES];
    [self trackPageForOmniture:OMNIPAGE_SPEED_TEST_SELECT_ACCOUNT];
    [self trackOmniClick:OMNICLICK_SPEED_TEST_START_SPEED_TEST];
}

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView {
    // Send the user back home
    [self.navigationController popViewControllerAnimated:YES];
}



# pragma mark - omniture methods

- (void)trackPageForOmniture:(NSString*)pageName
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
   
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackOmniClick:(NSString *)linkTitle
{
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",OMNIPAGE_SPEED_TEST_TEST_BROADBAND_SPEED,linkTitle]];
}

@end
