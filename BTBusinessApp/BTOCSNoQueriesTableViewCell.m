//
//  BTOCSNoQueriesTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 18/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSNoQueriesTableViewCell.h"

@implementation BTOCSNoQueriesTableViewCell

+ (NSString *)reuseId
{
    return @"BTOCSNoQueriesTableViewCell";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
