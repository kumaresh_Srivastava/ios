//
//  BTOCSProjectNotesTableViewController.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 07/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTOCSBaseTableViewController.h"

@interface BTOCSProjectNotesTableViewController : BTOCSBaseTableViewController

@property (strong, nonatomic) BTOCSProject *project;
@property (strong, nonatomic) BTOCSSite *selectedSite;

+ (BTOCSProjectNotesTableViewController *)getOCSProjectNotesViewController;

@end
