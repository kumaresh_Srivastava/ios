//
//  BTOCSRerquiredQuestionsTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

@interface BTOCSRerquiredQuestionsTableViewCell : BTBaseTableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *iconView;
@property (strong, nonatomic) IBOutlet UILabel *detailLabel;
@property (strong, nonatomic) IBOutlet UIImageView *disclosureIconView;

- (void)updateWithAnswered:(NSString*)answered ofTotal:(NSString*)total;

@end
