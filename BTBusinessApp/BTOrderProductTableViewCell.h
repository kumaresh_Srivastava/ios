//
//  BTOrderProductTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 27/11/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface BTOrderProductTableViewCell : BTBaseTableViewCell
@property (strong, nonatomic) IBOutlet UILabel *productTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *firstSubLabel;
@property (strong, nonatomic) IBOutlet UILabel *secondSubLabel;
@property (strong, nonatomic) IBOutlet UILabel *thirdSubLabel;

- (void)updateLabel:(UILabel*)label withBoldText:(NSString*)boldString andRegularText:(NSString*)regularString;

@end

NS_ASSUME_NONNULL_END
