//
//  BTWhatsNewViewController.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 01/03/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTWhatsNewViewController.h"
#import "AppConstants.h"
#import "AppManager.h"
#import "OmnitureManager.h"

@interface BTWhatsNewViewController ()

@end

@implementation BTWhatsNewViewController

+ (BTWhatsNewViewController *)getWhatsNewViewController {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"CloudVoice" bundle:nil];
    BTWhatsNewViewController *controller = (BTWhatsNewViewController *)[storyboard instantiateViewControllerWithIdentifier:@"BTWhatsNewID"];
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self trackPageForOmniture];
    
    self.btnGotItSuperView.layer.cornerRadius = 5.0;
    self.btnGotItContainerView.layer.cornerRadius = 5.0;
    self.btnGotIt.layer.cornerRadius = 5.0;
    
    
//    [self.whatsNewDetail setText:@"Free wi-fi for your guests.\nSecurity and speed protected for you."];
//    [self.whatsNewDetail setFont:[UIFont fontWithName:kBtFontRegular size:18.0]];
//    [self.whatsNewImageview.layer setCornerRadius:((MAX(self.whatsNewImageview.frame.size.height, self.whatsNewImageview.frame.size.width))/2)];
//    [self.whatsNewActionButton.layer setCornerRadius:self.whatsNewActionButton.frame.size.height/8];
//
//    NSArray *multfactors = @[@"1.3",@"1.5",@"2.0",@"2.3"];
//    for (int i=0; i<4; i++) {
//        CGFloat multfactor = [multfactors[i] doubleValue];
//        CGFloat width = self.whatsNewImageview.frame.size.width*multfactor;
//        CGFloat height = self.whatsNewImageview.frame.size.height*multfactor;
//        CGFloat x = self.whatsNewImageview.frame.origin.x - (width/2);
//        CGFloat y = self.whatsNewImageview.frame.origin.y - (height/2);
//        UIView *circle = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
//        if (i%2==0) {
//            [circle setBackgroundColor:self.view.backgroundColor];
//            [circle setAlpha:1.0];
//        } else {
//            [circle setBackgroundColor:[UIColor whiteColor]];
//            [circle setAlpha:0.01 + 0.02*i];
//        }
//        [circle setCenter:self.whatsNewImageview.center];
//        [circle setTag:1010];
//        [circle.layer setCornerRadius:(MAX(circle.frame.size.height, circle.frame.size.width)/2)];
//
//        //[self.view addConstraint:[NSLayoutConstraint constraintWithItem:circle attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
//        //[self.view addConstraint:[NSLayoutConstraint constraintWithItem:circle attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.whatsNewImageview attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
//
//        [self.view insertSubview:circle atIndex:0];
//    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

//- (void)viewDidLayoutSubviews
//{
//    [super viewDidLayoutSubviews];
//    // cheeky hack to make background circles appear in correct position
//    for (UIView *subview in self.view.subviews) {
//        if (subview.tag == 1010) {
//            [subview setCenter:self.whatsNewImageview.center];
//        }
//    }
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)whatsNewButtonAction:(id)sender {
    NSString *action;
    if (sender == self.btnGotIt) {
        action = kShortcutTypeUsage;
        [self trackOmniClick:OMNICLICK_ONBOARDING_CLOUDVOICE_VIEWUSAGE];
    }
//    else {
//        [self trackOmniClick:OMNICLICK_ONBOARDING_CLOUDVOICE_GOTIT];
//    }
    [self.delegate userDismissedWhatsNewScreen:self WithAction:action];
}

#pragma mark - Omniture Methods

- (void)trackPageForOmniture
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = OMNIPAGE_ONBOARDING_CLOUDVOICEEXPRESS;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackOmniClick:(NSString *)linkTitle
{
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@%@", STATE_PREFIX, linkTitle]];
}


@end
