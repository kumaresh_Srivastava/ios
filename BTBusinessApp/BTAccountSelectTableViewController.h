//
//  BTAccountSelectTableViewController.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 23/02/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTUser.h"

typedef NS_ENUM(NSInteger, BTAccountSelectContext) {
    BTAccountSelectContextAccount,
    BTAccountSelectContextUsage,
    BTAccountSelectContextServiceStatus,
    BTAccountSelectContextPublicWifi
};

@interface BTAccountSelectTableViewController : UITableViewController

@property (nonatomic) BTAccountSelectContext context;
@property (nonatomic, assign) BTUser *currentUser;
@property (nonatomic, copy) NSString *groupKey;
@property (nonatomic) NSArray *cugs;

@end
