//
//  BTPublicWifiToggleTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 19/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTPublicWifiToggleTableViewCell.h"

@implementation BTPublicWifiToggleTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupCellForToggleType:(BTPublicWifiToggleType)type
{
    self.toggleType = type;
    switch (type) {
        case BTPublicWifiToggleTypeBTWiFi:
            [self.networkNameLabel setText:@"BT Wi-Fi"];
            [self.networkDescriptionLabel setText:@"A pay-to-use wi-fi hotspot.\nBT customers can sign in for free."];
            break;
            
        case BTPublicWifiToggleTypeBTGuestWiFi:
            [self.networkNameLabel setText:@"Guest Wi-Fi"];
            [self.networkDescriptionLabel setText:@"Free wi-fi for your guests.\nSecurity and speed protected for you."];
            break;
            
        default:
            [self.networkNameLabel setText:@""];
            break;
    }
}

- (CGFloat)heightForCell
{
    return 101.0f;
}

- (void)updateToggleState:(BOOL)state
{
    [self.toggleSwitch setOn:state];
}

- (IBAction)toggleSwitched:(id)sender {
    UISwitch *theswitch = (UISwitch*)sender;
    [self.delegate BTPublicWiFiToggleCell:self toggledToState:theswitch.on];
}

@end
