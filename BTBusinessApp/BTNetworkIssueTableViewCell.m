//
//  BTNetworkIssueTableViewCell.m
//  BTBusinessApp
//
//  Created by Jim Purvis on 10/05/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTNetworkIssueTableViewCell.h"

@implementation BTNetworkIssueTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
