#import "SpeedTestViewController.h"
#import "DownloadSpeedTableViewCell.h"
#import "BTServiceStatusActionsHeaderView.h"
#import "BTServiceStatusTakeActionTableViewCell.h"
#import "CustomSpinnerView.h"
#import "DLMServiceStatusDetailsScreen.h"
#import "DLMSpeedTestScreen.h"
#import "OmnitureManager.h"
#import "BTRetryView.h"
#import "BTResilientHubAsset.h"
#import "BTInAppBrowserViewController.h"
#import "BTNavigationViewController.h"
#import "SpeedTestUpgradeHubViewController.h"
#import "AppManager.h"
#import "NLVordelWebServiceError.h"
#import "BTServiceStatusDetailsViewController.h"
#import <Lottie/Lottie.h>
#import "AppDelegate.h"

#define kTestComplete @"Your broadband speeds"
#define kGenericErrorTitle @"Sorry, we couldn't test your speed"
#define kGenericErrorDetail @"Broadband is not available right now. We\'re working to put things right."
#define kRestartHubErrorDetail @"Please restart your hub to help put things right.\n1. Switch off your hub using the Power button on the back\n2. Switch your hub back on\n3. When the hub lights are blue, run the test again"
#define kUpdateHubErrorDetail @"We need to update the software on your hub. Simply restart your hub at a convenient time - we'll do the rest automatically.\n1. Switch off your hub using the Power button on the back\n2. Switch your hub back on\n3. After 24 hours, run the test again\nIf you have the same issue after 24 hours, please report a fault."
#define k4GAssureErrorDetail @"Broadband is not available right now.\n\n4G Assure is keeping you online. When your broadband is up and running again, you’ll be able to run the speed test."

@interface SpeedTestViewController ()<UITableViewDelegate, UITableViewDataSource,
DLMServiceStatusDetailsScreenDelegate, DLMSpeedTestScreenDelegate, BTRetryViewDelegate, DownloadSpeedTableViewCellDelegate,BTServiceStatusTakeActionTableViewCellDelegate>

@property (strong, nonatomic) IBOutlet UIView *speedTestContainerView;
@property (weak, nonatomic) IBOutlet UITableView *speedTestTableView;
@property (nonatomic,strong)  CustomSpinnerView *loadingView;
@property (weak, nonatomic) IBOutlet UILabel *percentageLabel;
@property (weak, nonatomic) IBOutlet UIView *animationContainerView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (nonatomic) LOTAnimationView *animationView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *testingYourBroadbandHeightConstraint;

@property (nonatomic) BOOL speedTestComplete;
@property (nonatomic) int currentPercentage;
@property (nonatomic) BOOL refresh;

@property (nonatomic, readwrite,strong) DLMServiceStatusDetailsScreen *serviceStatusDetailsViewModel;
@property (nonatomic, readwrite) DLMSpeedTestScreen *speedTestViewModel;

@property (nonatomic) NSTimer *timer;

@property (nonatomic) NSString *currentOmniturePage;
@property (nonatomic)  BTRetryView *errorView;
@end

@implementation SpeedTestViewController

+ (SpeedTestViewController *)getSpeedTestViewController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SpeedTestViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SpeedTestViewController"];
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Model setup
    self.serviceStatusDetailsViewModel = [[DLMServiceStatusDetailsScreen alloc] init];
    self.serviceStatusDetailsViewModel.serviceStatusDetailScreenDelegate = self;
    self.serviceStatusDetailsViewModel.forSpeedTest = YES;
    _speedTestViewModel = [[DLMSpeedTestScreen alloc] init];
    _speedTestViewModel.speedTestScreenDelegate = self;
    
    // Navigation setup
    [self.navigationItem setTitle:@"Broadband speed test"];
   
    
    //TableView setup
    _speedTestTableView.delegate = self;
    //_speedTestTableView.dataSource = self;
    _speedTestTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _speedTestTableView.estimatedRowHeight = 277;
    _speedTestTableView.rowHeight = UITableViewAutomaticDimension;
    
    _cancelButton.layer.cornerRadius = 5.0;
    _cancelButton.layer.borderWidth = 1.0;
    _cancelButton.layer.borderColor =  [BrandColours colourBackgroundBTPurplePrimaryColor].CGColor;
    
    UINib *downloadSpeedCellNib = [UINib nibWithNibName:@"DownloadSpeedTableViewCell" bundle:[NSBundle mainBundle]];
    [_speedTestTableView registerNib:downloadSpeedCellNib forCellReuseIdentifier:@"DownloadSpeedTableViewCell"];
    
    UINib *takeActionCellNib = [UINib nibWithNibName:@"BTServiceStatusTakeActionTableViewCell" bundle:[NSBundle mainBundle]];
    [_speedTestTableView registerNib:takeActionCellNib forCellReuseIdentifier:@"BTServiceStatusTakeActionTableViewCell"];

    // Make initial service status call to get the asset
    [self makeAPICall];
}

- (void) viewWillDisappear:(BOOL)animated{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.isSingleBacCug == YES){
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if (self.isMovingFromParentViewController || self.isBeingDismissed) {
            appDelegate.isSingleBacCugBackNavButtonPressed = YES;
        }
        else{
            appDelegate.isSingleBacCugBackNavButtonPressed = NO;
        }
    }else {
        appDelegate.isSingleBacCugBackNavButtonPressed = NO;
    }
}

#pragma mark - UI methods
- (void) beginSpeedTestLoadingForTimePeriod:(int)seconds {
    _currentPercentage = 0;
    int interval = (seconds + 100 - 1) / 100;
    _timer = [NSTimer scheduledTimerWithTimeInterval:interval
                                                        target:self
                                                      selector:@selector(updateLoadingLabel)
                                                      userInfo:nil
                                                       repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

- (void)setNavigationTitleWithServiceID{
    UILabel *titleLabel = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, 150, 44)];
    titleLabel.numberOfLines = 2;
    titleLabel.font = [UIFont fontWithName:kBtFontBold size: 16.0f];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = [NSString stringWithFormat:@"Broadband speed test\n%@", self.serviceID];
    self.navigationItem.titleView = titleLabel;
}

- (void) loadingLabelTo100 {
    [_timer invalidate];
    _percentageLabel.text = kTestComplete;
}

- (void) updateLoadingLabel {
    _currentPercentage+= 0 + arc4random() % 3; // instead of just adding on a flat 1% every interval, make things a little more exciting for the user
    if(_currentPercentage <= 100) {
        _percentageLabel.text = [NSString stringWithFormat:@"%d%@", _currentPercentage, @"% complete"];
    } else {
        [_timer invalidate];
        _percentageLabel.text = kTestComplete;
    }
}

- (void) retrySpeedTest {
    _refresh = YES;
    [_timer invalidate];
    [self resetUI];
    [self makeAPICall];
}

- (void) resetUI {
    _cancelButton.hidden = NO;
    _percentageLabel.text = @"0% complete";
    _testingYourBroadbandHeightConstraint.constant = 42.5;
    CGRect containerFrame = _speedTestContainerView.frame;
    containerFrame.size.height = 292.0;
    [_speedTestContainerView setFrame:containerFrame];
    _speedTestTableView.dataSource = nil;
    [_speedTestTableView reloadData];
    if (self.errorView && [self.view.subviews containsObject:self.errorView]) {
        [self.errorView removeFromSuperview];
    }
    [self showLoadingView];
    [self playTestingAnimation];
}

- (void)playTestingAnimation {
    [self playAnimationWithFileName:@"speed_test_dots" andShouldLoop:YES];
}

- (void)playCompletionAnimation {
    if(_speedTestViewModel.mGals) {
        if(_speedTestViewModel.downStreamSpeed > _speedTestViewModel.mGals) {
            [self playPositiveCompletionAnimation];
        } else {
            [self playNegativeCompletionAnimation];
        }
    } else {
        [self playNeutralCompletionAnimation];
    }
}

- (void)playPositiveCompletionAnimation {
    [self playAnimationWithFileName:@"speed_test_positive_ending" andShouldLoop:NO];
}

- (void)playNegativeCompletionAnimation {
    [self playAnimationWithFileName:@"speed_test_negative_ending" andShouldLoop:NO];
}

- (void)playNeutralCompletionAnimation {
    [self playAnimationWithFileName:@"speed_test_neutral_ending" andShouldLoop:NO];
}

- (void)playAnimationWithFileName:(NSString*)fileName andShouldLoop:(BOOL)shouldLoop {
    [_animationContainerView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    _animationView = [LOTAnimationView animationNamed:fileName];
    [_animationView setContentMode:UIViewContentModeScaleAspectFit];
    CGRect animationFrame = _animationContainerView.frame;
    CGRect lottieFrame = _animationView.frame;
    lottieFrame.size.width = animationFrame.size.width;
    lottieFrame.size.height = 100.0;
    [_animationView setFrame:lottieFrame];
    [_animationView setBackgroundColor:[UIColor clearColor]];
    [_animationView setLoopAnimation:shouldLoop];
    [_animationView play];
    [_animationContainerView addSubview:_animationView];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:_animationView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    CGRect animationFrame = _animationContainerView.frame;
    CGRect lottieFrame = _animationView.frame;
    lottieFrame.size.width = animationFrame.size.width;
    [_animationView setFrame:lottieFrame];
}

#pragma mark - error view
- (void)showErrorViewWithTitle:(NSString*)title andDescription:(NSString*)description {
    if(self.errorView == nil){
        self.errorView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
    
    self.errorView.retryViewDelegate = self;
    self.errorView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.errorView updateRetryViewWithErrorHeadline:title errorDetail:@"" andButtonText:@"View service status"];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    NSMutableAttributedString *tryAgainIn15 = [[NSMutableAttributedString alloc] initWithString:description attributes:@{
                                                                                                                                                                                    NSFontAttributeName: [UIFont fontWithName:@"BTFont-Regular" size: 16.0],
                                                                                                                                                                                    NSForegroundColorAttributeName: [UIColor colorForHexString:@"333333"],
                                                                                                                                                                                    
                                                                                                                                                                                    NSParagraphStyleAttributeName:paragraphStyle                                                                                                              }];
    
    [tryAgainIn15 addAttribute: NSFontAttributeName value: [UIFont fontWithName:@"BTFont-Regular" size: 16.0] range: NSMakeRange(0, [tryAgainIn15 length] - 1)];
    [self.errorView updateRetryViewDetailWithAttributedString:tryAgainIn15];
    
    [self.view addSubview:self.errorView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.errorView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.errorView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.errorView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.errorView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-40.0]];
    }
}

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView {
    BTServiceStatusDetailsViewController *serviceStatusDetailsViewController = [BTServiceStatusDetailsViewController getServiceStatusDetailsViewController];
    serviceStatusDetailsViewController.currentServiceType = BTServiceTypeBroadband;
    serviceStatusDetailsViewController.serviceID = _serviceID;
    serviceStatusDetailsViewController.postCode = _postcode;
    serviceStatusDetailsViewController.selectedServiceName = @"Broadband";
    if([_productName isEqualToString:@"Business BroadBand Access"]) {
        serviceStatusDetailsViewController.productName = @"Business broadband";
    }
    serviceStatusDetailsViewController.productName = _productName;
    [self.navigationController pushViewController:serviceStatusDetailsViewController animated:YES];
}

#pragma mark - loading view
- (void) showLoadingView {
    if(!_loadingView){
        _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        _loadingView.backgroundColor = [UIColor whiteColor];
        _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
        _loadingView.loadingLabel.text = @"Checking your service...";
        [self.view addSubview:_loadingView];
    }
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
}

- (void) hideLoadingView {
    [_loadingView setHidden:YES];
}

#pragma mark - table view UI methods
- (void) setupUIForResults {
    [self loadingLabelTo100];
    [self playCompletionAnimation];
    _cancelButton.hidden = YES;
    CGRect containerFrame = _speedTestContainerView.frame;
    containerFrame.size.height = 200;
    [_speedTestContainerView setFrame:containerFrame];
    
    _testingYourBroadbandHeightConstraint.constant = 0;
    
    _speedTestTableView.dataSource = self;
    [_speedTestTableView reloadData];
    [_speedTestTableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 2)] withRowAnimation:UITableViewRowAnimationTop];
}

#pragma mark - table view delegate methods
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if(indexPath.section == 0) {
        DownloadSpeedTableViewCell *downloadSpeedCell = [tableView dequeueReusableCellWithIdentifier:@"DownloadSpeedTableViewCell" forIndexPath:indexPath];
        [downloadSpeedCell setUpCellForContext:_speedTestViewModel];
        _currentOmniturePage = downloadSpeedCell.currentOmniturePage; // maintain page parity between cell and view controller after speed test
        downloadSpeedCell.delegate = self;
        return downloadSpeedCell;
    } else if (indexPath.section == 1) {
        BTServiceStatusTakeActionTableViewCell *takeActionCell = [tableView dequeueReusableCellWithIdentifier:@"BTServiceStatusTakeActionTableViewCell" forIndexPath:indexPath];
        takeActionCell.serviceStatusTakeActionTableViewCellDelegate = self;
        [takeActionCell updateCellWithTitleText:@"About BT's speed guarantee"];
        return takeActionCell;
    } else if (indexPath.section == 2) {
        if(indexPath.row == 0) {
            BTServiceStatusTakeActionTableViewCell *takeActionCell = [tableView dequeueReusableCellWithIdentifier:@"BTServiceStatusTakeActionTableViewCell" forIndexPath:indexPath];
             takeActionCell.serviceStatusTakeActionTableViewCellDelegate = self;
            [takeActionCell updateCellWithTitleText:@"Broadband speed tips"];
            return takeActionCell;
        } else {
            BTServiceStatusTakeActionTableViewCell *takeActionCell = [tableView dequeueReusableCellWithIdentifier:@"BTServiceStatusTakeActionTableViewCell" forIndexPath:indexPath];
             takeActionCell.serviceStatusTakeActionTableViewCellDelegate = self;
            [takeActionCell updateCellWithTitleText:@"Wi-fi speed tips"];
            return takeActionCell;
        }
    }
    
    
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 2) {
        return 2;
    }
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if(_speedTestViewModel.downloadResultType == MeetsMGALRequirement) {
            return 185;
        } else {
            return 310;
        }
    } else {
        return 53;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    } else {
        return 36;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
//    if(section == 2) {
//        return 100;
//    } else {
//        return 0;
//    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    BTServiceStatusActionsHeaderView *serviceStatusActionHeaderView = [[[NSBundle mainBundle] loadNibNamed:@"BTServiceStatusActionsHeaderView" owner:nil options:nil] objectAtIndex:0];
    if(section == 1) {
        [serviceStatusActionHeaderView updateTitleLabelWithTitleText:@"Speed test help"];
        return serviceStatusActionHeaderView;
    } else if (section == 2) {
        [serviceStatusActionHeaderView updateTitleLabelWithTitleText:@"How to fix problems"];
        return serviceStatusActionHeaderView;
    } else {
        return serviceStatusActionHeaderView;
    }
}

- (void)userPressedCellForServiceStatusTakeActionCell:(BTServiceStatusTakeActionTableViewCell *)serviceStatusTakeActionTableViewCell{
    
     NSIndexPath *indexPath = [self.speedTestTableView indexPathForCell:serviceStatusTakeActionTableViewCell];
     [self openInAppBrowser:indexPath];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self openInAppBrowser:indexPath];
}

- (void) openInAppBrowser:(NSIndexPath*)indexPath{
    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
    if(indexPath.section == 1) {
        viewController.targetURLType = BTTargetURLTypeSpeedTestGuarantee;
    } else if(indexPath.section == 2){
        if(indexPath.row == 0) {
            viewController.targetURLType = BTTargetURLTypeBroadbandSpeedTips;
            [self trackOmniClick:OMNICLICK_SPEED_TEST_BROADBAND_SPEED_TIPS];
        } else if(indexPath.row == 1) {
            viewController.targetURLType = BTTargetURLTypeWifiSpeedTips;
            [self trackOmniClick:OMNICLICK_SPEED_TEST_WIFI_SPEED_TIPS];
        }
    } else {
        return;
    }
    
    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:NO andIsNeedCloseButton:YES];
    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
    [self presentViewController:navController animated:YES completion:^{
        
    }];
}

#pragma mark - network
- (void) makeAPICall {
    [self trackPageForOmniture:OMNIPAGE_SPEED_TEST_CHECKING_SERVICE_STATUS];
    [self showLoadingView];
    [self.serviceStatusDetailsViewModel fetchResilientHubAsset:self.serviceID];
}


#pragma mark - service status screen delegate methods
- (void)successfullyFetchedServiceStatusDetailsScreen:(DLMServiceStatusDetailsScreen *)serviceStatusDetailScreen {
    // Do nothing
}

- (void)serviceStatusDetailsScreen:(DLMServiceStatusDetailsScreen *)serviceStatusDetailScreen failedToFetchServiceDetailWithWebServiceError:(NLWebServiceError *)webServiceError {
    [self hideLoadingView];
    [self showErrorViewWithTitle:kGenericErrorTitle andDescription:kGenericErrorDetail];
    [self trackPageForOmniture:OMNIPAGE_SPEED_TEST_SERVICE_STATUS_ERROR];
    if (webServiceError && [webServiceError isKindOfClass:[NLVordelWebServiceError class]]) {
        NLVordelWebServiceError *error = (NLVordelWebServiceError*)webServiceError;
        if (error.errorCode && [error.errorCode isEqualToString:@"MB5-01"]) {
            [OmnitureManager trackError:OMNIERROR_RESILENT_HUB_INVALID_DATA_ERROR onPageWithName:OMNIPAGE_SPEED_TEST_SERVICE_STATUS_ERROR contextInfo:nil];
        } else if (error.errorCode && [error.errorCode isEqualToString:@"MBAF2-01"]) {
            [OmnitureManager trackError:OMNIERROR_RESILIENT_HUB_NOT_ALLOWED onPageWithName:OMNIPAGE_SPEED_TEST_SERVICE_STATUS_ERROR contextInfo:nil];
        }
    }
}

- (void)successfullyFetchedWorkflowStatus:(DLMServiceStatusDetailsScreen *)serviceStatusDetailScreen {
    
    [self setNavigationTitleWithServiceID];
    self.serviceStatusDetailsViewModel = serviceStatusDetailScreen;
    NSArray *resilientHubAssets = [self.serviceStatusDetailsViewModel resilientHubAssets];
    if(resilientHubAssets && resilientHubAssets.count > 0){
        BTResilientHubAsset *asset = (BTResilientHubAsset*) resilientHubAssets[0];
        _serialNumber = [asset hubSerialNumber];
        _hashCode = [asset hashCode];
    }
        
    //[self hideLoadingView];
    
    NSString *hubProductClass;
    if([[self.serviceStatusDetailsViewModel hubDetails] objectForKey:@"Product Class"]) {
        hubProductClass = [[self.serviceStatusDetailsViewModel hubDetails] objectForKey:@"Product Class"];
    }
    
    if (self.serviceStatusDetailsViewModel.current4GAssureStatus == SS4GAssureOn) {
        [self hideLoadingView];
        [self showErrorViewWithTitle:kGenericErrorTitle andDescription:k4GAssureErrorDetail];
        [self trackPageForOmniture:OMNICLICK_SPEED_TEST_4G_ASSURE];
    }
    else if ([hubProductClass isEqualToString:@"Business Hub 30 Type A"] ||
             [hubProductClass isEqualToString:@"Business Hub 50 Type A"]) {
        SpeedTestUpgradeHubViewController *upgradeHubViewController = [SpeedTestUpgradeHubViewController getSpeedTestUpgradeHubViewController];
        [upgradeHubViewController setUnsupportedHub:YES];
        [self.navigationController pushViewController:upgradeHubViewController animated:YES];
        if(self.errorView != nil){
            [self.errorView removeFromSuperview];
            self.errorView = nil;
        }
    }
    else if ((_serialNumber && ![_serialNumber isEqualToString:@""] && ![_serialNumber isKindOfClass:[NSNull class]])
        && (_hashCode && ![_hashCode isEqualToString:@""] && ![_hashCode isKindOfClass:[NSNull class]])) {
        [self hideLoadingView];
        [self playTestingAnimation];
        [self beginSpeedTestLoadingForTimePeriod:240];
        [_speedTestViewModel performSpeedTestWithServiceID:_serviceID serialNumber:_serialNumber hashCode:_hashCode andForceExecute:NO];
        _cancelButton.hidden = NO;
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"refresh"] style:UIBarButtonItemStylePlain target:self action:@selector(retrySpeedTest)];
        self.navigationItem.rightBarButtonItem.enabled = YES;
        [self trackPageForOmniture:OMNIPAGE_SPEED_TEST_CHECKING_SPEED];
        if(self.errorView != nil){
            [self.errorView removeFromSuperview];
            self.errorView = nil;
        }
    }
    else {
        [self showErrorViewWithTitle:kGenericErrorTitle andDescription:kGenericErrorDetail];
        [self trackPageForOmniture:OMNIPAGE_SPEED_TEST_SERVICE_STATUS_ERROR];
    }
}

- (void)performDummySpeedTest {
    NSString *dummyServiceID = @"01061889123";
    NSString *dummySerial = @"+088315+NQ82064617";
    NSString *dummyHash = @"$2a$12$2019/02/01UKBRoBTESBHO1WzGFXvtJqbMM0WxqnRpkV7lR30Mr8K";
    [_speedTestViewModel performSpeedTestWithServiceID:dummyServiceID serialNumber:dummySerial hashCode:dummyHash andForceExecute:NO];
}

- (void)serviceStatusDetailsScreen:(DLMServiceStatusDetailsScreen *)serviceStatusDetailScreen failedToFetchWorkFlowStatusWithWebServiceError:(NLWebServiceError *)webServiceError {
    [self hideLoadingView];
    [self showErrorViewWithTitle:kGenericErrorTitle andDescription:kGenericErrorDetail];
}

- (void)speedTestCompleteShowUpgradeHubScreen {
    SpeedTestUpgradeHubViewController *upgradeHubViewController = [SpeedTestUpgradeHubViewController getSpeedTestUpgradeHubViewController];
    [self.navigationController pushViewController:upgradeHubViewController animated:YES];
}

- (void)speedTestCompleteShowUpdateHubScreen {
    [self hideLoadingView];
    [self showErrorViewWithTitle:kGenericErrorTitle andDescription:kUpdateHubErrorDetail];
    [self trackPageForOmniture:OMNIPAGE_SPEED_TEST_SERVICE_STATUS_ERROR];
}

- (void)speedTestCompleteShowRestartHubScreen {
    [self hideLoadingView];
    [self showErrorViewWithTitle:kGenericErrorTitle andDescription:kRestartHubErrorDetail];
    [self trackPageForOmniture:OMNIPAGE_SPEED_TEST_SERVICE_STATUS_ERROR];
}


- (void)liveChatAvailableForFaultWithScreen:(DLMServiceStatusDetailsScreen *)serviceStatusDetailsScreen {
    // Do nothing
}


- (void)serviceStatusDetailsScreen:(DLMServiceStatusDetailsScreen *)serviceStatusDetailsScreen failedToCheckLiveChatAvailabilityWithWebServiceError:(NLWebServiceError *)error {
    // Do nothing
}


- (void)serviceStatusDetailsScreen:(DLMServiceStatusDetailsScreen *)serviceStatusDetailsScreen liveChatCurrentlyNotAvailableWithSpecificCategory:(NSArray *)specificCategory generalCategory:(NSArray *)generalCategory andCurrentTime:(NSString *)currentTime {
    // Do nothing
}

- (void)displayNetworkStatusIssue {
    // Do nothing
}


#pragma mark - speed test delegate methods
- (void)speedTestScreen:(DLMSpeedTestScreen *)speedTestScreen failedToPerformSpeedTestWithWebServiceError:(NLWebServiceError *)webServiceError {
    [self showErrorViewWithTitle:kGenericErrorTitle andDescription:kGenericErrorDetail];
}

- (void)successfullyPerformedSpeedTest:(DLMSpeedTestScreen *)speedTestScreen {
    _speedTestViewModel = speedTestScreen;
    [self setupUIForResults];
}

# pragma mark - download cell delegate methods
- (void)userPressedRetest {
    [self retrySpeedTest];
}

- (void)userPressedReportFault {
//    BTInAppBrowserViewController *viewController = [BTInAppBrowserViewController getInAppBrowserViewController];
//    viewController.targetURLType = BTTargetURLTypeReportAFault;
//    [viewController updateInAppBrowserWithIsPresentedModally:YES isComingFromHomeScreen:NO isNeedBackButton:NO andIsNeedCloseButton:YES];
//    BTNavigationViewController *navController = [[BTNavigationViewController alloc] initWithRootViewController:viewController];
//    [self presentViewController:navController animated:YES completion:^{
//        
//    }];
    [self trackOmniClick:OMNICLICK_SPEED_TEST_REPORT_A_FAULT];
    [AppManager openURL:[AppManager reportSpeedTestFaultForServiceID:self.serviceID]];
    
}

- (void)userPressedDone {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)userPressedCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - New Omniture Methods

- (NSString*) pageNameForOmnitureTracking {
    return OMNIPAGE_SPEED_TEST;
}

- (void)trackPageForOmniture:(NSString*)pageName
{
    _currentOmniturePage = pageName;
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    if(!_refresh && [self.serviceStatusDetailsViewModel.hubDetails objectForKey:@"Product Class"]) {
        [data setValue:[self.serviceStatusDetailsViewModel.hubDetails objectForKey:@"Product Class"] forKey:kOmniHubType];
    }
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackOmniClick:(NSString *)linkTitle
{
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",_currentOmniturePage,linkTitle]];
}

- (void)trackOmnitureError:(NSString *)error
{
    return;
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    NSString *pageName = OMNIPAGE_LOGINPIN;
    NSString *timestamp = [AppManager NSStringWithOmnitureFormatFromNSDate:[NSDate date]];
    [data setValue:timestamp forKey:@"TimeStamp"];
    
    [OmnitureManager trackError:error onPageWithName:pageName contextInfo:data];
}

- (void)trackOmnitureContentViewedTag:(NSString *)contentMessage {
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:contentMessage forKey:kOmniContentViewed];
    
    [OmnitureManager trackPage:OMNIPAGE_SPEED_TEST_SERVICE_STATUS_ERROR withContextInfo:data];
}

@end
