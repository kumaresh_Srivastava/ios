//
//  BTOCSOrderDetailsTableViewController.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 04/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSOrderDetailsTableViewController.h"

#import "NLGetProjectSummary.h"
#import "NLGetProjectEnquiries.h"

#import "BTOCSProjectDetailsTableViewController.h"
#import "BTOCSProjectNotesTableViewController.h"
#import "BTOCSSendQueryTableViewController.h"
#import "BTOCSRequiredQuestionsTableViewController.h"

#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDRecentSearchedOrder.h"


@interface BTOCSOrderDetailsTableViewController () <NLGetProjectSummaryDelegate,NLGetProjectEnquiriesDelegate,BTOCSDropDownTableViewCellDelegate,BTOCSHorizontalSelectorTableViewCellDelegate,BTOCSSummaryMilestoneTableViewCellDelegate,BTOCSSendQueryTableViewControllerDelegate>

@property (strong, nonatomic) NLGetProjectSummary *getProjectSummaryWebservice;
@property (strong, nonatomic) NLGetProjectEnquiries *getProjectEnquiriesWebservice;

@property (strong, nonatomic) BTOCSSite *selectedSite;
@property (strong, nonatomic) NSString *selectedTab;


@property (strong, nonatomic) BTOCSDetailsHeaderTableViewCell *headerCell;
@property (strong, nonatomic) BTOCSDropDownTableViewCell *siteSelectCell;
@property (strong, nonatomic) BTOCSSimpleDisclosureTableViewCell *projectDetailsCell;
@property (strong, nonatomic) BTOCSSimpleDisclosureTableViewCell *projectNotesCell;
@property (strong, nonatomic) BTOCSRerquiredQuestionsTableViewCell *requiredQuestionsCell;
@property (strong, nonatomic) BTOCSHorizontalSelectorTableViewCell *selectorCell;
@property (strong, nonatomic) BTOCSCompletionDateTableViewCell *completionDateCell;
@property (strong, nonatomic) BTOCSSimpleButtonTableViewCell *needHelpButtonCell;
@property (strong, nonatomic) BTOCSSectionHeaderTableViewCell *plannedScheduleHeaderCell;

@property (strong, nonatomic) NSMutableArray<BTBaseTableViewCell*> *summaryCells;
@property (strong, nonatomic) NSMutableArray<BTBaseTableViewCell*> *queryCells;

@end

@implementation BTOCSOrderDetailsTableViewController

+ (BTOCSOrderDetailsTableViewController *)getOCSOrderDetailsViewController
{
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"BTOCSOrders" bundle:nil];
    //BTOCSOrderDetailsTableViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BTOCSOrderDetailsTableViewController"];
    BTOCSOrderDetailsTableViewController *vc = [[BTOCSOrderDetailsTableViewController alloc] initWithStyle:UITableViewStylePlain];
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.title = @"Order details";
    [self getProjectSummary];
}

- (void)setupTableCells
{
    [super setupTableCells];
    
    // project info cell
    self.headerCell = (BTOCSDetailsHeaderTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSDetailsHeaderTableViewCell reuseId]];
    self.headerCell.projectReferenceLabel.text = self.project.projectRef;
    [self.headerCell updateWithProduct:self.project.productName];
    [self.headerCell updateWithSubStatus:self.project.subStatus];
    [self.tableCells addObject:self.headerCell];
    
    // project details cell
    self.projectDetailsCell = (BTOCSSimpleDisclosureTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSSimpleDisclosureTableViewCell reuseId]];
    self.projectDetailsCell.label.text = @"Project details";
    [self.tableCells addObject:self.projectDetailsCell];
    
    // site cell
    self.siteSelectCell = (BTOCSDropDownTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSDropDownTableViewCell reuseId]];
    self.siteSelectCell.delegate = self;
    [self.siteSelectCell updateWithTitle:@"Site" values:nil selectedValue:@""];
    [self.tableCells addObject:self.siteSelectCell];
    
    // required questions cell
    self.requiredQuestionsCell = (BTOCSRerquiredQuestionsTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSRerquiredQuestionsTableViewCell reuseId]];
    [self.requiredQuestionsCell updateWithAnswered:@"0" ofTotal:@"6"];
    [self.tableCells addObject:self.requiredQuestionsCell];
    
    // tab select cell
    self.selectorCell = (BTOCSHorizontalSelectorTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSHorizontalSelectorTableViewCell reuseId]];
    self.selectorCell.delegate = self;
    self.selectedTab = @"Summary";
    [self.selectorCell updateWithOptions:@[@"Summary",@"Queries"] andSelectedOption:self.selectedTab];
    [self.tableCells addObject:self.selectorCell];
    
}

- (void)updateTableCells
{
    [super updateTableCells];
    
    [self.headerCell updateWithProduct:self.project.productService];
    [self.headerCell updateWithSubStatus:self.project.subStatus];
    
    // updated site cell if necessary
    if ((!self.siteSelectCell.selectedValue ||[self.siteSelectCell.selectedValue isEqualToString:@""] ) && self.project.sites) {
        NSMutableArray *values = [[NSMutableArray alloc] initWithCapacity:self.project.sites.count];
        for (BTOCSSite *site in self.project.sites) {
            [values addObject:site.name];
        }
        if (!self.selectedSite) {
            self.selectedSite = [self.project.sites firstObject];
        }
        [self.siteSelectCell updateWithTitle:@"Site" values:values selectedValue:self.selectedSite.name];
    }
    
    // update required questions
    if (self.project.openQuestionsFlag && !self.project.hideContactPanel) {
        [self.requiredQuestionsCell updateWithAnswered:@"0" ofTotal:@"6"];
        if (![self.tableCells containsObject:self.requiredQuestionsCell]) {
            [self.tableCells insertObject:self.requiredQuestionsCell atIndex:3];
        }
    } else {
        [self.requiredQuestionsCell updateWithAnswered:@"6" ofTotal:@"6"];
        if ([self.tableCells containsObject:self.requiredQuestionsCell]) {
            [self.tableCells removeObject:self.requiredQuestionsCell];
        }
    }
    
    [self.tableCells removeObjectsInArray:self.summaryCells];
    [self.tableCells removeObjectsInArray:self.queryCells];
    
    // update based on tab selection
    if ([self.selectedTab isEqualToString:@"Summary"]) {
        // show summary cells
        [self updateSummaryCells];
        [self.tableCells addObjectsFromArray:self.summaryCells];
    } else if ([self.selectedTab isEqualToString:@"Queries"]) {
        // show query cells
        [self updateQueryCells];
        [self.tableCells addObjectsFromArray:self.queryCells];
    } else {
        // unhandled
    }
    
}

- (void)updateSummaryCells
{
    if (!self.summaryCells) {
        self.summaryCells = [NSMutableArray new];
    } else {
        [self.tableCells removeObjectsInArray:self.summaryCells];
        [self.summaryCells removeAllObjects];
    }
    
    if (!self.completionDateCell) {
        self.completionDateCell = (BTOCSCompletionDateTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSCompletionDateTableViewCell reuseId]];
    }
    self.completionDateCell.titleLabel.text = @"Expected completion";

    NSString *dateString = @"-";
    if (self.project.targetDeliveryDate) {
        dateString = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:self.project.targetDeliveryDate];
    } else if (self.project.projectEndDate) {
        dateString = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:self.project.projectEndDate];
    }
    self.completionDateCell.dateLabel.text = dateString;
    [self.summaryCells addObject:self.completionDateCell];
    
    // project notes cell
    if (!self.projectNotesCell) {
        self.projectNotesCell = (BTOCSSimpleDisclosureTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSSimpleDisclosureTableViewCell reuseId]];
    }
    self.projectNotesCell.label.text = @"Project notes";
    [self.summaryCells addObject:self.projectNotesCell];
    
    // planned schedule header
    if (!self.plannedScheduleHeaderCell) {
        self.plannedScheduleHeaderCell = (BTOCSSectionHeaderTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSSectionHeaderTableViewCell reuseId]];
        self.plannedScheduleHeaderCell.headerTextLabel.text = @"Planned schedule";
    }
    [self.summaryCells addObject:self.plannedScheduleHeaderCell];
    
    // planned schedule milestone cells
    for (BTOCSMilestone *milestone in self.selectedSite.milestones) {
        BTOCSSummaryMilestoneTableViewCell *milestoneCell = (BTOCSSummaryMilestoneTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSSummaryMilestoneTableViewCell reuseId]];
        // configure
        milestoneCell.headerLabel.text = milestone.name;
        milestoneCell.expandedLabel.text = milestone.information;
//        milestoneCell.targetDateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:milestone.dueDate];
//        milestoneCell.completionDateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:milestone.completionDate];
        [milestoneCell updateWithStartDate:milestone.startDate dueDate:milestone.dueDate completionDate:milestone.completionDate];
        milestoneCell.delegate = self;
        [self.summaryCells addObject:milestoneCell];
    }
    
    // need help cell
    if (!self.project.hideContactPanel) {
        if (!self.needHelpButtonCell) {
            self.needHelpButtonCell = (BTOCSSimpleButtonTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSSimpleButtonTableViewCell reuseId]];
            self.needHelpButtonCell.button.titleLabel.text = @"Need help? Get in touch";
            self.needHelpButtonCell.button.backgroundColor = [UIColor clearColor];
            self.needHelpButtonCell.button.titleLabel.textColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
            [self.needHelpButtonCell.button addTarget:self action:@selector(needHelpAction) forControlEvents:UIControlEventTouchUpInside];
            
        }
        [self.summaryCells addObject:self.needHelpButtonCell];
    }
}

- (void)updateQueryCells
{
    // query tab cells
    if (!self.queryCells) {
        self.queryCells = [NSMutableArray new];
    } else {
        [self.tableCells removeObjectsInArray:self.queryCells];
        [self.queryCells removeAllObjects];
    }
    
    // queries
    if (self.project.enquiries && (self.project.enquiries.count > 0)) {
        [self trackPageForOmnitureWithName:OMNIPAGE_OCS_ORDER_QUERIES productService:self.project.productService andOrderStatus:self.project.subStatus];
        for (BTOCSEnquiry *enquiry in self.project.enquiries) {
            BTOCSProjectEnquiryTableViewCell *cell = (BTOCSProjectEnquiryTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSProjectEnquiryTableViewCell reuseId]];
            [cell updateWithEnquiry:enquiry];
            [self.queryCells addObject:cell];
        }
    } else {
        [self trackPageForOmnitureWithName:OMNIPAGE_OCS_ORDER_NOQUERIES productService:self.project.productService andOrderStatus:self.project.subStatus];
        BTOCSNoQueriesTableViewCell *noQueryCell = (BTOCSNoQueriesTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSNoQueriesTableViewCell reuseId]];
        [self.queryCells addObject:noQueryCell];
    }

    
    // need help cell
    if (!self.project.hideContactPanel) {
        if (!self.needHelpButtonCell) {
            self.needHelpButtonCell = (BTOCSSimpleButtonTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:[BTOCSSimpleButtonTableViewCell reuseId]];
            self.needHelpButtonCell.button.titleLabel.text = @"Need help? Get in touch";
            self.needHelpButtonCell.button.backgroundColor = [UIColor clearColor];
            self.needHelpButtonCell.button.titleLabel.textColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
            [self.needHelpButtonCell.button addTarget:self action:@selector(needHelpAction) forControlEvents:UIControlEventTouchUpInside];
        }
        [self.queryCells addObject:self.needHelpButtonCell];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
//    return 0;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
//    return 0;
//}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - TableView delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTBaseTableViewCell *selectedCell = [self.tableCells objectAtIndex:indexPath.row];
    
    // project details
    if (selectedCell == self.projectDetailsCell) {
        BTOCSProjectDetailsTableViewController *projectDetailsVC = [BTOCSProjectDetailsTableViewController getOCSProjectDetailsViewController];
        projectDetailsVC.project = self.project;
        projectDetailsVC.selectedSite = self.selectedSite;
        if (projectDetailsVC.selectedSite) {
            [self.navigationController pushViewController:projectDetailsVC animated:YES];
        }
        [self trackOmniClick:OMNICLICK_OCS_ORDER_PROJECTDETAILS];
    }
    
    // project notes
    if (selectedCell == self.projectNotesCell) {
        BTOCSProjectNotesTableViewController *projectNotesVC = [BTOCSProjectNotesTableViewController getOCSProjectNotesViewController];
        projectNotesVC.project = self.project;
        projectNotesVC.selectedSite = self.selectedSite;
        if (projectNotesVC.selectedSite) {
            [self.navigationController pushViewController:projectNotesVC animated:YES];
        }
        [self trackOmniClick:OMNICLICK_OCS_ORDER_PROJECTNOTES];
    }
    
    // required questions
    if (selectedCell == self.requiredQuestionsCell) {
        BTOCSRequiredQuestionsTableViewController *requiredQuestionsVC = [BTOCSRequiredQuestionsTableViewController getOCSProjectQuestionsViewController];
        requiredQuestionsVC.project = self.project;
        requiredQuestionsVC.selectedSite = self.selectedSite;
        if (requiredQuestionsVC.selectedSite) {
            [self.navigationController pushViewController:requiredQuestionsVC animated:YES];
        }
        [self trackOmniClick:OMNICLICK_OCS_ORDER_REQDQUESTIONS];
    }
    
    // existng enquiry
    if ([selectedCell isKindOfClass:[BTOCSProjectEnquiryTableViewCell class]]) {
        if ([self.queryCells containsObject:selectedCell]) {
            NSUInteger idx = [self.queryCells indexOfObject:selectedCell];
            BTOCSEnquiry *selectedEnquiry = [self.project.enquiries objectAtIndex:idx];
            BTOCSSendQueryTableViewController *existingQueryVC = [BTOCSSendQueryTableViewController getOCSSendQueryViewController];
            existingQueryVC.project = self.project;
            existingQueryVC.selectedSite = self.selectedSite;
            existingQueryVC.existingEnquiry = selectedEnquiry;
            existingQueryVC.delegate = self;
            if (existingQueryVC.selectedSite && existingQueryVC.existingEnquiry) {
                [self.navigationController pushViewController:existingQueryVC animated:YES];
            }
            [self trackOmniClick:OMNICLICK_OCS_ORDER_QUERYDETAILS];
        }
        
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath;
}

#pragma mark - action button events

- (void)needHelpAction
{
    BTOCSSendQueryTableViewController *vc = [BTOCSSendQueryTableViewController getOCSSendQueryViewController];
    vc.project = self.project;
    vc.selectedSite = self.selectedSite;
    vc.delegate = self;
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    vc.view.frame = CGRectMake(0, 50, screenBounds.size.width, screenBounds.size.height);
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
   // [nav setModalPresentationStyle:UIModalPresentationPopover];//k
    //nav.popoverPresentationController.sourceView = self.view;
   // nav.popoverPresentationController.sourceRect = CGRectMake(0, 50, screenBounds.size.width, screenBounds.size.height);
    [self presentViewController:nav animated:YES completion:^{
        //
    }];
    [self trackOmniClick:self.needHelpButtonCell.button.titleLabel.text];
}

#pragma mark - BTOCSDropDownTableViewCellDelegate

- (void)cell:(BTOCSDropDownTableViewCell *)cell updatedSelection:(NSString *)selected
{
    for (BTOCSSite *site in self.project.sites) {
        if ([selected isEqualToString:site.name]) {
            self.selectedSite = site;
        }
    }
    [self refreshTableView];
}

#pragma mark - BTOCSHorizontalSelectorTableViewCellDelegate

- (void)horizontalSelectorCell:(id)cell updatedSelection:(NSString *)selected
{
    self.selectedTab = selected;
    if ([selected isEqualToString:@"Queries"]) {
//        [self trackPageForOmnitureWithName:OMNIPAGE_OCS_ORDER_QUERIES productService:self.project.productService andOrderStatus:self.project.subStatus];
        if (self.project.enquiries) {
            [self refreshTableView];
        } else {
            [self getProjectEnquiries];
        }
    } else {
        [self trackPageForOmnitureWithName:OMNIPAGE_OCS_ORDER_TRACKER productService:self.project.productService andOrderStatus:self.project.subStatus];
        [self refreshTableView];
    }
    
}

#pragma mark - BTOCSSummaryMilestoneTableViewCellDelegate

- (void)layoutChangeInCell:(BTBaseTableViewCell *)cell
{
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
}

#pragma mark - BTOCSSendQueryTableViewControllerDelegate

- (void)querySentFromForm:(BTOCSSendQueryTableViewController *)queryForm
{
    [self getProjectEnquiries];
}

#pragma mark - API methods

- (void)getProjectSummary
{
    if (self.getProjectSummaryWebservice) {
        [self.getProjectSummaryWebservice cancel];
        self.getProjectSummaryWebservice.getProjectSummaryDelegate = nil;
        self.getProjectSummaryWebservice = nil;
    }
    
    
    if ([AppManager isInternetConnectionAvailable]) {
        if (self.project && self.project.projectRef) {
            self.getProjectSummaryWebservice = [[NLGetProjectSummary alloc] initWithProjectRef:self.project.projectRef];
            self.getProjectSummaryWebservice.getProjectSummaryDelegate = self;
            if (!self.networkRequestInProgress) {
                self.networkRequestInProgress = YES;
            }
            [self.getProjectSummaryWebservice resume];
        }
    } else {
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:self.pageNameForOmnitureTracking];
    }
}

- (void)getProjectEnquiries
{
    self.hidesTableWhileLoading = NO;
    if (self.getProjectEnquiriesWebservice) {
        [self.getProjectEnquiriesWebservice cancel];
        self.getProjectEnquiriesWebservice.getProjectEnquiriesDelegate = nil;
        self.getProjectEnquiriesWebservice = nil;
    }
    self.getProjectEnquiriesWebservice = [[NLGetProjectEnquiries alloc] initWithProjectRef:self.project.projectRef];
    self.getProjectEnquiriesWebservice.getProjectEnquiriesDelegate = self;
    if (!self.networkRequestInProgress) {
        self.networkRequestInProgress = YES;
    }
    [self.getProjectEnquiriesWebservice resume];
}

#pragma mark - NLGetProjectSummary delegate

- (void)getProjectSummaryWebService:(NLGetProjectSummary *)webService finishedWithResponse:(NSObject *)response
{
    self.networkRequestInProgress = NO;
    if (!self.project.productName || [self.project.productName isEqualToString:@""]) {
        // initiated from search screen
        [self saveOrderSearchDetails];
    }
    [self.project updateWithProjectSummaryResponse:(NSDictionary*)response];
    [self refreshTableView];
}

-(void)getProjectSummaryWebService:(NLGetProjectSummary *)webService failedWithWebServiceError:(NLWebServiceError *)error
{
    self.networkRequestInProgress = NO;
    [AppManager trackWebServiceError:error FromAPI:webService.friendlyName OnPage:self.pageNameForOmnitureTracking];
    BOOL errorHandled = [self attemptVordelProtectedAPIErrorHandlingOfWebServiceError:error];
    if (!errorHandled) {
        if (!self.project.productName || [self.project.productName isEqualToString:@""]) {
            // initiated from search screen
            [self showEmptyViewWithTitle:@"No order found" andNeedToShowImage:YES];
        } else {
            [self showRetryViewWithInternetStrip:NO];
        }
    }
    
}

- (void)timedOutWithoutSuccessForGetProjectSummaryWebService:(NLGetProjectSummary *)webService
{
    self.networkRequestInProgress = NO;
}

#pragma mark - save order search

- (void)saveOrderSearchDetails
{
    NSManagedObjectContext *context = [AppDelegate sharedInstance].managedObjectContext;
    CDUser *persistenceUser = [CDUser userWithUsername:[AppDelegate sharedInstance].viewModel.app.loggedInUser.username inManagedObjectContext:context];
    
    NSArray *arrayOfRecentSearchedOrders = [persistenceUser.recentlySearchedOrders allObjects];
    
    CDRecentSearchedOrder *searchedOrder = nil;
    for (CDRecentSearchedOrder *recentSearchedOrder in arrayOfRecentSearchedOrders) {
        if ([recentSearchedOrder.orderRef isEqualToString:self.project.projectRef]) {
            searchedOrder = recentSearchedOrder;
        }
    }
    
    if (searchedOrder == nil) {
        searchedOrder = [CDRecentSearchedOrder newRecentSearchedOrderInManagedObjectContext:context];
        searchedOrder.orderRef = self.project.projectRef;
        searchedOrder.orderStatus = self.project.subStatus;
        searchedOrder.orderDescription = self.project.productName;
        searchedOrder.completionDate = self.project.targetDeliveryDate;
        searchedOrder.placedOnDate = self.project.projectStartDate;
        searchedOrder.lastSearchedDate = [NSDate date];
        
        [persistenceUser addRecentlySearchedOrdersObject:searchedOrder];
    }
    else
    {
        searchedOrder.orderStatus = self.project.subStatus;
        searchedOrder.completionDate = self.project.targetDeliveryDate;
        searchedOrder.lastSearchedDate = [NSDate date];
        
        [persistenceUser addRecentlySearchedOrdersObject:searchedOrder];
    }
}

#pragma mark - retry view delegate

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    [self getProjectSummary];
}

#pragma mark - NLGetProjectEnquiries delegate

- (void)getProjectEnquiriesWebService:(NLGetProjectEnquiries *)webService finishedWithResponse:(NSObject *)response
{
    self.networkRequestInProgress = NO;
    self.hidesTableWhileLoading = YES;
    [self.project updateWithProjectEnquiriesResponse:(NSDictionary*)response];
    [self refreshTableView];
}

- (void)getProjectEnquiriesWebService:(NLGetProjectEnquiries *)webService failedWithWebServiceError:(NLWebServiceError *)error
{
    [AppManager trackWebServiceError:error FromAPI:webService.friendlyName OnPage:self.pageNameForOmnitureTracking];
    self.networkRequestInProgress = NO;
    self.hidesTableWhileLoading = YES;
    [self refreshTableView];
}

- (void)timedOutWithoutSuccessForGetProjectEnquiriesWebService:(NLGetProjectEnquiries *)webService
{
    self.networkRequestInProgress = NO;
    self.hidesTableWhileLoading = YES;
}

#pragma mark - omniture

- (NSString *)pageNameForOmnitureTracking
{
    return OMNIPAGE_OCS_ORDER_DETAIL;
}

- (void)trackPageForOmniture
{
    if (self.project.productService && self.project.subStatus) {
        [self trackPageForOmnitureWithProductService:self.project.productService andOrderStatus:self.project.subStatus];
    } else {
        [super trackPageForOmniture];
    }
}

@end
