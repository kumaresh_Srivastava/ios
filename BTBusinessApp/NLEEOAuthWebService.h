//
//  NLEEOAuthWebService.h
//  BTBusinessApp
//

#import "NLEEWebService.h"

@class EEAuthenticationToken;
@class NLWebServiceError;
@class NLEEOAuthWebService;

@protocol NLEEOAuthWebServiceDelegate <NSObject>

- (void)eeOAuthWebService:(NLEEOAuthWebService *)service loginSuccesfulWithToken:(BTAuthenticationToken *)oauthToken;

- (void)eeOAuthWebService:(NLEEOAuthWebService *)service failedWithError:(NLWebServiceError *)error;

@end

@interface NLEEOAuthWebService : NLEEWebService {
    
}

@property (nonatomic, weak) id <NLEEOAuthWebServiceDelegate> eeOAuthServiceDelegate;

@end
