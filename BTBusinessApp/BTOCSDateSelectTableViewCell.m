//
//  BTOCSDateSelectTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSDateSelectTableViewCell.h"
#import <JTCalendar/JTCalendar.h>
#import "AppManager.h"
#import "BTOCSBaseTableViewController.h"


@interface BTOCSDateSelectTableViewCell () <JTCalendarDelegate>

@property (strong, nonatomic) JTCalendarMenuView *calanderMenuView;
@property (strong, nonatomic) JTCalendarManager *calendarManager;
@property (strong, nonatomic) JTVerticalCalendarView *calanderContentView;
@property (nonatomic, strong) UIViewController *calendarVC;

@end

@implementation BTOCSDateSelectTableViewCell

+ (NSString *)reuseId
{
    return @"BTOCSDateSelectTableViewCell";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.outlineView.layer.borderWidth = 1.0f;
    self.outlineView.layer.cornerRadius = 5.0f;
    self.outlineView.layer.borderColor = [BrandColours colourBtNeutral80].CGColor;
    
    UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showOptions)];
    [self.outlineView addGestureRecognizer:tapped];
    
    self.detailLabel.text = @"Select date";
    
    [self createCalanderView];
}

- (void) createCalanderView {
    
    self.calendarManager = [JTCalendarManager new];
    self.calendarManager.delegate = self;
    
    self.calanderMenuView = [[JTCalendarMenuView alloc] initWithFrame:CGRectMake(0, 0, kScreenSize.width, 40)];
    self.calanderMenuView.backgroundColor = [UIColor whiteColor];
    
    self.calanderContentView = [[JTVerticalCalendarView alloc] init];
    self.calanderContentView.backgroundColor = [UIColor whiteColor];
    
    [self.calendarManager setMenuView:self.calanderMenuView];
    [self.calendarManager setContentView:self.calanderContentView];
    if (self.dateSelected) {
        [self.calendarManager setDate:self.dateSelected];
    } else {
        [self.calendarManager setDate:[NSDate date]];
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)showOptions {
    if ([self.delegate isKindOfClass:[BTOCSBaseTableViewController class]]) {
        [(BTOCSBaseTableViewController*)self.delegate trackOmniClick:@"Calendar"];
    }
    self.calendarVC = [[UIViewController alloc] init];
    self.calendarVC.view = self.calanderContentView;
    if (self.dateSelected) {
        [self.calendarManager setDate:self.dateSelected];
        self.calendarVC.title = [AppManager NSStringFromNSDateWithMonthYearWithSpaceeFormat:self.dateSelected];
    } else {
        self.calendarVC.title = [AppManager NSStringFromNSDateWithMonthYearWithSpaceeFormat:[NSDate date]];
    }
    self.calendarVC.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.calendarVC.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveAndCloseCalendarView)];
    [[(UIViewController*)self.delegate navigationController] pushViewController:self.calendarVC animated:YES];
}

#pragma mark - CalendarManager delegate

// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    
    [dayView.dotView setHidden:YES];
    [dayView.circleView setHidden:YES];
    [dayView.textLabel setFont:[UIFont fontWithName:kBtFontBold size:15.0]];
    // Today
    // Selected date
    //BOOL isDateHighlighted = NO;
    if(self.dateSelected && [self.calendarManager.dateHelper date:self.dateSelected isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        //isDateHighlighted = YES;
        dayView.circleView.backgroundColor = [BrandColours colourBackgroundBTPurplePrimaryColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Other month
    else if(![self.calendarManager.dateHelper date:self.calanderContentView.date isTheSameMonthThan:dayView.date]){
        dayView.circleView.hidden = YES;
        dayView.textLabel.textColor = [BrandColours colourBtNeutral50];
    }
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
        dayView.textLabel.textColor = [BrandColours colourTextBTPurplePrimaryColor];
    }
    
    
    if([dayView isFromAnotherMonth]){
        dayView.circleView.hidden = YES;
        dayView.textLabel.textColor = [BrandColours colourMyBtLightPurple];;
    }
    
    NSDate *date = [NSDate date];
    if (([date compare:dayView.date] == NSOrderedDescending)) {
        dayView.textLabel.textColor = [BrandColours colourBtNeutral50];
    }
    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.day = 90;
    NSDate *newDate = [cal dateByAddingComponents:components toDate:date options:0];
    
    if (([newDate compare:dayView.date] == NSOrderedAscending)) {
        dayView.textLabel.textColor = [BrandColours colourBtNeutral50];
    }
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    if ([self.calendarManager.dateHelper date:dayView.date isEqualOrAfter:[NSDate date]] && ![self.calendarManager.dateHelper date:dayView.date isTheSameDayThan:[NSDate date]]) {
        self.dateSelected = dayView.date;
        self.detailLabel.text = [AppManager NSStringFromNSDateWithSlashFormat:self.dateSelected];
        self.calendarVC.title = [AppManager NSStringFromNSDateWithMonthYearWithSpaceeFormat:self.dateSelected];
        // Animation for the circleView
        dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
        
        
        // Don't change page in week mode because block the selection of days in first and last weeks of the month
        if(self.calendarManager.settings.weekModeEnabled){
            return;
        }
        
        if(![self.calendarManager.dateHelper date:self.calanderContentView.date isTheSameMonthThan:dayView.date]){
            if([self.calanderContentView.date compare:dayView.date] == NSOrderedAscending){
                [self.calanderContentView loadNextPageWithAnimation];
            }
            else{
                [self.calanderContentView loadPreviousPageWithAnimation];
            }
        }
        
        
        [UIView transitionWithView:dayView
                          duration:.3
                           options:0
                        animations:^{
                            dayView.circleView.transform = CGAffineTransformIdentity;
                            [self.calendarManager reload];
                        } completion:nil];
    }
    
}

#pragma mark - CalendarManager delegate - Page mangement

// Used to limit the date for the calendar, optional
- (BOOL)calendar:(JTCalendarManager *)calendar canDisplayPageWithDate:(NSDate *)date
{
    return [self.calendarManager.dateHelper date:date isEqualOrAfter:[NSDate date]];
}

//if the following methods are not getting used delete them
- (void)calendarDidLoadNextPage:(JTCalendarManager *)calendar
{
    
}

- (void)calendarDidLoadPreviousPage:(JTCalendarManager *)calendar
{
    
}

#pragma mark - button action

- (void)saveAndCloseCalendarView {
    [self.calendarVC.navigationController popViewControllerAnimated:YES];
}

@end
