//
//  BTPublicWifiUnavailableViewController.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTPublicWifiUnavailableViewController.h"
#import "AppManager.h"
#import "AppConstants.h"
#import "OmnitureManager.h"

#import "BTProductSelectTableViewController.h"
#import "BTHubHealthcheckViewController.h"
#import "BTResilientHubAsset.h"
#import "BTClientServiceInstance.h"
#import "BTCheckServiceStatusViewController.h"
#import "BTHomeViewController.h"

#import "UIButton+BTButtonProperties.h"


@interface BTPublicWifiUnavailableViewController ()

@property (nonatomic,strong) UILabel *pageTitleLabel;
@property (nonatomic,strong) UILabel *pageSubTitleLabel;
@property (nonatomic, strong) BTClientServiceInstance *serviceInstance;

@end

@implementation BTPublicWifiUnavailableViewController

+ (BTPublicWifiUnavailableViewController *)getBTPublicWifiUnavailableViewController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"BTPublicWifi" bundle:nil];
    BTPublicWifiUnavailableViewController *controller = (BTPublicWifiUnavailableViewController *)[storyboard instantiateViewControllerWithIdentifier:@"BTPublicWifiUnavailableViewController"];
    return controller;
}

- (instancetype)initWithContext:(BTPublicWifiUnavailableContext)context
{
    self = [self initWithClientServiceInstance:nil andContext:context];
    return self;
}

- (instancetype)initWithClientServiceInstance:(BTClientServiceInstance *)instance andContext:(BTPublicWifiUnavailableContext)context
{
    self = [BTPublicWifiUnavailableViewController getBTPublicWifiUnavailableViewController];
    if (self) {
        if (instance) {
            self.serviceInstance = instance;
        } else {
            self.serviceInstance = nil;
        }
        self.context = context;
    }
    return self;
}

- (instancetype)initWithClientServiceInstance:(BTClientServiceInstance *)instance context:(BTPublicWifiUnavailableContext)context wifiNetwork:(NSString *)network andState:(NSString *)state
{
    self = [self initWithClientServiceInstance:instance andContext:context];
    if (self) {
        if (self.context == BTPublicWifiUnavailableContextGuestWiFiInProgress) {
            
            [self.alertHeadlineLabel setText:[NSString stringWithFormat:@"We're switcing %@ %@ for you",state,network]];
            [self.alertDetailTextView setText:[NSString stringWithFormat:@"Switching %@ %@ can take up to 24 hours.\nSo you'll be up and running soon",state,network]];
            [self.alertDetailTextView setFont:[UIFont fontWithName:kBtFontRegular size:20.0]];
            [self.alertDetailTextView setTextAlignment:NSTextAlignmentCenter];
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createInitialUI];
    [self trackPageForOmniture];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)createInitialUI
{
    //CGSize screenSize = [UIScreen mainScreen].bounds.size;
    
    /*self.pageTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 208, 20)];
    self.pageTitleLabel.font = [UIFont fontWithName:kBtFontBold size:15.0];
    self.pageTitleLabel.textColor = [UIColor whiteColor];
    self.pageTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.pageTitleLabel.text = self.serviceInstance?self.serviceInstance.serviceName:@"Public wi-fi";
    
    self.pageSubTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 19, 208, 17.5)];
    self.pageSubTitleLabel.font = [UIFont fontWithName:kBtFontRegular size:13.0];
    self.pageSubTitleLabel.textColor = [UIColor whiteColor];
    self.pageSubTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.pageSubTitleLabel.text = self.serviceInstance?self.serviceInstance.serviceId:@"";
    
    UIView *titlesView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 208, 40)];
    [titlesView addSubview:self.pageSubTitleLabel];
    [titlesView addSubview:self.pageTitleLabel];
    
    [self.navigationItem setTitleView:titlesView];*/
    self.navigationItem.title = @"Public wi-fi";
    
    //self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon_Home_White"] style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonAction)];
    
    if (self.context == BTPublicWifiUnavailableContextContactAdmin) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon_Home_White"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonAction:)];
    } else {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Back_Arrow"] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonAction:)];
    }
    
    self.navigationItem.rightBarButtonItem = nil;
    
    //self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.view.backgroundColor = [BrandColours colourBtWhite];
    //[self.alertActionButton.layer setCornerRadius:self.alertActionButton.frame.size.height/8];
    
    [self.alertActionButton getBTButtonStyle];
    
    [self updateAlertImage];
    [self updateHeadlineText];
    [self updateDetailText];
    [self updateActionButton];
}

- (void)updateAlertImage
{
    switch (self.context) {
        case BTPublicWifiUnavailableContextGuestWiFiInProgress:
            [self.alertImageView setImage:[UIImage imageNamed:@"error_red"]];
            break;
            
        case BTPublicWifiUnavailableContext4GAssureEnabled:
        case BTPublicWifiUnavailableContextContactAdmin:
        case BTPublicWifiUnavailableContextBroadbandNotWorking:
        case BTPublicWifiUnavailableContextNoConnection:
        case BTPublicWifiUnavailableContextGenericError:
        case BTPublicWifiUnavailableContextGenericError4GAssure:
        default:
            [self.alertImageView setImage:[UIImage imageNamed:@"error_red"]];
            break;
    }
}

- (void)updateHeadlineText
{
    switch (self.context) {
        case BTPublicWifiUnavailableContextGuestWiFiInProgress:
            [self.alertHeadlineLabel setText:@"We're switcing on Guest Wi-Fi for you"];
            [self.alertHeadlineLabel setFont:[UIFont fontWithName:kBtFontBold size:18.0]];
            break;
            
        case BTPublicWifiUnavailableContext4GAssureEnabled:
        case BTPublicWifiUnavailableContextBroadbandNotWorking:
            [self.alertHeadlineLabel setText:@"Public wi-fi unavailable"];
            [self.alertHeadlineLabel setFont:[UIFont fontWithName:kBtFontBold size:18.0]];
            break;

        case BTPublicWifiUnavailableContextContactAdmin:
            [self.alertHeadlineLabel setText:@"Please check your sign-in details"];
            [self.alertHeadlineLabel setFont:[UIFont fontWithName:kBtFontRegular size:17.0]];
            [self.alertHeadlineLabel setTextColor: [UIColor colorForHexString:@"666666"]];
            break;
            
        case BTPublicWifiUnavailableContextNoConnection:
            [self.alertHeadlineLabel setText:@"No internet connection"];
            [self.alertHeadlineLabel setFont:[UIFont fontWithName:kBtFontBold size:18.0]];
            break;
            
        case BTPublicWifiUnavailableContextGenericError:
        case BTPublicWifiUnavailableContextGenericError4GAssure:
        default:
            [self.alertHeadlineLabel setText:@"Something went wrong"];
            [self.alertHeadlineLabel setFont:[UIFont fontWithName:kBtFontBold size:18.0]];
            break;
    }
}

- (void)updateDetailText
{
    switch (self.context) {
        case BTPublicWifiUnavailableContextGuestWiFiInProgress:
            [self.alertDetailTextView setText:@"Switching on Guest Wi-Fi can take up to 24 hours.\nSo you'll be up and running soon"];
            [self.alertDetailTextView setFont:[UIFont fontWithName:kBtFontRegular size:17.0]];
            [self.alertDetailTextView setTextAlignment:NSTextAlignmentCenter];
            break;
            
        case BTPublicWifiUnavailableContext4GAssureEnabled:
            [self.alertDetailTextView setText:@"We're sorry, public wi-fi can't be used when 4G Assure is on."];
            [self.alertDetailTextView setFont:[UIFont fontWithName:kBtFontRegular size:17.0]];
            [self.alertDetailTextView setTextAlignment:NSTextAlignmentCenter];
            break;
            
        case BTPublicWifiUnavailableContextBroadbandNotWorking:
            [self.alertDetailTextView setText:@"We're sorry, public wi-fi can't be used when broadband is not working."];
            [self.alertDetailTextView setFont:[UIFont fontWithName:kBtFontRegular size:17.0]];
            [self.alertDetailTextView setTextAlignment:NSTextAlignmentCenter];
            break;
            
        case BTPublicWifiUnavailableContextContactAdmin:
        {

            NSString* text1 = @"You'll need to have ordered broadband to use this service.\n\n";
            NSString* text2 = @"How to sign in:";
            NSString* text3 = @"\n\nPlease use the My Account username that we set up when the service was ordered.\n\nYou can find this on the order confirmation. It may be a @btconnect.com email.";

            NSString* baseString = [NSString stringWithFormat:@"%@%@%@",text1,text2,text3];
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:baseString attributes:@{
                                                                                                                                    NSFontAttributeName: [UIFont fontWithName:kBtFontRegular size: 14.0f],
                                                                                                                                    NSForegroundColorAttributeName: [UIColor colorForHexString:@"666666"]
                                                                                                                                    }];
            [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"BTFont-Bold" size: 14.0f] range:NSMakeRange(text1.length, text2.length)];
            [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorForHexString:@"333333"] range:NSMakeRange(text1.length, text2.length)];
            [self.alertDetailTextView setAttributedText:attributedString];
            [self.alertDetailTextView setTextAlignment:NSTextAlignmentCenter];
        }
            break;
            
        case BTPublicWifiUnavailableContextNoConnection:
            [self.alertDetailTextView setText:@"Please check your hub is connected correctly and try again."];
            [self.alertDetailTextView setFont:[UIFont fontWithName:kBtFontRegular size:17.0]];
            [self.alertDetailTextView setTextAlignment:NSTextAlignmentCenter];
            break;
            
        case BTPublicWifiUnavailableContextGenericError:
        case BTPublicWifiUnavailableContextGenericError4GAssure:
        default:
            [self.alertDetailTextView setText:@"Sorry there's been an unexpected error. Following the steps below can help put things right.\n\n\n1. Make sure your BT hub is plugged in and switched on.\n\n2. Restart your BT hub manually and wait for 15 minutes before trying again.\n\n3. If you've just set up a new BT hub it may still be updating - please try again in a day or two."];
            [self.alertDetailTextView setFont:[UIFont fontWithName:kBtFontRegular size:14.0]];
            [self.alertDetailTextView setTextAlignment:NSTextAlignmentJustified];
            break;
    }
}

- (void)updateActionButton
{
    switch (self.context) {
        case BTPublicWifiUnavailableContext4GAssureEnabled:
        case BTPublicWifiUnavailableContextBroadbandNotWorking:
        case BTPublicWifiUnavailableContextContactAdmin:
            [self.alertActionButton setEnabled:NO];
            [self.alertActionButton setHidden:YES];
            break;
        
        case BTPublicWifiUnavailableContextNoConnection:
            [self.alertActionButton setEnabled:YES];
            [self.alertActionButton setHidden:NO];
            [self.alertActionButton setTitle:@"Try again" forState:UIControlStateNormal];
            break;
            
        case BTPublicWifiUnavailableContextGuestWiFiInProgress:
            [self.alertActionButton setEnabled:YES];
            [self.alertActionButton setHidden:NO];
            [self.alertActionButton setTitle:@"OK" forState:UIControlStateNormal];
            break;
            
        case BTPublicWifiUnavailableContextGenericError:
        case BTPublicWifiUnavailableContextGenericError4GAssure:
        default:
            [self.alertActionButton setEnabled:YES];
            [self.alertActionButton setHidden:NO];
            [self.alertActionButton setTitle:@"Hub healthcheck" forState:UIControlStateNormal];
            break;
    }
}

#pragma mark - Action Methods


- (void)backButtonAction:(id)sender {
    
    UIViewController *parent = nil;
    
    switch (self.context) {
        case BTPublicWifiUnavailableContextGenericError4GAssure:
            for (UIViewController *vc in self.navigationController.viewControllers) {
                if ([vc isKindOfClass:[BTCheckServiceStatusViewController class]]) {
                    [self.navigationController popToViewController:vc animated:YES];
                    break;
                }
            }
            break;
            
        case BTPublicWifiUnavailableContextContactAdmin:
            
            for (UIViewController *vc in self.navigationController.viewControllers) {
                if ([vc isKindOfClass:[BTSideMenuViewController class]]) {
                    parent = vc;
                    [self.navigationController popToViewController:vc animated:YES];
                    break;
                }
            }
            for (UIViewController *vc in self.navigationController.viewControllers) {
                if (!parent) {
                    if ([vc isKindOfClass:[BTHomeViewController class]]) {
                        parent = vc;
                        [self.navigationController popToViewController:vc animated:YES];
                        break;
                    }
                }
            }
            break;
        
        case BTPublicWifiUnavailableContext4GAssureEnabled:
        case BTPublicWifiUnavailableContextBroadbandNotWorking:
        case BTPublicWifiUnavailableContextNoConnection:
        case BTPublicWifiUnavailableContextGuestWiFiInProgress:
        case BTPublicWifiUnavailableContextGenericError:
        default:
            [self.navigationController popViewControllerAnimated:YES];
            break;
    }
}

- (IBAction)buttonAction:(id)sender {
    
    BTResilientHubAsset *asset;
    BTHubHealthcheckViewController *healthcheckViewController;
    
    switch (self.context) {
        case BTPublicWifiUnavailableContext4GAssureEnabled:
        case BTPublicWifiUnavailableContextBroadbandNotWorking:
        case BTPublicWifiUnavailableContextContactAdmin:
            // nothing to do
            break;
            
        case BTPublicWifiUnavailableContextGuestWiFiInProgress:
            [self trackOmniClick:OMNICLICK_PUBLICWIFI_MANAGE_ERRACTION_OK];
            
        case BTPublicWifiUnavailableContextNoConnection:
            [self.navigationController popViewControllerAnimated:YES];
            if (self.context == BTPublicWifiUnavailableContextNoConnection) {
                [self trackOmniClick:OMNICLICK_PUBLICWIFI_MANAGE_ERRACTION_HUB];
                for (UIViewController *vc in self.navigationController.viewControllers) {
                    if ([vc isKindOfClass:[BTProductSelectTableViewController class]]) {
                        BTProductSelectTableViewController *parent = (BTProductSelectTableViewController*)vc;
                        if (parent.context == BTAccountSelectContextPublicWifi) {
                            [parent refreshAPICall];
                        }
                    }
                }
            }
            break;
        
        case BTPublicWifiUnavailableContextGenericError:
        case BTPublicWifiUnavailableContextGenericError4GAssure:
        default:
            // show hub healthcheck
            asset = [[BTResilientHubAsset alloc] init];
            asset.serviceID = self.serviceInstance.serviceId;
            asset.hubSerialNumber = self.serviceInstance.hubSerialNumber;
           
            healthcheckViewController = [[BTHubHealthcheckViewController alloc] initWithAssets:[@[asset] mutableCopy]];
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:healthcheckViewController];
            [self presentViewController:navController animated:YES completion:nil];
            [self trackOmniClick:OMNICLICK_PUBLICWIFI_MANAGE_ERRACTION_TRYAGAIN];
            break;
    }
}

#pragma mark - Omniture Methods

- (NSString*)pageNameForOmnitureTracking
{
    NSString *pageName = @"";
    switch (self.context) {
        case BTPublicWifiUnavailableContextGuestWiFiInProgress:
            pageName = OMNIPAGE_PUBLICWIFI_MANAGE_SWITCHINGON;
            break;
            
        case BTPublicWifiUnavailableContextContactAdmin:
            pageName = OMNIPAGE_PUBLICWIFI_MANAGE_ERRNOADMIN;
            break;
            
        case BTPublicWifiUnavailableContext4GAssureEnabled:
            pageName = OMNIPAGE_PUBLICWIFI_MANAGE_PWUNAVAILABLE;
            break;
            
        case BTPublicWifiUnavailableContextBroadbandNotWorking:
            pageName = OMNIPAGE_PUBLICWIFI_MANAGE_ERRNOBB;
            break;
            
        case BTPublicWifiUnavailableContextGenericError4GAssure:
            pageName = OMNIPAGE_SERVICE_STATUS_ERRSOMETHINGWRONG;
            break;
            
        case BTPublicWifiUnavailableContextNoConnection:
        case BTPublicWifiUnavailableContextGenericError:
        default:
            pageName = OMNIPAGE_PUBLICWIFI_MANAGE_ERRSOMETHINGWRONG;
            break;
    }
    return pageName;
}

- (void)trackPageForOmniture
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = [self pageNameForOmnitureTracking];;
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    [data setValue:self.serviceInstance.productClass forKey:kOmniHubType];
   
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackOmniClick:(NSString *)linkTitle
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:self.serviceInstance.productClass forKey:kOmniHubType];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",[self pageNameForOmnitureTracking],linkTitle] withContextInfo:data];
}

@end
