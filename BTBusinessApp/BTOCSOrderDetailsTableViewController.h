//
//  BTOCSOrderDetailsTableViewController.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 04/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTOCSBaseTableViewController.h"


@interface BTOCSOrderDetailsTableViewController : BTOCSBaseTableViewController

@property (strong, nonatomic) BTOCSProject *project;

+ (BTOCSOrderDetailsTableViewController *)getOCSOrderDetailsViewController;

- (void)getProjectSummary;
- (void)getProjectEnquiries;


@end
