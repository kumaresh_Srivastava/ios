//
//  BTDoubleDateTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 27/11/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTDoubleDateTableViewCell.h"

@implementation BTDoubleDateTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
