//
//  NLAPIGEEWebServiceError.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 19/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLWebServiceError.h"

@interface NLAPIGEEWebServiceError : NLWebServiceError

@property (nonatomic, readonly) NSString *errorCode;
@property (nonatomic, readonly) NSString *errorDescription;

- (id)initWithError:(NSError *)error andSourceError:(NLWebServiceError *)sourceError andErrorCode:(NSString*)errorCode andDescription:(NSString*)description;

+ (NLAPIGEEWebServiceError *)apigeeWebServiceNetworkErrorWithErrorCode:(NSString*)errorCode andDescription:(NSString*)description;

+ (BTNetworkErrorCode)codeForApigeeError:(NSString*)error;

@end
