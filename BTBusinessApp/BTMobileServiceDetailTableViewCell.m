//
//  BTMobileServiceDetailTableViewCell.m
//  BTBusinessApp
//
//  Created by kumaresh shrivastava on 25/06/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import "BTMobileServiceDetailTableViewCell.h"



@interface BTMobileServiceDetailTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *headerTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerValueLabel;

@end


@implementation BTMobileServiceDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)upadateWithRowWrapper:(MobileAssetsDetailRowWrapper *)rowWrapper{
    
    self.headerValueLabel.text = rowWrapper.mobilePlan;
   
}

@end
