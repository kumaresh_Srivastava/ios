//
//  NLGetProjectDetails.m
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLGetProjectDetails.h"
#import "NSObject+APIResponseCheck.h"
#import "NLWebServiceError.h"
#import "NLConstants.h"
#import "NLWebService+BackEndAPIErrorHandling.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "BTCug.h"
#import "CDCug+CoreDataClass.h"

@implementation NLGetProjectDetails

- (instancetype)initWithProjectRef:(NSString *)projectRef andSiteId:(NSString *)siteId
{
    NSMutableDictionary *projectDetails = [NSMutableDictionary new];
    [projectDetails setObject:projectRef forKey:@"ProjectRef"];
    [projectDetails setObject:@"BTBusinessApp" forKey:@"Source"];
    [projectDetails setObject:@"" forKey:@"UniqueKey"];
    NSDictionary *projectDetailRequest = [NSDictionary dictionaryWithObjectsAndKeys:projectDetails,@"ProjectDetails",siteId,@"SiteID", nil];
    self = [self initWithProjectDetails:projectDetailRequest];
    return self;
}

- (instancetype)initWithProjectDetails:(NSDictionary*)projectDetails {

    NSString *contactId = [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.contactId;
    
    NSString *endPointURLString =[NSString stringWithFormat:@"bt-business-auth/v1/ocs-orders/project-details/%@?src=%@&Cid=%@&Sid=%@",
                                  projectDetails[@"ProjectDetails"][@"ProjectRef"],
                                  projectDetails[@"ProjectDetails"][@"Source"],
                                  contactId,
                                  projectDetails[@"SiteID"]];
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    return self;
}

- (NSString *)friendlyName
{
    return @"getProjectDetails";
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    [self.getProjectDetailsDelegate getProjectDetailsWebService:self finishedWithResponse:responseObject];
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
    
    [self.getProjectDetailsDelegate getProjectDetailsWebService:self failedWithWebServiceError:webServiceError];
}

@end
