//
//  BTSegmentedControl.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 21/12/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTSegmentedControl.h"
#import "BrandColours.h"
#import "AppConstants.h"

@implementation BTSegmentedControl

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib
{
    [super awakeFromNib];
    UIFont *font = [UIFont fontWithName:kBtFontRegular size:16.0];
    NSDictionary* selectedAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[BrandColours colourBtWhite], NSForegroundColorAttributeName, font, NSFontAttributeName, nil];
    
    NSDictionary* normalAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[BrandColours colourBtLightBlack], NSForegroundColorAttributeName, font, NSFontAttributeName, nil];
    
    UIImage *unhighlightedImage = [[UIImage imageNamed:@"Segmented_Control_Left"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
    UIImage *highlightedImage = [[UIImage imageNamed:@"Segmented_Control_Right"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
    
    [self setTintColor:UIColor.clearColor];
    [self setBackgroundImage:unhighlightedImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [self setBackgroundImage:highlightedImage forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [self setDividerImage:[UIImage imageNamed:@"blank_divider"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];

    [self setTitleTextAttributes:normalAttributes forState:UIControlStateNormal];
    [self setTitleTextAttributes:selectedAttributes forState:UIControlStateSelected];
    
    for (int i=0; i<self.numberOfSegments; i++) {
        [self setContentOffset:CGSizeMake(0, -6) forSegmentAtIndex:i];
    }
    
}

@end
