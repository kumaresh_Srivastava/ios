//
//  BTOrderDashboardTableViewController.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 06/11/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSBaseTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BTOrderDashboardTableViewController : BTOCSBaseTableViewController

//typedef NS_ENUM(NSUInteger, TrackOrderDashBoardType) {
//    TrackOrderDashBoardTypeOpenOrder   = 1,
//    TrackOrderDashBoardTypeCompletedOrder  = 2
//};

@property (nonatomic) NSArray *cugs;
@property (nonatomic) NSDictionary *firstPageOpenOrderDataDict;

+ (BTOrderDashboardTableViewController*)getOrderDashboard;

@end

NS_ASSUME_NONNULL_END
