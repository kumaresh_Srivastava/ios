//
//  BTOCSSummaryMilestoneTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 05/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSSummaryMilestoneTableViewCell.h"
#import "AppManager.h"

@interface BTOCSSummaryMilestoneTableViewCell ()

@property (nonatomic, strong) NSLayoutConstraint *collapseConstraint;
@property (nonatomic) BOOL isShowingExpandedView;
@property (strong, nonatomic) IBOutlet BTShadowedView *shadowView;

@end

@implementation BTOCSSummaryMilestoneTableViewCell

+ (NSString *)reuseId
{
    return @"BTOCSSummaryMilestoneTableViewCell";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
//    self.statusLabel.layer.borderWidth = 1.0f;
//    self.statusLabel.layer.cornerRadius = 2.0f;
//    self.statusLabel.layer.borderColor = [BrandColours colourMyBtGreen].CGColor;
    
    [self.statusLabel setOrderStatus:@"Unknown"];
    
    [self.shadowView setElevation:2.0];
    
    UIBezierPath* trianglePath = [UIBezierPath bezierPath];
    [trianglePath setLineWidth:0.0];
    [trianglePath moveToPoint:CGPointMake(self.triangleView.frame.size.width/2, 0)];
    [trianglePath addLineToPoint:CGPointMake(self.triangleView.frame.size.width/2+10,self.triangleView.frame.size.height)];
    [trianglePath addLineToPoint:CGPointMake(self.triangleView.frame.size.width/2-10,self.triangleView.frame.size.height)];
    [trianglePath closePath];
    
    CAShapeLayer *triangleMaskLayer = [CAShapeLayer layer];
    [triangleMaskLayer setLineWidth:0.0];
    [triangleMaskLayer setPath:trianglePath.CGPath];
    
    self.triangleView.layer.mask = triangleMaskLayer;
    
    self.expandedView.hidden = YES;
    self.triangleView.hidden = YES;
    [self.actionButton setImage:[UIImage imageNamed:@"OCSTooltip"] forState:UIControlStateNormal];
    
    self.collapseConstraint = [NSLayoutConstraint constraintWithItem:self.expandedView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:0.0f];
    [self.expandedView addConstraint:self.collapseConstraint];
    
    self.isShowingExpandedView = NO;
    
    [self.actionButton addTarget:self action:@selector(showOrHideExpandedView) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateWithStartDate:(NSDate*)startDate dueDate:(NSDate*)dueDate completionDate:(NSDate*)completionDate
{
    NSDate *invalidDate = [AppManager dateFromString:@"0001-01-01T00:00:00"];
    
    if (completionDate && ![completionDate isEqualToDate:invalidDate]) {
        //self.statusLabel.text = @"Completed";
        [self.statusLabel setOrderStatus:@"Completed"];
        self.completionDateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:completionDate];
        self.completionDateLabel.hidden = NO;
        if (dueDate && ![dueDate isEqualToDate:invalidDate]) {
            self.targetDateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:dueDate];
            self.targetDateLabel.hidden = NO;
        } else {
            self.targetDateLabel.hidden = YES;
        }
    } else if (!startDate || [startDate isEqualToDate:invalidDate]) {
        //self.statusLabel.text = @"Not started";
        [self.statusLabel setOrderStatus:@"Not started"];
        self.completionDateLabel.hidden = YES;
        if (dueDate && ![dueDate isEqualToDate:invalidDate]) {
            self.targetDateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:dueDate];
            self.targetDateLabel.hidden = NO;
        } else {
            self.targetDateLabel.text = @"TBC";
            self.targetDateLabel.hidden = NO;
        }
    } else if (dueDate && ![dueDate isEqualToDate:invalidDate]) {
        self.targetDateLabel.text = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:dueDate];
        self.targetDateLabel.hidden = NO;
        NSDate *today = [NSDate date];
        if ([dueDate compare:today] == NSOrderedAscending) {
            //self.statusLabel.text = @"Overdue";
            [self.statusLabel setOrderStatus:@"Overdue"];
        } else {
            //self.statusLabel.text = @"In progress";
            [self.statusLabel setOrderStatus:@"In progress"];
        }
        self.completionDateLabel.hidden = YES;
    } else {
        //self.statusLabel.text = @"Unknown";
        [self.statusLabel setOrderStatus:@"Unknown"];
        self.completionDateLabel.hidden = YES;
        self.targetDateLabel.text = @"TBC";
        self.targetDateLabel.hidden = NO;
    }
}

- (void)showOrHideExpandedView
{
    if (self.isShowingExpandedView) {
        self.expandedView.hidden = YES;
        self.triangleView.hidden = YES;
        [self.actionButton setImage:[UIImage imageNamed:@"OCSTooltip"] forState:UIControlStateNormal];
        [self.expandedView addConstraint:self.collapseConstraint];
        self.isShowingExpandedView = NO;
    } else {
        self.expandedView.hidden = NO;
        self.triangleView.hidden = NO;
        [self.actionButton setImage:[UIImage imageNamed:@"OCSClose"] forState:UIControlStateNormal];
        [self.expandedView removeConstraint:self.collapseConstraint];
        self.isShowingExpandedView = YES;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(layoutChangeInCell:)]) {
        [self.delegate layoutChangeInCell:self];
    }
}

@end
