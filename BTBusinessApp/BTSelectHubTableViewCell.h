//
//  BTSelectHubTableViewCell.h
//  BTBusinessApp
//
//

#import <UIKit/UIKit.h>

@class HubHealthCheck;

@protocol ChangeHubHealthcheckDelegate

- (void)changeHubHealthcheck:(int)index;

@end

@interface BTSelectHubTableViewCell : UITableViewCell

- (IBAction)onHubChanged:(id)sender;

@property (weak, nonatomic) IBOutlet UISegmentedControl *hubSegmentedControl;

@property (nonatomic, weak) id <ChangeHubHealthcheckDelegate> changeHubHealthcheckDelegate;

@end
