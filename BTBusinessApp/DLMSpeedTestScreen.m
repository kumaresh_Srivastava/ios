#import "DLMSpeedTestScreen.h"
#import "NLBroadbandHubReactiveSpeedTest.h"

#define kMaxAttemptsAtSpeedTest 2

#define kErrorIncorrectIDA @"Not on correct IDA version" // update hub software

#define kErrorWANStatusDown @"WAN status is \"down\"" // please restart your hub
#define kErrorHubInfoNotFound @"HUBINFO_DEVICE_NOT_FOUND" // please restart your hub

#define kErrorHubNeverSupported @"HUBINFO_HUB_WILL_NEVER_BE_SUPPORTED" // upgrade your hub
#define kErrorHubNotYetSupported @"HUBINFO_HUB_NOT_SUPPORTED_YET" // upgrade your hub

@interface DLMSpeedTestScreen() <NLAPIGEEAuthenticationProtectedWebServiceDelegate>{
    
}

@property (nonatomic) NLBroadbandHubReactiveSpeedTest *speedTestWebService;
@property (nonatomic) NSString* serviceID;
@property (nonatomic) NSString* serialNumber;
@property (nonatomic) NSString* hashCode;

@end

@implementation DLMSpeedTestScreen

- (void)performSpeedTestWithServiceID:(NSString*)serviceID serialNumber:(NSString*)serialNumber hashCode:(NSString*)hashCode andForceExecute:(BOOL)forceExecute {
    _timeTestBegan = CFAbsoluteTimeGetCurrent();
    _serialNumber = serialNumber;
    _hashCode = hashCode;
    _serviceID = serviceID;
    self.speedTestWebService = [[NLBroadbandHubReactiveSpeedTest alloc] initWithSerialNumber:serialNumber serviceID:serviceID hashCode:hashCode andForceExecute:forceExecute];
    self.speedTestWebService.delegate = self;
    [self.speedTestWebService resume];
}

- (void)webService:(nonnull NLAPIGEEAuthenticatedWebService *)webService failedWithError:(nonnull NLWebServiceError *)error {
    if (webService == self.speedTestWebService) {
        BOOL handled = NO;
        if ([error isKindOfClass:[NLAPIGEEWebServiceError class]]) {
            NLAPIGEEWebServiceError *apigeeError = (NLAPIGEEWebServiceError*)error;
            if ([apigeeError.errorDescription.lowercaseString isEqualToString:@"requested"] || (apigeeError.errorCode.integerValue == 101)) {
                CFAbsoluteTime timeNow = CFAbsoluteTimeGetCurrent();
                if((timeNow - _timeTestBegan) <= 180) {
                    // keep trying every 15 seconds for 180 seconds - errors like 503s, etc. should be discounted before this time
                    handled = YES;
                    [self performSelector:@selector(rerunSpeedTest) withObject:nil afterDelay:15.0];
                }
            }
        }
        if (!handled) {
            [self.speedTestScreenDelegate speedTestScreen:self failedToPerformSpeedTestWithWebServiceError:error];
        }
    }
}

- (void)rerunSpeedTest {
    self.speedTestWebService = [[NLBroadbandHubReactiveSpeedTest alloc] initWithSerialNumber:_serialNumber serviceID:_serviceID hashCode:_hashCode andForceExecute:NO];
    self.speedTestWebService.delegate = self;
    [self.speedTestWebService resume];
}

- (void)webService:(nonnull NLAPIGEEAuthenticatedWebService *)webService finshedWithSuccessResponse:(nonnull NSObject *)responseObject {
    NSDictionary *responseDictionary = (NSDictionary*) responseObject;
    if(responseDictionary) {
//        if([responseDictionary objectForKey:@"code"]) {
//            [self.speedTestScreenDelegate speedTestScreen:self failedToPerformSpeedTestWithWebServiceError:nil];
//        }
        if([[responseDictionary objectForKey:@"errorMessage"] isEqualToString:kErrorIncorrectIDA]) {
            [self.speedTestScreenDelegate speedTestCompleteShowUpdateHubScreen];
        }
        else if([[responseDictionary objectForKey:@"errorMessage"] isEqualToString:kErrorWANStatusDown] ||
                [[responseDictionary objectForKey:@"errorMessage"] isEqualToString:kErrorHubInfoNotFound]) {
            [self.speedTestScreenDelegate speedTestCompleteShowRestartHubScreen];
        }
        else if([[responseDictionary objectForKey:@"errorMessage"] isEqualToString:kErrorHubNeverSupported] ||
                [[responseDictionary objectForKey:@"errorMessage"] isEqualToString:kErrorHubNotYetSupported]) {
            [self.speedTestScreenDelegate speedTestCompleteShowUpgradeHubScreen];
        }
        else {
            _downStreamSpeed = [[responseDictionary objectForKey:@"downStreamSpeed"] floatValue];
            _upStreamSpeed = [[responseDictionary objectForKey:@"upStreamSpeed"] floatValue];
            _downStreamNonSpeed = [[responseDictionary objectForKey:@"downStreamNonSpeed"] floatValue];
            _upStreamNonSpeed = [[responseDictionary objectForKey:@"upNonSpeed"] floatValue];
            _mGals = [[responseDictionary objectForKey:@"mGals"] floatValue];
            _estimatedSpeed = [[responseDictionary objectForKey:@"estimatedSpeed"] floatValue];
            
            _ragStatus = [responseDictionary objectForKey:@"ragStatus"];
            _errorMessage = [responseDictionary objectForKey:@"errorMessage"];
            
            if([((NSString*)[responseDictionary objectForKey:@"status"]).lowercaseString isEqualToString:@"pass"]) {
                _pass = YES;
            }
            _attemptsAtSpeedTest++;
            [self determineCellType];
            
            [self.speedTestScreenDelegate successfullyPerformedSpeedTest:self];
        }
        
        
    } else {
        [self.speedTestScreenDelegate speedTestScreen:self failedToPerformSpeedTestWithWebServiceError:nil];
    }
}

# pragma mark - utility
-(void)determineCellType {
    if(_attemptsAtSpeedTest < kMaxAttemptsAtSpeedTest) {
        if(_mGals != 0) {
            if(_downStreamSpeed >= _mGals) {
                _downloadResultType = MeetsMGALRequirement;
            } else {
                _downloadResultType = DoesntMeetMGALRequirement1stAttempt;
            }
        } else {
            _downloadResultType = NonMGAL1stAttempt;
        }
    } else {
        if(_mGals != 0) {
            if(_downStreamSpeed >= _mGals) {
                _downloadResultType = MeetsMGALRequirement;
            } else {
                _downloadResultType = DoesntMeetMGALRequirement2ndAttempt;
            }
        } else {
            _downloadResultType = NonMGAL2ndAttempt;
        }
    }
}

@end
