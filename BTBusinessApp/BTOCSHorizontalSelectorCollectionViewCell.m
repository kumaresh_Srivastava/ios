//
//  BTOCSHorizontalSelectorCollectionViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 01/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSHorizontalSelectorCollectionViewCell.h"

@implementation BTOCSHorizontalSelectorCollectionViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UIBezierPath* trianglePath = [UIBezierPath bezierPath];
    [trianglePath setLineWidth:0.0];
    [trianglePath moveToPoint:CGPointMake(self.indicatorView.frame.size.width/2, self.indicatorView.frame.size.height)];
    [trianglePath addLineToPoint:CGPointMake(self.indicatorView.frame.size.width/2+10,0)];
    [trianglePath addLineToPoint:CGPointMake(self.indicatorView.frame.size.width/2-10,0)];
    [trianglePath closePath];
    
    CAShapeLayer *triangleMaskLayer = [CAShapeLayer layer];
    [triangleMaskLayer setLineWidth:0.0];
    [triangleMaskLayer setPath:trianglePath.CGPath];
    
    self.indicatorView.layer.mask = triangleMaskLayer;
    
}

@end
