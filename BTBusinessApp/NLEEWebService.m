//
//  NLEEWebService.m
//  BTBusinessApp
//


#import "NLEEWebService.h"
#import "NLWebServiceError.h"
#import "AFNetworking.h"
#import "AppConstants.h"
#import "NLConstants.h"

@implementation NLEEWebService

- (instancetype)initWithMethod:(NSString *)method parameters:(NSString *)parameters andEndpointUrlString:(NSString *)endpointUrlString
{
    self = [super initWithMethod:method parameters:parameters endpointUrlString:endpointUrlString andBaseURLType:NLBaseURLTypeEE];
    
    return self;
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
}

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andError:(NSError *)error
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andError:error];
    
    if(errorToBeReturned == nil)
    {
        // PROCESS TO FIND ANY POTENTIAL EE ERRORS HERE
    }
    
    return errorToBeReturned;
}

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        
        // (hds) No handling in this class at this point of time.
    }
    
    return errorToBeReturned;
}

- (BOOL)attemptErrorHandlingIfNeededForError:(NLWebServiceError *)webServiceError
{
    BOOL errorHandled = [super attemptErrorHandlingIfNeededForError:webServiceError];
    
    if(errorHandled == NO)
    {
        // (hds) If there was no error handling from superclass, then this class can check if it needs to handle the error or not.
        
        // (hds) Handling the MBaassTimeOut Error and taking action on it.
//        if(webServiceError.error.domain == BTNetworkErrorDomain && webServiceError.error.code == BTNetworkErrorCodeVordelMbassTimeout)
//        {
//            if(_retryCount < kNLWebServiceTimeOutMaxRetryAttempts)
//            {
//                _retryCount++;
//                [self resume];
//                errorHandled = YES;
//            }
//        }
    }
    
    return errorHandled;
}

@end
