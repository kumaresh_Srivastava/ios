//
//  BTAppointmentPickerTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 29/11/2017.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "BTAppointmentPickerTableViewCell.h"

@implementation BTAppointmentPickerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
