//
//  NLUpdateLastLoggedInWebService.h
//  BTBusinessApp
//
//  Created by SaiCharan Movva on 25/04/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLAuthenticationProtectedWebService.h"

@class NLUpdateLastLoggedInWebService;
@class NLWebServiceError;

@protocol NLUpdateLastLoggedInWebServiceDelegate <NSObject>

- (void)versionSubmissionSuccesfullyFinished:(NLUpdateLastLoggedInWebService *)webService;

- (void)versionSubmitWebService:(NLUpdateLastLoggedInWebService *)webService failedToSubmitVersionDetailsWithWebServiceError:(NLWebServiceError *)error;

@end

@interface NLUpdateLastLoggedInWebService : NLAuthenticationProtectedWebService{
    
}
@property (nonatomic, weak) id<NLUpdateLastLoggedInWebServiceDelegate> updateLastLoggedInWebServiceDelegate;

- (instancetype)initWithVersionDetailsDictionary:(NSDictionary *)versionDetailsDictionary andGroupKey:(NSString *)groupKey;

@end
