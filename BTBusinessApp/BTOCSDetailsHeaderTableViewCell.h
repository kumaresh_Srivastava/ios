//
//  BTOCSDetailsHeaderTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

@interface BTOCSDetailsHeaderTableViewCell : BTBaseTableViewCell

@property (strong, nonatomic) IBOutlet BTOrderStatusLabel *prgressLabel;
@property (strong, nonatomic) IBOutlet UILabel *projectReferenceLabel;
@property (strong, nonatomic) IBOutlet UILabel *productLabel;

- (void)updateWithSubStatus:(NSString*)status;
- (void)updateWithProgress:(NSString*)progress andProgressColour:(UIColor*)color;
- (void)updateWithProduct:(NSString*)product;

@end
