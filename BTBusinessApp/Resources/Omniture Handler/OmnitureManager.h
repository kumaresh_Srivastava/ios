//
//  OmnitureManager.h
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 25/11/16.
//  Copyright © 2016 Accolite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADBMobile.h"

#define STATE_PREFIX @"BTB:App:Service:"


//Page tags
#define OMNIPAGE_LAUNCH @"Launch"
#define OMNIPAGE_ONBOARDING_1 @"Onboarding - track an order"
#define OMNIPAGE_ONBOARDING_2 @"Onboarding - bills and assets"
#define OMNIPAGE_ONBOARDING_3 @"Onboarding - faults"
#define OMNIPAGE_ONBOARDING_4 @"Onboarding - notifications"
#define OMNIPAGE_ONBOARDING_5 @"Onboarding - check your service status"
#define OMNIPAGE_ONBOARDING_6 @"Onboarding - cloud voice express"
#define OMNIPAGE_LOGIN @"Login"
#define OMNIPAGE_OneFA @"SecurityCheck:OneFA"
#define OMNIPAGE_SECURITY_NUMBER @"Set security number"
#define OMNIPAGE_SET_PASSOWRD @"Set password"
#define OMNIPAGE_CREATEPIN @"Login:Create PIN"
#define OMNIPAGE_RECREATEPIN @"Login:Re-enter PIN"
#define OMNIPAGE_LOGINPIN @"Login:Enter PIN" 
#define OMNIPAGE_HOME @"Home"
#define OMNIPAGE_NOTIFICATION @"Notifications"
#define OMNIPAGE_NAVIGATE @"Side navigate"
#define OMNIPAGE_MULTICUG @"Multi CUG Home"

// what's new - guest wi-fi
#define OMNIPAGE_ONBOARDING_GUESTWIFI @"Onboarding - guest wi-fi"
#define OMNICLICK_ONBOARDING_GUESTWIFI_MANAGE @"Manage Guest Wi-fi"
#define OMNICLICK_ONBOARDING_GUESTWIFI_SKIP   @"Skip"

#define OMNIPAGE_ONBOARDING_CLOUDVOICEEXPRESS @"Onboarding - Cloud Voice Express"
//#define OMNICLICK_ONBOARDING_CLOUDVOICE_GOTIT @"cloud voice - Got it"
#define OMNICLICK_ONBOARDING_CLOUDVOICE_VIEWUSAGE  @"Onboarding - Cloud Voice Express"

//Shortcuts
#define OMNISHORTCUT_BILLS @"Shortcut:Link:Bills"
#define OMNISHORTCUT_USAGE @"Shortcut:Link:Usage"
#define OMNISHORTCUT_ACCOUNT @"Shortcut:Link:Account"
#define OMNISHORTCUT_SERVICESTATUS @"Shortcut:Link:ServiceStatus"

//Usage
#define OMNIPAGE_USAGE_ACCOUNT @"Usage:Accounts"
#define OMNIPAGE_USAGE_ASSET @"Usage:Account assets"
#define OMNIPAGE_USAGE_PHONE @"Usage:Phone usage"
#define OMNIPAGE_USAGE_CLOUD_VOICE_EXPRESS_OVERVIEW @"Usage:Cloud Voice Express overview"
#define OMNIPAGE_USAGE_CLOUD_VOICE_EXPRESS_SELECT_SERVICE @"Usage:Cloud Voice Express select service"
#define OMNIPAGE_USAGE_CLOUD_VOICE_EXPRESS_USAGE @"Usage:Cloud Voice Express usage"
#define OMNIPAGE_USAGE_CLOUD_VOICE_EXPRESS_NO_DATA_FOUND @"Usage:Cloud Voice Express no data found"
#define OMNIPAGE_USAGE_CLOUD_VOICE_EXPRESS_SOMETHINGWENTWRONG @"Usage:Cloud Voice Express something went wrong"

#define OMNIPAGE_USAGE_BROADBAND_CURRENTLY @"Usage:Broadband currently usage"
#define OMNIPAGE_USAGE_BROADBAND_MONTHLY @"Usage:Broadband monthly usage"

//Settings
#define OMNIPAGE_SETTINGS_HOME @"Settings:Home"
#define OMNIPAGE_SETTINGS_CONTACT @"Settings:Contact details"
#define OMNIPAGE_SETTINGS_EDIT_CONTACT @"Settings:Edit contact details"
#define OMNIPAGE_SETTINGS_SECURITY @"Settings:Security setting"
#define OMNIPAGE_SETTINGS_CURRENT_PIN @"Settings:Enter your current PIN"
#define OMNIPAGE_SETTINGS_NEW_PIN @"Settings:Choose a new PIN"
#define OMNIPAGE_SETTINGS_CONFIRM_NEW_PIN @"Settings:Confirm new PIN"
#define OMNIPAGE_SETTINGS_UPDATE_PASSWORD @"Settings:Update password"
#define OMNIPAGE_SETTINGS_CHANGE_SECURE_QUESTION @"Settings:Change security question"
#define OMNIPAGE_SETTINGS_CHANGE_SECURE_ANSWER @"Settings:Change security answer"
#define OMNIPAGE_SETTINGS_CHANGE_SECURITY_NUMBER @"Settings:Change security number"
#define OMNIPAGE_SETTINGS_NOTIFICATION @"Settings:Notifications"

//More Apps
#define OMNIPAGE_MORE_APPS @"Our apps"

//Assets
#define OMNIPAGE_ASSETS_ACCOUNT @"Assets:Accounts"
#define OMNIPAGE_ASSETS_ACCOUNT_ASSETS @"Assets:Account assets"
#define OMNIPAGE_ASSETS_SERVICES @"Assets:Asset services"
#define OMNIPAGE_ASSETS_DETAIL_SUMMARY @"Assets:Asset details summary"
#define OMNIPAGE_ASSETS_DETAIL_RENTAL @"Assets:Asset details rental"
#define OMNIPAGE_ASSETS_EMPTY @"Assets:No assets to display"
#define OMNIPAGE_ASSETS_MOBILE_SERVICE @"Assets:Mobile Service"

//Public wi-fi
#define OMNIPAGE_PUBLICWIFI_ACCOUNT @"Public wi-fi:Choose account"
#define OMNIPAGE_PUBLICWIFI_ACCOUNT_ASSET @"Public wi-fi:Choose your broadband service"
#define OMNIPAGE_PUBLICWIFI_MANAGE @"Public wi-fi:Manage wi-fi"

#define OMNIPAGE_PUBLICWIFI_MANAGE_GWUNAVAILABLE12 @"Public wi-fi:Manage wi-fi:Guest wifi unavailable (hub1 and 2)"
#define OMNIPAGE_PUBLICWIFI_MANAGE_GWUNAVAILABLE3 @"Public wi-fi:Manage wi-fi:Guest wifi unavailable (hub3)"
//#define OMNIPAGE_PUBLICWIFI_MANAGE_PWUNAVAILABLE @"Public wi-fi:Manage wi-fi:Public wifi unavailable (4g Assure)"
#define OMNIPAGE_PUBLICWIFI_MANAGE_PWUNAVAILABLE @"Public wi-fi:Manage wi-fi:Public wi-fi unavailable (4g assure)"

#define OMNIPAGE_PUBLICWIFI_MANAGE_ERRFIRMWARE @"Public wi-fi:Manage wi-fi:Firmware update required"
#define OMNIPAGE_PUBLICWIFI_MANAGE_ERRNOADMIN @"Public wi-fi:Manage wi-fi:Please contact your administrator"
//#define OMNIPAGE_PUBLICWIFI_MANAGE_ERRNOBB @"Public wi-fi:Manage wi-fi:Public wifi unavailable (no bb)"
#define OMNIPAGE_PUBLICWIFI_MANAGE_ERRNOBB @"Public wi-fi:Manage wi-fi:Public wi-fi unavailable (no bb)"
#define OMNIPAGE_PUBLICWIFI_MANAGE_ERRSOMETHINGWRONG @"Public wi-fi:Manage wi-fi:Something went wrong"
//#define OMNIPAGE_PUBLICWIFI_MANAGE_SWITCHINGON @"Public wi-fi:Manage wi-fi:We're switching on guest wi-fi for you"
#define OMNIPAGE_PUBLICWIFI_MANAGE_SWITCHINGON @"Public wi-fi:Manage wi-fi:Were switched on guest wi-fi for you"

#define OMNICLICK_PUBLICWIFI_VIEWDETAILS @"View details"
#define OMNICLICK_PUBLICWIFI_PSTNVIEWDETAILS @"PSTN view details"
#define OMNICLICK_PUBLICWIFI_BBVIEWDETAILS   @"Broadband view details"

#define OMNICLICK_PUBLICWIFI_MANAGE_ERRACTION_HUB @"Hub healthcheck"
#define OMNICLICK_PUBLICWIFI_MANAGE_ERRACTION_TRYAGAIN @"Try again"
#define OMNICLICK_PUBLICWIFI_MANAGE_ERRACTION_OK @"OK"

#define OMNICLICK_PUBLICWIFI_MANAGE_ORDERNOW @"Order now"
#define OMNICLICK_PUBLICWIFI_MANAGE_UPRGADE @"Upgrade - chat now"

//Bills Screen
#define OMNIPAGE_BILLS_AWAITING_PAYMENT @"Bills:Bills - awaiting payment"
#define OMNIPAGE_BILLS_AWAITING_PAYMENT_CUG @"Bills:Bills - awaiting payment (CUG)"
#define OMNIPAGE_BILLS_PAID @"Bills:Bills - paid"
#define OMNIPAGE_BILLS_NO_BILLS @"Bills:Bills - no bills to display"

#define OMNIPAGE_BILLS_BILL_SUMMARY @"Bills:Bill details summary"
#define OMNIPAGE_BILLS_BILL_CHARGES @"Bills:Bill details charges"

#define OMNIPAGE_BILLS_PAY_BILLS @"Bills:Pick bills to pay"
#define OMNIPAGE_BILLS_PAY_BILL_BY_CARD @"Bills:Pay by Card saved card"

#define OMNIPAGE_BILLS_PAY_BILL_BY_NEW_CARD @"Bills:Pay by new card"
#define OMNIPAGE_BILLS_PAYMENT_CONFIRM @"Bills:Payment confirm"
#define OMNIPAGE_BILLS_PAYMENT_COMPLETE @"Bills:Payment complete"

//Order Tracker

#define OMNIPAGE_ORDER_OPEN @"Order tracker:Open orders"
#define OMNIPAGE_ORDER_OPEN_CUG @"Order tracker:Open orders (CUG)"
#define OMNIPAGE_ORDER_COMPLETED @"Order tracker:Completed orders"
#define OMNIPAGE_ORDER_COMPLETED_CUG @"Order tracker:Completed orders (CUG)"
#define OMNIPAGE_ORDER_SUMMARY @"Order tracker:Order summary"
#define OMNIPAGE_ORDER_DETAILS @"Order tracker:Order details"
#define OMNIPAGE_ORDER_DETAILS_TRACKER @"Order tracker:Order details tracker"
#define OMNIPAGE_ORDER_BILLING_DETAILS @"Order tracker:Order billing details"

#define OMNIPAGE_ORDER_CHANGE_ORDER_DETAIL @"Order tracker:Change order details"

#define OMNIPAGE_ORDER_SITECONTACT_SAVED_CONTACTS @"Order tracker:Site contact - Saved contacts"
#define OMNIPAGE_ORDER_SITECONTACT_HELP @"Order tracker:Site contact - help"
#define OMNIPAGE_ORDER_SITECONTACT_ADD_NEW_CONTACT @"Order tracker:Site contact - Add new contact"

#define OMNIPAGE_ORDER_ALTERNATE_SITECONTACT @"Order tracker:Alternate site contact - Site contacts"
#define OMNIPAGE_ORDER_ALTERNATE_SITECONTACT_HELP @"Order tracker:Alternate site contact - help"
#define OMNIPAGE_ORDER_ALTERNATE_SITECONTACT_ADD_NEW_CONTACT @"Order tracker:Alternate site contact - Add new contact"

#define OMNIPAGE_ORDER_AMEND_APPOINTMENT @"Order tracker:Amend appointment"
#define OMNIPAGE_ORDER_AMEND_APPOINTMENT_HELP @"Order tracker:Amend appointment - help"
#define OMNIPAGE_ORDER_AMEND_ACTIVATION_DATE @"Order tracker:Amend activation date"

#define OMNIPAGE_ORDER_OPEN_EMPTY @"Order tracker:No open orders"
#define OMNIPAGE_ORDER_CLOSED_EMPTY @"Order tracker:No closed orders"

#define OMNIPAGE_ORDER_SEARCH @"Order tracker:Order search"
#define OMNIPAGE_ORDER_SEARCH_HELP @"Order tracker:Order search - help"
#define OMNIPAGE_ORDER_SEARCH_RESULT @"Order tracker:Order search results"

#define OMNIPAGE_ORDER_RESTRICTED @"Order tracker:Order details limited"


#define OMNIPAGE_ORDER_GROUP_ORDER @"Order tracker:Group order"
#define OMNIPAGE_ORDER_BUNDLE_ORDER_SUMMARY @"Order tracker:Bundle order summary"
#define OMNIPAGE_ORDER_BUNDLE_ORDER_DETAILS @"Order tracker:Bundle order details"
#define OMNIPAGE_ORDER_BUNDLE_PRICING_DETAILS @"Order tracker:Bundle pricing details"
#define OMNIPAGE_ORDER_BUNDLE_PRICING_DETAILS_HELP @"Order tracker:Bundle pricing details - help"
#define OMNIMPAGE_ORDER_ERROR @"Order tracker:Error page"

// OCS order tracker
#define OMNIPAGE_OCS_ORDER_DETAIL @"Order tracker OCS:Summary"
#define OMNIPAGE_OCS_ORDER_TRACKER @"Order tracker OCS:Order tracker"
#define OMNIPAGE_OCS_ORDER_NOQUERIES @"Order tracker OCS:No Queries"
#define OMNIPAGE_OCS_ORDER_QUERIES @"Order tracker OCS:Query List"
#define OMNIPAGE_OCS_ORDER_RECENTQUERY @"Order tracker OCS:Recent Query"
#define OMNIPAGE_OCS_ORDER_SENDQUERY @"Order tracker OCS:Send Query"
#define OMNIPAGE_OCS_ORDER_PROJECTDETAILS @"Order tracker OCS:Project Details"
#define OMNIPAGE_OCS_ORDER_PROJECTNOTES @"Order tracker OCS:Project Notes"
#define OMNIPAGE_OCS_ORDER_REQUIREDQUESTIONS @"Order tracker OCS:Required Questions"
#define OMNIPAGE_OCS_ORDER_ACTIVATIONDATE @"Order tracker OCS:Activation date"

#define OMNICLICK_OCS_ORDER_BACK @"Back"
#define OMNICLICK_OCS_ORDER_CLOSE @"Close"
#define OMNICLICK_OCS_ORDER_SAVE @"Save"
#define OMNICLICK_OCS_ORDER_SHOWMORE @"Show more"
#define OMNICLICK_OCS_ORDER_PROJECTDETAILS @"Project details"
#define OMNICLICK_OCS_ORDER_PROJECTNOTES @"Project notes"
#define OMNICLICK_OCS_ORDER_DETAILSMILESTONE @"Details"
#define OMNICLICK_OCS_ORDER_REQDQUESTIONS @"Required questions"
#define OMNICLICK_OCS_ORDER_QUERYDETAILS @"Query details"

//Order Notification
#define OMNINOTIFY_ORDER_POSTCODE_CANCEL @"Enter postcode: cancel"
#define OMNINOTIFY_ORDER_POSTCODE_OK @"Postcode validated: OK"
#define OMNINOTIFY_ORDER_POSTCODE_VIEWDETAILS @"Enter postcode: View details"

//Notifcations
#define OMNICLICK_NOTIFICATIONS_DETAILS @"Notification details"

//Track Faluts
#define OMNIPAGE_FAULT_OPEN @"Track fault:Open faults"
#define OMNIPAGE_FAULT_OPEN_CUG @"Track fault:Open faults (CUG)"
#define OMNIPAGE_FAULT_CLOSED @"Track fault:Closed faults"
#define OMNIPAGE_FAULT_CLOSED_CUG @"Track fault:Closed faults (CUG)"
#define OMNIPAGE_FAULT_DETAIL @"Track fault:Fault details"
#define OMNIPAGE_FAULT_DETAIL_TRACKER @"Track fault:Fault details tracker"
#define OMNIPAGE_FAULT_CHANGE_FAULT_DETAILS @"Track fault:Change fault details"
#define OMNIPAGE_FAULT_DIVERT_CALLS @"Track fault:Amend fault details - divert calls"
#define OMNIPAGE_FAULT_CALL_DIVERSION @"Track fault:Amend Call diversion"
#define OMNIPAGE_FAULT_AMMEND_ENGG_APPOINTMENT @"Track fault:Amend engineer appointment"
#define OMNIPAGE_FAULT_AMEND_SITE_CONTACT    @"Track fault:Amend Site contact - Site Contacts"
#define OMNIPAGE_FAULT_AMEND_SITE_CONTACT_HELP    @"Track fault:Amend Site contact - help"
#define OMNIPAGE_FAULT_AMEND_ADD_NEW_CONTACT @"Track fault:Amend Site contact - Add new contact"
#define OMNIPAGE_FAULT_NO_OPEN_FAULTS @"Track fault:No open faults by fault ref"
#define OMNIPAGE_FAULT_NO_CLOSED_FAULTS  @"Track fault:No closed  faults by fault ref"
#define OMNIPAGE_FAULT_NO_OPEN_FAULTS_PHONE  @"Track fault:No open faults by phone number"
#define OMNIPAGE_FAULT_NO_CLOSED_FAULTS_PHONE @"Track fault:No closed faults by phone number"
#define OMNIPAGE_FAULT_AGENT_COMMENT @"Track fault:Agents comments"
#define OMNIPAGE_FAULT_SEARCH_FAULT  @"Search fault:Search faults"
#define OMNIPAGE_FAULT_SEARCH_FAULT_HELP  @"Search fault:Search faults - help"
#define OMNIPAGE_FAULT_SEARCH_FAULT_RESULTS  @"Search fault:Search faults results"
#define OMNIPAGE_FAULT_LOCKED @"Search fault:Fault details limited"
#define OMNIPAGE_FAULT_SEARCH_FAULT_OPEN @"Search phone number:Open fault details"
#define OMNIPAGAE_FAULT_SEARCH_FAULT_CLOSED @"Search phone number:Closed fault details"




//Help and support screen

#define OMNIPAGE_HELP_HOME_START @"Help:Home"
#define OMNIPAGE_HELP_HOME_START_CLOSED @"Help:Chat closed"
#define OMNIPAGE_HELP_CHAT_CATEGORY @"Help:Choose chat category"
#define OMNIPAGE_HELP_POPULAR_ARTICLES @"Help:Popular articles"

#define OMNICLICK_HELP_REPORTAFAULT @"Report a fault"
#define OMNICLICK_HELP_STARTLIVECHAT @"Start live chat"
#define OMNICLICK_HELP_CALL_HELP_DESK @"Call the helpdesk"
#define OMNICLICK_HELP_POPULAR_FAQ @"Popular FAQs"



//Optimise Service Screen

#define OMNIPAGE_SERVICE_STATUS_ACCOUNT @"Service status:Choose Account"
#define OMNIPAGE_SERVICE_STATUS_ACCOUNT_ASSET @"Service status:Choose your broadband service"
#define OMNIPAGE_SERVICE_STATUS_CHECKING @"Service status:Checking your service"
#define OMNIPAGE_SERVICE_STATUS_YOUR_SERVICE @"Service status:Your Broadband services"
#define OMNIPAGE_SERVICE_STATUS_HUB_STATUS @"Service status:Hub status"
#define OMNIPAGE_SERVICE_STATUS_4GSERVICE_STATUS @"Service status:Service status"

#define OMNIPAGE_SERVICE_STATUS_SCAN_INPROGRESS  @"Service status:Service status scan in progress"
#define OMNIPAGE_SERVICE_STATUS_CHECKING_SERVICE_FAILED @"Service status:Checking service failed"
#define OMNIPAGE_SERVICE_STATUS_AFFECTED_SERVICE @"Service status:Issue found:Affected service:" 
#define OMNIPAGE_SERVICE_STATUS_RECENTLY_FIXED_SERVICE @"Service status:Good service:Recent fix:"
#define OMNIPAGE_SERVICE_STATUS_GOOD_SERVICE @"Service status:Good service:Good:"
#define OMNIPAGE_SERVICE_STATUS_CVE_SCAN_INPROGRESS  @"Service status:Cloud Voice Express checking status"
#define OMNIPAGE_SERVICE_STATUS_CVE_WORKING_WELL @"Service status:Cloud Voice Express working well"
#define OMNIPAGE_SERVICE_STATUS_CVE_WORKING_WELL_PREVIOUS_ISSUE @"Service status:Cloud Voice Express working well (previous issue)"
#define OMNIPAGE_SERVICE_STATUS_CVE_ON_GOING_ISSUE @"Service status:Cloud Voice Express on going issue"
#define OMNIPAGE_SERVICE_STATUS_CVE_SOMETHING_WENT_WRONG  @"Service status:Cloud Voice Express something went wrong"
#define OMNIPAGE_SERVICE_STATUS_ERRSOMETHINGWRONG @"Service status:Something went wrong"

#define OMNIPAGE_SPEED_TEST @"Speed test"
#define OMNIPAGE_SPEED_TEST_TEST_BROADBAND_SPEED @"Speed test:Test broadband speed"
#define OMNIPAGE_SPEED_TEST_NO_PERMISSION @"Speed test:No permission"
#define OMNIPAGE_SPEED_TEST_UPGRADE_HUB @"Speed test:Upgrade hub"
#define OMNIPAGE_SPEED_TEST_SELECT_ACCOUNT @"Speed test:Select account"
#define OMNIPAGE_SPEED_TEST_SELECT_HUB @"Speed test:Select hub"
#define OMNIPAGE_SPEED_TEST_NO_HUB_OR_BROADBAND @"Speed test:No hub or broadband"
#define OMNIPAGE_SPEED_TEST_UNKNOWN_HUB @"Speed test:Unknown hub"
#define OMNIPAGE_SPEED_TEST_HUB_3_5_UPGRADE @"Speed test:Hub 3 & 5 upgrade"
#define OMNIPAGE_SPEED_TEST_SERVICE_STATUS_ERROR @"Speed test:Service status error"
#define OMNIPAGE_SPEED_TEST_PRE_SERVICE_STATUS_ERROR_4G_ASSURE @"Speed test:Pre sevice status 4g assure"
#define OMNIPAGE_SPEED_TEST_PRE_SERVICE_STATUS_ERROR @"Speed test:Pre sevice status"
#define OMNIPAGE_SPEED_TEST_CHECKING_SPEED @"Speed test:Checking speed"
#define OMNIPAGE_SPEED_TEST_RESULTS_MGAL @"Speed test:Results MGAL"
#define OMNIPAGE_SPEED_TEST_RESULTS_NON_MGAL @"Speed test:Results NON-MGAL"
#define OMNIPAGE_SPEED_TEST_FIRST_SPEED_TEST_ERROR @"Speed test:1st speed test error"
#define OMNIPAGE_SPEED_TEST_SECOND_SPEED_TEST_ERROR @"Speed test:2nd speed test error"


#define OMNICLICK_SERVICE_STATUS_REPORT_FAULT @"Report fault to track progess"
#define OMNICLICK_SERVICE_STATUS_CVE_REPORT_FAULT @"Report a fault"
#define OMNICLICK_SERVICE_STATUS_FAQ @"Browser popular FAQa"
#define OMNICLICK_SERVICE_STATUS_CVE_FAQ @"Browse populate FAQs"

#define OMNICLICK_SERVICE_STATUS_RESTART_HUB @"Restart hub"

#define OMNICLICK_SERVICE_STATUS_VIEW_DETAILS @"View details"
#define OMNICLICK_SERVICE_STATUS_VIEW_DETAILS_BB @"View details"
#define OMNICLICK_SERVICE_STATUS_VIEW_DETAILS_PSTN @"View details"
#define OMNICLICK_SERVICE_STATUS_VIEW_DETAILS_PSTN @"View details"
#define OMNICLICK_SERVICE_STATUS_VIEW_DETAILS_CVE @"Cloud Voice Express checking status"

#define OMNICLICK_SERVICE_STATUS_ORDERNOW @"Order now"

#define OMNICLICK_SERVICE_STATUS_FIX_PREFIX @"How to fix problems:"
#define OMNICLICK_SERVICE_STATUS_FIX_HUBHEALTHCHECK @"How to fix problems:Hub healthcheck"
#define OMNICLICK_SERVICE_STATUS_FIX_CVE_HUBHEALTHCHECK @"Hub healthcheck"
#define OMNICLICK_SERVICE_STATUS_FIX_REPORTAFAULT @"How to fix problems:Report a fault"
#define OMNICLICK_SERVICE_STATUS_FIX_FAQS @"How to fix problems:Browse popular FAQs"
#define OMNICLICK_SERVICE_STATUS_UPGRADE @"Upgrade - chat now"
#define OMNICLICK_SERVICE_STATUS_NETWORK_ISSUE @"Network issue details"

#define OMNICLICK_SERVICE_STATUS_HUBSTATUS_HUB5 @"BT Hub 5"
#define OMNICLICK_SERVICE_STATUS_HUBSTATUS_HUB6 @"BT Smart Hub"
#define OMNICLICK_SERVICE_STATUS_HUBSTATUS_CALL @"Call"



#define OMNI_KEYTASK_SERVICE_STATUS_SCAN_FAILED @"Service status scan result failed"


//***************Click tags***************
#define OMNICLICK_LOGIN_SIGNIN @"Sign in"
#define OMNICLICK_LOGIN_FORGOTUSERNAME @"Forgot username"
#define OMNICLICK_LOGIN_FORGOTPASSWORD @"Forgot password"
#define OMNICLICK_LOGIN_REGISTER @"Register"
#define OMNICLICK_LOGIN_RETURN_TO_LOGIN @"Return to login"
#define OMNICLICK_LOGIN_PIN_CREATED @"Login PIN created"

#define OMNICLICK_CUG_CHANGE @"Change CUG"

//***** Home Page Events ***** //

#define OMNICLICK_HOME_TRACK_FAULTS  @"Track faults"
#define OMNICLICK_HOME_TRACK_ORDERS  @"Track orders"
#define OMNICLICK_HOME_BILLS       @"Bills"
#define OMNICLICK_HOME_ACCOUNT @"Account"
#define OMNICLICK_HOME_USAGE @"Usage"
#define OMNICLICK_HOME_PUBLICWIFI @"Public wi-fi"
#define OMNICLICK_HOME_BROADBAND_SPEED_TEST @"Broadband speed test"
#define OMNICLICK_HOME_HELP @"Help"
#define OMNICLICK_HOME_DASHBOARD @"Dashboard"
#define OMNICLICK_HOME_SETTINGS @"Settings"
#define OMNICLICK_HOME_LOGOUT @"Log out"
#define OMNICLICK_HOME_SERVICE @"Service status"
#define OMNICLICK_HOME_REPORT_A_FAULT @"Report a fault"
#define OMNICLICK_HOME_REPORTAFAULT @"Home:Report a fault"




#define OMNICLICK_CUG_CHANGE @"Change CUG"


//****Settings Events ****//
#define OMNICLICK_SETTINGS_PERSONAL @"Personal"
#define OMNICLICK_SETTINGS_SECURITY @"Security"
#define OMNICLICK_SETTINGS_NOTIFICATION @"Notifications"
#define OMNICLICK_SETTINGS_SIGNOUT @"Sign out"
#define OMNICLICK_SETTINGS_EDIT @"Edit"
#define OMNICLICK_SETTINGS_SAVE @"Save"
#define OMNICLICK_SETTINGS_UPDATEPIN @"Update PIN"
#define OMNICLICK_SETTINGS_CHANGE_QUESTION @"Change security question"
#define OMNICLICK_SETTINGS_UPDATE_SECURITY_NUMBER @"Security number"
#define OMNICLICK_SETTINGS_CHANGE_PASSWORD @"Change password"
#define OMNICLICK_SETTINGS_TOUCH_ID @"Enable touch ID"


//Usage Events
#define OMNICLICK_USAGE_PHONELINE @"Phoneline usage"
#define OMNICLICK_USAGE_BROADBAND @"Broadband usage"


//Assets Event
#define OMNICLICK_ASSETS_VIEWBILL @"View latest bill"
#define OMNICLICK_ASSETS_SERVICE_DETAILS @"Service details"
#define OMNICLICK_ASSETS_CHECK_SERVICE_STATUS @"Check service status"
#define OMNICLICK_ASSETS_VIEW_PAY_BILL @"View or pay latest bill"
#define OMNICLICK_ASSETS_VIEW_MOBILESERVICE_FAQ @"Mobile Services FAQs"
#define OMNICLICK_ASSETS_VIEWBILL @"View latest bill"
#define OMNICLICK_ASSETS_REVIEW_USAGE @"Review my usage"


//Bills Event

#define OMNICLICK_BILLS_PAY_MULTIPLE_BILLS @"Pay multiple bills"
#define OMNICLICK_BILLS_BILL_DETAILS @"Bill details"
#define OMNICLICK_BILLS_PAY_BY_CARD_PNKBUTTON @"Pay by card (pink button)"
#define OMNICLICK_BILLS_DOWNLOAD_BILLS @"Download full bill as pdf"
#define OMNICLICK_BILLS_CHARGES_DETAILS @"Details"
#define OMNICLICK_BILLS_PAY_BY_CARD @"Pay by card"


//Track order Events

#define OMNICLICK_ORDER_DETAILS @"details"
#define OMNICLICK_ORDER_NEED_HELP @"Need help"
#define OMNICLICK_ORDER_BILLING_DETAILS @"Billing details"
#define OMNICLICK_ORDER_ENGG_APPOINTMENT @"Engineer appointment"
#define OMNICLICK_ORDER_SITE_CONTACT @"Site contact"
#define OMNICLICK_ORDER_ENGG_ALTERNATIVE_CONTACT @"Alternative contact"
#define OMNICLICK_ORDER_ENGG_ACTIVATION_DATE @"Activation date"
#define OMNICLICK_ORDER_CHANGE_ENGG_APPOINTMENT @"Change engineer appt"
#define OMNICLICK_ORDER_CHANGE_ENGG_APPOINTMENT_DETAILS @"Change engineer appt details"
#define OMNICLICK_ORDER_CHANGE_ORDER_DETAILS @"Change order details"
#define OMNICLICK_ORDER_ADD_APPT_CALENDAR @"Add appointment to calendar"
#define OMNICLICK_ORDER_CHANGE_COMPLETION_DATE @"Change completion date"

//Sunday
#define OMNICLICK_ORDER_SEARCH @"Search"
#define OMNICLICK_ORDER_ENGG_APPT_DETAILS @"Engineer appointment details"
#define OMNICLICK_ORDER_VIEW_FULL_DETAILS @"View full details"
#define OMNICLICK_ENTER_PASSCODE @"Enter postcode for full details"
#define OMNICLICK_ORDER_CANCEL_ORDER @"Cancel this order"
#define OMNICLICK_ORDER_HELP @"Help"
#define OMNICLICK_ORDER_RETRY @"Retry"


//----***** Track fault Click events *****----//
#define OMNICLICK_FAULT_DETAILS @"details"
#define OMNICLICK_FAULT_REPORT_A_FAULT @"Report a fault"
#define OMNICLICK_FAULT_AMEND_DETAILS @"Amend details"
#define OMNICLICK_FAULT_CANCEL_FAULT @"Cancel this fault"
#define OMNICLICK_FAULT_CHANGE_FAULT_DETAILS @"Change fault details"
#define OMNICLICK_FAULT_VIEW_MESSAGE @"View message"
#define OMNICLICK_FAULT_NEED_HELP @"Need help"
#define OMNICLICK_FAULT_ENGG_APPT @"Engineer appointment"
#define OMNICLICK_FAULT_SITE_CONTACTS @"Site contacts"
#define OMNICLICK_FAULT_CALL_DIVERSION @"Call diversion"
#define OMNICLICK_FAULT_SEARCH_BY_FAULT_REF @"Search by fault reference"
#define OMNICLICK_FAULT_REPORT_A_FAULT @"Report a fault"
#define OMNICLICK_FAULT_VIEW_FULL_DETAILS @"View full details"
#define OMNICLICK_FAULT_ENTER_ACCOUNT_NUMBER @"Enter account number for full details"

//**** Speed test ***//
#define OMNIPAGE_SPEED_TEST_UPGRADE_YOUR_HUB @"Upgrade your hub"
#define OMNIPAGE_SPEED_TEST_CHECKING_SERVICE_STATUS @"Checking service status"
#define OMNIPAGE_SPEED_TEST_HUB_3_OR_5_UPGRADE @"Hub 3 & 5 upgrade"
#define OMNIPAGE_SPEED_TEST_PRE_SERVICE_STATUS_4G_ASSURE @"Pre sevice status 4g assure"
#define OMNIPAGE_SPEED_TEST_RESULTS_UNKNOWN @"Results unknown/NON-MGAL"
#define OMNIPAGE_SPEED_TEST_RESULTS_MGAL_CONTENT_SPEED_MET_GOOD @"Speed test:Results MGAL"
#define OMNIPAGE_SPEED_TEST_RESULTS_MGAL_CONTENT_SPEED_NOT_MET_1ST_ATTEMPT @"Speed test:Results MGAL"
#define OMNIPAGE_SPEED_TEST_RESULTS_MGAL_CONTENT_SPEED_NOT_MET_2ND_ATTEMPT @"Speed test:Results MGAL"
#define OMNIPAGE_SPEED_TEST_RESULTS_UNKNOWN_CONTENT_SPEED_NOT_MET_1ST_ATTEMPT @"Speed test:Results unknown/NON-MGAL:1st attempt"
#define OMNIPAGE_SPEED_TEST_RESULTS_UNKNOWN_CONTENT_SPEED_NOT_MET_2ND_ATTEMPT @"Speed test:Results unknown/NON-MGAL:2nd attempt"
#define OMNIPAGE_SPEED_TEST_RESULTS_NON_MGAL_CONTENT_SPEED_NOT_MET_1ST_ATTEMPT @"Speed test:Results NON-MGAL:1st attempt"
#define OMNIPAGE_SPEED_TEST_RESULTS_NON_MGAL_CONTENT_SPEED_NOT_MET_2ND_ATTEMPT @"Speed test:Results NON-MGAL:2nd attempt"
#define OMNIPAGE_SPEED_TEST_ERROR_1ST_ATTEMPT @"Speed test:1st speed test error"
#define OMNIPAGE_SPEED_TEST_ERROR_2ND_ATTEMPT @"Speed test:2nd speed test error"

#define OMNIPAGE_SPEED_TEST_CONTENT_DATA_SPEED_MET_GOOD @"Speed met - good"
#define OMNIPAGE_SPEED_TEST_CONTENT_DATA_SPEED_NOT_MET_1ST_ATTEMPT @"Speed not met - 1st attempt"
#define OMNIPAGE_SPEED_TEST_CONTENT_DATA_SPEED_NOT_MET_2ND_ATTEMPT @"Speed not met - 2nd attempt"
#define OMNIPAGE_SPEED_TEST_CONTENT_DATA_1ST_ATTEMPT @"1st attempt"
#define OMNIPAGE_SPEED_TEST_CONTENT_DATA_2ND_ATTEMPT @"2nd attempt"

#define OMNICLICK_SPEED_TEST_NO_PERMISSION_LINK_DONE @"Done"
#define OMNICLICK_SPEED_TEST_START_SPEED_TEST @"Start speed test"
#define OMNICLICK_SPEED_TEST_DONT_HAVE_SMART_HUB @"I don't have a bt business smart hub"
#define OMNICLICK_SPEED_TEST_BT_BUSINESS_SMART_HUB @"BT Business Smart Hub"
#define OMNICLICK_SPEED_TEST_GUEST_WI_FI @"Guest Wi-Fi"
#define OMNICLICK_SPEED_TEST_4G_ASSURE @"4G Assure"
#define OMNICLICK_SPEED_TEST_BT_SPEED_GUARANTEE @"BT speed guarantee"
#define OMNICLICK_SPEED_TEST_UPGRADE_CHAT_NOW @"Upgrade - chat now"
#define OMNICLICK_SPEED_TEST_DETAILS @"Details"
#define OMNICLICK_SPEED_TEST_BT_BUSINESS_BROADBAND @"BT business broadband"
#define OMNICLICK_SPEED_TEST_FIND_OUT_MORE @"Find out more"
#define OMNICLICK_SPEED_TEST_RE_TEST_NOW @"Re-test now"
#define OMNICLICK_SPEED_TEST_REPORT_A_FAULT @"Report fault"
#define OMNICLICK_SPEED_TEST_BROADBAND_SPEED_TIPS @"Broadband speed tips"
#define OMNICLICK_SPEED_TEST_WIFI_SPEED_TIPS @"Wi-fi speed tips"

//****Notificaion Popup***//

#define OMNIPAGE_TOUCH_ID_DISABLED_CANCEL @"Touch ID currently disabled:Cancel"
#define OMNIPAGE_TOUCH_ID_DISABLED_SETTINGS @"Touch ID currently disabled:Setting"
#define OMNIPAGE_TOUCH_ID_CANCEL  @"Touch ID for BT Business:Cancel"

#define OMNIPAGE_PIN_CONFIRMED_NO_THANKS @"PIN confirmed:No thanks"
#define OMNIPAGE_PIN_CONFIRMED_OK @"PIN confirmed:OK"
#define OMNIPAGE_PIN_INCORRECT_OK @"Incorrect PIN:OK"
#define OMNIPAGE_SECURITY_CHECK @"PaPS:Security check"
#define OMNIPAGE_SET_SECURITY_NUMBER @"PaPS:Set security number"
#define OMNIPAGE_SET_NEW_PASSWORD @"PaPS:Set password"

//************** KEY TASK ***********//
//Setting Keytasks

#define OMNI_KEYTASK_SETTINGS_PIN_UPDATED @"Settings PIN updated"
#define OMNI_KEYTASK_SETTINGS_PASSWORD_CHANGE @"Settings password changed"
#define OMNI_KEYTASK_SETTINGS_SECURITY_QUESTION_UPDATED @"Settings security question changed"
#define OMNI_KEYTASK_SETTINGS_CONTACT @"Settings contact details saved"



//*** Order Key Task ****//

#define OMNI_KEYTASK_ORDER_TRACKER_SUMMARY @"Order tracker summary"
#define OMNI_KEYTASK_ORDER_TRACKER_DETAILS  @"Order tracker details"
#define OMNI_KEYTASK_ORDER_TRACKER_ENGG_APPOINTMENT_CHANGED  @"Order tracker engineer appointment changed"
#define OMNI_KEYTASK_ORDER_TRACKER_DETAILS_SITE_CONTACT_CHANGED  @"Order tracker site contact details changed"
#define OMNI_KEYTASK_ORDER_TRACKER_DETAILS_ALT_SITE_CONTACT_CHANGED  @"Order tracker alternative contact details changed"
#define OMNI_KEYTASK_ORDER_TRACKER_DETAILS_ACTIVATION_DATE_CHANGED  @"Order tracker activation date changed"
#define OMNI_KEYTASK_ORDER_TRACKER_APPOINTMENT_ADDED_TO_CALENDAR  @"Order tracker appointment added to calendar"
#define OMNI_KEYTASK_ORDER_TRACKER_ENGINEER_APPOINTMENT_CHECKED  @"Order tracker engineer appointment checked"


//*** Track Fault Key Task ****//

#define OMNI_KEYTASK_TRACK_FAULT_DETAILS @"Fault details"
#define OMNI_KEYTASK_FAULT_ENGG_APPOINTMENT_CHANGED  @"Fault engineer appointment changed"
#define OMNI_KEYTASK_FAULT_DETAILS_SITE_CONTACT_CHANGED  @"Fault site contact details changed"
#define OMNI_KEYTASK_FAULT_DETAILS_CALL_DIVERSION_CHANGED  @"Fault call diversion details changed"
#define OMNI_KEYTASK_FAULT_APPOINTMENT_ADDED_TO_CALENDAR  @"Fault tracker appointment added to calendar"
#define OMNI_KEYTASK_FAULT_ENGINEER_APPOINTMENT_CHECKED  @"Fault tracker engineer appointment checked"



//Params Keys

#define kOmniLoginStatus  @"LoginStatus"
#define kOmniTimeStamp @"TimeStamp"
#define kOmniKeyTask @"KeyTask"
#define kOmniOrderRef @"OrderRef"
#define kOmniFaultRef @"FaultRef"
#define kOmniContentViewed @"ContentViewed"
#define kOmniFaultRefFormat @"BTB:App:Service:Fault Tracker:"
#define kOmniHubType @"HubType"
#define kOmniProductService @"ProductServiceName"
#define kOmniOrderStatus @"OrderStatus"
#define kContactID @"ContactId"

//Error tags
#define OMNIERROR_LOGIN_INVALIDCREDENTIALS @"CE:Invalid username and password"
#define OMNIERROR_LOGIN_INVALID_PIN @"CE:Incorrect PIN"
#define OMNIERROR_LOGIN_INVALID_PIN_3_TIMES @"CE:Incorrect PIN 3 times"
#define OMNIERROR_LEGACY_PROFILE_LOGIN @"SE:legacy profile attempting log in"
#define OMNIERROR_REGISTRATION_INCORRECT_PIN @"CE:app registeration incorrect pin"
#define OMNIERROR_PROFILE_LOCKED @"CE:failed password profile locked"
#define OMNIERROR_INVALID_BAC_NUMBER @"CE:incorrect bill account number entry"
#define OMNIERROR_FAULT_REF_NOT_EXIST @"CE:fault reference does not exist"
#define OMNIERROR_FAULT_LAZY_LOADING_ERROR @"SE:lazy loading error on fault area"
#define OMNIERROR_ORDER_CHANGE_AMEND_NOT_ALLOWED @"SE:change order amend not allowed"
#define OMNIERROR_CONFRIM_AMENED_BUT_FAILS @"SE:confirm amend but amend fails"
#define OMNIERROR_AMEND_FAULT_FAILED @"SE:amend fault change failed"
#define OMNIERROR_CALL_DIVERT_PHONE_FAIL @"SE:call divert phone number fail"
#define OMNIERROR_FAULT_CANCEL_WARNING @"CE:fault cancellation warning message"
#define OMNIERROR_NOTIFICAION_NO_CONNECTION @"SE:notification page no connection"
#define OMNIERROR_EMPTY_ACCOUNT_PROCESSSING @"SE:asset list empty account still processing"
#define OMNIERROR_NO_ASSET_TO_DISPLAY @"SE:assets no assets available to show"
#define OMNIERROR_NO_INTERNET_LOGIN @"SE:no internet connection on login"
#define OMNIERROR_NO_INTERNET_PIN_UNLOCK @"SE:no internet connection on pin unlock"
#define OMNIERROR_RESILIENT_HUB_NOT_ALLOWED @"SE:GetResilientHubAsset:MBAF2-01:you are not allowed to perform this operation"
#define OMNIERROR_RESILENT_HUB_INVALID_DATA_ERROR @"SE:GetResilientHubAsset:MB5-01:invalid_data_error_occured"

#define OMNIERROR_EMPTY_MOBILE_SERVICE_PROCESSSING @"SE:asset list empty mobile service still processing"

#define OMNIERROR_NO_DATA_FOUND @"SE:no data found"
#define OMNIERROR_NO_INTERNET @"SE:no connection error"
#define OMNIERROR_SOMETHING_WENT_WRONG @"SE:generic error something went wrong"
#define OMNIERROR_SECURITY_QUESTION_CHANGE_PASSWORD_INCORRECT @"CE:settings security question change password incorrect"
#define OMNIERROR_SECURITY_NUMBER_CHANGE_PASSWORD_INCORRECT @"CE:settings security number change password incorrect"


//ForceUpdate ContentViewed tags
#define OMNI_VERSION_UPDATE @"Optional Version Update Reminder: OK"
#define OMNI_VERSION_MANDATORY_UPDATE @"Mandatory Version Update Reminder: OK"
#define OMNI_VERSION_UNSUPPORTED @"Device/OS Unsupported Reminder: OK"

//Logged out screen
#define OMNIPAGE_LOGGED_OUT @"Logged out"
#define OMNICLICK_LOG_IN @"Log in"
#define OMNICLICK_SWICH_USER @"Switch user"

//About the app screen
#define OMNIPAGE_ABOUT_APP @"About the app"
#define OMNICLICK_HOW_TO_USE_MY_BT @"How to use my BT"
#define OMNICLICK_TERMS_AND_CONDITIONS @"Terms and conditions"

@interface OmnitureManager : ADBMobile

+ (void)trackPage:(NSString*)state;
+ (void)trackPage:(NSString*)state withContextInfo:(NSDictionary*)contextInfo;
+ (void)trackClick:(NSString*)action;
+ (void)trackClick:(NSString*)action withContextInfo:(NSDictionary*)contextInfo;
+ (void)trackError:(NSString*)error onPageWithName:(NSString*)page;
+ (void)trackError:(NSString*)error onPageWithName:(NSString*)page contextInfo:(NSDictionary*)contextInfo;


+ (NSString *)getOrderRefFormattedString:(NSString *)orderRef;

@end
