#import "OmnitureManager.h"
#import "CDUser.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "AppManager.h"

@interface OmnitureManager()
 + (NSMutableDictionary *)createDirWithContextInfoAndContactId:(NSDictionary *)contextInfo;
@end

@implementation OmnitureManager

+ (void) trackPage:(NSString *)state {
    [self trackPage:state withContextInfo:nil];
}

+ (void) trackPage : (NSString*) state withContextInfo : (NSDictionary*) contextInfo {
    
    NSMutableDictionary * contextData = [self createDirWithContextInfoAndContactId:contextInfo];
    
    state = [NSString stringWithFormat:@"%@%@", STATE_PREFIX, state];
    if (state)
    {
        [contextData setObject:state forKey:@"StepName"];
    }
    
    [self trackState:state data:contextData];
}

+ (void) trackClick : (NSString *) action {

    [self trackClick:action withContextInfo:nil];
}

+ (void) trackClick : (NSString*) action withContextInfo : (NSDictionary*) contextInfo {
    
    NSMutableDictionary* contextData = [self createDirWithContextInfoAndContactId:contextInfo];
    
    action = [NSString stringWithFormat:@"%@%@", STATE_PREFIX, action];
    if (action)
    {
        [contextData setObject:action forKey:@"LinkName"];
    }
    
    [self trackAction:action data:contextData];
}

+ (NSMutableDictionary *)createDirWithContextInfoAndContactId:(NSDictionary *)contextInfo {
    NSMutableDictionary* contextData = [NSMutableDictionary new];
    
    CDUser *user = [AppDelegate sharedInstance].viewModel.app.loggedInUser;
    
    if (user) {
        [contextData setValue:user.contactId forKey:kContactID];
    }
    
    if (contextInfo) {
        [contextData addEntriesFromDictionary:contextInfo];
    }
    
    NSString *timestamp = [AppManager NSStringWithOmnitureFormatFromNSDate:[NSDate date]];
    [contextData setValue:timestamp forKey:kOmniTimeStamp];
    
    return contextData;
}

+ (void) trackError : (NSString*) error onPageWithName : (NSString*) page {
    [self trackError:error onPageWithName:page contextInfo:nil];
}

+ (void)  trackError : (NSString*) error onPageWithName : (NSString*) page contextInfo : (NSDictionary*) contextInfo {
    
    error = [NSString stringWithFormat:@"%@%@:%@", STATE_PREFIX, page, error];
    page = [NSString stringWithFormat:@"%@%@",STATE_PREFIX, page];
    
    NSMutableDictionary* contextData = [NSMutableDictionary new];
    
    if (contextInfo) {
        [contextData addEntriesFromDictionary:contextInfo];
    }
    
    if (error && page)
    {
        [contextData setObject:error forKey:@"ErrorDetails"];
    }
    
    NSString *timestamp = [AppManager NSStringWithOmnitureFormatFromNSDate:[NSDate date]];
    [contextData setValue:timestamp forKey:kOmniTimeStamp];

    [self trackState:page data:contextData];
}

+ (NSString *)getOrderRefFormattedString:(NSString *)orderRef
{
    NSString *string = [NSString stringWithFormat:@"%@Order Tracker:%@",STATE_PREFIX,orderRef];
    return string;
}




@end
