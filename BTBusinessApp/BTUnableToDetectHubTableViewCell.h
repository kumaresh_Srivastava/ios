//
//  BTUnableToDetectHubTableViewCell.h
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseTableViewCell.h"

typedef NS_ENUM(NSInteger, BTUnableToDetectType) {
    BTUnableToDetect4GAssure,
    BTUnableToDetectPublicWifi
};

@class BTUnableToDetectHubTableViewCell;

@protocol BTUnableToDetectHubTableViewCellDelegate <NSObject>

-(void)userPressedCellForUnableToDetectHub:(BTUnableToDetectHubTableViewCell*)BTUnableToDetectHubCell;

@end

@interface BTUnableToDetectHubTableViewCell : BTBaseTableViewCell {
    
}
@property (weak, nonatomic) IBOutlet UITextView *detailTextView;

@property (nonatomic, weak) id <BTUnableToDetectHubTableViewCellDelegate> BTUnableToDetectHubTableViewCellDelegate;

-(void)setupCellForContext:(BTUnableToDetectType)context;

@end
