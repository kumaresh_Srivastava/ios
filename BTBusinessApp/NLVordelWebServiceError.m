//
//  NLVordelWebServiceError.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 06/06/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLVordelWebServiceError.h"
#import "NLConstants.h"

@implementation NLVordelWebServiceError

- (id)initWithError:(NSError *)error andSourceError:(NLWebServiceError *)sourceError andErrorCode:(NSString *)errorCode andDescription:(NSString *)description
{
    self = [super init];
    if(self)
    {
        _errorCode = errorCode;
        _errorDescription = description;
    }
    return self;
}

+ (NLVordelWebServiceError *)VordelwebServiceNetworkErrorWithErrorCode:(NSString *)errorCode andDescription:(NSString *)description
{
    NSError *newErrorObject = [NSError errorWithDomain:BTVordelErrorDomain code:[NLVordelWebServiceError codeForVordelError:errorCode] userInfo:nil];
    NLVordelWebServiceError *vordelError = [[NLVordelWebServiceError alloc] initWithError:newErrorObject andSourceError:nil andErrorCode:errorCode andDescription:description];
    return vordelError;
}

+ (NLWebServiceError *)webServiceNetworkErrorWithErrorDomain:(NSString *)networkErrorDomain andNetworkErrorCode:(BTNetworkErrorCode)networkErrorCode
{
    NSError *newErrorObject = [NSError errorWithDomain:networkErrorDomain code:networkErrorCode userInfo:nil];
    NLWebServiceError *webServiceError = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    
    return webServiceError;
}

+ (BTNetworkErrorCode)codeForVordelError:(NSString*)error
{
    NSString *errorCodeFromServer = [error lowercaseString];
    BTNetworkErrorCode errorCode = BTNetworkErrorCodeNone;
    
    if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForParameterMissing])
    {
        errorCode = BTNetworkErrorCodeVordelParameterMissing;
    }
    else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForParameterInvalid])
    {
        errorCode = BTNetworkErrorCodeVordelParameterInvalid;
    }
    else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForClientUnauthenticated])
    {
        errorCode = BTNetworkErrorCodeVordelClientUnauthenticated;
    }
    else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForUserUnauthenticated])
    {
        errorCode = BTNetworkErrorCodeVordelUserUnauthenticated;
    }
    else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForTokenInvalidOrExpired])
    {
        errorCode = BTNetworkErrorCodeVordelTokenInvalidOrExpired;
    }
    else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForDeviceMismatch])
    {
        errorCode = BTNetworkErrorCodeVordelDeviceMismatch;
    }
    else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForScopeInvalid])
    {
        errorCode = BTNetworkErrorCodeVordelScopeInvalid;
    }
    else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForRefreshInvalidOrExpired])
    {
        errorCode = BTNetworkErrorCodeVordelRefreshInvalidOrExpired;
    }
    else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForAccountLocked])
    {
        errorCode = BTNetworkErrorCodeVordelAccountLocked;
    }
    else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForServiceDeprecated])
    {
        errorCode = BTNetworkErrorCodeVordelServiceDeprecated;
    }
    else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForInvalidDataError])
    {
        errorCode = BTNetworkErrorCodeVordelInvalidDataError;
    }
    else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForEntityTooLarge])
    {
        errorCode = BTNetworkErrorCodeVordelEntityTooLarge;
    }
    else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForMbaasInternalException])
    {
        errorCode = BTNetworkErrorCodeVordelMbassInternalExceptionError;
    }
    else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForPlannedServiceOutage])
    {
        errorCode = BTNetworkErrorCodeVordelPlannedServiceOutage;
    }
    else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForUnplannedServiceOutage])
    {
        errorCode = BTNetworkErrorCodeVordelUnplannedServiceOutage;
    }
    else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForMbaasTimeout])
    {
        errorCode = BTNetworkErrorCodeVordelMbassTimeout;
    }
    else if([errorCodeFromServer isEqualToString:kNLVordelErrorCodeFromServerForLegacyAccount])
    {
        errorCode = BTNetworkErrorCodeVordelLegacyAccount;
    }
    
    return errorCode;

}

@end
