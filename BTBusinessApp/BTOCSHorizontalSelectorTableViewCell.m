//
//  BTOCSHorizontalSelectorTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSHorizontalSelectorTableViewCell.h"

#import "BTOCSHorizontalSelectorCollectionViewCell.h"

@interface BTOCSHorizontalSelectorTableViewCell () <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@end

@implementation BTOCSHorizontalSelectorTableViewCell

+ (NSString *)reuseId
{
    return @"BTOCSHorizontalSelectorTableViewCell";
}

- (void)setSelectedOption:(NSString *)selectedOption
{
    _selectedOption = selectedOption;
    [self.collectionView reloadData];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.allowsSelection = YES;
    
//    UICollectionViewFlowLayout *flow = (UICollectionViewFlowLayout*)self.collectionView.collectionViewLayout;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"BTOCSHorizontalSelectorCollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"BTOCSHorizontalSelectorCollectionViewCell"];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateWithOptions:(NSArray<NSString *> *)options andSelectedOption:(NSString *)selected
{
    self.options = [NSArray arrayWithArray:options];
    self.selectedOption = selected;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger items = 0;
    if (self.options && self.options.count) {
        items = self.options.count;
    }
    return items;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BTOCSHorizontalSelectorCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BTOCSHorizontalSelectorCollectionViewCell" forIndexPath:indexPath];
    if (!cell) {
        cell = [[BTOCSHorizontalSelectorCollectionViewCell alloc] init];
    }
    if (self.options && indexPath.item < self.options.count) {
        NSString *title = [self.options objectAtIndex:indexPath.item];
        cell.textLabel.text = title;
        if (self.selectedOption && [title isEqualToString:self.selectedOption]) {
            // this is the selected cell
            cell.bgView.backgroundColor = [BrandColours colourBtLightBlack];
            cell.textLabel.textColor = [UIColor whiteColor];
            cell.indicatorView.backgroundColor = [BrandColours colourBtLightBlack];
            cell.indicatorView.hidden = NO;
        } else {
            cell.bgView.backgroundColor = [UIColor whiteColor];
            cell.bgView.layer.borderWidth = 1.0f;
            cell.bgView.layer.borderColor = [BrandColours colourBtLightBlack].CGColor;
            cell.textLabel.textColor = [BrandColours colourBtLightBlack];
            cell.indicatorView.hidden = YES;
        }
    }
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.options && indexPath.item < self.options.count) {
        self.selectedOption = [self.options objectAtIndex:indexPath.item];
        [self.collectionView reloadData];
        if (self.delegate && [self.delegate respondsToSelector:@selector(horizontalSelectorCell:updatedSelection:)]) {
            [self.delegate horizontalSelectorCell:self updatedSelection:self.selectedOption];
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = CGSizeMake(154, 51);
    CGFloat defaultWidth = 154;
    CGFloat dynamicWidth = (kScreenSize.width - 30 - ((self.options.count-1)*15))/self.options.count;
    if (self.options && self.options.count > 0) {
        size = CGSizeMake(MAX(defaultWidth, dynamicWidth), 51);
    }
    return size;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 15, 0, 15);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 15.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 15.0f;
}

@end
