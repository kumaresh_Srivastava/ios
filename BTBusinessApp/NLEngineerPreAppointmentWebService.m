//
//  NLEngineerPreAppointmentWebService.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 25/01/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLEngineerPreAppointmentWebService.h"

@implementation NLEngineerPreAppointmentWebService

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef andAppointmentDate:(NSString *)appointmentDate
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/Account/api/v3/OrderPreAppointments/%@/%@/%@",orderRef, itemRef,appointmentDate];
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
         _timeOutTimeInterval = 60.0;
    }
    return self;
}

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef appointmentDate:(NSString *)appointmentDate andPCP:(BOOL)pcpOnly
{
    NSString *pcpFlag = @"false";
    if(pcpOnly) {
        pcpFlag = @"true";
    }
    NSString *endPointURLString = [NSString stringWithFormat:@"/Account/api/v3.1/OrderPreAppointments/%@/%@/%@/%@",orderRef, itemRef,appointmentDate, pcpFlag];
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
         _timeOutTimeInterval = 60.0;
    }
    return self;
}

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef postCode:(NSString *)postCode andAppointmentDate:(NSString *)appointmentDate
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/Account/api/v3/OrderPreAppointments/%@/%@/%@/%@",orderRef, itemRef,postCode, appointmentDate];
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    
    if(self)
    {
         _timeOutTimeInterval = 60.0;
    }
    return self;
}

- (instancetype)initWithOrderRef:(NSString *)orderRef itemRef:(NSString *)itemRef postCode:(NSString *)postCode appointmentDate:(NSString *)appointmentDate andPCP:(BOOL)pcpOnly
{
    NSString *pcpFlag = @"false";
    if(pcpOnly) {
        pcpFlag = @"true";
    }
    NSString *endPointURLString = [NSString stringWithFormat:@"/account/api/v3.1/OrderPreAppointments/%@/%@/%@/%@/%@",orderRef, itemRef,postCode, appointmentDate, pcpFlag];
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    
    if(self)
    {
        
    }
    return self;
}


@end
