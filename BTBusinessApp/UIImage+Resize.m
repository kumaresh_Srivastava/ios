//
//  UIImage+Resize.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 22/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "UIImage+Resize.h"

@implementation UIImage (Resize)

- (UIImage *)resizeWithScaleFactor:(CGFloat)scale
{
    UIImage *returnImage = self;
    CGSize canvas = CGSizeMake(self.size.width*scale, self.size.height*scale);
    UIGraphicsBeginImageContextWithOptions(canvas, false, self.scale);
    CGRect rect = CGRectMake(0, 0, canvas.width, canvas.height);
    [self drawInRect:rect];
    returnImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return returnImage;
}

@end
