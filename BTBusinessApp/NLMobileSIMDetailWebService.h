//
//  NLMobileSIMDetailWebService.h
//  BTBusinessApp
//
//  Created by kumaresh shrivastava on 24/06/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NLAPIGEEAuthenticatedWebService.h"
#import "BTAsset.h"
#import "BTAssetModel.h"

NS_ASSUME_NONNULL_BEGIN

@class NLMobileSIMDetailWebService;
@class NLWebServiceError;

@protocol NLMobileSIMDetailWebServiceDelegate

- (void)getMobileSIMDetailWebService:(NLMobileSIMDetailWebService *)webService successfullyFetchedMobileSIMDetailData:(NSArray *)asset;

- (void)getMobileSIMDetailWebService:(NLMobileSIMDetailWebService *)webService failedToFetchMobileSIMDetailDataWithWebServiceError:(NLWebServiceError *)webServiceError;

@end


@interface NLMobileSIMDetailWebService : NLAPIGEEAuthenticatedWebService

- (instancetype)initWithMobileSIMDetailWithServiceID:(NSString *)mobileServiceId;

@property (nonatomic, weak) id <NLMobileSIMDetailWebServiceDelegate> mobileSIMDetailWebServiceDelegate;
@property (nonatomic, strong) NSString* mobileServiceID;

@end

NS_ASSUME_NONNULL_END
