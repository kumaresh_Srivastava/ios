//
//  BTOCSSectionHeaderTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 05/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface BTOCSSectionHeaderTableViewCell : BTBaseTableViewCell

@property (strong, nonatomic) IBOutlet UILabel *headerTextLabel;

@end

NS_ASSUME_NONNULL_END
