//
//  BTAccountSelectTableViewController.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 23/02/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTAccountSelectTableViewController.h"

#import "BTAssetsDashboardViewController.h"
#import "BTViewAssetBACOverviewViewController.h"
#import "BTUsageAccountViewController.h"
#import "BTCheckServiceStatusViewController.h"
#import "BTProductSelectTableViewController.h"
#import "AppConstants.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "DLMAssetDashboardScreen.h"
#import "BTAssetTableViewCell.h"
#import "BTCug.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "BTEmptyDashboardView.h"
#import "BTAsset.h"
#import "BTRetryView.h"
#import "CustomSpinnerView.h"
#import "CustomSpinnerAnimationView.h"
#import "OmnitureManager.h"
#import "UIViewController+WebServiceErrorHandling.h"
#import "NLWebServiceError.h"
#import "AppManager.h"

#define kAssetBACOverviewScreen @"BTViewAssetBACOverviewViewController"

@interface BTAccountSelectTableViewController () <UITableViewDataSource,UITableViewDelegate,DLMAssetDashboardScreenDelegate,BTRetryViewDelegate>

@property (nonatomic,strong) NSMutableArray *assetsArray;
@property (nonatomic, readwrite) DLMAssetDashboardScreen *assetDashboardScreenModel;
@property (nonatomic, assign) BOOL networkRequestInProgress;
@property (nonatomic,strong)  CustomSpinnerView *loadingView;

@property (strong, nonatomic) BTEmptyDashboardView *emptyDashboardView;
@property (strong, nonatomic) BTRetryView *retryView;


@property (nonatomic,strong) UILabel *pageTitleLabel;
@property (nonatomic,strong) UILabel *pageSubTitleLabel;

@end

@implementation BTAccountSelectTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    //View Model Initialization
    self.assetDashboardScreenModel = [[DLMAssetDashboardScreen alloc] init];
    self.assetDashboardScreenModel.assetDashboardScreenDelegate = self;
    
    if(!self.cugs) {
        self.cugs = [[AppDelegate sharedInstance] savedCugs];
    }
    
    [self createInitialUI];
    [self updateGroupSelection];
    
    UINib *dataCell = [UINib nibWithNibName:@"BTAssetTableViewCell" bundle:nil];
    [self.tableView registerNib:dataCell forCellReuseIdentifier:@"BTAssetTableViewCell"];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self createLoadingView];
    
    __weak typeof(self) selfWeak = self;
    [RACObserve(self, networkRequestInProgress) subscribeNext:^(NSNumber* state) {
        if ([selfWeak networkRequestInProgress])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [selfWeak hideLoadingItems:NO];
                selfWeak.navigationItem.rightBarButtonItem.enabled = NO;
                [selfWeak.loadingView startAnimatingLoadingIndicatorView];
            });
        }
        else
        {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];
            dispatch_async(dispatch_get_main_queue(), ^{
                [selfWeak hideLoadingItems:YES];
                selfWeak.navigationItem.rightBarButtonItem.enabled = YES;
                [selfWeak.loadingView stopAnimatingLoadingIndicatorView];
            });
        }
    }];
    
    
    [self fetchAssetsDashboardAPI];
    
    [self trackPageForOmniture];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.assetDashboardScreenModel cancelAssetsDashboardAPIRequest];
    self.networkRequestInProgress = NO;
}

- (NSString*)pageTitle
{
    NSString *title = @"";
    switch (self.context) {
        case BTAccountSelectContextAccount:
            title = @"Account";
            break;
        
        case BTAccountSelectContextUsage:
            title = @"Usage";
            break;
            
        case BTAccountSelectContextServiceStatus:
            title = @"Service status";
            break;
            
        case BTAccountSelectContextPublicWifi:
            title = @"Public wi-fi";
            break;
            
        default:
            break;
    }
    return title;
}

#pragma mark - InitialUI Methods

- (void)createInitialUI
{
    //CGSize screenSize = [UIScreen mainScreen].bounds.size;

    self.pageTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 208, 20)];
    self.pageTitleLabel.font = [UIFont fontWithName:kBtFontBold size:15.0];
    self.pageTitleLabel.textColor = [UIColor whiteColor];
    self.pageTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.pageTitleLabel.text = [self pageTitle];
    
    self.pageSubTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 19, 208, 17.5)];
    self.pageSubTitleLabel.font = [UIFont fontWithName:kBtFontRegular size:13.0];
    self.pageSubTitleLabel.textColor = [UIColor whiteColor];
    self.pageSubTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.pageSubTitleLabel.text = self.currentUser.currentSelectedCug.cugName;
    
    UIView *titlesView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 208, 40)];
    [titlesView addSubview:self.pageSubTitleLabel];
    [titlesView addSubview:self.pageTitleLabel];
    
    [self.navigationItem setTitleView:titlesView];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon_Home_White"] style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonAction)];

    if(self.cugs.count > 1)
    {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Group_Selection_White"] style:UIBarButtonItemStylePlain target:self action:@selector(groupSelectionAction)];
    }
    else
    {
        self.navigationItem.rightBarButtonItem = nil;
    }

    self.view.backgroundColor = [BrandColours colourBtNeutral30];
    self.tableView.backgroundColor = [BrandColours colourBtNeutral30];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger sections = 1;
    return sections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger rows = 0;
    if (self.assetsArray && self.assetsArray.count) {
        rows = self.assetsArray.count;
    }
    return rows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // FIXME: hardcoded value
    CGFloat height = 86.0;
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    BTAssetTableViewCell *assetCell = [tableView dequeueReusableCellWithIdentifier:@"BTAssetTableViewCell" forIndexPath:indexPath];
    [assetCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [assetCell updateAssetsCellWithAsset:self.assetsArray[indexPath.row]];
    cell = assetCell;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    
    UIViewController *nextViewController = nil;
    
    BTAsset *selectedAsset = [self.assetsArray objectAtIndex:indexPath.row];
    
    switch (self.context) {
        case BTAccountSelectContextAccount:
            nextViewController = (BTViewAssetBACOverviewViewController*)[storyboard instantiateViewControllerWithIdentifier:kAssetBACOverviewScreen];
            [(BTViewAssetBACOverviewViewController*)nextViewController setUserSelectedBTAsset:selectedAsset];
            break;
            
        case BTAccountSelectContextUsage:
            nextViewController = (BTUsageAccountViewController*)[BTUsageAccountViewController getUsageAccountViewController];
            [(BTUsageAccountViewController*)nextViewController setAssetsName:selectedAsset.assetName];
            [(BTUsageAccountViewController*)nextViewController setSelectedAsset:selectedAsset];
            [(BTUsageAccountViewController*)nextViewController setBillingAccountNumber:selectedAsset.billingAccountNumber];
            if(self.cugs.count > 1)
            {
                [(BTUsageAccountViewController*)nextViewController setGroupName:self.currentUser.currentSelectedCug.cugName];
            }
            break;
            
        case BTAccountSelectContextServiceStatus:
            nextViewController = (BTCheckServiceStatusViewController*)[BTCheckServiceStatusViewController getCheckServiceStatusViewController];
            [(BTCheckServiceStatusViewController*)nextViewController setBacID:selectedAsset.assetAccountNumber];
            if(self.cugs.count > 1)
                [(BTCheckServiceStatusViewController*)nextViewController setGroupName:self.currentUser.currentSelectedCug.cugName];
            break;
            
        case BTAccountSelectContextPublicWifi:
            nextViewController = [[BTProductSelectTableViewController alloc] initWithStyle:UITableViewStylePlain];
            [(BTProductSelectTableViewController*)nextViewController setContext:self.context];
            [(BTProductSelectTableViewController*)nextViewController setCurrentUser:self.currentUser];
            [(BTProductSelectTableViewController*)nextViewController setSelectedAsset:selectedAsset];
            [(BTProductSelectTableViewController*)nextViewController setGroupKey:self.groupKey];
            [(BTProductSelectTableViewController*)nextViewController setCugs:self.cugs];
            break;
            
        default:
            break;
    }
    
    if (nextViewController) {
        [self.navigationController pushViewController:nextViewController animated:YES];
    }
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Private Helper Methods

- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow  {
    
    if(self.retryView)
    {
        self.retryView.hidden = NO;
        self.retryView.retryViewDelegate = self;
        [self.retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
    } else {
        self.retryView = [[[NSBundle mainBundle] loadNibNamed:@"BTRetryView" owner:nil options:nil] objectAtIndex:0];
        self.retryView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.retryView updateRetryViewWithInternetStrip:internetStripNeedToShow];
        self.retryView.retryViewDelegate = self;
        
        [self.view addSubview:self.retryView];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:64.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
        
        [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.tableView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
        [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:self.retryView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.tableView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    }
}

- (void)hideRetryItems:(BOOL)isHide {
    // (SD) Hide or Show UI elements related to retry.
    [self.retryView setHidden:isHide];
}

- (void)createLoadingView {
    
    if(!_loadingView){
        
        _loadingView = [[[NSBundle mainBundle] loadNibNamed:@"CustomSpinnerView" owner:nil options:nil] objectAtIndex:0];
        _loadingView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        _loadingView.translatesAutoresizingMaskIntoConstraints = NO;
        [_loadingView setFrame:self.tableView.frame];
        
        [self.view addSubview:_loadingView];
    }
    
    [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.tableView  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.tableView  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.tableView  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.tableView  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
    [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.tableView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [self.tableView addConstraint:[NSLayoutConstraint constraintWithItem:_loadingView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.tableView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
}

- (void)hideLoadingItems:(BOOL)isHide {
    // (SD) Hide or Show UI elements related to loading.
    [self.loadingView setHidden:isHide];
}


/*
 Updates the currently selected group and UI on the basis of group selection
 */
- (void) checkForSuperUserAndUpdateUIWithIndex:(NSUInteger)index {
    for(BTCug *cug in self.cugs) {
        if(index == cug.indexInAPIResponse) {
            [self setGroupKey:cug.groupKey];
            [self.assetDashboardScreenModel changeSelectedCUGTo:cug];
            [self.pageSubTitleLabel setText:cug.cugName];
            [self trackCUGChange];
            [self resetDataAndUIAfterGroupChange];
        }
    }
}

- (void)resetDataAndUIAfterGroupChange
{
    BOOL needToRefresh = YES;
    if(self.retryView)
    {
        if((![AppManager isInternetConnectionAvailable] && !_retryView.isHidden))
            needToRefresh = NO;
    }
    
    if(needToRefresh){
        
        [self hideRetryItems:YES];
        //self.tableView.hidden = YES;
        [self fetchAssetsDashboardAPI];
    }
    else
    {
        [self.retryView updateRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:[self pageNameForOmnitureTracking]];
    }
}

#pragma mark - Action Methods

- (void)doneButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)groupSelectionAction
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Select Group" preferredStyle:UIAlertControllerStyleActionSheet];
    
    DDLogVerbose(@"Bills: Total cugs belongs to user %lu.",(unsigned long)self.cugs.count);
    
    // (SD) Add group names in actionsheet
    if (self.cugs != nil && self.cugs.count > 0) {
        
        // (SD) Add action for each cug selection.
        int index = 0;
        for (BTCug *groupData in self.cugs) {
            
            __weak typeof(self) selfWeak = self;
            
            UIAlertAction *action = [UIAlertAction actionWithTitle:groupData.cugName
                                                             style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                 [selfWeak checkForSuperUserAndUpdateUIWithIndex:groupData.indexInAPIResponse];
                                                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                                             }];
            [alert addAction:action];
            
            index++;
        }
    }
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    [alert addAction:cancel];
    

    [self presentViewController:alert animated:YES completion:^{
        
    }];
}

- (void)updateGroupSelection {
    if (self.cugs.count == 1) {
        return;
    }
    
    BTCug *cug = (BTCug *)[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug;
    [self setGroupKey:cug.groupKey];
    self.pageSubTitleLabel.text = cug.cugName;
}

#pragma mark - api call methods

- (void)fetchAssetsDashboardAPI
{
    if([AppManager isInternetConnectionAvailable])
    {
        [self createLoadingView];
        [self hideRetryItems:YES];
        self.networkRequestInProgress = YES;
        [self hideEmptyDashBoardItems:YES];
        [self.assetDashboardScreenModel fetchAssetsDashboardDetails];
    }
    else
    {
        [self showRetryViewWithInternetStrip:YES];
        [AppManager trackNoInternetErrorOnPage:OMNIPAGE_ASSETS_ACCOUNT];
    }
}

- (void)hideEmptyDashBoardItems:(BOOL)isHide {
    // Hide or Show UI elements related to retry.
    [self.emptyDashboardView setHidden:isHide];
    [self.emptyDashboardView removeFromSuperview];
    self.emptyDashboardView = nil;
}

- (void)showEmptyDashBoardView {
    
    //Error Tracker
    [OmnitureManager trackError:OMNIERROR_EMPTY_ACCOUNT_PROCESSSING onPageWithName:[self pageNameForOmnitureTracking] contextInfo:[AppManager getOmniDictionary]];
    
    self.emptyDashboardView = [[[NSBundle mainBundle] loadNibNamed:@"BTEmptyDashboardView" owner:nil options:nil] objectAtIndex:0];
    self.emptyDashboardView.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSURL *url;
    if ([kBTServerType isEqualToString:@"BTServerTypeProd"]) {
        url = [[NSURL alloc] initWithString:@"https://secure.business.bt.com/Account/Addbillingaccounts.aspx"];
    } else{
        url = [[NSURL alloc] initWithString:@"https://eric1-dmze2e-ukb.bt.com/Account/Addbillingaccounts.aspx"];
    }
    
    NSMutableAttributedString *hypLink = [[NSMutableAttributedString  alloc] initWithString:@"There’s no information to show just yet. Your account is either pending approval by your admin, or you can add an account here."];
    NSDictionary *linkAttributes = @{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle),
                                     NSLinkAttributeName : url};
    
    [hypLink addAttributes:linkAttributes range:NSMakeRange(hypLink.length-5,4)];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    
    [hypLink addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,hypLink.length)];
    
    [hypLink addAttribute:NSForegroundColorAttributeName value:[BrandColours colourBtNeutral70] range:NSMakeRange(0, hypLink.length)];
    
    [hypLink addAttribute:NSFontAttributeName
                    value:[UIFont fontWithName:kBtFontRegular size:16.0] range:NSMakeRange(0, hypLink.length)];
    
    [self.emptyDashboardView updateEmptyDashboardViewWithImageNameAndLink:@"package" title:@"No accounts to display" detailText:hypLink andButtonTitle:nil];
    
    [self.view addSubview:self.emptyDashboardView];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_emptyDashboardView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view  attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0]];
    
}

#pragma mark - RetryView Delgates

- (void)userPressedRetryButtonOfRetryView:(BTRetryView *)retryView
{
    [self fetchAssetsDashboardAPI];
}

#pragma mark - DLMAssetDashboardScreenDelegate

- (void)successfullyFetchedAssetsDashboardDataOnDLMAssetDashboardScreen:(DLMAssetDashboardScreen *)assetDashboardScreen
{
    self.networkRequestInProgress = NO;
    self.tableView.hidden = NO;
    if(assetDashboardScreen.assetsArray.count > 0)
    {
        self.assetsArray =  [assetDashboardScreen.assetsArray mutableCopy];
        [self.tableView reloadData];
        
    }
    else if(assetDashboardScreen.assetsArray.count == 0)
    {
        [self showEmptyDashBoardView];
    }
    else
    {
        DDLogError(@"Assets Dashboard: Assets not found.");
    }
}

- (void)assetDashboardScreen:(DLMAssetDashboardScreen *)assetDashboardScreen failedToFetchAssetsDashboardDataWithWebServiceError:(NLWebServiceError *)webServiceError {
    self.networkRequestInProgress = NO;
    BOOL errorHandled = [self attemptSMSessionProtectedAPIErrorHandlingOfWebServiceError:webServiceError];
    
    if([webServiceError.error.domain isEqualToString:BTNetworkErrorDomain] && errorHandled == NO)
    {
        
        switch (webServiceError.error.code)
        {
            case BTNetworkErrorCodeAPINoDataFound:
            {
                errorHandled = YES;
                [self showEmptyDashBoardView];
                [AppManager trackNoDataFoundErrorOnPage:OMNIPAGE_ASSETS_ACCOUNT];
                break;
            }
            default:
            {
                errorHandled = NO;
                break;
            }
        }
    }
    
    if(errorHandled == NO)
    {
        [self showRetryViewWithInternetStrip:NO];
        [AppManager trackGenericAPIErrorOnPage:OMNIPAGE_ASSETS_ACCOUNT];
    }
}

#pragma mark - Omniture Methods

- (NSString*)pageNameForOmnitureTracking
{
    NSString *pageName = @"";
    switch (self.context) {
        case BTAccountSelectContextAccount:
            pageName = OMNIPAGE_ASSETS_ACCOUNT;
            break;
            
        case BTAccountSelectContextUsage:
            pageName = OMNIPAGE_USAGE_ACCOUNT;
            break;
        
        case BTAccountSelectContextServiceStatus:
            pageName = OMNIPAGE_SERVICE_STATUS_ACCOUNT;
            break;
        
        case BTAccountSelectContextPublicWifi:
            pageName = OMNIPAGE_PUBLICWIFI_ACCOUNT;
            break;
            
        default:
            break;
    }
    return pageName;
}

- (void)trackPageForOmniture
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    NSString *pageName = [self pageNameForOmnitureTracking];;
    [data setObject:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackPage:pageName withContextInfo:data];
}

- (void)trackCUGChange
{
    NSString *pageName = [self pageNameForOmnitureTracking];
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setValue:@"Logged In" forKey:kOmniLoginStatus];
    
    [OmnitureManager trackClick:[NSString stringWithFormat:@"%@:Link:%@",pageName,OMNICLICK_CUG_CHANGE] withContextInfo:data];
}

@end
