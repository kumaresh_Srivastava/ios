//
//  BTSecurityNumberViewController.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 19/07/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseViewController.h"

typedef NS_ENUM(NSInteger, BTSecurityNumberContext) {
    BTSecurityNumberContextSettings,
    BTSecurityNumberContextPAPsRegistration,
    BTSecurityNumberContextAgentPasswordReset,
    BTSecurityNumberContextMigrate
};


@class BTSecurityNumberViewController;

@protocol BTSecurityNumberViewControllerDelegate <NSObject>

- (void)successfullyUpdatedSecurityNumberOnSecurityNumberViewController:(BTSecurityNumberViewController *)securityNumberViewController;

@end

@interface BTSecurityNumberViewController : BTBaseViewController

@property (nonatomic, assign) BOOL isScreenRelatedToInterceptorModule;
@property (nonatomic, assign) BOOL isScreenrelatedTo1FAMigration;
@property (nonatomic, weak) id<BTSecurityNumberViewControllerDelegate> delegate;
@property (strong,nonatomic) NSString *cugKeyFromSignInScreen;

@end
