

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SpeedTestUpgradeHubViewController : UIViewController

@property (nonatomic) BOOL unsupportedHub;

+ (SpeedTestUpgradeHubViewController *)getSpeedTestUpgradeHubViewController;

@end

NS_ASSUME_NONNULL_END
