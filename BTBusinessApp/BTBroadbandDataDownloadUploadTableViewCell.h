//
//  BTBroadbandDataDownloadUploadTableViewCell.h
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BroadbandUsageRowWrapper;

@interface BTBroadbandDataDownloadUploadTableViewCell : UITableViewCell

- (void)updateWithBroadbandUsageRowWrapper:(BroadbandUsageRowWrapper *)wrapper;

- (void)updateDataCellWithDataDownloaded:(NSString*)downloaded andUploaded:(NSString*)uploaded;

@end
