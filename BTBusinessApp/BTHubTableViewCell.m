//
//  BTHubTableViewCell.m
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTHubTableViewCell.h"

@implementation BTHubTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCellWithServiceStatusDetail:(BTServiceStatusDetail *)serviceStatusDetail andAffectedAreaText:(NSString *)affectedAreaText {
    // Do nothing
}

@end
