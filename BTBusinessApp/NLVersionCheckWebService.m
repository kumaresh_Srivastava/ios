//
//  NLVersionCheckWebService.m
//  BTBusinessApp
//
//  Created by VectoScalar on 3/16/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLVersionCheckWebService.h"
#import "BTVersionCheckResponse.h"
#import "NLWebServiceError.h"

@implementation NLVersionCheckWebService

#pragma mark - Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    
    BTVersionCheckResponse *versiosCheckResponse = [[BTVersionCheckResponse alloc] initWithResponseDict:responseObject];
    
    [self.versionCheckWebServiceDelegate versionCheckWebService:self succussfullyFinishedWithVersionResponse:versiosCheckResponse];
    
}


- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    DDLogError(@"Webservice with URL: %@ failed with error code: %ld.", [[task currentRequest] URL], (long)[webServiceError.error code]);
    
    [self.versionCheckWebServiceDelegate versionCheckWebService:self failedWithWebServiceError:webServiceError];
}


#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        
        if(responseObject && [responseObject isKindOfClass:[NSDictionary class]] &&
           [responseObject objectForKey:@"Version"] && [[responseObject objectForKey:@"Version"] isKindOfClass:[NSDictionary class]]){

            //Response is fine

        }
        else{
            
            errorToBeReturned = [NLWebServiceError webServiceNetworkErrorWithErrorDomain:BTNetworkErrorDomain andNetworkErrorCode:BTNetworkErrorCodeInvalidResponseObject];
            
            DDLogError(@"Invalid response object in response of webservice with URL: %@", [[task currentRequest] URL]);
            
        }
        
    }
    
    return errorToBeReturned;
}



@end
