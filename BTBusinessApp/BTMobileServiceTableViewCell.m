//
//  BTMobileServiceTableViewCell.m
//  BTBusinessApp
//
//  Created by kumaresh shrivastava on 24/06/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import "BTMobileServiceTableViewCell.h"

@implementation BTMobileServiceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    

    // Configure the view for the selected state
}

- (void)updateMobileServicesCell:(NSString*)serviceIdString {
    
    self.serviceIDLable.text = serviceIdString;
}

@end
