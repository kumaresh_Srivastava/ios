//
//  BTOCSRerquiredQuestionsTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTOCSRerquiredQuestionsTableViewCell.h"

@implementation BTOCSRerquiredQuestionsTableViewCell

+ (NSString *)reuseId
{
    return @"BTOCSRerquiredQuestionsTableViewCell";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.detailLabel.hidden = YES;
    [self.detailLabel addConstraint:[NSLayoutConstraint constraintWithItem:self.detailLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:0.0f]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateWithAnswered:(NSString *)answered ofTotal:(NSString *)total
{
    NSString *baseString = [NSString stringWithFormat:@"Answered: %@/%@",answered,total];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:baseString attributes:@{
                                                                                                                                  NSFontAttributeName: [UIFont fontWithName:kBtFontBold size: 14.0f],
                                                                                                                                  NSForegroundColorAttributeName: [UIColor blackColor]
                                                                                                                                  }];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:kBtFontRegular size: 14.0f] range:NSMakeRange(10, baseString.length-10)];
    
    [self.detailLabel setAttributedText:attributedString];
}

@end
