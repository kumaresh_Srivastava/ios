//
//  UIImage+Resize.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 22/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Resize)

- (UIImage*)resizeWithScaleFactor:(CGFloat)scale;

@end

NS_ASSUME_NONNULL_END
