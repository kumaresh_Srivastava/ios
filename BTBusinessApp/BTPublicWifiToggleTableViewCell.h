//
//  BTPublicWifiToggleTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 19/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBaseTableViewCell.h"

typedef NS_ENUM(NSInteger, BTPublicWifiToggleType) {
    BTPublicWifiToggleTypeBTWiFi,
    BTPublicWifiToggleTypeBTGuestWiFi
};

@class BTPublicWifiToggleTableViewCell;

@protocol BTPublicWifiToggleTableViewCellDelegate

- (void)BTPublicWiFiToggleCell:(BTPublicWifiToggleTableViewCell*)cell toggledToState:(BOOL)state;

@end

@interface BTPublicWifiToggleTableViewCell : BTBaseTableViewCell
@property (strong, nonatomic) IBOutlet UILabel *networkNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *networkDescriptionLabel;
@property (strong, nonatomic) IBOutlet UISwitch *toggleSwitch;

@property (nonatomic) BTPublicWifiToggleType toggleType;
@property (nonatomic) id<BTPublicWifiToggleTableViewCellDelegate> delegate;

- (void)setupCellForToggleType:(BTPublicWifiToggleType)type;
- (void)updateToggleState:(BOOL)state;
- (IBAction)toggleSwitched:(id)sender;

@end
