//
//  NLGetProjectEnquiries.h
//  BTBusinessApp
//
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLAPIGEEAuthenticatedWebService.h"

@class NLGetProjectEnquiries;
@class NLWebServiceError;

@protocol NLGetProjectEnquiriesDelegate <NLAPIGEEAuthenticationProtectedWebServiceDelegate>

- (void)getProjectEnquiriesWebService:(NLGetProjectEnquiries *)webService finishedWithResponse:(NSObject *)response;

- (void)getProjectEnquiriesWebService:(NLGetProjectEnquiries *)webService failedWithWebServiceError:(NLWebServiceError *)error;

@optional
- (void)timedOutWithoutSuccessForGetProjectEnquiriesWebService:(NLGetProjectEnquiries*)webService;

@end

@interface NLGetProjectEnquiries : NLAPIGEEAuthenticatedWebService

@property (nonatomic) NSDictionary* projectDetails;

@property (nonatomic, weak) id <NLGetProjectEnquiriesDelegate> getProjectEnquiriesDelegate;

- (instancetype)initWithProjectRef:(NSString *)projectRef;
- (instancetype)initWithProjectDetails:(NSDictionary*)projectDetails;

@end
