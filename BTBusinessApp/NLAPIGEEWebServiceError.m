//
//  NLAPIGEEWebServiceError.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 19/09/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLAPIGEEWebServiceError.h"

@implementation NLAPIGEEWebServiceError

- (id)initWithError:(NSError *)error andSourceError:(NLWebServiceError *)sourceError andErrorCode:(NSString *)errorCode andDescription:(NSString *)description
{
    self = [super init];
    if(self)
    {
        _errorCode = errorCode;
        _errorDescription = description;
    }
    return self;
}

+ (NLAPIGEEWebServiceError *)apigeeWebServiceNetworkErrorWithErrorCode:(NSString *)errorCode andDescription:(NSString *)description
{
    NSError *newErrorObject = [NSError errorWithDomain:BTVordelErrorDomain code:[NLAPIGEEWebServiceError codeForApigeeError:errorCode] userInfo:nil];
    NLAPIGEEWebServiceError *apigeeError = [[NLAPIGEEWebServiceError alloc] initWithError:newErrorObject andSourceError:nil andErrorCode:errorCode andDescription:description];
    return apigeeError;
}

+ (NLWebServiceError *)webServiceNetworkErrorWithErrorDomain:(NSString *)networkErrorDomain andNetworkErrorCode:(BTNetworkErrorCode)networkErrorCode
{
    NSError *newErrorObject = [NSError errorWithDomain:networkErrorDomain code:networkErrorCode userInfo:nil];
    NLWebServiceError *webServiceError = [[NLWebServiceError alloc] initWithError:newErrorObject andSourceError:nil];
    
    return webServiceError;
}

+ (BTNetworkErrorCode)codeForApigeeError:(NSString *)error
{
    return BTNetworkErrorCodeNone;
}

@end
