#import "NLEEWebService.h"
#import "BTAuthenticationToken.h"
#import "EENetworkStatus.h"

@class EEAuthenticationToken;
@class NLWebServiceError;
@class NLEENetworkStatusWebService;

@protocol NLEENetworkStatusWebServiceDelegate <NSObject>

- (void)eeNetworkStatusWebService:(NLEENetworkStatusWebService *)service successWithNetworkStatus:(EENetworkStatus *)network;

- (void)eeNetworkStatusWebService:(NLEENetworkStatusWebService *)service failedWithError:(NLWebServiceError *)error;

@end

@interface NLEENetworkStatusWebService : NLEEWebService {
    
}

@property (nonatomic) BTAuthenticationToken *token;

@property (nonatomic, weak) id <NLEENetworkStatusWebServiceDelegate> eeNetworkStatusServiceDelegate;
@property (nonatomic, weak) NSString* authToken;

-(instancetype)initWithToken:(BTAuthenticationToken*)authToken ;
-(instancetype)initWithToken:(BTAuthenticationToken*)authToken lat:(NSString*)latitude andLong:(NSString*)longitude;

@end

