//
//  NLEEGeocodingWebService.h
//  BTBusinessApp
//

#import "NLEEWebService.h"

@class EEAuthenticationToken;
@class NLWebServiceError;
@class NLEEGeocodingWebService;

@protocol NLEEGeocodingWebServiceDelegate <NSObject>

- (void)eeGeocodingWebService:(NLEEGeocodingWebService *)service successWithLat:(NSString*)latitude andLong:(NSString*)longitude;

- (void)eeGeocodingWebService:(NLEEGeocodingWebService *)service failedWithError:(NLWebServiceError *)error;

@end

@interface NLEEGeocodingWebService : NLEEWebService {
    
}

@property (nonatomic) BTAuthenticationToken *token;

-(instancetype)initWithToken:(BTAuthenticationToken*)authToken andPostcode:(NSString*)postcode;

@property (nonatomic, weak) id <NLEEGeocodingWebServiceDelegate> eeGeocodingServiceDelegate;
@property (nonatomic, weak) NSString* authToken;

@end
