//
//  NLEEGeocodingWebService.m
//  BTBusinessApp
//

#import "NLEEGeocodingWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"

@implementation NLEEGeocodingWebService

-(instancetype)initWithToken:(BTAuthenticationToken*)authToken andPostcode:(NSString*)postcode {
    if (authToken == nil || postcode == nil || [postcode isEqualToString:@""])
    {
        return nil;
    }
    
    _authToken = [NSString stringWithFormat:@"Bearer %@", [authToken accessToken]];
    
    postcode = [postcode stringByReplacingOccurrencesOfString:@" " withString:@""]; //No spaces allowed in the postcode
    NSString *endPointURLString = [NSString stringWithFormat:@"/v1/geocode?location=%@", postcode];
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    
    return self;
}

- (NSDictionary *)httpHeaderFields
{
    NSString *appID = @"BTBusinessApp";
    NSString *appUDID = [AppDelegate sharedInstance].viewModel.app.btInstallationID;
    NSString *requestID = [NSString stringWithFormat:@"%@-%@", appID, appUDID];
    
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *httpHeaders = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    [httpHeaders setObject:_authToken forKey:@"Authorization"];
    [httpHeaders setObject:requestID forKey:@"X-EE-EL-Tracking-Header"];
    
    return [httpHeaders copy];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    NSArray *locations = [responseObject valueForKey:@"locations"];
    NSDictionary *location = locations[0];
//    double latValue = [[locations valueForKey:@"latitude"] doubleValue];
//    double longValue = [[locations valueForKey:@"longitude"] doubleValue];
//    NSNumber *latitude = [NSNumber numberWithDouble:latValue];
//    NSNumber *longitude = [NSNumber numberWithDouble:longValue];
    
    NSNumber *latitude = [location valueForKey:@"latitude"];
    NSNumber *longitude = [location valueForKey:@"longitude"];
    
    [_eeGeocodingServiceDelegate eeGeocodingWebService:self successWithLat:[latitude stringValue] andLong:[longitude stringValue]];
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
}

#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andError:(NSError *)error
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andError:error];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
    } else {
        
    }
    
    return errorToBeReturned;
}

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        
        // (hds) No handling in this class at this point of time.
    }
    
    return errorToBeReturned;
}


- (BOOL)attemptErrorHandlingIfNeededForError:(NLWebServiceError *)webServiceError
{
    BOOL errorHandled = [super attemptErrorHandlingIfNeededForError:webServiceError];
    
    if(errorHandled == NO)
    {
        // (hds) If there was no error handling from superclass, then this class can check if it needs to handle the error or not.
        
        // (hds) Handling the MBaassTimeOut Error and taking action on it.
        
    }
    
    return errorHandled;
}

@end
