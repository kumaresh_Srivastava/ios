//
//  NLEENetworkStatusWebService.m
//  BTBusinessApp
//

#import "NLEENetworkStatusWebService.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"

@implementation NLEENetworkStatusWebService

// Regional call
-(instancetype)initWithToken:(BTAuthenticationToken*)authToken lat:(NSString*)latitude andLong:(NSString*)longitude {
    if (authToken == nil || longitude == nil || longitude == nil || [latitude isEqualToString:@""] || [longitude isEqualToString:@""])
    {
        return nil;
    }
    
    _authToken = [NSString stringWithFormat:@"Bearer %@", [authToken accessToken]];
    
    NSString *endPointURLString = [NSString stringWithFormat:@"v1/network-status/location?latitude=%@&longitude=%@&searchTerm=''", latitude, longitude];
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    
    return self;
}

// National call
-(instancetype)initWithToken:(BTAuthenticationToken *)authToken {
    if (authToken == nil)
    {
        return nil;
    }
    
    _authToken = [NSString stringWithFormat:@"Bearer %@", [authToken accessToken]];
    
    NSString *endPointURLString = @"v1/network-status/location";
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    
    return self;
}

- (NSDictionary *)httpHeaderFields
{
    NSString *appID = @"BTBusinessApp";
    NSString *appUDID = [AppDelegate sharedInstance].viewModel.app.btInstallationID;
    NSString *requestID = [NSString stringWithFormat:@"%@-%@", appID, appUDID];
    
    NSDictionary *headerFieldsFromSuperClasses = [super httpHeaderFields];
    NSMutableDictionary *httpHeaders = [NSMutableDictionary dictionaryWithDictionary:headerFieldsFromSuperClasses];
    [httpHeaders setObject:_authToken forKey:@"Authorization"];
    [httpHeaders setObject:requestID forKey:@"X-EE-EL-Tracking-Header"];
    [httpHeaders setObject:appID forKey:@"X-EE-API-Originator"];
    
    return [httpHeaders copy];
}

#pragma mark - NLWebService Response Handling Private Methods

- (void)handleSuccessWithSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    [super handleSuccessWithSessionDataTask:task andResponseObject:responseObject];
    EENetworkStatus *networkStatus = [[EENetworkStatus alloc] initWithDictionary:responseObject];
    [_eeNetworkStatusServiceDelegate eeNetworkStatusWebService:self successWithNetworkStatus:networkStatus];
    
}

- (void)handleFailureWithSessionDataTask:(NSURLSessionDataTask *)task andWebServiceError:(NLWebServiceError *)webServiceError
{
    [super handleFailureWithSessionDataTask:task andWebServiceError:webServiceError];
}

#pragma mark - Override Helper Methods For Error Handling

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andError:(NSError *)error
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andError:error];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
    } else {
        
    }
    
    return errorToBeReturned;
}

- (NLWebServiceError *)processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:(NSURLSessionDataTask *)task andResponseObject:(id)responseObject
{
    NLWebServiceError *errorToBeReturned = [super processEndOfAPIRequestAndReturnWebServiceErrorForSessionDataTask:task andResponseObject:responseObject];
    
    if(errorToBeReturned == nil)
    {
        // (hds) If there was no error from superclass, then this class can check for its level of errors.
        
        // (hds) No handling in this class at this point of time.
    }
    
    return errorToBeReturned;
}


- (BOOL)attemptErrorHandlingIfNeededForError:(NLWebServiceError *)webServiceError
{
    BOOL errorHandled = [super attemptErrorHandlingIfNeededForError:webServiceError];
    
    if(errorHandled == NO)
    {
        // (hds) If there was no error handling from superclass, then this class can check if it needs to handle the error or not.
        
        // (hds) Handling the MBaassTimeOut Error and taking action on it.
        
    }
    
    return errorHandled;
}

@end

