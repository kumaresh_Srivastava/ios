//
//  BTDeviceDetailTableViewCell.h
//  BTBusinessApp
//
//  Created by kumaresh shrivastava on 04/07/2019.
//  Copyright © 2019 BT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLMMobileServiceAssetDetailScreen.h"
NS_ASSUME_NONNULL_BEGIN

@interface BTDeviceDetailTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *mobileDeviceNameValueLable;
@property (weak, nonatomic) IBOutlet UILabel *mobileDeviceIMEIValueLable;

- (void)upadateWithRowWrapper:(MobileAssetsDetailRowWrapper *)rowWrapper;

@end

NS_ASSUME_NONNULL_END
