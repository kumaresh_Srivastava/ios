//
//  BTOCSProjectEnquiryTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 02/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

@class BTOCSEnquiry;

NS_ASSUME_NONNULL_BEGIN

@interface BTOCSProjectEnquiryTableViewCell : BTBaseTableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *indicatorImageView;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *subjectLabel;

- (void)updateWithEnquiry:(BTOCSEnquiry*)enquiry;

@end

NS_ASSUME_NONNULL_END
