//
//  BTUpgradeHubTableViewCell.m
//  BTBusinessApp
//

#import "BTUpgradeHubTableViewCell.h"

@implementation BTUpgradeHubTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    _upgradeButton.layer.borderColor = [BrandColours colourTextBTPinkColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)upgradePressed:(id)sender {
    [self.BTUpgradeHubTableViewCellDelegate userPressedCellForUpgradeHub:self];
}
@end
