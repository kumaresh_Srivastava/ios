//
//  BTOCSSimpleInputTableViewCell.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 03/10/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@class BTBaseTableViewCell;
@class BTOCSSimpleInputTableViewCell;

@protocol BTOCSSimpleInputTableViewCellDelegate <NSObject>

- (void)cell:(BTOCSSimpleInputTableViewCell*)cell finishedEditingWithValue:(NSString*)value;
- (void)nextButtonPressedOnCell:(BTBaseTableViewCell*)cell;
- (void)previousButtonPressedOnCell:(BTBaseTableViewCell*)cell;

@end

@interface BTOCSSimpleInputTableViewCell : BTBaseTableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UITextView *entryTextView;

@property (nonatomic) id<BTOCSSimpleInputTableViewCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
