//
//  NLVordelWebServiceError.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 06/06/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "NLWebServiceError.h"

@interface NLVordelWebServiceError : NLWebServiceError

@property (nonatomic, readonly) NSString *errorCode;
@property (nonatomic, readonly) NSString *errorDescription;

- (id)initWithError:(NSError *)error andSourceError:(NLWebServiceError *)sourceError andErrorCode:(NSString*)errorCode andDescription:(NSString*)description;

+ (NLVordelWebServiceError *)VordelwebServiceNetworkErrorWithErrorCode:(NSString*)errorCode andDescription:(NSString*)description;

+ (BTNetworkErrorCode)codeForVordelError:(NSString*)error;

@end
