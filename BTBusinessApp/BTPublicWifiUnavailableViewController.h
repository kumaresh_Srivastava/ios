//
//  BTPublicWifiUnavailableViewController.h
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 17/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, BTPublicWifiUnavailableContext) {
    BTPublicWifiUnavailableContext4GAssureEnabled,
    BTPublicWifiUnavailableContextBroadbandNotWorking,
    BTPublicWifiUnavailableContextContactAdmin,
    BTPublicWifiUnavailableContextNoConnection,
    BTPublicWifiUnavailableContextGenericError,
    BTPublicWifiUnavailableContextGuestWiFiInProgress,
    BTPublicWifiUnavailableContextGenericError4GAssure
};

@class BTClientServiceInstance;

@interface BTPublicWifiUnavailableViewController : UIViewController

@property (nonatomic) BTPublicWifiUnavailableContext context;
@property (strong, nonatomic) IBOutlet UIImageView *alertImageView;
@property (strong, nonatomic) IBOutlet UILabel *alertHeadlineLabel;
//@property (strong, nonatomic) IBOutlet UITextView *alertHeadlineTextView;
@property (strong, nonatomic) IBOutlet UITextView *alertDetailTextView;
@property (strong, nonatomic) IBOutlet UIButton *alertActionButton;

- (IBAction)buttonAction:(id)sender;
- (instancetype)initWithContext:(BTPublicWifiUnavailableContext)context;
- (instancetype)initWithClientServiceInstance:(BTClientServiceInstance*)instance andContext:(BTPublicWifiUnavailableContext)context;
- (instancetype)initWithClientServiceInstance:(BTClientServiceInstance*)instance context:(BTPublicWifiUnavailableContext)context wifiNetwork:(NSString*)network andState:(NSString*)state;

+ (BTPublicWifiUnavailableViewController*)getBTPublicWifiUnavailableViewController;

@end
