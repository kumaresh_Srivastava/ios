//
//  BTAppointmentPickerCollectionViewCell.h
//  ApptPicker
//
//  Created by Gareth Vaughan on 15/11/2017.
//  Copyright © 2017 Gareth Vaughan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTAppointmentPickerCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *dayNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateMonthLabel;
@property (strong, nonatomic) IBOutlet UIView *selectedIndicator;
@property (strong, nonatomic) IBOutlet UIView *loadingView;

@end
