//
//  BTPublicWifiBaseTableViewCell.m
//  BTBusinessApp
//
//  Created by Gareth Vaughan on 19/04/2018.
//  Copyright © 2018 BT. All rights reserved.
//

#import "BTBaseTableViewCell.h"

@implementation BTBaseTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (CGFloat)heightForCell
{
    // can be overridden by subcalss
    return UITableViewAutomaticDimension;
}

+ (NSString *)reuseId
{
    // should be overridden by subclass
    return @"";
}

@end
