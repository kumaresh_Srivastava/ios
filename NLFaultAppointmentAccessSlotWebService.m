//
//  NLFaultAppointmentAccessSlotWebService.m
//  BTBusinessApp
//
//  Created by VectoScalar on 3/6/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import "NLFaultAppointmentAccessSlotWebService.h"

@implementation NLFaultAppointmentAccessSlotWebService

- (instancetype)initWithFaultID:(NSString *)faultID andInputDate:(NSString *)inputDate
{
    NSString *endPointURLString = [NSString stringWithFormat:@"/Account/api/v3/FaultAppointment/AccessSlots/%@/%@",inputDate,faultID];
    
    self = [super initWithMethod:@"GET" parameters:nil andEndpointUrlString:endPointURLString];
    if(self)
    {
        
    }
    
    return self;
    
}

@end
