//
//  DLMOrderSummaryScreenTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/10/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock.h>
#import "DLMOrderSummaryScreen.h"
#import "NLGetOrderSummaryWebService.h"
#import "BTLiveChatAvailabilityChecker.h"
#import "BTProduct.h"
#import "BTOrder.h"

@interface DLMOrderSummaryScreen (test)

- (void)setupToFetchOrderSummaryWithOrderRef:(NSString *)orderRef;
- (void)fetchOrderSummaryForCurrentOrder;
- (void)setupToCheckLiveChatAvailability;
- (void)getAvailableSlotsForChat;
- (void)getBundleRelatedInfoFromOrder;

@end

@interface DLMOrderSummaryScreenTests : XCTestCase

@property (nonatomic) DLMOrderSummaryScreen *orderSummaryVM;

@end

@implementation DLMOrderSummaryScreenTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    _orderSummaryVM = [[DLMOrderSummaryScreen alloc] init];
    
    XCTAssertNotNil(_orderSummaryVM, @"DLMOrderSummaryScreen instance is not created.");
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _orderSummaryVM = nil;
}

- (void)testFetchOrderSummaryForOrderReference {
    
    id mockDLMOrderSummaryObj = [OCMockObject partialMockForObject:_orderSummaryVM];
    
    NSString *mockString = @"BT012345";
    [_orderSummaryVM fetchOrderSummaryForOrderReference:mockString];
    
    OCMVerify([mockDLMOrderSummaryObj setupToFetchOrderSummaryWithOrderRef:mockString]);
    OCMVerify([mockDLMOrderSummaryObj fetchOrderSummaryForCurrentOrder]);
    
    [mockDLMOrderSummaryObj stopMocking];
}

- (void)testSetupToFetchOrderSummaryWithOrderRef {
    
    [_orderSummaryVM setupToFetchOrderSummaryWithOrderRef:@"BT012345"];
    
    XCTAssertNotNil([_orderSummaryVM valueForKey:@"getOrderSummaryWebService"], @"NLGetOrderSummaryWebService instance is not created inside method.");
}

- (void)testFetchOrderSummaryForCurrentOrder {
    
    id mockOrderSummaryWSObj = OCMClassMock([NLGetOrderSummaryWebService class]);
    
    [_orderSummaryVM setValue:mockOrderSummaryWSObj forKey:@"getOrderSummaryWebService"];
    
    [_orderSummaryVM fetchOrderSummaryForCurrentOrder];
    
    OCMVerify([mockOrderSummaryWSObj resume]);
    
    [mockOrderSummaryWSObj stopMocking];
}

- (void)testCancelFetchOrderSummaryAPICall {
    
    id mockOrderSummaryWSObj = OCMClassMock([NLGetOrderSummaryWebService class]);
    
    [_orderSummaryVM setValue:mockOrderSummaryWSObj forKey:@"getOrderSummaryWebService"];
    
    [_orderSummaryVM cancelFetchOrderSummaryAPICall];
    
    OCMVerify([mockOrderSummaryWSObj cancel]);
    
    [mockOrderSummaryWSObj stopMocking];
}

- (void)testCheckForLiveChatAvailibilityForOrder
{
    id mockDLMOrderSummaryObj = [OCMockObject partialMockForObject:_orderSummaryVM];
    
    [_orderSummaryVM checkForLiveChatAvailibilityForOrder];
    
    OCMVerify([mockDLMOrderSummaryObj setupToCheckLiveChatAvailability]);
    OCMVerify([mockDLMOrderSummaryObj getAvailableSlotsForChat]);
    
    [mockDLMOrderSummaryObj stopMocking];
}

- (void)testSetupToCheckLiveChatAvailability
{
    [_orderSummaryVM setupToCheckLiveChatAvailability];
    
    XCTAssertNotNil([_orderSummaryVM valueForKey:@"_liveChatAvailabityChecker"], @"BTLiveChatAvailabilityChecker instance is not created inside method.");
}

- (void)testGetAvailableSlotsForChat
{
    id mockChatAvailabilityCheckerObj = OCMClassMock([BTLiveChatAvailabilityChecker class]);
    
    [_orderSummaryVM setValue:mockChatAvailabilityCheckerObj forKey:@"_liveChatAvailabityChecker"];
    
    [_orderSummaryVM getAvailableSlotsForChat];
    
    OCMVerify([mockChatAvailabilityCheckerObj getLiveChatAvailableSlots]);
    
    [mockChatAvailabilityCheckerObj stopMocking];
}

- (void)testCancelChatAvailabilityCheckerAPI
{
    id mockChatAvailabilityCheckerObj = OCMClassMock([BTLiveChatAvailabilityChecker class]);
    
    [_orderSummaryVM setValue:mockChatAvailabilityCheckerObj forKey:@"_liveChatAvailabityChecker"];
    
    [_orderSummaryVM cancelChatAvailabilityCheckerAPI];
    
    OCMVerify([mockChatAvailabilityCheckerObj cancel]);
    
    [mockChatAvailabilityCheckerObj stopMocking];
}

- (void)testGetBundleRelatedInfoFromOrder {
    
    NSDictionary *prodDict1 = [NSDictionary dictionaryWithObjectsAndKeys:@"BT0123451-1", @"OrderKey", [NSNumber numberWithBool:NO], @"IsBundleProduct", nil];
    BTProduct *prod1 = [[BTProduct alloc] initProductWithResponse:prodDict1 andIndexInAPIResponse:1];
    
    NSDictionary *prodDict2 = [NSDictionary dictionaryWithObjectsAndKeys:@"BT0123451-2", @"OrderKey", [NSNumber numberWithBool:YES], @"IsBundleProduct", nil];
    BTProduct *prod2 = [[BTProduct alloc] initProductWithResponse:prodDict2 andIndexInAPIResponse:2];
    
    BTOrder *order = [[BTOrder alloc] init];
    [order addProduct:prod1];
    [order addProduct:prod2];
    
    [_orderSummaryVM setValue:order forKey:@"order"];
    
    [_orderSummaryVM getBundleRelatedInfoFromOrder];
    
    XCTAssertNotNil(_orderSummaryVM.arrayOfProductsRelatedToBundle, @"Bundle products are not created in method");
    XCTAssertNotNil(_orderSummaryVM.arrayOfProductsNotRelatedToBundle, @"Products not related to bundle are not created in method.");
    
    XCTAssertEqual(_orderSummaryVM.arrayOfProductsRelatedToBundle.count, 1, @"Bundle related products count should be 1.");
    XCTAssertEqual(_orderSummaryVM.arrayOfProductsNotRelatedToBundle.count, 1, @"Products count not related to bundle should be 1.");
}

@end
