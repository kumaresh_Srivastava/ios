//
//  BTOrderTrackerDetailsViewControllerTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/10/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock.h>
#import "BTOrderTrackerDetailsViewController.h"
#import "DLMOrderSummaryScreen.h"
#import "AppConstants.h"
#import "AppManager.h"
#import "OmnitureManager.h"

@interface BTOrderTrackerDetailsViewController (test)

- (void)initialSetupForOrderSummaryScreen;
- (void)createLoadingView;
- (void)hideorShowControls:(BOOL)isHide;
- (void)checkChatAvailabilityAction;
- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message;
- (void)updateUserInterface;
- (void)hideRetryItems:(BOOL)isHide;
- (void)hideLoadingItems:(BOOL)isHide;
- (void)fetchOrderSummaryDetails;
- (void)showRetryWithInternetStrip:(BOOL)needToShowInternetStrip;
- (void)trackOmniPage;
- (void)trackOmniClick:(NSString *)clickEvent forPage:(NSString *)page;

@end

@interface BTOrderTrackerDetailsViewControllerTests : XCTestCase

@property (nonatomic) BTOrderTrackerDetailsViewController *orderTrackerDetailsVC;

@end

@implementation BTOrderTrackerDetailsViewControllerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];
    
    _orderTrackerDetailsVC = (BTOrderTrackerDetailsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"OrderTrackerDetailViewController"];
    
    XCTAssertNotNil(_orderTrackerDetailsVC.view);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _orderTrackerDetailsVC = nil;
}

- (void)testInitialSetupForOrderSummaryScreen {
    
    id mockOrderDetailsVCObj = [OCMockObject partialMockForObject:_orderTrackerDetailsVC];
    
    [_orderTrackerDetailsVC initialSetupForOrderSummaryScreen];
    
    OCMVerify([mockOrderDetailsVCObj createLoadingView]);
    OCMVerify([mockOrderDetailsVCObj hideorShowControls:YES]);
    
    [mockOrderDetailsVCObj stopMocking];
}

- (void)testCheckChatAvailabilityActionWithNoConnection {
    
    id mockOrderDetailsVCObj = [OCMockObject partialMockForObject:_orderTrackerDetailsVC];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(NO);
    
    [_orderTrackerDetailsVC checkChatAvailabilityAction];
    
    OCMVerify([mockOrderDetailsVCObj showAlertWithTitle:kNoConnectionTitle andMessage:kNoConnectionMessage]);
    
    [mockOrderDetailsVCObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testCheckChatAvailabilityActionWithInternetConnection {
    
    id mockOrderDetailsVCObj = [OCMockObject partialMockForObject:_orderTrackerDetailsVC];
    
    DLMOrderSummaryScreen *orderSummaryVM = [[DLMOrderSummaryScreen alloc] init];
    
    id mockDLMOrderSummaryObj = [OCMockObject partialMockForObject:orderSummaryVM];
    
    [_orderTrackerDetailsVC setValue:mockDLMOrderSummaryObj forKey:@"viewModel"];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(YES);
    
    [_orderTrackerDetailsVC checkChatAvailabilityAction];
    
    OCMVerify([mockOrderDetailsVCObj hideLoadingItems:NO]);
    OCMVerify([mockDLMOrderSummaryObj checkForLiveChatAvailibilityForOrder]);
    
    [mockOrderDetailsVCObj stopMocking];
    [mockDLMOrderSummaryObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testUpdateUserInterface {
    
    id mockOrderDetailsVCObj = [OCMockObject partialMockForObject:_orderTrackerDetailsVC];
    
    [_orderTrackerDetailsVC updateUserInterface];
    
    OCMVerify([mockOrderDetailsVCObj hideorShowControls:NO]);
    OCMVerify([mockOrderDetailsVCObj hideRetryItems:YES]);
    OCMVerify([mockOrderDetailsVCObj hideLoadingItems:YES]);
    
    [mockOrderDetailsVCObj stopMocking];
}

- (void)testFetchOrderSummaryDetailsWithNoConnection {
    
    id mockOrderDetailsVCObj = [OCMockObject partialMockForObject:_orderTrackerDetailsVC];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(NO);
    
    [_orderTrackerDetailsVC fetchOrderSummaryDetails];
    
    OCMVerify([mockOrderDetailsVCObj showRetryWithInternetStrip:YES]);
    OCMVerify([mockAppManagerObj trackNoInternetErrorOnPage:OMNIPAGE_ORDER_SUMMARY]);
    
    [mockOrderDetailsVCObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testFetchOrderSummaryDetailsWithInternetConnection {
    
    id mockOrderDetailsVCObj = [OCMockObject partialMockForObject:_orderTrackerDetailsVC];
    
    DLMOrderSummaryScreen *orderSummaryVM = [[DLMOrderSummaryScreen alloc] init];
    
    id mockDLMOrderSummaryObj = [OCMockObject partialMockForObject:orderSummaryVM];
    
    [_orderTrackerDetailsVC setValue:mockDLMOrderSummaryObj forKey:@"viewModel"];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(YES);
    
    [_orderTrackerDetailsVC fetchOrderSummaryDetails];
    
    OCMVerify([mockOrderDetailsVCObj hideLoadingItems:NO]);
    OCMVerify([mockOrderDetailsVCObj hideRetryItems:YES]);
    OCMVerify([mockDLMOrderSummaryObj fetchOrderSummaryForOrderReference:[OCMArg any]]);
    
    [mockOrderDetailsVCObj stopMocking];
    [mockDLMOrderSummaryObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testTrackOmniPage {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [_orderTrackerDetailsVC trackOmniPage];
    
    OCMVerify([mockOmnitureManagerObj trackPage:[OCMArg any] withContextInfo:[OCMArg any]]);
    
    [mockOmnitureManagerObj stopMocking];
}

- (void)testTrackOmniClickForPage {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [_orderTrackerDetailsVC trackOmniClick:@"SampleClick" forPage:OMNIPAGE_ORDER_SUMMARY];
    
    OCMVerify([mockOmnitureManagerObj trackClick:[OCMArg any] withContextInfo:[OCMArg any]]);
    
    [mockOmnitureManagerObj stopMocking];
}

@end
