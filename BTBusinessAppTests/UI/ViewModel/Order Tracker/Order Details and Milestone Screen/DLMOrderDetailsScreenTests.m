//
//  DLMOrderDetailsScreenTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/21/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock.h>
#import "DLMOrderDetailsScreen.h"
#import "NLOrderDetailsWebService.h"
#import "NLGetOrderMilestonesWebService.h"
#import "BTLiveChatAvailabilityChecker.h"

@interface DLMOrderDetailsScreen (test)

- (void)setupToFetchOrderDetailsWithOrderRef:(NSString *)orderRef andItemRef:(NSString *)itemRef;
- (void)fetchOrderDetailsForCurrentOrder;
- (void)setupToFetchCompleteOrderDetailsForOrderReference:(NSString *)orderRef ItemRef:(NSString *)itemRef postCode:(NSString *)postCode andErrorCount:(NSInteger)errorCount;
- (void)fetchCompleteOrderDetailsForCurrentOrder;
- (void)setupToFetchOrderMilestonesForOrderReference:(NSString *)orderRef andItemRef:(NSString *)itemRef;
- (void)fetchOrderMilestonesForCurrentOrder;
- (void)setupToFetchOrderMilestonesForOrderReference:(NSString *)orderRef ItemRef:(NSString *)itemRef postCode:(NSString *)postCode andErrorCount:(NSInteger)errorCount;
- (void)fetchCompleteOrderMilestonesForCurrentOrder;
- (void)setupToCheckLiveChatAvailability;
- (void)getAvailableSlotsForChat;
- (void)cancelGetOrderMilestonesRequest;

@end

@interface DLMOrderDetailsScreenTests : XCTestCase

@property (nonatomic) DLMOrderDetailsScreen *orderDetailsVM;

@end

@implementation DLMOrderDetailsScreenTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    _orderDetailsVM = [[DLMOrderDetailsScreen alloc] init];
    
    XCTAssertNotNil(_orderDetailsVM, @"DLMOrderDetailsScreen instance is not created.");
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _orderDetailsVM = nil;
}

- (void)testFetchOrderDetailsForOrderReferenceAndItemRef {
    
    id mockDLMOrderDetailsObj = [OCMockObject partialMockForObject:_orderDetailsVM];
    
    NSString *mockOrderRef = @"BT012345";
    NSString *mockItemRef = @"BT0123451-1";
    [_orderDetailsVM fetchOrderDetailsForOrderReference:mockOrderRef andItemRef:mockItemRef];
    
    OCMVerify([mockDLMOrderDetailsObj setupToFetchOrderDetailsWithOrderRef:OCMOCK_ANY andItemRef:OCMOCK_ANY]);
    OCMVerify([mockDLMOrderDetailsObj fetchOrderDetailsForCurrentOrder]);
    
    [mockDLMOrderDetailsObj stopMocking];
}

- (void)testSetupToFetchOrderDetailsWithOrderRefAndItemRef {
    
    NSString *mockOrderRef = @"BT012345";
    NSString *mockItemRef = @"BT0123451-1";
    [_orderDetailsVM setupToFetchOrderDetailsWithOrderRef:mockOrderRef andItemRef:mockItemRef];
    
    XCTAssertNotNil([_orderDetailsVM valueForKey:@"getOrderDetailsWebService"], @"NLGetOrderSummaryWebService instance is not created inside method.");
}

- (void)testFetchOrderDetailsForCurrentOrder {
    
    id mockOrderDetailsWSObj = OCMClassMock([NLOrderDetailsWebService class]);
    
    [_orderDetailsVM setValue:mockOrderDetailsWSObj forKey:@"getOrderDetailsWebService"];
    
    [_orderDetailsVM fetchOrderDetailsForCurrentOrder];
    
    OCMVerify([mockOrderDetailsWSObj resume]);
    
    [mockOrderDetailsWSObj stopMocking];
}

- (void)testFetchCompleteOrderDetailsForOrderReferenceAndItemRefAndPostCode {
    
    id mockDLMOrderDetailsObj = [OCMockObject partialMockForObject:_orderDetailsVM];
    
    NSString *mockOrderRef = @"BT012345";
    NSString *mockItemRef = @"BT0123451-1";
    NSString *mockPostCode = @"AB101BA";
    [_orderDetailsVM fetchCompleteOrderDetailsForOrderReference:mockOrderRef ItemRef:mockItemRef postCode:mockPostCode andErrorCount:1];
    
    OCMVerify([mockDLMOrderDetailsObj setupToFetchCompleteOrderDetailsForOrderReference:OCMOCK_ANY ItemRef:OCMOCK_ANY postCode:OCMOCK_ANY andErrorCount:1]);
    OCMVerify([mockDLMOrderDetailsObj fetchCompleteOrderDetailsForCurrentOrder]);
    
    [mockDLMOrderDetailsObj stopMocking];
}

- (void)testSetupToFetchCompleteOrderDetailsForOrderReferenceAndItemRefAndPostCode {
    
    NSString *mockOrderRef = @"BT012345";
    NSString *mockItemRef = @"BT0123451-1";
    NSString *mockPostCode = @"AB101BA";
    [_orderDetailsVM setupToFetchCompleteOrderDetailsForOrderReference:mockOrderRef ItemRef:mockItemRef postCode:mockPostCode andErrorCount:1];
    
    XCTAssertNotNil([_orderDetailsVM valueForKey:@"getOrderDetailsWithPostCodeWebService"], @"NLGetOrderSummaryWebService instance is not created inside method.");
}

- (void)testFetchCompleteOrderDetailsForCurrentOrder {
    
    id mockOrderDetailsWSObj = OCMClassMock([NLOrderDetailsWebService class]);
    
    [_orderDetailsVM setValue:mockOrderDetailsWSObj forKey:@"getOrderDetailsWithPostCodeWebService"];
    
    [_orderDetailsVM fetchCompleteOrderDetailsForCurrentOrder];
    
    OCMVerify([mockOrderDetailsWSObj resume]);
    
    [mockOrderDetailsWSObj stopMocking];
}

- (void)testFetchOrderMilestonesForOrderReferenceAndItemRef {
    
    id mockDLMOrderDetailsObj = [OCMockObject partialMockForObject:_orderDetailsVM];
    
    NSString *mockOrderRef = @"BT012345";
    NSString *mockItemRef = @"BT0123451-1";
    [_orderDetailsVM fetchOrderMilestonesForOrderReference:mockOrderRef andItemRef:mockItemRef];
    
    OCMVerify([mockDLMOrderDetailsObj setupToFetchOrderMilestonesForOrderReference:OCMOCK_ANY andItemRef:OCMOCK_ANY]);
    OCMVerify([mockDLMOrderDetailsObj fetchOrderMilestonesForCurrentOrder]);
    
    [mockDLMOrderDetailsObj stopMocking];
}

- (void)testSetupToFetchOrderMilestonesForOrderReferenceAndItemRef {
    
    NSString *mockOrderRef = @"BT012345";
    NSString *mockItemRef = @"BT0123451-1";
    
    [_orderDetailsVM setupToFetchOrderMilestonesForOrderReference:mockOrderRef andItemRef:mockItemRef];
    
    XCTAssertNotNil([_orderDetailsVM valueForKey:@"getOrderMilestonesWebService"], @"NLGetOrderMilestonesWebService instance is not created inside method.");
}

- (void)testFetchOrderMilestonesForCurrentOrder {
    
    id mockOrderMilestonesWSObj = OCMClassMock([NLGetOrderMilestonesWebService class]);
    
    [_orderDetailsVM setValue:mockOrderMilestonesWSObj forKey:@"getOrderMilestonesWebService"];
    
    [_orderDetailsVM fetchOrderMilestonesForCurrentOrder];
    
    OCMVerify([mockOrderMilestonesWSObj resume]);
    
    [mockOrderMilestonesWSObj stopMocking];
}

- (void)testFetchOrderMilestonesForOrderReferenceAndItemRefAndPostCode {
    
    id mockDLMOrderDetailsObj = [OCMockObject partialMockForObject:_orderDetailsVM];
    
    NSString *mockOrderRef = @"BT012345";
    NSString *mockItemRef = @"BT0123451-1";
    NSString *mockPostCode = @"AB101BA";
    [_orderDetailsVM fetchOrderMilestonesForOrderReference:mockOrderRef ItemRef:mockItemRef postCode:mockPostCode andErrorCount:1];
    
    OCMVerify([mockDLMOrderDetailsObj setupToFetchOrderMilestonesForOrderReference:OCMOCK_ANY ItemRef:OCMOCK_ANY postCode:OCMOCK_ANY andErrorCount:1]);
    OCMVerify([mockDLMOrderDetailsObj fetchCompleteOrderMilestonesForCurrentOrder]);
    
    [mockDLMOrderDetailsObj stopMocking];
}

- (void)testSetupToFetchOrderfetchCompleteOrderMilestonesForCurrentOrderForOrderReferenceAndItemRefAndPostCode {
    
    NSString *mockOrderRef = @"BT012345";
    NSString *mockItemRef = @"BT0123451-1";
    NSString *mockPostCode = @"AB101BA";
    [_orderDetailsVM setupToFetchOrderMilestonesForOrderReference:mockOrderRef ItemRef:mockItemRef postCode:mockPostCode andErrorCount:1];
    
    XCTAssertNotNil([_orderDetailsVM valueForKey:@"getOrderMilestonesWithPostCodeWebService"], @"NLGetOrderMilestonesWebService instance is not created inside method.");
}

- (void)testFetchCompleteOrderMilestonesForCurrentOrder {
    
    id mockOrderMilestonesWSObj = OCMClassMock([NLGetOrderMilestonesWebService class]);
    
    [_orderDetailsVM setValue:mockOrderMilestonesWSObj forKey:@"getOrderMilestonesWithPostCodeWebService"];
    
    [_orderDetailsVM fetchCompleteOrderMilestonesForCurrentOrder];
    
    OCMVerify([mockOrderMilestonesWSObj resume]);
    
    [mockOrderMilestonesWSObj stopMocking];
}

- (void)testCheckForLiveChatAvailibilityForOrder
{
    id mockDLMOrderDetailsObj = [OCMockObject partialMockForObject:_orderDetailsVM];
    
    [_orderDetailsVM checkForLiveChatAvailibilityForOrder];
    
    OCMVerify([mockDLMOrderDetailsObj setupToCheckLiveChatAvailability]);
    OCMVerify([mockDLMOrderDetailsObj getAvailableSlotsForChat]);
    
    [mockDLMOrderDetailsObj stopMocking];
}

- (void)testSetupToCheckLiveChatAvailability
{
    [_orderDetailsVM setupToCheckLiveChatAvailability];
    
    XCTAssertNotNil([_orderDetailsVM valueForKey:@"_liveChatAvailabityChecker"], @"BTLiveChatAvailabilityChecker instance is not created inside method.");
}

- (void)testGetAvailableSlotsForChat
{
    id mockChatAvailabilityCheckerObj = OCMClassMock([BTLiveChatAvailabilityChecker class]);
    
    [_orderDetailsVM setValue:mockChatAvailabilityCheckerObj forKey:@"_liveChatAvailabityChecker"];
    
    [_orderDetailsVM getAvailableSlotsForChat];
    
    OCMVerify([mockChatAvailabilityCheckerObj getLiveChatAvailableSlots]);
    
    [mockChatAvailabilityCheckerObj stopMocking];
}

- (void)testGetImageNameForProductIcon {
    
    XCTAssertTrue([[_orderDetailsVM getImageNameForProductIcon:@"broadbandIcon"] isEqualToString:@"broadband"]);
    XCTAssertTrue([[_orderDetailsVM getImageNameForProductIcon:@"phoneIcon"] isEqualToString:@"phoneline"]);
    XCTAssertTrue([[_orderDetailsVM getImageNameForProductIcon:@"hubIcon"] isEqualToString:@"broadband"]);
    XCTAssertTrue([[_orderDetailsVM getImageNameForProductIcon:@"isdnIcon"] isEqualToString:@"phoneline"]);
    XCTAssertNil([_orderDetailsVM getImageNameForProductIcon:@"randomIcon"]);
}

- (void)testCancelCurrentRequest {
    
    id mockOrderDetailsWSObj = OCMClassMock([NLOrderDetailsWebService class]);
    
    [_orderDetailsVM setValue:mockOrderDetailsWSObj forKey:@"getOrderDetailsWebService"];
    
    [_orderDetailsVM cancelCurrentRequest];
    
    OCMVerify([mockOrderDetailsWSObj cancel]);
    
    [mockOrderDetailsWSObj stopMocking];
}

- (void)testCancelGetOrderMilestonesRequest {
    
    id mockOrderMilestonesWSObj = OCMClassMock([NLGetOrderMilestonesWebService class]);
    
    [_orderDetailsVM setValue:mockOrderMilestonesWSObj forKey:@"getOrderMilestonesWebService"];
    
    [_orderDetailsVM cancelGetOrderMilestonesRequest];
    
    OCMVerify([mockOrderMilestonesWSObj cancel]);
    
    [mockOrderMilestonesWSObj stopMocking];
}

- (void)testCancelChatAvailabilityCheckerAPI
{
    id mockChatAvailabilityCheckerObj = OCMClassMock([BTLiveChatAvailabilityChecker class]);
    
    [_orderDetailsVM setValue:mockChatAvailabilityCheckerObj forKey:@"_liveChatAvailabityChecker"];
    
    [_orderDetailsVM cancelChatAvailabilityCheckerAPI];
    
    OCMVerify([mockChatAvailabilityCheckerObj cancel]);
    
    [mockChatAvailabilityCheckerObj stopMocking];
}

@end
