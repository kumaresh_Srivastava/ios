//
//  BTOrderDetailsMilestoneViewControllerTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/17/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock.h>
#import "BTOrderDetailsMilestoneViewController.h"
#import "DLMOrderDetailsScreen.h"
#import "AppManager.h"
#import "OmnitureManager.h"
#import "BTPricingDetails.h"
#import "BTCustomInputAlertView.h"
#import "CDEngineerAppointmentForOrder.h"
#import "AppDelegate.h"
#import "AppConstants.h"

@interface BTOrderDetailsMilestoneViewController (test)

- (void)fetchOrderDetailsFromAPI;
- (void)fetchCompleteOrderDetailsData;
- (void)fetchOrderMilestonesData;
- (void)fetchCompleteOrderMilestonesData;
- (void)hideLoadingItems:(BOOL)isHide;
- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow;
- (void)hideLoaderView;
- (void)updateVisibilityOfEngineerAppointmentDetailsButton;
- (void)fetchDataWithPostalCode:(NSString *)postalCode;
- (void)doValidationOnPostCode:(NSString *)postCode;
- (BOOL)checkForAlreadyAddedAppointmentAndDeleteForCurrentOrder;
- (void)checkChatAvailabilityAction;
- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message;
- (void)initialUISetupForOrderDetails;
- (void)createLoadingView;
- (void)createOrderSummaryView;
- (void)createViewFullDetailsView;
- (void)createTakeActionView;
- (void)createCompletedActionView;
- (void)createEmptyOrderSummaryAndMilestoneView;
- (void)hideTakeActionView;
- (void)trackOmniPage:(NSString *)page;
- (void)trackOmniClick:(NSString *)clickEvent forPage:(NSString *)page;
- (void)trackContentViewedForPage:(NSString *)page andNotificationPopupDetails:(NSString *)notificationPopup;
- (void)trackPageWithKeyTask:(NSString *)keytask;

@end

@interface BTOrderDetailsMilestoneViewControllerTests : XCTestCase

@property (nonatomic) BTOrderDetailsMilestoneViewController *orderDetailsAndMilestoneVC;

@end

@implementation BTOrderDetailsMilestoneViewControllerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    _orderDetailsAndMilestoneVC = [storyboard instantiateViewControllerWithIdentifier:@"DetailsMileStoneScene"];
    
    XCTAssertNotNil(_orderDetailsAndMilestoneVC.view, @"BTOrderDetailsMilestoneViewController instance is not created.");
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _orderDetailsAndMilestoneVC = nil;
}

- (void)testFetchOrderDetailsFromAPIWithNoConnection {
    
    id mockOrderDetailsMilestoneVCObj = [OCMockObject partialMockForObject:_orderDetailsAndMilestoneVC];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(NO);
    
    [_orderDetailsAndMilestoneVC fetchOrderDetailsFromAPI];
    
    OCMVerify([mockOrderDetailsMilestoneVCObj hideLoadingItems:YES]);
    OCMVerify([mockOrderDetailsMilestoneVCObj showRetryViewWithInternetStrip:YES]);
    OCMVerify([mockAppManagerObj trackNoInternetErrorOnPage:[OCMArg any]]);
    
    [mockOrderDetailsMilestoneVCObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testFetchOrderSummaryDetailsFromAPIWithInternetConnection {
    
    id mockOrderDetailsMilestoneVCObj = [OCMockObject partialMockForObject:_orderDetailsAndMilestoneVC];
    
    DLMOrderDetailsScreen *orderDetailsVM = [[DLMOrderDetailsScreen alloc] init];
    
    id mockDLMOrderDetailsObj = [OCMockObject partialMockForObject:orderDetailsVM];
    
    [_orderDetailsAndMilestoneVC setValue:mockDLMOrderDetailsObj forKey:@"orderDetailsViewModel"];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(YES);
    
    [_orderDetailsAndMilestoneVC fetchOrderDetailsFromAPI];
    
    OCMVerify([mockOrderDetailsMilestoneVCObj hideLoadingItems:NO]);
    OCMVerify([mockDLMOrderDetailsObj fetchOrderDetailsForOrderReference:[OCMArg any] andItemRef:[OCMArg any]]);
    
    [mockOrderDetailsMilestoneVCObj stopMocking];
    [mockDLMOrderDetailsObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testFetchCompleteOrderDetailsDataWithNoConnection {
    
    id mockOrderDetailsMilestoneVCObj = [OCMockObject partialMockForObject:_orderDetailsAndMilestoneVC];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(NO);
    
    [_orderDetailsAndMilestoneVC fetchCompleteOrderDetailsData];
    
    OCMVerify([mockOrderDetailsMilestoneVCObj hideLoaderView]);
    OCMVerify([mockOrderDetailsMilestoneVCObj showRetryViewWithInternetStrip:YES]);
    OCMVerify([mockAppManagerObj trackNoInternetErrorOnPage:[OCMArg any]]);
    
    [mockOrderDetailsMilestoneVCObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testFetchCompleteOrderDetailsDataWithInternetConnection {
    
    DLMOrderDetailsScreen *orderDetailsVM = [[DLMOrderDetailsScreen alloc] init];
    
    id mockDLMOrderDetailsObj = [OCMockObject partialMockForObject:orderDetailsVM];
    
    [_orderDetailsAndMilestoneVC setValue:mockDLMOrderDetailsObj forKey:@"orderDetailsViewModel"];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(YES);
    
    [_orderDetailsAndMilestoneVC fetchCompleteOrderDetailsData];
    
    OCMVerify([mockDLMOrderDetailsObj fetchCompleteOrderDetailsForOrderReference:OCMOCK_ANY ItemRef:OCMOCK_ANY postCode:OCMOCK_ANY andErrorCount:0]);
    
    [mockDLMOrderDetailsObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testFetchOrderMilestonesDataWithNoConnection {
    
    id mockOrderDetailsMilestoneVCObj = [OCMockObject partialMockForObject:_orderDetailsAndMilestoneVC];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(NO);
    
    [_orderDetailsAndMilestoneVC fetchOrderMilestonesData];
    
    OCMVerify([mockOrderDetailsMilestoneVCObj hideLoaderView]);
    OCMVerify([mockOrderDetailsMilestoneVCObj showRetryViewWithInternetStrip:YES]);
    OCMVerify([mockAppManagerObj trackNoInternetErrorOnPage:[OCMArg any]]);
    
    [mockOrderDetailsMilestoneVCObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testFetchOrderMilestonesDataWithInternetConnection {
    
    id mockOrderDetailsMilestoneVCObj = [OCMockObject partialMockForObject:_orderDetailsAndMilestoneVC];
    
    DLMOrderDetailsScreen *orderDetailsVM = [[DLMOrderDetailsScreen alloc] init];
    
    id mockDLMOrderDetailsObj = [OCMockObject partialMockForObject:orderDetailsVM];
    
    [_orderDetailsAndMilestoneVC setValue:mockDLMOrderDetailsObj forKey:@"orderDetailsViewModel"];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(YES);
    
    [_orderDetailsAndMilestoneVC fetchOrderMilestonesData];
    
    OCMVerify([mockDLMOrderDetailsObj fetchOrderMilestonesForOrderReference:OCMOCK_ANY andItemRef:OCMOCK_ANY]);
    OCMVerify([mockOrderDetailsMilestoneVCObj updateVisibilityOfEngineerAppointmentDetailsButton]);
    
    [mockOrderDetailsMilestoneVCObj stopMocking];
    [mockDLMOrderDetailsObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testFetchCompleteOrderMilestonesDataWithNoConnection {
    
    id mockOrderDetailsMilestoneVCObj = [OCMockObject partialMockForObject:_orderDetailsAndMilestoneVC];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(NO);
    
    [_orderDetailsAndMilestoneVC fetchCompleteOrderMilestonesData];
    
    OCMVerify([mockOrderDetailsMilestoneVCObj hideLoaderView]);
    OCMVerify([mockOrderDetailsMilestoneVCObj showRetryViewWithInternetStrip:YES]);
    OCMVerify([mockAppManagerObj trackNoInternetErrorOnPage:[OCMArg any]]);
    
    [mockOrderDetailsMilestoneVCObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testFetchCompleteOrderMilestonesDataWithInternetConnection {
    
    id mockOrderDetailsMilestoneVCObj = [OCMockObject partialMockForObject:_orderDetailsAndMilestoneVC];
    
    DLMOrderDetailsScreen *orderDetailsVM = [[DLMOrderDetailsScreen alloc] init];
    
    id mockDLMOrderDetailsObj = [OCMockObject partialMockForObject:orderDetailsVM];
    
    [_orderDetailsAndMilestoneVC setValue:mockDLMOrderDetailsObj forKey:@"orderDetailsViewModel"];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(YES);
    
    [_orderDetailsAndMilestoneVC fetchCompleteOrderMilestonesData];
    
    OCMVerify([mockDLMOrderDetailsObj fetchOrderMilestonesForOrderReference:OCMOCK_ANY ItemRef:OCMOCK_ANY postCode:OCMOCK_ANY andErrorCount:0]);
    OCMVerify([mockOrderDetailsMilestoneVCObj updateVisibilityOfEngineerAppointmentDetailsButton]);
    
    [mockOrderDetailsMilestoneVCObj stopMocking];
    [mockDLMOrderDetailsObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testFetchDataWithPostalCodeAndJourneyType2 {
    
    id mockOrderDetailsMilestoneVCObj = [OCMockObject partialMockForObject:_orderDetailsAndMilestoneVC];
    
    BTPricingDetails *mockPricingDetails = [[BTPricingDetails alloc] initPricingDetailsWithResponse:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:2], @"JourneyType", nil]];
    
    [_orderDetailsAndMilestoneVC setValue:mockPricingDetails forKeyPath:@"orderDetailsViewModel.pricingDetails"];
    
    [_orderDetailsAndMilestoneVC fetchDataWithPostalCode:@"AB101SA"];
    
    OCMVerify([mockOrderDetailsMilestoneVCObj fetchCompleteOrderDetailsData]);
    
    [mockOrderDetailsMilestoneVCObj stopMocking];
}

- (void)testFetchDataWithPostalCodeWithSelectedTab0 {
    
    id mockOrderDetailsMilestoneVCObj = [OCMockObject partialMockForObject:_orderDetailsAndMilestoneVC];
    
    [_orderDetailsAndMilestoneVC setValue:[NSNumber numberWithInteger:0] forKey:@"selectedTab"];
    
    [_orderDetailsAndMilestoneVC fetchDataWithPostalCode:@"AB101SA"];
    
    OCMVerify([mockOrderDetailsMilestoneVCObj fetchCompleteOrderDetailsData]);
    
    [mockOrderDetailsMilestoneVCObj stopMocking];
}

- (void)testFetchDataWithPostalCodeWithSelectedTab1 {
    
    id mockOrderDetailsMilestoneVCObj = [OCMockObject partialMockForObject:_orderDetailsAndMilestoneVC];
    
    [_orderDetailsAndMilestoneVC setValue:[NSNumber numberWithInteger:1] forKey:@"selectedTab"];
    
    [_orderDetailsAndMilestoneVC fetchDataWithPostalCode:@"AB101SA"];
    
    OCMVerify([mockOrderDetailsMilestoneVCObj fetchCompleteOrderMilestonesData]);
    
    [mockOrderDetailsMilestoneVCObj stopMocking];
}

- (void)testDoValidationOnPostCodeWithValidPostcodeWithSpace {
    
    id mockCustomAlertViewObj = OCMClassMock([BTCustomInputAlertView class]);
    
    id mockOrderDetailsMilestoneVCObj = [OCMockObject partialMockForObject:_orderDetailsAndMilestoneVC];
    
    [_orderDetailsAndMilestoneVC setValue:mockCustomAlertViewObj forKey:@"_customlAlertView"];
    
    NSString *validPostCode = @"AB10 1BA";
    [_orderDetailsAndMilestoneVC doValidationOnPostCode:validPostCode];
    
    OCMVerify([mockCustomAlertViewObj removeErrorMeesageFromCustomInputAlertView]);
    OCMVerify([mockOrderDetailsMilestoneVCObj fetchDataWithPostalCode:OCMOCK_ANY]);
    
    [mockCustomAlertViewObj stopMocking];
    [mockOrderDetailsMilestoneVCObj stopMocking];
}

- (void)testDoValidationOnPostCodeWithValidPostcodeWithoutSpace {
    
    id mockCustomAlertViewObj = OCMClassMock([BTCustomInputAlertView class]);
    
    id mockOrderDetailsMilestoneVCObj = [OCMockObject partialMockForObject:_orderDetailsAndMilestoneVC];
    
    [_orderDetailsAndMilestoneVC setValue:mockCustomAlertViewObj forKey:@"_customlAlertView"];
    
    NSString *validPostCode = @"AB101BA";
    [_orderDetailsAndMilestoneVC doValidationOnPostCode:validPostCode];
    
    OCMVerify([mockCustomAlertViewObj removeErrorMeesageFromCustomInputAlertView]);
    OCMVerify([mockOrderDetailsMilestoneVCObj fetchDataWithPostalCode:OCMOCK_ANY]);
    
    [mockCustomAlertViewObj stopMocking];
    [mockOrderDetailsMilestoneVCObj stopMocking];
}

- (void)testDoValidationOnPostCodeWithInvalidPostcode {
    
    id mockCustomAlertViewObj = OCMClassMock([BTCustomInputAlertView class]);
    
    [_orderDetailsAndMilestoneVC setValue:mockCustomAlertViewObj forKey:@"_customlAlertView"];
    
    NSString *invalidPostCode = @"POSTCODE";
    [_orderDetailsAndMilestoneVC doValidationOnPostCode:invalidPostCode];
    
    OCMVerify([mockCustomAlertViewObj showErrorOnCustomInputWithWithErrro:OCMOCK_ANY]);
    
    [mockCustomAlertViewObj stopMocking];
}

- (void)testCheckForAlreadyAddedAppointmentAndDeleteForCurrentOrderWithUnsavedItemRef {
    
    _orderDetailsAndMilestoneVC.itemRef = @"BT012341-1";
    
    BOOL isNotAlreadyAdded = [_orderDetailsAndMilestoneVC checkForAlreadyAddedAppointmentAndDeleteForCurrentOrder];
    
    XCTAssert(isNotAlreadyAdded==NO, @"Add Appointment to calender is not already added for current item ref.");
}

- (void)testCheckForAlreadyAddedAppointmentAndDeleteForCurrentOrderWithSavedItemRef {
    
    NSString *mockEventIdentifier = @"mockeventidentifier";
    NSString *mockItemRef = @"BT012341-1";
    
    CDEngineerAppointmentForOrder *newEngineerAppointmentForOrder = [CDEngineerAppointmentForOrder newEngineerAppointmentForOrderInManagedObjectContext:[[AppDelegate sharedInstance] managedObjectContext] withItemRef:mockItemRef andCalenderEventID:mockEventIdentifier];
    
    XCTAssertNotNil(newEngineerAppointmentForOrder, @"Engineer appointment for order data entry is failed in core data.");
    
    [[AppDelegate sharedInstance] saveContext];
    
    _orderDetailsAndMilestoneVC.itemRef = mockItemRef;
    
    BOOL isAlreadyAddedAndNotAbleToDelete = [_orderDetailsAndMilestoneVC checkForAlreadyAddedAppointmentAndDeleteForCurrentOrder];
    
    XCTAssert(isAlreadyAddedAndNotAbleToDelete == NO, @"Add Appointment to calender is already added for current item ref or not able to delete from event store.");
}

- (void)testCheckChatAvailabilityActionWithNoConnection {
    
    id mockOrderDetailsMilestoneVCObj = [OCMockObject partialMockForObject:_orderDetailsAndMilestoneVC];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(NO);
    
    [_orderDetailsAndMilestoneVC checkChatAvailabilityAction];
    
    OCMVerify([mockOrderDetailsMilestoneVCObj showAlertWithTitle:kNoConnectionTitle andMessage:kNoConnectionMessage]);
    
    [mockOrderDetailsMilestoneVCObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testCheckChatAvailabilityActionWithInternetConnection {
    
    id mockOrderDetailsMilestoneVCObj = [OCMockObject partialMockForObject:_orderDetailsAndMilestoneVC];
    
    DLMOrderDetailsScreen *orderDetailsVM = [[DLMOrderDetailsScreen alloc] init];
    
    id mockDLMOrderDetailsObj = [OCMockObject partialMockForObject:orderDetailsVM];
    
    [_orderDetailsAndMilestoneVC setValue:mockDLMOrderDetailsObj forKey:@"orderDetailsViewModel"];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(YES);
    
    [_orderDetailsAndMilestoneVC checkChatAvailabilityAction];
    
    OCMVerify([mockOrderDetailsMilestoneVCObj hideLoadingItems:NO]);
    OCMVerify([mockDLMOrderDetailsObj checkForLiveChatAvailibilityForOrder]);
    
    [mockOrderDetailsMilestoneVCObj stopMocking];
    [mockDLMOrderDetailsObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testInitialUISetupForOrderDetails {
    
    id mockOrderDetailsMilestoneVCObj = [OCMockObject partialMockForObject:_orderDetailsAndMilestoneVC];
    
    [_orderDetailsAndMilestoneVC initialUISetupForOrderDetails];
    
    OCMVerify([mockOrderDetailsMilestoneVCObj createLoadingView]);
    OCMVerify([mockOrderDetailsMilestoneVCObj createOrderSummaryView]);
    OCMVerify([mockOrderDetailsMilestoneVCObj createViewFullDetailsView]);
    OCMVerify([mockOrderDetailsMilestoneVCObj createTakeActionView]);
    OCMVerify([mockOrderDetailsMilestoneVCObj createCompletedActionView]);
    OCMVerify([mockOrderDetailsMilestoneVCObj createEmptyOrderSummaryAndMilestoneView]);
    OCMVerify([mockOrderDetailsMilestoneVCObj hideTakeActionView]);
    
    [mockOrderDetailsMilestoneVCObj stopMocking];
}

- (void)testTrackOmniPage {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [_orderDetailsAndMilestoneVC trackOmniPage:OMNIPAGE_ORDER_DETAILS];
    
    OCMVerify([mockOmnitureManagerObj trackPage:[OCMArg any] withContextInfo:[OCMArg any]]);
    
    [mockOmnitureManagerObj stopMocking];
}

- (void)testTrackOmniClickForPage {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [_orderDetailsAndMilestoneVC trackOmniClick:OMNICLICK_ORDER_VIEW_FULL_DETAILS forPage:OMNIPAGE_ORDER_DETAILS];
    
    OCMVerify([mockOmnitureManagerObj trackClick:[OCMArg any] withContextInfo:[OCMArg any]]);
    
    [mockOmnitureManagerObj stopMocking];
}

- (void)testTrackContentViewedForPageAndNotificationPopupDetails {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [_orderDetailsAndMilestoneVC trackContentViewedForPage:OMNIPAGE_ORDER_DETAILS andNotificationPopupDetails:OMNINOTIFY_ORDER_POSTCODE_VIEWDETAILS];
    
    OCMVerify([mockOmnitureManagerObj trackPage:[OCMArg any] withContextInfo:[OCMArg any]]);
    
    [mockOmnitureManagerObj stopMocking];
}

- (void)testTrackPageWithKeyTask {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [_orderDetailsAndMilestoneVC trackPageWithKeyTask:OMNI_KEYTASK_ORDER_TRACKER_APPOINTMENT_ADDED_TO_CALENDAR];
    
    OCMVerify([mockOmnitureManagerObj trackPage:[OCMArg any] withContextInfo:[OCMArg any]]);
    
    [mockOmnitureManagerObj stopMocking];
}

@end
