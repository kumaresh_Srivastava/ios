//
//  BTOrderTrackerBundleSummaryViewControllerTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/16/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock.h>
#import "BTOrderTrackerBundleSummaryViewController.h"
#import "DLMBundleOrderSummaryScreen.h"
#import "AppManager.h"
#import "OmnitureManager.h"
#import "AppConstants.h"

@interface BTOrderTrackerBundleSummaryViewController (test)

- (void)callGetBundleOrderSummaryAPI;
- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow;
- (void)hideLoadingItems:(BOOL)isHide;
- (void)hideRetryItems:(BOOL)isHide;
- (void)initialSetupForBundleOrderSummaryScreen;
- (void)createLoadingView;
- (void)hideorShowControls:(BOOL)isHide;
- (void)updateUserInterface;
- (NSAttributedString *)billingDetailsStringForOneOff:(double)oneOff andRegular:(double)regular;
- (void)checkChatAvailabilityAction;
- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message;
- (void)trackOmniPage:(NSString *)page;

@end

@interface BTOrderTrackerBundleSummaryViewControllerTests : XCTestCase

@property (nonatomic) BTOrderTrackerBundleSummaryViewController *bundleSummaryVC;

@end

@implementation BTOrderTrackerBundleSummaryViewControllerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    _bundleSummaryVC = [storyboard instantiateViewControllerWithIdentifier:@"BundleOrderSummaryScene"];
    
    XCTAssertNotNil(_bundleSummaryVC.view);
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _bundleSummaryVC = nil;
}

- (void)testCallGetBundleOrderSummaryAPIWithNoConnection {
    
    id mockBundleOrderVCObj = [OCMockObject partialMockForObject:_bundleSummaryVC];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(NO);
    
    [_bundleSummaryVC callGetBundleOrderSummaryAPI];
    
    OCMVerify([mockBundleOrderVCObj showRetryViewWithInternetStrip:YES]);
    OCMVerify([mockBundleOrderVCObj hideLoadingItems:YES]);
    OCMVerify([mockAppManagerObj trackNoInternetErrorOnPage:OMNIPAGE_ORDER_BUNDLE_ORDER_SUMMARY]);
    
    [mockBundleOrderVCObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testCallGetBundleOrderSummaryAPIWithInternetConnection {
    
    id mockBundleOrderVCObj = [OCMockObject partialMockForObject:_bundleSummaryVC];
    
    DLMBundleOrderSummaryScreen *bundleOrderSummaryVM = [[DLMBundleOrderSummaryScreen alloc] init];
    
    id mockDLMBundleOrderSummaryObj = [OCMockObject partialMockForObject:bundleOrderSummaryVM];
    
    [mockBundleOrderVCObj setValue:mockDLMBundleOrderSummaryObj forKey:@"viewModel"];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(YES);
    
    [_bundleSummaryVC callGetBundleOrderSummaryAPI];
    
    OCMVerify([mockBundleOrderVCObj hideLoadingItems:NO]);
    OCMVerify([mockBundleOrderVCObj hideRetryItems:YES]);
    OCMVerify([mockDLMBundleOrderSummaryObj fetchBundleOrderSummaryForBundleOrderReference:[OCMArg any] andItemRef:[OCMArg any]]);
    
    [mockBundleOrderVCObj stopMocking];
    [mockDLMBundleOrderSummaryObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testInitialSetupForBundleOrderSummaryScreen {
    
    id mockBundleOrderVCObj = [OCMockObject partialMockForObject:_bundleSummaryVC];
    
    [_bundleSummaryVC initialSetupForBundleOrderSummaryScreen];
    
    OCMVerify([mockBundleOrderVCObj createLoadingView]);
    OCMVerify([mockBundleOrderVCObj hideorShowControls:YES]);
    
    [mockBundleOrderVCObj stopMocking];
              
}

- (void)testUpdateUserInterface {
    
    id mockBundleOrderVCObj = [OCMockObject partialMockForObject:_bundleSummaryVC];
    
    [_bundleSummaryVC updateUserInterface];
    
    OCMVerify([mockBundleOrderVCObj hideorShowControls:NO]);
    OCMVerify([mockBundleOrderVCObj hideRetryItems:YES]);
    OCMVerify([mockBundleOrderVCObj hideLoadingItems:YES]);
    
    [mockBundleOrderVCObj stopMocking];
}

- (void)testBillingDetailsStringForOneOffAndRegular {
    
    double oneOff = 1.23;
    double regular = 3.21;
    
    NSString *oneOffString = [NSString stringWithFormat:@"£%.2f", oneOff];
    NSString *regularString = [NSString stringWithFormat:@"£%.2f", regular];
    
    NSString *expectedString = [NSString stringWithFormat:@"%@ /month  %@ one-off", regularString, oneOffString];
    
    NSAttributedString *returnedAttributedString = [_bundleSummaryVC billingDetailsStringForOneOff:oneOff andRegular:regular];
    NSString *returnedString = [returnedAttributedString string];
    
    XCTAssertTrue([returnedString isEqualToString:expectedString]);
}

- (void)testCheckChatAvailabilityActionWithNoConnection {
    
    id mockBundleOrderVCObj = [OCMockObject partialMockForObject:_bundleSummaryVC];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(NO);
    
    [_bundleSummaryVC checkChatAvailabilityAction];
    
    OCMVerify([mockBundleOrderVCObj showAlertWithTitle:kNoConnectionTitle andMessage:kNoConnectionMessage]);
    
    [mockBundleOrderVCObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testCheckChatAvailabilityActionWithInternetConnection {
    
    id mockBundleOrderVCObj = [OCMockObject partialMockForObject:_bundleSummaryVC];
    
    DLMBundleOrderSummaryScreen *bundleOrderSummaryVM = [[DLMBundleOrderSummaryScreen alloc] init];
    
    id mockDLMBundleOrderSummaryObj = [OCMockObject partialMockForObject:bundleOrderSummaryVM];
    
    [_bundleSummaryVC setValue:mockDLMBundleOrderSummaryObj forKey:@"viewModel"];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(YES);
    
    [_bundleSummaryVC checkChatAvailabilityAction];
    
    OCMVerify([mockBundleOrderVCObj hideLoadingItems:NO]);
    OCMVerify([mockDLMBundleOrderSummaryObj checkForLiveChatAvailibilityForOrder]);
    
    [mockBundleOrderVCObj stopMocking];
    [mockDLMBundleOrderSummaryObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testTrackOmniPage {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [_bundleSummaryVC trackOmniPage:OMNIPAGE_ORDER_BUNDLE_ORDER_SUMMARY];
    
    OCMVerify([mockOmnitureManagerObj trackPage:[OCMArg any] withContextInfo:[OCMArg any]]);
    
    [mockOmnitureManagerObj stopMocking];
}

@end
