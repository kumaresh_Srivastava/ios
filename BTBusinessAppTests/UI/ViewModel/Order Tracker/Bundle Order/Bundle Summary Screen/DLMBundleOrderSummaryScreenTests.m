//
//  DLMBundleOrderSummaryScreenTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/16/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock.h>
#import "DLMBundleOrderSummaryScreen.h"
#import "NLGetBundleOrderSummaryWebService.h"
#import "BTLiveChatAvailabilityChecker.h"

@interface DLMBundleOrderSummaryScreen (test)

- (void)setupToFetchBundleOrderSummaryForBundleOrderRef:(NSString *)bundleOrderRef andItemRef:(NSString *)itemRef;
- (void)fetchBundleOrderSummaryForCurrentBundleOrder;
- (void)setupToCheckLiveChatAvailability;
- (void)getAvailableSlotsForChat;

@end

@interface DLMBundleOrderSummaryScreenTests : XCTestCase

@property (nonatomic) DLMBundleOrderSummaryScreen *bundleOrderSummaryVM;

@end

@implementation DLMBundleOrderSummaryScreenTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    _bundleOrderSummaryVM = [[DLMBundleOrderSummaryScreen alloc] init];
    
    XCTAssertNotNil(_bundleOrderSummaryVM, @"DLMBundleOrderSummaryScreen instance is not created inside the init method.");
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _bundleOrderSummaryVM = nil;
}

- (void)testFetchBundleOrderSummaryForBundleOrderRefAndItemRef {
    
    id mockDLMBundleOrderSummaryObj = [OCMockObject partialMockForObject:_bundleOrderSummaryVM];
    
    NSString *mockOrderRef = @"BT012345";
    NSString *mockItemRef = @"BT012345-1";
    [_bundleOrderSummaryVM fetchBundleOrderSummaryForBundleOrderReference:mockOrderRef andItemRef:mockItemRef];
    
    OCMVerify([mockDLMBundleOrderSummaryObj setupToFetchBundleOrderSummaryForBundleOrderRef:mockOrderRef andItemRef:mockItemRef]);
    OCMVerify([mockDLMBundleOrderSummaryObj fetchBundleOrderSummaryForCurrentBundleOrder]);
    
    [mockDLMBundleOrderSummaryObj stopMocking];
}

- (void)testSetupToFetchBundleOrderSummaryWithOrderRefAndItemRef {
    
    NSString *mockOrderRef = @"BT012345";
    NSString *mockItemRef = @"BT012345-1";
    [_bundleOrderSummaryVM setupToFetchBundleOrderSummaryForBundleOrderRef:mockOrderRef andItemRef:mockItemRef];
    
    XCTAssertNotNil([_bundleOrderSummaryVM valueForKey:@"getBundleOrderSummaryWebService"], @"NLGetBundleOrderSummaryWebService instance is not created inside method.");
}

- (void)testFetchOrderSummaryForCurrentOrder {
    
    id mockBundleOrderSummaryWSObj = OCMClassMock([NLGetBundleOrderSummaryWebService class]);
    
    [_bundleOrderSummaryVM setValue:mockBundleOrderSummaryWSObj forKey:@"getBundleOrderSummaryWebService"];
    
    [_bundleOrderSummaryVM fetchBundleOrderSummaryForCurrentBundleOrder];
    
    OCMVerify([mockBundleOrderSummaryWSObj resume]);
    
    [mockBundleOrderSummaryWSObj stopMocking];
}

- (void)testCheckForLiveChatAvailibilityForOrder
{
    id mockDLMBundleOrderSummaryObj = [OCMockObject partialMockForObject:_bundleOrderSummaryVM];
    
    [_bundleOrderSummaryVM checkForLiveChatAvailibilityForOrder];
    
    OCMVerify([mockDLMBundleOrderSummaryObj setupToCheckLiveChatAvailability]);
    OCMVerify([mockDLMBundleOrderSummaryObj getAvailableSlotsForChat]);
    
    [mockDLMBundleOrderSummaryObj stopMocking];
}

- (void)testSetupToCheckLiveChatAvailability
{
    [_bundleOrderSummaryVM setupToCheckLiveChatAvailability];
    
    XCTAssertNotNil([_bundleOrderSummaryVM valueForKey:@"_liveChatAvailabityChecker"], @"BTLiveChatAvailabilityChecker instance is not created inside method.");
}

- (void)testGetAvailableSlotsForChat
{
    id mockChatAvailabilityCheckerObj = OCMClassMock([BTLiveChatAvailabilityChecker class]);
    
    [_bundleOrderSummaryVM setValue:mockChatAvailabilityCheckerObj forKey:@"_liveChatAvailabityChecker"];
    
    [_bundleOrderSummaryVM getAvailableSlotsForChat];
    
    OCMVerify([mockChatAvailabilityCheckerObj getLiveChatAvailableSlots]);
    
    [mockChatAvailabilityCheckerObj stopMocking];
}

- (void)testCancelChatAvailabilityCheckerAPI
{
    id mockChatAvailabilityCheckerObj = OCMClassMock([BTLiveChatAvailabilityChecker class]);
    
    [_bundleOrderSummaryVM setValue:mockChatAvailabilityCheckerObj forKey:@"_liveChatAvailabityChecker"];
    
    [_bundleOrderSummaryVM cancelChatAvailabilityCheckerAPI];
    
    OCMVerify([mockChatAvailabilityCheckerObj cancel]);
    
    [mockChatAvailabilityCheckerObj stopMocking];
}

@end
