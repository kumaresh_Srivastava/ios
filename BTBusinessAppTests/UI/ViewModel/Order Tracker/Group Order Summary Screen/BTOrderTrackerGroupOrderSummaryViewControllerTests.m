//
//  BTOrderTrackerGroupOrderSummaryViewControllerTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/11/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock.h>
#import "BTOrderTrackerGroupOrderSummaryViewController.h"
#import "AppConstants.h"
#import "OmnitureManager.h"
#import "DLMGroupOrderSummaryScreen.h"
#import "AppManager.h"

@interface BTOrderTrackerGroupOrderSummaryViewController (test)

- (void)initialSetupForGroupOrderSummaryScreen;
- (void)createLoadingView;
- (void)hideorShowControls:(BOOL)isHide;
- (void)trackOmniPage;
- (void)trackOmniClick:(NSString *)clickEvent forPage:(NSString *)page;
- (void)fetchGroupOrderSummaryDetails;
- (void)updateUserInterface;
- (void)hideRetryItems:(BOOL)isHide;
- (void)hideLoadingItems:(BOOL)isHide;
- (void)showRetryViewWithInernetStrip:(BOOL)needToShowInternetStrip;

@end

@interface BTOrderTrackerGroupOrderSummaryViewControllerTests : XCTestCase

@property (nonatomic) BTOrderTrackerGroupOrderSummaryViewController *groupOrderSummaryViewController;

@end

@implementation BTOrderTrackerGroupOrderSummaryViewControllerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];
    
    _groupOrderSummaryViewController = [storyboard instantiateViewControllerWithIdentifier:@"OrderTrackerGroupOrderSummaryViewController"];
    
    XCTAssertNotNil(_groupOrderSummaryViewController.view);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _groupOrderSummaryViewController = nil;
}

- (void)testInitialSetupForGroupOrderSummaryScreen {
    
    id mockGroupOrderVCObj = [OCMockObject partialMockForObject:_groupOrderSummaryViewController];
    
    [_groupOrderSummaryViewController initialSetupForGroupOrderSummaryScreen];
    
    OCMVerify([mockGroupOrderVCObj createLoadingView]);
    OCMVerify([mockGroupOrderVCObj hideorShowControls:YES]);
    
    [mockGroupOrderVCObj stopMocking];
}

- (void)testTrackOmniPage {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [_groupOrderSummaryViewController trackOmniPage];
    
    OCMVerify([mockOmnitureManagerObj trackPage:OMNIPAGE_ORDER_GROUP_ORDER withContextInfo:[OCMArg any]]);
    
    [mockOmnitureManagerObj stopMocking];
}

- (void)testTrackOmniClickWithPageName {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [_groupOrderSummaryViewController trackOmniClick:@"dummyLink" forPage:OMNIPAGE_ORDER_GROUP_ORDER];
    
    OCMVerify([mockOmnitureManagerObj trackClick:[OCMArg any] withContextInfo:[OCMArg any]]);
    
    [mockOmnitureManagerObj stopMocking];
}

- (void)testFetchGroupOrderSummaryDetailsWithNoConnection {
    
    id mockGroupOrderVCObj = [OCMockObject partialMockForObject:_groupOrderSummaryViewController];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(NO);
    
    [_groupOrderSummaryViewController fetchGroupOrderSummaryDetails];
    
    OCMVerify([mockGroupOrderVCObj showRetryViewWithInernetStrip:YES]);
    OCMVerify([mockAppManagerObj trackNoInternetErrorOnPage:OMNIPAGE_ORDER_GROUP_ORDER]);
    
    [mockGroupOrderVCObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testFetchGroupOrderSummaryDetailsWithInternetConnection {
    
    id mockGroupOrderVCObj = [OCMockObject partialMockForObject:_groupOrderSummaryViewController];
    
    DLMGroupOrderSummaryScreen *groupOrderSummaryVM = [[DLMGroupOrderSummaryScreen alloc] init];
    
    id mockDLMGroupOrderSummaryObj = [OCMockObject partialMockForObject:groupOrderSummaryVM];
    
    [mockGroupOrderVCObj setValue:mockDLMGroupOrderSummaryObj forKey:@"viewModel"];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(YES);
    
    [_groupOrderSummaryViewController fetchGroupOrderSummaryDetails];
    
    OCMVerify([mockGroupOrderVCObj hideLoadingItems:NO]);
    OCMVerify([mockGroupOrderVCObj hideRetryItems:YES]);
    OCMVerify([mockDLMGroupOrderSummaryObj fetchGroupOrderSummaryForOrderReference:[OCMArg any]]);
    
    [mockGroupOrderVCObj stopMocking];
    [mockDLMGroupOrderSummaryObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testUpdateUserInterface {
    
    id mockGroupOrderVCObj = [OCMockObject partialMockForObject:_groupOrderSummaryViewController];
    
    [_groupOrderSummaryViewController updateUserInterface];
    
    OCMVerify([mockGroupOrderVCObj hideorShowControls:NO]);
    OCMVerify([mockGroupOrderVCObj hideRetryItems:YES]);
    OCMVerify([mockGroupOrderVCObj hideLoadingItems:YES]);
    
    [mockGroupOrderVCObj stopMocking];
}

@end
