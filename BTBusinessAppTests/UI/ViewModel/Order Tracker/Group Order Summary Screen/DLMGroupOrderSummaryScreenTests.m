//
//  DLMGroupOrderSummaryScreenTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/16/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock.h>
#import "DLMGroupOrderSummaryScreen.h"
#import "NLGetGroupOrderSummaryWebService.h"

@interface DLMGroupOrderSummaryScreen (test)

- (void)setupToFetchGroupOrderSummaryForOrderRef:(NSString *)groupOrderRef;
- (void)fetchGroupOrderSummaryForCurrentGroupOrder;

@end

@interface DLMGroupOrderSummaryScreenTests : XCTestCase

@property (nonatomic) DLMGroupOrderSummaryScreen *groupOrderSummaryVM;

@end

@implementation DLMGroupOrderSummaryScreenTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    _groupOrderSummaryVM = [[DLMGroupOrderSummaryScreen alloc] init];
    
    XCTAssertNotNil(_groupOrderSummaryVM, @"DLMGroupOrderSummaryScreen instance is not created inside init method.");
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _groupOrderSummaryVM = nil;
}

- (void)testSetupToFetchGroupOrderSummaryForOrderRef {
    
    [_groupOrderSummaryVM setupToFetchGroupOrderSummaryForOrderRef:@"GBT01234"];
    
    XCTAssertNotNil([_groupOrderSummaryVM valueForKey:@"getGroupOrderSummaryWebService"], @"NLGetGroupOrderSummaryWebService is not created inside method.");
}

- (void)testFetchGroupOrderSummaryForCurrentGroupOrder {
    
    id mockGroupOrderSummaryWS = OCMClassMock([NLGetGroupOrderSummaryWebService class]);
    
    [_groupOrderSummaryVM setValue:mockGroupOrderSummaryWS forKey:@"getGroupOrderSummaryWebService"];
    
    [_groupOrderSummaryVM fetchGroupOrderSummaryForCurrentGroupOrder];
    
    OCMVerify([mockGroupOrderSummaryWS resume]);
    
    [mockGroupOrderSummaryWS stopMocking];
}

- (void)testFetchGroupOrderSummaryForOrderReference {
    
    id mockDLMGroupOrderSummaryObj = [OCMockObject partialMockForObject:_groupOrderSummaryVM];
    
    NSString *mockGroupOrderRef = @"GBT01234";
    [_groupOrderSummaryVM fetchGroupOrderSummaryForOrderReference:mockGroupOrderRef];
    
    OCMVerify([mockDLMGroupOrderSummaryObj setupToFetchGroupOrderSummaryForOrderRef:mockGroupOrderRef]);
    OCMVerify([mockDLMGroupOrderSummaryObj fetchGroupOrderSummaryForCurrentGroupOrder]);
    
    [mockDLMGroupOrderSummaryObj stopMocking];
}

@end
