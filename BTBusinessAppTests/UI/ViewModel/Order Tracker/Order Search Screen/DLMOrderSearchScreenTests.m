//
//  DLMOrderSearchScreenTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/9/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DLMOrderSearchScreen.h"

@interface DLMOrderSearchScreenTests : XCTestCase <DLMOrderSearchScreenDelegate> {
    
    XCTestExpectation *_fetchDataExpectation;
}

@property (nonatomic) DLMOrderSearchScreen *orderSearchViewModel;

@end

@implementation DLMOrderSearchScreenTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    _orderSearchViewModel = [[DLMOrderSearchScreen alloc] init];
    _orderSearchViewModel.orderSearchScreenDelegate = self;
    
    XCTAssertNotNil(_orderSearchViewModel, @"DLMOrderSearchScreen instance is not created.");
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _orderSearchViewModel.orderSearchScreenDelegate = nil;
    _orderSearchViewModel = nil;
}

- (void)testFetchRecentSearchedOrders {
    
    _fetchDataExpectation = [self expectationWithDescription:@"Recent searched data fetched"];
    
    [_orderSearchViewModel fetchRecentSearchedOrders];
    
    [self waitForExpectationsWithTimeout:1 handler:^(NSError * _Nullable error) {
        if (error)
        {
            NSLog(@"Expectation \"%@\" is failed.", [_fetchDataExpectation debugDescription]);
        }
    }];
    
    XCTAssertNotNil(_orderSearchViewModel.arrayOfRecentSearchedOrders, @"Array for recent searched orders is not created");
}

- (void)successfullyFetchedRecentSearchedOrderDataOnOrderSearchScreen:(DLMOrderSearchScreen *)orderSearchScreen
{
    [_fetchDataExpectation fulfill];
}

@end
