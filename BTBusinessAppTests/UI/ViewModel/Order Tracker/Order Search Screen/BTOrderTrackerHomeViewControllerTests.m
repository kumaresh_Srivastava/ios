//
//  BTOrderTrackerHomeViewControllerTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/9/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock.h>
#import "BTOrderTrackerHomeViewController.h"
#import "AppConstants.h"
#import "OmnitureManager.h"

@interface BTOrderTrackerHomeViewController (test)

- (NSString *)validateOrderKey:(NSString *)orderID;
- (BOOL)regexMatchesInString:(NSString*)sourceString withComparableString:(NSString *)comparableString;
- (void)trackOmniPage:(NSString *)page;
- (void)trackContentViewedForPage:(NSString *)page andNotificationPopupDetails:(NSString *)notificationPopup;

@end

@interface BTOrderTrackerHomeViewControllerTests : XCTestCase

@property (nonatomic) BTOrderTrackerHomeViewController *orderTrackerHomeVC;

@end

@implementation BTOrderTrackerHomeViewControllerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];
    
    _orderTrackerHomeVC = (BTOrderTrackerHomeViewController *)[storyboard instantiateViewControllerWithIdentifier:@"OrderTrackerInputViewController"];
    
    XCTAssertNotNil(_orderTrackerHomeVC.view);
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testValidateOrderKeyWithInvalidOrderID {
    
    NSString *mockString = nil;
    NSString *expectedString = [_orderTrackerHomeVC validateOrderKey:mockString];
    
    BOOL isInvalidOrder = ![[_orderTrackerHomeVC valueForKey:@"_isValidOrder"] boolValue];
    XCTAssertTrue(isInvalidOrder, @"It is an invalid order");
    XCTAssertNil(expectedString, @"Returned string should be nil.");
}

- (void)testValidateOrderKeyWithValidOrderID {
    
    NSString *mockString = @"BT012345";
    NSString *expectedString = [_orderTrackerHomeVC validateOrderKey:mockString];
    
    XCTAssertTrue([[_orderTrackerHomeVC valueForKey:@"_isValidOrder"] boolValue], @"It is a valid order");
    XCTAssertTrue([expectedString isEqualToString:mockString], @"Returned order ref is not correct or changed.");
}

- (void)testValidateOrderKeyWithValidGroupOrderID {
    
    NSString *mockString = @"GBT01234";
    NSString *expectedString = [_orderTrackerHomeVC validateOrderKey:mockString];
    
    XCTAssertTrue([[_orderTrackerHomeVC valueForKey:@"_isValidOrder"] boolValue], @"It is a valid order");
    XCTAssertTrue([[_orderTrackerHomeVC valueForKey:@"_isGroupOrder"] boolValue], @"It is a valid group order");
    XCTAssertTrue([expectedString isEqualToString:mockString], @"Returned order ref is not correct or changed.");
}

 - (void)testRegexMatchesWithNilSourceStringAndComparableString {
    
    NSString *nilSourceString = nil;
    NSString *comparableString = @"^[Bb][Tt][A-Za-z0-9]{6,6}$";
    
    XCTAssertFalse([_orderTrackerHomeVC regexMatchesInString:nilSourceString withComparableString:comparableString], @"Given order reference is not valid.");
}

- (void)testRegexMatchesWithEmptySourceStringAndComparableString {
    
    NSString *emptySourceString = @"";
    NSString *comparableString = @"^[Bb][Tt][A-Za-z0-9]{6,6}$";
    
    XCTAssertFalse([_orderTrackerHomeVC regexMatchesInString:emptySourceString withComparableString:comparableString], @"Given order reference is not valid.");
}

- (void)testRegexMatchesWithValidSourceStringAndComparableString {
    
    NSString *sourceString = @"BT012345";
    NSString *comparableString = @"^[Bb][Tt][A-Za-z0-9]{6,6}$";
    
    XCTAssertTrue([_orderTrackerHomeVC regexMatchesInString:sourceString withComparableString:comparableString], @"Given order reference is valid.");
}

- (void)testTrackOmniPage {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [_orderTrackerHomeVC trackOmniPage:OMNIPAGE_ORDER_SEARCH];
    
    OCMVerify([mockOmnitureManagerObj trackPage:[OCMArg any] withContextInfo:[OCMArg any]]);
    
    [mockOmnitureManagerObj stopMocking];
}

- (void)testTrackContentViewedForPageWithNotificationPopupDetails {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [_orderTrackerHomeVC trackContentViewedForPage:OMNIPAGE_ORDER_SEARCH andNotificationPopupDetails:@"Incorrect Order Reference"];
    
    OCMVerify([mockOmnitureManagerObj trackPage:[OCMArg any] withContextInfo:[OCMArg any]]);
    
    [mockOmnitureManagerObj stopMocking];
}

@end
