//
//  BTOrderTrackerDashboardViewControllerTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/8/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BTOrderTrackerDashboardViewController.h"
#import "AppConstants.h"
#import <OCMock/OCMock.h>
#import "BTCug.h"
#import "DLMTrackOrderDashBoardScreen.h"
#import "AppManager.h"
#import "BTOrderDashBoardTableFooterView.h"
#import "OmnitureManager.h"

@interface BTOrderTrackerDashboardViewController (test)

- (void)updateGroupSelection;
- (void)hideGroupSelectionForOneAvailableGroup;
- (void)setCurrentGroupKey:(NSString *)currentGroupKey;
- (void)checkForSuperUserAndUpdateUIWithIndex:(NSUInteger)index;
- (void)resetDataAndUIAfterGroupChange;
- (void)resetUIAndMakeAPIRequest;
- (void)hideRetryItems:(BOOL)isHide;
- (void)hideEmmptyDashBoardItems:(BOOL)isHide;
- (void)cancelOpenOrderAPI;
- (void)cancelCompletedOrderAPI;
- (void)fetchOrderDashBoardDetailsWithPageIndex:(int)pageIndex;
- (void)loadNextOpenOrders;
- (void)createFooterView;
- (void)hideShowFooterIndicatorView:(BOOL)isHide;
- (void)fetchOpenOrdersAutomatically;
- (void)fetchCompletedOrdersAutomatically;
- (void)trackOmniPage:(NSString *)page;
- (void)trackOmniClick:(NSString *)clickEvent forPage:(NSString *)page;
- (void)trackOmnitureError:(NSString *)error;
- (NSString *)getPageNameForCurrentGroup;

@end

@interface BTOrderTrackerDashboardViewControllerTests : XCTestCase

@property (nonatomic) BTOrderTrackerDashboardViewController *orderTrackerDashboardVC;

@end

@implementation BTOrderTrackerDashboardViewControllerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];
    
    _orderTrackerDashboardVC = (BTOrderTrackerDashboardViewController *)[storyboard instantiateViewControllerWithIdentifier:@"orderDashboardScene"];
    
    XCTAssertNotNil(_orderTrackerDashboardVC.view);
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _orderTrackerDashboardVC = nil;
}

- (void)testUpdateGroupSelectionWithSingleGroup {
    
    id mockOrderDashboardVCObj = [OCMockObject partialMockForObject:_orderTrackerDashboardVC];
    
    BTCug *cug1 = [[BTCug alloc] initWithCugName:@"cug1" groupKey:@"group1" cugRole:1 indexInAPIResponse:1 andRefKey:@"refkey1"];
    
    NSArray *mockArray = [NSArray arrayWithObjects:cug1, nil];
    _orderTrackerDashboardVC.cugs = mockArray;
    
    [_orderTrackerDashboardVC updateGroupSelection];
    
    OCMVerify([mockOrderDashboardVCObj hideGroupSelectionForOneAvailableGroup]);
    
    [mockOrderDashboardVCObj stopMocking];
}

- (void)testUpdateGroupSelectionWithMultipleGroup {
    
    id mockOrderDashboardVCObj = [OCMockObject partialMockForObject:_orderTrackerDashboardVC];
    
    BTCug *cug1 = [[BTCug alloc] initWithCugName:@"cug1" groupKey:@"group1" cugRole:1 indexInAPIResponse:1 andRefKey:@"refkey1"];
    BTCug *cug2 = [[BTCug alloc] initWithCugName:@"cug2" groupKey:@"group2" cugRole:1 indexInAPIResponse:2 andRefKey:@"refkey2"];
    
    NSArray *mockArray = [NSArray arrayWithObjects:cug1, cug2, nil];
    _orderTrackerDashboardVC.cugs = mockArray;
    
    [_orderTrackerDashboardVC updateGroupSelection];
    
    OCMVerify([mockOrderDashboardVCObj setCurrentGroupKey:[OCMArg any]]);
    
    [mockOrderDashboardVCObj stopMocking];
}

- (void)testCheckForSuperUserAndUpdateUIWithIndex {
    
    id mockOrderDashboardVCObj = [OCMockObject partialMockForObject:_orderTrackerDashboardVC];
    id mockDLMOpenOrderObj = OCMClassMock([DLMTrackOrderDashBoardScreen class]);
    
    BTCug *cug1 = [[BTCug alloc] initWithCugName:@"cug1" groupKey:@"group1" cugRole:1 indexInAPIResponse:1 andRefKey:@"refkey1"];
    BTCug *cug2 = [[BTCug alloc] initWithCugName:@"cug2" groupKey:@"group2" cugRole:1 indexInAPIResponse:2 andRefKey:@"refkey2"];
    
    NSArray *mockArray = [NSArray arrayWithObjects:cug1, cug2, nil];
    _orderTrackerDashboardVC.cugs = mockArray;
    
    [_orderTrackerDashboardVC setValue:@"group3" forKey:@"currentGroupKey"];
    [_orderTrackerDashboardVC setValue:mockDLMOpenOrderObj forKey:@"openOrderViewModel"];
    
    [_orderTrackerDashboardVC checkForSuperUserAndUpdateUIWithIndex:1];
    
    [mockDLMOpenOrderObj changeSelectedCUGTo:[OCMArg any]];
    [mockOrderDashboardVCObj setCurrentGroupKey:[OCMArg any]];
    [mockOrderDashboardVCObj resetDataAndUIAfterGroupChange];
    
    [mockOrderDashboardVCObj stopMocking];
    [mockDLMOpenOrderObj stopMocking];
}

- (void)testResetUIAndMakeAPIRequestWithRetryViewShown {
    
    id mockOrderDashboardVCObj = [OCMockObject partialMockForObject:_orderTrackerDashboardVC];
    
    [_orderTrackerDashboardVC setValue:[NSNumber numberWithBool:YES] forKey:@"_isRetryShown"];
    
    [_orderTrackerDashboardVC resetUIAndMakeAPIRequest];
    
    OCMVerify([mockOrderDashboardVCObj hideRetryItems:YES]);
    
    [mockOrderDashboardVCObj stopMocking];
}

- (void)testResetUIAndMakeAPIRequestWithEmptyViewShown {
    
    id mockOrderDashboardVCObj = [OCMockObject partialMockForObject:_orderTrackerDashboardVC];
    
    [_orderTrackerDashboardVC setValue:[NSNumber numberWithBool:YES] forKey:@"_isEmptyViewShown"];
    
    [_orderTrackerDashboardVC resetUIAndMakeAPIRequest];
    
    OCMVerify([mockOrderDashboardVCObj hideEmmptyDashBoardItems:YES]);
    
    [mockOrderDashboardVCObj stopMocking];
}

- (void)testResetUIAndMakeAPIRequestWithSegmenetedIndexZero {
    
    id mockOrderDashboardVCObj = [OCMockObject partialMockForObject:_orderTrackerDashboardVC];
    
    [_orderTrackerDashboardVC setValue:[NSNumber numberWithInteger:0] forKeyPath:@"ordersSegmentedControl.selectedSegmentIndex"];
    
    [_orderTrackerDashboardVC resetUIAndMakeAPIRequest];
    
    OCMVerify([mockOrderDashboardVCObj cancelOpenOrderAPI]);
    OCMVerify([mockOrderDashboardVCObj fetchOrderDashBoardDetailsWithPageIndex:1]);
    
    [mockOrderDashboardVCObj stopMocking];
}

- (void)testResetUIAndMakeAPIRequestWithSegmenetedIndexOne {
    
    id mockOrderDashboardVCObj = [OCMockObject partialMockForObject:_orderTrackerDashboardVC];
    
    [_orderTrackerDashboardVC setValue:[NSNumber numberWithInteger:1] forKeyPath:@"ordersSegmentedControl.selectedSegmentIndex"];
    
    [_orderTrackerDashboardVC resetUIAndMakeAPIRequest];
    
    OCMVerify([mockOrderDashboardVCObj cancelCompletedOrderAPI]);
    OCMVerify([mockOrderDashboardVCObj fetchOrderDashBoardDetailsWithPageIndex:1]);
    
    [mockOrderDashboardVCObj stopMocking];
}

- (void)testLoadNextOpenOrders {
    
    id mockOrderDashboardVCObj = [OCMockObject partialMockForObject:_orderTrackerDashboardVC];
    
    DLMTrackOrderDashBoardScreen *openOrderViewModel = [[DLMTrackOrderDashBoardScreen alloc] init];
    openOrderViewModel.pageIndex = 2;
    openOrderViewModel.totalSize = 6;
    
    id mockDLMOpenOrderObj = [OCMockObject partialMockForObject:openOrderViewModel];
    
    [_orderTrackerDashboardVC setValue:mockDLMOpenOrderObj forKey:@"openOrderViewModel"];
    
    [_orderTrackerDashboardVC loadNextOpenOrders];
    
    OCMVerify([mockOrderDashboardVCObj hideShowFooterIndicatorView:NO]);
    
    [mockOrderDashboardVCObj stopMocking];
    [mockDLMOpenOrderObj stopMocking];
}

- (void)testLoadNextOpenOrdersWithFooterViewNotCreated {
    
    id mockOrderDashboardVCObj = [OCMockObject partialMockForObject:_orderTrackerDashboardVC];
    
    [_orderTrackerDashboardVC setValue:[NSNumber numberWithBool:NO] forKeyPath:@"_isFooterViewCreated"];
    
    [_orderTrackerDashboardVC loadNextOpenOrders];
    
    OCMVerify([mockOrderDashboardVCObj createFooterView]);
    
    [mockOrderDashboardVCObj stopMocking];
}

- (void)testFetchOrderDashBoardDetailsForOpenOrderWithPageIndex {
    
    id mockDLMOpenOrderObj = OCMClassMock([DLMTrackOrderDashBoardScreen class]);
    
    [_orderTrackerDashboardVC setValue:mockDLMOpenOrderObj forKey:@"openOrderViewModel"];
    [_orderTrackerDashboardVC setValue:[NSNumber numberWithUnsignedInteger:1] forKey:@"trackOrderType"];
    
    [_orderTrackerDashboardVC fetchOrderDashBoardDetailsWithPageIndex:1];
    
    OCMVerify([mockDLMOpenOrderObj fetchOrderDashBoardDetailsForLoggedInUser]);
    
    [mockDLMOpenOrderObj stopMocking];
}

- (void)testFetchOrderDashBoardDetailsForCompletedOrderWithPageIndex {
    
    id mockDLMCompletedOrderObj = OCMClassMock([DLMTrackOrderDashBoardScreen class]);
    
    [_orderTrackerDashboardVC setValue:mockDLMCompletedOrderObj forKey:@"completedOrderViewModel"];
    [_orderTrackerDashboardVC setValue:[NSNumber numberWithUnsignedInteger:2] forKey:@"trackOrderType"];
    
    [_orderTrackerDashboardVC fetchOrderDashBoardDetailsWithPageIndex:1];
    
    OCMVerify([mockDLMCompletedOrderObj fetchOrderDashBoardDetailsForLoggedInUser]);
    
    [mockDLMCompletedOrderObj stopMocking];
}

- (void)testFetchOpenOrdersAutomatically {
    
    id mockOrderDashboardVCObj = [OCMockObject partialMockForObject:_orderTrackerDashboardVC];
    
    DLMTrackOrderDashBoardScreen *openOrderViewModel = [[DLMTrackOrderDashBoardScreen alloc] init];
    openOrderViewModel.pageIndex = 2;
    openOrderViewModel.totalSize = 6;
    
    id mockDLMOpenOrderObj = [OCMockObject partialMockForObject:openOrderViewModel];
    
    [_orderTrackerDashboardVC setValue:mockDLMOpenOrderObj forKey:@"openOrderViewModel"];
    
    [_orderTrackerDashboardVC fetchOpenOrdersAutomatically];
    
    OCMVerify([mockOrderDashboardVCObj hideShowFooterIndicatorView:NO]);
    OCMVerify([mockOrderDashboardVCObj fetchOrderDashBoardDetailsWithPageIndex:openOrderViewModel.pageIndex]);
    
    [mockOrderDashboardVCObj stopMocking];
    [mockDLMOpenOrderObj stopMocking];
}

- (void)testFetchOpenOrdersAutomaticallyWithNoInternetConnection {
    
    id mockOrderDashboardVCObj = [OCMockObject partialMockForObject:_orderTrackerDashboardVC];
    
    DLMTrackOrderDashBoardScreen *openOrderViewModel = [[DLMTrackOrderDashBoardScreen alloc] init];
    openOrderViewModel.pageIndex = 2;
    openOrderViewModel.totalSize = 6;
    
    id mockDLMOpenOrderObj = [OCMockObject partialMockForObject:openOrderViewModel];
    
    [_orderTrackerDashboardVC setValue:mockDLMOpenOrderObj forKey:@"openOrderViewModel"];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(NO);
    
    id mockOrderDashboardFooterViewObj = OCMClassMock([BTOrderDashBoardTableFooterView class]);
    [_orderTrackerDashboardVC setValue:mockOrderDashboardFooterViewObj forKey:@"_orderDashBoardTableFooterView"];
    
    [_orderTrackerDashboardVC fetchOpenOrdersAutomatically];
    
    OCMVerify([mockOrderDashboardFooterViewObj showRetryViewWithErrorMessage:[OCMArg any]]);
    OCMVerify([mockOrderDashboardFooterViewObj stopCustomLoadingIndicatorAnimation]);
    OCMVerify([mockAppManagerObj trackNoInternetErrorOnPage:[OCMArg any]]);
    
    [mockOrderDashboardVCObj stopMocking];
    [mockDLMOpenOrderObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testFetchCompletedOrdersAutomatically {
    
    id mockOrderDashboardVCObj = [OCMockObject partialMockForObject:_orderTrackerDashboardVC];
    
    DLMTrackOrderDashBoardScreen *completedOrderViewModel = [[DLMTrackOrderDashBoardScreen alloc] init];
    completedOrderViewModel.pageIndex = 2;
    completedOrderViewModel.totalSize = 6;
    
    id mockDLMCompletedOrderObj = [OCMockObject partialMockForObject:completedOrderViewModel];
    
    [_orderTrackerDashboardVC setValue:mockDLMCompletedOrderObj forKey:@"completedOrderViewModel"];
    
    [_orderTrackerDashboardVC fetchCompletedOrdersAutomatically];
    
    OCMVerify([mockOrderDashboardVCObj hideShowFooterIndicatorView:NO]);
    OCMVerify([mockOrderDashboardVCObj fetchOrderDashBoardDetailsWithPageIndex:completedOrderViewModel.pageIndex]);
    
    [mockOrderDashboardVCObj stopMocking];
    [mockDLMCompletedOrderObj stopMocking];
}

- (void)testFetchCompletedOrdersAutomaticallyWithNoInternetConnection {
    
    id mockOrderDashboardVCObj = [OCMockObject partialMockForObject:_orderTrackerDashboardVC];
    
    DLMTrackOrderDashBoardScreen *completedOrderViewModel = [[DLMTrackOrderDashBoardScreen alloc] init];
    completedOrderViewModel.pageIndex = 2;
    completedOrderViewModel.totalSize = 6;
    
    id mockDLMCompletedOrderObj = [OCMockObject partialMockForObject:completedOrderViewModel];
    
    [_orderTrackerDashboardVC setValue:mockDLMCompletedOrderObj forKey:@"completedOrderViewModel"];
    
    id mockAppManagerObj = OCMClassMock([AppManager class]);
    OCMStub([mockAppManagerObj isInternetConnectionAvailable]).andReturn(NO);
    
    id mockOrderDashboardFooterViewObj = OCMClassMock([BTOrderDashBoardTableFooterView class]);
    [_orderTrackerDashboardVC setValue:mockOrderDashboardFooterViewObj forKey:@"_orderDashBoardTableFooterView"];
    
    [_orderTrackerDashboardVC fetchCompletedOrdersAutomatically];
    
    OCMVerify([mockOrderDashboardFooterViewObj showRetryViewWithErrorMessage:[OCMArg any]]);
    OCMVerify([mockOrderDashboardFooterViewObj stopCustomLoadingIndicatorAnimation]);
    OCMVerify([mockAppManagerObj trackNoInternetErrorOnPage:[OCMArg any]]);
    
    [mockOrderDashboardVCObj stopMocking];
    [mockDLMCompletedOrderObj stopMocking];
    [mockAppManagerObj stopMocking];
}

- (void)testCancelOpenOrderAPI {
    
    id mockOrderDashboardVCObj = [OCMockObject partialMockForObject:_orderTrackerDashboardVC];
    id mockDLMOpenOrderObj = OCMClassMock([DLMTrackOrderDashBoardScreen class]);
    
    [_orderTrackerDashboardVC setValue:[NSNumber numberWithBool:YES] forKey:@"_isLazyLoadingInOpenOrder"];
    [_orderTrackerDashboardVC setValue:mockDLMOpenOrderObj forKey:@"openOrderViewModel"];
    
    [_orderTrackerDashboardVC cancelOpenOrderAPI];
    
    OCMVerify([mockOrderDashboardVCObj hideShowFooterIndicatorView:YES]);
    OCMVerify([mockDLMOpenOrderObj cancelDashBoardAPIRequest]);
    
    [mockDLMOpenOrderObj stopMocking];
    [mockOrderDashboardVCObj stopMocking];
}

- (void)testCancelCompletedOrderAPI {
    
    id mockOrderDashboardVCObj = [OCMockObject partialMockForObject:_orderTrackerDashboardVC];
    id mockDLMCompletedOrderObj = OCMClassMock([DLMTrackOrderDashBoardScreen class]);
    
    [_orderTrackerDashboardVC setValue:[NSNumber numberWithBool:YES] forKey:@"_isLazyLoadingInCompleted"];
    [_orderTrackerDashboardVC setValue:mockDLMCompletedOrderObj forKey:@"completedOrderViewModel"];
    
    [_orderTrackerDashboardVC cancelCompletedOrderAPI];
    
    OCMVerify([mockOrderDashboardVCObj hideShowFooterIndicatorView:YES]);
    OCMVerify([mockDLMCompletedOrderObj cancelDashBoardAPIRequest]);
    
    [mockDLMCompletedOrderObj stopMocking];
    [mockOrderDashboardVCObj stopMocking];
}

- (void)testGetPageNameForCurrentGroupWithSingleCug {
    
    BTCug *cug1 = [[BTCug alloc] initWithCugName:@"cug1" groupKey:@"group1" cugRole:1 indexInAPIResponse:1 andRefKey:@"refkey1"];
    
    NSArray *mockArray = [NSArray arrayWithObjects:cug1, nil];
    _orderTrackerDashboardVC.cugs = mockArray;
    
    [_orderTrackerDashboardVC setValue:[NSNumber numberWithInteger:0] forKeyPath:@"ordersSegmentedControl.selectedSegmentIndex"];
    
    XCTAssertTrue([[_orderTrackerDashboardVC getPageNameForCurrentGroup] isEqualToString:OMNIPAGE_ORDER_OPEN], @"Returned page name is not correct.");
    
}

- (void)testGetPageNameForCurrentGroupWithMultipleCug {
    
    BTCug *cug1 = [[BTCug alloc] initWithCugName:@"cug1" groupKey:@"group1" cugRole:1 indexInAPIResponse:1 andRefKey:@"refkey1"];
    BTCug *cug2 = [[BTCug alloc] initWithCugName:@"cug2" groupKey:@"group2" cugRole:1 indexInAPIResponse:2 andRefKey:@"refkey2"];
    
    NSArray *mockArray = [NSArray arrayWithObjects:cug1, cug2, nil];
    _orderTrackerDashboardVC.cugs = mockArray;
    
    [_orderTrackerDashboardVC setValue:[NSNumber numberWithInteger:0] forKeyPath:@"ordersSegmentedControl.selectedSegmentIndex"];
    
    XCTAssertTrue([[_orderTrackerDashboardVC getPageNameForCurrentGroup] isEqualToString:OMNIPAGE_ORDER_OPEN_CUG], @"Returned page name is not correct.");
    
}

- (void)testTrackOmniPage {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [_orderTrackerDashboardVC trackOmniPage:OMNIPAGE_ORDER_OPEN];
    
    OCMVerify([mockOmnitureManagerObj trackPage:[OCMArg any] withContextInfo:[OCMArg any]]);
    
    [mockOmnitureManagerObj stopMocking];
}

- (void)testTrackOmniClickForPage {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [_orderTrackerDashboardVC trackOmniClick:@"SampleClick" forPage:OMNIPAGE_ORDER_OPEN];
    
    OCMVerify([mockOmnitureManagerObj trackClick:[OCMArg any] withContextInfo:[OCMArg any]]);
    
    [mockOmnitureManagerObj stopMocking];
}

- (void)testTrackOmnitureError {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [_orderTrackerDashboardVC trackOmnitureError:@"SampleError"];
    
    OCMVerify([mockOmnitureManagerObj trackError:[OCMArg any] onPageWithName:[OCMArg any] contextInfo:[OCMArg any]]);
    
    [mockOmnitureManagerObj stopMocking];
}

@end
