//
//  DLMTrackOrderDashboardScreenTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/9/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DLMTrackOrderDashBoardScreen.h"
#import "NLTrackOrderDashBoardWebService.h"
#import <OCMock.h>
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "BTCug.h"

@interface DLMTrackOrderDashBoardScreen (test)

- (void)checkForAlreadyFetchedInitialOpenOrders;
- (void)setupToFetchOrderDashboardDetails;
- (void)fetchOrderDashboardDetails;

@end

@interface DLMTrackOrderDashboardScreenTests : XCTestCase

@property (nonatomic) DLMTrackOrderDashBoardScreen *trackOrderDashboardVM;

@end

@implementation DLMTrackOrderDashboardScreenTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    _trackOrderDashboardVM = [[DLMTrackOrderDashBoardScreen alloc] init];
    
    XCTAssertNotNil(_trackOrderDashboardVM, @"DLMTrackOrderDashBoardScreen instance is not created");
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _trackOrderDashboardVM = nil;
}

- (void)testInitWithFirstPageOpenOrderDict {
    
    DLMTrackOrderDashBoardScreen *tempObj = [[DLMTrackOrderDashBoardScreen alloc] initWithFirstPageOpenOrderDict:[NSDictionary dictionary]];
    
    XCTAssertNotNil(tempObj, @"Custom Init method is not created/returned DLMTrackOrderDashBoardScreen instance.");
}

- (void)testFetchOrderDashBoardDetailsForLoggedInUserWithAlreadyFetchedOpenOrderData {
    
    id mockOrderDashboardVMObj = [OCMockObject partialMockForObject:_trackOrderDashboardVM];
    
    [_trackOrderDashboardVM setValue:[NSDictionary dictionary] forKey:@"_firstPageOpenOrderDict"];
    _trackOrderDashboardVM.pageIndex = 1;
    _trackOrderDashboardVM.tabID = 1;
    
    [_trackOrderDashboardVM fetchOrderDashBoardDetailsForLoggedInUser];
    
    OCMVerify([mockOrderDashboardVMObj checkForAlreadyFetchedInitialOpenOrders]);
    
    [mockOrderDashboardVMObj stopMocking];
    
}

- (void)testFetchOrderDashBoardDetailsForLoggedInUser {
    
    id mockOrderDashboardVMObj = [OCMockObject partialMockForObject:_trackOrderDashboardVM];
    
    [_trackOrderDashboardVM fetchOrderDashBoardDetailsForLoggedInUser];
    
    OCMVerify([mockOrderDashboardVMObj setupToFetchOrderDashboardDetails]);
    OCMVerify([mockOrderDashboardVMObj fetchOrderDashboardDetails]);
    
    [mockOrderDashboardVMObj stopMocking];
    
}

- (void)testChangeSelectedCUGToWithInvalidSelectedCug {
    
    [_trackOrderDashboardVM changeSelectedCUGTo:nil];
    
    XCTAssertNil([AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey, @"Test case failed, currentSelectedCug should have nil value for groupKey");
    XCTAssertNil([AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugName, @"Test case failed, currentSelectedCug should have nil value for cugName");
    XCTAssertTrue([[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugRole intValue]==0, @"Test case failed, currentSelectedCug should have nil value for cugRole");
    XCTAssertNil([AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.refKey, @"Test case failed, currentSelectedCug should have nil value for refKey");
    XCTAssertTrue([[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.indexInAPIResponse intValue]==0, @"Test case failed, currentSelectedCug should have nil value for indexInAPIResponse");
}

- (void)testChangeSelectedCUGToWithEmptySelectedCug {
    
    [_trackOrderDashboardVM changeSelectedCUGTo:[BTCug new]];
    
    XCTAssertNil([AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey, @"Test case failed, currentSelectedCug should have nil value for groupKey");
    XCTAssertNil([AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugName, @"Test case failed, currentSelectedCug should have nil value for cugName");
    XCTAssertTrue([[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugRole intValue]==0, @"Test case failed, currentSelectedCug should have nil value for cugRole");
    XCTAssertNil([AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.refKey, @"Test case failed, currentSelectedCug should have nil value for refKey");
    XCTAssertTrue([[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.indexInAPIResponse intValue]==0, @"Test case failed, currentSelectedCug should have nil value for indexInAPIResponse");
}

- (void)testChangeSelectedCUGToWithValidSelectedCug {
    
    BTCug *selectedCug = [[BTCug alloc] initWithCugName:@"cug1" groupKey:@"groupkey" cugRole:1 andIndexInAPIResponse:2];
    
    [_trackOrderDashboardVM changeSelectedCUGTo:selectedCug];
    
    XCTAssertEqual(selectedCug.groupKey, [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey, @"Test case failed, currentSelectedCug should have given value for groupKey");
    XCTAssertEqual(selectedCug.cugName, [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugName, @"Test case failed, currentSelectedCug should have given value for cugName");
    XCTAssertEqual(selectedCug.cugRole, [[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugRole intValue], @"Test case failed, currentSelectedCug should have given value for cugRole");
    XCTAssertEqual(selectedCug.refKey, [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.refKey, @"Test case failed, currentSelectedCug should have given value for refKey");
    XCTAssertEqual(selectedCug.indexInAPIResponse, [[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.indexInAPIResponse intValue], @"Test case failed, currentSelectedCug should have given value for indexInAPIResponse");
}

- (void)testGetTotalNumberOfPagesToFetch {
    
    _trackOrderDashboardVM.totalSize = 8;
    
    XCTAssertTrue([_trackOrderDashboardVM getTotalNumberOfPagesToFetch] == 3, @"Returned value is not correct.");
    
    _trackOrderDashboardVM.totalSize = 6;
    
    XCTAssertTrue([_trackOrderDashboardVM getTotalNumberOfPagesToFetch] == 2, @"Returned value is not correct.");
}

- (void)testCancelDashBoardAPIRequest {
    
    id mockOrderDashobaordWSObj = OCMClassMock([NLTrackOrderDashBoardWebService class]);
    
    [_trackOrderDashboardVM setValue:mockOrderDashobaordWSObj forKey:@"getOrderDashBoardWebService"];
    
    [_trackOrderDashboardVM cancelDashBoardAPIRequest];
    
    OCMVerify([mockOrderDashobaordWSObj cancel]);
    XCTAssertNil([_trackOrderDashboardVM valueForKey:@"getOrderDashBoardWebService"], @"Method must set getOrderDashBoardWebService property value to nil.");
    
    [mockOrderDashobaordWSObj stopMocking];
}

@end
