//
//  BTFaultDetailsMilestoneViewControllerTests.m
//  BTBusinessApp
//
//  Created by Rohini Kumar on 18/08/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "BTFaultDetailsMilestoneViewController.h"
#import "AppConstants.h"
#import "AppManager.h"
#import "CustomSpinnerView.h"
#import "DLMFaultDetailsScreen.h"
#import "OmnitureManager.h"

@interface BTFaultDetailsMilestoneViewControllerTests : XCTestCase
@property (nonatomic) BTFaultDetailsMilestoneViewController *faultDetailsMilestoneVC;
@end

@interface BTFaultDetailsMilestoneViewController (test)
- (void)initialUISetupForFaultDetails;
- (void)createFaultSummaryView;
- (void)createTakeActionView;
- (void)createClosedActionView;
- (void)createLoadingView;
- (void)hideLoadingItems:(BOOL)isHide;
- (void) hideorShowControls:(BOOL)isHide;
- (void)hideRetryItems:(BOOL)isHide;
- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow;

- (void)fetchFaultDetailsData;
- (void) setupFaultDetailsViewModel;
- (void) callFaultDetailsForFaultRef:(NSString *)faultRef;
- (void)cancelFaultDetailsData;

- (void)trackOmniPage:(NSString *)page  withFaultRef:(NSString *)faultRef;
- (void)trackOmniClick:(NSString *)clickEvent forPage:(NSString *)page withFaultRef:(NSString *)faultRef;
- (void)trackPageWithKeyTask:(NSString *)keytask;
@end

@implementation BTFaultDetailsMilestoneViewControllerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    //faultDetailsMileStoneScene
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];
    _faultDetailsMilestoneVC = (BTFaultDetailsMilestoneViewController *)[storyboard instantiateViewControllerWithIdentifier:kStoryboardViewFaultDetailsMilestone];
    
    XCTAssertNotNil(_faultDetailsMilestoneVC.view, @"test case failed, BTFaultDetailsMilestoneViewController instance should not be nil");
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    _faultDetailsMilestoneVC = nil;
}

#pragma mark -
#pragma mark initialUISetupForFaultDetails

- (void) testInitialUISetupForFaultDetails {
    
    id partialVCMock = OCMPartialMock(_faultDetailsMilestoneVC);
    
    [_faultDetailsMilestoneVC initialUISetupForFaultDetails];
    
    OCMVerify([partialVCMock createFaultSummaryView]);
    OCMVerify([partialVCMock createTakeActionView]);
    OCMVerify([partialVCMock createClosedActionView]);
    OCMVerify([partialVCMock createLoadingView]);
    OCMVerify([partialVCMock hideLoadingItems:YES]);
    OCMVerify([partialVCMock hideorShowControls:YES]);
    [partialVCMock stopMocking];
}

#pragma mark -
#pragma mark fetchFaultDetailsData

- (void) testFetchFaultDetailsDataWhenRedirectedFromDashboardWithNetworkConnection {
    
    id partialMock = OCMPartialMock(_faultDetailsMilestoneVC);
    id loadingClassMock = OCMClassMock([CustomSpinnerView class]);
    [_faultDetailsMilestoneVC setValue:[NSNumber numberWithBool:YES] forKey:@"_redirectedFromDashboard"];
    id appManagerClassMock = OCMClassMock([AppManager class]);
    OCMStub([appManagerClassMock isInternetConnectionAvailable]).andReturn(YES);
    
    [_faultDetailsMilestoneVC setValue:loadingClassMock forKey:@"_loadingView"];
    
    [_faultDetailsMilestoneVC fetchFaultDetailsData];
    
    OCMVerify([partialMock hideLoadingItems:NO]);
    OCMVerify([partialMock hideRetryItems:YES]);
    OCMVerify([loadingClassMock startAnimatingLoadingIndicatorView]);
    OCMVerify([partialMock setupFaultDetailsViewModel]);
    OCMVerify([partialMock callFaultDetailsForFaultRef:OCMOCK_ANY]);
    
    [partialMock stopMocking];
    [appManagerClassMock stopMocking];
    [loadingClassMock stopMocking];
}

- (void) testFetchFaultDetailsDataWhenRedirectedFromDashboardWithNoNetworkConnection {
    
    id partialMock = OCMPartialMock(_faultDetailsMilestoneVC);
    [_faultDetailsMilestoneVC setValue:[NSNumber numberWithBool:YES] forKey:@"_redirectedFromDashboard"];
    id appManagerClassMock = OCMClassMock([AppManager class]);
    OCMStub([appManagerClassMock isInternetConnectionAvailable]).andReturn(NO);

    [_faultDetailsMilestoneVC fetchFaultDetailsData];
    
    OCMVerify([partialMock hideLoadingItems:YES]);
    OCMVerify([partialMock showRetryViewWithInternetStrip:YES]);
    OCMVerify([appManagerClassMock trackNoInternetErrorOnPage:OCMOCK_ANY]);
    [partialMock stopMocking];
    [appManagerClassMock stopMocking];
}

- (void) testFetchFaultDetailsDataWhenNoBacNeededWithNetworkConnection {
   
    id partialMock = OCMPartialMock(_faultDetailsMilestoneVC);
    id loadingClassMock = OCMClassMock([CustomSpinnerView class]);
    id dlmClassMock = OCMClassMock([DLMFaultDetailsScreen class]);
    [_faultDetailsMilestoneVC setValue:[NSNumber numberWithBool:NO] forKey:@"_redirectedFromDashboard"];
    [_faultDetailsMilestoneVC setValue:[NSNumber numberWithBool:NO] forKey:@"_bacNeeded"];
    id appManagerClassMock = OCMClassMock([AppManager class]);
    OCMStub([appManagerClassMock isInternetConnectionAvailable]).andReturn(YES);
    
    [_faultDetailsMilestoneVC setValue:loadingClassMock forKey:@"_loadingView"];
    [_faultDetailsMilestoneVC setValue:dlmClassMock forKey:@"faultDetailsViewModel"];
    
    [_faultDetailsMilestoneVC fetchFaultDetailsData];
    
    OCMVerify([partialMock hideLoadingItems:NO]);
    OCMVerify([partialMock hideRetryItems:YES]);
    OCMVerify([loadingClassMock startAnimatingLoadingIndicatorView]);
    OCMVerify([dlmClassMock fetchFaultSummaryDetailsForFaultReference:OCMOCK_ANY andAssetId:OCMOCK_ANY]);
    
    [partialMock stopMocking];
    [loadingClassMock stopMocking];
    [appManagerClassMock stopMocking];
    [dlmClassMock stopMocking];
}

- (void) testFetchFaultDetailsDataWhenNoBacNeededWithNoNetworkConnection {
    
    id partialMock = OCMPartialMock(_faultDetailsMilestoneVC);
    [_faultDetailsMilestoneVC setValue:[NSNumber numberWithBool:NO] forKey:@"_redirectedFromDashboard"];
    [_faultDetailsMilestoneVC setValue:[NSNumber numberWithBool:NO] forKey:@"_bacNeeded"];
    id appManagerClassMock = OCMClassMock([AppManager class]);
    OCMStub([appManagerClassMock isInternetConnectionAvailable]).andReturn(NO);
    
    
    [_faultDetailsMilestoneVC fetchFaultDetailsData];
    
    OCMVerify([partialMock hideLoadingItems:YES]);
    OCMVerify([partialMock showRetryViewWithInternetStrip:YES]);
    OCMVerify([appManagerClassMock trackNoInternetErrorOnPage:OCMOCK_ANY]);
    
    [partialMock stopMocking];
    [appManagerClassMock stopMocking];
}

#pragma mark -
#pragma mark cancelFaultDetailsData 

- (void) testCancelFaultDetailsData {
    
    id partialMock = OCMPartialMock(_faultDetailsMilestoneVC);
    id loadingClassMock = OCMClassMock([CustomSpinnerView class]);
    id dlmClassMock = OCMClassMock([DLMFaultDetailsScreen class]);
    
    [_faultDetailsMilestoneVC setValue:loadingClassMock forKey:@"_loadingView"];
    [_faultDetailsMilestoneVC setValue:dlmClassMock forKey:@"faultDetailsViewModel"];

    [_faultDetailsMilestoneVC cancelFaultDetailsData];
    
    OCMVerify([loadingClassMock startAnimatingLoadingIndicatorView]);
    OCMVerify([dlmClassMock cancelSummaryDetailsForFaultReference:OCMOCK_ANY andAssetId:OCMOCK_ANY]);
    
    [partialMock stopMocking];
    [loadingClassMock stopMocking];
    [dlmClassMock stopMocking];
}

#pragma mark -
#pragma mark OmnitureMethods

#pragma mark trackOmniPage 

- (void) testTrackOmniPage {
    
    id omnitureClassMock = OCMClassMock([OmnitureManager class]);
    
    [_faultDetailsMilestoneVC trackOmniPage:@"Sample Page" withFaultRef:@"Sample ref"];
    
    OCMVerify([omnitureClassMock trackPage:OCMOCK_ANY withContextInfo:OCMOCK_ANY]);
    [omnitureClassMock stopMocking];
}

#pragma mark trackOmniClick

- (void) testTrackOmniClick {
    
    id omnitureClassMock = OCMClassMock([OmnitureManager class]);
    
    [_faultDetailsMilestoneVC trackOmniClick:@"Sample Click" forPage:@"sample page" withFaultRef:@"sample fault"];
    
    OCMVerify([omnitureClassMock trackClick:OCMOCK_ANY withContextInfo:OCMOCK_ANY]);
    [omnitureClassMock stopMocking];
}

#pragma mark trackPageWithKeyTask

- (void) testTrackPageWithKeyTask {
    
    id omnitureClassMock = OCMClassMock([OmnitureManager class]);
    
    [_faultDetailsMilestoneVC trackPageWithKeyTask:@"Sample keytask"];
    
    OCMVerify([omnitureClassMock trackPage:OCMOCK_ANY withContextInfo:OCMOCK_ANY]);
    [omnitureClassMock stopMocking];
}

@end
