//
//  DLMFaultDetailsScreenTests.m
//  BTBusinessApp
//
//  Created by Rohini Kumar on 22/08/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "DLMFaultDetailsScreen.h"
#import "AppManager.h"
#import "NLFaultDetailsWebService.h"
#import "NLGetFaultSummaryDetailsWebService.h"
#import "BTLiveChatAvailabilityChecker.h"

@interface DLMFaultDetailsScreenTests : XCTestCase
@property (nonatomic) DLMFaultDetailsScreen *faultDetailsVM;
@end

@interface DLMFaultDetailsScreen (test)
- (void) setupToFetchFaultSummaryDetailsForFaultReference:(NSString *)faultRef;
- (void) callFaultSummaryDetails;

- (void) setupToFetchFaultSummaryDetailsForFaultReference:(NSString *)faultRef andAssetId:(NSString *)assetId;
- (void) callFaultSummaryDetailsWithAssetID;
- (void) setupToCancelSummaryDetailsForFaultReference:(NSString *)faultRef andAssetId:(NSString *)assetId;
- (void) callCancelSummaryDetails;

- (void) setupToCheckForLiveChatAvailibilityForFault;
- (void) callLiveChatAvailableSlots;
@end
@implementation DLMFaultDetailsScreenTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _faultDetailsVM = [[DLMFaultDetailsScreen alloc] init];
    XCTAssertNotNil(_faultDetailsVM);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    _faultDetailsVM = nil;
}

#pragma mark -
#pragma mark fetchFaultSummaryDetailsForFaultReference

- (void) testFetchFaultSummaryDetailsForFaultReference {
    
    id partialMock = OCMPartialMock(_faultDetailsVM);
    
    [_faultDetailsVM fetchFaultSummaryDetailsForFaultReference:@"1-103051964"];
    
    OCMVerify([partialMock setupToFetchFaultSummaryDetailsForFaultReference:OCMOCK_ANY]);
    OCMVerify([partialMock callFaultSummaryDetails]);
    [partialMock stopMocking];
}

#pragma mark -
#pragma mark fetchFaultSummaryDetailsForFaultReferenceAndAssetID

- (void) testFetchFaultSummaryDetailsForFaultReferenceAndAssetID {
  
    id partialMock = OCMPartialMock(_faultDetailsVM);
    
    [_faultDetailsVM fetchFaultSummaryDetailsForFaultReference:@"1-103051964" andAssetId:@"103051964"];
    
    OCMVerify([partialMock setupToFetchFaultSummaryDetailsForFaultReference:OCMOCK_ANY andAssetId:OCMOCK_ANY]);
    OCMVerify([partialMock callFaultSummaryDetailsWithAssetID]);
    [partialMock stopMocking];
}

#pragma mark -
#pragma mark cancelSummaryDetailsForFaultReference 

- (void) testCancelSummaryDetailsForFaultReference {
    
    id partialMock = OCMPartialMock(_faultDetailsVM);
    
    [_faultDetailsVM cancelSummaryDetailsForFaultReference:@"1-103051964" andAssetId:@"103051964"];
    
    OCMVerify([partialMock setupToCancelSummaryDetailsForFaultReference:OCMOCK_ANY andAssetId:OCMOCK_ANY]);
    OCMVerify([partialMock callCancelSummaryDetails]);
    [partialMock stopMocking];
}

#pragma mark -
#pragma mark checkForLiveChatAvailibilityForFault

- (void) testCheckForLiveChatAvailibilityForFault {
    
    id partialMock = OCMPartialMock(_faultDetailsVM);
    
    [_faultDetailsVM checkForLiveChatAvailibilityForFault];
    
    OCMVerify([partialMock setupToCheckForLiveChatAvailibilityForFault]);
    OCMVerify([partialMock callLiveChatAvailableSlots]);
    [partialMock stopMocking];
}

#pragma mark -
#pragma mark cancelDetailsAPIWithAssetID 

- (void) testCancelDetailsAPIWithAssetID {
    
    id partialMock = OCMPartialMock(_faultDetailsVM);
    id nlmock = OCMClassMock([NLFaultDetailsWebService class]);
    [_faultDetailsVM setValue:nlmock forKey:@"faultDetailsWebService"];
    [_faultDetailsVM cancelDetailsAPIWithAssetID];
    
    OCMVerify([nlmock cancel]);
    XCTAssertNil([_faultDetailsVM valueForKey:@"faultDetailsWebService"],@"test case failed, faultDetailsWebService should have nil.");
    [partialMock stopMocking];
    [nlmock stopMocking];
}

#pragma mark -
#pragma mark cancelDetailsAPI

- (void) testCancelDetailsAPI {
    
    id partialMock = OCMPartialMock(_faultDetailsVM);
    id nlmock = OCMClassMock([NLGetFaultSummaryDetailsWebService class]);
    [_faultDetailsVM setValue:nlmock forKey:@"getFaultSummaryDetailsWebService"];
    [_faultDetailsVM cancelDetailsAPI];
    
    OCMVerify([nlmock cancel]);
    XCTAssertNil([_faultDetailsVM valueForKey:@"getFaultSummaryDetailsWebService"],@"test case failed, getFaultSummaryDetailsWebService should have nil.");
    [partialMock stopMocking];
    [nlmock stopMocking];
}

#pragma mark -
#pragma mark cancelFaultAPI

- (void) testCancelFaultAPI {
    
    id partialMock = OCMPartialMock(_faultDetailsVM);
    id nlmock = OCMClassMock([NLFaultDetailsWebService class]);
    [_faultDetailsVM setValue:nlmock forKey:@"faultDetailsWebService"];
    [_faultDetailsVM cancelFaultAPI];
    
    OCMVerify([nlmock cancel]);
    XCTAssertNil([_faultDetailsVM valueForKey:@"faultDetailsWebService"],@"test case failed, faultDetailsWebService should have nil.");
    [partialMock stopMocking];
    [nlmock stopMocking];
}

#pragma mark -
#pragma mark cancelChatAvailabilityCheckerAPI

- (void) testCancelChatAvailabilityCheckerAPI {
    
    id partialMock = OCMPartialMock(_faultDetailsVM);
    id liveChatmock = OCMClassMock([BTLiveChatAvailabilityChecker class]);
    [_faultDetailsVM setValue:liveChatmock forKey:@"_liveChatAvailabityChecker"];
    [_faultDetailsVM cancelChatAvailabilityCheckerAPI];
    
    OCMVerify([liveChatmock cancel]);
    XCTAssertNil([_faultDetailsVM valueForKey:@"_liveChatAvailabityChecker"],@"test case failed, _liveChatAvailabityChecker should have nil.");
    [partialMock stopMocking];
    [liveChatmock stopMocking];
}

@end
