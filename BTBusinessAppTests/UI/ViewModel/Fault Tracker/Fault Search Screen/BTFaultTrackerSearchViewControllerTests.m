//
//  BTFaultTrackerSearchViewControllerTests.m
//  BTBusinessApp
//
//  Created by Rohini Kumar on 17/08/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "BTFaultTrackerSearchViewController.h"
#import "AppConstants.h"
#import "OmnitureManager.h"

@interface BTFaultTrackerSearchViewControllerTests : XCTestCase
@property (nonatomic) BTFaultTrackerSearchViewController *faultSearchVC;
@end

@interface BTFaultTrackerSearchViewController (test)
- (BOOL)validateFaultIdOrServiceId:(NSString *)inputString;
- (void)trackOmniPage:(NSString *)page;
- (void)trackOmniClick:(NSString *)clickEvent forPage:(NSString *)page;
- (void)trackOmniClick:(NSString *)clickEvent forPage:(NSString *)page withFaultRef:(NSString *)faultRef;
- (void)trackContentViewedForPage:(NSString *)page andNotificationPopupDetails:(NSString *)notificationPopup;
@end
@implementation BTFaultTrackerSearchViewControllerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];
    
    _faultSearchVC = (BTFaultTrackerSearchViewController *)[storyboard instantiateViewControllerWithIdentifier:kStoryboardViewFaultSearch];
    
    XCTAssertNotNil(_faultSearchVC.view);

}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    _faultSearchVC = nil;
}

#pragma mark -
#pragma mark validateFaultIdOrServiceId

- (void) testValidateFaultIdOrServiceIdWithInValidFautRef {
    BOOL isValidFaultRef = [_faultSearchVC validateFaultIdOrServiceId:@"0-098765432"];
    XCTAssertFalse(isValidFaultRef,@"Test case failed, Given FaultRef Should have invalid");
}

- (void) testValidateFaultIdOrServiceIdWithInValidServiceID {
    BOOL isValidFaultRef = [_faultSearchVC validateFaultIdOrServiceId:@"12345678910"];
    XCTAssertFalse(isValidFaultRef,@"Test case failed, Given FaultRef Should have invalid");
}

//Regex:^[Vv][Oo0][Ll][0][1234Ii]{2}[-]{0,1}[0-9OoSsIi]{9}$ FaultRef:VOL022-190384011
- (void) testValidateFaultIdOrServiceIdWithValidRegex1 {
    BOOL isValidFaultRef = [_faultSearchVC validateFaultIdOrServiceId:@"VOL022-190384011"];
    XCTAssertTrue(isValidFaultRef,@"Test case failed, Given FaultRef is not valid");
}

//Regex:^[Vv][Oo0][Ll][0][1234Ii]{2}[-]{0,1}[0-9OoSsIi]{11}$ FaultRef:VOL012-12345678901
- (void) testValidateFaultIdOrServiceIdWithValidRegex2 {
    BOOL isValidFaultRef = [_faultSearchVC validateFaultIdOrServiceId:@"VOL012-12345678901"];
    XCTAssertTrue(isValidFaultRef,@"Test case failed, Given FaultRef is not valid");
}

//Regex:^[1Ii3][-]{0,1}[0-9OoSsIi]{9}$      FaultRef:1-190251813    ----   PSTN ML
- (void) testValidateFaultIdOrServiceIdWithValidRegex3 {
    BOOL isValidFaultRef = [_faultSearchVC validateFaultIdOrServiceId:@"1-190251813"];
    XCTAssertTrue(isValidFaultRef,@"Test case failed,Given FaultRef is not valid");
}

////Sample FaultRef:^[0Oo][0-9OoSsIi]{10}$   Ref:01212345678
- (void) testValidateFaultIdOrServiceIdWithValidRegex4 {
    BOOL isValidFaultRef = [_faultSearchVC validateFaultIdOrServiceId:@"01212345678"];
    XCTAssertTrue(isValidFaultRef,@"Test case failed,Given FaultRef is not valid");
}

//^[0Oo][12Ii]{2}[-]{0,1}[0-9OoSsIi]{9}$			—- 012-123456789
- (void) testValidateFaultIdOrServiceIdWithValidRegex5 {
    BOOL isValidFaultRef = [_faultSearchVC validateFaultIdOrServiceId:@"012-123456789"];
    XCTAssertTrue(isValidFaultRef,@"Test case failed,Given FaultRef is not valid");
}

//^[0Oo][12Ii]{2}[-]{0,1}[0-9OoSsIi]{11}$			—- 012-12345678912
- (void) testValidateFaultIdOrServiceIdWithValidRegex6 {
    BOOL isValidFaultRef = [_faultSearchVC validateFaultIdOrServiceId:@"012-12345678912"];
    XCTAssertTrue(isValidFaultRef,@"Test case failed,Given FaultRef is not valid");
}

#pragma mark -
#pragma mark trackOmniPage

- (void) testTrackOmniPage {
    
    id omnitureManagerClassMock = OCMClassMock([OmnitureManager class]);
    [_faultSearchVC trackOmniPage:OMNIPAGE_FAULT_SEARCH_FAULT];
    
    OCMVerify([omnitureManagerClassMock trackPage:[OCMArg any] withContextInfo:[OCMArg any]]);
    [omnitureManagerClassMock stopMocking];
}

#pragma mark trackOmniClick

- (void) testTrackOmniClick {
    
    id omnitureManagerClassMock = OCMClassMock([OmnitureManager class]);
    
    [_faultSearchVC trackOmniClick:@"sample click" forPage:OMNIPAGE_FAULT_SEARCH_FAULT];
    
    OCMVerify([omnitureManagerClassMock trackClick:[OCMArg any] withContextInfo:[OCMArg any]]);
    [omnitureManagerClassMock stopMocking];
}

#pragma mark trackOmniClickwithFaultRef

- (void) testtrackOmniClickwithFaultRef {
    
    id omnitureManagerClassMock = OCMClassMock([OmnitureManager class]);
    
    [_faultSearchVC trackOmniClick:@"SampleClick" forPage:OMNIPAGE_FAULT_SEARCH_FAULT withFaultRef:@"1-190257166"];
    
    OCMVerify([omnitureManagerClassMock trackClick:[OCMArg any] withContextInfo:[OCMArg any]]);
    [omnitureManagerClassMock stopMocking];
}

- (void) testtrackOmniClickwithNoFaultRef {

    id omnitureManagerClassMock = OCMClassMock([OmnitureManager class]);
    
    [_faultSearchVC trackOmniClick:@"SampleClick" forPage:OMNIPAGE_FAULT_SEARCH_FAULT withFaultRef:nil];
    
    OCMVerify([omnitureManagerClassMock trackClick:[OCMArg any] withContextInfo:[OCMArg any]]);
    [omnitureManagerClassMock stopMocking];
}

#pragma mark trackContentViewedForPage

- (void) testTrackContentViewedForPage {

    id omnitureManagerClassMock = OCMClassMock([OmnitureManager class]);
    
    [_faultSearchVC trackContentViewedForPage:OMNIPAGE_FAULT_SEARCH_FAULT andNotificationPopupDetails:@"Sample popup details"];
    
    OCMVerify([omnitureManagerClassMock trackPage:[OCMArg any] withContextInfo:[OCMArg any]]);
    [omnitureManagerClassMock stopMocking];
}

@end
