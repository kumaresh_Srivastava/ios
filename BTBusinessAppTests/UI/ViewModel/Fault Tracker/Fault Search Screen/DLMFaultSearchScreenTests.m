//
//  DLMFaultSearchScreenTests.m
//  BTBusinessApp
//
//  Created by Rohini Kumar on 17/08/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "DLMFaultSearchScreen.h"

@interface DLMFaultSearchScreenTests : XCTestCase

@property (nonatomic) DLMFaultSearchScreen *faultSearchScreenVM;
@end

@implementation DLMFaultSearchScreenTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _faultSearchScreenVM = [[DLMFaultSearchScreen alloc] init];
    XCTAssertNotNil(_faultSearchScreenVM,@"DLMFaultSearchScreen instance is not created");
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    _faultSearchScreenVM = nil;
}

#pragma mark -
#pragma mark fetchRecentSearchedFaults

-(void) testFetchRecentSearchedFaults {
    
    
    id protocolMock = OCMProtocolMock(@protocol(DLMFaultSearchScreenDelegate));
    _faultSearchScreenVM.faultSearchScreenDelegate = protocolMock;
    [_faultSearchScreenVM fetchRecentSearchedFaults];
    XCTAssertNotNil(_faultSearchScreenVM.arrayOfRecentSearchedFaults,@"Test case failed, arrayOfRecentSearchedFaults is not created");
    OCMVerify([protocolMock successfullyFetchedRecentSearchedFaultDataOnFaultSearchScreen:[OCMArg any]]);
    [protocolMock stopMocking];
}

@end
