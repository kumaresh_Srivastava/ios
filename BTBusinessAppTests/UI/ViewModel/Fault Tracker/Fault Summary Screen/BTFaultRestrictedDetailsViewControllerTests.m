//
//  BTFaultRestrictedDetailsViewControllerTests.m
//  BTBusinessApp
//
//  Created by Rohini Kumar on 18/08/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "BTFaultRestrictedDetailsViewController.h"
#import "AppConstants.h"
#import "AppManager.h"
#import "DLMFaultSummaryScreen.h"
#import "CustomSpinnerView.h"
#import "BTCustomInputAlertView.h"
#import "OmnitureManager.h"


@interface BTFaultRestrictedDetailsViewControllerTests : XCTestCase
@property (nonatomic) BTFaultRestrictedDetailsViewController *faultRestrictedDetailsVC;
@end

@interface BTFaultRestrictedDetailsViewController (test)

- (void)initialSetupForFaultSummaryScreen;
- (void)createLoadingView;
- (void) hideorShowControls:(BOOL)isHide;

- (void)showRetryViewWithInternetStrip:(BOOL)needToShowInternetStrip;
- (void)fetchFaultSummaryDetails;
- (void)hideLoadingItems:(BOOL)isHide;
- (void)hideRetryItems:(BOOL)isHide;
- (void)callTrackFaultDetailsAPIWithFaultId:(NSString *)faultId withAssetId:(NSString *)assetId withBAC:(NSString *)bac;
- (void)removeCustomAlertViewFromWindow;
- (void)doValidationOnBAC:(NSString *)accountNumber;
- (void)trackOmniPage:(NSString *)page  withFaultRef:(NSString *)faultRef;
- (void)trackOmniClick:(NSString *)clickEvent forPage:(NSString *)page withFaultRef:(NSString *)faultRef;
- (void)trackContentViewedForPage:(NSString *)page andNotificationPopupDetails:(NSString *)notificationPopup;
@end
@implementation BTFaultRestrictedDetailsViewControllerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
//FaultRestrictedDetailsScene
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];
    _faultRestrictedDetailsVC = (BTFaultRestrictedDetailsViewController *)[storyboard instantiateViewControllerWithIdentifier:kStoryboardViewFaultRestrictedDetails];
    XCTAssertNotNil(_faultRestrictedDetailsVC.view);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    _faultRestrictedDetailsVC = nil;
}

#pragma mark - 
#pragma mark initialSetupForFaultSummaryScreen

- (void) testInitialSetupForFaultSummaryScreen {
    
    id partialVCMock = OCMPartialMock(_faultRestrictedDetailsVC);
    
    [_faultRestrictedDetailsVC initialSetupForFaultSummaryScreen];
    
    OCMVerify([partialVCMock createLoadingView]);
    OCMVerify([partialVCMock hideorShowControls:[OCMArg any]]);
    [partialVCMock stopMocking];
}

#pragma mark -
#pragma mark fetchFaultSummaryDetails

- (void) testFetchFaultSummaryDetailsWithNoNetworkConnection {
    
    id partialMock = OCMPartialMock(_faultRestrictedDetailsVC);
    id appmanagerClassMock = OCMClassMock([AppManager class]);
    OCMStub([appmanagerClassMock isInternetConnectionAvailable]).andReturn(NO);
    
    [_faultRestrictedDetailsVC fetchFaultSummaryDetails];
    OCMVerify([partialMock showRetryViewWithInternetStrip:YES]);
    OCMVerify([appmanagerClassMock trackNoInternetErrorOnPage:OCMOCK_ANY]);
    [appmanagerClassMock stopMocking];
    [partialMock stopMocking];
}

- (void) testFetchFaultSummaryDetailsWithNetworkConnection {
    
    id partialMock = OCMPartialMock(_faultRestrictedDetailsVC);
    id appmanagerClassMock = OCMClassMock([AppManager class]);
    id loadingViewMock = OCMClassMock([CustomSpinnerView class]);
    id dlmMock = OCMClassMock([DLMFaultSummaryScreen class]);
    OCMStub([appmanagerClassMock isInternetConnectionAvailable]).andReturn(YES);
    
    [_faultRestrictedDetailsVC setValue:loadingViewMock forKey:@"_loadingView"];
    [_faultRestrictedDetailsVC setValue:dlmMock forKey:@"viewModel"];
    [_faultRestrictedDetailsVC fetchFaultSummaryDetails];
    OCMVerify([partialMock hideLoadingItems:NO]);
    OCMVerify([partialMock hideRetryItems:YES]);
    OCMVerify([loadingViewMock startAnimatingLoadingIndicatorView]);
    OCMVerify([dlmMock fetchFaultSummaryForFaultReference:OCMOCK_ANY]);
    
    [appmanagerClassMock stopMocking];
    [partialMock stopMocking];
    [loadingViewMock stopMocking];
    [dlmMock stopMocking];
}

#pragma mark -
#pragma mark callTrackFaultDetailsAPIWithFaultId

- (void) testCallTrackFaultDetailsAPIWithFaultId {
    
    NSString *faultID = @"sample fault";
    NSString *assetID = @"sample AssetID";
    NSString *bac = @"sample BAC";
    
    id dlmMock = OCMClassMock([DLMFaultSummaryScreen class]);
    [_faultRestrictedDetailsVC setValue:dlmMock forKey:@"viewModel"];
    [_faultRestrictedDetailsVC callTrackFaultDetailsAPIWithFaultId:faultID withAssetId:assetID withBAC:bac];
    OCMVerify([dlmMock fetchFaultDetailsForFaultReference:OCMOCK_ANY assetId:OCMOCK_ANY andBAC:OCMOCK_ANY]);
    [dlmMock stopMocking];
}

#pragma mark - 
#pragma mark removeCustomAlertViewFromWindow

- (void) testRemoveCustomAlertViewFromWindow {
    
    id alertViewMock = OCMClassMock([BTCustomInputAlertView class]);
    [_faultRestrictedDetailsVC setValue:alertViewMock forKey:@"_customlAlertView"];
    
    [_faultRestrictedDetailsVC removeCustomAlertViewFromWindow];
    
    OCMVerify([alertViewMock stopLoading]);
    OCMVerify([alertViewMock deRegisterKeyboardNotification]);
    XCTAssertNil([_faultRestrictedDetailsVC valueForKey:@"_customlAlertView"], @"Test Case Failed, _customlAlertView object must be NIl");
    [alertViewMock stopMocking];
}

#pragma mark -
#pragma mark doValidationOnBAC

- (void) testDoValidationOnInvalidBAC {
    id alertViewMock = OCMClassMock([BTCustomInputAlertView class]);
    [_faultRestrictedDetailsVC setValue:alertViewMock forKey:@"_customlAlertView"];
    
    [_faultRestrictedDetailsVC doValidationOnBAC:@"123"];
    OCMVerify([alertViewMock showErrorOnCustomInputWithWithErrro:OCMOCK_ANY]);
    [alertViewMock stopMocking];
}

- (void) testDoValidationOnValidBACWithNoNetworkConnection {

    id alertViewMock = OCMClassMock([BTCustomInputAlertView class]);
    id appmanagerClassMock = OCMClassMock([AppManager class]);
    OCMStub([appmanagerClassMock isInternetConnectionAvailable]).andReturn(NO);
    [_faultRestrictedDetailsVC setValue:alertViewMock forKey:@"_customlAlertView"];
    
    [_faultRestrictedDetailsVC doValidationOnBAC:@"1234567890"];
    OCMVerify([alertViewMock showErrorOnCustomInputWithWithErrro:OCMOCK_ANY]);
    OCMVerify([appmanagerClassMock trackNoInternetErrorOnPage:OCMOCK_ANY]);
    [alertViewMock stopMocking];
    [appmanagerClassMock stopMocking];
}

- (void) testDoValidationOnValidBACWithNetworkConnection {
    
    id alertViewMock = OCMClassMock([BTCustomInputAlertView class]);
    id appmanagerClassMock = OCMClassMock([AppManager class]);
    OCMStub([appmanagerClassMock isInternetConnectionAvailable]).andReturn(YES);
    id partialMock = OCMPartialMock(_faultRestrictedDetailsVC);

    [_faultRestrictedDetailsVC setValue:alertViewMock forKey:@"_customlAlertView"];
    
    [_faultRestrictedDetailsVC doValidationOnBAC:@"1234567890"];
    OCMVerify([alertViewMock removeErrorMeesageFromCustomInputAlertView]);
    OCMVerify([alertViewMock startLoading]);
    OCMVerify([partialMock callTrackFaultDetailsAPIWithFaultId:OCMOCK_ANY withAssetId:OCMOCK_ANY withBAC:OCMOCK_ANY]);
    [alertViewMock stopMocking];
    [appmanagerClassMock stopMocking];
    [partialMock stopMocking];
}

#pragma mark -
#pragma mark OmnitureMethods
#pragma mark trackOmniPage

- (void) testtrackOmniPage {
    id omnitureManagerClassMock = OCMClassMock([OmnitureManager class]);
    
    [_faultRestrictedDetailsVC trackOmniPage:@"sample fault" withFaultRef:@"Sample fault"];
    OCMVerify([omnitureManagerClassMock trackPage:OCMOCK_ANY withContextInfo:OCMOCK_ANY]);
    [omnitureManagerClassMock stopMocking];
}

#pragma mark trackOmniClick
- (void) testtrackOmniClick {

    id omnitureManagerClassMock = OCMClassMock([OmnitureManager class]);
    
    [_faultRestrictedDetailsVC trackOmniClick:@"sample click" forPage:@"sample page" withFaultRef:@"sample fault"];
    OCMVerify([omnitureManagerClassMock trackClick:OCMOCK_ANY withContextInfo:OCMOCK_ANY]);
    [omnitureManagerClassMock stopMocking];
}

#pragma mark trackContentViewedForPage

- (void) testTrackContentViewedForPage {
    
    id omnitureManagerClassMock = OCMClassMock([OmnitureManager class]);
    
    [_faultRestrictedDetailsVC trackContentViewedForPage:@"sample page" andNotificationPopupDetails:@"sample notification"];
    OCMVerify([omnitureManagerClassMock trackPage:OCMOCK_ANY withContextInfo:OCMOCK_ANY]);
    [omnitureManagerClassMock stopMocking];
}

@end
