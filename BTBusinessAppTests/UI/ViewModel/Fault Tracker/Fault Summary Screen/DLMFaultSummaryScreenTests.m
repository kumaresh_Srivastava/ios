//
//  DLMFaultSummaryScreenTests.m
//  BTBusinessApp
//
//  Created by Rohini Kumar on 22/08/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "DLMFaultSummaryScreen.h"
#import "NLGetFaultSummaryWebService.h"
#import "NLFaultDetailsWebService.h"
#import "BTLiveChatAvailabilityChecker.h"

@interface DLMFaultSummaryScreenTests : XCTestCase
@property (nonatomic) DLMFaultSummaryScreen *faultSummaryVM;
@end

@interface DLMFaultSummaryScreen (test)
- (void) setupToFetchFaultSummaryForFaultReference:(NSString *) faultRef;
- (void) fetchFaultSummaryForCurrentFault;
- (void) setupToFetchFaultDetailsForFaultReference:(NSString *)faultRef assetId:(NSString *)assetId andBAC:(NSString *)BAC;
- (void) fetchFaultDetails;
- (void) setupToCheckForLiveChatAvailibilityForFault;
- (void) checkFaultLiveChatAvailability;
@end

@implementation DLMFaultSummaryScreenTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _faultSummaryVM = [[DLMFaultSummaryScreen alloc] init];
    XCTAssertNotNil(_faultSummaryVM);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    _faultSummaryVM = nil;
}

#pragma mark -
#pragma mark fetchFaultSummaryForFaultReference

- (void) testFetchFaultSummaryForFaultReference {
    
    id partialMock = OCMPartialMock(_faultSummaryVM);
    NSString *faultRef = @"1-103051964";
    [_faultSummaryVM fetchFaultSummaryForFaultReference:faultRef];
    
    OCMVerify([partialMock setupToFetchFaultSummaryForFaultReference:OCMOCK_ANY]);
    OCMVerify([partialMock fetchFaultSummaryForCurrentFault]);
    [partialMock stopMocking];
}

#pragma mark - 
#pragma mark fetchFaultDetailsForFaultReference 

- (void) testFetchFaultDetailsForFaultReference {
    
    id partialMock = OCMPartialMock(_faultSummaryVM);
    NSString *faultRef = @"1-103051964";
    NSString *assetID = @"103051964";
    NSString *BAC = @"NE22714602";
    [_faultSummaryVM fetchFaultDetailsForFaultReference:faultRef assetId:assetID andBAC:BAC];
    
    OCMVerify([partialMock setupToFetchFaultDetailsForFaultReference:OCMOCK_ANY assetId:OCMOCK_ANY andBAC:OCMOCK_ANY]);
    OCMVerify([partialMock fetchFaultDetails]);
    [partialMock stopMocking];
}

#pragma mark -
#pragma mark checkForLiveChatAvailibilityForFault

- (void) testCheckForLiveChatAvailibilityForFault {
    
    id partialMock = OCMPartialMock(_faultSummaryVM);
    [_faultSummaryVM checkForLiveChatAvailibilityForFault];
    
    OCMVerify([partialMock setupToCheckForLiveChatAvailibilityForFault]);
    OCMVerify([partialMock checkFaultLiveChatAvailability]);
    [partialMock stopMocking];
}

#pragma mark -
#pragma mark cancelSummaryAPI 

- (void) testCancelSummaryAPI {
    
    id NLclassMock = OCMClassMock([NLGetFaultSummaryWebService class]);
    [_faultSummaryVM setValue:NLclassMock forKey:@"getFaultSummaryWebService"];
    
    [_faultSummaryVM cancelSummaryAPI];
    
    OCMVerify([NLclassMock cancel]);
    XCTAssertNil([_faultSummaryVM valueForKey:@"getFaultSummaryWebService"],@"Test case failed, getFaultSummaryWebService object should have nil");
    [NLclassMock stopMocking];
}

#pragma mark -
#pragma mark cancelDetailsAPI

- (void) testCancelDetailsAPI {
    
    id NLclassMock = OCMClassMock([NLFaultDetailsWebService class]);
    [_faultSummaryVM setValue:NLclassMock forKey:@"getFaultDetailsWebService"];
    
    [_faultSummaryVM cancelDetailsAPI];
    
    OCMVerify([NLclassMock cancel]);
    XCTAssertNil([_faultSummaryVM valueForKey:@"getFaultDetailsWebService"],@"Test case failed, getFaultDetailsWebService object should have nil");
    [NLclassMock stopMocking];
}

#pragma mark -
#pragma mark cancelChatAvailabilityCheckerAPI

- (void) testCancelChatAvailabilityCheckerAPI {
    
    id chatClassMock = OCMClassMock([BTLiveChatAvailabilityChecker class]);
    [_faultSummaryVM setValue:chatClassMock forKey:@"_liveChatAvailabityChecker"];
    
    [_faultSummaryVM cancelChatAvailabilityCheckerAPI];
    
    OCMVerify([chatClassMock cancel]);
    XCTAssertNil([_faultSummaryVM valueForKey:@"_liveChatAvailabityChecker"],@"Test case failed, _liveChatAvailabityChecker object should have nil");
    [chatClassMock stopMocking];
}

@end
