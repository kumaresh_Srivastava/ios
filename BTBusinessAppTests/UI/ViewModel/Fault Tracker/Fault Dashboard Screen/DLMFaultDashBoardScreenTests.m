//
//  DLMFaultDashBoardScreenTests.m
//  BTBusinessApp
//
//  Created by Accolite on 09/08/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "DLMFaultDashBoardScreen.h"
#import "NLFaultDashBoardWebService.h"
#import "BTLiveChatAvailabilityChecker.h"
#import "AppDelegate.h"
#import "CDUser.h"
#import "BTCug.h"
#import "AppDelegateViewModel.h"
#import "CDApp.h"
#import "CDCug+CoreDataClass.h"

@interface DLMFaultDashBoardScreenTests : XCTestCase

@property (nonatomic) DLMFaultDashBoardScreen *faultDashBoarVM;
@end

@interface DLMFaultDashBoardScreen (test)
- (void) checkForAlreadyFetchedOpenFaults;
- (void) setupToFetchFaultDashboardDetailsWithPageSize:(int)pageSize withPageIndex:(int)pageIndex withTabID:(int)tabID;
- (void) fetchFaultDashboardDetails;
- (void) getLiveChatAvailableSlotsForFaults;
@end

@implementation DLMFaultDashBoardScreenTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _faultDashBoarVM = [[DLMFaultDashBoardScreen alloc] init];
    XCTAssertNotNil(_faultDashBoarVM,@"DLMFaultDashBoardScreen object is not created");
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    _faultDashBoarVM = nil;
}

#pragma mark -
#pragma mark fetchFaultDashBoardDetailsForLoggedInUserWithPageSize

- (void) testfetchFaultDashBoardDetailsForLoggedInUserForAlreadyFetchedOpenFault {
    
    id dashboardVMMockObj = [OCMockObject partialMockForObject:_faultDashBoarVM];
    
    [_faultDashBoarVM setValue:[NSDictionary dictionary] forKey:@"_firstPageOpenFaultDict"];
    _faultDashBoarVM.pageIndex = 1;
    _faultDashBoarVM.tabID = 1;
    
    [_faultDashBoarVM fetchFaultDashBoardDetailsForLoggedInUserWithPageSize:3 pageIndex:1 tabID:1];
    OCMVerify([dashboardVMMockObj checkForAlreadyFetchedOpenFaults]);
    
    [dashboardVMMockObj stopMocking];
}

- (void) testfetchFaultDashBoardDetailsForLoggedInUser {

    id dashboardVMMockObj = [OCMockObject partialMockForObject:_faultDashBoarVM];
    
    int pageSize = 3;
    int pageIndex = 1;
    int tabID = 1;
    [_faultDashBoarVM fetchFaultDashBoardDetailsForLoggedInUserWithPageSize:pageSize pageIndex:pageIndex tabID:tabID];
    
    OCMVerify([dashboardVMMockObj setupToFetchFaultDashboardDetailsWithPageSize:pageSize withPageIndex:pageIndex withTabID:tabID]);
    OCMVerify([dashboardVMMockObj fetchFaultDashboardDetails]);
    
    [dashboardVMMockObj stopMocking];
}

#pragma mark fetchFaultDashboardDetails

- (void) testFetchFaultDashboardDetails {

    id dashBoardNLMock = OCMClassMock([NLFaultDashBoardWebService class]);
    [_faultDashBoarVM setValue:dashBoardNLMock forKeyPath:@"self.getFaultDashBoardWebService"];
    
    [_faultDashBoarVM fetchFaultDashboardDetails];
    
    OCMVerify([dashBoardNLMock resume]);
    [dashBoardNLMock stopMocking];
}

#pragma mark - 
#pragma mark getTotalNumberOfPagesToFetch

- (void) testGetTotalNumberOfPagesToFetchWithRemainingAsGreaterThanZero {
    
    _faultDashBoarVM.totalSize = 7;
    int numOfpages = [_faultDashBoarVM getTotalNumberOfPagesToFetch];
    XCTAssertTrue(numOfpages == 3,@"Value is not true.");
}

- (void) testGetTotalNumberOfPagesToFetchWithRemainingAsZero {
    
    _faultDashBoarVM.totalSize = 12;
    int numOfpages = [_faultDashBoarVM getTotalNumberOfPagesToFetch];
    XCTAssertTrue(numOfpages == 4,@"Value is not true.");
}

#pragma mark -
#pragma mark getLiveChatAvailableSlotsForFaults

- (void) testGetLiveChatAvailableSlotsForFaults {
    
    id liveChatCheckerMock = OCMClassMock([BTLiveChatAvailabilityChecker class]);
    [_faultDashBoarVM setValue:liveChatCheckerMock forKey:@"_liveChatAvailabityChecker"];
    
    [_faultDashBoarVM getLiveChatAvailableSlotsForFaults];
    
    OCMVerify([liveChatCheckerMock getLiveChatAvailableSlots]);
    [liveChatCheckerMock stopMocking];
}

#pragma mark -
#pragma mark changeSelectedCUGTo

- (void)testChangeSelectedCUGToInvalidCug {
    
    [_faultDashBoarVM changeSelectedCUGTo:nil];
    
    XCTAssertNil([AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey, @"Test case failed, currentSelectedCug should have nil value for groupKey");
    XCTAssertNil([AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.refKey, @"Test case failed, currentSelectedCug should have nil value for refKey");
    XCTAssertNil([AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugName, @"Test case failed, currentSelectedCug should have nil value for cugName");
    XCTAssertTrue([[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugRole intValue]==0, @"Test case failed, currentSelectedCug should have nil value for cugRole");
    XCTAssertTrue([[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.indexInAPIResponse intValue]==0, @"Test case failed, currentSelectedCug should have nil value for indexInAPIResponse");
}

- (void)testChangeSelectedCUGToWithEmptySelectedCug {
    
    [_faultDashBoarVM changeSelectedCUGTo:[BTCug new]];
    
    XCTAssertNil([AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey, @"Test case failed, currentSelectedCug should have nil value for groupKey");
    XCTAssertNil([AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.refKey, @"Test case failed, currentSelectedCug should have nil value for refKey");
    XCTAssertNil([AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugName, @"Test case failed, currentSelectedCug should have nil value for cugName");
    XCTAssertTrue([[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugRole intValue]==0, @"Test case failed, currentSelectedCug should have nil value for cugRole");
    XCTAssertTrue([[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.indexInAPIResponse intValue]==0, @"Test case failed, currentSelectedCug should have nil value for indexInAPIResponse");
}

- (void)testChangeSelectedCUGToWithValidSelectedCug {
    
    BTCug *selectedCug = [[BTCug alloc] initWithCugName:@"cug1" groupKey:@"groupkey" cugRole:1 andIndexInAPIResponse:2];
    
    [_faultDashBoarVM changeSelectedCUGTo:selectedCug];
    
    XCTAssertEqual(selectedCug.groupKey, [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey, @"Test case failed, currentSelectedCug should have given value for groupKey");
    XCTAssertEqual(selectedCug.refKey, [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.refKey, @"Test case failed, currentSelectedCug should have given value for refKey");
    XCTAssertEqual(selectedCug.cugName, [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugName, @"Test case failed, currentSelectedCug should have given value for cugName");
    XCTAssertEqual(selectedCug.cugRole, [[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugRole intValue], @"Test case failed, currentSelectedCug should have given value for cugRole");
    XCTAssertEqual(selectedCug.indexInAPIResponse, [[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.indexInAPIResponse intValue], @"Test case failed, currentSelectedCug should have given value for indexInAPIResponse");
}

#pragma mark -
#pragma mark cancelFaultDashBoardAPIRequest

- (void) testCancelFaultDashBoardAPIRequest {
    
    id dashboardNLMock = OCMClassMock([NLFaultDashBoardWebService class]);
    
    [_faultDashBoarVM setValue:dashboardNLMock forKey:@"getFaultDashBoardWebService"];
    
    [_faultDashBoarVM cancelFaultDashBoardAPIRequest];
    OCMVerify([dashboardNLMock cancel]);
    XCTAssertNil([_faultDashBoarVM valueForKey:@"getFaultDashBoardWebService"],@"Method must set getFaultDashBoardWebService property value to nil.");
    [dashboardNLMock stopMocking];
}

#pragma mark -
#pragma mark cancelChatAvailabilityCheckerAPI

- (void) testCancelChatAvailabilityCheckerAPI {
    
    id chatCheckerMock = OCMClassMock([BTLiveChatAvailabilityChecker class]);
    
    [_faultDashBoarVM setValue:chatCheckerMock forKey:@"_liveChatAvailabityChecker"];
    
    [_faultDashBoarVM cancelChatAvailabilityCheckerAPI];
    OCMVerify([chatCheckerMock cancel]);
    XCTAssertNil([_faultDashBoarVM valueForKey:@"_liveChatAvailabityChecker"],@"Method must set _liveChatAvailabityChecker property value to nil.");
    [chatCheckerMock stopMocking];
}

@end
