//
//  BTFaultTrackerDashboardViewControllerTests.m
//  BTBusinessApp
//
//  Created by Accolite on 09/08/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "BTFaultTrackerDashboardViewController.h"
#import "AppConstants.h"
#import "BTCug.h"
#import "DLMFaultDashBoardScreen.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "BTRetryView.h"
#import "DLMFaultDashBoardScreen.h"
#import "BTFaultDashBoardTableFooterView.h"

@interface BTFaultTrackerDashboardViewControllerTests : XCTestCase
@property (nonatomic) BTFaultTrackerDashboardViewController *faultDashboardVC;
@end

@interface BTFaultTrackerDashboardViewController (test)
@property (nonatomic,strong) NSString *currentGroupKey;
@property (nonatomic) DLMFaultDashBoardScreen *openFaultViewModel;

- (void)updateGroupSelection;
- (void)hideGroupSelectionForOneAvailableGroup;
- (void) checkForSuperUserAndUpdateUIWithIndex:(NSUInteger)index;
- (void)resetDataAndUIAfterGroupChange;

- (void)resetUIAndMakeAPIRequest;
- (void) cancelOpenFaultAPI;
- (void)cancelCompletedFaultAPI;
- (void)fetchFaultDashBoardAPIWithPageIndex:(int)pageIndex;
- (void) showEmptyOrdersView;
- (void)showEmptyDashBoardView;
- (NSString *)getPageNameForCurrentGroup;
- (void)trackOmniPage:(NSString *)page;
- (void)trackOmniClick:(NSString *)clickEvent forPage:(NSString *)page withFaultRef:(NSString *)faultRef;
- (void)manageAPIRequestBasedOnSegmentSelection;
- (void)hideRetryItems:(BOOL)isHide;
- (void)hideEmmptyDashBoardItems:(BOOL)isHide;
- (void)showRetryViewWithInternetStrip:(BOOL)internetStripNeedToShow;
- (void)fetchOpenFaultsAutomatically;
- (void)fetchCompletedFaultsAutomatically;
- (void)hideShowFooterIndicatorView:(BOOL)isHide;
- (void)resizeTableViewFooterToFit;
- (void)loadNextOpenFaults;
- (void)createFooterView;
@end

@implementation BTFaultTrackerDashboardViewControllerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];
    _faultDashboardVC = (BTFaultTrackerDashboardViewController *)[storyboard instantiateViewControllerWithIdentifier:@"trackFaultDashboardScene"];

    XCTAssertNotNil(_faultDashboardVC.view);

}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    _faultDashboardVC = nil;
}

#pragma mark - 
#pragma mark UpdateGroupSelection

- (void) testUpdateGroupSelection {
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    BTCug *cug = [[BTCug alloc] initWithCugName:@"testCugName" groupKey:@"groupkey" cugRole:1 andIndexInAPIResponse:1];
    NSArray *cugArray = [NSArray arrayWithObjects:cug, nil];
    _faultDashboardVC.cugs = cugArray;

    [_faultDashboardVC updateGroupSelection];
    OCMVerify([dashboardMockObj hideGroupSelectionForOneAvailableGroup]);
    [dashboardMockObj stopMocking];
}

- (void) testUpdateMultiGroupSelection {
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    BTCug *cug1 = [[BTCug alloc] initWithCugName:@"testCugName" groupKey:@"group1" cugRole:1 andIndexInAPIResponse:1];
    BTCug *cug2 = [[BTCug alloc] initWithCugName:@"testCugName" groupKey:@"group2" cugRole:1 andIndexInAPIResponse:1];

    [_faultDashboardVC setCugs:[NSArray arrayWithObjects:cug1, cug2, nil]];
    [_faultDashboardVC updateGroupSelection];
    OCMVerify([dashboardMockObj setCurrentGroupKey:[OCMArg any]]);
    [dashboardMockObj stopMocking];
}

#pragma mark -

- (void) testcheckForSuperUserAndUpdateUIWithIndex {
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    id dlmMockObj = OCMClassMock([DLMFaultDashBoardScreen class]);
    
    BTCug *cug3 = [[BTCug alloc] initWithCugName:@"testcugName" groupKey:@"group1" cugRole:1 indexInAPIResponse:1 andRefKey:@"refkey1"];
    BTCug *cug4 = [[BTCug alloc] initWithCugName:@"testcugName" groupKey:@"group2" cugRole:1 indexInAPIResponse:1 andRefKey:@"refkey2"];
    
    NSArray *cugArray = [NSArray arrayWithObjects:cug3, cug4, nil];
    [_faultDashboardVC setCugs:cugArray];
    [_faultDashboardVC setCurrentGroupKey:@"testKey"];
    [_faultDashboardVC setOpenFaultViewModel:dlmMockObj];
    
    [dashboardMockObj checkForSuperUserAndUpdateUIWithIndex:1];
    
    OCMVerify([dlmMockObj changeSelectedCUGTo:[OCMArg any]]);
    OCMVerify([dashboardMockObj setCurrentGroupKey:[OCMArg any]]);
    OCMVerify([dashboardMockObj resetDataAndUIAfterGroupChange]);
    
    [dashboardMockObj stopMocking];
    [dlmMockObj stopMocking];
}

#pragma mark -
#pragma mark ResetUIAndMakeAPIRequest

- (void) testResetUIAndMakeAPIRequestForSegmentIndexZero {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    
    [_faultDashboardVC setValue:[NSNumber numberWithInt:0] forKeyPath:@"faultSegmentedControl.selectedSegmentIndex"];
    
    [_faultDashboardVC resetUIAndMakeAPIRequest];
    
    OCMVerify([dashboardMockObj cancelOpenFaultAPI]);
    OCMVerify([dashboardMockObj fetchFaultDashBoardAPIWithPageIndex:1]);
    [dashboardMockObj stopMocking];
}

- (void) testResetUIAndMakeAPIRequestForSegmentIndexOne {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    [_faultDashboardVC setValue:[NSNumber numberWithInt:1] forKeyPath:@"faultSegmentedControl.selectedSegmentIndex"];
    
    [_faultDashboardVC resetUIAndMakeAPIRequest];
    
    OCMVerify([dashboardMockObj cancelCompletedFaultAPI]);
    OCMVerify([dashboardMockObj fetchFaultDashBoardAPIWithPageIndex:1]);
    [dashboardMockObj stopMocking];

}

- (void) testShowEmptyOrdersView {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    
    [_faultDashboardVC showEmptyOrdersView];
    OCMVerify([dashboardMockObj showEmptyDashBoardView]);
    [dashboardMockObj stopMocking];
}

#pragma mark -
#pragma mark GetPageNameForCurrentGroup

- (void)testGetPageNameForCurrentGroupWithOneCug {
    
    BTCug *cug1 = [[BTCug alloc] initWithCugName:@"cug" groupKey:@"testgroup" cugRole:1 indexInAPIResponse:1 andRefKey:@"testrefkey"];
    
    [_faultDashboardVC setCugs:[NSArray arrayWithObjects:cug1, nil]];
    
    [_faultDashboardVC setValue:[NSNumber numberWithInteger:0] forKeyPath:@"faultSegmentedControl.selectedSegmentIndex"];
    
    XCTAssertTrue([[_faultDashboardVC getPageNameForCurrentGroup] isEqualToString:OMNIPAGE_FAULT_OPEN], @"Returned page name is not true.");
    
}

- (void)testGetPageNameForCurrentGroupWithMultiCug {
    
    BTCug *cug1 = [[BTCug alloc] initWithCugName:@"cug1" groupKey:@"testgroup" cugRole:1 indexInAPIResponse:1 andRefKey:@"testrefkey"];
    BTCug *cug2 = [[BTCug alloc] initWithCugName:@"cug2" groupKey:@"testgroup" cugRole:1 indexInAPIResponse:1 andRefKey:@"testrefkey"];
    
    [_faultDashboardVC setCugs:[NSArray arrayWithObjects:cug1,cug2, nil]];
    
    [_faultDashboardVC setValue:[NSNumber numberWithInteger:0] forKeyPath:@"faultSegmentedControl.selectedSegmentIndex"];
    
    XCTAssertTrue([[_faultDashboardVC getPageNameForCurrentGroup] isEqualToString:OMNIPAGE_FAULT_OPEN_CUG], @"Returned page name is not true.");
    
}

#pragma mark - 
#pragma mark OmnitureMethods

- (void) testTrackOmniPage {
    
    id omnitureMockObj = OCMClassMock([OmnitureManager class]);
    [_faultDashboardVC trackOmniPage:@"Sample Fualt page"];
    OCMVerify([omnitureMockObj trackPage:[OCMArg any] withContextInfo:[OCMArg any]]);
    [omnitureMockObj stopMocking];
}

- (void) testTrackOmniClick {

    id omnitureMockObj = OCMClassMock([OmnitureManager class]);
    [_faultDashboardVC trackOmniClick:@"test Click" forPage:@"Sample fault Page" withFaultRef:@"testFaultref"];
    OCMVerify([omnitureMockObj trackClick:[OCMArg any] withContextInfo:[OCMArg any]]);
    
    [omnitureMockObj stopMocking];
}

#pragma mark -
#pragma mark manageAPIRequestBasedOnSegmentSelection

- (void) testManageAPIRequestBasedOnSegmentedSelectionForRetryShownWithInternetConnection {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    id appmanagerClassMock = OCMClassMock([AppManager class]);
    OCMStub([appmanagerClassMock isInternetConnectionAvailable]).andReturn(YES);
    [_faultDashboardVC setValue:[NSNumber numberWithBool:NO] forKeyPath:@"_apiExecutedForOpen"];
    [_faultDashboardVC setValue:[NSNumber numberWithBool:YES] forKeyPath:@"_isRetryShown"];
    
    [_faultDashboardVC manageAPIRequestBasedOnSegmentSelection];
    OCMVerify([dashboardMockObj hideRetryItems:YES]);
    [dashboardMockObj stopMocking];
    [appmanagerClassMock stopMocking];
}


- (void) testManageAPIRequestBasedOnSegmentedSelectionForRetryShownWithNoInternetConnection {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    id appmanagerClassMock = OCMClassMock([AppManager class]);
    id retryViewClassMock = OCMClassMock([BTRetryView class]);
    OCMStub([appmanagerClassMock isInternetConnectionAvailable]).andReturn(NO);
    [_faultDashboardVC setValue:[NSNumber numberWithBool:NO] forKeyPath:@"_apiExecutedForOpen"];
    [_faultDashboardVC setValue:[NSNumber numberWithBool:YES] forKeyPath:@"_isRetryShown"];
    [_faultDashboardVC setValue:retryViewClassMock forKey:@"_retryView"];
    
    [_faultDashboardVC manageAPIRequestBasedOnSegmentSelection];
    OCMVerify([retryViewClassMock updateRetryViewWithInternetStrip:YES]);
    OCMVerify([appmanagerClassMock trackNoInternetErrorOnPage:[OCMArg any]]);
    [dashboardMockObj stopMocking];
    [appmanagerClassMock stopMocking];
    [retryViewClassMock stopMocking];
}

- (void) testManageAPIRequestBasedOnSegmentedSelectionForEmptyViewShownWithNoInternetConnection {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    id appmanagerClassMock = OCMClassMock([AppManager class]);
    OCMStub([appmanagerClassMock isInternetConnectionAvailable]).andReturn(NO);
    [_faultDashboardVC setValue:[NSNumber numberWithBool:NO] forKeyPath:@"_apiExecutedForOpen"];
    [_faultDashboardVC setValue:[NSNumber numberWithBool:YES] forKeyPath:@"_isEmptyViewShown"];
    
    [_faultDashboardVC manageAPIRequestBasedOnSegmentSelection];
    OCMVerify([dashboardMockObj hideEmmptyDashBoardItems:[OCMArg any]]);
    OCMVerify([dashboardMockObj showRetryViewWithInternetStrip:[OCMArg any]]);
    OCMVerify([appmanagerClassMock trackNoInternetErrorOnPage:[OCMArg any]]);
    [dashboardMockObj stopMocking];
    [appmanagerClassMock stopMocking];
}

- (void) testManageAPIRequestBasedOnSegmentedSelectionForZeroSegmentIndexWithNoData {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    
    [_faultDashboardVC setValue:[NSNumber numberWithInt:0] forKeyPath:@"faultSegmentedControl.selectedSegmentIndex"];
    [_faultDashboardVC setValue:[NSArray array] forKeyPath:@"openFaultData"];
    
    [_faultDashboardVC manageAPIRequestBasedOnSegmentSelection];
    OCMVerify([dashboardMockObj fetchFaultDashBoardAPIWithPageIndex:1]);
    [dashboardMockObj stopMocking];
}

- (void) testManageAPIRequestBasedOnSegmentedSelectionForZeroSegmentIndexWithData {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    
    [_faultDashboardVC setValue:[NSNumber numberWithInt:0] forKeyPath:@"faultSegmentedControl.selectedSegmentIndex"];
    [_faultDashboardVC setValue:[NSArray arrayWithObject:@"testObject"] forKeyPath:@"openFaultData"];
    
    [_faultDashboardVC manageAPIRequestBasedOnSegmentSelection];
    OCMVerify([dashboardMockObj fetchOpenFaultsAutomatically]);
    [dashboardMockObj stopMocking];
}

- (void) testManageAPIRequestBasedOnSegmentedSelectionForSegmentIndexOneWithNoData {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    
    [_faultDashboardVC setValue:[NSNumber numberWithInt:1] forKeyPath:@"faultSegmentedControl.selectedSegmentIndex"];
    [_faultDashboardVC setValue:[NSArray array] forKeyPath:@"completedFaultData"];
    
    [_faultDashboardVC manageAPIRequestBasedOnSegmentSelection];
    OCMVerify([dashboardMockObj fetchFaultDashBoardAPIWithPageIndex:1]);
    [dashboardMockObj stopMocking];
}

- (void) testManageAPIRequestBasedOnSegmentedSelectionForSegmentIndexOneWithData {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    
    [_faultDashboardVC setValue:[NSNumber numberWithInt:1] forKeyPath:@"faultSegmentedControl.selectedSegmentIndex"];
    [_faultDashboardVC setValue:[NSArray arrayWithObject:@"testObject"] forKeyPath:@"completedFaultData"];
    
    [_faultDashboardVC manageAPIRequestBasedOnSegmentSelection];
    OCMVerify([dashboardMockObj fetchCompletedFaultsAutomatically]);
    [dashboardMockObj stopMocking];
}

#pragma mark -
#pragma mark fetchFaultDashBoardAPIWithPageIndex

- (void) testFetchFaultDashBoardAPIWithPageIndexForOpenFault {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    id dlmDashboardMockObj = OCMClassMock([DLMFaultDashBoardScreen class]);
    [_faultDashboardVC setValue:[NSNumber numberWithInt:TrackFaultDashBoardTypeOpenOrder] forKey:@"trackFaultType"];
    [_faultDashboardVC setValue:dlmDashboardMockObj forKey:@"openFaultViewModel"];
    
    [_faultDashboardVC fetchFaultDashBoardAPIWithPageIndex:1];
    
    OCMVerify([dlmDashboardMockObj fetchFaultDashBoardDetailsForLoggedInUserWithPageSize:3 pageIndex:1 tabID:TrackFaultDashBoardTypeOpenOrder]);
    [dlmDashboardMockObj stopMocking];
    [dashboardMockObj stopMocking];
}

- (void) testFetchFaultDashBoardAPIWithPageIndexForCompletedFault {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    id dlmDashboardMockObj = OCMClassMock([DLMFaultDashBoardScreen class]);
    [_faultDashboardVC setValue:[NSNumber numberWithInt:TrackFaultDashBoardTypeCompletedOrder] forKey:@"trackFaultType"];
    [_faultDashboardVC setValue:dlmDashboardMockObj forKey:@"recentlyUsedFaultViewModel"];
    
    [_faultDashboardVC fetchFaultDashBoardAPIWithPageIndex:1];
    
    OCMVerify([dlmDashboardMockObj fetchFaultDashBoardDetailsForLoggedInUserWithPageSize:3 pageIndex:1 tabID:TrackFaultDashBoardTypeCompletedOrder]);
    [dlmDashboardMockObj stopMocking];
    [dashboardMockObj stopMocking];
}

#pragma mark -
#pragma mark fetchOpenFaultsAutomatically

- (void) testFetchOpenFaultsAutomaticallyWithNetworkConnection {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    id footerMock = OCMClassMock([BTFaultDashBoardTableFooterView class]);
    id appmanagerClassMock = OCMClassMock([AppManager class]);
    OCMStub([appmanagerClassMock isInternetConnectionAvailable]).andReturn(YES);
    
    int pageindex = 2;
    int totalSize = 4;
    [_faultDashboardVC setValue:footerMock forKey:@"faultDashboardFooterView"];
    [_faultDashboardVC setValue:[NSNumber numberWithInt:pageindex] forKeyPath:@"_openFaultViewModel.pageIndex"];
    [_faultDashboardVC setValue:[NSNumber numberWithInt:totalSize] forKeyPath:@"_openFaultViewModel.totalSize"];
    [_faultDashboardVC setValue:[NSArray arrayWithObjects:@"testObject1",@"testObject2", nil] forKey:@"openFaultData"];
    
    [_faultDashboardVC fetchOpenFaultsAutomatically];
    
    OCMVerify([footerMock hideRetryView]);
    OCMVerify([dashboardMockObj hideShowFooterIndicatorView:NO]);
    OCMVerify([footerMock showLoaderContainerView]);
    OCMVerify([dashboardMockObj resizeTableViewFooterToFit]);
    OCMVerify([dashboardMockObj fetchFaultDashBoardAPIWithPageIndex:pageindex]);
    
    [dashboardMockObj stopMocking];
    [footerMock stopMocking];
    [appmanagerClassMock stopMocking];
}

- (void) testFetchOpenFaultsAutomaticallyWithNoNetworkConnection {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    id footerMock = OCMClassMock([BTFaultDashBoardTableFooterView class]);
    id appmanagerClassMock = OCMClassMock([AppManager class]);
    OCMStub([appmanagerClassMock isInternetConnectionAvailable]).andReturn(NO);
    
    int pageindex = 2;
    int totalSize = 4;
    [_faultDashboardVC setValue:footerMock forKey:@"faultDashboardFooterView"];
    [_faultDashboardVC setValue:[NSNumber numberWithInt:pageindex] forKeyPath:@"_openFaultViewModel.pageIndex"];
    [_faultDashboardVC setValue:[NSNumber numberWithInt:totalSize] forKeyPath:@"_openFaultViewModel.totalSize"];
    [_faultDashboardVC setValue:[NSArray arrayWithObjects:@"testObject1",@"testObject2", nil] forKey:@"openFaultData"];
    
    [_faultDashboardVC fetchOpenFaultsAutomatically];
    
    OCMVerify([footerMock showLoaderContainerView]);
    OCMVerify([footerMock showRetryView]);
    OCMVerify([dashboardMockObj resizeTableViewFooterToFit]);
    OCMVerify([footerMock showRetryViewWithErrorMessage:[OCMArg any]]);
    OCMVerify([appmanagerClassMock trackNoInternetErrorOnPage:[OCMArg any]]);
    
    [dashboardMockObj stopMocking];
    [footerMock stopMocking];
    [appmanagerClassMock stopMocking];
}

#pragma mark -
#pragma mark fetchCompletedFaultsAutomatically

- (void) testFetchCompletedFaultsAutomaticallyWithNetworkConnection {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    id footerMock = OCMClassMock([BTFaultDashBoardTableFooterView class]);
    id appmanagerClassMock = OCMClassMock([AppManager class]);
    OCMStub([appmanagerClassMock isInternetConnectionAvailable]).andReturn(YES);
    
    int pageindex = 2;
    int totalSize = 4;
    [_faultDashboardVC setValue:footerMock forKey:@"faultDashboardFooterView"];
    [_faultDashboardVC setValue:[NSNumber numberWithInt:pageindex] forKeyPath:@"_recentlyUsedFaultViewModel.pageIndex"];
    [_faultDashboardVC setValue:[NSNumber numberWithInt:totalSize] forKeyPath:@"_recentlyUsedFaultViewModel.totalSize"];
    [_faultDashboardVC setValue:[NSArray arrayWithObjects:@"testObject1",@"testObject2", nil] forKey:@"completedFaultData"];
    
    [_faultDashboardVC fetchCompletedFaultsAutomatically];
    
    OCMVerify([footerMock hideRetryView]);
    OCMVerify([dashboardMockObj hideShowFooterIndicatorView:NO]);
    OCMVerify([footerMock showLoaderContainerView]);
    OCMVerify([dashboardMockObj resizeTableViewFooterToFit]);
    OCMVerify([dashboardMockObj fetchFaultDashBoardAPIWithPageIndex:pageindex]);
    
    [dashboardMockObj stopMocking];
    [footerMock stopMocking];
    [appmanagerClassMock stopMocking];
}

- (void) testFetchCompletedFaultsAutomaticallyWithNoNetworkConnection {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    id footerMock = OCMClassMock([BTFaultDashBoardTableFooterView class]);
    id appmanagerClassMock = OCMClassMock([AppManager class]);
    OCMStub([appmanagerClassMock isInternetConnectionAvailable]).andReturn(NO);
    
    int pageindex = 2;
    int totalSize = 4;
    [_faultDashboardVC setValue:footerMock forKey:@"faultDashboardFooterView"];
    [_faultDashboardVC setValue:[NSNumber numberWithInt:pageindex] forKeyPath:@"_recentlyUsedFaultViewModel.pageIndex"];
    [_faultDashboardVC setValue:[NSNumber numberWithInt:totalSize] forKeyPath:@"_recentlyUsedFaultViewModel.totalSize"];
    [_faultDashboardVC setValue:[NSArray arrayWithObjects:@"testObject1",@"testObject2", nil] forKey:@"completedFaultData"];
    
    [_faultDashboardVC fetchCompletedFaultsAutomatically];
    
    OCMVerify([footerMock showLoaderContainerView]);
    OCMVerify([footerMock showRetryView]);
    OCMVerify([dashboardMockObj resizeTableViewFooterToFit]);
    OCMVerify([footerMock showRetryViewWithErrorMessage:[OCMArg any]]);
    OCMVerify([appmanagerClassMock trackNoInternetErrorOnPage:[OCMArg any]]);
    
    [dashboardMockObj stopMocking];
    [footerMock stopMocking];
    [appmanagerClassMock stopMocking];
}

#pragma mark -
#pragma mark cancelOpenFaultAPI

- (void) testCancelOpenFaultAPI {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    id footerMockObj = OCMClassMock([BTFaultDashBoardTableFooterView class]);
    id dlmDashboardMockObj = OCMClassMock([DLMFaultDashBoardScreen class]);
    
    [_faultDashboardVC setValue:footerMockObj forKey:@"faultDashboardFooterView"];
    [_faultDashboardVC setValue:dlmDashboardMockObj forKey:@"openFaultViewModel"];
    
    [_faultDashboardVC cancelOpenFaultAPI];
    
    OCMVerify([dashboardMockObj hideShowFooterIndicatorView:YES]);
    OCMVerify([footerMockObj hideLoaderContainerView]);
    OCMVerify([dashboardMockObj resizeTableViewFooterToFit]);
    OCMVerify([dlmDashboardMockObj cancelFaultDashBoardAPIRequest]);
    
    [dashboardMockObj stopMocking];
    [footerMockObj stopMocking];
    [dlmDashboardMockObj stopMocking];
}

#pragma mark -
#pragma mark cancelCompletedFaultAPI

- (void) testCancelCompltedFaultAPI {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    id footerMockObj = OCMClassMock([BTFaultDashBoardTableFooterView class]);
    id dlmDashboardMockObj = OCMClassMock([DLMFaultDashBoardScreen class]);
    
    [_faultDashboardVC setValue:footerMockObj forKey:@"faultDashboardFooterView"];
    [_faultDashboardVC setValue:dlmDashboardMockObj forKey:@"recentlyUsedFaultViewModel"];
    
    [_faultDashboardVC cancelCompletedFaultAPI];
    
    OCMVerify([dashboardMockObj hideShowFooterIndicatorView:YES]);
    OCMVerify([footerMockObj hideLoaderContainerView]);
    OCMVerify([dashboardMockObj resizeTableViewFooterToFit]);
    OCMVerify([dlmDashboardMockObj cancelFaultDashBoardAPIRequest]);
    
    [dashboardMockObj stopMocking];
    [footerMockObj stopMocking];
    [dlmDashboardMockObj stopMocking];
}

#pragma mark -
#pragma mark loadNextOpenFaults

- (void) testLoadNextOpenFaultsWithFooterViewCreation {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];

    [_faultDashboardVC setValue:[NSNumber numberWithBool:NO] forKey:@"_isFooterViewCreated"];

    [_faultDashboardVC loadNextOpenFaults];
    
    OCMVerify([dashboardMockObj createFooterView]);
    
    [dashboardMockObj stopMocking];
}

- (void) testLoadNextOpenFaultsWithNetworkConnection {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    id footerMock = OCMClassMock([BTFaultDashBoardTableFooterView class]);
    id appmanagerClassMock = OCMClassMock([AppManager class]);
    OCMStub([appmanagerClassMock isInternetConnectionAvailable]).andReturn(YES);
    
    int pageindex = 2;
    int totalSize = 4;
    [_faultDashboardVC setValue:footerMock forKey:@"faultDashboardFooterView"];
    [_faultDashboardVC setValue:[NSNumber numberWithInt:pageindex] forKeyPath:@"_openFaultViewModel.pageIndex"];
    [_faultDashboardVC setValue:[NSNumber numberWithInt:totalSize] forKeyPath:@"_openFaultViewModel.totalSize"];
    [_faultDashboardVC setValue:[NSArray arrayWithObjects:@"testObject1",@"testObject2", nil] forKey:@"openFaultData"];
    [_faultDashboardVC setValue:[NSNumber numberWithBool:YES] forKey:@"_isFooterViewCreated"];
    
    [_faultDashboardVC loadNextOpenFaults];
    
    OCMVerify([dashboardMockObj hideShowFooterIndicatorView:NO]);
    OCMVerify([dashboardMockObj resizeTableViewFooterToFit]);
    OCMVerify([footerMock showLoaderContainerView]);
    
    //verifying a method in dispatch_async & mainqueue
//    [[(OCMockObject *)dashboardMockObj expect] fetchFaultDashBoardAPIWithPageIndex:pageindex];
//    [dashboardMockObj view];
//    [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.09]];
//    [dashboardMockObj verify];
    
    [dashboardMockObj stopMocking];
    [footerMock stopMocking];
    [appmanagerClassMock stopMocking];

}

- (void) testLoadNextOpenFaultsWithNoNetworkConnection {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    id footerMock = OCMClassMock([BTFaultDashBoardTableFooterView class]);
    id appmanagerClassMock = OCMClassMock([AppManager class]);
    OCMStub([appmanagerClassMock isInternetConnectionAvailable]).andReturn(NO);
    
    int pageindex = 2;
    int totalSize = 4;
    [_faultDashboardVC setValue:footerMock forKey:@"faultDashboardFooterView"];
    [_faultDashboardVC setValue:[NSNumber numberWithInt:pageindex] forKeyPath:@"_openFaultViewModel.pageIndex"];
    [_faultDashboardVC setValue:[NSNumber numberWithInt:totalSize] forKeyPath:@"_openFaultViewModel.totalSize"];
    [_faultDashboardVC setValue:[NSArray arrayWithObjects:@"testObject1",@"testObject2", nil] forKey:@"openFaultData"];
    [_faultDashboardVC setValue:[NSNumber numberWithBool:YES] forKey:@"_isFooterViewCreated"];
    
    [_faultDashboardVC loadNextOpenFaults];
    
    OCMVerify([dashboardMockObj hideShowFooterIndicatorView:YES]);
    OCMVerify([footerMock showRetryViewWithErrorMessage:[OCMArg any]]);
    OCMVerify([footerMock showLoaderContainerView]);
    OCMVerify([dashboardMockObj resizeTableViewFooterToFit]);
    
    [dashboardMockObj stopMocking];
    [footerMock stopMocking];
    [appmanagerClassMock stopMocking];
    
}

- (void) testLoadNextOpenFaultsWhenNoLoadingRequired {
    
    id dashboardMockObj = [OCMockObject partialMockForObject:_faultDashboardVC];
    id footerMock = OCMClassMock([BTFaultDashBoardTableFooterView class]);
    
    int pageindex = 2;
    int totalSize = 4;
    [_faultDashboardVC setValue:footerMock forKey:@"faultDashboardFooterView"];
    [_faultDashboardVC setValue:[NSNumber numberWithInt:pageindex] forKeyPath:@"_openFaultViewModel.pageIndex"];
    [_faultDashboardVC setValue:[NSNumber numberWithInt:totalSize] forKeyPath:@"_openFaultViewModel.totalSize"];
    [_faultDashboardVC setValue:[NSArray arrayWithObjects:@"testObject1",@"testObject2",@"testObject1",@"testObject2", nil] forKey:@"openFaultData"];
    [_faultDashboardVC setValue:[NSNumber numberWithBool:YES] forKey:@"_isFooterViewCreated"];
    
    [_faultDashboardVC loadNextOpenFaults];
    
    OCMVerify([dashboardMockObj hideShowFooterIndicatorView:YES]);
    OCMVerify([footerMock hideRetryView]);
    OCMVerify([footerMock hideLoaderContainerView]);
    OCMVerify([dashboardMockObj resizeTableViewFooterToFit]);
    
    [dashboardMockObj stopMocking];
    [footerMock stopMocking];
    
}

@end
