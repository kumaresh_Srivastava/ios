//
//  BTFaultTrackerSummaryViewControllerTests.m
//  BTBusinessApp
//
//  Created by Rohini Kumar on 23/08/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "BTFaultTrackerSummaryViewController.h"
#import "AppConstants.h"
#import "AppManager.h"
#import "CustomSpinnerView.h"
#import "DLMFaultServiceIDSummaryScreen.h"
#import "BTRetryView.h"
#import "BTNoFaultSummaryView.h"
#import "OmnitureManager.h"

@interface BTFaultTrackerSummaryViewControllerTests : XCTestCase
@property (nonatomic) BTFaultTrackerSummaryViewController *fualtTrackerSummaryVC;
@end

@interface BTFaultTrackerSummaryViewController (test)
- (void)fetchServiceIDSummaryAPI;

- (void)showRetryViewWithInternetStrip:(BOOL)needToShowInternetStrip;
- (void)hideLoadingItems:(BOOL)isHide;
- (void)trackPage;
- (void)trackPage:(NSString *)pageName;
- (void)hideRetryItems:(BOOL)isHide;
- (void)hideEmptyDashboardItems:(BOOL)isHide;
- (NSDictionary *)getDataDictForIndex:(NSInteger)index;
- (NSArray *)getOpenFaultArrayfromDataDict:(NSDictionary *)dict;
- (NSArray *)getCloseFaultArrayfromDataDict:(NSDictionary *)dict;
- (void)preparingDataFromDataDictArray:(NSArray *)dataDictArray;
@end

@implementation BTFaultTrackerSummaryViewControllerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];
    _fualtTrackerSummaryVC = (BTFaultTrackerSummaryViewController *)[storyboard instantiateViewControllerWithIdentifier:kStoryboardViewFaultTrackerSummary];
    XCTAssertNotNil(_fualtTrackerSummaryVC.view);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    _fualtTrackerSummaryVC = nil;
}

#pragma mark -
#pragma mark fetchServiceIDSummaryAPI 

- (void) testFetchServiceIDSummaryAPIWithNoNetworkConnectionAvailable {
    
    id partialMock = OCMPartialMock(_fualtTrackerSummaryVC);
    id appmanagerClassMock = OCMClassMock([AppManager class]);
    OCMStub([appmanagerClassMock isInternetConnectionAvailable]).andReturn(NO);
    
    [_fualtTrackerSummaryVC fetchServiceIDSummaryAPI];
    OCMVerify([partialMock showRetryViewWithInternetStrip:YES]);
    OCMVerify([appmanagerClassMock trackNoInternetErrorOnPage:OCMOCK_ANY]);
    
    [partialMock stopMocking];
    [appmanagerClassMock stopMocking];
}

- (void) testFetchServiceIDSummaryAPIWithNetworkConnectionAvailable {
    
    id partialMock = OCMPartialMock(_fualtTrackerSummaryVC);
    id loadingMock = OCMClassMock([CustomSpinnerView class]);
    id dlmMock = OCMClassMock([DLMFaultServiceIDSummaryScreen class]);
    id appmanagerClassMock = OCMClassMock([AppManager class]);
    OCMStub([appmanagerClassMock isInternetConnectionAvailable]).andReturn(YES);
    
    [_fualtTrackerSummaryVC setValue:loadingMock forKey:@"_loadingView"];
    [_fualtTrackerSummaryVC setValue:dlmMock forKey:@"serviceIDSummaryModel"];
    
    [_fualtTrackerSummaryVC fetchServiceIDSummaryAPI];
    
    OCMVerify([partialMock hideLoadingItems:NO]);
    OCMVerify([loadingMock startAnimatingLoadingIndicatorView]);
    OCMVerify([dlmMock fetchFaultServiceIDSummaryDetailsForLoggedInUserWithServiceID:OCMOCK_ANY]);
    
    [partialMock stopMocking];
    [appmanagerClassMock stopMocking];
}

#pragma mark -
#pragma mark trackPage

- (void) testTrackPageWith_currentSelectedSegmentedIndexZeroAndWithFaultArray {
    
    id partialMock = OCMPartialMock(_fualtTrackerSummaryVC);
    
    [_fualtTrackerSummaryVC setValue:[NSNumber numberWithInteger:0] forKey:@"_currentSelectedSegmentedIndex"];
    [_fualtTrackerSummaryVC setValue:[NSArray arrayWithObjects:@"First", @"Second", nil] forKey:@"_faultArray"];
    
    [_fualtTrackerSummaryVC trackPage];
    OCMVerify([partialMock trackPage:OCMOCK_ANY]);
    [partialMock stopMocking];
}

- (void) testTrackPageWith_currentSelectedSegmentedIndexZeroAndWithNoFaultArray {
    
    id partialMock = OCMPartialMock(_fualtTrackerSummaryVC);
    
    [_fualtTrackerSummaryVC setValue:[NSNumber numberWithInteger:0] forKey:@"_currentSelectedSegmentedIndex"];
    [_fualtTrackerSummaryVC setValue:[NSArray array] forKey:@"_faultArray"];
    
    [_fualtTrackerSummaryVC trackPage];
    OCMVerify([partialMock trackPage:OCMOCK_ANY]);
    [partialMock stopMocking];
}

- (void) testTrackPageWith_currentSelectedSegmentedIndexNonZeroAndWithFaultArray {
    
    id partialMock = OCMPartialMock(_fualtTrackerSummaryVC);
    
    [_fualtTrackerSummaryVC setValue:[NSNumber numberWithInteger:1] forKey:@"_currentSelectedSegmentedIndex"];
    [_fualtTrackerSummaryVC setValue:[NSArray arrayWithObjects:@"First", @"Second", nil] forKey:@"_faultArray"];
    
    [_fualtTrackerSummaryVC trackPage];
    OCMVerify([partialMock trackPage:OCMOCK_ANY]);
    [partialMock stopMocking];
}

- (void) testTrackPageWith_currentSelectedSegmentedIndexNonZeroAndWithNoFaultArray {
    
    id partialMock = OCMPartialMock(_fualtTrackerSummaryVC);
    
    [_fualtTrackerSummaryVC setValue:[NSNumber numberWithInteger:1] forKey:@"_currentSelectedSegmentedIndex"];
    [_fualtTrackerSummaryVC setValue:[NSArray array] forKey:@"_faultArray"];
    
    [_fualtTrackerSummaryVC trackPage];
    OCMVerify([partialMock trackPage:OCMOCK_ANY]);
    [partialMock stopMocking];
}

#pragma mark -
#pragma mark hideLoadingItems 

- (void) testHideLoadingItems {
    id loadingMock = OCMClassMock([CustomSpinnerView class]);
    [_fualtTrackerSummaryVC setValue:loadingMock forKey:@"_loadingView"];
    [_fualtTrackerSummaryVC hideLoadingItems:YES];
    OCMVerify([loadingMock setHidden:YES]);
    [loadingMock stopMocking];
}

#pragma mark -
#pragma mark hideRetryItems

- (void) testHideRetryItems {
    
    id retryMock = OCMClassMock([BTRetryView class]);
    [_fualtTrackerSummaryVC setValue:retryMock forKey:@"_retryView"];
    [_fualtTrackerSummaryVC hideRetryItems:YES];
    OCMVerify([retryMock setRetryViewDelegate:OCMOCK_ANY]);
    OCMVerify([retryMock setHidden:YES]);
    [retryMock stopMocking];
}

#pragma mark -
#pragma mark hideEmptyDashboardItems

- (void) testHideEmptyDashboardItems {
    
    id emptyMock = OCMClassMock([BTNoFaultSummaryView class]);
    [_fualtTrackerSummaryVC setValue:emptyMock forKey:@"_emptyDashboardView"];
    [_fualtTrackerSummaryVC hideEmptyDashboardItems:YES];
    OCMVerify([emptyMock setDelegate:OCMOCK_ANY]);
    OCMVerify([emptyMock setHidden:YES]);
    [emptyMock stopMocking];
}

#pragma mark -
#pragma mark getDataDictForIndex 

- (void) testGetDataDictForIndex {
    
    [_fualtTrackerSummaryVC setValue:[NSArray arrayWithObjects:[NSDictionary dictionaryWithObject:@"FirstDict" forKey:@"firstKey"], [NSDictionary dictionaryWithObject:@"SecondDict" forKey:@"secondKey"], nil] forKey:@"_faultListArray"];
    
    NSDictionary *testDict = [_fualtTrackerSummaryVC getDataDictForIndex:0];
    XCTAssertNotNil(testDict,@"Test Case failed, testDict should have non nil");
}

#pragma mark -
#pragma mark getOpenFaultArrayfromDataDict

- (void) testGetOpenFaultArrayfromDataDictNonNil {
    
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[NSArray arrayWithObjects:@"First", @"Second", nil] forKey:@"openFaults"];
    NSArray *testArray = [_fualtTrackerSummaryVC getOpenFaultArrayfromDataDict:dict];
    XCTAssertNotNil(testArray,@"Test case failed, testArray should have non nil");
}

- (void) testGetOpenFaultArrayfromDataDictNilOutput {
    
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[NSArray arrayWithObjects:@"First", @"Second", nil] forKey:@"open"];
    NSArray *testArray = [_fualtTrackerSummaryVC getOpenFaultArrayfromDataDict:dict];
    XCTAssertNil(testArray,@"Test case failed, testArray should have nil");
}

#pragma mark -
#pragma mark getOpenFaultArrayfromDataDict

- (void) testGetCloseFaultArrayfromDataDictNonNil {
    
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[NSArray arrayWithObjects:@"First", @"Second", nil] forKey:@"closedFaults"];
    NSArray *testArray = [_fualtTrackerSummaryVC getCloseFaultArrayfromDataDict:dict];
    XCTAssertNotNil(testArray,@"Test case failed, testArray should have non nil");
}

- (void) testGetCloseFaultArrayfromDataDictNilOutput {
    
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[NSArray arrayWithObjects:@"First", @"Second", nil] forKey:@"closed"];
    NSArray *testArray = [_fualtTrackerSummaryVC getCloseFaultArrayfromDataDict:dict];
    XCTAssertNil(testArray,@"Test case failed, testArray should have nil");
}

#pragma mark -
#pragma mark preparingDataFromDataDictArray 

- (void) testPreparingDataFromDataDictArrayWith_currentSelectedSegmentedIndexZeroWithArrayCountOne {
    
    NSArray *dictArray = [NSArray arrayWithObject:[NSDictionary dictionaryWithObject:[NSArray arrayWithObjects:@"First", @"Second", nil] forKey:@"openFaults"]];
    
    [_fualtTrackerSummaryVC setValue:[NSNumber numberWithInteger:0] forKey:@"_currentSelectedSegmentedIndex"];
    [_fualtTrackerSummaryVC  preparingDataFromDataDictArray:dictArray];
    NSArray *testArray = [_fualtTrackerSummaryVC valueForKey:@"_faultListArray"];
    XCTAssertTrue(dictArray.count == testArray.count, @"Test case failed, dictArray should have elements");
}

- (void) testPreparingDataFromDataDictArrayWith_currentSelectedSegmentedIndexZeroWithArrayWrongData {
    
    NSArray *dictArray = [NSArray arrayWithObject:[NSDictionary dictionaryWithObject:[NSArray arrayWithObjects:@"First", @"Second", nil] forKey:@"open"]];
    [_fualtTrackerSummaryVC setValue:[NSNumber numberWithInteger:0] forKey:@"_currentSelectedSegmentedIndex"];
    [_fualtTrackerSummaryVC  preparingDataFromDataDictArray:dictArray];
    NSArray *testArray = [_fualtTrackerSummaryVC valueForKey:@"_faultListArray"];
    XCTAssertTrue(dictArray.count != testArray.count, @"Test case failed, dictArray should have elements");
}

- (void) testPreparingDataFromDataDictArrayWith_currentSelectedSegmentedIndexZeroWithArrayCountMoreThanOne {
    
    NSArray *dictArray = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObject:[NSArray arrayWithObjects:@"First", @"Second", nil] forKey:@"openFaults"],[NSDictionary dictionaryWithObject:[NSArray arrayWithObjects:@"First", @"Second", nil] forKey:@"openFaults"],nil];
    
    [_fualtTrackerSummaryVC setValue:[NSNumber numberWithInteger:0] forKey:@"_currentSelectedSegmentedIndex"];
    [_fualtTrackerSummaryVC  preparingDataFromDataDictArray:dictArray];
    NSArray *testArray = [_fualtTrackerSummaryVC valueForKey:@"_faultListArray"];
    XCTAssertTrue(dictArray.count == testArray.count, @"Test case failed, dictArray should have elements");
}

- (void) testPreparingDataFromDataDictArrayWith_currentSelectedSegmentedIndexZeroWithArrayCountMorethanOneWithWrongData {
    
    NSArray *dictArray = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObject:[NSArray arrayWithObjects:@"First", @"Second", nil] forKey:@"open"], [NSDictionary dictionaryWithObject:[NSArray arrayWithObjects:@"First", @"Second", nil] forKey:@"open"],nil];
    [_fualtTrackerSummaryVC setValue:[NSNumber numberWithInteger:0] forKey:@"_currentSelectedSegmentedIndex"];
    [_fualtTrackerSummaryVC  preparingDataFromDataDictArray:dictArray];
    NSArray *testArray = [_fualtTrackerSummaryVC valueForKey:@"_faultListArray"];
    XCTAssertTrue(dictArray.count != testArray.count, @"Test case failed, dictArray should have elements");
}

- (void) testPreparingDataFromDataDictArrayWith_currentSelectedSegmentedIndexOneWithArrayCountOne {
    
    NSArray *dictArray = [NSArray arrayWithObject:[NSDictionary dictionaryWithObject:[NSArray arrayWithObjects:@"First", @"Second", nil] forKey:@"closedFaults"]];
    
    [_fualtTrackerSummaryVC setValue:[NSNumber numberWithInteger:1] forKey:@"_currentSelectedSegmentedIndex"];
    [_fualtTrackerSummaryVC  preparingDataFromDataDictArray:dictArray];
    NSArray *testArray = [_fualtTrackerSummaryVC valueForKey:@"_faultListArray"];
    XCTAssertTrue(dictArray.count == testArray.count, @"Test case failed, dictArray should have elements");
}

- (void) testPreparingDataFromDataDictArrayWith_currentSelectedSegmentedIndexOneWithArrayWrongData {
    
    NSArray *dictArray = [NSArray arrayWithObject:[NSDictionary dictionaryWithObject:[NSArray arrayWithObjects:@"First", @"Second", nil] forKey:@"closed"]];
    [_fualtTrackerSummaryVC setValue:[NSNumber numberWithInteger:1] forKey:@"_currentSelectedSegmentedIndex"];
    [_fualtTrackerSummaryVC  preparingDataFromDataDictArray:dictArray];
    NSArray *testArray = [_fualtTrackerSummaryVC valueForKey:@"_faultListArray"];
    XCTAssertTrue(dictArray.count != testArray.count, @"Test case failed, dictArray should have elements");
}

- (void) testPreparingDataFromDataDictArrayWith_currentSelectedSegmentedIndexOneWithArrayCountMoreThanOne {
    
    NSArray *dictArray = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObject:[NSArray arrayWithObjects:@"First", @"Second", nil] forKey:@"closedFaults"],[NSDictionary dictionaryWithObject:[NSArray arrayWithObjects:@"First", @"Second", nil] forKey:@"closedFaults"],nil];
    
    [_fualtTrackerSummaryVC setValue:[NSNumber numberWithInteger:1] forKey:@"_currentSelectedSegmentedIndex"];
    [_fualtTrackerSummaryVC  preparingDataFromDataDictArray:dictArray];
    NSArray *testArray = [_fualtTrackerSummaryVC valueForKey:@"_faultListArray"];
    XCTAssertTrue(dictArray.count == testArray.count, @"Test case failed, dictArray should have elements");
}

- (void) testPreparingDataFromDataDictArrayWith_currentSelectedSegmentedIndexOneWithArrayCountMorethanOneWithWrongData {
    
    NSArray *dictArray = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObject:[NSArray arrayWithObjects:@"First", @"Second", nil] forKey:@"closed"], [NSDictionary dictionaryWithObject:[NSArray arrayWithObjects:@"First", @"Second", nil] forKey:@"closed"],nil];
    [_fualtTrackerSummaryVC setValue:[NSNumber numberWithInteger:1] forKey:@"_currentSelectedSegmentedIndex"];
    [_fualtTrackerSummaryVC  preparingDataFromDataDictArray:dictArray];
    NSArray *testArray = [_fualtTrackerSummaryVC valueForKey:@"_faultListArray"];
    XCTAssertTrue(dictArray.count != testArray.count, @"Test case failed, dictArray should have elements");
}

#pragma mark -
#pragma mark Omniture

#pragma mark trackPage 

- (void) testTrackPage {
    
    id omnitureClassMock = OCMClassMock([OmnitureManager class]);
    [_fualtTrackerSummaryVC trackPage:@"Sample page"];
    
    OCMVerify([omnitureClassMock trackPage:OCMOCK_ANY withContextInfo:OCMOCK_ANY]);
    [omnitureClassMock stopMocking];
}

@end
