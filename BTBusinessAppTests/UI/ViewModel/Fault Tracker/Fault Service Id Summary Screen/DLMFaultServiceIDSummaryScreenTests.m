//
//  DLMFaultServiceIDSummaryScreenTests.m
//  BTBusinessApp
//
//  Created by Rohini Kumar on 28/08/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "DLMFaultServiceIDSummaryScreen.h"
#import "NLFaultServiceIDSummaryWebService.h"
#import "BTLiveChatAvailabilityChecker.h"

@interface DLMFaultServiceIDSummaryScreenTests : XCTestCase
@property (nonatomic) DLMFaultServiceIDSummaryScreen *faultSummaryVM;
@end

@interface DLMFaultServiceIDSummaryScreen (test)
- (void)setupToFetchFaultServiceIDSummaryDetailsForLoggedInUserWithServiceID:(NSString *)serviceID;
- (void) callFaultServiceIDSummaryDetails;
- (void)setupToCheckForLiveChatAvailibilityForFault;
- (void) callLiveChatAvailableSlots;

@end

@implementation DLMFaultServiceIDSummaryScreenTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _faultSummaryVM = [[DLMFaultServiceIDSummaryScreen alloc] init];
    XCTAssertNotNil(_faultSummaryVM);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    _faultSummaryVM = nil;
}

#pragma mark -
#pragma mark fetchFaultServiceIDSummaryDetailsForLoggedInUserWithServiceID 

- (void) testFetchFaultServiceIDSummaryDetailsForLoggedInUserWithServiceID {
    
    id partialMock = OCMPartialMock(_faultSummaryVM);
    NSString *serviceID = @"sampleServiceID";
    [_faultSummaryVM fetchFaultServiceIDSummaryDetailsForLoggedInUserWithServiceID:serviceID];
    OCMVerify([partialMock setupToFetchFaultServiceIDSummaryDetailsForLoggedInUserWithServiceID:OCMOCK_ANY]);
    OCMVerify([partialMock callFaultServiceIDSummaryDetails]);
    NSString *faultReference = [_faultSummaryVM valueForKey:@"faultReference"];
    XCTAssertTrue([faultReference isEqualToString:serviceID],@"Test case failed, both objects should have same value");
    [partialMock stopMocking];
}

#pragma mark -
#pragma mark callFaultServiceIDSummaryDetails 

- (void) testCallFaultServiceIDSummaryDetails {
    
    id classMockNL = OCMClassMock([NLFaultServiceIDSummaryWebService class]);
    [_faultSummaryVM setValue:classMockNL forKey:@"getFaultServiceIDSummaryWebService"];
    [_faultSummaryVM callFaultServiceIDSummaryDetails];
    
    OCMVerify([classMockNL resume]);
    [classMockNL stopMocking];
}

#pragma mark -
#pragma mark checkForLiveChatAvailibilityForFault

- (void) testCheckForLiveChatAvailibilityForFault {
    
    id partialMock = OCMPartialMock(_faultSummaryVM);
    [_faultSummaryVM checkForLiveChatAvailibilityForFault];
    
    OCMVerify([partialMock setupToCheckForLiveChatAvailibilityForFault]);
    OCMVerify([partialMock callLiveChatAvailableSlots]);
    [partialMock stopMocking];
}

#pragma mark -
#pragma mark callLiveChatAvailableSlots 

- (void) testCallLiveChatAvailableSlots {
 
    id classMock = OCMClassMock([BTLiveChatAvailabilityChecker class]);
    [_faultSummaryVM setValue:classMock forKey:@"_liveChatAvailabityChecker"];
    [_faultSummaryVM callLiveChatAvailableSlots];
    
    OCMVerify([classMock getLiveChatAvailableSlots]);
    [classMock stopMocking];
}

#pragma mark -
#pragma mark cancelServiceIDSummaryAPIRequest

- (void) testCancelServiceIDSummaryAPIRequest {
    id classMock = OCMClassMock([NLFaultServiceIDSummaryWebService class]);
    [_faultSummaryVM setValue:classMock forKey:@"getFaultServiceIDSummaryWebService"];
    [_faultSummaryVM cancelServiceIDSummaryAPIRequest];
    OCMVerify([classMock cancel]);
    id faultSummaryWebservice = [_faultSummaryVM valueForKey:@"getFaultServiceIDSummaryWebService"];
    XCTAssertNil(faultSummaryWebservice,@"Test Case Failed, faultSummaryWebservice should have nil");
    [classMock stopMocking];
}

#pragma mark -
#pragma mark cancelChatAvailabilityCheckerAPI

- (void) testCancelChatAvailabilityCheckerAPI {
    id classMock = OCMClassMock([BTLiveChatAvailabilityChecker class]);
    [_faultSummaryVM setValue:classMock forKey:@"_liveChatAvailabityChecker"];
    [_faultSummaryVM cancelChatAvailabilityCheckerAPI];
    OCMVerify([classMock cancel]);
    id faultSummaryWebservice = [_faultSummaryVM valueForKey:@"_liveChatAvailabityChecker"];
    XCTAssertNil(faultSummaryWebservice,@"Test Case Failed, faultSummaryWebservice should have nil");
    [classMock stopMocking];
}

@end
