//
//  BTHomeViewControllerTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/3/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BTHomeViewController.h"
#import "AppConstants.h"
#import "OCMock/OCMock.h"
#import "DLMHomeScreen.h"
#import "OmnitureManager.h"
#import "AppManager.h"
#import "BTCug.h"

@interface BTHomeViewControllerTests : XCTestCase

@property (nonatomic) BTHomeViewController *homeViewController;

@end

@interface BTHomeViewController (test)

- (void)updateUserAccountNotificationsData;
- (void)noUnReadItemsLeftInNotifications;
- (void)setUpGridsUI;
- (void)updateGridsIfNeeded;
- (void)fetchUserDetails;
- (void)showLoaderView;
- (void)hideRetryView;
- (void)fetchUserAccountNotifications;
- (void)checkForUpdatedScreenTitle;
- (void)updateScreenTitle;
- (void)trackPage;

@end

@implementation BTHomeViewControllerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:kStoryboardMainStoryboardFile bundle:nil];
    
    _homeViewController = (BTHomeViewController *)[storyboard instantiateViewControllerWithIdentifier:kStoryboardViewHome];
    
    XCTAssertNotNil(_homeViewController.view);
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _homeViewController = nil;
}

#pragma mark - Omniture related methods

- (void)testTrackPage {
    
    id mockOMClass = OCMClassMock([OmnitureManager class]);
    
    [_homeViewController trackPage];
    
    OCMVerify([mockOMClass trackPage:[OCMArg any] withContextInfo:[OCMArg any]]);
    
    [mockOMClass stopMocking];
}

#pragma mark - UI related methods

- (void)testUpdateUserAccountNotificationData {
    
    BTCug *cug = [[BTCug alloc] initWithCugName:[OCMArg any] groupKey:@"invalidkey" cugRole:1 andIndexInAPIResponse:1];
    [_homeViewController setValue:cug forKeyPath:@"currentCug"];
    
    id mockDLMHome = OCMClassMock([DLMHomeScreen class]);
    id mockHomeVC = [OCMockObject partialMockForObject:_homeViewController];
    
    [_homeViewController setViewModel:mockDLMHome];
    [_homeViewController updateUserAccountNotificationsData];
    
    OCMVerify([mockHomeVC noUnReadItemsLeftInNotifications]);
    OCMVerify([mockDLMHome cancelGetUserAccountNotificationsAPICall]);
    OCMVerify([mockDLMHome fetchUserGroupNotifications]);
    
    [mockHomeVC stopMocking];
    [mockDLMHome stopMocking];
}

- (void)testUpdateGridsIfNeeded {
    
    BTCug *cug = [[BTCug alloc] initWithCugName:[OCMArg any] groupKey:@"invalidkey" cugRole:1 andIndexInAPIResponse:1];
    [_homeViewController setValue:cug forKeyPath:@"currentCug"];
    
    id mockDLMHome = OCMClassMock([DLMHomeScreen class]);
    id mockHomeVC = [OCMockObject partialMockForObject:_homeViewController];
    
    [_homeViewController setViewModel:mockDLMHome];
    [_homeViewController updateGridsIfNeeded];
    
    OCMVerify([mockHomeVC setUpGridsUI]);
    OCMVerify([mockDLMHome cancelAPIs]);
    OCMVerify([mockDLMHome fetchGridData]);
    
    [mockHomeVC stopMocking];
    [mockDLMHome stopMocking];
}

- (void)testCheckForUpdatedScreenTitle {
    
    id mockHomeVC = [OCMockObject partialMockForObject:_homeViewController];
    
    [_homeViewController checkForUpdatedScreenTitle];
    
    OCMVerify([mockHomeVC updateScreenTitle]);
    
    [mockHomeVC stopMocking];
}

#pragma mark - API call related methods

- (void)testFetchUserDetails {

    id mockDLMHome = OCMClassMock([DLMHomeScreen class]);
    id mockHomeVC = [OCMockObject partialMockForObject:_homeViewController];
    
    [_homeViewController setViewModel:mockDLMHome];
    [_homeViewController fetchUserDetails];

    OCMVerify([mockDLMHome fetchUserDetails]);
    XCTAssertTrue(_homeViewController.networkRequestInProgress, @"Value of 'networkRequestInProgress' should be YES");
    OCMVerify([mockHomeVC showLoaderView]);
    OCMVerify([mockHomeVC hideRetryView]);
    
    [mockHomeVC stopMocking];
    [mockDLMHome stopMocking];
}

- (void)testFetchUserAccountNotifications {
    
    id mockDLMHome = OCMClassMock([DLMHomeScreen class]);
    
    [_homeViewController setViewModel:mockDLMHome];
    [_homeViewController fetchUserAccountNotifications];
    
    OCMVerify([mockDLMHome fetchUserGroupNotifications]);
    
    [mockDLMHome stopMocking];
}

@end
