//
//  DLMHomeScreenTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/4/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DLMHomeScreen.h"
#import <OCMock/OCMock.h>
#import "NLQueryUserDetailsWebService.h"
#import "NLGetNotificationsWebService.h"
#import "NLFaultDashBoardWebService.h"
#import "NLTrackOrderDashBoardWebService.h"
#import "NLGetBillPaymentHistoryWebService.h"
#import "BTCug.h"
#import "AppDelegateViewModel.h"
#import "AppDelegate.h"
#import "CDApp.h"
#import "CDUser.h"
#import "CDCug+CoreDataClass.h"
#import "DLMUpdateContactInerceptScreen.h"

@interface DLMHomeScreen (test)

- (void)setupToFetchUserDetails;
- (void)fetchUserDetailsForLoggedInUser;
- (void)setupToFetchUserGroupNotifications;
- (void)fetchUserNotifications;
- (void)saveUserDataInPersistenceWithUser:(BTUser *)user;
- (void)saveInterceptorWithOrder:(NSString *)order andInterfaces:(NSString *)interfaces;
- (void)takeActionAfterFetchingUserDetailsWithGroup:(NSString *)groupKey;
- (void)fetchFaultDashBoardDetailsForLoggedInUser;
- (void)fetchOrderDashBoardDetailsForLoggedInUser;
- (void)fetchBillPaymentHistoryData;
- (void)cancelFaultDashboardAPICall;
- (void)cancelOrderDashboardAPICall;
- (void)cancelBillDashboardAPICall;
- (void)setupToFetchOrderDashboardData;
- (void)fetchOrderDashBoardData;
- (void)setupToFetchFaultDashboardData;
- (void)fetchFaultDashBoardData;
- (void)setupToFetchBillDashboardData;
- (void)fetchBillDashBoardData;
- (InterceptorType)interceptorTypeForIntercentorName:(NSString *)interceptorName;
- (void)makeAPIReqeustWithParams:(NSArray *)contactArray;
- (NSString *)getStatusMatrixForHomeScreen;

@end

@interface DLMHomeScreenTests : XCTestCase

@property (nonatomic) DLMHomeScreen *homeScreenViewModel;

@end

@implementation DLMHomeScreenTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    _homeScreenViewModel = [[DLMHomeScreen alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _homeScreenViewModel = nil;
}

- (void)testSetupToFetchUserDetails {
    
    [_homeScreenViewModel setupToFetchUserDetails];
    XCTAssertNotNil([_homeScreenViewModel valueForKey:@"queryUserDetailsWebService"], @"NLQueryUserDetailsWebService instance is not created inside method.");
}

- (void)testFetchUserDetails {
    
    id mockDLMHomeScreenObject = [OCMockObject partialMockForObject:_homeScreenViewModel];
    
    [_homeScreenViewModel fetchUserDetails];
    
    OCMVerify([mockDLMHomeScreenObject setupToFetchUserDetails]);
    OCMVerify([mockDLMHomeScreenObject fetchUserDetailsForLoggedInUser]);
    
    [mockDLMHomeScreenObject stopMocking];
}

- (void)testFetchUserDetailsForLoggedInUser {
    
    id mockUserDetailsWSObject = OCMClassMock([NLQueryUserDetailsWebService class]);
    
    [_homeScreenViewModel setValue:mockUserDetailsWSObject forKey:@"queryUserDetailsWebService"];
    
    [_homeScreenViewModel fetchUserDetailsForLoggedInUser];
    
    OCMVerify([mockUserDetailsWSObject resume]);
    
    [mockUserDetailsWSObject stopMocking];
}

- (void)testTakeActionOnPrefetchedUserDetailsWithUserDetailsData {
    
    id mockDLMHomeScreenObject = [OCMockObject partialMockForObject:_homeScreenViewModel];
    
    [_homeScreenViewModel takeActionOnPrefetchedUserDetails:[NSDictionary dictionary]];
    
    OCMVerify([mockDLMHomeScreenObject saveUserDataInPersistenceWithUser:[OCMArg any]]);
    OCMVerify([mockDLMHomeScreenObject saveInterceptorWithOrder:[OCMArg any] andInterfaces:[OCMArg any]]);
    OCMVerify([mockDLMHomeScreenObject takeActionAfterFetchingUserDetailsWithGroup:[OCMArg any]]);
    
    [mockDLMHomeScreenObject stopMocking];
}

- (void)testSetupToFetchUserGroupNotifications {
    
    [_homeScreenViewModel setupToFetchUserGroupNotifications];
    XCTAssertNotNil([_homeScreenViewModel valueForKey:@"getNotificationsWebService"], @"NLGetNotificationsWebService instance is not created inside method.");
}

- (void)testFetchUserGroupNotifications {
    
    id mockDLMHomeScreenObject = [OCMockObject partialMockForObject:_homeScreenViewModel];
    
    [_homeScreenViewModel fetchUserGroupNotifications];
    
    OCMVerify([mockDLMHomeScreenObject setupToFetchUserGroupNotifications]);
    OCMVerify([mockDLMHomeScreenObject fetchUserNotifications]);
    
    [mockDLMHomeScreenObject stopMocking];
}

- (void)testFetchUserNotifications {
    
    id mockUserNotificationsWSObject = OCMClassMock([NLGetNotificationsWebService class]);
    id mockNotificationCenterObj = [OCMockObject partialMockForObject:[NSNotificationCenter defaultCenter]];
    
    [_homeScreenViewModel setValue:mockUserNotificationsWSObject forKey:@"getNotificationsWebService"];
    
    [_homeScreenViewModel fetchUserNotifications];
    
    OCMVerify([mockUserNotificationsWSObject resume]);
    
    // The notification name should be correct so we have to check with proper notification name.
    OCMVerify([mockNotificationCenterObj postNotificationName:@"APICallForGetNotificationsStartedNotification" object:nil]);
    
    [mockUserNotificationsWSObject stopMocking];
    [mockNotificationCenterObj stopMocking];
}

- (void)testCancelGetUserAccountNotificationsAPICall {
    
    id mockUserNotificationsWSObject = OCMClassMock([NLGetNotificationsWebService class]);
    id mockNotificationCenterObj = [OCMockObject partialMockForObject:[NSNotificationCenter defaultCenter]];
    
    [_homeScreenViewModel setValue:mockUserNotificationsWSObject forKey:@"getNotificationsWebService"];
    
    [_homeScreenViewModel cancelGetUserAccountNotificationsAPICall];
    
    OCMVerify([mockUserNotificationsWSObject cancel]);
    
    // The notification name should be correct so we have to check with proper notification name.
    OCMVerify([mockNotificationCenterObj postNotificationName:@"APICallForGetNotificationsCancelledNotification" object:nil]);
    
    [mockUserNotificationsWSObject stopMocking];
    [mockNotificationCenterObj stopMocking];
}

- (void)testChangeSelectedCUGToWithInvalidSelectedCug {
    
    [_homeScreenViewModel changeSelectedCUGTo:nil];
    
    XCTAssertNotNil(_homeScreenViewModel.user, @"BTUser instance has not been created");
    XCTAssertNil([AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey, @"Test case failed, currentSelectedCug should have nil value for groupKey");
    XCTAssertNil([AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugName, @"Test case failed, currentSelectedCug should have nil value for cugName");
    XCTAssertTrue([[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugRole intValue]==0, @"Test case failed, currentSelectedCug should have nil value for cugRole");
    XCTAssertNil([AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.refKey, @"Test case failed, currentSelectedCug should have nil value for refKey");
    XCTAssertTrue([[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.indexInAPIResponse intValue]==0, @"Test case failed, currentSelectedCug should have nil value for indexInAPIResponse");
}

- (void)testChangeSelectedCUGToWithEmptySelectedCug {
    
    [_homeScreenViewModel changeSelectedCUGTo:[BTCug new]];
    
    XCTAssertNotNil(_homeScreenViewModel.user, @"BTUser instance has not been created");
    XCTAssertNil([AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey, @"Test case failed, currentSelectedCug should have nil value for groupKey");
    XCTAssertNil([AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugName, @"Test case failed, currentSelectedCug should have nil value for cugName");
    XCTAssertTrue([[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugRole intValue]==0, @"Test case failed, currentSelectedCug should have nil value for cugRole");
    XCTAssertNil([AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.refKey, @"Test case failed, currentSelectedCug should have nil value for refKey");
    XCTAssertTrue([[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.indexInAPIResponse intValue]==0, @"Test case failed, currentSelectedCug should have nil value for indexInAPIResponse");
}

- (void)testChangeSelectedCUGToWithValidSelectedCug {
    
    BTCug *selectedCug = [[BTCug alloc] initWithCugName:@"cug1" groupKey:@"groupkey" cugRole:1 andIndexInAPIResponse:2];
    
    [_homeScreenViewModel changeSelectedCUGTo:selectedCug];
    
    XCTAssertNotNil(_homeScreenViewModel.user, @"BTUser instance has not been created");
    XCTAssertEqual(selectedCug.groupKey, [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.groupKey, @"Test case failed, currentSelectedCug should have given value for groupKey");
    XCTAssertEqual(selectedCug.cugName, [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugName, @"Test case failed, currentSelectedCug should have given value for cugName");
    XCTAssertEqual(selectedCug.cugRole, [[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.cugRole intValue], @"Test case failed, currentSelectedCug should have given value for cugRole");
    XCTAssertEqual(selectedCug.refKey, [AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.refKey, @"Test case failed, currentSelectedCug should have given value for refKey");
    XCTAssertEqual(selectedCug.indexInAPIResponse, [[AppDelegate sharedInstance].viewModel.app.loggedInUser.currentSelectedCug.indexInAPIResponse intValue], @"Test case failed, currentSelectedCug should have given value for indexInAPIResponse");
}

- (void)testFetchGridData {
    
    id mockDLMHomeScreenObject = [OCMockObject partialMockForObject:_homeScreenViewModel];
    
    [_homeScreenViewModel fetchGridData];
    
    OCMVerify([mockDLMHomeScreenObject fetchFaultDashBoardDetailsForLoggedInUser]);
    OCMVerify([mockDLMHomeScreenObject fetchOrderDashBoardDetailsForLoggedInUser]);
    OCMVerify([mockDLMHomeScreenObject fetchBillPaymentHistoryData]);
    
    [mockDLMHomeScreenObject stopMocking];
}

- (void)testCancelAPIs {
    
    id mockDLMHomeScreenObject = [OCMockObject partialMockForObject:_homeScreenViewModel];
    
    [_homeScreenViewModel cancelAPIs];
    
    OCMVerify([mockDLMHomeScreenObject cancelFaultDashboardAPICall]);
    OCMVerify([mockDLMHomeScreenObject cancelOrderDashboardAPICall]);
    OCMVerify([mockDLMHomeScreenObject cancelBillDashboardAPICall]);
    
    [mockDLMHomeScreenObject stopMocking];
}

- (void)testCancelFaultDashboardAPICall {
    
    id mockFaultDashboardWSObject = OCMClassMock([NLFaultDashBoardWebService class]);
    id mockNotificationCenterObj = [OCMockObject partialMockForObject:[NSNotificationCenter defaultCenter]];
    
    [_homeScreenViewModel setValue:mockFaultDashboardWSObject forKey:@"getFaultDashBoardWebService"];
    
    [_homeScreenViewModel cancelFaultDashboardAPICall];
    
    OCMVerify([mockFaultDashboardWSObject cancel]);
    
    // The notification name should be correct so we have to check with proper notification name.
    OCMVerify([mockNotificationCenterObj postNotificationName:@"APICallForOpenFaultsCancelledAtHomeScreenNotification" object:nil]);
    
    [mockFaultDashboardWSObject stopMocking];
    [mockNotificationCenterObj stopMocking];
}

- (void)testCancelOrderDashboardAPICall {
    
    id mockOrderDashboardWSObject = OCMClassMock([NLTrackOrderDashBoardWebService class]);
    id mockNotificationCenterObj = [OCMockObject partialMockForObject:[NSNotificationCenter defaultCenter]];
    
    [_homeScreenViewModel setValue:mockOrderDashboardWSObject forKey:@"getOrderDashBoardWebService"];
    
    [_homeScreenViewModel cancelOrderDashboardAPICall];
    
    OCMVerify([mockOrderDashboardWSObject cancel]);
    
    // The notification name should be correct so we have to check with proper notification name.
    OCMVerify([mockNotificationCenterObj postNotificationName:@"APICallForActiveOrdersCancelledAtHomeScreenNotification" object:nil]);
    
    [mockOrderDashboardWSObject stopMocking];
    [mockNotificationCenterObj stopMocking];
}

- (void)testCancelBillDashboardAPICall {
    
    id mockBillDashboardWSObject = OCMClassMock([NLGetBillPaymentHistoryWebService class]);
    id mockNotificationCenterObj = [OCMockObject partialMockForObject:[NSNotificationCenter defaultCenter]];
    
    [_homeScreenViewModel setValue:mockBillDashboardWSObject forKey:@"getBillPaymentHistoryWebService"];
    
    [_homeScreenViewModel cancelBillDashboardAPICall];
    
    OCMVerify([mockBillDashboardWSObject cancel]);
    
    // The notification name should be correct so we have to check with proper notification name.
    OCMVerify([mockNotificationCenterObj postNotificationName:@"APICallForReadyBillsCancelledAtHomeScreenNotification" object:nil]);
    
    [mockBillDashboardWSObject stopMocking];
    [mockNotificationCenterObj stopMocking];
}

- (void)testSetupToFetchOrderDashboardData {
    
    [_homeScreenViewModel setupToFetchOrderDashboardData];
    XCTAssertNotNil([_homeScreenViewModel valueForKey:@"getOrderDashBoardWebService"], @"NLTrackOrderDashBoardWebService instance is not created inside method.");
}

- (void)testFetchOrderDashBoardDetailsForLoggedInUser {
    
    id mockDLMHomeScreenObject = [OCMockObject partialMockForObject:_homeScreenViewModel];
    
    [_homeScreenViewModel fetchOrderDashBoardDetailsForLoggedInUser];
    
    OCMVerify([mockDLMHomeScreenObject setupToFetchOrderDashboardData]);
    OCMVerify([mockDLMHomeScreenObject fetchOrderDashBoardData]);
    
    [mockDLMHomeScreenObject stopMocking];
}

- (void)testFetchOrderDashBoardData {
    
    id mockOrderDashboardWSObject = OCMClassMock([NLTrackOrderDashBoardWebService class]);
    id mockNotificationCenterObj = [OCMockObject partialMockForObject:[NSNotificationCenter defaultCenter]];
    
    [_homeScreenViewModel setValue:mockOrderDashboardWSObject forKey:@"getOrderDashBoardWebService"];
    
    [_homeScreenViewModel fetchOrderDashBoardData];
    
    OCMVerify([mockOrderDashboardWSObject resume]);
    
    // The notification name should be correct so we have to check with proper notification name.
    OCMVerify([mockNotificationCenterObj postNotificationName:@"APICallForActiveOrdersStartedAtHomeScreenNotification" object:nil]);
    
    [mockOrderDashboardWSObject stopMocking];
    [mockNotificationCenterObj stopMocking];
}

- (void)testSetupToFetchFaultDashboardData {
    
    [_homeScreenViewModel setupToFetchFaultDashboardData];
    XCTAssertNotNil([_homeScreenViewModel valueForKey:@"getFaultDashBoardWebService"], @"NLFaultDashBoardWebService instance is not created inside method.");
}

- (void)testFetchFaultDashBoardDetailsForLoggedInUser {
    
    id mockDLMHomeScreenObject = [OCMockObject partialMockForObject:_homeScreenViewModel];
    
    [_homeScreenViewModel fetchFaultDashBoardDetailsForLoggedInUser];
    
    OCMVerify([mockDLMHomeScreenObject setupToFetchFaultDashboardData]);
    OCMVerify([mockDLMHomeScreenObject fetchFaultDashBoardData]);
    
    [mockDLMHomeScreenObject stopMocking];
}

- (void)testFetchFaultDashBoardData {
    
    id mockFaultDashboardWSObject = OCMClassMock([NLFaultDashBoardWebService class]);
    id mockNotificationCenterObj = [OCMockObject partialMockForObject:[NSNotificationCenter defaultCenter]];
    
    [_homeScreenViewModel setValue:mockFaultDashboardWSObject forKey:@"getFaultDashBoardWebService"];
    
    [_homeScreenViewModel fetchFaultDashBoardData];
    
    OCMVerify([mockFaultDashboardWSObject resume]);
    
    // The notification name should be correct so we have to check with proper notification name.
    OCMVerify([mockNotificationCenterObj postNotificationName:@"APICallForOpenFaultsStartedAtHomeScreenNotification" object:nil]);
    
    [mockFaultDashboardWSObject stopMocking];
    [mockNotificationCenterObj stopMocking];
}

- (void)testSetupToFetchBillDashboardData {
    
    [_homeScreenViewModel setupToFetchBillDashboardData];
    XCTAssertNotNil([_homeScreenViewModel valueForKey:@"getBillPaymentHistoryWebService"], @"NLGetBillPaymentHistoryWebService instance is not created inside method.");
}

- (void)testFetchBillPaymentHistoryData {
    
    id mockDLMHomeScreenObject = [OCMockObject partialMockForObject:_homeScreenViewModel];
    
    [_homeScreenViewModel fetchBillPaymentHistoryData];
    
    OCMVerify([mockDLMHomeScreenObject setupToFetchBillDashboardData]);
    OCMVerify([mockDLMHomeScreenObject fetchBillDashBoardData]);
    
    [mockDLMHomeScreenObject stopMocking];
}

- (void)testFetchBillDashBoardData {
    
    id mockBillDashboardWSObject = OCMClassMock([NLGetBillPaymentHistoryWebService class]);
    id mockNotificationCenterObj = [OCMockObject partialMockForObject:[NSNotificationCenter defaultCenter]];
    
    [_homeScreenViewModel setValue:mockBillDashboardWSObject forKey:@"getBillPaymentHistoryWebService"];
    
    [_homeScreenViewModel fetchBillDashBoardData];
    
    OCMVerify([mockBillDashboardWSObject resume]);
    
    // The notification name should be correct so we have to check with proper notification name.
    OCMVerify([mockNotificationCenterObj postNotificationName:@"APICallForReadyBillsStartedAtHomeScreenNotification" object:nil]);
    
    [mockBillDashboardWSObject stopMocking];
    [mockNotificationCenterObj stopMocking];
}

- (void)testGridModelArray {
    
    XCTAssertNotNil([_homeScreenViewModel gridModelArray], @"Method is not able to create or not returning Grid model array instance.");
    
}

- (void)testBottomGridModelArray {
    
    NSArray *dummyArray = nil;
    [_homeScreenViewModel setValue:dummyArray forKey:@"_bottomGridModelArray"];
    
    XCTAssertNotNil([_homeScreenViewModel bottomGridModelArray], @"Method is not able to create or not returning Grid model array instance.");
    
}

- (void)testBottomGridModelArrayWithAlreadyCreatedArray {
    
    NSArray *dummyArray = [NSArray arrayWithObject:@"Asd"];
    [_homeScreenViewModel setValue:dummyArray forKey:@"_bottomGridModelArray"];
    
    XCTAssertEqualObjects(dummyArray, [_homeScreenViewModel bottomGridModelArray], @"Method is not able to create or not returning Grid model array instance.");
    
}

- (void)testInterceptorTypeForIntercentorName {
    
    XCTAssertEqual(InterceptorTypeUnknown, [_homeScreenViewModel interceptorTypeForIntercentorName:@"UnknownInterceptor"]);
    XCTAssertEqual(InterceptorTypeFirstTimeLogin, [_homeScreenViewModel interceptorTypeForIntercentorName:kFirstTimeLogin]);
    XCTAssertEqual(InterceptorTypeCreatePassword, [_homeScreenViewModel interceptorTypeForIntercentorName:kCreatePassword]);
    XCTAssertEqual(InterceptorTypeUpdateContact, [_homeScreenViewModel interceptorTypeForIntercentorName:kUpdateContact]);
    XCTAssertEqual(InterceptorTypeUpdateProfile, [_homeScreenViewModel interceptorTypeForIntercentorName:kUpdateProfile]);
    
}

- (void)testGetOrdersDataDictionary {
    
    NSDictionary *mockDict = [NSDictionary dictionary];
    [_homeScreenViewModel setValue:mockDict forKey:@"_orderDataDictionary"];
    NSDictionary *returnedDict = [_homeScreenViewModel getOrdersDataDictionary];
    
    XCTAssertTrue([returnedDict isKindOfClass:[NSDictionary class]], @"Returned object is not NSDictionary object.");
    XCTAssertEqualObjects(mockDict, returnedDict, @"Returned object is different than we are expecting.");
}

- (void)testGetFaultsDataDictionary {
    
    NSDictionary *mockDict = [NSDictionary dictionary];
    [_homeScreenViewModel setValue:mockDict forKey:@"_faultDataDictionary"];
    NSDictionary *returnedDict = [_homeScreenViewModel getFaultsDataDictionary];
    
    XCTAssertTrue([returnedDict isKindOfClass:[NSDictionary class]], @"Returned object is not NSDictionary object.");
    XCTAssertEqualObjects(mockDict, returnedDict, @"Returned object is different than we are expecting.");
}

- (void)testGetBillsDataDictionary {
    
    NSDictionary *mockDict = [NSDictionary dictionary];
    [_homeScreenViewModel setValue:mockDict forKey:@"_billDataDictionary"];
    NSDictionary *returnedDict = [_homeScreenViewModel getBillsDataDictionary];
    
    XCTAssertTrue([returnedDict isKindOfClass:[NSDictionary class]], @"Returned object is not NSDictionary object.");
    XCTAssertEqualObjects(mockDict, returnedDict, @"Returned object is different than we are expecting.");
}

- (void)testSaveInterceptorWithOrderAndInterfacesWithNilValues {
    
    [_homeScreenViewModel saveInterceptorWithOrder:nil andInterfaces:nil];
    
    XCTAssertTrue([[_homeScreenViewModel valueForKey:@"_interceptorArray"] count] == 0, @"Interceptor array must be empty array");
    
}

- (void)testSaveInterceptorWithOrderAndInterfacesWithValidValues {
    
    [_homeScreenViewModel saveInterceptorWithOrder:@"FirstTimeLogin" andInterfaces:@"FirstTimeLogin"];
    
    XCTAssertTrue([[_homeScreenViewModel valueForKey:@"_interceptorArray"] count] == 1, @"Interceptor array count must increase by one.");
    
}

- (void)testGetStatusMatrixForHomeScreen {
    
    [_homeScreenViewModel setValue:nil forKey:@"_faultStatusMatrix"];
    [_homeScreenViewModel setValue:nil forKey:@"_orderStatusMatrix"];
    [_homeScreenViewModel setValue:nil forKey:@"_billStatusMatrix"];
    
    XCTAssertTrue([[_homeScreenViewModel getStatusMatrixForHomeScreen] length] == 0, @"Returned string length must be zero.");
    
}

- (void)testGetStatusMatrixForHomeScreenWithValidOrderStatusMatrix {
    
    [_homeScreenViewModel setValue:@"mockOrderStatusMatrix" forKey:@"_orderStatusMatrix"];
    
    XCTAssertEqual([_homeScreenViewModel getStatusMatrixForHomeScreen], @"mockOrderStatusMatrix", @"Returned string must be same as order status matrix");
    
}

- (void)testMakeUpdateContactInterceptorAPIRequestWithNilContactArray {
    
    [_homeScreenViewModel setValue:nil forKey:@"updateContactInterceptorArray"];
    
    NSArray *mockArray = nil;
    [_homeScreenViewModel makeUpdateContactInterceptorAPIRequestWithContactArray:mockArray];
    
    XCTAssertNil([_homeScreenViewModel valueForKey:@"updateContactInterceptorArray"], @"Method must return immediately when input arrya is nil.");
    
}

- (void)testMakeUpdateContactInterceptorAPIRequestWithEmptyContactArray {
    
    id dlmHomeMockObj = [OCMockObject partialMockForObject:_homeScreenViewModel];
    
    [_homeScreenViewModel setValue:nil forKey:@"updateContactInterceptorArray"];
    
    NSArray *mockArray = [NSArray array];
    [_homeScreenViewModel makeUpdateContactInterceptorAPIRequestWithContactArray:mockArray];
    
    XCTAssertNotNil([_homeScreenViewModel valueForKey:@"updateContactInterceptorArray"], @"Method must return immediately when input arrya is nil.");
    OCMVerify([dlmHomeMockObj makeAPIReqeustWithParams:[OCMArg any]]);
    
    [dlmHomeMockObj stopMocking];
    
}

- (void)testMakeUpdateContactInterceptorAPIRequestWithContactArray {
    
    [_homeScreenViewModel setValue:[NSMutableArray array] forKey:@"updateContactInterceptorArray"];
    
    NSArray *mockArray = [NSArray array];
    [_homeScreenViewModel makeUpdateContactInterceptorAPIRequestWithContactArray:mockArray];
    
    XCTAssertTrue([[_homeScreenViewModel valueForKey:@"updateContactInterceptorArray"] count] == 1, @"Method must increase array count by one.");
    
}

- (void)testMakeAPIRequestWithParameters {
    
    id updateContactInterceptorModelMockObject = OCMClassMock([DLMUpdateContactInerceptScreen class]);
    
    [_homeScreenViewModel setValue:updateContactInterceptorModelMockObject forKey:@"updateContactInterceptorModel"];
    
    NSArray *mockArray = [NSArray array];
    [_homeScreenViewModel makeAPIReqeustWithParams:mockArray];
    
    OCMVerify([updateContactInterceptorModelMockObject submitAndCreateContactDetailsModelWithContactDetailsDictionary:[OCMArg any] andPassword:[OCMArg any]]);
}

@end
