//
//  BTContactTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/22/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BTContact.h"

@interface BTContactTests : XCTestCase

@property (nonatomic) BTContact *contact;

@end

@implementation BTContactTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _contact = nil;
}

- (void)testInitWithResponseDictionaryFromSiteContactsAPIResponse {
    
    NSMutableDictionary *mockContactDict = [NSMutableDictionary dictionary];
    
    NSString *contactKey = @"contactkey";
    NSString *email = @"email";
    NSString *firstName = @"firstname";
    NSString *lastName = @"lastname";
    NSString *title = @"title";
    NSString *mobile = @"mobile";
    NSString *phone = @"phone";
    NSString *formattedContactLine = @"formattedcontactline";
    
    [mockContactDict setValue:contactKey forKey:@"ContactKey"];
    [mockContactDict setValue:email forKey:@"Email"];
    [mockContactDict setValue:firstName forKey:@"FirstName"];
    [mockContactDict setValue:lastName forKey:@"LastName"];
    [mockContactDict setValue:title forKey:@"Title"];
    [mockContactDict setValue:mobile forKey:@"Mobile"];
    [mockContactDict setValue:phone forKey:@"Phone"];
    [mockContactDict setValue:formattedContactLine forKey:@"ForamattedContactLine"];
    
    _contact = [[BTContact alloc] initWithResponseDictionaryFromSiteContactsAPIResponse:mockContactDict];
    
    XCTAssertNotNil(_contact, @"BTContact instance is not created.");
    XCTAssertTrue([_contact.contactKey isEqualToString:contactKey]);
    XCTAssertTrue([_contact.email isEqualToString:email]);
    XCTAssertTrue([_contact.firstName isEqualToString:firstName]);
    XCTAssertTrue([_contact.lastName isEqualToString:lastName]);
    XCTAssertTrue([_contact.title isEqualToString:title]);
    XCTAssertTrue([_contact.mobile isEqualToString:mobile]);
    XCTAssertTrue([_contact.phone isEqualToString:phone]);
    XCTAssertTrue([_contact.formattedContactLine isEqualToString:formattedContactLine]);
}

- (void)testInitWithResponseDictionaryFromInvalidSiteContactsAPIResponse {
    
    _contact = [[BTContact alloc] initWithResponseDictionaryFromSiteContactsAPIResponse:nil];
    
    XCTAssertNotNil(_contact, @"BTContact instance is not created.");
    
}

@end
