//
//  BTBundleOrderTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/21/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BTBundleOrder.h"

@interface BTBundleOrderTests : XCTestCase

@property (nonatomic) BTBundleOrder *bundleOrder;

@end

@implementation BTBundleOrderTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _bundleOrder = nil;
}

- (void)testInitBundleOrderWithResponseDictionaryFromBundleOrderSummaryAPIResponse {
    
    NSMutableDictionary *mockOrderDict = [NSMutableDictionary dictionary];
    
    double bundleExtraCharges = 1.23;
    double bundleTotalOneOffCharge = 3.21;
    
    [mockOrderDict setValue:[NSNumber numberWithDouble:bundleExtraCharges] forKey:@"BundleExtraCharges"];
    [mockOrderDict setValue:[NSNumber numberWithDouble:bundleTotalOneOffCharge] forKey:@"BundleTotalOneOffCharge"];
    
    _bundleOrder = [[BTBundleOrder alloc] initBundleOrderWithResponseDictionaryFromBundleOrderSummaryAPIResponse:mockOrderDict];
    
    XCTAssertNotNil(_bundleOrder, @"BTBundleOrder instance is not created.");
    XCTAssertEqual(_bundleOrder.bundleExtraCharges, bundleExtraCharges);
    XCTAssertEqual(_bundleOrder.bundleTotalOneOffCharge, bundleTotalOneOffCharge);
    
}

- (void)testInitBundleOrderWithResponseDictionaryFromInvalidBundleOrderSummaryAPIResponse {
    
    _bundleOrder = [[BTBundleOrder alloc] initBundleOrderWithResponseDictionaryFromBundleOrderSummaryAPIResponse:nil];
    
    XCTAssertNotNil(_bundleOrder, @"BTBundleOrder instance is not created.");
    
}

@end
