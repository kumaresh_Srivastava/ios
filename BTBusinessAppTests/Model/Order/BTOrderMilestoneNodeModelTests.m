//
//  BTOrderMilestoneNodeModelTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/22/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BTOrderMilestoneNodeModel.h"
#import "AppManager.h"

@interface BTOrderMilestoneNodeModelTests : XCTestCase

@property (nonatomic) BTOrderMilestoneNodeModel *milestoneNodeModel;

@end

@implementation BTOrderMilestoneNodeModelTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _milestoneNodeModel = nil;
}

- (void)testInitMilestoneNodeWithResponseDic {
    
    int authLevel = 1;
    NSString *date = @"2017-08-12T12:12:12Z";
    NSDictionary *siteContact = [NSDictionary dictionary];
    int appointmentAmendSource = 2;
    BOOL isActiveNode = YES;
    NSString *displayName = @"displayname";
    
    NSDictionary *milestoneDict = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInt:authLevel], @"AuthLevel", date, @"Date", siteContact, @"SiteContact", [NSNumber numberWithInt:appointmentAmendSource], @"AppointmentAmendSource", [NSNumber numberWithBool:isActiveNode], @"IsActiveNode", displayName, @"DisplayName", nil];
    
    _milestoneNodeModel = [[BTOrderMilestoneNodeModel alloc] initMilestoneNodeWithResponseDic:milestoneDict];
    
    XCTAssertNotNil(_milestoneNodeModel, @"BTOrderMilestoneNodeModel instance is not created.");
    XCTAssertTrue(_milestoneNodeModel.authLevel == authLevel);
    XCTAssertTrue([_milestoneNodeModel.date compare:[AppManager NSDateWithUTCFormatFromNSString:date]]==NSOrderedSame);
    XCTAssertTrue(_milestoneNodeModel.isSiteContactAvailable);
    XCTAssertFalse(_milestoneNodeModel.isDispatchDetailsAvailable);
    XCTAssertTrue(_milestoneNodeModel.isAppointmentAmendSource);
    XCTAssertFalse(_milestoneNodeModel.isSiteContactAmendSource);
    XCTAssertTrue(_milestoneNodeModel.isActiveNode);
    XCTAssertFalse(_milestoneNodeModel.isFirstActiveNode);
    XCTAssertTrue([_milestoneNodeModel.nodeDisplayName isEqualToString:displayName]);
    
}

- (void)testInitMilestoneNodeWithInvalidResponseDic {
    
    _milestoneNodeModel = [[BTOrderMilestoneNodeModel alloc] initMilestoneNodeWithResponseDic:nil];
    
    XCTAssertNotNil(_milestoneNodeModel, @"BTOrderMilestoneNodeModel instance is not created.");
    
}

- (void)testUpdateNodeDataWithEngineerAppointmentDate {
    
    _milestoneNodeModel = [[BTOrderMilestoneNodeModel alloc] init];
    
    NSDate *mockDate = [AppManager NSDateWithUTCFormatFromNSString:@"2017-08-22T12:12:12Z"];
    
    [_milestoneNodeModel updateNodeDataWithEngineerAppointmentDate:mockDate];
    
    NSString *expectedString = [NSString stringWithFormat:@"The engineer visit is appointed to %@ (8am - 1pm). Ensure the address and contact information are correct.", [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:mockDate]];
    
    XCTAssertTrue([_milestoneNodeModel.nodeDescription isEqualToString:expectedString]);
}

- (void)testUpdateNodeDataWithActivationDate {
    
    _milestoneNodeModel = [[BTOrderMilestoneNodeModel alloc] init];
    
    NSDate *mockDate = [AppManager NSDateWithUTCFormatFromNSString:@"2017-08-22T12:12:12Z"];
    
    [_milestoneNodeModel updateNodeDataWithActivationDate:mockDate];
    
    NSString *expectedString = [NSString stringWithFormat:@"Order is estimated to be completed at %@.", [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:mockDate]];
    
    XCTAssertTrue([_milestoneNodeModel.nodeDescription isEqualToString:expectedString]);
}

@end
