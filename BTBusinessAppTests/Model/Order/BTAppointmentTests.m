//
//  BTAppointmentTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/21/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BTAppointment.h"

@interface BTAppointmentTests : XCTestCase

@property (nonatomic) BTAppointment *appointment;

@end

@implementation BTAppointmentTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _appointment = nil;
}

- (void)testInitWithResponseDictionaryFromAppointmentsAPIResponse {
    
    NSMutableDictionary *mockDict = [NSMutableDictionary dictionary];
    
    [mockDict setValue:@"12/12/2017" forKey:@"DateInfo"];
    [mockDict setValue:@"2017-12-12T12:12:12Z" forKey:@"Date"];
    [mockDict setValue:@"Mon" forKey:@"DayOfWeek"];
    [mockDict setValue:[NSNumber numberWithBool:YES] forKey:@"HasAM"];
    [mockDict setValue:[NSNumber numberWithBool:NO] forKey:@"IsAMSelected"];
    
    _appointment = [[BTAppointment alloc] initWithResponseDictionaryFromAppointmentsAPIResponse:mockDict];
    
    XCTAssertNotNil(_appointment, @"BTAppointment instance is not created.");
    XCTAssertNotNil(_appointment.dateInfo);
    XCTAssertNotNil(_appointment.date);
    XCTAssertTrue([_appointment.DayOfWeek isEqualToString:@"Mon"]);
    XCTAssertTrue(_appointment.hasAM);
}

- (void)testInitWithResponseDictionaryFromInvalidAppointmentsAPIResponse {
    
    _appointment = [[BTAppointment alloc] initWithResponseDictionaryFromAppointmentsAPIResponse:nil];
    
    XCTAssertNotNil(_appointment, @"BTAppointment instance is not created.");
    
}

@end
