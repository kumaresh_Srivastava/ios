//
//  BTRecentSearchedOrderTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/23/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BTRecentSearchedOrder.h"
#import "CDRecentSearchedOrder.h"
#import "AppDelegate.h"
#import "CDUser.h"

@interface BTRecentSearchedOrderTests : XCTestCase

@property (nonatomic) BTRecentSearchedOrder *recentSearchedOrder;

@end

@implementation BTRecentSearchedOrderTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _recentSearchedOrder = nil;
}

- (void)testInitWithCDRecentSearchedOrder {
    
    NSManagedObjectContext *context = [AppDelegate sharedInstance].managedObjectContext;
    CDRecentSearchedOrder *cdRecentSearchedOrder = [CDRecentSearchedOrder newRecentSearchedOrderInManagedObjectContext:context];
    cdRecentSearchedOrder.orderRef = @"BT012345";
    cdRecentSearchedOrder.lastSearchedDate = [NSDate date];
    cdRecentSearchedOrder.orderDescription = @"description";
    cdRecentSearchedOrder.placedOnDate = [NSDate date];
    cdRecentSearchedOrder.completionDate = [NSDate date];
    cdRecentSearchedOrder.orderStatus = @"status";
    //cdRecentSearchedOrder.user = [[CDUser alloc] init];
    
    _recentSearchedOrder = [[BTRecentSearchedOrder alloc] initWithCDRecentSearchedOrder:cdRecentSearchedOrder];
    
    XCTAssertNotNil(_recentSearchedOrder, @"BTRecentSearchedOrder instance is not created.");
    XCTAssertTrue([_recentSearchedOrder.orderRef isEqualToString:cdRecentSearchedOrder.orderRef]);
    XCTAssertTrue([_recentSearchedOrder.lastSearchedDate compare:cdRecentSearchedOrder.lastSearchedDate]==NSOrderedSame);
    XCTAssertTrue([_recentSearchedOrder.orderDescription isEqualToString:cdRecentSearchedOrder.orderDescription]);
    XCTAssertTrue([_recentSearchedOrder.placedOnDate compare:cdRecentSearchedOrder.placedOnDate] == NSOrderedSame);
    XCTAssertTrue([_recentSearchedOrder.completionDate compare:cdRecentSearchedOrder.completionDate] == NSOrderedSame);
    XCTAssertTrue([_recentSearchedOrder.orderStatus isEqualToString:cdRecentSearchedOrder.orderStatus]);
    
}

- (void)testInitWithInvalidCDRecentSearchedOrder {
    
    _recentSearchedOrder = [[BTRecentSearchedOrder alloc] initWithCDRecentSearchedOrder:nil];
    
    XCTAssertNotNil(_recentSearchedOrder, @"BTRecentSearchedOrder instance is not created.");
    
}

@end
