//
//  BTOrderTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/22/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BTOrder.h"

@interface BTOrderTests : XCTestCase

@property (nonatomic) BTOrder *order;

@end

@implementation BTOrderTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _order = nil;
}

- (void)testInitWithResponseDictionaryFromOrderDashBoardAPIResponse {
    
    NSDate *orderDate = [NSDate date];
    NSString *orderRef = @"BT012345";
    NSString *orderDesc = @"orderdesc";
    NSDate *orderPlacedDate = [NSDate date];
    NSDate *estimatedCompletionDate = [NSDate date];
    NSString *orderStatus = @"status";
    
    NSDictionary *mockDict = [[NSDictionary alloc] initWithObjectsAndKeys:orderDate, @"<OrderDate>k__BackingField", orderRef, @"<OrderIdentifier>k__BackingField", orderDesc, @"<LongDescription>k__BackingField", orderPlacedDate, @"<PlacedOnDate>k__BackingField", estimatedCompletionDate, @"<CompletionDate>k__BackingField", orderStatus, @"<CompleteOrderStatus>k__BackingField", nil];
    
    _order = [[BTOrder alloc] initWithResponseDictionaryFromOrderDashBoardAPIResponse:mockDict];
    
    XCTAssertNotNil(_order, @"BTOrder instance is not created.");
    XCTAssertTrue([_order.orderDate compare:orderDate]==NSOrderedSame);
    XCTAssertTrue([_order.orderRef isEqualToString:orderRef]);
    XCTAssertTrue([_order.orderDescription isEqualToString:orderDesc]);
    XCTAssertTrue([_order.orderPlacedDate compare:orderPlacedDate]==NSOrderedSame);
    XCTAssertTrue([_order.estimatedCompletionDate compare:estimatedCompletionDate]==NSOrderedSame);
    XCTAssertTrue([_order.orderStatus isEqualToString:orderStatus]);
    
}

- (void)testInitWithResponseDictionaryFromInvalidOrderDashBoardAPIResponse {
    
    _order = [[BTOrder alloc] initWithResponseDictionaryFromOrderDashBoardAPIResponse:nil];
    
    XCTAssertNotNil(_order, @"BTOrder instance is not created.");
    
}

- (void)testInitOrderWithResponseDictionaryFromGroupOrderSummaryAPIResponse {
    
    NSString *orderRef = @"BT012345";
    NSString *orderDesc = @"orderdesc";
    NSDate *orderPlacedDate = [NSDate date];
    NSDate *estimatedCompletionDate = [NSDate date];
    NSString *orderStatus = @"status";
    
    NSDictionary *mockDict = [[NSDictionary alloc] initWithObjectsAndKeys:orderRef, @"OrderNumber", orderDesc, @"Description", orderPlacedDate, @"OrderDate", estimatedCompletionDate, @"CompletionDate", orderStatus, @"Status",  nil];
    
    _order = [[BTOrder alloc] initOrderWithResponseDictionaryFromGroupOrderSummaryAPIResponse:mockDict];
    
    XCTAssertNotNil(_order, @"BTOrder instance is not created.");
    XCTAssertTrue([_order.orderRef isEqualToString:orderRef]);
    XCTAssertTrue([_order.orderDescription isEqualToString:orderDesc]);
    XCTAssertTrue([_order.orderPlacedDate compare:orderPlacedDate]==NSOrderedSame);
    XCTAssertTrue([_order.estimatedCompletionDate compare:estimatedCompletionDate]==NSOrderedSame);
    XCTAssertTrue([_order.orderStatus isEqualToString:orderStatus]);
}

- (void)testInitOrderWithResponseDictionaryFromInvalidGroupOrderSummaryAPIResponse {
    
    _order = [[BTOrder alloc] initOrderWithResponseDictionaryFromGroupOrderSummaryAPIResponse:nil];
    
    XCTAssertNotNil(_order, @"BTOrder instance is not created.");
    
}

- (void)testInitOrderWithResponseDictionaryFromOrderSummaryAPIResponse {
    
    NSString *orderRef = @"BT012345";
    NSString *orderDesc = @"orderdesc";
    NSDate *orderPlacedDate = [NSDate date];
    NSDate *estimatedCompletionDate = [NSDate date];
    NSString *orderStatus = @"status";
    
    NSDictionary *orderInfo = [[NSDictionary alloc] initWithObjectsAndKeys:orderRef, @"OrderRef", orderDesc, @"OrderDescription", orderPlacedDate, @"OrderPlacedDate", estimatedCompletionDate, @"EstimatedCompletionDate", orderStatus, @"OrderStatus", nil];
    
    NSString *pOrderKey = @"BT012345";
    NSString *pItemRef = @"BT0123451-1";
    
    NSDictionary *prodDict = [[NSDictionary alloc] initWithObjectsAndKeys:pOrderKey, @"OrderKey", pItemRef, @"OrderItemReference", nil];
    
    NSArray *prodArray = [[NSArray alloc] initWithObjects:prodDict, nil];
    
    NSDictionary *orderDict = [[NSDictionary alloc] initWithObjectsAndKeys:orderInfo, @"OrderMetaInfo", prodArray, @"Products", nil];
    
    _order = [[BTOrder alloc] initOrderWithResponseDictionaryFromOrderSummaryAPIResponse:orderDict];
    
    XCTAssertNotNil(_order, @"BTOrder instance is not created.");
    XCTAssertNotNil(_order.arrayOfProducts, @"Prodcuts list is not created.");
    XCTAssertTrue([_order.orderRef isEqualToString:orderRef]);
    XCTAssertTrue([_order.orderDescription isEqualToString:orderDesc]);
    XCTAssertTrue([_order.orderPlacedDate compare:orderPlacedDate]==NSOrderedSame);
    XCTAssertTrue([_order.estimatedCompletionDate compare:estimatedCompletionDate]==NSOrderedSame);
    XCTAssertTrue([_order.orderStatus isEqualToString:orderStatus]);
    XCTAssertTrue(_order.arrayOfProducts.count==1);
    
}

- (void)testInitOrderWithResponseDictionaryFromInvalidOrderSummaryAPIResponse {
    
    _order = [[BTOrder alloc] initOrderWithResponseDictionaryFromOrderSummaryAPIResponse:nil];
    
    XCTAssertNotNil(_order, @"BTOrder instance is not created.");
    
}

- (void)testInitOrderWithResponseDictionaryFromBundleOrderSummaryAPIResponse {
    
    NSString *orderRef = @"BT012345";
    NSString *orderDesc = @"orderdesc";
    NSDate *orderPlacedDate = [NSDate date];
    NSDate *estimatedCompletionDate = [NSDate date];
    NSString *orderStatus = @"status";
    
    NSDictionary *orderInfo = [[NSDictionary alloc] initWithObjectsAndKeys:orderRef, @"OrderRef", orderDesc, @"OrderDescription", orderPlacedDate, @"OrderPlacedDate", estimatedCompletionDate, @"EstimatedCompletionDate", orderStatus, @"OrderStatus", nil];
    
    NSString *pOrderKey = @"BT012345";
    NSString *pItemRef = @"BT0123451-1";
    
    NSDictionary *prodDict = [[NSDictionary alloc] initWithObjectsAndKeys:pOrderKey, @"OrderKey", pItemRef, @"OrderItemReference", nil];
    
    NSArray *prodArray = [[NSArray alloc] initWithObjects:prodDict, nil];
    
    NSDictionary *orderDict = [[NSDictionary alloc] initWithObjectsAndKeys:orderInfo, @"OrderMetaInfo", prodArray, @"BundleItemsSummary", nil];
    
    _order = [[BTOrder alloc] initOrderWithResponseDictionaryFromBundleOrderSummaryAPIResponse:orderDict];
    
    XCTAssertNotNil(_order, @"BTOrder instance is not created.");
    XCTAssertNotNil(_order.arrayOfProducts, @"Prodcuts list is not created.");
    XCTAssertTrue([_order.orderRef isEqualToString:orderRef]);
    XCTAssertTrue([_order.orderDescription isEqualToString:orderDesc]);
    XCTAssertTrue([_order.orderPlacedDate compare:orderPlacedDate]==NSOrderedSame);
    XCTAssertTrue([_order.estimatedCompletionDate compare:estimatedCompletionDate]==NSOrderedSame);
    XCTAssertTrue([_order.orderStatus isEqualToString:orderStatus]);
    XCTAssertTrue(_order.arrayOfProducts.count==1);
    
}

- (void)testInitOrderWithResponseDictionaryFromInvalidBundleOrderSummaryAPIResponse {
    
    _order = [[BTOrder alloc] initOrderWithResponseDictionaryFromBundleOrderSummaryAPIResponse:nil];
    
    XCTAssertNotNil(_order, @"BTOrder instance is not created.");
    
}

@end
