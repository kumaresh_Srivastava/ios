//
//  BTProductDetailsTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/23/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BTProductDetails.h"

@interface BTProductDetailsTests : XCTestCase

@property (nonatomic) BTProductDetails *productDetails;

@end

@implementation BTProductDetailsTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _productDetails = nil;
}

- (void)testInitProductDetailsWithResponse {
    
    NSString *phoneNumber = @"phonenumber";
    NSString *networkUserId = @"networkuserid";
    NSString *email = @"emailaddress";
    
    NSDictionary *productDetails = [[NSDictionary alloc] initWithObjectsAndKeys:phoneNumber, @"PhoneNumber", networkUserId, @"NetworkUserId", email, @"PrimaryEmailAddress", nil];
    
    _productDetails = [[BTProductDetails alloc] initProductDetailsWithResponse:productDetails];
    
    XCTAssertNotNil(_productDetails, @"BTProductDetails instance is not created.");
    XCTAssertTrue([_productDetails.phoneNumber isEqualToString:phoneNumber]);
    XCTAssertTrue([_productDetails.networkUserId isEqualToString:networkUserId]);
    XCTAssertTrue([_productDetails.primaryEmailAddress isEqualToString:email]);
}

- (void)testInitProductDetailsWithInvalidResponse {
    
    _productDetails = [[BTProductDetails alloc] initProductDetailsWithResponse:nil];
    
    XCTAssertNotNil(_productDetails, @"BTProductDetails instance is not created.");
    
}

@end
