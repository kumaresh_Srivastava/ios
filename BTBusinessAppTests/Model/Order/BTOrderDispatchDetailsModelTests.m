//
//  BTOrderDispatchDetailsModelTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/22/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BTOrderDispatchDetailsModel.h"

@interface BTOrderDispatchDetailsModelTests : XCTestCase

@property (nonatomic) BTOrderDispatchDetailsModel *dispatchDetailsModel;

@end

@implementation BTOrderDispatchDetailsModelTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    _dispatchDetailsModel = [[BTOrderDispatchDetailsModel alloc] init];
    
    XCTAssertNotNil(_dispatchDetailsModel, @"BTOrderDispatchDetailsModel instance is not created.");
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _dispatchDetailsModel = nil;
}

- (void)testGetDispatchDetailsWithValidDispatchDetails {
    
    NSString *dispatchDate = @"12/08/2017";
    NSString *equipments = @"equipmentnames";
    NSString *trackingRefNumber = @"AXY123";
    NSString *trackingRefURL = @"trackingreferenceurl";
    
    NSDictionary *mockDispatchDetails = [[NSDictionary alloc] initWithObjectsAndKeys:dispatchDate, @"DispatchDate", equipments, @"EquipmentNames", trackingRefNumber, @"TrackingReferenceNumber", trackingRefURL, @"TrackingReferenceUrl", nil];
    
    NSArray *dispatchDetailsArray = [[NSArray alloc] initWithObjects:mockDispatchDetails, nil];
    
    NSDictionary *dispatchDetails = [[NSDictionary alloc] initWithObjectsAndKeys:dispatchDetailsArray, @"DispatchDetails", nil];
    
    NSArray *returnedArray = [_dispatchDetailsModel getDispatchDetailsFrom:dispatchDetails];
    
    XCTAssertNotNil(returnedArray, @"Dispatch details array is not created.");
    XCTAssertTrue(returnedArray.count==1);
}

- (void)testGetDispatchDetailsWithInvalidDispatchDetails {
    
    NSArray *returnedArray = [_dispatchDetailsModel getDispatchDetailsFrom:nil];
    
    XCTAssertNotNil(returnedArray, @"Dispatch details array is not created.");
    XCTAssertTrue(returnedArray.count==0);
}

@end
