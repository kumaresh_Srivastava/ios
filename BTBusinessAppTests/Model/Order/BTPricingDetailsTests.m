//
//  BTPricingDetailsTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/23/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BTPricingDetails.h"

@interface BTPricingDetailsTests : XCTestCase

@property (nonatomic) BTPricingDetails *pricingDetails;

@end

@implementation BTPricingDetailsTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _pricingDetails = nil;
}

- (void)testInitPricingDetailsWithResponse {
    
    NSArray *orderItems = [NSArray array];
    NSDictionary *parentOrderItem = [NSDictionary dictionary];
    double totalOneOffCharges = 1.12;
    double totalMonthlyCharges = 1.13;
    double totalQuarterlyCharges = 1.14;
    double totalHTTPCharges = 1.15;
    double totalTerminationCharges = 1.16;
    NSString *regularChargesView = @"monthly";
    int journeyType = 2;
    
    NSDictionary *pricingDetails = [[NSDictionary alloc] initWithObjectsAndKeys:orderItems, @"OrderItems", parentOrderItem, @"ParentOrderItem", [NSNumber numberWithDouble:totalOneOffCharges], @"TotalOneOffPrice", [NSNumber numberWithDouble:totalMonthlyCharges], @"TotalMonthlyPrice", [NSNumber numberWithDouble:totalQuarterlyCharges], @"TotalQuarterlyPrice", [NSNumber numberWithDouble:totalHTTPCharges], @"TotalHttCharge", [NSNumber numberWithDouble:totalTerminationCharges], @"TotalTerminationCharge", regularChargesView, @"RegularChargesView", [NSNumber numberWithInt:journeyType], @"JourneyType", nil];
    
    _pricingDetails = [[BTPricingDetails alloc] initPricingDetailsWithResponse:pricingDetails];
    
    XCTAssertNotNil(_pricingDetails, @"BTPricingDetails instance is not created.");
    XCTAssertNotNil(_pricingDetails.orderItems);
    XCTAssertNotNil(_pricingDetails.parentOrderItem);
    XCTAssertTrue(_pricingDetails.totalOneOffPrice == totalOneOffCharges);
    XCTAssertTrue(_pricingDetails.totalMonthlyPrice == totalMonthlyCharges);
    XCTAssertTrue(_pricingDetails.totalQuarterlyPrice == totalQuarterlyCharges);
    XCTAssertTrue(_pricingDetails.totalHttpCharge == totalHTTPCharges);
    XCTAssertTrue(_pricingDetails.totalTerminationCharge == totalTerminationCharges);
    XCTAssertTrue([_pricingDetails.regularChargesView isEqualToString:regularChargesView]);
    XCTAssertTrue(_pricingDetails.journeyType == journeyType);
    
}

- (void)testInitPricingDetailsWithInvalidResponse {
    
    _pricingDetails = [[BTPricingDetails alloc] initPricingDetailsWithResponse:nil];
    
    XCTAssertNotNil(_pricingDetails, @"BTPricingDetails instance is not created.");
    
}

@end
