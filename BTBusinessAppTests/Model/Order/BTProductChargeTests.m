//
//  BTProductChargeTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/23/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BTProductCharge.h"

@interface BTProductChargeTests : XCTestCase

@property (nonatomic) BTProductCharge *productCharge;

@end

@implementation BTProductChargeTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _productCharge = nil;
}

- (void)testInitProductChargeWithData {
    
    NSString *productName = @"productname";
    NSString *code = @"productcode";
    NSInteger noOfInstallations = 1;
    double totalCharges = 1.12;
    
    NSDictionary *productChargeInfo = [[NSDictionary alloc] initWithObjectsAndKeys:productName, @"ProductName", code, @"ProductCode", [NSNumber numberWithInteger:noOfInstallations], @"NumberOfInstallations", [NSNumber numberWithDouble:totalCharges], @"TotalCharges", nil];
    
    _productCharge = [[BTProductCharge alloc] initProductChargeWithData:productChargeInfo];
    
    XCTAssertNotNil(_productCharge, @"BTProductCharge instance is not created.");
    XCTAssertTrue([_productCharge.productName isEqualToString:productName]);
    XCTAssertTrue([_productCharge.productCode isEqualToString:code]);
    XCTAssertTrue(_productCharge.numberOfInstallations == noOfInstallations);
    XCTAssertTrue(_productCharge.totalCharges == totalCharges);
}

- (void)testInitProductChargeWithInvalidData {
    
    _productCharge = [[BTProductCharge alloc] initProductChargeWithData:nil];
    
    XCTAssertNotNil(_productCharge, @"BTProductCharge instance is not created.");
    
}

@end
