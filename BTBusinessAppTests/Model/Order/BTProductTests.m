//
//  BTProductTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/23/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BTProduct.h"
#import "AppManager.h"

@interface BTProductTests : XCTestCase

@property (nonatomic) BTProduct *product;

@end

@implementation BTProductTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _product = nil;
}

- (void)testInitProductWithResponseAndIndexInApiResponse {
    
    NSString *orderKey = @"BT012345";
    NSString *itemRef = @"BT0123451-1";
    NSString *productName = @"productname";
    NSString *description = @"description";
    NSString *dueDate = @"12 May 2017";
    double oneOffPrice = 1.12;
    double monthlyPrice = 1.13;
    NSString *expectedDispatchDate = @"2017-12-12T12:12:12Z";
    NSString *engineeringAppointmentDate = @"2017-12-12T12:12:12Z";
    BOOL isCeasedOrCancelled = YES;
    NSInteger index = 1;
    int productType = 2;
    
    NSDictionary *productDict = [[NSDictionary alloc] initWithObjectsAndKeys:orderKey, @"OrderKey", itemRef, @"OrderItemReference", productName, @"ProductName", description, @"ProductDescription", dueDate, @"ProductDuedate", [NSNumber numberWithDouble:oneOffPrice], @"OneOffPrice", [NSNumber numberWithDouble:monthlyPrice], @"MonthlyPrice", expectedDispatchDate, @"ExpectedDispatchDate", engineeringAppointmentDate, @"EngineeringAppointmentDate", [NSNumber numberWithBool:isCeasedOrCancelled], @"isCeaseOrCancelled", [NSNumber numberWithInt:productType], @"ProductType", nil];
    
    _product = [[BTProduct alloc] initProductWithResponse:productDict andIndexInAPIResponse:index];
    
    XCTAssertNotNil(_product, @"BTProduct instance is not created.");
    XCTAssertTrue([_product.orderKey isEqualToString:orderKey]);
    XCTAssertTrue([_product.orderItemReference isEqualToString:itemRef]);
    XCTAssertTrue([_product.productName isEqualToString:productName]);
    XCTAssertTrue([_product.productDescription isEqualToString:description]);
    XCTAssertTrue([_product.productDuedate compare:[AppManager NSDateWithMonthNameWithSpaceFormatFromNSString:dueDate]] == NSOrderedSame);
    XCTAssertTrue(_product.oneOffPrice == oneOffPrice);
    XCTAssertTrue(_product.monthlyPrice == monthlyPrice);
    XCTAssertTrue([_product.expectedDispatchDate compare:[AppManager NSDateWithUTCFormatFromNSString:expectedDispatchDate]] == NSOrderedSame);
    XCTAssertTrue([_product.engineeringAppointmentDate compare:[AppManager NSDateWithUTCFormatFromNSString:engineeringAppointmentDate]] == NSOrderedSame);
    XCTAssertTrue(_product.isCeaseOrCancelled);
    XCTAssertTrue(_product.indexInAPIResponse == index);
    XCTAssertTrue(_product.productType == productType);
    
}

- (void)testInitProductWithInvalidResponseAndIndexInApiResponse {
    
    _product = [[BTProduct alloc] initProductWithResponse:nil andIndexInAPIResponse:1];
    
    XCTAssertNotNil(_product, @"BTProduct instance is not created.");
    
}

- (void)testUpdateProductDuedateWithNSDate {
    
    _product = [[BTProduct alloc] init];
    
    NSDate *newDueDate = [NSDate date];
    [_product updateProductDuedateWithNSDate:newDueDate];
    
    XCTAssertTrue([_product.productDuedate compare:newDueDate] == NSOrderedSame);
}

- (void)testUpdateEngineeringAppointmentDateWithNSDate {
    
    _product = [[BTProduct alloc] init];
    
    NSDate *newAppointmentDate = [NSDate date];
    [_product updateEngineeringAppointmentDateWithNSDate:newAppointmentDate];
    
    XCTAssertTrue([_product.engineeringAppointmentDate compare:newAppointmentDate] == NSOrderedSame);
}

- (void)testUpdateIsProductRelatedToStrategicOrder {
    
    _product = [[BTProduct alloc] init];
    
    NSDictionary *orderInfo = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithBool:YES], @"IsStrategic", nil];
    
    [_product updateIsProductRelatedToStrategicOrderWith:orderInfo];
    
    XCTAssertTrue(_product.isRelatedOrderStrategic);
}

@end
