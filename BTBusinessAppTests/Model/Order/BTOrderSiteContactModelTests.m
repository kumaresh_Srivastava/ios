//
//  BTOrderSiteContactModelTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/22/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BTOrderSiteContactModel.h"

@interface BTOrderSiteContactModelTests : XCTestCase

@property (nonatomic) BTOrderSiteContactModel *siteContactModel;

@end

@implementation BTOrderSiteContactModelTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _siteContactModel = nil;
}

- (void)testInitSiteContactWithResponseDict {
    
    NSMutableDictionary *mockContactDict = [NSMutableDictionary dictionary];
    
    NSString *contactContext = @"contactcontext";
    NSString *contactKey = @"contactkey";
    NSString *email = @"email";
    NSString *extension = @"ext";
    NSString *firstName = @"firstname";
    NSString *lastName = @"lastname";
    NSString *title = @"title";
    NSString *mobile = @"mobile";
    NSString *preferredContact = @"mobile";
    NSString *phone = @"phone";
    
    [mockContactDict setValue:contactContext forKey:@"ContactContext"];
    [mockContactDict setValue:contactKey forKey:@"ContactKey"];
    [mockContactDict setValue:email forKey:@"Email"];
    [mockContactDict setValue:extension forKey:@"Extension"];
    [mockContactDict setValue:firstName forKey:@"FirstName"];
    [mockContactDict setValue:lastName forKey:@"LastName"];
    [mockContactDict setValue:title forKey:@"Title"];
    [mockContactDict setValue:mobile forKey:@"Mobile"];
    [mockContactDict setValue:preferredContact forKey:@"PreferredContact"];
    [mockContactDict setValue:phone forKey:@"Phone"];
    
    _siteContactModel = [[BTOrderSiteContactModel alloc] initSiteContactWithResponseDict:mockContactDict];
    
    XCTAssertNotNil(_siteContactModel, @"BTContact instance is not created.");
    XCTAssertTrue([_siteContactModel.contactContext isEqualToString:contactContext]);
    XCTAssertTrue([_siteContactModel.contactKey isEqualToString:contactKey]);
    XCTAssertTrue([_siteContactModel.email isEqualToString:email]);
    XCTAssertTrue([_siteContactModel.extension isEqualToString:extension]);
    XCTAssertTrue([_siteContactModel.firstName isEqualToString:firstName]);
    XCTAssertTrue([_siteContactModel.lastName isEqualToString:lastName]);
    XCTAssertTrue([_siteContactModel.title isEqualToString:title]);
    XCTAssertTrue([_siteContactModel.mobile isEqualToString:mobile]);
    XCTAssertTrue([_siteContactModel.preferredContact isEqualToString:preferredContact]);
    XCTAssertTrue([_siteContactModel.phone isEqualToString:phone]);
    
}

- (void)testInitSiteContactWithInvalidResponseDict {
    
    _siteContactModel = [[BTOrderSiteContactModel alloc] initSiteContactWithResponseDict:nil];
    
    XCTAssertNotNil(_siteContactModel, @"BTContact instance is not created.");
    
}

- (void)testInitWithResponseDictionaryFromSiteContactsAPIResponse {
    
    NSMutableDictionary *mockContactDict = [NSMutableDictionary dictionary];
    
    NSString *contactKey = @"contactkey";
    NSString *email = @"email";
    NSString *firstName = @"firstname";
    NSString *lastName = @"lastname";
    NSString *title = @"title";
    NSString *mobile = @"mobile";
    NSString *phone = @"phone";
    NSString *formattedContactLine = @"formattedcontactline";
    
    [mockContactDict setValue:contactKey forKey:@"ContactKey"];
    [mockContactDict setValue:email forKey:@"Email"];
    [mockContactDict setValue:firstName forKey:@"FirstName"];
    [mockContactDict setValue:lastName forKey:@"LastName"];
    [mockContactDict setValue:title forKey:@"Title"];
    [mockContactDict setValue:mobile forKey:@"Mobile"];
    [mockContactDict setValue:phone forKey:@"Phone"];
    [mockContactDict setValue:formattedContactLine forKey:@"ForamattedContactLine"];
    
    _siteContactModel = [[BTOrderSiteContactModel alloc] initWithResponseDictionaryFromSiteContactsAPIResponse:mockContactDict];
    
    XCTAssertNotNil(_siteContactModel, @"BTContact instance is not created.");
    XCTAssertTrue([_siteContactModel.contactKey isEqualToString:contactKey]);
    XCTAssertTrue([_siteContactModel.email isEqualToString:email]);
    XCTAssertTrue([_siteContactModel.firstName isEqualToString:firstName]);
    XCTAssertTrue([_siteContactModel.lastName isEqualToString:lastName]);
    XCTAssertTrue([_siteContactModel.title isEqualToString:title]);
    XCTAssertTrue([_siteContactModel.mobile isEqualToString:mobile]);
    XCTAssertTrue([_siteContactModel.phone isEqualToString:phone]);
    XCTAssertTrue([_siteContactModel.formattedContactLine isEqualToString:formattedContactLine]);
}

- (void)testInitWithResponseDictionaryFromInvalidSiteContactsAPIResponse {
    
    _siteContactModel = [[BTOrderSiteContactModel alloc] initWithResponseDictionaryFromSiteContactsAPIResponse:nil];
    
    XCTAssertNotNil(_siteContactModel, @"BTContact instance is not created.");

}

- (void)testInitSiteContactWithAddedSiteContactDict {
    
    NSMutableDictionary *mockContactDict = [NSMutableDictionary dictionary];
    
    NSString *contactContext = @"contactcontext";
    NSString *contactKey = @"contactkey";
    NSString *email = @"email";
    NSString *extension = @"ext";
    NSString *firstName = @"firstname";
    NSString *lastName = @"lastname";
    NSString *title = @"title";
    NSString *mobile = @"mobile";
    NSString *preferredContact = @"mobile";
    NSString *phone = @"phone";
    
    [mockContactDict setValue:contactContext forKey:@"ContactContext"];
    [mockContactDict setValue:contactKey forKey:@"ContactKey"];
    [mockContactDict setValue:email forKey:@"Email"];
    [mockContactDict setValue:extension forKey:@"Extension"];
    [mockContactDict setValue:firstName forKey:@"FirstName"];
    [mockContactDict setValue:lastName forKey:@"LastName"];
    [mockContactDict setValue:title forKey:@"Title"];
    [mockContactDict setValue:mobile forKey:@"altPhone"];
    [mockContactDict setValue:preferredContact forKey:@"PreferredContact"];
    [mockContactDict setValue:phone forKey:@"Phone"];
    
    _siteContactModel = [[BTOrderSiteContactModel alloc] initSiteContactWithAddedSiteContactDict:mockContactDict];
    
    XCTAssertNotNil(_siteContactModel, @"BTContact instance is not created.");
    XCTAssertTrue([_siteContactModel.contactContext isEqualToString:contactContext]);
    XCTAssertTrue([_siteContactModel.contactKey isEqualToString:contactKey]);
    XCTAssertTrue([_siteContactModel.email isEqualToString:email]);
    XCTAssertTrue([_siteContactModel.extension isEqualToString:extension]);
    XCTAssertTrue([_siteContactModel.firstName isEqualToString:firstName]);
    XCTAssertTrue([_siteContactModel.lastName isEqualToString:lastName]);
    XCTAssertTrue([_siteContactModel.title isEqualToString:title]);
    XCTAssertTrue([_siteContactModel.mobile isEqualToString:mobile]);
    XCTAssertTrue([_siteContactModel.preferredContact isEqualToString:preferredContact]);
    XCTAssertTrue([_siteContactModel.phone isEqualToString:phone]);
}

- (void)testInitSiteContactWithAddedInvalidSiteContactDict {
    
    _siteContactModel = [[BTOrderSiteContactModel alloc] initSiteContactWithAddedSiteContactDict:nil];
    
    XCTAssertNotNil(_siteContactModel, @"BTContact instance is not created.");
    
}

- (void)testInitWithResponseDictionaryFromInvalidFaultContactDetailsAPIResponse {
    
    _siteContactModel = [[BTOrderSiteContactModel alloc] initWithResponseDictionaryFromFaultContactDetailsAPIResponse:nil];
    
    XCTAssertNotNil(_siteContactModel, @"BTContact instance is not created.");
    
}

@end
