//
//  BTFaultAppointmentSlotDetailTests.m
//  BTBusinessApp
//
//  Created by Rohini Kumar on 29/08/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BTFaultAppointmentSlotDetail.h"

@interface BTFaultAppointmentSlotDetailTests : XCTestCase
@property (nonatomic) BTFaultAppointmentSlotDetail *appointmentSlotDetailModel;
@end

@implementation BTFaultAppointmentSlotDetailTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _appointmentSlotDetailModel = [[BTFaultAppointmentSlotDetail alloc] init];
    XCTAssertNotNil(_appointmentSlotDetailModel);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    _appointmentSlotDetailModel = nil;
}

#pragma mark -
#pragma mark initWithFaultAppointmentSlotArray

- (void) testInitWithFaultAppointmentSlotArray {
    
    NSArray *faultAppointmentSlotArray = [NSArray arrayWithObject:@"sample Object"];
    NSDate *previousSlotDate = [NSDate dateWithTimeIntervalSinceNow:100];
    NSDate *laterSlotDate = [NSDate dateWithTimeIntervalSinceNow:200];
    
    id returnedObject = [_appointmentSlotDetailModel initWithFaultAppointmentSlotArray:faultAppointmentSlotArray priviousSlotDate:previousSlotDate andLaterSlotDate:laterSlotDate];
    XCTAssertNotNil(returnedObject, @"object should not be nil");
    
    NSArray *returnedFaultAppointmentSlotArray = [_appointmentSlotDetailModel valueForKey:@"_faultAppointmentSlotArray"];
    NSDate *returnedpreviousSlotDate = [_appointmentSlotDetailModel valueForKey:@"_previousSlotDate"];
    NSDate *returnedlaterSlotDate = [_appointmentSlotDetailModel valueForKey:@"_laterSlotDate"];
    
    XCTAssertTrue(faultAppointmentSlotArray == returnedFaultAppointmentSlotArray, @"test case failed, both values should be same");
    XCTAssertTrue(([previousSlotDate compare:returnedpreviousSlotDate] == NSOrderedSame), @"test case failed, both values should be same");
    XCTAssertTrue(([laterSlotDate compare:returnedlaterSlotDate] == NSOrderedSame), @"test case failed, both values should be same");
}

- (void) testInitWithFaultAppointmentSlotArrayNilValues {
    
    id returnedObject = [_appointmentSlotDetailModel initWithFaultAppointmentSlotArray:nil priviousSlotDate:nil andLaterSlotDate:nil];
    XCTAssertNotNil(returnedObject, @"object should not be nil");
    
    NSArray *returnedFaultAppointmentSlotArray = [_appointmentSlotDetailModel valueForKey:@"_faultAppointmentSlotArray"];
    NSDate *returnedpreviousSlotDate = [_appointmentSlotDetailModel valueForKey:@"_previousSlotDate"];
    NSDate *returnedlaterSlotDate = [_appointmentSlotDetailModel valueForKey:@"_laterSlotDate"];
    
    XCTAssertNil(returnedFaultAppointmentSlotArray,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedpreviousSlotDate,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedlaterSlotDate,@"Test case failed, Object should be nil");
}
@end
