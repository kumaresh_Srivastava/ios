//
//  BTFaultTests.m
//  BTBusinessApp
//
//  Created by Rohini Kumar on 28/08/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "BTFault.h"
#import "AppManager.h"
#import "BTFaultAppointmentDetail.h"

@interface BTFaultTests : XCTestCase
@property (nonatomic) BTFault *faultModel;
@end

@implementation BTFaultTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _faultModel = [[BTFault alloc] init];
    XCTAssertNotNil(_faultModel);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    _faultModel = nil;
}

#pragma mark -
#pragma mark initWithResponseDictionaryFromFaultDetailsAPIRespopnse

- (void) testInitWithResponseDictionaryFromFaultDetailsAPIRespopnse {
    
    NSString *faultReference = @"Smpale fault reference";
    NSString *serviceID = @"Smpale serviceID";
    NSString *status = @"Smpale status";
    NSString *colorWithStatus = @"Smpale colorWithStatus";
    NSString *targetFixDateString = @"10061987";
    NSDate *targetDate = [AppManager NSDateWithMonthNameWithSpaceFormatFromNSString:targetFixDateString];
    NSString *faultLocation = @"Smpale faultLocation";
    NSString *currentMileStone = @"Smpale currentMileStone";
    NSString *currentMileStoneDescription = @"Smpale currentMileStoneDescription";
    NSInteger primaryClickId = 1234;
    NSInteger secondaryClickId = 1234;
    
    NSArray *objects = [NSArray arrayWithObjects:faultReference, serviceID, status, colorWithStatus, targetFixDateString, faultLocation, currentMileStone, currentMileStoneDescription, [NSNumber numberWithInteger:primaryClickId], [NSNumber numberWithInteger:secondaryClickId], nil];
    NSArray *keys = [NSArray arrayWithObjects:@"faultId",@"serviceId",@"statusOnUI",@"colourforStatus",@"targetFixDate",@"faultLocation",@"currentMilestone",@"currentMilestoneDesc",@"btnPrimaryClickID",@"btnSecondaryClickID", nil];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    id returnedObject = [_faultModel initWithResponseDictionaryFromFaultDetailsAPIRespopnse:dict];
    XCTAssertNotNil(returnedObject, @"object should not be nil");
    
    NSString *returnedfaultReference = [_faultModel valueForKey:@"_faultReference"];
    NSString *returnedserviceID = [_faultModel valueForKey:@"_serviceID"];
    NSString *returnedstatus = [_faultModel valueForKey:@"_status"];
    NSString *returnedcolorWithStatus = [_faultModel valueForKey:@"_colourwithStatus"];
    NSDate *returnedtargetFixDate = [_faultModel valueForKey:@"_targetFixDate"];
    NSString *returnedfaultLocation = [_faultModel valueForKey:@"_faultLocation"];
    NSString *returnedcurrentMileStone = [_faultModel valueForKey:@"_currentMilestone"];
    NSString *returnedcurrentMileStoneDescription = [_faultModel valueForKey:@"_currentMilestoneDescription"];
    NSInteger returnedprimaryClickId = [[_faultModel valueForKey:@"_btnPrimaryClickID"] integerValue];
    NSInteger returnedsecondaryClickId = [[_faultModel valueForKey:@"_btnSecondaryClickID"] integerValue];

    XCTAssertTrue([faultReference isEqualToString:returnedfaultReference], @"test case failed, both values should be same");
    XCTAssertTrue([serviceID isEqualToString:returnedserviceID], @"test case failed, both values should be same");
    XCTAssertTrue([status isEqualToString:returnedstatus], @"test case failed, both values should be same");
    XCTAssertTrue([colorWithStatus isEqualToString:returnedcolorWithStatus], @"test case failed, both values should be same");
    XCTAssertTrue(([targetDate compare:returnedtargetFixDate] == NSOrderedSame), @"test case failed, both values should be same");
    XCTAssertTrue([faultLocation isEqualToString:returnedfaultLocation], @"test case failed, both values should be same");
    XCTAssertTrue([currentMileStone isEqualToString:returnedcurrentMileStone], @"test case failed, both values should be same");
    XCTAssertTrue([currentMileStoneDescription isEqualToString:returnedcurrentMileStoneDescription], @"test case failed, both values should be same");
    XCTAssertTrue(primaryClickId == returnedprimaryClickId, @"test case failed, both values should be same");
    XCTAssertTrue(secondaryClickId == returnedsecondaryClickId, @"test case failed, both values should be same");

}


- (void) testInitWithResponseDictionaryFromFaultDetailsAPIRespopnseWithNilDictionary {
   
    id returnedObject = [_faultModel initWithResponseDictionaryFromFaultDetailsAPIRespopnse:nil];
    XCTAssertNotNil(returnedObject, @"object should not be nil");
    
    NSString *returnedfaultReference = [_faultModel valueForKey:@"_faultReference"];
    NSString *returnedserviceID = [_faultModel valueForKey:@"_serviceID"];
    NSString *returnedstatus = [_faultModel valueForKey:@"_status"];
    NSString *returnedcolorWithStatus = [_faultModel valueForKey:@"_colourwithStatus"];
    NSDate *returnedtargetFixDate = [_faultModel valueForKey:@"_targetFixDate"];
    NSString *returnedfaultLocation = [_faultModel valueForKey:@"_faultLocation"];
    NSString *returnedcurrentMileStone = [_faultModel valueForKey:@"_currentMilestone"];
    NSString *returnedcurrentMileStoneDescription = [_faultModel valueForKey:@"_currentMilestoneDescription"];
    NSInteger returnedprimaryClickId = [[_faultModel valueForKey:@"_btnPrimaryClickID"] integerValue];
    NSInteger returnedsecondaryClickId = [[_faultModel valueForKey:@"_btnSecondaryClickID"] integerValue];

    XCTAssertNil(returnedfaultReference,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedserviceID,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedstatus,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedcolorWithStatus,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedtargetFixDate,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedfaultLocation,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedcurrentMileStone,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedcurrentMileStoneDescription,@"Test case failed, Object should be nil");
    XCTAssertTrue(returnedprimaryClickId == 0,@"test case failed, value should be zero");
    XCTAssertTrue(returnedsecondaryClickId == 0,@"test case failed, value should be zero");
}

#pragma mark -
#pragma mark initWithResponseDictionaryFromFaultDashBoardAPIResponse

- (void) testInitWithResponseDictionaryFromFaultDashBoardAPIResponse {
    
    NSString *faultReference = @"Smpale fault reference";
    NSString *productName = @"Smpale serviceID";
    NSString *reportedOnDateString = @"1987-10-02T10:20:23Z";
    NSDate *reportedOnDate = [AppManager NSDateWithUTCFormatFromNSString:reportedOnDateString];
    NSString *status = @"Smpale status";
    
    NSArray *objects = [NSArray arrayWithObjects:faultReference, productName, reportedOnDateString, status, nil];
    NSArray *keys = [NSArray arrayWithObjects:@"FaultReference",@"ProductName",@"ReportedOn",@"Status",nil];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    id returnedObject = [_faultModel initWithResponseDictionaryFromFaultDashBoardAPIResponse:dict];
    XCTAssertNotNil(returnedObject, @"object should not be nil");
    
    NSString *returnedfaultReference = [_faultModel valueForKey:@"_faultReference"];
    NSString *returnedproductName = [_faultModel valueForKey:@"_productName"];
    NSDate *returnedreportedOnDate = [_faultModel valueForKey:@"_reportedOnDate"];
    NSString *returnedstatus = [_faultModel valueForKey:@"_status"];
    
    XCTAssertTrue([faultReference isEqualToString:returnedfaultReference], @"test case failed, both values should be same");
    XCTAssertTrue([productName isEqualToString:returnedproductName], @"test case failed, both values should be same");
    XCTAssertTrue(([reportedOnDate compare:returnedreportedOnDate] == NSOrderedSame), @"test case failed, both values should be same");
    XCTAssertTrue([status isEqualToString:returnedstatus], @"test case failed, both values should be same");
}

- (void) testInitWithResponseDictionaryFromFaultDashBoardAPIResponseWithNilDictionary {
    
    id returnedObject = [_faultModel initWithResponseDictionaryFromFaultDashBoardAPIResponse:nil];
    XCTAssertNotNil(returnedObject, @"object should not be nil");
    
    NSString *returnedfaultReference = [_faultModel valueForKey:@"_faultReference"];
    NSString *returnedproductName = [_faultModel valueForKey:@"_productName"];
    NSDate *returnedreportedOnDate = [_faultModel valueForKey:@"_reportedOnDate"];
    NSString *returnedstatus = [_faultModel valueForKey:@"_status"];
    
    XCTAssertNil(returnedfaultReference,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedproductName,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedreportedOnDate,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedstatus,@"Test case failed, Object should be nil");
}

#pragma mark -
#pragma mark initWithResponseDictionaryFromFaultSummaryAPIResponse

- (void) testInitWithResponseDictionaryFromFaultSummaryAPIResponse {
    
    NSString *faultReference = @"Smpale fault reference";
    NSString *productName = @"Smpale productName";
    NSString *reportedOnDateString = @"1987-10-02T10:20:23Z";
    NSDate *reportedOnDate = [AppManager NSDateWithUTCFormatFromNSString:reportedOnDateString];
    NSString *status = @"Smpale status";
    NSString *customerID = @"Smpale customerID";
    NSString *serviceID = @"Smpale serviceID";
    NSString *colorWithStatus = @"Smpale colorWithStatus";
    NSString *assetID = @"Smpale assetID";
    
    NSArray *objects = [NSArray arrayWithObjects:faultReference, productName, reportedOnDateString, status, customerID, serviceID, colorWithStatus, assetID,nil];
    NSArray *keys = [NSArray arrayWithObjects:@"faultReference",@"productName",@"reportedOn",@"statusOnUI", @"customerId", @"lineNumber", @"colourwithStatus", @"assetId",nil];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    id returnedObject = [_faultModel initWithResponseDictionaryFromFaultSummaryAPIResponse:dict];
    XCTAssertNotNil(returnedObject, @"object should not be nil");
    
    NSString *returnedfaultReference = [_faultModel valueForKey:@"_faultReference"];
    NSString *returnedproductName = [_faultModel valueForKey:@"_productName"];
    NSDate *returnedreportedOnDate = [_faultModel valueForKey:@"_reportedOnDate"];
    NSString *returnedstatus = [_faultModel valueForKey:@"_status"];
    NSString *returnedcustomerID = [_faultModel valueForKey:@"_customerId"];
    NSString *returnedserviceID = [_faultModel valueForKey:@"_serviceID"];
    NSString *returnedcolorWithStatus = [_faultModel valueForKey:@"_colourwithStatus"];
    NSString *returnedassetID = [_faultModel valueForKey:@"_assetId"];
    
    XCTAssertTrue([faultReference isEqualToString:returnedfaultReference], @"test case failed, both values should be same");
    XCTAssertTrue([productName isEqualToString:returnedproductName], @"test case failed, both values should be same");
    XCTAssertTrue(([reportedOnDate compare:returnedreportedOnDate] == NSOrderedSame), @"test case failed, both values should be same");
    XCTAssertTrue([status isEqualToString:returnedstatus], @"test case failed, both values should be same");
    XCTAssertTrue([customerID isEqualToString:returnedcustomerID], @"test case failed, both values should be same");
    XCTAssertTrue([serviceID isEqualToString:returnedserviceID], @"test case failed, both values should be same");
    XCTAssertTrue([colorWithStatus isEqualToString:returnedcolorWithStatus], @"test case failed, both values should be same");
    XCTAssertTrue([assetID isEqualToString:returnedassetID], @"test case failed, both values should be same");
}

- (void) testInitWithResponseDictionaryFromFaultSummaryAPIResponseWithNilDictionary {
    
    id returnedObject = [_faultModel initWithResponseDictionaryFromFaultSummaryAPIResponse:nil];
    XCTAssertNotNil(returnedObject, @"object should not be nil");
    
    NSString *returnedfaultReference = [_faultModel valueForKey:@"_faultReference"];
    NSString *returnedproductName = [_faultModel valueForKey:@"_productName"];
    NSDate *returnedreportedOnDate = [_faultModel valueForKey:@"_reportedOnDate"];
    NSString *returnedstatus = [_faultModel valueForKey:@"_status"];
    NSString *returnedcustomerID = [_faultModel valueForKey:@"_customerId"];
    NSString *returnedserviceID = [_faultModel valueForKey:@"_serviceID"];
    NSString *returnedcolorWithStatus = [_faultModel valueForKey:@"_colourwithStatus"];
    NSString *returnedassetID = [_faultModel valueForKey:@"_assetId"];
    
    XCTAssertNil(returnedfaultReference,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedproductName,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedreportedOnDate,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedstatus,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedcustomerID,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedserviceID,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedcolorWithStatus,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedassetID,@"Test case failed, Object should be nil");
}

#pragma mark -
#pragma mark initWithResponseDictionaryFromFaultServiceIDAPIResponse

- (void) testInitWithResponseDictionaryFromFaultServiceIDAPIResponse {
    
    NSString *faultReference = @"Smpale fault reference";
    NSString *reportedOnDateString = @"1987-10-02T10:20:23Z";
    NSDate *reportedOnDate = [AppManager NSDateWithUTCFormatFromNSString:reportedOnDateString];
    NSString *status = @"Smpale status";
    NSString *substatus = @"Smpale substatus";
    NSString *statusOnUI = @"Smpale statusOnUI";
    NSString *assetID = @"Smpale assetID";
    NSString *productName = @"Smpale productName";
    NSString *colorWithStatus = @"Smpale colorWithStatus";
    NSString *lineNumber = @"Smpale lineNumber";
    BOOL isVissible = YES;
    
    NSArray *objects = [NSArray arrayWithObjects:faultReference, reportedOnDateString, status, substatus, statusOnUI, assetID, productName, colorWithStatus, lineNumber, [NSNumber numberWithBool:isVissible], nil];
    NSArray *keys = [NSArray arrayWithObjects:@"faultReference",@"reportedOn",@"status",@"subStatus", @"statusOnUI", @"assetId", @"productName", @"colourwithStatus", @"lineNumber", @"IsVisible", nil];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    id returnedObject = [_faultModel initWithResponseDictionaryFromFaultServiceIDAPIResponse:dict];
    XCTAssertNotNil(returnedObject, @"object should not be nil");
    
    NSString *returnedfaultReference = [_faultModel valueForKey:@"_faultReference"];
    NSDate *returnedreportedOnDate = [_faultModel valueForKey:@"_reportedOnDate"];
    NSString *returnedstatus = [_faultModel valueForKey:@"_status"];
    NSString *returnedsubstatus = [_faultModel valueForKey:@"_subStatus"];
    NSString *returnedstatusOnUI = [_faultModel valueForKey:@"_statusOnUI"];
    NSString *returnedassetID = [_faultModel valueForKey:@"_assetId"];
    NSString *returnedproductName = [_faultModel valueForKey:@"_productName"];
    NSString *returnedcolorWithStatus = [_faultModel valueForKey:@"_colourwithStatus"];
    NSString *returnedlineNumber = [_faultModel valueForKey:@"_lineNumber"];
    BOOL returnedIsVissible = [[_faultModel valueForKey:@"_isVissible"] boolValue];
    
    XCTAssertTrue([faultReference isEqualToString:returnedfaultReference], @"test case failed, both values should be same");
    XCTAssertTrue(([reportedOnDate compare:returnedreportedOnDate] == NSOrderedSame), @"test case failed, both values should be same");
    XCTAssertTrue([status isEqualToString:returnedstatus], @"test case failed, both values should be same");
    XCTAssertTrue([substatus isEqualToString:returnedsubstatus], @"test case failed, both values should be same");
    XCTAssertTrue([statusOnUI isEqualToString:returnedstatusOnUI], @"test case failed, both values should be same");
    XCTAssertTrue([assetID isEqualToString:returnedassetID], @"test case failed, both values should be same");
    XCTAssertTrue([productName isEqualToString:returnedproductName], @"test case failed, both values should be same");
    XCTAssertTrue([colorWithStatus isEqualToString:returnedcolorWithStatus], @"test case failed, both values should be same");
    XCTAssertTrue([lineNumber isEqualToString:returnedlineNumber], @"test case failed, both values should be same");
    XCTAssertTrue(isVissible == returnedIsVissible, @"test case failed, both values should be same");
}

- (void) testInitWithResponseDictionaryFromFaultServiceIDAPIResponseWithNilDictionary {
    
    id returnedObject = [_faultModel initWithResponseDictionaryFromFaultServiceIDAPIResponse:nil];
    XCTAssertNotNil(returnedObject, @"object should not be nil");
    
    NSString *returnedfaultReference = [_faultModel valueForKey:@"_faultReference"];
    NSDate *returnedreportedOnDate = [_faultModel valueForKey:@"_reportedOnDate"];
    NSString *returnedstatus = [_faultModel valueForKey:@"_status"];
    NSString *returnedsubstatus = [_faultModel valueForKey:@"_subStatus"];
    NSString *returnedstatusOnUI = [_faultModel valueForKey:@"_statusOnUI"];
    NSString *returnedassetID = [_faultModel valueForKey:@"_assetId"];
    NSString *returnedproductName = [_faultModel valueForKey:@"_productName"];
    NSString *returnedcolorWithStatus = [_faultModel valueForKey:@"_colourwithStatus"];
    NSString *returnedlineNumber = [_faultModel valueForKey:@"_lineNumber"];
    BOOL returnedIsVissible = [[_faultModel valueForKey:@"_isVissible"] boolValue];
    
    XCTAssertNil(returnedfaultReference,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedreportedOnDate,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedstatus,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedsubstatus,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedstatusOnUI,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedassetID,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedproductName,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedcolorWithStatus,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedlineNumber,@"Test case failed, Object should be nil");
    XCTAssertFalse(returnedIsVissible,@"Test case failed, value should be false");
}

#pragma mark -
#pragma mark updateFaultWithFullDetailsWithData

- (void) testUpdateFaultWithFullDetailsWithData {
    
    NSString *faultLocation = @"Smpale faultLocation";
    NSString *targetFixDateString = @"100011987";
    NSDate *targetFixDate = [AppManager NSDateWithMonthNameWithSpaceFormatFromNSString:targetFixDateString];
    NSString *currentMilestone = @"Smpale currentMilestone";
    NSString *currentMilestoneDescription = @"Smpale currentMilestoneDescription";
    NSInteger btnPrimaryClickID = 1234;
    NSInteger btnSecondaryClickID = 1234;
    
    NSArray *objects = [NSArray arrayWithObjects:faultLocation, targetFixDateString, currentMilestone, currentMilestoneDescription, [NSNumber numberWithInteger:btnPrimaryClickID], [NSNumber numberWithInteger:btnSecondaryClickID], nil];
    NSArray *keys = [NSArray arrayWithObjects:@"faultLocation",@"targetFixDate",@"currentMilestone",@"currentMilestoneDesc", @"btnPrimaryClickID", @"btnSecondaryClickID",nil];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    [_faultModel updateFaultWithFullDetailsWithData:dict];
    
    NSString *returnedfaultLocation = [_faultModel valueForKey:@"_faultLocation"];
    NSDate *returnedtargetFixDate = [_faultModel valueForKey:@"_targetFixDate"];
    NSString *returnedcurrentMilestone = [_faultModel valueForKey:@"_currentMilestone"];
    NSString *returnedcurrentMilestoneDescription = [_faultModel valueForKey:@"_currentMilestoneDescription"];
    NSInteger returnedbtnPrimaryClickID = [[_faultModel valueForKey:@"_btnPrimaryClickID"] integerValue];
    NSInteger returnedbtnSecondaryClickID = [[_faultModel valueForKey:@"_btnSecondaryClickID"] integerValue];
    
    XCTAssertTrue([faultLocation isEqualToString:returnedfaultLocation], @"test case failed, both values should be same");
    XCTAssertTrue(([targetFixDate compare:returnedtargetFixDate] == NSOrderedSame), @"test case failed, both values should be same");
    XCTAssertTrue([currentMilestone isEqualToString:returnedcurrentMilestone], @"test case failed, both values should be same");
    XCTAssertTrue([currentMilestoneDescription isEqualToString:returnedcurrentMilestoneDescription], @"test case failed, both values should be same");
    XCTAssertTrue(btnPrimaryClickID == returnedbtnPrimaryClickID, @"test case failed, both values should be same");
    XCTAssertTrue(btnSecondaryClickID == returnedbtnSecondaryClickID, @"test case failed, both values should be same");
}

- (void) testUpdateFaultWithFullDetailsWithDataWithNilDictionary {
    
    [_faultModel updateFaultWithFullDetailsWithData:nil];
    
    NSString *returnedfaultLocation = [_faultModel valueForKey:@"_faultLocation"];
    NSDate *returnedtargetFixDate = [_faultModel valueForKey:@"_targetFixDate"];
    NSString *returnedcurrentMilestone = [_faultModel valueForKey:@"_currentMilestone"];
    NSString *returnedcurrentMilestoneDescription = [_faultModel valueForKey:@"_currentMilestoneDescription"];
    NSInteger returnedbtnPrimaryClickID = [[_faultModel valueForKey:@"_btnPrimaryClickID"] integerValue];
    NSInteger returnedbtnSecondaryClickID = [[_faultModel valueForKey:@"_btnSecondaryClickID"] integerValue];
    
    XCTAssertNil(returnedfaultLocation,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedtargetFixDate,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedcurrentMilestone,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedcurrentMilestoneDescription,@"Test case failed, Object should be nil");
    XCTAssertTrue(returnedbtnPrimaryClickID == 0, @"test case failed, value should be zero");
    XCTAssertTrue(returnedbtnSecondaryClickID == 0, @"test case failed, value should be zero");
}

#pragma mark -
#pragma mark updateReportedOnDate 

- (void) testUpdateReportedOnDate {
    NSDate *reporteddate = [NSDate date];
    [_faultModel updateReportedOnDate:reporteddate];
    NSDate *returnedDate = [_faultModel valueForKey:@"_reportedOnDate"];
    
    XCTAssertTrue(([reporteddate compare:returnedDate] == NSOrderedSame), @"testcasefailed, date should be same");
}

#pragma mark -
#pragma mark updateFaultWithAppointmentDetails

- (void) testUpdateFaultWithAppointmentDetails {
    BTFaultAppointmentDetail *detail = [[BTFaultAppointmentDetail alloc] init];
    [_faultModel updateFaultWithAppointmentDetails:detail];
    id returnedDetail = (BTFaultAppointmentDetail *)[_faultModel valueForKey:@"_faultAppointmentDetail"];
    
    XCTAssertTrue(detail == returnedDetail, @"testcasefailed, date should be same");
}

@end
