//
//  BTFaultAppointmentTests.m
//  BTBusinessApp
//
//  Created by Rohini Kumar on 29/08/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BTFaultAppointment.h"

@interface BTFaultAppointmentTests : XCTestCase
@property (nonatomic) BTFaultAppointment *faultAppointmentModel;
@end

@interface BTFaultAppointment (test)
- (NSDate *)getDateFromDateString:(NSString *)dateString;
@end
@implementation BTFaultAppointmentTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _faultAppointmentModel = [[BTFaultAppointment alloc] init];
    XCTAssertNotNil(_faultAppointmentModel);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    _faultAppointmentModel = nil;
}

#pragma mark -
#pragma mark initWithResponseDictionaryFromFaultAppointmentsAPIResponse

- (void) testInitWithResponseDictionaryFromFaultAppointmentsAPIResponse {
    
    NSString *dateInfoString = @"10/06/1987";
    NSDate *dateInfo = [_faultAppointmentModel getDateFromDateString:dateInfoString];
    BOOL amSlot = YES;
    BOOL pmSlot = NO;
    NSString *morningDateShift = @"Smpale morningDateShift";
    NSString *eveningDateShift = @"Smpale eveningDateShift";
    BOOL isAmSelected = YES;
    BOOL isPmSelected = NO;
    
    NSArray *objects = [NSArray arrayWithObjects:dateInfoString, [NSNumber numberWithBool:amSlot], [NSNumber numberWithBool:pmSlot], morningDateShift, eveningDateShift, [NSNumber numberWithBool:isPmSelected], [NSNumber numberWithBool:isAmSelected], nil];
    NSArray *keys = [NSArray arrayWithObjects:@"slotDate",@"AMSlot",@"PMSlot",@"slotMorningDateShift",@"slotEveningDateShift",@"isPMSelected",@"isAMSelected", nil];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    id returnedObject = [_faultAppointmentModel initWithResponseDictionaryFromFaultAppointmentsAPIResponse:dict];
    XCTAssertNotNil(returnedObject, @"object should not be nil");
    
    NSDate *returneddateinfo = [_faultAppointmentModel valueForKey:@"_dateInfo"];
    BOOL returnedamSlot = [[_faultAppointmentModel valueForKey:@"_AMSlot"] boolValue];
    BOOL returnedpmSlot = [[_faultAppointmentModel valueForKey:@"_PMSlot"] boolValue];
    NSString *returnedmorningDateShift = [_faultAppointmentModel valueForKey:@"_morningDateShift"];
    NSString *returnedeveningDateShift = [_faultAppointmentModel valueForKey:@"_eveningDateShift"];
    BOOL returnedPmSelected = [[_faultAppointmentModel valueForKey:@"_isPmSelected"] boolValue];
    BOOL returnedisAmSelected = [[_faultAppointmentModel valueForKey:@"_isAmSelected"] boolValue];
    
    XCTAssertTrue(([dateInfo compare:returneddateinfo] == NSOrderedSame), @"test case failed, both values should be same");
    XCTAssertTrue(amSlot == returnedamSlot, @"test case failed, both values should be same");
    XCTAssertTrue(pmSlot == returnedpmSlot, @"test case failed, both values should be same");
    XCTAssertTrue([morningDateShift isEqualToString:returnedmorningDateShift], @"test case failed, both values should be same");
    XCTAssertTrue([eveningDateShift isEqualToString:returnedeveningDateShift], @"test case failed, both values should be same");
    XCTAssertTrue(isPmSelected == returnedPmSelected, @"test case failed, both values should be same");
    XCTAssertTrue(isAmSelected == returnedisAmSelected, @"test case failed, both values should be same");

}


- (void) testInitWithResponseDictionaryFromFaultAppointmentsAPIResponseWithNilDictionary {
    
    id returnedObject = [_faultAppointmentModel initWithResponseDictionaryFromFaultAppointmentsAPIResponse:nil];
    XCTAssertNotNil(returnedObject, @"object should not be nil");
    
    NSDate *returneddateinfo = [_faultAppointmentModel valueForKey:@"_dateInfo"];
    BOOL returnedamSlot = [[_faultAppointmentModel valueForKey:@"_AMSlot"] boolValue];
    BOOL returnedpmSlot = [[_faultAppointmentModel valueForKey:@"_PMSlot"] boolValue];
    NSString *returnedmorningDateShift = [_faultAppointmentModel valueForKey:@"_morningDateShift"];
    NSString *returnedeveningDateShift = [_faultAppointmentModel valueForKey:@"_eveningDateShift"];
    BOOL returnedPmSelected = [[_faultAppointmentModel valueForKey:@"_isPmSelected"] boolValue];
    BOOL returnedisAmSelected = [[_faultAppointmentModel valueForKey:@"_isAmSelected"] boolValue];
    
    XCTAssertNil(returneddateinfo,@"Test case failed, Object should be nil");
    XCTAssertFalse(returnedamSlot,@"test case failed, value should be false");
    XCTAssertFalse(returnedpmSlot,@"test case failed, value should be false");
    XCTAssertNil(returnedmorningDateShift,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedeveningDateShift,@"Test case failed, Object should be nil");
    XCTAssertFalse(returnedPmSelected,@"test case failed, value should be false");
    XCTAssertFalse(returnedisAmSelected,@"test case failed, value should be false");
}

#pragma mark -
#pragma mark updateFaultAppointmentWithCurrentAppointmentDate

- (void) testUpdateFaultAppointmentWithCurrentAppointmentDateGreaterThan13 {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfMonth | NSCalendarUnitDay | NSCalendarUnitHour ) fromDate:[NSDate date]];
    [components setHour:6];
    NSDate *todayAt6AM = [calendar dateFromComponents:components];
    
    [_faultAppointmentModel updateFaultAppointmentWithCurrentAppointmentDate:todayAt6AM];
    BOOL isAMSlot = [[_faultAppointmentModel valueForKey:@"_AMSlot"] boolValue];
    XCTAssertFalse(isAMSlot,@"Testcase failed, _AMSlot should be false");
}

- (void) testUpdateFaultAppointmentWithCurrentAppointmentDateLessThan13 {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:( NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfMonth | NSCalendarUnitDay | NSCalendarUnitHour ) fromDate:[NSDate date]];
    [components setHour:18];
    NSDate *todayAt6PM = [calendar dateFromComponents:components];
    [_faultAppointmentModel updateFaultAppointmentWithCurrentAppointmentDate:todayAt6PM];
    BOOL isPMSlot = [[_faultAppointmentModel valueForKey:@"_PMSlot"] boolValue];
    XCTAssertFalse(isPMSlot,@"Testcase failed, _PMSlot should be false");

}

@end
