//
//  BTCallDiversionDetailTests.m
//  BTBusinessApp
//
//  Created by Rohini Kumar on 28/08/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "BTCallDiversionDetail.h"

@interface BTCallDiversionDetailTests : XCTestCase
@property (nonatomic) BTCallDiversionDetail *callDiversionDetailModel;
@end

@implementation BTCallDiversionDetailTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _callDiversionDetailModel = [[BTCallDiversionDetail alloc] init];
    XCTAssertNotNil(_callDiversionDetailModel);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    _callDiversionDetailModel = nil;
}

#pragma mark -
#pragma mark initCallDiversionDetailWithResponse

- (void) testInitCallDiversionDetailWithResponse {
    NSString *callDivertFlag = @"callDivertFlag";
    BOOL showDivertBool = YES;
    NSNumber *showDivertFlag = [NSNumber numberWithBool:showDivertBool];
    NSDate *callDivertDate = [NSDate date];
    NSString *custCug = @"custcugValue";
    NSString *callDivertNumber = @"calldivertnumberValue";
    
    NSArray *keys = [NSArray arrayWithObjects:@"callDivertFlag",@"showDivert",@"callDivertDate",@"CustCug",@"calldivertNumber", nil];
    NSArray *objects = [NSArray arrayWithObjects:callDivertFlag, showDivertFlag,callDivertDate, custCug,callDivertNumber, nil];
    NSDictionary *dict = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    
    id returnedObject = [_callDiversionDetailModel initCallDiversionDetailWithResponse:dict];
    XCTAssertNotNil(returnedObject, @"test case failed, returnedobject should not be nil");
    
    NSString *returnedcallDivertFlag = [_callDiversionDetailModel valueForKey:@"_callDivertFlag"];
    BOOL returnedshowDivertFlag = [_callDiversionDetailModel valueForKey:@"_showDivertFlag"];
    NSDate *returnedcallDivertDate = [_callDiversionDetailModel valueForKey:@"_callDivertDate"];
    NSString *returnedcustCug = [_callDiversionDetailModel valueForKey:@"_customerCug"];
    NSString *returnedcallDivertNumber = [_callDiversionDetailModel valueForKey:@"_callDivertNumber"];

    XCTAssertTrue([callDivertFlag isEqualToString:returnedcallDivertFlag], @"both objects should be equal");
    XCTAssertTrue(showDivertBool == returnedshowDivertFlag, @"both values should be equal");
    XCTAssertTrue(([callDivertDate compare:returnedcallDivertDate] == NSOrderedSame), @"both objects should be equal");
    XCTAssertTrue([custCug isEqualToString:returnedcustCug], @"both objects should be equal");
    XCTAssertTrue([callDivertNumber isEqualToString:returnedcallDivertNumber], @"both objects should be equal");

}

- (void) testInitCallDiversionDetailWithResponseWithNilDict {
    id returnedObject = [_callDiversionDetailModel initCallDiversionDetailWithResponse:nil];
    XCTAssertNotNil(returnedObject, @"test case failed, returnedobject should not be nil");
    
    NSString *returnedcallDivertFlag = [_callDiversionDetailModel valueForKey:@"_callDivertFlag"];
    BOOL returnedshowDivertFlag = [[_callDiversionDetailModel valueForKey:@"_showDivertFlag"] boolValue];
    NSDate *returnedcallDivertDate = [_callDiversionDetailModel valueForKey:@"_callDivertDate"];
    NSString *returnedcustCug = [_callDiversionDetailModel valueForKey:@"_customerCug"];
    NSString *returnedcallDivertNumber = [_callDiversionDetailModel valueForKey:@"_callDivertNumber"];

    XCTAssertNil(returnedcallDivertFlag, @"test case failed, object should have nil");
    XCTAssertFalse(returnedshowDivertFlag, @"bool value should have NO value");
    XCTAssertNil(returnedcallDivertDate, @"test case failed, object should have nil");
    XCTAssertNil(returnedcustCug, @"test case failed, object should have nil");
    XCTAssertNil(returnedcallDivertNumber, @"test case failed, object should have nil");
}

#pragma mark - 
#pragma mark setDivertFlag

- (void) testSetDivertFlag {
    
    NSString *flagVaue = @"CallDivertFlag";
    [_callDiversionDetailModel setDivertFlag:flagVaue];
    
    NSString *returnedcallDivertFlag = [_callDiversionDetailModel valueForKey:@"_callDivertFlag"];
    
    XCTAssertTrue([flagVaue isEqualToString:returnedcallDivertFlag], @"Test case failed, both objects should be equal");
}

@end
