//
//  BTFaultAppointmentDetailTests.m
//  BTBusinessApp
//
//  Created by Rohini Kumar on 29/08/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BTFaultAppointmentDetail.h"
#import "AppManager.h"

@interface BTFaultAppointmentDetailTests : XCTestCase
@property (nonatomic) BTFaultAppointmentDetail *appointmentDetailModel;
@end

@implementation BTFaultAppointmentDetailTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _appointmentDetailModel = [[BTFaultAppointmentDetail alloc] init];
    XCTAssertNotNil(_appointmentDetailModel);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    _appointmentDetailModel = nil;
}

#pragma mark -
#pragma mark initWithFaultAppointmentsArray

- (void) testInitWithFaultAppointmentsArray {
    
    NSArray *faultAppointmentsArray = [NSArray array];
    
    NSString *bookedStartDateString = @"1987-09-10T20:20:30Z";
    NSDate *bookedStartDate = [AppManager NSDateWithUTCFormatFromNSString:bookedStartDateString];
    NSString *bookedEndDateString = @"1986-09-10T20:20:30Z";
    NSDate *bookedEndDate = [AppManager NSDateWithUTCFormatFromNSString:bookedEndDateString];
    NSString *presentDateTimeString = @"1983-09-10T20:20:30Z";
    NSDate *presentDateTime = [AppManager NSDateWithUTCFormatFromNSString:presentDateTimeString];
    NSArray *accessTimeOptionArray = [NSArray arrayWithObject:@"sample"];
    BOOL isHourAccessPresent = YES;
    NSString *earlierAccessTime = @"Smpale earlierAccessTime";
    NSString *latestAccessTime = @"Smpale latestAccessTime";
    
    NSArray *objects = [NSArray arrayWithObjects:bookedStartDateString, bookedEndDateString, presentDateTimeString, accessTimeOptionArray, [NSNumber numberWithBool:isHourAccessPresent], earlierAccessTime, latestAccessTime, nil];
    NSArray *keys = [NSArray arrayWithObjects:@"apptBookedStartDate",@"apptBookedEndDate",@"presentDateTime",@"accessTimesDDLOptns",@"isHourAccessPresent",@"earlierAccessTime",@"latestAccessTime", nil];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    id returnedObject = [_appointmentDetailModel initWithFaultAppointmentsArray:faultAppointmentsArray andAPIResponseDict:dict];
    XCTAssertNotNil(returnedObject, @"object should not be nil");
    
    NSDate *returnedbookedStartDate = [_appointmentDetailModel valueForKey:@"_bookedStartDate"];
    NSDate *returnedbookedEndDate = [_appointmentDetailModel valueForKey:@"_bookedEndDate"];
    NSDate *returnedpresentDateTime = [_appointmentDetailModel valueForKey:@"_presentDateTime"];
    NSArray *returnedaccessTimeOptionArray = [_appointmentDetailModel valueForKey:@"_acessTimeOptionArray"];
    BOOL returnedamisHourAccessPresent = [[_appointmentDetailModel valueForKey:@"_isHourAccessPresent"] boolValue];
    NSString *returnedearlierAccessTime = [_appointmentDetailModel valueForKey:@"_earlierAccessTime"];
    NSString *returnedlatestAccessTime = [_appointmentDetailModel valueForKey:@"_latestAccessTime"];
    
    XCTAssertTrue(([bookedStartDate compare:returnedbookedStartDate] == NSOrderedSame), @"test case failed, both values should be same");
    XCTAssertTrue(([bookedEndDate compare:returnedbookedEndDate] == NSOrderedSame), @"test case failed, both values should be same");
    XCTAssertTrue(([presentDateTime compare:returnedpresentDateTime] == NSOrderedSame), @"test case failed, both values should be same");
    XCTAssertTrue(accessTimeOptionArray == returnedaccessTimeOptionArray, @"test case failed, both values should be same");
    XCTAssertTrue(isHourAccessPresent == returnedamisHourAccessPresent, @"test case failed, both values should be same");
    XCTAssertTrue([earlierAccessTime isEqualToString:returnedearlierAccessTime], @"test case failed, both values should be same");
    XCTAssertTrue([latestAccessTime isEqualToString:returnedlatestAccessTime], @"test case failed, both values should be same");
}


- (void) testInitWithResponseDictionaryFromFaultAppointmentsAPIResponseWithNilValues {
    
    id returnedObject = [_appointmentDetailModel initWithFaultAppointmentsArray:nil andAPIResponseDict:nil];
    XCTAssertNotNil(returnedObject, @"object should not be nil");
    
    NSDate *returnedbookedStartDate = [_appointmentDetailModel valueForKey:@"_bookedStartDate"];
    NSDate *returnedbookedEndDate = [_appointmentDetailModel valueForKey:@"_bookedEndDate"];
    NSDate *returnedpresentDateTime = [_appointmentDetailModel valueForKey:@"_presentDateTime"];
    NSArray *returnedaccessTimeOptionArray = [_appointmentDetailModel valueForKey:@"_acessTimeOptionArray"];
    BOOL returnedamisHourAccessPresent = [[_appointmentDetailModel valueForKey:@"_isHourAccessPresent"] boolValue];
    NSString *returnedearlierAccessTime = [_appointmentDetailModel valueForKey:@"_earlierAccessTime"];
    NSString *returnedlatestAccessTime = [_appointmentDetailModel valueForKey:@"_latestAccessTime"];
    
    XCTAssertNil(returnedbookedStartDate,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedbookedEndDate,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedpresentDateTime,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedaccessTimeOptionArray,@"Test case failed, Object should be nil");
    XCTAssertFalse(returnedamisHourAccessPresent,@"test case failed, value should be false");
    XCTAssertNil(returnedearlierAccessTime,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedlatestAccessTime,@"Test case failed, Object should be nil");
}

#pragma mark -
#pragma mark initWithFaultAppointmentDetail

- (void) testInitWithFaultAppointmentDetail {
    
    NSString *bookedStartDateString = @"1987-09-10T20:20:30Z";
    NSDate *bookedStartDate = [AppManager NSDateWithUTCFormatFromNSString:bookedStartDateString];
    NSString *bookedEndDateString = @"1986-09-10T20:20:30Z";
    NSDate *bookedEndDate = [AppManager NSDateWithUTCFormatFromNSString:bookedEndDateString];
    BOOL isHourAccessPresent = YES;
    NSString *earlierAccessTime = @"Smpale earlierAccessTime";
    NSString *latestAccessTime = @"Smpale latestAccessTime";
    
    NSArray *objects = [NSArray arrayWithObjects:bookedStartDateString, bookedEndDateString, [NSNumber numberWithBool:isHourAccessPresent], earlierAccessTime, latestAccessTime, nil];
    NSArray *keys = [NSArray arrayWithObjects:@"apptBookedStartDate",@"apptBookedEndDate",@"isHourAccessPresent",@"earlierAccessTime",@"latestAccessTime", nil];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    id returnedObject = [_appointmentDetailModel initWithFaultAppointmentDetail:dict];
    XCTAssertNotNil(returnedObject, @"object should not be nil");
    
    NSDate *returnedbookedStartDate = [_appointmentDetailModel valueForKey:@"_bookedStartDate"];
    NSDate *returnedbookedEndDate = [_appointmentDetailModel valueForKey:@"_bookedEndDate"];
    BOOL returnedamisHourAccessPresent = [[_appointmentDetailModel valueForKey:@"_isHourAccessPresent"] boolValue];
    NSString *returnedearlierAccessTime = [_appointmentDetailModel valueForKey:@"_earlierAccessTime"];
    NSString *returnedlatestAccessTime = [_appointmentDetailModel valueForKey:@"_latestAccessTime"];
    
    XCTAssertTrue(([bookedStartDate compare:returnedbookedStartDate] == NSOrderedSame), @"test case failed, both values should be same");
    XCTAssertTrue(([bookedEndDate compare:returnedbookedEndDate] == NSOrderedSame), @"test case failed, both values should be same");
    XCTAssertTrue(isHourAccessPresent == returnedamisHourAccessPresent, @"test case failed, both values should be same");
    XCTAssertTrue([earlierAccessTime isEqualToString:returnedearlierAccessTime], @"test case failed, both values should be same");
    XCTAssertTrue([latestAccessTime isEqualToString:returnedlatestAccessTime], @"test case failed, both values should be same");
}


- (void) testInitWithFaultAppointmentDetailWithNilValues {
    
    id returnedObject = [_appointmentDetailModel initWithFaultAppointmentDetail:nil];
    XCTAssertNotNil(returnedObject, @"object should not be nil");
    
    NSDate *returnedbookedStartDate = [_appointmentDetailModel valueForKey:@"_bookedStartDate"];
    NSDate *returnedbookedEndDate = [_appointmentDetailModel valueForKey:@"_bookedEndDate"];
    BOOL returnedamisHourAccessPresent = [[_appointmentDetailModel valueForKey:@"_isHourAccessPresent"] boolValue];
    NSString *returnedearlierAccessTime = [_appointmentDetailModel valueForKey:@"_earlierAccessTime"];
    NSString *returnedlatestAccessTime = [_appointmentDetailModel valueForKey:@"_latestAccessTime"];
    
    XCTAssertNil(returnedbookedStartDate,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedbookedEndDate,@"Test case failed, Object should be nil");
    XCTAssertFalse(returnedamisHourAccessPresent,@"test case failed, value should be false");
    XCTAssertNil(returnedearlierAccessTime,@"Test case failed, Object should be nil");
    XCTAssertNil(returnedlatestAccessTime,@"Test case failed, Object should be nil");
}

#pragma mark -
#pragma mark updateBookedStartDateWithNSDate

- (void) testUpdateBookedStartDateWithNSDate {
    NSDate *date = [NSDate date];
    [_appointmentDetailModel updateBookedStartDateWithNSDate:date];
    NSDate *returnedDate = [_appointmentDetailModel valueForKey:@"_bookedStartDate"];
    XCTAssertTrue(([date compare:returnedDate] == NSOrderedSame), @"Test case faile, both values should be same");
}

#pragma mark -
#pragma mark UpdateBookedEndDateWithNSDate

- (void) testUpdateBookedEndDateWithNSDate {
    NSDate *date = [NSDate date];
    [_appointmentDetailModel updateBookedEndDateWithNSDate:date];
    NSDate *returnedDate = [_appointmentDetailModel valueForKey:@"_bookedEndDate"];
    XCTAssertTrue(([date compare:returnedDate] == NSOrderedSame), @"Test case faile, both values should be same");
}

#pragma mark -
#pragma mark updateearlierAccessTimeWithString

- (void) testUpdateearlierAccessTimeWithString {
    NSString *earlierAccessTImeString = @"Sample access time";
    [_appointmentDetailModel updateearlierAccessTimeWithString:earlierAccessTImeString];
    NSString *returnedearlierAccessTImeString = [_appointmentDetailModel valueForKey:@"_earlierAccessTime"];
    XCTAssertTrue([earlierAccessTImeString isEqualToString:returnedearlierAccessTImeString], @"Test case faile, both values should be same");
}

#pragma mark -
#pragma mark updateLatestAccessTimeWithString

- (void) testUpdateLatestAccessTimeWithString {
    NSString *latestAccessTImeString = @"Sample access time";
    [_appointmentDetailModel updateLatestAccessTimeWithString:latestAccessTImeString];
    NSString *returnedlatestAccessTImeString = [_appointmentDetailModel valueForKey:@"_latestAccessTime"];
    XCTAssertTrue([latestAccessTImeString isEqualToString:returnedlatestAccessTImeString], @"Test case faile, both values should be same");
}

@end
