//
//  BTUserTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/7/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BTUser.h"

@interface BTUserTests : XCTestCase

@property (nonatomic) BTUser *user;

@end

@implementation BTUserTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _user = nil;
}

- (void)testInitWithUsernameAndResponseDictWithSingleCug {
    
    NSMutableDictionary *mockDict = [NSMutableDictionary dictionary];
    
    [mockDict setValue:@"Mr" forKey:@"Title"];
    [mockDict setValue:@"abc@xyz.com" forKey:@"AlternativeEmailAddress"];
    
    NSMutableDictionary *group = [NSMutableDictionary dictionary];
    [group setValue:@"name" forKey:@"Name"];
    [group setValue:@"key" forKey:@"Key"];
    [group setValue:[NSNumber numberWithInt:1] forKey:@"RoleId"];
    
    NSDictionary *groups = [NSDictionary dictionaryWithObjectsAndKeys:group, @"Group", nil];
    
    [mockDict setValue:groups forKey:@"Groups"];
    
    _user = [[BTUser alloc] initWithUsername:@"username" andResponseDictionaryFromQueryUserDetailsAPIResponse:mockDict];
    
    XCTAssertNotNil(_user, @"BTUser instance is not created inside method.");
    XCTAssertEqual(@"Mr", _user.titleInName);
    XCTAssertEqual(@"abc@xyz.com", _user.alternativeEmailAddress);
    XCTAssertTrue([_user.arrayOfCugs count] == 1, @"User should have single cug.");
}

- (void)testInitWithUsernameAndResponseDictWithMultipleCugs {
    
    NSMutableDictionary *mockDict = [NSMutableDictionary dictionary];
    
    [mockDict setValue:@"Mr" forKey:@"Title"];
    [mockDict setValue:@"abc@xyz.com" forKey:@"AlternativeEmailAddress"];
    
    NSMutableDictionary *group1 = [NSMutableDictionary dictionary];
    [group1 setValue:@"key" forKey:@"Key"];
    [group1 setValue:[NSNumber numberWithInt:1] forKey:@"RoleId"];
    
    NSMutableDictionary *group2 = [NSMutableDictionary dictionary];
    [group2 setValue:@"name" forKey:@"Name"];
    [group2 setValue:[NSNumber numberWithInt:2] forKey:@"RoleId"];
    
    NSArray *group = [NSArray arrayWithObjects:group1, group2, nil];
    
    NSDictionary *groups = [NSDictionary dictionaryWithObjectsAndKeys:group, @"Group", nil];
    
    [mockDict setValue:groups forKey:@"Groups"];
    
    _user = [[BTUser alloc] initWithUsername:@"username" andResponseDictionaryFromQueryUserDetailsAPIResponse:mockDict];
    
    XCTAssertNotNil(_user, @"BTUser instance is not created inside method.");
    XCTAssertEqual(@"Mr", _user.titleInName);
    XCTAssertEqual(@"abc@xyz.com", _user.alternativeEmailAddress);
    XCTAssertNotNil(_user.arrayOfCugs, @"User should have single cug.");
    XCTAssertTrue([_user.arrayOfCugs count] == 2, @"User should have single cug.");
}

- (void)testIndexOfCugWithCugName {
    
    NSMutableDictionary *mockDict = [NSMutableDictionary dictionary];
    
    [mockDict setValue:@"Mr" forKey:@"Title"];
    [mockDict setValue:@"abc@xyz.com" forKey:@"AlternativeEmailAddress"];
    
    NSMutableDictionary *group1 = [NSMutableDictionary dictionary];
    [group1 setValue:@"name1" forKey:@"Name"];
    [group1 setValue:@"key1" forKey:@"Key"];
    [group1 setValue:[NSNumber numberWithInt:1] forKey:@"RoleId"];
    
    NSMutableDictionary *group2 = [NSMutableDictionary dictionary];
    [group2 setValue:@"name2" forKey:@"Name"];
    [group2 setValue:@"key2" forKey:@"Key"];
    [group2 setValue:[NSNumber numberWithInt:2] forKey:@"RoleId"];
    
    NSArray *group = [NSArray arrayWithObjects:group1, group2, nil];
    
    NSDictionary *groups = [NSDictionary dictionaryWithObjectsAndKeys:group, @"Group", nil];
    
    [mockDict setValue:groups forKey:@"Groups"];
    
    _user = [[BTUser alloc] initWithUsername:@"username" andResponseDictionaryFromQueryUserDetailsAPIResponse:mockDict];
    
    NSUInteger cugIndex1 = [_user indexOfCugWithCugName:@"name1"];
    NSUInteger cugIndex2 = [_user indexOfCugWithCugName:@"name2"];
    XCTAssertTrue(cugIndex1 == 0, @"CugIndex value is not correct");
    XCTAssertTrue(cugIndex2 == 1, @"CugIndex value is not correct");
    
    XCTAssertFalse(cugIndex1 == cugIndex2, @"CugIndex value for different cugs must be different.");
}

@end
