//
//  BTCugTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/7/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BTCug.h"

@interface BTCugTests : XCTestCase

@property (nonatomic) BTCug *cug;

@end

@implementation BTCugTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _cug = nil;
}

- (void)testInitWithCugNameAndGroupKeyAndCugRoleAndIndexInAPIResponse {
    
    _cug = [[BTCug alloc] initWithCugName:@"cug" groupKey:@"group" cugRole:1 andIndexInAPIResponse:1];
    
    XCTAssertNotNil(_cug, @"BTCug instance is not created inside method.");
    
}

- (void)testInitWithCugNameAndGroupKeyAndCugRoleAndIndexInAPIResponseAndRefKey {
    
    _cug = [[BTCug alloc] initWithCugName:@"cug" groupKey:@"group" cugRole:1 indexInAPIResponse:1 andRefKey:@"refkey"];
    
    XCTAssertNotNil(_cug, @"BTCug instance is not created inside method.");
    
}

@end
