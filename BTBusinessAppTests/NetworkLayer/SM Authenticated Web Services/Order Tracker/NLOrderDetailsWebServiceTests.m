//
//  NLOrderDetailsWebServiceTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/10/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NLOrderDetailsWebService.h"

@interface NLOrderDetailsWebServiceTests : XCTestCase

@property (nonatomic) NLOrderDetailsWebService *getOrderDetailsWS;

@end

@implementation NLOrderDetailsWebServiceTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _getOrderDetailsWS = nil;
}

- (void)testInitWithOrderRefAndItemRef {
    
    NSString *mockOrderRef = @"BT012345";
    NSString *mockItemRef = @"BT012345-1";
    _getOrderDetailsWS = [[NLOrderDetailsWebService alloc] initWithOrderRef:mockOrderRef itemRef:mockItemRef];
    
    XCTAssertNotNil(_getOrderDetailsWS, @"NLOrderDetailsWebService instance is not created inside init method.");
    
}

- (void)testInitWithOrderRefAndItemRefAndPostCodeAndErrorCount {
    
    NSString *mockOrderRef = @"BT012345";
    NSString *mockItemRef = @"BT012345-1";
    _getOrderDetailsWS = [[NLOrderDetailsWebService alloc] initWithOrderRef:mockOrderRef itemRef:mockItemRef postCode:@"mockPostCode" errorCount:1];
    
    XCTAssertNotNil(_getOrderDetailsWS, @"NLGetOrderMilestonesWebService instance is not created inside init method.");
    
}

@end
