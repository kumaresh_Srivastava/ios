//
//  NLGetOrderMilestonesWebServiceTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/10/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NLGetOrderMilestonesWebService.h"

@interface NLGetOrderMilestonesWebServiceTests : XCTestCase

@property (nonatomic) NLGetOrderMilestonesWebService *getOrderMilestonesWS;

@end

@implementation NLGetOrderMilestonesWebServiceTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _getOrderMilestonesWS = nil;
}

- (void)testInitWithOrderRefAndItemRef {
    
    NSString *mockOrderRef = @"BT012345";
    NSString *mockItemRef = @"BT012345-1";
    _getOrderMilestonesWS = [[NLGetOrderMilestonesWebService alloc] initWithOrderRef:mockOrderRef itemRef:mockItemRef];
    
    XCTAssertNotNil(_getOrderMilestonesWS, @"NLGetOrderMilestonesWebService instance is not created inside init method.");
    XCTAssert([_getOrderMilestonesWS.orderRef isEqualToString:mockOrderRef], @"Order reference of class is not same as input order reference.");
    XCTAssert([_getOrderMilestonesWS.itemRef isEqualToString:mockItemRef], @"Order reference of class is not same as input order reference.");
}

- (void)testInitWithOrderRefAndItemRefAndPostCodeAndErrorCount {
    
    NSString *mockOrderRef = @"BT012345";
    NSString *mockItemRef = @"BT012345-1";
    _getOrderMilestonesWS = [[NLGetOrderMilestonesWebService alloc] initWithOrderRef:mockOrderRef itemRef:mockItemRef postCode:@"mockPostCode" errorCount:1];
    
    XCTAssertNotNil(_getOrderMilestonesWS, @"NLGetOrderMilestonesWebService instance is not created inside init method.");
    XCTAssert([_getOrderMilestonesWS.orderRef isEqualToString:mockOrderRef], @"Order reference of class is not same as input order reference.");
    XCTAssert([_getOrderMilestonesWS.itemRef isEqualToString:mockItemRef], @"Order reference of class is not same as input order reference.");
}

@end
