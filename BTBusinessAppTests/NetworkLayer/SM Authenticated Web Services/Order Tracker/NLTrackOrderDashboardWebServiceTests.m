//
//  NLTrackOrderDashboardWebServiceTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/9/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NLTrackOrderDashBoardWebService.h"

@interface NLTrackOrderDashboardWebServiceTests : XCTestCase

@property NLTrackOrderDashBoardWebService *trackOrderDashboardWS;

@end

@implementation NLTrackOrderDashboardWebServiceTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _trackOrderDashboardWS = nil;
}

- (void)testInitWithPageSizeAndPageIndexAndTabID {
    
    _trackOrderDashboardWS = [[NLTrackOrderDashBoardWebService alloc] initWithPageSize:3 pageIndex:1 andTabID:1];
    
    XCTAssertNotNil(_trackOrderDashboardWS, @"NLTrackOrderDashBoardWebService instance is not created inside init method.");
    XCTAssert([[_trackOrderDashboardWS valueForKey:@"_tabID"] intValue] == 1, @"Tab ID in webservice is not correct.");
    XCTAssert([[_trackOrderDashboardWS valueForKey:@"_pageIndex"] intValue] == 1, @"Page Index in webservice is not correct.");
}

@end
