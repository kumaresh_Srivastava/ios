//
//  NLGetBundleOrderSummaryWebServiceTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/10/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NLGetBundleOrderSummaryWebService.h"

@interface NLGetBundleOrderSummaryWebServiceTests : XCTestCase

@property (nonatomic) NLGetBundleOrderSummaryWebService *getBundleOrderSummaryWS;

@end

@implementation NLGetBundleOrderSummaryWebServiceTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _getBundleOrderSummaryWS = nil;
}

- (void)testInitWithOrderRefAndItemRef {
    
    _getBundleOrderSummaryWS = [[NLGetBundleOrderSummaryWebService alloc] initWithOrderRef:@"BT012345" andItemRef:@"BT012345-1"];
    
    XCTAssertNotNil(_getBundleOrderSummaryWS, @"NLGetBundleOrderSummaryWebService instance is not created inside init method.");
}

@end
