//
//  NLGetGroupOrderSummaryWebServiceTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/10/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NLGetGroupOrderSummaryWebService.h"

@interface NLGetGroupOrderSummaryWebServiceTests : XCTestCase

@property (nonatomic) NLGetGroupOrderSummaryWebService *getGroupOrderSummaryWS;

@end

@implementation NLGetGroupOrderSummaryWebServiceTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _getGroupOrderSummaryWS = nil;
}

- (void)testInitWithGroupOrderRef {
    
    NSString *mockGroupOrder = @"GBT01234";
    _getGroupOrderSummaryWS = [[NLGetGroupOrderSummaryWebService alloc] initWithGroupOrderRef:mockGroupOrder];
    
    XCTAssertNotNil(_getGroupOrderSummaryWS, @"NLGetGroupOrderSummaryWebService instance is not created inside init method.");
    XCTAssert([_getGroupOrderSummaryWS.groupOrderRef isEqualToString:mockGroupOrder], @"Group order reference of class is not same as input order reference.");
}

@end
