//
//  NLGetOrderSummaryWebServiceTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/10/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NLGetOrderSummaryWebService.h"

@interface NLGetOrderSummaryWebServiceTests : XCTestCase

@property (nonatomic) NLGetOrderSummaryWebService *getOrderSummaryWS;

@end

@implementation NLGetOrderSummaryWebServiceTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _getOrderSummaryWS = nil;
}

- (void)testInitWithOrderRef {
    
    NSString *mockOrderRef = @"BT012345";
    _getOrderSummaryWS = [[NLGetOrderSummaryWebService alloc] initWithOrderRef:mockOrderRef];
    
    XCTAssertNotNil(_getOrderSummaryWS, @"NLGetOrderSummaryWebService instance is not created inside init method.");
    XCTAssert([_getOrderSummaryWS.orderRef isEqualToString:mockOrderRef], @"Order reference of class is not same as input order reference.");
}


@end
