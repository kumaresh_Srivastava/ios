//
//  NLQueryUserDetailsWebServiceTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/3/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NLQueryUserDetailsWebService.h"

@interface NLQueryUserDetailsWebServiceTests : XCTestCase

@property (nonatomic) NLQueryUserDetailsWebService *queryUserDetailsWebService;

@end

@implementation NLQueryUserDetailsWebServiceTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _queryUserDetailsWebService = nil;
}

- (void)testInitWithUsernameValidInput {
    
    NSString *username = @"xyz@bt.com";
    
    _queryUserDetailsWebService = [[NLQueryUserDetailsWebService alloc] initWithUsername:username];
    
    XCTAssertNotNil(_queryUserDetailsWebService);
    XCTAssertNotNil(_queryUserDetailsWebService.username);
    XCTAssertEqual(username, _queryUserDetailsWebService.username);
}

- (void)testInitWithUsernameInvalidInput {
    
    NSString *username = nil;
    
    _queryUserDetailsWebService = [[NLQueryUserDetailsWebService alloc] initWithUsername:username];
    
    XCTAssertNil(_queryUserDetailsWebService);
    XCTAssertNil(_queryUserDetailsWebService.username);
    
}

- (void)testInitWithUsernameEmptyValueInput {
    
    NSString *username = [NSString string];
    
    _queryUserDetailsWebService = [[NLQueryUserDetailsWebService alloc] initWithUsername:username];
    
    XCTAssertNil(_queryUserDetailsWebService);
    XCTAssertNil(_queryUserDetailsWebService.username);
    
}


- (void)testInitWithUsernameAndTokenValidInput {
    
    NSString *username = @"xyz@bt.com";
    NSString *accessToken = @"validtoken";
    
    _queryUserDetailsWebService = [[NLQueryUserDetailsWebService alloc] initWithUsername:username andAuthenticationToken:accessToken];
    
    XCTAssertNotNil(_queryUserDetailsWebService);
    XCTAssertNotNil(_queryUserDetailsWebService.username);
    XCTAssertEqual(username, _queryUserDetailsWebService.username);
    XCTAssertEqual(accessToken, [_queryUserDetailsWebService valueForKey:@"_accessToken"]);
    
}

- (void)testInitWithUsernameAndAuthenticationTokenInvalidInput {
    
    NSString *username = nil;
    NSString *accessToken = nil;
    
    _queryUserDetailsWebService = [[NLQueryUserDetailsWebService alloc] initWithUsername:username andAuthenticationToken:accessToken];
    
    XCTAssertNil(_queryUserDetailsWebService);
    XCTAssertNil(_queryUserDetailsWebService.username);
    XCTAssertNil([_queryUserDetailsWebService valueForKey:@"_accessToken"]);
    
}

- (void)testInitWithUsernameAndAuthenticationTokenEmptyValueInput {
    
    NSString *username = [NSString string];
    NSString *accessToken = [NSString string];
    
    _queryUserDetailsWebService = [[NLQueryUserDetailsWebService alloc] initWithUsername:username andAuthenticationToken:accessToken];
    
    XCTAssertNil(_queryUserDetailsWebService);
    XCTAssertNil(_queryUserDetailsWebService.username);
    XCTAssertNil([_queryUserDetailsWebService valueForKey:@"_accessToken"]);
    
}

@end
