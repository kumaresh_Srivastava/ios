//
//  AppManagerTests.m
//  BTBusinessApp
//
//  Created by Lakhpat Meena on 8/24/17.
//  Copyright © 2017 Accolite. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock.h>
#import "AppManager.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import "AppDelegateViewModel.h"
#import "BTOrderSiteContactModel.h"
#import "OmnitureManager.h"

@interface AppManagerTests : XCTestCase

@end

@implementation AppManagerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark - Singleton method test

- (void)testSharedAppManager {
    
    XCTAssertNotNil([AppManager sharedAppManager], @"AppManager instance is not created.");
}

#pragma mark - Base URL setting method tests

- (void)testBaseURLStringFromBaseURLTypeVordel {
    
    NSString *expectedBaseURL = [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeVordel];
    
    if (kBTServerType == BTServerTypeModelA)
    {
        XCTAssertTrue([expectedBaseURL isEqualToString:kNLBaseURLVordelURLModelA]);
    }
    else
    {
        XCTAssertTrue([expectedBaseURL isEqualToString:kNLBaseURLVordelURLProd]);
    }
    
}

- (void)testBaseURLStringFromBaseURLTypeNonVordel {
    
    NSString *expectedBaseURL = [AppManager baseURLStringFromBaseURLType:NLBaseURLTypeNonVordel];
    
    if (kBTServerType == BTServerTypeModelA)
    {
        XCTAssertTrue([expectedBaseURL isEqualToString:kNLBaseURLNONVordelURLModelA]);
    }
    else
    {
        XCTAssertTrue([expectedBaseURL isEqualToString:kNLBaseURLNONVordelURLProd]);
    }
    
}

#pragma mark - Internet check method tests

- (void)testIsInternetConnectionAvailableWithInternetConnection {
    
    id mockReachabilityObj = OCMClassMock([Reachability class]);
    OCMStub([mockReachabilityObj reachabilityForInternetConnection]).andReturn(mockReachabilityObj);
    
    OCMStub([mockReachabilityObj currentReachabilityStatus]).andReturn(ReachableViaWiFi);
    
    BOOL reachable = [AppManager isInternetConnectionAvailable];
    
    XCTAssertTrue(reachable, @"Bool variable should have true value.");
    
    [mockReachabilityObj stopMocking];

}

- (void)testIsInternetConnectionAvailableWithNoConnection {
    
    id mockReachabilityObj = OCMClassMock([Reachability class]);
    OCMStub([mockReachabilityObj reachabilityForInternetConnection]).andReturn(mockReachabilityObj);
    
    OCMStub([mockReachabilityObj currentReachabilityStatus]).andReturn(NotReachable);
    
    BOOL notReachable = [AppManager isInternetConnectionAvailable];
    
    XCTAssertFalse(notReachable, @"Bool variable should have false value.");
    
    [mockReachabilityObj stopMocking];
    
}

#pragma mark - RSA encryption tests

- (void)testGetRSAEncryptedString {
    
    NSString *returnedString = [AppManager getRSAEncryptedString:@"sample"];
    
    XCTAssertNotNil(returnedString, @"Enrypted string is nil.");
    XCTAssertTrue(returnedString.length > 0);
    
}

#pragma mark - Helper method tests

- (void)testStringByTrimmingWhitespaceInString {
    
    NSString *givenString = @"   sample  ";
    NSString *expectedString = @"sample";
    
    NSString *returnedString = [AppManager stringByTrimmingWhitespaceInString:givenString];
    
    XCTAssertTrue([returnedString isEqualToString:expectedString]);
}

- (void)testDocumentDirectoryPath {
    
    XCTAssertNotNil([AppManager documentDirectoryPath], @"Path is nil.");
}

- (void)testFetchSegmentedControlFontDict {
    
    NSDictionary *fontDict = [[AppManager sharedAppManager] fetchSegmentedControlFontDict];
    
    XCTAssertNotNil(fontDict, @"Returned font dictionary is nil.");
    XCTAssertNotNil([fontDict valueForKey:NSFontAttributeName]);
    
}

#pragma mark - Date format related method tests

- (void)testNSDateWithMonthNameWithSpaceFormatFromNSString {
    
    NSDate *returnedDate = [AppManager NSDateWithMonthNameWithSpaceFormatFromNSString:@"28 Aug 2017"];
    
    XCTAssertNotNil(returnedDate, @"Returned date is nil.");
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear) fromDate:returnedDate];
    
    XCTAssertTrue([components day]==28);
    XCTAssertTrue([components month]==8);
    XCTAssertTrue([components year]==2017);
}

- (void)testNSStringFromNSDateWithMonthNameWithSpaceeFormat {
    
    NSString *dateString = @"28 Aug 2017";
    NSDate *date = [AppManager NSDateWithMonthNameWithSpaceFormatFromNSString:dateString];
    
    NSString *returnedDateString = [AppManager NSStringFromNSDateWithMonthNameWithSpaceeFormat:date];
    
    XCTAssertNotNil(returnedDateString, @"Returned date is nil.");
    XCTAssertTrue([returnedDateString isEqualToString:dateString]);
}

- (void)testNSStringFromNSDateWithSlashFormat {
    
    NSString *expectedDateString = @"28/08/2017";
    NSString *dateString = @"28 Aug 2017";
    NSDate *date = [AppManager NSDateWithMonthNameWithSpaceFormatFromNSString:dateString];
    
    NSString *returnedDateString = [AppManager NSStringFromNSDateWithSlashFormat:date];
    
    XCTAssertNotNil(returnedDateString, @"Returned date is nil.");
    XCTAssertTrue([returnedDateString isEqualToString:expectedDateString]);
}

- (void)testNSDateWithMonthNameFromNSString {
    
    NSString *expectedDateString = @"28-Aug-2017";
    NSString *dateString = @"2017-08-28";
    
    NSString *returnedDateString = [AppManager NSDateWithMonthNameFromNSString:dateString];
    
    XCTAssertNotNil(returnedDateString, @"Returned date is nil.");
    XCTAssertTrue([returnedDateString isEqualToString:expectedDateString]);
}

- (void)testNSDateWithUTCFormatFromNSString {
    
    NSString *expectedDateString = @"2017-08-28T12:12:12Z";
    
    NSDate *returnedDate = [AppManager NSDateWithUTCFormatFromNSString:expectedDateString];
    
    XCTAssertNotNil(returnedDate, @"Returned date is nil.");
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:returnedDate];
    
    XCTAssertTrue([components day]==28);
    XCTAssertTrue([components month]==8);
    XCTAssertTrue([components year]==2017);
    XCTAssertTrue([components hour]==12);
    XCTAssertTrue([components minute]==12);
    XCTAssertTrue([components second]==12);
}

- (void)testNSStringWithOmnitureFormatFromNSDate {
    
    NSString *expectedDateString = @"2017/08/28 12:12:12";
    
    NSDate *date = [AppManager NSDateWithUTCFormatFromNSString:@"2017-08-28T12:12:12Z"];
    
    NSString *returnedDateString = [AppManager NSStringWithOmnitureFormatFromNSDate:date];
    
    XCTAssertNotNil(returnedDateString, @"Returned date is nil.");
    XCTAssertTrue([returnedDateString isEqualToString:expectedDateString]);
}

- (void)testNSStringFromNSDateWithMonthNameWithHiphenFormat {
    
    NSString *expectedDateString = @"28-Aug-2017";
    
    NSDate *date = [AppManager NSDateWithUTCFormatFromNSString:@"2017-08-28T12:12:12Z"];
    
    NSString *returnedDateString = [AppManager NSStringFromNSDateWithMonthNameWithHiphenFormat:date];
    
    XCTAssertNotNil(returnedDateString, @"Returned date is nil.");
    XCTAssertTrue([returnedDateString isEqualToString:expectedDateString]);
}

- (void)testNSStringFromNSDateWithMonthHourDateFormat {
    
    NSString *expectedDateString = @"28 Aug, 12:12";
    
    NSDate *date = [AppManager NSDateWithUTCFormatFromNSString:@"2017-08-28T12:12:12Z"];
    
    NSString *returnedDateString = [AppManager NSStringFromNSDateWithMonthHourDateFormat:date];
    
    XCTAssertNotNil(returnedDateString, @"Returned date is nil.");
    XCTAssertTrue([returnedDateString isEqualToString:expectedDateString]);
}

- (void)testNSStringFromNSDateWithReverseDateFormat {
    
    NSString *expectedDateString = @"2017-08-28";
    
    NSDate *date = [AppManager NSDateWithUTCFormatFromNSString:@"2017-08-28T12:12:12Z"];
    
    NSString *returnedDateString = [AppManager NSStringFromNSDateWithReverseDateFormat:date];
    
    XCTAssertNotNil(returnedDateString, @"Returned date is nil.");
    XCTAssertTrue([returnedDateString isEqualToString:expectedDateString]);
}

- (void)testGetDayDateMonthFormattedStringFromDate {
    
    NSString *expectedDateString = @"Mon 28 Aug";
    
    NSDate *date = [AppManager NSDateWithUTCFormatFromNSString:@"2017-08-28T12:12:12Z"];
    
    NSString *returnedDateString = [AppManager getDayDateMonthFormattedStringFromDate:date];
    
    XCTAssertNotNil(returnedDateString, @"Returned date is nil.");
    XCTAssertTrue([returnedDateString isEqualToString:expectedDateString]);
}

- (void)testGetTimeFromDate {
    
    NSString *expectedDateString = @"12:12";
    
    NSDate *date = [AppManager NSDateWithUTCFormatFromNSString:@"2017-08-28T12:12:12Z"];
    
    NSString *returnedDateString = [AppManager getTimeFromDate:date];
    
    XCTAssertNotNil(returnedDateString, @"Returned date is nil.");
    XCTAssertTrue([returnedDateString isEqualToString:expectedDateString]);
}

- (void)testGetDayMonthFromDate {
    
    NSString *expectedDateString = @"28 Aug";
    
    NSDate *date = [AppManager NSDateWithUTCFormatFromNSString:@"2017-08-28T12:12:12Z"];
    
    NSString *returnedDateString = [AppManager getDayMonthFromDate:date];
    
    XCTAssertNotNil(returnedDateString, @"Returned date is nil.");
    XCTAssertTrue([returnedDateString isEqualToString:expectedDateString]);
}

- (void)testGetHourFromDate {
    
    NSDate *date = [AppManager NSDateWithUTCFormatFromNSString:@"2017-08-28T12:12:12Z"];
    
    XCTAssertTrue([AppManager getHourFromDate:date]==12);
}

- (void)testGetMinuteFromDate {
    
    NSDate *date = [AppManager NSDateWithUTCFormatFromNSString:@"2017-08-28T12:12:12Z"];
    
    XCTAssertTrue([AppManager getMinuteFromDate:date]==12);
}

- (void)testIsValidDateWithValidDate {
    
    NSDate *date = [NSDate date];
    
    XCTAssertTrue([AppManager isValidDate:date]);
}

- (void)testIsValidDateWithNilDate {
    
    XCTAssertFalse([AppManager isValidDate:nil]);
}

- (void)testIsValidDateWithInvalidDate {
    
    NSDate *date = [AppManager NSDateWithUTCFormatFromNSString:@"0001-01-01T00:00:00Z"];
    
    XCTAssertFalse([AppManager isValidDate:date]);
}

- (void)testIsFirstDateEqualToSecondDateWithValidDifferentDates {
    
    NSDate *date1 = [NSDate date];
    NSDate *date2 = [NSDate dateWithTimeInterval:100 sinceDate:date1];
    
    XCTAssertFalse([AppManager isDate:date1 isEqualTo:date2]);
}

- (void)testIsFirstDateEqualToSecondDateWithValidEqualDates {
    
    NSDate *date1 = [AppManager NSDateWithMonthNameWithSpaceFormatFromNSString:@"28 Aug 2017"];
    NSDate *date2 = [AppManager NSDateWithMonthNameWithSpaceFormatFromNSString:@"28 Aug 2017"];
    
    XCTAssertTrue([AppManager isDate:date1 isEqualTo:date2]);
}

- (void)testIsFirstDateEqualToSecondDateWithInvalidFirstDate {
    
    NSDate *date1 = nil;
    NSDate *date2 = [AppManager NSDateWithMonthNameWithSpaceFormatFromNSString:@"28 Aug 2017"];
    
    XCTAssertFalse([AppManager isDate:date1 isEqualTo:date2]);
}

- (void)testIsFirstDateEqualToSecondDateWithInvalidSecondDate {
    
    NSDate *date1 = [AppManager NSDateWithMonthNameWithSpaceFormatFromNSString:@"28 Aug 2017"];
    NSDate *date2 = nil;
    
    XCTAssertFalse([AppManager isDate:date1 isEqualTo:date2]);
}

#pragma mark - Get user titles method tests

- (void)testGetBTUserTitlesFromPlist {
    
    NSArray *arrayOfTitles = [AppManager getBTUserTitles];
    
    XCTAssertNotNil(arrayOfTitles, @"Titles array is nil");
    XCTAssertTrue(arrayOfTitles.count > 0);
}

- (void)testGetBTUserTitlesFromServer {
    
    NSArray *mockArrayOfTitles = [[NSArray alloc] initWithObjects:@"Mr", @"Mrs", nil];
    [[AppDelegate sharedInstance].viewModel setValue:mockArrayOfTitles forKey:@"arrayOfTitles"];
    
    NSArray *arrayOfTitles = [AppManager getBTUserTitles];
    
    XCTAssertNotNil(arrayOfTitles, @"Titles array is nil");
    XCTAssertTrue(arrayOfTitles.count > 0);
}

#pragma mark - Get security questions method tests

- (void)testGetBTUserSecurityQuestionsFromPlist {
    
    NSArray *arrayOfSecurityQuestions = [AppManager getBTUserSecurityQuestions];
    
    XCTAssertNotNil(arrayOfSecurityQuestions, @"Security questions array is nil");
    XCTAssertTrue(arrayOfSecurityQuestions.count > 0);
}

- (void)testGetBTUserSecurityQuestionsFromServer {
    
    NSArray *mockArrayOfSecurityQuestions = [[NSArray alloc] initWithObjects:@"What's your mother's maiden name?", @"What's your child's name?", nil];
    [[AppDelegate sharedInstance].viewModel setValue:mockArrayOfSecurityQuestions forKey:@"arrayOfSecurityQuestions"];
    
    NSArray *arrayOfSecurityQuestions = [AppManager getBTUserSecurityQuestions];
    
    XCTAssertNotNil(arrayOfSecurityQuestions, @"Security questions array is nil");
    XCTAssertTrue(arrayOfSecurityQuestions.count > 0);
}

- (void)testGetContactNumberFromSiteContactWithValidSiteContactModelAndPreferredContactMobile {
    
    NSMutableDictionary *mockContactDict = [NSMutableDictionary dictionary];
    
    NSString *extension = @"ext";
    NSString *mobile = @"mobile";
    NSString *phone = @"phone";
    NSString *homePhone = @"homephone";
    NSString *workPhone = @"workphone";
    NSString *preferredContact = @"mobile";
    
    [mockContactDict setValue:extension forKey:@"Extension"];
    [mockContactDict setValue:mobile forKey:@"Mobile"];
    [mockContactDict setValue:phone forKey:@"Phone"];
    [mockContactDict setValue:homePhone forKey:@"HomePhone"];
    [mockContactDict setValue:workPhone forKey:@"WorkPhone"];
    [mockContactDict setValue:preferredContact forKey:@"PreferredContact"];
    
    BTOrderSiteContactModel *siteContactModel = [[BTOrderSiteContactModel alloc] initSiteContactWithResponseDict:mockContactDict];
    
    NSString *returnedContactNumber = [AppManager getContactNumberFromSiteContact:siteContactModel];
    
    XCTAssertNotNil(returnedContactNumber, @"Contact number is nil.");
    XCTAssertTrue([returnedContactNumber isEqualToString:mobile]);
}

- (void)testGetContactNumberFromSiteContactWithValidSiteContactModelAndPreferredContactPhone {
    
    NSMutableDictionary *mockContactDict = [NSMutableDictionary dictionary];
    
    NSString *extension = @"ext";
    NSString *mobile = @"mobile";
    NSString *phone = @"phone";
    NSString *homePhone = @"homephone";
    NSString *workPhone = @"workphone";
    NSString *preferredContact = @"phone";
    
    [mockContactDict setValue:extension forKey:@"Extension"];
    [mockContactDict setValue:mobile forKey:@"Mobile"];
    [mockContactDict setValue:phone forKey:@"Phone"];
    [mockContactDict setValue:homePhone forKey:@"HomePhone"];
    [mockContactDict setValue:workPhone forKey:@"WorkPhone"];
    [mockContactDict setValue:preferredContact forKey:@"PreferredContact"];
    
    BTOrderSiteContactModel *siteContactModel = [[BTOrderSiteContactModel alloc] initSiteContactWithResponseDict:mockContactDict];
    
    NSString *returnedContactNumber = [AppManager getContactNumberFromSiteContact:siteContactModel];
    
    XCTAssertNotNil(returnedContactNumber, @"Contact number is nil.");
    XCTAssertTrue([returnedContactNumber isEqualToString:phone]);
}

- (void)testGetContactNumberFromSiteContactWithValidSiteContactModelAndPreferredContactWork {
    
    NSMutableDictionary *mockContactDict = [NSMutableDictionary dictionary];
    
    NSString *extension = @"ext";
    NSString *mobile = @"mobile";
    NSString *phone = @"phone";
    NSString *homePhone = @"homephone";
    NSString *workPhone = @"workphone";
    NSString *preferredContact = @"work";
    
    [mockContactDict setValue:extension forKey:@"Extension"];
    [mockContactDict setValue:mobile forKey:@"Mobile"];
    [mockContactDict setValue:phone forKey:@"Phone"];
    [mockContactDict setValue:homePhone forKey:@"HomePhone"];
    [mockContactDict setValue:workPhone forKey:@"WorkPhone"];
    [mockContactDict setValue:preferredContact forKey:@"PreferredContact"];
    
    BTOrderSiteContactModel *siteContactModel = [[BTOrderSiteContactModel alloc] initSiteContactWithResponseDict:mockContactDict];
    
    NSString *returnedContactNumber = [AppManager getContactNumberFromSiteContact:siteContactModel];
    
    NSString *expectedContactNumber = [NSString stringWithFormat:@"%@-%@",workPhone, extension];
    
    XCTAssertNotNil(returnedContactNumber, @"Contact number is nil.");
    XCTAssertTrue([returnedContactNumber isEqualToString:expectedContactNumber]);
    
}

- (void)testGetContactNumberFromSiteContactWithValidSiteContactModelAndPreferredContactHome {
    
    NSMutableDictionary *mockContactDict = [NSMutableDictionary dictionary];
    
    NSString *extension = @"ext";
    NSString *mobile = @"mobile";
    NSString *phone = @"phone";
    NSString *homePhone = @"homephone";
    NSString *workPhone = @"workphone";
    NSString *preferredContact = @"home";
    
    [mockContactDict setValue:extension forKey:@"Extension"];
    [mockContactDict setValue:mobile forKey:@"Mobile"];
    [mockContactDict setValue:phone forKey:@"Phone"];
    [mockContactDict setValue:homePhone forKey:@"HomePhone"];
    [mockContactDict setValue:workPhone forKey:@"WorkPhone"];
    [mockContactDict setValue:preferredContact forKey:@"PreferredContact"];
    
    BTOrderSiteContactModel *siteContactModel = [[BTOrderSiteContactModel alloc] initSiteContactWithResponseDict:mockContactDict];
    
    NSString *returnedContactNumber = [AppManager getContactNumberFromSiteContact:siteContactModel];
    
    XCTAssertNotNil(returnedContactNumber, @"Contact number is nil.");
    XCTAssertTrue([returnedContactNumber isEqualToString:homePhone]);
}

- (void)testGetContactNumberFromSiteContactWithValidSiteContactModelWithPhoneAndNoPreferredContact {
    
    NSMutableDictionary *mockContactDict = [NSMutableDictionary dictionary];
    
    NSString *extension = @"ext";
    NSString *mobile = @"mobile";
    NSString *phone = @"phone";
    NSString *homePhone = @"homephone";
    NSString *workPhone = @"workphone";
    
    [mockContactDict setValue:extension forKey:@"Extension"];
    [mockContactDict setValue:mobile forKey:@"Mobile"];
    [mockContactDict setValue:phone forKey:@"Phone"];
    [mockContactDict setValue:homePhone forKey:@"HomePhone"];
    [mockContactDict setValue:workPhone forKey:@"WorkPhone"];
    
    BTOrderSiteContactModel *siteContactModel = [[BTOrderSiteContactModel alloc] initSiteContactWithResponseDict:mockContactDict];
    
    NSString *returnedContactNumber = [AppManager getContactNumberFromSiteContact:siteContactModel];
    
    XCTAssertNotNil(returnedContactNumber, @"Contact number is nil.");
    XCTAssertTrue([returnedContactNumber isEqualToString:phone]);
}

- (void)testGetContactNumberFromSiteContactWithValidSiteContactModelWithMobileAndNoPreferredContact {
    
    NSMutableDictionary *mockContactDict = [NSMutableDictionary dictionary];
    
    NSString *extension = @"ext";
    NSString *mobile = @"mobile";
    NSString *homePhone = @"homephone";
    NSString *workPhone = @"workphone";
    
    [mockContactDict setValue:extension forKey:@"Extension"];
    [mockContactDict setValue:mobile forKey:@"Mobile"];
    [mockContactDict setValue:homePhone forKey:@"HomePhone"];
    [mockContactDict setValue:workPhone forKey:@"WorkPhone"];
    
    BTOrderSiteContactModel *siteContactModel = [[BTOrderSiteContactModel alloc] initSiteContactWithResponseDict:mockContactDict];
    
    NSString *returnedContactNumber = [AppManager getContactNumberFromSiteContact:siteContactModel];
    
    XCTAssertNotNil(returnedContactNumber, @"Contact number is nil.");
    XCTAssertTrue([returnedContactNumber isEqualToString:mobile]);
}

- (void)testGetContactNumberFromSiteContactWithValidSiteContactModelWithWorkAndNoPreferredContact {
    
    NSMutableDictionary *mockContactDict = [NSMutableDictionary dictionary];
    
    NSString *extension = @"ext";
    NSString *homePhone = @"homephone";
    NSString *workPhone = @"workphone";
    
    [mockContactDict setValue:extension forKey:@"Extension"];
    [mockContactDict setValue:homePhone forKey:@"HomePhone"];
    [mockContactDict setValue:workPhone forKey:@"WorkPhone"];
    
    BTOrderSiteContactModel *siteContactModel = [[BTOrderSiteContactModel alloc] initSiteContactWithResponseDict:mockContactDict];
    
    NSString *returnedContactNumber = [AppManager getContactNumberFromSiteContact:siteContactModel];
    
    NSString *expectedContactNumber = [NSString stringWithFormat:@"%@-%@",workPhone, extension];
    
    XCTAssertNotNil(returnedContactNumber, @"Contact number is nil.");
    XCTAssertTrue([returnedContactNumber isEqualToString:expectedContactNumber]);
}

- (void)testGetContactNumberFromSiteContactWithValidSiteContactModelWithHomeAndNoPreferredContact {
    
    NSMutableDictionary *mockContactDict = [NSMutableDictionary dictionary];
    
    NSString *extension = @"ext";
    NSString *homePhone = @"homephone";
    
    [mockContactDict setValue:extension forKey:@"Extension"];
    [mockContactDict setValue:homePhone forKey:@"HomePhone"];
    
    BTOrderSiteContactModel *siteContactModel = [[BTOrderSiteContactModel alloc] initSiteContactWithResponseDict:mockContactDict];
    
    NSString *returnedContactNumber = [AppManager getContactNumberFromSiteContact:siteContactModel];
    
    XCTAssertNotNil(returnedContactNumber, @"Contact number is nil.");
    XCTAssertTrue([returnedContactNumber isEqualToString:homePhone]);
}

- (void)testGetContactNumberFromSiteContactWithInvalidSiteContactModel {
    
    NSString *returnedContactNumber = [AppManager getContactNumberFromSiteContact:nil];
    
    XCTAssertNil(returnedContactNumber, @"Contact number should be nil.");
    
}

- (void)testTrackOmnitureKeyTask {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [AppManager trackOmnitureKeyTask:@"keytask" forPageName:@"pagename"];
    
    OCMVerify([mockOmnitureManagerObj trackPage:[OCMArg any] withContextInfo:[OCMArg any]]);
    
    [mockOmnitureManagerObj stopMocking];
}

- (void)testTrackNoInternetErrorOnPage {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [AppManager trackNoInternetErrorOnPage:@"pagename"];
    
    OCMVerify([mockOmnitureManagerObj trackError:OCMOCK_ANY onPageWithName:OCMOCK_ANY contextInfo:OCMOCK_ANY]);
    
    [mockOmnitureManagerObj stopMocking];
}

- (void)testTrackNoInternetErrorBeforeLoginOnPage {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [AppManager trackNoInternetErrorBeforeLoginOnPage:@"pagename"];
    
    OCMVerify([mockOmnitureManagerObj trackError:OCMOCK_ANY onPageWithName:OCMOCK_ANY contextInfo:OCMOCK_ANY]);
    
    [mockOmnitureManagerObj stopMocking];
}

- (void)testTrackGenericAPIErrorOnPage {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [AppManager trackGenericAPIErrorOnPage:@"pagename"];
    
    OCMVerify([mockOmnitureManagerObj trackError:OCMOCK_ANY onPageWithName:OCMOCK_ANY contextInfo:OCMOCK_ANY]);
    
    [mockOmnitureManagerObj stopMocking];
}

- (void)testTrackGenericAPIErrorBeforeLoginOnPage {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [AppManager trackGenericAPIErrorBeforeLoginOnPage:@"pagename"];
    
    OCMVerify([mockOmnitureManagerObj trackError:OCMOCK_ANY onPageWithName:OCMOCK_ANY contextInfo:OCMOCK_ANY]);
    
    [mockOmnitureManagerObj stopMocking];
}

- (void)testTrackNoDataFoundErrorOnPage {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [AppManager trackNoDataFoundErrorOnPage:@"pagename"];
    
    OCMVerify([mockOmnitureManagerObj trackError:OCMOCK_ANY onPageWithName:OCMOCK_ANY contextInfo:OCMOCK_ANY]);
    
    [mockOmnitureManagerObj stopMocking];
}

- (void)testTrackNoDataFoundErrorBeforeLoginOnPage {
    
    id mockOmnitureManagerObj = OCMClassMock([OmnitureManager class]);
    
    [AppManager trackNoDataFoundErrorBeforeLoginOnPage:@"pagename"];
    
    OCMVerify([mockOmnitureManagerObj trackError:OCMOCK_ANY onPageWithName:OCMOCK_ANY contextInfo:OCMOCK_ANY]);
    
    [mockOmnitureManagerObj stopMocking];
}

- (void)testGetOmniDictionary {
    
    XCTAssertNotNil([AppManager getOmniDictionary], @"Omniture Data is nil");
}

- (void)testGetSecureCertificatesForPinning {
    
    XCTAssertNotNil([AppManager getSecureCertificatesForPinning], @"Certificates array is nil");
}

- (void)testSetDeepLinkedActivationCode {
    
    NSString *mockActivationCode = @"activationcode";
    [[AppManager sharedAppManager] setDeepLinkedActivationCode:mockActivationCode];
    
    XCTAssertTrue([[[AppManager sharedAppManager] getDeepLinkedActivationCode] isEqualToString:mockActivationCode]);
}

- (void)testSetDeeplinkAlertShownStatusOnSignin {
    
    [[AppManager sharedAppManager] setDeeplinkAlertShownStatusOnSignin:true];
    
    XCTAssertTrue([[AppManager sharedAppManager] getDeeplinkAlertShownStatusOnSignin]);
}

- (void)testSetIsVersionCheckFinished {
    
    [[AppManager sharedAppManager] setIsVersionCheckFinished:true];
    
    XCTAssertTrue([[AppManager sharedAppManager] getIsVersionCheckFinished]);
}

@end
